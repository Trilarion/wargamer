/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 * Weather Logic
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "weather.hpp"
#include "misc.hpp"
#include "myassert.hpp"
#include "scenario.hpp"
#include "wldfile.hpp"
#include "filebase.hpp"
#include "filecnk.hpp"
#include "savegame.hpp"
#include "resstr.hpp"

#ifdef DEBUG
LogFileFlush weatherLog("weather.log");
#endif

const int MaxCurrentConditions = UBYTE_MAX;

CampaignWeather::CampaignWeather() :
  d_weather(Sunny),
  d_groundConditions(Dry),
  d_conditionCounter(MaxCurrentConditions)
{
}

void CampaignWeather::setWeather(Weather w)
{
  ASSERT(w < Weather_HowMany);
  d_weather = w;

  const Table1D<SWORD>& table = scenario->getConditionsAdjustmentTable();

  adjustCurrentConditions(table.getValue(d_weather));
}

static CampaignWeather::GroundConditions getGroundCondition(int value)
{

  if(value < 44)
    return CampaignWeather::VeryMuddy;
  else if(value < 128)
    return CampaignWeather::Muddy;
  else
    return CampaignWeather::Dry;

}

void CampaignWeather::adjustCurrentConditions(int by)
{
  /*
   * Use int first in case value goes below 0
   */

  int currentConditions = d_conditionCounter;
  currentConditions += by;
  currentConditions = clipValue(currentConditions, 0, MaxCurrentConditions);
  d_conditionCounter = static_cast<UBYTE>(currentConditions);

  d_groundConditions = getGroundCondition(d_conditionCounter);

#ifdef DEBUG
  weatherLog.printf("Condition counter = %d", static_cast<int>(d_conditionCounter));
#endif
}

#if 0
static const char* weatherText[CampaignWeather::Weather_HowMany] = {
  "Sunny",
  "Light Rain",
  "Heavy Rain"
};

static const char* conditionsText[CampaignWeather::GroundConditions_HowMany] = {
  "Dry",
  "Muddy",
  "Very Muddy"
};
#endif

const char* CampaignWeather::getWeatherText(Weather w)
{
  ASSERT(w < Weather_HowMany);
  // return weatherText[w];
  return InGameText::get(IDS_WEATHER + w);
}

const char* CampaignWeather::getConditionsText(GroundConditions c)
{
  ASSERT(c < GroundConditions_HowMany);
  //  return conditionsText[c];
  return InGameText::get(IDS_GROUNDCONDITION_1 + c);
}

/*
 * file interface
 */

UWORD CampaignWeather::fileVersion = 0x0000;

Boolean CampaignWeather::read(FileReader& f)
{
   if(f.isAscii())
   {
      // ToDo
   }
   else
   {
      if(f.getMode() == SaveGame::SaveGame)
      {
         FileChunkReader fr(f, getChunkName());
         if(!fr.isOK())
            return False;

         UWORD version;
         f.getUWord(version);
         ASSERT(version <= fileVersion);

         UBYTE b;
         f.getUByte(b);
         d_weather = static_cast<Weather>(b);

         f.getUByte(b);
         d_groundConditions = static_cast<GroundConditions>(b);

         f.getUByte(d_conditionCounter);

      }
   }

  return f.isOK();
}

Boolean CampaignWeather::write(FileWriter& f) const
{
   if(f.isAscii())
   {
      // Todo
   }
   else
   {
      if(f.getMode() == SaveGame::SaveGame)
      {
         FileChunkWriter fc(f, getChunkName());

         f.putUWord(fileVersion);

         UBYTE b = static_cast<UBYTE>(d_weather);
         f.putUByte(b);

         b = static_cast<UBYTE>(d_groundConditions);
         f.putUByte(b);

         f.putUByte(d_conditionCounter);
      }
   }

  return f.isOK();
}

const char* CampaignWeather::getChunkName() const
{
  return weatherChunkName;
}

/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.2  2001/06/18 08:25:44  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
