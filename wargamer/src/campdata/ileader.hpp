/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef ILEADERS_HPP
#define ILEADERS_HPP

#include "sllist.hpp"
#include "gamedefs.hpp"
#include "cpidef.hpp"
#include "measure.hpp"

#include "CRC.hpp"

class FileReader;
class FileWriter;
class OrderBattle;

/*
 * Independent and Captured Leaders
 */


/*-----------------------------------------------------------
 * Independent leaders and lists
 */

class IndependentLeader : public SLink {
    ILeader d_leader;                     // who
    ITown d_town;                         // current location (if not traveling or at SHQ)
    Boolean d_atSHQ;                      // are we located at shq?
    ICommandPosition d_attachTo;          // attach to this unit
    TimeTick d_travelTime;                // travel time
    Boolean d_active;


  public:
    IndependentLeader() :
      d_leader(NoLeader),
      d_town(NoTown),
      d_atSHQ(False),
      d_attachTo(NoCommandPosition),
      d_travelTime(0),
      d_active(True) {}

    IndependentLeader(ILeader leader) :
      d_leader(leader),
      d_town(NoTown),
      d_atSHQ(False),
      d_attachTo(NoCommandPosition),
      d_travelTime(0),
      d_active(True) {}

    ~IndependentLeader() {}

    void town(ITown t)                  { d_town = t; }
    void atSHQ(Boolean f)               { d_atSHQ = f; }
    void attachTo(ICommandPosition cpi) { d_attachTo = cpi; }
    void travelTime(TimeTick tick)      { d_travelTime = tick; }
    void active(Boolean f)              { d_active = f; }

    ILeader leader()            const { return d_leader; }
    ITown town()                const { return d_town; }
    Boolean atSHQ()             const { return d_atSHQ; }
    ICommandPosition attachTo() const { return d_attachTo; }
    TimeTick travelTime()       const { return d_travelTime; }
    Boolean active()            const { return d_active; }
    
    /*
     * File Interface
     */

    Boolean read(FileReader& f, OrderBattle& ob);
    Boolean write(FileWriter& f, OrderBattle& ob) const;



    /*
     * Allocators
     */

    void* operator new(size_t size);
#ifdef _MSC_VER
   void operator delete(void* deadObject);
#else
    void operator delete(void* deadObject, size_t size);
#endif
    enum { ChunkSize = 10 };

};

class IndependentLeaderList : public SList<IndependentLeader> {
public:
    IndependentLeaderList() {}
    ~IndependentLeaderList() { reset(); }

    /*
     * File Interface
     */

    Boolean read(FileReader& f, OrderBattle& ob);
    Boolean write(FileWriter& f, OrderBattle& ob) const;
};

/*---------------------------------------------------------------
 * Captured leaders and lists
 */

class CapturedLeader : public SLink {
    ILeader d_leader;
  public:
    CapturedLeader() :
      d_leader(NoLeader) {}
    CapturedLeader(ILeader leader) :
      d_leader(leader) {}
    ~CapturedLeader() {}

    ILeader capturedLeader() const { return d_leader; }

    /*
     * File Interface
     */

    Boolean read(FileReader& f, OrderBattle& ob);
    Boolean write(FileWriter& f, OrderBattle& ob) const;

    /*
     * Allocators
     */

    void* operator new(size_t size);
#ifdef _MSC_VER
    void operator delete(void* deadObject);
#else
    void operator delete(void* deadObject, size_t size);
#endif
    enum { ChunkSize = 50 };
};

class CapturedLeaderList : public SList<CapturedLeader> {
public:
    CapturedLeaderList() {}
    ~CapturedLeaderList() { reset(); }

    /*
     * File Interface
     */

    Boolean read(FileReader& f, OrderBattle& ob);
    Boolean write(FileWriter& f, OrderBattle& ob) const;
};

#endif
