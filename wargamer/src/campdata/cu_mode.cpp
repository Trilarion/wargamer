/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
#include "stdinc.hpp"
#include "cu_mode.hpp"
#include "campdint.hpp"
#include "compos.hpp"
#include "town.hpp"
#include "route.hpp"
#ifdef DEBUG
#include "unitlog.hpp"
#endif

#if !defined(EDITOR)
Boolean CP_ModeUtil::setMode(CampaignData* campData, ICommandPosition cpi, CampaignMovement::CP_Mode mode)
{
  ASSERT(mode >= 0);
  ASSERT(mode < CampaignMovement::CPM_HowMany);
  // CampaignMovement::CP_Mode newMode = static_cast<CampaignMovement::CP_Mode>(mode);

  ASSERT(cpi != NoCommandPosition);
  CommandPosition* cp = campData->getCommand(cpi);

#ifdef DEBUG
  CampaignMovement::CP_Mode oldMode = cp->getMode();
#endif

  Boolean result = cp->setMode(mode);
  if(result)
  {
     /*
      * If installing supply or building fort, reset town
      */

     if(cp->atTown())
     {
       Town& town = campData->getTown(cp->getTown());
       if(town.hasFortUpgrader())
         town.setFortUpgrader(NoCommandPosition);

       else if(town.hasSupplyInstaller())
         town.setSupplyInstaller(NoCommandPosition);

         /*
          * Set town's garrison/siege/raid mode
          */

        switch(mode)
        {
            case CampaignMovement::CPM_Garrison:
                town.setGarrison(true);
                break;
            case CampaignMovement::CPM_Sieging:
                town.setSiege(true);
                break;
            case CampaignMovement::CPM_Raiding:
                town.setRaid(true);
                break;
        }

     }

#ifdef DEBUG
     cuLog.printf("\nChanging mode of %s", (const char*)campData->getUnitName(cpi).toStr());
     cuLog.printf("\n From: \"%s\"", CampaignMovement::movementModeTable[oldMode].modeName);
     cuLog.printf("\n To \"%s\"", CampaignMovement::movementModeTable[mode].modeName);
#endif

     cp->startActivity(campData->getTick());

     if(cp->isRetreating())
       cp->resetActivityCount();
  }

  // if we're moving (and not avoiding or retreating), plot new route
  if(!cpi->isAvoiding() &&
     !cpi->isRetreating() &&
     cpi->shouldMove())
  {
    RouteList& rl = cpi->currentRouteList();
    CampaignRouteUtil::plotUnitRoute(campData, cpi);
  }

  return result;
}
#endif


/*
 *----------------------------------------------------------------------
 * $Log$
 *----------------------------------------------------------------------
 */
