/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
#include "stdinc.hpp"
#include "townordr.hpp"
#include "filebase.hpp"
#include "fsalloc.hpp"
// #include "c_obutil.hpp"
//#include "armybase.hpp"


#ifdef DEBUG
static FixedSizeAlloc<Town_COrder> townOrderAlloc("Town_COrder");
#else
static FixedSizeAlloc<Town_COrder> townOrderAlloc;
#endif

void* Town_COrder::operator new(size_t size)
{
  return townOrderAlloc.alloc(size);
}

#ifdef _MSC_VER
void Town_COrder::operator delete(void* deadObject)
{
  townOrderAlloc.release(deadObject);
}                       
#else
void Town_COrder::operator delete(void* deadObject, size_t size)
{
  townOrderAlloc.release(deadObject, size);
}                       
#endif

inline bool operator >> (FileReader& f, Town_COrder::Type& t)
{
   UBYTE b;
   f >> b;

   t = static_cast<Town_COrder::Type>(b);
   return true;
}

inline bool operator << (FileWriter& f, const Town_COrder::Type& t)
{
   return f.putUByte(t);
}

const UWORD c_toFileVersion = 0x0001;

Boolean Town_COrder::read(FileReader& f)
{
  UWORD version;
  f >> version;
  ASSERT(version <= c_toFileVersion);

  if(version < 0x0001)
  {
    UWORD rubbish;
    f >> rubbish;
  }
  if(version >= 0x0001)
  {
    f >> d_town;
  }
  f >> d_type;
  f >> d_tillWhen;

  return f.isOK();
}

Boolean Town_COrder::write(FileWriter& f) const
{
  f << c_toFileVersion;
//  f << d_cpIndex;
  f << d_town; 
  f << d_type;
  f << d_tillWhen;

  return f.isOK();
}

#if 0
#if 0
ICommandPosition Town_COrder::cpi(OrderBattle& ob) const
{
  return CampaignOBUtil::cpIndexToCPI(d_cpIndex, ob);
}
#else
ICommandPosition Town_COrder::cpi(ArmyBase& army) const
{
  return army.command(d_cpIndex);
}
#endif
#endif

const UWORD c_tolFileVersion = 0x0000;

Boolean Town_COrderList::read(FileReader& f)
{
  UWORD version;
  f >> version;
  ASSERT(version <= c_tolFileVersion);

  int nNodes;
  f >> nNodes;

  while(nNodes--)
  {
    Town_COrder* to = new Town_COrder;
    ASSERT(to);

    to->read(f);
    append(to);
  }

  return f.isOK();
}

Boolean Town_COrderList::write(FileWriter& f) const
{
  f << c_tolFileVersion;

  int nNodes = entries();
  f << nNodes;

  SListIterR<Town_COrder> iter(this);
  while(++iter)
  {
    iter.current()->write(f);
  }

  return f.isOK();
}

/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.2  2001/06/18 08:25:44  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
