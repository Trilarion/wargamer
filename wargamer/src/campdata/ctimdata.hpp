/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef CTIMDATA_HPP
#define CTIMDATA_HPP

#ifndef __cplusplus
#error ctimdata.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 * Campaign Time Data
 *
 *----------------------------------------------------------------------
 */

#include "cdatadll.h"
#include "critical.hpp"    // For SharedData
#include "camptime.hpp"    // For CampaignTime
#include "measure.hpp"     // For TimeTick

using Greenius_System::DateDefinitions;
using Greenius_System::TimeDefinitions;

class CampaignTimeData :
//    private DateDefinitions,
//    private TimeDefinitions
    public DateDefinitions,
    public TimeDefinitions
{
   private:
      SharedData d_timeLock;

      TimeTick d_campaignTick;  // Number of campaign Ticks since beginning of StartYear

      /*
       * These values are the equivalents of d_campaignTick
       */

      CampaignTime d_theTime;   // Time being processed in hours
      Date d_date;              // Date being processed
      Time d_time;              // Time during current runCampaign();
      // CampaignTime fineTime;  // Time incremented in 1/10 section
   public:
      CAMPDATA_DLL CampaignTimeData();
      virtual ~CampaignTimeData() { }

      const CampaignTime& getCTime() const { return d_theTime; }
      const Date& getDate() const { return d_date; }
      const Time& getTime() const { return d_time; }
      CAMPDATA_DLL void setTimeTick(TimeTick t);
      TimeTick getTick() const { return d_campaignTick; }
      const char* asciiTime(char* buffer = 0) const;
   // CampaignTime& getFineTime() { return fineTime; }
      CAMPDATA_DLL void initTime(const Date& date);
      CAMPDATA_DLL void initTime(Year startYear, GameTick timeValue);
};

#endif /* CTIMDATA_HPP */


