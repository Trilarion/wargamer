/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef CLEADER_HPP
#define CLEADER_HPP

#ifndef __cplusplus
#error cleader.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *	Campaign Leader Interface
 *
 * This is just a wrapper for the OB\GenericLeader
 *
 *----------------------------------------------------------------------
 */

#include "leader.hpp"		// from ob
// #include "cp.hpp"
#include "cpidef.hpp"

/*
 * Leader has no more data elements than GLeader, so can be
 * fairly safely cast.
 *
 * The purpose of this class is so that functions that return
 * generic things can be overloaded to return a campaign one.
 *
 * e.g. leader::getSupremeHQ will return a Campaign CommandPosition
 *      instead of a generic one.
 */

class Leader : public GLeader
{
	public:
		Leader() : GLeader() { }
		~Leader() { }

		GLeader* generic() { return this; }
		const GLeader* generic() const { return this; }

		ICommandPosition command() const { return campCP(GLeader::command()); }
		ICommandPosition getCommand() const { return command(); }

};

/*
 * Utility function to convert from GenericLeader to CampaignLeader
 */


inline Leader* campLeader(GLeader* gleader)
{
	return static_cast<Leader*>(gleader);
}

inline const Leader* campLeader(const GLeader* gleader)
{
	return static_cast<const Leader*>(gleader);
}

// inline ILeader campLeader(RefGLeader& gleader)
//{
//	return campLeader(gleader);
//}

inline ILeader campLeader(const RefGLeader& gleader)
{
	return static_cast<Leader*>(static_cast<GLeader*>(gleader));
}

inline ConstILeader campLeader(const ConstRefGLeader& gleader)
{
	return static_cast<const Leader*>(static_cast<const GLeader*>(gleader));
}


inline GLeader* generic(const ILeader& leader)
{
	if(leader == NoLeader)
		return 0;
	else
		return leader->generic();
}

inline const GLeader* generic(const ConstILeader& leader)
{
	if(leader == NoLeader)
		return 0;
	else
		return leader->generic();
}




#endif /* CLEADER_HPP */

