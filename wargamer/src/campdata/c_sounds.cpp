/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*-------------------------------------------------------------------------------------------------------------------

        File : C_SOUNDS
        Description : Sound library for campaign game
        
-------------------------------------------------------------------------------------------------------------------*/

#include "stdinc.hpp"
#include "c_sounds.hpp"



static const CampaignSoundTableEntry campsound1 = CampaignSoundTableEntry("sounds\\campaign\\messagebell.wav", 0, 4);
static const CampaignSoundTableEntry campsound2 = CampaignSoundTableEntry("sounds\\campaign\\drumroll.wav", 0, 4);
static const CampaignSoundTableEntry campsound3 = CampaignSoundTableEntry("sounds\\campaign\\marching.wav", 0, 4);
static const CampaignSoundTableEntry campsound4 = CampaignSoundTableEntry("sounds\\campaign\\hearbattle.wav", 0, 4);
static const CampaignSoundTableEntry campsound5 = CampaignSoundTableEntry("sounds\\campaign\\townfire.wav", 0, 4);
static const CampaignSoundTableEntry campsound6 = CampaignSoundTableEntry("sounds\\campaign\\mortar.wav", 0, 4);
static const CampaignSoundTableEntry campsound7 = CampaignSoundTableEntry("sounds\\campaign\\bugle.wav", 0, 4);
static const CampaignSoundTableEntry campsound8 = CampaignSoundTableEntry("sounds\\campaign\\cheering.wav", 0, 4);

static const CampaignSoundTableEntry campsound9 = CampaignSoundTableEntry("sounds\\common\\menu_popup.wav", 0, 4);
static const CampaignSoundTableEntry campsound10 = CampaignSoundTableEntry("sounds\\common\\menu_select.wav", 0, 4);
static const CampaignSoundTableEntry campsound11 = CampaignSoundTableEntry("sounds\\common\\window_open.wav", 0, 4);
static const CampaignSoundTableEntry campsound12 = CampaignSoundTableEntry("sounds\\common\\window_close.wav", 0, 4);

const CampaignSoundTableEntry * CampaignSoundTable[] = {
    &campsound1,
    &campsound2,
    &campsound3,
    &campsound4,
    &campsound5,
    &campsound6,
    &campsound7,
    &campsound8,
   &campsound9,
   &campsound10,
   &campsound11,
   &campsound12,
    0
};












/*
 *----------------------------------------------------------------------
 * $Log$
 *----------------------------------------------------------------------
 */
