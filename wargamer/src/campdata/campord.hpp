/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef CAMPORD_H
#define CAMPORD_H


#ifndef __cplusplus
#error campord.hpp is for use with C++
#endif

#ifdef __WATCOM_CPLUSPLUS__
#pragma warning 549 9     // disable sizeof warnings
//#pragma warning 549 5
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1995, Steven Morle-Green, All Rights Reserved
 *----------------------------------------------------------------------
 *
 *	Campaign Orders
 *
 *----------------------------------------------------------------------
 */

#include "cdatadll.h"
// #include <string.hpp>
#include "gamedefs.hpp"
#include "cpidef.hpp"
#include "measure.hpp"
#include "sllist.hpp"
#include "loss_def.hpp"
#include "camptime.hpp"
#include "refptr.hpp"
#include "fsalloc.hpp"

/*
 * Forward References
 */

class CampaignOrder;
#define OrderBase CampaignOrder			// Backward compatibility
#define OrderValues CampaignOrder		// Backward compatibility
#define CompleteOrder CampaignOrder

struct TipTextID;
class OrderCheckData;	// Defined in realord.cpp
struct AdvancedOrders;
// class CampaignLogicOwner;
class CampaignData;
class OrderBattle;
//class Date;

/*
 * Methods of getting description
 */

enum OD_TYPE {
	OD_BRIEF,
	OD_LONG
};

/*
 * Some Useful Types
 */

namespace Orders {
	namespace Type {
		enum Value {
			First = 0,

			MoveTo = First,
			Hold,					// Defending or sieging
			RestRally,
			Attach,    			// Take Command if it is a leader
			AttachSP,  			// Take over StrengthPoint
			Leader,    	      // Used for detaching leaders
			Garrison,			// Garrison town
			InstallSupply,		// Install Supply Centre at current town
			UpgradeFort,		// Install or Upgrade fortifications
			BlowBridge,
			RepairBridge,
			RemoveDepot,     // Remove a depot

			/*
			 * Note: If changing this enum, then please update:
			 *		cu_order : s_orerImp[]
			 *
			 *------------------------------------
			 *		campord.cpp: orderTable
			 *		campord.cpp: makeOrder()
			 *
			 *
			 *		campord.cpp:	orderTable[]
			 * 	campunit.cpp:	orderUpdated(), isOrderAllowed()
			 *		unittb.cpp:		orderToImage[], orderTip[], unitOrderPopupMenu()
			 *    nap1813.rc, acw.rc, etc... Add tooltips, popup menu, etc...
			 */

			HowMany
		};

		/*
		 * Note: any code that uses CommandPosition::setCurrentOrder()
		 *       must use makeStaticOrder in order to prevent memory leaks
		 */

		// CampaignOrder* makeOrder(Value t);
		// const CampaignOrder* makeStaticOrder(Value t);
#ifdef DEBUG	// For backwards compatibility
		CAMPDATA_DLL const CampaignOrder* makeStaticOrder(Value t);
#endif

		CAMPDATA_DLL Boolean findMenuID(int id, Value& t);
		CAMPDATA_DLL int getMenuID(Value t);
		CAMPDATA_DLL Boolean canGiveToUnit(Value t);
//		Boolean canGiveToLeader(Value t);
		CAMPDATA_DLL Boolean canGiveOnArrival(Value t);

		CAMPDATA_DLL int getIconImage(Value t);

		//--- moved to realord.cpp/CampaignOrderUtil
		// Boolean allowed(Value t, ICommandPosition cpi, CampaignData* campData);
		CAMPDATA_DLL const TipTextID* getToolTip(Value t);
		CAMPDATA_DLL const char* text(Value t);
	};

	/*
	 * Aggression
	 */

	class Aggression {
		public:
			enum Value {
				First = 0,
				Timid = First,				// Run away...
				DefendSmall,		// Defend if enemy similar/smaller, else run
				AttackSmall,		// Attack if enemy similar/smaller, else defend
				Attack,				// Attack anything

				HowMany,
				Default = DefendSmall
			};

			CAMPDATA_DLL static const char* getName(Value ag);
	};

	/*
	 * This could be moved to realord.hpp
	 * which would make it hidden from more things
	 */

	class Vias {
	public:
		enum { MaxVia = 3 };
	private:
		ITown viaTown[MaxVia];
	public:
		CAMPDATA_DLL Vias();

		CAMPDATA_DLL ITown get(int i) const;
		CAMPDATA_DLL void set(int i, ITown t);
		CAMPDATA_DLL void insert(int i, ITown t);
		CAMPDATA_DLL void remove(int i);
		CAMPDATA_DLL ITown get();				// Get and delete 1st entry
		CAMPDATA_DLL void clear();
		CAMPDATA_DLL int getNVias() const;

		CAMPDATA_DLL Vias& operator = (const Vias& v);

		CAMPDATA_DLL friend bool operator != (const Vias& v1, const Vias& v2); // { return !(v1 == v2); }

		CAMPDATA_DLL Boolean read(FileReader& f);
		CAMPDATA_DLL Boolean write(FileWriter& f) const;
	};

	// advanced order class for allowed advanced orders
	class AdvancedOrders {
	  public:

		 enum MoveHow {           // how are we marching
			  MoveHow_First = 0,
			  Normal = MoveHow_First,
			  Easy,
			  Force,
			  MoveHow_HowMany
		 };

#if 0
		 enum OrderOnArrival {                    // what to do upon completion of orders
			  OrderOnArrival_First = 0,
			  Hold = OrderOnArrival_First,
			  RestRally,
			  Garrison,
			  Attach,
			  OrderOnArrival_HowMany
		 };
#endif

	  private:

		 enum {
			 SoundGuns       = 0x01,  // will march to the sound of battle
			 Pursue          = 0x02,  // will pursue after battle
			 SiegeActive     = 0x04,  // will conduct active siege operations
			 DropOffGarrison = 0x08,  // will try to drop-off garrisons if routed or withdrawing
			 AutoStorm       = 0x10   // will try immediate storm (level-1 forts only)

		 };

		 UBYTE d_flags;

		 MoveHow d_moveHow;
		 Type::Value d_orderOnArrival;
//		 OrderOnArrival d_orderOnArrival;

		 void setFlags(UBYTE mask, Boolean f)
		 {
			if(f)
			  d_flags |= mask;
			else
			  d_flags &= ~mask;
		 }

	  public:
		 AdvancedOrders() { clear(); }
		 CAMPDATA_DLL void clear();


		 void soundGuns(Boolean f) { setFlags(SoundGuns, f); }
		 void pursue(Boolean f) { setFlags(Pursue, f); }
		 void siegeActive(Boolean f) { setFlags(SiegeActive, f); }
		 void dropOffGarrison(Boolean f) { setFlags(DropOffGarrison, f); }
		 void autoStorm(Boolean f) { setFlags(AutoStorm, f); }

		 void moveHow(MoveHow mh) { d_moveHow = mh; }
//		 void orderOnArrival(OrderOnArrival ooa) { d_orderOnArrival = ooa; }
		 void orderOnArrival(Type::Value ooa) { d_orderOnArrival = ooa; }


		 UBYTE flags() const { return d_flags; }
		 void flags(UBYTE f) { d_flags = f; }

		 Boolean soundGuns() const { return (d_flags & SoundGuns) != 0; }
		 Boolean pursue() const { return (d_flags & Pursue) != 0; }
		 Boolean siegeActive() const { return (d_flags & SiegeActive) != 0; }
		 Boolean dropOffGarrison() const { return (d_flags & DropOffGarrison) != 0; }
		 Boolean autoStorm() const { return (d_flags & AutoStorm) != 0; }

		 MoveHow moveHow() const { return d_moveHow; }
//		 OrderOnArrival orderOnArrival() const { return d_orderOnArrival; }
		 Type::Value orderOnArrival() const { return d_orderOnArrival; }

		 Boolean shouldNormalMarch() const { return (d_moveHow == Normal); }
		 Boolean shouldForceMarch() const { return (d_moveHow == Force); }
		 Boolean shouldEasyMarch() const { return (d_moveHow == Easy); }

		 Boolean holdOnArrival() const { return (d_orderOnArrival == Type::Hold); }
		 Boolean restRallyOnArrival() const { return (d_orderOnArrival == Type::RestRally); }
		 Boolean garrisonOnArrival() const { return (d_orderOnArrival == Type::Garrison); }
		 Boolean attachOnArrival() const { return (d_orderOnArrival == Type::Attach); }
		 Boolean installSupplyOnArrival() const { return (d_orderOnArrival == Type::InstallSupply); }
		 Boolean upgradeFortOnArrival() const { return (d_orderOnArrival == Type::UpgradeFort); }
		 Boolean blowBridgeOnArrival() const { return (d_orderOnArrival == Type::BlowBridge); }
		 Boolean repairBridgeOnArrival() const { return (d_orderOnArrival == Type::RepairBridge); }

		 /*
		  * file interface
		  */

		 CAMPDATA_DLL Boolean read(FileReader& f);
		 CAMPDATA_DLL Boolean write(FileWriter& f) const;

		 /*
		  * static functions
		  */

		 CAMPDATA_DLL static const char* getOrderOnArrivalName(Type::Value v);
		 CAMPDATA_DLL static const char* getMoveHowName(MoveHow v);

		 // these need to be go from resource
		 CAMPDATA_DLL static const char* pursueText(); // { return "Pursue after Battle"; }
		 CAMPDATA_DLL static const char* siegeActiveText(); // { return "Active Siege"; }
		 CAMPDATA_DLL static const char* soundGunsText(); // { return "March To Guns"; }
		 CAMPDATA_DLL static const char* dropOffGarrisonText();
		 CAMPDATA_DLL static const char* autoStormText();
	};

	class Posture {
	  public:
		 enum Type {
			First = 0,

			Offensive = First,
			Defensive,

			HowMany
		 };

		 CAMPDATA_DLL static const char* postureName(Type t);

	};

};		// namespace




/*
 *  New CampaignOrder
 */

class CampaignOrder
{
	 	Orders::Type::Value d_type;
	 	Orders::Aggression::Value d_aggression;
		Orders::Vias d_vias;
		Orders::AdvancedOrders d_advancedOrders;
	 	Orders::Posture::Type d_posture;

	 	ITown d_destTown;
	 	ConstICommandPosition d_destUnit;
	 	ISP d_destSP;

		enum {
		  AdvancedOnly = 0x01,  // if set we just copy advanced to current order
        Changed      = 0x02
		};

		UBYTE d_flags;
		Date d_date;     // effective date for order to be carried out

		void setFlag(UBYTE mask, Boolean f)
		{
		  if(f)
			 d_flags |= mask;
		  else
			 d_flags &= ~mask;
		}

	 public:
		CampaignOrder() :
			d_type(Orders::Type::Hold),
			d_aggression(Orders::Aggression::DefendSmall),
			d_vias(),
			d_advancedOrders(),
			d_posture(Orders::Posture::Defensive),
			d_destTown(NoTown),
			d_destUnit(NoCommandPosition),
			d_destSP(NoStrengthPoint),
			d_flags(0),
         d_date(0,0,0)
		{
		}

		virtual ~CampaignOrder() { }

		CAMPDATA_DLL CampaignOrder& operator = (const CampaignOrder& rhs);


		CAMPDATA_DLL void clearValues();
			// Set to default values

		void setType(Orders::Type::Value t)
      {
         if(d_type != t)
         {
            d_type = t;
            changed(True);
            advancedOnly(False);
         }
      }
		Orders::Type::Value getType() const { return d_type; }

		Orders::Aggression::Value getAggressLevel() const { return d_aggression; }
		void setAggressLevel(Orders::Aggression::Value v) { d_aggression = v; }
		CAMPDATA_DLL const char* getAggressDescription() const;

		CAMPDATA_DLL ITown getNextDest() const; // { return NoTown; }
		CAMPDATA_DLL ITown advanceNextDest();  // { return NoTown; }

		ITown getDestTown() const { return d_destTown;}
		void setDestTown(ITown t) { d_destTown = t; d_destUnit == NoCommandPosition; }

		const ConstICommandPosition& getTarget() const { return d_destUnit; }
		const ConstICommandPosition& getTargetUnit() const { return getTarget(); }

		void setTarget(ConstParamCP cpi) { d_destUnit = cpi; d_destTown = NoTown; }
		void setTargetUnit(ConstParamCP cpi) { setTarget(cpi);}
      void clearTargetUnit() { d_destUnit = NoCommandPosition; }
		Boolean hasTargetUnit() const { return (d_destUnit != NoCommandPosition); }

		ISP getTargetSP() const { return d_destSP; }
		void setTargetSP(const ISP& isp) { d_destSP = isp; }

		bool advancedOnly() const { return (d_flags & AdvancedOnly); }
		void advancedOnly(bool f) { setFlag(AdvancedOnly, f); }
      bool changed() const { return (d_flags & Changed) != 0; }
      void changed(bool f) { setFlag(Changed, f); }

		void copyAOFlags(const CampaignOrder& co)
		{
		  d_advancedOrders.flags(co.d_advancedOrders.flags());
		}

		CAMPDATA_DLL Boolean isMoveOrder() const;
		CAMPDATA_DLL Boolean requiresDetach() const;
		Boolean hasRoute() const  { return isMoveOrder(); }
		Boolean hasDestTown() const  { return (d_destTown != NoTown); }
		Boolean hasDestUnit() const  { return (d_destUnit != NoCommandPosition); }
		CAMPDATA_DLL Boolean hasTargetSP() const;  //{ return Boolean(d_destSP != NoStrengthPoint); }
		CAMPDATA_DLL Boolean isAttach() const;
		Boolean hasAdvanced() const   { return True; }

		void setSoundGuns(Boolean f)     { d_advancedOrders.soundGuns(f); }
		Boolean getSoundGuns() const     { return d_advancedOrders.soundGuns(); }
		void setPursue(Boolean f) 		 {	d_advancedOrders.pursue(f); }
		Boolean getPursue() const		    {	return d_advancedOrders.pursue(); }
		void setSiegeActive(Boolean f)   {	d_advancedOrders.siegeActive(f); }
		Boolean getSiegeActive() const   {	return d_advancedOrders.siegeActive(); }
		void setDropOffGarrison(Boolean f)   {	d_advancedOrders.dropOffGarrison(f); }
		Boolean getDropOffGarrison() const   {	return d_advancedOrders.dropOffGarrison(); }
		void setAutoStorm(Boolean f)   {	d_advancedOrders.autoStorm(f); }
		Boolean getAutoStorm() const   {	return d_advancedOrders.autoStorm(); }

		// these need to be eliminated
		void setDigIn(Boolean f) 	 {} //      {	d_advancedOrders.digIn(f); }
		//-- Boolean getDigIn() const	   { return False; } //    {	return d_advancedOrders.digIn(); }
		//-- void setAutoStorm(Boolean f)  {} //	 {	d_advancedOrders.autoStorm(f); }
		//-- Boolean getAutoStorm() const	  { return True; } //  {	return d_advancedOrders.autoStorm(); }

		void setMoveHow(Orders::AdvancedOrders::MoveHow how) { d_advancedOrders.moveHow(how); }
		Orders::AdvancedOrders::MoveHow getMoveHow() const   { return d_advancedOrders.moveHow(); }
//		void setOrderOnArrival(Orders::AdvancedOrders::OrderOnArrival order) { d_advancedOrders.orderOnArrival(order); }
//		Orders::AdvancedOrders::OrderOnArrival getOrderOnArrival() const { return d_advancedOrders.orderOnArrival(); }
		void setOrderOnArrival(Orders::Type::Value order) { d_advancedOrders.orderOnArrival(order); }
		Orders::Type::Value getOrderOnArrival() const { return d_advancedOrders.orderOnArrival(); }

		Orders::AdvancedOrders& getAdvancedOrders() { return d_advancedOrders; }   //lint !e1536 ... exposing member
		const Orders::AdvancedOrders& getAdvancedOrders() const { return d_advancedOrders; }
		void copyAdvanced(const Orders::AdvancedOrders& ao) { d_advancedOrders = ao; }

		Boolean shouldForceMarch() const { return d_advancedOrders.shouldForceMarch(); }
		Boolean shouldEasyMarch() const { return d_advancedOrders.shouldEasyMarch(); }
		Boolean shouldNormalMarch() const { return d_advancedOrders.shouldNormalMarch(); }

		Boolean holdOnArrival() const { return d_advancedOrders.holdOnArrival(); }
		Boolean restRallyOnArrival() const { return d_advancedOrders.restRallyOnArrival(); }
		Boolean garrisonOnArrival() const { return d_advancedOrders.garrisonOnArrival(); }
		Boolean attachOnArrival() const { return d_advancedOrders.attachOnArrival(); }
		Boolean installSupplyOnArrival() const { return d_advancedOrders.installSupplyOnArrival(); }
		Boolean upgradeFortOnArrival() const { return d_advancedOrders.upgradeFortOnArrival(); }
		Boolean blowBridgeOnArrival() const { return d_advancedOrders.blowBridgeOnArrival(); }
		Boolean repairBridgeOnArrival() const { return d_advancedOrders.repairBridgeOnArrival(); }

		const Orders::Vias* getVias() const { return &d_vias; }
		Orders::Vias* getVias() { return &d_vias; }   //lint !e1536 ... exposing member
		ITown getVia(int i) const { return d_vias.get(i); }
		void copyVia(const Orders::Vias& src);
		int getNVias() const { return d_vias.getNVias(); }
		void setVia(ITown t, int i) { d_vias.set(i, t); }


		void setDate(const Date& date) { d_date = date; }
		const Date& getDate() const { return d_date; }
		Boolean isTimedOrder() const { return d_date.year != 0; }
		CAMPDATA_DLL Boolean shouldCarryOut(const Date& date); // { return !(d_date > date); }

		void posture(Orders::Posture::Type p) { d_posture = p; }
		Orders::Posture::Type posture() const { return d_posture; }
		void setPosture(Orders::Posture::Type p) { posture(p); }
		Orders::Posture::Type getPosture() const { return posture(); }

		void makeHold()
		{
		  setType(Orders::Type::RestRally);
		  setDestTown(NoTown);
		  setTargetUnit(NoCommandPosition);
		  setOrderOnArrival(Orders::Type::Hold);
		  d_vias.clear();
		}


//		friend bool operator == (const AdvancedOrder& o1, const AdvancedOrder& o2);
//		friend bool operator != (const CampaignOrder& o1, const CampaignOrder& o2);
#if 0
		{
			return !(o1 == o2);
		}
#endif
		/*
		 * File interface
		 */

		CAMPDATA_DLL Boolean read(FileReader& f, OrderBattle& ob);
		CAMPDATA_DLL Boolean write(FileWriter& f, OrderBattle& ob) const;

		/*
		* Message packing / unpacking for multiplayer
		*/

		CAMPDATA_DLL int pack(void * buffer, OrderBattle * ob);
		CAMPDATA_DLL void unpack(void * buffer, OrderBattle * ob);
};

/*
 * Order as sent through the order queue
 */

class CampaignOrderNode : public SLink
{
	public:
		enum Mode { Messenger, InTray };


	private:


	public:
		CampaignOrderNode() :
		  d_arrival(0),
		  d_tryAgainAt(0),
		  d_mode(Messenger),
		  d_order()
		  {}

		CAMPDATA_DLL CampaignOrderNode(const CampaignOrder* order, Mode _mode, TimeTick _arrival, TimeTick _tryAgainAt);

		CAMPDATA_DLL static void* operator new(size_t size);
#ifdef _MSC_VER
   void operator delete(void* deadObject);
#else
		CAMPDATA_DLL static void operator delete(void* deadObject, size_t size);
#endif
		// Need some access functions!

		const CampaignOrder* order() const { return &d_order; }
		Mode mode() const { return d_mode; }
		void mode(Mode mode) { d_mode = mode; }

		TimeTick arrival() const { return d_arrival; }
		void arrival(TimeTick tick) { d_arrival = tick; }

		TimeTick tryAgainAt() const { return d_tryAgainAt; }
		void tryAgainAt(TimeTick tick) { d_tryAgainAt = tick; }

		/*
		 * File interface
		 */

		CAMPDATA_DLL Boolean read(FileReader& f, OrderBattle& ob);
		CAMPDATA_DLL Boolean write(FileWriter& f, OrderBattle& ob) const;

	private:

		enum { ChunkSize = 100 };		// For allocator
		static FixedSize_Allocator allocator;

		CampaignOrder	d_order;
		TimeTick d_arrival;
		TimeTick d_tryAgainAt;
		Mode d_mode;
};


/*
 * List of orders in transit for unit
 * the currentOrder is what he is acting on
 */


class CampaignOrderList
{
	public:
		typedef SList<CampaignOrderNode> CampaignOrderNodeList;

	private:
	 TimeTick d_acting;
	 CampaignOrder d_currentOrder;
	 CampaignOrder d_actioningOrder;
	 CampaignOrderNodeList d_orderQueue;
  public:
	 CAMPDATA_DLL CampaignOrderList();
	 CampaignOrderList(const CampaignOrderList& list) { copy(list); }    //lint !e1926 ... default intialiser called
	 CAMPDATA_DLL ~CampaignOrderList();

	 CampaignOrderList& operator = (const CampaignOrderList& list) { if(&list != this) copy(list); return *this; }

	 CampaignOrder* getCurrentOrder() { return &d_currentOrder; }     //lint !e1536 ... Exposing low member
	 const CampaignOrder* getCurrentOrder() const { return &d_currentOrder; }
	 CAMPDATA_DLL void setCurrentOrder(const CampaignOrder* newOrder);

	 CAMPDATA_DLL void addOrder(const CampaignOrder* newOrder, TimeTick arrival, TimeTick tryAgainAt);
	 CAMPDATA_DLL const CampaignOrder* getLastOrder() const;

	 CampaignOrderNode* first() { return d_orderQueue.first(); }
	 CampaignOrderNode* next() { return d_orderQueue.next(); }
	 void remove() { d_orderQueue.remove(); }

	 int entries() const { return d_orderQueue.entries(); }

     void reset();
	 /*
	  * Actioning Order functions
	  */

	 CAMPDATA_DLL void setActioningOrder(const CampaignOrder* newOrder, TimeTick when);
	 CampaignOrder* getActioningOrder() { return &d_actioningOrder; }     //lint !e1536 ... Exposing low member
	 const CampaignOrder* getActioningOrder() const { return &d_actioningOrder; }
	 CAMPDATA_DLL void resetActing();
	 // void setActing(TimeTick a) { d_acting = a; }
	 TimeTick getActing() const { return d_acting; }
	 Boolean isActing() const { return (d_acting != 0); }

	 CAMPDATA_DLL Boolean hasIntray() const;
	 CAMPDATA_DLL Boolean hasMessenger() const;


	 /*
	  * File interface
	  */

	 CAMPDATA_DLL Boolean read(FileReader& f, OrderBattle& ob);
	 CAMPDATA_DLL Boolean write(FileWriter& f, OrderBattle& ob) const;


	 const CampaignOrderNodeList* nodeList() const { return &d_orderQueue; }

	class ConstIter
	{
		public:
			ConstIter(const CampaignOrderList& list) : d_iter(list.nodeList())
			{
			}

			bool operator ++() { return ++d_iter; }
			const CampaignOrderNode* current() const { return d_iter.current(); }

		private:
			SListIterR<CampaignOrderNode> d_iter;
	};

	private:
	    CAMPDATA_DLL void copy(const CampaignOrderList& list);
};


/*
 * Prevent crashing by using #define instead of constants and typedefs
 */


#define AGGRESS_HowMany 		Orders::Aggression::HowMany
#define AGGRESS_TIMID			Orders::Aggression::Timid
#define AGGRESS_DEFEND_SMALL	Orders::Aggression::DefendSmall
#define AGGRESS_ATTACK_SMALL	Orders::Aggression::AttackSmall
#define AGGRESS_ATTACK 			Orders::Aggression::Attack


// #define ORDER_MOVETO					= Orders::Type::MoveTo
// #define ORDER_HOLD					= Orders::Type::Hold
// #define ORDER_REST_RALLY			= Orders::Type::RestRally
// #define ORDER_ROUT					= Orders::Type::Rout
// #define ORDER_ATTACH					= Orders::Type::Attach
// #define ORDER_ATTACHSP				= Orders::Type::AttachSP
// #define ORDER_LEADER					= Orders::Type::Leader
// #define ORDER_GARRISON				= Orders::Type::Garrison
// #define ORDER_SURRENDER				= Orders::Type::Surrender
// #define ORDER_STORM					= Orders::Type::Storm
// #define ORDER_SALLY					= Orders::Type::Sally
// #define ORDER_INSTALLSUPPLY		= Orders::Type::InstallSupply
// #define ORDER_UPGRADEFORT			= Orders::Type::UpgradeFort
// #define ORDER_HowMany				= Orders::Type::HowMany


#endif /* CAMPORD_H */

