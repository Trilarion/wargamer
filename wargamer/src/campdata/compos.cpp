/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 * CommandPosition Implementation, split from armies.cpp
 * (also includes leaders and strength points)
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "compos.hpp"
#include "filebase.hpp"
#include "filecnk.hpp"
#include "wldfile.hpp"
#include "ob.hpp"
#include "savegame.hpp"
#include "options.hpp"
#include "fsalloc.hpp"
#ifdef DEBUG
#include "unitlog.hpp"
#include "cplog.hpp"
#endif
#include "CRC.hpp"
/*
 * CampaignInfo functions
 */

/*
 * Campaign Info List
 */


CommandPositionList::CommandPositionList()
{
   // ToDo
}

CommandPositionList::~CommandPositionList()
{
   // ToDo
#ifdef DEBUG
   /*
    * Note that this line is important because it removes the
    * ICommmandPosition reference in cuLog, which would otherwise
    * crash it later on.
    */

   cuLog.printf(NoCommandPosition, "CommandPositionList deleted");
#endif
}

void CommandPositionList::reset()
{
   Super::reset();
}

/*-----------------------------------------------------------------
 * Garbage collection
 */

void CommandPositionList::cleanup()
{
#ifdef DEBUG
   cpLog.printf("CampaignCP::cleanup");
   int count = 0;
#endif


   SListIter<CommandPosition> iter(this);
   while(++iter)
   {
     // GenericCP* g = iter.current()->generic();
     // ASSERT(g != 0);
     // if(g->refCount() == 0)
      if(iter.current()->refCount() == 0)
     {
#ifdef DEBUG
       cpLog.printf("clearing %d, %s", (int)iter.current()->getSelf(), (const char*)iter.current()->getName());
       ++count;
#endif
       iter.current()->reset();
       iter.remove();
     }
   }

#ifdef DEBUG
   cpLog.printf("Deleted %d CampaignCPs", count);
#endif
}

/*
 * File Interface
 */

const UWORD CommandPositionList::s_fileVersion = 0x0001;

Boolean CommandPositionList::readData(FileReader& f, OrderBattle* ob)
{
   ASSERT(entries() == 0);

   if(entries() > 0)
     reset();

   ASSERT(!f.isAscii());

   /*
    * For old compatibility
    * check out CommandPosition List chunk
    */

   const char* chunkName = campaignCPChunkName;

   FileChunkReader fr(f, chunkName);
   UWORD version;
   f >> version;
   ASSERT(version <= s_fileVersion);
   UWORD count;
   f >> count;


   if(version < 0x0001)
   {
      while(count--)
      {
         CPIndex cpi;
         f >> cpi;

         CommandPosition* cp = new CommandPosition();
         ASSERT(cp);

         cp->init(ob->getCommand(cpi));
         cp->readData(f, ob);
         append(cp);
      }
   }
   else
   {  // Version 1 reader
      /*
       * Pass 1: Read and initialise Generic CPIndex
       */

      int n = count;
      while(n--)
      {
         CPIndex cpi;
         f >> cpi;
         CommandPosition* cp = new CommandPosition();
         ASSERT(cp);
         cp->init(ob->getCommand(cpi));
         append(cp);
      }

      /*
       * Pass 2: Read data
       */

      n = count;
      SListIter<CommandPosition> iter(this);
      while(n--)
      {
         bool ok = ++iter;
         ASSERT(ok);
         CommandPosition* cp = iter.current();
         ASSERT(cp != 0);
         IFDEBUG(RefGenericCP generic = cp->generic());
         ASSERT(generic != NoGenericCP);
         ASSERT(campCP(generic) == cp);
         cp->readData(f, ob);
      }
      ASSERT(!++iter);     // at end of list?
   }

   return f.isOK();
}

Boolean CommandPositionList::writeData(FileWriter& f, OrderBattle* ob) const
{
   FileChunkWriter fc(f, campaignCPChunkName);

   /*
    * Find the last used item
    */

   int nUsed = entries();

   if(f.isAscii())
   {
      f.printf(fmt_sdn, entriesToken, (int) nUsed);
   }
   else
   {
      f.putUWord(s_fileVersion);
      ASSERT(entries() <= UWORD_MAX);
      ASSERT(entries() >= UWORD_MIN);
      f.putUWord(static_cast<UWORD>(nUsed));
   }


   /*
    * Pass 1: writes CPIndexes
    */

   SListIterR<CommandPosition> iter(this);
   while(++iter)
   {
      if(f.isAscii())
      {
      }
      else
      {
         const CommandPosition* cp = iter.current();
         ASSERT(cp->generic());

         CPIndex cpi = cp->generic()->getSelf();

         f << cpi;
      }
   }

   /*
    * Pass 2: writes data
    */


   iter.rewind();
   while(++iter)
   {
      if(f.isAscii())
      {
      }
      else
      {
         const CommandPosition* cp = iter.current();
         cp->writeData(f, ob);
      }
   }

   return f.isOK();
}

#if 0
CommandPosition* CommandPositionList::addCP(CPIndex cpi, RefGenericCP gcp)
{
   ASSERT(cpi != NoCPIndex);  // ommandPosition);
   ASSERT(gcp != 0);

   CommandPosition* cp = new CommandPosition();
   ASSERT(cp);

   if(cpi < entries())
   {
#ifdef DEBUG
     Boolean inserted = False;
#endif
     SListIter<CommandPosition> iter(this);
     for(int i = 1; ++iter; i++)
     {
       if(i >= cpi)
       {
         CommandPosition* prevCP = iter.current();
         insertAfter(prevCP, cp);
#ifdef DEBUG
         inserted = True;
#endif
         break;
       }
     }
#ifdef DEBUG
     if(!inserted)
       FORCEASSERT("Failure in CommandPositionList::addCP()");
#endif
   }
   else
     append(cp);

   cp->init(gcp);
   return cp;

}

void CommandPositionList::delCP(CPIndex cpi)
{
}

#else

CommandPosition* CommandPositionList::addCP(RefGenericCP gcp)
{
   ASSERT(gcp != 0);
   CommandPosition* cp = new CommandPosition();
   ASSERT(cp);
    append(cp);
   cp->init(gcp);
   return cp;
}
#endif


/*---------------------------------------------------------------
 * CampaignCommandPosition
 */

CommandPosition::CommandPosition() :
   d_generic(0),
   d_flags(0),
   d_location(),
#if !defined(EDITOR)
   d_movement(),
#endif // !defined(EDITOR)
   // d_morale(0),
   // d_fatigue(0),
   // d_supply(0),
   d_forcedHoldTime(0),
   d_unitParalizedTime(0),
   d_fatigueCounter(0)
#if !defined(EDITOR)
   ,d_lastSeenLocation(),
   d_visibleUntil(0),
   d_enemyKnowsThis(NotSeen),
   d_fowPercent(0)
   // d_aicData()
#endif
{
}

/*
 * Copy Constructor is required by STL
 * It will normally only be used to copy a default temporary CP
 */

CommandPosition::CommandPosition(const CommandPosition& cp)
{
   operator = (cp);
}



CommandPosition::~CommandPosition()
{
   // ToDo
}

/*
 * Copy functions:
 * Don't really wnat this, but STL complains if it is missing
 */

const CommandPosition& CommandPosition::operator = (const CommandPosition& cp)
{

#if 0
// ASSERT(cp.d_generic == 0);    // Check that cp is a temporary one
// ASSERT(!cp.d_used);
#if !defined(EDITOR)
   // ASSERT(*cp.d_aicData == 0);
#endif
   d_generic            = cp.d_generic;
   d_used               = cp.d_used;
   d_location           = cp.d_location;
#if !defined(EDITOR)
   d_movement           = cp.d_movement;
#endif   // !defined(EDITOR)
   d_morale             = cp.d_morale;
   d_fatigue            = cp.d_fatigue;
   d_supply             = cp.d_supply;
   d_orders             = cp.d_orders;
   d_active             = cp.d_active;
   d_forcedHoldTime     = cp.d_forcedHoldTime;
   d_unitParalizedTime  = cp.d_unitParalizedTime;
// d_fieldWorks         = cp.d_fieldWorks;
   d_fatigueCounter     = cp.d_fatigueCounter;
#if !defined(EDITOR)
   d_lastSeenLocation   = cp.d_lastSeenLocation;
// d_seen               = cp.d_seen;
   d_visibleUntil       = cp.d_visibleUntil;
   d_enemyKnowsThis     = cp.d_enemyKnowsThis;
   // d_aicData            = 0;     // cp.d_aicData;
#endif
#endif // 0
   return *this;
}

void CommandPosition::init(RefGenericCP cp)
{
   ASSERT(cp != 0);
   ASSERT(!isUsed());
   ASSERT(cp->campaignInfo() == 0);

   d_generic = cp;
   isUsed(true);
   cp->campaignInfo(this);
}

void CommandPosition::reset()
{
   ASSERT(d_generic != 0);
   d_generic->campaignInfo(0);
   d_generic = 0;
   isUsed(false);
}

/*
 * operator new, delete overloads
 */

const int CommandPosition::ChunkSize = 25;
#ifdef DEBUG
static FixedSizeAlloc<CommandPosition> s_allocator("CommandPosition");
#else
static FixedSizeAlloc<CommandPosition> s_allocator;
#endif  // DEBUG

void* CommandPosition::operator new(size_t size)
{
  return s_allocator.alloc(size);
}

#ifdef _MSC_VER
void CommandPosition::operator delete(void* deadObject)
{
  s_allocator.release(deadObject);
}
#else
void CommandPosition::operator delete(void* deadObject, size_t size)
{
  s_allocator.release(deadObject, size);
}
#endif


/*
 * File Interface
 */

#if !defined(EDITOR)
/*
 * Useful << and >> functions for enumerations
 */

inline bool operator >> (FileReader& f, CommandPosition::InfoQuality& q)
{
   UBYTE b;
   f >> b;

   q = static_cast<CommandPosition::InfoQuality>(b);
   return true;
}

inline bool operator << (FileWriter& f, const CommandPosition::InfoQuality& q)
{
   return f.putUByte(q);
}

#endif   // EDITOR


const UWORD CommandPosition::s_fileVersion = 0x0007;

Boolean CommandPosition::readData(FileReader& f, OrderBattle* ob) // , RefGenericCP gcp)
{
   ASSERT(!f.isAscii());
   ASSERT(d_generic != 0);

   isUsed(true);

   UWORD version;
   f >> version;
   ASSERT(version <= s_fileVersion);

   f >> d_location;

    if(version < 0x0006)
    {
        Attribute atr;
       f >> atr;
        setMorale(atr);
        f >> atr;
        setFatigue(atr);
       f >> atr;
       setSupply(atr);
    }

   if(version < 0x0002)
   {
     UBYTE rubbish;
     f >> rubbish;
     f >> rubbish;
   }

   if(version < 0x0003)
   {
     Boolean r;
     f >> r;
     isActive(r);
   }
   else
     f >> d_flags;

   ASSERT(ob);
   d_orders.read(f, *ob);

   if(version >= 0x0007)
      d_townOrders.read(f);

   // This info is only saved in the save game

#if !defined(EDITOR)
   if(f.getMode() == SaveGame::SaveGame)
   {
      UBYTE rubbish;
      if(version >= 0x0001)
        d_movement.read(f, *ob);

      f >> d_forcedHoldTime;
      f >> d_unitParalizedTime;
      if(version < 0x0002)
        f >> rubbish;
      f >> d_fatigueCounter;
      f >> d_lastSeenLocation;
      if(version < 0x0002)
        f >> rubbish;
      f >> d_visibleUntil;
      f >> d_enemyKnowsThis;

      if(version >= 0x0004)
        f >> d_fowPercent;
      // ToDo: f >> d_aicData;
   }
   else
   {
      d_lastSeenLocation = d_location;
   }
#endif

   return f.isOK();
}

Boolean CommandPosition::writeData(FileWriter& f, OrderBattle* ob) const
{
   if(f.isAscii())
   {
      f.printf(startItemString);
      f << d_location;
      // d_movement;
      // d_orders
      // d_active
      // d_forcedHoldTime
      // d_unitParalizedTime
      // d_fieldWorks
      // d_fatigueCounter
      // d_lastSeenLocation
      // d_seen
      // d_visibleUntil
      // d_enemyKnowsThis
      // d_startOrder
      // d_startAggression
      // d_aicData
      f.printf(endItemString);
   }
   else
   {
      f << s_fileVersion;
      f << d_location;
      f << d_flags;

      ASSERT(ob);
      d_orders.write(f, *ob);

      d_townOrders.write(f);

#if !defined(EDITOR)
      // This info is only saved in the save game

      if(f.getMode() == SaveGame::SaveGame)
      {
         d_movement.write(f, *ob);

         f << d_forcedHoldTime;
         f << d_unitParalizedTime;
         f << d_fatigueCounter;
         f << d_lastSeenLocation;
         f << d_visibleUntil;
         f << d_enemyKnowsThis;
            f << d_fowPercent;
      }
#endif
   }

   return f.isOK();
}

// return supply level if that option is active, otherwise return 256.
Attribute CommandPosition::getSupply() const
{
  if(CampaignOptions::get(OPT_Supply))
    return d_generic->supply();
  else
    return Attribute_Range;
}

// check if leader is attached to real unit
Boolean CommandPosition::isRealUnit() const
{
   return True; //getRankEnum() != Rank_Brigade;
}


#if !defined(EDITOR)

void CommandPosition::setSeen(TimeTick tick)
{
   d_lastSeenLocation = d_location;
   d_visibleUntil = tick + HoursToTicks(48);
   if(d_enemyKnowsThis == NotSeen)
     d_enemyKnowsThis = NoInfo;
}


/*
 * Check for special cases where units can move at night
 */

Boolean CommandPosition::canMoveAtNight() const
{
  /*
   *  If unit has withdraw orders it cannot move at night
   */

   if(isWithdrawing())
     return False;


   /*
    * 1. Unit is retreating
    */

   if(isAvoiding())
      return True;

   /*
    * 2. Unit has head at town, and tail wants to catch up
    */

   if(shouldMove() && comingToTown())
      return True;

   /*
    * If none of the above, then can not move at night
    */

   return False;

}
#endif   // !EDITOR

#if 0 // RefTODO

void CommandPosition::setOrder(const OrderBase* order)
{
   d_orders.setCurrentOrder(order);
}

#endif   // Reftodo

/*
 * Send a Campaign Order to a unit
 */

void CommandPosition::sendOrder(const OrderBase* order, TimeTick arrival, TimeTick tryAgainAt)
{
    if(!isDead())
      d_orders.addOrder(order, arrival, tryAgainAt);
}


#if !defined(EDITOR)

/*
 * Is unit seen
 */

Boolean CommandPosition::isSeen() const
{
   //  return ( (d_enemyKnowsThis > NotSeen) || (!CampaignOptions::get(OPT_LimitedUnitInfo)) );
   return (getInfoQuality() > NotSeen);
}


CommandPosition::InfoQuality CommandPosition::getInfoQuality() const
{
   if(CampaignOptions::get(OPT_FogOfWar))
      return d_enemyKnowsThis;
   else
      return FullInfo;
}

#endif   // !defined(EDITOR)


#if 0 // Reftodo

void CommandPosition::startFieldWorks()
{
// setMode(CampaignMovement::CPM_Digging);
}

#endif   // Reftodo



unsigned short
CommandPosition::calculateChecksum(void) {

   unsigned short crc = 0;

   crc = addShortCRC(crc, d_location.calculateCRC());

#if !defined(EDITOR)
   crc = addShortCRC(crc, d_movement.calculateCRC());
#endif

   // NOTE : these 2 not yet done

   //crc = addShortCRC(crc, d_orders.calculateCRC());
   //crc = addShortCRC(crc, d_townOrders.calculateCRC());

   crc = addIntCRC(crc, (unsigned int) d_forcedHoldTime);
   crc = addIntCRC(crc, (unsigned int) d_unitParalizedTime);
   crc = addIntCRC(crc, (unsigned int) d_fatigueCounter);

#if !defined(EDITOR)
   crc = addShortCRC(crc, d_lastSeenLocation.calculateCRC());

   crc = addIntCRC(crc, (unsigned int) d_visibleUntil);

   // NOTE : this not yet done

   //crc = addShortCRC(crc, (unsigned int) d_enemyKnowsThis.calculateCRC());

   crc = addShortCRC(crc, (unsigned short) d_fowPercent);
#endif

   return crc;

}
















/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.2  2001/06/18 08:25:44  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
