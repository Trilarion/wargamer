/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1995, Steven Morle-Green, All Rights Reserved
 *----------------------------------------------------------------------
 *
 * Campaign Route Finder
 *
 * Finds shortest routes between two towns
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/06/13 08:52:37  greenius
 * Initial Import
 *
 * Revision 1.4  1996/06/22 19:06:06  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.3  1996/03/01 17:29:27  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.2  1996/02/29 18:09:03  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.1  1996/02/29 11:01:53  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

 //lint -esym(613, CampaignRoute::d_campData, CampaignRoute::d_nodes) ... possible Null pointer

#include "stdinc.hpp"
#include "route.hpp"
#include "town.hpp"
#include "campdint.hpp"
#include "misc.hpp"
#include "options.hpp"
#include "camppos.hpp"
#include "scenario.hpp"
#include "campord.hpp"
#include "routelst.hpp"
#include "compos.hpp"

#ifdef LOG_ROUTE
 #include "clog.hpp"
 #include "todolog.hpp"
 #include "unitlog.hpp"
 LogFile routeLog("route.log");
#endif

#undef DEBUG_ROUTE

/*
 * Data tables
 */

/*
 * Table of relative speeds of different connection types
 *
 * This really ought to be in a data file:
 * It ought also to be able to be passed to the routefinder by the caller
 * so that different types of units can use different tables.
 */

struct {
   Speed speeds[CQ_Max];         // Speeds at different qualities
   RouteCost embarkTime;         // Time to get onto transport method
   RouteCost disembarkTime;      // Time to get off transport method
} routeCosts[CT_Max] = {
#if 0
   { { MPH(1), MPH(2),  MPH(4)  }, HoursToTicks(1), HoursToTicks(1) },  // Road
   { { MPH(4), MPH(10), MPH(20) }, HoursToTicks(4), HoursToTicks(1) },  // Rail
   { { MPH(2), MPH(4),  MPH(6)  }, HoursToTicks(8), HoursToTicks(2) },  // River
#else // Less difference between qualities
   { { MPH(3), YPH((YardsPerMile*7)/2),  MPH(4)  }, HoursToTicks(1), HoursToTicks(1) },   // Road
   { { MPH(15), MPH(18), MPH(20) }, HoursToTicks(4), HoursToTicks(1) }, // Rail
   { { MPH(4), MPH(5),  MPH(6)  }, HoursToTicks(8), HoursToTicks(2) },  // River
#endif
};



/*
 * Exception Handler
 */

class RouteError {
};

/*
 * Constructor
 */

CampaignRoute::CampaignRoute(const CampaignData* campData) :
   d_campData(campData),
   d_nodes(0),
   d_nodeCount(0),
   d_usedList(NoTown),
   d_bestCost(MaxRNcost),
#ifdef DEBUG
   // d_iterations(0),
   d_aStarCount(0),
#endif
   d_flags(0),
   d_travelSide(SIDE_Neutral),
   d_enemyDelay(0)
{
}

/*
 * Destructor
 */

CampaignRoute::~CampaignRoute()
{
   if(d_nodes != 0)
   {
      delete[] d_nodes;
      d_nodes = 0;
      d_nodeCount = 0;
   }
   d_campData = 0;   // keep lint happy
}

/*
 * Private Function to check if town is allowed
 */

inline bool CampaignRoute::isTownAllowed(const Town& nTown) const
{
   /*
    * Is it an enemy town?
    */

   if( (d_flags & NoEnemy) && scenario->isEnemy(nTown.getSide(), d_travelSide) )
   {
#ifdef DEBUG_ROUTE
      routeLog.printf("Found enemy town %s", nTown.getNameNotNull());
#endif
      return false;
   }

   /*
    * Is it on our side?
    */

   if( (d_flags & SameSide) && (nTown.getSide() != d_travelSide) )
   {
#ifdef DEBUG_ROUTE
      routeLog.printf("Found town on different side %s", nTown.getNameNotNull());
#endif
      return false;
   }

   return true;
}

/*
 * Private Function to find routes
 *
 * From can be NoTown, in which case the node table is filled in completely
 * based on connections from dest and the return value is meaningless.
 */


IConnection CampaignRoute::searchRoute(ITown from, ITown dest)
{
#ifdef LOG_ROUTE
   // Make sure routelog is flushed on return
   class CloseLog {
   public: ~CloseLog() { routeLog.close(); }
   };
#endif


   try {

      ASSERT(d_campData);
      ASSERT(! ((d_flags & SameSide) && (d_flags & NoEnemy)));

      if( (d_flags & SameSide) && (d_flags & NoEnemy) )
         d_flags &= ~NoEnemy;


      /*
       * Get some useful values
       */

      const TownList& tl = d_campData->getTowns();
      const ConnectionList& cl = d_campData->getConnections();

      const Town* fromTown;
      if(from == NoTown)
         fromTown = 0;
      else
         fromTown = &tl[from];

#ifdef DEBUG_ROUTE
      routeLog.printf("===================================================");
      routeLog.printf("planRoute(%d %s to %d %s)",
         (int) from,
         (const char*) d_campData->getTownName(from),
         (int) dest,
         (const char*) d_campData->getTownName(dest));
      routeLog.printf("useCosts = %s", boolToAscii( (d_flags & UseCosts) != 0));
      routeLog.printf("findBest = %s", boolToAscii( (d_flags & FindBest) != 0));
      routeLog.printf("Side = %s", (const char*) scenario->getSideName(d_travelSide));
      routeLog.printf("enemyDelay = %d", (int) d_enemyDelay);
      routeLog.printf("fillTable = %s", boolToAscii((d_flags & FillTable) != 0));
      routeLog.printf("-------");
#endif

      /*
       * Set up node structure
       */

      int count = tl.entries();
      ASSERT(count > 0);
      if(count <= 0)
         throw RouteError();

      if(d_nodes == 0)
         d_nodes = new CampaignRouteNode[count];
      else
         ASSERT(d_nodeCount == count);

      ASSERT(d_nodes != 0);
      if(d_nodes == 0)
         throw RouteError();
      d_nodeCount = count;

      for(int i = 0; i < count; ++i)
      {
         const Town& town = tl[i];

         d_nodes[i].next = NoTown;
         d_nodes[i].cost = MaxRNcost;
         d_nodes[i].connection = NoConnection;
         // if(from != NoTown)
         if(fromTown != 0)
            d_nodes[i].distance = distanceLocation(town.getLocation(), fromTown->getLocation());
         else
            d_nodes[i].distance = 0;
         // d_nodes[i].flags = 0;
         d_nodes[i].d_inQueue = false;
      }

      d_usedList = NoTown;
      d_bestCost = MaxRNcost;
#ifdef DEBUG
      int iterations = 0;
      d_aStarCount = 0;
#endif

      // Set offMap flag if we start in an offMap location

      // bool startedOffMap = tl[dest].isOffScreen();
      // bool startedOffMap = tl[from].isOffScreen();

      /*
       * Prepare for main loop
       */

      addNode(dest, 0, NoConnection);

      while(d_usedList != NoTown)
      {
#ifdef DEBUG
         iterations++;
#endif

         ITown n = getNode();
         ASSERT(n != NoTown);

         // Go through each connection and add

         const Town& town = tl[n];
         CampaignRouteNode& rn = d_nodes[n];    //lint !e662 ... possible out of bounds
         RouteCost cost = rn.cost;

         if(n == from)
         {
            if((d_flags & FindBest) || (d_flags & FillTable))
            {
#ifdef DEBUG_ROUTE
               routeLog.printf("Route found at cost %ld", (long) cost);
#endif
               if(cost < d_bestCost)
                  d_bestCost = cost;
            }
            else
               break;
         }
         else
         if( (d_flags & DoingSupply) && (n != dest) && (town.getIsDepot() || town.getIsSupplySource()))
         {
#ifdef DEBUG_ROUTE
            routeLog.printf("Found supply Depot/Source %s", town.getNameNotNull());
#endif
               // Do nothing..
         }
         else
         if( (cost < d_bestCost) || (d_flags & FillTable))
         {
            for(int i = 0; i < MaxConnections; i++)
            {
               IConnection ci = town.getConnection(i);
               if(ci == NoConnection)
                  break;

               const Connection& con = cl[ci];
               ITown nextTown = con.getOtherTown(n);

               const Town& nTown = tl[nextTown];

               /*
                * Don't add towns if we are disallowing enemy or not on same side
                */

               if( !(d_flags & AllowOffMap) &&
                     !nTown.isOffScreen() && town.isOffScreen())
//                    town.isOffScreen() &&
//                   (nTown.isOffScreen() != startedOffMap))
//                   (!startedOffMap ||
//                   (!town.isOffScreen() && nTown.isOffScreen())))
//                   (nTown.isOffScreen() != town.isOffScreen()) &&
//                   (nTown.isOffScreen() != startedOffMap) ) // Modified SWG:9Sep99 was ==
               {
#ifdef DEBUG_ROUTE
                  routeLog.printf("%s is off map", nTown.getNameNotNull());
#endif
               }
               else  if(isTownAllowed(nTown))
               {
                  CampaignRouteNode& nextrn = d_nodes[nextTown];

                  // Calculate new cost... should depend on length, connection type, etc

                  RouteCost newCost = cost;

                  if(d_flags & UseCosts)
                  {
                     Distance length = d_campData->getConnectionLength(ci);

                     /*
                      * Adjust based on type of connection
                      */

                     // ASSERT(con.how >= 0);   //lint !e568 ... unsigned can not be <0
                     ASSERT(con.how < CT_Max);
                     // ASSERT(con.quality >= 0);   //lint !e568 ... unsigned can not be <0
                     ASSERT(con.quality < CQ_Max);

                     TimeTick timeTaken = timeToTravel(length, routeCosts[con.how].speeds[con.quality]);

                     newCost += timeTaken;
                  }
                  else
                     newCost++;  // Count connections

                  /*
                   * Add on delayTime if on different side
                   */

                  if(nTown.getSide() != d_travelSide)
                     newCost += d_enemyDelay;

                  if( (newCost < d_bestCost) || (d_flags & FillTable))
                  {
                     if(nextrn.cost > newCost)
                        addNode(nextTown, newCost, ci);
                  }
               }
            }
         }
      }

      /*
       * Work out the resultant connection
       */

#ifdef DEBUG_ROUTE
      routeLog.printf("-------");
      routeLog.printf("Results of planRoute:");
      routeLog.printf("Iterations = %d", iterations);
      routeLog.printf("aStar iterations = %d", d_aStarCount);
      routeLog.printf("-------");

#ifdef DEBUG_ROUTE
      for(i = 0; i < d_nodeCount; i++)
      {
         routeLog.printf("%3d cost=%3d, dist=%5ld, connection=%3d, %s",
            i,
            (int) d_nodes[i].cost,
            (long) d_nodes[i].distance,
            (int) d_nodes[i].connection,
            tl[i].getNameNotNull());
      }

      routeLog.printf("-------");
#endif

#endif

      if(from != NoTown)
      {
         IConnection bestCon = d_nodes[from].connection;

#ifdef DEBUG_ROUTE
         if(bestCon == NoConnection)
            routeLog.printf("No Route found");
         else
         {
            routeLog.printf("Next Connection = %d (%s to %s)",
               (int) bestCon,
               tl[cl[bestCon].node1].getNameNotNull(),
               tl[cl[bestCon].node2].getNameNotNull());

            routeLog.printf("Cost = %ld", (long) d_nodes[from].cost);

            routeLog.printf("-------");

            ITown t = from;
            while((t != dest) && (t != NoTown))
            {
               routeLog.printf("--> %s = %d", tl[t].getNameNotNull(), d_nodes[t].cost);

               IConnection c = d_nodes[t].connection;
               if(c == NoConnection)
                  break;

               t = cl[c].getOtherTown(t);
            }
            routeLog.printf("--> %s = %d", tl[dest].getNameNotNull(), d_nodes[dest].cost);

            routeLog.printf("-------");
         }
         // routeLog.close();
#endif

         return bestCon;
      }
      else
         return NoConnection;

   }
   catch(const RouteError& e)
   {
#ifdef LOG_ROUTE
      routeLog.printf("Route Error exception caught");
      // routeLog.close();
#endif
      return NoConnection;
   }
}

/*
 * Add a town to the d_usedList
 *
 * It ought to keep list sorted on lowest distance
 * For now though we just add it the beginning
 */


void CampaignRoute::addNode(ITown t, RouteCost cost, IConnection con)
{
   d_nodes[t].cost = cost;
   d_nodes[t].connection = con;

   // if( (d_nodes[t].flags & RN_InQueue) == 0)
   if(!d_nodes[t].d_inQueue)
   {
      // d_nodes[t].flags |= RN_InQueue;
      d_nodes[t].d_inQueue = true;

#if 0
      d_nodes[t].next = d_usedList;
      d_usedList = t;
#else
      /*
       * Insert in list, so d_nodes with smallest distance appear first
       */

      Distance d = d_nodes[t].distance;

      ITown prev = NoTown;
      ITown t1 = d_usedList;
      while( (t1 != NoTown) && (d_nodes[t1].distance < d) )
      {
#ifdef DEBUG
         d_aStarCount++;
#endif
         prev = t1;
         t1 = d_nodes[t1].next;
      }

      if(prev == NoTown)
      {
         d_nodes[t].next = d_usedList;
         d_usedList = t;
      }
      else
      {
         d_nodes[t].next = d_nodes[prev].next;
         d_nodes[prev].next = t;
      }

#endif

   }

#ifdef DEBUG_ROUTE
   routeLog.printf("addNode(%d) %s, cost = %d, distance=%ld, con=%d",
      (int) t,
      d_campData->getTown(t).getNameNotNull(),
      (int) d_nodes[t].cost,
      (long) d_nodes[t].distance,
      (int) con);
#endif
}

ITown CampaignRoute::getNode()
{
   ITown t = d_usedList;

   if(t != NoTown)
   {
      // ASSERT(d_nodes[t].flags & RN_InQueue);
      ASSERT(d_nodes[t].d_inQueue);

      d_usedList = d_nodes[t].next;
      // d_nodes[t].flags &= ~RN_InQueue;
      d_nodes[t].d_inQueue = false;
      d_nodes[t].next = NoTown;     // Unneccesary, but keeps it tidy
   }

   return t;
}

void CampaignRoute::setup(RouteFlag f, Side side, RouteCost delay)
{
   // useCosts = uc;
#ifdef DEBUG
   // findBest = getOption(OPT_FullRoute);
#else
   // findBest = fb;
#endif

//   if(Options::get(OPT_FullRoute))
      f |= FindBest;

   d_flags = f;

   d_travelSide = side;
   d_enemyDelay = delay;
}

/*
 * Find a route between 2 towns
 * Returns next connection to travel on.
 */

IConnection CampaignRoute::planRoute(ITown from, ITown dest)
{
   ASSERT(from != NoTown);
   ASSERT(dest != NoTown);
   ASSERT(from != dest);

   d_flags |= FillTable;
   d_flags &= ~DoingSupply;

   return searchRoute(from, dest);
}

/*
 * Fill in all distances from a given town.
 */

Boolean CampaignRoute::fillRoute(ITown from)
{
   ASSERT(from != NoTown);

   d_flags |= FillTable;

   searchRoute(NoTown, from);

   return True;
}

/*
 * Find out the distance between two campaign locations
 * Returns TRUE if the distance < maxDistance
 * Returns FALSE if distance > maxDistance...
 *
 * Note: If the 'Boolean actual' parameter is True then we return the actual route
 *       distance. If False then we return straight-line distance
 *
 */

#ifdef DEBUG
//#define DEBUG_ROUTEDISTANCE
#undef DEBUG_ROUTEDISTANCE
#endif

Boolean CampaignRoute::getRouteDistance(const CampaignPosition* from, const CampaignPosition* dest, Distance maxDist, Distance& dist, Boolean actual)
{
   // special case of being at the same town...
#ifdef DEBUG_ROUTEDISTANCE
   String locName1;
   String locName2;
   d_campData->getCampaignPositionString(from, locName1);
   d_campData->getCampaignPositionString(dest, locName2);
   routeLog.printf("\n================================");
   routeLog.printf("Getting route distance from %s to %s", (const char*) locName1, (const char*)locName2);
   routeLog.printf("Max distance = %ld, Actual Distance = %s", maxDist, (actual) ? "True" : "False");
#endif

   if(from->atTown() && dest->atTown() && from->getTown() == dest->getTown())
   {
     dist = 0;
#ifdef DEBUG_ROUTEDISTANCE
     routeLog.printf("Distance is 0. At the same town");
#endif
     return (dist <= maxDist);
   }

   if(actual)
   {
//      if(Options::get(OPT_FullRoute))
         d_flags |= FindBest;

      /*
       * get start and end towns
       */

      ITown startTown = from->getDestTown();
      ITown endTown = dest->getLastTown();

      ASSERT(startTown != NoTown);
      ASSERT(endTown != NoTown);

      // from location to start town
      Distance d = 0;

      /*
       * First, special cases:
       *
       * See if we're on the same connection or at adjacent towns
       *
       */

      if( (!from->atTown() && !dest->atTown() && from->getConnection() == dest->getConnection()) ||
          (!from->atTown() && dest->atTown() && from->isConnected(endTown)) ||
          (from->atTown() && !dest->atTown() && dest->isConnected(startTown)) ||
          (from->atTown() && dest->atTown() && CampaignRouteUtil::areTownsAdjacent(d_campData, from->getTown(), dest->getTown())) )

      {
         // set actual to false so a straight line distance is done
         actual = False;
      }

      else
      {
         /*
          * now work out distance
          */

         // from location to start town
         d = from->getRemainingDistance();

         Boolean thisSide = False;

         if(startTown == endTown)
         {
            ASSERT(!dest->atTown());
            ASSERT(!from->atTown());
            ASSERT(from->getConnection() != dest->getConnection());
            // do nothing
         }
         else
         {
            /*
             * let planRoute do its work
             */

            planRoute(startTown, endTown);
            ITown t = startTown;
            // const Town& town1 = d_campData->getTown(t);

            while( (t != endTown) && (t != NoTown) )
            {
               IConnection c = d_nodes[t].connection;
               if(c == NoConnection)
                  break;

               const Connection& con = d_campData->getConnection(c);
               ITown nextTown = con.getOtherTown(t);

               if(nextTown == endTown)
               {
                  if(!dest->atTown() && dest->getConnection() == c)
                  {
                     thisSide = True;
                     break;
                  }
               }

               d += con.distance;
               t = nextTown;
            }
         }

         d += (thisSide) ? dest->getRemainingDistance() : dest->getDistanceAlong();
      }

      dist = d;
   }

   // use if instead of else because previous if statement may change 'actual' value
   if(!actual)
   {
      /*
       * Temporarily store the physical distance from each position's head
       */

      Location l1;
      Location l2;
      Location l3;
      Location l4;
      from->getHeadLocation(l1);
      from->getTailLocation(l2);
      dest->getHeadLocation(l3);
      dest->getTailLocation(l4);

      Distance d1 = distanceLocation(l1, l3);
      Distance d2 = distanceLocation(l1, l4);
      Distance d3 = distanceLocation(l2, l3);
      Distance d4 = distanceLocation(l2, l4);

      Distance d = minimum(d1, d2);
      d = minimum(d, d3);
      d = minimum(d, d4);

      dist = d;
   }


#ifdef DEBUG_ROUTEDISTANCE
   if(dist > maxDist)
     routeLog.printf("Route Distance = %ld", dist);
#endif

   return (dist <= maxDist);
}


/*
 * Fill out complete node table related to supply depots
 *
 * Routes are not allowed to go through enemy towns
 * Route planning stops at any supply depot/source
 * Result is that only directly connected depots/sources have
 * a valid distance.
 */

Boolean CampaignRoute::planSupply(ITown from)
{
   /*
    * for debugging... just do an ordinary route
    */

   d_flags = FindBest | DoingSupply | SameSide;
   d_travelSide = d_campData->getTown(from).getSide();

   fillRoute(from);

   return True;
}

/*--------------------------------------------------------
 * Route plotting utility
 */

void CampaignRouteUtil::plotUnitRoute(const CampaignData* campData, const ICommandPosition& cpi)
{
  RouteList& rl = cpi->currentRouteList();
  rl.reset();

#ifdef LOG_ROUTE
  routeLog.printf("\n==========================================");
  routeLog.printf("---- Plotting route for %s", (const char*)campData->getUnitName(cpi).toStr());
//  cuLog.printf("----- Plotting route for %s", (const char*)campData->getUnitName(cpi).toStr());
#endif

  // this data is shared by interface thread so we need to lock it
  // RWLock lock;
  // lock.startWrite();

  /*
   * if we currently have a target unit, and target unit != orders.getTargetUnit()
   * (i.e. if we are moving into battle, or marching to the sound of battle, etc.)
   * plot route directly to that unit, otherwise use current order to plot route
   */

  const CampaignOrder* order = cpi->getCurrentOrder();

  if(cpi->getTarget() != NoCommandPosition && order->getTargetUnit() != cpi->getTarget())
  {
    ICommandPosition targetCPI = cpi->getTarget();
    plotRoute(campData, rl, cpi->getPosition(), targetCPI->getPosition(), cpi->getSide());
#ifdef LOG_ROUTE
    routeLog.printf("Plotting route directly to %s", (const char*)campData->getUnitName(targetCPI).toStr());
#endif
  }
  else
  {
    plotRoute(campData, rl, cpi->getPosition(), *order, cpi->getSide());
  }

  // lock.endWrite();

#ifdef LOG_ROUTE
  routeLog.printf(" ------- Final Route ");
  cpi->rewindCurrentRoute();
  while(cpi->currentRouteIter())
  {
    ITown iTown = cpi->currentRouteNode();
    const Town& t = campData->getTown(iTown);

    routeLog.printf("   -------------> %s", t.getNameNotNull());
  }
#endif
}

/*
 * Plot route from one town to another
 */

void CampaignRouteUtil::plotRoute(const CampaignData* campData, RouteList& list,
             Side startSide, ITown startTown, ITown destTown, IConnection iCon)
{
  ASSERT(startTown != destTown);
  {
    // set up flags

    // Modified SWG: 10Aug99 : can't be static because campData can change between games
    // static CampaignRoute route(campData);
    CampaignRoute route(campData);
    CampaignRoute::RouteFlag flags = 0;
    RouteCost cost = 0;

    if(startSide != SIDE_Neutral)
    {
      flags |= CampaignRoute::UseCosts;
      cost = TicksPerHour * 8;
    }

    route.setup(flags, startSide, cost);
    route.planRoute(startTown, destTown);

    const ConnectionList& cl = campData->getConnections();
    ITown t = startTown;

    while((t != destTown) && (t != NoTown))
    {
      IConnection c = route.getNodes()[t].connection;
      if(c == NoConnection)
        break;

      if( (t != startTown) ||
          (iCon != NoConnection && iCon != c) )
      {
        list.addNode(t);
      }

      t = cl[c].getOtherTown(t);
    }

    // add nextDest to list
    list.addNode(destTown);

  }
}

/*
 * Private function to plot route from on CampaignPosition to another
 */

void CampaignRouteUtil::plotRoute(const CampaignData* campData, RouteList& list,
              const CampaignPosition& from, const CampaignPosition& dest, Side startSide)
{
//  ASSERT(list.entries() == 0);

  /*
   * Get start and end towns of route
   */

  ITown startTown = from.getCloseTown();
  ITown endTown = dest.getCloseTown();

  /*
   * Special cases...
   */

  // if we're both at the same town do nothing...
  if(from.atTown() && dest.atTown() && startTown == endTown)
      ;
  // if on the same connection make target town the town that is on the other side of target unit
  else if(!from.atTown() && !dest.atTown() && from.getConnection() == dest.getConnection())
  {
    ITown targetTown = NoTown;

      // if we're both headed in the same way
      if(from.getLastTown() == dest.getLastTown())
      {
        targetTown = (from.getDistanceAlong() > dest.getDistanceAlong()) ?
              from.getLastTown() : from.getDestTown();
      }
      else
      {
        targetTown = (from.getDistanceAlong() > dest.getRemainingDistance()) ?
              from.getLastTown() : from.getDestTown();
      }

    ASSERT(targetTown);
    list.addNode(targetTown);
  }

  // if we're connected to targets close town
  else if(from.isConnected(endTown))
  {
    if(from.atTown())
    {
      ASSERT(!dest.atTown());
      const Connection& c = campData->getConnection(dest.getConnection());
      endTown = c.getOtherTown(endTown);
    }
    list.addNode(endTown);
  }

  // or he's connected to us
  else if(dest.isConnected(startTown))
  {
    if(from.atTown())
    {
      ASSERT(!dest.atTown());
      const Connection& c = campData->getConnection(dest.getConnection());
      startTown = c.getOtherTown(startTown);
    }
    list.addNode(startTown);
  }

  // otherwise, plot it normally
  else
  {
    ASSERT(startTown != endTown);
    IConnection con = (!from.atTown()) ? from.getConnection() : NoConnection;
    plotRoute(campData, list, startSide, startTown, endTown, con);
    ASSERT(list.entries() > 0);
  }

}

void CampaignRouteUtil::plotRoute(const CampaignData* campData, RouteList& list, const CampaignPosition& startPos, const CampaignOrder& order, Side side)
{
   //--- Modified SWG:22Jul99:
   //   Removed ASSERT because:
   //     It is possible for this to be called from Campaign Logic
   //     where a mode has been set, but the order is hold.. e.g. retreating from enemy

   // ASSERT(order.isMoveOrder());

   // clear out list
   list.reset();

   CampaignPosition lastPos = startPos;

   for(int i = 0; (i < Orders::Vias::MaxVia) && (order.getVia(i) != NoTown); i++)
   {
      const CampaignPosition& newPos = order.getVia(i);

      plotRoute(campData, list, lastPos, newPos, side);
      lastPos = newPos;
   }

   // Do Dest Town
   if(order.hasDestTown())
   {
      const CampaignPosition& newPos = order.getDestTown();
      plotRoute(campData, list, lastPos, newPos, side);
      lastPos = newPos;
   }

   // And finally the destination target
   else if(order.hasTargetUnit())
   {
      // const CommandPosition* target = mapData.d_campData->getCommand(order.getTarget());
      ConstICommandPosition target = order.getTarget();
      const CampaignPosition& newPos = target->getPosition();

      plotRoute(campData, list, lastPos, newPos, side);
      lastPos = newPos;
   }

}

void CampaignRouteUtil::plotRoute(const CampaignData* campData, RouteList& list, ITown startTown, ITown endTown, IConnection iCon)
{
  ASSERT(startTown != NoTown);
  ASSERT(endTown != NoTown);
  list.reset();
  plotRoute(campData, list, SIDE_Neutral, startTown, endTown, iCon);
}

IConnection CampaignRouteUtil::commonConnection(const CampaignData* campData, ITown town1, ITown town2)
{
  ASSERT(town1 != NoTown);
  ASSERT(town2 != NoTown);

  const Town& t = campData->getTown(town1);

  Boolean isConnected = False;
  IConnection commonConnection = NoConnection;

  for(int i = 0; i < MaxConnections; i++)
  {
    IConnection iCon = t.getConnection(i);
    if(iCon == NoConnection)
      break;

    const Connection& con = campData->getConnection(iCon);

    isConnected = ( (con.node1 == town2) || (con.node2 == town2) );
    if(isConnected)
    {
      commonConnection = iCon;
      break;
    }
  }

  return commonConnection;
}

Boolean CampaignRouteUtil::areTownsAdjacent(const CampaignData* campData, ITown town1, ITown town2)
{
  ASSERT(town1 != NoTown);
  ASSERT(town2 != NoTown);

  const Town& t = campData->getTown(town1);

  Boolean isConnected = False;
  for(int i = 0; i < MaxConnections; i++)
  {
    IConnection iCon = t.getConnection(i);
    if(iCon == NoConnection)
      break;

    const Connection& con = campData->getConnection(iCon);

    isConnected = ( (con.node1 == town2) || (con.node2 == town2) );
    if(isConnected)
      break;
  }

  return isConnected;
}

/*
 * are 2 positions at adjacent towns or connections, or connected to
 * adjacent towns and connections
 *
 */

Boolean CampaignRouteUtil::isAdjacent(const CampaignData* campData, CampaignPosition& pos1, CampaignPosition& pos2)
{
  // if both are at towns
  if(pos1.atTown() && pos2.atTown())
     return areTownsAdjacent(campData, pos1.getTown(), pos2.getTown());

  // if on the same connection or adjacent
  else if(!pos1.atTown() && !pos2.atTown())
  {
    if(pos1.getConnection() == pos2.getConnection())
      return True;
    else
    {
      const Connection& con1 = campData->getConnection(pos1.getConnection());
      const Connection& con2 = campData->getConnection(pos2.getConnection());

      if( (con1.node1 == con2.node1 || con1.node1 == con2.node2) ||
          (con1.node2 == con2.node1 || con1.node2 == con2.node2) )
      {
        return True;
      }
    }
  }

  // or connected
  else if(pos1.atTown() && !pos2.atTown())
  {
    if(pos2.isConnected(pos1.getTown()))
      return True;
    else
    {
      const Connection& con2 = campData->getConnection(pos2.getConnection());
      if( (areTownsAdjacent(campData, pos1.getTown(), con2.node1)) ||
          (areTownsAdjacent(campData, pos1.getTown(), con2.node2)) )
      {
        return True;
      }
    }
  }

  // or connected
  else if(!pos1.atTown() && pos2.atTown())
  {
    if(pos1.isConnected(pos2.getTown()))
      return True;
    else
    {
      const Connection& con1 = campData->getConnection(pos1.getConnection());
      if(areTownsAdjacent(campData, pos2.getTown(), con1.node1) ||
         areTownsAdjacent(campData, pos2.getTown(), con1.node2) )
      {
        return True;
      }
    }
  }

  return False;
}



/*
 *----------------------------------------------------------------------
 * $Log$
 *----------------------------------------------------------------------
 */
