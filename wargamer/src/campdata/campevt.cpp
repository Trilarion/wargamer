/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 * Campaign Timer Events
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "campevt.hpp"

/*
 * Campaign Events
 */

CampaignEvent::CampaignEvent()
{
   tick = 0;
}

void CampaignEvent::set()
{
   event.set();
}

void CampaignEvent::wait()
{
   tick = 0;
   event.wait();
}

void CampaignEvent::waitUntil(TimeTick t)
{
   tick = t;
   event.wait();
}

void CampaignEvent::procTick(TimeTick t)
{
   if(t >= tick)
      set();
}

void EventList::process(TimeTick tick)
{
   SListIter<CampaignEvent> iter = this;
   while(++iter)
   {
      CampaignEvent* event = iter.current();
      event->procTick(tick);
   }
}



/*
 *----------------------------------------------------------------------
 * $Log$
 *----------------------------------------------------------------------
 */
