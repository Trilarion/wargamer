/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef ARMISTIC_HPP
#define ARMISTIC_HPP

#include "gamedefs.hpp"
#include "measure.hpp"

class FileReader;
class FileWriter;

class Armistice {
    TimeTick d_endArmistice;
  public:
    Armistice() { d_endArmistice = 0; }
    ~Armistice() {}

    Boolean inEffect() const { return static_cast<Boolean>(d_endArmistice > 0); }
    void startArmistice(TimeTick end);// { d_endArmistice = end; }
    void update(TimeTick theTime)
    {
      if(theTime >= d_endArmistice)
        d_endArmistice = 0;
    }

    Boolean read(FileReader& f);
    Boolean write(FileWriter& f) const;
};

#endif
