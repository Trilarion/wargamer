/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef C_OBUTIL_HPP
#define C_OBUTIL_HPP

#ifndef __cplusplus
#error c_obutil.hpp is for use with C++
#endif

#include "cdatadll.h"
#include "cpdef.hpp"

class OrderBattle;

namespace CampaignOBUtil {
	CAMPDATA_DLL CPIndex cpiToCPIndex(ConstParamCP cpi, const OrderBattle& ob);
	CAMPDATA_DLL ICommandPosition cpIndexToCPI(CPIndex cpi, OrderBattle& ob);

	CAMPDATA_DLL LeaderIndex leaderToLeaderIndex(ConstILeader leader, const OrderBattle& ob);
	CAMPDATA_DLL ILeader leaderIndexToLeader(LeaderIndex iLeader, OrderBattle& ob);

	CAMPDATA_DLL SPIndex spiToSPIndex(ISP sp, const OrderBattle& ob);
	CAMPDATA_DLL ISP spIndexToISP(SPIndex spi, OrderBattle& ob);
};

#endif
