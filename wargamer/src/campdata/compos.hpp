/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
#ifndef COMPOS_HPP
#define COMPOS_HPP

#ifndef __cplusplus
#error compos.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 * CommandPositions.  Used to be part of armies.hpp
 * But armies.cpp was rather large.
 *
 *----------------------------------------------------------------------
 */

#include "cdatadll.h"
#include "sllist.hpp"
#include "cleader.hpp"  // For backwards compatibility
#include "cp.hpp"       // GenericCP
#include "camppos.hpp"
#include "campord.hpp"
#include "townordr.hpp"
#if !defined(EDITOR)
#include "campunit.hpp"
#endif
#include "refptr.hpp"
#include "cpidef.hpp"

class OrderBattle;
class RouteList;


/*
 * Campaign CommandPosition Class
 */

class CommandPosition : public SLink    //, public RefBaseCount
{
   public:
      enum { MaxFieldWorks = 4 };
      typedef UBYTE FieldWorksLevel;

      enum InfoQuality {
         InfoQuality_First = 0,
         NotSeen,
         NoInfo,
         VeryLimitedInfo,
         LimitedInfo,
         FullInfo,

         InfoQuality_HowMany
      };

   private:

      // disallow copy
      CommandPosition(const CommandPosition&);
      const CommandPosition& operator = (const CommandPosition&);

   public:
      CAMPDATA_DLL ~CommandPosition();
      CAMPDATA_DLL CommandPosition();

      /*
       * File Interface
       */

      CAMPDATA_DLL Boolean readData(FileReader& f, OrderBattle* ob); //, RefGenericCP gcp);
      CAMPDATA_DLL Boolean writeData(FileWriter& f, OrderBattle* ob) const;

      /*
       * Reference counting
       */

        //--- Let RefBaseCount be used
      void addRef() const { d_generic->addRef(); }
      void delRef() const { d_generic->delRef(); }
        int refCount() const { return d_generic->refCount(); }

      // Accessors

      //--- Might need to make these reference pointers instead
      GenericCP* generic() { return d_generic; }
      const GenericCP* generic() const { return d_generic; }

      /*
       * flags
       */

      void isUsed(Boolean f) { setFlags(Used, f); }
      bool isUsed() const { return (d_flags & Used); } //d_used; }

      void trapped(Boolean f) { setFlags(Trapped, f); }
      Boolean trapped() const { return (d_flags & Trapped) != 0; }

      void isActive(Boolean f) { setFlags(Active, f); }
      Boolean isActive() const { return (d_flags & Active) != 0; } //d_active; }

      void aggressionLocked(Boolean f) { setFlags(AggressionLocked, f); }
      Boolean aggressionLocked() const { return (d_flags & AggressionLocked) != 0; } //d_active; }

      void crossingRiver(Boolean f) { setFlags(CrossingRiver, f); }
      Boolean crossingRiver() const { return (d_flags & CrossingRiver) != 0; }

      void newCommand(Boolean f) { setFlags(NewCommand, f); }
      Boolean newCommand() const { return (d_flags & NewCommand) != 0; }

      /*
       * Campaign Position
       */

      void setPosition(const CampaignPosition& pos) { d_location = pos; }
      CampaignPosition* location() { return &d_location; }
      const CampaignPosition* location() const { return &d_location; }
      CampaignPosition& getPosition() { return d_location; }
      const CampaignPosition& getPosition() const { return d_location; }
      void getHeadLocation(Location& l) const { d_location.getHeadLocation(l); }
      void getTailLocation(Location& l) const { d_location.getTailLocation(l); }
      void getLocation(Location& l) const { getHeadLocation(l); }
      Boolean atTown() const { return d_location.atTown(); }
      ITown getTown() const { return d_location.getTown(); }
      ITown getDestTown() const { return d_location.getDestTown(); }
      ITown getLastTown() const { return d_location.getLastTown(); }
      ITown getCloseTown() const { return d_location.getCloseTown(); }
      Distance getRemainingDistance() const { return d_location.getRemainingDistance(); }
      Distance getDistanceAlong() const { return d_location.getDistanceAlong(); }
      IConnection getConnection() const { return d_location.getConnection(); }
      IConnection lastConnection() const { return d_location.lastConnection(); }
      Boolean isConnected(ITown town) const { return d_location.isConnected(town); }
      Boolean isIntersecting(const CampaignPosition* target) const { return d_location.isIntersecting(target); }
      void moveTowards(const CampaignPosition* target) { d_location.moveTowards(target); }
      void moveTowards(ITown town) { d_location.moveTowards(town); }
      Boolean comingToTown() const {return d_location.comingToTown(); }
      void startMoving(IConnection c, ITown t) { d_location.startMoving(c, t); }
      void reverseDirection() { d_location.reverseDirection(); }
      Boolean movePosition(Distance d, Distance columnLength) { return d_location.movePosition(d, columnLength); }

#if !defined(EDITOR)
      // CampaignMovement

      CampaignMovement::CP_Mode getMode() const { return d_movement.getMode(); }
      Boolean setMode(CampaignMovement::CP_Mode newMode) { return d_movement.setMode(newMode); }

      void fowPercent(SWORD p) { d_fowPercent = p; }
      SWORD fowPercent() const { return d_fowPercent; }

//    void setModeTime(TimeTick theTime, TimeTick tilWhen) { d_movement.setModeTime(theTime, tilWhen); }
//    unsigned int modeTimeCompletion(TimeTick theTime, unsigned int range) const { return d_movement.modeTimeCompletion(theTime, range); }

      Boolean doingForceMarch() const { return d_movement.doingForceMarch(); }
      void setForceMarch(Boolean f) { d_movement.setForceMarch(f); }
      Boolean isSiegeActive() const { return d_movement.isSiegeActive(); }
      void setSiegeActive(Boolean f) { d_movement.setSiegeActive(f); }

      void battleNear(Boolean f) { d_movement.battleNear(f); }
      Boolean battleNear() const { return d_movement.battleNear(); }

      void marchingToSoundOfBattle(Boolean f) { d_movement.marchingToSoundOfBattle(f); }
      Boolean marchingToSoundOfBattle() const { return d_movement.marchingToSoundOfBattle(); }

      Boolean shouldMove() const { return d_movement.shouldMove(); }
      Boolean willDoBattle() const { return d_movement.willDoBattle(); }
      Boolean isAvoiding() const { return d_movement.isAvoiding(); }
      Boolean shouldCheckEnemy() const { return d_movement.shouldCheckEnemy(); }
      Boolean isRetreating() const { return d_movement.isRetreating(); }
      Boolean isNormalActivity() const { return d_movement.isNormalActivity(getSide()); }

      Boolean isHolding() const { return d_movement.isHolding(); }
      Boolean isResting() const { return d_movement.isResting(); }
      Boolean isDigging() const { return d_movement.isDigging(); }
      Boolean isMoving() const { return d_movement.isMoving(); }
      Boolean isSieging() const { return d_movement.isSieging(); }
      Boolean isGarrison() const { return d_movement.isGarrison(); }
      Boolean isRaiding() const { return d_movement.isRaiding(); }
      Boolean isMovingToBattle() const { return d_movement.isMovingToBattle(); }
      Boolean isInBattle() const { return d_movement.isInBattle(); }
      Boolean isWithdrawing() const { return d_movement.isWithdrawing(); }
      Boolean isRouting() const { return d_movement.isRouting(); }
      Boolean isSurrendering() const { return d_movement.isSurrendering(); }
      Boolean shouldCheckForAutoRest() const { return d_movement.shouldCheckForAutoRest(); }
      Boolean canBeInBattle() const { return d_movement.canBeInBattle(); }


      void hasPursuitTarget(Boolean f) { d_movement.hasPursuitTarget(f); }
      Boolean hasPursuitTarget() const { return d_movement.hasPursuitTarget(); }

      void takingThisTown(ITown t) { d_movement.takingThisTown(t); }
      void notTakingTown() { d_movement.notTakingTown(); }
      ITown takingThisTown() const { return d_movement.takingThisTown(); }
      Boolean takingOverTown() const { return d_movement.takingOverTown(); }


      void clearEnemyRatio() { d_movement.clearEnemyRatio(); }
      ForceRatio enemyRatio() const { return d_movement.enemyRatio(); }
      void enemyRatio(ForceRatio ratio) { d_movement.enemyRatio(ratio); }
      void setAggressionValues(Orders::Aggression::Value ag, Orders::Posture::Type p)
      {
        if(!aggressionLocked())
          d_movement.setAggressionValues(ag, p);
      }

      Orders::Aggression::Value getActiveAggression() const { return d_movement.getActiveAggression(); }
      Orders::Posture::Type posture() const { return d_movement.posture(); }

      void setNextDest(ITown t) { d_movement.setNextDest(t); }
      void setTarget(const ICommandPosition& cpi) { d_movement.setTarget(cpi); }
      void retreatTown(ITown t) { d_movement.retreatTown(t); }
      void clearRetreatTown() { d_movement.clearRetreatTown(); }

      Boolean hasRetreatTown() const { return d_movement.hasRetreatTown(); }
      ITown getNextDest() const { return d_movement.getNextDest(); }
      ICommandPosition getTarget() const { return d_movement.getTarget(); }
      ITown retreatTown() const { return d_movement.retreatTown(); }

      /*
       * Current route
       */

      RouteList& currentRouteList() { return d_movement.currentRouteList(); }
      const RouteList& currentRouteList() const { return d_movement.currentRouteList(); }
      void rewindCurrentRoute() { d_movement.rewindCurrentRoute(); }
      void resetCurrentRoute() { d_movement.resetCurrentRoute(); }
      Boolean currentRouteIter() { return d_movement.currentRouteIter(); }
      void currentRouteNode(ITown town) { d_movement.currentRouteNode(town); }
      void removeCurrentRouteNode(ITown town) { d_movement.removeCurrentRouteNode(town); }
      ITown currentRouteNode() { return d_movement.currentRouteNode(); }

      /*
       *  previous route
       */

      void rewindPreviousRoute() { d_movement.rewindPreviousRoute(); }
      Boolean previousRouteIter() { return d_movement.previousRouteIter(); }
      void previousRouteNode(ITown town) { d_movement.previousRouteNode(town); }
      void removePreviousRouteNode(ITown town) { d_movement.removePreviousRouteNode(town); }
      ITown previousRouteNode() { return d_movement.previousRouteNode(); }

      /*
       * Supply Line
       */

      RouteList& supplyRouteList() { return d_movement.supplyRouteList(); }
      const RouteList& supplyRouteList() const { return d_movement.supplyRouteList(); }
      void rewindSupplyRoute() { d_movement.rewindSupplyRoute(); }
      void resetSupplyRoute() { d_movement.resetSupplyRoute(); }
      void supplyRouteNode(ITown town) { d_movement.supplyRouteNode(town); }
      Boolean supplyRouteIter() { return d_movement.supplyRouteIter(); }
      ITown supplyRouteNode() { return d_movement.supplyRouteNode(); }

      void startActivity(TimeTick theTime) { d_movement.startActivity(theTime); }
      void adjustActivityCount(TimeTick theTime) { d_movement.adjustActivityCount(theTime, getSide()); }
      Attribute activityCount() const { return d_movement.activityCount(); }
      void resetActivityCount() { d_movement.resetActivityCount(); }

//    Boolean modeTimeFinished(TimeTick theTime) { return d_movement.modeTimeFinished(theTime); }

      const char* getActionDescription(OD_TYPE how, TimeTick theTime) const { return d_movement.getActionDescription(how, theTime); }
      // void clearActionDescription() const { d_movement.clearActionDescription(); }

      void resetHoursResting() { d_movement.resetHoursResting(); }
      void incHoursResting() { d_movement.incHoursResting(); }
      UBYTE hoursResting() const { return d_movement.hoursResting(); }
#endif   // !defined(EDITOR)

      // Morale, Fatigue, Supply

      Attribute getMorale() const { return d_generic->morale(); }
      Attribute getFatigue() const { return d_generic->fatigue(); }
      CAMPDATA_DLL Attribute getSupply() const;
      void setMorale(Attribute m) { d_generic->morale(m); }
      void setFatigue(Attribute f) { d_generic->fatigue(f); }
      void setSupply(Attribute s) { d_generic->supply(s); }

      // Orders

      CampaignOrderList& getOrders() { return d_orders; }
      const CampaignOrderList& getOrders() const { return d_orders; }
      CampaignOrder* getCurrentOrder() { return d_orders.getCurrentOrder(); }
      const CampaignOrder* getCurrentOrder() const { return d_orders.getCurrentOrder(); }
      const CampaignOrder* getLastOrder() const { return d_orders.getLastOrder(); }
      CAMPDATA_DLL void sendOrder(const CampaignOrder* order, TimeTick arrival, TimeTick tryAgainAt);
      void clearOrders() { d_orders.reset(); }

      void addTownOrder(ITown town, Town_COrder::Type type, TimeTick tillWhen)
      {
         d_townOrders.append(new Town_COrder(town, type, tillWhen));
      }

      Town_COrderList& townOrders() { return d_townOrders; }
      const Town_COrderList& townOrders() const { return d_townOrders; }

      /*
       *  Unit Active functions
       */


      void forceHold(TimeTick howLong) { d_forcedHoldTime = howLong; }
      Boolean isForcedHold(TimeTick theTime) const { return theTime < d_forcedHoldTime; }
      void unitParalized(TimeTick howLong) { d_unitParalizedTime = howLong; }
      Boolean isParalized(TimeTick theTime) const { return  theTime < d_unitParalizedTime; }

      /*
       * Functions for digging fieldworks
       */

      Boolean hasFieldWorks() { return False; } //(d_fieldWorks > 0); }
      void invalidateFieldWorks() { } //d_fieldWorks = 0; }
      FieldWorksLevel getFieldWorks() const { return 0; }
#if defined(CUSTOMIZE)
      void setFieldWorks(UBYTE f) {}
#endif
      // fatigue counter

      int getFatigueCount() const { return d_fatigueCounter; }
      void addToFatigueCount(int value) { d_fatigueCounter += value; }
      void clearFatigueCount() { d_fatigueCounter = 0; }

      // FOG of war

#if !defined(EDITOR)
      /*
       * Fog of war related functions
       */

      CAMPDATA_DLL void setSeen(TimeTick tick);
      void setNotSeen() { d_enemyKnowsThis = NotSeen; }
      CAMPDATA_DLL Boolean isSeen() const;// { return d_seen; }
      Boolean stillVisible(TimeTick tick) const { return (d_visibleUntil > tick); }
      const CampaignPosition& getLastSeenPosition() const { return d_lastSeenLocation; }
      void setLastSeenLocation() { d_lastSeenLocation = d_location; }
      void setInfoQuality(InfoQuality v) { d_enemyKnowsThis = v; }
      CAMPDATA_DLL InfoQuality getInfoQuality() const;
#else
      InfoQuality getInfoQuality() const { return FullInfo; }
#endif   // EDITOR

      // Interface to Generic

      bool isDead() const { return d_generic->isDead(); }
      void isDead(bool flag) { d_generic->isDead(flag); }

      ILeader getLeader() { return campLeader(d_generic->leader()); }
      const ILeader getLeader() const { return campLeader(d_generic->leader()); }

      Side getSide() const { return d_generic->side(); }
      void setSide(Side side) { d_generic->side(side); }

      void setNation(Nationality n) { d_generic->nation(n); }
      Nationality getNation() const { return d_generic->nation(); }

      CAMPDATA_DLL void init(RefGenericCP cp);
         // Mark as used
      CAMPDATA_DLL void reset();
         // Mark as unused


      const char* getName() const { return d_generic->getName(); }
      const char* getNameNotNull(const char* def = 0) const  { return d_generic->getNameNotNull(def); }
      void setName(char* s) { d_generic->setName(s); }


      ICommandPosition getParent() const { return campCP(d_generic->getParent()); }
      ICommandPosition getSister() const { return campCP(d_generic->getSister()); }
      ICommandPosition getChild() const { return campCP(d_generic->getChild()); }

      CPIndex getSelf() const { return d_generic->getSelf(); }

      bool hasChild() const { return d_generic->hasChild(); }
      bool hasSister() const { return d_generic->hasSister(); }
      bool hasParent() const { return d_generic->hasParent(); }

      ISP getSPEntry() const { return d_generic->getSPEntry(); }
      SPCount spCount(bool all) const { return d_generic->spCount(all); }
      bool isSPattached(const ConstISP& isp) const { return d_generic->isSPattached(isp); }

      CAMPDATA_DLL Boolean isRealUnit() const;

      void clearTypeFlags()                   { d_generic->clearFlags(); }
      void makeInfantryType(Boolean f)        { d_generic->makeInfantryType(f); }
      void makeCavalryType(Boolean f)         { d_generic->makeCavalryType(f); }
      void makeArtilleryType(Boolean f)       { d_generic->makeArtilleryType(f); }
      void makeGuard(Boolean f)               { d_generic->makeGuard(f); }
      void makeOldGuard(Boolean f)            { d_generic->makeGuard(f); }
      void makeCoO(Boolean f)                 { d_generic->makeCoO(f); }

      Boolean isInfantry()     const { return d_generic->isInfantry(); }
      Boolean isCavalry()      const { return d_generic->isCavalry(); }
      Boolean isArtillery()    const { return d_generic->isArtillery(); }
      Boolean isCombinedArms() const { return d_generic->isCombinedArms(); }
      Boolean isGuard()        const { return d_generic->isGuard(); }
      Boolean isOldGuard()     const { return d_generic->isOldGuard(); }
      Boolean isCoO()          const { return d_generic->isCoO(); }

#if !defined(EDITOR)
#ifdef DEBUG
      static const char* getModeDescription(CampaignMovement::CP_Mode mode) { return CampaignMovement::getModeDescription(mode); }
#endif
#endif


      // Ranks

      const Rank& getRank() const { return d_generic->getRank(); }
      void setRank(const Rank& r) { d_generic->setRank(r); }
      RankEnum getRankEnum() const { return d_generic->getRankEnum(); }
      Boolean isHigher(RankEnum r) const { return d_generic->isHigher(r); }
      Boolean isLower(RankEnum r) const { return d_generic->isLower(r); }
      Boolean sameRank(RankEnum r) const { return d_generic->sameRank(r); }

      /*
       * Inital orders/aggression at start up
       */

      Orders::Type::Value startOrder() const { return getCurrentOrder()->getOrderOnArrival(); }
      void startOrder(Orders::Type::Value o) { getCurrentOrder()->setOrderOnArrival(o); }

      Orders::Aggression::Value startAggression() const { return getCurrentOrder()->getAggressLevel(); }
      void startAggression(Orders::Aggression::Value a) { getCurrentOrder()->setAggressLevel(a); }

      // Algorithms
      CAMPDATA_DLL Boolean canMoveAtNight() const;

      // allocators / deallocators
      void* operator new(size_t size);
#ifdef _MSC_VER
      void operator delete(void* deadObject);
#else
      void operator delete(void* deadObject, size_t size);
#endif
      unsigned short calculateChecksum(void);
      static const int ChunkSize;

   private:
      /*
       * The data
       */

      static const UWORD s_fileVersion;

      GenericCP*  d_generic;        // pointer to generic ComamndPosition

      void setFlags(UBYTE mask, Boolean f)
      {
        if(f)
          d_flags |= mask;
        else
          d_flags &= ~mask;
      }

      // Campaign Specific Information
      enum Flags {
         Used             = 0x01,     // is this item used
         Active           = 0x02,     // is this unit active
         Trapped          = 0x04,     // is it trapped (surrounded)
         AggressionLocked = 0x08,     // are aggression values locked
         CrossingRiver    = 0x10,     // are we in the process of crossing a river
         NewCommand       = 0x20      // is this a newly created command (via town builds)
      };

      UBYTE d_flags;

      CampaignPosition  d_location;    // Campaign Position
#if !defined(EDITOR)
      CampaignMovement  d_movement;    // Combat and movement variables
#endif   // !defined(EDITOR)
      CampaignOrderList d_orders;
      Town_COrderList   d_townOrders;  // town related orders, building depots, blowing bridges,e tc.

      TimeTick          d_forcedHoldTime;    // Unit is forced to temporarily hold due to town fire, etc.
      TimeTick          d_unitParalizedTime; // Unit is temporarily paralized and unable to do anything
      int               d_fatigueCounter;    // running count of today's fatigue losses
#if !defined(EDITOR)
      // FOG of War variables
      CampaignPosition  d_lastSeenLocation;
      TimeTick          d_visibleUntil;           // timer counter
      InfoQuality       d_enemyKnowsThis;         // The enemy knows this much info about us
      SWORD             d_fowPercent;             // percent differience in what
                                                  // enemy thinks we have and what we have
#endif   // EDITOR


};


/*
 * Storage of Campaign Command Position Info
 * Stored in array for more efficient memory usage
 */

class CommandPositionList : public SList<CommandPosition>
{
      typedef SList<CommandPosition> Super;
   public:
      CommandPositionList();
      ~CommandPositionList();

      /*
       * File Interface
       */

      Boolean readData(FileReader& f, OrderBattle* ob);
      Boolean writeData(FileWriter& f, OrderBattle* ob) const;

      // CommandPosition* addCP(CPIndex cpi, RefGenericCP cp);
      CommandPosition* addCP(RefGenericCP cp);
      // void delCP(CPIndex cpi);

      void cleanup();
         // Garbage collection

      void reset();
         // delete all items

   private:
      static const UWORD s_fileVersion;

};

/*
 * Some useful little functions
 */

inline GenericCP* generic(const ICommandPosition& cpi)
{
   if(cpi == NoCommandPosition)
      return 0;
   else
      return cpi->generic();
}

inline const GenericCP* generic(const ConstICommandPosition& cpi)
{
   if(cpi == NoCommandPosition)
      return 0;
   else
      return cpi->generic();
}



#endif /* COMPOS_HPP */

/*
 *----------------------------------------------------------------------
 * $Log$
 *----------------------------------------------------------------------
 */
