# Microsoft Developer Studio Generated NMAKE File, Based on campdata.dsp
!IF "$(CFG)" == ""
CFG=campdata - Win32 Editor Debug
!MESSAGE No configuration specified. Defaulting to campdata - Win32 Editor Debug.
!ENDIF 

!IF "$(CFG)" != "campdata - Win32 Release" && "$(CFG)" != "campdata - Win32 Debug" && "$(CFG)" != "campdata - Win32 Editor Debug" && "$(CFG)" != "campdata - Win32 Editor Release"
!MESSAGE Invalid configuration "$(CFG)" specified.
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "campdata.mak" CFG="campdata - Win32 Editor Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "campdata - Win32 Release" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE "campdata - Win32 Debug" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE "campdata - Win32 Editor Debug" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE "campdata - Win32 Editor Release" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE 
!ERROR An invalid configuration is specified.
!ENDIF 

!IF "$(OS)" == "Windows_NT"
NULL=
!ELSE 
NULL=nul
!ENDIF 

CPP=cl.exe
MTL=midl.exe
RSC=rc.exe

!IF  "$(CFG)" == "campdata - Win32 Release"

OUTDIR=.\..\..\exe
INTDIR=.\o\release
# Begin Custom Macros
OutDir=.\..\..\exe
# End Custom Macros

!IF "$(RECURSE)" == "0" 

ALL : "$(OUTDIR)\campdata.dll"

!ELSE 

ALL : "gamesup - Win32 Release" "system - Win32 Release" "ob - Win32 Release" "$(OUTDIR)\campdata.dll"

!ENDIF 

!IF "$(RECURSE)" == "1" 
CLEAN :"ob - Win32 ReleaseCLEAN" "system - Win32 ReleaseCLEAN" "gamesup - Win32 ReleaseCLEAN" 
!ELSE 
CLEAN :
!ENDIF 
	-@erase "$(INTDIR)\armies.obj"
	-@erase "$(INTDIR)\armistic.obj"
	-@erase "$(INTDIR)\armyutil.obj"
	-@erase "$(INTDIR)\c_cond.obj"
	-@erase "$(INTDIR)\c_obutil.obj"
	-@erase "$(INTDIR)\c_rand.obj"
	-@erase "$(INTDIR)\c_sounds.obj"
	-@erase "$(INTDIR)\camp_snd.obj"
	-@erase "$(INTDIR)\campdimp.obj"
	-@erase "$(INTDIR)\campdint.obj"
	-@erase "$(INTDIR)\campevt.obj"
	-@erase "$(INTDIR)\campinfo.obj"
	-@erase "$(INTDIR)\campint.obj"
	-@erase "$(INTDIR)\campmsg.obj"
	-@erase "$(INTDIR)\campord.obj"
	-@erase "$(INTDIR)\camppos.obj"
	-@erase "$(INTDIR)\campside.obj"
	-@erase "$(INTDIR)\camptime.obj"
	-@erase "$(INTDIR)\campunit.obj"
	-@erase "$(INTDIR)\cbattle.obj"
	-@erase "$(INTDIR)\compos.obj"
	-@erase "$(INTDIR)\ctimdata.obj"
	-@erase "$(INTDIR)\cu_mode.obj"
	-@erase "$(INTDIR)\cwin_int.obj"
	-@erase "$(INTDIR)\gameob.obj"
	-@erase "$(INTDIR)\ileader.obj"
	-@erase "$(INTDIR)\measure.obj"
	-@erase "$(INTDIR)\morale.obj"
	-@erase "$(INTDIR)\randevnt.obj"
	-@erase "$(INTDIR)\repopool.obj"
	-@erase "$(INTDIR)\route.obj"
	-@erase "$(INTDIR)\routelst.obj"
	-@erase "$(INTDIR)\supplyline.obj"
	-@erase "$(INTDIR)\terrain.obj"
	-@erase "$(INTDIR)\town.obj"
	-@erase "$(INTDIR)\townbld.obj"
	-@erase "$(INTDIR)\townordr.obj"
	-@erase "$(INTDIR)\unitlist.obj"
	-@erase "$(INTDIR)\vc60.idb"
	-@erase "$(INTDIR)\vc60.pdb"
	-@erase "$(INTDIR)\weather.obj"
	-@erase "$(OUTDIR)\campdata.dll"
	-@erase "$(OUTDIR)\campdata.exp"
	-@erase "$(OUTDIR)\campdata.lib"
	-@erase "$(OUTDIR)\campdata.pdb"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

"$(INTDIR)" :
    if not exist "$(INTDIR)/$(NULL)" mkdir "$(INTDIR)"

CPP_PROJ=/nologo /MD /W3 /Gi /GR /GX /Zi /O2 /I "..\h" /I "..\..\res" /I "..\system" /I "..\gamesup" /I "..\ob" /I "..\campLogic" /D "NDEBUG" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "EXPORT_CAMPDATA_DLL" /Fp"$(INTDIR)\campdata.pch" /YX"stdinc.hpp" /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /c 
MTL_PROJ=/nologo /D "NDEBUG" /mktyplib203 /win32 
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\campdata.bsc" 
BSC32_SBRS= \
	
LINK32=link.exe
LINK32_FLAGS=User32.lib /nologo /dll /incremental:no /pdb:"$(OUTDIR)\campdata.pdb" /debug /machine:I386 /out:"$(OUTDIR)\campdata.dll" /implib:"$(OUTDIR)\campdata.lib" 
LINK32_OBJS= \
	"$(INTDIR)\armies.obj" \
	"$(INTDIR)\armistic.obj" \
	"$(INTDIR)\armyutil.obj" \
	"$(INTDIR)\c_cond.obj" \
	"$(INTDIR)\c_obutil.obj" \
	"$(INTDIR)\c_rand.obj" \
	"$(INTDIR)\c_sounds.obj" \
	"$(INTDIR)\camp_snd.obj" \
	"$(INTDIR)\campdimp.obj" \
	"$(INTDIR)\campdint.obj" \
	"$(INTDIR)\campevt.obj" \
	"$(INTDIR)\campinfo.obj" \
	"$(INTDIR)\campint.obj" \
	"$(INTDIR)\campmsg.obj" \
	"$(INTDIR)\campord.obj" \
	"$(INTDIR)\camppos.obj" \
	"$(INTDIR)\campside.obj" \
	"$(INTDIR)\camptime.obj" \
	"$(INTDIR)\campunit.obj" \
	"$(INTDIR)\cbattle.obj" \
	"$(INTDIR)\compos.obj" \
	"$(INTDIR)\ctimdata.obj" \
	"$(INTDIR)\cu_mode.obj" \
	"$(INTDIR)\cwin_int.obj" \
	"$(INTDIR)\gameob.obj" \
	"$(INTDIR)\ileader.obj" \
	"$(INTDIR)\measure.obj" \
	"$(INTDIR)\morale.obj" \
	"$(INTDIR)\randevnt.obj" \
	"$(INTDIR)\repopool.obj" \
	"$(INTDIR)\route.obj" \
	"$(INTDIR)\routelst.obj" \
	"$(INTDIR)\supplyline.obj" \
	"$(INTDIR)\terrain.obj" \
	"$(INTDIR)\town.obj" \
	"$(INTDIR)\townbld.obj" \
	"$(INTDIR)\townordr.obj" \
	"$(INTDIR)\unitlist.obj" \
	"$(INTDIR)\weather.obj" \
	"$(OUTDIR)\ob.lib" \
	"$(OUTDIR)\system.lib" \
	"$(OUTDIR)\gamesup.lib"

"$(OUTDIR)\campdata.dll" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

!ELSEIF  "$(CFG)" == "campdata - Win32 Debug"

OUTDIR=.\..\..\exe
INTDIR=.\o\debug
# Begin Custom Macros
OutDir=.\..\..\exe
# End Custom Macros

!IF "$(RECURSE)" == "0" 

ALL : "$(OUTDIR)\campdataDB.dll"

!ELSE 

ALL : "gamesup - Win32 Debug" "system - Win32 Debug" "ob - Win32 Debug" "$(OUTDIR)\campdataDB.dll"

!ENDIF 

!IF "$(RECURSE)" == "1" 
CLEAN :"ob - Win32 DebugCLEAN" "system - Win32 DebugCLEAN" "gamesup - Win32 DebugCLEAN" 
!ELSE 
CLEAN :
!ENDIF 
	-@erase "$(INTDIR)\armies.obj"
	-@erase "$(INTDIR)\armistic.obj"
	-@erase "$(INTDIR)\armyutil.obj"
	-@erase "$(INTDIR)\c_cond.obj"
	-@erase "$(INTDIR)\c_obutil.obj"
	-@erase "$(INTDIR)\c_rand.obj"
	-@erase "$(INTDIR)\c_sounds.obj"
	-@erase "$(INTDIR)\camp_snd.obj"
	-@erase "$(INTDIR)\campdimp.obj"
	-@erase "$(INTDIR)\campdint.obj"
	-@erase "$(INTDIR)\campevt.obj"
	-@erase "$(INTDIR)\campinfo.obj"
	-@erase "$(INTDIR)\campint.obj"
	-@erase "$(INTDIR)\campmsg.obj"
	-@erase "$(INTDIR)\campord.obj"
	-@erase "$(INTDIR)\camppos.obj"
	-@erase "$(INTDIR)\campside.obj"
	-@erase "$(INTDIR)\camptime.obj"
	-@erase "$(INTDIR)\campunit.obj"
	-@erase "$(INTDIR)\cbattle.obj"
	-@erase "$(INTDIR)\compos.obj"
	-@erase "$(INTDIR)\ctimdata.obj"
	-@erase "$(INTDIR)\cu_mode.obj"
	-@erase "$(INTDIR)\cwin_int.obj"
	-@erase "$(INTDIR)\gameob.obj"
	-@erase "$(INTDIR)\ileader.obj"
	-@erase "$(INTDIR)\measure.obj"
	-@erase "$(INTDIR)\morale.obj"
	-@erase "$(INTDIR)\randevnt.obj"
	-@erase "$(INTDIR)\repopool.obj"
	-@erase "$(INTDIR)\route.obj"
	-@erase "$(INTDIR)\routelst.obj"
	-@erase "$(INTDIR)\supplyline.obj"
	-@erase "$(INTDIR)\terrain.obj"
	-@erase "$(INTDIR)\town.obj"
	-@erase "$(INTDIR)\townbld.obj"
	-@erase "$(INTDIR)\townordr.obj"
	-@erase "$(INTDIR)\unitlist.obj"
	-@erase "$(INTDIR)\unitlog.obj"
	-@erase "$(INTDIR)\vc60.idb"
	-@erase "$(INTDIR)\vc60.pdb"
	-@erase "$(INTDIR)\weather.obj"
	-@erase "$(OUTDIR)\campdataDB.dll"
	-@erase "$(OUTDIR)\campdataDB.exp"
	-@erase "$(OUTDIR)\campdataDB.ilk"
	-@erase "$(OUTDIR)\campdataDB.lib"
	-@erase "$(OUTDIR)\campdataDB.pdb"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

"$(INTDIR)" :
    if not exist "$(INTDIR)/$(NULL)" mkdir "$(INTDIR)"

CPP_PROJ=/nologo /MDd /W3 /Gm /Gi /GR /GX /ZI /Od /I "..\h" /I "..\..\res" /I "..\system" /I "..\gamesup" /I "..\ob" /I "..\campLogic" /D "_DEBUG" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "EXPORT_CAMPDATA_DLL" /Fp"$(INTDIR)\campdata.pch" /YX"stdinc.hpp" /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /GZ /c 
MTL_PROJ=/nologo /D "_DEBUG" /mktyplib203 /win32 
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\campdata.bsc" 
BSC32_SBRS= \
	
LINK32=link.exe
LINK32_FLAGS=User32.lib /nologo /dll /incremental:yes /pdb:"$(OUTDIR)\campdataDB.pdb" /debug /machine:I386 /out:"$(OUTDIR)\campdataDB.dll" /implib:"$(OUTDIR)\campdataDB.lib" /pdbtype:sept 
LINK32_OBJS= \
	"$(INTDIR)\armies.obj" \
	"$(INTDIR)\armistic.obj" \
	"$(INTDIR)\armyutil.obj" \
	"$(INTDIR)\c_cond.obj" \
	"$(INTDIR)\c_obutil.obj" \
	"$(INTDIR)\c_rand.obj" \
	"$(INTDIR)\c_sounds.obj" \
	"$(INTDIR)\camp_snd.obj" \
	"$(INTDIR)\campdimp.obj" \
	"$(INTDIR)\campdint.obj" \
	"$(INTDIR)\campevt.obj" \
	"$(INTDIR)\campinfo.obj" \
	"$(INTDIR)\campint.obj" \
	"$(INTDIR)\campmsg.obj" \
	"$(INTDIR)\campord.obj" \
	"$(INTDIR)\camppos.obj" \
	"$(INTDIR)\campside.obj" \
	"$(INTDIR)\camptime.obj" \
	"$(INTDIR)\campunit.obj" \
	"$(INTDIR)\cbattle.obj" \
	"$(INTDIR)\compos.obj" \
	"$(INTDIR)\ctimdata.obj" \
	"$(INTDIR)\cu_mode.obj" \
	"$(INTDIR)\cwin_int.obj" \
	"$(INTDIR)\gameob.obj" \
	"$(INTDIR)\ileader.obj" \
	"$(INTDIR)\measure.obj" \
	"$(INTDIR)\morale.obj" \
	"$(INTDIR)\randevnt.obj" \
	"$(INTDIR)\repopool.obj" \
	"$(INTDIR)\route.obj" \
	"$(INTDIR)\routelst.obj" \
	"$(INTDIR)\supplyline.obj" \
	"$(INTDIR)\terrain.obj" \
	"$(INTDIR)\town.obj" \
	"$(INTDIR)\townbld.obj" \
	"$(INTDIR)\townordr.obj" \
	"$(INTDIR)\unitlist.obj" \
	"$(INTDIR)\unitlog.obj" \
	"$(INTDIR)\weather.obj" \
	"$(OUTDIR)\obDB.lib" \
	"$(OUTDIR)\systemDB.lib" \
	"$(OUTDIR)\gamesupDB.lib"

"$(OUTDIR)\campdataDB.dll" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

!ELSEIF  "$(CFG)" == "campdata - Win32 Editor Debug"

OUTDIR=.\..\..\exe
INTDIR=.\o\Editor\Debug
# Begin Custom Macros
OutDir=.\..\..\exe
# End Custom Macros

!IF "$(RECURSE)" == "0" 

ALL : "$(OUTDIR)\campdataEDDB.dll"

!ELSE 

ALL : "gamesup - Win32 Editor Debug" "system - Win32 Editor Debug" "ob - Win32 Editor Debug" "$(OUTDIR)\campdataEDDB.dll"

!ENDIF 

!IF "$(RECURSE)" == "1" 
CLEAN :"ob - Win32 Editor DebugCLEAN" "system - Win32 Editor DebugCLEAN" "gamesup - Win32 Editor DebugCLEAN" 
!ELSE 
CLEAN :
!ENDIF 
	-@erase "$(INTDIR)\armies.obj"
	-@erase "$(INTDIR)\armistic.obj"
	-@erase "$(INTDIR)\armyutil.obj"
	-@erase "$(INTDIR)\c_cond.obj"
	-@erase "$(INTDIR)\c_obutil.obj"
	-@erase "$(INTDIR)\c_rand.obj"
	-@erase "$(INTDIR)\c_sounds.obj"
	-@erase "$(INTDIR)\camp_snd.obj"
	-@erase "$(INTDIR)\campdimp.obj"
	-@erase "$(INTDIR)\campdint.obj"
	-@erase "$(INTDIR)\campevt.obj"
	-@erase "$(INTDIR)\campinfo.obj"
	-@erase "$(INTDIR)\campint.obj"
	-@erase "$(INTDIR)\campmsg.obj"
	-@erase "$(INTDIR)\campord.obj"
	-@erase "$(INTDIR)\camppos.obj"
	-@erase "$(INTDIR)\campside.obj"
	-@erase "$(INTDIR)\camptime.obj"
	-@erase "$(INTDIR)\campunit.obj"
	-@erase "$(INTDIR)\compos.obj"
	-@erase "$(INTDIR)\ctimdata.obj"
	-@erase "$(INTDIR)\cu_mode.obj"
	-@erase "$(INTDIR)\cwin_int.obj"
	-@erase "$(INTDIR)\gameob.obj"
	-@erase "$(INTDIR)\ileader.obj"
	-@erase "$(INTDIR)\measure.obj"
	-@erase "$(INTDIR)\morale.obj"
	-@erase "$(INTDIR)\randevnt.obj"
	-@erase "$(INTDIR)\repopool.obj"
	-@erase "$(INTDIR)\routelst.obj"
	-@erase "$(INTDIR)\supplyline.obj"
	-@erase "$(INTDIR)\terrain.obj"
	-@erase "$(INTDIR)\town.obj"
	-@erase "$(INTDIR)\townbld.obj"
	-@erase "$(INTDIR)\townordr.obj"
	-@erase "$(INTDIR)\unitlist.obj"
	-@erase "$(INTDIR)\unitlog.obj"
	-@erase "$(INTDIR)\vc60.idb"
	-@erase "$(INTDIR)\vc60.pdb"
	-@erase "$(INTDIR)\weather.obj"
	-@erase "$(OUTDIR)\campdataEDDB.dll"
	-@erase "$(OUTDIR)\campdataEDDB.exp"
	-@erase "$(OUTDIR)\campdataEDDB.ilk"
	-@erase "$(OUTDIR)\campdataEDDB.lib"
	-@erase "$(OUTDIR)\campdataEDDB.pdb"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

"$(INTDIR)" :
    if not exist "$(INTDIR)/$(NULL)" mkdir "$(INTDIR)"

CPP_PROJ=/nologo /MDd /W3 /Gm /Gi /GR /GX /ZI /Od /I "..\h" /I "..\..\res" /I "..\system" /I "..\gamesup" /I "..\ob" /I "..\campLogic" /D "_USRDLL" /D "EXPORT_CAMPDATA_DLL" /D "_DEBUG" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /D "EDITOR" /Fp"$(INTDIR)\campdata.pch" /YX"stdinc.hpp" /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /GZ /c 
MTL_PROJ=/nologo /D "_DEBUG" /mktyplib203 /win32 
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\campdata.bsc" 
BSC32_SBRS= \
	
LINK32=link.exe
LINK32_FLAGS=User32.lib /nologo /dll /incremental:yes /pdb:"$(OUTDIR)\campdataEDDB.pdb" /debug /machine:I386 /out:"$(OUTDIR)\campdataEDDB.dll" /implib:"$(OUTDIR)\campdataEDDB.lib" /pdbtype:sept 
LINK32_OBJS= \
	"$(INTDIR)\armies.obj" \
	"$(INTDIR)\armistic.obj" \
	"$(INTDIR)\armyutil.obj" \
	"$(INTDIR)\c_cond.obj" \
	"$(INTDIR)\c_obutil.obj" \
	"$(INTDIR)\c_rand.obj" \
	"$(INTDIR)\c_sounds.obj" \
	"$(INTDIR)\camp_snd.obj" \
	"$(INTDIR)\campdimp.obj" \
	"$(INTDIR)\campdint.obj" \
	"$(INTDIR)\campevt.obj" \
	"$(INTDIR)\campinfo.obj" \
	"$(INTDIR)\campint.obj" \
	"$(INTDIR)\campmsg.obj" \
	"$(INTDIR)\campord.obj" \
	"$(INTDIR)\camppos.obj" \
	"$(INTDIR)\campside.obj" \
	"$(INTDIR)\camptime.obj" \
	"$(INTDIR)\campunit.obj" \
	"$(INTDIR)\compos.obj" \
	"$(INTDIR)\ctimdata.obj" \
	"$(INTDIR)\cu_mode.obj" \
	"$(INTDIR)\cwin_int.obj" \
	"$(INTDIR)\gameob.obj" \
	"$(INTDIR)\ileader.obj" \
	"$(INTDIR)\measure.obj" \
	"$(INTDIR)\morale.obj" \
	"$(INTDIR)\randevnt.obj" \
	"$(INTDIR)\repopool.obj" \
	"$(INTDIR)\routelst.obj" \
	"$(INTDIR)\supplyline.obj" \
	"$(INTDIR)\terrain.obj" \
	"$(INTDIR)\town.obj" \
	"$(INTDIR)\townbld.obj" \
	"$(INTDIR)\townordr.obj" \
	"$(INTDIR)\unitlist.obj" \
	"$(INTDIR)\unitlog.obj" \
	"$(INTDIR)\weather.obj" \
	"$(OUTDIR)\obEDDB.lib" \
	"$(OUTDIR)\systemDB.lib" \
	"$(OUTDIR)\gamesupDB.lib"

"$(OUTDIR)\campdataEDDB.dll" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

!ELSEIF  "$(CFG)" == "campdata - Win32 Editor Release"

OUTDIR=.\..\..\exe
INTDIR=.\o\Editor\Release
# Begin Custom Macros
OutDir=.\..\..\exe
# End Custom Macros

!IF "$(RECURSE)" == "0" 

ALL : "$(OUTDIR)\campdataED.dll"

!ELSE 

ALL : "gamesup - Win32 Editor Release" "system - Win32 Editor Release" "ob - Win32 Editor Release" "$(OUTDIR)\campdataED.dll"

!ENDIF 

!IF "$(RECURSE)" == "1" 
CLEAN :"ob - Win32 Editor ReleaseCLEAN" "system - Win32 Editor ReleaseCLEAN" "gamesup - Win32 Editor ReleaseCLEAN" 
!ELSE 
CLEAN :
!ENDIF 
	-@erase "$(INTDIR)\armies.obj"
	-@erase "$(INTDIR)\armistic.obj"
	-@erase "$(INTDIR)\armyutil.obj"
	-@erase "$(INTDIR)\c_cond.obj"
	-@erase "$(INTDIR)\c_obutil.obj"
	-@erase "$(INTDIR)\c_rand.obj"
	-@erase "$(INTDIR)\c_sounds.obj"
	-@erase "$(INTDIR)\camp_snd.obj"
	-@erase "$(INTDIR)\campdimp.obj"
	-@erase "$(INTDIR)\campdint.obj"
	-@erase "$(INTDIR)\campevt.obj"
	-@erase "$(INTDIR)\campinfo.obj"
	-@erase "$(INTDIR)\campint.obj"
	-@erase "$(INTDIR)\campmsg.obj"
	-@erase "$(INTDIR)\campord.obj"
	-@erase "$(INTDIR)\camppos.obj"
	-@erase "$(INTDIR)\campside.obj"
	-@erase "$(INTDIR)\camptime.obj"
	-@erase "$(INTDIR)\campunit.obj"
	-@erase "$(INTDIR)\compos.obj"
	-@erase "$(INTDIR)\ctimdata.obj"
	-@erase "$(INTDIR)\cu_mode.obj"
	-@erase "$(INTDIR)\cwin_int.obj"
	-@erase "$(INTDIR)\gameob.obj"
	-@erase "$(INTDIR)\ileader.obj"
	-@erase "$(INTDIR)\measure.obj"
	-@erase "$(INTDIR)\morale.obj"
	-@erase "$(INTDIR)\randevnt.obj"
	-@erase "$(INTDIR)\repopool.obj"
	-@erase "$(INTDIR)\routelst.obj"
	-@erase "$(INTDIR)\supplyline.obj"
	-@erase "$(INTDIR)\terrain.obj"
	-@erase "$(INTDIR)\town.obj"
	-@erase "$(INTDIR)\townbld.obj"
	-@erase "$(INTDIR)\townordr.obj"
	-@erase "$(INTDIR)\unitlist.obj"
	-@erase "$(INTDIR)\vc60.idb"
	-@erase "$(INTDIR)\weather.obj"
	-@erase "$(OUTDIR)\campdataED.dll"
	-@erase "$(OUTDIR)\campdataED.exp"
	-@erase "$(OUTDIR)\campdataED.lib"
	-@erase "$(OUTDIR)\campdataED.pdb"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

"$(INTDIR)" :
    if not exist "$(INTDIR)/$(NULL)" mkdir "$(INTDIR)"

CPP_PROJ=/nologo /MD /W3 /Gi /GR /GX /Zd /O2 /I "..\h" /I "..\..\res" /I "..\system" /I "..\gamesup" /I "..\ob" /I "..\campLogic" /D "NDEBUG" /D "_USRDLL" /D "EXPORT_CAMPDATA_DLL" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /D "EDITOR" /Fp"$(INTDIR)\campdata.pch" /YX"stdinc.hpp" /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /c 
MTL_PROJ=/nologo /D "NDEBUG" /mktyplib203 /win32 
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\campdata.bsc" 
BSC32_SBRS= \
	
LINK32=link.exe
LINK32_FLAGS=User32.lib /nologo /dll /incremental:no /pdb:"$(OUTDIR)\campdataED.pdb" /debug /machine:I386 /out:"$(OUTDIR)\campdataED.dll" /implib:"$(OUTDIR)\campdataED.lib" 
LINK32_OBJS= \
	"$(INTDIR)\armies.obj" \
	"$(INTDIR)\armistic.obj" \
	"$(INTDIR)\armyutil.obj" \
	"$(INTDIR)\c_cond.obj" \
	"$(INTDIR)\c_obutil.obj" \
	"$(INTDIR)\c_rand.obj" \
	"$(INTDIR)\c_sounds.obj" \
	"$(INTDIR)\camp_snd.obj" \
	"$(INTDIR)\campdimp.obj" \
	"$(INTDIR)\campdint.obj" \
	"$(INTDIR)\campevt.obj" \
	"$(INTDIR)\campinfo.obj" \
	"$(INTDIR)\campint.obj" \
	"$(INTDIR)\campmsg.obj" \
	"$(INTDIR)\campord.obj" \
	"$(INTDIR)\camppos.obj" \
	"$(INTDIR)\campside.obj" \
	"$(INTDIR)\camptime.obj" \
	"$(INTDIR)\campunit.obj" \
	"$(INTDIR)\compos.obj" \
	"$(INTDIR)\ctimdata.obj" \
	"$(INTDIR)\cu_mode.obj" \
	"$(INTDIR)\cwin_int.obj" \
	"$(INTDIR)\gameob.obj" \
	"$(INTDIR)\ileader.obj" \
	"$(INTDIR)\measure.obj" \
	"$(INTDIR)\morale.obj" \
	"$(INTDIR)\randevnt.obj" \
	"$(INTDIR)\repopool.obj" \
	"$(INTDIR)\routelst.obj" \
	"$(INTDIR)\supplyline.obj" \
	"$(INTDIR)\terrain.obj" \
	"$(INTDIR)\town.obj" \
	"$(INTDIR)\townbld.obj" \
	"$(INTDIR)\townordr.obj" \
	"$(INTDIR)\unitlist.obj" \
	"$(INTDIR)\weather.obj" \
	"$(OUTDIR)\obED.lib" \
	"$(OUTDIR)\system.lib" \
	"$(OUTDIR)\gamesup.lib"

"$(OUTDIR)\campdataED.dll" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

!ENDIF 

.c{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.c{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<


!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("campdata.dep")
!INCLUDE "campdata.dep"
!ELSE 
!MESSAGE Warning: cannot find "campdata.dep"
!ENDIF 
!ENDIF 


!IF "$(CFG)" == "campdata - Win32 Release" || "$(CFG)" == "campdata - Win32 Debug" || "$(CFG)" == "campdata - Win32 Editor Debug" || "$(CFG)" == "campdata - Win32 Editor Release"
SOURCE=.\armies.cpp

"$(INTDIR)\armies.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\armistic.cpp

"$(INTDIR)\armistic.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\armyutil.cpp

"$(INTDIR)\armyutil.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\c_cond.cpp

"$(INTDIR)\c_cond.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\c_obutil.cpp

"$(INTDIR)\c_obutil.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\c_rand.cpp

"$(INTDIR)\c_rand.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\c_sounds.cpp

"$(INTDIR)\c_sounds.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\camp_snd.cpp

"$(INTDIR)\camp_snd.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\campdimp.cpp

"$(INTDIR)\campdimp.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\campdint.cpp

"$(INTDIR)\campdint.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\campevt.cpp

"$(INTDIR)\campevt.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\campinfo.cpp

"$(INTDIR)\campinfo.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\campint.cpp

"$(INTDIR)\campint.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\campmsg.cpp

"$(INTDIR)\campmsg.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\campord.cpp

"$(INTDIR)\campord.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\camppos.cpp

"$(INTDIR)\camppos.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\campside.cpp

"$(INTDIR)\campside.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\camptime.cpp

"$(INTDIR)\camptime.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\campunit.cpp

"$(INTDIR)\campunit.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\cbattle.cpp

!IF  "$(CFG)" == "campdata - Win32 Release"


"$(INTDIR)\cbattle.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "campdata - Win32 Debug"


"$(INTDIR)\cbattle.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "campdata - Win32 Editor Debug"

!ELSEIF  "$(CFG)" == "campdata - Win32 Editor Release"

!ENDIF 

SOURCE=.\compos.cpp

"$(INTDIR)\compos.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\ctimdata.cpp

"$(INTDIR)\ctimdata.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\cu_mode.cpp

"$(INTDIR)\cu_mode.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\cwin_int.cpp

"$(INTDIR)\cwin_int.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\gameob.cpp

"$(INTDIR)\gameob.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\ileader.cpp

"$(INTDIR)\ileader.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\makearmy.cpp
SOURCE=.\measure.cpp

"$(INTDIR)\measure.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\morale.cpp

"$(INTDIR)\morale.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\randevnt.cpp

"$(INTDIR)\randevnt.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\repopool.cpp

"$(INTDIR)\repopool.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\route.cpp

!IF  "$(CFG)" == "campdata - Win32 Release"


"$(INTDIR)\route.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "campdata - Win32 Debug"


"$(INTDIR)\route.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "campdata - Win32 Editor Debug"

!ELSEIF  "$(CFG)" == "campdata - Win32 Editor Release"

!ENDIF 

SOURCE=.\routelst.cpp

"$(INTDIR)\routelst.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\supplyline.cpp

"$(INTDIR)\supplyline.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\terrain.cpp

"$(INTDIR)\terrain.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\town.cpp

"$(INTDIR)\town.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\townbld.cpp

"$(INTDIR)\townbld.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\townordr.cpp

"$(INTDIR)\townordr.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\unitlist.cpp

"$(INTDIR)\unitlist.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\unitlog.cpp

!IF  "$(CFG)" == "campdata - Win32 Release"

!ELSEIF  "$(CFG)" == "campdata - Win32 Debug"


"$(INTDIR)\unitlog.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "campdata - Win32 Editor Debug"


"$(INTDIR)\unitlog.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "campdata - Win32 Editor Release"

!ENDIF 

SOURCE=.\weather.cpp

"$(INTDIR)\weather.obj" : $(SOURCE) "$(INTDIR)"


!IF  "$(CFG)" == "campdata - Win32 Release"

"ob - Win32 Release" : 
   cd "\src\sf\wargamer\SRC\OB"
   $(MAKE) /$(MAKEFLAGS) /F .\ob.mak CFG="ob - Win32 Release" 
   cd "..\campdata"

"ob - Win32 ReleaseCLEAN" : 
   cd "\src\sf\wargamer\SRC\OB"
   $(MAKE) /$(MAKEFLAGS) /F .\ob.mak CFG="ob - Win32 Release" RECURSE=1 CLEAN 
   cd "..\campdata"

!ELSEIF  "$(CFG)" == "campdata - Win32 Debug"

"ob - Win32 Debug" : 
   cd "\src\sf\wargamer\SRC\OB"
   $(MAKE) /$(MAKEFLAGS) /F .\ob.mak CFG="ob - Win32 Debug" 
   cd "..\campdata"

"ob - Win32 DebugCLEAN" : 
   cd "\src\sf\wargamer\SRC\OB"
   $(MAKE) /$(MAKEFLAGS) /F .\ob.mak CFG="ob - Win32 Debug" RECURSE=1 CLEAN 
   cd "..\campdata"

!ELSEIF  "$(CFG)" == "campdata - Win32 Editor Debug"

"ob - Win32 Editor Debug" : 
   cd "\src\sf\wargamer\SRC\OB"
   $(MAKE) /$(MAKEFLAGS) /F .\ob.mak CFG="ob - Win32 Editor Debug" 
   cd "..\campdata"

"ob - Win32 Editor DebugCLEAN" : 
   cd "\src\sf\wargamer\SRC\OB"
   $(MAKE) /$(MAKEFLAGS) /F .\ob.mak CFG="ob - Win32 Editor Debug" RECURSE=1 CLEAN 
   cd "..\campdata"

!ELSEIF  "$(CFG)" == "campdata - Win32 Editor Release"

"ob - Win32 Editor Release" : 
   cd "\src\sf\wargamer\SRC\OB"
   $(MAKE) /$(MAKEFLAGS) /F .\ob.mak CFG="ob - Win32 Editor Release" 
   cd "..\campdata"

"ob - Win32 Editor ReleaseCLEAN" : 
   cd "\src\sf\wargamer\SRC\OB"
   $(MAKE) /$(MAKEFLAGS) /F .\ob.mak CFG="ob - Win32 Editor Release" RECURSE=1 CLEAN 
   cd "..\campdata"

!ENDIF 

!IF  "$(CFG)" == "campdata - Win32 Release"

"system - Win32 Release" : 
   cd "\src\sf\wargamer\SRC\SYSTEM"
   $(MAKE) /$(MAKEFLAGS) /F .\system.mak CFG="system - Win32 Release" 
   cd "..\campdata"

"system - Win32 ReleaseCLEAN" : 
   cd "\src\sf\wargamer\SRC\SYSTEM"
   $(MAKE) /$(MAKEFLAGS) /F .\system.mak CFG="system - Win32 Release" RECURSE=1 CLEAN 
   cd "..\campdata"

!ELSEIF  "$(CFG)" == "campdata - Win32 Debug"

"system - Win32 Debug" : 
   cd "\src\sf\wargamer\SRC\SYSTEM"
   $(MAKE) /$(MAKEFLAGS) /F .\system.mak CFG="system - Win32 Debug" 
   cd "..\campdata"

"system - Win32 DebugCLEAN" : 
   cd "\src\sf\wargamer\SRC\SYSTEM"
   $(MAKE) /$(MAKEFLAGS) /F .\system.mak CFG="system - Win32 Debug" RECURSE=1 CLEAN 
   cd "..\campdata"

!ELSEIF  "$(CFG)" == "campdata - Win32 Editor Debug"

"system - Win32 Editor Debug" : 
   cd "\src\sf\wargamer\SRC\SYSTEM"
   $(MAKE) /$(MAKEFLAGS) /F .\system.mak CFG="system - Win32 Editor Debug" 
   cd "..\campdata"

"system - Win32 Editor DebugCLEAN" : 
   cd "\src\sf\wargamer\SRC\SYSTEM"
   $(MAKE) /$(MAKEFLAGS) /F .\system.mak CFG="system - Win32 Editor Debug" RECURSE=1 CLEAN 
   cd "..\campdata"

!ELSEIF  "$(CFG)" == "campdata - Win32 Editor Release"

"system - Win32 Editor Release" : 
   cd "\src\sf\wargamer\SRC\SYSTEM"
   $(MAKE) /$(MAKEFLAGS) /F .\system.mak CFG="system - Win32 Editor Release" 
   cd "..\campdata"

"system - Win32 Editor ReleaseCLEAN" : 
   cd "\src\sf\wargamer\SRC\SYSTEM"
   $(MAKE) /$(MAKEFLAGS) /F .\system.mak CFG="system - Win32 Editor Release" RECURSE=1 CLEAN 
   cd "..\campdata"

!ENDIF 

!IF  "$(CFG)" == "campdata - Win32 Release"

"gamesup - Win32 Release" : 
   cd "\src\sf\wargamer\SRC\GAMESUP"
   $(MAKE) /$(MAKEFLAGS) /F .\gamesup.mak CFG="gamesup - Win32 Release" 
   cd "..\campdata"

"gamesup - Win32 ReleaseCLEAN" : 
   cd "\src\sf\wargamer\SRC\GAMESUP"
   $(MAKE) /$(MAKEFLAGS) /F .\gamesup.mak CFG="gamesup - Win32 Release" RECURSE=1 CLEAN 
   cd "..\campdata"

!ELSEIF  "$(CFG)" == "campdata - Win32 Debug"

"gamesup - Win32 Debug" : 
   cd "\src\sf\wargamer\SRC\GAMESUP"
   $(MAKE) /$(MAKEFLAGS) /F .\gamesup.mak CFG="gamesup - Win32 Debug" 
   cd "..\campdata"

"gamesup - Win32 DebugCLEAN" : 
   cd "\src\sf\wargamer\SRC\GAMESUP"
   $(MAKE) /$(MAKEFLAGS) /F .\gamesup.mak CFG="gamesup - Win32 Debug" RECURSE=1 CLEAN 
   cd "..\campdata"

!ELSEIF  "$(CFG)" == "campdata - Win32 Editor Debug"

"gamesup - Win32 Editor Debug" : 
   cd "\src\sf\wargamer\SRC\GAMESUP"
   $(MAKE) /$(MAKEFLAGS) /F .\gamesup.mak CFG="gamesup - Win32 Editor Debug" 
   cd "..\campdata"

"gamesup - Win32 Editor DebugCLEAN" : 
   cd "\src\sf\wargamer\SRC\GAMESUP"
   $(MAKE) /$(MAKEFLAGS) /F .\gamesup.mak CFG="gamesup - Win32 Editor Debug" RECURSE=1 CLEAN 
   cd "..\campdata"

!ELSEIF  "$(CFG)" == "campdata - Win32 Editor Release"

"gamesup - Win32 Editor Release" : 
   cd "\src\sf\wargamer\SRC\GAMESUP"
   $(MAKE) /$(MAKEFLAGS) /F .\gamesup.mak CFG="gamesup - Win32 Editor Release" 
   cd "..\campdata"

"gamesup - Win32 Editor ReleaseCLEAN" : 
   cd "\src\sf\wargamer\SRC\GAMESUP"
   $(MAKE) /$(MAKEFLAGS) /F .\gamesup.mak CFG="gamesup - Win32 Editor Release" RECURSE=1 CLEAN 
   cd "..\campdata"

!ENDIF 


!ENDIF 

