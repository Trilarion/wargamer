/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef ARMIES_HPP
#define ARMIES_HPP

#ifndef __cplusplus
#error armies.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1995, Steven Morle-Green, All Rights Reserved
 *----------------------------------------------------------------------
 *
 * Data structures defining Generals, Nations, Command Positions, Units
 *
 * Campaign Interface to Order of Battle
 *
 * We are using names such as "Armies" rather than CampaignArmies
 * to retain backward compatibility with old code.
 *
 *----------------------------------------------------------------------
 */

#include "cdatadll.h"
#include "gamedefs.hpp"
#include "compos.hpp"
#include "ob.hpp"          // Generic OB
#include "cleader.hpp"
#include "ileader.hpp"     // independent and captured leaders
#include "repopool.hpp"
#include "armybase.hpp"

class OrderBattle;         // Generic OB defined in ob\ob.hpp
class CommandPositionList; // Campaign CommandPosition defined in campdata\compos.hpp
class CampaignCP;          // Campaign Command Position defined in campdata\compos.hpp
class Leader;              // Generic Leader defined in ob\leader.hpp
//class CampaignData;

#define BACKWARD_COMPATIBLE

/*
 * Campaign army interface
 */

class Armies : public ArmyBase
{
  friend class CampaignArmy_Util;
      // Prevent Copy constructor
      Armies(const Armies&);
      const Armies& operator = (const Armies&);
   public:
      CAMPDATA_DLL Armies();
      CAMPDATA_DLL ~Armies();

      /*
       * File Interface
       */

      CAMPDATA_DLL Boolean readData(FileReader& f);
      CAMPDATA_DLL Boolean writeData(FileWriter& f) const;

      /*
       * Thread locking
       * Any code that may fail if a CP, Leader or SP is deleted or
       * transfered during operations should enclose the code between
       *   army->startRead();
       *    ...code
       *   army->endRead();
       *
       * Any code that is modifying the Order of battle should
       * use startWrite and endWrite.
       */

      void startRead() const { d_ob->startRead(); }
      void endRead() const { d_ob->endRead(); }

      void startWrite() { d_ob->startWrite(); }
      void endWrite() { d_ob->endWrite(); }

      /*
       * Data access
       */

      // UBYTE getSideCount() const;
      Side getSideCount() const
      {
         ASSERT(d_ob != 0);
         return d_ob->sideCount();
      }

      SPIndex getIndex(const ConstISP& spi) const { return d_ob->getIndex(spi); }
      CPIndex getIndex(const ConstICommandPosition& cpi) const { return d_ob->getIndex(generic(cpi)); }
      LeaderIndex getIndex(const ConstILeader& leader) const { return d_ob->getIndex(generic(leader)); }

      /*------------------------------------------------------------
       * These functions are included for backwards compatibility
       */

#ifdef BACKWARD_COMPATIBLE
      // For backwards compatibility
      static CommandPosition* getCommand(const ICommandPosition& cpi) { return cpi; }
      static const CommandPosition* getCommand(const ConstICommandPosition& cpi) { return cpi; }
      // const CommandPosition* getCommand(const ICommandPosition& cpi) const { return cpi; }
      //----------- For backwards compatibility
      static ILeader getLeader(const ILeader& li) { return li; }
      static StrengthPoint* getStrengthPoint(const ISP& isp) { return isp; }

      void addCPReference(const ICommandPosition& cpi) { }
      bool delCPReference(const ICommandPosition& cpi) { return false; }
         // return true if unit is deleted
      void addLeaderReference(const ILeader& iLeader) { }   //--- RefTODO: { d_ob->addLeaderReference(iLeader); }
      bool delLeaderReference(const ILeader& iLeader) { return false; } //--- RefTODO: { return d_ob->delLeaderReference(iLeader); }
#endif

      /*
       * End of backward compatibility
       *------------------------------------------------------------
       */


      ICommandPosition command(CPIndex cpi) { return campCP(d_ob->command(cpi)); }//&d_cpList[cpi]; }
      ConstICommandPosition command(CPIndex cpi) const { return campCP(d_ob->command(cpi)); }//&d_cpList[cpi]; }


      ILeader leader(LeaderIndex ileader) { return campLeader(d_ob->leader(ileader)); }
      ConstILeader leader(LeaderIndex ileader) const { return campLeader(d_ob->leader(ileader)); }

      static ILeader getUnitLeader(ConstParamCP cpi)
      {
         // ASSERT(d_ob != 0);
         return cpi->getLeader();
         // return campLeader(d_ob->getUnitLeader(generic(cpi)));
      }

      static ICommandPosition getSister(const ICommandPosition& cp) { return cp->getSister(); }
      static ICommandPosition getChild(const ICommandPosition& cp) { return cp->getChild(); }
      static ICommandPosition getParent(const ICommandPosition& cp) { return cp->getParent(); }


      const OrderBattle* ob() const { return d_ob; }
      OrderBattle* ob() { return d_ob; }

//      CAMPDATA_DLL void addIndependentLeader(ILeader il);
      CAMPDATA_DLL const IndependentLeaderList& independentLeaders() const { return d_independentLeaders; }
      CAMPDATA_DLL IndependentLeaderList& independentLeaders() { return d_independentLeaders; }
      void chooseNewSHQ(Side side) { d_ob->chooseNewSHQ(side); }

      CAMPDATA_DLL void addCapturedLeader(ILeader il);
      const CapturedLeaderList& capturedLeaders() const { return d_capturedLeaders; }
      CapturedLeaderList& capturedLeaders() { return d_capturedLeaders; }

      CAMPDATA_DLL void addRepoSP(ISP sp, ITown town);
      CAMPDATA_DLL const RepoSPList& repoSPList() const { return d_repoList; }
      CAMPDATA_DLL RepoSPList& repoSPList() { return d_repoList; }

      const UnitTypeTable& getUnitTypes() const { return d_ob->getUnitTypes(); }
      int getMaxUnitType() const { return d_ob->getMaxUnitType(); }
      const UnitTypeItem& getUnitType(UnitType n) const { return d_ob->getUnitType(n); }

      // useful hasType functions
		CAMPDATA_DLL Boolean hasInfantry(ConstParamCP cpi) const { return d_ob->hasInfantry(generic(cpi)); }
		CAMPDATA_DLL Boolean hasCavalry(ConstParamCP cpi) const { return d_ob->hasCavalry(generic(cpi)); }
		CAMPDATA_DLL Boolean hasArtillery(ConstParamCP cpi) const { return d_ob->hasArtillery(generic(cpi)); }
		CAMPDATA_DLL Boolean hasEngineer(ConstParamCP cpi) const { return d_ob->hasEngineer(generic(cpi)); }
		CAMPDATA_DLL Boolean hasBridgeTrain(ConstParamCP cpi) const { return d_ob->hasBridgeTrain(generic(cpi)); }
      CAMPDATA_DLL Boolean hasSiegeTrain(ConstParamCP cpi) const { return d_ob->hasSiegeTrain(generic(cpi)); }
#ifdef CUSTOMIZE
      CAMPDATA_DLL Boolean unitTypeSanityCheck(const ICommandPosition& cpi);
#endif

      ICommandPosition getPresident(Side s) const { return d_ob->getPresident(s)->campaignInfo(); }
      ICommandPosition getFirstUnit(Side s) const { return campCP(d_ob->getFirstUnit(s)); }
      ICommandPosition getTop() const { return campCP(d_ob->getTop()); }

      CAMPDATA_DLL ConstICommandPosition getSHQ(Side side) const;
      void setSupremeLeader(Side side, const ILeader& iLeader) { d_ob->setSupremeLeader(side, generic(iLeader)); }
      ILeader getSupremeLeader(Side side) const { return campLeader(d_ob->getSupremeLeader(side)); }
      ILeader getSupremeLeaderID(Side side) const { return getSupremeLeader(side); }
      static ConstICommandPosition getTopParent(const ConstICommandPosition& cpi) { return OrderBattle::getTopParent(generic(cpi))->campaignInfo(); }
      static ICommandPosition getTopParent(const ICommandPosition& cpi)
      {
         RefGenericCP gcp = generic(cpi);
         return OrderBattle::getTopParent(gcp)->campaignInfo();
      }

      static const char* getUnitName(const ConstICommandPosition& i, char* buffer = 0) { return OrderBattle::getUnitName(generic(i), buffer); }

      // Strength Points

      ISP createStrengthPoint() { return d_ob->createStrengthPoint(); }
      Boolean detachStrengthPoint(const ICommandPosition& cpi, const ISP& spi) { return d_ob->detachStrengthPoint(generic(cpi), spi); }
      void deleteStrengthPoint(const ISP& isp) { d_ob->deleteStrengthPoint(isp); }
      CAMPDATA_DLL void attachStrengthPoint(const ICommandPosition& cpi, const ISP& spi);
      CAMPDATA_DLL void transferStrengthPoint(const ICommandPosition& src, const ICommandPosition& dest, const ISP& spi);


      // Attaching and Detaching units


      CAMPDATA_DLL ICommandPosition createChildCP(const ICommandPosition& parent);
         // Create a new CP as a child of another one

      void attachLeader(const ICommandPosition& cpi, const ILeader& gi)
      {
        d_ob->attachLeader(generic(cpi), generic(gi));
      }

      void detachLeader(const ILeader& iLeader) { d_ob->detachLeader(generic(iLeader)); }

      void removeLeader(const ILeader& iLeader) { d_ob->deleteLeader(generic(iLeader)); }
      void deleteLeader(const ILeader& iLeader) { d_ob->deleteLeader(generic(iLeader)); }

      void autoPromote(const ICommandPosition& cpi) { d_ob->autoPromote(generic(cpi)); }
      void autoPromote(const ICommandPosition& cpi, Side side) { autoPromote(cpi); }
      CAMPDATA_DLL void detachCommand(const ICommandPosition& i);
      Boolean canLeaderTakeControl(const ILeader& iLeader, const ICommandPosition& cpi) { return d_ob->canLeaderTakeControl(generic(iLeader), generic(cpi)); }

      void killAndReplaceLeader(const ILeader& iLeader) { d_ob->killAndReplaceLeader(generic(iLeader)); }
         // Delete Leader.  If he was attached to a unit, then automatically
         // replace him using autoPromote.

      /*
       * Changeable Nations data
       */

      NationsList& getNations() { return d_ob->getNations(); }
      const NationsList& getNations() const { return d_ob->getNations(); }
      Side getNationAllegiance(Nationality n) const { return d_ob->getNationAllegiance(n); }
      Boolean isNationActive(Nationality n) const { return d_ob->isNationActive(n); }
      Boolean shouldNationEnter(Nationality n) { return d_ob->shouldNationEnter(n); }
      Attribute getNationMorale(Nationality n) const { return d_ob->getNationMorale(n); }
      Boolean shouldReactivateNation(Nationality n, TimeTick t) { return d_ob->shouldReactivateNation(n, t); }

      void setNationAllegiance(Nationality n, Side s) { d_ob->setNationAllegiance(n, s); }
      void nationNeverEnters(Nationality n) { d_ob->nationNeverEnters(n); }
      void setNationMorale(Nationality n, Attribute m) { d_ob->setNationMorale(n, m); }
      void ractivateNationIn(Nationality n, TimeTick t) { d_ob->reactivateNationIn(n, t); }

      CAMPDATA_DLL void removeUnitAndChildren(const ICommandPosition& cpi);
      ILeader createLeader(Side side, Nationality n, RankEnum defRank, Boolean noModify = False)
      {
        return campLeader(d_ob->createLeader(side, n, defRank, noModify));
      }

      ConstILeader whoShouldCommand(const ConstILeader& leader1, const ConstILeader& leader2) const
      {
        return campLeader(d_ob->whoShouldCommand(generic(leader1), generic(leader2)));
      }

      static Boolean isRealUnit(const ConstICommandPosition& cpi) { return cpi->isRealUnit(); }

      CAMPDATA_DLL static Boolean isUnitOrderable(const ConstICommandPosition& cpi);
#if !defined(EDITOR)
      Boolean isPlayerUnit(const ConstICommandPosition& cpi) const { return d_ob->isPlayerUnit(generic(cpi)); }
#endif

#ifdef DEBUG
      CAMPDATA_DLL void create(UBYTE n);     // Create army with n sides
#endif
#if defined(CUSTOMIZE)
      CAMPDATA_DLL void removeAllUnits();    // Must delete campaign data as well as generic
#endif


      Boolean canNationControlNation(Nationality nControl, Nationality nSubordinate) const { return d_ob->canNationControlNation(nControl, nSubordinate); }
      Boolean canNationControlThisUnit(ConstParamCP cpi, Nationality n) { return d_ob->canNationControlThisUnit(generic(cpi), n); }

      // Algorithms
      // (Note many of these really ought to be in higher level utility classes)

      void makeUnitName(const ICommandPosition& cpi, Rank r) { d_ob->makeUnitName(generic(cpi), r); }
         // Make up unit name using nation->rankCount
         // Note that the unit's side must have been set first!


      Rank getRank(const ConstICommandPosition& cpi) const { return d_ob->getRank(generic(cpi)); }
      SPCount getUnitSPCount(const ConstICommandPosition& cpi, Boolean all) const { return d_ob->getUnitSPCount(generic(cpi), all); }
      SPCount getUnitSPValue(const ConstICommandPosition& cpi) const { return d_ob->getUnitSPValue(generic(cpi)); }
      SPCount getUnitSPValue(const ConstICommandPosition& cpi, BasicUnitType::value type) const { return d_ob->getUnitSPValue(generic(cpi), type); }
      SPCount getNType(const ConstICommandPosition& cpi, BasicUnitType::value type, Boolean except) const { return d_ob->getNType(generic(cpi), type, except); }
      BombValue getBombardmentValue(const ICommandPosition& cpi) const { return d_ob->getBombardmentValue(generic(cpi)); }
      UWORD getCommandSubordination(const ConstICommandPosition& cpi) const { return d_ob->getCommandSubordination(generic(cpi)); }
      UWORD getCommandSubordination(const ConstILeader& leader) const { return d_ob->getCommandSubordination(generic(leader)); }
      // Boolean findSupplyUnit(const ICommandPosition& cpi, ICommandPosition& supplyCP, ISP& supplySP) const { return d_ob->findSupplyUnit(cpi, supplyCP, supplySP); }
      // Boolean findEngineer(const ICommandPosition& cpi, ICommandPosition& engCP, ISP& engSP) const { return d_ob->findEngineer(cpi, engCP, engSP); }

      Boolean findSpecialType(const ICommandPosition& cpi, ICommandPosition& typeCP, ISP& typeSP, SpecialUnitType::value type) const
      {
         RefGenericCP cp;
         ISP sp;
         Boolean val = d_ob->findSpecialType(generic(cpi), cp, sp, type);
         typeCP = campCP(cp);
         typeSP = sp;
         return val;
      }

      // we no longer have supply SP's
      Boolean findSupplyUnit(const ICommandPosition& cpi, ICommandPosition& supplyCP, ISP& supplySP) const
      {
         return False;
//       return findSpecialType(cpi, supplyCP, supplySP, SpecialUnitType::Supply);
      }

      Boolean findEngineer(const ICommandPosition& cpi, ICommandPosition& engCP, ISP& engSP) const
      {
         return findSpecialType(cpi, engCP, engSP, SpecialUnitType::Engineer);
      }

      Boolean findBridgeTrain(const ICommandPosition& cpi, ICommandPosition& btCP, ISP& btSP) const
      {
         return findSpecialType(cpi, btCP, btSP, SpecialUnitType::BridgeTrain);
      }

      Boolean findSiegeTrain(const ICommandPosition& cpi, ICommandPosition& stCP, ISP& stSP) const
      {
         return findSpecialType(cpi, stCP, stSP, SpecialUnitType::SiegeTrain);
      }

      CAMPDATA_DLL void setCommandSpecialistFlags(const ICommandPosition& cpi);

      CAMPDATA_DLL Distance getColumnLength(ConstParamCP cpi) const;
      CAMPDATA_DLL void applyFatigue(const ICommandPosition& cpi, int recover);
         // Process Fatigue loss\recovery

      CAMPDATA_DLL void eliminateNationUnits(Nationality n, Side whichSide);
         // wipe out all units belonging to minor or generic nation


      Attribute getBaseMorale(ConstParamCP cpi) const { return d_ob->getBaseMorale(generic(cpi)); }

      CAMPDATA_DLL void averageCommandMorale(const ICommandPosition& cpi);
         // Calculates average morale of an organization

      CAMPDATA_DLL void setCommandSupply(const ICommandPosition& cpi, Attribute level);
         // set supply level for unit and all its subordinates

      CAMPDATA_DLL Boolean doDiggingFieldWorks(const ICommandPosition& cpi);
         // Process CommandPosition doing fieldworks

      CAMPDATA_DLL SPCount getNumClassSP(ConstParamCP cpi, AttritionClass::value c) const;
         // return number of SPs of given attritionclass in organization

      CAMPDATA_DLL void setCPNeedsDeleting(const ICommandPosition& cpi);
         // delete Unit (removeUnitAndChildren)

      CAMPDATA_DLL SPCount getNumLightCavalry(ConstParamCP cpi) const;
         // returns number of light cavlary for an organization

      CAMPDATA_DLL Boolean isForceMounted(ConstParamCP cpi) const;
         // returns true if unit is composed entirely of mounted troops

      CAMPDATA_DLL UWORD getForceSpeedModifer(ConstParamCP cpi) const;
         // returns lowest modifier to speed in force.
         // Actual modifier is function/100

      /*
       * Garbage collection: clear out any unreferenced objects
       */

      CAMPDATA_DLL void cleanup();


   private:
      void setChild(const ICommandPosition& cp, const ICommandPosition& child);
      void setSister(const ICommandPosition& cp, const ICommandPosition& child);
      void setParent(const ICommandPosition& cp, const ICommandPosition& child);

      void assignLeader(const ICommandPosition& cpi, const ILeader& iLeader) { d_ob->attachLeader(generic(cpi), generic(iLeader)); }

      ICommandPosition createCommand();
         // Create a new CommandPosition

      void addAsChild(const ICommandPosition& uDest, const ICommandPosition& uFrom);   // { d_ob->addAsChild(uDest, uFrom); }
         // Link Unit to another one
      void unlinkUnit(const ICommandPosition& unit);
         // Detach Unit from organization leaving in Limbo
      void removeUnit(const ICommandPosition& cpi);
         // Detach and Kill unit, including leader and SPs.


      void setUnitSpecialistFlags(const ICommandPosition& cpi);
         // is Unit all Specialist? i.e. all cavalry, artillery, etc?

      void setBaseMorale(const ICommandPosition& cpi);
        // sets initial base morale for a unit

      void initStartingValues(Boolean savedGame);
        // inits anything that needs it at the beginning of a game

      void initStartingMorale(Boolean savedGame);
        //  inits base morale for all units

      Attribute getAverageMorale(ConstParamCP cpi) const;
        // returns a units average morale

      void initStartingFatigue();
        // inits starting fatigue level for saved games

      void applyFatigueToUnit(const ICommandPosition& cpi, int recover);
         // Process Fatigue loss\recovery to unit

      void averageCommandFatigue(const ICommandPosition& cpi);
         // Calculate average fatigue for every unit in a command

      Attribute averageFatigue(const ICommandPosition& cpi) const;
         // Calculate average fatigue for a unit and subordinates

public:

	  CAMPDATA_DLL unsigned short calculateChecksum(void);


   private:
      OrderBattle*           d_ob;                 // Generic Order of Battle
      CommandPositionList    d_cpList;             // Campaign CommandPosition Information
      IndependentLeaderList  d_independentLeaders; // list of independent leaders
      CapturedLeaderList     d_capturedLeaders;    // list of captured leaders
        RepoSPList             d_repoList;

      static const UWORD fileVersion;
};

/*=====================================================================
 * Iterates through sides
 * Just a wrapper for OBSideIterso that it can be passed an army
 */

class NationIter : public OBSideIter
{
   public:
      NationIter(const Armies* army) :
         OBSideIter(army->ob())
      {
         ASSERT(army != 0);
      }
};

/*=====================================================================
 * Iterates through units in an army
 */

class UnitIterValidator
{
    public:
        virtual bool isValid(const ConstICommandPosition& cp) = 0;
};

class DefaultUnitIterValidator : public UnitIterValidator
{
    public:
        DefaultUnitIterValidator(bool all) : d_all(all) { }
        CAMPDATA_DLL virtual bool isValid(const ConstICommandPosition& cp);

    private:
        bool d_all;
};

class CAMPDATA_DLL UnitIter : private DefaultUnitIterValidator
{
    public:
        UnitIter(Armies* a, Boolean all = False) :
            d_army(a),
            d_iter(a->ob()),
            DefaultUnitIterValidator(all)
        {
            ASSERT(d_army != 0);
        }

        UnitIter(Armies* a, const ICommandPosition& startUnit, Boolean all = False) :
            d_army(a),
            d_iter(a->ob(), generic(startUnit)),
            DefaultUnitIterValidator(all)
        {
            ASSERT(d_army != 0);
        }

        void reset(const ICommandPosition& startUnit) { d_iter.reset(generic(startUnit)); }
        // void resetSide(Side s) { d_iter.resetSide(s); }
        void reset() { d_iter.reset(); }

        ICommandPosition current() const { return d_iter.current()->campaignInfo(); }

#ifdef BACKWARD_COMPATIBLE
        ConstICommandPosition currentCommand() const
        {
            // return d_army->getCommand(d_iter.current());

            return current(); //d_army->getCommand(current());
        }

        ICommandPosition currentCommand()
        {
            // return d_army->getCommand(d_iter.current());

            // return &d_iter.current();
            return current(); // d_army->getCommand(current());
        }
#endif

       Boolean sister();
       Boolean next();        // Goes through all units (from top to bottom)

    private:
        Armies* d_army;
        G_UnitIter  d_iter;        // Generic Iterator
};

class ConstUnitIter : private DefaultUnitIterValidator {
    public:
        ConstUnitIter(Armies* a, Boolean all = False) :
            d_army(a),
            d_iter(a->ob()),
            DefaultUnitIterValidator(all)
        {
            ASSERT(d_army != 0);
        }

        ConstUnitIter(const Armies* a, const ConstICommandPosition& startUnit, Boolean all = False) :
            d_army(a),
            d_iter(a->ob(), generic(startUnit)),
            DefaultUnitIterValidator(all)
        {
            ASSERT(d_army != 0);
        }

        void reset(const ConstICommandPosition& startUnit) { d_iter.reset(generic(startUnit)); }
        // void resetSide(Side s) { d_iter.resetSide(s); }
        void reset() { d_iter.reset(); }

        ConstICommandPosition current() const { return d_iter.current()->campaignInfo(); }

#ifdef BACKWARD_COMPATIBLE
        ConstICommandPosition currentCommand() const
        {
            // return d_army->getCommand(current());
            return current();
        }
#endif

        CAMPDATA_DLL bool sister();      // step through sister
        CAMPDATA_DLL bool next(bool intoChildren = true);        // Goes through all units (from top to bottom)

    private:
        const Armies* d_army;
        G_ConstUnitIter   d_iter;        // Generic Iterator
};


/*=====================================================================
 * Combination of Nation and Unit Iterators
 * Goes through every unit in every side.
 */

class CPIter : private DefaultUnitIterValidator
{
      CPIter(const CPIter&);
      const CPIter& operator =(const CPIter&);
   public:
      CPIter(Armies* a, Boolean all = False) :
         d_iter(a->ob()),
         d_army(a),
         DefaultUnitIterValidator(all)
      {
         ASSERT(d_army != 0);
      }

      ICommandPosition current() const
      {
         return d_iter.current()->campaignInfo();
      }

#ifdef BACKWARD_COMPATIBLE
      ICommandPosition currentCommand()
      {
         // return d_army->getCommand(d_iter.current());
         return current();
      }

      ConstICommandPosition currentCommand() const
      {
         // return d_army->getCommand(d_iter.current());
         return current();
      }
#endif

      CAMPDATA_DLL Boolean sister();
      CAMPDATA_DLL Boolean next();

      void reset()
      {
         d_iter.reset();
      }

   private:
      G_CPIter d_iter;
      Armies* d_army;
};

class ConstCPIter : private DefaultUnitIterValidator
{
      ConstCPIter(const CPIter&);
      const ConstCPIter& operator =(const CPIter&);
   public:
      ConstCPIter(const Armies* a, Boolean all = False) :
         d_iter(a->ob()),
         d_army(a),
         DefaultUnitIterValidator(all)
      {
         ASSERT(d_army != 0);
      }

      ConstICommandPosition current() const
      {
         return d_iter.current()->campaignInfo();
      }

      ConstICommandPosition currentCommand() const
      {
         return current(); // d_army->getCommand(d_iter.current());
      }

      CAMPDATA_DLL Boolean sister();
      CAMPDATA_DLL Boolean next();

      void reset()
      {
         d_iter.reset();
      }

   private:
      G_ConstCPIter  d_iter;
      const Armies* d_army;
};



class StrengthPointIter : public G_StrengthPointIter
{
   public:
      StrengthPointIter(Armies* a) :
         // d_army(a),
         G_StrengthPointIter(a->ob())
      {
      }

      // StrengthPointIter(const Armies* a, const ICommandPosition cp) :
      StrengthPointIter(Armies* a, const ICommandPosition& cp) :
         // d_army(a),
         G_StrengthPointIter(a->ob(), cp->getSPEntry())  // spList)
      {
      }

      ISP currentISP() const { return current(); }
};

class ConstStrengthPointIter : public G_ConstStrengthPointIter
{
   public:
      ConstStrengthPointIter(const Armies* a) :
         // d_army(a),
         G_ConstStrengthPointIter(a->ob())
      {
      }

      ConstStrengthPointIter(const Armies* a, const ConstICommandPosition& cp) :
         // d_army(a),
         G_ConstStrengthPointIter(a->ob(), cp->getSPEntry())   // spList)
      {
      }

      ConstISP currentISP() const { return current(); }

   // private:
      // const Armies* d_army;
};



/*=====================================================================
 * Goes through all StrengthPoints in an organization
 */

class SPIter : public G_SPIter
{
public:
   SPIter(Armies* a, const ICommandPosition& startUnit) :
      G_SPIter(a->ob(), generic(startUnit))
   {
   }

   ICommandPosition currentICP() const { return G_SPIter::currentICP()->campaignInfo(); }

};

class ConstSPIter : public G_ConstSPIter
{
public:
   ConstSPIter(const Armies* a, const ConstICommandPosition& startUnit) :
      G_ConstSPIter(a->ob(), generic(startUnit))
   {
   }

   ConstICommandPosition currentICP() const { return G_ConstSPIter::currentICP()->campaignInfo(); }

};

class ArmyReadLocker
{
   public:
      ArmyReadLocker(const Armies& armies) : d_army(&armies) { d_army->startRead(); }
      ~ArmyReadLocker() { d_army->endRead(); }
   private:
      const Armies* d_army;
};

class ArmyWriteLocker
{
   public:
      ArmyWriteLocker(Armies& armies) : d_army(&armies) { d_army->startWrite(); }
      ~ArmyWriteLocker() { d_army->endWrite(); }
   private:
      Armies* d_army;
};

/*
 * Useful functions for converting to and from LPARAM
 * so can be used in window controls
 */


inline LPARAM leaderToLPARAM(const ConstILeader& leader)
{
   return reinterpret_cast<LPARAM>(static_cast<const Leader*>(leader));
}

inline ILeader lparamToLeader(LPARAM l)
{
   return reinterpret_cast<Leader*>(l);
}

inline LPARAM cpToLPARAM(ConstParamCP cp)
{
   return reinterpret_cast<LPARAM>(static_cast<const CommandPosition*>(cp));
}

inline ICommandPosition lparamToCP(LPARAM l)
{
   return reinterpret_cast<CommandPosition*>(l);
}

inline LPARAM spToLPARAM(const ISP& sp)
{
   return reinterpret_cast<LPARAM>(static_cast<StrengthPoint*>(sp));
}

inline ISP lparamToSP(LPARAM l)
{
   return reinterpret_cast<StrengthPoint*>(l);
}


#endif /* ARMIES_HPP */

