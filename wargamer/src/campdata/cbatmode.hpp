/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef CBATMODE_HPP
#define CBATMODE_HPP

#ifndef __cplusplus
#error cbatmode.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 * Campaign Battle: Definitions Used outside of campdata
 *
 *----------------------------------------------------------------------
 */

struct BattleMode {
    enum Mode {
        Setup,				// Battle is still being set up
        Calculated,
        Tactical,
        Finished,
        NoValue = -1
    };
};

struct EndOfDayMode {
    enum Mode {
        Withdraw,
        Continue
    };
};



#endif /* CBATMODE_HPP */

