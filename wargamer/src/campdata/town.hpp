/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef TOWN_H
#define TOWN_H

#ifndef __cplusplus
#error town.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1995, Steven Morle-Green, All Rights Reserved
 *----------------------------------------------------------------------
 *
 *	Data Structures and Class for Definition of Campaign Locations
 *
 * Locations are nodes in the transport network, representing a
 * town, railway connection, port, etc.
 *
 * Locations will be stored in an array, so that they can be
 * referenced with 16 bits indeces instead of 32 bit pointers.
 *
 *----------------------------------------------------------------------
 *
 * IMPORTANT
 *   When updating these structures and classes.  Always remember to
 *   update the file readers/writers in wldfile.cpp
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.2  2001/07/02 15:23:17  greenius
 * Battle, Campaign, Combined game and Campaign Editor now compile and run... mostly
 *
 * Revision 1.1  2001/06/13 08:52:37  greenius
 * Initial Import
 *
 *
 *----------------------------------------------------------------------
 */

#include "cdatadll.h"
#include "mytypes.h"
#include "gameob.hpp"
#include "array.hpp"
#include "gamedefs.hpp"
#include "cpdef.hpp"
#include "wg_rand.hpp"
#include "townbld.hpp"
#include "sync.hpp"
// #include "townordr.hpp"


class DrawDIB;
class MapWindow;

class FileReader;
class FileWriter;

/*
 * Types of connection
 * Defined as enumerations, but if we later decide to use bitfields
 * then we'd better define them as UBYTE instead.
 */

enum ConnectType {
	CT_Road,
	CT_Rail,
	CT_River,
	CT_Max
};

enum ConnectQuality {
	CQ_Poor,
	CQ_Average,
	CQ_Good,
	CQ_Max
};

class WhichSideChokePoint {
public:
  enum Type {
	 NoChokePoint,
	 ThisSide,
	 ThatSide,

	 HowMany
  };

#ifdef DEBUG
  static const char* name(Type t);
#endif
};

typedef UBYTE ConnectCapacity;
#define MaxConnectCapacity UBYTE_MAX

class SupplyLevel {
	UWORD value;
public:
	enum {
		MaxLevel = UWORD_MAX
	};

	SupplyLevel() :
	  value(0) { }			// Unitialised constructor
	SupplyLevel(const SupplyLevel& l) { value = l.value; }
	SupplyLevel(int v) { ASSERT(v <= MaxLevel); value = UWORD(v); }

	operator int() const { return value; }
	SupplyLevel& operator += (const SupplyLevel& sup)
	{
		if( (value >= (MaxLevel - sup.value)))
			value = MaxLevel;
		else
			value += sup.value;
		return *this;
	}

	SupplyLevel& operator -= (const SupplyLevel& sup)
	{
		if(value <= sup.value)
			value = 0;
		else
			value -= sup.value;
		return *this;
	}

	Boolean readData(FileReader& f);
	Boolean writeData(FileWriter& f) const;
};

/*
 * Structure Defining a connection
 */

struct Connection {
	static const UWORD fileVersion;

	ITown node1;				              // Where the connection goes between
	ITown node2;
	ConnectType how;					        // What type of connection (road/rail/river)
	ConnectQuality quality;			        // Poor, Average, Good
	ConnectCapacity capacity;		        // How many units can use it in a given time
	WhichSideChokePoint::Type whichSide1; // If terminating at a chokepoint, which side are we on
	WhichSideChokePoint::Type whichSide2; // If terminating at a chokepoint, which side are we on
	Boolean offScreen;                    // Is an off screen connection?
	Distance distance;

	/*
	 * File Interface
	 */

	Connection() :
	  node1(NoTown),
	  node2(NoTown),
	  how(CT_Road),
	  quality(CQ_Average),
	  capacity(0),
	  whichSide1(WhichSideChokePoint::NoChokePoint),
	  whichSide2(WhichSideChokePoint::NoChokePoint),
	  offScreen(False),
	  distance(0) {}

	CAMPDATA_DLL Boolean readData(FileReader& f);
	CAMPDATA_DLL Boolean writeData(FileWriter& f) const;

#if defined(CUSTOMIZE)
	CAMPDATA_DLL void setWhichSideChokePoint(ITown node, WhichSideChokePoint::Type whichSide);
#endif

	CAMPDATA_DLL ITown getOtherTown(ITown t1) const;
	CAMPDATA_DLL WhichSideChokePoint::Type whichSideChokePoint(ITown node) const;
};


/*
 * Maximum number of connections from a town to another
 */

#define MaxConnections 16

/*
 * Values used for towns
 */

enum TownSize {
	TOWN_Capital,
	TOWN_City,
	TOWN_Town,
	TOWN_Other,

	TownSize_MAX,
};

/*
 * Alignment of name against dot
 */

enum NameAlign {
	NA_TOPLEFT,
	NA_TOP,
	NA_TOPRIGHT,
	NA_RIGHT,
	NA_BOTTOMRIGHT,
	NA_BOTTOM,
	NA_BOTTOMLEFT,
	NA_LEFT,

	NA_MAX
};

/*
 * Class defining a Location Node
 * It is friendlier to call them Towns
 *
 */

class Town :
//	public NamedItem,
	public SideObject,
	public LocationObject
{
	friend class TownList;

	static const UWORD fileVersion;
   static StringFileTable* s_nameTable;
   static const char s_nameFile[];

public:
	enum { NumberSides = 2, MaxFortifications = 4 };
	typedef UBYTE FortLevel;

private:
	IConnection d_connections[MaxConnections];		// Where is it connected to?
	IProvince d_province;			// What State/Province/Area is it in?

   StringFileTable::ID d_nameID;

	NameAlign d_nameAlign;			// How is the name displayed?
	SBYTE d_nameX;					// Pixel adjustment from above
	SBYTE d_nameY;

	/*
	 * Constant Values
	 */

	TownSize  d_size;				// What size of town is it?
	Attribute d_victory[NumberSides];		// Victory points for each side
	Attribute d_political;			// Political value
	Attribute d_manPowerRate;		// Manpower generation
	Attribute d_resourceRate;		// Resource generation
	Attribute d_supplyRate;		// Supply Rate Value if it is a supply source
	Attribute d_forageBase;		// Base forage value. Weekly Repenishment is a % of this value
	Attribute d_forageStock;   // Current forage in Stock. Stockpile can never exceed base
	Attribute d_strength;			// size needed to take over

	UBYTE d_stacking;				// Maximum SPs allowed in this location
	TerrainType d_terrain;			// What type of terrain is here

//	Town_COrderList d_orders;      // town orders

	/*
	 * Flags
	 */

	enum {
		mpTransferable = 0x01,	   // Man power is tranferable when taken over
		isSupplySource = 0x02,	   // Point is a supply source
		isDepot			= 0x04,	   // Point is a supply depot
		// isCapital		= 0x08,	// Is the province's capital city
		siegeable		= 0x10,	   // Location can be sieged
		Offscreen      = 0x20,     // Is this an offscreen town?
		BuildingFort   = 0x40,     // are we building a fort?
		BuildingDepot  = 0x80      // are we building a depot?
	};
	UBYTE d_flags;

	/*
	 * Variable Attributes
	 */

	FortLevel d_fortifications;	// Fortification Level
	SupplyLevel d_supplyLevel;	// Current Supply Level

	ICommandPosition d_fortUpgrade;		// Who is upgrading our fortifications?
	ICommandPosition d_supplyInstall;	// Who is installing suply depot?

	/*
	 * Current state flags
	 *		Add things here such as:
	 *			List of units at this location
	 *			Current stacking level
	 *			Supplied
	 *			Sieged (and list of units)
	 *
	 * Siege/Garrison/Occupied flags are updated during each logic loop
	 * rather than try to maintain complicated lists of units.
	 */

	enum {
		Garrison               = 0x01,   // Is there a garrison here?
		Siege                  = 0x02,   // Is there a siege here?
		Raid                   = 0x04,   // Town is being raided
		Occupied               = 0x08,   // Are there units here?
		Breached               = 0x10,   // A breach has occurred during siege
		DevastatingBombardment = 0x20,   // A devastating bombardment occured during breach
		Seen                   = 0x40,   // Does enemy know whats here?
		BridgeUp               = 0x80    // Is the bridge still standing?
													// Note: Always True if not a river chokepoint
	};
	UBYTE d_stateFlags;

	UWORD d_weeksUnderSiege;
	TimeTick d_catastrophe;  // Has a catastrophe recently occured ?

	HelpID d_helpID;          // Help ID, if any

public:
	CAMPDATA_DLL Town();
	~Town() { }

	void setSize(TownSize sz) { d_size = sz; }
	TownSize getSize() const { return d_size; }

	void setNameAlign(NameAlign a) { d_nameAlign = a; }
	NameAlign getNameAlign() const { return d_nameAlign; }

	SBYTE getNameX() const { return d_nameX; }
	void setNameX(SBYTE x) { d_nameX = x; }
	SBYTE getNameY() const { return d_nameY; }
	void setNameY(SBYTE y) { d_nameY = y; }

	CAMPDATA_DLL void setName(char* s);
	CAMPDATA_DLL const char* getName() const;
	const char* getNameNotNull(const char* def = 0) const { return getName(); }

	void setProvince(IProvince p) { d_province = p; }
	IProvince getProvince() const { return d_province; }

	void setConnection(int r, IConnection i) { d_connections[r] = i; }
	IConnection getConnection(int r) const { return d_connections[r]; }
	const IConnection* getConnections() const { return d_connections; }

	Attribute getVictory(int i) const { return d_victory[i]; }
	void setVictory(int i, Attribute v) { d_victory[i] = v; }
	void setVictory(Attribute v) { d_victory[0] = d_victory[1] = v; }

	Attribute getPolitical() const { return d_political; }
	void setPolitical(Attribute p) { d_political = p; }

	Attribute getManPowerRate() const { return d_manPowerRate; }
	void setManPowerRate(Attribute r) { d_manPowerRate = r; }
	// temp function for generating random mp-rate
	void setManPowerRate() { d_manPowerRate = (Attribute)CRandom::get(0, 255);}

	Attribute getResourceRate() const { return d_resourceRate; }
	void setResourceRate(Attribute p) { d_resourceRate = p; }
	// temp function for generating random mp-rate
	void setResourceRate() { d_resourceRate = (Attribute)CRandom::get(0, 255); }

	Attribute getSupplyRate() const { return d_supplyRate; }
	void setSupplyRate(Attribute p) { d_supplyRate = p; }

	Attribute getForageBase() const { return d_forageBase; }
	void setForageBase(Attribute p) { d_forageBase = p; }

	Attribute getForageStock() const { return d_forageStock; }
	void setForageStock(Attribute p) { d_forageStock = p; }

	// for backward compatiblity
	Attribute getForage() const { return d_forageStock; }
//	void setForageStock(Attribute p) { d_forageStock = p; }

	const SupplyLevel& getSupplyLevel() const { return d_supplyLevel; }
	void setSupplyLevel(const SupplyLevel& l) { d_supplyLevel = l; }
	void addSupplyLevel(const SupplyLevel& sup) { d_supplyLevel += sup; }

	CAMPDATA_DLL Boolean getSupplyRange(Distance& fullRange, Distance& maxRange) const;
	CAMPDATA_DLL static Distance getSupplyMaxRange(TownSize ts);

	UBYTE getStacking() const { return d_stacking; }
	void setStacking(UBYTE p) { d_stacking = p; }

	UBYTE getTerrain() const { return d_terrain; }
	void setTerrain(UBYTE t) { d_terrain = t; }

	FortLevel getFortifications() const { return d_fortifications; }
	void setFortifications(FortLevel p) { d_fortifications = p; }
	CAMPDATA_DLL Boolean upgradeFortifications();

	Attribute getStrength() const { return d_strength; }
	void setStrength(Attribute sp) { d_strength = sp; }

	HelpID getHelpID() const { return d_helpID; }
	void setHelpID(HelpID id) { d_helpID = id; }

	Boolean canBuildDepot(Side s) const { return ((getSide() == s) && !(d_flags & isDepot)); }

//	void addOrder(Town_COrder* to) { d_orders.append(to); }
//	Town_COrderList& townOrders()  { return d_orders; }
//	const Town_COrderList& townOrders() const { return d_orders; }
	// SPCount getStrength(Side s) const;
	//--- Moved to CampaignData

   static void writeNameTable();
   static void destroyNameTable();

private:
	void setFlag(UBYTE& flags, UBYTE mask, Boolean f)
	{
	  if(f)
		 flags |= mask;
	  else
		 flags &= ~mask;
	}

public:
	void setGarrison(Boolean f) { setFlag(d_stateFlags, Garrison, f); }
	void setSiege(Boolean f) { setFlag(d_stateFlags, Siege, f); }
	void setRaid(Boolean f) { setFlag(d_stateFlags, Raid, f); }
	void setOccupied(Boolean f) { setFlag(d_stateFlags, Occupied, f); }
	void setBreached(Boolean f) { setFlag(d_stateFlags, Breached, f); }
	void setDevBomb(Boolean f) { setFlag(d_stateFlags, DevastatingBombardment, f); }
	void isSeen(Boolean f) { setFlag(d_stateFlags, Seen, f); }
	void isBridgeUp(Boolean f) { setFlag(d_stateFlags, BridgeUp, f); }
	Boolean isGarrisoned() const { return (d_stateFlags & Garrison) != 0; }
	Boolean isSieged() const { return (d_stateFlags & Siege) != 0; }
	Boolean isRaided() const { return (d_stateFlags & Raid) != 0; }
	Boolean isOccupied() const { return (d_stateFlags & Occupied) != 0; }
	Boolean isBreached() const { return (d_stateFlags & Breached) != 0; }
	Boolean isDevBomb() const { return (d_stateFlags & DevastatingBombardment) != 0; }
	Boolean isSeen() const { return (d_stateFlags & Seen) != 0; }
	Boolean isBridgeUp() const { return (d_stateFlags & BridgeUp) != 0; }

	void resetWeeksUnderSiege() { d_weeksUnderSiege = 0; }
	UWORD getWeeksUnderSiege() const { return d_weeksUnderSiege; }
	void incrementWeeksUnderSiege() { d_weeksUnderSiege++; }
	Boolean surrenderMinimumReached() const
	{
	  return ( (d_weeksUnderSiege > 0) && (d_weeksUnderSiege >= (d_fortifications * 2)) );
	}
	Boolean halfSurrenderMinimumReached() const
	{
	  return ( (d_weeksUnderSiege > 0) && (d_weeksUnderSiege >= d_fortifications) );
	}

	ICommandPosition getFortUpgrader() const { return d_fortUpgrade; }
	CAMPDATA_DLL void setFortUpgrader(ICommandPosition cpi);
	Boolean hasFortUpgrader() const { return Boolean(d_fortUpgrade != NoCommandPosition); }

	ICommandPosition getSupplyInstaller() const { return d_supplyInstall; }
	CAMPDATA_DLL void setSupplyInstaller(ICommandPosition cpi);
	Boolean hasSupplyInstaller() const { return Boolean(d_supplyInstall != NoCommandPosition); }


	void setCatastrophe(TimeTick howLong) { d_catastrophe = howLong; }
	Boolean hasCatastrophe(TimeTick theTime) const { return (theTime < d_catastrophe); }

private:
	Boolean getFlag(UBYTE mask) const { return (d_flags & mask) != 0; }

public:
	void setMpTransferable(Boolean f) { setFlag(d_flags, mpTransferable, f); }
	void setIsSupplySource(Boolean f) { setFlag(d_flags, isSupplySource, f); }
	void setIsDepot(Boolean f) 		 { setFlag(d_flags, isDepot, 			f); }
	void setSiegeable(Boolean f) 		 { setFlag(d_flags, siegeable, 		f); }
	void setOffScreen(Boolean f)      { setFlag(d_flags, Offscreen, f); }
	void setisBuildingFort(Boolean f) { setFlag(d_flags, BuildingFort, f); }
	void setisBuildingDepot(Boolean f){ setFlag(d_flags, BuildingDepot, f); }

	Boolean getMpTransferable() const { return getFlag(mpTransferable); 	 }
	Boolean getIsSupplySource() const { return ( (getFlag(isSupplySource)) || (getIsDepot() && isSieged()) ); }
	Boolean getIsDepot() const 		 { return getFlag(isDepot); 			 }
	Boolean getSiegeable() const 		 { return getFlag(siegeable); 			 }
	Boolean isOffScreen() const       { return getFlag(Offscreen); }
	Boolean isBuildingFort()  const { return (d_flags & BuildingFort) != 0; }
	Boolean isBuildingDepot() const { return (d_flags & BuildingDepot) != 0; }

	Boolean getIsCapital() const 		 { return getSize() == TOWN_Capital;  }

	/*
	 * File Interface
	 */

	CAMPDATA_DLL Boolean readData(FileReader& f);
	CAMPDATA_DLL Boolean writeData(FileWriter& f) const;

	// void draw(const MapWindow* mapWindow, HDC hdc, DrawDIB* dib);


	CAMPDATA_DLL void adjustConnections(IConnection ci);

	CAMPDATA_DLL int addConnection(IConnection ci);

   private:
      static StringFileTable* getNameTable();

};

/*
 * Class Defining an Area, Province or State
 * I'm going to call them Province, because State and Area both
 * have alternative meanings.
 */

class Province :
//	public NamedItem,
	public SideObject,
	public StartSideObject,
	public LocationObject
	// public MapDisplayObject
{
	friend class ProvinceList;

	static const UWORD fileVersion;
   static StringFileTable* s_nameTable;
   static const char s_nameFile[];

//	NamedItem d_shortName;			// Abreviated Name for display on zoomed out map
   StringFileTable::ID d_shortNameID;
   StringFileTable::ID d_nameID;

	ITown d_capital;					// Capital City
	ITown d_town;						// Index into town list for towns in this state
	UWORD d_nTowns;					// Number of towns in this state
	AttributePoints d_resource;
	AttributePoints d_manPower;
	Nationality d_nationality;
	BuildItem d_builds[BasicUnitType::HowMany];

	enum {
	  OrderOnTheWay = 0x01,
	  OffScreen = 0x02
	};

	UBYTE d_flags;

	HelpID d_helpID;             // Help ID, if any
	NamedItem d_picName;

	/*
	 * Also need:
	 *		Manpower usage: high value reduces resource point production
	 *		Allocation of resource/manpower points to production
	 */

	void setFlag(UBYTE mask, Boolean f)
	{
	  if(f)
		 d_flags |= mask;
	  else
		 d_flags &= ~mask;
	}

public:
	CAMPDATA_DLL Province();
	~Province() { }

// 	void setShortName(char* s) { d_shortName.setName(s); }
// 	const char* getShortName() const { return d_shortName.getName(); }
 	CAMPDATA_DLL void setShortName(char* s);
 	CAMPDATA_DLL const char* getShortName() const;

	CAMPDATA_DLL void setName(char* s);
	CAMPDATA_DLL const char* getName() const;
	const char* getNameNotNull(const char* def = 0) const { return getName(); }

	ITown getCapital() const { return d_capital; }
	void setCapital(ITown i) { d_capital = i; }

	AttributePoints getResource() const { return d_resource; }
	void setResource(AttributePoints r) { d_resource = r; }
	void addResource(AttributePoints r) { d_resource = minimum<int>(AttributePoint_Range, d_resource + r); }
	void takeResource(AttributePoints r) { d_resource = maximum<int>(0, d_resource - r); }

	AttributePoints getManPower() const { return d_manPower; }
	void setManPower(AttributePoints m) { d_manPower = m; }
	void addManPower(AttributePoints r) { d_manPower = minimum<int>(AttributePoint_Range, d_manPower + r); }
	void takeManPower(AttributePoints r) { d_manPower = maximum<int>(0, d_manPower - r); }

	void setTown(ITown t) { d_town = t; }
	ITown getTown() const { return d_town; }
	void setNTowns(UWORD n) { d_nTowns = n; }
	UWORD getNTowns() const { return d_nTowns; }

	void setNationality(Nationality n) { d_nationality = n; }
	Nationality getNationality() const { return d_nationality; }
	Boolean isNationality(Nationality n) const { return (n == d_nationality); }

	const BuildItem* builds() const { return d_builds; }

	BuildItem& getBuild(int i)
	{
		ASSERT(i < BasicUnitType::HowMany);
		ASSERT(i >= 0);
		return d_builds[i];
	}


	const BuildItem& getBuild(int i)  const
	{
		ASSERT(i < BasicUnitType::HowMany);
		ASSERT(i >= 0);
		return d_builds[i];
	}

	CAMPDATA_DLL void cancelBuilds();


	void orderOnTheWay(Boolean f) { setFlag(OrderOnTheWay, f); }
	void setOffScreen(Boolean f) { setFlag(OffScreen, f); }
	Boolean orderOnTheWay() const { return (d_flags & OrderOnTheWay) != 0; }
	Boolean isOffScreen() const { return (d_flags & OffScreen) != 0; }

	void setHelpID(HelpID id) { d_helpID = id; }
	HelpID getHelpID() const { return d_helpID; }

	const char* picture() const { return d_picName.getName(); }
	void picture(char* name) { d_picName.setName(name); }

   static void writeNameTable();
   static void destroyNameTable();

	/*
	 * File Interface
	 */

	CAMPDATA_DLL Boolean readData(FileReader& f);
	CAMPDATA_DLL Boolean writeData(FileWriter& f) const;

	// void draw(const MapWindow* mapWindow, HDC hdc, DrawDIB* dib);

   private:
      static StringFileTable* getNameTable();
};

/*
 * Class representing a set of Towns
 * Using Array because it makes access with indeces very fast.
 * The disadvantage is that it is difficult to add and remove
 * entries.
 *
 * If this is a problem, then use DynamicArray
 */

class TownList :
	public Array<Town>,
	public RWLock
{
public:
   TownList();
   ~TownList();

	/*
	 * File Interface
	 */

	Boolean readData(FileReader& f);
	Boolean writeData(FileWriter& f) const;

	// void draw(const MapWindow* mapWindow, HDC hdc, DrawDIB* dib);
};

class TownIter : public CArrayIter<Town> {
	// typedef CArrayIter<Town> Super;

	const TownList* list;

public:
	CAMPDATA_DLL TownIter(const TownList& tl);
	CAMPDATA_DLL ~TownIter();

	// const Town& current() const { return Super::current(); }
};

class TownIterWrite : public ArrayIter<Town> {
	typedef ArrayIter<Town> Super;

	TownList* list;
public:
	CAMPDATA_DLL TownIterWrite(TownList& tl);
	CAMPDATA_DLL ~TownIterWrite();
};

class ProvinceList :
	public Array<Province>,
	public RWLock
{
//  	class NullProvinceGenerator
//  	{
//  		public:
//  			NullProvinceGenerator();
//  			~NullProvinceGenerator();
//
//  			Province& get() { return *instance(); }
//  			const Province& get() const { return *instance(); }
//  		private:
//          Province* instance() const;
//
//  			mutable Province* d_province;
//  	};

public:
   ProvinceList();
   ~ProvinceList();

	/*
	 * File Interface
	 */

	CAMPDATA_DLL Boolean readData(FileReader& f);
	CAMPDATA_DLL Boolean writeData(FileWriter& f) const;

	Province& get(IProvince i)
	{
		ASSERT(i != NoProvince);

		if(i != NoProvince)
			return Array<Province>::get(i);
		else
			return *nullProvince();  // s_nullProvince.get();
	}

	const Province& get(IProvince i) const
	{
		ASSERT(i != NoProvince);

		if(i != NoProvince)
			return Array<Province>::get(i);
		else
			return *nullProvince();  // s_nullProvince.get();
	}

	Province& operator[](IProvince i) { return get(i); }
	const Province& operator[](IProvince i) const { return get(i); }


private:
//	CAMPDATA_DLL static NullProvinceGenerator s_nullProvince;
   CAMPDATA_DLL Province* nullProvince() const;
   mutable Province* d_nullProvince;
};

class ProvinceIter : public CArrayIter<Province> {
	const ProvinceList* list;
public:
	CAMPDATA_DLL ProvinceIter(const ProvinceList& tl);
	CAMPDATA_DLL ~ProvinceIter();
};

class ProvinceIterWrite : public ArrayIter<Province> {
	ProvinceList* list;
public:
	ProvinceIterWrite(ProvinceList& tl);
	~ProvinceIterWrite();
};

class ConnectionList :
	public Array<Connection>,
	public RWLock
{
public:
	/*
	 * File Interface
	 */

	Boolean readData(FileReader& f);
	Boolean writeData(FileWriter& f) const;

	// void draw(const MapWindow* mapWindow, HDC hdc, DrawDIB* dib);
};

class ConnectionIter : public ArrayIter<Connection> {
	ConnectionList* list;
public:
	ConnectionIter(ConnectionList& tl);
	~ConnectionIter();
};

class ConnectionIterWrite : public ArrayIter<Connection> {
	ConnectionList* list;
public:
	ConnectionIterWrite(ConnectionList& tl);
	~ConnectionIterWrite();
};

#endif /* TOWN_H */

