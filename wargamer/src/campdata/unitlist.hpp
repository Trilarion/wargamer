/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef UNITLIST_H
#define UNITLIST_H

#ifndef __cplusplus
#error unitlist.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1995, Steven Morle-Green, All Rights Reserved
 *----------------------------------------------------------------------
 *
 *	List of units used for stacking
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.4  1996/06/22 19:06:39  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.3  1996/02/23 14:08:58  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.2  1996/02/22 10:29:01  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.1  1996/02/16 17:34:14  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#include "cdatadll.h"
#include "dynarray.hpp"
#include "gamedefs.hpp"
#include "cpdef.hpp"
// #include "armies.hpp"


struct UnitIdentify {
	ConstICommandPosition unit;
	Boolean added;

	UnitIdentify() { }
	UnitIdentify(ConstParamCP i) { unit = i; added = True; }
};

struct GeneralIdentify {
	// Side side;
	ConstILeader general;
	Boolean added;

	GeneralIdentify() { }
	GeneralIdentify(const ConstILeader& g) { general = g; added = True; }
};

/*
 * Typedefs to make things nicer
 */

typedef UWORD UnitListID;
#define NoUnitListID UWORD_MAX

/*
 * List of units and generals being tracked
 */

class StackedUnitList {
	ResizeArray<UnitIdentify> units;
	ResizeArray<GeneralIdentify> generals;

	Boolean changed;

	static const int BlockSize;		// How many to allocate at a time

public:
	CAMPDATA_DLL StackedUnitList();
	CAMPDATA_DLL ~StackedUnitList();

	CAMPDATA_DLL void reset();

	CAMPDATA_DLL void addUnit(ConstParamCP i);
	CAMPDATA_DLL void addGeneral(const ConstILeader& g);

	int unitCount() const { return units.getCount(); }
	int generalCount() const { return generals.getCount(); }

	// const UnitIdentify* getUnit(UnitListID i) const;
	CAMPDATA_DLL ConstICommandPosition getUnit(UnitListID i) const;

    CAMPDATA_DLL void removeUnit(UnitListID i);

	CAMPDATA_DLL ConstILeader getGeneral(UnitListID i) const;

	CAMPDATA_DLL void clearFlags();
	CAMPDATA_DLL Boolean checkFlags();
};

#endif /* UNITLIST_H */

