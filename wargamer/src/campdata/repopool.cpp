/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
#include "stdinc.hpp"
#include "repopool.hpp"
#include "fsalloc.hpp"
#include "c_obutil.hpp"
#include "filebase.hpp"    // FileReader and FileWriter
#include "filecnk.hpp"

/*---------------------------------------------------------------
 *   Independent leaders and lists
 */

#ifdef DEBUG
static FixedSizeAlloc<RepoSP> s_repoAlloc("RepoSP");
#else
static FixedSizeAlloc<RepoSP> s_repoAlloc;
#endif  // DEBUG

void* RepoSP::operator new(size_t size)
{
   return s_repoAlloc.alloc(size);
}

#ifdef _MSC_VER
void RepoSP::operator delete(void* deadObject)
{
   s_repoAlloc.release(deadObject);
}
#else
void RepoSP::operator delete(void* deadObject, size_t size)
{
   s_repoAlloc.release(deadObject, size);
}
#endif


static const UWORD s_repoSPFileVersion = 0x0001;
Boolean RepoSP::read(FileReader& f, OrderBattle& ob)
{
   UWORD version;

   f >> version;

   SPIndex isp;
   f >> isp;
   d_sp = CampaignOBUtil::spIndexToISP(isp, ob);
   ASSERT(d_sp != NoStrengthPoint);

   f >> d_town;

   CPIndex cpi;
   f >> cpi;
   d_attachTo = CampaignOBUtil::cpIndexToCPI(cpi, ob);

   f >> d_travelTime;

   if(version < 0x0001)
      d_active = true;
   else
      f >> d_active;
   return f.isOK();
}

Boolean RepoSP::write(FileWriter& f, OrderBattle& ob) const
{
   if(f.isAscii())
   {
      // Todo:
   }
   else
   {

      f << s_repoSPFileVersion;

      SPIndex isp = CampaignOBUtil::spiToSPIndex(d_sp, ob);
      f << isp;

      f << d_town;

      CPIndex cpi = CampaignOBUtil::cpiToCPIndex(d_attachTo, ob);
      f << cpi;

      f << d_travelTime;

      f << d_active;
   }
   return f.isOK();
}


/*------------------------------------------------------
 * Independent SP list
 */

static const char s_repoSPListChunkName[] = "RepoSPs";
static UWORD s_repoSPListFileVersion = 0x0000;

Boolean RepoSPList::read(FileReader& f, OrderBattle& ob)
{
   FileChunkReader fr(f, s_repoSPListChunkName);

   UWORD version;
   f >> version;

   int nItems;
   f >> nItems;

   while(nItems--)
   {
      RepoSP* il = new RepoSP();
      ASSERT(il);

      il->read(f, ob);

      append(il);
   }

   return f.isOK();
}

Boolean RepoSPList::write(FileWriter& f, OrderBattle& ob) const
{
   FileChunkWriter fc(f, s_repoSPListChunkName);
   if(f.isAscii())
   {
      // Todo:
   }
   else
   {

      f << s_repoSPListFileVersion;

      int nItems = entries();
      f << nItems;

      SListIterR<RepoSP> iter(this);
      while(++iter)
      {
         iter.current()->write(f, ob);
      }
   }
   return f.isOK();
}




/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.2  2001/06/18 08:25:44  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
