/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef CAMPINT_HPP
#define CAMPINT_HPP

#ifndef __cplusplus
#error campint.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *	Campaign Interface
 *
 * Declares Virtual Interface to Campaign to use internally
 * without making components dependent on everything else
 *
 *----------------------------------------------------------------------
 */

#include "cdatadll.h"
#include "gamedefs.hpp"		// For ICommandPosition and ITown
#include "measure.hpp"		// For TimeTick
#include "control.hpp"
#include "gamebase.hpp"

#define GLOBAL_CAMPAIGN
// #undef GLOBAL_CAMPAIGN

class CampaignData;
class CampaignWindows;
class CampaignWindowsInterface;
class CampaignEvent;
class CampaignTimeControl;
class CampaignMessageInfo;
#ifdef DEBUG
class DrawDIBDC;
class PixelPoint;
class IMapWindow;
#endif
class ThreadManager;
class CampaignBattle;
class GameVictory;

class CampaignInterface : virtual public GameOwner
{

	// Unimplemented Functions to prevent copying

	CampaignInterface(const CampaignInterface& campInt);

 public:
// 		enum CampaignMode {
// 			CM_RESOURCE,
// 			CM_UNITS
// 		};

 public:
#if defined(GLOBAL_CAMPAIGN)
 	CAMPDATA_DLL CampaignInterface();
	CAMPDATA_DLL virtual ~CampaignInterface();
#else
	CampaignInterface() { }
	virtual ~CampaignInterface() { }
#endif

	/*
	 * Interface Functions
	 */

	// virtual bool saveGame() const = 0;
	virtual bool requestSaveWorld() const = 0;


	virtual CampaignWindowsInterface* getCampaignWindows() = 0;
		// Get Campaign Windows

	virtual CampaignData* getCampaignData() = 0;
		// Get pointer to Campaign Data Interface

// #if !defined(EDITOR)
// 	virtual CampaignMode getMode() const = 0;
// 	virtual void setMode(CampaignMode newMode) = 0;
// 		// Get and set the campaign mode
// #endif

	/*
	 * Campaign Data Time Functions
	 */

	virtual const CampaignTime& getCTime() = 0;
	virtual const Date& getDate() const = 0;
	virtual const Time& getTime() const = 0;
	virtual const char* asciiTime(char* buffer = 0) const = 0;
#if !defined(EDITOR)
	virtual TimeTick getTick() const = 0;
		// Return current Campaign TimeTick
#endif

	virtual void redrawMap() = 0;
		// Redraw the Campaign Map, static and dynamic
	virtual void repaintMap() = 0;
      // Redraw the Campaign Map, dynamic only
#if !defined(EDITOR)
	virtual CampaignTimeControl* getTimeControl() = 0;
#endif	// EDITOR

#if defined(CUSTOMIZE)
	virtual void removeAllTowns() = 0;
	virtual void removeAllUnits() = 0;
#endif

#if !defined(EDITOR)

#ifdef DEBUG
	virtual void showAI(DrawDIBDC* dib, ITown itown, const PixelPoint& p) = 0;
   virtual void drawAI(const IMapWindow& mw) = 0;
		// Display AI on map
#endif

	virtual void setController(Side side, GamePlayerControl::Controller control) = 0;

	virtual void startTactical(CampaignBattle* battle) = 0;
	// virtual void stopTactical() = 0;

   virtual void endCampaign(const GameVictory& victory) = 0;
   virtual void endVictoryScreen() = 0;

   virtual void armisticeBegun() = 0;
   virtual void armisticeEnded() = 0;

#endif	// !EDITOR

	// virtual ThreadManager* threadManager() = 0;
};


class CampaignTacticalOwner : virtual public GameOwner
{
    public:
        virtual ~CampaignTacticalOwner() { }
        virtual void tacticalOver() = 0;
    	  virtual CampaignData* getCampaignData() = 0;
        virtual void stopTime() = 0;
        virtual void startTime() = 0;
};

#if defined(GLOBAL_CAMPAIGN)
/*
 * Global Variables... These should be removed eventually
 */

CAMPDATA_DLL extern CampaignInterface* campaign;

#endif  // GLOBAL_CAMPAIGN

#endif /* CAMPINT_HPP */

