/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
#include "stdinc.hpp"
#include "c_obutil.hpp"
#include "ob.hpp"
#include "cleader.hpp"
#include "compos.hpp"

namespace CampaignOBUtil
{

CPIndex cpiToCPIndex(ConstParamCP cp, const OrderBattle& ob)
{
  if(cp != NoCommandPosition)
  {
    return ob.getIndex(cp->generic());
  }
  else
    return NoCPIndex;
}

ICommandPosition cpIndexToCPI(CPIndex cpi, OrderBattle& ob)
{
  if(cpi != NoCPIndex)
    return campCP(ob.command(cpi));
  else
    return NoCommandPosition;
}

LeaderIndex leaderToLeaderIndex(ConstILeader leader, const OrderBattle& ob)
{
  if(leader != NoLeader)
    return ob.getIndex(leader->generic());
  else
    return NoLeaderIndex;
}

ILeader leaderIndexToLeader(LeaderIndex li, OrderBattle& ob)
{
  if(li != NoLeaderIndex)
    return campLeader(ob.leader(li));
  else
    return NoLeader;
}


SPIndex spiToSPIndex(ISP sp, const OrderBattle& ob)
{
  if(sp != NoStrengthPoint)
    return ob.getIndex(sp);
  else
    return NoSPIndex;
}

ISP spIndexToISP(SPIndex spi, OrderBattle& ob)
{
  if(spi != NoSPIndex)
    return ob.sp(spi);
  else
    return NoStrengthPoint;
}

}; // namespace

/*
 *----------------------------------------------------------------------
 * $Log$
 *----------------------------------------------------------------------
 */
