/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef CPIDEF_HPP
#define CPIDEF_HPP

#ifndef __cplusplus
#error cpidef.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *	Lower level file used by cpdef.hpp
 *
 *----------------------------------------------------------------------
 */

#include "obdefs.hpp"

class CommandPosition;
class Leader;

typedef RefPtr<CommandPosition> ICommandPosition;
typedef RefPtr<Leader> ILeader;

typedef CRefPtr<CommandPosition> ConstICommandPosition;
typedef CRefPtr<Leader> ConstILeader;

static const ICommandPosition NoCommandPosition = 0;
static const ILeader NoLeader = 0;

typedef const ConstICommandPosition& ConstParamCP;
typedef const ICommandPosition& ParamCP;

/*
 * Naughty function to convert constant reference to CP into a non-constant CP
 *
 * It is used because the user interface should only deal with constant objects
 * so the order has constant targets.  But the logic needs them to be non-constant
 */

inline Leader* constLeaderToLeader(const ConstILeader& iLeader)
{
   const Leader* leader = iLeader;
   return const_cast<Leader*>(leader);
}

inline CommandPosition* constCPtoCP(ConstParamCP cp)
{
   const CommandPosition* cptr = cp;
   return const_cast<CommandPosition*>(cptr);
}

#endif /* CPIDEF_HPP */

