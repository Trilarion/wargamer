/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef VICTINT_HPP
#define VICTINT_HPP

#ifndef __cplusplus
#error victint.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *	Interface for Victory Message
 *
 *----------------------------------------------------------------------
 */

#include "cdatadll.h"
#include "wg_msg.hpp"

#if 0       // Todo.... put function in campctrl.hpp, etc...

namespace WargameMessage 
{
	class Victory : public MessageBase
	{
			CampaignData* d_campData;
		public:
			Victory(CampaignData* campData) : d_campData(campData) { }

			CAMPDATA_DLL void run();			// Implemented elsewhere
			void clear() { delete this; }
	};


};	// namespace WargameMessage 

#endif

#endif /* VICTINT_HPP */

