/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef CU_MODE_HPP
#define CU_MODE_HPP

#include "cdatadll.h"
#include "gamedefs.hpp"
#include "cpdef.hpp"
#include "campunit.hpp"

class CampaignData;

/*
 * Utility class for setting unit modes(CampaignMovement::CP_Mode).
 * This is the only ascess for setting unit modes.
 */

class CP_ModeUtil {
  public:
#if !defined(EDITOR)
	 CAMPDATA_DLL static Boolean setMode(CampaignData* campData, ICommandPosition cpi, CampaignMovement::CP_Mode mode);
#else
	 CAMPDATA_DLL static Boolean setMode(CampaignData* campData, ICommandPosition cpi, CampaignMovement::CP_Mode mode) { return True;}
#endif
};

#endif
