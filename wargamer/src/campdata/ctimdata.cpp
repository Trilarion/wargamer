/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 * Campaign Time Data
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "ctimdata.hpp"


/*
 * CampaignTimeData Class Functions
 */


CampaignTimeData::CampaignTimeData() :
   d_timeLock(),
   d_campaignTick(0),
   d_theTime(),
   d_date(),
   d_time()
{
   d_theTime.getDate(&d_date);
   d_theTime.getTime(&d_time);
   d_campaignTick = 0;
}

void CampaignTimeData::initTime(Year startYear, GameTick timeValue)
{
   d_theTime.setStartYear(startYear);
   d_theTime = timeValue;
// fineTime = d_theTime;
   d_theTime.getDate(&d_date);
   d_theTime.getTime(&d_time);
   d_campaignTick = gameTicksToTicks(d_theTime);
}

void CampaignTimeData::initTime(const Date& newDate)
{
   d_theTime.setStartYear(newDate.year);
   d_theTime.setDate(&newDate);
   d_theTime.getDate(&d_date);
   d_theTime.getTime(&d_time);
// fineTime = d_theTime;
   d_campaignTick = gameTicksToTicks(d_theTime);
}


void CampaignTimeData::setTimeTick(TimeTick t)
{
   GameTick gTick = ticksToGameTicks(t);

   d_timeLock.enter();

   d_campaignTick = t;
   d_theTime.set(gTick);
   d_theTime.getDate(&d_date);
   d_theTime.getTime(&d_time);

   d_timeLock.leave();
}


const char* CampaignTimeData::asciiTime(char* buffer) const
{
   static char cBuffer[200];

   if(buffer == 0)
      buffer = cBuffer;

   d_timeLock.enter();
   timeToAscii(buffer, d_date, d_time);
   d_timeLock.leave();

   return buffer;
}

/*
 *----------------------------------------------------------------------
 * $Log$
 *----------------------------------------------------------------------
 */
