/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1995, Steven Morle-Green, All Rights Reserved
 *----------------------------------------------------------------------
 *
 * Things that can be built in towns
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/06/13 08:52:37  greenius
 * Initial Import
 *
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "townbld.hpp"
#include "filebase.hpp"    // FileReader and FileWriter

#if !defined(EDITOR)


Boolean BuildItem::reduceNeeded(AttributePoints man, AttributePoints res)
{
// ASSERT(d_manpowerNeeded >= man);
// ASSERT(d_resourceNeeded >= res);

   if(d_manpowerNeeded < man)
      d_manpowerNeeded = 0;
   else
      d_manpowerNeeded -= man;

   if(d_resourceNeeded < res)
      d_resourceNeeded = 0;
   else
      d_resourceNeeded -= res;

   return (d_manpowerNeeded == 0) && (d_resourceNeeded == 0);
}

#endif   // !EDITOR

static UWORD s_tbFileVersion = 0x0000;
Boolean BuildItem::read(FileReader& f)
{
  UWORD version;

  f >> version;

  f >> d_what;
  f >> d_manpowerNeeded;
  f >> d_resourceNeeded;
  f >> d_priority;
  f >> d_quantity;
  f >> d_flags;

  return f.isOK();
}

Boolean BuildItem::write(FileWriter& f) const
{
  f << s_tbFileVersion;

  f << d_what;
  f << d_manpowerNeeded;
  f << d_resourceNeeded;
  f << d_priority;
  f << d_quantity;
  f << d_flags;

  return f.isOK();
}




int
BuildItem::pack(void * buffer) {

   int * data_ptr = (int *) buffer;

   *data_ptr = (int) d_what;
   data_ptr++;
   *data_ptr = (int) d_flags;
   data_ptr++;
   *data_ptr = (int) d_priority;
   data_ptr++;
   *data_ptr = (int) d_quantity;
   data_ptr++;
   *data_ptr = (int) d_manpowerNeeded;
   data_ptr++;
   *data_ptr = (int) d_resourceNeeded;

   return 24; // bytes
}


int
BuildItem::unpack(void * buffer) {

   int * data_ptr = (int *) buffer;

   d_what = (BuildID) *data_ptr;
   data_ptr++;
   d_flags = (UBYTE) *data_ptr;
   data_ptr++;
   d_priority = (BuildPriority) *data_ptr;
   data_ptr++;
   d_quantity = (BuildQuantity) *data_ptr;
   data_ptr++;
   d_manpowerNeeded = (AttributePoints) *data_ptr;
   data_ptr++;
   d_resourceNeeded = (AttributePoints) *data_ptr;

   return 24; // bytes
}














/*
 *----------------------------------------------------------------------
 * $Log$
 *----------------------------------------------------------------------
 */
