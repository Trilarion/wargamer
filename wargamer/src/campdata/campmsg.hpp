/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef CAMPMSG_HPP
#define CAMPMSG_HPP

#ifndef __cplusplus
#error campmsg.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *      Campaign Messages
 *
 *----------------------------------------------------------------------
 */

#include "cdatadll.h"
#include "gamedefs.hpp"
#include "cmsg_id.hpp"
#include "cpdef.hpp"
#include "gpmsg.hpp"            // GenericPlayerMessage
#include "c_sounds.hpp"

class SimpleString;
class CampaignData;


namespace CampaignMessages
{
   CAMPDATA_DLL	int getResID(CMSG_ID id);
   CAMPDATA_DLL	int getDescriptionID(CMSG_ID id);
   CAMPDATA_DLL	const char* getText(CMSG_ID id);
   CAMPDATA_DLL	MessageGroup getGroup(CMSG_ID id);
   CAMPDATA_DLL	int getGroupID(MessageGroup group);

   CAMPDATA_DLL	void getDescription(SimpleString& s, CMSG_ID id);
   CAMPDATA_DLL	void getGroupName(SimpleString& s, MessageGroup group);
};		// namespace CampaignMessages


// using namespace CampaignMessages;

class CampaignMessageInfo : public GenericPlayerMessage
{
 public:
        typedef CampaignMessages::CMSG_ID CMSG_ID;
 private:
        /*
         * {Key} is what is used in formatted messages to substitute the data
         * Note that only the 1st 2 letters are used.
         */

        CMSG_ID                         d_id;                   // ID of message
        Side                                    d_side;         // Which side is the message for?
        ICommandPosition        d_cp;                   // {Unit}       Who is the message from?
        ConstICommandPosition   d_cpTarget;     // {Target} Enemy Unit
        ITown                           d_where;                // {Town}       Town where event happened
        UnitType                        d_what;         // {Type}       Type of unit that has been built
        ILeader                         d_leader;       // {Leader}     Leader
        const char*                     d_s1;                   // {S1}         Miscellaneous text 1
        const char*                     d_s2;                   // {S2}         Miscellaneous text 2
        int                                     d_value1;       // {N1}         An integer value
        CampaignSoundTypeEnum d_sound;
        // More complex messages may need more data?

 public:
        CAMPDATA_DLL CampaignMessageInfo();          // Default constructor needed for STL containers
        CAMPDATA_DLL CampaignMessageInfo(Side s, CMSG_ID id);

        // Accessors

        void cp(const ICommandPosition& cp) { d_cp = cp; }
        void cpTarget(const ConstICommandPosition& enemy) { d_cpTarget = enemy; }
        void where(ITown town) { d_where = town; }
        void what(UnitType what) { d_what = what; }
        void leader(const ILeader& leader) { d_leader = leader; }
        void text1(const char* s) { d_s1 = s; }
        void text2(const char* s) { d_s2 = s; }
        void value1(int n) { d_value1 = n; }
        void sound(CampaignSoundTypeEnum sound) { d_sound = sound; }

        CMSG_ID                         id() const { return d_id; }
        Side                                    side() const { return d_side; }
        ICommandPosition        cp() const { return d_cp; }
        ConstICommandPosition   cpTarget() const { return d_cpTarget; }
        ITown                           where() const { return d_where; }
        UnitType                        what() const { return d_what; }
        ILeader                         leader() const { return d_leader; }
        const char*                     s1() const { return d_s1; }
        const char*                     s2() const { return d_s2; }
        int                                     value1() const { return d_value1; }
        CampaignSoundTypeEnum sound() const { return d_sound; }

        //------ Moved to cmsgutil
        // void text(SimpleString& s, CampaignData* campData) const;
                // Create a human readable version of the message

        int getResID() const { return CampaignMessages::getResID(d_id); }
        int getDescriptionID() const { return CampaignMessages::getDescriptionID(d_id); }
        const char* getText() const { return CampaignMessages::getText(d_id); }

 private:
        //------ Moved to cmsgutil
        // void formatMessage(SimpleString& s, const char* fmt, CampaignData* campData) const;


        friend bool operator < (const CampaignMessageInfo& m1, const CampaignMessageInfo& m2);
        friend bool operator == (const CampaignMessageInfo& m1, const CampaignMessageInfo& m2);
};



#endif /* CAMPMSG_HPP */

