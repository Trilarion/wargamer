/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
#include "stdinc.hpp"
#include "ileader.hpp"
#include "fsalloc.hpp"
#include "c_obutil.hpp"
#include "filebase.hpp"    // FileReader and FileWriter
#include "filecnk.hpp"

/*---------------------------------------------------------------
 *   Independent leaders and lists
 */

#ifdef DEBUG
static FixedSizeAlloc<IndependentLeader> s_iLeaderAlloc("IndependentLeader");
#else
static FixedSizeAlloc<IndependentLeader> s_iLeaderAlloc;
#endif  // DEBUG

void* IndependentLeader::operator new(size_t size)
{
  return s_iLeaderAlloc.alloc(size);
}

#ifdef _MSC_VER
void IndependentLeader::operator delete(void* deadObject)
{
  s_iLeaderAlloc.release(deadObject);
}
#else
void IndependentLeader::operator delete(void* deadObject, size_t size)
{
  s_iLeaderAlloc.release(deadObject, size);
}
#endif

static UWORD s_iLeaderFileVersion = 0x0001;
Boolean IndependentLeader::read(FileReader& f, OrderBattle& ob)
{
  UWORD version;

  f >> version;

  LeaderIndex iLeader;
  f >> iLeader;
  d_leader = CampaignOBUtil::leaderIndexToLeader(iLeader, ob);
  ASSERT(d_leader != NoLeader);

  f >> d_town;
  f >> d_atSHQ;

  CPIndex cpi;
  f >> cpi;
  d_attachTo = CampaignOBUtil::cpIndexToCPI(cpi, ob);

  f >> d_travelTime;

  if(version >= 0x0001)
    f >> d_active;

  return f.isOK();
}

Boolean IndependentLeader::write(FileWriter& f, OrderBattle& ob) const
{
   if(f.isAscii())
   {
      // Todo:
   }
   else
   {
      f << s_iLeaderFileVersion;

      LeaderIndex iLeader = CampaignOBUtil::leaderToLeaderIndex(d_leader, ob);
      f << iLeader;

      f << d_town;
      f << d_atSHQ;

      CPIndex cpi = CampaignOBUtil::cpiToCPIndex(d_attachTo, ob);
      f << cpi;

      f << d_travelTime;
      f << d_active;
   }
   return f.isOK();
}





/*------------------------------------------------------
 * Independent Leader list
 */

static const char s_iLeaderChunkName[] = "IndependentLeaders";
static UWORD s_iLeaderListFileVersion = 0x0000;

Boolean IndependentLeaderList::read(FileReader& f, OrderBattle& ob)
{
  FileChunkReader fr(f, s_iLeaderChunkName);

  UWORD version;
  f >> version;

  int nItems;
  f >> nItems;

  while(nItems--)
  {
    IndependentLeader* il = new IndependentLeader();
    ASSERT(il);

    il->read(f, ob);

    append(il);
  }

  return f.isOK();
}

Boolean IndependentLeaderList::write(FileWriter& f, OrderBattle& ob) const
{
   FileChunkWriter fc(f, s_iLeaderChunkName);

   if(f.isAscii())
   {
      // Todo:
   }
   else
   {
      f << s_iLeaderListFileVersion;

      int nItems = entries();
      f << nItems;

      SListIterR<IndependentLeader> iter(this);
      while(++iter)
      {
         iter.current()->write(f, ob);
      }
   }

   return f.isOK();
}

/*--------------------------------------------------------------------
 * Captured Leader
 */

#ifdef DEBUG
static FixedSizeAlloc<CapturedLeader> s_cLeaderAlloc("CapturedLeader");
#else
static FixedSizeAlloc<CapturedLeader> s_cLeaderAlloc;
#endif  // DEBUG

void* CapturedLeader::operator new(size_t size)
{
  return s_cLeaderAlloc.alloc(size);
}

#ifdef _MSC_VER
void CapturedLeader::operator delete(void* deadObject)
{
  s_cLeaderAlloc.release(deadObject);
}
#else
void CapturedLeader::operator delete(void* deadObject, size_t size)
{
  s_cLeaderAlloc.release(deadObject, size);
}
#endif

static UWORD s_cLeaderFileVersion = 0x0000;
Boolean CapturedLeader::read(FileReader& f, OrderBattle& ob)
{
  UWORD version;
  f >> version;

  LeaderIndex iLeader;
  f >> iLeader;
  d_leader = CampaignOBUtil::leaderIndexToLeader(iLeader, ob);
  ASSERT(d_leader != NoLeader);

  return f.isOK();
}

Boolean CapturedLeader::write(FileWriter& f, OrderBattle& ob) const
{
   if(f.isAscii())
   {
      // Todo:
   }
   else
   {
      f << s_cLeaderFileVersion;

      LeaderIndex iLeader = CampaignOBUtil::leaderToLeaderIndex(d_leader, ob);
      f << iLeader;
   }
   return f.isOK();
}


/*------------------------------------------------------
 * Captured Leader list
 */

static const char s_cLeaderChunkName[] = "CapturedLeaders";
static UWORD s_cLeaderListFileVersion = 0x0000;

Boolean CapturedLeaderList::read(FileReader& f, OrderBattle& ob)
{
  FileChunkReader fr(f, s_cLeaderChunkName);
  UWORD version;
  f >> version;

  int nItems;
  f >> nItems;

  while(nItems--)
  {
    CapturedLeader* cl = new CapturedLeader();
    ASSERT(cl);

    cl->read(f, ob);

    append(cl);
  }

  return f.isOK();
}

Boolean CapturedLeaderList::write(FileWriter& f, OrderBattle& ob) const
{
   FileChunkWriter fc(f, s_cLeaderChunkName);
   if(f.isAscii())
   {
      // Todo:
   }
   else
   {
      f << s_cLeaderListFileVersion;

      int nItems = entries();
      f << nItems;

      SListIterR<CapturedLeader> iter(this);
      while(++iter)
      {
         iter.current()->write(f, ob);
      }
   }
   return f.isOK();
}

/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.2  2001/06/18 08:25:44  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
