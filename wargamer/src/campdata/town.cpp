/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1995, Steven Morle-Green, All Rights Reserved
 *----------------------------------------------------------------------
 *
 * Provinces, Towns and Connections Imlementation
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "town.hpp"
#include "wldfile.hpp"
#include "filecnk.hpp"
#include "savegame.hpp"
#include "scenario.hpp"

/*------------------------------------------------------------------
 * Province Code
 */

StringFileTable* Town::s_nameTable = 0;
StringFileTable* Province::s_nameTable = 0;

const char Town::s_nameFile[] = "townNames.dat";
const char Province::s_nameFile[] = "provinceNames.dat";

StringFileTable* Town::getNameTable()
{
   if (s_nameTable == 0)
   {
      CString fName = scenario->makeScenarioFileName(s_nameFile, false);
      s_nameTable = new StringFileTable(fName);
   }
   return s_nameTable;
}

void Town::writeNameTable()
{
   if(s_nameTable)
      s_nameTable->writeFile();
}

void Town::destroyNameTable()
{
   delete s_nameTable;
   s_nameTable = 0;
}



StringFileTable* Province::getNameTable()
{
   if (s_nameTable == 0)
   {
      CString fName = scenario->makeScenarioFileName(s_nameFile, false);
      s_nameTable = new StringFileTable(fName);
   }
   return s_nameTable;
}

void Province::writeNameTable()
{
   if(s_nameTable)
      s_nameTable->writeFile();
}

void Province::destroyNameTable()
{
   delete s_nameTable;
   s_nameTable = 0;
}


/*
 * Constructor with no parameters sets all values to known state
 */


Province::Province() :
   d_nameID(StringFileTable::Null),
   d_shortNameID(StringFileTable::Null),
   d_capital(NoTown),
   d_nationality(0),
   d_resource(0),
   d_manPower(0),
   d_nTowns(0),
   d_town(NoTown),
   d_flags(0),
   d_helpID(NoHelpID) {}


void Province::cancelBuilds()
{
  for(int i = 0; i < BasicUnitType::HowMany; i++)
  {
    d_builds[i].reset();
  }
}


const UWORD Province::fileVersion = 0x0008;

Boolean Province::readData(FileReader& f)
{
   if(f.isAscii())
   {
   }
   else
   {
      UWORD version;
      f.getUWord(version);

      ASSERT(version <= fileVersion);

      if (version < 0x0008)
      {
         d_nameID = getNameTable()->add(f.getString());
      }
      else
      {
         f >> d_nameID;
      }
      ASSERT(getNameTable()->isValid(d_nameID));




      LocationObject::readData(f);
      // Location l;
      // readLocation(f, l);
      // setLocation(l);

      if (version < 0x0008)
      {
         // d_shortName.setName(f.getString());
         d_shortNameID = getNameTable()->add(f.getString());
      }
      else
         f >> d_shortNameID;

      ASSERT(getNameTable()->isValid(d_shortNameID));


      if(version < 0x0007)
      {
         d_picName.setName(copyString(getName()));
      }
      else
         f >> d_picName;

      f.getUWord(d_capital);
      f.getUWord(d_town);
      f.getUWord(d_nTowns);

      Side newSide;
      f.getUByte(newSide);
      setSide(newSide);
      f.getUByte(newSide);
      setStartSide(newSide);

      if(version < 0x0001)
         d_nationality = getStartSide();
      else
         f.getUByte(d_nationality);


      if(version < 0x0002)
      {
         UWORD trash;
         f.getUWord(trash);
         f.getUWord(trash);
      }

      if(version >= 0x0005)
      {
        f >> d_resource;
        f >> d_manPower;
      }

      if(version >= 0x0003)
      {
        if(version < 0x0005)
        {
          Boolean offScreen;
          f >> offScreen;  // .getUByte(offScreen);
          setFlag(OffScreen, offScreen);
        }
        else
          f >> d_flags;
      }

      if(version >= 0x0004)
        f.getUWord(d_helpID);
      else
        d_helpID = NoHelpID;

      if(version >= 0x0006)
      {
        for(int i = 0; i < BasicUnitType::HowMany; i++)
          d_builds[i].read(f);
      }

      return f.isOK();
   }

   return False;
}

Boolean Province::writeData(FileWriter& f) const
{
   if(f.isAscii())
   {
      f.printf(startItemString);
      if(getName())
         f.printf(fmt_ssn, nameToken, getName());
      if(getShortName())
         f.printf(fmt_ssn, abrevToken, getShortName());
//    if(d_shortName.getName())
//       f.printf(fmt_ssn, abrevToken, d_shortName.getName());
      f.printf(fmt_slln, locationToken, getLocation().getX(), getLocation().getY());
      f.printf(fmt_sdn, capitalToken, (int) d_capital);
      f.printf(fmt_sddn, townsToken, (int) d_town, (int) d_nTowns);
      f.printf(fmt_sddn, sideToken, (int) getSide(), (int) getStartSide(), (int) d_nationality);
      f.printf(fmt_sdn, offScreenToken, (int)isOffScreen());
      // f.printf(fmt_sdn, resourceToken, resource);
      // f.printf(fmt_sdn, manPowerToken, manPower);
      f.printf(endItemString);
   }
   else
   {
      f.putUWord(fileVersion);
//    f.putString(getName());
      f << d_nameID;
      // putLocation(f, getLocation());
      LocationObject::writeData(f);
//    f.putString(d_shortName.getName());
      f << d_shortNameID;
      f << d_picName;
      f.putUWord(d_capital);
      f.putUWord(d_town);
      f.putUWord(d_nTowns);
      f.putUByte(getSide());
      f.putUByte(getStartSide());
      f.putUByte(d_nationality);
      f.putUWord(d_resource);
      f.putUWord(d_manPower);
      f.putUByte(d_flags);
      f.putUWord(d_helpID);

      for(int i = 0; i < BasicUnitType::HowMany; i++)
        d_builds[i].write(f);
      // f.putUWord(resource);
      // f.putUWord(manPower);

      // More to go in here

   }

   return f.isOK();
}

void Province::setName(char* s)
{
   d_nameID = getNameTable()->update(d_nameID, s);
}

const char* Province::getName() const
{
   return getNameTable()->get(d_nameID);
}

void Province::setShortName(char* s)
{
   d_shortNameID = getNameTable()->update(d_shortNameID, s);
}

const char* Province::getShortName() const
{
   return getNameTable()->get(d_shortNameID);
}


/*------------------------------------------------------------------
 * Code for Towns
 */

Town::Town() :
  d_nameID(StringFileTable::Null),
  d_province(NoProvince),
  d_nameAlign(NA_TOP),
  d_nameX(0),
  d_nameY(0),
  d_size(TOWN_Other),
  d_political(0),
  d_manPowerRate(0),
  d_resourceRate(0),
  d_supplyRate(0),
  d_forageBase(0),
  d_forageStock(0),
  d_strength(0),
  d_stacking(UBYTE_MAX),
  d_terrain(NoTerrainType),
  d_flags(0),
  d_fortifications(0),
  d_fortUpgrade(NoCommandPosition),
  d_supplyInstall(NoCommandPosition),
  d_stateFlags(BridgeUp),
  d_weeksUnderSiege(0),
  d_catastrophe(0),
  d_helpID(NoHelpID)
{
   for(int i = 0; i < MaxConnections; i++)
      d_connections[i] = NoConnection;

   for(Side s = 0; s < NumberSides; s++)
     d_victory[s] = 0;
}

void Town::setName(char* s)
{
   d_nameID = getNameTable()->update(d_nameID, s);
}

const char* Town::getName() const
{
   return getNameTable()->get(d_nameID);
}

/*
 * Adjust connection list if connection ci when been deleted
 */

void Town::adjustConnections(IConnection ci)
{
   int ifrom = 0;
   int ito = 0;

   while( (d_connections[ifrom] != NoConnection) && (ifrom < MaxConnections))
   {
      if(d_connections[ifrom] == ci)
         ;
      else if(d_connections[ifrom] > ci)
      {
         d_connections[ito] = IConnection(d_connections[ifrom] - 1);
         ito++;
      }
      else
      {
         d_connections[ito] = d_connections[ifrom];
         ito++;
      }
      ifrom++;
   }

   while(ito < ifrom)
   {
      d_connections[ito] = NoConnection;
      ito++;
   }
}

/*
 * Add a new conection to town's connection list
 */

int Town::addConnection(IConnection ci)
{
   for(int i = 0; i < MaxConnections; i++)
   {
      if(d_connections[i] == NoConnection)
      {
         d_connections[i] = ci;
         return i;
      }
   }

#ifdef DEBUG
   throw GeneralError("Connection Slots all used up for %s\n", getNameNotNull());
#else
   return -1;
#endif
}

/*
 * Return how far away a town can supply a unit
 * This might depend on the size of the town, or it's supply level
 *
 * For now, we just return a constant!
 *
 * Two values are filled in:
 *    fullRange, is that distance away that a unit can get 100% supply
 *    maxRange, is the distance at which a unit will get no supply
 *
 * Thus a unit gets supplies as:
 *    0..fullRange: 100% (if town has enough supplies)
 *    fullRange..maxRange: gets (maxRange - distance) / (maxRange-fullRange)
 *    > maxRange: Unsupplied
 *
 * Return True if it can supply
 */


// multipliers to distances, using a base of 10
// Note: should get this from scenario table
static const int s_modifiers[TownSize_MAX] = {
      12,
      10,
       8,
       6
};

const int s_base = 10;

Boolean Town::getSupplyRange(Distance& fullRange, Distance& maxRange) const
{
   static const Distance MaxSupplyDistance = MilesToDistance(60);
   static const Distance FullSupplyDistance = MilesToDistance(30);

   ASSERT(getIsDepot() || getIsSupplySource());

   if( (getIsDepot()) ||
       ((getIsSupplySource()) && (getSupplyLevel() != 0)) )
   {
      fullRange = MulDiv(FullSupplyDistance, s_modifiers[d_size], s_base);
      maxRange = MulDiv(MaxSupplyDistance, s_modifiers[d_size], s_base);
      return True;
   }
   else
   {
      fullRange = 0;
      maxRange = 0;
      return False;
   }
}

Distance Town::getSupplyMaxRange(TownSize ts)
{
   ASSERT(ts < TownSize_MAX);
   static const Distance MaxSupplyDistance = MilesToDistance(60);

   return MulDiv(MaxSupplyDistance, s_modifiers[ts], s_base);
}

Boolean Town::upgradeFortifications()
{
   ASSERT(d_fortifications < MaxFortifications);
   ASSERT(d_fortUpgrade != NoCommandPosition);

   if(d_fortifications < MaxFortifications)
   {
      d_fortifications++;
      return True;
   }
   return False;
}


void Town::setFortUpgrader(ICommandPosition cpi)
{
// #ifdef DEBUG
//    if(cpi == NoCommandPosition)
//       ASSERT(d_fortUpgrade != NoCommandPosition);
//    else
//       ASSERT(d_fortUpgrade == NoCommandPosition);
// #endif
   d_fortUpgrade = cpi;
}

void Town::setSupplyInstaller(ICommandPosition cpi)
{
// #ifdef DEBUG
//    if(cpi == NoCommandPosition)
//       ASSERT(d_supplyInstall != NoCommandPosition);
//    else
//       ASSERT(d_supplyInstall == NoCommandPosition);
// #endif
   d_supplyInstall = cpi;
}


/*----------------------------------------------------
 * file interface
 */

inline bool operator >> (FileReader& f, NameAlign& n)
{
   UBYTE b;
   f >> b;

   n = static_cast<NameAlign>(b);
   return true;
}

inline bool operator << (FileWriter& f, const NameAlign& n)
{
   return f.putUByte(n);
}

inline bool operator >> (FileReader& f, TownSize& ts)
{
   UBYTE b;
   f >> b;

   ts = static_cast<TownSize>(b);
   return true;
}

inline bool operator << (FileWriter& f, const TownSize& ts)
{
   return f.putUByte(ts);
}

const UWORD Town::fileVersion = 0x0006;

Boolean Town::readData(FileReader& f)
{
   if(f.isAscii())
   {
   }
   else
   {
      UWORD version;
      f >> version;
      ASSERT(version <= fileVersion);

      if (version < 0x0005)
      {
         d_nameID = getNameTable()->add(f.getString());
      }
      else
         f >> d_nameID;

      ASSERT(getNameTable()->isValid(d_nameID));

      Side newSide;
      f >> newSide;
      setSide(newSide);

      LocationObject::readData(f);

      for(int i = 0; i < MaxConnections; i++)
         f >> d_connections[i];

      f >> d_province;

      UBYTE tb;
      f >> tb;
      d_nameAlign = NameAlign(tb);

      f >> d_nameX;
      f >> d_nameY;

      f >> tb;
      d_size = TownSize(tb);

      for(Side s = 0; s < NumberSides; s++)
        f >> d_victory[s];

      f >> d_political;
      f >> d_manPowerRate;
      f >> d_resourceRate;
      f >> d_supplyRate;
      f >> d_forageBase;
      if(version >= 0x0001)
        f >> d_forageStock;

      f >> d_stacking;
      f >> d_terrain;
      f >> d_flags;
      f >> d_fortifications;
      if(version >= 0x0004)
         f >> d_supplyLevel;

      f >> d_helpID;
      f >> d_strength;

      if(f.getMode() == SaveGame::SaveGame)
      {
        f >> d_stateFlags;

        // temporary bodge
        if(version <= 0x0002)
          d_stateFlags |= BridgeUp;

        if( (version >= 0x0002) && (version < 0x0006) )
        {
          // d_orders.read(f);

           UWORD version;
           f >> version;
           ASSERT(version == 0);

           int nNodes;
           f >> nNodes;

           ASSERT(nNodes == 0);
        }

      }
   }

   return f.isOK();
}

Boolean Town::writeData(FileWriter& f) const
{
   if(f.isAscii())
   {
      f.printf(startItemString);

      if(getName())
         f.printf(fmt_ssn, nameToken, getName());

      // f.printf(fmt_sddn, sideToken, (int) getSide(), (int) getStartSide());
      f.printf(fmt_slln, locationToken, getLocation().getX(), getLocation().getY());

      if(d_connections[0] != NoConnection)
      {
         f.printf(connectionToken);
         for(int i = 0; i < MaxConnections; i++)
         {
            if(d_connections[i] == NoConnection)
               break;
            f.printf(" %d", (int) d_connections[i]);
         }
         f.printf(eolString);
      }

      f.printf(fmt_sdn,    provinceToken,       (int) d_province);
      f.printf(fmt_sdddn,  nameAlignToken,      (int) d_nameAlign,      (int) d_nameX, (int) d_nameY);
      f.printf(fmt_sdn,    sizeToken,           (int) d_size);
      f.printf(fmt_sddn,   victoryToken,        (int) d_victory[0],  (int) d_victory[1]);
      f.printf(fmt_sdn,    politicalToken,      (int) d_political);
      f.printf(fmt_sdn,    manPowerToken,       (int) d_manPowerRate);
      f.printf(fmt_sdn,    resourceToken,       (int) d_resourceRate);
      f.printf(fmt_sdn,    supplyToken,         (int) d_supplyRate);
      f.printf(fmt_sdn,    forageToken,         (int) d_forageBase);
      f.printf(fmt_sdn,    stackingToken,       (int) d_stacking);
      f.printf(fmt_sdn,    terrainToken,        (int) d_terrain);
      f.printf(fmt_sdn,    flagsToken,          (int) d_flags);
      f.printf(fmt_sdn,    fortificationsToken, (int) d_fortifications);
      f.printf(endItemString);
   }
   else
   {
      f << fileVersion;
//    f.putString(getName());
      f << d_nameID;
      f << getSide();

      LocationObject::writeData(f);

      for(int i = 0; i < MaxConnections; i++)
         f << d_connections[i];

      f << d_province;
      CHECKUBYTE(d_nameAlign);
      f << d_nameAlign;
      f << d_nameX;
      f << d_nameY;
      CHECKUBYTE(d_size);
      f << d_size;

      for(Side s = 0; s < NumberSides; s++)
        f << d_victory[s];

      f << d_political;
      f << d_manPowerRate;
      f << d_resourceRate;
      f << d_supplyRate;
      f << d_forageBase;
      f << d_forageStock;
      f << d_stacking;
      f << d_terrain;
      f << d_flags;
      f << d_fortifications;
      f << d_supplyLevel;
      f << d_helpID;
      f << d_strength;

      if(f.getMode() == SaveGame::SaveGame)
      {
        f << d_stateFlags;
        // d_orders.write(f);
      }
   }

   return f.isOK();
}


/*
 * Supply Level
 */

Boolean SupplyLevel::readData(FileReader& f)
{
   f >> value;
   return f.isOK();
}

Boolean SupplyLevel::writeData(FileWriter& f) const
{
   f << value;
   return f.isOK();
}


/*
 * ---------------------- Connections ----------------------------
 */

#ifdef DEBUG
const char* WhichSideChokePoint::name(WhichSideChokePoint::Type t)
{
  ASSERT(t < HowMany);

  static const char* nameText[] = {
    "No Chokepoint",
    "This side of chokepoint",
    "That side of chokepoint"
  };

  return nameText[t];
}
#endif

ITown Connection::getOtherTown(ITown t1) const
{
   ASSERT((t1 == node1) || (t1 == node2));

   if(t1 == node1)
      return node2;
   else if(t1 == node2)
      return node1;
   else
      return NoTown;
}


const UWORD Connection::fileVersion = 0x0003;

Boolean Connection::readData(FileReader& f)
{
   if(f.isAscii())
   {
      return False;
   }
   else
   {
      UWORD version;
      f.getUWord(version);
      ASSERT(version <= fileVersion);
      f.getUWord(node1);
      f.getUWord(node2);

      UBYTE b;
      f.getUByte(b);
      how = ConnectType(b);

      f.getUByte(b);
      quality = ConnectQuality(b);

      f.getUByte(capacity);

      if(version >= 0x0001)
         f >> offScreen;
        // f.getUByte(offScreen);
      else
        offScreen = False;

      if(version >= 0x0001)
        f.getSLong(distance);
      else
        distance = 0;

      if(version >= 0x0002)
      {
        f.getUByte(b);
        whichSide1 = static_cast<WhichSideChokePoint::Type>(b);

        if(version >= 0x0003)
        {
          f.getUByte(b);
          whichSide2 = static_cast<WhichSideChokePoint::Type>(b);
        }
        else
          whichSide2 = whichSide1;
      }

      return f.isOK();
   }
}

Boolean Connection::writeData(FileWriter& f) const
{
   if(f.isAscii())
   {
      f.printf(startItemString);
      f.printf(fmt_sddn,
         nodeToken,
         (int) node1,
         (int) node2);
      f.printf(fmt_sdn, howToken, (int) how);
      f.printf(fmt_sdn, qualityToken, (int) quality);
      f.printf(fmt_sdn, capacityToken, (int) capacity);
      f.printf(fmt_sdn, offScreenToken, (int)offScreen);
      f.printf(fmt_sln, distanceToken, (SLONG)distance);
      f.printf(endItemString);
   }
   else
   {
      f.putUWord(fileVersion);
      f.putUWord(node1);
      f.putUWord(node2);
      CHECKUBYTE(how);
      f.putUByte(how);
      CHECKUBYTE(quality);
      f.putUByte(quality);
      f.putUByte(capacity);
      f.putUByte(offScreen);
      f.putSLong(distance);
      CHECKUBYTE(whichSide1);
      f.putUByte(whichSide1);
      CHECKUBYTE(whichSide2);
      f.putUByte(whichSide2);
   }

   return f.isOK();
}

WhichSideChokePoint::Type Connection::whichSideChokePoint(ITown node) const
{
   ASSERT((node == node1) || (node == node2));

   if(node == node1)
      return whichSide1;
   else if(node == node2)
      return whichSide2;
   else
      return WhichSideChokePoint::NoChokePoint;
}

#if defined(CUSTOMIZE)
void Connection::setWhichSideChokePoint(ITown node, WhichSideChokePoint::Type whichSide)
{
   ASSERT((node == node1) || (node == node2));

   if(node == node1)
      whichSide1 = whichSide;
   else if(node == node2)
      whichSide2 = whichSide;
}
#endif


/*
 * Town Iterator
 */


TownIter::TownIter(const TownList& tl) : CArrayIter<Town>(tl)
{
   list = &tl;
   tl.startRead();
}

TownIter::~TownIter()
{
   list->endRead();
}

TownIterWrite::TownIterWrite(TownList& tl) : ArrayIter<Town>(tl)
{
   list = &tl;
   tl.startWrite();
}

TownIterWrite::~TownIterWrite()
{
   list->endWrite();
}


ProvinceIter::ProvinceIter(const ProvinceList& tl) : CArrayIter<Province>(tl)
{
   list = &tl;
   tl.startRead();
}

ProvinceIter::~ProvinceIter()
{
   list->endRead();
}

ProvinceIterWrite::ProvinceIterWrite(ProvinceList& tl) : ArrayIter<Province>(tl)
{
   list = &tl;
   tl.startWrite();
}

ProvinceIterWrite::~ProvinceIterWrite()
{
   list->endWrite();
}


ConnectionIter::ConnectionIter(ConnectionList& tl) : ArrayIter<Connection>(tl)
{
   list = &tl;
   tl.startRead();
}

ConnectionIter::~ConnectionIter()
{
   list->endRead();
}

ConnectionIterWrite::ConnectionIterWrite(ConnectionList& tl) : ArrayIter<Connection>(tl)
{
   list = &tl;
   tl.startRead();
}

ConnectionIterWrite::~ConnectionIterWrite()
{
   list->endRead();
}


/*================================================================
 * Lists
 */

static const UWORD arrayListVersion = 0x0000;

template<class T>
Boolean readData(Array<T>& list, FileReader& f, const char* chunkName)
{
   FileChunkReader fr(f, chunkName);
   if(!fr.isOK())
      return False;

   if(f.isAscii())
   {
      return False;
   }
   else
   {
      UWORD version;
      f.getUWord(version);
      ASSERT(version <= arrayListVersion);
      UWORD count;
      f.getUWord(count);
      list.init(count);
   }

   for(ArrayIndex i = 0; i < list.entries(); i++)
   {
      T& item = list[i];

      item.readData(f);
   }

   return f.isOK();
}


template<class T>
Boolean writeData(const Array<T>& list, FileWriter& f, const char* chunkName)
{
   FileChunkWriter fc(f, chunkName);

   /*
    * Header
    */

   if(f.isAscii())
   {
      f.printf(fmt_sdn, entriesToken, (int) list.entries());
   }
   else
   {
      /*
       * Header
       */

      f.putUWord(arrayListVersion);
      f.putUWord(list.entries());
   }

   /*
    * Each Connection
    */

   CArrayIter<T> tl = list;   // campaignData->getConnections();

   while(++tl)
   {
      const T& item = tl.current();

      if(!item.writeData(f))
         return False;
   }

   return f.isOK();
}

/*========================================================
 * Connections
 */

// const UWORD DConnectionListVersion  = 0x0000;

Boolean ConnectionList::readData(FileReader& f)
{
   return ::readData(*this, f, connectionChunkName);
#if 0
   FileChunkReader fr(f, connectionChunkName);
   if(!fr.isOK())
      return False;

   if(f.isAscii())
   {
      return False;
   }
   else
   {
      UWORD version;
      f.getUWord(version);
      ASSERT(version <= DConnectionListVersion);
      UWORD count;
      f.getUWord(count);
      init(count);
   }

   for(ArrayIndex i = 0; i < entries(); i++)
   {
      Connection& conn = get(i);

      conn.readData(f);
   }

   return f.isOK();
#endif
}

Boolean ConnectionList::writeData(FileWriter& f) const
{
   return ::writeData(*this, f, connectionChunkName);
#if 0
   FileChunkWriter fc(f, connectionChunkName);

   /*
    * Header
    */

   if(f.isAscii())
   {
      f.printf(fmt_sdn, entriesToken, (int) entries());
   }
   else
   {
      /*
       * Header
       */

      f.putUWord(DConnectionListVersion);
      f.putUWord(entries());
   }

   /*
    * Each Connection
    */

   CArrayIter<Connection> tl = *this;  // campaignData->getConnections();

   while(++tl)
   {
      Connection& conn = tl.current();

      if(!conn.writeData(f))
         return False;
   }

   return f.isOK();
#endif
}



/*========================================================
 * Provinces
 */

ProvinceList::ProvinceList() :
   d_nullProvince(0)
{
}

ProvinceList::~ProvinceList()
{
   Province::destroyNameTable();
   delete d_nullProvince;
   d_nullProvince = 0;
}

Province* ProvinceList::nullProvince() const
{
   if (!d_nullProvince)
   {
      d_nullProvince = new Province;
      d_nullProvince->setName("<No Province>");
      d_nullProvince->setShortName("<Null>");

   }
   return d_nullProvince;

}

// static ProvinceList::NullProvinceGenerator ProvinceList::s_nullProvince;

// ProvinceList::NullProvinceGenerator::NullProvinceGenerator()
// {
//   d_province = 0;
//    d_province.setName(copyString("<No Province>"));
//    d_province.setShortName(copyString("<Null>"));
//}

// ProvinceList::NullProvinceGenerator::NullProvinceGenerator()
// {
//    delete d_province;
//    d_province = 0;
// }
//
// Province* NullProvinceGenerator::instance() const
// {
//    d_province = new Province;
//    d_province.setName("<No Province>");
//    d_province.setShortName("<Null>");
// }

// const UWORD DProvinceListVersion = 0x0000;

Boolean ProvinceList::readData(FileReader& f)
{
   return ::readData(*this, f, provinceChunkName);
#if 0
   FileChunkReader fr(f, provinceChunkName);
   if(!fr.isOK())
      return False;

   if(f.isAscii())
   {
      return False;
   }
   else
   {
      UWORD version;
      f.getUWord(version);
      ASSERT(version <= DProvinceListVersion);
      UWORD count;
      f.getUWord(count);
      init(count);
   }

   for(ArrayIndex i = 0; i < entries(); i++)
   {
      Province& prov = get(i);

      prov.readData(f);
   }

   return f.isOK();
#endif
}

Boolean ProvinceList::writeData(FileWriter& f) const
{
#ifdef EDITOR
   Province::writeNameTable();
#endif
   return ::writeData(*this, f, provinceChunkName);
#if 0
   FileChunkWriter fc(f, provinceChunkName);

   /*
    * Header
    */

   if(f.isAscii())
   {
      f.printf(fmt_sdn, entriesToken, (int) entries());
   }
   else
   {
      /*
       * Header
       */

      f.putUWord(DProvinceListVersion);
      f.putUWord(entries());
   }

   /*
    * Each Province
    */

   ArrayIter<Province> tl = *this;  // campaignData->getProvinces();

   while(++tl)
   {
      Province& prov = tl.current();

      if(!prov.writeData(f))
         return False;
   }

   return f.isOK();
#endif
}


/*========================================================
 * Towns
 */

TownList::TownList()
{
}

TownList::~TownList()
{
   Town::destroyNameTable();
}

// const UWORD DTownListVersion  = 0x0000;

Boolean TownList::readData(FileReader& f)
{
   return ::readData(*this, f, townChunkName);
#if 0
   FileChunkReader fr(f, townChunkName);
   if(!fr.isOK())
      return False;

   if(f.isAscii())
   {
      char* s = f.readLine();
      ASSERT(s != 0);
      if(s == 0)
         return False;

      StringParse line(s);

      char* tok = line.getToken();

      if(tok && stricmp(tok, entriesToken) == 0)
      {
      }
      else
         return False;

      return False;
   }
   else
   {
      UWORD version;
      f.getUWord(version);
      ASSERT(version <= DTownListVersion);
      UWORD count;
      f.getUWord(count);
      init(count);
   }

   for(ArrayIndex i = 0; i < entries(); i++)
   {
      Town& town = get(i);

      town.readData(f);
   }

   return f.isOK();
#endif
}

Boolean TownList::writeData(FileWriter& f) const
{
#ifdef EDITOR
   Town::writeNameTable();
#endif
   return ::writeData(*this, f, townChunkName);
#if 0
   FileChunkWriter fc(f, townChunkName);

   if(f.isAscii())
   {
      f.printf(fmt_sdn, entriesToken, (int) entries());
   }
   else
   {
      /*
       * Header
       */

      f.putUWord(DTownListVersion);
      f.putUWord(entries());
   }

   /*
    * Each town
    */

   ArrayIter<Town> tl = *this;   // campaignData->getTowns();

   while(++tl)
   {
      const Town& town = tl.current();

      if(!town.writeData(f))
         return False;
   }

   return f.isOK();
#endif
}




/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.2  2001/06/18 08:25:44  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
