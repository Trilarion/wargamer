/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef CAMPUNIT_H
#define CAMPUNIT_H

#ifndef __cplusplus
#error campunit.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1995, Steven Morle-Green, All Rights Reserved
 *----------------------------------------------------------------------
 *
 *	Campaign Unit Movement
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/06/13 08:52:37  greenius
 * Initial Import
 *
 * Revision 1.1  1996/02/28 09:33:33  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#include "cdatadll.h"
#include "measure.hpp"
#include "gamedefs.hpp"
#include "campord.hpp"
#include "routelst.hpp"
//#include "permbuf.hpp"


/*
 * Force Ratio
 * Descriptions are on what the enemy is like to us
 */

enum ForceRatio {
	FR_None,							// There is no enemy!
	FR_Inconsequential,			// We outweigh enemy by > 3:1
	FR_Smaller2,					// enemy < 1/2 of our SP
	FR_Smaller,						// enemy < 2/3 of our SP
	FR_Equal,						// enemy < 4/3 of our SP
	FR_Larger,						// enemy > 4/3 of our SP
	FR_Larger2,						// enemy > 2*SP
	FR_Outnumbered,				// Enemy > 3*SP.

	FR_HowMany
};

/*
 * CommandPosition is derived from this.
 * Placing it in this file, allows it to be seperate from armies.hpp
 * and more self-contained.
 */

class CampaignMovement {
#ifdef DEBUG
friend class CP_ModeUtil;
#endif
public:

	//	 Note... need more than 1 mode flag
	// For example, if unit goes into AvoidContact mode
	// then he should continue with his normal order unless
	// he gets within 8 miles of enemy.
	// Thus either the old mode must be stored, or the avoid contact
	// must be a seperate flag.

	/*
	 * If changing CP_Mode enumeration, then you must update:
	 *		campunit.cpp: ModeTable
	 *		campunit.cpp: moveCampaignUnit... mode may require special code
	 */

	enum CP_Mode
	{
		CPM_None,						// Unit is not doing anything
		CPM_Holding,               // Unit is holding
		CPM_Resting,               // Unit is resting on a march
		CPM_Rallying,              // Unit has RestRally orders
		CPM_Moving,						// Unit is moving
		CPM_Sieging,					// Unit is sieging a town
		CPM_Garrison,					// Unit is garrisoning
		CPM_Raiding,	  				// Pseudo Siege
		CPM_AcceptBattle,				// Unit is holding, and will accept battle with unit if it comes within 8 miles
		CPM_IntoBattle,				// Unit is moving to force battle
		CPM_Pursuing,              // Unit is pursuing beaten enemy

		CPM_AvoidContact,				// Sitting around, but will move if enemy < 8
		CPM_AvoidContactHold,		// Halted to avoid contact
		CPM_AvoidContactRetreat,	// Moving to avoid enemy

		CPM_AvoidContactFast,		// As above but 24 miles, and force march!
		CPM_AvoidContactFastHold,	// Halted out of enemy range
		CPM_AvoidContactFastRetreat,	// Moving out of enemy range

		CPM_StartingWithdraw,      // Withdraw under way
		CPM_Withdrawing,            // Withdraw under way
		CPM_InBattle,					// Unis is involved in a battle

		CPM_StartingRetreat,
		CPM_Retreating,
		CPM_AfterWinningBattle,	  // Rallying after a victorious battle

		CPM_InstallSupply,		// Installing a Supply Unit
		CPM_InstallFort,			// Installing or upgrading Fortification

		CPM_Surrendering,

		CPM_HowMany
	};

private:

	class AggressionValues {
		 Orders::Aggression::Value d_activeAggression;   // current active aggression
		 Orders::Posture::Type d_posture;                // current active posture
		 TimeTick d_lockedUntil;                         // can't change if locked
		 Boolean d_isLocked;	  									 // i.e after a losing battle
	  public:
		 AggressionValues() :
			d_activeAggression(Orders::Aggression::DefendSmall),
			d_posture(Orders::Posture::Defensive),
			d_lockedUntil(0),
			d_isLocked(False) {}

		 void lockUntil(TimeTick tilWhen) { d_lockedUntil = tilWhen; d_isLocked = True; }

		 Boolean isLocked() const { return d_isLocked; }
		 Boolean isLocked(TimeTick theTime)
		 {
			d_isLocked = (theTime < d_lockedUntil);
			return d_isLocked;
		 }

		 void set(Orders::Aggression::Value ag, Orders::Posture::Type p)
		 {
			if(!d_isLocked)
			{
			  d_activeAggression = ag;
			  d_posture = p;
			}
		 }

		 void unLock()
		 {
			d_isLocked = False;
			d_lockedUntil = 0;
		 }

		 Orders::Aggression::Value activeAggression() const { return d_activeAggression; }
		 Orders::Posture::Type posture() const { return d_posture; }

		 /*
		  * File interface
		  */

		 Boolean read(FileReader& f);
		 Boolean write(FileWriter& f) const;

		 unsigned short calculateCRC(void);
	};


#if !defined(EDITOR)
	CAMPDATA_DLL static const struct ModeTable {
		Boolean willDoBattle;			// Unit is actively accepting battles
		Boolean isAvoiding;				// Unit is trying to avoid contact
		Boolean shouldMove;				// Unit should move (i.e. not halted)
		Boolean outOfIt;					// Unit may not be part of a new battle
		Boolean retreating;				// Unit is currently retreating from enemy
		Boolean finishedBattle;       // Unit just finished a battle
		Boolean normalActivity;       // Unit is engaged in a normal activity
												//  Abnormal activity includes Retreating, Pursuing, etc.
		const char* modeName;			// Description of activity
	} CampaignMovement::movementModeTable[CPM_HowMany];
#endif

	CP_Mode d_mode;	 				        // What is it up to?
	AggressionValues d_aggressionValues;  // Current Posture, aggression
	RouteList d_currentRoute;             // Where is it going
	RouteList d_previousRoute;            // Where has it been lately
	RouteList d_supplyRoute;              // Supply Line!
	ForceRatio d_enemyRatio;

	/*
	 * Flags
	 */

	enum {
		ForceMarching    = 0x01,
		SiegeActive      = 0x02,
		BattleNear       = 0x04,
		HasPursuitTarget = 0x08,
		SoundGuns        = 0x10
	};

	UBYTE d_flags;

	ITown d_nextDest;					        // Where it is currently moving to
	ITown d_takingThisTown;       // are we in a town and taking it over?
	ITown d_retreatTown;          // if retreating or withdrawing, where to
	ICommandPosition d_nextTarget;	     // Temporary target

	Attribute d_activityCount;    // Activity count. Incremented when doing abnormal activity,
											// decremented when doing normal activity.
											// Abnormal activity is IntoBattle, AcceptBattle, Pursuing, etc.
	TimeTick d_activityTime;      // when was current activity started

//	TimeTick d_modeStartTime;		        // When was timer last updated?
//	TimeTick d_modeTime;				        // Timer used depending on mode

	UBYTE d_hoursResting;         // how many hours we've spent resting today

	void setFlags(UBYTE mask, Boolean f)
	{
	  if(f)
		 d_flags |= mask;
	  else
		 d_flags &= ~mask;
	}

public:
	CAMPDATA_DLL CampaignMovement();

	CP_Mode getMode() const { return d_mode; }
	CAMPDATA_DLL Boolean setMode(CP_Mode newMode);

#if !defined(EDITOR)
#ifdef DEBUG
	static const char* getModeDescription(CP_Mode mode) { return movementModeTable[mode].modeName; }
#endif
#endif

	/*
	 * Mode flags
	 */

	void hasPursuitTarget(Boolean f) { setFlags(HasPursuitTarget, f); }
	void setForceMarch(Boolean f) { setFlags(ForceMarching, f); }
	void setSiegeActive(Boolean f) { setFlags(SiegeActive, f); }
	void battleNear(Boolean f) { setFlags(BattleNear, f); }
	void marchingToSoundOfBattle(Boolean f) { setFlags(SoundGuns, f);}
	Boolean hasPursuitTarget() const { return (d_flags & HasPursuitTarget) != 0; }
	Boolean doingForceMarch() const { return (d_flags & ForceMarching) != 0; }
	Boolean isSiegeActive() const { return (d_flags & SiegeActive) != 0; }
	Boolean battleNear() const { return (d_flags & BattleNear) != 0; }
	Boolean marchingToSoundOfBattle() const { return (d_flags & SoundGuns) != 0; }

	/*
	 * static flags
	 */

	Boolean isHolding() const { return (d_mode == CPM_Holding); }
	Boolean isResting() const { return (d_mode == CPM_Resting || d_mode == CPM_Rallying); }
	Boolean isDigging() const { return False; } // needs to be removed
	Boolean isMoving() const { return (d_mode == CPM_Moving); }
	Boolean isSieging() const { return (d_mode == CPM_Sieging); }
	Boolean isGarrison() const { return (d_mode == CPM_Garrison); }
	Boolean isRaiding() const { return (d_mode == CPM_Raiding); }
	Boolean isMovingToBattle() const { return (d_mode == CPM_IntoBattle); }
	Boolean isInBattle() const { return (d_mode == CPM_InBattle); }
	Boolean isWithdrawing() const { return (d_mode == CPM_Withdrawing || d_mode == CPM_StartingWithdraw); }
	Boolean isRouting() const { return (d_mode == CPM_Retreating || d_mode == CPM_StartingRetreat); }
	Boolean isSurrendering() const { return (d_mode == CPM_Surrendering); }
	Boolean isPursuing() const { return (d_mode == CPM_Pursuing); }
	Boolean shouldCheckForAutoRest() const { return ( d_mode == CPM_Resting  ||
																	  d_mode == CPM_Moving   ||
																	  d_mode == CPM_Pursuing ||
																	  d_mode == CPM_AfterWinningBattle ); }
	Boolean canBeInBattle() const { return ( d_mode != CPM_Garrison &&
														  d_mode != CPM_InBattle &&
														  d_mode != CPM_Surrendering); }

#if !defined(EDITOR)
	CAMPDATA_DLL Boolean shouldMove() const;
	CAMPDATA_DLL Boolean willDoBattle() const;
	CAMPDATA_DLL Boolean isAvoiding() const;
	CAMPDATA_DLL Boolean shouldCheckEnemy() const;
	CAMPDATA_DLL Boolean isRetreating() const;
	CAMPDATA_DLL Boolean isAfterBattle() const;
	CAMPDATA_DLL Boolean isNormalActivity(Side s) const;
#endif // EDITOR


	/*
	 * Taking over this town
	 */

	void takingThisTown(ITown t) { d_takingThisTown = t; }
	void notTakingTown() { d_takingThisTown = NoTown; }
	ITown takingThisTown() const { return d_takingThisTown; }
	Boolean takingOverTown() const { return (d_takingThisTown != NoTown); }

	/*
	 * Next Destinations
	 */

	void setNextDest(ITown t) { d_nextDest = t; d_nextTarget = NoCommandPosition; }
	void setTarget(const ICommandPosition& cpi) { d_nextDest = NoTown; d_nextTarget = cpi; }
	void retreatTown(ITown t) { d_retreatTown = t; }
	void clearRetreatTown() { d_retreatTown = NoTown; }
	Boolean hasRetreatTown() const { return (d_retreatTown != NoTown); }
	ITown getNextDest() const { return d_nextDest; }
	ICommandPosition getTarget() const { return d_nextTarget; }
	ITown retreatTown() const { return d_retreatTown; }

	/*
	 * Current route
	 */

	RouteList& currentRouteList() { return d_currentRoute; }
	const RouteList& currentRouteList() const { return d_currentRoute; }
	void rewindCurrentRoute() { d_currentRoute.rewind(); }
	void resetCurrentRoute() { d_currentRoute.reset(); }
	void currentRouteNode(ITown town) { d_currentRoute.addNode(town); }
	void removeCurrentRouteNode(ITown town) { d_currentRoute.removeNode(town); }
	Boolean currentRouteIter() { return ++d_currentRoute; }
	ITown currentRouteNode() { return (d_currentRoute.entries() > 0) ? d_currentRoute.getCurrent()->d_town : NoTown; }

	/*
	 * Previous route
	 */

	void rewindPreviousRoute() { d_previousRoute.rewind(); }
	void previousRouteNode(ITown town) { d_previousRoute.addPreviousNode(town); }
	void removePreviousRouteNode(ITown town) { d_previousRoute.removeNode(town); }
	Boolean previousRouteIter() { return ++d_previousRoute; }
	ITown previousRouteNode() { return (d_previousRoute.entries() > 0) ? d_previousRoute.getCurrent()->d_town : NoTown; }

	/*
	 * Supply Line
	 */

	RouteList& supplyRouteList() { return d_supplyRoute; }
	const RouteList& supplyRouteList() const { return d_supplyRoute; }
	void rewindSupplyRoute() { d_supplyRoute.rewind(); }
	void resetSupplyRoute() { d_supplyRoute.reset(); }
	void supplyRouteNode(ITown town) { d_supplyRoute.addNode(town); }
	Boolean supplyRouteIter() { return ++d_supplyRoute; }
	ITown supplyRouteNode() { return (d_supplyRoute.entries() > 0) ? d_supplyRoute.getCurrent()->d_town : NoTown; }

	/*
	 * Abnormal activity counts
	 */

#if !defined(EDITOR)
	void startActivity(TimeTick theTime) { d_activityTime = theTime; }
	CAMPDATA_DLL void adjustActivityCount(TimeTick theTime, Side s);   // increment or deincrement activity counter
	void resetActivityCount() { d_activityCount = 0; }
	Attribute activityCount() const { return d_activityCount; }
#endif // !defined(EDITOR)

	/*
	 * Mode time
	 */

//	CAMPDATA_DLL void setModeTime(TimeTick theTime, TimeTick tilWhen);
//	CAMPDATA_DLL unsigned int modeTimeCompletion(TimeTick theTime, unsigned int range) const;
//	CAMPDATA_DLL Boolean modeTimeFinished(TimeTick theTime);

	/*
	 * Fatigue, Resting
	 */

	void resetHoursResting() { d_hoursResting = 0; }
	void incHoursResting() { d_hoursResting++; }
	UBYTE hoursResting() const { return d_hoursResting; }

	/*
	 * text descriptions
	 */

#if !defined(EDITOR)
	CAMPDATA_DLL const char* getActionDescription(OD_TYPE how, TimeTick theTime) const;
	// static void clearActionDescription() {} // needs to be removed
#endif // !defined(EDITOR)

	/*
	 * Aggression, Posture, etc.
	 */

	void setAggressionValues(Orders::Aggression::Value ag, Orders::Posture::Type p) { d_aggressionValues.set(ag, p); }
	void lockAggression(TimeTick tilWhen) { d_aggressionValues.lockUntil(tilWhen); }
	Boolean aggressionLocked() const { return d_aggressionValues.isLocked(); }
	Boolean aggressionLocked(TimeTick theTime) { return d_aggressionValues.isLocked(theTime); }
	Orders::Aggression::Value getActiveAggression() const { return d_aggressionValues.activeAggression(); }
	Orders::Posture::Type posture() const { return d_aggressionValues.posture(); }

	/*
	 * Force ratios for near enemies
	 */

	void clearEnemyRatio() { d_enemyRatio = FR_None; }
	void enemyRatio(ForceRatio ratio) { d_enemyRatio = ratio; }
	ForceRatio enemyRatio() const { return d_enemyRatio; }

	/*
	 * File interface
	 */

	CAMPDATA_DLL Boolean read(FileReader& f, OrderBattle& ob);
	CAMPDATA_DLL Boolean write(FileWriter& f, OrderBattle& ob) const;

	unsigned short calculateCRC(void);
};



#endif /* CAMPUNIT_H */

