/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
#ifndef IMapWindow_HPP
#define IMapWindow_HPP

/*
 * Interface to MapWindow
 */

class PixelPoint;
class DrawDIBDC;

class IMapWindow
{
   public:
      virtual void locationToPixel(const Location& lc, PixelPoint& pc) const = 0;
      virtual DrawDIBDC* dib() const = 0;
};


#endif

/*
 *----------------------------------------------------------------------
 * $Log$
 *----------------------------------------------------------------------
 */
