/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef GAMEOB_HPP
#define GAMEOB_HPP

#ifndef __cplusplus
#error gameob.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *	Game Objects
 *
 * Common Base Classes (Used to be in mapob)
 *
 *----------------------------------------------------------------------
 */

#include "measure.hpp"
#include "gamedefs.hpp"
// #include "StringPtr.hpp"
#include "name.hpp"			// NamedObject for backward compatibility

/*
 * Object with CampaignLocation
 */

class LocationObject {
	Location location;		// Where it is
public:
	void setLocation(const Location& l) { location = l; }
	const Location& getLocation() const { return location; }

	Boolean readData(FileReader& f) { return location.readData(f); }
	Boolean writeData(FileWriter& f) const { return location.writeData(f); }
};

#if 0	// Now in own file... gamesup\name.hpp

/*
 * Base class for objects with names
 */

class NamedItem : private CString {
	static const UWORD fileVersion;

public:
	NamedItem() : CString() { }
	NamedItem(NamedItem& ni);

	virtual ~NamedItem()	{ }

	void setName(char* s) {	set(s); }

	const char* getName() const
	{
#ifdef DEBUG
		return getNameNotNull("Unnamed");
#else
		return get();
#endif
	}

	const char* getNameNotNull(const char* def = 0) const 
	{
		return get() ? get() : (def ? def : "");
	}

	NamedItem& operator = (NamedItem& ni);

	/*
	 * File Interface
	 */

	Boolean readData(FileReader& f);
	Boolean writeData(FileWriter& f) const;
};
#endif	// Now in own file... gamesup\name.hpp

/*
 * Base class for objects with a side
 */

class SideObject {
	static const UWORD fileVersion;

	Side owner;						// Who controls it?
public:
	SideObject() { owner = SIDE_Neutral; }

	Side getSide() const { return owner; }
	void setSide(Side side) { owner = side; }

	/*
	 * File Interface
	 */

	Boolean readData(FileReader& f);
	Boolean writeData(FileWriter& f) const;
};

class StartSideObject {
	SideObject cSide;
public:
	Side getStartSide() const { return cSide.getSide(); }
	void setStartSide(Side side) { cSide.setSide(side); }
};

/*
 * Base class for objects with a Nation
 */

class NationObject {
	static const UWORD fileVersion;

	 Nationality nation;
  public:
	 NationObject() { nation = NATION_Neutral; }

	 void setNation(Nationality n) { nation = n; }
	 Nationality getNation() const { return nation; }

	/*
	 * File Interface
	 */
	 Boolean readData(FileReader& f);
	 Boolean writeData(FileWriter& f) const;
};


#endif /* GAMEOB_HPP */

