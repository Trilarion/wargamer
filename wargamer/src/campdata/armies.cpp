/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1995, Steven Morle-Green, All Rights Reserved
 *----------------------------------------------------------------------
 *
 * Generals (Leaders) and Units Implementation
 *
 *----------------------------------------------------------------------
 *
 * Note: This is very badly written code.  It makes use of a global
 *       variable (campaignData) and one of it's members (armies)
 *
 * Many of the functions currently peformed at the CommandPosition
 * or Leader level, ought to be moved up to the Armies:: or
 * CampaignData:: level.
 *
 *----------------------------------------------------------------------
 */


#include "stdinc.hpp"
#include "armies.hpp"
#include "scenario.hpp"
#include "savegame.hpp"
//#include "sync.hpp"
#ifdef DEBUG
#include "unitlog.hpp"
#endif

#if defined(CUSTOMIZE)
#include "morale.hpp"   // this is naughty because it makes it cyclic
#endif

#include "CRC.hpp"

Armies::Armies() :
   d_ob(0),
   d_cpList()
{
}

Armies::~Armies()
{
#ifdef DEBUG
   // This line is important because it removes cuLog's reference to a cpi
   cuLog.printf(NoCommandPosition, "Armies deleted");
#endif
   d_repoList.reset();     // Clear RepoList before deleting OB
   d_independentLeaders.reset();
   d_capturedLeaders.reset();
   removeUnitAndChildren(getTop());
   d_ob->reset();    // Clear topCommand, presidents, etc
   cleanup();
   delete d_ob;
}

/*
 * garbage collection
 */

void Armies::cleanup()
{
   d_cpList.cleanup();
   d_ob->cleanup();
}

/*
 * File Storage
 */

const UWORD Armies::fileVersion = 0x0000;

Boolean Armies::readData(FileReader& f)
{
// UWORD version;
// f >> version;
   delete d_ob;
   d_ob = new OrderBattle;
   ASSERT(d_ob);
   d_ob->readData(f);
   d_cpList.readData(f, d_ob);

   d_independentLeaders.read(f, *d_ob);
   d_capturedLeaders.read(f, *d_ob);
   d_repoList.read(f, *d_ob);

   initStartingValues((f.getMode() == SaveGame::Campaign));
   return f.isOK();
}

Boolean Armies::writeData(FileWriter& f) const
{
// f << fileVersion;

   d_ob->writeData(f);
   d_cpList.writeData(f, d_ob);
   d_independentLeaders.write(f, *d_ob);
   d_capturedLeaders.write(f, *d_ob);
   d_repoList.write(f, *d_ob);
   return f.isOK();
}

void Armies::initStartingValues(Boolean savedGame)
{
  // starting morale
  initStartingMorale(savedGame);

  // fatigue
  if(savedGame)
    initStartingFatigue();
}

/*-----------------------------------------------------------------
 * CommandPosition Attach/Detach Functions
 */

#if 0 // RefTODO:
void Armies::addCPReference(ICommandPosition cpi)
{
   d_cpList.addRef(cpi);
}

bool Armies::delCPReference(ICommandPosition cpi)
{
   bool flag = d_cpList.delRef(cpi);
   if(flag)
      d_ob->delCPReference(cpi);
   return flag;
}

void Armies::refCP(ICommandPosition& cpi, ICommandPosition newCP)
{
   if(cpi != NoCommandPosition)
      delCPReference(cpi);
   cpi = newCP;
   if(cpi != NoCommandPosition)
      addCPReference(cpi);
}
#endif

/*
 * Private Function to Create a new CommandPosition slot and return its ID
 */

ICommandPosition Armies::createCommand()
{
   RefGenericCP gcp = d_ob->createCommand();
   ASSERT(gcp != NoGenericCP);
   // CPIndex cpi = gcp->getSelf();
   // return d_cpList.addCP(cpi, gcp);
   return d_cpList.addCP(gcp);
}

/*
 * Public Function to create a new unit
 */

ICommandPosition Armies::createChildCP(const ICommandPosition& parent)
{
   ICommandPosition cpi = createCommand();
   ASSERT(cpi != NoCommandPosition);
   addAsChild(parent, cpi);
   return cpi;
}

/*
 * Set CP Links
 */

void Armies::setChild(const ICommandPosition& cp, const ICommandPosition& child)
{
   ASSERT(cp != NoCommandPosition);
   // d_ob->setChild(cp->generic(), generic(child));
   cp->generic()->setChild(generic(child));
}

void Armies::setSister(const ICommandPosition& cp, const ICommandPosition& sister)
{
   ASSERT(cp != NoCommandPosition);
   // d_ob->setSister(cp->generic(), generic(sister));
   cp->generic()->setSister(generic(sister));
}

void Armies::setParent(const ICommandPosition& cp, const ICommandPosition& parent)
{
   ASSERT(cp != NoCommandPosition);
   // d_ob->setParent(cp->generic(), generic(parent));
   cp->generic()->setParent(generic(parent));
}

/*
 * Unlink a CommandPosition from Order of Battle Tree
 */


void Armies::unlinkUnit(const ICommandPosition& cp)
{

#ifdef DEBUG
   cuLog.printf("Armies::unlinkUnit(%s)", cp->getName());
#endif

   ICommandPosition pi = getParent(cp);   // ->getParent();

   /*
    * Remove from tree
    */

   if(pi == NoCommandPosition)
   {
      // We are deleting God!

      ASSERT(d_ob->topCommand().value() == cp->generic());
      ASSERT(!cp->hasSister());

      // delCPReference(d_topCommand);
      d_ob->topCommand(NoGenericCP);   // CommandPosition);
   }
   else
   {
      // CommandPosition* cpp = getCommand(pi);
      ICommandPosition ci = getChild(pi);
      if(ci == cp)   // hunit)
      {
         // cpp->setChild(cp->getSister());
         setChild(pi, getSister(cp));
      }
      else
      {
         ICommandPosition si;
         do
         {
            si = ci;
            // cpp = getCommand(ci);
            ci = getSister(ci);

            ASSERT(ci != NoCommandPosition);    // Problem... not a child of its own parent!
         } while(ci != cp);

         ASSERT(ci == cp);
         ASSERT(ci != NoCommandPosition);

         if(ci == cp)
         {
            // cpp->setSister(cp->getSister());
            setSister(si, getSister(cp));
         }
      }

      // cp->setSister(NoCommandPosition);
      // cp->setParent(NoCommandPosition);
      setSister(cp, NoCommandPosition);
      setParent(cp, NoCommandPosition);
   }

   /*
    * If parent or any of its children no longer have any SP's clear them out
    */

// orgClearDestroyedUnits(pi);    This can't be done here!

   // d_ob->noteOBChange();
}



/*
 * Add CP as a child of another
 */

void Armies::addAsChild(const ICommandPosition& cpDest, const ICommandPosition& cpFrom)
{
   // CommandPosition* cpDest = getCommand(uDest);
   // CommandPosition* cpFrom = getCommand(uFrom);

#if defined(DEBUG)
   cuLog.printf("OrderBattle::addAsChild(%s,%s)",
      cpDest->getName(), cpFrom->getName());
#endif

   ASSERT(!cpFrom->hasParent());

   ICommandPosition ci = getChild(cpDest);
   if(ci == NoCommandPosition)
   {
      // cpDest->setChild(uFrom);
      setChild(cpDest, cpFrom);
   }
   else
   {
      ICommandPosition lastCP;
      // ICommandPosition cpi;
      do
      {
         lastCP = ci;
         // cpi = ci;   // getCommand(ci);
         ci = getSister(lastCP); // cpi->getSister();
      } while(ci != NoCommandPosition);

      // ASSERT(cpi != 0);
      ASSERT(lastCP != NoCommandPosition);

      // cpi->setSister(uFrom);
      setSister(lastCP, cpFrom);
   }

   // cpFrom->setParent(uDest);
   setParent(cpFrom, cpDest);

   // d_ob->noteOBChange();
}


#if defined(CUSTOMIZE)

/*
 * Create a new OrderOfBattle, with God and Presidents set up
 */

void Armies::removeAllUnits()
{
   startWrite();
#ifdef DEBUG
   cuLog.printf(NoCommandPosition, "Armies deleted");
#endif
   removeUnitAndChildren(getTop());
   d_ob->reset();    // Clear topCommand, presidents, etc
   cleanup();
   d_independentLeaders.reset();
   d_capturedLeaders.reset();
   d_ob->deleteAll();
   d_cpList.reset();

   ICommandPosition god = createCommand();
   ASSERT(god != NoCommandPosition);
   makeUnitName(god, Rank_God);
   god->setRank(Rank_God);
   d_ob->topCommand(generic(god));

   NationIter nIter = this;
   while(++nIter)
   {
      Side side = nIter.current();
      OBSideInfo* nation = d_ob->getNation(side);

      nation->clearRankCounts();

      ICommandPosition president = createChildCP(god);
      president->setSide(side);
      president->setRank(Rank_President);
      president->setNation(scenario->getGenericNation(side));
      // president->setParent(topCommand);
      makeUnitName(president, Rank_President);

      nation->setPresident(generic(president));
   }

   endWrite();
}

#endif   // CUSTOMIZE


/*
 * Move given unit (and all his children)
 * to the top level of the Order of Battle tree.
 */

void Armies::detachCommand(const ICommandPosition& cp)
{
   ASSERT(cp != NoCommandPosition);

   // CommandPosition* cp = getCommand(i);

#ifdef DEBUG
   cuLog.printf("Detaching %s", cp->getName());
#endif

   ASSERT(cp->getRank().isLower(Rank_President));

   ICommandPosition pi = getParent(cp);

   ASSERT(pi != NoCommandPosition);

   if(pi != NoCommandPosition)
   {
      /*
       * Remove from tree
       */

      // CommandPosition* cpp = getCommand(pi);

      if(pi->getRank().isLower(Rank_President))
      {

#ifdef DEBUG
         cuLog.printf("Detaching %s from %s", cp->getName(), pi->getName());
#endif

         unlinkUnit(cp);
         addAsChild(getPresident(cp->getSide()), cp);
//       orgClearDestroyedUnits(pi);
         averageCommandMorale(pi);
         averageCommandFatigue(pi);
      }
   }
}

#if 0
void Armies::addIndependentLeader(ILeader leader)
{
  ASSERT(leader != NoLeader);
  ASSERT(leader->getCommand() == NoCommandPosition);
//  IndependentLeader
}
#endif

void Armies::addCapturedLeader(ILeader leader)
{
  ASSERT(leader != NoLeader);

   if (leader->isSupremeLeader())
   {
      chooseNewSHQ(leader->side());
   }


  CapturedLeader* cl = new CapturedLeader(leader);
  d_capturedLeaders.append(cl);
}

void Armies::addRepoSP(ISP sp, ITown town)
{
  ASSERT(sp != NoStrengthPoint);
  ASSERT(town != NoTown);

  RepoSP* rp = new RepoSP(sp);
  rp->town(town);

  d_repoList.append(rp);
}

ConstICommandPosition Armies::getSHQ(Side side) const
{
  ASSERT(side != SIDE_Neutral);

  ConstILeader leader = getSupremeLeader(side);
  ASSERT(leader->isSupremeLeader());
  ASSERT(leader->getCommand() != NoCommandPosition);

  return leader->getCommand();
}

#ifdef CUSTOMIZE

/*
 * make sure units are of proper type.
 * i.e. XX's composed of
 *      1. Inf SP's and Arty SP's
 *      2. All Inf SP's
 *      3. Cav SP's and Mounted Arty SP's
 *      4. All Cav SP's
 *      5. All Artillery SP's
 *      6. A Max of 12 SP
 *      7. A Max of 10 Cav or Inf SP's
 *      8. Special Types (i.e. Siege-Trains, Engineers, count as Inf)
 *
 *   XXX and above
 *      1. Can be multiple types (i.e. it can hav Inf XX's, Cav XX's etc.)
 *      2. Only Artillery, or Special Types can be assigned (for now)
 *
 */

Boolean Armies::unitTypeSanityCheck(const ICommandPosition& cpi)
{
  if(cpi->getChild() == NoCommandPosition)
  {
    // check infantry units
    if(cpi->isInfantry())
    {
      int nInf = 0;
      int nArt = 0;
      int nCav = 0;

      SPIter iter(this, cpi);
      while(++iter)
      {
        const StrengthPoint* sp = iter.current();
        const UnitTypeItem& uti = getUnitType(sp->getUnitType());

        if( (uti.getBasicType() == BasicUnitType::Cavalry) )
          nCav++;
        else if(uti.getBasicType() == BasicUnitType::Artillery)
          nArt++;
        else
          nInf++;

      }

      Boolean result = ((nInf <= 12) && (nCav <= 2) && (nArt <= 2) && (nInf + nArt + nCav <= 14));
      return result;
    }

    // check cavalry units
    // it is cavalry if it is all mounted
    else if(cpi->isCavalry())
    {
      int nCav = 0;
      int nArt = 0;

      SPIter iter(this, cpi);
      while(++iter)
      {
        const StrengthPoint* sp = iter.current();
        const UnitTypeItem& uti = getUnitType(sp->getUnitType());

        if( (!uti.isMounted()) )
        {
          return False;
        }

        if(uti.getBasicType() == BasicUnitType::Artillery)
          nArt++;
        else
          nCav++;
      }

      Boolean result = ((nCav <= 10) && (nCav + nArt <= 12));
      return result;
    }

    // check Artillery units
    else if(cpi->isArtillery())
    {
      int nInf = 0;
      int nArt = 0;

      SPIter iter(this, cpi);
      while(++iter)
      {
        const StrengthPoint* sp = iter.current();
        const UnitTypeItem& uti = getUnitType(sp->getUnitType());

        if( (uti.getBasicType() == BasicUnitType::Cavalry) ||
            (uti.getBasicType() == BasicUnitType::Infantry) )
        {
          return False;
        }
        else if(uti.getBasicType() == BasicUnitType::Artillery)
          nArt++;
      }

      Boolean result = (nArt <= 12);
      return result;
    }

    return False;
  }

  /*
   * If we this is a XXX or above
   * Unit may only have Special, or Artillery SP's
   */

  else
  {
    int n = 0;

    StrengthPointIter iter(this, cpi);
    while(++iter)
    {
      const StrengthPoint* sp = iter.current();
      const UnitTypeItem& uti = getUnitType(sp->getUnitType());

      if(uti.getBasicType() == BasicUnitType::Infantry ||
         uti.getBasicType() == BasicUnitType::Cavalry)
      {
        return False;
      }

      n++;
    }

    return (n <= 12);
  }
}


#endif   // CUSTOMIZE


/*
 * Set unit types (i.e. Inf, Cav, or Art)
 */

void Armies::setUnitSpecialistFlags(const ICommandPosition& cpi)
{
// Unchecked update --------------------------------------------
   d_ob->setUnitSpecialistFlags(cpi->generic());
#if 0
  ASSERT(cpi != NoCommandPosition);
  cpi->clearTypeFlags();

  // CommandPosition* cp = getCommand(cpi);
//  ILeader leader = getUnitLeader(cpi);

  int nInf = 0;
  int nCav = 0;
  int nArt = 0;
  Boolean allGuard = True;
  Boolean allOldGuard = True;

  /*
   * If this is a XX, then check SP to get Type
   * otherwise use immediate subordinates
   */

  ICommandPosition cpChild = cpi->getChild();
  if(cpChild == NoCommandPosition)
  {
    SPIter iter(this, cpi);
    while(++iter)
    {
      const StrengthPoint* sp = iter.current();
      const UnitTypeItem& uti = getUnitType(sp->getUnitType());

      // count types
      if(uti.getBasicType() == BasicUnitType::Artillery)
        nArt++;
      else if(uti.getBasicType() == BasicUnitType::Cavalry)
        nCav++;
      else
        nInf++;

      if(allGuard && !uti.isGuard() && !uti.isOldGuard())
        allGuard = False;
    }

    /*
     * It is Infantry if XX has more inf SP's than Art Sp's
     * It is Cavalry if XX has more cav SP's than Art Sp's
     */

//  ASSERT(nInf == 0 || nCav == 0);
    if(nInf != 0)
    {
      if(nInf >= nArt)
        cpi->makeInfantryType(True);
      else
        cpi->makeArtilleryType(True);
    }
    else
    {
      if(nCav >= nArt)
        cpi->makeCavalryType(True);
      else
        cpi->makeArtilleryType(True);
    }

  }

  else
  {
    UnitIter uiter(this, cpChild);
    while(uiter.sister())
    {
      ICommandPosition cpi = uiter.current();
      if(cpi->isInfantry())
        nInf++;
      if(cpi->isArtillery())
        nArt++;
      if(cpi->isCavalry())
        nCav++;

      if(!cpi->isGuard())
        allGuard = False;
    }

    cpi->makeInfantryType((nInf > 0));
    cpi->makeCavalryType((nCav > 0));
    cpi->makeArtilleryType((nArt > 0));
  }

  cpi->makeGuard(allGuard);
//  cpi->makeOldGuard(allOldGuard);

  /*
   * set leader specialist flags
   * Set as Commanding Type if a specialist is
   * commanding its type or if a non-specialist is commanding inf or combined arms
   */

  Boolean commandingType = False;

  ILeader leader = cpi->getLeader();

  if(leader->isSpecialist())
  {
    commandingType = ( ( (leader->getSpecialistType() == Specialist::Cavalry) &&
                         (cpi->isCavalry()) )  ||
                       ( (leader->getSpecialistType() == Specialist::Artillery) &&
                         (cpi->isArtillery()) ) );
  }
  else
  {
    commandingType = (cpi->isInfantry() || cpi->isCombinedArms());
  }

  leader->isCommandingType(commandingType);

  // set leaders command rank above ceiling
  leader->ranksAboveCeiling(static_cast<UBYTE>(maximum(0, leader->getRankLevel().getRankEnum() - cpi->getRankEnum())));

#ifdef DEBUG
//  unitTypeSanityCheck(cpi);
#endif
#endif
}


/*
 * Set all specialist flags for all units in command
 * Warning! used recursively with setUnitSpecialistFlags
 */

void Armies::setCommandSpecialistFlags(const ICommandPosition& cpi)
{

  ICommandPosition cpChild = cpi->getChild();

  while(cpChild != NoCommandPosition)
  {
    if(cpChild->getChild() != NoCommandPosition)
      setCommandSpecialistFlags(cpChild);
    else
      setUnitSpecialistFlags(cpChild);

    cpChild = cpChild->getSister();
  }

  setUnitSpecialistFlags(cpi);


#if 0
  ASSERT(cpi != NoCommandPosition);

  UnitIter uiter(this, cpi);
  while(uiter.next())
  {
    ICommandPosition cpi = uiter.current();
    // CommandPosition* cp = uiter.currentCommand();

    setUnitSpecialistFlags(cpi);
  }
#endif
}

/*
 * Attach a NEW strength point
 * updating morale
 */

void Armies::attachStrengthPoint(const ICommandPosition& cpi, const ISP& spi)
{
   /*
    * get old morale values
    */

   int nSP = getUnitSPCount(cpi, true);
   long total = cpi->getMorale() * nSP;

   d_ob->attachStrengthPoint(cpi->generic(), spi);

   const UnitTypeItem& uti = getUnitType(spi->getUnitType());

   total += uti.getBaseMorale();
   nSP++;

   total = (total + nSP/2) / nSP;

   ASSERT(total <= Attribute_Range);

   cpi->setMorale(static_cast<Attribute>(total));

   ICommandPosition top = getTopParent(cpi);
   averageCommandMorale(top);
}

/*
 * Transfer Strength Point
 */

void Armies::transferStrengthPoint(const ICommandPosition& src, const ICommandPosition& dest, const ISP& spi)
{
   ASSERT(src != NoCommandPosition);
   ASSERT(dest != NoCommandPosition);
   ASSERT(spi != NoStrengthPoint);

   if(src == dest)
   {
#ifdef DEBUG
      const CommandPosition* cpSrc = getCommand(src);
      cuLog.printf("transferStrengthPoint: src==dest (%s)!", cpSrc->getName());
#endif
      return;
   }

#ifdef DEBUG
   StrengthPointItem* sp = getStrengthPoint(spi);

   cuLog.printf("Transferring SP %d from %s to %s",
         (int) d_ob->getIndex(spi),
         src->getName(),
         dest->getName());
#endif

   /*
    *  get info for readjusting fatigue/ Morale levels
    */

   Attribute srcFatigue = src->getFatigue();
   Attribute destFatigue = dest->getFatigue();

   Attribute srcMorale = src->getMorale();
   Attribute destMorale = dest->getMorale();

   SPCount destSPs = d_ob->getSPListCount(dest->generic());

#ifdef DEBUG
   cuLog.printf("Current fatigue for %s before tranfer is %d", dest->getName(), (int)destFatigue);
   cuLog.printf("Current morale for %s before tranfer is %d", dest->getName(), (int)destMorale);
#endif

   /*
    *  detach and attach
    */

   detachStrengthPoint(src, spi);
   attachStrengthPoint(dest, spi);

   /*
    * Clear out any destroyed units from src
    */

// Boolean wipedOut = orgClearDestroyedUnits(src);

   /*
    *  readjust fatigue and morale levels
    */

   ULONG totalFatigue = destSPs*destFatigue;
   totalFatigue += srcFatigue;
   dest->setFatigue(static_cast<Attribute>(totalFatigue/(destSPs+1)));

   ULONG totalMorale = destSPs*destMorale;
   totalMorale += srcMorale;
   dest->setMorale(static_cast<Attribute>(totalMorale/(destSPs+1)));

#ifdef DEBUG
   cuLog.printf("Current fatigue for %s after tranfer is %d", dest->getName(), (int)dest->getFatigue());
   cuLog.printf("Current morale for %s after tranfer is %d", dest->getName(), (int)dest->getMorale());
#endif

   /*
    *  Readjust fatigue of Top units.
    */

   ICommandPosition srcTop = getTopParent(src);

   ICommandPosition destTop = getTopParent(dest);

   if(srcTop != destTop)
   {
       averageCommandFatigue(srcTop);
       averageCommandMorale(srcTop);
   }

   averageCommandFatigue(destTop);
   averageCommandMorale(destTop);
}


/*
 * Free a CommandPosition
 *
 * With automatic reference counting, this function only needs
 * to clear its references.  This must be carefully done because
 * of the mutual references between parents and children, and
 * between leaders and units.
 */

void Armies::removeUnit(const ICommandPosition& cpi)
{
    cpi->isDead(true);
#if !defined(EDITOR)
    cpi->setTarget(NoCommandPosition);
#endif

   // CommandPosition* cp = getCommand(cpi);
   ASSERT(!cpi->hasChild());

   /*
    * Unlink the strength points
    */

   d_ob->deleteSPchain(cpi->generic());
   ASSERT(cpi->getSPEntry() == NoStrengthPoint);

   /*
    * Unlink the leader
    */

   ILeader iLeader = cpi->getLeader();
   if(iLeader != NoLeader)
   {
      // unassignLeader(cpi);
      detachLeader(iLeader);
   }

    /*
     * Remove Orders
     */

    cpi->clearOrders();


   /*
    * Unlink the CommandPosition
    */

   unlinkUnit(cpi);

   // deleteCommand(cpi);

}


/*
 * function removes unit and all children and SP's. It is recursive
 * Note that it doesn't have to physically delete the units or SPs
 * because the automatic reference counting will make this happen
 * automatically as long as all references are removed.
 */

void Armies::removeUnitAndChildren(const ICommandPosition& cpi)
{
   ASSERT(cpi != NoCommandPosition);
   // CommandPosition* cp = getCommand(cpi);

   Boolean childRemoved;

   do
   {
     childRemoved = False;
     ICommandPosition iChild = cpi->getChild();
     ICommandPosition iCurrent = cpi;

     while(iChild != NoCommandPosition)
     {
       iCurrent = iChild;
       // CommandPosition* cpChild = getCommand(iCurrent);

       iChild = getChild(iCurrent);

       if(iChild == NoCommandPosition)
       {
         childRemoved = True;
       }
     }

     removeUnit(iCurrent);

   } while(childRemoved);

}


Distance Armies::getColumnLength(ConstParamCP cpi) const
{
   // SPCount nsp = getUnitSPCount(cpi, True, True);
   SPCount nsp = getUnitSPCount(cpi, True);
   return YardsToDistance((nsp * YardsPerMile) / 10);
}


/*
 *  Sets Morale attribute for a unit and all its children(if any)
 *  from average base morale. Called at beginning of game or
 *  when a new unit appears;
 */

void Armies::setBaseMorale(const ICommandPosition& cpi)
{
  ASSERT(cpi != NoCommandPosition);

  UnitIter unitIter(this, cpi);
  while(unitIter.next())
  {
    // CommandPosition* cp = unitIter.currentCommand();
    ICommandPosition unit = unitIter.current();
    unit->setMorale(getBaseMorale(unit));
  }
}


/*
 *  Initialize morale attribute for all units
 */

void Armies::initStartingMorale(Boolean savedGame)
{
#if defined(CUSTOMIZE)
   if(savedGame)  // this flag actually means it is NOT a saved game
   {
      // Calculate base morales for any unit that has zero morale

      MoraleUtility::setDefaultMorales(this, getTop());
   }
#endif

   NationIter nationIter = this;

   while(++nationIter)
   {
      Side side = nationIter.current();

      UnitIter unitIter(this, getFirstUnit(side));
      while(unitIter.sister())
      {
         ICommandPosition cpi = unitIter.current();

         if(savedGame)
           averageCommandMorale(cpi);
         else
           setBaseMorale(cpi);
      }
   }
}

/*
 *  Initialize fatigue attribute for all units
 */

void Armies::initStartingFatigue()
{
   NationIter nationIter = this;

   while(++nationIter)
   {
      Side side = nationIter.current();

      UnitIter unitIter(this, getFirstUnit(side));
      while(unitIter.sister())
      {
         ICommandPosition cpi = unitIter.current();
         averageCommandFatigue(cpi);
      }
   }
}

/*
 * Returns average morale of units subordinates. Should be called after battle
 * and whenever Units attach or detach. For now it calculates
 * only units that have SP's as I believe that is what Ben intended.
 */

Attribute Armies::getAverageMorale(ConstParamCP cpi) const
{
  ASSERT(cpi != NoCommandPosition);

  ULONG moraleTotal = 0;
  Attribute morale = 0;
  SPCount nUnits = 0;

  ConstUnitIter unitIter(this, cpi);
  while(unitIter.next())
  {
    ConstICommandPosition cp = unitIter.current();
    // SPCount nSP = d_ob->getSPListCount(unitIter.current(), True);
    SPCount nSP = d_ob->getSPListCount(unitIter.current()->generic());
    moraleTotal += (cp->getMorale() * nSP);
    nUnits += nSP;
  }

  if(nUnits > 0)
  {
    morale = (Attribute)(moraleTotal/nUnits);
  }

  /*
   * Make sure morale is not higher than average base morale.
   * If so set morale to average base morale
   */

  Attribute baseMorale = getBaseMorale(cpi);

  if(morale > baseMorale)
    morale = baseMorale;

  return morale;
}

/*
 * Sets average morale of an organization
 */

void Armies::averageCommandMorale(const ICommandPosition& cpi)
{
  ASSERT(cpi != NoCommandPosition);


  /*
   * Now re-average morale for each command.
   */

  UnitIter unitIter(this, cpi);
  while(unitIter.next())
  {
    ICommandPosition cp = unitIter.current();

    /*
     *  If unit has no children then no reason to average
     */

    if(cp->hasChild())
    {
      cp->setMorale(getAverageMorale(unitIter.current()));
    }
  }
}


/*
 * Apply fatigue loss\recovery to unit
 */

void Armies::applyFatigueToUnit(const ICommandPosition& cpi, int recover)
{
  ASSERT(cpi != NoCommandPosition);

  // CommandPosition* cp = getCommand(cpi);

  /*
   *  Use int in case value goes out of Attribute range
   */

  int fatigue = cpi->getFatigue();
  fatigue += recover;
  fatigue = maximum(0, minimum(255, fatigue));

  cpi->setFatigue(Attribute(fatigue));
}


/*
 * Apply fatigue loss\recevery to unit and its subordinates
 */

void Armies::applyFatigue(const ICommandPosition& cpi, int recover)
{
  ASSERT(cpi != NoCommandPosition);

  UnitIter unitIter(this, cpi);

  while(unitIter.next())
  {
    /*
     *  Only units with SP's get level adjusted.
     *  higher units get fatigue level from average of subordinate units
     */

    ICommandPosition cp = unitIter.current();
//  if(cp->getSPEntry() != NoStrengthPoint)
    {
      applyFatigueToUnit(cp, recover);
    }
  }

  /*
   * Now go through and reaverage fatigue for all units in command
   */

  averageCommandFatigue(cpi);
}


void Armies::averageCommandFatigue(const ICommandPosition& cpi)
{
  ASSERT(cpi != NoCommandPosition);

  UnitIter unitIter(this, cpi);

  while(unitIter.next())
  {
    ICommandPosition cp = unitIter.current();

    /*
     *  If unit has no children then no reason to average
     */

    if(cp->hasChild())
    {
      Attribute fatigue = averageFatigue(cp);
      cp->setFatigue(fatigue);
    }
  }
}


Attribute Armies::averageFatigue(const ICommandPosition& cpi) const
{
  ASSERT(cpi != NoCommandPosition);

  ULONG fatigueTotal = 0;
  Attribute fatigue = 0;
  SPCount nUnits = 0;

  ConstUnitIter unitIter(this, cpi);
  while(unitIter.next())
  {
    ConstICommandPosition cp = unitIter.current();
    // SPCount nSP = d_ob->getSPListCount(unitIter.current(), True);
    SPCount nSP = d_ob->getSPListCount(cp->generic());
    fatigueTotal += (cp->getFatigue() * nSP);
    nUnits += nSP;
  }

  if(nUnits > 0)
  {
    fatigue = (Attribute)(fatigueTotal/nUnits);
  }
  else
    fatigue = cpi->getFatigue();

  return fatigue;
}

/*
 * Return number of SP of given class in organiztion
 */

SPCount Armies::getNumClassSP(ConstParamCP cpi, AttritionClass::value c) const
{
  ASSERT(c < AttritionClass::HowMany);
  ASSERT(cpi != NoCommandPosition);

  SPCount count = 0;

  ConstSPIter iter(this, cpi);
  while(++iter)
  {
    const StrengthPointItem* sp = iter.current();
    const UnitTypeItem& uti = getUnitType(sp->getUnitType());

    if(uti.getAttritionClass() == c)
    {
      count++;
    }
  }

  return count;
}

/*
 * returns true if unit is composed entirely of mounted troops
 */

Boolean Armies::isForceMounted(ConstParamCP cpi) const
{
  ASSERT(cpi != NoCommandPosition);

#ifdef DEBUG
  const CommandPosition* cp = getCommand(cpi);
  ASSERT(cp->isRealUnit() != False);
#endif

  ConstSPIter iter(this, cpi);

  while(++iter)
  {
    ConstISP sp = iter.current();
    // const StrengthPointItem* sp = iter.current();
    const UnitTypeItem& uti = getUnitType(sp->getUnitType());

    if(!uti.isMounted())
      return False;
  }

  return True;
}

/*
 * returns lowest modifier to speed in force
 * Actual modifier is function/100
 */

UWORD Armies::getForceSpeedModifer(ConstParamCP cpi) const
{
  ASSERT(cpi != NoCommandPosition);

#ifdef DEBUG
  const CommandPosition* cp = getCommand(cpi);
  ASSERT(cp->isRealUnit() != False);
#endif

  UWORD lowest = 125;

  ConstSPIter iter(this, cpi);
  while(++iter)
  {
    // const StrengthPointItem* sp = iter.current();
    ConstISP sp = iter.current();
    const UnitTypeItem& uti = getUnitType(sp->getUnitType());

    UWORD modifier = uti.getStrategicSpeedModifier();
    if(modifier < lowest)
      lowest = modifier;

    if(lowest == 100)
      break;
  }

  return lowest;

}

#if 0
Boolean Armies::clearDestroyedUnits(const ICommandPosition& cpi)
{
  Boolean wipedOut = orgClearDestroyedUnits(cpi);

  /*
   * If we're not completely out of it, recalculate
   */

  if(!wipedOut)
  {
    averageCommandMorale(cpi);
    averageCommandFatigue(cpi);
  }

  return wipedOut;
}

/*
 *  Units with SPCount of 0 are destroyed
 *  returns true if the whole organization is destroyed
 *
 * This needs rewriting to work with the new reference counting
 * Suggested method will be:
 *    do
 *    {
 *       iterate through each unit
 *          IF unit has no SPs and no children
 *             detach it (which will also delete it)
 *    } while units were destroyed
 *
 * Alternatively detach it can walk up through parent detaching each
 * parent if it is empty.
 * Need to be careful with the iterators.
 */

Boolean Armies::orgClearDestroyedUnits(const ICommandPosition& cpi)
{
  ASSERT(cpi != 0);

#ifdef DEBUG
  cuLog.printf("====== Checking for Destroyed Units =======");
#endif

  Boolean unitDestroyed;

  do
  {
    unitDestroyed = False;

    UnitIter iter(this, cpi);

    while(iter.next())
    {
      ICommandPosition curCPI = iter.current();

      SPCount spCount = getUnitSPCount(curCPI, True);

      if(spCount <= 0)
      {

#ifdef DEBUG
        CommandPosition* cp = getCommand(curCPI);
        cuLog.printf("%s(index %d) and all its units have been destroyed", cp->getNameNotNull(), (int)d_ob->getIndex(curCPI->generic()));
#endif

        unitDestroyed = True;

        removeUnitAndChildren(curCPI, True, curCPI->trapped());

        if(curCPI == cpi)
        {
#ifdef DEBUG
          cuLog.printf("Whole organization wiped out");
#endif
          return True;
        }

        break;
      }
    }

  } while(unitDestroyed);

#ifdef DEBUG
  cuLog.printf("====== End Destroyed Unit Check ========");
#endif

  return False;

}
#endif

SPCount Armies::getNumLightCavalry(ConstParamCP cpi) const
{
  ASSERT(cpi != NoCommandPosition);

  SPCount count = 0;

  ConstSPIter iter(this, cpi);
  while(++iter)
  {
    // const StrengthPointItem* sp = iter.current();
    ConstISP sp = iter.current();
    const UnitTypeItem& uti = getUnitType(sp->getUnitType());

    if(uti.isLightCavalry())
      count++;
  }

  return count;
}


/*
 * Set Supply level for unit and all its subordinates
 */

void Armies::setCommandSupply(const ICommandPosition& cpi, Attribute level)
{
  ASSERT(cpi != NoCommandPosition);

  UnitIter iter(this, cpi);
  while(iter.next())
  {
    ICommandPosition cp = iter.current();
    cp->setSupply(level);
  }
}



#if !defined(EDITOR)

Boolean Armies::doDiggingFieldWorks(const ICommandPosition& cpi)
{
#ifdef OB_TODO    // ToDo: This should probably be a higher level function
   ASSERT(cpi != NoCommandPosition);

   cpi->incFieldWorks();

   if(cpi->getFieldWorks() >= CommandPosition::MaxFieldWorks)
   {
//    cp->setMode(CampaignMovement::CPM_None);
      CP_ModeUtil::setMode(d_campData, cpi, CampaignMovement::CPM_None);

      /*
       *  Remove engineer unit
       */

      ICommandPosition engCP;
      ISP engSP;

      Boolean result = findSpecialType(cpi, engCP, engSP, SpecialUnitType::Engineer);
      ASSERT(result == True);

      if(result)
      {
          detachStrengthPoint(engCP, engSP);
#if 0 // Do nothing, simple act of removing all references will kill it
          deleteStrengthPoint(engSP);
#endif
      }

      return False;
   }
   else
      return True;
#else
   return False;
#endif   // OB_TODO
}
#endif






unsigned short
Armies::calculateChecksum(void) {

   unsigned short crc = 0;

   /*
   Do all CPs
   */
   CPIter cpiter(this, true);
   cpiter.reset();
   cpiter.next();

   do {

      ICommandPosition cpi = cpiter.current();
      crc = addShortCRC(crc, cpi->calculateChecksum());

   } while(cpiter.next());

   /*
   Do all IndependentLeaders
   */
/*
   if(d_independentLeaders.first()) {

      do {
   
         IndependentLeader * leader = d_independentLeaders.current();
         crc = addShortCRC(crc, leader->calculatechecksum());
         
      } while(d_independentLeaders.next());
   }

*/
   return crc;
}



/*===============================================================
 * UnitIter
 */

bool DefaultUnitIterValidator::isValid(const ConstICommandPosition& cp)
{
    return d_all || (cp->isActive() && !cp->isDead());
}


Boolean UnitIter::sister()
{
   Boolean flag;

   do
   {
      flag = d_iter.sister();
   } while(flag && !isValid(d_iter.current()->campaignInfo()));
   // } while(!d_all && flag && !d_iter.current()->campaignInfo()->isActive()); //!d_army->isUnitActive(d_iter.current()->campaignInfo()));

   return flag;
}

Boolean UnitIter::next()
{
   Boolean flag;

   do
   {
      flag = d_iter.next();
   } while(flag && !isValid(d_iter.current()->campaignInfo()));

   return flag;
}


Boolean ConstUnitIter::sister()
{
   Boolean flag;

   do
   {
      flag = d_iter.sister();
   } while(flag && !isValid(d_iter.current()->campaignInfo()));

   return flag;
}

Boolean ConstUnitIter::next(bool intoChildren)
{
   Boolean flag;

   do
   {
      flag = d_iter.next(intoChildren);
      intoChildren = true;
   } while(flag && !isValid(d_iter.current()->campaignInfo()));

   return flag;
}


Boolean CPIter::sister()
{
   Boolean flag;
   do
   {
      flag = d_iter.sister();
   } while(flag && !isValid(d_iter.current()->campaignInfo()));

   return flag;
}

Boolean CPIter::next()
{
   Boolean flag;
   do
   {
      flag = d_iter.next();
   } while(flag && !isValid(d_iter.current()->campaignInfo()));

   return flag;
}

Boolean ConstCPIter::sister()
{
   Boolean flag;
   do
   {
      flag = d_iter.sister();
   } while(flag && !isValid(d_iter.current()->campaignInfo()));

   return flag;
}

Boolean ConstCPIter::next()
{
   Boolean flag;
   do
   {
      flag = d_iter.next();
   } while(flag && !isValid(d_iter.current()->campaignInfo()));

   return flag;
}




/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.3  2001/07/02 15:23:16  greenius
 * Battle, Campaign, Combined game and Campaign Editor now compile and run... mostly
 *
 * Revision 1.2  2001/06/18 08:25:44  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
