/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
#include "stdinc.hpp"
#include "campside.hpp"
#include "filebase.hpp"
#include "filecnk.hpp"
//#include "wldfile.hpp"
#include "savegame.hpp"


//================== SideLoss read/write ==============================

UWORD SideLoss::s_fileVersion = 0x0000;

Boolean SideLoss::read(FileReader& f)
{
   if(f.isAscii())
   {
      // ToDo
   }
   else
   {
      UWORD version;
      f.getUWord(version);
      ASSERT(version <= s_fileVersion);

      f.getUWord(d_infantryLosses);
      f.getUWord(d_cavalryLosses);
      f.getUWord(d_artilleryLosses);
      f.getUWord(d_otherLosses);
   }


  return f.isOK();
}

Boolean SideLoss::write(FileWriter& f) const
{
   if(f.isAscii())
   {
      // ToDo
   }
   else
   {
      f.putUWord(s_fileVersion);

      f.putUWord(d_infantryLosses);
      f.putUWord(d_cavalryLosses);
      f.putUWord(d_artilleryLosses);
      f.putUWord(d_otherLosses);
   }

  return f.isOK();
}

/*------------------------------------------------------------------
 * CampSide implementation
 */

UWORD CampSide::s_fileVersion = 0x0000;

Boolean CampSide::read(FileReader& f)
{
   if(f.isAscii())
   {
      // ToDo
   }
   else
   {
      UWORD version;
      f >> version;
      ASSERT(version <= s_fileVersion);

      d_sideLoss.read(f);
      f >> d_vp;
      f >> d_nDepots;
      f >> d_nDepotsUC;
   }


  return f.isOK();
}

Boolean CampSide::write(FileWriter& f) const
{
   if(f.isAscii())
   {
      // ToDo
   }
   else
   {
      f << s_fileVersion;

      d_sideLoss.write(f);
      f << d_vp;
      f << d_nDepots;
      f << d_nDepotsUC;
   }

  return f.isOK();
}

/*------------------------------------------------------------------
 * Campaign Sides implementation
 */

UWORD CampaignSides::s_fileVersion = 0x0000;

Boolean CampaignSides::read(FileReader& f)
{
   if(f.isAscii())
   {
      // ToDo
   }
   else
   {
      if(f.getMode() == SaveGame::SaveGame)
      {
         FileChunkReader fr(f, getChunkName());
         if(!fr.isOK())
            return False;

         UWORD version;
         f.getUWord(version);
         ASSERT(version <= s_fileVersion);

         for(Side s = 0; s < NumberSides; s++)
         {
            if(!d_sideData[s].read(f))
              return False;
         }
      }
   }

   return f.isOK();
}

Boolean CampaignSides::write(FileWriter& f) const
{
   if(f.isAscii())
   {
      // ToDo
   }
   else
   {
      if(f.getMode() == SaveGame::SaveGame)
      {
         FileChunkWriter fc(f, getChunkName());

         f.putUWord(s_fileVersion);

         for(Side s = 0; s < NumberSides; s++)
         {
            if(!d_sideData[s].write(f))
            return False;
         }
      }
   }
  return f.isOK();
}

const char* CampaignSides::getChunkName() const
{
  return "CampaignSidesData";
}

void CampaignSides::addSideLosses(Side s, SPCount loss, BasicUnitType::value type)
{
  ASSERT(s < NumberSides);

  switch(type)
  {
    case BasicUnitType::Infantry:
      d_sideData[s].d_sideLoss.d_infantryLosses += loss;
      break;

    case BasicUnitType::Cavalry:
      d_sideData[s].d_sideLoss.d_cavalryLosses += loss;
      break;

    case BasicUnitType::Artillery:
      d_sideData[s].d_sideLoss.d_artilleryLosses += loss;
      break;

    case BasicUnitType::Special:
      d_sideData[s].d_sideLoss.d_otherLosses += loss;
      break;

#ifdef DEBUG
    default:
      FORCEASSERT("Improper unittype in CampaignSides::addSideLosses()");
#endif
  }
}

SPCount CampaignSides::getSideLosses(Side s) const
{
  ASSERT(s < NumberSides);

  return d_sideData[s].d_sideLoss.getTotal();
}

SPCount CampaignSides::getTypeLosses(Side s, BasicUnitType::value type) const
{
  ASSERT(s < NumberSides);

  switch(type)
  {
    case BasicUnitType::Infantry:
      return d_sideData[s].d_sideLoss.d_infantryLosses;

    case BasicUnitType::Cavalry:
      return d_sideData[s].d_sideLoss.d_cavalryLosses;

    case BasicUnitType::Artillery:
      return d_sideData[s].d_sideLoss.d_artilleryLosses;

    case BasicUnitType::Special:
      return d_sideData[s].d_sideLoss.d_otherLosses;

#ifdef DEBUG
    default:
      FORCEASSERT("Improper unittype in TotalLosses::getTypeLosses()");
#endif
  }

  return 0;
}

/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.2  2001/06/18 08:25:44  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
