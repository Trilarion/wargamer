/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
 * SupplyLine calculator
 */

#include "stdinc.hpp"
#include "supplyLine.hpp"
#include "campdint.hpp"
#include "town.hpp"
#include <stack>

#ifdef LOG_SUPPLYLINE
#include "clog.hpp"
LogFileFlush sLog("supplyLine.log");
#endif

/*
 * Private Functions
 */

class SideValidator
{
   public:
      SideValidator(Side s) : d_side(s) { }
      ~SideValidator() { }

      bool isValid(const Town& t)
      {
         if(d_side == SIDE_Neutral)
            return true;
         else
            return d_side == t.getSide();
      }

   private:
      Side d_side;
};

/*==============================================================================
 * Public Functions
 */

/*
 * Constructor
 */

CampaignSupplyLine::CampaignSupplyLine() :
#ifdef DEBUG
   d_campData(0),
#endif
   d_nodes(0)
{
}

/*
 * Destructor
 */

CampaignSupplyLine::~CampaignSupplyLine()
{
   delete[] d_nodes;
#ifdef DEBUG
   d_campData = 0;
#endif
}

/*
 * Calculator
 *
 * Side: IF Neutral then all supply depots/sources can be used
 *       ELSE only count source/depot of given side
 * construction: depots under construction are counted as being completed
 */

#ifdef _MSC_VER  // MSVC can't use local classes as template arguments!
   struct StackItem
   {
      ITown d_itown;
      Distance d_distance;
      Distance d_maxDistance;

      StackItem() : d_itown(NoTown), d_distance(0), d_maxDistance(0) { }
      StackItem(ITown t, Distance d, Distance dMax) : d_itown(t), d_distance(d), d_maxDistance(dMax) { }

      bool operator<(const StackItem& item) const { return d_itown < item.d_itown; }
      bool operator==(const StackItem& item) const { return d_itown == item.d_itown; }
   };

   typedef std::stack<StackItem, std::vector<StackItem> > TownStack;
#endif

void CampaignSupplyLine::calculate(const CampaignData* campData, Side side, bool includeConstruction)
{
//   ASSERT(d_nodes == 0);
   ASSERT(campData != 0);

#ifdef DEBUG
   d_campData = campData;

   int iterations = 0;
   int stackSize = 0;
#endif

   const TownList& townList = campData->getTowns();
   const ConnectionList& cl = campData->getConnections();

   /*
    * Create Nodelist
    */

   if(d_nodes == 0)
      d_nodes = new Node[townList.entries()];

   SideValidator validSide(side);

   /*
    * Clear NodeList
    */

   {
      for(int i = 0; i < townList.entries(); ++i)
         d_nodes[i].d_supplyLevel = 0;
   }

   /*
    * Create stack
    */
#ifndef _MSC_VER  // MSVC can't use local classes as template arguments!
   struct StackItem
   {
      ITown d_itown;
      Distance d_distance;
      Distance d_maxDistance;

      StackItem() : d_itown(NoTown), d_distance(0), d_maxDistance(0) { }
      StackItem(ITown t, Distance d, Distance dMax) : d_itown(t), d_distance(d), d_maxDistance(dMax) { }

      bool operator<(const StackItem& item) const { return d_itown < item.d_itown; }
      bool operator==(const StackItem& item) const { return d_itown == item.d_itown; }
   };

   typedef std::stack<StackItem, std::vector<StackItem> > TownStack;
#endif
   TownStack townStack;

   {
      for (int i = 0; i < townList.entries(); ++i)
      {
         if (townList[i].getIsSupplySource())
         {
            if(validSide.isValid(townList[i]))
            {
               townStack.push(StackItem(i, 0, Town::getSupplyMaxRange(townList[i].getSize())));
            }
         }
      }
   }

   while (!townStack.empty())
   {
#ifdef DEBUG
      ++iterations;
      stackSize = maximum<int>(townStack.size(), stackSize);
#endif


      StackItem item = townStack.top();
      townStack.pop();

      const Town& town = townList[item.d_itown];

      Level level = 0;
      if(item.d_distance < item.d_maxDistance)
         level = MulDiv(item.d_maxDistance - item.d_distance, FullSupply, item.d_maxDistance);

      // Set level

      if (d_nodes[item.d_itown].d_supplyLevel < level)
      {
         d_nodes[item.d_itown].d_supplyLevel = level;

         // Follow connections and push onto stack

         for(int i = 0; i < MaxConnections; i++)
         {
            IConnection ci = town.getConnection(i);
            if(ci == NoConnection)
               break;

            const Connection& con = cl[ci];
            ITown nextTown = con.getOtherTown(item.d_itown);

            const Town& nTown = townList[nextTown];
            if(validSide.isValid(nTown) || validSide.isValid(town))
            {
               Distance newDistance = item.d_distance + con.distance;
               Level newLevel = 0;
               if(newDistance < item.d_maxDistance)
                  newLevel = MulDiv(item.d_maxDistance - newDistance, FullSupply, item.d_maxDistance);

               if(d_nodes[nextTown].d_supplyLevel < newLevel)
               {
                  Distance range = item.d_maxDistance;

                  if(nTown.getIsDepot() ||
                     (includeConstruction && nTown.isBuildingDepot()))
                  {
                     newDistance = 0;
                     range =  Town::getSupplyMaxRange(nTown.getSize());
                  }

                  townStack.push(StackItem(nextTown, newDistance, range));
               }
            }
         }
      }
   }

#ifdef LOG_SUPPLYLINE
   {
      sLog.printf("");
      sLog.printf("SupplyLine Results");
      sLog.printf("------------------");
      sLog.printf("Iterations=%d, stackUsage=%d, nTowns=%d",
               (int)iterations, (int)stackSize, (int)townList.entries());
      for (int i = 0; i < townList.entries(); ++i)
      {
         sLog.printf("%30s: %d%%",
               (const char*) townList[i].getName(),
               (int) MulDiv(d_nodes[i].d_supplyLevel, 100, FullSupply));
      }
   }



#endif

}

/*
 * results:
 */

CampaignSupplyLine::Level CampaignSupplyLine::operator[](ITown itown) const
{
   ASSERT(d_nodes != 0);
   ASSERT(itown < d_campData->getTowns().entries());

   if(d_nodes == 0)
      return 0;
   else
      return d_nodes[itown].d_supplyLevel;
}

/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.3  2001/07/02 15:23:17  greenius
 * Battle, Campaign, Combined game and Campaign Editor now compile and run... mostly
 *
 * Revision 1.2  2001/06/18 08:25:44  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
