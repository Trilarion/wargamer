/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef ARMYUTIL_HPP
#define ARMYUTIL_HPP

#include "cdatadll.h"
#include "cpidef.hpp"
#include "gamedefs.hpp"
#include "unittype.hpp"
#include "rank.hpp"

class CampaignData;
class CampaignPosition;
class BuildItem;
class UnitTypeItem;
class CampaignLogicOwner;

class CAMPDATA_DLL CampaignArmy_Util {
   public:
   static Boolean clearDestroyedUnits(CampaignData* campData, const ICommandPosition& cpi, bool useEffective = False);
   static void destroyedUnitLeaderProc(CampaignData* campData, ICommandPosition cpi);
   static Boolean isUnitOrderable(const ConstICommandPosition& cpi);
   static Boolean isUnitVisible(const CampaignData* campData, const ConstICommandPosition& cp);
   static const CampaignPosition& getUnitDisplayPosition(const ConstICommandPosition& cp);
   static void transferLeader(CampaignData* campData, const ILeader& general,
      const ICommandPosition& dest, Boolean autoProm, Boolean makeIndependent = True);
   static void makeIndependentLeader(CampaignData* campData, const ILeader& leader, const CampaignPosition& pos, Boolean active = True);
   static void eliminateNationUnits(CampaignData* campdata, Nationality n, Side whichSide);
   static void attachUnits(CampaignData* campData, const ICommandPosition& unit1, const ICommandPosition& unit2);
   static void killLeader(CampaignData* campData, const ILeader& leader);
   static void captureLeader(CampaignData* campData, const ILeader& leader);
   //  static ILeader whoShouldCommand(const CampaignData* campData, const ILeader& leader1, const ILeader& leader2);
   static ULONG totalManpower(const CampaignData* campData, ConstICommandPosition& cpi);
   static ULONG manpowerByType(const CampaignData* campData, ConstParamCP cpi,
      const BasicUnitType::value& type);
   static ULONG totalGuns(const CampaignData* campData, ConstParamCP cpi);
   static Boolean bridgeTrainAtTown(const CampaignData* campData, ITown town, Side siegingSide);
   static void loseEquipment(CampaignData* campData, const ICommandPosition& cpi);
   static void buildStrengthPoint(CampaignLogicOwner* campGame, ITown itown, const BuildItem& b);

   //--- Modified SWG: 20Jul99:
   //          Default to 1,1 for SPCounts
   //          Parameters changed to const
//    static Boolean canAttachTo(const CampaignData* campData, ConstParamCP cpi, const UnitTypeItem& ui)
//    {
//       return canAttachTo(campData, cpi, ui, 0, 0);
//    }

   static Boolean canAttachTo(const CampaignData* campData,
                              ConstParamCP cpi,
                              const UnitTypeItem& ui,
                              SPCount nType = 1,
                              SPCount nTotal = 1);

   //  static ILeader createLeader(CampaignData* campData, Side side, Nationality n, RankEnum r);
};

#endif

