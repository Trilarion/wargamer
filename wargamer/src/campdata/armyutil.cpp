/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
#include "stdinc.hpp"
#include "armyutil.hpp"
#include "campdint.hpp"
#include "armies.hpp"
#include "options.hpp"
#include "control.hpp"
#include "wg_rand.hpp"
#include "route.hpp"
#include "town.hpp"
#include "sync.hpp"
#include "scenario.hpp"
#include "campctrl.hpp"
#include "nations.hpp"

#ifdef DEBUG
//#include "unitlog.hpp"
#include "clog.hpp"

static LogFile auLog("ArmyUtil.log");
#endif

#if !defined(EDITOR)
/*
 *  Units with SPCount of 0 are destroyed
 *  returns true if the whole organization is destroyed
 *
 * This needs rewriting to work with the new reference counting
 * Suggested method will be:
 *    do
 *    {
 *       iterate through each unit
 *          IF unit has no SPs and no children
 *             detach it (which will also delete it)
 *    } while units were destroyed
 *
 * Alternatively detach it can walk up through parent detaching each
 * parent if it is empty.
 * Need to be careful with the iterators.
 */

Boolean CampaignArmy_Util::clearDestroyedUnits(CampaignData* campData, const ICommandPosition& cpi, bool useEffective)
{
  ASSERT(cpi != 0);
  ASSERT(campData);

#ifdef DEBUG
  auLog.printf("====== Checking for Destroyed Units =======");
#endif

  Armies& armies = campData->getArmies();

  Boolean wipedOut = False;
  Boolean unitDestroyed = False;;

  do
  {
    unitDestroyed = False;

    UnitIter iter(&armies, cpi);

    while(iter.next())
    {
      ICommandPosition curCPI = iter.current();

      SPCount spCount = (!useEffective) ?
            armies.getUnitSPCount(curCPI, True) : armies.getUnitSPValue(curCPI);

      if(spCount <= 0)
      {

#ifdef DEBUG
        auLog.printf("%s(index %d) and all its units have been destroyed",
           curCPI->getNameNotNull(), (int)armies.ob()->getIndex(curCPI->generic()));
#endif

        unitDestroyed = True;

        /*
         * Test to see what happens to leaders
         */

        destroyedUnitLeaderProc(campData, curCPI);

        armies.removeUnitAndChildren(curCPI);

        if(curCPI == cpi)
        {
#ifdef DEBUG
          auLog.printf("Whole organization wiped out");
#endif
          wipedOut = True;
          unitDestroyed = False;
        }

        break;
      }
    }

  } while(unitDestroyed);

#ifdef DEBUG
  auLog.printf("====== End Destroyed Unit Check ========");
#endif


  /*
   * If we're not completely out of it, recalculate
   */

  if(!wipedOut)
  {
    armies.averageCommandMorale(cpi);
    armies.averageCommandFatigue(cpi);
  }

  return wipedOut;
}

#endif  // !defined(EDITOR)

#if !defined(EDITOR)

void CampaignArmy_Util::destroyedUnitLeaderProc(CampaignData* campData, ICommandPosition cpi)
{
  ASSERT(campData);

  Armies& armies = campData->getArmies();

  UnitIter iter(&armies, cpi);

  /*
   * Test each leader in command to see what happens to him
   */

  while(iter.next())
  {


    /*
     * First, detemine if he lives or dies (50/50 chance)
     */

    Boolean kill = False;

    const ILeader& iLeader = iter.current()->getLeader();
    ASSERT(iLeader != NoLeader);

#ifdef DEBUG
    auLog.printf("--- Testing %s for survival", iLeader->getNameNotNull());
#endif

    if(CRandom::get(100) < 50)
      kill = True;
    else
    {
      /*
       * If he lieves, determine if he escapes
       * or is captured (50/50 unless trapped, then 100%)
       */

      if(iter.current()->trapped() ||
         CRandom::get(100) < 50)
      {
#ifdef DEBUG
        auLog.printf("------ %s is Captured", iLeader->getNameNotNull());
#endif

        armies.addCapturedLeader(iLeader);
      }
      else
      {
#ifdef DEBUG
        auLog.printf("------ %s Survives", iLeader->getNameNotNull());
#endif

        makeIndependentLeader(campData, iLeader, cpi->getPosition());
      }
    }

    if(kill)
    {
#ifdef DEBUG
      auLog.printf("------ %s is Killed", iLeader->getNameNotNull());
#endif

      if(iLeader->isSupremeLeader())
      {
         armies.chooseNewSHQ(iLeader->side());
      }

      armies.detachLeader(iLeader);
      armies.deleteLeader(iLeader);

      /*
       * Check killed-leader condition
       */
    }
  }
}

#endif  // !defined(EDITOR)


Boolean CampaignArmy_Util::isUnitOrderable(const ConstICommandPosition& cpi)
{
  ASSERT(cpi != NoCommandPosition);

  if(cpi->isDead())     // Added SWG: 29Aug99
   return false;

  return GamePlayerControl::canControl(cpi->getSide());

}


Boolean CampaignArmy_Util::isUnitVisible(const CampaignData* campData, const ConstICommandPosition& cp)
{
#if !defined(EDITOR)

  ASSERT(cp != NoCommandPosition);
  // const CommandPosition* cp = getCommand(cpi);

   /*
    *  If No Fog of War or Unit is players side return True
    */

   if(!CampaignOptions::get(OPT_FogOfWar) ||
      (GamePlayerControl::getControl(cp->getSide()) == GamePlayerControl::Player))
      return True;

   ASSERT(GamePlayerControl::getControl(cp->getSide()) != GamePlayerControl::Player);

   /*
    * If here then Fog of War is active and unit is an enemy
    *
    * return true if unit is seen or last seen timer hasn't run out
    */

   if( (cp->isSeen()) || (cp->stillVisible(campData->getTick())) )
     return True;
   else
     return False;
#else // !EDITOR
  return True;
#endif   // EDITOR
}

const CampaignPosition& CampaignArmy_Util::getUnitDisplayPosition(const ConstICommandPosition& cp)
{
  ASSERT(cp != NoCommandPosition);

#if !defined(EDITOR)

   /*
    *  If No Fog of War or Unit is players side return actual position
    */

   if(!CampaignOptions::get(OPT_FogOfWar) ||
      (GamePlayerControl::getControl(cp->getSide()) == GamePlayerControl::Player))
   {
     return cp->getPosition();
   }

   ASSERT(GamePlayerControl::getControl(cp->getSide()) != GamePlayerControl::Player);

   /*
    *  Otherwise, If unit is visible return actual position
    */

   if(cp->isSeen())
     return cp->getPosition();

   /*
    *  If not, return last seen location
    */

   return cp->getLastSeenPosition();
#else
   return cp->getPosition();
#endif //(EDITOR)!
}

void CampaignArmy_Util::transferLeader(CampaignData* campData, const ILeader& general,
    const ICommandPosition& dest, Boolean autoProm, Boolean makeIndependent)
{
  ASSERT(campData);
  ASSERT(general != NoLeader);
  ICommandPosition cpi = general->command();

  Armies& armies = campData->getArmies();

  if(cpi != NoCommandPosition)
    armies.detachLeader(general);

  /*
   * make independent if dest == NoCommandPosition
   * and leader is not dead or captured
   */

  if( (dest == NoCommandPosition) &&
      (!general->isDead()) &&
      (!general->isCaptured()) )
  {
    ASSERT(cpi != NoCommandPosition);
    makeIndependentLeader(campData, general, cpi->getPosition());
  }

  if(cpi != NoCommandPosition)
  {
    if(autoProm)
    {
      armies.autoPromote(cpi, cpi->getSide());
    }
    else
    {
      ILeader iLeader = armies.createLeader(cpi->getSide(), cpi->getNation(), Rank_Division);
      armies.assignLeader(cpi, iLeader);
    }
  }

  if(dest != NoCommandPosition)
  {
    if(dest->getLeader() != NoLeader)
    {
      ILeader destLeader = dest->getLeader();
      armies.detachLeader(destLeader);

      if(makeIndependent)
        makeIndependentLeader(campData, destLeader, dest->getPosition());
    }

    armies.assignLeader(dest, general);   // iLeader);
  }
}


void CampaignArmy_Util::makeIndependentLeader(CampaignData* campData, const ILeader& leader, const CampaignPosition& pos, Boolean active)
{
   ASSERT(campData);
   ASSERT(leader != NoLeader);

   Armies& armies = campData->getArmies();

   /*
    * If the leader is Supreme Leader, then set a new leader to be SHQ
    */

   if (leader->isSupremeLeader())
   {
      armies.chooseNewSHQ(leader->side());
   }




   IndependentLeader* il = new IndependentLeader(leader);
   ASSERT(il);

   il->active(active);

   /*
    * Figure out travel time to leader pool
    *
    * Note: Temporary code
    * this needs more proper design.
    * For now, French leaders will go to SHQ, Allies to closest capital
    *
    */

   CampaignPosition toLoc;

   // temporary. we'll need to get this from a data file
   if(leader->getSide() == 0)
   {
      const CommandPosition* shqCP = armies.getSHQ(leader->getSide());
      toLoc.setPosition(shqCP->getPosition());
      il->atSHQ(True);
   }
   else
   {
      ASSERT(leader->getSide() == 1);

      // go to capital
      Nationality n = scenario->getDefaultNation(leader->getSide());

      Boolean found = False;

      TownList& tl = campData->getTowns();
      for(ITown iTown = 0; iTown < tl.entries(); iTown++)
      {
         const Town& t = tl[iTown];

         if(t.getSize() == TOWN_Capital)
         {
            const Province& p = campData->getProvince(t.getProvince());
            if(p.getNationality() == n)
            {
               toLoc.setPosition(iTown);
               il->town(iTown);
               found = True;
               break;
            }
         }
      }
      ASSERT(found);
   }

#if !defined(EDITOR)

   if(active)
   {
      TimeTick travelTime = 0;
      Distance routeDistance = 0;
      static CampaignRoute route(campData);

      if(route.getRouteDistance(&toLoc, &pos, Distance_MAX, routeDistance, True))
      {
         int miles = DistanceToMile(routeDistance);

         /*
          *  Travel time is 12 mph for 8 hours per day(96 mpd).
          *  timeToTravel Function caculates raw time,
          *  so set MPH macro to MPH(4) to account for courier rest time
          */

         travelTime = timeToTravel(routeDistance, MPH(4));
         il->travelTime(travelTime + campData->getTick());
      }
   }
#endif

   armies.d_independentLeaders.append(il);
}


/*
 * Attach 2 units
 *
 * Unit of lowest rank is detached from wherever it is
 * and attached as a child of the higher rank
 *
 * Units of equal rank:
 *    Deal with this later... will involve making new organisations
 *    promoting generals, etc...
 *
 *    For now:
 *    2nd unit is added as child to 1st unit (even if this makes
 *    silly tree).
 *
 */

void CampaignArmy_Util::attachUnits(CampaignData* campData, const ICommandPosition& unit1, const ICommandPosition& unit2)
{
   ASSERT(campData);
   Armies& armies = campData->getArmies();

   ArmyWriteLocker lock(armies);      // Bugfix: SWG 21Jul99 : Added write lock to stop display crashing

   if(unit1->isDead() || unit2->isDead())
      return;


   // lock this in
   // RWLock lock;
   // lock.startWrite();

   Boolean createdNew = False; // did we create a new unit

   ASSERT(unit1 != unit2);
   ASSERT(unit1->getSide() == unit2->getSide());

   if(unit1 == unit2)      // Defensive coding
      return;

   ICommandPosition cp1 = unit1;    // Copy these because they may be altered later
   ICommandPosition cp2 = unit2;

#ifdef DEBUG
   auLog.printf("Attaching %s and %s", cp1->getName(), cp2->getName());
#endif

   Rank r1 = cp1->getRank();
   Rank r2 = cp2->getRank();

   // Can't attach 2 army groups

   ASSERT( (r1.getRankEnum() != Rank_ArmyGroup) || (r2.getRankEnum() != Rank_ArmyGroup) );

   /*
    * If the same rank
    */

   if(r1.sameRank(r2))
   {
      /*
       * If unit1 is not a top-level unit, unit 2 attaches to unit1's parent
       */

      if(cp1->getParent() != armies.getPresident(cp1->getSide()))
      {
        cp1 = cp1->getParent();
      }

      /*
       *  else if a top-level unit
       */

      else
      {

         /*
          * Find out which leader is more noble. He will be promoted to new command
          */

         ILeader promoteThisLeader = constLeaderToLeader(armies.whoShouldCommand(cp1->getLeader(), cp2->getLeader()));

         RankEnum rank = r1.getRankEnum();
         DECREMENT(rank);
         ASSERT(rank > Rank_President);

         // bodge, army wing to be removed for now
#if 0
         if(rank == Rank_ArmyWing)
           rank = Rank_Army;
#endif
         /*
          *  Create new command and attach units
          */

         ICommandPosition newCPI = armies.createChildCP(armies.getPresident(cp1->getSide()));
         newCPI->setRank(rank);
         newCPI->setPosition(cp1->getPosition());
         newCPI->setSide(cp1->getSide());
         newCPI->isActive(True);
         armies.makeUnitName(newCPI, rank);
         transferLeader(campData, promoteThisLeader, newCPI, True);

         // recursive!
         attachUnits(campData, newCPI, cp1);
         // recursive!
         attachUnits(campData, newCPI, cp2);

         createdNew = True;
      }
   }

   if(!createdNew)
   {

      /*
       * Adjust so cp1 is higher rank than cp2
       */

      if(r2.isHigher(r1))                 // Rank class overloads this to mean higher rank
      {
         std::swap(cp1, cp2);
      }

      /*
       *  First get parent index. If parent is not president call
       */

      ICommandPosition iParent = armies.getParent(cp2);
      ASSERT(iParent != NoCommandPosition);

      armies.unlinkUnit(cp2);

      /*
       * Make r2 a child of r1
       */

      armies.addAsChild(cp1, cp2);

      /*
       * Reaverage Fatigue/Morale
       */

      if(iParent != cp1 && iParent->getRank().isLower(Rank_President))
      {
         armies.averageCommandMorale(iParent);
         armies.averageCommandFatigue(iParent);
      }

      armies.averageCommandMorale(cp1);
      armies.averageCommandFatigue(cp1);
   }

   // lock.endWrite();
}

#if !defined(EDITOR)

void CampaignArmy_Util::eliminateNationUnits(CampaignData* campdata, Nationality n, Side whichSide)
{
#if 0
  ASSERT(campData);
  Armies& armies = campData->getArmies();

#ifdef DEBUG
  NationType nType = scenario->getNationType(n);
  ASSERT(nType == MinorNation || nType == GenericNation);
#endif

  NationIter nIter = this;

  while(++nIter)
  {
    Side side = nIter.current();

    if(side == whichSide)
    {
      UnitIter iter(&armies, getFirstUnit(side));

      ICommandPosition lastCPI = NoCommandPosition;
      Boolean resetIter = False;

      while(iter.next())
      {
        ICommandPosition cp = iter.current();
        ASSERT(cp->getLeader() != NoLeader);

        if(cp->getNation() == n)
        {
#ifdef DEBUG
          cuLog.printf("===%s's nation(%s) has changed sides===",
             cp->getNameNotNull(),
             scenario->getNationName(n));
#endif

          /*
           * If unit has children of another nationality, then only leader
           * is destroyed(or maybe sent back home) and one of the other leaders is
           * promoted. Unit now becomes nationality of promoted leader
           *
           * If unit has children all of its nationality, do nothing.
           * Children will be wiped out when they come up in the iteration
           * and parent will be wiped out when orgClearDestroyedUnits is called.
           * This prevents trying to delete a unit twice or more
           *
           * If no children then delete unit
           */

          if(cp->hasChild())
          {
            UnitIter uiter(&armies, getChild(cp));
            ILeader bestLeader = NoLeader;
#if 0 // RankRating is obsolete now!
            /*
             * TODO: Find alternative way of picking best commander
             */

            Attribute bestRating = 0;
#endif

            while(uiter.sister())
            {
              ICommandPosition subCP = uiter.current();
              if(subCP->getNation() != n)
              {
                /*
                 * Leader with highest rank rating will be promoted
                 */

                ILeader leader = cp->getLeader();
#if 0 // RankRating is obsolete now!
                if(bestLeader == NoLeader || bestRating < leader->getRankRating())
                {
                  bestLeader = subCP->getLeader();
                  bestRating = leader->getRankRating();
                }
#else
                bestLeader = subCP->getLeader();
#endif

              }
            }

            if(bestLeader != NoLeader)
            {
              /*
               * Promote this leader
               */

              ILeader newLeader = bestLeader;
              ILeader oldLeader = cp->getLeader();

#ifdef DEBUG
              // CommandPosition* oldCP = getCommand(newLeader->getCommand());
              ICommandPosition oldCP = getCommand(newLeader->getCommand());
              cuLog.printf("%s of %s is promoted to take control of %s",
                 newLeader->getName(),
                 oldCP->getNameNotNull(),
                 cp->getNameNotNull());
#endif

              armies.transferLeader(bestLeader, iter.current(), True);
              cp->setNation(newLeader->getNation());

              armies.removeUnitAndChildren(oldLeader->getCommand());

            }
          }
          else
          {
            armies.removeUnitAndChildren(iter.current());
            if(lastCPI != NoCommandPosition)
            {
              iter.reset(lastCPI);
              resetIter = True;
            }
            else
              break;
          }
        }

        // lastCPI = !resetIter ? iter.current() : NoCommandPosition;
        if(!resetIter)
            lastCPI = iter.current();
        else
            lastCPI = NoCommandPosition;
      }
    }
  }
#endif
}

#endif  // !defined(EDITOR)

#if !defined(EDITOR)

void CampaignArmy_Util::captureLeader(CampaignData* campData, const ILeader& leader)
{
  ASSERT(campData);

  leader->isCaptured(True);

  /*
   * Transfer leader out of his old command
   */

  transferLeader(campData, leader, NoCommandPosition, True);

  // add to captured list
  campData->addCapturedLeader(leader);

  // TODO: Need player message
}

void CampaignArmy_Util::killLeader(CampaignData* campData, const ILeader& leader)
{
  ASSERT(campData);

  leader->isDead(True);

  /*
   * Update killed leader conditions
   */


  // kill of leader
  campData->getArmies().killAndReplaceLeader(leader);
}

static const int s_strength[BasicUnitType::HowMany] = {
  UnitTypeConst::InfantryPerSP,
  UnitTypeConst::CavalryPerSP,
  UnitTypeConst::ArtilleryPerSP,
  UnitTypeConst::SpecialPerSP
};

ULONG CampaignArmy_Util::totalManpower(const CampaignData* campData, ConstICommandPosition& cpi)
{
  ULONG value = 0;

  ConstSPIter iter(&campData->getArmies(), cpi);
  while(++iter)
  {
    const UnitTypeItem& uti = campData->getUnitType(iter.current()->getUnitType());
    ASSERT(uti.getBasicType() < BasicUnitType::HowMany);

    value += s_strength[uti.getBasicType()];
  }

  return value;
}

ULONG CampaignArmy_Util::manpowerByType(const CampaignData* campData, ConstParamCP cpi,
    const BasicUnitType::value& type)
{
  ULONG value = 0;

  ConstSPIter iter(&campData->getArmies(), cpi);
  while(++iter)
  {
    const UnitTypeItem& uti = campData->getUnitType(iter.current()->getUnitType());
    ASSERT(uti.getBasicType() < BasicUnitType::HowMany);

    if(uti.getBasicType() == type)
      value += s_strength[type];
  }

  return value;
}

ULONG CampaignArmy_Util::totalGuns(const CampaignData* campData, ConstParamCP cpi)
{
  ULONG value = 0;

  ConstSPIter iter(&campData->getArmies(), cpi);
  while(++iter)
  {
    const UnitTypeItem& uti = campData->getUnitType(iter.current()->getUnitType());

    if(uti.getBasicType() == BasicUnitType::Artillery)
      value += UnitTypeConst::GunsPerSP;
  }

  return value;
}

Boolean CampaignArmy_Util::bridgeTrainAtTown(const CampaignData* campData, ITown town, Side siegingSide)
{
  ConstUnitIter iter(&campData->getArmies(), campData->getArmies().getFirstUnit(siegingSide));
  while(iter.sister())
  {
    if(iter.current()->atTown() && iter.current()->getTown() == town)
    {
      if(campData->getArmies().hasBridgeTrain(iter.current()))
        return True;
    }
  }

  return False;
}

/*
 * All heavy equipment in this command is lost
 * i.e. siege-trains, bridge-trains
 */

void CampaignArmy_Util::loseEquipment(CampaignData* campData, const ICommandPosition& cpi)
{
  for(;;)
  {
    Boolean noMore = True;

    for(SpecialUnitType::value v = SpecialUnitType::FirstHeavy;
        v < SpecialUnitType::LastHeavy + 1; INCREMENT(v))
    {
      ICommandPosition eqCPI = NoCommandPosition;
      ISP eqSP = NoStrengthPoint;
      if(campData->getArmies().findSpecialType(cpi, eqCPI, eqSP, v))
      {
        campData->getArmies().detachStrengthPoint(eqCPI, eqSP);
        campData->getArmies().deleteStrengthPoint(eqSP);

        noMore = False;
      }
    }

    if(noMore)
      break;
  }
}

/*
 * Can this unit-type attach to this command
 */

bool CampaignArmy_Util::canAttachTo(
   const CampaignData* campData,
   ConstParamCP cpi,
   const UnitTypeItem& ui,
   SPCount nType,
   SPCount nTotal)
{
    if(cpi->getRank().isHigher(Rank_Division))
        return false;

  const SPCount spCount = campData->getArmies().getUnitSPCount(cpi, False);

  // if unit is filled up, we cannot attach
  if((nTotal + spCount) >= UnitTypeConst::MaxAllowedPerXX)
    return False;

  // test for nation status,
  if(!ui.isNationality(cpi->getNation()))
  {
    Boolean passTest = False;

    // if not an sp of units nationality
    for(Nationality n = 0; n < scenario->getNumNations(); n++)
    {
      if( (ui.isNationality(n)) &&
          (campData->getArmies().canNationControlNation(cpi->getNation(), n)) )
      {
        passTest = True;
        break;
      }
    }

    if(!passTest)
      return False;
  }

  // get useful flags
  Boolean artyHasCav = False;
  Boolean artyHasInf = False;
  Boolean allMounted = True;
  if(cpi->isArtillery() && cpi->getRankEnum() == Rank_Division)
  {
    ConstSPIter iter(&campData->getArmies(), cpi);
    while(++iter)
    {
      const StrengthPoint* sp = iter.current();
      const UnitTypeItem& uti = campData->getUnitType(sp->getUnitType());

      if(!uti.isMounted())
        allMounted = False;

      if(uti.getBasicType() == BasicUnitType::Cavalry)
        artyHasCav = True;

      if(uti.getBasicType() == BasicUnitType::Infantry)
        artyHasInf = True;
    }
  }

//ASSERT(artyHasInf == False || artyHasCav == False);

  // infantry can attach to inf or art that has no cav,
  // cavalry can attach to cav, or art with horse arty only
  // any artillery can attach to inf, only horse can attach to cav
  // special can attach to combined-arms (corps and above), or inf

  const SPCount typeCount = campData->getArmies().getNType(cpi, ui.getBasicType(), False);
  switch(ui.getBasicType())
  {
    case BasicUnitType::Infantry:
    {
      // we can join inf if unit is below limit
      if(cpi->isInfantry())
        return ((nType + typeCount) < UnitTypeConst::InfAllowedPerXX);

      // we can join artillery if it has no cav
      else if(cpi->isArtillery())
        return !(artyHasCav);

      // we cannot join cav
      return False;
    }

    case BasicUnitType::Cavalry:
    {
      // we can join cav if unit is below limit
      if(cpi->isCavalry())
        return ((nType + typeCount) < UnitTypeConst::CavAllowedPerXX);

      // we can join artillery if it has no inf
      else if(cpi->isArtillery())
        return (allMounted && !artyHasInf);

      // we cannot join inf
      return False;
    }

    case BasicUnitType::Artillery:
    {
      // can join cav if we are mounted
      if(cpi->isCavalry())
        return ui.isMounted();

      // can always join inf
      else if(cpi->isInfantry())
        return True;

      // otherwise this is an artillery so we can always join
      return True;
    }

    case BasicUnitType::Special:
    {
      // we can only join XX if inf,
      if(cpi->getRankEnum() == Rank_Division)
      {
        return (cpi->isInfantry());
      }

      // we can always attach if XXX or above
      return True;
    }
  }

  return False;
}

void CampaignArmy_Util::buildStrengthPoint(CampaignLogicOwner* campGame, ITown itown, const BuildItem& build)
{
   CampaignData* campData = campGame->campaignData();

   ASSERT(itown != NoTown);
   ASSERT(build.getWhat() < campData->getMaxUnitType());

   const Town& town = campData->getTown(itown); // getTown(itown);
   const Province& prov = campData->getProvince(town.getProvince());

   /*
    * Make new Strength Point
    */

   Armies& armies = campData->getArmies();

   ISP newSP = armies.createStrengthPoint();
   StrengthPointItem* sp = armies.getStrengthPoint(newSP);
   sp->setUnitType(build.getWhat());

   /*
    * if this is a Replacement-Pool SP, send to the pool
    */

   if(build.repoPool())
   {
     armies.addRepoSP(newSP, itown);
   }

   /*
    * If not a pool-sp, attach it to new command
    */

   else
   {
     const UnitTypeItem& ui = campData->getUnitType(sp->getUnitType());

     // first determine if a new unit is already in town
     ICommandPosition newCPI = NoCommandPosition;
     UnitIter iter(&armies, armies.getFirstUnit(town.getSide()));
     while(iter.sister())
     {
       const ICommandPosition& cpi = iter.current();
       if( (cpi->atTown()) &&
           (cpi->getTown() == itown) &&
           (cpi->newCommand()) )
       {
         if(canAttachTo(campData, cpi, ui))
         {
           newCPI = cpi;
           break;
         }
       }
     }

     // if we didn't find a new unit in town,
     // create another
     if(newCPI == NoCommandPosition)
     {
       newCPI = armies.createChildCP(armies.getPresident(town.getSide()));

       newCPI->setNation(prov.getNationality());
       newCPI->setSide(town.getSide());
       newCPI->setRank(Rank_Division);
       newCPI->setPosition(itown);
       newCPI->isActive(True);
       newCPI->newCommand(True);
       newCPI->setFatigue(Attribute_Range);
       armies.makeUnitName(newCPI, Rank_Division);

       ILeader g = armies.createLeader(town.getSide(), newCPI->getNation(), Rank_Division);

       Leader* leader = armies.getLeader(g);
       leader->setNation(prov.getNationality());

       armies.attachLeader(newCPI, g);
     }

     ASSERT(newCPI != NoCommandPosition);
     armies.attachStrengthPoint(newCPI, newSP);
     newCPI->setMorale(armies.getBaseMorale(newCPI));
     armies.setUnitSpecialistFlags(newCPI);

     campGame->redrawMap();
   }
}

#if 0
static ILeader CampaignArmy_Util::createLeader(CampaignData* campData, Side side, Nationality n, RankEnum r)
{
  ASSERT(n < scenario->getNumNations());

  campData->getNations().incLeadersCreated(n);

  int modifier = 0;

  // modifier equals #leaders created / table-value
  // Note: Value cannot be greater than 10
  const Table1D<UBYTE>& table = scenario->getLeaderDecreaseAbilityTable();
  const int mult = table.getValue(n);
  if(mult > 0)
  {
    modifier = minimum(10, campData->getNations().nLeadersCreated(n) / mult);
  }
  else
    modifier = minimum(10, campData->getNations().nLeadersCreated(n));

  return campData->getArmies().createLeader(side, n, r, modifier);
}
#endif


#endif  // !defined(EDITOR)

/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.3  2001/07/02 15:23:16  greenius
 * Battle, Campaign, Combined game and Campaign Editor now compile and run... mostly
 *
 * Revision 1.2  2001/06/18 08:25:44  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
