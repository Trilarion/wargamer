/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 * Common Base Classes for game objects (used to be in mapob)
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "gameob.hpp"
#include "misc.hpp"
#include "wldfile.hpp"
#include "filebase.hpp"
#ifdef DEBUG
#include "todolog.hpp"
#endif

#if 0 // Now in own file... gamesup\name.hpp

NamedItem::NamedItem(NamedItem& ni) : CString()
{
   const char* s = ni.getName();
   if(s)
      set(copyString(s));
}

NamedItem& NamedItem::operator = (NamedItem& ni)
{
   const char* s = ni.getName();
   if(s)
      setName(copyString(s));
   else
      setName(0);
   return *this;
}

/*
 * File handling
 */


static const UWORD NamedItem::fileVersion = 0x0000;

Boolean NamedItem::readData(FileReader& f)
{
   if(f.isAscii())
   {
#ifdef DEBUG
      todoLog.printf("TODO: NamedItem::readData().ascii unwritten\n");
#endif
      return False;
   }
   else
   {
      UWORD version;
      f.getUWord(version);
      ASSERT(version <= fileVersion);

      setName(f.getString());
   }

   return f.isOK();
}

Boolean NamedItem::writeData(FileWriter& f) const
{
   if(f.isAscii())
   {
      f.printf(fmt_ssn, nameToken, getName());
   }
   else
   {
      f.putUWord(fileVersion);
      f.putString(getName());
   }

   return f.isOK();
}

#endif   // Now in own file... gamesup\name.hpp

const UWORD SideObject::fileVersion = 0x0000;

Boolean SideObject::readData(FileReader& f)
{
   if(f.isAscii())
   {
#ifdef DEBUG
      todoLog.printf("TODO: SideObject::readData().ascii unwritten\n");
#endif
      return False;
   }
   else
   {
      UWORD version;
      f.getUWord(version);
      ASSERT(version <= fileVersion);

      UBYTE b;
      f.getUByte(b);
      owner = b;
   }

   return f.isOK();
}

Boolean SideObject::writeData(FileWriter& f) const
{
   if(f.isAscii())
   {
      f.printf(fmt_sdn, sideToken, (int) owner);
   }
   else
   {
      f.putUWord(fileVersion);
      f.putUByte(owner);
   }

   return f.isOK();
}

/*------------------------------------------------------------------
 * NationObject
 */

const UWORD NationObject::fileVersion = 0x0000;

Boolean NationObject::readData(FileReader& f)
{
   if(f.isAscii())
   {
#ifdef DEBUG
      todoLog.printf("TODO: NationObject::readData().ascii unwritten\n");
#endif
      return False;
   }
   else
   {
      UWORD version;
      f.getUWord(version);
      ASSERT(version <= fileVersion);

      UBYTE b;
      f.getUByte(b);
      nation = b;
   }

   return f.isOK();
}

Boolean NationObject::writeData(FileWriter& f) const
{
   if(f.isAscii())
   {
      f.printf(fmt_sdn, nationToken, (int) nation);
   }
   else
   {
      f.putUWord(fileVersion);
      f.putUByte(nation);
   }

   return f.isOK();
}


/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.2  2001/06/18 08:25:44  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
