/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef CAMPDIMP_HPP
#define CAMPDIMP_HPP

#ifndef __cplusplus
#error campdimp.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *      Campaign Data Implementation (equivalent to old campdat.hpp)
 *
 * Much simplified
 * Functions here should be simple manipulations with the campaign data.
 * The game movement logic and AI is handled at a higher level.
 *
 *----------------------------------------------------------------------
 */

#include "cdatadll.h"
#include "campdint.hpp" // Campaign Data Interface
#include "ctimdata.hpp" // Campaign Time Data

#include "town.hpp"
#include "armies.hpp"
#include "unittype.hpp"
#include "terrain.hpp"
#include "campinfo.hpp"
#include "c_cond.hpp"
#include "armistic.hpp"

#if !defined(EDITOR)
#include "cbattle.hpp"
#include "weather.hpp"
#include "victory.hpp"
#include "campside.hpp"
#include "c_rand.hpp"
#endif

#ifdef DEBUG
#include "clog.hpp"
#endif
// #ifdef DEBUG
// class String;
// #endif

class CampaignLogicOwner;                // Cammpaign Game that data refers to

#if !defined(EDITOR)
class CloseUnits;
class BestTown;
#endif

#if !defined(EDITOR)
#endif


class RealCampaignData :
        public CampaignData,
        public CampaignTimeData
{
    private:
        static const UWORD s_fileVersion;

        CampaignInterface* d_campGame;              // The Campaign Game that the data is part of
        // CampaignInterface* d_owner;       // Logic Thread that is controlling the data

        CampaignInfo d_campaignInfo;     // Info for starting screens

        TownList             d_towns;             // Locations
        ProvinceList         d_provinces;         // Provinces/States/Regions
        ConnectionList       d_connections;       // Roads/Railways/etc
        Armies*              d_armies;            // Order of battle
        TerrainTypeTable     d_terrainTypes;      // Terrain Type table
        WG_CampaignConditions::ConditionList* d_conditionList;     // Campaign Conditions
        Boolean              d_changed;           // Set if changed and not saved

#if !defined(EDITOR)
        CampaignBattleList* d_battleList;  // Battle List
        CampaignSides* d_sideData;
        CampaignWeather d_weather;       // Weather and Ground conditions
        Armistice* d_armistice;
        GameVictory d_gameVictory;
        CampaignSideRandomEvents* d_sideRandomEvents;
//        int d_pauseCount;               // 0 if time is running.
#endif


        CampaignSoundSystem * d_campaignSoundSystem;


    private:
        // Unimplemented Functions to prevent copying of class

        RealCampaignData(const RealCampaignData& campData);
        const RealCampaignData& operator = (const RealCampaignData& campData);

    public:
        CAMPDATA_DLL RealCampaignData(CampaignInterface* campGame);
        // CAMPDATA_DLL RealCampaignData();
        CAMPDATA_DLL ~RealCampaignData();

        // void create();               // Create some data

        CampaignInterface* game() { return d_campGame; }

        CampaignSoundSystem * soundSystem() const { return d_campaignSoundSystem; }

        /*
         * Towns
         */

        TownList& getTowns() { return d_towns; }
        const TownList& getTowns() const { return d_towns; }
        const Town& getTown(ITown i) const { return d_towns[i]; }
        Town& getTown(ITown i) { return d_towns[i]; }
#if defined(CUSTOMIZE)
        CAMPDATA_DLL void removeTown(ITown ti);
        CAMPDATA_DLL void swapTowns(ITown t1, ITown t2);
        CAMPDATA_DLL void sortTowns();
        CAMPDATA_DLL void setCapital(IProvince iProv, ITown iTown);
        CAMPDATA_DLL void moveTownToProvince(IProvince iProv, ITown iTown);
#endif
        CAMPDATA_DLL SPCount getTownStrength(ITown itown, Side s) const;

        IProvince getTownProvince(ITown t) const { return d_towns[t].getProvince(); }

        CAMPDATA_DLL const char* getTownName(ITown i) const;
        CAMPDATA_DLL const char* getTownName(ITown i, const char* defName) const;

        /*
         * Provinces
         */

        const ProvinceList& getProvinces() const { return d_provinces; }
        ProvinceList& getProvinces() { return d_provinces; }
        Province& getProvince(IProvince i) { return d_provinces[i]; }
        const Province& getProvince(IProvince i) const { return d_provinces[i]; }
#if defined(CUSTOMIZE)
        CAMPDATA_DLL void removeProvince(IProvince pi);
#endif

        /*
         * Connections
         */

        ConnectionList& getConnections() { return d_connections; }
        const ConnectionList& getConnections() const { return d_connections; }
        const Connection& getConnection(IConnection i) const { return d_connections[i]; }
        Connection& getConnection(IConnection i) { return d_connections[i]; }
#if defined(CUSTOMIZE)
        CAMPDATA_DLL void removeConnection(IConnection ti);
#endif

        CAMPDATA_DLL Distance getConnectionLength(IConnection i) const;
        CAMPDATA_DLL void calculateConnectionLength(IConnection i);
        CAMPDATA_DLL void getConnectionLocation(IConnection connection, Distance distAlong, Location& l) const;

        /*
         * CommandPositions
         */

        Armies& getArmies() { return *d_armies; }
        const Armies& getArmies() const { return *d_armies; }
        int getSideCount() const { return d_armies->getSideCount(); }
        CAMPDATA_DLL SimpleString getUnitName(ConstICommandPosition cpi) const;
        const UnitTypeTable& getUnitTypes() const { return d_armies->getUnitTypes(); }
        int getMaxUnitType() const { return d_armies->getMaxUnitType(); }
        const UnitTypeItem& getUnitType(UnitType n) const { return d_armies->getUnitType(n); }

        /*
         * Unattached leaders
         */

//        void addIndependentLeader(ILeader l) { armies.addIndependentLeader(l); }
        const IndependentLeaderList& independentLeaders() const { return d_armies->independentLeaders(); }
        IndependentLeaderList& independentLeaders() { return d_armies->independentLeaders(); }

        void addCapturedLeader(ILeader l) { d_armies->addCapturedLeader(l); }
        const CapturedLeaderList& capturedLeaders() const { return d_armies->capturedLeaders(); }
        CapturedLeaderList& capturedLeaders() { return d_armies->capturedLeaders(); }


        /*
         * TerrainTypes
         */

        const TerrainTypeTable& getTerrainTypes() const { return d_terrainTypes; }
        int getMaxTerrainType() const { return d_terrainTypes.entries(); }
        const TerrainTypeItem& getTerrainType(TerrainType n) const { return d_terrainTypes[n]; }
        CAMPDATA_DLL Boolean isChokePoint(ITown itown) const;
        CAMPDATA_DLL Boolean isSameSideOfChokePoint(IConnection con1, IConnection con2) const;
#if !defined(EDITOR)
        /*
         * Battles
         */

        CampaignBattleList& getBattleList() { return *d_battleList; }
        const CampaignBattleList& getBattleList() const { return *d_battleList; }

        /*
         * Victory level functions
         */

        void resetVictory() { d_sideData->resetVictory(); }
        void addVictoryLevel(ULONG points, Side side) { d_sideData->addSideVictory(points, side); }
				void removeVictoryLevel(LONG points, Side side) { d_sideData->removeSideVictory(points, side); }
				LONG getVictoryLevel(Side side) const{ return d_sideData->getSideVictory(side); }

				/*
         * Weather related functions
         */

        CampaignWeather& getWeather() { return d_weather; }
        const CampaignWeather& getWeather() const { return d_weather; }

        /*
         *  Loss count related functions
         */

        CampaignSides& getSideData() { return *d_sideData; }
        const CampaignSides& getSideData() const { return *d_sideData; }

        /*
         * Armistice
         */

        Boolean armisticeInEffect() const;
        void startArmistice(TimeTick tillWhen);
        virtual void updateArmistice(TimeTick theTime);

        /*
         * GameVictory
         */

        Boolean hasVictor() const { return d_gameVictory.gameOver(); }
        // void setWinner(Side s) { d_gameVictory.setWinner(s); }
        const GameVictory& getGameVictory() const { return d_gameVictory; }
        // GameVictory& getGameVictory() { return d_gameVictory; }
        void setVictory(const GameVictory& v) { d_gameVictory = v; }

        /*
         * Side Random Events;
         */

        void setSHQInChaos(Side side, TimeTick tilWhen) { d_sideRandomEvents->setSHQInChaos(side, tilWhen); }
        Boolean isSHQInChaos(Side side, TimeTick theTime) const  { return d_sideRandomEvents->isSHQInChaos(side, theTime); }
        void setEnemyPlansCaptured(Side side, TimeTick tilWhen) { d_sideRandomEvents->setEnemyPlansCaptured(side, tilWhen); }
        Boolean isEnemyPlansCaptured(Side side, TimeTick theTime) const { return d_sideRandomEvents->isEnemyPlansCaptured(side, theTime); }


#endif

        /*
         * Changeable Nations data
         */

        NationsList& getNations() { return d_armies->getNations(); }
        const NationsList& getNations() const { return d_armies->getNations(); }
        Side getNationAllegiance(Nationality n) const { return d_armies->getNationAllegiance(n); }
        Boolean isNationActive(Nationality n) const { return d_armies->isNationActive(n); }
        Boolean shouldNationEnter(Nationality n) { return d_armies->shouldNationEnter(n); }
        Attribute getNationMorale(Nationality n) const { return d_armies->getNationMorale(n); }

        void setNationAllegiance(Nationality n, Side s) { d_armies->setNationAllegiance(n, s); }
        void nationNeverEnters(Nationality n) { d_armies->nationNeverEnters(n); }
        void setNationMorale(Nationality n, Attribute m) { d_armies->setNationMorale(n, m); }

#if 0
        NationsList& getNations() { return nationsList; }
        const NationsList& getNations() const { return nationsList; }
        Side getNationAllegiance(Nationality n) const { return nationsList.getAllegiance(n); }
        Boolean isNationActive(Nationality n) const { return nationsList.isActive(n); }
        Boolean shouldNationEnter(Nationality n) { return !nationsList.nationShouldNotEnter(n); }
        Attribute getNationMorale(Nationality n) const { return nationsList.getMorale(n); }

        void setNationAllegiance(Nationality n, Side s) { nationsList.setAllegiance(n, s); }
        void nationNeverEnters(Nationality n) { nationsList.setNeverEnter(n); }
        void setNationMorale(Nationality n, Attribute m) { nationsList.setMorale(n, m); }
#endif
        /*
         *  Conditions
         */

        WG_CampaignConditions::ConditionList& getConditions() { return *d_conditionList; }

        /*
         * Campaign Info for starting screens
         */

        void setCampaignInfo(const CampaignInfo& info) { d_campaignInfo = info; }
        CampaignInfo& getCampaignInfo() { return d_campaignInfo; }

        /*
         * File Interface
         */

        CAMPDATA_DLL Boolean readData(FileReader& f);
        CAMPDATA_DLL Boolean writeData(FileWriter& f) const;

        /*
         * Time and Date
         */

        CampaignTimeData* getTimeData() { return this; }
        const CampaignTimeData* getTimeData() const { return this; }

        const CampaignTime& getCTime() const { return CampaignTimeData::getCTime(); }
        const Date& getDate() const { return CampaignTimeData::getDate(); }
        const char* asciiTime(char* buffer = 0) const { return CampaignTimeData::asciiTime(buffer); }
        void setTimeTick(TimeTick t) { CampaignTimeData::setTimeTick(t); }
#if !defined(EDITOR)
        TimeTick getTick() const { return CampaignTimeData::getTick(); }
#endif

        void setChanged() { d_changed = True; }
        void clearChanged() { d_changed = False; }
        Boolean hasChanged() const { return d_changed; }


#if defined(CUSTOMIZE)
        CAMPDATA_DLL void removeAllTowns();
        CAMPDATA_DLL void removeAllUnits();
#endif

#if !defined(EDITOR)            // This should be part of CampaignLogic
        /*
         * Combat and game mechanics support
         */

        CAMPDATA_DLL void setLastSeenLocation(ICommandPosition cpi);

        CAMPDATA_DLL Boolean isTownOrderable(ITown iTown) const;

        CAMPDATA_DLL Boolean isPlayerSide(Side side) const;
        CAMPDATA_DLL void killLeader(ICommandPosition cpi);

        // void startTactical(CampaignBattle* battle);

#ifdef DEBUG
        CAMPDATA_DLL void getCampaignPositionString(const CampaignPosition* pos, String& s) const;
#endif
#endif  // !EDITOR

        // CampaignInterface* campaignInterface() { return d_campaignGame; }

        /*
         * Synchronisation
         */

        // virtual void startWriteLock();
        // virtual void endWriteLock();
        // virtual void startReadLock() const;
        // virtual void endReadLock() const;

// #if !defined(EDITOR)
//         bool isTimeRunning() const { return d_pauseCount == 0; }
//         void stopTime() { ++d_pauseCount; }
//         void startTime() { --d_pauseCount; }
// #endif


    private:
#if 0   // unused
        void calculateAllConnections();
                // Calculate ALL Connection Lengths using the town's locations
#endif
};


#endif /* CAMPDIMP_HPP */

