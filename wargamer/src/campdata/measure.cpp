/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 * Implementation of Measurements
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "measure.hpp"
#include "filebase.hpp"

#define FORCE_LOCATIONS_ONTO_MAP


#if defined(FORCE_LOCATIONS_ONTO_MAP)
#include "scenario.hpp"
#endif

Boolean Location::writeData(FileWriter& f) const
{
   f.putULong(x);
   f.putULong(y);
   return f.isOK();
}

Boolean Location::readData(FileReader& f)
{
   f.getSLong(x);
   f.getSLong(y);

#if defined(FORCE_LOCATIONS_ONTO_MAP)
   const MapScaleInfo* info = scenario->getMapScaleInfo();
   Distance maxX = MilesToDistance(info->s_widthMiles) - 1;
   Distance maxY = MilesToDistance(info->s_heightMiles) - 1;

#if 0
   if( (x >= maxX) || (x < 0) )
      x = maxX/2;
   if( (y >= maxY) || (y < 0) )
      y = maxY/2;
#else

#ifdef DEBUG
   LONG oldX = x;
   LONG oldY = y;
#endif

   if(x > maxX)
      x = maxX;
   else if(x < 0)
      x = 0;

   if(y > maxY)
      y = maxY;
   else if(y < 0)
      y = 0;



#ifdef DEBUG

   if((x != oldX) || (y != oldY))
      alert("Object off-screen",
         "Original Position was %d,%d\rAllowed area=%d,%d\rMoved to %d,%d",
         static_cast<int>(oldX),
         static_cast<int>(oldY),
         static_cast<int>(maxX),
         static_cast<int>(maxY),
         static_cast<int>(x),
         static_cast<int>(y));
#endif

#endif

#endif

   return f.isOK();
}



/*
 *----------------------------------------------------------------------
 * $Log$
 *----------------------------------------------------------------------
 */
