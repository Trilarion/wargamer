/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 * Battle Windows Interface
 *
 * Classes that are part of BattleWindows use the virtual functions as a
 * way of passing information up to the top to avoid a complicated
 * set of interdependencies.
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "btwin_i.hpp"


#if !defined(NOLOG)

#include "clog.hpp"

LogFile& operator << (LogFile& file, const PixelPoint& p)
{
   file << '[' << p.x() << ',' << p.y() << ']';
   return file;
}

LogFile& operator << (LogFile& file, const BattleMapSelect& bms)
{
   file << bms.pixelPoint() << ',' << bms.location() << ',' << bms.button();
   return file;
}

#if 0
LogFile& operator << (LogFile& file, const BattleWindows_Internal::BattleMapSelect& bms)
{
   file << bms.pixelPoint() << ',' << bms.location() << ',' << bms.button();
   return file;
}
#endif
#endif


/*
 *----------------------------------------------------------------------
 * $Log$
 *----------------------------------------------------------------------
 */
