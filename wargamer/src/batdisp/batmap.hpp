/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef BATMAP_HPP
#define BATMAP_HPP

#ifndef __cplusplus
#error batmap.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *      Battle Main Viewing Window
 * It is called a MapWindow as it is analagous to the CampaignMapWindow
 *
 *----------------------------------------------------------------------
 */

#include "bdisp_dll.h"
#include "batdata.hpp"
#include "batmap_i.hpp"
#include "btwin_i.hpp"
#include "batdisp.hpp"
#include "wind.hpp"

// using namespace WG_BattleData;
class BattleMapWindImp;

class BattleMapWind : public Window
{
                BattleMapWindImp* d_imp;

        public:
                BATDISP_DLL BattleMapWind(PBattleWindows batWind, RCPBattleData battleData);
                BATDISP_DLL ~BattleMapWind();

                BATDISP_DLL void create();
                BATDISP_DLL void reset();

                BATDISP_DLL HWND getHWND() const;

                // BATDISP_DLL void show();
                // BATDISP_DLL void hide();
                BATDISP_DLL void enable(bool visible);
                BATDISP_DLL void show(bool visible);
                BATDISP_DLL bool isVisible() const;
                BATDISP_DLL bool isEnabled() const;

                BATDISP_DLL BattleMapDisplay * getDisplay(void);

                BATDISP_DLL BattleMapInfo::Mode mode() const;
                        // Return Zoom level

                BATDISP_DLL void mode(BattleMapInfo::Mode mode);
                        // set Zoom level

                BATDISP_DLL void BattleMapWind::startZoom();
                        // Initiate/Cancel Zoom Mode

                BATDISP_DLL bool isZooming() const;
                        // Return Zooming mode.

                BATDISP_DLL bool canZoom() const;
                        // return true if zoom button should be enabled


                BATDISP_DLL bool toggleHexOutline();
                BATDISP_DLL bool toggleHexDebug();
                BATDISP_DLL bool hexOutline() const;
                BATDISP_DLL bool hexDebug() const;

                BATDISP_DLL bool setLocation(const BattleLocation& loc);
                        // set location
                        // return true if it requires a redraw

				BATDISP_DLL void centerLocation(void);
						// center location on playable area

                BATDISP_DLL BattleArea area() const;
                        // return the area being displayed

                BATDISP_DLL void update(bool all);
                        // redraw dynamic map

				BATDISP_DLL void updateScrollBars(void);

				BATDISP_DLL bool isVisibilityRunning(void);

				BATDISP_DLL void startVisibilityChecks(void);

				BATDISP_DLL void stopVisibilityChecks(void);
				

};




#endif /* BATMAP_HPP */

