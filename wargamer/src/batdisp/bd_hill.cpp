/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *      Battle Map Display: Shading to represent slopes
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "bd_hill.hpp"
#include "hexiter.hpp"
#include "hexdata.hpp"
#include "batcord.hpp"
#include "dib.hpp"
#include "tempdib.hpp"
#include "dib_util.hpp"
#include "dib_poly.hpp"
#include "random.hpp"
#include "ftol.hpp"


#ifdef DEBUG
#include "clog.hpp"
LogFileFlush bdHillLog("bd_hill.log");
// LogFile bdLog("batdisp.log");
#endif


using namespace BattleMeasure;

namespace BattleDisplayUtility
{




   // Similar to the terrain overlap ditherer, but is given an irregular rectangle to dither.
   // Ideally it should be able to create a gradual dither from one edge to the other
   // But for now we'll just dither the whole thing equally with a 50% dither


   /*
   For initial optimizations I've replaced all ulong ^2 divides by shifts
   And any repeated int divs by float recip muls
   I think dib alocation may have some overhead too
   */




   HillDitherMaskGenerator d_ditherer;



   HillDitherMaskGenerator::HillDitherMaskGenerator(void) {

      d_ditherDIB = NULL;
      CreateDitherDIB(128,128);

   }



   HillDitherMaskGenerator::~HillDitherMaskGenerator(void)  {

      delete d_ditherDIB;
      d_ditherDIB = NULL;


   }






   DrawDIB *
      HillDitherMaskGenerator::GetDitherDIB(int width, int height) {


      // return if too small
      if((width<=1) || (height<=1)) return 0;

      /*
      Create new dither DIB if necessary
      */

      if(!d_ditherDIB || width > d_ditherDIB->getWidth() || height > d_ditherDIB->getHeight() ) CreateDitherDIB(width, height);

      return d_ditherDIB;
   }



   void
      HillDitherMaskGenerator::CreateDitherDIB(int width, int height) {

      if(d_ditherDIB) { delete d_ditherDIB; d_ditherDIB = NULL;
      }
      d_ditherDIB = new DrawDIB(width, height);
      ASSERT(d_ditherDIB);

      // fill initially with non mask colour
      d_ditherDIB->fill(NonMaskColor);

      // set rand number
      RandomNumber rand(width + height);

      // go through DIB, randomly changing bits to mask colour
      UBYTE * destPtr = d_ditherDIB->getBits();
      size_t lineOffset = d_ditherDIB->getStorageWidth();

      for(int y = 0;  y < height; ++y, destPtr += lineOffset) {
         UBYTE* linePtr = destPtr;
         for(int x = 0; x < width; ++x, ++linePtr) {
            // 50% dither
            if( (*linePtr == MaskColor) && (rand(100) < 50) ) *linePtr = NonMaskColor;
         }
      }

      // set the mask colour
      d_ditherDIB->setMaskColour(NonMaskColor);
   }




#ifdef CACHED_REMAP_TABLES
   /*
   define CACHED_REMAP_TABLES to enable
   code to globally cache 200 remap table pointers
   */


   // global instance within this namespace
   HillDisplayRemapHandler d_remapHandler;



   HillDisplayRemapHandler::HillDisplayRemapHandler(void) {

      for(int f=0; f<100; f++) {
         d_brightRemapTables[f] = 0;
         d_darkRemapTables[f] = 0;
      }
      d_numUsers = 0;

#ifdef DEBUG
      num_bright_tables = 0;
      num_dark_tables = 0;
#endif
   }


   HillDisplayRemapHandler::~HillDisplayRemapHandler(void) {

      cleanUpTables();
   }


   void
      HillDisplayRemapHandler::cleanUpTables(void) {
#ifdef DEBUG
      bdHillLog.printf("cleaning up tables (num refs left %i)\n", d_numUsers);
#endif

      for(int f=0; f<100; f++) {

         if(d_brightRemapTables[f]) {
            Palette::releaseTable(d_brightRemapTables[f]);
            d_brightRemapTables[f] = 0;
#ifdef DEBUG
            bdHillLog.printf("releasing bright remap-table (num left : %i)\n", num_bright_tables);
            num_bright_tables--;
#endif
         }

         if(d_darkRemapTables[f]) {
            Palette::releaseTable(d_darkRemapTables[f]);
            d_darkRemapTables[f] = 0;
#ifdef DEBUG
            bdHillLog.printf("releasing dark remap-table (num left : %i)\n", num_dark_tables);
            num_dark_tables--;
#endif
         }

      }
   }


   const UBYTE *
      HillDisplayRemapHandler::getBrightTable(int percent) {

      if(d_brightRemapTables[percent]) return d_brightRemapTables[percent];
      else {
         d_brightRemapTables[percent] = Palette::getBrightRemapTable(percent);

#ifdef DEBUG
         bdHillLog.printf("creating +%i remap table (%i created so far)\n", percent, num_bright_tables);
         num_bright_tables++;
#endif

         return d_brightRemapTables[percent];
      }
   }

   const UBYTE *
      HillDisplayRemapHandler::getDarkTable(int percent) {

      if(d_darkRemapTables[percent]) return d_darkRemapTables[percent];
      else {
         d_darkRemapTables[percent] = Palette::getDarkRemapTable(percent);

#ifdef DEBUG
         bdHillLog.printf("creating -%i remap table (%i created so far)\n", percent, num_dark_tables);
         num_dark_tables++;
#endif

         return d_darkRemapTables[percent];
      }
   }


   // end of define CACHED_REMAP_TABLES

#endif



   /*
   Class to define edges of a hex to shade
   */

   struct HeightEdgeInfo {

      BattleMeasure::HexCord::HexDirection d_edge;
      int points[6];          // Index to innerPoints[] in clockwise order

   };






   //    const int EdgeResolution = 2*(BattleTerrainHex::MaxHeight +1);
   //    const float EdgeResolutionRecip = 1.0 / (float) EdgeResolution;
   //    const float ShadePercent = 75.0 / (float) BattleTerrainHex::MaxHeight;
   //    const int d_HeightDisplayEpsilon = 5;
   const int EdgeResolution = 5*(BattleTerrainHex::MaxHeight +1);
   const float EdgeResolutionRecip = 1.0 / (float) EdgeResolution;
   const float ShadePercent = 3.0;
   const int d_HeightDisplayEpsilon = 0;

   static const HeightEdgeInfo d_edgeInfo[3] = {
            { HexCord::Right, { 0,1,5,9,8,4 } },
            { HexCord::UpRight, { 1,2,6,11,10,5 } },
            { HexCord::UpLeft, { 2,3,7,13,12,6 } }
        };




















   HillDisplay::HillDisplay(DrawDIB* destdib, RCPBattleData batData) {

      d_destDIB = destdib;
      d_batData = batData;

      d_polyDIB = new DrawDIB(128,128);
      ASSERT(d_polyDIB);

      d_bufferDIB = new DrawDIB(128,128);
      ASSERT(d_bufferDIB);

#ifdef CACHED_REMAP_TABLES
      d_remapHandler.incUsers();
#endif

#ifdef DEBUG
      bdHillLog.printf("Creating HillDisplayer\n\n");
#ifdef CACHED_REMAP_TABLES
      bdHillLog.printf("Num d_remapHandler refs : %i", d_remapHandler.getNumUsers());
#endif
#endif
   }



   HillDisplay::~HillDisplay() {

      if(d_polyDIB) delete d_polyDIB;
      if(d_bufferDIB) delete d_bufferDIB;

#ifdef CACHED_REMAP_TABLES
      d_remapHandler.decUsers();
#endif

#ifdef DEBUG
      bdHillLog.printf("Destroying HillDisplayer\n\n");
#ifdef CACHED_REMAP_TABLES
      bdHillLog.printf("Num d_remapHandler refs : %i", d_remapHandler.getNumUsers());
#endif
#endif
   }




   bool
   HillDisplay::ConstructInnerPoints(const HexIterator& iter) {

      ColourIndex color = d_destDIB->getColour(PALETTERGB(192,0,192));

      HexCord edgeHex;
      const BattleTerrainHex& edgeTerrain = d_batData->getTerrain(iter.hex() );
      BattleTerrainHex::Height current_height = edgeTerrain.d_height;

      // average heights in all hexes

      // float av_height = 0;
      double av_height = 0;
      long average_height;
      int edge_num;

      for(edge_num=0; edge_num<3; edge_num++) {
         if(d_batData->moveHex(iter.hex(), d_edgeInfo[edge_num].d_edge, edgeHex)) {
            const BattleTerrainHex& edgeTerrain = d_batData->getTerrain(edgeHex);
            av_height += edgeTerrain.d_height;
         }
      }

      // Bug:swg:17jun2001:
      // The next line of code did nothing because average_height was
      // not initialised, and was overwritten in the next line.
      // Hills will probably be a 3rd of the size they used to be now.
      // Chances are Jim bodged it somewhere else, so we may want to
      // lose this line!
      //
      // Also is there any reason to use floats instead of doubles?

//      average_height *= 0.33333333;
      av_height *= 0.33333333;
      FloatToLong(&average_height, av_height);


      // see if height difference is deemed worth displaying

      if(abs(current_height - average_height) > d_HeightDisplayEpsilon) {

         int dHeight = abs(current_height - average_height);
         //        if(!dHeight) dHeight++;
         int HWidth = dHeight + 4;

         float tmp;
         long dx1;
         long dy1;
         long dy2;

         tmp = ((float)(iter.right() - iter.midX())) * EdgeResolutionRecip;
         FloatToLong(&dx1, tmp);
         dx1 *= HWidth;

         tmp = ((float)(iter.midY() - iter.topCorner())) * EdgeResolutionRecip;
         FloatToLong(&dy1, tmp);
         dy1 *= HWidth;

         tmp = ((float)(iter.midY() - iter.top())) * EdgeResolutionRecip;
         FloatToLong(&dy2, tmp);
         dy2 *= HWidth;

         /*
         LONG dx1 = HWidth * (iter.right() - iter.midX()) * EdgeResolutionRecip;
         LONG dy1 = HWidth * (iter.midY() - iter.topCorner()) * EdgeResolutionRecip;
         LONG dy2 = HWidth * (iter.midY() - iter.top()) * EdgeResolutionRecip;

         */
         d_innerPoints[0].x(iter.right() - dx1);
         d_innerPoints[0].y(iter.lowCorner() - dy1);

         d_innerPoints[1].x(iter.right() - dx1);
         d_innerPoints[1].y(iter.topCorner() + dy1);

         d_innerPoints[2].x(iter.midX());
         d_innerPoints[2].y(iter.top() + dy2);

         d_innerPoints[3].x(iter.left() + dx1);
         d_innerPoints[3].y(iter.topCorner() + dy1);

         d_innerPoints[4].x(iter.right());
         d_innerPoints[4].y(iter.lowCorner());

         d_innerPoints[5].x(iter.right());
         d_innerPoints[5].y(iter.topCorner());

         d_innerPoints[6].x(iter.midX());
         d_innerPoints[6].y(iter.top());

         d_innerPoints[7].x(iter.left());
         d_innerPoints[7].y(iter.topCorner());

         d_innerPoints[8].x(iter.right() + dx1);
         d_innerPoints[8].y(iter.lowCorner() - dy1);

         d_innerPoints[9].x(iter.right() + dx1);
         d_innerPoints[9].y(iter.topCorner() + dy1);

         d_innerPoints[10].x(iter.right());
         d_innerPoints[10].y(iter.topCorner() - dy2);

         d_innerPoints[11].x(iter.midX() + dx1);
         d_innerPoints[11].y(iter.top() - dy1);

         d_innerPoints[12].x(iter.midX() - dx1);
         d_innerPoints[12].y(iter.top() - dy1);

         d_innerPoints[13].x(iter.left());
         d_innerPoints[13].y(iter.topCorner() - dy2);

         return true;
      }
      else return false;

   }



   void
      HillDisplay::ShadeInnerPoints(const HexIterator& iter) {

      HexCord edgeHex;
      const BattleTerrainHex& edgeTerrain = d_batData->getTerrain(iter.hex() );
      BattleTerrainHex::Height current_height = edgeTerrain.d_height;
      BattleTerrainHex::Height hex_height;



      int edge_num;
      for(edge_num=0; edge_num<3; edge_num++) {

         // get height of destination hex
         if(d_batData->moveHex(iter.hex(), d_edgeInfo[edge_num].d_edge, edgeHex)) {
            const BattleTerrainHex& edgeTerrain = d_batData->getTerrain(edgeHex);
            hex_height = edgeTerrain.d_height;
         }

         // only apply shading, if height diference is over a certain threshold
         if(abs(current_height - hex_height) > d_HeightDisplayEpsilon) {

            // Set up poly point indexes
            for(int i = 0; i < NUMPOINTS; ++i) { poly[i] = d_innerPoints[d_edgeInfo[edge_num].points[i]];
            }

            // Adjust shading so light comes from upper left
            int shade = ShadePercent * (hex_height - current_height);
            if( (d_edgeInfo[edge_num].d_edge == HexCord::Left) || (d_edgeInfo[edge_num].d_edge == HexCord::UpLeft) || (d_edgeInfo[edge_num].d_edge == HexCord::UpRight) ) {
               shade = -shade;
            }

            if(shade > 99) shade = 99;
            else if(shade < -99) shade = -99;

            // shade a downhill gradient
            if(current_height > hex_height) ShadePoly(shade, false);

            // shade an uphill gradient
            else ShadePoly(shade, true);
         }
      }

   }









   void
      HillDisplay::ShadePoly(int shade, bool upHill) {
      // upHill = lighter, !upHill = darker

      /*
      Build a new 4-sided poly dependent on uphill / downhill status
      */

      if(upHill) {
         PolyPoints[0] = poly[5];
         PolyPoints[1] = poly[0];
         PolyPoints[2] = poly[1];
         PolyPoints[3] = poly[2];
      }
      else {
         PolyPoints[0] = poly[2];
         PolyPoints[1] = poly[3];
         PolyPoints[2] = poly[4];
         PolyPoints[3] = poly[5];
      }

      /*
      Find minimum and maximum points of polygon
      */

      LONG minX = PolyPoints[0].x();
      LONG maxX = PolyPoints[0].x();
      LONG minY = PolyPoints[0].y();
      LONG maxY = PolyPoints[0].y();

      int i;
      for(i = 1; i < 4; ++i) {
         minX = minimum(PolyPoints[i].x(), minX);
         maxX = maximum(PolyPoints[i].x(), maxX);
         minY = minimum(PolyPoints[i].y(), minY);
         maxY = maximum(PolyPoints[i].y(), maxY);
      }

      LONG w = maxX - minX + 1;
      LONG h = maxY - minY + 1;

      if((w<=1) || (h<=1)) return;

      ColourIndex backColor = 0;
      ColourIndex fillColor = 1;

      // make sure d_polyDIB is big enough to accomodate
      if(d_polyDIB->getWidth() < w || d_polyDIB->getHeight() < h) {
         delete d_polyDIB; d_polyDIB = new DrawDIB(w,h);
      }

      d_polyDIB->rect(0, 0, w, h, backColor);

      /*
      Adjust points relative to minimum
      */

      /*
      PixelPoint PixPoints[4];
      for(i = 0; i < 4; ++i) {
      PixPoints[i].x( PolyPoints[i].x() - minX );
      PixPoints[i].y( PolyPoints[i].y() - minY );
      }
      */

      for(i = 0; i < 4; ++i) {
         PolyPoints[i].x( PolyPoints[i].x() - minX );
         PolyPoints[i].y( PolyPoints[i].y() - minY );
      }

      // draw poly into d_polyDIB
      DIB_Utility::drawPoly(d_polyDIB, PolyPoints, 4, fillColor);

      // get pointer to dither DIB of same dimensions
      DrawDIB * ditherDIB = d_ditherer.GetDitherDIB(w, h);
      if(!ditherDIB) return;

      // make sure buffer DIB is big enough to accomodate
      if(d_bufferDIB->getWidth() < w || d_bufferDIB->getHeight() < h) {
         delete d_bufferDIB; d_bufferDIB = new DrawDIB(w, h);
      }

      // create a copy of this, because color-keying only works from source
      d_bufferDIB->blit(0, 0, w, h, ditherDIB, 0, 0);

      // mask blit the poly DIB onto the dither DIB
      d_bufferDIB->blit(0, 0, w, h, d_polyDIB, 0, 0);


      /*
      Apply masked dither DIB as shading to destination
      */
      // const UBYTE * table;

#ifdef CACHED_REMAP_TABLES
      if(shade < 0) table = d_remapHandler.getDarkTable(-shade);
      else table = d_remapHandler.getBrightTable(shade);
      DIB_Utility::remapColor(d_destDIB, d_bufferDIB, table, fillColor, minX, minY, w, h, 0, 0);
#else
      CPalette::RemapTablePtr table =
         (shade < 0)
         ?  d_destDIB->getPalette()->getDarkRemapTable(-shade)
      :  d_destDIB->getPalette()->getBrightRemapTable(+shade);

      // if(shade < 0) table = Palette::getDarkRemapTable(-shade);
      // else table = Palette::getBrightRemapTable(+shade);
      DIB_Utility::remapColor(d_destDIB, d_bufferDIB, table, fillColor, minX, minY, w, h, 0, 0);
      // Palette::releaseTable(table);
#endif


      // for debugging purposes
#ifdef DEBUG
      num_done++;
#endif

   }




   /*
   Public function
   */

   void
      HillDisplay::Init(DrawDIB* destdib) {

      // set dest dib
      d_destDIB = destdib;

   }


   int
      HillDisplay::ShadeHeights(const HexIterator& iter) {

#ifdef DEBUG
      num_done = 0;
#endif

      if(ConstructInnerPoints(iter)) ShadeInnerPoints(iter);

#ifdef DEBUG
      return num_done;
#else
      return 0;
#endif
   }


};      // namespace BattleDisplayUtility



/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.3  2001/07/02 13:18:24  greenius
 * Battle, Campaign, Combined game and Campaign Editor now compile and run... mostly
 *
 * Revision 1.2  2001/06/18 08:25:43  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
