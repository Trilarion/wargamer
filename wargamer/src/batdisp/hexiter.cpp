/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *      Hex Iterator
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "hexiter.hpp"
#include "batdata.hpp"
#include "hexdata.hpp"
#include "scenario.hpp"

namespace BattleDisplay {

    // scaling from hex to pixel coords
    float X_HexScale;
    float Y_HexScale;

};


/*=======================================================================
 * Iterator Functions
 */

HexIterator::HexIterator(const BattleMapDisplayInt* display) :
        d_display(display),
        d_finished(true),                // this will be cleared later on
        d_scaleCalculated(false)
{
        ASSERT(d_display);

        // CPBattleData& batData = display->d_batData;
        CPBattleData batData = display->getBattleData();

        ASSERT(batData);

        batData->startRead();

      BattleMeasure::HexCord bottomleft;
      batData->getPlayingArea(&bottomleft, &d_hexSize);

      d_hexSize.x( d_hexSize.x() + bottomleft.x() );
      d_hexSize.y( d_hexSize.y() + bottomleft.y() );

        //d_hexSize = batData->hexSize();
        // d_hex = batData->getHex(display->getCorner() + display->getSize(), BattleData::Top);
        d_hex = batData->getHex(display->getCorner(), BattleData::Top);

        /*
         * Get first hex
         */

        // Adjust if y is even to make sure odd rows get left hex

        if( ((d_hex.y() & 1) == 0) && (d_hex.x() != 0))
        {
                d_hex.x(d_hex.x()-1);
        }

        BattleLocation bLoc = batData->getLocation(d_hex);

        // Get coordinate of bottom left of hex of even row

        if(d_hex.y() & 1)
                bLoc -= BattleLocation(BattleMeasure::OneXHex, BattleMeasure::HalfYHex);        // QuantaPerYHex/2);
        else
                bLoc -= BattleLocation(BattleMeasure::OneXHex/2, BattleMeasure::HalfYHex);      // QuantaPerYHex/2);

        // d_pLoc = display->locationToPixel(bLoc);
        PixelPoint pLoc = display->locationToPixel(bLoc);
        d_xStart = pLoc.getX();
        for(int i = 0; i < XHowMany; i++)
                d_x[i] = pLoc.getX();
        for(i = 0; i < YHowMany; i++)
                d_y[i] = pLoc.getY();

        /*
         * Adjust screen coordinates for bottom left of hex
         */

        d_yNum = display->getPixelSize().y() * BattleMeasure::HexYOffset;       // QuantaPerYHex * 1;
        d_yDen = display->getSize().y() * 3;            // 4;
        d_yDiff = d_yNum / d_yDen;
        d_yRem = d_yNum - d_yDiff * d_yDen;
        d_yRemainder = d_yDen / 2;

        d_xNum = display->getPixelSize().x() * BattleMeasure::OneXHex;          // QuantaPerXHex;
        d_xDen = display->getSize().x() * 2;            // Half a hex each iteration
        d_xDiff = d_xNum / d_xDen;
        d_xRem = d_xNum - d_xDiff * d_xDen;

        d_finished = false;

        // Fill 1st hex worth of coordinates

        i = 4;
        while(i--)
                nextY();

        // Occasionally rounding errors cause 1st row of hexes to be off screen
        // If this is the case, then iterate to next row

        while(!d_finished && (d_y[YTop] >= d_display->getPixelSize().y()))
        {
                iterateY();
        }

        if(!d_finished)
                initX();
}

HexIterator::~HexIterator()
{
    ASSERT(d_display);
    d_display->getBattleData()->endRead();
}

HexIterator::HexIterator(const HexIterator& it)
{
    ASSERT(it.d_display);
    it.d_display->getBattleData()->startRead();
    copy(it);
}

HexIterator& HexIterator::operator=(const HexIterator& it)
{
    ASSERT(d_display);
    ASSERT(it.d_display);
    d_display->getBattleData()->endRead();
    it.d_display->getBattleData()->startRead();
    copy(it);
    return *this;
}

void HexIterator::copy(const HexIterator& it)
{
    d_display = it.d_display;

    d_finished = it.d_finished;

    d_hex = it.d_hex;

    d_yNum = it.d_yNum;
    d_yDen = it.d_yDen;
    d_yDiff = it.d_yDiff;
    d_yRem = it.d_yRem;
    d_yRemainder = it.d_yRemainder;

    d_xNum = it.d_xNum;
    d_xDen = it.d_xDen;
    d_xDiff = it.d_xDiff;
    d_xRem = it.d_xRem;
    d_xRemainder = it.d_xRemainder;
    d_xStart = it.d_xStart;

    d_hexSize = it.d_hexSize;
    d_hRow = it.d_hRow;

    memcpy(d_x, it.d_x, sizeof(d_x));
    memcpy(d_y, it.d_y, sizeof(d_y));

    d_battleScale = it.d_battleScale;
    d_scale = it.d_scale;
    d_troopScale = it.d_troopScale;
    d_fxScale = it.d_fxScale;
    d_buildScale = it.d_buildScale;

    d_scaleCalculated = it.d_scaleCalculated;
}


bool HexIterator::operator () () const
{
        return !d_finished;
}


/*
 * Iterate one quarter of a hex in height
 */

void HexIterator::nextY()
{

        LONG hexHeight = d_yDiff;

        d_yRemainder += d_yRem;
        if(d_yRemainder >= d_yDen)
        {
                ++hexHeight;
                d_yRemainder -= d_yDen;
        }

        // Assumes that YBottom is 0, and YTop is YHowMany-1
        for(int i = 0; i < (YHowMany - 1); i++)
                d_y[i] = d_y[i+1];
        d_y[i] -= hexHeight;
}


void HexIterator::initX()
{
                d_hRow = d_hex;

                d_xRemainder = d_xDen / 2;

                for(int i = 0; i < XHowMany; i++)
                        d_x[i] = d_xStart;

                nextX();
                nextX();
                if(d_hRow.y() & 1)
                        nextX();
}

void HexIterator::nextX()
{

        LONG width = d_xDiff;

        d_xRemainder += d_xRem;
        if(d_xRemainder >= d_xDen)
        {
                ++width;
                d_xRemainder -= d_xDen;
        }

        for(int i = 0; i < (XHowMany-1); i++)
                d_x[i] = d_x[i+1];
        d_x[i] += width;
}

// Iterate next Y, return true if finished

bool HexIterator::iterateY()
{

        d_hex.y(d_hex.y() + 1);

        if( (d_y[YTopCorner] >= 0) &&
                (d_hex.y() < d_hexSize.y()) )
        {
                int i = 3;
                while(i--)
                        nextY();
        }
        else
                d_finished = true;

        return d_finished;
}

HexIterator& HexIterator::operator ++ ()
{
        ASSERT(!d_finished);
        ASSERT(d_y[YBottom] >= 0);
        ASSERT(d_hex.y() < d_hexSize.y());

        d_scaleCalculated = false;

        if(!d_finished)
        {
                // if( (d_pRow.getX() < d_display->d_staticDIB->getWidth())
                // if( (d_x[XRight] < d_display->d_staticDIB->getWidth())
                if( (d_x[XRight] < d_display->getPixelSize().x())
                        &&       (d_hRow.x() < d_hexSize.x()) )
                {
                        d_hRow.x(d_hRow.x() + 1);
                        nextX();
                        nextX();
                }
                else
                {
                        if(!iterateY())
                                initX();
                }
                // getHeight();

        }

#ifdef DEBUG
        if(!d_finished)
        {
                // ASSERT(d_x[XLeft] < d_display->d_staticDIB->getWidth());
                ASSERT(d_x[XLeft] < d_display->getPixelSize().x());
                // ASSERT(d_x[XRight] >= 0);
                // ASSERT(d_y[YTop] < d_display->d_staticDIB->getHeight());
                ASSERT(d_y[YTop] < d_display->getPixelSize().y());
                ASSERT(d_y[YBottom] >= 0);
        }
#endif  // DEBUG

        return *this;
}

void HexIterator::drawHexOutline(DrawDIB* dib, ColourIndex c)
{
#if 1
        dib->line(midX(), bottom(), right(), lowCorner(), c);
        // dib->line(x3, y2, x3, y3, c);
        // dib->line(x3, y3, x2, y4, c);
        // dib->line(x2, y4, x1, y3, c);
        dib->line(left(), topCorner(), left(), lowCorner(), c);
        dib->line(left(), lowCorner(), midX(), bottom(), c);
#else
        dib->line(midX(), bottom(), right(), lowCorner(), c);
        dib->line(left(), topCorner(), left(), lowCorner(), c);
        dib->line(left(), lowCorner(), midX(), bottom(), c);

        if(d_hOffset != 0)
        {
                // Draw 3 vertical lines, and 2 bottom bits

                dib->line(left(),  d_y[YLowCorner] + d_hOffset, left(),  d_y[YLowCorner], c);
                dib->line(midX(),  d_y[YBottom]    + d_hOffset, midX(),  d_y[YBottom],    c);
                dib->line(right(), d_y[YLowCorner] + d_hOffset, right(), d_y[YLowCorner], c);

                // dib->line(left(), d_y[YLowCorner], midX(), d_y[YBottom], c);
                // dib->line(midX(), d_y[YBottom], right(), d_y[YLowCorner], c);
        }

#endif
}


void HexIterator::drawFullHexOutline(DrawDIB * dib, ColourIndex c) {

        dib->line(midX(), bottom(), right(), lowCorner(), c);
        dib->line(left(), topCorner(), left(), lowCorner(), c);
        dib->line(left(), lowCorner(), midX(), bottom(), c);
        dib->line(left(), topCorner(), midX(), top(), c);
        dib->line(midX(), top(), right(), topCorner(), c);
        dib->line(right(), topCorner(), right(), lowCorner(), c);

}

/*
 * get rectangle for an aproximation of a hex
 *
 * The width is the full hex
 * Height is such that hexes on adjacent lines will line up with
 * no overlap
 *
 * With heights, this returning the rect of the top part of the hex
 * i.e. not the slopes.
 */

Rect<LONG> HexIterator::squareRect() const
{
        LONG y12 = (top() + topCorner() + 1) / 2;
        LONG y34 = (bottom() + lowCorner() + 1) / 2;

        // return Rect<LONG>(left(), y12, width(), y34-y12 + d_hOffset);
        return Rect<LONG>(left(), y12, width(), y34-y12);
}


// The size that the graphics are really drawn at

bool HexIterator::s_ratioInitialised = false;
float HexIterator::s_troopRatio;
float HexIterator::s_fxRatio;
float HexIterator::s_buildRatio;
Point<int> HexIterator::s_hexGraphicSize;    // (82,44);

void HexIterator::calcScale() const
{
    calcRatios();
    if(!d_scaleCalculated)
    {
        
        d_scale = Scale(
         (float(width()) / float(s_hexGraphicSize.x())),
         (float(lowCorner() - top()) / float(s_hexGraphicSize.y()))
      );
        float scale = d_scale.x();

        d_battleScale = Scale(
         float(width()) / float(BattleMeasure::OneXHex),
         float((lowCorner() - top())) / float(BattleMeasure::OneYHex)
      );

        d_troopScale = Scale(scale * s_troopRatio, scale * s_troopRatio);
        d_fxScale = Scale(scale * s_fxRatio, scale * s_fxRatio);
        d_buildScale = Scale(scale * s_buildRatio, scale * s_buildRatio);

        d_scaleCalculated = true;

    }
}


// Get scaling factor for graphics

HexIterator::Scale HexIterator::scale() const
{
    // return Scale(float(width()) / float(S_HexGraphicSize.x()), float(lowCorner() - top()) / float(S_HexGraphicSize.y()));
    calcScale();
    return d_scale;
}

HexIterator::Scale HexIterator::troopScale() const
{
    calcScale();
    return d_troopScale;
}

HexIterator::Scale HexIterator::buildingScale() const
{
    calcScale();
    return d_buildScale;
}

HexIterator::Scale HexIterator::fxScale() const
{
    calcScale();
    return d_fxScale;
}


// HexIterator::Scale HexIterator::xScale() const
// {
//     // float ratio = float(width()) / float(S_HexGraphicSize.x());
//     // return Scale(ratio, ratio);
//     calcScale();
//     return Scale(d_scale.x(), d_scale.x());
// }

PixelPoint HexIterator::position(const BattleLocation& pos) const
{
    calcScale();
    return PixelPoint(
        midX() + float(pos.x()) * d_battleScale.x(),
        midY() + float(pos.y()) * d_battleScale.y() );
}

void HexIterator::calcRatios()
{
    if(!s_ratioInitialised)
    {
        Scenario::LockConfig lock;

      /*
      Note from Jim : I've decremented each of these to that the d_scale member
      is increased slightly.  This covers up the unsightly gaps between roads, etc.
      */
        s_hexGraphicSize.x(scenario->getInt("HexPixelX") -1);
        s_hexGraphicSize.y(scenario->getInt("HexPixelY") -1);

        s_troopRatio = 1.0;
        s_fxRatio = 1.0;
        s_buildRatio = 1.0;

        try
        {
            int troopSize = scenario->getInt("TroopPixel");
            s_troopRatio = float(s_hexGraphicSize.x()) / float(troopSize);
        }
        catch(const Scenario::KeywordNotFound& e)
        {
            // do nothing
        }

        try
        {
            int fxSize = scenario->getInt("FXPixel");
            s_fxRatio = float(s_hexGraphicSize.x()) / float(fxSize);
        }
        catch(const Scenario::KeywordNotFound& e)
        {
            // do nothing
        }

        try
        {
            int buildSize = scenario->getInt("BuildingPixel");
            s_buildRatio = float(s_hexGraphicSize.x()) / float(buildSize);
        }
        catch(const Scenario::KeywordNotFound& e)
        {
            // do nothing
        }

        s_ratioInitialised = true;
    }
}

Point<int> HexIterator::hexGraphicSize()
{
    calcRatios();
    return s_hexGraphicSize;
}


/*
 *----------------------------------------------------------------------
 * $Log$
 *----------------------------------------------------------------------
 */
