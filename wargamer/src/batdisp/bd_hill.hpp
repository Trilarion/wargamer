/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef HILLS_HPP
#define HILLS_HPP

#ifndef __cplusplus
#error bd_hill.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *      Battle Map Display: Shading to represent slopes
 *
 *----------------------------------------------------------------------
 */


#include "batdata.hpp"

class DrawDIB;
class HexIterator;
class TempDIB;



//#define CACHED_REMAP_TABLES




namespace BattleDisplayUtility {





class HillDitherMaskGenerator {
    
    public:
    
        HillDitherMaskGenerator(void);
        ~HillDitherMaskGenerator(void);

        DrawDIB * GetDitherDIB(int width, int height);
        void CreateDitherDIB(int width, int height);

        enum {
            NonMaskColor = 1,
            MaskColor = 0
        };

    private:
        DrawDIB * d_ditherDIB;

};


extern HillDitherMaskGenerator d_ditherer;



#ifdef CACHED_REMAP_TABLES
/*
	define CACHED_REMAP_TABLES to enable
	code to globally cache 200 remap table pointers
*/

class HillDisplayRemapHandler {

public:

	HillDisplayRemapHandler(void);
	~HillDisplayRemapHandler(void);

	const UBYTE * getBrightTable(int percent);
	const UBYTE * getDarkTable(int percent);

	void cleanUpTables(void);

	void incUsers(void) {
		d_numUsers++;
	}
	void decUsers(void) {
		d_numUsers--;
		if(d_numUsers <= 0) cleanUpTables();
	}
	int getNumUsers(void) {
		return d_numUsers;
	}

private:

	const UBYTE * d_brightRemapTables[100];
	const UBYTE * d_darkRemapTables[100];

	int d_numUsers;

#ifdef DEBUG
	int num_bright_tables;
	int num_dark_tables;
#endif

};

extern HillDisplayRemapHandler d_remapHandler;
#endif












#define NUMPOINTS 6

class HillDisplay {

    private:
    
        DrawDIB * d_destDIB;
        // destination DIB
        DrawDIB * d_polyDIB;
        // temporary buffer DIB
        DrawDIB * d_bufferDIB;
        // poly mask DIB
        CPBattleData d_batData;
        // battle data
        //Point<LONG>
		PixelPoint d_innerPoints[14];
        // inner points of hex
		PixelPoint poly[NUMPOINTS];
		PixelPoint PolyPoints[4];



#ifdef DEBUG
		int num_tables_created;
        int num_done;
#endif

    public:
        
        HillDisplay(DrawDIB* destdib, RCPBattleData batData);
        ~HillDisplay(void);

		void Init(DrawDIB* destdib);
        int ShadeHeights(const HexIterator& iter);

    private:

        bool ConstructInnerPoints(const HexIterator& iter);
        void ShadeInnerPoints(const HexIterator& iter);
        void ShadePoly(int shade, bool upHill);



};



};      // namespace BattleDisplayUtility


#endif /* HILLS_HPP */

