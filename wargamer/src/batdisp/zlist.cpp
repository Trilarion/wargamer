/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 * Sorted list of objects, sorted by Y-Cordinate
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "zlist.hpp"

/*
 * Destructor
 */

BattleDisplay::ZDisplayObjectList::~ZDisplayObjectList()
{
}

/*
 * Add object to list
 */

void BattleDisplay::ZDisplayObjectList::draw(const RefPtr<DisplayObject>& ob, const PixelPoint& p)
{
   d_items.insert(Container::value_type(p, ob));
}

/*
 * Display all objects and clear
 */

void BattleDisplay::ZDisplayObjectList::draw(DrawDIB* dib) const
{
   for(Container::const_iterator i = d_items.begin(); i != d_items.end(); ++i)
   {
      (*i).second->draw(dib, (*i).first);
   }
}

/*
 * Delete all objects
 */

void BattleDisplay::ZDisplayObjectList::clear()
{
   d_items.erase(d_items.begin(), d_items.end());
}

/*
 *----------------------------------------------------------------------
 * $Log$
 *----------------------------------------------------------------------
 */
