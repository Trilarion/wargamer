/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
#include "stdinc.hpp"
#include "b_vis.hpp"
//#include "ftol.hpp"


/*

  Filename : b_vis.cpp

  Description : Unit->Hex visibility checks, for realtime FOW

  Notes : These routines have been (& will be more) optimized for speed.  Best performance will be seen
  when running a decent number of hexes per call (ie. don't do 1 hex at a time, every 10ms !).  This should
  maximize hits on your L1 cache for the code & the data look-ups.  Unfortunately we may to be thrashing it big-time
  when referencing the hexmap, as we don't know the cache-state when this thread is called.
  
*/


// define this to make the thread process the whole hexmap on contstuction
// otherwise, it will just set all the hexes to non-visible
// and the user will have to wait for processing to take place normall, before the screen is updated
#define PROCESS_WHOLE_MAP_ON_CONSTRUCTION


using namespace BattleMeasure;
using namespace Greenius_System;


#if 0
// define to do full terrain-aware LOS
#define TERRAIN_LOS
// define to use precalculated distance recips
#define PRECALC_DISTANCE
#endif

BattleHexVisibility::BattleHexVisibility(BattleData * batdata, BattleMapDisplay * display, Side side) :
   Thread(GameThreads::instance()),
    d_los(batdata) {

   ASSERT(batdata);

   d_batData = batdata;
   d_display = display;
   d_side = side;

   d_position = HexCord(0,0);
#if 0 
   for(int f=0; f<MAX_DIMENSION_OF_BATTLEFIELD; f++) {
      float val = f;
      d_distanceRecips[f] = 1.0f / val;
   }
#endif
   Initialize();
}


BattleHexVisibility::~BattleHexVisibility(void) {


}



void
BattleHexVisibility::Initialize(void) {

   BattleArea area = d_display->area();

   BattleLocation bottom_left_pos(area.left(), area.top());
   BattleLocation top_right_pos(
      area.left() + area.width() + BattleMeasure::HalfXHex,
      area.top() + area.height() + BattleMeasure::QuarterYHex
   );

   HexCord bottom_left_hex = d_batData->getHex(bottom_left_pos, BattleData::Top);
   HexCord top_right_hex = d_batData->getHex(top_right_pos, BattleData::Top);

   d_minX = BATTLEMAP_BUFFERHEXES;
   d_minY = BATTLEMAP_BUFFERHEXES;
   d_maxX = (d_batData->map()->getSize().x() - BATTLEMAP_BUFFERHEXES) + 1;
   d_maxY = (d_batData->map()->getSize().y() - BATTLEMAP_BUFFERHEXES) + 1;

   d_position = HexCord(d_minX, d_minY);

   // initial pass does whole
#ifdef PROCESS_WHOLE_MAP_ON_CONSTRUCTION
   int xsize = d_maxX - d_minX;
   int ysize = d_maxY - d_minY;

   int num_hexes = xsize * ysize;

   Process(num_hexes);
#else
   // just set all hexes to not-seen
   for(int y=d_minY; y<d_maxY; y++) {
      for(int x=d_minX; x<d_maxX; x++) {
         HexCord h(x,y);
         BattleTerrainHex& maphex = d_batData->map()->get(h);
         maphex.d_lastFullVisUnit = NULL;
         maphex.d_visibility = Visibility::NotSeen;
      }
   }
#endif

}


void
BattleHexVisibility::run(void) {

   Greenius_System::WaitableCounter timer(1000, true);

   while(wait(timer) != TWV_Quit) {

        int ticks = timer.getAndClear();

      BattleArea area = d_display->area();

      BattleLocation bottom_left_pos(area.left(), area.top());
      BattleLocation top_right_pos(
         area.left() + area.width() + BattleMeasure::HalfXHex,
         area.top() + area.height() + BattleMeasure::QuarterYHex
      );

      HexCord bottom_left_hex = d_batData->getHex(bottom_left_pos, BattleData::Top);
      HexCord top_right_hex = d_batData->getHex(top_right_pos, BattleData::Top);

      d_minX = bottom_left_hex.x();
      d_minY = bottom_left_hex.y();
      d_maxX = top_right_hex.x() + 1;
      d_maxY = top_right_hex.y() + 1;

      if(d_position.x() < d_minX) d_position.x(d_minX);
      else if(d_position.x() > d_maxX) d_position.x(d_maxX);
      if(d_position.y() < d_minY) d_position.y(d_minY);
      else if(d_position.y() > d_maxY) d_position.y(d_maxY);

      int num_hexes = (d_maxX - d_minX) * ((d_maxY - d_minY)>>2); // div 4
      if(num_hexes < 64) num_hexes = 64;

      Process(num_hexes);

   }

   timer.stop();
}


/*

  This is the main function to process visibility from units to hexes
  'nhexes' hexes will be processed each time, 'd_position' is the current working
  position, which wraps around the visible map area.
  This routine has been optimised for graded visibiliy, & keeps track of the last
  unit which had full visibility to a particular hex. This unit is tried first.

*/

void
BattleHexVisibility::Process(int n_hexes) {

   BattleMap * map = d_batData->map();

   while(n_hexes > 0) {

      BattleUnitIter unitIter(d_batData->ob(), d_side);

      BattleUnit * fullVisUnit = NULL;
      BattleTerrainHex& maphex = map->get(d_position);

      Visibility::Value best_visibility = Visibility::NotSeen;
      Visibility::Value current_visibility;

      // if last time we had full visibility, see if that's still the case
      if(maphex.d_lastFullVisUnit) {
         best_visibility = d_los.FastLOS(map, maphex.d_lastFullVisUnit->hex(), d_position);
         if(best_visibility == Visibility::Full) {
            fullVisUnit = maphex.d_lastFullVisUnit;
            goto end_loop;
         }
      }

      // go through OB
      while(!unitIter.isFinished()) {

         BattleCP * cp = unitIter.cp();

         HexCord cp_hex = cp->hex();

         if(cp_hex == d_position) {
            best_visibility = Visibility::Full;
            goto end_loop;
         }
         
         current_visibility = d_los.FastLOS(map, cp_hex, d_position);
         if(current_visibility > best_visibility) best_visibility = current_visibility;
         // if this is full visibility, quit out
         if(best_visibility == Visibility::Full) {
            fullVisUnit = cp;
            goto end_loop;
         }

         // go through child SPs
         BattleSP * sp = cp->sp();

         while(sp) {

            HexCord sp_hex = sp->hex();

            if(sp_hex == d_position) {
               best_visibility = Visibility::Full;
               goto end_loop;
            }

            current_visibility = d_los.FastLOS(map, sp_hex, d_position);
            if(current_visibility > best_visibility) best_visibility = current_visibility;
            // if this is full visibility, quit out
            if(best_visibility == Visibility::Full) {
               fullVisUnit = sp;
               goto end_loop;
            }

            sp = sp->sister();
         }

         unitIter.next();
      }


// no-one said goto's were illegal !
end_loop:

      // mark this hex's visibility value
      maphex.d_visibility = best_visibility;

      // if we have full visibility this will be a valid BattleUnit, otherwise it'll be NULL
      maphex.d_lastFullVisUnit = fullVisUnit;


      // move to next hex
      d_position.x( d_position.x()+1 );

      if(d_position.x() >= d_maxX) {

         d_position.x(d_minX);
         d_position.y( d_position.y()+1 );

         if(d_position.y() >= d_maxY) {
            d_position.y(d_minY);
         }
      }

      n_hexes--;
   }
}

#if 0
/*
Presquared distance calculations to eliminate sqrt()s
*/
#define MAX_VIS_DIST_SQUARED  36*36
#define FAIR_VIS_DIST_SQUARED 18*18
#define GOOD_VIS_DIST_SQUARED 12*12
#define FULL_VIS_DIST_SQUARED 8*8


#define VISIBILITY_DELTA      -4
#define VISIBILITY_CUTOFF     VISIBILITY_DELTA - 3


/*

  This routine takes care of the LineOfSight calculations for the hex-visibility checks.
  Initially it worked on an on-off basis, but has now been adapted for graded visibility.
  Obscuring terrain is calculated correctly, as is the proper height gradient.
  Lots of optimizes have been made, including pre-squared distance comparisons to avoid sqrt()s,
  and a 1/z lookup to avoid FDIVs

*/


Visibility::Value
BattleHexVisibility::FastLOS(BattleMap * map, const HexCord & from_hex, const HexCord & to_hex) {

#ifdef TERRAIN_LOS

   struct LOSStruct {
      UBYTE d_seeThrough; // how many hexes of this type of terrain can we see through?
      UBYTE d_height;     // height in meters of this terrain.
   };

   static const LOSStruct s_losTable[GT_HowMany] = {
      { 0,      0  }, // GT_None  shouldn't be used
      { UBYTE_MAX, 0  }, // GT_Grass
      { UBYTE_MAX, 0  }, // GT_Water
      { 4,      5  }, // GT_Orchard
      { 4,      3  }, // GT_Vineyard
      { 2,      10 }, // GT_LightWood
      { 1,      30 }, // GT_DenseWood
      { UBYTE_MAX, 0  }, // GT_PloughedField
      { 4,         2  }, // GT_CornField
      { 4,         2  }, // GT_WheatField
      { 3,         1  }, // GT_Rough
      { UBYTE_MAX, 0  }, // GT_Marsh
      { 1,         20 }, // GT_Town
      { 2,         15 }, // GT_Village
      { UBYTE_MAX, 0  }, // GT_Farm
      { UBYTE_MAX, 0  }  // GT_Cemetary
   };

   // this *could* be cleared better in ASM, but it should always be in L1 cache
   for(int f=GT_HowMany-1; f>=0; f--) losVals[f] = 0;

#endif

   // both are +2 to account for unit heights
   int start_height = d_batData->map()->get(from_hex).d_height;
   int end_height = d_batData->map()->get(to_hex).d_height;

    int d, x, y, ax, ay, sx, sy, dx, dy;

   int x1 = from_hex.x();
   int y1 = from_hex.y();
   int x2 = to_hex.x();
   int y2 = to_hex.y();

    dx = to_hex.x()-from_hex.x();
   ax = abs(dx);
    dy = to_hex.y()-from_hex.y();
   ay = abs(dy);

   // sx = sgn(dx);
   if(dx>0) sx = 1;
   else if(dx<0) sx = -1;
   else sx = 0;

   // sy = sgn(dy);
   if(dy>0) sy = 1;
   else if(dy<0) sy = -1;
   else sy = 0;

    x = from_hex.x();
    y = from_hex.y();

   int squared_dist = (ax*ax)+(ay*ay);
   if(squared_dist > MAX_VIS_DIST_SQUARED) return Visibility::NotSeen;

   // NB : why are these doubled ??
   ax = ax<<1;
   ay = ay<<1;


   /*
   x dominant
   */
    if(ax>ay) {

#ifdef PRECALC_DISTANCE
      float height_inc = ((float)(end_height - start_height)) * d_distanceRecips[ax>>1];
#else
      float height_inc = ((float)(end_height - start_height)) / ((float)(ax>>1));
#endif
      float current_height = start_height;

      d = ay-(ax>>1);
      for (;;) {

         // we reached the end without being obscured
         if (x==to_hex.x()) {
            if(squared_dist > FAIR_VIS_DIST_SQUARED) return Visibility::Poor;
            else if(squared_dist > GOOD_VIS_DIST_SQUARED) return Visibility::Fair;
            else if(squared_dist > FULL_VIS_DIST_SQUARED) return Visibility::Good;
            else return Visibility::Full;
         }
         // step along line
         if (d>=0) {
            y += sy;
            d -= ax;
         }
         x += sx;
         d += ay;

         // check height on hex
         BattleTerrainHex & maphex = d_batData->map()->get(HexCord(x,y));
         float height_diff = (current_height - maphex.d_height);
         if(height_diff < VISIBILITY_DELTA) {
            if(height_diff < VISIBILITY_CUTOFF) height_diff = VISIBILITY_CUTOFF;
            long vis_val;
            FloatToLong(&vis_val, height_diff);
            return static_cast<Visibility::Value>(vis_val);
         }


#ifdef TERRAIN_LOS
         // check if we are looking through terrain
         if(height_diff < s_losTable[maphex.d_terrainType].d_height) {

            losVals[maphex.d_terrainType]++;
            // if we've reached the maximum allowed ammount
            if(losVals[maphex.d_terrainType] > s_losTable[maphex.d_terrainType].d_seeThrough) return Visibility::NotSeen;
         }
#endif

         current_height += height_inc;
      }
    }

   /*
   y dominant
   */
    else {

#ifdef PRECALC_DISTANCE
      float height_inc = ((float)(end_height - start_height)) * d_distanceRecips[ay>>1];
#else
      float height_inc = ((float)(end_height - start_height)) / ((float)(ay>>1));
#endif
      float current_height = start_height;

      d = ax-(ay>>1);
      for (;;) {

         // we reached the end without being obscured
         if(y==to_hex.y()) {
            if(squared_dist > FAIR_VIS_DIST_SQUARED) return Visibility::Poor;
            else if(squared_dist > GOOD_VIS_DIST_SQUARED) return Visibility::Fair;
            else if(squared_dist > FULL_VIS_DIST_SQUARED) return Visibility::Good;
            else return Visibility::Full;
         }
         // step along line
         if(d>=0) {
            x += sx;
            d -= ay;
         }
         y += sy;
         d += ax;

         // check height on hex
         BattleTerrainHex & maphex = d_batData->map()->get(HexCord(x,y));
         float height_diff = (current_height - maphex.d_height);
         if(height_diff < VISIBILITY_DELTA) {
            if(height_diff < VISIBILITY_CUTOFF) height_diff = VISIBILITY_CUTOFF;
            long vis_val;
            FloatToLong(&vis_val, height_diff);
            return static_cast<Visibility::Value>(vis_val);
         }


#ifdef TERRAIN_LOS
         // check if we are looking through terrain
         if(height_diff < s_losTable[maphex.d_terrainType].d_height) {

            losVals[maphex.d_terrainType]++;
            // if we've reached the maximum allowed ammount
            if(losVals[maphex.d_terrainType] > s_losTable[maphex.d_terrainType].d_seeThrough) return Visibility::NotSeen;
         }
#endif

         current_height += height_inc;
      }
    }


}


#endif















#if 0

void getNextHexLOS(RPBattleData bd, const HexCord& hex, HexCord& tHex,
    UWORD& xs, UWORD& ys, const int incX, const int incY)
{
      if(xs)
      {
         tHex.x(tHex.x() + incX);
      }

      if(ys)
      {
         tHex.y(tHex.y() + incY);
      }

      if(B_Logic::adjacentHex(bd, hex, tHex))
      {
         ys = static_cast<UWORD>(maximum(0, ys - 1));
         xs = static_cast<UWORD>(maximum(0, xs - 1));
      }
      else
      {
         // if that doesn't work...
         ASSERT(xs);
         ASSERT(ys);

         tHex = hex;

         if(ys > xs)
         {
            tHex.y(tHex.y() + incY);
            if(!B_Logic::adjacentHex(bd, hex, tHex))
               FORCEASSERT("Hex not found in lineOfSight()");
            else
               ys = static_cast<UWORD>(maximum(0, ys - 1));
         }
         else
         {
            tHex.x(tHex.x() + incX);
            if(!B_Logic::adjacentHex(bd, hex, tHex))
               FORCEASSERT("Hex not found in lineOfSight()");
            else
               xs = static_cast<UWORD>(maximum(0, xs - 1));
         }
      }
}

bool lineOfSight(RPBattleData bd, const HexCord& srcHex, const HexCord& destHex, HexList* hl, RefBattleCP cp, bool* hasFriendly)
{
   ASSERT(srcHex != destHex);

   const SWORD dy = destHex.y() - srcHex.y();
   const SWORD dx = destHex.x() - srcHex.x();

   UWORD dist = static_cast<UWORD>(sqrt(squared(dx) + squared(dy)));

   // we can always see / fire into next hex
   if(dist <= 1)
      return True;

   if(dist > 36)
      return False;

   const BattleTerrainHex& startHexInfo = bd->getTerrain(srcHex);
   const BattleTerrainHex& destHexInfo = bd->getTerrain(destHex);

   HexCord hex = srcHex;
   const int incY = (dy >= 0) ? 1 : -1;
   const int incX = (dx >= 0) ? 1 : -1;
   UWORD xs = (dx >= 0) ? dx : -dx;
   UWORD ys = (dy >= 0) ? dy : -dy;

   HexCord tHex;
// int count = 0;
   while(ys || xs) //hex != destHex)
   {
      // get next hex
      tHex = hex;
      getNextHexLOS(bd, hex, tHex, xs, ys, incX, incY);

      // we can always see into the last hex
      if( /*(count++ > 0) &&*/ (xs || ys) )
      {
         //---------------------------------------
         //    const BattleTerrainHex& lastHexInfo = bd->getTerrain(hex);
         const BattleTerrainHex& thisHexInfo = bd->getTerrain(tHex);

         struct LOSStruct {
            UBYTE d_seeThrough; // how many hexes of this type of terrain can we see through?
            UBYTE d_height;     // height in meters of this terrain.
         };

         static const LOSStruct s_losTable[GT_HowMany] = {
            { 0,           0  }, // GT_None  shouldn't be used
            { UBYTE_MAX, 0  }, // GT_Grass
            { UBYTE_MAX, 0  }, // GT_Water
            { 4,              5  }, // GT_Orchard
            { 4,              3  }, // GT_Vineyard
            { 2,           10 }, // GT_LightWood
            { 1,           30 }, // GT_DenseWood
            { UBYTE_MAX, 0  },// GT_PloughedField
            { 4,         2  }, // GT_CornField
            { 4,         2  }, // GT_WheatField
            { 3,         1  }, // GT_Rough
            { UBYTE_MAX, 0  }, // GT_Marsh
            { 1,         20 }, // GT_Town
            { 2,         15 }, // GT_Village
            { UBYTE_MAX, 0  }, // GT_Farm
            { UBYTE_MAX, 0  }  // GT_Cemetary
         };

         ASSERT(thisHexInfo.d_terrainType > 0);
         ASSERT(thisHexInfo.d_terrainType < GT_HowMany);

         if(cp != NoBattleCP && hasFriendly)
         {
            if(!BobUtility::unitsUnderHex(bd, tHex, cp, BobUtility::Friendly, True))
               *hasFriendly = True;
         }

         int maxThis = (hasFriendly && *hasFriendly) ? 7 : 0; // can we shoot over friendly head
         int eyeLevel = startHexInfo.d_height + 2; // 6 ft tall human
         int tHexHeight = thisHexInfo.d_height + maximum(maxThis, s_losTable[thisHexInfo.d_terrainType].d_height);
         int destHeadLevel = destHexInfo.d_height + 2; // 6 ft tall human

         // first. a quick check for direct elevation obstruction
         if(eyeLevel < tHexHeight && tHexHeight > destHeadLevel)
            return False;

         bool canSeeOver = False;
         // if we are higher than top of this hex
         if(eyeLevel > tHexHeight)
         {
            // if top of this hex is higher than
//          if( (abValue(eyeLevel - destHeadLevel) > 2) &&
            if(tHexHeight > destHeadLevel) //)
            {
               int h1 = eyeLevel - tHexHeight;
               int d1 = getDist(srcHex, tHex) * BattleMeasure::XYardsPerHex;
               Wangle startToHereAngle = direction(d1, h1);

               int h2 = tHexHeight - destHeadLevel;
               int d2 = getDist(tHex, destHex) * BattleMeasure::XYardsPerHex;
               Wangle hereToThereAngle = direction(d2, h2);

               if(startToHereAngle < hereToThereAngle)
                  return False;
            }

            // we can see over
         }

         // or we are lower than top of this hex
         else
         {
            int h1 = tHexHeight - eyeLevel;
            int d1 = getDist(srcHex, tHex) * BattleMeasure::XYardsPerHex;
            Wangle startToHereAngle = direction(d1, h1);

            int h2 = destHeadLevel - tHexHeight;
            int d2 = getDist(tHex, destHex) * BattleMeasure::XYardsPerHex;
            Wangle hereToThereAngle = direction(d2, h2);

            if(startToHereAngle > hereToThereAngle)
               return False;
         }
      }

      // update hex
      hex = tHex;

      // add to list if not NULL
      if(hl)
         hl->newItem(hex);
   }

   ASSERT(hex == destHex);

   return True;
}



#endif






/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.3  2003/02/15 14:42:16  greenius
 * Fixed a possible deadlock situation when starting a thread.
 * Tidied up source code, and removed using namespace from header file.
 *
 * Revision 1.2  2001/06/18 08:25:43  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
