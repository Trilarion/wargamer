/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef UNITDISP_HPP
#define UNITDISP_HPP

#ifndef __cplusplus
#error unitdisp.hpp is for use with C++
#endif

#include "batcord.hpp"
#include "bobdef.hpp"
#include "unittype.hpp"
#include "initunit.hpp"     // for the InfantryPerSP definitions

namespace SWG_Sprite { class SpriteLibrary; }



/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *      Display Units on Battlefield
 *
 *----------------------------------------------------------------------
 */

class DrawDIB;
class HexIterator;
class BattleData;

using namespace BOB_Definitions;
using namespace BattleMeasure;









        /*
        These constants were originally declared in virtual unitdraw function
        They are now here
        */



        static const int InfantryPerSprite  = BatUnitUtils::InfantryPerSprite;
        static const int CavalryPerSprite   = BatUnitUtils::CavalryPerSprite;
        static const int ArtilleryPerSprite = BatUnitUtils::ArtilleryPerSprite;
        static const int SpecialPerSprite   = BatUnitUtils::SpecialPerSprite;

        static const int InfantryPerSP  = BatUnitUtils::InfantryPerSP; 
        static const int CavalryPerSP   = BatUnitUtils::CavalryPerSP;  
        static const int ArtilleryPerSP = BatUnitUtils::ArtilleryPerSP;
        static const int SpecialPerSP   = BatUnitUtils::SpecialPerSP;  

        static const int imagesPerSP[BasicUnitType::HowMany] = {
            InfantryPerSP / InfantryPerSprite,      // Infantry (16)
            CavalryPerSP / CavalryPerSprite,        // Cavalry (8)
            ArtilleryPerSP / ArtilleryPerSprite,    // Artillery (4)
            SpecialPerSP / SpecialPerSprite         // Special (16)
        };

        static const int Half_Infantry_Numbers = imagesPerSP[0] / 2;

        // Leave 10% of hex for edges
        static const BattleMeasure::Distance usedHexWidth = (BattleMeasure::OneXHex * 9) / 10;
        
        static const int InfantryFrontage  = InfantryPerSP / 2; // Number of men who can squeeze in a hex width
        static const int CavalryFrontage   = CavalryPerSP / 2;  // Number of men who can squeeze in a hex width
        static const int ArtilleryFrontage = ArtilleryPerSP;            // Number of men who can squeeze in a hex width
        static const int SpecialFrontage   = SpecialPerSP / 2;  // Number of men who can squeeze in a hex width

        static const BattleMeasure::Distance spacings[BasicUnitType::HowMany] = {
                ( (usedHexWidth * InfantryPerSprite) / InfantryFrontage) + 100,     // Infantry
                (usedHexWidth * CavalryPerSprite)   / CavalryFrontage + 600,  // Cavalry
                (usedHexWidth * ArtilleryPerSprite) / ArtilleryFrontage,        // Artillery
                (usedHexWidth * SpecialPerSprite)       / SpecialFrontage       // Special
        };











/*
Just for reference - these are the 5 valid troop formations :

        SP_MarchFormation,
        SP_ColumnFormation,
        SP_ClosedColumnFormation,
        SP_LineFormation,
        SP_SquareFormation
*/


// we'll assume that a maximum of 40 troop sprites are going to be displayed for each SP
#define MAX_TROOPS    25
#define MAX_GRAPHICSPERFORMATION 4

#define MAX_FORMATIONS 5
#define MAX_FACINGS 12
#define MAX_UNITTYPES 5

/*
This structure holds precalculated positions of every troop, formation & direction
Currently occupies just over 10k
Could feasibly be extended to provide the movement direction a troop has to go
to reach its corresponding position in another formation / direction
*/

enum SPTypesEnum {
    SPTYPE_INFANTRY,
    SPTYPE_CAVALRY,
    SPTYPE_FOOT_ARTILLERY,
    SPTYPE_HORSE_ARTILLERY,
    SPTYPE_COMMAND_POSITION,

    MAX_SP_TYPES
};

struct UnitPointInfo {
    BattleLocation pos;
    char graphic_number;
};


struct UnitFacingInfo {
    HexPosition::Facing dir[MAX_GRAPHICSPERFORMATION];
};    


enum FacingMode {
    NORMAL,
    MIRRORED
};

enum GraphicIndexEnum {
    Index_0,
    Index_1,
    Index_2,
    Index_3,
    Index_Random
};



int FacingToIndex(HexPosition::Facing facing);
HexPosition::Facing IndexToFacing(int index);
HexPosition::Facing RandomFacing(void);

struct UnitFormationPointsType {


    /*
    Tables and Accessors for Formation Position Info
    */

    int NumberOfUnits[MAX_FORMATIONS][MAX_UNITTYPES];
    int GetNumUnits(SPFormation formation_type, SPTypesEnum SPType) { return NumberOfUnits[formation_type][SPType]; }
    void SetNumUnits(SPFormation formation_type, SPTypesEnum SPType, int num) { NumberOfUnits[formation_type][SPType] = num; }


   
    // this is in the format [FormationType][FacingDirection][BasicUnitType][TroopNumber] to return graphic indexes for each troop in a formation
    UnitPointInfo Points[MAX_FORMATIONS][MAX_FACINGS][MAX_SP_TYPES][MAX_TROOPS];

    // this will return the battle location of a given unit, in a given formation, at a given direction !
    UnitPointInfo * GetUnitPoint(SPFormation formation_type, HexPosition::Facing facing, SPTypesEnum SPType, int unit_no, FacingMode mode) {
        // make sure we're dealing with a valid formation
        ASSERT((int)formation_type < MAX_FORMATIONS);
        // make sure we're dealing with a valid troop number
        ASSERT(unit_no >= 0 && unit_no < MAX_TROOPS);
        // alter unit_no index, depending on normal or mirrored mode
        if(mode == MIRRORED) unit_no = (MAX_TROOPS - unit_no);
        // return the correctly facing point (had to switch on this cause HexPosition::Facing is a UBYTE)
        return &Points[formation_type][FacingToIndex(facing)][SPType][unit_no];
    }        

    void SetUnitPoint(SPFormation formation_type, HexPosition::Facing facing, SPTypesEnum SPType, int unit_no, BattleLocation pos, int graphic_number) {
        // make sure we're dealing with a valid formation
        ASSERT((int)formation_type < 5);
        // make sure we're dealing with a valid troop number
        ASSERT(unit_no >= 0 && unit_no < MAX_TROOPS);

        Points[(int)formation_type][FacingToIndex(facing)][SPType][unit_no].pos = pos;
        Points[(int)formation_type][FacingToIndex(facing)][SPType][unit_no].graphic_number = graphic_number;

    }        





    /*
    Tables and Accessors for Formation Facing Info (ie. for each of the different 4 graphics within a formation)
    */

            
    // this is in the format [FormationType][FacingDirection][BasicUnitType] to return troop facings for a given formation
    UnitFacingInfo Facings[MAX_FORMATIONS][MAX_FACINGS][MAX_UNITTYPES];

    UnitFacingInfo * GetUnitFacing(SPFormation formation_type, HexPosition::Facing facing, SPTypesEnum SPType, FacingMode mode) {
        // make sure we're dealing with a valid formation
        ASSERT((int)formation_type < MAX_FORMATIONS);
        return &Facings[formation_type][FacingToIndex(facing)][SPType];
    }

    // call the function below using all the same parameters
    void SetUnitFacing(SPFormation formation_type, HexPosition::Facing facing, SPTypesEnum SPType, HexPosition::Facing unit_facing) {
        SetUnitFacing(formation_type, facing, SPType, unit_facing, unit_facing, unit_facing, unit_facing); }
              
    void SetUnitFacing(SPFormation formation_type, HexPosition::Facing facing, SPTypesEnum SPType, HexPosition::Facing facing1, HexPosition::Facing facing2, HexPosition::Facing facing3, HexPosition::Facing facing4) {
        // make sure we're dealing with a valid formation
        ASSERT((int)formation_type < MAX_FORMATIONS);

        Facings[formation_type][FacingToIndex(facing)][SPType].dir[0] = facing1;
        Facings[formation_type][FacingToIndex(facing)][SPType].dir[1] = facing2;
        Facings[formation_type][FacingToIndex(facing)][SPType].dir[2] = facing3;
        Facings[formation_type][FacingToIndex(facing)][SPType].dir[3] = facing4;


    }


    BattleLocation InfantryRandomOffsets[200];
    void SetInfantryRandomOffset(int num, BattleLocation pos) { InfantryRandomOffsets[num] = pos; }
    BattleLocation & GetInfantryPositionOffset(int num) { return InfantryRandomOffsets[num]; }
    int GetInfantryXPositionOffset(int num) { return InfantryRandomOffsets[num].x(); }
    int GetInfantryYPositionOffset(int num) { return InfantryRandomOffsets[num].y(); }

    BattleLocation CavalryRandomOffsets[200];
    void SetCavalryRandomOffset(int num, BattleLocation pos) { CavalryRandomOffsets[num] = pos; }
    BattleLocation & GetCavalryPositionOffset(int num) { return CavalryRandomOffsets[num]; }
    int GetCavalryXPositionOffset(int num) { return CavalryRandomOffsets[num].x(); }
    int GetCavalryYPositionOffset(int num) { return CavalryRandomOffsets[num].y(); }

    BattleLocation ArtilleryRandomOffsets[200];
    void SetArtilleryRandomOffset(int num, BattleLocation pos) { ArtilleryRandomOffsets[num] = pos; }
    BattleLocation & GetArtilleryPositionOffset(int num) { return ArtilleryRandomOffsets[num]; }
    int GetArtilleryXPositionOffset(int num) { return ArtilleryRandomOffsets[num].x(); }
    int GetArtilleryYPositionOffset(int num) { return ArtilleryRandomOffsets[num].y(); }


    int GetUnitXPositionOffset(SPTypesEnum type, int num) {
        switch(type) {
            case SPTYPE_INFANTRY : return InfantryRandomOffsets[num].x();
            case SPTYPE_CAVALRY : return CavalryRandomOffsets[num].x();
            case SPTYPE_FOOT_ARTILLERY : return ArtilleryRandomOffsets[num].x();
            case SPTYPE_HORSE_ARTILLERY : return ArtilleryRandomOffsets[num].x();
            default : {
                FORCEASSERT("Oops - Invalid unit basicType when trying to get random position offsets (must be Infantry, Cavalry or Artillery)");
                return 0;
            }
        }
    }
            
    int GetUnitYPositionOffset(SPTypesEnum type, int num) {
        switch(type) {
            case SPTYPE_INFANTRY : return InfantryRandomOffsets[num].y();
            case SPTYPE_CAVALRY : return CavalryRandomOffsets[num].y();
            case SPTYPE_FOOT_ARTILLERY : return ArtilleryRandomOffsets[num].y();
            case SPTYPE_HORSE_ARTILLERY : return ArtilleryRandomOffsets[num].y();
            default :{
                FORCEASSERT("Oops - Invalid unit basicType when trying to get random position offsets (must be Infantry, Cavalry or Artillery)");
                return 0;
            }
        }
    }


            
    char FormationIndexes[20][MAX_SP_TYPES][MAX_TROOPS];
    void SetFormationIndex(int array_no, SPTypesEnum SPType, int index, char val) { FormationIndexes[array_no][SPType][index] = val; }
    char GetFormationIndex(int array_no, SPTypesEnum SPType, int index) { return FormationIndexes[array_no][SPType][index]; }




    BattleLocation InHexUnitPositions[HexPosition::Center_Center+1];
    void SetInHexPosition(HexPosition::MoveTo pos_enum, BattleLocation pos) { InHexUnitPositions[pos_enum] = pos; }
    BattleLocation & GetInHexPosition(HexPosition::MoveTo pos_enum) { return InHexUnitPositions[pos_enum]; }

    BattleLocation InHexUnitStepVals[HexPosition::Center_Center+1][HexPosition::Center_Center+1];
    void SetInHexUnitStepVal(HexPosition::MoveTo src_pos, HexPosition::MoveTo dest_pos, BattleLocation stepval) { InHexUnitStepVals[src_pos][dest_pos] = stepval; }
    BattleLocation GetInHexUnitStepVal(HexPosition::MoveTo src_pos, HexPosition::MoveTo dest_pos) { return InHexUnitStepVals[src_pos][dest_pos]; }

    float InHexUnitDistances[HexPosition::Center_Center+1][HexPosition::Center_Center+1];
    void SetInHexUnitDistances(HexPosition::MoveTo src_pos, HexPosition::MoveTo dest_pos, float val) { InHexUnitDistances[src_pos][dest_pos] = val; }
    float GetInHexUnitDistances(HexPosition::MoveTo src_pos, HexPosition::MoveTo dest_pos) { return InHexUnitDistances[src_pos][dest_pos]; }

};

















namespace BattleDisplay {

class Displayer;
//extern BattleMeasure::BattleTime::Tick LastTime;
//extern BattleMeasure::BattleTime::Tick NewTime;
//extern unsigned int FrameCounter;
// precalc points for a SP formations
extern UnitFormationPointsType UnitFormationPoints;
void PrecalcFormations(const BattleData * d_batData);
extern bool bFormationsPrecalced;


void PrecalcFacings(
                        SPFormation formation_type,
                        HexPosition::Facing formation_facing,
                        HexPosition::Facing unit_facings);

void PrecalcFacings(
                        SPFormation formation_type,
                        HexPosition::Facing formation_facing,
                        HexPosition::Facing unit1_facing,
                        HexPosition::Facing unit2_facing,
                        HexPosition::Facing unit3_facing,
                        HexPosition::Facing unit4_facing);

void PrecalcPoints(
                        SPFormation formation_type, // formation type
                        HexPosition::Facing formation_facing, // formation facing
                        SPTypesEnum SPType, // basic type of unit
                        int index_start, // start index to put data in [0...MAXUNITS]
                        BattleLocation& point, // point to rotate about origin
                        unsigned int sprite_number); // graphic index for units in this line (helps when drawing sprites) [0...MAX_GRAPHICSPERFORMATION]


void PrecalcLines(
                        SPFormation formation_type, // formation type
                        HexPosition::Facing formation_facing, // formation facing
                        SPTypesEnum SPType, // basic type of unit
                        int index_start, // start index to put data in [0...MAXUNITS]
                        const BattleLocation& centre, // centre of line
                        BattleMeasure::Distance hSpace, // horizontal spacing
                        BattleMeasure::Distance vSpace,  // vertical spacing
                        HexPosition::Facing line_facing, // facing normal to line
                        GraphicIndexEnum sprite_number, // graphic index for units in this line (helps when drawing sprites) [0...MAX_GRAPHICSPERFORMATION]
                        int nSprites, // total number of sprites
                        int nRows,
                        bool Offset); // number of rows to create


void PrecalcFormationIndexes(void);

void PrecalcInHexPositions(void);

void drawUnits(Displayer* display, const HexIterator& iter, const BattleData* batData, SWG_Sprite::SpriteLibrary* spriteLib);

};

#endif /* UNITDISP_HPP */

