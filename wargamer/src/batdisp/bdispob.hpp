/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef BDISPOB_HPP
#define BDISPOB_HPP

#ifndef __cplusplus
#error bdispob.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *      Some low level virtual base classes used to display things on the battlemap
 *
 *----------------------------------------------------------------------
 */

class DrawDIB;
class PixelPoint;

#include "refptr.hpp"

namespace BattleDisplay
{

/*
 * An object that can be drawn on the battlefield
 * the draw function should physically draw it.
 */

class DisplayObject : public RefBaseDel
{
        public:
                virtual ~DisplayObject() = 0;
                virtual void draw(DrawDIB* dib, const PixelPoint& p) const = 0;
};

/*
 * Something that can display objects
 * The draw function may physically draw it, or it may just
 * add the object to a list to be sorted and drawn later
 */

class Displayer
{
        public:
                virtual ~Displayer() = 0;
                virtual void draw(const RefPtr<DisplayObject>& ob, const PixelPoint& p) = 0;
};

/*
 * A simple displayer to draw directly to a DIB
 */

class DIBDisplayer : public Displayer
{
        public:
                DIBDisplayer(DrawDIB* dib) : d_dib(dib) { }
                virtual ~DIBDisplayer() { }
                virtual void draw(const RefPtr<DisplayObject>& ob, const PixelPoint& p) 
                {
                        ob->draw(d_dib, p); 
                }
        private:
                DrawDIB* d_dib;
};


inline DisplayObject::~DisplayObject() { }
inline Displayer::~Displayer() { }

};      // BattleDisplay

#endif /* BDISPOB_HPP */

