/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef HEXITER_HPP
#define HEXITER_HPP

#ifndef __cplusplus
#error hexiter.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *      Hex Iterator
 *
 *----------------------------------------------------------------------
 */

#include "batcord.hpp"
#include "dib.hpp"
#include "batdata.hpp"

// using namespace WG_BattleData;


// namespace BattleDisplay {
// 
//     // scaling from hex to pixel coords
//     extern float X_HexScale;
//     extern float Y_HexScale;
// 
// };


class BattleMapDisplayInt
{
        public:
                virtual ~BattleMapDisplayInt() { }

                virtual RCPBattleData getBattleData() const = 0;
                virtual const BattleLocation& getCorner() const = 0;
                virtual PixelPoint locationToPixel(const BattleLocation& b) const = 0;
                virtual PixelPoint getPixelSize() const = 0;
                virtual const BattleLocation& getSize() const = 0;
                                         virtual PixelPoint hexPixelSize() const = 0; // not checked
};

class HexIterator
{

    public:
        HexIterator(const BattleMapDisplayInt* display);
        ~HexIterator();

        HexIterator(const HexIterator& it);
        HexIterator& operator=(const HexIterator& it);

        bool operator()() const;
        HexIterator& operator ++();

        void drawHexOutline(DrawDIB* dib, ColourIndex c);
        void drawFullHexOutline(DrawDIB * dib, ColourIndex c);

        LONG left() const { return d_x[XLeft]; }                                // Left pixel edge
        LONG midX() const { return d_x[XMid]; }                         // Centre pixel
        LONG right() const { return d_x[XRight]; }                      // Right pixel edge
        LONG width() const { return right() - left(); }

        LONG bottom() const { return d_y[YBottom]; }
        LONG lowCorner() const { return d_y[YLowCorner]; }              // Bottom Corner
        LONG midY() const { return d_y[YMid]; }                         // MidPoint
        LONG topCorner() const { return d_y[YTopCorner]; }              // Top Corner
        LONG top() const { return d_y[YTop]; }                          // Top of Hex
        LONG height() const { return bottom() - top(); }

        Rect<LONG> squareRect() const;
                // Get rectangle representing aproximate hex
                // This goes from midway between the real top/bottom
                // and the corners.


        const BattleMeasure::HexCord& hex() const { return d_hRow; }
        BattleMeasure::HexCord& hex() { return d_hRow; }

        const BattleMeasure::HexCord& hexSize() const { return d_hexSize; }

        typedef Point<float> Scale;
        Scale scale() const;
            // Scaling of X and Y
        // Scale xScale() const;
            // Scaling only based on width/X to preserve aspect ratio
        Scale troopScale() const;
            // Scaling for dsplaying troops
        Scale buildingScale() const;
        Scale fxScale() const;
        static Point<int> hexGraphicSize();

        // Scale battleScale() const;
        PixelPoint position(const BattleLocation& pos) const;

    private:
        void nextY();
        void nextX();
        void initX();
        bool iterateY();
        // void getHeight();

        void calcScale() const;
        static void HexIterator::calcRatios();

        void copy(const HexIterator& it);

    private:
         
        const BattleMapDisplayInt* d_display;

        bool                    d_finished;

        BattleMeasure::HexCord          d_hex;

        LONG d_yNum;
        LONG d_yDen;
        LONG d_yDiff;
        LONG d_yRem;
        LONG d_yRemainder;

        LONG d_xNum;
        LONG d_xDen;
        LONG d_xDiff;
        LONG d_xRem;
        LONG d_xRemainder;
        LONG d_xStart;

        BattleMeasure::HexCord          d_hexSize;                      // Size of battlefield in hexes
        BattleMeasure::HexCord          d_hRow;                         // Current Hex Coordinate

        enum {
                XLeft,
                XMid,
                XRight,
                XHowMany
        };

        enum {
                YBottom,
                YLowCorner,
                YMid,
                YTopCorner,
                YTop,
                YHowMany
        };

        LONG                    d_x[XHowMany];
        LONG                    d_y[YHowMany];

        mutable Scale d_battleScale;
        mutable Scale d_scale;
        mutable Scale d_troopScale;
        mutable Scale d_fxScale;
        mutable Scale d_buildScale;

        mutable bool d_scaleCalculated;

        static float s_troopRatio;
        static float s_fxRatio;
        static float s_buildRatio;
        static Point<int> s_hexGraphicSize;
        static bool s_ratioInitialised;
};

#endif /* HEXITER_HPP */

