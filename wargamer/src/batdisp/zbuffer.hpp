/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef ZBUFFER_HPP
#define ZBUFFER_HPP

#ifndef __cplusplus
#error zbuffer.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *	ZBuffer
 *
 *----------------------------------------------------------------------
 */

#include "myassert.hpp"

template<class T>
class ZBuffer
{
		int d_width;
		int d_height;
		T* d_data;

		static const T s_max;

	public:
		ZBuffer();
		~ZBuffer();

		void setSize(int w, int h);
			// Resize the buffer and clear it

		void clear();
			// Set all values to maximum

		bool update(int x, int y, T value);
			// If value <= existing value, then set it and return true
			// return false if value >= existing value

		bool isVisible(int x, int y, T value) const;
			// return true if value <= stored value

	private:
		T& get(int x, int y);
		const T& get(int x, int y) const;
};

/*
 * Implementation
 */


template<class T>
static const T ZBuffer<T>::s_max = static_cast<T>(-1);

template<class T>
ZBuffer<T>::ZBuffer() :
	d_width(0),
	d_height(0),
	d_data(0)
{
}

template<class T>
ZBuffer<T>::~ZBuffer()
{
	delete[] d_data;
}

template<class T>
void ZBuffer<T>::setSize(int w, int h)
{
	size_t total = w * h;

	if(total != (d_width * d_height) )
	{
		delete[] d_data;
		d_data = new T[total];
	}

	d_width = w;
	d_height = h;

	clear();
}

template<class T>
void ZBuffer<T>::clear()
{
	ASSERT(d_data != 0);

	size_t total = d_width * d_height;

	for(size_t i = 0; i < total; ++i)
		d_data[i] = s_max;
}

template<class T>
bool ZBuffer<T>::update(int x, int y, T value)
{
	T& cell = get(x, y);
	if(value <= cell)
	{
		cell = value;
		return true;
	}
	return false;
}

template<class T>
bool ZBuffer<T>::isVisible(int x, int y, T value) const
{
	return value <= get(x, y);
}

template<class T>
T& ZBuffer<T>::get(int x, int y)
{
	ASSERT(x < d_width);
	ASSERT(x >= 0);
	ASSERT(y < d_height);
	ASSERT(y >= 0);
	ASSERT(d_data != 0);
	return d_data[x + y * d_width];
}

template<class T>
const T& ZBuffer<T>::get(int x, int y) const
{
	ASSERT(x < d_width);
	ASSERT(x >= 0);
	ASSERT(y < d_height);
	ASSERT(y >= 0);
	ASSERT(d_data != 0);
	return d_data[x + y * d_width];
}


#endif /* ZBUFFER_HPP */

