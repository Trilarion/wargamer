/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef BUILDISP_HPP
#define BUILDISP_HPP

#ifndef __cplusplus
#error buildisp.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *      Building Display
 *
 *----------------------------------------------------------------------
 */

#include "bdispob.hpp"
#include "batdata.hpp"
#include "bdispute.hpp"


namespace SWG_Sprite { class SpriteLibrary; }



class HexIterator;



namespace BattleBuildings
{
        class BuildingList;
        class BuildingTable;
        class EffectsTable;
};

using namespace BattleBuildings;

namespace BattleDisplay
{
        class Displayer;
};

using namespace BattleDisplay;

class DrawDIB;

class BuildingDisplay
{
        public:
                static void drawBuildings(Displayer * d_display, const HexIterator& iter, const BattleData * batData, SWG_Sprite::SpriteLibrary* sprLib);

				BuildingDisplay::BuildingDisplay(void) {
					BattleDisplay::BuildingsLastTime = 0;
				}
};



#endif /* BUILDISP_HPP */

