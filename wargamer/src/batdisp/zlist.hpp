/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef ZLIST_HPP
#define ZLIST_HPP

#ifndef __cplusplus
#error zlist.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *      Sorted list of objects, sorted by Y-Cordinate
 * so that objects on the display can be displayed from top to bottom
 *
 *----------------------------------------------------------------------
 */

#include <map>
// #includp.h>
#include "grtypes.hpp"
#include "bdispob.hpp"
#include "refptr.hpp"
#include "bdispob.hpp"


// Stupid function required by STL so that multimap can be used

inline bool operator < (const RefPtr<BattleDisplay::DisplayObject>& l, const RefPtr<BattleDisplay::DisplayObject>& r)
{
        return l.value() < r.value();
}

inline bool operator < (const PixelPoint& l, const PixelPoint& r)
{
        return (l.y() == r.y()) ? (l.x() < r.x()) : (l.y() < r.y());
}

struct YComparePixelPoint : public std::binary_function<PixelPoint, PixelPoint, bool>
{
        bool operator()(const PixelPoint& l, const PixelPoint& r) const
        {
                return l.y() < r.y();
        }
};

namespace BattleDisplay
{

class ZDisplayObjectList : public Displayer
{
         typedef std::multimap<PixelPoint, RefPtr<DisplayObject>, YComparePixelPoint > Container;
        public:
                virtual ~ZDisplayObjectList();
                virtual void draw(const RefPtr<DisplayObject>& ob, const PixelPoint& p);
                        // Add object to list

                void draw(DrawDIB* dib) const;
                        // Draw all objects to dib

                void clear();
                        // Delete all objects

                void drawAndClear(DrawDIB* dib)
                {
                        draw(dib);
                        clear();
                }

        private:
                Container d_items;
};


};      // namespace BattleDisplay

#endif /* ZLIST_HPP */

