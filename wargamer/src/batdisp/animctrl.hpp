/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef ANIMCTRL_HPP
#define ANIMCTRL_HPP

#include "random.hpp"
#include "fx.hpp"

/*-------------------------------------------------------------------------------------------------------------------

        File : ANIMCTRL
        Description : Classes for controling animation sequences
        
-------------------------------------------------------------------------------------------------------------------*/

enum SpriteActionEnum {

    // basic actions for infantry
    NONE,
    STANDING,
    WALKING,
    RUNNING,
    CHARGING,
    SHOOTING,
    STABBING,
    UNUSED2,
    
    DEAD,
    MAX_SPRITE_ACTIONS,
            
    // overwritten actions for cavalry
    TROTTING = RUNNING,
    GALLOPING = STABBING,
    SWORDOUT = SHOOTING,
    MELEE = UNUSED2,

    // overwritten actions for atrillery
    LIMBERING = RUNNING,
    UNLIMBERING = CHARGING,
    STANDING_LIMBERED = STANDING,
    STANDING_UNLIMBERED = STABBING,

    // overwritten actions for officers
    ORDERING = UNUSED2

};

enum SpriteTypeEnum {

    SPRITE_NONE,
    SPRITE_INFANTRY,
    SPRITE_CAVALRY,
    SPRITE_FOOT_ARTILLERY,
    SPRITE_HORSE_ARTILLERY,
    SPRITE_OFFICER,
    SPRITE_FLAG,
    SPRITE_FOOTMAN,
    SPRITE_HORSEMAN

};



// flags (as in nationality flags)
enum FlagTypeEnum {

    FLAG_NORMAL,
    FLAG_FLAPPING,

    MAX_FLAGTYPES

};



#define INFANTRY_TOTALFRAMES 35
#define CAVALRY_TOTALFRAMES 34
#define FOOTARTILLERY_TOTALFRAMES 36
#define HORSEARTILLERY_TOTALFRAMES 36
#define OFFICER_TOTALFRAMES 33
#define FOOTMAN_TOTALFRAMES 14
#define HORSEMAN_TOTALFRAMES 20

#define FLAG_TOTALFRAMES 4

#define MAX_FRAMES_PER_SEQUENCE 40

#define ANIMCUE_NONE 0
#define ANIMCUE_ENDOFSEQUENCE 1
#define ANIMCUE_MUSKETFIRE 2
#define ANIMCUE_CANNONFIRE 4
#define ANIMCUE_EXPLOSION 8

struct FrameSequence {

    // how many frames are in sequence
    unsigned int NumFrames;
    // wether this sequence loops at end    
    bool LoopFlag;
    // index of frame
    unsigned int FrameIndex[MAX_FRAMES_PER_SEQUENCE];
    // delay for this frame
    unsigned int FrameDelay[MAX_FRAMES_PER_SEQUENCE];
    // additional cues which need to be sync'd to animations (eg. sounds & fx)
    unsigned char FrameFlag[MAX_FRAMES_PER_SEQUENCE];

};




class AnimationControl {

private:

    FrameSequence InfantryAnimations[MAX_SPRITE_ACTIONS];
    FrameSequence CavalryAnimations[MAX_SPRITE_ACTIONS];
    FrameSequence FootArtilleryAnimations[MAX_SPRITE_ACTIONS];
    FrameSequence HorseArtilleryAnimations[MAX_SPRITE_ACTIONS];
    FrameSequence OfficerAnimations[MAX_SPRITE_ACTIONS];
    FrameSequence EffectsAnimations[MAX_EFFECTS];
    FrameSequence FlagAnimations[MAX_FLAGTYPES];
    FrameSequence FootManAnimations[MAX_SPRITE_ACTIONS];
    FrameSequence HorseManAnimations[MAX_SPRITE_ACTIONS];

public:

    AnimationControl(void);
    ~AnimationControl(void);

    inline unsigned int GetInfantryFrame(SpriteActionEnum action, unsigned int index) {
        return InfantryAnimations[action].FrameIndex[index]; }

	unsigned int GetNumInfantryFrames(SpriteActionEnum action) { return InfantryAnimations[action].NumFrames; }

    inline unsigned int GetCavalryFrame(SpriteActionEnum action, unsigned int index) {
        return CavalryAnimations[action].FrameIndex[index]; }

	unsigned int GetNumCavalryFrames(SpriteActionEnum action) { return CavalryAnimations[action].NumFrames; }

    inline unsigned int GetFootArtilleryFrame(SpriteActionEnum action, unsigned int index) {
        return FootArtilleryAnimations[action].FrameIndex[index]; }

	unsigned int GetNumFootArtilleryFrames(SpriteActionEnum action) { return FootArtilleryAnimations[action].NumFrames; }

    inline unsigned int GetHorseArtilleryFrame(SpriteActionEnum action, unsigned int index)  {
        return HorseArtilleryAnimations[action].FrameIndex[index]; }

	unsigned int GetNumHorseArtilleryFrames(SpriteActionEnum action) { return HorseArtilleryAnimations[action].NumFrames; }
        
    inline unsigned int GetOfficerFrame(SpriteActionEnum action, unsigned int index) {
        return OfficerAnimations[action].FrameIndex[index]; }

	unsigned int GetNumOfficerFrames(SpriteActionEnum action) { return OfficerAnimations[action].NumFrames; }

    inline unsigned int GetEffectFrame(EffectTypeEnum action, unsigned int index) {
        return EffectsAnimations[action].FrameIndex[index]; }

	unsigned int GetNumEffectFrames(SpriteActionEnum action) { return EffectsAnimations[action].NumFrames; }

    inline unsigned int GetFlagFrame(FlagTypeEnum action, unsigned int index) {
        return FlagAnimations[action].FrameIndex[index]; }

	unsigned int GetNumFlagFrames(SpriteActionEnum action) { return FlagAnimations[action].NumFrames; }

    inline unsigned int GetFootManFrame(SpriteActionEnum action, unsigned int index) {
        return FootManAnimations[action].FrameIndex[index]; }

	unsigned int GetNumFootManFrames(SpriteActionEnum action) { return FootManAnimations[action].NumFrames; }

    inline unsigned int GetHorseManFrame(SpriteActionEnum action, unsigned int index) {
        return HorseManAnimations[action].FrameIndex[index]; }

	unsigned int GetNumHorseManFrames(SpriteActionEnum action) { return HorseManAnimations[action].NumFrames; }



    unsigned char UpdateInfantryFrame(SpriteActionEnum action, unsigned int &index, unsigned int &timer, int speed, bool RandomizeTimer);

    unsigned char UpdateCavalryFrame(SpriteActionEnum action, unsigned int &index, unsigned int &timer, int speed);

    unsigned char UpdateFootArtilleryFrame(SpriteActionEnum action, unsigned int &index, unsigned int &timer, int speed);

    unsigned char UpdateHorseArtilleryFrame(SpriteActionEnum action, unsigned int &index, unsigned int &timer, int speed);

    unsigned char UpdateOfficerFrame(SpriteActionEnum action, unsigned int &index, unsigned int &timer, int speed);

    unsigned char UpdateEffectFrame(EffectTypeEnum action, unsigned int &index, unsigned int &timer, int speed);

    unsigned char UpdateFlagFrame(FlagTypeEnum action, unsigned int &index, unsigned int &timer, int speed);

    unsigned char UpdateFootManFrame(SpriteActionEnum action, unsigned int &index, unsigned int &timer, int speed);

    unsigned char UpdateHorseManFrame(SpriteActionEnum action, unsigned int &index, unsigned int &timer, int speed);
 
};

// global static definition
static AnimationControl s_AnimationControl;


#endif











