/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/


#include "stdinc.hpp"
#include "fx_disp.hpp"

/*-------------------------------------------------------------------------------------------------------------------

        File : FX_DISP
        Description : Classes for displaying spot effects on battlefield
        
-------------------------------------------------------------------------------------------------------------------*/


using namespace BattleDisplay;


namespace BattleDisplay {

            BattleMeasure::BattleTime::Tick d_LastTick;

    // static const char d_EffectsSpriteFileName[] = "nap1813\\troops";
    // SWG_Sprite::SpriteLibrary d_EffectsSprites(d_EffectsSpriteFileName);

}




void
BattleEffectDisplayObject::draw(DrawDIB* dib, const PixelPoint& p) const 
{
#if 0
        ASSERT(d_height > 0);
        PixelPoint fullSize = d_sprite->fullSize();
        ASSERT(fullSize.x() != 0);
        int width = (d_height * fullSize.x() + fullSize.y() - 1) / fullSize.y();

        PixelRect rect(p.x(), p.y(), p.x() + width, p.y() + d_height);
        BattleDisplayUtility::drawSquashedGraphic(d_sprite, d_sprite, dib, rect, PixelPoint(0,0));
#else
        BattleDisplayUtility::drawSquashedGraphic(d_sprite, dib, p, d_scale);
#endif
}







void
BattleDisplay::drawEffects(Displayer * d_display, const HexIterator& iter, CPBattleData batData, SWG_Sprite::SpriteLibrary* sprites) {


    EffectsList * list = batData->effectsList();

    HexEffectsInfo * hex_effects = list->get(iter.hex() );

    if(hex_effects == NULL) return;

    // get the list of effects in this hex
    HexEffectsInfo::EffectsInHex & effects_list = hex_effects->Effects();

    // traverse through list
    for(HexEffectsInfo::EffectsInHex::iterator fx_iter = effects_list.begin(); fx_iter != effects_list.end(); ++fx_iter) {

        // get effect from list
        EffectDisplayInfo * effect = &*fx_iter;

       /*
      Check wether to update frame
      */

      unsigned int DeltaFrames = (BattleDisplay::FrameCounter - effect->LastTime);
      effect->LastTime = BattleDisplay::FrameCounter;

      // modify by gametime to realtime ratio
      int UpdateAnim = DeltaFrames * (batData->timeRatio()<<1);

      /*
      Display
      */
      SWG_Sprite::SpriteLibrary::Index gIndex;
      gIndex = effect->SpriteIndex + s_AnimationControl.GetEffectFrame(effect->Type, effect->FrameCounter);
      SWG_Sprite::SpriteBlockPtr sprite(sprites, gIndex);
      RefPtrBattleEffectDisplayObject fx_disp_obj(sprite, iter.fxScale());
      d_display->draw(fx_disp_obj, iter.position(effect->Pos));


      /*
      Process timing, sounds & deletion, etc.
      */
    
      unsigned char fx_flags = effect->Flags;
        
      unsigned char anim_flags;
      // TODO : make this take FrameSpeed - without mucking up flag animations
      anim_flags = s_AnimationControl.UpdateEffectFrame(effect->Type, effect->FrameCounter , effect->FrameTimer, UpdateAnim);

      // trigger sound...
      if(anim_flags & ANIMCUE_EXPLOSION) {
         batData->soundSystem()->triggerSound(GetExplosionSound(), BATTLESOUNDPRIORITY_MEDIUM, iter.hex() );
      }
        
      // decrease duration by one if end of sequence reached
      if(fx_flags & EFFECT_DURATION_REPEATS) { if(anim_flags & ANIMCUE_ENDOFSEQUENCE) effect->Duration--; }

      // decrease duration by one each cycle
      else if(fx_flags & EFFECT_DURATION_TIMED) { effect->Duration--; }

      if(effect->Duration <= 0) {
         effects_list.erase(fx_iter);
         fx_iter--;
      }

   }

}








/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.3  2001/11/26 10:34:41  greenius
 * Current Directory detection improved
 *
 * Battlegame sound system initialised properly
 *
 * BattleAI Message queue problems fixed
 *
 * Order of Battles loading
 *
 * HexMap uses reference counted units
 *
 * BattleEditor open-file dialogs use wrapped classes
 *
 * PathFind compiler warnings removed
 *
 * Orphan detection in BattleOB detected and removed
 *
 * Revision 1.2  2001/06/18 08:25:43  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
