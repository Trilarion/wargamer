/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
#include "stdinc.hpp"
#include "scr_opt.hpp"
#include "f_utils.hpp"
#include "options.hpp"
#include "dib.hpp"
#include "dib_blt.hpp"
#include "fonts.hpp"
#include "resstr.hpp"



OptionsScreenClass::OptionsScreenClass(HWND parent) {

   // just in case skill level settings weren't set up properly at constuction
   // CampaignOptions::setSkillLevel(CampaignOptions::getSkillLevel());

    // make sure that backdground & screen DIBs start out as NULL
    bkDIB = NULL;
    screenDIB = NULL;
   d_textBoxDIB = NULL;
   d_noviceDIB = NULL;
   d_normalDIB = NULL;
   d_expertDIB = NULL;
   d_plaqueDIB = NULL;


   bkDIB = FrontEndUtilsClass::loadBMPDIB("frontend\\LeatherScreen.bmp");
   ASSERT(bkDIB);
   d_noviceDIB = FrontEndUtilsClass::loadBMPDIB("frontend\\Novice.bmp");
   ASSERT(d_noviceDIB);
   d_normalDIB = FrontEndUtilsClass::loadBMPDIB("frontend\\Normal.bmp");
   ASSERT(d_normalDIB);
   d_expertDIB = FrontEndUtilsClass::loadBMPDIB("frontend\\Expert.bmp");
   ASSERT(d_expertDIB);
   d_textBoxDIB = FrontEndUtilsClass::loadBMPDIB("frontend\\DifficultyPaper.bmp");
   ASSERT(d_textBoxDIB);
   d_plaqueDIB = FrontEndUtilsClass::loadBMPDIB("frontend\\Plaque.bmp");
   ASSERT(d_plaqueDIB);

   d_optionsWindow = NULL;

   d_darkenAmmount = 40;

   calculateWindowSizes();

    strcpy(ScreenNameString,"OptionsScreen");

    createWindow(
        WS_EX_LEFT,
        // GenericNoBackClass::className(),
        ScreenNameString,
        WS_CHILD | WS_CLIPSIBLINGS | WS_CLIPCHILDREN,
        d_mainRect.left(), d_mainRect.top(), d_mainRect.width(), d_mainRect.height(),
        parent,
        NULL
        // APP::instance()
   );

   ASSERT(getHWND());

    SetWindowText(getHWND(),ScreenNameString);

   // create the options window
   d_optionsWindow = new OptionsWindow(
      getHWND(),
      /*
      Note : Had to change these around cos, annoyingly, campaignOptionsSettings & gameOptionsSettings are the wrong way round
      */
      CAMPAIGN_OPTIONS, // | GENERAL_OPTIONS,
      4, // num columns
      &optionsRect,
      this);

   ASSERT(d_optionsWindow);

    // draw the background DIB to the screen DIB
    drawScreenDIB();

    // set draw state for this window
    ShowWindow(getHWND(), SW_SHOW);
}




OptionsScreenClass::~OptionsScreenClass(void) {

   delete d_optionsWindow;
    d_optionsWindow = 0;

    selfDestruct();

    if(bkDIB) delete bkDIB;
    if(screenDIB) delete screenDIB;

   if(d_noviceDIB) delete d_noviceDIB;
   if(d_normalDIB) delete d_normalDIB;
   if(d_expertDIB) delete d_expertDIB;
   if(d_textBoxDIB) delete d_textBoxDIB;
   if(d_plaqueDIB) delete d_plaqueDIB;



}


void OptionsScreenClass::SettingsChanged(void) {

   drawScreenDIB();
   d_optionsWindow->reinit();
   InvalidateRect(d_optionsWindow->getHWND(), NULL, FALSE);
   InvalidateRect(getHWND(), NULL, FALSE);
}

inline void drawText(HDC hdc, const char* s, const PixelRect& r)
{
   DrawText(
      hdc,
      s,
      lstrlen(s),
      const_cast<RECT*>(&r),
      DT_SINGLELINE | DT_CENTER | DT_VCENTER
   );
}


void
OptionsScreenClass::calculateWindowSizes(void) {

   PixelRect r;
   GetClientRect(GetParent(getHWND()), &r);

   int width = r.right();
   int height = r.bottom();

   d_mainRect = PixelRect(r.left(), r.top(), r.right(), r.bottom() );

   // work out difficulty rect
   int xoffset = width / 16;
   int yoffset = height / 16;

   selectDifficultyRect.left = xoffset;
   selectDifficultyRect.right = width - (xoffset*2);
   selectDifficultyRect.top = yoffset;
   selectDifficultyRect.bottom = yoffset*2;

   // work out custom game rect
   xoffset = width / 16;
   yoffset = height / 16;

   customGameRect.left = xoffset;
   customGameRect.right = width - (xoffset*2);
   customGameRect.top = height/2 + (yoffset);
   customGameRect.bottom = yoffset;

   // work out options window rect
   xoffset = width / 16;
   yoffset = height / 16;

   optionsRect.left = xoffset;
   optionsRect.right = width - (xoffset*2);
   optionsRect.top = customGameRect.top + yoffset;
   optionsRect.bottom = height/2 - (yoffset*2);

   // work out novice, normal & expert rects
   float center_line = width / 4.0f;
   int half_rect = (center_line / 4.0f);// - (center_line / 8.0f);
   int rect_width = half_rect*2;//(center_line / 2.0f) - (center_line / 8.0f);

   noviceRect.left = (center_line*1.0f) - half_rect;
   noviceRect.right = rect_width;
   noviceRect.top = yoffset*4;
   noviceRect.bottom = yoffset*4;

   noviceTextRect.left = noviceRect.left;
   noviceTextRect.right = noviceRect.right;
   noviceTextRect.top = (noviceRect.top + noviceRect.bottom) + yoffset/4;
   noviceTextRect.bottom = yoffset/2;

   normalRect.left = (center_line*2.0f) - half_rect;
   normalRect.right = rect_width;
   normalRect.top = yoffset*4;
   normalRect.bottom = yoffset*4;

   normalTextRect.left = normalRect.left;
   normalTextRect.right = normalRect.right;
   normalTextRect.top = (normalRect.top + normalRect.bottom) + yoffset/4;
   normalTextRect.bottom = yoffset/2;

   expertRect.left = (center_line*3.0f) - half_rect;
   expertRect.right = rect_width;
   expertRect.top = yoffset*4;
   expertRect.bottom = yoffset*4;

   expertTextRect.left = expertRect.left;
   expertTextRect.right = expertRect.right;
   expertTextRect.top = (expertRect.top + expertRect.bottom) + yoffset/4;
   expertTextRect.bottom = yoffset/2;
}



void
OptionsScreenClass::drawScreenDIB(void) {

   int width = d_mainRect.right();
   int height = d_mainRect.bottom();

   if(!screenDIB) {
      screenDIB = new DrawDIBDC(width, height);
   }

   else if(screenDIB->getWidth() != width || screenDIB->getHeight() != height) {
      delete screenDIB;
      screenDIB = new DrawDIBDC(width, height);
   }

   /*
   setup a font for use
   */
   HDC hdc = screenDIB->getDC();

   int max_height = selectDifficultyRect.bottom * 2;

   const int minFontHeight = 10;
   int fontHeight = 15;
   fontHeight = (fontHeight * (max_height)) / FrontEndUtils.s_baseWidth;
   int h = maximum(fontHeight, minFontHeight) + 15;

   LogFont logFont;
   logFont.height(h);
   logFont.weight(FW_MEDIUM);
   logFont.face(scenario->fontName(Font_Bold));

   Fonts font;
   font.set(hdc, logFont);

   COLORREF color = RGB(0,0,0);//RGB(148,115,33);
   COLORREF oldColor = SetTextColor(hdc, color);

   SetBkMode(hdc, TRANSPARENT);


   // blit bkground to DIB
   DIB_Utility::stretchDIB(screenDIB,0,0,width,height,bkDIB,0,0,bkDIB->getWidth(),bkDIB->getHeight());


   // blit difficulty rect
   DIB_Utility::stretchDIB(
      screenDIB,
      selectDifficultyRect.left,
      selectDifficultyRect.top,
      selectDifficultyRect.right,
      selectDifficultyRect.bottom,
      d_textBoxDIB,
      0,0,d_textBoxDIB->getWidth(),d_textBoxDIB->getHeight());
   // blit custom game rect
   DIB_Utility::stretchDIB(
      screenDIB,
      customGameRect.left,
      customGameRect.top,
      customGameRect.right,
      customGameRect.bottom,
      d_textBoxDIB,
      0,0,d_textBoxDIB->getWidth(),d_textBoxDIB->getHeight());
   // blit novice rect
   DIB_Utility::stretchDIB(
      screenDIB,
      noviceRect.left,
      noviceRect.top,
      noviceRect.right,
      noviceRect.bottom,
      d_noviceDIB,
      0,0,d_noviceDIB->getWidth(),d_noviceDIB->getHeight());
   if(CampaignOptions::getSkillLevel() != CampaignOptions::Novice) {
      screenDIB->darkenRectangle(noviceRect.left, noviceRect.top, noviceRect.right, noviceRect.bottom, d_darkenAmmount);
   }
   // blit normal rect
   DIB_Utility::stretchDIB(
      screenDIB,
      normalRect.left,
      normalRect.top,
      normalRect.right,
      normalRect.bottom,
      d_normalDIB,
      0,0,d_normalDIB->getWidth(),d_normalDIB->getHeight());
   if(CampaignOptions::getSkillLevel() != CampaignOptions::Normal) {
      screenDIB->darkenRectangle(normalRect.left, normalRect.top, normalRect.right, normalRect.bottom, d_darkenAmmount);
   }
   // blit expert rect
   DIB_Utility::stretchDIB(
      screenDIB,
      expertRect.left,
      expertRect.top,
      expertRect.right,
      expertRect.bottom,
      d_expertDIB,
      0,0,d_expertDIB->getWidth(),d_expertDIB->getHeight());
   if(CampaignOptions::getSkillLevel() != CampaignOptions::Expert) {
      screenDIB->darkenRectangle(expertRect.left, expertRect.top, expertRect.right, expertRect.bottom, d_darkenAmmount);
   }
   // blit novice text rect
   DIB_Utility::stretchDIB(
      screenDIB,
      noviceTextRect.left,
      noviceTextRect.top,
      noviceTextRect.right,
      noviceTextRect.bottom,
      d_plaqueDIB,
      0,0,d_plaqueDIB->getWidth(),d_plaqueDIB->getHeight());
   if(CampaignOptions::getSkillLevel() != CampaignOptions::Novice) {
      screenDIB->darkenRectangle(noviceTextRect.left, noviceTextRect.top, noviceTextRect.right, noviceTextRect.bottom, d_darkenAmmount);
   }
   // blit normal text rect
   DIB_Utility::stretchDIB(
      screenDIB,
      normalTextRect.left,
      normalTextRect.top,
      normalTextRect.right,
      normalTextRect.bottom,
      d_plaqueDIB,
      0,0,d_plaqueDIB->getWidth(),d_plaqueDIB->getHeight());
   if(CampaignOptions::getSkillLevel() != CampaignOptions::Normal) {
      screenDIB->darkenRectangle(normalTextRect.left, normalTextRect.top, normalTextRect.right, normalTextRect.bottom, d_darkenAmmount);
   }
   // blit expert text rect
   DIB_Utility::stretchDIB(
      screenDIB,
      expertTextRect.left,
      expertTextRect.top,
      expertTextRect.right,
      expertTextRect.bottom,
      d_plaqueDIB,
      0,0,d_plaqueDIB->getWidth(),d_plaqueDIB->getHeight());
   if(CampaignOptions::getSkillLevel() != CampaignOptions::Expert) {
      screenDIB->darkenRectangle(expertTextRect.left, expertTextRect.top, expertTextRect.right, expertTextRect.bottom, d_darkenAmmount);
   }

   RECT textRect;
   textRect.left = selectDifficultyRect.left;
   textRect.right = selectDifficultyRect.left + selectDifficultyRect.right;
   textRect.top = selectDifficultyRect.top;
   textRect.bottom = selectDifficultyRect.top + selectDifficultyRect.bottom;

   drawText(hdc, InGameText::get(IDS_SelectDifficultyLevel), textRect);

   textRect.left = customGameRect.left;
   textRect.right = customGameRect.left + customGameRect.right;
   textRect.top = customGameRect.top;
   textRect.bottom = customGameRect.top + customGameRect.bottom;

   drawText(hdc, InGameText::get(IDS_CustomGameSettings), textRect);

   textRect.left = noviceTextRect.left;
   textRect.right = noviceTextRect.left + noviceTextRect.right;
   textRect.top = noviceTextRect.top;
   textRect.bottom = noviceTextRect.top + noviceTextRect.bottom;

   drawText(hdc, InGameText::get(IDS_Novice), textRect);

   textRect.left = normalTextRect.left;
   textRect.right = normalTextRect.left + normalTextRect.right;
   textRect.top = normalTextRect.top;
   textRect.bottom = normalTextRect.top + normalTextRect.bottom;

   drawText(hdc, InGameText::get(IDS_Normal), textRect);

   textRect.left = expertTextRect.left;
   textRect.right = expertTextRect.left + expertTextRect.right;
   textRect.top = expertTextRect.top;
   textRect.bottom = expertTextRect.top + expertTextRect.bottom;

   drawText(hdc, InGameText::get(IDS_Expert), textRect);

}




LRESULT
OptionsScreenClass::procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam) {

        switch(msg) {

//            HANDLE_MSG(hWnd, WM_CREATE, onCreate);
//            HANDLE_MSG(hWnd, WM_DESTROY, onDestroy);
//            HANDLE_MSG(hWnd, WM_COMMAND, onCommand);
            HANDLE_MSG(hWnd, WM_SIZE, onSize);
            HANDLE_MSG(hWnd, WM_PAINT, onPaint);
//            HANDLE_MSG(hWnd, WM_MOUSEMOVE, onMouseMove);
//            HANDLE_MSG(hWnd, WM_DRAWITEM, onDrawItem);
            HANDLE_MSG(hWnd, WM_LBUTTONDOWN, onLButtonDown);
            HANDLE_MSG(hWnd, WM_LBUTTONUP, onLButtonUp);
//            HANDLE_MSG(hWnd, WM_SETCURSOR, onSetCursor);

            default:
                return defProc(hWnd, msg, wParam, lParam);
        }
}



void
OptionsScreenClass::onSize(HWND hwnd, UINT state, int cx, int cy) {

   calculateWindowSizes();

   if(d_optionsWindow) d_optionsWindow->resize(&optionsRect);

    MoveWindow(getHWND(), d_mainRect.left(), d_mainRect.top(), d_mainRect.width(), d_mainRect.height(), TRUE);

    drawScreenDIB();
}





void
OptionsScreenClass::onPaint(HWND handle) {

   if(!screenDIB) return;


    PAINTSTRUCT ps;
    HDC hdc = BeginPaint(handle, &ps);
    HPALETTE oldPal = SelectPalette(hdc, Palette::get(), FALSE);
    RealizePalette(hdc);


    BitBlt(hdc,0,0,screenDIB->getWidth(),screenDIB->getHeight(),screenDIB->getDC(),0,0,SRCCOPY);


    SelectPalette(hdc, oldPal, FALSE);
    EndPaint(handle, &ps);
}



void
OptionsScreenClass::onLButtonDown(HWND hwnd, BOOL fDoubleClick, int x, int y, UINT keyFlags) {

   POINT pt;
   pt.x = x;
   pt.y = y;

   RECT imageRect;
   RECT textRect;

   // check for mouse down in novice rects
   imageRect.left = noviceRect.left;
   imageRect.top = noviceRect.top;
   imageRect.right = noviceRect.left + noviceRect.right;
   imageRect.bottom = noviceRect.top + noviceRect.bottom;

   textRect.left = noviceTextRect.left;
   textRect.top = noviceTextRect.top;
   textRect.right = noviceTextRect.left + noviceTextRect.right;
   textRect.bottom = noviceTextRect.top + noviceTextRect.bottom;

   if(PtInRect(&imageRect, pt) || PtInRect(&textRect, pt)) {

      d_mouseDownBox = Novice;
      return;
   }

   // check for mouse down in normal rects
   imageRect.left = normalRect.left;
   imageRect.top = normalRect.top;
   imageRect.right = normalRect.left + normalRect.right;
   imageRect.bottom = normalRect.top + normalRect.bottom;

   textRect.left = normalTextRect.left;
   textRect.top = normalTextRect.top;
   textRect.right = normalTextRect.left + normalTextRect.right;
   textRect.bottom = normalTextRect.top + normalTextRect.bottom;

   if(PtInRect(&imageRect, pt) || PtInRect(&textRect, pt)) {

      d_mouseDownBox = Normal;
      return;
   }

   // check for mouse down in expert rects
   imageRect.left = expertRect.left;
   imageRect.top = expertRect.top;
   imageRect.right = expertRect.left + expertRect.right;
   imageRect.bottom = expertRect.top + expertRect.bottom;

   textRect.left = expertTextRect.left;
   textRect.top = expertTextRect.top;
   textRect.right = expertTextRect.left + expertTextRect.right;
   textRect.bottom = expertTextRect.top + expertTextRect.bottom;

   if(PtInRect(&imageRect, pt) || PtInRect(&textRect, pt)) {

      d_mouseDownBox = Expert;
      return;
   }

   // no more mouse checks
   d_mouseDownBox = None;
   return;
}




void
OptionsScreenClass::onLButtonUp(HWND hwnd, int x, int y, UINT keyFlags) {

   POINT pt;
   pt.x = x;
   pt.y = y;

   RECT imageRect;
   RECT textRect;

   // check for mouse up in novice rects
   imageRect.left = noviceRect.left;
   imageRect.top = noviceRect.top;
   imageRect.right = noviceRect.left + noviceRect.right;
   imageRect.bottom = noviceRect.top + noviceRect.bottom;

   textRect.left = noviceTextRect.left;
   textRect.top = noviceTextRect.top;
   textRect.right = noviceTextRect.left + noviceTextRect.right;
   textRect.bottom = noviceTextRect.top + noviceTextRect.bottom;

   if(PtInRect(&imageRect, pt) || PtInRect(&textRect, pt)) {

      // if we clicked on novice originally
      if(d_mouseDownBox == Novice) {

         CampaignOptions::setSkillLevel(CampaignOptions::Novice);
         d_mouseDownBox = None;
         drawScreenDIB();
         d_optionsWindow->reinit();
         InvalidateRect(d_optionsWindow->getHWND(), NULL, FALSE);
         InvalidateRect(getHWND(), NULL, FALSE);
         return;
      }
   }

   // check for mouse up in normal rects
   imageRect.left = normalRect.left;
   imageRect.top = normalRect.top;
   imageRect.right = normalRect.left + normalRect.right;
   imageRect.bottom = normalRect.top + normalRect.bottom;

   textRect.left = normalTextRect.left;
   textRect.top = normalTextRect.top;
   textRect.right = normalTextRect.left + normalTextRect.right;
   textRect.bottom = normalTextRect.top + normalTextRect.bottom;

   if(PtInRect(&imageRect, pt) || PtInRect(&textRect, pt)) {

      // if we clicked on normal originally
      if(d_mouseDownBox == Normal) {

         CampaignOptions::setSkillLevel(CampaignOptions::Normal);
         d_mouseDownBox = None;
         drawScreenDIB();
         d_optionsWindow->reinit();
         InvalidateRect(d_optionsWindow->getHWND(), NULL, FALSE);
         InvalidateRect(getHWND(), NULL, FALSE);
         return;
      }
   }

   // check for mouse up in expert rects
   imageRect.left = expertRect.left;
   imageRect.top = expertRect.top;
   imageRect.right = expertRect.left + expertRect.right;
   imageRect.bottom = expertRect.top + expertRect.bottom;

   textRect.left = expertTextRect.left;
   textRect.top = expertTextRect.top;
   textRect.right = expertTextRect.left + expertTextRect.right;
   textRect.bottom = expertTextRect.top + expertTextRect.bottom;

   if(PtInRect(&imageRect, pt) || PtInRect(&textRect, pt)) {

      // if we clicked on normal originally
      if(d_mouseDownBox == Expert) {

         CampaignOptions::setSkillLevel(CampaignOptions::Expert);
         d_mouseDownBox = None;
         drawScreenDIB();
         d_optionsWindow->reinit();
         InvalidateRect(d_optionsWindow->getHWND(), NULL, FALSE);
         InvalidateRect(getHWND(), NULL, FALSE);
         return;
      }
   }

   return;
}













/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.3  2001/07/02 15:23:17  greenius
 * Battle, Campaign, Combined game and Campaign Editor now compile and run... mostly
 *
 * Revision 1.2  2001/06/18 08:25:45  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
