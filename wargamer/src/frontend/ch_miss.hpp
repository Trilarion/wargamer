/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef CH_MISS_HPP
#define CH_MISS_HPP

#ifndef __cplusplus
#error ch_miss.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *      FrontEnd choose mission screen.. used by choose campaign and choose battle
 *
 *----------------------------------------------------------------------
 */

#include "menubase.hpp"
#include "f_utils.hpp"
#include "BitmapWindow.hpp"

#if defined(DEBUG) && !defined(NOLOG)
#include "clog.hpp"
extern LogFileFlush ChooseMissionLog;
#endif


class ChooseMissionListWindowClass;


/*---------------------------------------------------------
 * InfoItem struct
 *
 * For holding each campaign files' CampaignInfo
 */

struct MissionItem   // : public SLink
{
//   SimpleString fileName;               // Campaign *.WGC file
//   SimpleString INFfileName;
//   MissionInfo info;

   StringPtr d_fileName;
   StringPtr d_map;
   StringPtr d_text;
   StringPtr d_description;
   StringPtr d_key0;
   StringPtr d_key1;

   MissionItem()
   {
   }

//    InfoItem(const char* fname, const char* mapName, const char* text, const char* description) :
//       d_fileName(fname),
//       d_map(mapName),
//       d_text(text),
//       d_description(description)
//    {
//       ASSERT(d_fileName);
//       ASSERT(d_map);
//       ASSERT(d_text);
//       ASSERT(d_description);
//    }

//   InfoItem(MissionInfo& i, const char* fname, const char * INFfname) :
//    info(i)
//    {
//       ASSERT(fname);
//       ASSERT(INFfname);
//       fileName = copyString(fname);
//       INFfileName = copyString(INFfname);
//    }
};


class MissionInfoList
{
   public:
      const MissionItem& operator[](int i) const { return d_items[i]; }
      void add(const char* infName);

      typedef std::vector<MissionItem> Container;

      typedef Container::const_iterator const_iterator;
      const_iterator begin() const { return d_items.begin(); }
      const_iterator end() const { return d_items.end(); }


   private:
      Container d_items;
};

// class InfoItemList : public vector<InfoItem>
// {
//    public:
//       InfoItemList()
//       {
//       }
//
//       ~InfoItemList()
//       {
//       }
//
//
//       InfoItem& operator [] (int i)
//       {
//          InfoItem* item = first();
//          while(i-- && item)
//          {
//             item = next(item);
//          }
//          ASSERT(item);
//          if(item == 0)
//             throw GeneralError();
//          return *item;
//       }
//
//
//   private:
//      // SList<InfoItem> d_items;
// };






// typedef InfoItemList MissionInfoList;


class ChooseMissionScreenClass : public MenuScreenBaseClass
{
        /*
         * Variables
         */

        private:


        // the windows in use in this screen
        ChooseMissionListWindowClass * d_listWindow;

		BitmapWindow * d_mapWindow;
		BitmapWindow * d_textWindow;

        // structure containing filenames of .wgc & .inf files
        const MissionItem* d_currentMission;

        // list of Missions
        MissionInfoList d_missionList;

        StringPtr* d_resultName;

        PixelRect d_lbRect;
        PixelRect d_tabRect;
        PixelRect d_statRect;

		// to fill in annoying little gaps betwen controls
		RECT d_bkRects[8];
		int d_numBkRects;

		DIB * d_mapDIB;
		DrawDIBDC * d_textDIB;
		DrawDIB * d_paperDIB;
		DrawDIB * d_leatherDIB;


        public:

        // currently selected Mission
        int selected_Mission;

        // child window IDs
        enum Child_IDs {
                ListWindow_ID
        };


        /*
         * Functions
         */

        public:

        ChooseMissionScreenClass(HWND parent, StringPtr* result);
        virtual ~ChooseMissionScreenClass();

        protected:
           // manipulation of Mission list
           MissionInfoList& GetMissionInfoList() { return d_missionList; }
           void currentMission(const MissionItem* currentItem);
           const MissionItem* currentMission(void) { return d_currentMission; }

           void makeMissionList(const char* ext);

                void init();

        private:

				void LoadMapDib(void);
				void DrawTextDib(void);

				DIB * loadBMPDIB(const char* name);

                void calculateSizes();
                void createListBox();

                // find all .wgc & .inf files
                virtual void constructMissionFilesList() = 0;
				virtual void setPalette() = 0;

                // parse the .inf file for Mission info
//                void ReadInfoFile(void);

                // update the display when a new Mission is selected
                void RefreshScreens(void);

                // message processing
                LRESULT procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
                BOOL onCreate(HWND hwnd, CREATESTRUCT* lpCreateStruct);
                void onCommand(HWND hwnd, int id, HWND hwndCtl, UINT codeNotify);
                void onSize(HWND hwnd, UINT state, int cx, int cy);
				void onPaint(HWND handle);
};



#endif /* CH_MISS_HPP */

