/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef FRONT_HPP
#define FRONT_HPP

#ifndef __cplusplus
#error front.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 * Front End Class Definition
 * Defines the FrontEndwindow, containing the NavigateBar and MenuScreen
 *
 *----------------------------------------------------------------------
 */

#include "gamebase.hpp"

#if defined(NO_WG_DLL)
    #define FRONT_DLL
#elif defined(EXPORT_FRONT_DLL)
    #define FRONT_DLL __declspec(dllexport)
#else
    #define FRONT_DLL __declspec(dllimport)
#endif



class FrontEndClass;

class FrontEnd : public GameBase {

  private:

	FrontEndClass* d_frontEnd;

  public:

	  // FrontEnd(HWND hwnd, const char* scenarioFile = 0);
	 FRONT_DLL FrontEnd(GameOwner* owner, const char* scenarioFile = 0);
	 FRONT_DLL ~FrontEnd();

     /*
      * Implement GameBase
      */

	 virtual void pauseGame(bool pauseMode) { }
	 virtual void addTime(SysTick::Value ticks) { }
	 // virtual void finishBattle() { }
     virtual bool saveGame(bool prompt) const { return false; }
     virtual void saveGame(const char* fileName) const { }

	 virtual void doNewGame(const char * current, SaveGame::FileFormat format);
	 virtual void syncSlave(SysTick::Value ticks);
	 virtual void timeSyncAcknowledged(SysTick::Value ticks);
	 virtual void slaveReady(void);

	  virtual void processBattleOrder(MP_MSG_BATTLEORDER * msg);
	  virtual void processCampaignOrder(MP_MSG_CAMPAIGNORDER *msg);
};



#endif /* FRONT_HPP */

