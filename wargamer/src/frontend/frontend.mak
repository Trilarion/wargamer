# Microsoft Developer Studio Generated NMAKE File, Based on frontend.dsp
!IF "$(CFG)" == ""
CFG=frontend - Win32 Debug
!MESSAGE No configuration specified. Defaulting to frontend - Win32 Debug.
!ENDIF 

!IF "$(CFG)" != "frontend - Win32 Release" && "$(CFG)" != "frontend - Win32 Debug"
!MESSAGE Invalid configuration "$(CFG)" specified.
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "frontend.mak" CFG="frontend - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "frontend - Win32 Release" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE "frontend - Win32 Debug" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE 
!ERROR An invalid configuration is specified.
!ENDIF 

!IF "$(OS)" == "Windows_NT"
NULL=
!ELSE 
NULL=nul
!ENDIF 

CPP=cl.exe
MTL=midl.exe
RSC=rc.exe

!IF  "$(CFG)" == "frontend - Win32 Release"

OUTDIR=.\..\..\exe
INTDIR=.\o\release
# Begin Custom Macros
OutDir=.\..\..\exe
# End Custom Macros

!IF "$(RECURSE)" == "0" 

ALL : "$(OUTDIR)\frontend.dll"

!ELSE 

ALL : "gamesup - Win32 Release" "system - Win32 Release" "gwind - Win32 Release" "$(OUTDIR)\frontend.dll"

!ENDIF 

!IF "$(RECURSE)" == "1" 
CLEAN :"gwind - Win32 ReleaseCLEAN" "system - Win32 ReleaseCLEAN" "gamesup - Win32 ReleaseCLEAN" 
!ELSE 
CLEAN :
!ENDIF 
	-@erase "$(INTDIR)\ch_miss.obj"
	-@erase "$(INTDIR)\f_utils.obj"
	-@erase "$(INTDIR)\front.obj"
	-@erase "$(INTDIR)\menubase.obj"
	-@erase "$(INTDIR)\navbar.obj"
	-@erase "$(INTDIR)\scr_batt.obj"
	-@erase "$(INTDIR)\scr_camp.obj"
	-@erase "$(INTDIR)\scr_last.obj"
	-@erase "$(INTDIR)\scr_load.obj"
	-@erase "$(INTDIR)\scr_main.obj"
	-@erase "$(INTDIR)\scr_opt.obj"
	-@erase "$(INTDIR)\scr_side.obj"
	-@erase "$(INTDIR)\tabctrl.obj"
	-@erase "$(INTDIR)\vc60.idb"
	-@erase "$(INTDIR)\vc60.pdb"
	-@erase "$(OUTDIR)\frontend.dll"
	-@erase "$(OUTDIR)\frontend.exp"
	-@erase "$(OUTDIR)\frontend.lib"
	-@erase "$(OUTDIR)\frontend.pdb"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

"$(INTDIR)" :
    if not exist "$(INTDIR)/$(NULL)" mkdir "$(INTDIR)"

CPP_PROJ=/nologo /MD /W3 /Gi /GR /GX /Zi /O2 /I "..\h" /I "..\..\res" /I "..\system" /I "..\gamesup" /I "..\ob" /I "..\campdata" /I "..\gwind" /D "NDEBUG" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "EXPORT_FRONTEND_DLL" /Fp"$(INTDIR)\frontend.pch" /YX"stdinc.hpp" /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /c 
MTL_PROJ=/nologo /D "NDEBUG" /mktyplib203 /win32 
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\frontend.bsc" 
BSC32_SBRS= \
	
LINK32=link.exe
LINK32_FLAGS=gdi32.lib user32.lib /nologo /dll /incremental:no /pdb:"$(OUTDIR)\frontend.pdb" /debug /machine:I386 /out:"$(OUTDIR)\frontend.dll" /implib:"$(OUTDIR)\frontend.lib" 
LINK32_OBJS= \
	"$(INTDIR)\ch_miss.obj" \
	"$(INTDIR)\f_utils.obj" \
	"$(INTDIR)\front.obj" \
	"$(INTDIR)\menubase.obj" \
	"$(INTDIR)\navbar.obj" \
	"$(INTDIR)\scr_batt.obj" \
	"$(INTDIR)\scr_camp.obj" \
	"$(INTDIR)\scr_last.obj" \
	"$(INTDIR)\scr_load.obj" \
	"$(INTDIR)\scr_main.obj" \
	"$(INTDIR)\scr_opt.obj" \
	"$(INTDIR)\scr_side.obj" \
	"$(INTDIR)\tabctrl.obj" \
	"$(OUTDIR)\gwind.lib" \
	"$(OUTDIR)\system.lib" \
	"$(OUTDIR)\gamesup.lib"

"$(OUTDIR)\frontend.dll" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

!ELSEIF  "$(CFG)" == "frontend - Win32 Debug"

OUTDIR=.\..\..\exe
INTDIR=.\o\debug
# Begin Custom Macros
OutDir=.\..\..\exe
# End Custom Macros

!IF "$(RECURSE)" == "0" 

ALL : "$(OUTDIR)\frontendDB.dll"

!ELSE 

ALL : "gamesup - Win32 Debug" "system - Win32 Debug" "gwind - Win32 Debug" "$(OUTDIR)\frontendDB.dll"

!ENDIF 

!IF "$(RECURSE)" == "1" 
CLEAN :"gwind - Win32 DebugCLEAN" "system - Win32 DebugCLEAN" "gamesup - Win32 DebugCLEAN" 
!ELSE 
CLEAN :
!ENDIF 
	-@erase "$(INTDIR)\ch_miss.obj"
	-@erase "$(INTDIR)\f_utils.obj"
	-@erase "$(INTDIR)\front.obj"
	-@erase "$(INTDIR)\menubase.obj"
	-@erase "$(INTDIR)\navbar.obj"
	-@erase "$(INTDIR)\scr_batt.obj"
	-@erase "$(INTDIR)\scr_camp.obj"
	-@erase "$(INTDIR)\scr_last.obj"
	-@erase "$(INTDIR)\scr_load.obj"
	-@erase "$(INTDIR)\scr_main.obj"
	-@erase "$(INTDIR)\scr_opt.obj"
	-@erase "$(INTDIR)\scr_side.obj"
	-@erase "$(INTDIR)\tabctrl.obj"
	-@erase "$(INTDIR)\vc60.idb"
	-@erase "$(INTDIR)\vc60.pdb"
	-@erase "$(OUTDIR)\frontendDB.dll"
	-@erase "$(OUTDIR)\frontendDB.exp"
	-@erase "$(OUTDIR)\frontendDB.ilk"
	-@erase "$(OUTDIR)\frontendDB.lib"
	-@erase "$(OUTDIR)\frontendDB.pdb"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

"$(INTDIR)" :
    if not exist "$(INTDIR)/$(NULL)" mkdir "$(INTDIR)"

CPP_PROJ=/nologo /MDd /W3 /Gm /Gi /GR /GX /ZI /Od /I "..\h" /I "..\..\res" /I "..\system" /I "..\gamesup" /I "..\ob" /I "..\campdata" /I "..\gwind" /D "_DEBUG" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "EXPORT_FRONTEND_DLL" /Fp"$(INTDIR)\frontend.pch" /YX"stdinc.hpp" /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /GZ /c 
MTL_PROJ=/nologo /D "_DEBUG" /mktyplib203 /win32 
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\frontend.bsc" 
BSC32_SBRS= \
	
LINK32=link.exe
LINK32_FLAGS=gdi32.lib user32.lib /nologo /dll /incremental:yes /pdb:"$(OUTDIR)\frontendDB.pdb" /debug /machine:I386 /out:"$(OUTDIR)\frontendDB.dll" /implib:"$(OUTDIR)\frontendDB.lib" /pdbtype:sept 
LINK32_OBJS= \
	"$(INTDIR)\ch_miss.obj" \
	"$(INTDIR)\f_utils.obj" \
	"$(INTDIR)\front.obj" \
	"$(INTDIR)\menubase.obj" \
	"$(INTDIR)\navbar.obj" \
	"$(INTDIR)\scr_batt.obj" \
	"$(INTDIR)\scr_camp.obj" \
	"$(INTDIR)\scr_last.obj" \
	"$(INTDIR)\scr_load.obj" \
	"$(INTDIR)\scr_main.obj" \
	"$(INTDIR)\scr_opt.obj" \
	"$(INTDIR)\scr_side.obj" \
	"$(INTDIR)\tabctrl.obj" \
	"$(OUTDIR)\gwindDB.lib" \
	"$(OUTDIR)\systemDB.lib" \
	"$(OUTDIR)\gamesupDB.lib"

"$(OUTDIR)\frontendDB.dll" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

!ENDIF 

.c{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.c{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<


!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("frontend.dep")
!INCLUDE "frontend.dep"
!ELSE 
!MESSAGE Warning: cannot find "frontend.dep"
!ENDIF 
!ENDIF 


!IF "$(CFG)" == "frontend - Win32 Release" || "$(CFG)" == "frontend - Win32 Debug"
SOURCE=.\ch_miss.cpp

"$(INTDIR)\ch_miss.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\f_utils.cpp

"$(INTDIR)\f_utils.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\front.cpp

"$(INTDIR)\front.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\inf_read.cpp
SOURCE=.\menubase.cpp

"$(INTDIR)\menubase.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\navbar.cpp

"$(INTDIR)\navbar.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\scr_batt.cpp

"$(INTDIR)\scr_batt.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\scr_bbrf.cpp
SOURCE=.\scr_camp.cpp

"$(INTDIR)\scr_camp.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\scr_cbrf.cpp
SOURCE=.\scr_last.cpp

"$(INTDIR)\scr_last.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\scr_load.cpp

"$(INTDIR)\scr_load.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\scr_main.cpp

"$(INTDIR)\scr_main.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\scr_opt.cpp

"$(INTDIR)\scr_opt.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\scr_side.cpp

"$(INTDIR)\scr_side.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\tabctrl.cpp

"$(INTDIR)\tabctrl.obj" : $(SOURCE) "$(INTDIR)"


!IF  "$(CFG)" == "frontend - Win32 Release"

"gwind - Win32 Release" : 
   cd "\src\sf\wargamer\src\gwind"
   $(MAKE) /$(MAKEFLAGS) /F .\gwind.mak CFG="gwind - Win32 Release" 
   cd "..\frontend"

"gwind - Win32 ReleaseCLEAN" : 
   cd "\src\sf\wargamer\src\gwind"
   $(MAKE) /$(MAKEFLAGS) /F .\gwind.mak CFG="gwind - Win32 Release" RECURSE=1 CLEAN 
   cd "..\frontend"

!ELSEIF  "$(CFG)" == "frontend - Win32 Debug"

"gwind - Win32 Debug" : 
   cd "\src\sf\wargamer\src\gwind"
   $(MAKE) /$(MAKEFLAGS) /F .\gwind.mak CFG="gwind - Win32 Debug" 
   cd "..\frontend"

"gwind - Win32 DebugCLEAN" : 
   cd "\src\sf\wargamer\src\gwind"
   $(MAKE) /$(MAKEFLAGS) /F .\gwind.mak CFG="gwind - Win32 Debug" RECURSE=1 CLEAN 
   cd "..\frontend"

!ENDIF 

!IF  "$(CFG)" == "frontend - Win32 Release"

"system - Win32 Release" : 
   cd "\src\sf\wargamer\SRC\SYSTEM"
   $(MAKE) /$(MAKEFLAGS) /F .\system.mak CFG="system - Win32 Release" 
   cd "..\frontend"

"system - Win32 ReleaseCLEAN" : 
   cd "\src\sf\wargamer\SRC\SYSTEM"
   $(MAKE) /$(MAKEFLAGS) /F .\system.mak CFG="system - Win32 Release" RECURSE=1 CLEAN 
   cd "..\frontend"

!ELSEIF  "$(CFG)" == "frontend - Win32 Debug"

"system - Win32 Debug" : 
   cd "\src\sf\wargamer\SRC\SYSTEM"
   $(MAKE) /$(MAKEFLAGS) /F .\system.mak CFG="system - Win32 Debug" 
   cd "..\frontend"

"system - Win32 DebugCLEAN" : 
   cd "\src\sf\wargamer\SRC\SYSTEM"
   $(MAKE) /$(MAKEFLAGS) /F .\system.mak CFG="system - Win32 Debug" RECURSE=1 CLEAN 
   cd "..\frontend"

!ENDIF 

!IF  "$(CFG)" == "frontend - Win32 Release"

"gamesup - Win32 Release" : 
   cd "\src\sf\wargamer\SRC\GAMESUP"
   $(MAKE) /$(MAKEFLAGS) /F .\gamesup.mak CFG="gamesup - Win32 Release" 
   cd "..\frontend"

"gamesup - Win32 ReleaseCLEAN" : 
   cd "\src\sf\wargamer\SRC\GAMESUP"
   $(MAKE) /$(MAKEFLAGS) /F .\gamesup.mak CFG="gamesup - Win32 Release" RECURSE=1 CLEAN 
   cd "..\frontend"

!ELSEIF  "$(CFG)" == "frontend - Win32 Debug"

"gamesup - Win32 Debug" : 
   cd "\src\sf\wargamer\SRC\GAMESUP"
   $(MAKE) /$(MAKEFLAGS) /F .\gamesup.mak CFG="gamesup - Win32 Debug" 
   cd "..\frontend"

"gamesup - Win32 DebugCLEAN" : 
   cd "\src\sf\wargamer\SRC\GAMESUP"
   $(MAKE) /$(MAKEFLAGS) /F .\gamesup.mak CFG="gamesup - Win32 Debug" RECURSE=1 CLEAN 
   cd "..\frontend"

!ENDIF 


!ENDIF 

