/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *      FrontEnd : Choose Battle Screen
 *
 *----------------------------------------------------------------------
 */


#include "stdinc.hpp"
#include "scr_batt.hpp"
#include "filecnk.hpp"


void
ChooseBattleScreenClass::setPalette(void) {

   FrontEndUtilsClass::setPalette("ChooseBattlePalette.bmp");
}



/*
 * Read in all .WGB files in scenario directory
 */

void ChooseBattleScreenClass::constructMissionFilesList()
{
#if defined(DEBUG) && !defined(NOLOG)
        ChooseMissionLog.printf("\nSCANNING FOR .WGB & .INB FILES");
#endif

   makeMissionList(FrontEndUtilsClass::s_battleExt);
}

#if 0
        char path[MAX_PATH];
        char WGB_filemask[MAX_PATH];
        char INB_filemask[MAX_PATH];

        // add the wargamer default folder & WGB extension to the path
        FrontEndUtils.addBattleFolderAndExtensions(WGB_filemask);
        FrontEndUtils.addBattleFolderAndInfoExtensions(INB_filemask);

        // add the wargamer default folder
        lstrcpy(path, scenario->getFolderName());
        lstrcat(path, FrontEndUtils.s_slash);

        // the windows find structures for the two files to be found .WGB & .inf
        WIN32_FIND_DATA findWGBdata;
        WIN32_FIND_DATA findINBdata;

        // locate directory containing .WGB files
        HANDLE WGB_filehandle;
        HANDLE INB_filehandle;

        // find first foile satifsfying this path & mask
        WGB_filehandle = FindFirstFile(WGB_filemask, &findWGBdata);

        // if not files found, check on CD
        if(WGB_filehandle == INVALID_HANDLE_VALUE)
        {
        FrontEndUtils.insertCDPath(path);
        FrontEndUtils.insertCDPath(WGB_filemask);
        FrontEndUtils.insertCDPath(INB_filemask);

        WGB_filehandle = FindFirstFile(WGB_filemask, &findWGBdata);

        if(WGB_filehandle == INVALID_HANDLE_VALUE)
                {
        // throw StartScreenError("WGB file cannot be found on hard disc or CD");
        FORCEASSERT("WGB file cannot be found on hard disc or CD");
        }
        }


        ASSERT(WGB_filehandle != INVALID_HANDLE_VALUE);

        /*
         * Read version number from each WGB file to establish compatibility
         */

        do
        {
        MissionInfo info;
        char WGB_filename[MAX_PATH];
        char INB_filename[MAX_PATH];

//        try
//                {

        // retrieve the actual filename for the .WGB file
        FrontEndUtils.addFolder(WGB_filename, findWGBdata.cFileName);

        WinFileBinaryChunkReader fr(WGB_filename);
/*
        FileChunkReader r(fr, campaignChunkName);

        if(!r.isOK()) { throw FileError(); }

        UWORD version;
        if(!fr.getUWord(version)) { throw FileError(); }

        if(!info.read(fr)) { throw FileError(); }
*/
//        }
//        catch(FileError) { throw GeneralError("Unable to read Battle Info"); }



                /*
                 * Search for a .inf file with the same name as the .WGB file
                 */

        // copy the Battle filename to the INF filemask
        lstrcpy(INB_filemask, WGB_filename);
        // remove the .WGB extension
        FrontEndUtils.removeWGBextension(INB_filemask);
        // add the .INF extension
        FrontEndUtils.addINBextension(INB_filemask);
        INB_filehandle = FindFirstFile(INB_filemask, &findINBdata);

        // if no .inf file is found, use default.inf
        if(INB_filehandle == INVALID_HANDLE_VALUE)
                {
        lstrcpy(INB_filename, path);
        lstrcat(INB_filename, FrontEndUtils.s_battleDefaultInfoFile);
        }
        else FrontEndUtils.addFolder(INB_filename, findINBdata.cFileName);


        // fill in the mission info field from inf file
        FrontEndUtils.ParseInfoFile(INB_filename);
        info.setDescription(FrontEndUtils.STRING_Text);

        // add the filenames to the list
        InfoItem* item = new InfoItem(info, WGB_filename, INB_filename);
        ASSERT(item != 0);
        GetMissionInfoList().append(item);

        #if defined(DEBUG) && !defined(NOLOG)
                ChooseMissionLog.printf(WGB_filename);
                ChooseMissionLog.printf(INB_filename);
        #endif

        // repeat until no more .WGB files can be found
        } while(FindNextFile(WGB_filehandle, &findWGBdata));

        // set the current item to first in list
        const InfoItem* item = GetMissionInfoList().first();
        CurrentItem(item);
}
#endif



/*
 *----------------------------------------------------------------------
 * $Log$
 *----------------------------------------------------------------------
 */
