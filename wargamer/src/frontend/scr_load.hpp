/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef SCR_LOAD_HPP
#define SCR_LOAD_HPP

#include "menubase.hpp"

class StringPtr;

class LoadGameScreenClass : public MenuScreenBaseClass {

public:

    LoadGameScreenClass(HWND parent, StringPtr* result);
    ~LoadGameScreenClass(void);

};

#endif
