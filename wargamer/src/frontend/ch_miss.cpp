/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *      FrontEnd : Choose Mission Screen
 *
 *----------------------------------------------------------------------
 */


#include "stdinc.hpp"
#include "ch_miss.hpp"
#include "scn_img.hpp"
#include "itemwind.hpp"
#include "dib.hpp"
#include "fonts.hpp"
#include "dib_blt.hpp"
#include "scrnbase.hpp"
#include "bmp.hpp"
#include <memory>
#include "scenario.hpp"
#include "fileiter.hpp"
#include "winfile.hpp"
#include "memptr.hpp"

#if defined(DEBUG) && !defined(NOLOG)
#include "clog.hpp"
LogFileFlush ChooseMissionLog("ch_miss.log");
#endif

// #include "palette.hpp"
#include "tabctrl.hpp"
// #include "sllist.hpp"
// #include "winctrl.hpp"
// #include "filecnk.hpp"
// #include "wldfile.hpp"
//
//

inline LPARAM missionItemToLParam(const MissionItem* item)
{
   return reinterpret_cast<LPARAM>(item);
}

inline const MissionItem* lParamToMissionItem(LPARAM lParam)
{
   return reinterpret_cast<const MissionItem*>(lParam);
}



/*
 *
 * The listbox window which displays the available Missions
 *
 */


typedef ItemWindow CustomListBox;
typedef ItemWindowData CustomListBoxData;


class ChooseMissionListBox : public CustomListBox
{
        public:

        ChooseMissionListBox(const CustomListBoxData& data, const SIZE& s);
        ~ChooseMissionListBox();

        void init(const void* data);
        void addItems(const MissionInfoList* missionList);

                void setBackground(const DIBDC* dib, const PixelRect& rect)
                {
                        d_dib = dib;
                        d_rect = rect;
                }

        private:
        void drawListItem(const DRAWITEMSTRUCT* lpDrawItem);
        void drawCombo(DrawDIBDC* dib, const void* data) { }
                void drawHeader(const DRAWITEMSTRUCT* lpDrawItem);
                void drawBackground(DrawDIBDC* dib, const ItemWindowLayout& info);

        private:

                const DIBDC* d_dib;
                PixelRect d_rect;
};

class ChooseMissionListWindowClass : public WindowBaseND
{
        public:
                ChooseMissionListWindowClass(const CustomListBoxData& data, const SIZE& size);
                ~ChooseMissionListWindowClass();

        // void init(const void* data);
        void addItems(const MissionInfoList* campList);
                void show(const PixelPoint& p);
                void move(const PixelRect& r);  // { MoveWindow(getHWND(), r.left(). r.top(), r.width(), r.height(), TRUE);
                void setCurrentIndex(int index);
                int currentIndex();

        private:
            // disable base::show function
            void show(bool v) { WindowBaseND::show(v); }


        private:
                LRESULT procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
                BOOL onCreate(HWND hwnd, CREATESTRUCT* lpCreateStruct);
                void onSize(HWND hwnd, UINT state, int cx, int cy);
                void onPaint(HWND handle);
                void onCommand(HWND hwnd, int id, HWND hwndCtl, UINT codeNotify);
                BOOL onEraseBk(HWND hwnd, HDC hdc);

                const PixelRect& calcListBoxRect();
        private:
                ChooseMissionListBox* d_listBox;
                DIBDC* d_DIB;
                DIBDC* d_fullDIB;
                PixelRect d_lbRect;
};



class ChooseMissionStatusWindowClass : public WindowBaseND {

/*
Variables
*/

public:

private:

    // HWND window_handle;
    HWND parent_window_handle;

    DIB * status_window_DIB;
    DrawDIBDC * status_window_DIBDC;

/*
Functions
*/

public:

    ChooseMissionStatusWindowClass(HWND parent, const PixelRect& rect);
    ~ChooseMissionStatusWindowClass(void);

    inline HWND GetWindowHandle(void) { return getHWND(); }  // (window_handle); }
    // inline void SetWindowHandle(HWND handle) { }    // window_handle = handle; }
    inline HWND GetParentWindowHandle(void) { return(parent_window_handle); }
    inline void SetParentWindowHandle(HWND parent) { parent_window_handle = parent; }

    void DrawDib(void);
    void setSize(const PixelRect& rect);


private:


    // message processing
    LRESULT procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
    void onPaint(HWND handle);

};



/*

The main window for selecting Missions
This window includes :

    a list box where available Missions are displayed

    a status window giving a summary of the selected Mission

    a tabbed window displaying :
        a) a map of the Mission area
        b) a textual desciption of the Mission

*/


ChooseMissionScreenClass::ChooseMissionScreenClass(HWND parent, StringPtr* result) :
        MenuScreenBaseClass(parent),
        d_listWindow(0),
        d_currentMission(0),
        d_missionList(),
        d_resultName(result),
        selected_Mission(0),
      d_mapWindow(0),
      d_textWindow(0),
      d_paperDIB(0),
      d_textDIB(0),
      d_mapDIB(0)
{
}

/*
 * This moved to seperate function otherwise virtual functions aren't called
 * during constructor
 */

void
ChooseMissionScreenClass::init() {

   //-- SWG: 9jun2001: What is this line here? has something been deleted?
   // HWND wh;

   // make sure that background & screen DIBs start out as NULL
    bkDIB = NULL;
    screenDIB = NULL;
   d_textDIB = NULL;
   d_paperDIB = NULL;

   DIB * tmpDIB = loadBMPDIB("frontend\\paper.bmp");
   //-- SWG: 9jun2001: What is this line here? has something been deleted?
   // (tmpDIB);
   d_paperDIB = new DrawDIBDC(tmpDIB->getWidth(), tmpDIB->getHeight());
   d_paperDIB->blitFrom(tmpDIB);
   ASSERT(d_paperDIB);
   delete tmpDIB;


   tmpDIB = loadBMPDIB("frontend\\LeatherTile.bmp");
   ASSERT(tmpDIB);
   d_leatherDIB = new DrawDIBDC(tmpDIB->getWidth(), tmpDIB->getHeight());
   d_leatherDIB->blitFrom(tmpDIB);
   ASSERT(d_leatherDIB);
   delete tmpDIB;

   // set palette
   setPalette();


   d_listWindow = NULL;

    // construct the list of WGC files for this scenario
    constructMissionFilesList();
    selected_Mission = 0;

    // parse the details of the initially selected Mission
//    ReadInfoFile();

    // size the window to fill the screen area, minus the nav-bar
    RECT rect;
    // CalculateScreenSize(&rect);
    GetClientRect(GetParentWindowHandle(), &rect);

    strcpy(ScreenNameString,"ChooseMissionScreen");

    createWindow(
        WS_EX_LEFT,
        // GenericNoBackClass::className(),
        ScreenNameString,
        WS_CHILD | WS_CLIPSIBLINGS,
        rect.left, rect.top, rect.right, rect.bottom,
        GetParentWindowHandle(),
        NULL);
        // APP::instance() ));

   SetWindowText(getHWND(),ScreenNameString);

    calculateSizes();

    createListBox();

   // load the initial bitmaps, etc.
    RefreshScreens();

   RECT windRect;

   windRect.top = d_tabRect.top();
   windRect.left = d_tabRect.left();
   windRect.right = d_tabRect.width();
   windRect.bottom = d_tabRect.height();
   LoadMapDib();
   d_mapWindow = new BitmapWindow(getHWND(), &windRect, d_mapDIB);
   d_mapWindow->setFillDIB(d_leatherDIB);

   windRect.top = d_statRect.top();
   windRect.left = d_statRect.left();
   windRect.right = d_statRect.width();
   windRect.bottom = d_statRect.height();
   DrawTextDib();
   d_textWindow = new BitmapWindow(getHWND(), &windRect, d_textDIB);
   d_textWindow->setFillDIB(d_paperDIB);


    // set show state for main window
   d_mapWindow->centerBitmap();
   d_mapWindow->showWindow(true);
   d_textWindow->positionBitmap(0,0);
    d_textWindow->showWindow(true);

   ShowWindow(getHWND(), SW_SHOW);
    UpdateWindow(getHWND());

    // set show state for listbox window
    d_listWindow->show(PixelPoint(d_lbRect.left(), d_lbRect.top()));
}

/*
 * Destructor
 */

ChooseMissionScreenClass::~ChooseMissionScreenClass(void)
{
    delete d_listWindow;
    d_listWindow = 0;

   if(d_mapWindow) delete d_mapWindow;
   if(d_textWindow) delete d_textWindow;

   delete d_leatherDIB;
   delete d_paperDIB;
   delete d_textDIB;

    selfDestruct();
}




/*
 * Work out where all the bits and pieces go
 */

void ChooseMissionScreenClass::calculateSizes()
{
   RECT rect;
   GetClientRect(GetParentWindowHandle(), &rect);

        int x1 = (rect.right / 4) * 3;
        int y1 = rect.bottom - (rect.bottom / 6);

        int lbWidth = rect.right - x1;
        int statHeight = rect.bottom - y1;

      int tabInset = x1 / 32;
      int statInset = x1 / 32;

      int VInset = y1 / 24;


      d_bkRects[0].top = 0;
      d_bkRects[0].left = 0;
      d_bkRects[0].right = tabInset;
      d_bkRects[0].bottom = y1;

      d_bkRects[1].top = 0;
      d_bkRects[1].left = x1-(tabInset*2);
      d_bkRects[1].right = tabInset;
      d_bkRects[1].bottom = y1;

      d_bkRects[2].top = 0;
      d_bkRects[2].left = rect.right-tabInset;
      d_bkRects[2].right = tabInset;
      d_bkRects[2].bottom = y1;

      d_bkRects[3].top = y1;
      d_bkRects[3].left = 0;
      d_bkRects[3].right = statInset;
      d_bkRects[3].bottom = rect.bottom - y1;

      d_bkRects[4].top = y1;
      d_bkRects[4].left = rect.right-statInset;
      d_bkRects[4].right = statInset;
      d_bkRects[4].bottom = rect.bottom - y1;

      d_bkRects[5].top = y1-VInset;
      d_bkRects[5].left = tabInset;
      d_bkRects[5].right = rect.right;//x1;
      d_bkRects[5].bottom = VInset;

      d_numBkRects = 6;


        d_tabRect = PixelRect(tabInset, 0, x1-(tabInset*2), y1-VInset);
        d_lbRect = PixelRect(x1-tabInset, 0, rect.right-tabInset, y1-VInset);
        d_statRect = PixelRect(statInset, y1, rect.right-statInset, rect.bottom);
/*
        d_tabRect = PixelRect(0, 0, x1, y1);
        d_lbRect = PixelRect(x1, 0, rect.right, y1);
        d_statRect = PixelRect(0, y1, rect.right, rect.bottom);
*/
}


/*
Load the Mission map bitmap
*/

void ChooseMissionScreenClass::LoadMapDib()
{
   const char* mapName = currentMission()->d_map;

   if(mapName == NULL)
      return;

   if(d_mapDIB != NULL)
      delete d_mapDIB;

   d_mapDIB = FrontEndUtilsClass::loadBMPDIB(mapName);
   ASSERT(d_mapDIB != NULL);
}


inline void calcTextRect(DIBDC* dib, const char* text, const RECT* rect)
{

   DrawTextEx(
      dib->getDC(),
      const_cast<char*>(text),
      strlen(text),
      const_cast<RECT*>(rect),
      DT_EDITCONTROL | DT_WORDBREAK | DT_CALCRECT,
      NULL
   );
}

inline void drawTextRect(DIBDC* dib, const char* text, const RECT* rect)
{
   DrawTextEx(
      dib->getDC(),
      const_cast<char*>(text),
      strlen(text),
      const_cast<RECT*>(rect),
      DT_EDITCONTROL | DT_WORDBREAK,
      NULL
   );
}

/*

  Draws the mission briefint text & map-key info to the textDIB

*/

void
ChooseMissionScreenClass::DrawTextDib(void) {

   int defaultWidth = 1024;
   int defaultHeight = 768;

   // init DIB
   if(d_textDIB) delete d_textDIB;
   d_textDIB = new DrawDIBDC(defaultWidth, defaultHeight);
   // copy bkgnd paper into DIB
   d_textDIB->fill(d_paperDIB);


   // setup dimensions
   int width = d_textDIB->getWidth();
   int height = d_textDIB->getHeight();

   int textHorizInset = width / 64;
   int textVertInset = height / 64;

   int textLeft = textHorizInset;
   int textRight = (width / 2) - textHorizInset;

   int frenchKeyLeft = (width / 2) + textHorizInset;
   int frenchKeyRight = ((width / 4) * 3) - textHorizInset;

   int alliedKeyLeft = ((width / 4) * 3) + textHorizInset;
   int alliedKeyRight = width - textHorizInset;



   RECT textRect;
   textRect.left = textLeft;
   textRect.top = textVertInset;
   textRect.right = textRight;
   textRect.bottom = height - textVertInset;

   RECT frenchKeyRect;
   frenchKeyRect.left = frenchKeyLeft;
   frenchKeyRect.top = textVertInset;
   frenchKeyRect.right = frenchKeyRight;
   frenchKeyRect.bottom = height - textVertInset;

   RECT alliedKeyRect;
   alliedKeyRect.left = alliedKeyLeft;
   alliedKeyRect.top = textVertInset;
   alliedKeyRect.right = alliedKeyRight;
   alliedKeyRect.bottom = height - textVertInset;

   int itemHeight = height / 64;

   const int minFontHeight = 15;
   int fontHeight = 20;
   fontHeight = (fontHeight * (itemHeight)) / 1024;
   int h = maximum(fontHeight, minFontHeight) + 5;

   LogFont logFont;
   logFont.height(h);
   logFont.weight(FW_MEDIUM);
   logFont.face(scenario->fontName(Font_Bold));

   Fonts font;
   font.set(d_textDIB->getDC(), logFont);

   SetBkMode(d_textDIB->getDC(), TRANSPARENT);

   COLORREF colour, oldColour;
   colour = RGB(0,0,0);//RGB(148,115,33);
   oldColour = SetTextColor(d_textDIB->getDC(), colour);

   /*
   * Test draw each text block with the flag DT_CALCRECT
   * to return the desired height to format text
   * choose the greatest value of Description & Allied / French keys
   */

   calcTextRect(d_textDIB, currentMission()->d_description.toStr(), &textRect);
   calcTextRect(d_textDIB, currentMission()->d_key0.toStr(), &frenchKeyRect);
   calcTextRect(d_textDIB, currentMission()->d_key1.toStr(), &alliedKeyRect);

   /*
   * textRect is now modified to accomodate text
   * must add on an additional line for bottom line
   * resize DIB to accomodate, if required
   */

   int max_height = maximum((alliedKeyRect.bottom - alliedKeyRect.top), (frenchKeyRect.bottom - frenchKeyRect.top));
   max_height = maximum(max_height, (textRect.bottom - textRect.top));

   int new_height = max_height + minFontHeight;

   if(d_textDIB->getHeight() < new_height)
   {
      if(d_textDIB) delete d_textDIB;
      d_textDIB = new DrawDIBDC(defaultWidth, new_height);
      // set up parameters
      d_textDIB->fill(d_paperDIB);
      font.set(d_textDIB->getDC(), logFont);
      SetBkMode(d_textDIB->getDC(), TRANSPARENT);
      oldColour = SetTextColor(d_textDIB->getDC(), colour);
   }


   drawTextRect(d_textDIB, currentMission()->d_description.toStr(), &textRect);
#if 0
   DrawTextEx(
      d_textDIB->getDC(),
      FrontEndUtils.STRING_Description,
      strlen(FrontEndUtils.STRING_Description),
      &textRect,
      DT_EDITCONTROL | DT_WORDBREAK,
      NULL
   );
#endif

   colour = scenario->getSideColour(0);
   oldColour = SetTextColor(d_textDIB->getDC(), colour);

   drawTextRect(d_textDIB, currentMission()->d_key0.toStr(), &frenchKeyRect);

   colour = scenario->getSideColour(1);
   oldColour = SetTextColor(d_textDIB->getDC(), colour);

   drawTextRect(d_textDIB, currentMission()->d_key1.toStr(), &alliedKeyRect);
}













/*
Sets the current item in the Missions Slist
*/

void ChooseMissionScreenClass::currentMission(const MissionItem* currentItem)
{
   ASSERT(currentItem != 0);
   d_currentMission = currentItem;
   *d_resultName = currentItem->d_fileName.toStr();
}








/*
Reload resources when user changes the selected Mission
*/

void
ChooseMissionScreenClass::RefreshScreens(void) {

// set the tab window's children to load in the new map & text descriptions

}





LRESULT
ChooseMissionScreenClass::procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam) {

        switch(msg) {

          HANDLE_MSG(hWnd, WM_CREATE, onCreate);
          HANDLE_MSG(hWnd, WM_COMMAND, onCommand);
          HANDLE_MSG(hWnd, WM_SIZE, onSize);
        HANDLE_MSG(hWnd, WM_PAINT, onPaint);

            default:
                return defProc(hWnd, msg, wParam, lParam);
        }
}


/*
this is where we create the listbox
*/
BOOL ChooseMissionScreenClass::onCreate(HWND hwnd, CREATESTRUCT* lpCreateStruct)
{
        return TRUE;
}

void ChooseMissionScreenClass::createListBox()
{
    // work out the dimensions of the listbox from dialog units
    const LONG dbUnits = GetDialogBaseUnits();
#if 0
    // the size must be calculated before the box is created
    CalculateListWindowSize(&d_lbRect);
    // set the dimensions of th listbox, relative to creation size of parent
    SIZE size;
    size.cx = lpCreateStruct->cx / 4;
    size.cy = (lpCreateStruct->cy / 8) * 7;
#endif
        // SIZE size;
        // size.cx = d_lbRect.width();
        // size.cy = d_lbRect.height();

    // fill out the data structure defining the list box
    CustomListBoxData item_data;

    item_data.d_hParent = getHWND();
    item_data.d_style = WS_CHILD;       // | WS_CLIPSIBLINGS;
    item_data.d_id = ListWindow_ID;
    // item_data.d_dbX = LOWORD(dbUnits);
    // item_data.d_dbY = HIWORD(dbUnits);
    item_data.d_scrollCX = GetSystemMetrics(SM_CXVSCROLL);
    item_data.d_itemCY = ScreenBase::y(24); // static_cast<UWORD>((24*item_data.d_dbY)/8);
    item_data.d_maxItemsShowing = 8;
    item_data.d_flags = CustomListBoxData::HasScroll  |
                                                                CustomListBoxData::NoAutoHide |
                                                                CustomListBoxData::NoBorder   |
                                                                CustomListBoxData::OwnerDraw    |
                                                                // CustomListBoxData::TextHeader |
                                                                CustomListBoxData::InclusiveSize;

         // New fields that Paul added...
         item_data.d_windowFillDib = scenario->getSideBkDIB(SIDE_Neutral);
         item_data.d_fillDib = scenario->getScenarioBkDIB(Scenario::BackPatterns::PaperBackground);
         item_data.d_scrollBtns = ScenarioImageLibrary::get(ScenarioImageLibrary::VScrollButtons);
         item_data.d_scrollThumbDib = scenario->getScenarioBkDIB(Scenario::BackPatterns::VScrollThumbBackground);
         item_data.d_scrollFillDib = scenario->getScenarioBkDIB(Scenario::BackPatterns::VScrollBackground);
         item_data.d_bColors = scenario->getBorderColors();


    // setup a font for use within the listbox
    const int minFontHeight = 20;
    int fontHeight = 30;
    fontHeight = (fontHeight * item_data.d_itemCY) / FrontEndUtils.s_baseWidth;
    fontHeight = maximum(fontHeight, minFontHeight);

    LogFont lf;
    lf.height(fontHeight);
    lf.weight(FW_MEDIUM);
    lf.face(scenario->fontName(Font_Bold));

    Font font;
    font.set(lf);

    // set the font atrribute of the listbox itemdata
    item_data.d_hFont = font;

    // create the campign list window
         ASSERT(d_lbRect.width() != 0);
         ASSERT(d_lbRect.height() != 0);
         SIZE size;
         size.cx = d_lbRect.width();
         size.cy = d_lbRect.height();
    d_listWindow = new ChooseMissionListWindowClass(item_data, size);
    // ChooseMissionListWindow = new ChooseMissionListWindowClass(item_data, d_lbRect);

         d_listWindow->move(d_lbRect);


    // assign the list of .wgc files to listbox
    // d_listWindow->init(&GetMissionInfoList());
    d_listWindow->addItems(&GetMissionInfoList());
    d_listWindow->setCurrentIndex(selected_Mission);
    // set the list window's position
    // CalculateListWindowSize(&d_lbRect);
#ifdef JAMES_ORIGINAL
    // d_listWindow->show(&listbox_rectangle);
#else
    // d_listWindow->show(PixelPoint(listbox_rectangle.left, listbox_rectangle.top));
         // d_listWindow->move(d_lbRect);
         // d_listWindow->show(PixelPoint(d_lbRect.left(), d_lbRect.top()));
#endif

        // return TRUE;
}




/*
process commands passed up to this window from child windows
*/

void
ChooseMissionScreenClass::onCommand(HWND hwnd, int id, HWND hwndCtl, UINT codeNotify) {

    switch(id) {

        case ListWindow_ID : {
            int index = d_listWindow->currentIndex();

            // only read in info file if a different Mission has been selected
            if(index != selected_Mission)
                                {
                selected_Mission = index;
            currentMission(&d_missionList[index]);
//                ReadInfoFile();
                RefreshScreens();
            LoadMapDib();
            d_mapWindow->setBitmap(d_mapDIB);
            d_mapWindow->centerBitmap();
            DrawTextDib();
            d_textWindow->setBitmap(d_textDIB);
            d_textWindow->positionBitmap(0,0);
         }
            break;

        }

    } // end of switch
}




/*
Resize all child windows explicitly
*/

void ChooseMissionScreenClass::onSize(HWND hwnd, UINT state, int cx, int cy)
{

    // resize this window in relation to parent window
    // RECT rect;
    // CalculateScreenSize(&rect);
    // MoveWindow(getHWND(), rect.left, rect.top, rect.right, rect.bottom, FALSE);

         calculateSizes();

         PixelRect r;
         GetClientRect(GetParentWindowHandle(), &r);
         MoveWindow(getHWND(), r.left(), r.top(), r.width(), r.height(), FALSE);

        /*
         * resize the listbox, if it exists
         */

        if (d_listWindow != NULL)
                  {

            // SIZE s;
            // int baseunitX = LOWORD(GetDialogBaseUnits);
            // int baseunitY = HIWORD(GetDialogBaseUnits);

            // set the listbox's dimensions relative to parent window
            // CalculateListWindowSize(&d_lbRect);
            // s.cx = cx /4 ;
            // s.cy = (cy / 8) *7;

#if 0
            // set the list window's position
            d_listWindow->init(&GetMissionInfoList());
            d_listWindow->setCurrentIndex(selected_Mission);
#ifdef JAMES_ORIGINAL
            // d_listWindow->show(&d_lbRect);
#else
                            // d_listWindow->show(PixelPoint(d_lbRect.left(), d_lbRect.top()));
#endif
#endif
                                d_listWindow->move(d_lbRect);
        }

        /*
         * resize the tab contrl window, if it exists
         */


        /*
         * Resize the status window, if it exists
         */



      if(d_mapWindow) {
         RECT windRect;
         windRect.top = d_tabRect.top();
         windRect.left = d_tabRect.left();
         windRect.right = d_tabRect.width();
         windRect.bottom = d_tabRect.height();
         d_mapWindow->setSize(&windRect);
         d_mapWindow->centerBitmap();
      }

      if(d_textWindow) {
         RECT windRect;
         windRect.top = d_statRect.top();
         windRect.left = d_statRect.left();
         windRect.right = d_statRect.width();
         windRect.bottom = d_statRect.height();
         d_textWindow->setSize(&windRect);
         d_textWindow->positionBitmap(0,0);
      }

}







void
ChooseMissionScreenClass::onPaint(HWND handle) {

    PAINTSTRUCT ps;
    HDC hdc = BeginPaint(handle, &ps);
    HPALETTE oldPal = SelectPalette(hdc, Palette::get(), FALSE);
    RealizePalette(hdc);

   for(int n=0; n<d_numBkRects; n++) {

      DrawDIBDC * tmpDIB = new DrawDIBDC(d_bkRects[n].right, d_bkRects[n].bottom);
      ASSERT(tmpDIB);
      tmpDIB->fill(d_leatherDIB);

      BitBlt(
         hdc,
         d_bkRects[n].left,
         d_bkRects[n].top,
         d_bkRects[n].right,
         d_bkRects[n].bottom,
         tmpDIB->getDC(),
         0,0,
         SRCCOPY
      );

      delete tmpDIB;
   }

    SelectPalette(hdc, oldPal, FALSE);
    EndPaint(handle, &ps);
}













/*
--------------------------------------------------------------------------------------------------------

Functions for controlling ChooseMissionListBox

--------------------------------------------------------------------------------------------------------
*/


#if 0

/*
read the .inf file associated with the selected Mission
*/
void ChooseMissionScreenClass::ReadInfoFile(void)
{
        int missionNumber = selected_Mission;

    InfoItem * TmpList = d_missionList.first();
        while(missionNumber-- && TmpList != NULL) {
            // skip through infolist until correct item is found
            TmpList = d_missionList.next(TmpList);
        }

     ASSERT(TmpList != NULL);

    // call the .inf parsing function
    FrontEndUtils.ParseInfoFile(TmpList->INFfileName.toStr() );
}

#endif



/*
 * Initialise the listbox
 */

void ChooseMissionListBox::init(const void* dataPtr)
{
        init(reinterpret_cast<const MissionInfoList*>(dataPtr));
}

ChooseMissionListBox::ChooseMissionListBox(const CustomListBoxData& data, const SIZE& s) :
        CustomListBox(data, s),
        d_dib(0)
{
    // d_listBoxDIB = BMP::newDrawDIB("frontend\\listbox.bmp", BMP::RBMP_Normal);
}

ChooseMissionListBox::~ChooseMissionListBox()
{
    selfDestruct();
//      if(d_listBoxDIB)
//      delete d_listBoxDIB;
}

void ChooseMissionListBox::addItems(const MissionInfoList* missionList)
{
   ASSERT(missionList);

   resetItems();

   for (MissionInfoList::const_iterator it = missionList->begin();
      it != missionList->end();
      ++it)
   {
      const MissionItem *item = &*it;
      addItem("", missionItemToLParam(item));
   }

//     SListIterR<InfoItem> iter(campList);
//     while(++iter)
//          {
//         const InfoItem* item = iter.current();
//         addItem("", FrontEndUtils.infoItemToLParam(item));
//     }

    // ensure this was suucessful
    // int nItems = lb.getNItems();
    int nItems = getNItems();
    ASSERT(nItems > 0);

// #if 0        // Let listbox take care of this!
//    lb.set(0);


    // set viewable items & individual item height
    int itemsShowing = minimum(data().d_maxItemsShowing, nItems);
    const LONG itemHeight = data().d_itemCY*itemsShowing;


    // RECT r;
    // GetClientRect(getHWND(), &r);

    // const LONG itemWidth = (r.right-r.left)-(2*offsetX);

//    MoveWindow(lb.getHWND(),listbox_rectangle.left, listbox_rectangle.top, listbox_rectangle.right, listbox_rectangle.bottom, TRUE);
//    MoveWindow(lb.getHWND(),listbox_rectangle.left, listbox_rectangle.top, listbox_rectangle.right - (offsetX*2) -1, listbox_rectangle.bottom - (offsetY*2) -1, TRUE);
//              move(d_lbRect);

/*
NOTE: inset the window's edges to allow for the framing border
*/

//    int framesize = 4;
//    MoveWindow(lb.getHWND(),listbox_rectangle.left+framesize, listbox_rectangle.top+framesize, listbox_rectangle.right-framesize, listbox_rectangle.bottom-framesize, TRUE);

    // If we're already showing, and we need a scroll bar, show it
    if(showing() && (nItems > data().d_maxItemsShowing))
                showScroll();
// #endif
}







/*
Draws a listbox item
*/

void
ChooseMissionListBox::drawListItem(const DRAWITEMSTRUCT* lpDrawItem)
{
    const int itemCX = lpDrawItem->rcItem.right-lpDrawItem->rcItem.left;
    const int itemCY = lpDrawItem->rcItem.bottom-lpDrawItem->rcItem.top;

    // Allocate dib if needed
    DrawDIBDC* itemDIB = allocateItemDib(itemCX, itemCY);

    ASSERT(itemDIB);

        if(d_dib)
        {
                BitBlt(itemDIB->getDC(), 0,0,itemCX,itemCY,
                        d_dib->getDC(), lpDrawItem->rcItem.left+d_rect.left(), lpDrawItem->rcItem.top+d_rect.top(), SRCCOPY);
        }
        else
        itemDIB->rect(0, 0, itemCX, itemCY, fillDib());

        itemDIB->line(0, 0, itemCX, 0, itemDIB->getColour(RGB(148,115,33)));
        itemDIB->line(0, itemCY-1, itemCX, itemCY-1, itemDIB->getColour(RGB(148,115,33)));

    if(lpDrawItem->itemState & ODS_SELECTED)
         {
                itemDIB->frame(0, 0, itemCX, itemCY, itemDIB->getColour(RGB(255,255,255)));
                itemDIB->frame(1, 1, itemCX-2, itemCY-2, itemDIB->getColour(RGB(255,255,255)));
        }

    // Find item in message list
    ListBox lb(lpDrawItem->hwndItem);
    int nItems = lb.getNItems();

    if(nItems > 0)
         {
        ASSERT(lpDrawItem->itemID < nItems);
        const MissionItem* item = lParamToMissionItem(lb.getValue(lpDrawItem->itemID));
//        const char* text = item->info.getDescription();
        const char* text = item->d_text.toStr();

        RECT r;
        SetRect(&r, 3, 3, itemCX-6, itemCY-6);

                if(lpDrawItem->itemState & ODS_SELECTED)
                                itemDIB->setTextColor(RGB(255,255,255));
                        else
                                itemDIB->setTextColor(RGB(148,115,33));

        DrawText(itemDIB->getDC(), text, lstrlen(text), &r, DT_WORDBREAK);
    }

    BitBlt(lpDrawItem->hDC, lpDrawItem->rcItem.left, lpDrawItem->rcItem.top, itemCX, itemCY,itemDIB->getDC(), 0, 0, SRCCOPY);

}

void ChooseMissionListBox::drawHeader(const DRAWITEMSTRUCT* lpDrawItem)
{
#if 0
        const int itemCX = lpDrawItem->rcItem.right-lpDrawItem->rcItem.left;
        const int itemCY = lpDrawItem->rcItem.bottom-lpDrawItem->rcItem.top;

        // DrawDIBDC* itemDIB = allocateItemDib(itemCX, itemCY);
        // ASSERT(itemDIB);

        // itemDIB->rect(0, 0, itemCX, itemCY, fillDib());

        const char* text = "Choose Mission";

        RECT r;
        SetRect(&r, 0, 0, itemCX, itemCY);

   const LONG dbUnits = GetDialogBaseUnits();

   int h = (30 * 4 * HIWORD(dbUnits)) / (FrontEndUtilsClass::s_baseHeight);
        h = maximum(h, 20);

        LogFont logFont;
        logFont.height(h);
        logFont.weight(FW_BOLD);
        logFont.underline(true);

        Fonts font;
        font.set(lpDrawItem->hDC, logFont);

        COLORREF color = scenario->getColour("CaptionText");

        COLORREF oldColor = SetTextColor(lpDrawItem->hDC, color);

        // DrawText(itemDIB->getDC(), text, lstrlen(text), &r, DT_WORDBREAK | DT_CENTER);
        DrawText(lpDrawItem->hDC, text, lstrlen(text), &r, DT_WORDBREAK | DT_CENTER);
        SetTextColor(lpDrawItem->hDC, oldColor);

        // BitBlt(lpDrawItem->hDC, lpDrawItem->rcItem.left, lpDrawItem->rcItem.top, itemCX, itemCY,itemDIB->getDC(), 0, 0, SRCCOPY);
#endif
}


void ChooseMissionListBox::drawBackground(DrawDIBDC* dib, const ItemWindowLayout& info)
{
#if 0
        ASSERT(d_listBoxDIB);

   DIB_Utility::stretchDIB(dib, 0, 0, info.s_totalW, info.s_totalH,
                d_listBoxDIB, 0, 0, d_listBoxDIB->getWidth(), d_listBoxDIB->getHeight());
#endif
#if 0
                // Copy bits from underneath

        POINT p;
        p.x = 0;
        p.y = 0;

        HWND parent = GetParent(getHWND());
        HDC hdc = GetDC(parent);
        ClientToScreen(getHWND(), &p);
        ScreenToClient(parent, &p);
        BitBlt(dib->getDC(), 0,0,info.s_totalW,info.s_totalH,
                hdc, p.x,p.y, SRCCOPY);
        ReleaseDC(parent, hdc);
#endif

        if(d_dib)
        {

                ASSERT(info.s_totalW == d_rect.width());
                ASSERT(info.s_totalH == d_rect.height());

                BitBlt(dib->getDC(), 0, 0, d_rect.width(), d_rect.height(),
                        d_dib->getDC(), d_rect.left(), d_rect.top(), SRCCOPY);
        }
}



















ChooseMissionListWindowClass::ChooseMissionListWindowClass(const CustomListBoxData& data, const SIZE& size) :
        d_listBox(0),
        d_DIB(0),
        d_fullDIB(0)
{
        d_DIB = FrontEndUtilsClass::loadBMPDIBDC("frontend\\listbox.bmp");

        createWindow(0,
                // GenericClass::className(),
                "ChooseMissionListWindow",
                WS_CHILD | WS_VISIBLE,
                0, 0, size.cx, size.cy,
                data.d_hParent,
                NULL);
                // APP::instance() );

        calcListBoxRect();
        SIZE innerSize;
        innerSize.cx = d_lbRect.width();
        innerSize.cy = d_lbRect.height();

        CustomListBoxData lbData = data;
        lbData.d_hParent = getHWND();
        lbData.d_exStyle |= WS_EX_TRANSPARENT;
        d_listBox = new ChooseMissionListBox(lbData, innerSize);

        ASSERT(d_fullDIB);
        d_listBox->setBackground(d_fullDIB, d_lbRect);
}

ChooseMissionListWindowClass::~ChooseMissionListWindowClass()
{
    if(d_listBox)
       delete d_listBox;

    selfDestruct();

    delete d_fullDIB;
    d_fullDIB = 0;

    delete d_DIB;
    d_DIB = 0;
}

// void ChooseMissionListWindowClass::init(const void* data)
// {
// }

void ChooseMissionListWindowClass::addItems(const MissionInfoList* campList)
{
        d_listBox->addItems(campList);
}

void ChooseMissionListWindowClass::show(const PixelPoint& p)
{
        SetWindowPos(getHWND(), NULL, p.x(), p.y(), 0,0,
                SWP_NOACTIVATE |
                SWP_NOSIZE |
                SWP_NOZORDER);

        PixelPoint lbp(d_lbRect.left(), d_lbRect.top());

        d_listBox->show(lbp);
}

void ChooseMissionListWindowClass::move(const PixelRect& r)
{
        SetWindowPos(getHWND(), NULL, r.left(), r.top(), r.width(), r.height(), SWP_NOZORDER);
        calcListBoxRect();
        d_listBox->move(d_lbRect);
}

void ChooseMissionListWindowClass::setCurrentIndex(int index)
{
        d_listBox->setCurrentIndex(index);
}

int ChooseMissionListWindowClass::currentIndex()
{
        return d_listBox->currentIndex();
}


LRESULT ChooseMissionListWindowClass::procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
        switch(msg)
        {
        HANDLE_MSG(hWnd, WM_CREATE, onCreate);
        HANDLE_MSG(hWnd, WM_COMMAND, onCommand);
        HANDLE_MSG(hWnd, WM_SIZE, onSize);
        HANDLE_MSG(hWnd, WM_PAINT, onPaint);
                HANDLE_MSG(hWnd, WM_ERASEBKGND, onEraseBk);

        default:
                return defProc(hWnd, msg, wParam, lParam);
        }
}

BOOL ChooseMissionListWindowClass::onCreate(HWND hwnd, CREATESTRUCT* lpCreateStruct)
{
        return TRUE;
}


void ChooseMissionListWindowClass::onSize(HWND hwnd, UINT state, int cx, int cy)
{
        bool needNew = false;

        if(d_fullDIB == 0)
        {
                d_fullDIB = new DIBDC(cx, cy);
                needNew = true;
        }
        else
        if((d_fullDIB->getWidth() != cx) ||
                (d_fullDIB->getHeight() != cy) )
        {
                d_fullDIB->resize(cx, cy);
                needNew = true;
        }

        if(needNew)
        {
                SetStretchBltMode(d_fullDIB->getDC(), COLORONCOLOR);
                HRESULT result = StretchBlt(d_fullDIB->getDC(), 0,0,cx,cy,
                        d_DIB->getDC(),0,0,d_DIB->getWidth(),d_DIB->getHeight(), SRCCOPY);
               ASSERT(result);
        }

        calcListBoxRect();
        if(d_listBox)
        {
                d_listBox->setBackground(d_fullDIB, d_lbRect);
                d_listBox->move(d_lbRect);
        }
}

void ChooseMissionListWindowClass::onPaint(HWND handle)
{
        PAINTSTRUCT ps;
        HDC hdc = BeginPaint(handle, &ps);
        // do nothing
        EndPaint(handle, &ps);
}

BOOL ChooseMissionListWindowClass::onEraseBk(HWND hwnd, HDC hdc)
{
        ASSERT(d_fullDIB);

   HPALETTE oldPal = SelectPalette(hdc, Palette::get(), FALSE);
   RealizePalette(hdc);

        PixelRect r;
        GetClientRect(hwnd, &r);

#if 0


        SetStretchBltMode(hdc, COLORONCOLOR);
        StretchBlt(hdc, r.left(), r.top(), r.width(),r.height(),
                        d_DIB->getDC(), 0, 0, d_DIB->getWidth(), d_DIB->getHeight(),
                        SRCCOPY);


   // DIB_Utility::stretchDIB(dib, 0, 0, info.s_totalW, info.s_totalH,
        //      d_listBoxDIB, 0, 0, d_listBoxDIB->getWidth(), d_listBoxDIB->getHeight());
#endif

        ASSERT(d_fullDIB->getWidth() == r.width());
        ASSERT(d_fullDIB->getHeight() == r.height());

        BitBlt(hdc, 0,0,r.width(),r.height(),
                d_fullDIB->getDC(),0,0, SRCCOPY);

        SelectPalette(hdc, oldPal, FALSE);

        return TRUE;
}

void ChooseMissionListWindowClass::onCommand(HWND hwnd, int id, HWND hwndCtl, UINT codeNotify)
{
        FORWARD_WM_COMMAND(GetParent(hwnd), id, hwndCtl, codeNotify, SendMessage);
}

const PixelRect& ChooseMissionListWindowClass::calcListBoxRect()
{
        PixelRect r;
        GetClientRect(getHWND(), &r);

        int w = r.width();
        int h = r.height();

        d_lbRect.left  ( (w * 30 ) / 256);
        d_lbRect.right ( (w * 226) / 256);
        d_lbRect.top   ( (h * 88 ) / 658);
        d_lbRect.bottom( (h * 637) / 658);

        return d_lbRect;
}






DIB* ChooseMissionScreenClass::loadBMPDIB(const char* name)
{
   return FrontEndUtilsClass::loadBMPDIB(name, false);
// auto_ptr<char> fName(scenario->makeScenarioFileName(name));
// DIB* dib = BMP::newDIB(fName.get(), BMP::RBMP_Normal);
// return dib;
}





void ChooseMissionScreenClass::makeMissionList(const char* ext)
{
   /*
    * Scan for *.wgb or *.wgc files
    */

   SimpleString wildName = scenario->getFolderName();
   wildName += FrontEndUtilsClass::s_slashStar;
   wildName += ext;

   for (FindFileIterator fIter(wildName.toStr());
      fIter();
      ++fIter)
   {
      const char* name = fIter.name();    // this is the basename

      d_missionList.add(name);
   }

//   d_currentMission = d_missionList.begin();
   currentMission(&*(d_missionList.begin()));
}



namespace {

static const char TOKEN_Map[] = "MAP";
static const char TOKEN_Text[] = "TEXT";
static const char TOKEN_Description[] = "DESCRIPTION";
static const char TOKEN_Key0[] = "FRENCH_KEY";
static const char TOKEN_Key1[] = "ALLIED_KEY";
static const char TOKEN_Quote = '"';

inline CString getString(char* buffer, const char* token)
{
   const char* ptr = strstr(buffer, token);
   if(ptr)
   {
      ptr += strlen(token);

      while(ptr[0] && isspace(ptr[0]))
         ++ptr;
      ASSERT(ptr[0]);
      ASSERT(ptr[0] == TOKEN_Quote);

      if(ptr[0] == TOKEN_Quote)
         ++ptr;

      const char* start = ptr;

      while (ptr[0] && (ptr[0] != TOKEN_Quote))
      {
         ++ptr;
      }

      size_t length = ptr - start;
      char* retStr = new char[length + 1];
      retStr[length] = 0;
      strncpy(retStr, start, length);
      return retStr;
   }

   return 0;

}

};

#if 0    // Rewritten for better INF file parsing




void
FrontEndUtilsClass::ParseInfoFile(char const * filename) {

   file_buffer = NULL;

   /*
   Delete strings if already allocated & reset to NULL
   */

   if(FILENAME_Map != NULL)
   {
      delete FILENAME_Map;
      FILENAME_Map = NULL;
   }

   if(STRING_Text != NULL)
   {
      delete STRING_Text;
      STRING_Text = NULL;
   }

   if(STRING_Description != NULL)
   {
      delete STRING_Description;
      STRING_Description = NULL;
   }

   if(STRING_FrenchKey != NULL)
   {
      delete STRING_FrenchKey;
      STRING_FrenchKey = NULL;
   }

   if(STRING_AlliedKey != NULL)
   {
      delete STRING_AlliedKey;
      STRING_AlliedKey = NULL;
   }

   /*
   Reset language section pointers to NULL
   */

   DEFAULT_SectionStart = DEFAULT_SectionEnd = NULL;
   LOCALE_SectionStart = LOCALE_SectionEnd = NULL;

   /*
   Open the .inf file
   */

   HANDLE filehandle = CreateFile(filename, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   // return error opening file
   if(!filehandle)
   {
#if defined(DEBUG) && !defined(NOLOG)
      ReadInfLog.printf("couldn't open campaign/battle info file");
#endif
      return;
   }

   // find the file's size
   int filesize = GetFileSize(filehandle, NULL);
   unsigned long bytesread;

   if(filesize <= 0)
   {
#if defined(DEBUG) && !defined(NOLOG)
      ReadInfLog.printf("couldn't open campaign/battle info file (says size is <= 0)");
#endif
      return;
   }


   file_buffer = new char[filesize];

   // start at the beginning
   SetFilePointer(filehandle, NULL, NULL, FILE_BEGIN);

   // read file into buffer
   BOOL readresult = ReadFile(filehandle, file_buffer, filesize, &bytesread, NULL);

   // close file
   CloseHandle(filehandle);

   // return after deleting buffer, if read error
   if(!readresult)
   {
#if defined(DEBUG) && !defined(NOLOG)
      ReadInfLog.printf("couldn't read from .inf file");
#endif
      delete file_buffer;
      file_buffer = NULL;
      return;
   }

   // return after deleting buffer, if anomoly with file size
   if(bytesread != filesize)
   {
#if defined(DEBUG) && !defined(NOLOG)
      ReadInfLog.printf("bytes read from .inf file was not same as its file size");
#endif
      delete file_buffer;
      file_buffer = NULL;
      return;
   }

   // find the separate default & locale sections
   LocateSections();

   // read in LOCALE specific data
#if defined(DEBUG) && !defined(NOLOG)
   ReadInfLog.printf("Now processing LOCALE language section");
#endif

   ReadSymbols(LOCALE_SectionStart, LOCALE_SectionEnd);

   // read in DEFAULT specific data
#if defined(DEBUG) && !defined(NOLOG)
   ReadInfLog.printf("Now processing DEFAULT language section");
#endif

   ReadSymbols(DEFAULT_SectionStart, DEFAULT_SectionEnd);


   delete file_buffer;
   file_buffer = NULL;

}



/*
Fills out the start & end pointers to each section
*/

void
FrontEndUtilsClass::LocateSections(void) {

   ASSERT(file_buffer);

   // find occurance of DEFAULT section
   DEFAULT_SectionStart = strstr(file_buffer,"[DEFAULT]");
   if(DEFAULT_SectionStart != NULL)
   {
      // skip header
      DEFAULT_SectionStart += strlen("[DEFAULT]");
      // find start of data '{' & skip
      DEFAULT_SectionStart = strchr(DEFAULT_SectionStart, TOKEN_OpenBracket) +1;
      // find end of data '}'
      DEFAULT_SectionEnd = strchr(DEFAULT_SectionStart, TOKEN_CloseBracket);

#if defined(DEBUG) && !defined(NOLOG)
      ReadInfLog.printf("Located DEFAULT language section");
#endif
   }
   else
   {
#if defined(DEBUG) && !defined(NOLOG)
      ReadInfLog.printf("Couldn't locate DEFAULT language section");
#endif
   }



   // find occurance of LOCALE section
   LOCALE_SectionStart = strstr(file_buffer,LOCALECODE_String);
   if(LOCALE_SectionStart != NULL)
   {
      // skip header
      LOCALE_SectionStart += strlen(LOCALECODE_String);
      // find start of data '{' & skip
      LOCALE_SectionStart = strchr(LOCALE_SectionStart, TOKEN_OpenBracket) +1;
      // find end of data '}'
      LOCALE_SectionEnd = strchr(LOCALE_SectionStart, TOKEN_CloseBracket);

#if defined(DEBUG) && !defined(NOLOG)
      ReadInfLog.printf("Located LOCALE Section");
#endif
   }
   else
   {
#if defined(DEBUG) && !defined(NOLOG)
      ReadInfLog.printf("Couldn't locate LOCALE Section");
#endif
   }


}









/*
Locates the symbols within given section delimiters, and copies text into the rsource string
*/


void
FrontEndUtilsClass::ReadSymbols(char * section_start, char * section_end) {

   // return if start or end section are invalid
   if(section_start == NULL || section_end == NULL || section_end <= section_start)
   {
#if defined(DEBUG) && !defined(NOLOG)
      ReadInfLog.printf("Bad Section Delimiters");
#endif
      return;
   }


   char * ptr = section_start;


   // find the entry for campaign map
   ptr = strstr(section_start, TOKEN_Map);
   if(ptr != NULL && ptr < section_end)
   {
      ptr+=strlen(TOKEN_Map);  // skip past the token
      if(FILENAME_Map == NULL)
         FILENAME_Map = ScanString(ptr);
   }

   // find the entry for campaign map
   ptr = strstr(section_start, TOKEN_Text);
   if(ptr != NULL && ptr < section_end)
   {
      ptr+=strlen(TOKEN_Text);  // skip past the token
      if(STRING_Text == NULL)
         STRING_Text = ScanString(ptr);
   }

   // find the entry for campaign map
   ptr = strstr(section_start, TOKEN_Description);
   if(ptr != NULL && ptr < section_end)
   {
      ptr+=strlen(TOKEN_Description);  // skip past the token
      if(STRING_Description == NULL)
         STRING_Description = ScanString(ptr);
   }

   // find the entry for french map keys text
   ptr = strstr(section_start, TOKEN_FrenchKey);
   if(ptr != NULL && ptr < section_end)
   {
      ptr+=strlen(TOKEN_FrenchKey);  // skip past the token
      if(STRING_FrenchKey == NULL)
         STRING_FrenchKey = ScanString(ptr);
   }

   // find the entry for french map keys text
   ptr = strstr(section_start, TOKEN_AlliedKey);
   if(ptr != NULL && ptr < section_end)
   {
      ptr+=strlen(TOKEN_AlliedKey);  // skip past the token
      if(STRING_AlliedKey == NULL)
         STRING_AlliedKey = ScanString(ptr);
   }

}













/*
scans a string enclosed in quotes, removes the quotes, allocates string & returns a pointer to it
*/

char *
FrontEndUtilsClass::ScanString(char * src) {

   char * ptr = src;

   // remove leading spaces
   while(*ptr == TOKEN_Space && ptr != NULL) ptr++;
   if(ptr==NULL)
   {
#if defined(DEBUG) && !defined(NOLOG)
      ReadInfLog.printf("Reached end of file whilst removing leading spaces");
#endif
      return NULL;
   }

   // skip leading quote
   if(*ptr == TOKEN_Quote) ptr++;

   // check for empty quotes
   if(*ptr == TOKEN_Quote)
   {
#if defined(DEBUG) && !defined(NOLOG)
      ReadInfLog.printf("Empty quotes in section entry");
#endif
      return NULL;
   }

   // store this point as the string start
   char* string_start = ptr;
   char * string_end = ptr;

   // skip through string until terminating quote is found
   while(*string_end != TOKEN_Quote)
   {
      if(*string_end == NULL)
      {
#if defined(DEBUG) && !defined(NOLOG)
         ReadInfLog.printf("Reached end of file whilst searching for closing quote");
#endif
         return NULL;
      }
      string_end++;
   }


   // find out number of characters between the two quotes
   // plus one, for the terminator which will be put in later
   int num = (string_end - string_start);

   // ensure that the number of characters is valid
   if(num <= 0)
   {
#if defined(DEBUG) && !defined(NOLOG)
      ReadInfLog.printf("Length of string within section entry is zero or negative !");
#endif
      return NULL;
   }


   /*
   NOTE 1 : apparently it is OK to use standard function with windows for memory allocation
   NOTE 2 : an extra byte should probably be allocated for the string terminating character \0
   NOTE 3 : 'strncpy' doesn't append a terminator - so it'll have to be put on manually
   */

   // allocate enough memory at dest pointer to accomodate string
   char* dest = new char[num+1];

   // copy num characters to dest
   strncpy(dest,string_start,num);
   // set the last character of string to be a terminator
   *(dest+num) = '\0';

#if defined(DEBUG) && !defined(NOLOG)
   ReadInfLog.printf("String Scanned & Stored Ok:");
   ReadInfLog.printf(dest);
#endif

   return dest;
}
#endif   // 0    // Rewritten for better INF file parsing




/*
 * Mission Info Handling
 *
 * The missionName is the baseName such as "campaign.wgc"
 */

void MissionInfoList::add(const char* missionName)
{
   // Open and parse file

   // fill in MissionItem and add to list

   d_items.push_back(MissionItem());
   MissionItem& item = d_items.back();

   // Convert missionName into real filename

   CString fullName(scenario->makeScenarioFileName(missionName));

   item.d_fileName = fullName;

   // Find the INF file

   char infBuffer[MAX_PATH];
   strcpy(infBuffer, missionName);
   char* s = infBuffer;
   while(s[0] && (s[0] != '.'))
      ++s;
   ASSERT(s[0] == '.');
   *s++ = '.';
   ASSERT(toupper(s[0]) == 'W');
   *s++ = 'i';
   ASSERT(toupper(s[0]) == 'G');
   *s++ = 'n';
   char typeChar = s[0];
   ASSERT( (toupper(typeChar) == 'C') || (toupper(typeChar) == 'B') );
   ASSERT(s[1] == 0);

   bool customMission = false;

   CString infName;
   try
   {
      infName = scenario->makeScenarioFileName(infBuffer);
   }
   catch(const Scenario::ScenarioFileException& e)
   {
      sprintf(infBuffer, "default.in%c", typeChar);
      infName = scenario->makeScenarioFileName(infBuffer);
      customMission = true;
   }

   // Parse the infFile

   WinFileBinaryReader fh(infName.toStr());
   DWORD fLen = fh.fileSize();
   MemPtr<char> fileBuffer(fLen + 1);
   fh.read(fileBuffer.get(), fLen);
   fileBuffer[fLen] = 0;   // force a null at the end

   item.d_map = getString(fileBuffer.get(), TOKEN_Map).toStr();
   if (customMission)
   {
      char* s = infBuffer;
      *s++ = '<';
      strcpy(s, missionName);
      while(s[0] && (s[0] != '.'))
         ++s;
      *s++ = '>';
      *s = 0;
      item.d_text = infBuffer;
   }
   else
      item.d_text = getString(fileBuffer.get(), TOKEN_Text).toStr();
   item.d_description = getString(fileBuffer.get(), TOKEN_Description).toStr();
   item.d_key0 = getString(fileBuffer.get(), TOKEN_Key0).toStr();
   item.d_key1 = getString(fileBuffer.get(), TOKEN_Key1).toStr();
}




#if 0
void
FrontEndUtilsClass::addCampaignFolderAndExtensions(char* buf) {

   ASSERT(buf);

   lstrcpy(buf, scenario->getFolderName());
   lstrcat(buf, s_slashStar);
   lstrcat(buf, s_campaignExt);
}

// new function to support campaign .INF files
void
FrontEndUtilsClass::addCampaignFolderAndInfoExtensions(char* buf) {

   ASSERT(buf);

   lstrcpy(buf, scenario->getFolderName());
   lstrcat(buf, s_slashStar);
   lstrcat(buf, s_campaignInfoExt);
}



void
FrontEndUtilsClass::addBattleFolderAndExtensions(char* buf) {

   ASSERT(buf);

   lstrcpy(buf, scenario->getFolderName());
   lstrcat(buf, s_slashStar);
   lstrcat(buf, s_battleExt);
}

// new function to support campaign .INF files
void
FrontEndUtilsClass::addBattleFolderAndInfoExtensions(char* buf) {

   ASSERT(buf);

   lstrcpy(buf, scenario->getFolderName());
   lstrcat(buf, s_slashStar);
   lstrcat(buf, s_battleInfoExt);
}




void
FrontEndUtilsClass::removeWGCextension(char * buf) {

   ASSERT(buf);

   int buflen = lstrlen(buf);
   int extlen = lstrlen(s_campaignInfoExt);
   int difference = (buflen - extlen+1);

   // make sure there's space for removig the extension
   ASSERT(buflen > extlen);

   char tmp_string[MAX_PATH];

   // copy buffer - extension - 1 characters, to temp (one less due to terminating character)
   lstrcpyn(tmp_string, buf, difference);

   // copy back into buffer
   lstrcpy(buf, tmp_string);

}


void
FrontEndUtilsClass::addINCextension(char * buf) {

   ASSERT(buf);

   lstrcat(buf, s_campaignInfoExt);
}





void
FrontEndUtilsClass::removeWGBextension(char * buf) {

   ASSERT(buf);

   int buflen = lstrlen(buf);
   int extlen = lstrlen(s_battleInfoExt);
   int difference = (buflen - extlen+1);

   // make sure there's space for removig the extension
   ASSERT(buflen > extlen);

   char tmp_string[MAX_PATH];

   // copy buffer - extension - 1 characters, to temp (one less due to terminating character)
   lstrcpyn(tmp_string, buf, difference);

   // copy back into buffer
   lstrcpy(buf, tmp_string);

}


void
FrontEndUtilsClass::addINBextension(char * buf) {

   ASSERT(buf);

   lstrcat(buf, s_battleInfoExt);
}



void
FrontEndUtilsClass::addFolder(char* buf, const char* fileName) {

   ASSERT(buf);
   ASSERT(fileName);

   lstrcpy(buf, scenario->getFolderName());
   lstrcat(buf, s_slash);
   lstrcat(buf, fileName);
}

#endif
/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.4  2002/11/16 18:03:32  greenius
 * Various changes so that it will compile with Visual Studio .net
 *
 * Revision 1.3  2001/07/02 15:23:17  greenius
 * Battle, Campaign, Combined game and Campaign Editor now compile and run... mostly
 *
 * Revision 1.2  2001/06/18 08:25:45  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
