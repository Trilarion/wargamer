/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
/*
NAV_BAR.HPP
Defines the NavigateBar at the bottom of the font-end menu screens.
*/

#ifndef NAVBAR_HPP
#define NAVBAR_HPP


#include "wind.hpp"

class DIB;
class DrawDIBDC;

class NavigateBarClass : public WindowBaseND {

public:

    enum BarButtonId {  // ID no's for each button
        BarButtonId_A,
        BarButtonId_B,
        BarButtonId_C,
        BarButtonId_D };


    enum BarModeEnum {  // possible functional modes of bar
        Bar_Hidden,
        Bar_Normal,
        Bar_Final };


/*---------------------------------------------------------------

        VARIABLES

---------------------------------------------------------------*/

private:

    HWND parent_window_handle;  // handle to parent window
    // HWND window_handle;  // handle of this window

    BarModeEnum BarMode;  // which mode the bar is in

    HWND Button_A, Button_B, Button_C, Button_D;  // the four buttons on bar

    DIB * prev_DIB, * next_DIB, * help_DIB, * quit_DIB, * start_DIB;  // preloaded dibs for normal buttons
    DIB * prev_pushed_DIB, * next_pushed_DIB, * help_pushed_DIB, * quit_pushed_DIB, * start_pushed_DIB;  // preloaded dibs for pushed buttons

    DIB * buttonA_DIB, * buttonB_DIB, * buttonC_DIB, * buttonD_DIB;
    DIB * buttonApushed_DIB, * buttonBpushed_DIB, * buttonCpushed_DIB, * buttonDpushed_DIB;

   
    unsigned int BarWidth;  // width of the bar - dependent upon owner window
    unsigned int ButtonSpacing;  // spacing value of buttons

    DIB * bkDIB;  // static background DIB
    DrawDIBDC * screenDIB;  // dynamic screen DIB

    DrawDIBDC * button_screenDIB;  // dynamic button screen DIB


/*---------------------------------------------------------------

        FUNCTION PROTOTYPES

---------------------------------------------------------------*/
public:

    NavigateBarClass(void);
    NavigateBarClass(HWND parent, BarModeEnum mode, RECT * rect);
    ~NavigateBarClass(void);

    HWND GetWindowHandle(void) { return getHWND(); }
    // inline void SetWindowHandle(HWND handle) { window_handle = handle; }
    HWND GetParentWindowHandle(void) { return(parent_window_handle); }
    void SetParentWindowHandle(HWND parent) { parent_window_handle = parent; }

    void SetBarMode(BarModeEnum mode);
	BarModeEnum getMode() const { return BarMode; }

	void setSize(RECT * rect);
    void DisplayBar(void);
    
private:

    void DrawScreenDIB(void);

    LRESULT procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
    void onSize(HWND hwnd, UINT state, int cx, int cy);
    void onPaint(HWND handle);
    void onCommand(HWND hwnd, int id, HWND hwndCtl, UINT codeNotify);
    void onDrawItem(HWND handle, const DRAWITEMSTRUCT* lpDrawItem);
    HBRUSH onCtlColorBtn(HWND parent, HDC hdc, HWND button, int type);


    HWND CreateButton(HWND hParent, char * text, int id);
    void LoadButtonsBitmaps(void);
    void SetButtonsBitmaps(void);
    void CreateButtons(void);
    void DrawButtonBitmap(HWND button, DIB * bitmap, const DRAWITEMSTRUCT* lpDrawItem, int blitmode);
    void SetButtonsPositions(void);




};  // end of NavigateBarClass

#endif


