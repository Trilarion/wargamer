/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef SCR_BATT_HPP
#define SCR_BATT_HPP

#include "ch_miss.hpp"

class ChooseBattleScreenClass : public ChooseMissionScreenClass 
{
	public:
   	ChooseBattleScreenClass(HWND parent, StringPtr* result) :
			ChooseMissionScreenClass(parent, result)
		{
			init();
		}

	private:
		void constructMissionFilesList();
		void setPalette();

};


#endif

