/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef F_UTILS_HPP
#define F_UTILS_HPP


#include "sllist.hpp"
#include "simpstr.hpp"
// #include "StringPtr.hpp"
#include "campinfo.hpp"
#include "registry.hpp"
#include "scenario.hpp"
#include "app.hpp"
#include "bmp.hpp"
#include "palette.hpp"
#include <vector>

#ifdef DEBUG
static const char DefaultScenarioFileName[] = "NAP1813.SWG";
#endif

struct GameFile : public SLink
{
   SimpleString fileName;
   UWORD id;                // menu id

   GameFile(const char* name, UWORD i) :
   id(i)
   {
      ASSERT(name);

      fileName = name;
      ASSERT(fileName.toStr());
   }
};




/*-----------------------------------------------------------
 * struct to hold display rect and id of a control
 */

struct ControlInfo {    //
   UWORD d_x;
   UWORD d_y;
   UWORD d_w;
   UWORD d_h;

   UWORD d_id;
};








/*-----------------------------------------------------------------
 * Utility class
 */

class FrontEndUtilsClass {

   public:

      /*
      Some useful constants
      */

      static const char s_slash[];
      static const char s_slashChar;
      static const char s_dotChar;
      static const char s_slashStar[];

      static const char s_campaignExt[];
      static const char s_campaignInfoExt[];
      static const char s_campaignDefaultInfoFile[];
      static const char s_battleExt[];
      static const char s_battleInfoExt[];
      static const char s_battleDefaultInfoFile[];

      static const char s_savedGamesRegistryName[];
      static const char s_scenarioRegistryName[];
      static const char s_richEditDLLName[];
      static const char s_cdRegistryPath[];
      static const char s_cdRegistryValue[];

      static const UWORD s_baseWidth;
      static const UWORD s_baseHeight;
      static const UWORD s_baseButtonWidth;
      static const UWORD s_baseButtonHeight;
      static const UWORD s_screenOffsetX;
      static const UWORD s_screenOffsetY;
      static const UWORD s_baseListCX;
      static const UWORD s_baseListCY;






      /*
      Utility functions
      */

#if 0
      static void addCampaignFolderAndExtensions(char* buf);
      static void addCampaignFolderAndInfoExtensions(char* buf);

      static void addBattleFolderAndExtensions(char* buf);
      static void addBattleFolderAndInfoExtensions(char* buf);

      // campaign extension
      static void removeWGCextension(char * buf);
      // campaign info extension
      static void addINCextension(char * buf);

      // battle extension
      static void removeWGBextension(char * buf);
      // battle info extension
      static void addINBextension(char * buf);
#endif

//      static void addFolder(char* buf, const char* fileName);
      static Boolean parseFileName(SimpleString& parsed, const char* fileName);
//      static void insertCDPath(char* fileName);

//      static LPARAM missionItemToLParam(const MissionItem* item);
//      static const MissionItem* lParamToMissionItem(LPARAM lParam);

      // static void LoadScenario(const char* scenarioFile);
      // static void CreateScenarioData(void);

      static void baseToScreen(ControlInfo& info, const SIZE& s);
      static void baseToScreen(int& x, int& y, const SIZE& s);
      static HWND createButton(HWND hParent, const char* text, int id);


      /*
      FrontEnd campaign.INF file parsing variables
      */

//       // character buffer for INF file
//       char* file_buffer;
//
//       // tokens & delimiters
//       static const char TOKEN_OpenBracket;
//       static const char TOKEN_CloseBracket;
//       static const char TOKEN_Quote;
//       static const char TOKEN_Space;
//
//       char * TOKEN_Map;
//       char * TOKEN_Text;
//       char * TOKEN_Description;
//       char * TOKEN_FrenchKey;
//       char * TOKEN_AlliedKey;

//       // 2 character locale code with brackets & terminator
//       char LOCALECODE_String[8];
//
//       // section pointers
//       char* DEFAULT_SectionStart;
//       char * DEFAULT_SectionEnd;
//
//       char * LOCALE_SectionStart;
//       char * LOCALE_SectionEnd;
//
//       // filenames & strings
//
//       char * FILENAME_Map;
//       char * STRING_Text;
//       char * STRING_Description;
//       char * STRING_FrenchKey;
//       char * STRING_AlliedKey;


      /*
      FrontEnd campaign.INF file parsing functions
      */

      void InitialiseGlobals(void);
      void DeallocateGlobals(void);

      void ReadLocaleSetings(void);

//      void ParseInfoFile(char const * filename);
//      void ParseInfoFile(char const * filename);
//      void LocateSections(void);
//      void ReadSymbols(char * section_start, char * section_end);
//      char * ScanString(char * src);

      static DIB* loadBMPDIB(const char* name, bool defaultPalette = false);
      static DIBDC* loadBMPDIBDC(const char* name, bool defaultPalette = false);

      static void setPalette(const char* name);
};

// global instance of FrontEndUtilsClass
extern FrontEndUtilsClass FrontEndUtils;




#endif

