/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef TABCTRL_HPP
#define TABCTRL_HPP

#include "ctab.hpp"
#include "wind.hpp"
#include "grtypes.hpp"

class DIB;
class DrawDIB;
class DrawDIBDC;

class FE_TabbedInfoWindow : public WindowBaseND {
         HWND d_hParent;
         // handle of the tab window
         HWND hwndTab;
         // handle of close-button
         HWND hButton;
         
         const DrawDIB* d_infoFillDib;
         const DIB* d_bkDib;

         int d_nItems;
         const char** d_tabText;

         static DrawDIBDC* s_dib;
         static int s_instanceCount;

         PixelRect d_dRect;     // display area rect

         CustomDrawnTab d_cTab;

         enum ID {
//               TimerID = 50,
                 ID_Tab = 100,
                 ID_Close

         };

//       Boolean d_timerActive;

         int d_winCX; 
         int d_winCY;
         const int d_offsetX;
         const int d_offsetY;
         const int d_shadowWidth;

  public:

         FE_TabbedInfoWindow(HWND hParent, RECT * size, int nItems, const char** text);
         ~FE_TabbedInfoWindow();

//       void setFillDib(const DrawDIB* fillDib) { d_fillDib = fillDib; }
         void run(const PixelPoint& p);
         // void destroy();
         // void hide();
         void forceRedraw();
         void fillDisplayArea(DrawDIB * bitmap);
         void fillStatic();
         void setSize(const PixelRect& size);
         const PixelRect& getSize();

         CustomDrawnTab * GetCustomTab(void) { return &d_cTab; }
         HWND GetTabHandle(void) { return hwndTab; }
         virtual void onSelChange() = 0;

  protected:

         DrawDIBDC* dib() const { return s_dib; }
         int currentTab();

        const PixelRect& dRect() const { return d_dRect; }

  private:
         LRESULT procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
         void onCommand(HWND hwnd, int id, HWND hwndCtl, UINT codeNotify);
         BOOL onCreate(HWND hWnd, CREATESTRUCT* lpCreateStruct);
         BOOL onEraseBk(HWND hwnd, HDC hdc);
         void onPaint(HWND hWnd);
         void onDestroy(HWND hWnd);
         LRESULT onNotify(HWND hWnd, int id, NMHDR* lpNMHDR);
         UINT onNCHitTest(HWND hwnd, int x, int y);
         void onMove(HWND hwnd, int x, int y);
         void onTimer(HWND hwnd, UINT id);

};



#endif
