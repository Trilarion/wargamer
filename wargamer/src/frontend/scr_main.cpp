/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
#include "stdinc.hpp"
#include "scr_main.hpp"
#include "f_utils.hpp"
#include "palette.hpp"
#include "dib.hpp"
#include "dib_blt.hpp"
#include "fonts.hpp"
#include "resstr.hpp"

#if defined(DEBUG) && !defined(NOLOG)
#include "clog.hpp"
LogFileFlush MainMenuLog("MainMenu.log");
#endif

/*
 * Button is subclasses
 */

namespace WG_FrontEnd
{

class MainButton : public SubClassWindowBase, public HWNDbase
{
   public:
      MainButton(HWND parent, const char* text, int id);
      ~MainButton();

   private:
      virtual LRESULT procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);

      BOOL onEraseBkgnd(HWND hwnd, HDC hdc);
      void onDestroy(HWND hwnd);
};


MainButton::MainButton(HWND parent, const char* text, int id)
{
    hWnd = CreateWindowEx(
         WS_EX_TRANSPARENT,
        "BUTTON",
        text,
        BS_OWNERDRAW | WS_CHILD,
        0, 0, 0, 0,
        parent,
        (HMENU) id,
        APP::instance(),
        NULL);

      init(hWnd);
}

MainButton::~MainButton()
{
   selfDestruct();
}

LRESULT MainButton::procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
  switch(msg)
  {
      HANDLE_MSG(hWnd, WM_ERASEBKGND, onEraseBkgnd);
      HANDLE_MSG(hWnd, WM_DESTROY, onDestroy);

    default:
      return defProc(hWnd, msg, wParam, lParam);
  }
}

BOOL MainButton::onEraseBkgnd(HWND hwnd, HDC hdc)
{
   return TRUE;
}

void MainButton::onDestroy(HWND hwnd)
{
   clear(hwnd);
}

};

using WG_FrontEnd::MainButton;


/*---------------------------------------------------------------

        FUNCTION DEFINITIONS

---------------------------------------------------------------*/




/*
Constructor takes pointer to the current scenario file
used for loading scenario specific bitmaps, dialogs, etc
*/


MainMenuScreenClass::MainMenuScreenClass(HWND parent)
{

    // create the menu window
    SetParentWindowHandle(parent);

    // make sure that backdground & screen DIBs start out as NULL
    bkDIB = NULL;
    screenDIB = NULL;
    MenuBar = NULL;

    // size the window to fill the front-end screen
    // RECT rect;
    // CalculateScreenSize(&rect);
    calculateSizes();

    strcpy(ScreenNameString,"MainMenuScreen");

    // SetWindowHandle(
    createWindow(
        WS_EX_LEFT,
        // GenericNoBackClass::className(),
        ScreenNameString,
        WS_CHILD, // | WS_CLIPSIBLINGS,
        d_screenRect.left(), d_screenRect.top(), d_screenRect.width(), d_screenRect.height(),
        GetParentWindowHandle(),
        NULL);
        // APP::instance() ));

    SetWindowText(GetWindowHandle(), ScreenNameString);

    // obtain the MainMenuScreen DIB from Wargamer Scenario file
    bkDIB = FrontEndUtilsClass::loadBMPDIB("frontend\\OpeningScreen.bmp");

    // draw the background DIB to the screen DIB
    DrawScreenDIB();

    // create the menu bar
    // RECT bar_rect;
    // CalculateBarSize(&bar_rect);
    MenuBar = new MenuBarClass(parent, d_barRect);

    // set draw state for this window
    ShowWindow(GetWindowHandle(), SW_SHOW);
    UpdateWindow(GetWindowHandle());

}





/*
Default destructor
*/

MainMenuScreenClass::~MainMenuScreenClass(void)
{
    selfDestruct();

    if(bkDIB != NULL) delete(bkDIB);
    if(screenDIB != NULL) delete(screenDIB);

    delete(MenuBar);

}


void MainMenuScreenClass::calculateSizes()
{
    RECT parent_rect;
    GetClientRect(GetParentWindowHandle(),&parent_rect);
    int h = parent_rect.bottom - parent_rect.top;
    int y1 = parent_rect.bottom;
    int y2 = y1 - h / 8;

    d_screenRect = PixelRect(parent_rect.left, parent_rect.top, parent_rect.right, y2);
    d_barRect = PixelRect(parent_rect.left, y2, parent_rect.right, y1);

}


LRESULT
MainMenuScreenClass::procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam) {

        switch(msg) {

            HANDLE_MSG(hWnd, WM_SIZE, onSize);
            HANDLE_MSG(hWnd, WM_PAINT, onPaint);

            default:
                return defProc(hWnd, msg, wParam, lParam);
        }
}








/*
Resize the menu window, bar window & buttons
*/

void
MainMenuScreenClass::onSize(HWND hwnd, UINT state, int cx, int cy) {

    // resize this window in relation to parent window
    // RECT rect;
    // CalculateScreenSize(&rect);

    calculateSizes();

    MoveWindow(GetWindowHandle(), d_screenRect.left(), d_screenRect.top(), d_screenRect.width(), d_screenRect.height(), TRUE);
    // rescale the background DIB into the screenDIB
    DrawScreenDIB();

   if(MenuBar != NULL) {
      // resize the bar
      // CalculateBarSize(&rect);
      MoveWindow(MenuBar->GetWindowHandle(), d_barRect.left(), d_barRect.top(), d_barRect.width(), d_barRect.height(), TRUE);
      // resize the buttons
      MenuBar->SetButtonsPositions();
      // rescale the background DIB into the screenDIB
      MenuBar->DrawScreenDIB();


      // invalidate the whole of the menu bar
      InvalidateRect(MenuBar->GetWindowHandle(), NULL, FALSE);

      // now validate the portions wherer buttons are
      RECT rect;
      GetWindowRect(GetWindowHandle(),&rect);
      int x=rect.left;
      int y=rect.top;

      // validate the area of button A

      GetWindowRect(MenuBar->Button_A->getHWND(),&rect);
      rect.left -= x;
      rect.top -= y;
      rect.right -=x;
      rect.bottom -=y;
      ValidateRect(GetWindowHandle(),&rect);
      // validate the area of button B
      GetWindowRect(MenuBar->Button_B->getHWND(),&rect);
      rect.left -= x;
      rect.top -= y;
      rect.right -=x;
      rect.bottom -=y;
      ValidateRect(GetWindowHandle(),&rect);
      // validate the area of button C
      GetWindowRect(MenuBar->Button_C->getHWND(),&rect);
      rect.left -= x;
      rect.top -= y;
      rect.right -=x;
      rect.bottom -=y;
      ValidateRect(GetWindowHandle(),&rect);
      // validate the area of button D
      GetWindowRect(MenuBar->Button_D->getHWND(),&rect);
      rect.left -= x;
      rect.top -= y;
      rect.right -=x;
      rect.bottom -=y;
      ValidateRect(GetWindowHandle(),&rect);

      // now invalidate each button
      InvalidateRect(MenuBar->Button_A->getHWND(),NULL,FALSE);
      InvalidateRect(MenuBar->Button_B->getHWND(),NULL,FALSE);
      InvalidateRect(MenuBar->Button_C->getHWND(),NULL,FALSE);
      InvalidateRect(MenuBar->Button_D->getHWND(),NULL,FALSE);
   }


}










/*
Paint the menu window
*/

void
MainMenuScreenClass::onPaint(HWND handle) {

// if DIBs aren't loaded then return
if(bkDIB == NULL || screenDIB == NULL) return;


    // allocate device context for painting
    PAINTSTRUCT ps;
    // begin paint session
    HDC hdc = BeginPaint(handle, &ps);

    // integrate palettes
    HPALETTE oldPal = SelectPalette(hdc, Palette::get(), FALSE);
    RealizePalette(hdc);

    // blit screen DIB to window
    BitBlt(hdc,0,0,screenDIB->getWidth(),screenDIB->getHeight(),screenDIB->getDC(),0,0,SRCCOPY);

    // revert to old palette
    SelectPalette(hdc, oldPal, FALSE);

    // end paint session
    EndPaint(handle, &ps);

    UpdateWindow(GetWindowHandle());
}











/*

Menu Bar Functions

*/




MenuBarClass::MenuBarClass(HWND parent, const PixelRect& rect) :
   Button_A(0),
   Button_B(0),
   Button_C(0),
   Button_D(0),
   Button_A_DIB(0),
   Button_B_DIB(0),
   Button_C_DIB(0),
   Button_D_DIB(0),
   Button_A_pushed_DIB(0),
   Button_B_pushed_DIB(0),
   Button_C_pushed_DIB(0),
   Button_D_pushed_DIB(0),
   bkDIB(0),
   screenDIB(0),
   button_screenDIB(0)
{

    // setup window
    SetParentWindowHandle(parent);

    // ensure pointers start as NULL
    bkDIB = NULL;
    screenDIB = NULL;

    // create this window
    // SetWindowHandle(
    createWindow(
        WS_EX_LEFT,
        // GenericNoBackClass::className(),
        "MenuBar",
        WS_CHILD | WS_CLIPSIBLINGS,
        rect.left(), rect.top(), rect.width(), rect.height(),
        parent,
        NULL);
        // APP::instance() ) );

    SetWindowText(GetWindowHandle(),"MenuBar");

    // set navbar background bitmap
    bkDIB = FrontEndUtilsClass::loadBMPDIB("frontend\\Leather1024x86.bmp");
    // draw the background DIB to the screen DIB
    DrawScreenDIB();

    // ensure that pointers start as NULL
    Button_A = Button_B = Button_C = Button_D = NULL;
    Button_A_DIB = Button_B_DIB = Button_C_DIB = Button_D_DIB = NULL;
    Button_A_pushed_DIB = Button_B_pushed_DIB = Button_C_pushed_DIB = Button_D_pushed_DIB = NULL;
    button_screenDIB = NULL;

    // create the bar buttons
    LoadButtonsBitmaps();
    CreateButtons();
    SetButtonsPositions();

    ShowWindow(GetWindowHandle(), SW_SHOW);
    ShowWindow(Button_A->getHWND(), SW_SHOW);
    ShowWindow(Button_B->getHWND(), SW_SHOW);
    ShowWindow(Button_C->getHWND(), SW_SHOW);
    ShowWindow(Button_D->getHWND(), SW_SHOW);

    UpdateWindow(GetWindowHandle());
    UpdateWindow(Button_A->getHWND());
    UpdateWindow(Button_B->getHWND());
    UpdateWindow(Button_C->getHWND());
    UpdateWindow(Button_D->getHWND());
}




MenuBarClass::~MenuBarClass(void) {

    // WindowBase destructor
    selfDestruct();

    // DestroyWindow(Button_A->getHWND());
    // DestroyWindow(Button_B->getHWND());
    // DestroyWindow(Button_C->getHWND());
    // DestroyWindow(Button_D->getHWND());

    delete Button_A;
    delete Button_B;
    delete Button_C;
    delete Button_D;

    delete(Button_A_DIB);  delete(Button_B_DIB);  delete(Button_C_DIB);  delete(Button_D_DIB);
    delete(Button_A_pushed_DIB);  delete(Button_B_pushed_DIB);  delete(Button_C_pushed_DIB);  delete(Button_D_pushed_DIB);

    delete(bkDIB);
    delete(screenDIB);

   delete button_screenDIB;

}





void
MenuBarClass::DrawScreenDIB(void) {

// can't proceed if no background DIB has been loaded
if(bkDIB == NULL) return;

    // get client area of bar
    RECT rect;
    GetClientRect(GetWindowHandle(), &rect);

    int width = rect.right - rect.left;
    int height = rect.bottom - rect.top;

    // allocate screenDIB if NULL or has incorrect dimensions
    if(screenDIB == NULL) {
        screenDIB = new DrawDIBDC(width,height); }

    else if (screenDIB->getWidth() != width || screenDIB->getHeight() != height) {
        delete(screenDIB);
        screenDIB = new DrawDIBDC(width,height); }

    // stretch the background DIB into the screen DIB
    DIB_Utility::stretchDIB(screenDIB,0,0,width,height,bkDIB,0,0,bkDIB->getWidth(),bkDIB->getHeight());

}








void
MenuBarClass::CreateButtons(void) {

    // Button_A = CreateButton(GetWindowHandle(), "NewCampaign", ButtonId_A);
    // Button_B = CreateButton(GetWindowHandle(), "NewBattle", ButtonId_B);
    // Button_C = CreateButton(GetWindowHandle(), "LoadCampaign", ButtonId_C);
    // Button_D = CreateButton(GetWindowHandle(), "LastCampaign", ButtonId_D);

    Button_A = new MainButton(GetWindowHandle(), InGameText::get(IDS_FE_NEWCAMPAIGN), ButtonId_A);
    Button_B = new MainButton(GetWindowHandle(), InGameText::get(IDS_FE_NEWBATTLE),   ButtonId_B);
    Button_C = new MainButton(GetWindowHandle(), InGameText::get(IDS_FE_LoadGame),    ButtonId_C);
    Button_D = new MainButton(GetWindowHandle(), InGameText::get(IDS_FE_LastGame),    ButtonId_D);

}



void
MenuBarClass::SetButtonsPositions(void) {

   if(Button_A != NULL && Button_B != NULL && Button_C != NULL && Button_D != NULL)
   {
      RECT parent_rect;
      GetClientRect(GetWindowHandle(), &parent_rect);

      // set up various dimensions
      int width = parent_rect.right;
      int height = parent_rect.bottom;

      int quater_width = width / 4;
      int button_width = width / 8;
      int half_button_width = button_width / 2;

      int x_offst = button_width + half_button_width;

      int ypos = 0;
      int button_height = height;

      // place the buttons
      int xpos = quater_width - x_offst;
      MoveWindow(Button_A->getHWND(),xpos,ypos,button_width,button_height,FALSE);

      xpos = (quater_width * 2) - x_offst;
      MoveWindow(Button_B->getHWND(),xpos,ypos,button_width,button_height,FALSE);

      xpos = (quater_width * 3) - x_offst;
      MoveWindow(Button_C->getHWND(),xpos,ypos,button_width,button_height,FALSE);

      xpos = (quater_width * 4) - x_offst;
      MoveWindow(Button_D->getHWND(),xpos,ypos,button_width,button_height,FALSE);

   }

}



#if 0
void
MenuBarClass::SetButtonsPositions(void) {

   if(Button_A != NULL && Button_B != NULL && Button_C != NULL && Button_D != NULL)
   {
      RECT parent_rect;
      GetClientRect(GetWindowHandle(), &parent_rect);

      // set up various dimensions
      int width = parent_rect.right;
      int height = parent_rect.bottom;

      int quater_width = width / 4;
      int x_inset = quater_width / 8;

      int button_width = quater_width - x_inset/2;

      int xpos = x_inset;
      int ypos = 0;
      int button_height = height;

      // place the buttons
      MoveWindow(Button_A->getHWND(),xpos,ypos,button_width,button_height,FALSE);

      xpos+=quater_width;
      MoveWindow(Button_B->getHWND(),xpos,ypos,button_width,button_height,FALSE);

      xpos+=quater_width;
      MoveWindow(Button_C->getHWND(),xpos,ypos,button_width,button_height,FALSE);

      xpos+=quater_width;
      MoveWindow(Button_D->getHWND(),xpos,ypos,button_width,button_height,FALSE);

   }
}
#endif


void
MenuBarClass::LoadButtonsBitmaps(void) {

    Button_A_DIB = FrontEndUtilsClass::loadBMPDIBDC("frontend\\NewCampaign.bmp");
    Button_A_pushed_DIB = FrontEndUtilsClass::loadBMPDIBDC("frontend\\NewCampaign-in.bmp");

    Button_B_DIB = FrontEndUtilsClass::loadBMPDIBDC("frontend\\NewBattle.bmp");
    Button_B_pushed_DIB = FrontEndUtilsClass::loadBMPDIBDC("frontend\\NewBattle-in.bmp");

    Button_C_DIB = FrontEndUtilsClass::loadBMPDIBDC("frontend\\OpenSavedGame.bmp");
    Button_C_pushed_DIB = FrontEndUtilsClass::loadBMPDIBDC("frontend\\OpenSavedGame-in.bmp");

    Button_D_DIB = FrontEndUtilsClass::loadBMPDIBDC("frontend\\OpenLastGame.bmp");
    Button_D_pushed_DIB = FrontEndUtilsClass::loadBMPDIBDC("frontend\\OpenLastGame-in.bmp");

}





/*
Stretch a bitmap into a owner-drawn button
*/

void MenuBarClass::DrawButtonBitmap(HWND button, DIB * bitmap, const DRAWITEMSTRUCT* lpDrawItem, int blitmode) {

    if(bitmap == NULL)
      return;

    // get dimensions of the button from the windows structure
    int width = lpDrawItem->rcItem.right - lpDrawItem->rcItem.left;
    int height = lpDrawItem->rcItem.bottom - lpDrawItem->rcItem.top;

    // stretch the static bitmap into the screen bitmap
    // maintaining correct aspect ratio!

    int bmWidth = bitmap->getWidth();
    int bmHeight = bitmap->getHeight();

    int textHeight = height / 4;
    textHeight = minimum(textHeight, 16);
    int allowedHeight = height - textHeight;    // leave space for text

    int destW = width;
    int destH = (destW * bmHeight) / bmWidth;

    if(destH > allowedHeight)
    {
      destH = allowedHeight;
      destW = (destH * bmWidth) / bmHeight;
    }
    ASSERT(destW <= width);
    ASSERT(destH <= height);

    int destX = lpDrawItem->rcItem.left + (width - destW) / 2;
    int destY = lpDrawItem->rcItem.top + (allowedHeight - destH) / 2;

    // if no screenDIB defined, then allocate
    if(button_screenDIB == NULL)
    {
        button_screenDIB = new DrawDIBDC(destW, destH);
    }
    else if(button_screenDIB->getWidth() != destW || button_screenDIB->getHeight() != destH)
    {    // or if screenDIB is the wrong size, delete & reallocate
        button_screenDIB->resize(destW, destH);
    }

    DIB_Utility::stretchDIB(button_screenDIB, 0, 0, destW, destH,
                                       bitmap, 0, 0, bmWidth, bmHeight);

    // blit screen DIBDC to button DC
    BitBlt(lpDrawItem->hDC,destX, destY, button_screenDIB->getWidth(), button_screenDIB->getHeight(),button_screenDIB->getDC(),0,0,blitmode);

    // Draw Text underneath

    const int MaxLength = 80;
    char text[MaxLength];
    int length = GetWindowText(button, text, MaxLength);
    ASSERT(length > 0);

    RECT textRect = lpDrawItem->rcItem;
    textRect.top += allowedHeight;

    LogFont lf;

    // lf.height(-MulDiv(allowedHeight, GetDeviceCaps(lpDrawItem->hDC, LOGPIXELSY), 72));
    lf.height(textHeight);
    Fonts font;
    font.set(lpDrawItem->hDC, lf);

    int oldBkMode = SetBkMode(lpDrawItem->hDC, TRANSPARENT);

    COLORREF oldCol = SetTextColor(lpDrawItem->hDC, scenario->getColour("MenuText"));

    DrawText(lpDrawItem->hDC, text, lstrlen(text), &textRect, DT_CENTER | DT_VCENTER);

    SetTextColor(lpDrawItem->hDC, oldCol);
    SetBkMode(lpDrawItem->hDC, oldBkMode);

}




/*
Message processing is derived from the "procMessage" virtual base function
*/

LRESULT
MenuBarClass::procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam) {

        switch(msg) {

            HANDLE_MSG(hWnd, WM_COMMAND, onCommand);
            HANDLE_MSG(hWnd, WM_PAINT, onPaint);
            HANDLE_MSG(hWnd, WM_DRAWITEM, onDrawItem);
            HANDLE_MSG(hWnd, WM_CTLCOLORBTN, onCtlColorBtn);
            HANDLE_MSG(hWnd, WM_ERASEBKGND, onEraseBkgnd);

            default:
                return defProc(hWnd, msg, wParam, lParam);
        }
}


/*
Forward any commands up to parent window - which in turn passes them up to FrontEnd window
*/


void
MenuBarClass::onCommand(HWND hwnd, int id, HWND hwndCtl, UINT codeNotify) {

FORWARD_WM_COMMAND(GetParentWindowHandle(), id, hwndCtl, codeNotify, PostMessage);

}






void
MenuBarClass::onPaint(HWND handle) {

// if DIBs aren't loaded then return
if(bkDIB == NULL || screenDIB == NULL) return;


    // allocate device context for painting
    PAINTSTRUCT ps;
    // begin paint session
    HDC hdc = BeginPaint(handle, &ps);

    // integrate palettes
    HPALETTE oldPal = SelectPalette(hdc, Palette::get(), FALSE);
    RealizePalette(hdc);

    // blit screen DIB to window
    BitBlt(hdc,0,0,screenDIB->getWidth(),screenDIB->getHeight(),screenDIB->getDC(),0,0,SRCCOPY);

    // revert to old palette
    SelectPalette(hdc, oldPal, FALSE);

    // end paint session
    EndPaint(handle, &ps);

}




/*
Process drawing of UsedDrawn buttons
Each button may be normal or pushed
*/


void
MenuBarClass::onDrawItem(HWND handle, const DRAWITEMSTRUCT* lpDrawItem) {
int pushed=0;

switch(lpDrawItem->CtlID) {

    case ButtonId_A : {  // Button A
        pushed = SendMessage(Button_A->getHWND(), BM_GETSTATE, 0, 0);
        if(pushed & BST_PUSHED) DrawButtonBitmap(Button_A->getHWND(), Button_A_pushed_DIB, lpDrawItem, SRCCOPY);
        else DrawButtonBitmap(Button_A->getHWND(), Button_A_DIB, lpDrawItem,SRCCOPY);
    break; }


    case ButtonId_B : {  // Button B
        pushed = SendMessage(Button_B->getHWND(), BM_GETSTATE, 0, 0);
        if(pushed & BST_PUSHED) DrawButtonBitmap(Button_B->getHWND(), Button_B_pushed_DIB, lpDrawItem,SRCCOPY);
        else DrawButtonBitmap(Button_B->getHWND(), Button_B_DIB, lpDrawItem,SRCCOPY);
    break; }


    case ButtonId_C : {  // Button C
        pushed = SendMessage(Button_C->getHWND(), BM_GETSTATE, 0, 0);
        if(pushed & BST_PUSHED) DrawButtonBitmap(Button_C->getHWND(), Button_C_pushed_DIB, lpDrawItem,SRCCOPY);
        else DrawButtonBitmap(Button_C->getHWND(), Button_C_DIB, lpDrawItem,SRCCOPY);
    break; }


    case ButtonId_D : {  // Button D
        pushed = SendMessage(Button_D->getHWND(), BM_GETSTATE, 0, 0);
        if(pushed & BST_PUSHED) DrawButtonBitmap(Button_D->getHWND(), Button_D_pushed_DIB, lpDrawItem,SRCCOPY);
        else DrawButtonBitmap(Button_D->getHWND(), Button_D_DIB, lpDrawItem,SRCCOPY);
    break; }



    default : {
        FORCEASSERT("ERROR - invalid button ID in owner-draw situation !");
    break; }

}  // end of switch

}



HBRUSH
MenuBarClass::onCtlColorBtn(HWND hwnd, HDC hdc, HWND hwndChild, int type) {

    return NULL;

}


/*
Return true to signify that no more background erasing is needed
*/

BOOL
MenuBarClass::onEraseBkgnd(HWND hwnd, HDC hdc) {

    return TRUE;

}




/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.2  2001/06/18 08:25:45  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
