/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef SCR_SIDE_HPP
#define SCR_SIDE_HPP

#include "menubase.hpp"
#include "palette.hpp"
#include "DIB.hpp"
#include "gamedefs.hpp"





class ChooseSidesScreenClass : public MenuScreenBaseClass {

public:

    ChooseSidesScreenClass(HWND parent);
    ~ChooseSidesScreenClass(void);


private:

	bool d_mouseDownNapoleon;
	bool d_mouseDownPrussia;

	PixelRect d_mainRect;

	// leather background
    DIB * bkDIB;
	// main screen DIB
    DrawDIBDC * screenDIB;

	// flags
	DIB * napoleonSelectedDIB;
	DIB * napoleonDeselectedDIB;
	DIB * prussiaSelectedDIB;
	DIB * prussiaDeselectedDIB;

	// plaques
	DIB * plaqueDIB;

	// rects for the above items
	RECT napoleonRect;
	RECT prussiaRect;
	RECT frenchRect;
	RECT alliedRect;

	void calculateWindowSizes(void);
	void drawScreenDIB(void);
   void drawSide(Side side, DIB* portrait, const RECT& picRect, const RECT& plaqueRect);

    LRESULT procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
    void onCommand(HWND hwnd, int id, HWND hwndCtl, UINT codeNotify);
    void onSize(HWND hwnd, UINT state, int cx, int cy);
    void onPaint(HWND handle);
	void onLButtonDown(HWND hwnd, BOOL fDoubleClick, int x, int y, UINT keyFlags);
	void onLButtonUp(HWND hwnd, int x, int y, UINT keyFlags);

};

#endif
