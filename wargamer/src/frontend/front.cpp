/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 * Implements the FrontEndwindow, containing the NavigateBar and MenuScreen
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "front.hpp"
#include "navbar.hpp"
#include "titlebar.hpp"
#include "menubase.hpp"
#include "f_utils.hpp"
#include "fonts.hpp"
#include "wg_msg.hpp"

// menu screen classes
#include "scr_main.hpp"
#include "scr_camp.hpp"
#include "scr_side.hpp"
#include "scr_opt.hpp"
#include "scr_batt.hpp"
#include "scr_load.hpp"
#include "scr_last.hpp"

#include "scn_img.hpp"
#include "scrnbase.hpp"
#include "savegame.hpp"

// system includes
#include "wind.hpp"
#include "app.hpp"
#include "scenario.hpp"
#include "cbutton.hpp"
#include "resdef.h"
#include "resstr.hpp"
#include "cmenu.hpp"
#include "fileiter.hpp"

#include "titlebar.hpp"

#include "soundsys.hpp"
#include "splash.hpp"

#include "control.hpp"
#include "options.hpp"

#include "MultiPlayerDialog.hpp"
#include "MultiPlayerMsg.hpp"
#include "MultiPlayerConnection.hpp"


#if defined(DEBUG) && !defined(NOLOG)
#include "clog.hpp"
LogFileFlush FrontEndLog("FrontEnd.log");
#endif



#define MULTIPLAYER

class FrontCentralWindow : public WindowBaseND
{
        public:
                FrontCentralWindow();
                ~FrontCentralWindow();
                void create(HWND parent, const RECT& r);

                void resize(const RECT& r);

        private:
                LRESULT procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
                void onCommand(HWND hwnd, int id, HWND hwndCtl, UINT codeNotify);

};

FrontCentralWindow::FrontCentralWindow()
{
}

FrontCentralWindow::~FrontCentralWindow()
{
}

void FrontCentralWindow::create(HWND parent, const RECT& r)
{
        ASSERT(getHWND() == NULL);

        createWindow(
                0,
                // className(),
                "FrontEnd CentralWindow",
                WS_CHILD | WS_VISIBLE,
                r.left, r.top, r.right-r.left, r.bottom-r.top,
                parent,
                NULL);
                // APP::instance());

}

void FrontCentralWindow::resize(const RECT& r)
{
        MoveWindow(getHWND(), r.left, r.top, r.right-r.left, r.bottom-r.top, FALSE);
}


LRESULT FrontCentralWindow::procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
        switch(msg)
        {
                HANDLE_MSG(hWnd, WM_COMMAND, onCommand);
        default:
         return defProc(hWnd, msg, wParam, lParam);
        }
}

void FrontCentralWindow::onCommand(HWND hwnd, int id, HWND hwndCtl, UINT codeNotify)
{
        HWND parent = GetParent(hWnd);
        ASSERT(parent);
        if(parent != NULL)
        {
                FORWARD_WM_COMMAND(parent, id, hwndCtl, codeNotify, PostMessage);
        }
}



/*
 * Hidden Implementation Class
 */

class FrontEndClass : public SubClassWindowBase, public CMultiPlayerDialogUser, public CDirectPlayUser
{

    /*---------------------------------------------------------------
     *
     *    FUNCTION PROTOTYPES
     *
     *---------------------------------------------------------------*/

    public:


        FrontEndClass(GameOwner* owner, const char* scenarioFile);
        ~FrontEndClass(void);
        void Setup(void);  // a temporary measure because can't use constructor to initialise

        inline HWND getHWND(void) { return window_handle; }

      // these are now public
      void doNewGame(const char * current, SaveGame::FileFormat format);
        void doStartGame(const char* fileName);
        void doLoadGame();
        void doLastGame();
        void doQuit();

    private:

        LRESULT procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
        void onSize(HWND hwnd, UINT state, int cx, int cy);
        void onCommand(HWND hwnd, int id, HWND hwndCtl, UINT codeNotify);
        void onPaint(HWND handle);
//        void onClose(HWND handle);
        void onDestroy(HWND handle);

        void SetupScreens(void);  // creates the correct instance of MenuScreen & NavigateBar depending on mode settings
        void RefreshScreens(void);

      void calculateBarSizes(void);
        PixelRect getCentralSize() const;

        void NextMenuScreen(void);
        void PrevMenuScreen(void);
        void Quit(void);
        void Help(void);

        void getOldGames();
                // Get previous filenames from system registry

        void setupMenu();
        bool doDynamicMenu(UWORD id);
        bool onNewScenario(const char* fileName);
        void createScenarioData();
        void loadScenario(const char* scenarioFile);


        void setNavBarMode(NavigateBarClass::BarModeEnum mode);
        void setTitleBarMode(TitleBarClass::ModeEnum mode);
        void setTitleBarText(const char * text);

      void showScreens();
      void hideScreens();

      /*
      * Interfaces functions from CMultiPlayerDialogUser
      */

      virtual void connectionHosting(void);
      virtual void connectionJoined(void);
      virtual void connectionCancelled(void);


      virtual void onNewMessage(LPVOID data, DWORD data_length) { }
      virtual void onOpponentJoin(void);
      virtual void onOpponentQuit(void) { }
      virtual void onConnectionLost(void) { }
      virtual void onChatMessage(LPSTR text) { }
      virtual void onError(char * context, char * desc) { }

    /*---------------------------------------------------------------
     *
     *    Data Members
     *
     *---------------------------------------------------------------*/

    private:

        HWND window_handle;  // handle of this window
        // GameOwner* d_owner;
        CustomMenu d_menu;

        char ScreenNameString[128];

        NavigateBarClass* NavigateBar;  // navigator bar displayed at the bottom of most front end menus
      TitleBarClass* TitleBar;      // title bar at top of screen, below menu
        MenuScreenBaseClass* MenuScreen;  // menu screen which occupies the rest of the screen
        FrontCentralWindow* d_centralWindow;                         // Central Window area

      RECT navBarRect;
      RECT titleBarRect;

      enum GameSelectMode {
         Undefined,
         Campaign,
         Battle
      };

      GameSelectMode d_gameMode;

      GameSelectMode gameMode(void) { return d_gameMode; }
      void gameMode(GameSelectMode mode) { d_gameMode = mode; }

        enum FrontEndModeEnum {  // possible functional modes of front-end defining which menu screen to instance
         MainMenuScreen,
         ChooseCampaignScreen,
         ChooseSidesScreen,
         OptionsScreen,
         ChooseBattleScreen,
         LoadGameScreen,
         LastGameScreen
      };

        unsigned int FrontEndMode;  // which mode of operation the front end is currently in

        StringPtr d_fileName;           // File to load when starting game

        SList<GameFile> d_scenarioFiles;    // list of scenario files
        SList<GameFile> d_savedGames;       // list of previous saved games
        SimpleString    d_scenarioFileName;

        SaveGame::FileFormat d_format;

      CMultiPlayerDialog * d_multiplayerDialog;

      GameOwner* d_owner;

}; // end of FrontEndClass




/*---------------------------------------------------------------

        FUNCTION DEFINITIONS

---------------------------------------------------------------*/









/*
FrontEnd constructor, taking same parameters as normal FrontEndWindow from FRONTEND.CPP
*/


FrontEndClass::FrontEndClass(GameOwner* owner, const char* scenarioFile) :
      d_owner(owner),
        SubClassWindowBase(owner->hwnd()),
        window_handle(owner->hwnd()),
        // d_owner(owner),
        d_menu(),
        NavigateBar(0),
      TitleBar(0),
        MenuScreen(0),
        d_centralWindow(0),
        FrontEndMode(MainMenuScreen),
      d_gameMode(FrontEndClass::Undefined),
        d_format(SaveGame::Campaign),
      d_multiplayerDialog(0)
{
   /*
   *
   * start Wargamer : Napoleon 1813 theme music
   *
   */
   GlobalSoundSystemObj.PlayCDTrack(3);

    /*
     * Allocate the globals needed by front end for holding string & parsing tokens
     * A call must be made to DeallocateGlobals() on destruction
     */

    FrontEndUtils.InitialiseGlobals();

    /*
     * Read the regional language settings from system
     * This defines strings used for tooltips & campaign / battle description parsing
     */

    FrontEndUtils.ReadLocaleSetings();

    /*
     * Get old games from registry
     */

    getOldGames();

    /*
     * Custom buttons must be set up before use
     */

    CustomButton::initCustomButton(APP::instance());


    /*
     * Load scenario
     */

    loadScenario(scenarioFile);
    ASSERT(scenario != NULL);


    /*
     * Remove existing menu
     */

    HMENU hMenu = GetMenu(getHWND());
    if(hMenu) {

      SetMenu(getHWND(), NULL);
      DestroyMenu(hMenu);
   }

   HRESULT result = SetMenu(getHWND(), hMenu);
   ASSERT(result);

   /*
    * Set menu data
    */

    CustomMenuData md;
    md.d_type = CustomMenuData::CMT_BarMenu;
    md.d_menuID = MENU_STARTSCREEN;
    md.d_fillDib = scenario->getScenarioBkDIB(Scenario::BackPatterns::CherryWoodBackground);
    md.d_checkImages = ScenarioImageLibrary::get(ScenarioImageLibrary::CustomMenuImages);
    md.d_hCommand = window_handle;
    md.d_hParent = window_handle;
    md.d_borderColors = scenario->getBorderColors();
    scenario->getColour("MenuText", md.d_color);
    scenario->getColour("MenuHilite", md.d_hColor);

   /*
    * Set menu fonts (one normal, the other underlined
    */

    LogFont lf;
    lf.height((8 * ScreenBase::dbY()) / ScreenBase::baseY());
    lf.weight(FW_MEDIUM);
    lf.face(scenario->fontName(Font_Bold));

    Font font;
    font.set(lf);
    md.d_hFont = font;

    lf.underline(true);
    font.set(lf);
    md.d_hULFont = font;

    /*
     * initialize menu
     */

    d_menu.init(md);
    d_menu.run(PixelPoint(0,0));

   /*
   Reset player control, in case it was left as Player vs. Remote before
   */
   if(GamePlayerControl::getControl(0) == GamePlayerControl::Remote) {
      GamePlayerControl::setControl(0, GamePlayerControl::AI);
      GamePlayerControl::setControl(1, GamePlayerControl::Player);
   }
   if(GamePlayerControl::getControl(1) == GamePlayerControl::Remote) {
      GamePlayerControl::setControl(1, GamePlayerControl::AI);
      GamePlayerControl::setControl(0, GamePlayerControl::Player);
   }

#ifdef MULTIPLAYER

   /*
    * Add MultiPlayer item to menu
    */
   MenuWindow* hcMenu = d_menu.getSubMenu(0,0);
   MenuWindow* fileMenu = d_menu.getSubMenu(hcMenu, 0);//IDM_BAT_MENU_DEBUG);
   // put item before 'Quit'
   int i = d_menu.getMenuItemCount(fileMenu) - 2;

    MenuItemInfo miSep;
    miSep.d_id = -1;
    miSep.d_state = MenuItemInfo::CMS_Seperator;
    miSep.d_subMenu = 0;
    miSep.d_text = "---";
    SetRect(&miSep.d_rect, 0,0,0,0);
    d_menu.insertMenuItem(fileMenu, i++, TRUE, &miSep);

    MenuItemInfo mi;
    mi.d_state = MenuItemInfo::CMS_Enabled;
    mi.d_subMenu = 0;
    SetRect(&mi.d_rect, 0,0,0,0);

    mi.d_id = FE_IDM_Multiplayer;
    mi.d_text = "Multiplayer";
    d_menu.insertMenuItem(fileMenu, i++, TRUE, &mi);

    d_menu.insertMenuItem(fileMenu, i++, TRUE, &miSep);


#endif

    /*
     * Create top & bottom bars
     */

   calculateBarSizes();
    NavigateBar = new NavigateBarClass(getHWND(),NavigateBarClass::Bar_Hidden, &navBarRect);
   TitleBar = new TitleBarClass(getHWND(), TitleBarClass::Hidden, &titleBarRect);

    /*
     * Setup initial central screen
     */

    MenuScreen = NULL;
    d_centralWindow = new FrontCentralWindow;
    strcpy(ScreenNameString,"FrontEndWindow(SubClass)");
    FrontEndMode = MainMenuScreen;

    /*
     * Create central screen
     */

    PixelRect r = getCentralSize();
    ASSERT(d_centralWindow);
    d_centralWindow->create(getHWND(), r);

    setupMenu();
    SetupScreens();

}





/*
FrontEndDestructor
*/


FrontEndClass::~FrontEndClass(void) {

    // window classes must be destroyed by parent, or externally
    // their destructors deal with freeing up class specific information

    if(MenuScreen != NULL)
         {
                // DestroyWindow(MenuScreen->getHWND());
                delete MenuScreen;
                MenuScreen = 0;
         }

   if(TitleBar != NULL) {
      // DestroyWindow(TitleBar->getHWND());
       delete TitleBar;
       TitleBar = 0;
   }

    if(NavigateBar != NULL)
         {
                // DestroyWindow(NavigateBar->getHWND());
                delete NavigateBar;
                NavigateBar = 0;
         }

    if(d_centralWindow)
         {
                delete d_centralWindow;
                d_centralWindow = 0;
         }



    SubClassWindowBase::clear(getHWND());

    FrontEndUtils.DeallocateGlobals();
}



void
FrontEndClass::calculateBarSizes(void) {

   RECT windowRect;
   GetClientRect(getHWND(), &windowRect);
   int cx = windowRect.right;
   int cy = windowRect.bottom;

   navBarRect.left = 0;
   navBarRect.top = cy - (cy/16);
   navBarRect.right = cx;
   navBarRect.bottom = cy - navBarRect.top;

   int h = cy / 16;
   titleBarRect.left = 0;
   titleBarRect.top = d_menu.height();
   titleBarRect.right = cx;
   titleBarRect.bottom = h + d_menu.height();

}




PixelRect FrontEndClass::getCentralSize() const
{
        PixelRect r;

        GetClientRect(window_handle, &r);

        ASSERT(NavigateBar);

        if(NavigateBar && (NavigateBar->getMode() != NavigateBarClass::Bar_Hidden))
        {
                PixelRect navRect;

                GetWindowRect(NavigateBar->getHWND(), &navRect);
                POINT p;
                p.x = navRect.left();
                p.y = navRect.top();
                ScreenToClient(window_handle, &p);
                r.bottom(p.y);

        }

      ASSERT(TitleBar);

      if(TitleBar && (TitleBar->getMode() != TitleBarClass::Hidden)) {

         PixelRect titleRect;

         GetWindowRect(TitleBar->getHWND(), &titleRect);
         POINT p;
         p.x = titleRect.left();
         p.y = titleRect.bottom();
         ScreenToClient(window_handle, &p);
         r.top(p.y);
      }

      else r.top(r.top() + d_menu.height());

        ASSERT(r.top() < r.bottom());

        return r;
}










/*
Change the FrontEndMode to the correct MenuScreen enumeration
This function follows possible routes through the MenuScreens when then NEXT / START button on the bar is pressed
*/


void
FrontEndClass::NextMenuScreen(void) {

    switch(FrontEndMode) {

      case MainMenuScreen : {
         break;
      }

      case ChooseCampaignScreen : {
         FrontEndMode = ChooseSidesScreen;
         gameMode(FrontEndClass::Campaign);


         #ifdef MULTIPLAYER
         /*
         send chosen campaign filename to multiplayer opponent
         */
         MP_MSG_SETGAME setgame_msg;
         setgame_msg.msg_type = MP_MSG_SetGame;
         strcpy(setgame_msg.gamefilename, d_fileName.toStr() );
         setgame_msg.checksum = 0; // TODO : make this meaningful
         setgame_msg.format = (int) d_format;

         if(g_directPlay) {
#ifdef DEBUG
            FrontEndLog.printf("Sending MP_MSG_SETGAME for campaign to opponent\n");
#endif
            g_directPlay->sendMessage(
               (LPVOID) &setgame_msg,
               sizeof(MP_MSG_SETGAME)
            );
         }
         else {
#ifdef DEBUG
            FrontEndLog.printf("No opponent setup to propogate MP_MSG_SETGAME to\n");
#endif
         }
         #endif

         break;
      }

      case ChooseSidesScreen : {
         FrontEndMode = OptionsScreen;

         #ifdef MULTIPLAYER

         if(CMultiPlayerConnection::connectionType() != ConnectionNone) {
            /*
            Notify remote player that we have chosen a side to play
            */
            MP_MSG_SETSIDE setside_msg;
            setside_msg.msg_type = MP_MSG_SetSide;
            if(GamePlayerControl::getControl(0) == GamePlayerControl::Player) {
               setside_msg.opponent_side = 0;
               GamePlayerControl::setControl(1, GamePlayerControl::Remote);
            }
            else if(GamePlayerControl::getControl(1) == GamePlayerControl::Player) {
               setside_msg.opponent_side = 1;
               GamePlayerControl::setControl(0, GamePlayerControl::Remote);
            }

            if(g_directPlay) {

               #ifdef DEBUG
               FrontEndLog.printf("Sending MP_MSG_SETSIDE to opponent\n");
               #endif

               g_directPlay->sendMessage(
                  (LPVOID) &setside_msg,
                  sizeof(MP_MSG_SETSIDE)
               );
            }
            else {

               #ifdef DEBUG
               FrontEndLog.printf("No opponent setup to propogate MP_MSG_SETSIDE to\n");
               #endif
            }

         }
            #endif

         break;
      }

      case OptionsScreen : {

         #ifdef MULTIPLAYER
         /*
         sende options settings to opponent
         send start-game prompt multiplayer opponent
         opponent may need to have missing files downloaded first
         */
         MP_MSG_SETOPTIONS setoptions_msg;
         setoptions_msg.msg_type = MP_MSG_SetOptions;
         setoptions_msg.options = Options::getBitfield();
         setoptions_msg.campaign_options = CampaignOptions::getBitfield();

         if(g_directPlay) {
#ifdef DEBUG
            FrontEndLog.printf("Sending MP_MSG_SETOPTIONS to opponent\n");
#endif
            g_directPlay->sendMessage(
               (LPVOID) &setoptions_msg,
               sizeof(MP_MSG_SETOPTIONS)
            );
         }
         else {
#ifdef DEBUG
            FrontEndLog.printf("No opponent setup to propogate MP_MSG_SETOPTIONS to\n");
#endif
         }


         MP_MSG_STARTGAME startgame_msg;
         startgame_msg.msg_type = MP_MSG_StartGame;


         if(g_directPlay) {
#ifdef DEBUG
            FrontEndLog.printf("Sending MP_MSG_STARTGAME to opponent\n");
#endif
            g_directPlay->sendMessage(
               (LPVOID) &startgame_msg,
               sizeof(MP_MSG_STARTGAME)
            );
         }
         else {
#ifdef DEBUG
            FrontEndLog.printf("No opponent setup to propogate MP_MSG_STARTGAME to\n");
#endif
         }

         #endif

         doNewGame(d_fileName.toStr(), d_format); // Added by Steven
         break;
      }  // start game

      case ChooseBattleScreen : {
         FrontEndMode = ChooseSidesScreen;
         gameMode(FrontEndClass::Battle);

         #ifdef MULTIPLAYER
         /*
         send chosen battle filename to multiplayer opponent
         */
         MP_MSG_SETGAME setgame_msg;
         setgame_msg.msg_type = MP_MSG_SetGame;
         strcpy(setgame_msg.gamefilename, d_fileName.toStr() );
         setgame_msg.checksum = 0; // TODO : make this meaningful
         setgame_msg.format = (int) d_format;

         if(g_directPlay) {
#ifdef DEBUG
            FrontEndLog.printf("Sending MP_MSG_SETGAME for battle to opponent\n");
#endif
            g_directPlay->sendMessage(
               (LPVOID) &setgame_msg,
               sizeof(MP_MSG_SETGAME)
            );
         }
         else {
#ifdef DEBUG
            FrontEndLog.printf("No opponent setup to propogate MP_MSG_SETGAME to\n");
#endif
         }
         #endif

         break;
      }

    } // switch
}



/*
Change the FrontEndMode to the correct MenuScreen enumeration
This function follows possible routes through the MenuScreens when then PREV button on the bar is pressed
*/


void
FrontEndClass::PrevMenuScreen(void) {

    switch(FrontEndMode) {

        case MainMenuScreen: {
        break; }

        case ChooseCampaignScreen: {
            FrontEndMode = MainMenuScreen;
         gameMode(FrontEndClass::Undefined);
        break; }

        case ChooseSidesScreen: {
         if(gameMode() == FrontEndClass::Campaign) FrontEndMode = ChooseCampaignScreen;
         else if(gameMode() == FrontEndClass::Battle) FrontEndMode = ChooseBattleScreen;
        break; }

        case OptionsScreen: {
            FrontEndMode = ChooseSidesScreen;
        break; }

        case ChooseBattleScreen: {
            FrontEndMode = MainMenuScreen;
         gameMode(FrontEndClass::Undefined);
        break; }

    }
}


/*
Display the help screen
*/

void
FrontEndClass::Help(void) {

}


/*
Display the quit screen
*/

void
FrontEndClass::Quit(void) {

// this should prompt the player wether they really want to quit, but for now it just closes

PostMessage(getHWND(), WM_CLOSE, NULL, NULL);


}


void FrontEndClass::setNavBarMode(NavigateBarClass::BarModeEnum mode)
{
        NavigateBar->SetBarMode(mode);

        d_centralWindow->resize(getCentralSize());
}


void FrontEndClass::setTitleBarMode(TitleBarClass::ModeEnum mode)
{
        TitleBar->setMode(mode);

        d_centralWindow->resize(getCentralSize());
}


void FrontEndClass::setTitleBarText(const char * text)
{
        TitleBar->setText(text);

}



void FrontEndClass::showScreens(void) {

   NavigateBar->DisplayBar();
   ShowWindow(TitleBar->getHWND(), SW_SHOW);
   ShowWindow(d_centralWindow->getHWND(), SW_SHOW);
   ShowWindow(getHWND(), SW_SHOW);
}

void FrontEndClass::hideScreens(void) {

   ShowWindow(NavigateBar->getHWND(), SW_HIDE);
   ShowWindow(TitleBar->getHWND(), SW_HIDE);
   ShowWindow(d_centralWindow->getHWND(), SW_HIDE);

}


/*
Given the current FrontEndMode setting, this function instances the correct Navigate Bar & Menu Screen
*/

void
FrontEndClass::SetupScreens(void) {


    switch(FrontEndMode) {

        case MainMenuScreen: {
            setNavBarMode(NavigateBarClass::Bar_Hidden);
         setTitleBarText(0);
         setTitleBarMode(TitleBarClass::Hidden);
            MenuScreen = new MainMenuScreenClass(d_centralWindow->getHWND());
            ASSERT(MenuScreen != NULL);
        break; }

        case ChooseCampaignScreen: {
            setNavBarMode(NavigateBarClass::Bar_Normal);
         setTitleBarText(InGameText::get(IDS_TITLE_ChooseCampaign));
         setTitleBarMode(TitleBarClass::Normal);
            d_format = SaveGame::Campaign;
            MenuScreen = new ChooseCampaignScreenClass(d_centralWindow->getHWND(), &d_fileName);
            ASSERT(MenuScreen != NULL);
        break; }

        case ChooseSidesScreen: {
            setNavBarMode(NavigateBarClass::Bar_Normal);
         setTitleBarText(InGameText::get(IDS_TITLE_ChooseSides));
         setTitleBarMode(TitleBarClass::Normal);
            MenuScreen = new ChooseSidesScreenClass(d_centralWindow->getHWND());
            ASSERT(MenuScreen != NULL);
        break; }

        case OptionsScreen: {
            setNavBarMode(NavigateBarClass::Bar_Final);
         setTitleBarText(InGameText::get(IDS_TITLE_GameOptions));
         setTitleBarMode(TitleBarClass::Normal);
            MenuScreen = new OptionsScreenClass(d_centralWindow->getHWND());
            ASSERT(MenuScreen != NULL);
        break; }

        case ChooseBattleScreen: {
            setNavBarMode(NavigateBarClass::Bar_Normal);
         setTitleBarText(InGameText::get(IDS_TITLE_ChooseBattle));
         setTitleBarMode(TitleBarClass::Normal);
            d_format = SaveGame::Battle;
            MenuScreen = new ChooseBattleScreenClass(d_centralWindow->getHWND(), &d_fileName);
            ASSERT(MenuScreen != NULL);
        break; }

        case LoadGameScreen: {
            setNavBarMode(NavigateBarClass::Bar_Hidden);
         setTitleBarText(InGameText::get(IDS_TITLE_LoadSavedGame));
         setTitleBarMode(TitleBarClass::Normal);
            MenuScreen = new LoadGameScreenClass(d_centralWindow->getHWND(), &d_fileName);
            ASSERT(MenuScreen != NULL);
        break; }

        case LastGameScreen: {
            setNavBarMode(NavigateBarClass::Bar_Hidden);
         setTitleBarText(InGameText::get(IDS_TITLE_PlayLastGame));
         setTitleBarMode(TitleBarClass::Normal);
            MenuScreen = new LoadGameScreenClass(d_centralWindow->getHWND(), &d_fileName);
            ASSERT(MenuScreen != NULL);
        break; }

        default: {
            FORCEASSERT("ERROR: Invalid mode for FrontEnd");
        break; }

    }

}



void
FrontEndClass::RefreshScreens(void) {

    // window classes must be destroyed by parent, or externally
    // their destructors deal with freeing up class specific information

    if(MenuScreen != NULL)
    {
       delete MenuScreen;
       MenuScreen = 0;
       // DestroyWindow(MenuScreen->getHWND());
    }

    SetupScreens();

}










/*
Main FrontEnd message loop
Handles messages from NavigateBar
The only MenuScreen messages handled are from the MainMenuScreen buttons
All other menu screens process their own messages
*/

LRESULT
FrontEndClass::procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{

    switch(msg)
    {
        HANDLE_MSG(hWnd, WM_COMMAND, onCommand);
        HANDLE_MSG(hWnd, WM_SIZE, onSize);
//        HANDLE_MSG(hWnd, WM_CLOSE, onClose);

        default:
            return defProc(hWnd, msg, wParam, lParam);
    }
}




/*
Handle commands
All button presses to ther navigate bar are posted up to FrontEndClass
Which then controls the navigation between different screens
*/

void
FrontEndClass::onCommand(HWND hwnd, int id, HWND hwndCtl, UINT codeNotify)
{
#if defined(DEBUG) && !defined(NOLOG)
    FrontEndLog.printf("FrontEndClass::WM_COMMAND message received from NavBar button");
#endif

    if(codeNotify == BN_CLICKED)
    {
        if(!doDynamicMenu(id))
        {

            switch(id)
            {
                /*
                * Pull down menu items
                */

         case FE_IDM_NewCampaign: {
               hideScreens();
               SplashScreen splash(getHWND());
                    FrontEndMode = ChooseCampaignScreen;
                    RefreshScreens();
               showScreens();
               splash.destroy();
                    break;
         }

         case FE_IDM_NewBattle: {
               hideScreens();
               SplashScreen splash(getHWND());
                    FrontEndMode = ChooseBattleScreen;
                    RefreshScreens();
               showScreens();
               splash.destroy();
                    break;
         }

                case FE_IDM_ResumeGame:
                    doLastGame();
                    break;

#ifdef MULTIPLAYER

            case FE_IDM_Multiplayer : {

               if(!d_multiplayerDialog) d_multiplayerDialog = new CMultiPlayerDialog(getHWND());
               d_multiplayerDialog->registerUser(this);
               if(g_directPlay) g_directPlay->registerUser(this);

               break;
            }

#endif
                case FE_IDM_Quit:
                    doQuit();
                    break;

                // these four conditions are for the NavigateBar buttons

                case NavigateBarClass::BarButtonId_A: {
               hideScreens();
               SplashScreen splash(getHWND());
                    PrevMenuScreen();
                    RefreshScreens();
               showScreens();
               splash.destroy();
                    break; }

                case NavigateBarClass::BarButtonId_B: {
               hideScreens();
               SplashScreen splash(getHWND());
               NextMenuScreen();
                    RefreshScreens();
               showScreens();
               splash.destroy();
                    break; }

                case NavigateBarClass::BarButtonId_C: {
                    Help();
                    break; }

                case NavigateBarClass::BarButtonId_D: {
                    Quit();
                    break; }


                // these next four conditions are for the MainMenuScreen buttons


                case MenuBarClass::ButtonId_A: {
               hideScreens();
               SplashScreen splash(getHWND());
                    FrontEndMode = ChooseCampaignScreen;
                    RefreshScreens();
               showScreens();
               splash.destroy();
                    break; }

                case MenuBarClass::ButtonId_B: {
               hideScreens();
               SplashScreen splash(getHWND());
                    FrontEndMode = ChooseBattleScreen;
                    RefreshScreens();
               showScreens();
               splash.destroy();
                    break; }

                case MenuBarClass::ButtonId_C: {
                    doLoadGame();
                    // FrontEndMode = LoadGameScreen;
                    // RefreshScreens();
                    break; }

                case MenuBarClass::ButtonId_D: {
                    doLastGame();
                    // FrontEndMode = LastGameScreen;
                    // RefreshScreens();
                    break; }

                // all other input will be handled by the respective screen itself

                default:
                    FORWARD_WM_COMMAND(hwnd, id, hwndCtl, codeNotify, defProc);
                    break;

            } // end of switch on button id
        }      // end of if(!doDynamicMenu)
    } // end of BN_CLICKED codeNotify



}




/*
When the FrontEnd window is resized, it must send the resize message on to both
NavBar and MenuScreen windows
*/


void FrontEndClass::onSize(HWND hwnd, UINT state, int cx, int cy)
{
   // return if we're Minimized
   if(cx <= 0 || cy <= 0) return;

#if defined(DEBUG) && !defined(NOLOG)
        FrontEndLog.printf("FrontEndClass::WM_SIZE message recieved");
#endif

        d_menu.run(PixelPoint(0,0));

      calculateBarSizes();


        if(NavigateBar != NULL) {
         NavigateBar->setSize(&navBarRect);
         InvalidateRect(NavigateBar->GetWindowHandle(), NULL, false);
      }

        if(TitleBar != NULL) {
         TitleBar->setSize(&titleBarRect);
      }


        d_centralWindow->resize(getCentralSize());

        if(MenuScreen != NULL)
        {
                PixelRect r = getCentralSize();
                SendMessage(MenuScreen->getHWND(),WM_SIZE,NULL,NULL);
        }

        // Forward on so wg_main can deal with the Caption Bar
        FORWARD_WM_SIZE(hwnd, state, cx, cy, defProc);
}


#if 0
/*
Promt the player if they really want to quit
*/

void
FrontEndClass::onClose(HWND handle) {

    PostQuitMessage(0);

}
#endif




/*
Terminate Application (probably handled further up in WG_MAIN - but here for the moment)
*/

void
FrontEndClass::onDestroy(HWND handle) {



}


/*
 *  Extract recent saved games from registry and
 *  and add to the File menu
 */

const UWORD MenuIDFirst = 1000;

void FrontEndClass::getOldGames()
{
  char buf[200];

  UWORD menuID = MenuIDFirst;

  RegistryFile registry(FrontEndUtilsClass::s_savedGamesRegistryName);

  while(registry.getFileName(buf))
  {
          GameFile* file = new GameFile(buf, menuID++);
          ASSERT(file != 0);

          d_savedGames.append(file);
  }

  registry.reset(FrontEndUtilsClass::s_scenarioRegistryName);
  while(registry.getFileName(buf))
  {
          GameFile* file = new GameFile(buf, menuID++);
          ASSERT(file != 0);

          d_scenarioFiles.append(file);
  }

}

/*
 * Set up the menu bar
 */



void FrontEndClass::setupMenu()
{
#if defined(DEBUG) && !defined(NOLOG)
        FrontEndLog.printf("FrontEndWindow::setupMenu()");
#endif

  /*
   *  Get Menu Handle
   */

  // HMENU hMenu = GetMenu(parent_window_handle);
  // ASSERT(hMenu != 0);

  MenuWindow* hMenu = d_menu.getSubMenu(0,0);
  ASSERT(hMenu != 0);

  /*
   *  Get handle to File menu
   */

  MenuWindow* hFileMenu = d_menu.getSubMenu(hMenu, 0);
  ASSERT(hFileMenu != 0);

  /*
   *  Iterate through list and add saved games to menu
   */

  SListIter<GameFile> iterSaved(&d_savedGames);
  int i = d_menu.getMenuItemCount(hFileMenu);

  while(++iterSaved)
  {
         GameFile* file = iterSaved.current();

         /*
          *  Parse file name, get rid of path and extension, if any
          */

         SimpleString parsed;
         FrontEndUtilsClass::parseFileName(parsed, file->fileName.toStr());

         MenuItemInfo mi;

        mi.d_id = file->id;
        mi.d_state = MenuItemInfo::CMS_Enabled;
        mi.d_subMenu = 0;
        mi.d_text = parsed.toStr();
        SetRect(&mi.d_rect, 0,0,0,0);

         BOOL result = d_menu.insertMenuItem(hFileMenu, i++, TRUE, &mi);
         ASSERT(result != False);
  }

  /*
   *  Create popup menu for Scenario
   * ONLY if there is more than 1 scenario...
   */

        if(d_scenarioFiles.entries() > 1)
        {

                // HMENU hPopup = CreatePopupMenu();
                MenuWindow* hPopup = d_menu.createPopupMenu();
                ASSERT(hPopup != 0);

                MenuItemInfo mi;

                mi.d_id = 0;
                mi.d_state = MenuItemInfo::CMS_Enabled;
                mi.d_subMenu = hPopup;
                mi.d_text = InGameText::get(IDS_Scenario);;                        // TODO: Take from Resource file
                SetRect(&mi.d_rect, 0,0,0,0);
                BOOL result = d_menu.insertMenuItem(hMenu, 1, TRUE, &mi);

                /*
                 *  Iterate through GameFile list and add scenarios to menu
                 */

                SListIter<GameFile> iter(&d_scenarioFiles);

                i = 0;
                while(++iter)
                {
                        GameFile* file = iter.current();

                        /*
                         *  Parse file name, get rid of path and extension, if any
                         */

                        SimpleString parsed;
                        FrontEndUtilsClass::parseFileName(parsed, file->fileName.toStr());

                        MenuItemInfo mi;
                        mi.d_id = file->id;
                        mi.d_state = MenuItemInfo::CMS_Enabled;
                        mi.d_subMenu = 0;
                        mi.d_text = parsed.toStr();
                        SetRect(&mi.d_rect, 0,0,0,0);

                        BOOL result = d_menu.insertMenuItem(hPopup, i++, TRUE, &mi);
                        ASSERT(result);
                }
        }
}

bool FrontEndClass::doDynamicMenu(UWORD id)
{
#if defined(DEBUG) && !defined(NOLOG)
        FrontEndLog.printf("FrontEndWindow::doDynamicMenu()");
#endif

  /*
   *  See if a scenario file has been selected
   */

  SListIter<GameFile> iter(&d_scenarioFiles);

  while(++iter)
  {
         GameFile* f = iter.current();

         if(f->id == id)
         {
                if(onNewScenario(f->fileName.toStr()))
                {
                        FrontEndMode = MainMenuScreen;
                        RefreshScreens();
                }
                return true;
         }
  }

  /*
   *  See if a recent saved game has been selected
   */

  SListIter<GameFile> iterSaved(&d_savedGames);

  while(++iterSaved)
  {
         GameFile* f = iterSaved.current();

         if(f->id == id)
         {
                RegistryFile registry(FrontEndUtilsClass::s_savedGamesRegistryName);
                registry.setFileName(f->fileName.toStr());

                doStartGame(f->fileName.toStr());
                return true;
         }
  }

  return false;
}

bool FrontEndClass::onNewScenario(const char* fileName)
{
#if defined(DEBUG) && !defined(NOLOG)
        FrontEndLog.printf("FrontEndWindow::onNewScenario()");
#endif

  ASSERT(fileName != 0);
  ASSERT(d_scenarioFileName.toStr() != 0);

  if(lstrcmpi(d_scenarioFileName.toStr(), fileName) != 0)       // || d_mode != StartScreen)
  {
         d_scenarioFileName = fileName;
         ASSERT(d_scenarioFileName.toStr() != 0);

         createScenarioData();
         return true;
  }
  return false;

}

void FrontEndClass::createScenarioData()
{
    ASSERT(d_scenarioFileName.toStr() != 0);

    Scenario::create(d_scenarioFileName.toStr());
    ASSERT(scenario != 0);

    // setup palette
    // readBMP automatically sets gApp.hPalette
    const char* fileName = scenario->getPaletteFileName();
    ASSERT(fileName != 0);

        try {
        BMP::getPalette(fileName);
        }
        catch(const BMP::BMPError& e) {
        FORCEASSERT("Palette not found!"); }
}

void FrontEndClass::loadScenario(const char* scenarioFile)
{
        if(scenarioFile == 0)
        {
                char buf[200];
                RegistryFile registry(FrontEndUtilsClass::s_scenarioRegistryName);

                if(!registry.getFileName(buf))
                {
                        // bodge in case file isn't in registry

                        /*
                         * Scan directory for SWG files... and and then add them to registry
                         */

                        FindFileIterator fileIter("*.swg");

                        if(fileIter())
                        {
                                d_scenarioFileName = fileIter.name();

                                for(; fileIter(); ++fileIter)
                                {
                                        BOOL success = registry.setFileName(fileIter.name());
                                        ASSERT(success);
                                }
                        }
                        else
                        {
                                throw GeneralError("No scenario has been installed!");
                        }


                }
                else
                        d_scenarioFileName = buf;
        }
        else
        {
                d_scenarioFileName = scenarioFile;
        }
        ASSERT(d_scenarioFileName.toStr() != 0);

        createScenarioData();
}


/*
 * Functions added by Steven
 */

void FrontEndClass::doStartGame(const char* fileName)
{
#if defined(DEBUG) && !defined(NOLOG)
        FrontEndLog.printf("FrontEndWindow::doStartGame()");
#endif

        ASSERT(fileName != 0);

        /*
         *  Use copyString because postLoadGame deletes string
         */


                        d_format = SaveGame::SaveGame;


        if(fileName)
          WargameMessage::postLoadGame(fileName);

}

void FrontEndClass::doNewGame(const char * current, SaveGame::FileFormat format)
{
#if defined(DEBUG) && !defined(NOLOG)
        FrontEndLog.printf("FrontEndWindow::doStartCampaignGame()");
#endif
/*
      const char* current = d_fileName.toStr();
        ASSERT(current != 0);
*/
        if(current)
         WargameMessage::postNewGame(current, format);

}


void FrontEndClass::doLoadGame()
{
#if defined(DEBUG) && !defined(NOLOG)
        FrontEndLog.printf("FrontEndWindow::doLoadGame()");
#endif

        WargameMessage::postLoadGame();
}

void FrontEndClass::doLastGame()
{
#if defined(DEBUG) && !defined(NOLOG)
        FrontEndLog.printf("FrontEndWindow::doLastGame()");
#endif

  if(d_savedGames.entries() > 0)
  {
         GameFile* item = d_savedGames.first();
         ASSERT(item != 0);

         doStartGame(item->fileName.toStr());
  }
  else
  {
         MessageBox(window_handle,
            InGameText::get(IDS_NoGamePlayed),
            InGameText::get(IDS_NoGame),
            MB_OK | MB_ICONEXCLAMATION);
  }
}


void FrontEndClass::doQuit()
{
#if defined(DEBUG) && !defined(NOLOG)
        FrontEndLog.printf("FrontEndWindow::doQuit()");
#endif

  PostQuitMessage(0);
}


/*
   Inherited from CDirectPlayUser & used to tell when remote machine has joined
*/

void
FrontEndClass::onOpponentJoin(void) {

   if(d_multiplayerDialog) {
      delete d_multiplayerDialog;
      d_multiplayerDialog = 0;
   }

   if(d_owner) d_owner->multiplayerConnectionComplete(true);
}




/*

  Inherited from CMultiPlayerDialog

*/

void
FrontEndClass::connectionHosting(void) {

   /*
   * Put up 'please wait for remote to join' message
   */

}

void
FrontEndClass::connectionJoined(void) {

   if(d_multiplayerDialog) {
      delete d_multiplayerDialog;
      d_multiplayerDialog = 0;
   }

   if(d_owner) d_owner->multiplayerConnectionComplete(false);
}

void
FrontEndClass::connectionCancelled(void) {

   if(d_multiplayerDialog) {
      delete d_multiplayerDialog;
      d_multiplayerDialog = 0;
   }

}






/*==================================================================
 * Public Functions to create and destroy FrontEnd
 */


FrontEnd::FrontEnd(GameOwner* owner, const char* scenarioFile) :
    GameBase()
{
        ASSERT(owner != NULL);

        // first of all, show the parent wargamer application window
        ShowWindow(owner->hwnd(), SW_SHOW);
        UpdateWindow(owner->hwnd());

        d_frontEnd = new FrontEndClass(owner, scenarioFile);
}

FrontEnd::~FrontEnd()
{
        delete d_frontEnd;
}

// call GameBase 'doStartGame()'
void
FrontEnd::doNewGame(const char * current, SaveGame::FileFormat format) {

   if(d_frontEnd)
      d_frontEnd->doNewGame(current, format);
}


void
FrontEnd::syncSlave(SysTick::Value ticks) {
   // do nothing
   // this doesn't happen here

}

void
FrontEnd::timeSyncAcknowledged(SysTick::Value ticks) {
   // do nothing
   // this doesn't happen here

}


void
FrontEnd::slaveReady(void) {
   // do nothing
   // this doesn't happen here

}


void FrontEnd::processBattleOrder(MP_MSG_BATTLEORDER * msg)
{
   FORCEASSERT("Multiplayer Battle Message sent to FrontEnd!!!!");
}

void FrontEnd::processCampaignOrder(MP_MSG_CAMPAIGNORDER *msg)
{
   FORCEASSERT("Multiplayer Campaign Message sent to FrontEnd!!!!");
}

/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.2  2001/06/18 08:25:45  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
