/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1995, Steven Green, All Rights Reserved
 *----------------------------------------------------------------------
 *
 * Calculate Influence of Units on a town
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "aic_threat.hpp"
#include "aic_unit.hpp"
#include "campdint.hpp"
#include "armies.hpp"
#include "aic_sdat.hpp"
#include "route.hpp"
#include "town.hpp"
#include "scenario.hpp"

UnitThreat::Influence UnitThreat::getInfluence(const AIC_UnitList* aiUnits, AIC_StrategyData* sData, ITown itown, Side side)
{
   Influence totalInfluence = 0;

   const CampaignData* campData = sData->campData();
   // const Armies* army = sData->armies();

   // Get Route Info from current location

   // Assume it will take 7 days to get past an enemy controlled town
   // This will force route finder to try to avoid enemy towns, but not
   // make them completely out of bounds.

   const TimeTick enemyDelay = DaysToTicks(7);

   // Flags to use for route finding
   //    - UseCosts : RouteCost will be time in TimeTick
   //    - NoEnemy : We might want to use this instead of the enemyDelay
   //    - SameSide : We may want to consider this to avoid neutral towns

   const CampaignRoute::RouteFlag flags = CampaignRoute::UseCosts;

   CampaignRoute route(campData);
   route.setup(flags, side, enemyDelay);
   route.fillRoute(itown);

//    ConstUnitIter unitIter(army, army->getFirstUnit(side));
//    while(unitIter.sister())
//    {
//       // AIC_ConstUnitRef aiUnit = (*it);
//       ConstICommandPosition cp = unitIter.current();
//       // ConstICommandPosition cp = aiUnit.cp();
//       SPCount spCount = army->getUnitSPCount(cp, true);
//       // SPCount spCount = aiUnit.spCount();


   for (AIC_UnitList::const_iterator it = aiUnits->begin();
         it != aiUnits->end();
         ++it)
   {
      AIC_ConstUnitRef aiUnit((*it).first, &(*it).second);

      ConstICommandPosition cp = aiUnit.cp();
      SPCount spCount = aiUnit.spCount();


      // Calculate how long it will take unit to get here

      ITown iUnitTown = cp->getCloseTown();
      const Town& unitTown = campData->getTown(iUnitTown);

      // If unit is under siege then skip unit unless target of this objective

      if((iUnitTown != itown) && cp->isGarrison() && unitTown.isSieged())
      {
#ifdef DEBUG
         sData->logWin("%s is under siege",
            static_cast<const char*>(cp->getName()));
#endif
      }
      else if(route.isConnected(iUnitTown))
      {
         TimeTick arrivalTime = route.cost(iUnitTown);

         const float HalfTime = HoursToTicks(36);    // At 1.5 days it has 0.5 priority
         const float H2 = HalfTime * HalfTime;
         float d = arrivalTime;
         float distInfluence = H2 / (d * d + H2);

         float townSPInfluence = distInfluence * float(spCount);

         totalInfluence += townSPInfluence;
      }
   }

#ifdef DEBUG
   sData->logWin("Total Influence for %s=%f",
      static_cast<const char*>(scenario->getSideName(side)),
      static_cast<float>(totalInfluence));
#endif

   return totalInfluence;
}


/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.2  2001/06/18 08:25:44  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
