/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 * Campaign AI Codelet Base class
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "codelet.hpp"

#ifdef DEBUG
#include "logwin.hpp"
#endif


void AIC_Codelet::priority(unsigned int p)
{
   addPriority(p - d_priority);
}

unsigned int AIC_Codelet::addPriority(int p)
{
   AIC_Codelet* code = this;

   while(code)
   {
      ASSERT(code->d_priority <= INT_MAX);

      if(static_cast<int>(code->d_priority) < -p)
      {
         ASSERT(code == this);
         p = -static_cast<int>(code->d_priority);
      }

      code->d_priority += p;

      ASSERT(code->d_priority <= INT_MAX);
      // ASSERT(code->d_priority >= 0);

      code = code->d_parent;
   }

   return d_priority;
}

unsigned int AIC_Codelet::subPriority(unsigned int  p)
{
   ASSERT(p <= INT_MAX);
   // ASSERT(p >= 0);

   AIC_Codelet* code = this;

   while(code)
   {
      ASSERT(code->d_priority <= INT_MAX);

      if(code->d_priority < p)
      {
         ASSERT(code == this);
         p = code->d_priority;
      }

      code->d_priority -= p;

      ASSERT(code->d_priority <= INT_MAX);
      // ASSERT(code->d_priority >= 0);

      code = code->d_parent;
   }
   return d_priority;
}

#ifdef DEBUG
void AIC_Codelet::log(LogWindow* log) const
{
   log->printf("%22s: p=%d", static_cast<const char*>(name().c_str()), static_cast<int>(d_priority));
}

#endif

/*
 *----------------------------------------------------------------------
 * $Log$
 *----------------------------------------------------------------------
 */
