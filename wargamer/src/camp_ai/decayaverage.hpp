/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef DecayAverage_HPP
#define DecayAverage_HPP

/*
 * Decaying Average
 */

#include "myassert.hpp"

template<class T>
class DecayAverage
{
   public:
      DecayAverage(unsigned int samples) :
         d_min(0),
         d_max(0),
         d_samples(samples),
         d_initialized(false)
      {
         ASSERT(samples > 0);
      }

      T updateAndNormalize(T value)
      {
         if(!d_initialized)
         {
            d_min = value;
            d_max = value;
            d_initialized = true;
         }
         else
         {
            if(value <= d_min)
               d_min = value;
            else
            {
               d_min = (value + d_min * (d_samples - 1)) / d_samples;
            }

            if(value >= d_max)
               d_max = value;
            else
            {
               d_max = (value + d_max * (d_samples - 1)) / d_samples;
            }
         }

         ASSERT(d_max >= d_min);
         ASSERT(value >= d_min);
         ASSERT(value <= d_max);

         if(d_min != d_max)
         {
            return (value - d_min) / (d_max - d_min);
         }
         else
            return 1.0;
      }

   private:
      T d_min;
      T d_max;
      int d_samples;
      bool d_initialized;
};



#endif   // DecayAverage_HPP
