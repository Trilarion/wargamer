/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef AIC_ORG_HPP
#define AIC_ORG_HPP

#ifndef __cplusplus
#error aic_org.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *	Campaign AI: Unit Organization and allocator
 *
 *----------------------------------------------------------------------
 */

#include "codelet.hpp"
#include "cpidef.hpp"

class AIC_StrategyData;
class AIC_UnitList;
class RepoSP;

class AIC_Reorganize : public AIC_Codelet
{
   public:
      AIC_Reorganize(AIC_StrategyData* sData, AIC_UnitList* units);
      ~AIC_Reorganize();

      virtual void init();
		virtual void run(unsigned int rand);
      virtual void timeUpdate(TimeTick t);
#ifdef DEBUG
      virtual String name() const;
#endif
   private:
      /*
       * Private Functions
       */

      void assignNewSPs();
      void adjustOB();
      ConstICommandPosition findSuitableOrg(const RepoSP* repo);
      void orderSPtoAttach(ConstParamCP cp, const RepoSP* rsp);
      void reorgDivision(ConstParamCP cp);
      void reorgCorps(ConstParamCP cp);
      void reorgArmy(ConstParamCP cp);
      void splitDivision(ConstParamCP cp);

      /*
       * Data
       */

      AIC_StrategyData* d_sData;
      AIC_UnitList* d_units;
};


#endif /* AIC_ORG_HPP */

