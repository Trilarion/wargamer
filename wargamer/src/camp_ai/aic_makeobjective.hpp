/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef AIC_makeObjective_HPP
#define AIC_makeObjective_HPP

#ifndef __cplusplus
#error AIC_makeObjective.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *	Create Objectives
 *
 *----------------------------------------------------------------------
 */

#include "codelet.hpp"
#include "aic_UnitInfluence.hpp"

class AIC_StrategyData;
class AIC_TownList;
class AIC_ObjectiveList;
class AIC_Objective;
class AIC_UnitList;
#ifdef DEBUG
class ObjectiveDisplay;
#endif

class AIC_ObjectiveCreator : public AIC_Codelet
{
   public:
      AIC_ObjectiveCreator(AIC_StrategyData* sData, AIC_TownList* towns, AIC_ObjectiveList* objectives, AIC_UnitList* units);
      ~AIC_ObjectiveCreator();

      virtual void init();
		virtual void run(unsigned int rand);
      virtual void timeUpdate(TimeTick t);
#ifdef DEBUG
      virtual String name() const;
#endif

   private:
      void removeObjective(AIC_Objective* objective);
      void removeUnit(ConstParamCP cp);
      void tryTown(const AIC_TownInfo& tInfo);
      bool buildObjective(const AIC_TownInfo& tInfo);


   private:
      TownInfluence d_townInfluence;

      AIC_StrategyData* d_sData;
      AIC_TownList* d_towns;
      AIC_ObjectiveList* d_objectives;
      AIC_UnitList* d_units;
#ifdef DEBUG
      ObjectiveDisplay* d_objectiveDisplay;
#endif
};



#endif /* AIC_makeObjective_HPP */
