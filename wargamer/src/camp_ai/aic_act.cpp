/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
#error "Obsolete"
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 * Campaign AI: Activities Base class
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "aic_act.hpp"
#include "armies.hpp"
#include "compos.hpp"
#include "campdint.hpp"
#include "random.hpp"
#include <stdio.h>
#include "ailog.hpp"

#ifdef DEBUG
#include "logwin.hpp"
#include <algo.h>
#endif

/*
 * Activity List
 */

AIC_ActivityList::AIC_ActivityList() :
   d_top(0)
{
}

AIC_ActivityList::~AIC_ActivityList()
{
   ASSERT(d_top == 0);  // Utility class should remove events
   // kill(d_top);
}

AIC_Activity* AIC_ActivityList::create(AIC_Activity* parent)
{
   AIC_Activity* activity = new AIC_Activity(parent);
   ASSERT(activity != 0);

   if(parent)
   {
      parent->d_children.append(activity);
      // parent->needUpdate();
   }
   else
   {
      ASSERT(d_top == 0);
      d_top = activity;
   }

   activity->d_parent = parent;

#ifdef DEBUG
   if(d_top)
      d_top->checkPriorities();
#endif

   return activity;
}


/*
 * Kill Activity and its children
 */

void AIC_ActivityList::kill(AIC_Activity* activity)
{
   ASSERT(activity != 0);
   ASSERT(activity->d_children.entries() == 0);
   ASSERT(activity->d_units.entries() == 0);
   ASSERT(activity->d_towns.entries() == 0);

   AIC_Activity* parent = activity->d_parent;
   if(parent)
   {
      parent->d_children.unlink(activity);
      activity->d_parent = 0;
      // parent->needUpdate();
      parent->addTotalPriority(-activity->d_totalPriority);
   }

   if(activity == d_top)
      d_top = 0;

   delete activity;

#ifdef DEBUG
   if(d_top)
      d_top->checkPriorities();
#endif
}


/*
 * Pick a random activity
 */

AIC_Activity* AIC_ActivityList::pick(AIC_Activity::ThingsToDo* what, RandomNumber& randGen)
{
   ASSERT(what != 0);


   AIC_Activity* activity = d_top;

#ifdef DEBUG
   activity->checkPriorities();
#endif

   int totalPriority = activity->totalPriority();
   int r = randGen(totalPriority);

   while(activity->childCount() && (r >= activity->priority()) )
   {
      r -= activity->priority();

      DiList<AIC_Activity>* children = activity->children();
      activity = children->first();

      while(r >= activity->totalPriority())
      {
         r -= activity->totalPriority();
         activity = children->next(activity);
         ASSERT(activity != 0);
      }
   }

#if 0
   if(activity->childCount() != 0)
   {
      for(;;)
      {
         ASSERT(r < activity->totalPriority());

         if(activity->childCount() != 0)
         {
            if(r >= activity->priority())
            {
               r -= activity->priority();

               DiList<AIC_Activity>* children = activity->children();
               AIC_Activity* child = children->first();

               while(child)
               {
                  if(r < child->totalPriority())
                  {
                     activity = child;
                     break;
                  }
                  else
                     r -= child->totalPriority();

                  child = children->next(child);
               }

               ASSERT(child != 0);
            }
            else
               break;
         }
         else
            break;
      }

      ASSERT(r < activity->priority());
   }
#endif

   ASSERT(activity != 0);
   ASSERT(r < activity->priority());

   // Set what

   if(what != 0)
   {
      AIC_Activity::ThingsToDo i = AIC_Activity::TDD_First;

      while(r >= activity->priority(i))
      {
         r -= activity->priority(i);
         INCREMENT(i);
         ASSERT(i < AIC_Activity::TDD_HowMany);
      }

      *what = i;

#if 0
      for(AIC_Activity::ThingsToDo i = AIC_Activity::TDD_First;
         i < AIC_Activity::TDD_HowMany;
         INCREMENT(i))
      {
         if(r < activity->priority(i))
         {
            *what = i;
            break;
         }

         r -= activity->priority(i);
      }
#endif

      ASSERT(i < AIC_Activity::TDD_HowMany);
   }

   
   return activity;
}

/*
 * Write ActivityList to logfile
 */

#ifdef DEBUG

void AIC_ActivityList::log(LogWindow* logWin)
{
   logWin->printf("-----------------------");
   logWin->printf("Current activities:");

   AIC_ActivityIter iter(top(), True);    // Top down iterator
   while(++iter)
   {
      AIC_Activity* activity = iter.current();

      char buffer[80];

      logWin->printf("%-15s: %3d towns, imp=%d, spNeeded=%3d, spAlloc=%3d, p=%s, totalp=%5d, age=%d, happiness=%d",
         (const char*) activity->description(),
         (int) activity->towns()->entries(),
         (int) activity->importance(),
         (int) activity->spNeeded(),
         (int) activity->spAllocated(),
         (const char*) activity->priorityStr(buffer),
         (int) activity->totalPriority(),
         (int) activity->age(),
         (int) activity->happiness());
   }
   logWin->printf("-----------------------");
}

#endif


#ifdef DEBUG
static const char* s_thingsToDoStr[AIC_Activity::TDD_HowMany] = {
   "Update",
   "Split",
   "Expand",
   "AssignSP to",
   // "TakeSP from",
   "SendOrder to",
   "Organize Towns",
   "Fizzle"
};


static const char* AIC_Activity::thingToDoStr(ThingsToDo which)
{
   ASSERT((which >= TDD_First) && (which < AIC_Activity::TDD_HowMany));

   return s_thingsToDoStr[which];
}


const char* AIC_Activity::priorityStr(char* buffer) const
{
   for(int i = 0; i < TDD_HowMany; i++)
   {
      if(i == 0)
         sprintf(buffer, "%d", (int) d_priorities[i]);
      else
         sprintf(buffer + strlen(buffer), "+%d", (int) d_priorities[i]);
      ASSERT(strlen(buffer) < 80);
   }

   sprintf(buffer + strlen(buffer), "=%d", d_priority);
   return buffer;
}


#endif

/*
 * Common Activities
 */

#ifdef DEBUG
static AIC_Activity::s_id = 0;
#endif

AIC_Activity::AIC_Activity(AIC_Activity* parent) :
   d_parent(parent),
   d_children(),
   d_active(True),
   d_units(),
   d_towns(),
   d_priority(0),
   d_totalPriority(0),
   d_importance(0),
   d_spNeeded(0),
   d_spAllocated(0),
   d_takeNeeded(0),
   d_takeImportance(0),
   d_age(0),
   d_happiness(0)
   // d_needUpdate(True)
{
#ifdef DEBUG
   if(parent == 0)
      d_id = "A";
   else
   {
      char buffer[100];
      sprintf(buffer, "%s.%d", (const char*) parent->description(), (int) ++s_id);
      d_id = buffer;
   }
#endif

   for(int i = 0; i < TDD_HowMany; i++)
   {
      d_priorities[i] = 0;
   }

   // Activity has 0 priority and importance, and will not
   // have any chance of being picked until it has at least one
   // town added to it.
}

AIC_Activity::~AIC_Activity()
{
   ASSERT(d_units.entries() == 0);
   ASSERT(d_towns.entries() == 0);
   ASSERT(d_parent == 0);
}

/*
 * remove Unit from Activity List
 * Note that this does not update the UnitAI's data, only the activities.
 */


void AIC_Activity::addUnit(ConstICommandPosition cpi)
{
#ifdef DEBUG      // Check not already in list
   IFDEBUG(Boolean found = False);

   for(int i = 0; i < d_units.entries(); i++)
   {
      if(d_units[i] == cpi)
      {
         d_units.remove(i);
         IFDEBUG(found = True);
         break;
      }
   }

   ASSERT(!found);
#endif

   d_units.add(cpi);

   // needUpdate();
}

void AIC_Activity::removeUnit(ConstICommandPosition cpi)
{
   IFDEBUG(Boolean found = False);

   for(int i = 0; i < d_units.entries(); i++)
   {
      if(d_units[i] == cpi)
      {
         d_units.remove(i);
         IFDEBUG(found = True);
         break;
      }
   }

   ASSERT(found);

   // needUpdate();
}

void AIC_Activity::addTown(ITown itown)
{
#ifdef DEBUG      // Check not already in list
   IFDEBUG(Boolean found = False);

   for(int i = 0; i < d_towns.entries(); i++)
   {
      if(d_towns[i] == itown)
      {
         d_towns.remove(i);
         IFDEBUG(found = True);
         break;
      }
   }

   ASSERT(!found);
#endif

   d_towns.add(itown);

   // needUpdate();
}

void AIC_Activity::removeTown(ITown itown)
{
   IFDEBUG(Boolean found = False);

   for(int i = 0; i < d_towns.entries(); i++)
   {
      if(d_towns[i] == itown)
      {
         d_towns.remove(i);
         IFDEBUG(found = True);
         break;
      }
   }

   ASSERT(found);

   // needUpdate();
}

/*
 * Update importance of activity
 * Note that importance is the total importance including children
 * importance may be -ve to reduce the total.
 */

void AIC_Activity::addImportance(int importance)
{
   if(importance != 0)
   {
      AIC_Activity* activity = this;
      while(activity)
      {
         ASSERT(-importance <= activity->d_importance);     // can't reduce below zero.

         activity->d_importance += importance;

#ifdef DEBUG
         aiLog.printf("%s's importance %screased by %d to %d",
            (const char*) activity->description(),
            (const char*) (importance < 0) ? "de" : "in",
            (int) abs(importance),
            (int) activity->d_importance);
#endif

         activity = activity->d_parent;
      }
   }
}


void AIC_Activity::addSP(SPCount spCount) 
{
   if(spCount > 0)
   {
      AIC_Activity* activity = this;
      while(activity)
      {
         activity->d_spAllocated += spCount;
#ifdef DEBUG
         aiLog.printf("Activity %s's SPAllocated increased by %d to %d",
            (const char*) activity->description(),
            (int) spCount,
            (int) activity->d_spAllocated);
#endif
         activity = activity->parent();
      }
   }
}

void AIC_Activity::delSP(SPCount spCount)
{
   if(spCount > 0)
   {
      AIC_Activity* activity = this;
      while(activity)
      {
         ASSERT(activity->d_spAllocated >= spCount);

         if(activity->d_spAllocated >= spCount)
            activity->d_spAllocated -= spCount;
         else
            activity->d_spAllocated = 0;

#ifdef DEBUG
         aiLog.printf("Activity %s's SPAllocated decreased by %d to %d",
            (const char*) activity->description(),
            (int) spCount,
            (int) activity->d_spAllocated);
#endif
         activity = activity->parent();
      }
   }
}

/*
 * Adjust SPNeeded for activity (and it's parents)
 */

void AIC_Activity::addSPNeeded(SPCount add)
{
   if(add > 0)
   {
      AIC_Activity* activity = this;
      while(activity)
      {
         activity->d_spNeeded += add;
#ifdef DEBUG
         aiLog.printf("Activity %s's SPNeeded increased by %d to %d",
            (const char*) activity->description(),
            (int) add,
            (int) activity->d_spNeeded);
#endif
         activity = activity->parent();
      }
   }
}

void AIC_Activity::subSPNeeded(SPCount sub)
{
   if(sub > 0)
   {
      AIC_Activity* activity = this;
      while(activity)
      {
         ASSERT(activity->d_spNeeded >= sub);

         if(activity->d_spNeeded >= sub)
            activity->d_spNeeded -= sub;
         else
            activity->d_spNeeded = 0;

#ifdef DEBUG
         aiLog.printf("Activity %s's SPNeeded decreased by %d to %d",
            (const char*) activity->description(),
            (int) sub,
            (int) activity->d_spNeeded);
#endif
         activity = activity->parent();
      }
   }
}

/*
 * Update hungriness for new units
 */

void AIC_Activity::addTakeNeeded(SPCount needed, int importance)
{
   d_takeImportance = MulDiv(d_takeImportance, d_takeNeeded, d_takeNeeded + needed) +
                      MulDiv(importance, needed, d_takeNeeded + needed);
   d_takeNeeded += needed;

#ifdef DEBUG
   aiLog.printf("Activity %s, takeNeeded updated by %d,%d to %d,%d",
      (const char*) description(),
      (int) needed, (int) importance,
      (int) d_takeNeeded, (int) d_takeImportance);
#endif
}

void AIC_Activity::addHappiness(int h)
{
   ASSERT((h < 0) || (d_happiness < (INT_MAX - h)));

   if( (h > 0) && (d_happiness >= (INT_MAX - h)))
      d_happiness = INT_MAX;
   else
   {
      d_happiness += h;
      if(d_happiness < 0)
         d_happiness = 0;
   }

   ASSERT(d_happiness >= 0);
}

/*
 * Note that something has happened to an activity
 * Reset the age, and reduce the Fizzle priority
 */

void AIC_Activity::resetAge()
{
   d_age = 0;
   int p = priority(TDD_Fizzle) / 2;
   priority(TDD_Fizzle, p);
}


/*
 * Return True if p is an ancestor (parent) of child
 * Also Returns True if p and child are the same
 *
 * Returns False if p is not an ancestor and they are different
 */

static Boolean AIC_Activity::isChildOf(const AIC_Activity* p, const AIC_Activity* child)
{
   while(child)
   {
      if(child == p)
         return True;
      child = child->parent();
   }

   return False;
}

/*
 * Priority Functions
 */

#if 0
void AIC_Activity::priority(int p)
{
   addPriority(p - d_priority);
}
#endif

void AIC_Activity::addTotalPriority(int p)
{
   AIC_Activity* act = this;
   do
   {
      act->d_totalPriority += p;
      act = act->d_parent;
   } while(act != 0);
}

void AIC_Activity::addPriority(int p)
{
   d_priority += p;
   addTotalPriority(p);
}

#ifdef DEBUG

void AIC_Activity::checkPriorities() const
{
   ASSERT(d_priority >= 0);
   ASSERT(d_totalPriority >= 0);

   struct AssertPositive
   {
      void operator()(int n) const
      {
         ASSERT(n >= 0);
      }
   };

   for_each(&d_priorities[TDD_First], &d_priorities[TDD_HowMany], AssertPositive());

   // Check that the task priorities match up

   int total = accumulate(&d_priorities[TDD_First], &d_priorities[TDD_HowMany], 0);
   ASSERT(total == d_priority);

   // Check Children

   DiListIter<AIC_Activity> iter(&d_children);
   while(++iter)
   {
      const AIC_Activity* child = iter.current();

      child->checkPriorities();

      total += child->d_totalPriority;

      // Do some more checks while we're here

      ASSERT(d_importance >= child->d_importance);
      ASSERT(d_spNeeded >= child->d_spNeeded);
      ASSERT(d_spAllocated >= child->d_spAllocated);
   }

   ASSERT(total == d_totalPriority);


#if 0
   int total = 0;

   for(int i = 0; i < TDD_HowMany; i++)
   {
      total += d_priorities[i];
   }
#endif
}

#endif

#if 0
/*
 * Mark activity and all its parents as needing to have its totals updated
 */

void AIC_Activity::needUpdate()
{
   AIC_Activity* activity = this;
   while(activity && !activity->d_needUpdate)
   {
      activity->d_needUpdate = True;
      activity = activity->d_parent;
   }
}


/*
 * Get total priority for an activity
 * This is recursive!
 */

int AIC_Activity::totalPriority() const
{
   AIC_Activity* ncActivity = (AIC_Activity*) this;

   if(d_needUpdate)
   {
#ifdef DEBUG
      int p = d_priority;
      ncActivity->makePriority();
      ASSERT(p == d_priority);
#endif

      int total = d_priority;

      DiListIter<AIC_Activity> iter(&d_children);
      while(++iter)
      {
         const AIC_Activity* child = iter.current();
         total += child->totalPriority();

         // Do some more checks while we're here

         ASSERT(d_importance >= child->d_importance);
         ASSERT(d_spNeeded >= child->d_spNeeded);
         ASSERT(d_spAllocated >= child->d_spAllocated);
      }

      ncActivity->d_totalPriority = total;

      ncActivity->d_needUpdate = False;
   }

   return d_totalPriority;
}

/*
 * Sum up the priorities[] to set priority
 * updateNeeded is called, so that the totalPrioritie's of
 * it and it's parent can be updated.
 */

void AIC_Activity::makePriority()
{
   IFDEBUG(int oldP = d_priority);

   int total = 0;

   for(int i = 0; i < TDD_HowMany; i++)
   {
      total += d_priorities[i];
   }

   d_priority = total;

   needUpdate();

#ifdef DEBUG_ALL
   if(d_priority != oldP)
   {
      char buffer[80];
      aiLog.printf("%s's priority changed to %s",
            (const char*) description(),
            priorityStr(buffer));
   }
#endif
}
#endif // 0

void AIC_Activity::priority(ThingsToDo which, int p)
{
   ASSERT((which >= TDD_First) && (which < AIC_Activity::TDD_HowMany));
   ASSERT(p >= 0);

   int diff = p - d_priorities[which];

   d_priorities[which] = p;

   addPriority(diff);

   checkPriorities();
}

/*
 * reduce Activity's priority due to inaction
 */

void AIC_Activity::lowerPriority(ThingsToDo which)
{
   ASSERT((which >= TDD_First) && (which < AIC_Activity::TDD_HowMany));

   int rp = d_priorities[which];

   IFDEBUG(int oldPriority = rp);

   rp = (rp + 1) / 2;

   priority(which, rp);

#ifdef DEBUG
   if(oldPriority != rp)
      aiLog.printf("%s %s's priority lowered to %d",
            thingToDoStr(which),
            (const char*) description(),
            (int) rp);
#endif
}

/*
 * Increment activity's priority
 */

void AIC_Activity::addPriority(ThingsToDo which, int add)
{
   ASSERT((which >= TDD_First) && (which < AIC_Activity::TDD_HowMany));
   // ASSERT(add >= 0);
   // ASSERT(add >= d_priorities[which]);

   d_priorities[which] += add;
   ASSERT(d_priorities[which] >= 0);
   addPriority(add);
   checkPriorities();

#ifdef DEBUG
   char buffer[80];
   aiLog.printf("%s %s's priority increased by %d to %s",
            (const char*) thingToDoStr(which),
            (const char*) description(),
            (int) add,
            (const char*) priorityStr(buffer));
#endif
}

/*=======================================================
 * Iterator
 */

AIC_ActivityIter::AIC_ActivityIter(AIC_Activity* top, Boolean topDown) :
   d_top(top),
   d_current(0),
   d_topDown(topDown),
   d_finished(top == 0),
   d_gotNext(False)
{
}

Boolean AIC_ActivityIter::operator ++ ()
{
   if(d_finished)
      return False;

   if(d_gotNext)
   {
      d_gotNext = False;
      return True;
   }

   if(d_topDown)
   {
      /*
       * Top Down Iterator
       */

      if(d_current == 0)
      {
         d_current = d_top;
      }
      else
      {
         if(d_current->childCount())
            d_current = d_current->children()->first();
         else
         {
            for(;;)
            {
               if(d_current == d_top)
               {
                  d_finished = True;
                  return False;
               }

               AIC_Activity* parent = d_current->parent();
               AIC_Activity* next = parent->children()->next(d_current);
               if(next)
               {
                  d_current = next;
                  break;
               }
               else
                  d_current = parent;
            }
         }
      }

      return True;

   }
   else
   {
      /*
       * Bottom Up iterator
       */


      if(d_current == 0)
      {
         /*
          * Get down to bottom child
          */

         d_current = d_top;
         while(d_current->childCount())
         {
            d_current = d_current->children()->first();
         }
      }
      else
      {
         if(d_current == d_top)
         {
            d_finished = True;
            return False;
         }

         AIC_Activity* parent = d_current->parent();

         ASSERT(parent != 0); // Top unit can not have sisters.

         if(parent)
         {
            AIC_Activity* next = parent->children()->next(d_current);
            if(next)
            {
               d_current = next;
               while(d_current->childCount())
               {
                  d_current = d_current->children()->first();
               }
            }
            else
            {
               d_current = parent;
            }
         }
      }

      return True;
   }
}

// Current item and children are about to be deleted

void AIC_ActivityIter::invalidate()
{
   if(d_topDown)
   {
      if(d_current == 0)
      {
         d_finished = True;
      }
      else
      {
         for(;;)
         {
            if(d_current == d_top)
            {
               d_finished = True;
               break;
            }

            AIC_Activity* parent = d_current->parent();
            AIC_Activity* next = parent->children()->next(d_current);
            if(next)
            {
               d_current = next;
               break;
            }
            else
               d_current = parent;
         }
      }
   }
   else
   {
      if( (d_current == 0) || (d_current == d_top))
      {
         d_finished = True;
      }
      else
      {
         AIC_Activity* parent = d_current->parent();

         ASSERT(parent != 0); // Top unit can not have sisters.

         if(parent)
         {
            AIC_Activity* next = parent->children()->next(d_current);
            if(next)
            {
               d_current = next;
               while(d_current->childCount())
               {
                  d_current = d_current->children()->first();
               }
            }
            else
            {
               d_current = parent;
            }
         }
      }
   }

   d_gotNext = True;
}


/*
 *----------------------------------------------------------------------
 * $Log$
 *----------------------------------------------------------------------
 */
