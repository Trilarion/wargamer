/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
#error "Obsolete"
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 * Campaign AI: Assigning units to activities
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "aic_asgn.hpp"
#include "aic_act.hpp"
#include "aic_sdat.hpp"
#include "aic_org.hpp"
#include "aic_unit.hpp"
#include "aic_util.hpp"
// #include "sllist.hpp"
#include "campdint.hpp"
#include "armies.hpp"
#include <vector>
#include <algo.h>

/*
 * Local Data structures
 */

struct AIC_UnitTakeInfo // : public SLink
{
   ConstICommandPosition cpi;    // Unit
   SPCount spCount;           // SP's willing to let go
   int happiness;             // Chances of getting picked

   AIC_UnitTakeInfo() :
      cpi(NoCommandPosition),
      spCount(0),
      happiness(0)
   {
   }

   AIC_UnitTakeInfo(ConstICommandPosition p_cpi, SPCount p_spCount, int p_happiness)
   {
      cpi = p_cpi;
      spCount = p_spCount;
      happiness = p_happiness;
   }
};

class AIC_UnitTakeList  // : public SList<AIC_UnitTakeInfo>
{
   typedef std::vector<AIC_UnitTakeInfo> Container;
   
   Container d_items;
   // int d_totalHappiness;
 public:
   AIC_UnitTakeList() 
   {
      // d_totalHappiness = 0;
   }

   ~AIC_UnitTakeList() { }

   void add(ConstICommandPosition cpi, SPCount spCount, int happiness);

   AIC_UnitTakeInfo* pick(RandomNumber r);
      // Pick a unit from the list
      // return 0 if none found or the address of an Info structure
};

void AIC_UnitTakeList::add(ConstICommandPosition cpi, SPCount spCount, int happiness)
{
   // AIC_UnitTakeInfo* info = new AIC_UnitTakeInfo(cpi, spCount, happiness);
   // append(info);

   AIC_UnitTakeInfo item(cpi, spCount, happiness);

   d_items.push_back(item);
   // d_totalHappiness += happiness;
}

/*
 * Pick a random entry from list
 */

AIC_UnitTakeInfo* AIC_UnitTakeList::pick(RandomNumber r)
{
#if 0
   struct MinInfo
   {
      MinInfo() : d_val(INT_MAX) { }

      void operator()(const AIC_UnitTakeInfo& info)
      {
         d_val = minimum(d_val, info.happiness);
      }

      operator int() const { return d_val; }
      // int operator()()
      // {
      //    return d_val;
      // }

      int d_val;
   };
#endif

   struct TotalInfo
   {
      TotalInfo(int r) : d_minimum(r), d_total(0) { }

      void operator()(AIC_UnitTakeInfo& info)
      {
         info.happiness -= d_min;
         d_total += info.happiness;
      }

      operator int() const { return d_total; }
      // int operator()()
      // {
      //    return d_total;
      // }

      int d_min;
      int d_total;
   };

   // int minVal = for_each(d_items.begin(), d_items.end(), MinInfo());
   // int total = for_each(d_items.begin(), d_items.end(), TotalInfo(minVal));
   int total = for_each(d_items.begin(), d_items.end(), TotalInfo(0));

   ASSERT(total >= 0);

   if(total != 0)
   {
      int rNum = r(total);

      for(Container::iterator item = d_items.begin(); item != d_items.end(); ++item)
      {
         if(rNum < item->happiness)
         {
            // d_totalHappiness -= item->happiness;
            item->happiness = 0;

            return item;
         }
         rNum -= item->happiness;
      }
   }

   return 0;
}

/*
 * Obtain strength points for an activity
 * Changed now... takes them from anywhere
 * But units are very happy to be transferred to child activities.
 */

static Boolean AIC_AssignUnits::assignSP(AIC_StrategyData* sData, AIC_Activity* activity)
{
#ifdef DEBUG
   sData->logWin("assignSP (need=%d, alloc=%d)",
      (int) activity->spNeeded(),
      (int) activity->spAllocated() );
#endif   

   /*
    * How many SP's do we need?
    *
    * This depends on the type of activity
    * For example an attack activity needs a minimum of SP's with an infinite maximum
    * A defense activity has a 0 minimum, and a given maximum.
    * A mixed activity, will just take whatever it can.
    */

   int spNeed;
   
   if(activity->spNeeded() > activity->spAllocated())
   {
      spNeed = (SPCount) (activity->spNeeded() - activity->spAllocated());

      /*
       * Go through every unit, and add to a table
       * Table contains, SPCount, ConstICommandPosition and happiness.
       * Then we randomly pick entries until we make up our needed amount.
       */

      AIC_UnitTakeList takeList;

      const Armies* armies = sData->armies();
      ConstICommandPosition president = armies->getPresident(sData->side());
      ConstUnitIter iter(armies, president, false);
      while(iter.next())
      {
         ConstICommandPosition cpi = iter.current();

         if(cpi != president)
         {
            const AIC_UnitData* uData = sData->getUnitInfo(cpi);
            if(uData && uData->independent() && (uData->activity() != activity))
            {
               /*
                * Never take from own children
                */

               if(uData->activity() && !AIC_Activity::isChildOf(activity, uData->activity()))
               {
                  int happiness = activity->happiness();
   
                  // if(happiness < 10)
                  //    happiness = 10;
   
                  if(AIC_Activity::isChildOf(uData->activity(), activity))
                  {
                     happiness += uData->activity()->happiness();
                     // happiness += happiness;    // double if coming from parent
                  }
                  else
                     happiness -= uData->activity()->happiness();

                  if(happiness > 0)
                  {
#ifdef DEBUG
                     sData->logWin("Considering(%s, %d, %d)",
                        (const char*) armies->getUnitName(cpi),
                        (int) uData->spCount(),
                        (int) happiness);
#endif
                     takeList.add(cpi, uData->spCount(), happiness);
                  }
               }
            }
         }
      }

      while(spNeed > 0)
      {
         AIC_UnitTakeInfo* info = takeList.pick(sData->random());

         if(info)
         {
#ifdef DEBUG
            sData->logWin("Picked(%s, %d, %d)",
               (const char*) armies->getUnitName(info->cpi),
               (int) info->spCount,
               (int) info->happiness);
#endif
            AIC_UnitAllocator::moveUnit(sData->unitList(), activity, info->cpi);

            activity->resetAge();

            spNeed -= info->spCount;
         }
         else
            break;
      }
   }

   return True;      // Don't decrease priority
}


/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.2  2001/06/18 08:25:44  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
