/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1995, Steven Green, All Rights Reserved
 *----------------------------------------------------------------------
 *
 * Calculate Influence of Units on a town
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "aic_UnitInfluence.hpp"
#include "aic_town.hpp"
#include "aic_sdat.hpp"
#include "campdint.hpp"
#include "armies.hpp"
#include "town.hpp"
#include "aic_unit.hpp"
#include "route.hpp"
#include "scenario.hpp"
#include "aic_objective.hpp"

namespace {
/*
 * Internal Class used when doing calculations
 */

struct UnitInfo
{
   public:
      UnitInfo() :
         d_cp(NoCommandPosition),
         // d_spCount(0),
         d_distInfluence(0)
      {
      }

      // UnitInfo(ConstParamCP cp, SPCount spCount, float distInfluence) :
      UnitInfo(ConstParamCP cp, float distInfluence) :
         d_cp(cp),
         // d_spCount(spCount),
         d_distInfluence(distInfluence)
      {
      }

      ~UnitInfo() { }


      ConstICommandPosition d_cp;
      // SPCount d_spCount;
      float d_distInfluence;
};


class InfluenceCalculator
{
   public:
      typedef TownInfluence::Influence Influence;

      InfluenceCalculator(
            AIC_StrategyData* sData,
            AIC_UnitList* units,
            const AIC_TownInfo& townInfo,
            TownInfluence::UnitList* unitList,
            const DecayAverage<float>& threatRange);

      ~InfluenceCalculator();
      void calculate();

      Influence totalUnitPriority() const { return d_totalUnitPriority; }
      SPCount spNeeded() const { return d_spNeeded; }
      SPCount spAvailable() const { return d_spAvailable; }
      DecayAverage<float> threatRange() const { return d_threatRange; }
      // Influence highestPriority() const { return d_highestPriority; }

      // Accessor functions

   private:
      static void setupRoute(CampaignRoute& route, ITown itown, Side s);
      SPCount calcSPNeeded() const;
      void calcInfluence(Side side, const CampaignRoute& route, Influence* totalInfluence, SPCount* totalSPCount);
      void makeUnitList();
      Influence calcTownPriority() const;

      AIC_StrategyData* d_sData;
      const AIC_TownInfo* d_townInfo;
      AIC_UnitList* d_aiUnits;         // Global list of units

      CampaignRoute d_enemyRoute;
      CampaignRoute d_friendlyRoute;

      /*
       * Intermediate Values
       */

      Side d_enemySide;
      float d_enemyThreat;    // 0..1 value of threat
      Influence d_totalEnemyInfluence;
      SPCount d_totalEnemySPCount;
      Influence d_totalFriendlyInfluence;
      SPCount d_totalFriendlySPCount;
      std::vector<UnitInfo> d_unitInfo;

      // Values to return to player

      TownInfluence::UnitList* d_unitList;
      Influence d_totalUnitPriority;
      SPCount d_spNeeded;
      SPCount d_spAvailable;
      DecayAverage<float> d_threatRange;
      // Influence d_highestPriority;


};

InfluenceCalculator::InfluenceCalculator(
            AIC_StrategyData* sData,
            AIC_UnitList* units,
            const AIC_TownInfo& townInfo,
            TownInfluence::UnitList* unitList,
            const DecayAverage<float>& threatRange) :
   d_sData(sData),
   d_townInfo(&townInfo),
   d_aiUnits(units),
   d_enemyRoute(sData->campData()),
   d_friendlyRoute(sData->campData()),

   d_enemySide(sData->enemySide()),
   d_enemyThreat(0),
   d_totalEnemyInfluence(0),
   d_totalEnemySPCount(0),
   d_totalFriendlyInfluence(0),
   d_totalFriendlySPCount(0),
   d_unitInfo(),

   d_unitList(unitList),
   d_totalUnitPriority(0),
   d_spNeeded(0),
   d_spAvailable(0),
   d_threatRange(threatRange)
//   d_highestPriority(0)
   // d_outData(&outData)
{
   ASSERT(d_sData != 0);
   // ASSERT(d_townInfo != 0);
   ASSERT(d_aiUnits != 0);
   ASSERT(d_aiUnits->size() != 0);
}

InfluenceCalculator::~InfluenceCalculator()
{
   d_sData = 0;
   d_townInfo = 0;
   d_aiUnits = 0;
   // d_outData = 0;
   d_unitList = 0;
}

void InfluenceCalculator::setupRoute(CampaignRoute& route, ITown itown, Side side)
{
   // Assume it will take 7 days to get past an enemy controlled town
   // This will force route finder to try to avoid enemy towns, but not
   // make them completely out of bounds.

   const TimeTick enemyDelay = DaysToTicks(7);

   // Flags to use for route finding
   //    - UseCosts : RouteCost will be time in TimeTick
   //    - NoEnemy : We might want to use this instead of the enemyDelay
   //    - SameSide : We may want to consider this to avoid neutral towns

   const CampaignRoute::RouteFlag flags = CampaignRoute::UseCosts;

   route.setup(flags, side, enemyDelay);
   route.fillRoute(itown);
}

void InfluenceCalculator::calculate()
{
   ASSERT(d_townInfo != 0);

   // Useful values

   // const CampaignData* campData = d_sData->campData();
   // Side enemySide = otherSide(d_sData->side());

   ITown itown = d_townInfo->town();
   const Town& town = d_sData->campData()->getTown(itown);

   setupRoute(d_enemyRoute, itown, d_enemySide);
   setupRoute(d_friendlyRoute, itown, d_sData->side());

   // Get enemy Influence

   calcInfluence(d_enemySide, d_enemyRoute, &d_totalEnemyInfluence, &d_totalEnemySPCount);
   calcInfluence(d_sData->side(), d_friendlyRoute, &d_totalFriendlyInfluence, &d_totalFriendlySPCount);

   // if(d_totalEnemySPCount != 0)
   //    d_enemyThreat = d_totalEnemyInfluence / static_cast<float>(d_totalEnemySPCount);
   if (d_sData->side() == town.getSide())
   {
      d_enemyThreat = d_threatRange.updateAndNormalize(d_totalEnemyInfluence);
      // d_enemyThreat = d_sData->normalizeThreat(d_totalEnemyInfluence);
   }
   else
      d_enemyThreat = 1.0;

   d_spNeeded = calcSPNeeded();

   if(d_spNeeded > 0)
      makeUnitList();

#ifdef DEBUG
   // d_sData->logWin("enemyInfluence=%f/%d, friendInfluence=%f/%d, townSP=%d\n",
   d_sData->logWin("enemyInfluence=%f/%d, friendInfluence=%f/%d\n",
      static_cast<float>(d_totalEnemyInfluence),
      static_cast<int>(d_totalEnemySPCount),
      static_cast<float>(d_totalFriendlyInfluence),
      static_cast<int>(d_totalFriendlySPCount));
//      static_cast<int>(townStrength) );

   d_sData->logWin("spNeeded=%d, spAvailable=%d, availableInfluence=%f",
         static_cast<int>(d_spNeeded),
         static_cast<int>(d_spAvailable),
         static_cast<float>(d_totalUnitPriority));
#endif
}

/*
 * Get the SP Needed to take or defend the town
 */

SPCount InfluenceCalculator::calcSPNeeded() const
{
   Influence spNeeded = d_totalEnemyInfluence - d_totalFriendlyInfluence;
//   SPCount spNeeded  = 0;
//   if (d_totalFriendlyInfluence < d_totalEnemyInfluence)
//   {
//      spNeeded = d_totalEnemyInfluence - d_totalFriendlyInfluence;
//   }

   // Calculate town's strength
   // If it is our town then reduce spNeeded
   // If it is enemy then increase
   // If neutral then do nothing.

   const CampaignData* campData = d_sData->campData();
   const Town& town = campData->getTown(d_townInfo->town());

   if(town.getSide() == d_sData->side())
   {
      // How much strength does it provide gainst enemy?

      // SPCount townStrength = campData->getTownStrength(d_townInfo->town(), town.getSide());
      SPCount townStrength = campData->getTownStrength(d_townInfo->town(), d_enemySide);
      if(townStrength < spNeeded)
         spNeeded -= townStrength;
      else
         spNeeded = 0;
   }
   else // if(town.getSide() == d_enemySide) OR neutral
   {
      // How much strength is needed to take it over
      // SPCount townStrength = campData->getTownStrength(d_townInfo->town(), d_enemySide);
      SPCount townStrength = campData->getTownStrength(d_townInfo->town(), d_sData->side());

      if(spNeeded < 0)
         spNeeded = 0;

      spNeeded += townStrength;
   }

   /*
    * Adjust by global caution
    */

   if (spNeeded > 0)
   {
      spNeeded *= d_sData->caution();
   }

   if(spNeeded > 0)
      return static_cast<SPCount>(spNeeded + 1);
   else
      return 0;
}

/*
 * Calculates Influence on town by given side.
 *
 * Fills in
 */

void InfluenceCalculator::calcInfluence(Side side, const CampaignRoute& route, Influence* pTotalInfluence, SPCount* pTotalSPCount)
{
   ASSERT(pTotalInfluence);
   ASSERT(pTotalSPCount);

   Influence totalInfluence = 0;
   SPCount totalSPCount = 0;

   const CampaignData* campData = d_sData->campData();
   const Armies* army = d_sData->armies();
   ITown itown = d_townInfo->town();

   // Get Route Info from current location

//--- Use the AI Unit list instead
//--- NO.. must use real data because enemy side is not in AI UnitList.

    ConstUnitIter unitIter(army, army->getFirstUnit(side));
    while(unitIter.sister())
    {
       ConstICommandPosition cp = unitIter.current();

       // AIC_UnitRef aiUnit = d_aiUnits->getOrCreate(infUnit.cp());
       // SPCount spCount = aiUnit.spCount();
       // SPCount spCount = army->getUnitSPCount(cp, true);

//    for (AIC_UnitList::const_iterator it = d_aiUnits->begin();
//          it != d_aiUnits->end();
//          ++it)
//    {
//       AIC_ConstUnitRef aiUnit((*it).first, &(*it).second);
//
//       ConstICommandPosition cp = aiUnit.cp();
//       SPCount spCount = aiUnit.spCount();

      // Calculate how long it will take unit to get here

      ITown iUnitTown = cp->getCloseTown();
      const Town& unitTown = campData->getTown(iUnitTown);

      // If unit is under siege then skip unit unless target of this objective

      if((iUnitTown != itown) && cp->isGarrison() && unitTown.isSieged())
      {
#ifdef DEBUG
         d_sData->logWin("%s is under siege",
            static_cast<const char*>(cp->getName()));
#endif
      }
      else if(route.isConnected(iUnitTown))
      {
         TimeTick arrivalTime = route.cost(iUnitTown);
         SPCount spCount = army->getUnitSPCount(cp, true);

         float distInfluence = 1;
         const float FlatTime = HoursToTicks(24);     // 1 Day march has no time penalty
         if (arrivalTime > FlatTime)
         {
            float d = arrivalTime - FlatTime;

            // const float HalfTime = HoursToTicks(36);    // At 1.5 days it has 0.5 priority
            // const float HalfTime = HoursToTicks(12);    // At .5 days it has 0.5 priority
            const float HalfTime = HoursToTicks(24*3);    // At 3 days it has 0.5 priority
            const float H2 = HalfTime * HalfTime;
            distInfluence = H2 / (d * d + H2);
         }

         float townSPInfluence = distInfluence * float(spCount);

         totalSPCount += spCount;
         totalInfluence += townSPInfluence;

         if (side == d_sData->side())
         {
            // Add to list of units that can be ordered

            // d_unitInfo.push_back(UnitInfo(cp, spCount, distInfluence));
            d_unitInfo.push_back(UnitInfo(cp, distInfluence));

            // Also update the AI Unit List

            AIC_UnitRef aiUnit = d_aiUnits->getOrCreate(cp);
            aiUnit.update(army);
         }

      }
   }

#ifdef DEBUG
   d_sData->logWin("Total Influence for %s=%f / %d",
      static_cast<const char*>(scenario->getSideName(side)),
      static_cast<float>(totalInfluence),
      static_cast<int>(totalSPCount));
#endif

   *pTotalInfluence = totalInfluence;
   *pTotalSPCount = totalSPCount;
}

/*
 * Get a base town priority
 *
 * We used to just use the townInfo.priority()
 *
 * But we want to add in some more factors, such as the threat value
 *
 */

InfluenceCalculator::Influence InfluenceCalculator::calcTownPriority() const
{
   const Town& town = d_sData->campData()->getTown(d_townInfo->town());

   /*
    * get initial value as calculated during WorldProcess
    * This is a combo of:
    *    Resource,
    *    Victory points
    *    Connectivity
    *    Distance from front line
    */

   Influence influence = d_townInfo->importance();

   ASSERT(d_enemyThreat >= 0.0);
   ASSERT(d_enemyThreat <= 1.0);
   influence *= d_enemyThreat;

   if (town.getSide() == d_sData->side())
   {
      /*
       * Friendly Town: Defend
       *                Priority is reduced if not under threat
       *
       * Modify: 1Sep99: Increase Priority if under threat!
       */

//      ASSERT(d_enemyThreat >= 0.0);
//      ASSERT(d_enemyThreat <= 1.0);

//      influence *= d_enemyThreat;
   }
   else if (town.getSide() == SIDE_Neutral)
   {
      /*
       * Neutral Town: Attack
       *               Increase if agressive
       */

      influence *= d_sData->agression();
   }
   else
   {
      /*
       * Enemy Town: Attack
       *             Adjust if agressive
       */

      ASSERT(town.getSide() == d_enemySide);
      influence *= d_sData->agression();
   }

   /*
    * Apply some randomness as well
    */

   // const int RandomPercent = 10;     // +/- 10% variance
   const int RandomPercent = 5;     // +/- 5% variance
   const int RandomRange = 0x4000;
   const int RandomAdjust = (RandomPercent * RandomRange) / 100;
   int r = d_sData->rand(RandomRange - RandomAdjust, RandomRange + RandomAdjust);
   float rf = static_cast<float>(r) / static_cast<float>(RandomRange);
   influence *= rf;

#ifdef DEBUG
   d_sData->logWin("Priority is %f (rand=%f, agress=%f, threat=%f)",
            static_cast<float>(influence),
            static_cast<float>(rf),
            static_cast<float>(d_sData->agression()),
            static_cast<float>(d_enemyThreat));
#endif

   return influence;
}

/*
 * Build UnitList that can do objective
 */

void InfluenceCalculator::makeUnitList()
{
   TownInfluence::UnitList* unitList = d_unitList;

   ASSERT(unitList->size() == 0);
   // ASSERT(d_spNeeded == 0);
   ASSERT(d_spAvailable == 0);
   ASSERT(d_totalUnitPriority == 0);
   ASSERT(d_aiUnits->size() != 0);

   SPCount spAvailable = 0;
   Influence totalUnitPriority = 0;

   Influence townPriority = calcTownPriority();

   for(; d_unitInfo.size(); d_unitInfo.pop_back())
   {
      UnitInfo& unitInfo = d_unitInfo.back();
      ConstICommandPosition cp = unitInfo.d_cp;
      // SPCount spCount = unitInfo.d_spCount;

      float unitPriority = unitInfo.d_distInfluence * townPriority;
      // float currentPriority = 0;

      ASSERT(d_aiUnits->isMember(cp));
      AIC_ConstUnitRef aiUnit = (*d_aiUnits)[cp];
      SPCount spCount = aiUnit.spCount();
      ASSERT( (aiUnit.priority() == 0) || (aiUnit.objective() != 0) );     // Must have an objective if priority not 0

      float currentPriority = TownInfluence::effectivePriority(aiUnit); // aiUnit.priority();

      // adjust currentPriority to take into account glut of
      // units assigned to objective


//       if(d_aiUnits->isMember(cp))
//       {
//          AIC_ConstUnitRef aiUnit = (*d_aiUnits)[cp];
//          ASSERT( (aiUnit.priority() == 0) || (aiUnit.objective() != 0) );     // Must have an objective if priority not 0
//          currentPriority = aiUnit.priority();
//       }

      if(currentPriority < unitPriority) // townInfluence)
      {
         // Add to list

         unitList->push_back(TownInfluence::Unit(cp, unitPriority));   // townInfluence));
         spAvailable += spCount;
         totalUnitPriority += unitPriority;         // townInfluence;

//         if(unitPriority > d_highestPriority)
//            d_highestPriority = unitPriority;



#ifdef DEBUG
         TimeTick arrivalTime = d_friendlyRoute.cost(cp->getCloseTown());

         d_sData->logWin("Can Use %5.2f [%5.2f] SP=%3d hours=%2d %s at %s",
            static_cast<float>(unitPriority),
            static_cast<float>(currentPriority),
            static_cast<int>(spCount),
            static_cast<int>(arrivalTime / HoursToTicks(1)),
            static_cast<const char*>(cp->getName()),
            static_cast<const char*>(d_sData->campData()->getTownName(cp->getCloseTown()))
            );
#endif
      }
   }


   d_spAvailable = spAvailable;
   d_totalUnitPriority = totalUnitPriority;

#ifdef DEBUG
   d_sData->logWin("assignedInfluence=%f, SP=%d",
         static_cast<float>(totalUnitPriority),
         static_cast<int>(spAvailable));
#endif
}


}; // internal namespace

/*
 * Constructor
 */

TownInfluence::TownInfluence(AIC_StrategyData* sData, AIC_UnitList* units) :
   d_sData(sData),
   d_aiUnits(units),
   d_units(),
   d_totalUnitPriority(0),
   d_spNeeded(0),
   d_spAvailable(0),
   d_threatRange(sData->campData()->getTowns().entries())
//   d_highestPriority(0)
{
   ASSERT(sData != 0);
}

/*
 * Destructor
 */

TownInfluence::~TownInfluence()
{
   d_sData = 0;
}

void TownInfluence::init(const AIC_TownInfo& townInfo)
{
   reset();
   // InfluenceCalculator::OutData data(&d_units, &d_totalUnitPriority, &d_spNeeded, &d_spAvailable);

   InfluenceCalculator calc(d_sData, d_aiUnits, townInfo, &d_units, d_threatRange);
   calc.calculate();             // maybe need parameters

   // Retrieve values

   d_totalUnitPriority = calc.totalUnitPriority();
   d_spNeeded = calc.spNeeded();
   d_spAvailable = calc.spAvailable();
   d_threatRange = calc.threatRange();
//   d_highestPriority = calc.highestPriority();
}

void TownInfluence::reset()
{
   d_totalUnitPriority = 0;
   d_spNeeded = 0;
   d_spAvailable = 0;
   // d_threatRange = 0;
//   d_highestPriority = 0;
   d_units.erase(d_units.begin(), d_units.end());
}

/*
 * return true if this objective is possible
 */

bool TownInfluence::canAchieve()
{
   // calculate();
   return (d_spAvailable > 0) && (d_spAvailable >= d_spNeeded) && (d_spNeeded > 0);
}

SPCount TownInfluence::spNeeded()
{
   // calculate();
   return d_spNeeded;
}

// TownInfluence::Influence TownInfluence::threat()
// {
//    calculate();
//    return d_enemyThreat;
// }

#if 0
/*
 * Do the actual calculation
 */

void TownInfluence::calculate()
{
   if(!d_calculated)
   {
      d_calculated = true;

      InfluenceCalculator calc;     // will need some paramaters
      calc.calculate();             // maybe need parameters

      // Copy values out of calculator into our own data
   }
}
#endif

/*
 * Choose a random unit from list and remove it.
 *
 * This is slightly complicated because influence() is float, but random uses integers
 * We will pick a random intenger, but then cast it to a float and hope that we don't lose much accuracy
 *
 * Another approach might be to normalise the priorities and then round to the nearest integer
 *
 * It may be that if there are lots of units with tiny influences() they may not get picked?
 */

bool TownInfluence::pickAndRemove(Unit* result)
{
   ASSERT(result != 0);

    if(d_units.size() == 0)
        return false;

   ASSERT(d_units.size() != 0);
   ASSERT(d_totalUnitPriority != 0);

   const int MaxValue = INT_MAX;
   // double normal = MaxValue / d_totalUnitPriority;

   int ir = d_sData->rand(static_cast<int>(MaxValue));
   // double r = ir;
   double r = (ir * d_totalUnitPriority) / MaxValue;

   for(UnitList::iterator it = d_units.begin();
      it != d_units.end();
      ++it)
   {
      float influence = (*it).influence();
      // double normVal = influence * normal;

      // if(normVal >= r)
      if(influence >= r)
      {
         *result = *it;
         // ASSERT(d_totalUnitPriority >= influence);
         ASSERT( (d_totalUnitPriority - influence) > -(d_totalUnitPriority / 1000));   //-- should be >= 0 within an error margin
         if(d_totalUnitPriority < influence)
            d_totalUnitPriority = 0;
         else
            d_totalUnitPriority -= influence;
         d_units.erase(it);
         return true;
      }

      // r -= normVal;
      r -= influence;

      ASSERT(r >= 0);
   }

   return false;
}

TownInfluence::Influence TownInfluence::effectivePriority(const AIC_ConstUnitRef& aiUnit)
{
   float currentPriority = aiUnit.priority();
   const AIC_Objective* objective = aiUnit.objective();
   if(objective)
   {
      SPCount spNeeded = objective->spNeeded();
      SPCount spAlloced = objective->spAllocated();

      if(spAlloced && (spAlloced > spNeeded))
      {
         // p = p (n/a)^k
         // try k=2 for now

         float ratio = static_cast<float>(spNeeded) / static_cast<float>(spAlloced);
         ratio *= ratio;
         currentPriority *= ratio;
      }
   }
   return currentPriority;
}


/*
 *----------------------------------------------------------------------
 * $Log$
 *----------------------------------------------------------------------
 */
