/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 * Campaign AI : Town priorities
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "aic_town.hpp"
#include "campdint.hpp"
#include "town.hpp"
#include "aic_sdat.hpp"
#include "filecnk.hpp"
// #include <algo.h>

#include "supplyLine.hpp"
#ifdef DEBUG
#include "scenario.hpp"
#include "fonts.hpp"
#include "dib.hpp"
#include "wmisc.hpp"

extern LogFileFlush aiLog;

#endif

//lint -save -e613 -e662 -e661 ... possible null pointer, possible array out of bounds

/*
 * We use code that is similar to the route finder, but not similar
 * enough to re-use.
 * Fill out nodes to get distance (and nodes) that each location is
 * from a non-friendly node
 */

struct TownAINode {
   ITown    d_next;              // Next node if on stack

   UWORD    d_nodeCount;         // How many nodes from enemy location
   Distance d_nodeDistance;   // Physical distance from enemy location

   Boolean  d_inQueue;        // Set if in queue
   Boolean  d_visited;        // Clear if not been visited yet

 public:
   TownAINode();
};

#define NodeCount_MAX UWORD_MAX

class TownAICalc {
   TownAINode* nodes;
   int nodeCount;
   Side side;              // Which side is this being calculated for?

   ITown usedList;         // Entry point to queue of towns
#ifdef DEBUG
   int iterations;
   int firstIntersection;
#endif

public:
   TownAICalc(Side s);
   ~TownAICalc();

   void calculate(const CampaignData* campData);

   const TownAINode* getNodes() const { return nodes; }
   TownAINode* getNodes() { return nodes; }

private:
   void addNode(ITown t);
   ITown getNode();
   void makeNodes(int howMany);
};



namespace   // private namespace
{

/*
 * Handly calculation
 * Reduces value such that when n==k, value is halved
 * As n tends to infinity, then value tends to 0.
 * k must be >= 1
 */

inline int modifyByN(int value, int n, int k)
{
   ASSERT(value >= 0);
   ASSERT(n >= 0);
   ASSERT(k > 0);

   return MulDiv(value, k, n + k);
}

template<class T, class R>
inline R normalise(T val, const MinMax<T>& inRange, R outRange)
{
   ASSERT(val >= inRange.minValue());
   ASSERT(val <= inRange.maxValue());

   return static_cast<R>((outRange * (val - inRange.minValue())) /
         (inRange.maxValue() - inRange.minValue()));
}

/*
 * Modify value:
 *    if range == 0   : value -= value * change;
 *    if range == 1.0 : value = value;
 *
 * e.g. modify(value, 0.8, 0.75, 1.0)
 *       will return value * (0.75 + 0.8 * 0.25) = 0.95
 */

template<class T>
inline unsigned int modify(unsigned int value, T attribute, const MinMax<T>& range, float minVal, float maxVal)
{
   ASSERT(minVal >= 0);
   ASSERT(minVal <= 1.0);
   ASSERT(maxVal >= 0);
   ASSERT(maxVal <= 1.0);

   float rangeValue = static_cast<float>(attribute - range.minValue()) / static_cast<float>(range.maxValue() - range.minValue());
   return static_cast<unsigned int>(value * (minVal + rangeValue * (maxVal - minVal)));
}


}; // private namespace



TownAICalc::TownAICalc(Side s) :
#ifdef DEBUG
   iterations(0),
   firstIntersection(0),
#endif
   nodes(0),
   nodeCount(0),
   side(s),
   usedList(NoTown)
{
}

TownAICalc::~TownAICalc()
{
   if(nodes != 0)
      delete[] nodes;
}

TownAINode::TownAINode()
{
   d_next = NoTown;
   d_nodeCount = NodeCount_MAX;
   d_nodeDistance = Distance_MAX;
   d_inQueue = False;
   d_visited = False;
}

void TownAICalc::makeNodes(int howMany)
{
   if(nodes == 0)
   {
      /*
       * Create Nodes
       */

      nodeCount = howMany;
      ASSERT(nodeCount > 0);
      nodes = new TownAINode[nodeCount];
      ASSERT(nodes != 0);
   }

   ASSERT(howMany == nodeCount);
}

void TownAICalc::calculate(const CampaignData* campData)
{
   const TownList& tl = campData->getTowns();
   const ConnectionList& cl = campData->getConnections();

   makeNodes(tl.entries());

   ASSERT(nodes != 0);

   /*
    * Initialise nodes
    */

   for(int i = 0; i < nodeCount; i++)
   {
      nodes[i].d_next = NoTown;
      nodes[i].d_nodeCount = NodeCount_MAX;
      nodes[i].d_nodeDistance = Distance_MAX;
      nodes[i].d_inQueue = False;
      nodes[i].d_visited = False;
   }

   usedList = NoTown;

#ifdef DEBUG
   iterations = 0;
   firstIntersection = 0;
#endif

   /*
    * Add a starting node
    */

   addNode(ITown(0));

   while(usedList != NoTown)
   {
#ifdef DEBUG
      iterations++;
#endif
      ITown n = getNode();
      ASSERT(n != NoTown);

      /*
       * Go through connections
       */

      const Town& town = tl[n];
      TownAINode& rn = nodes[n];

      UWORD nodeCount = rn.d_nodeCount;
      if(nodeCount < NodeCount_MAX)
         nodeCount++;

      for(int i = 0; i < MaxConnections; i++)
      {
         IConnection ci = town.getConnection(i);
         if(ci == NoConnection)
            break;

         const Connection& con = cl[ci];
         ITown nextTown = con.getOtherTown(n);

         const Town& nTown = tl[nextTown];

         TownAINode& nextrn = nodes[nextTown];

         Distance newDistance;
         UWORD newNodeCount;

         Distance conDist = distanceLocation(town.getLocation(), nTown.getLocation());

         //lint -e(514) ... unusual use of bool
         Boolean otherSide = ( ((town.getSide() == side) ^ (nTown.getSide() == side)) != 0);
         if(otherSide)
         {
#ifdef DEBUG
            if(firstIntersection == 0)
               firstIntersection = iterations;
#endif

            newDistance = conDist;
            newNodeCount = 0;

            /*
             * Add ourselves back into queue
             */

            if( (rn.d_nodeCount > newNodeCount) || (rn.d_nodeDistance > newDistance) )
            {
               rn.d_nodeCount = newNodeCount;
               rn.d_nodeDistance = newDistance;
               addNode(n);
               break;      // Break out of connection loop, pointless continuing
            }
         }
         else
         {
            newDistance = rn.d_nodeDistance;
            if(newDistance != Distance_MAX)
               newDistance += conDist;
            newNodeCount = nodeCount;
         }

         if(!nextrn.d_visited || (nextrn.d_nodeCount > newNodeCount) || (nextrn.d_nodeDistance > newDistance) )
         {
            nextrn.d_nodeCount = newNodeCount;
            nextrn.d_nodeDistance = newDistance;
            addNode(nextTown);
         }
      }
   }


#ifdef DEBUG
   aiLog.printf("Iterations = %d", iterations);
   aiLog.printf("iterations until first intersection = %d", firstIntersection);
#endif
}


void TownAICalc::addNode(ITown t)
{
   if(!nodes[t].d_inQueue)
   {
      nodes[t].d_inQueue = True;
      nodes[t].d_visited = True;

      nodes[t].d_next = usedList;
      usedList = t;
   }
}

ITown TownAICalc::getNode()
{
   ITown t = usedList;

   if(usedList != NoTown)
   {
      ASSERT(nodes[t].d_inQueue);

      usedList = nodes[usedList].d_next;
      nodes[t].d_inQueue = False;
      nodes[t].d_next = NoTown;     // Unneccesary, but keeps it tidy
   }

   return t;
}




/*=================================================================
 * Public Functions
 */

/*=================================================================
 * AIC_TownInfo Functions
 */

AIC_TownInfo::AIC_TownInfo() :
   d_town(NoTown),
   d_enemyTownLinks(0),
   d_enemyDistance(0),
   d_resValue(0),
   d_conValue(0),
   d_needSupplyDepot(false),
   d_importance(0),
   d_pickPriority(0)
   // d_threat(0)
{
}


UWORD AIC_TownInfo::s_fileVersion = 0x0001;

bool AIC_TownInfo::readData(FileReader& f)
{
   UWORD version;
   f >> version;
   ASSERT(version <= s_fileVersion);

   f >> d_town;
   f >> d_enemyTownLinks;
   f >> d_enemyDistance;
   f >> d_resValue;
   f >> d_conValue;

   if(version < 0x0001)
      d_needSupplyDepot = false;
   else
      f >> d_needSupplyDepot;

   f >> d_importance;
   f >> d_pickPriority;

   return f.isOK();
}

bool AIC_TownInfo::writeData(FileWriter& f) const
{
   f << s_fileVersion;

   f << d_town;
   f << d_enemyTownLinks;
   f << d_enemyDistance;
   f << d_resValue;
   f << d_conValue;
   f << d_needSupplyDepot;
   f << d_importance;
   f << d_pickPriority;

   return f.isOK();
}


/*=================================================================
 * TownList Functions
 */

AIC_TownList::AIC_TownList(AIC_StrategyData* sData) :
   d_sData(sData),
   d_items(),
   d_connectivity(),
   d_totalImportance(0),
   d_totalPickPriority(0),
   d_conModifier(256),
   d_resModifier(32)          // Resources less important than connectivity
{
   const TownList& towns = d_sData->campData()->getTowns();
   d_items.resize(towns.entries());

   /*
    * Initialise town indeces
    */

   for(ITown i = 0; i < towns.entries(); i++)
   {
      d_items[i].d_town = i;
   }

   d_connectivity.make(d_sData->campData());
}

AIC_TownList::~AIC_TownList()
{
   d_sData = 0;
}

UWORD AIC_TownList::s_fileVersion = 0x0000;

bool AIC_TownList::readData(FileReader& f)
{
   UWORD version;
   f >> version;
   ASSERT(version <= s_fileVersion);

   f >> d_conModifier;
   f >> d_resModifier;
   f >> d_totalImportance;
   f >> d_totalPickPriority;

   int count;
   f >> count;
   ASSERT(count == d_items.size());
   for (Container::iterator it = d_items.begin();
      it != d_items.end();
      ++it)
   {
      (*it).readData(f);
   }

   return f.isOK();
}

bool AIC_TownList::writeData(FileWriter& f) const
{
   f << s_fileVersion;

   f << d_conModifier;
   f << d_resModifier;
   f << d_totalImportance;
   f << d_totalPickPriority;

   int count = d_items.size();
   f << count;
   for (Container::const_iterator it = d_items.begin();
      it != d_items.end();
      ++it)
   {
      (*it).writeData(f);
   }

   return f.isOK();
}


void AIC_TownList::setPriority(AIC_TownInfo& info, unsigned int p)
{
   ASSERT(d_totalPickPriority >= info.priority());
   d_totalPickPriority -= info.priority();
   info.d_pickPriority = p;
   d_totalPickPriority += info.priority();
   d_sData->priority(AIC_CODE_Create, d_totalPickPriority);
   // d_sData->addPriority(AIC_CODE_Create, p, d_totalPickPriority);
}

void AIC_TownList::setImportance(AIC_TownInfo& info, unsigned int p)
{
   d_totalImportance -= info.d_importance;
   info.d_importance = p;
   d_totalImportance += p;
   setPriority(info, p);
}

/*
 * Fill out Town priorities for the AI side
 */

void AIC_TownList::make()
{
#ifdef DEBUG
   aiLog.printf("-----------------------------------");
   aiLog.printf("Making Town Info for side %s", scenario->getSideName(d_sData->side()));
#endif

   const CampaignData* campData = d_sData->campData();
   const TownList& towns = campData->getTowns();
   ASSERT(towns.entries() > 0);

   ASSERT(entries() == towns.entries());

   // Use the route finder to get info

   TownAICalc calc(d_sData->side());

   calc.calculate(campData);

   /*
    * Work out good locations for new supply depots
    */


   calcSupplyDepots(campData);


#ifdef DEBUG
   int maxNode = 0;
   Distance maxDistance = 0;
#endif

   MinMax<unsigned int> resRange;
   MinMax<AIC_TownConnectivity::Type> conRange;
   MinMax<Attribute> supplyRange;
   MinMax<Attribute> victoryRange;

   /*
    * Calculate useful values
    */

   // Side enemySide = otherSide(d_sData->side());

   {

      const TownAINode* nodes = calc.getNodes();

      for(ITown iInfo = 0; iInfo < entries(); ++iInfo)   //lint !e632 !e638 ... Strong type mismatch
      {
         /*
          * Calculate value of town
          */

         Writer writeLock(this);
         AIC_TownInfo& info = d_items[iInfo];
         ITown iTown = info.d_town;
         const Town& t = towns[iTown];

         /*
          * Calculate Resource Value
          */

         unsigned int resValue = t.getManPowerRate() + t.getResourceRate();

         if(t.getIsCapital())
         {
            // Add up province's resources

            const Province& province = campData->getProvince(t.getProvince());
            int total = 0;
            ITown stop = ITown(province.getTown() + province.getNTowns());
            for(ITown tIndex = province.getTown(); tIndex < stop; tIndex++)
            {
               const Town& town = campData->getTown(tIndex);
               total += town.getManPowerRate() + town.getResourceRate();
            }

            resValue += total;
         }

         info.d_resValue = resValue;
         resRange += resValue;

         Attribute supply = 0;
         if(t.getIsSupplySource())
            supply = t.getSupplyRate();
         supplyRange += supply;

         victoryRange += t.getVictory(d_sData->side());

         // info.d_conValue = d_connectivity.get(iTown) / (1 + nodes[iTown].d_nodeCount);
         int distSquared = nodes[iTown].d_nodeCount;
         distSquared *= distSquared;
         info.d_conValue = d_connectivity.get(iTown) / (1 + distSquared);
         conRange += info.d_conValue;

         info.d_enemyTownLinks = nodes[iTown].d_nodeCount;
         info.d_enemyDistance = nodes[iTown].d_nodeDistance;

         #ifdef DEBUG
            if(info.d_enemyTownLinks > maxNode)
               maxNode = info.d_enemyTownLinks;
            if(info.d_enemyDistance > maxDistance)
               maxDistance = info.d_enemyDistance;
         #endif

      }
   }

   /*
    * Calculate priorities
    */

   {
      for(ITown iInfo = 0; iInfo < entries(); ++iInfo)
      {
         Writer writeLock(this);
         AIC_TownInfo& info = d_items[iInfo];
         const Town& t = towns[info.d_town];

         /*
          * All this needs changing:
          *
          * if friendly town
          *    consider threat, connectivity, resources
          * else if enemy town
          *    consider resource, distance from front line
          * else neutral town
          *    consider threat, resource, connectivity
          */

         /*
          * Start with Connectivity as a base
          */

         // Some constants, which we may want to make variables later

         const unsigned int MaxPriority = 0x10000; // arbitary value for maximum
         const float ResourceModifier = 0.2;       // Resources have 20% effect
         const float SupplyModifier = 0.2;         // Supply has 20% effect
         const float VictoryModifier = 0.3;        // Resource has 20% effect



         unsigned int priority = normalise(info.d_conValue, conRange, MaxPriority);

         // Adjust using various other attributes

         // Resources
         priority = modify(priority, info.d_resValue, resRange, 1.0f - ResourceModifier, 1.0f);

         // Supply
         Attribute supply = 0;
         if(t.getIsSupplySource())
            supply = t.getSupplyRate();
         priority = modify(priority, supply, supplyRange, 1.0f - SupplyModifier, 1.0f);

         // Victory

         priority = modify(priority, t.getVictory(d_sData->side()), victoryRange, 1.0f - VictoryModifier, 1.0f);

         if(info.d_needSupplyDepot)
            priority += priority / 4;     // +25%

         // Would this cut a Supply route?
         // Todo:


//          AIC_TownConnectivity::Type conValue = info.d_conValue;
//          unsigned int resValue = info.d_resValue;
//
//          unsigned int priority = normalise(conValue, conRange.minValue(), conRange.maxValue(), d_conModifier);
//          priority += normalise(resValue, resRange.minValue(), resRange.maxValue(), d_resModifier);

         // UnitThreat::Influence MaxThreat = 1.0;
         // UnitThreat::Influence threat = normalise(info.d_threat, threatRange.minValue(), threatRange.maxValue(), MaxThreat);
         // priority = static_cast<float>(priority) * threat;

         setPriority(info, priority);
         setImportance(info, priority);
      }
   }

   /*
    * Adjust priority/importance so each town adds on half
    * of all neighbouring town priorities
    */

   modifyNeighbours();

   /*
    * remove all towns with priority lower than average
    */

   int average = d_totalImportance / entries();
   for (Container::iterator it = d_items.begin(); it != d_items.end(); ++it)
   {
      if(it->priority() < average)
         setPriority(*it, 0);
   }


#ifdef DEBUG
   aiLog.printf("Total Importance = %d", (int) d_totalImportance);
   aiLog.printf("Total Priority = %d", (int) d_totalPickPriority);
#endif

#ifdef DEBUG
   aiLog.printf("Maximum = %d (%d miles)",
      (int) maxNode,
      (int) DistanceToMile(maxDistance));
#endif

//    sort();
//
#ifdef DEBUG
    aiLog.printf("List of towns");
    logTowns();
#endif
}

/*
 * Add on proportion of each towns priority to its neighbour
 */

void AIC_TownList::modifyNeighbours()
{
   std::vector<unsigned int> basePriorities;
   std::vector<unsigned int> newPriorities;

   newPriorities.reserve(entries());

   // Initialise values

   {
      for (ITown i = 0; i < entries(); ++i)
      {
         newPriorities.push_back(d_items[i].importance());
      }
   }

   // For each town trace connections and adjust

   {
      const CampaignData* campData = d_sData->campData();
      const ConnectionList& cl = campData->getConnections();
      const TownList& towns = campData->getTowns();

      for (ITown i = 0; i < entries(); ++i)
      {
         AIC_TownInfo& info = d_items[i];
         ITown iTown = info.d_town;
         const Town& town = towns[iTown];

         for(int c = 0; c < MaxConnections; c++)
         {
            IConnection ci = town.getConnection(c);
            if(ci == NoConnection)
               break;

            const Connection& con = cl[ci];

            ITown otherITown = con.getOtherTown(iTown);
            // const Town& otherTown = towns[otherITown];

            // this is slow!!!
            // AIC_TownInfo& otherInfo = find(otherITown);
            AIC_TownInfo& otherInfo = d_items[otherITown];
            // Only works if items are not sorted
            ASSERT(otherInfo.town() == otherITown);

            newPriorities[i] += otherInfo.importance() / 2;
         }
      }
   }

   // Copy values back to items

   {
      for (ITown i = 0; i < entries(); ++i)
      {
         // setImportance sets priority and importance

         setImportance(d_items[i], newPriorities[i]);
      }
   }
}

void AIC_TownList::calcSupplyDepots(const CampaignData* campData)
{
   const TownList& tl = campData->getTowns();
   const ConnectionList& cl = campData->getConnections();


   CampaignSupplyLine supplyLine;
   supplyLine.calculate(campData, SIDE_Neutral, true);

   /*
    * Any location that in in supply, but adjacent to one that is out of supply
    * gets marked as a good location
    */

   for(int townCount = 0; townCount < entries(); ++townCount)
   {
      AIC_TownInfo& info = d_items[townCount];
      ITown iTown = info.d_town;

      if(supplyLine[iTown] != CampaignSupplyLine::Empty)
      {
         const Town& town = tl[iTown];

         for(int conCount = 0; conCount < MaxConnections; ++conCount)
         {
            IConnection ci = town.getConnection(conCount);
            if(ci == NoConnection)
               break;

            const Connection& con = cl[ci];
            ITown nextTown = con.getOtherTown(iTown);

            if (supplyLine[nextTown] == CampaignSupplyLine::Empty)
            {
#ifdef DEBUG
//               d_sData->logWin("NeedDepot at %s", town.getName());
#endif


               info.d_needSupplyDepot = true;
               break;
            }
         }
      }
   }
}


AIC_TownInfo& AIC_TownList::find(ITown town)
{
   for(ITown i = 0; i < entries(); i++)
   {
      AIC_TownInfo& info = d_items[i];
      if(info.d_town == town)
         return info;
   }
   throw GeneralError("Town %d Not in AIC_TownList", (int) town);
}

const AIC_TownInfo& AIC_TownList::find(ITown town) const
{
   for(ITown i = 0; i < entries(); i++)
   {
      const AIC_TownInfo& info = d_items[i];
      if(info.d_town == town)
         return info;
   }
   throw GeneralError("Town %d Not in AIC_TownList", (int) town);
}


class AIC_TownInfoCompare
{
 public:
   AIC_TownInfoCompare() { }

   int operator()(const AIC_TownInfo& t1, const AIC_TownInfo& t2) const
   {
      return (t1.importance() > t2.importance());
   }
};

#if 0
void AIC_TownList::sort()
{
   Writer writeLock(this);
   ::sort(d_items.begin(), d_items.end(), AIC_TownInfoCompare());
}
#endif

/*
 * Pick a random town using it's priority as a probability
 */

const AIC_TownInfo& AIC_TownList::pick(unsigned int value)
{
   // int value = r(d_totalPickPriority);
   ASSERT(value < d_totalPickPriority);

   unsigned int cValue = 0;

   for(ITown i = 0; i < entries(); i++)
   {
      AIC_TownInfo& info = d_items[i];
      cValue += info.priority();
      if(value <= cValue)
      {
         setPriority(info, 0);
         return info;
      }
   }

   throw GeneralError("AIC_TownList::pick did not find anything, value=%d, total=%d",
      (int) value, (int) d_totalPickPriority);

}

void AIC_TownList::unpick(ITown itown)
{
   ASSERT(itown != NoTown);
   if(itown != NoTown)
      setPriority(d_items[itown], d_items[itown].importance());

      // d_totalPickPriority += d_items[itown].unPick();
}

unsigned int AIC_TownList::maxPriority() const
{
   unsigned int maxValue = 0;
   for (Container::const_iterator it = d_items.begin();
         it != d_items.end();
         ++it)
   {
      if(it->priority() > maxValue)
         maxValue = it->priority();
   }

   return maxValue;
}

#ifdef DEBUG
void AIC_TownList::logTowns()
{
   Reader readLock(this);

   /*
    * Make a copy of the towns and sort them
    */

   Container sortedItems = d_items;
   std::sort(sortedItems.begin(), sortedItems.end(), AIC_TownInfoCompare());


   const TownList& towns = d_sData->campData()->getTowns();

   // Display info to the log file

//    static const char header[] = "-----------------:--+----+-----+-----+-----+---+-----+";
//    static const char titles[] = "             Name|Lk|Mile|  res|  con|conVl|Pri|%    |";
//    static const char fmt[] = "%17s,%2d,%4d,%5d,%5.2f,%5.2f,%3d,%-5.2f%%";
   static const char header[] = "-----------------:--|-----|------|--+----+-----+-----+-----+-----+-----|";
   static const char titles[] = "             Name|Sd|Prity|Imprt|     %|Lk|Mile|  res|  con|conVl|depot|";
   static const char fmt[] = "%17s|%2s|%5d|%5d|%5.2f%%|%2d|%4d|%5d|%5.2f|%5.2f|%5s";
//   static const char header[] = "-----------------:--+----+-----+-----+-----+------+---+-----+";
//   static const char titles[] = "             Name Lk Mile   res   con conVl Threat Pri %";
//   static const char fmt[] = "%17s %2d %4d %5d %5.2f %5.2f %6.2f %3d %-5.2f%%";


   d_sData->logWin("%s", header);
   d_sData->logWin("%s", titles);
   d_sData->logWin("%s", header);

   for(ITown i = 0; i < sortedItems.size(); i++)
   {
      const AIC_TownInfo& info = sortedItems[i];

      float percent = static_cast<float>((static_cast<float>(info.importance()) * 100.0) / static_cast<float>(d_totalImportance));

      const int SideBufLen = 2;
      char sideBuf[SideBufLen+1];
      strncpy(sideBuf, scenario->getSideName(towns[info.d_town].getSide()), SideBufLen);
      sideBuf[SideBufLen] = 0;

      d_sData->logWin(fmt,
         (const char*) towns[info.d_town].getNameNotNull(),
         (const char*) sideBuf,
         (int) info.importance(),
         (int) info.priority(),
         (float) percent,
         (int) info.d_enemyTownLinks,
         (int) DistanceToMile(info.d_enemyDistance),
         (int) info.d_resValue,
         (float) d_connectivity.get(info.d_town),
         (float) info.d_conValue,
         (info.d_needSupplyDepot ? "yes" : "no")
      );
         // (float) info.d_threat,
   }
   d_sData->logWin("%s", header);
}

void AIC_TownList::showTown(DrawDIBDC* dib, ITown itown, const PixelPoint& p)
{
   Reader readLock(this);

   const AIC_TownInfo& info = find(itown);
   AIC_TownConnectivity::Type connectivity = d_connectivity.get(itown);

   DCsave dcSave;
   dcSave.save(dib->getDC());

   const int Spacing = 6;

   Font font;
   font.set(8, Spacing);
   dib->setFont(font.getHandle());
   dib->setBkMode(OPAQUE);
   dib->setTextColor(RGB(0,0,0));
   dib->setTextAlign(TA_BOTTOM | TA_CENTER);
   SetBkColor(dib->getDC(), RGB(192,255,192));

   int y = p.getY() - Spacing;

   char buffer[40];

   wsprintf(buffer, "%d,%d,%d",
      (int) info.enemyTownLinks(),
      (int) DistanceToMile(info.enemyDistance()),
      (int) info.priority());

   wTextOut(dib->getDC(), p.getX(), y, buffer);
   y -= Spacing;

   sprintf(buffer, "%1.2f", (float) connectivity);
   wTextOut(dib->getDC(), p.getX(), y, buffer);
   y -= Spacing;

   dcSave.restore(dib->getDC());
}

#endif   // DEBUG


#ifdef AIC_PROVINCES

/*---------------------------------------------------------------------
 * Province Info
 */

AIC_ProvinceInfo::AIC_ProvinceInfo()
{
   d_province = NoProvince;
   d_priority = 0;
   d_strength = 0;
}

AIC_ProvinceInfoList::AIC_ProvinceInfoList(CampaignData* campData) :
   d_totalPriority(0)
{
   const ProvinceList& provList = campData->getProvinces();
   init(provList.entries());
}

void AIC_ProvinceInfoList::make()
{
   const ProvinceList& provList = campData->getProvinces();

#if 0
   if(entries() != provList.entries())
      init(provList.entries());
#endif
   ASSERT(entries() == provList.entries());


   /*
    * Clear out the information
    */

   for(IProvince p = 0; p < entries(); p++)
   {
      AIC_ProvinceInfo& info = d_items[p];
      info.d_province = p;
      info.d_priority = 0;
      info.d_strength = 0;
   }

   d_totalPriority = 0;

   /*
    * Step through towns, adding onto province values
    */

   for(ITown t = 0; t < townInfo.entries(); t++)
   {
      const AIC_TownInfo& tInfo = townInfo[t];
      const Town& town = campData->getTown(tinfo.d_town);
      IProvince iProv = town.getProvince();
      AIC_ProvinceInfo& pInfo = d_items[iProv];

      pInfo.d_priority += tinfo.d_priority;
      pInfo.d_strength += tinfo.d_strength;

      // Also, check for minimum connections, etc...


      d_totalPriority += tinfo.d_priority;
   }
#ifdef DEBUG
   log(provList);
#endif
}
/*
 * This is identical to the TownInfoList::pick
 * should consider using a base class or template
 */

const AIC_ProvinceInfo& AIC_ProvinceInfoList::pick(RandomNumber& r) const
{
   int value = r(d_totalPriority);
   int cValue = 0;

   for(int i = 0; i < entries(); i++)
   {
      const AIC_ProvinceInfo& info = d_items[i];
      cValue += info.d_priority;
      if(value <= cValue)
         return info;
   }

   throw GeneralError("AIC_ProvinceInfoList::pick did not find anything, value=%d, total=%d",
      (int) value, (int) d_totalPriority);
}

const AIC_ProvinceInfo& AIC_ProvinceInfoList::find(IProvince prov) const
{
   // We could probably replace this with a simple array look up
   // seeing as the province list is never sorted

   for(int i = 0; i < entries(); i++)
   {
      const AIC_ProvinceInfo& info = d_items[i];
      if(info.d_province == prov)
         return info;
   }
   throw GeneralError("Province %d Not in AIC_ProvinceList", (int) prov);
}

#ifdef DEBUG

void AIC_ProvinceInfoList::log(const ProvinceList& provList)
{
   aiLog.lock();
   aiLog.printf("Provinces");
   aiLog.printf("---------------------:-----+------+----+");
   aiLog.printf("                Name :  SP : Prio | %% |");
   aiLog.printf("---------------------:-----+------+----+");
   for(int i = 0; i < entries(); i++)
   {
      const AIC_ProvinceInfo& info = d_items[i];
      aiLog.printf("%20s : %3d : %4d | %d%%",
         provList[info.d_province].getName(),
         info.d_strength,
         info.d_priority,
         (int) ((info.d_priority * 100) / d_totalPriority));
   }
   aiLog.printf("---------------------:-----+------+----+");
   aiLog.unLock();
}

#endif
#endif   // AIC_PROVINCES



//lint -restore

/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.2  2001/06/18 08:25:44  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
