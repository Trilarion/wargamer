/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef AIC_OBJECTIVE_HPP
#define AIC_OBJECTIVE_HPP

#ifndef __cplusplus
#error aic_OBJECTIVE.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *	Campaign AI Objectives
 *
 *----------------------------------------------------------------------
 */

#include "gamedefs.hpp"
#include "aic_unit.hpp"
#include <list>

class AIC_StrategyData;

class AIC_IssueOrders;
class AIC_ChooseObjective;

class AIC_Town;
class RandomNumber;

#ifdef DEBUG
class ObjectiveDisplay;
class IMapWindow;
#endif
/*
 * A single Objective
 */

class AIC_Objective // : public AIC_Codelet
{
   public:
      AIC_Objective();
      AIC_Objective(ITown itown);
      ~AIC_Objective();

      bool readData(FileReader& f, const Armies* army);
      bool writeData(FileWriter& f) const;

      ITown town() const { return d_town; }
      unsigned int priority() const { return d_priority; }
      void priority(unsigned int p) { d_priority = p; }
      const AIC_CPList& unitList() const { return d_units; }
      AIC_CPList& unitList() { return d_units; }      //lint !e1536 ... exposing low level member

      void addUnit(ConstParamCP cp);
      void removeUnit(ConstParamCP cp);
      SPCount spNeeded() const { return d_spNeeded; }
      void spNeeded(SPCount count) { d_spNeeded = count; }
      SPCount spAllocated() const { return d_units.spCount(); }

   private:
      ITown d_town;
      unsigned int d_priority;
      AIC_CPList d_units;

      SPCount d_spNeeded;

      static UWORD s_fileVersion;

};

// Functions to make STL work better
// Assumes that there is only one objective per town

inline bool operator == (const AIC_Objective& lhs, const AIC_Objective& rhs)
{
   return lhs.town() == rhs.town();
}

inline bool operator < (const AIC_Objective& lhs, const AIC_Objective& rhs)
{
   return lhs.town() < rhs.town();
}



/*
 * Container of Objectives
 */

class AIC_ObjectiveList : public RWLock
{
      typedef std::list<AIC_Objective> Container;
   public:
      AIC_ObjectiveList(AIC_UnitList* unitList, unsigned int nTowns = 0);
      ~AIC_ObjectiveList();

      bool readData(FileReader& f, const Armies* army);
      bool writeData(FileWriter& f) const;

      AIC_Objective* find(ITown itown);
      AIC_Objective* addOrUpdate(ITown itown, unsigned int priority);
      AIC_Objective* pick(unsigned int r);

      unsigned int priority() const { return d_totalPriority; }
      bool remove(ITown itown);
      void remove(ConstParamCP cp);

      // Iterator stuff.. used by debug window

      typedef Container::iterator iterator;
      typedef Container::const_iterator const_iterator;
      iterator begin() { return d_items.begin(); }
      iterator end() { return d_items.end(); }
      const_iterator begin() const { return d_items.begin(); }
      const_iterator end() const { return d_items.end(); }
      unsigned int size() const { return d_items.size(); }
      AIC_Objective& front() { return d_items.front(); }
      const AIC_Objective& front() const { return d_items.front(); }

   private:
      iterator add(ITown itown);
      iterator get(ITown itown);
      const_iterator get(ITown itown) const;
      bool isMember(ITown itown) const;

      Container d_items;
      unsigned int d_totalPriority;
      AIC_UnitList* d_unitList;     // where the CPs come from

      static UWORD s_fileVersion;

};

#endif /* AIC_OBJECTIVE_HPP */
