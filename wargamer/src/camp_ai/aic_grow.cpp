/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
#error "Obsolete"
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 * Campaign AI: Expand Activity
 *
 * This is quite complex
 * A list of towns that border on the activity is created
 *
 * A change of happiness is calculated for each one, which is calculated
 * from a number of values including:
 *    - Distance from centre of activity
 *    - Connectiveness to activity
 *    - On same side as rest of activity's towns
 *    - Priority and Depth of the activity
 *    - How much the activity wants to split or expand
 *
 * An actual town is chosen randomly using the happiness differences
 * as a bias.  Note that towns that have a -ve happiness change can
 * still be picked, but they are less likely.
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "aic_grow.hpp"
#include "aic_sdat.hpp"
#include "aic_act.hpp"
// #include "aic_data.hpp"
#include "aic_wld.hpp"
#include "aic_util.hpp"
#include "campdint.hpp"
#include "town.hpp"

/*
 * Structure for a town to assist us in working out what is connected
 *
 * There will be one of these for each town
 * If we were worried about memory rather than speed, then this would
 * be stored in some kind of a list whereby only towns that are in
 * an activity or neighbouring are stored.  However since there are
 * only a few hundred towns, we create one for every town in the world
 * so that we access it instantly.
 */

struct AIC_TownGrowInfo
{
   Boolean inActivity;     // Set if town is in the destination activity
   int connected;          // Count of how many of the town's activities it connects to
   int deltaHappiness;     // How it's happiness changes if moved to new event
   int newHappiness;       // New happiness value if moved
   bool picked;

   AIC_TownGrowInfo();
};

AIC_TownGrowInfo::AIC_TownGrowInfo()
{
   inActivity = False;
   connected = 0;
   deltaHappiness = 0;
   newHappiness = 0;
   picked = false;
}

/*
 * Manager of the array of TownGrowInfo structures
 */

class AIC_TownGrowList
{
   Array<AIC_TownGrowInfo> d_growInfo;    // Pointer to array of info

   AIC_StrategyData* d_sData;             // StrategyData to avoid having to pass lots of parameters around
   AIC_Activity* d_activity;              // New activity being considered
   const TownList& d_townList;            // World's town list
   const ConnectionList& d_connectList;   // World's connection list

   int d_nConnected;
   int d_totalHappiness;

 public:
   AIC_TownGrowList(AIC_StrategyData* sData, AIC_Activity* activity);
   ~AIC_TownGrowList();

   void add(ITown itown);
      // mark town as being in list, and calculate neighbours

   void calculateValues();

   ITown pick();
      // Pick a town based on deltaHappiness
};


/*
 * Construct grow list
 */

AIC_TownGrowList::AIC_TownGrowList(AIC_StrategyData* sData, AIC_Activity* activity) :
   d_sData(sData),
   d_activity(activity),
   d_townList(sData->campData()->getTowns()),
   d_connectList(sData->campData()->getConnections()),
   d_nConnected(0),
   d_totalHappiness(0)
{
   ASSERT(sData != 0);
   d_growInfo.init(d_townList.entries());
}

/*
 * destruct grow list
 */

AIC_TownGrowList::~AIC_TownGrowList()
{
}

// mark town as being in list, and calculate neighbours

void AIC_TownGrowList::add(ITown itown)
{
   AIC_TownGrowInfo& info = d_growInfo[itown];
   info.inActivity = True;

   /*
    * Step through connections
    */

   const Town& town = d_townList[itown];
   for(int i = 0; i < MaxConnections; i++)
   {
      IConnection iCon = town.getConnection(i);

      if(iCon != NoConnection)
      {
         const Connection& con = d_connectList[iCon];
         ITown otherTown = con.getOtherTown(itown);

         AIC_TownGrowInfo& otherInfo = d_growInfo[otherTown];

         otherInfo.connected++;
      }
      else
         break;
   }
}

void AIC_TownGrowList::calculateValues()
{
   /*
    * Work out how many are connected
    * work out happinesses
    * work out minimum happiness
    * adjust happiness using minimum happiness
    * pick random using bias
    */

   d_nConnected = 0;    // How many neighbouring towns are there?
   d_totalHappiness = 0;   // Total happiness

   for(ITown i = 0; i < d_growInfo.entries(); i++)
   {
      AIC_TownGrowInfo& info = d_growInfo[i];

      if(!info.inActivity && (info.connected != 0))
      {
         // This calculation will need to be moved somewhere else
         // and made more complicated
         // It should be a measure of how attached it is to an activity
         // and not so dependant on the priorities activity

         // I have simplified things, by disallowing -ve changes of happiness

         info.newHappiness = d_activity->priority() * info.connected;

         const AIC_TownInfo& townInfo = d_sData->worldInfo()->getTown(i);

#ifdef DEBUG
         d_sData->logWin("Considering expanding with %s (%d->%d)",
            d_sData->campData()->getTownName(i),
            (int) townInfo.happiness,
            (int) info.newHappiness);
#endif

         int deltaH = info.newHappiness - townInfo.happiness + 1;

         if(deltaH <= 0)
            info.deltaHappiness = 0;
         else
         {
            d_nConnected++;
            info.deltaHappiness += deltaH;
            d_totalHappiness += deltaH;
         }
      }
   }
}

// Pick a town based on deltaHappiness

ITown AIC_TownGrowList::pick()
{

   if(d_nConnected != 0)
   {
      int r = d_sData->rand(d_totalHappiness);

      for(int i = 0; i < d_growInfo.entries(); i++)
      {
         AIC_TownGrowInfo& info = d_growInfo[i];

         if(r < info.deltaHappiness)
         {
            if(info.picked)
               return NoTown;

            // We've got one!!!!

            AIC_TownInfo& townInfo = d_sData->worldInfo()->getTown(i);
            townInfo.happiness = info.newHappiness;

            /*
             * Towns keep being picked until same one chosen twice
             */

            info.picked = true;

#ifdef DEBUG
            d_sData->logWin("%s grown to include %s",
               d_activity->description(),
               d_sData->campData()->getTownName(i));
#endif

            return i;
         }
         else
            r -= info.deltaHappiness;
      }
   }

   return NoTown;
}


/*
 * Attempt to expand activity to include more towns
 * Return True if new towns were added
 */

static Boolean AIC_Expand::expandActivity(AIC_StrategyData* sData, AIC_Activity* activity)
{
#ifdef DEBUG
   sData->logWin("expand %s", activity->description());
#endif   

   AIC_TownGrowList growList(sData, activity);

   /*
    * This must iterate into the children...
    */

   AIC_ActivityIter iter(activity);
   while(++iter)
   {
      AIC_Activity* child = iter.current();

      AIC_ActivityTownIter townIter(child->towns());
      while(++townIter)
      {
         ITown itown = townIter.current();
         growList.add(itown);
      }
   }

#if 0
   // Get down to bottom

   AIC_Activity* child = activity;
   while(child->childCount())
   {
      child = child->children()->first();
   }

   do
   {
      AIC_ActivityTownIter townIter(child->towns());
      while(++townIter)
      {
         ITown itown = townIter.current();
         growList.add(itown);
      }

      AIC_Activity* parent = child->parent();

      if(parent && (child != activity))
      {
         if(parent->children()->next(child))
         {
            child = parent->children()->next(child);
         }
         else
         {
            child = parent;
         }
      }
   } while(child != activity);
#endif

   growList.calculateValues();

   bool picked = false;

   for(;;)
   {
      ITown itown = growList.pick();
      if(itown == NoTown)
         break;
      else
      {
         AIC_ActivityUtility util(sData);
         util.moveTown(activity, itown);
         picked = true;
      }
   }

   return picked;

}


/*
 *----------------------------------------------------------------------
 * $Log$
 *----------------------------------------------------------------------
 */
