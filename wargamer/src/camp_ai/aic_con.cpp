/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 * Campaign AI : Connectivity of the route network
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "aic_con.hpp"
#include "town.hpp"
#include "campdint.hpp"
#include "ailog.hpp"

struct TownConnectNode {
   ITown next;             // Next node if on stack

   UWORD townNodeCount;    // node Count used when doing town influence
   AIC_TownConnectivity::Type sumInfluence;        // Sum of distances from other towns

   Boolean inQueue;        // Set if in queue
   Boolean visited;        // Clear if not been visited yet

 public:
   TownConnectNode();
};

class TownConnectCalc {
   TownConnectNode* nodes;
   int nodeCount;

   ITown usedList;         // Entry point to queue of towns

public:
   TownConnectCalc();
   ~TownConnectCalc();

   void addTownInfluence(const CampaignData* campData, ITown t);

   const TownConnectNode* getNodes() const { return nodes; }

private:
   void addNode(ITown t);
   ITown getNode();
   void makeNodes(int howMany);
};


/*=================================================================
 * Calculate Connectivity
 */

TownConnectNode::TownConnectNode()
{
   next = NoTown;
   townNodeCount = 0;
   sumInfluence = 0;
   inQueue = False;
   visited = False;
}

TownConnectCalc::TownConnectCalc() :
   nodes(0),
   nodeCount(0),
   usedList(NoTown)
{
}

TownConnectCalc::~TownConnectCalc()
{
   delete[] nodes;
}

void TownConnectCalc::addNode(ITown t)
{
   ASSERT(nodes);

   if(!nodes[t].inQueue)
   {
      nodes[t].inQueue = True;
      nodes[t].visited = True;

      nodes[t].next = usedList;
      usedList = t;
   }
}

ITown TownConnectCalc::getNode()
{
   ASSERT(nodes);

   ITown t = usedList;

   if(usedList != NoTown)
   {
      ASSERT(nodes[t].inQueue);

      usedList = nodes[usedList].next;
      nodes[t].inQueue = False;
      nodes[t].next = NoTown;    // Unneccesary, but keeps it tidy
   }

   return t;
}


void TownConnectCalc::makeNodes(int howMany)
{
   if(nodes == 0)
   {
      /*
       * Create Nodes
       */

      nodeCount = howMany;
      ASSERT(nodeCount > 0);
      nodes = new TownConnectNode[nodeCount];
      ASSERT(nodes != 0);
   }

   ASSERT(howMany == nodeCount);
}

/*
 * Add on influence to other towns from given town
 */

void TownConnectCalc::addTownInfluence(const CampaignData* campData, ITown t)
{
   const TownList& tl = campData->getTowns();
   const ConnectionList& cl = campData->getConnections();

   makeNodes(tl.entries());

   ASSERT(nodes != 0);


   for(int i = 0; i < nodeCount; i++)
   {
      nodes[i].visited = False;
      nodes[i].next = NoTown;
      nodes[i].townNodeCount = 0;
   }
   usedList = NoTown;

   addNode(t);

   while(usedList != NoTown)
   {
      ITown n = getNode();
      ASSERT(n != NoTown);
      const Town& town = tl[n];
      TownConnectNode& rn = nodes[n];

      if(!town.isOffScreen())
      {
         UWORD dist = UWORD(rn.townNodeCount + 1);

         for(int i = 0; i < MaxConnections; i++)
         {
            IConnection ci = town.getConnection(i);
            if(ci == NoConnection)
               break;

            const Connection& con = cl[ci];

            if(!con.offScreen)
            {
               ITown nextTown = con.getOtherTown(n);

               static float qualityVal[CQ_Max][CT_Max] =
               {
                  // Road,    Rail,    River
                  { 1.0/3, 2.0/3,   1.5/3 },
                  { 2.0/3, 4.0/3,   3.0/3 },
                  { 1.0,   2.0,     1.5   }
               };

               float conValue = qualityVal[con.quality][con.how] * con.capacity / MaxConnectCapacity;
               conValue = conValue / dist;

               if(!nodes[nextTown].visited)
               {
                  nodes[nextTown].sumInfluence += conValue;
                  nodes[nextTown].townNodeCount = dist;

                  addNode(nextTown);
               }
            }
         }
      }
   }
}

/*
 * Calculate Connectivities
 */

// const AIC_TownConnectivity::Type AIC_TownConnectivity::TypeMax = FLT_MAX;

void AIC_TownConnectivity::make(const CampaignData* campData)
{
   const TownList& towns = campData->getTowns();
   // ITown townCount = towns.entries();
   unsigned int townCount = towns.entries();

   if(d_values.entries() != townCount)
      d_values.init(townCount);

   d_range.clear();

   TownConnectCalc calc;

   ITown i;
   for(i = 0; i < townCount; i++)
      calc.addTownInfluence(campData, i);

   const TownConnectNode* nodes = calc.getNodes();
   for(i = 0; i < townCount; i++)
   {
      d_values[i] = nodes[i].sumInfluence;

#if 0
      if(d_values[i] > d_maxValue)
         d_maxValue = d_values[i];
      if(d_values[i] < d_minValue)
         d_minValue = d_values[i];
#endif
      d_range += d_values[i];
   }


#ifdef DEBUG
   aiLog.printf("Minimum connectivity = %f", (float) d_range.minValue());
   aiLog.printf("Maximum connectivity = %f", (float) d_range.maxValue());
#endif
}



/*
 *----------------------------------------------------------------------
 * $Log$
 *----------------------------------------------------------------------
 */
