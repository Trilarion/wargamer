/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef CODELET_HPP
#define CODELET_HPP

#ifndef __cplusplus
#error codelet.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *	Campaign AI Codelet base class
 *
 *----------------------------------------------------------------------
 */

#include <vector>
#include "myassert.hpp"
#include "measure.hpp"     // For TimeTick

#ifdef DEBUG
// #include <string.hpp>
class LogWindow;
#endif

class AIC_StrategyData;


class AIC_Codelet
{
		friend class AIC_CodeletList;

	public:
		AIC_Codelet() : d_priority(0), d_parent(0) { }
		virtual ~AIC_Codelet() = 0;

		unsigned int priority() const { return d_priority; }

		void priority(unsigned int p);
		unsigned int addPriority(int  p);     // Add or Subtract
		unsigned int subPriority(unsigned int  p);

      virtual void init() = 0;
		virtual void run(unsigned int rand) = 0;     // Process this action
      virtual void timeUpdate(TimeTick t) = 0;     // Note that gametime has passed

#ifdef DEBUG
      virtual void log(LogWindow* log) const;
      virtual String name() const = 0;
#endif

		void parent(AIC_Codelet* parent) { d_parent = parent; }
		AIC_Codelet* parent() const { return d_parent; }

	private:

		unsigned int d_priority;
		AIC_Codelet* d_parent;
};

inline AIC_Codelet::~AIC_Codelet() { d_parent = 0; }


#endif /* CODELET_HPP */

