/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 * AI Utility functions
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "aic_util.hpp"
// #include "myassert.hpp"
#include "aic_sdat.hpp"
// #include "aic_org.hpp"
// #include "aic_act.hpp"
#include "campdint.hpp"
#include "resinfo.hpp"

#if 0
#if 0
AIC_Activity* AIC_ActivityUtility::createActivity(AIC_Activity* parent)
{
   return d_sData->activities()->create(parent);
}


/*
 * Kill Activity and its children
 */

void AIC_ActivityUtility::killActivity(AIC_Activity* activity)
{
   ASSERT(activity != 0);

#ifdef DEBUG
   d_sData->logWin("Killing %s", (const char*) activity->description());
#endif


   while(activity->childCount())
   {
      AIC_Activity* child = activity->children()->first();
      killActivity(child);
   }

   /*
    * Move Towns to parent
    */

   AIC_Activity* parent = activity->parent();

   AIC_Activity::ActivityTownList* townList = activity->towns();

   if(parent)
   {
      while(townList->entries())
      {
         moveTown(parent, (*townList)[0]);
      }
   }
   else
      townList->reset();

   /*
    * Move units to parent
    */

   AIC_Activity::ActivityUnitList* unitList = activity->units();

   if(parent)
   {
      while(unitList->entries())
      {
         AIC_UnitAllocator::moveUnit(d_sData->unitList(), parent, (*unitList)[0]);
      }
   }
   else
      unitList->reset();

   d_sData->activities()->kill(activity);
}

void AIC_ActivityUtility::killAllActivities()
{
   killActivity(d_sData->activities()->top());
}

/*
 * Update activities ages
 */


void AIC_ActivityUtility::ageActivities()
{
   AIC_ActivityIter activity(topActivity());
   while(++activity)
   {
      if(activity->parent())     // Never fizzle top activity
      {
         activity->incAge();
         activity->addPriority(AIC_Activity::TDD_Fizzle, activity->age());

         // reduce happiness if not enough SPs are allocated

         if(activity->spAllocated() < activity->spNeeded())
         {
            activity->addHappiness( ((activity->spAllocated() * 100) / activity->spNeeded()) - 100);
         }
      }

   }
}


AIC_Activity* AIC_ActivityUtility::topActivity()
{
   return d_sData->activities()->top();
}
#endif

void AIC_ActivityUtility::eventInTown(ITown itown)
{
#if 0
   AIC_TownInfo* townInfo = &d_sData->worldInfo()->getTown(itown);
   ASSERT(townInfo != 0);
   AIC_Activity* activity = townInfo->activity;
   ASSERT(activity != 0);
   activity->resetAge();
#endif
}

#if 0
/*
 * Move a town to an activity
 */

void AIC_ActivityUtility::moveTown(AIC_Activity* activity, ITown itown)
{
   ASSERT(activity != 0);
   ASSERT(itown != NoTown);

   AIC_TownInfo* townInfo = &d_sData->worldInfo()->getTown(itown);
   ASSERT(townInfo != 0);

   /*
    * Remove from old activity
    */

   AIC_Activity* oldActivity = townInfo->activity;

   if(oldActivity)
   {
      townInfo->activity = 0;
      oldActivity->removeTown(itown);
      oldActivity->addImportance(-townInfo->priority);
      oldActivity->subSPNeeded(townInfo->strength);

      oldActivity->addPriority(AIC_Activity::TDD_Update, townInfo->priority);
      // oldActivity->addPriority(AIC_Activity::TDD_Expand, townInfo->priority);
      // oldActivity->lowerPriority(AIC_Activity::TDD_Split);

      /*
       * Set up the old activity to consider re-organizing itself
       * e.g, remove unconnected nodes, delete itself if nothing left.
       *
       * Maybe better to just go ahead and do it, apart from it ought really
       * to be done at a higher level.
       */

      oldActivity->addPriority(AIC_Activity::TDD_OrganizeTowns, townInfo->priority);


#ifdef DEBUG
      d_sData->logWin("Removed %s from %s (%d towns)",
         (const char*) d_sData->campData()->getTownName(itown),
         (const char*) oldActivity->description(),
         (int) oldActivity->towns()->entries());
#endif
   }

   /*
    * Add to new activity
    */

   activity->addTown(itown);
   activity->addImportance(townInfo->priority);
   activity->addSPNeeded(townInfo->strength);
   activity->addHappiness(townInfo->priority);

   activity->addPriority(AIC_Activity::TDD_Update, townInfo->priority);
   // activity->addPriority(AIC_Activity::TDD_Split, townInfo->priority);
   activity->addPriority(AIC_Activity::TDD_AssignSP, townInfo->priority);
   // activity->lowerPriority(AIC_Activity::TDD_Expand);

   townInfo->activity = activity;

#ifdef DEBUG
   if(oldActivity || activity->parent())  // Prevent log during initial setup
   {
      d_sData->logWin("Added %s to %s (%d towns)",
         (const char*) d_sData->campData()->getTownName(itown),
         (const char*) activity->description(),
         (int) activity->towns()->entries());
   }
#endif
}
#endif


#if 0
#ifdef DEBUG

void AIC_ActivityUtility::log()
{
   logWin("-----------------------");
   logWin("Current activities:");

   AIC_ActivityIter iter(d_sData->topActivity(), True);     // Top down iterator
   while(++iter)
   {
      AIC_Activity* activity = iter.current();

      char buffer[80];

      logWin("%-15s: %3d towns, imp=%d, spNeeded=%3d, spAlloc=%3d, p=%s, totalp=%5d, age=%d, happiness=%d",
         (const char*) activity->description(),
         (int) activity->towns()->entries(),
         (int) activity->importance(),
         (int) activity->spNeeded(),
         (int) activity->spAllocated(),
         (const char*) activity->priorityStr(buffer),
         (int) activity->totalPriority(),
         (int) activity->age(),
         (int) activity->happiness());
   }
   logWin("-----------------------");
}

#endif
#endif



/*----------------------------------------------------------------
 * Functions to rewrite/update
 */

#if 0
/*
 *  Add up SPneeded for all towns in list
 */

SPCount AIC_ActivityUtility::addTownSP(const AIC_ActivityTownList* towns) const
{
   SPCount total = 0;

   const AIC_WorldInfo* world = d_sData->worldInfo();

   AIC_ActivityTownIter iter(towns);
   while(++iter)
   {
      ITown itown = iter.current();
      const AIC_TownInfo& townInfo = world->getTown(itown);
      total += townInfo.strength;
   }

   return total;
}
#endif

#if 0
/*
 * Update activities need for Strength Points
 *
 * This updates the priority of the takeSP and assignSP actions
 *
 * This should probably use some calculation to work out the ratio
 * of each actions priority.
 *
 * Maybe, if takeSP fails, it automatically increases the assignSP
 * priority
 */

void AIC_StrategyData::requestSP(AIC_Activity* activity, SPCount needed, int importance)
{
   // activity->addPriority(AIC_Activity::TDD_TakeSP, importance);
   activity->addPriority(AIC_Activity::TDD_AssignSP, importance);

   activity->addTakeNeeded(needed, importance);

#ifdef DEBUG
   logWin("Increasing %s's need for units by %d@%d",
         (const char*) activity->description(),
         (int) needed,
         (int) importance);
#endif
}

#endif

#endif



/*
 * Alter AI variables
 */

static const float MaxCaution = 5.0;
static const float MinCaution = 0.2;
static const float MaxAgression = 5.0;
static const float MinAgression = 0.2;
static const float CautionIncrease = 1.1;
static const float CautionDecrease = 1/CautionIncrease;
static const float AgressionIncrease = 1.1;
static const float AgressionDecrease = 1/AgressionIncrease;

void AIC_Utility::increaseCaution()
{
   float caution = d_sData->caution();
   caution = caution * CautionIncrease;
   if(caution > MaxCaution)
      caution = MaxCaution;
   else if(caution < MinCaution)
      caution = MinCaution;
   d_sData->caution(caution);

#ifdef DEBUG
   d_sData->logWin("Caution increased to %f", (double) d_sData->caution());
#endif
}

void AIC_Utility::decreaseCaution()
{
   float caution = d_sData->caution();
   caution = caution * CautionDecrease;
   if(caution > MaxCaution)
      caution = MaxCaution;
   else if(caution < MinCaution)
      caution = MinCaution;
   d_sData->caution(caution);

#ifdef DEBUG
   d_sData->logWin("Caution decreased to %f", (double) d_sData->caution());
#endif
}

void AIC_Utility::increaseAgression()
{
   float agression = d_sData->agression();
   agression = agression * AgressionIncrease;
   if(agression > MaxAgression)
      agression = MaxAgression;
   else if(agression < MinAgression)
      agression = MinAgression;
   d_sData->agression(agression);

#ifdef DEBUG
   d_sData->logWin("Agression increased to %f", (double) d_sData->agression());
#endif
}

void AIC_Utility::decreaseAgression()
{
   float agression = d_sData->agression();
   agression = agression * AgressionDecrease;
   if(agression > MaxAgression)
      agression = MaxAgression;
   else if(agression < MinAgression)
      agression = MinAgression;
   d_sData->agression(agression);
#ifdef DEBUG
   d_sData->logWin("Agression decreased to %f", (double) d_sData->agression());
#endif
}

#if 0
void AIC_Utility::increaseResourcePriority(ITown town)
{
   ASSERT(town != NoTown);

   if(town != NoTown)
   {
      AIC_ResourceInformation* resInfo = d_sData->resInfo();

      AIC_WorldInfo* world = d_sData->worldInfo();
      const AIC_TownInfo& townInfo = world->getTown(town);

      resInfo->addPriority(townInfo.priority());
   }
}
#endif


/*
 *----------------------------------------------------------------------
 * $Log$
 *----------------------------------------------------------------------
 */
