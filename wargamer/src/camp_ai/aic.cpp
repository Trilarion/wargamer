/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 * Campaign AI : Overall Control
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "aic.hpp"
#include "aic_stra.hpp"
#include "sllist.hpp"
#include "thread.hpp"
#include "campevt.hpp"
#include "simpstr.hpp"
#include "campdint.hpp"
#include "ailog.hpp"
#include "gthread.hpp"
#include "ai_panel.hpp"
#include "scenario.hpp"
#include "campmsg.hpp"
#include "filecnk.hpp"
#include "control.hpp"


using Greenius_System::Thread;
/*-----------------------------------------------------------
 * CampaignSideAI class:
 *
 *  There is one of these for each side that the AI
 *  controls.  Each runs in its own thread.
 *  It registers itself with the campaign's event list so
 *  that an event gets set everytime some gametime is
 *  processed.  This allows the AI to think for longer if
 *  time is paused, and issue orders at an appropriate time.
 *  AIC_Strategy holds all of the AI's processing events and
 *  data.
 *---------------------------------------------------------*/

class CampaignSideAI:
   public SLink,
   public Thread,
   public AI_ControlInterface      // AI_Panel communicates via this interface
{
   /*----------------------------------------
    * Public Functions
    */

   public:
      CampaignSideAI(Side s, const CampaignData* campData, EventList* eventList);
      ~CampaignSideAI();

      bool readData(FileReader& f);
      bool writeData(FileWriter& f) const;

      void sendMessage(const CampaignMessageInfo& msg);
         // Interpret report from a leader about some event

      Side side() const { return d_strategy.side(); }

      #ifdef DEBUG

         void showTown(DrawDIBDC* dib, ITown itown, const PixelPoint& p)
         {
            d_strategy.showTown(dib, itown, p);
         }

         // Over-ride Thread::start() so the control panel can be initialised
         //lint -e(1511)
         void start()
         {
            Thread::start();
            d_panel.init(this);
         }

         void drawAI(const IMapWindow& mw)
         {
            d_strategy.drawAI(mw);
         }
      #endif

         /*
          * Implement AI_ControlInterface
          */

         //lint -e(1511) .... Overiding Thread::pause(bool)
         virtual void pause(bool mode) { Thread::pause(mode); }
         virtual void setRate(TimerRate ms) { d_timer.setRate(ms); }
         virtual String getTitle() const;
         virtual TimerRate getMinRate() const { return s_minThinkSpeed; }
         virtual TimerRate getMaxRate() const { return s_maxThinkSpeed; }
         virtual TimerRate getRate() const { return d_timer.getRate(); }

   /*----------------------------------------
    * Private Functions
    */

   private:

      void run();
         // Run the AI thread

      const CampaignData* campData() { return d_strategy.campData(); }

   /*----------------------------------------
    * Private Data
    */

   private:
      #ifdef DEBUG
         AI_ControlPanel d_panel;
      #endif

      EventList*  d_eventList;      // Way for game to tell us about passing time
      CampaignEvent d_campEvent;    // Event that is registered with eventList... signalled when gametime changes
      AIC_Strategy  d_strategy;     // The AI Processes and Data
      WaitableTimer d_timer;        // Timer used to control rate of thinking
//      int           d_rate;         // ms between thoughts


      static TimerRate s_defaultThinkSpeed;  // Thinking speed in millieconds
      static TimerRate s_minThinkSpeed;      // Thinking speed in millieconds
      static TimerRate s_maxThinkSpeed;      // Thinking speed in millieconds

      static UWORD s_fileVersion;
};


/*
 * CampaignAIList
 *
 * A container of CampaignSideAI.
 *
 * Normally there will only be one of these.. but this
 * gives us the flexibility to allow the AI to play
 * against itself.
 */


class CampaignAIList : public SList<CampaignSideAI>
{
      CampaignAIList(const CampaignAIList&);      //lint !e1714 !e1526... not referenced or defined
      CampaignAIList& operator = (const CampaignAIList&);      //lint !e1714 !e1526... not referenced or defined
   public:
      typedef SListIter<CampaignSideAI> Iter;
      typedef SListIterR<CampaignSideAI> ConstIter;

      //lint -e(1928) ... not calling all constructors
      CampaignAIList(const CampaignData* campData, EventList* eventList) :
         d_campData(campData),
         d_eventList(eventList),
         d_started(false)
      {
      }

      ~CampaignAIList()
      {
         d_campData = 0;
         d_eventList = 0;
      }

      CampaignSideAI* findAI(Side side);
         // Find AI belonging to side
         // return 0 if no AI running for that side
      CampaignSideAI* addAI(Side side);
      void start();

      bool readData(FileReader& f);
      bool writeData(FileWriter& f) const;

      // Other functions from the base class are used.
      // It may be better to make the base class private and
      // explicitly forward them on.
      //    append(CampaignSideAI*);
      //    CampaignSideAI* first();

   private:
      const CampaignData* d_campData;
      EventList* d_eventList;
      bool d_started;

      static const char s_fileChunkName[];
      static const UWORD s_fileVersion;
};

const char CampaignAIList::s_fileChunkName[] = "CampaignAI";
const UWORD CampaignAIList::s_fileVersion = 0x0000;


CampaignSideAI* CampaignAIList::findAI(Side side)
{
   CampaignAIList::Iter aiIter(this);
   while(++aiIter)
   {
      CampaignSideAI* aic = aiIter.current();

      if(aic->side() == side)
      {
         return aic;
      }
   }

   return 0;
}

CampaignSideAI* CampaignAIList::addAI(Side side)
{
   /*
    * Check that this side isn't already set up for AI
    */

   CampaignAIList::Iter aiIter(this);
   while(++aiIter)
   {
      CampaignSideAI* aic = aiIter.current();

//      ASSERT(aic->side() != side);

      if(aic->side() == side)
         return aic;
   }

   CampaignSideAI* aic = new CampaignSideAI(side, d_campData, d_eventList);
   append(aic);

   // If AI already started then start this one

   if(d_started)
      aic->start();

   return aic;
}

void CampaignAIList::start()
{
   ASSERT(!d_started);
   d_started = true;

   CampaignAIList::Iter aiIter(this);
   while(++aiIter)
   {
      CampaignSideAI* aic = aiIter.current();
      aic->start();
   }

}

bool CampaignAIList::readData(FileReader& f)
{
   FileChunkReader fc(f, s_fileChunkName);
   if (fc.isOK())
   {
      UWORD version;
      f >> version;
      ASSERT(version <= s_fileVersion);

      for (;;)
      {
         Side s;
         f >> s;
         if(s == SIDE_Neutral)
            break;

         ASSERT(GamePlayerControl::getControl(s) == GamePlayerControl::AI);

         CampaignSideAI* aic = addAI(s);
         aic->readData(f);
      }
   }
   return f.isOK();

}

bool CampaignAIList::writeData(FileWriter& f) const
{
   FileChunkWriter fc(f, s_fileChunkName);
   f << s_fileVersion;

   CampaignAIList::ConstIter aiIter(this);
   while(++aiIter)
   {
      const CampaignSideAI* aic = aiIter.current();

      f << aic->side();
      aic->writeData(f);
   }
   f << SIDE_Neutral;


   return f.isOK();
}

/*====================================================================
 * CampaignSideAI Functions
 */

// static CampaignSideAI::TimerRate CampaignSideAI::s_defaultThinkSpeed = 100;   // 10 thoughts per second (100ms)
CampaignSideAI::TimerRate CampaignSideAI::s_defaultThinkSpeed = 20;   // 50 thoughts per second (20ms)
// static CampaignSideAI::TimerRate CampaignSideAI::s_minThinkSpeed = 10;       // 10 milli-second per thought
CampaignSideAI::TimerRate CampaignSideAI::s_minThinkSpeed = 5;         // 5 milli-second per thought
CampaignSideAI::TimerRate CampaignSideAI::s_maxThinkSpeed = 5000;     // 5 second per thought

CampaignSideAI::CampaignSideAI(Side s, const CampaignData* campData, EventList* eventList) :
   SLink(),
   Thread(GameThreads::instance()),
#ifdef DEBUG
   AI_ControlInterface(),
#endif
#ifdef DEBUG
   d_panel(),
#endif
   d_eventList(eventList),
   d_campEvent(),
   d_strategy(s, campData),
   d_timer(s_defaultThinkSpeed, False)
//   d_rate(s_defaultThinkSpeed)
{

   ASSERT(campData != 0);
   ASSERT(eventList != 0);
   ASSERT(s != SIDE_Neutral);
}


CampaignSideAI::~CampaignSideAI()
{
#ifdef DEBUG
   aiLog.printf("CampaignAI (side=%s) destroyed", scenario->getSideName(side()));

   d_panel.clear();
#endif
   d_eventList->remove(&d_campEvent);
   d_eventList = 0;

   stop();     // To avoid ~Thread being called
}

UWORD CampaignSideAI::s_fileVersion = 0x0001;

bool CampaignSideAI::readData(FileReader& f)
{
   UWORD version;
   f >> version;
   ASSERT(version <= s_fileVersion);

   if (version < 0x0001)
   {
      int rate;
      f >> rate;
   }

   d_strategy.readData(f);

   return f.isOK();
}

bool CampaignSideAI::writeData(FileWriter& f) const
{
   f << s_fileVersion;
   // f << d_rate;
   d_strategy.writeData(f);
   return f.isOK();
}

/*
 * Main Thread Function
 */

void CampaignSideAI::run()
{
#ifdef DEBUG
   aiLog.printf("CampaignAI started");

   // d_aicData.logWin("Campaign AI started for %s", scenario->getSideName(side()));
#endif

   d_eventList->append(&d_campEvent);

   // Make sure some world is analyzed and some ideas are set up

   TimeTick now = campData()->getTick();

   d_strategy.init();

   // strategySchedule.run(now);
   d_strategy.run(now);    // Make scheduler wait until next interval

   d_timer.start();

   for(;;)
   {
      WaitValue result = wait(d_timer, INFINITE);
      if(result == TWV_Quit)
         break;
      else if(result == TWV_Event)
      {
         // LockData lock(&d_strategy);

         if(d_campEvent.isSet())
            d_strategy.run(campData()->getTick());

         d_strategy.think();
      }
#ifdef DEBUG
      else
         FORCEASSERT("Illegal WaitValue");
#endif
   }
}

/*
 * Receive Message from the field...
 */

void CampaignSideAI::sendMessage(const CampaignMessageInfo& msg)
{
   d_strategy.receiveGameMessage(msg);
}


String CampaignSideAI::getTitle() const
{
   String result = scenario->getSideName(side());
   result += " Campaign AI Control Panel";
   return result;
}


/*===============================================================
 * Insulated Class Implementation
 */

CampaignAI::CampaignAI(const CampaignData* campData, EventList* eventList) :
   d_aiList(new CampaignAIList(campData, eventList))
   // d_paused(false)
{
   ASSERT(d_aiList != 0);
}

CampaignAI::~CampaignAI()
{
   delete d_aiList;
}

void CampaignAI::removeAI(Side s)
{
   CampaignAIList::Iter aiIter = d_aiList;
   while(++aiIter)
   {
      CampaignSideAI* aic = aiIter.current();

      if(aic->side() == s)
      {
         aiIter.remove();
         break;
      }
   }
}

void CampaignAI::addAI(Side s)
{
   d_aiList->addAI(s);
}

void CampaignAI::start()
{
   d_aiList->start();
}

void CampaignAI::pause(bool paused)
{
   CampaignAIList::Iter aiIter = d_aiList;
   while(++aiIter)
   {
      CampaignSideAI* aic = aiIter.current();
      aic->pause(paused);
   }
}

void CampaignAI::sendMessage(const CampaignMessageInfo& msg)
{
   //lint -e(613) ... possible NULL
   CampaignSideAI* aic = d_aiList->findAI(msg.side());

   ASSERT(aic != 0);

   if(aic != 0)   //lint !e774 ... evaluates to true
   {
      aic->sendMessage(msg);
   }
}

bool CampaignAI::readData(FileReader& f)
{
   return d_aiList->readData(f);
}

bool CampaignAI::writeData(FileWriter& f) const
{
   return d_aiList->writeData(f);
}


#ifdef DEBUG
void CampaignAI::showAI(DrawDIBDC* dib, ITown itown, const PixelPoint& p) const
{
   //lint -save -e(613) ... possible NULL
   CampaignSideAI* aic = d_aiList->first();
   if(aic != 0)
      aic->showTown(dib, itown, p);
}

void CampaignAI::drawAI(const IMapWindow& mw)
{
   CampaignAIList::Iter aiIter = d_aiList;
   while(++aiIter)
   {
      CampaignSideAI* aic = aiIter.current();

      aic->drawAI(mw);
   }
}

#endif



/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.3  2001/07/02 15:23:16  greenius
 * Battle, Campaign, Combined game and Campaign Editor now compile and run... mostly
 *
 * Revision 1.2  2001/06/18 08:25:44  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
