/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef AIC_UNIT_HPP
#define AIC_UNIT_HPP

#ifndef __cplusplus
#error aic_unit.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *	Campaign AI: Data associated with a command position
 *
 *----------------------------------------------------------------------
 */

#include "cpdef.hpp"
#include "gamedefs.hpp"
// #include "refptr.hpp"
// #include <vector>
#include <map>
#include <set>

// class AIC_Activity;
class Armies;
class AIC_Objective;


/*
 * Data stored about CommandPosition
 */

class AIC_UnitBase
{
   public:
      typedef float Priority;

};


class AIC_UnitData : public AIC_UnitBase
{
   public:
      // Define some Types

      // typedef unsigned int Priority;      // Priority of Unit within current Objective


 	   AIC_UnitData();
	   ~AIC_UnitData();  //lint !e1510 ... base class has no destructor

      bool readData(FileReader& f);
      bool writeData(FileWriter& f) const;

	   // operator AIC_UnitData* () { return this; }
	   // operator const AIC_UnitData* () const { return this; }

	   SPCount spCount() const { return d_spCount; }
	   void spCount(SPCount spCount) { d_spCount = spCount; }

      void objective(AIC_Objective* obj) { d_objective = obj; }
      const AIC_Objective* objective() const { return d_objective; }
      AIC_Objective* objective() { return d_objective; }

      void priority(Priority p) { d_priority = p; }
      Priority priority() const { return d_priority; }

	   // Boolean independent() const { return d_independent; }
	   // void independent(Boolean i) { d_independent = i; }

	   // bool inUse() const { return d_inUse; }
	   // void inUse(bool f) { d_inUse = f; }

      // const ConstICommandPosition& cp() const { return d_cp; }

      // Keep STL Happy... not really sure why it needs == and < since CP is used as a key
      bool operator <(const AIC_UnitData& d) const { return this < &d; }
      bool operator ==(const AIC_UnitData& d) const { return this == &d; }

      /*
       * Utility Functions
       */

      void update(const Armies* army, ConstParamCP cp);    // Setup SPCount

   private:
      friend class AIC_UnitList;

      // AIC_UnitData(ConstParamCP cp);

      // Function for Container class to sort them with
      // struct Compare
      // {
      //    bool operator()(const AIC_UnitData& d1, const AIC_UnitData& d2)
      //    {
      //       return d1.cp() < d2.cp();
      //    }
      // };


   private:
	   // bool d_inUse;
	   // bool d_independent;				// True if different activity than parent
	   SPCount d_spCount;				// Strength of this organization excluding independent children
      // ConstICommandPosition d_cp;   // Which CP is it?
	   // AIC_Activity* d_activity;		// Activity it beongs to.
      AIC_Objective* d_objective;      // Current Objective
      Priority d_priority;                  // Priority within objective

      static UWORD s_fileVersion;

};

#if 0
// Function to keep STL Happy
inline bool operator < (const AIC_Unit& lhs, const RefPtr<AIC_UnitData>& rhs)
{
      return lhs->cp()->getSelf() < rhs->cp()->getSelf();
}
#endif

// typedef pair<const ConstICommandPosition, AIC_UnitData> AIC_Unit;

#if 0    // Simplify, avoid pair<> and references
class AIC_UnitRef : public AIC_UnitBase
{
   friend class AIC_ConstUnitRef;

   public:
      AIC_UnitRef(pair<const ConstICommandPosition, AIC_UnitData>& p) : d_pair(&p) { }

      const ConstICommandPosition& cp() { return d_pair->first; }

	   SPCount spCount() const { return d_pair->second.spCount(); }
	   void spCount(SPCount spCount) { d_pair->second.spCount(spCount); }

      void objective(AIC_Objective* obj) { d_pair->second.objective(obj); }
      const AIC_Objective* objective() const { return d_pair->second.objective(); }
      AIC_Objective* objective() { return d_pair->second.objective(); }

      void priority(Priority p) { d_pair->second.priority(p); }
      Priority priority() const { return d_pair->second.priority(); }

      void update(const Armies* army, ConstParamCP cp) { d_pair->second.update(army, d_pair->first); }

   private:
      pair<const ConstICommandPosition, AIC_UnitData>* d_pair;
};

class AIC_ConstUnitRef : public AIC_UnitBase
{
   public:
      AIC_ConstUnitRef(const pair<const ConstICommandPosition, AIC_UnitData>& p) : d_pair(&p) { }
      AIC_ConstUnitRef(const AIC_UnitRef& r) : d_pair(r.d_pair) { }

      const ConstICommandPosition& cp() const { return d_pair->first; }
	   SPCount spCount() const { return d_pair->second.spCount(); }
      const AIC_Objective* objective() const { return d_pair->second.objective(); }
      Priority priority() const { return d_pair->second.priority(); }
   private:
      const pair<const ConstICommandPosition, AIC_UnitData>* d_pair;
};
#endif

class AIC_UnitRef : public AIC_UnitBase
{
   friend class AIC_ConstUnitRef;

   public:
      AIC_UnitRef(ConstParamCP cp, AIC_UnitData* unitData) : d_cp(cp), d_unitData(unitData) { }

      const ConstICommandPosition& cp() const { return d_cp; }

	   SPCount spCount() const { return d_unitData->spCount(); }
	   void spCount(SPCount spCount) { d_unitData->spCount(spCount); }

      void objective(AIC_Objective* obj) { d_unitData->objective(obj); }
      const AIC_Objective* objective() const { return d_unitData->objective(); }
      AIC_Objective* objective() { return d_unitData->objective(); }

      void priority(Priority p) { d_unitData->priority(p); }
      Priority priority() const { return d_unitData->priority(); }

      // void update(const Armies* army, ConstParamCP cp) { d_unitData->update(army, d_cp); }
      void update(const Armies* army) { d_unitData->update(army, d_cp); }

      AIC_UnitData* unitData() { return d_unitData; }

   private:
      // pair<const ConstICommandPosition, AIC_UnitData>* d_pair;
      ConstICommandPosition d_cp;
      AIC_UnitData* d_unitData;
};

class AIC_ConstUnitRef : public AIC_UnitBase
{
   public:
      // AIC_ConstUnitRef(const pair<const ConstICommandPosition, AIC_UnitData>& p) : d_pair(&p) { }
      AIC_ConstUnitRef(ConstParamCP cp, const AIC_UnitData* unitData) : d_cp(cp), d_unitData(unitData) { }
      AIC_ConstUnitRef(const AIC_UnitRef& r) : d_cp(r.d_cp), d_unitData(r.d_unitData) { }

      const ConstICommandPosition& cp() const { return d_cp; }
	   SPCount spCount() const { return d_unitData->spCount(); }
      const AIC_Objective* objective() const { return d_unitData->objective(); }
      Priority priority() const { return d_unitData->priority(); }

      const AIC_UnitData* unitData() const { return d_unitData; }

   private:
      ConstICommandPosition d_cp;
      const AIC_UnitData* d_unitData;
};


class AIC_UnitList
{
		// typedef map<CPIndex, RefPtr<AIC_UnitData>, less<CPIndex> > Container;
		// typedef map<const CommandPosition*, AIC_UnitData, less<CommandPosition*> > Container;
      typedef std::map<ConstICommandPosition, AIC_UnitData, std::less<ConstICommandPosition> > Container;
      // typedef set<AIC_UnitData, AIC_UnitData::Compare> Container;

	public:
		AIC_UnitList();
		~AIC_UnitList();

      bool readData(FileReader& f, const Armies* army);
      bool writeData(FileWriter& f) const;

      void init(const Armies* army, Side side);        // Create the Unit List
      // void update(const Armies* army, Side side);      // Update the Unit List

		AIC_UnitRef operator[](ConstParamCP cp);
		AIC_ConstUnitRef operator[](ConstParamCP cp) const;

		bool isMember(ConstParamCP cp) const;
      AIC_UnitRef getOrCreate(ConstParamCP cp);

      static bool isUnitValid(ConstParamCP cp);

		// AIC_Unit* get(ConstParamCP cp) { return &operator[](cp); }
		// const AIC_Unit* get(ConstParamCP cp) const { return &operator[](cp); }
		// AIC_UnitData* getOrCreate(ConstParamCP cp);

		// const Armies* army() const { return d_army; }

		typedef Container::iterator iterator;
		iterator begin() { return d_items.begin(); }
		iterator end() { return d_items.end(); }
		typedef Container::const_iterator const_iterator;
		const_iterator begin() const { return d_items.begin(); }    //lint !e1036
		const_iterator end() const { return d_items.end(); }        //lint !e1036

      unsigned int size() const { return d_items.size(); }

      void erase(iterator it) { d_items.erase(it); }

	private:
		AIC_UnitRef create(ConstParamCP cp);
		void remove(ConstParamCP cp);

      Side d_side;            // which side's units are stored here?
		Container d_items;      // One for each side

      static UWORD s_fileVersion;

};

class AIC_CPList
{
      typedef std::set<ConstICommandPosition, std::less<ConstICommandPosition> > Container;
   public:
      AIC_CPList() :
         // d_spCount(0),
         d_items()
      {
      }

      ~AIC_CPList()
      {
      }

      bool readData(FileReader& f, const Armies* army);
      bool writeData(FileWriter& f) const;

      bool isMember(ConstParamCP cp) const { return d_items.find(cp) != d_items.end(); }
      SPCount spCount() const;   // { return d_spCount; }

      // Make it look like STL class, but maintain SPCount

      typedef Container::iterator iterator;
      typedef Container::const_iterator const_iterator;
      iterator begin() { return d_items.begin(); }
      iterator end() { return d_items.end(); }
      const_iterator begin() const { return d_items.begin(); }
      const_iterator end() const { return d_items.end(); }


      iterator find(ConstParamCP cp) { return d_items.find(cp); }
      std::pair<iterator,bool> insert(ConstParamCP cp)
      {
//         d_spCount += cp->spCount(true);
         return d_items.insert(cp);
      }

      Container::size_type erase(ConstParamCP cp)
      {
         Container::size_type t = d_items.erase(cp);
         ASSERT(t == 1);
//          if(t >= 1)
//          {
//             ASSERT(d_spCount >= cp->spCount(true));
//             d_spCount -= cp->spCount(true);
//          }
         return t;
      }

      void clear()
      {
         d_items.clear();
         // d_spCount = 0;
      }

      Container::size_type size() const { return d_items.size(); }

   private:
      // SPCount d_spCount;
      Container d_items;

      static UWORD s_fileVersion;
};

#endif /* AIC_UNIT_HPP */

