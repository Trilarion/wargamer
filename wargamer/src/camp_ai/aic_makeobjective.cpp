/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 * Create Objectives
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "aic_makeObjective.hpp"
#include "aic_sdat.hpp"
#include "aic_town.hpp"
#include "aic_objective.hpp"
#include "aic_unit.hpp"
// #include "aic_UnitInfluence.hpp"
#ifdef DEBUG
#include "objectiveWindow.hpp"
#include "campdint.hpp"
#include "campint.hpp"  // naughty, global campaign
#endif

AIC_ObjectiveCreator::AIC_ObjectiveCreator(AIC_StrategyData* sData, AIC_TownList* towns, AIC_ObjectiveList* objectives, AIC_UnitList* units) :
   d_sData(sData),
   d_towns(towns),
   d_objectives(objectives),
#ifdef DEBUG
   d_objectiveDisplay(0),
#endif
   d_units(units),
   d_townInfluence(sData, units)
{
   ASSERT(sData != 0);
   ASSERT(towns != 0);
   ASSERT(objectives != 0);
   ASSERT(units != 0);

}

AIC_ObjectiveCreator::~AIC_ObjectiveCreator()
{
   // Remove All objectives to stop ASSERTions because there are still units attached to objectives

   while(d_objectives->size())
   {
      removeObjective(&d_objectives->front());
   }

#ifdef DEBUG
   delete d_objectiveDisplay;
   d_objectiveDisplay = 0;
#endif
   d_sData = 0;
   d_towns = 0;
   d_objectives = 0;
   d_units = 0;
}

void AIC_ObjectiveCreator::init()
{

#ifdef DEBUG
   d_objectiveDisplay = new ObjectiveDisplay(d_objectives, d_sData);
#endif
}

void AIC_ObjectiveCreator::run(unsigned int rand)
{
#ifdef DEBUG
   d_sData->logWin("Create Objective(%d)", rand);
#endif

   ASSERT(rand < priority());

   int nPasses = 10;

   while (nPasses--)
   {
      if (d_towns->priority())
      {
         rand = d_sData->rand(d_towns->priority());
         ASSERT(rand < d_towns->priority());
         // ASSERT(priority() == d_towns->priority());

         /*
          * Pick an objective: High priority locations are more likely to get picked
          */

         const AIC_TownInfo& tInfo = d_towns->pick(rand);

      #ifdef DEBUG
         d_sData->logWin("Picked %s", d_sData->campData()->getTownName(tInfo.town()));
      #endif

         tryTown(tInfo);
      }
   }
}

void AIC_ObjectiveCreator::tryTown(const AIC_TownInfo& tInfo)
{
   /*
    * Calculate influences
    */

   d_townInfluence.init(tInfo);

   /*
    * Is objective possible at the moment?  friendly influence > enemy influence
    */

   if(!d_townInfluence.canAchieve() || !buildObjective(tInfo))
   {
#ifdef DEBUG
      d_sData->logWin("Impossible Objective: %s", d_sData->campData()->getTownName(tInfo.town()));
#endif

      AIC_Objective* obj = d_objectives->find(tInfo.town());
      if(obj != 0)
      {
         removeObjective(obj);
      }
   }

   d_townInfluence.reset();
}


bool AIC_ObjectiveCreator::buildObjective(const AIC_TownInfo& tInfo)
{
#ifdef DEBUG
   d_sData->logWin("Assigning units to %s", d_sData->campData()->getTownName(tInfo.town()));
#endif

   /*
    * Pass 1:
    *    build list of units and keep track of SPs removed from
    *    other objectives
    *
    *    Ony units that would not destroy an objective with
    *    a higher townImportance can be used
    */

   std::map<ITown, int, std::less<int> > otherObjectives;
   std::vector<TownInfluence::Unit> allocatedUnits;

   SPCount spNeeded = d_townInfluence.spNeeded();
   SPCount spAlloced = 0;
   SPCount spToAllocate = d_sData->rand(spNeeded, d_townInfluence.spAvailable());

   TownInfluence::Unit infUnit;
   while((spAlloced < spToAllocate) &&
      d_townInfluence.pickAndRemove(&infUnit))
   {
      ASSERT(infUnit.cp() != NoCommandPosition);

      if(infUnit.cp()->isDead())
         continue;

      AIC_UnitRef aiUnit = d_units->getOrCreate(infUnit.cp());
      TownInfluence::Influence unitInfluence = infUnit.influence();   // friendlyInfluence.influence(aiUnit.cp());

      float oldPriority = d_townInfluence.effectivePriority(aiUnit);
      if(unitInfluence >= oldPriority)
      {
         SPCount spCount = aiUnit.spCount();

#ifdef DEBUG
         d_sData->logWin("Picked %s [SP=%d, pri=%f / %f]",
            (const char*) infUnit.cp()->getName(),
            (int) spCount,
            (float) unitInfluence,
            (float) oldPriority);
#endif

         /*
          * If it already has an objective
          * Then update the otherObjective list
          */

         AIC_Objective* oldObjective = aiUnit.objective();
         if(oldObjective)
         {
            ITown objTown = oldObjective->town();

            if (spAlloced > spNeeded)
            {

#ifdef DEBUG
               d_sData->logWin("Not using %s from %s because we already have enough SPs",
                  (const char*) infUnit.cp()->getName(),
                  (const char*) d_sData->campData()->getTownName(objTown));
#endif
               continue;
            }


            if (objTown != tInfo.town())
            {
               const AIC_TownInfo& objTownInf = d_towns->find(objTown);
               if(objTownInf.importance() >= tInfo.importance())
               {
                  int* otherCount = 0;
                  if(otherObjectives.find(objTown) == otherObjectives.end())
                  {
                     otherCount = &otherObjectives[objTown];
                     *otherCount = oldObjective->spAllocated() - oldObjective->spNeeded();
                  }
                  else
                     otherCount = &otherObjectives[objTown];

                  if(*otherCount >= spCount)
                     *otherCount -= spCount;
                  else
                  {
#ifdef DEBUG
                     d_sData->logWin("Can not use %s because it would break objective at %s",
                        (const char*) infUnit.cp()->getName(),
                        (const char*) d_sData->campData()->getTownName(objTown));
#endif
                     continue;
                  }
               }
            }
         }

         allocatedUnits.push_back(infUnit);
         spAlloced += spCount;
      }
   }

   if (spAlloced < spNeeded)
   {
#ifdef DEBUG
      d_sData->logWin("Can not be achieved without breaking more important objective");
#endif
      return false;
   }

   /*
    * Assign the allocated Units to objective
    */

   Writer lock(d_objectives);

   AIC_Objective* objective = d_objectives->addOrUpdate(tInfo.town(), tInfo.importance());
   ASSERT(objective != 0);
   if(objective == 0)   //lint !e774 ... always true
      return false;

#ifdef DEBUG
   d_sData->logWin("Creating Objective %s", d_sData->campData()->getTownName(tInfo.town()));
   d_sData->logWin("There are %d objectives", (int)d_objectives->size());
#endif

   objective->spNeeded(spNeeded);

   for (std::vector<TownInfluence::Unit>::iterator it = allocatedUnits.begin();
      it != allocatedUnits.end();
      ++it)
   {
      const TownInfluence::Unit& infUnit = *it;

      AIC_UnitRef aiUnit = d_units->getOrCreate(infUnit.cp());
      TownInfluence::Influence unitInfluence = infUnit.influence();   // friendlyInfluence.influence(aiUnit.cp());

#ifdef DEBUG
         d_sData->logWin("Adding %s",
            (const char*) infUnit.cp()->getName());
#endif

      // Remove unit from its existing Objective
      // Unless it is already attached to this one

      AIC_Objective* oldObjective = aiUnit.objective();

      if(oldObjective != objective)
      {
         if(oldObjective != 0)
         {
            // Remove Unit from Objective
            // If objective does not have enough SPs then
            // remove the objective

            removeUnit(infUnit.cp());
         }

         ASSERT(aiUnit.objective() == 0);

         // Add it to the objective table

         aiUnit.objective(objective);
         objective->addUnit(infUnit.cp());
      }

      // Set priority to a higher value to
      // reduce the problem of objectives being
      // created and destroyed too quickly.

      const float PriorityObjectiveIncrease = 1.5;
      aiUnit.priority(unitInfluence * PriorityObjectiveIncrease);
   }

#ifdef DEBUG
   if(d_objectiveDisplay)
      d_objectiveDisplay->update();
   if(campaign)
      campaign->repaintMap();
#endif

   return true;
}

void AIC_ObjectiveCreator::timeUpdate(TimeTick t)
{

}

#ifdef DEBUG
String AIC_ObjectiveCreator::name() const
{
   return "AIC_ObjectiveCreator";
}
#endif

void AIC_ObjectiveCreator::removeObjective(AIC_Objective* objective)
{
   ITown itown = objective->town();

#ifdef DEBUG
   d_sData->logWin("Removing %s", d_sData->campData()->getTownName(itown));
#endif

   d_objectives->remove(itown);

   #ifdef DEBUG
      d_sData->logWin("There are %d objectives", (int)d_objectives->size());
   #endif
}

/*
 * Remove CP from its current objective
 * If it wa the last unit then delete the objective
 */

void AIC_ObjectiveCreator::removeUnit(ConstParamCP cp)
{
   AIC_UnitRef aiUnit = (*d_units)[cp];
   AIC_Objective* objective = aiUnit.objective();
   ASSERT(objective != 0);

   aiUnit.objective(0);
   aiUnit.priority(0);

   // Remove objective if SP is less than a random value
   // between 1/4 and 3/4 of the spNeeded

   // It might be better to base this on the priority of the
   // units left rather than the raw SPCount



   SPCount minSP = objective->spNeeded() / 4;
   SPCount maxSP = (objective->spNeeded() * 3) / 4;

   if(maxSP != minSP)
      minSP = d_sData->rand(minSP, maxSP);

   objective->removeUnit(cp);
   if( (objective->unitList().size() == 0) || (objective->spAllocated() <= minSP))
   {
//      d_objectives->remove(objective->town());
      removeObjective(objective);
   }


//    aiUnit.objective(0);
//    aiUnit.priority(0);
//
//    AIC_CPList& cpList = objective->unitList();
//    ASSERT(cpList.isMember(cp));
//    cpList.erase(cp);
//
//    if(cpList.size() == 0)
//       remove(objective->town());
}


/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.2  2001/06/18 08:25:44  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
