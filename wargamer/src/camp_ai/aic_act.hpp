/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#error "Obsolete"

#ifndef AIC_ACT_HPP
#define AIC_ACT_HPP

#ifndef __cplusplus
#error aic_act.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *	Campaign AI: Activities
 *
 *----------------------------------------------------------------------
 */

#include "mytypes.h"
#include "dlist.hpp"
#include "dynarray.hpp"
#include "gamedefs.hpp"
#include "cpdef.hpp"
#include "myassert.hpp"
#include "refptr.hpp"

#ifdef DEBUG
#include "StringPtr.hpp"

class LogWindow;
#endif

class Armies;
class CampaignData;
class RandomNumber;

class AIC_ActivityUnitList : public ResizeArray<ConstICommandPosition>
{
};

class AIC_ActivityUnitIter : public ResizeArrayIter<ConstICommandPosition>
{
 public:
	AIC_ActivityUnitIter(const AIC_ActivityUnitList* list) :
		ResizeArrayIter<ConstICommandPosition>(list)
	{
	}
};

class AIC_ActivityTownList : public ResizeArray<ITown>
{
};

class AIC_ActivityTownIter : public ResizeArrayIter<ITown>
{
 public:
	AIC_ActivityTownIter(const AIC_ActivityTownList* list) :
		ResizeArrayIter<ITown>(list)
	{
	}
};

/*
 * Common Data for any activity
 */

class AIC_Activity : public RefBaseDel, public DLink
{
	friend class AIC_ActivityList;
	friend class AIC_ActivityIter;

	typedef AIC_ActivityUnitList ActivityUnitList;
	typedef AIC_ActivityTownList ActivityTownList;
	typedef AIC_ActivityTownIter ActivityTownIter;

 	// Tree structure

	AIC_Activity* 			d_parent;
	DiList<AIC_Activity> d_children;		// Doubly linked list of self

	Boolean d_active;				// Clear this if should be deleted

	AIC_ActivityUnitList	d_units;		// List of Units
	AIC_ActivityTownList	d_towns;		// Towns in this activity

	/*
	 * Priorities of each action
	 */

 public:

 	// Note: If this enum is changed,
	//			Please update s_thingsToDoStr[] in aic_act.cpp

	enum ThingsToDo {
		TDD_First = 0,

		TDD_Update = TDD_First,
		TDD_Split,
		TDD_Expand,
		TDD_AssignSP,		// Assign itself SP's from parent
		// TDD_TakeSP,			// Take SP's from children
		TDD_SendOrder,
		TDD_OrganizeTowns,	// Remove disconnected towns
		TDD_Fizzle,				// Destroy activity
		TDD_Last = TDD_Fizzle // TDD_OrganizeTowns
   };

   enum {
		TDD_HowMany = TDD_Last + 1
	};

 private:
 	int d_priorities[TDD_HowMany];		// Chance of executing each action

	int d_priority;			// Priority (sum of priorities[])

	int d_totalPriority;		// Priority including children
		// Priority must be altered through the parent list
		// to ensure that the list's priority is kept up to date

	int d_importance;			// Activities importance, sum of all 
									// contained town's importances 
									// (including children)

	SPCount d_spNeeded;					// Strength Needed including children
	SPCount d_spAllocated;				// Strength currently allocated including children

	SPCount d_takeNeeded;				// How many SPs needed by children
	int d_takeImportance;					// How important it is to obtain new SPs for children

	int d_age;							// How old is this activity
	int d_happiness;					// How happy is this activity to exist? when 0, activity will be deleted


	// Boolean d_needUpdate;				// totalPriority and importance needs recalculating

#ifdef DEBUG
	static int s_id;
	StringPtr d_id;
#endif

 public:
	AIC_Activity(AIC_Activity* parent);
	~AIC_Activity();

	void addUnit(ConstICommandPosition cpi);
 	void removeUnit(ConstICommandPosition cpi);

	void addTown(ITown itown);
	void removeTown(ITown itown);

	void addSP(SPCount spCount);
	void delSP(SPCount spCount);

	void addSPNeeded(SPCount add);
	void subSPNeeded(SPCount sub);
		// Adjust SPNeeded

	void addImportance(int importance);
		// Adjust importance.
		// value can be -ve to reduce it
		// Applies value to parents, so that total is kept uptodate.

	int importance() const { return d_importance; }

	/*
	 * Priority Functions
	 */

	int priority() const; 								// Get priority
	int priority(ThingsToDo which) const;			// Get task Priority
	void priority(ThingsToDo which, int value);	// Set Task Priority
	int totalPriority() const;							// Get total Priority

	void addPriority(ThingsToDo which, int add);
		// Add value to priority, and to parent activities

	void lowerPriority(ThingsToDo which);
		// Lower priority, due to inaction

	void addTakeNeeded(SPCount needed, int importance);
		// Update takeNeeded

	// Accessors

	// void spNeeded(SPCount sp) { d_spNeeded = sp; }
	SPCount spNeeded() const { return d_spNeeded; }

	SPCount spAllocated() const { return d_spAllocated; }


	int age() const { return d_age; }
	void incAge() { d_age++; }
	void resetAge();

	int happiness() const { return d_happiness; }
	void happiness(int h) { d_happiness = h; }
	void addHappiness(int h);

	Boolean active() const { return d_active; }
		// Return True if activity should not be deleted
	void setInactive() { d_active = False; }
		// Mark activity as inactive, and able to be deleted

	int childCount() const { return d_children.entries(); }
		// Return number of child activities

	void appendChild(AIC_Activity* activity) { d_children.append(activity); }

    //lint -save -e1536 ... prevent exposing low access member
	DiList<AIC_Activity>* children() { return &d_children; }
	const DiList<AIC_Activity>* children() const { return &d_children; }
		// Return pointer to list of children

	ActivityUnitList* units() { return &d_units; }
	const ActivityUnitList* units() const { return &d_units; }
		// Return pointer to Unit List

	ActivityTownList* towns() { return &d_towns; }
	const ActivityTownList* towns() const { return &d_towns; }
		// Return Pointer to Town List

	AIC_Activity* parent() { return d_parent; }
	const AIC_Activity* parent() const { return d_parent; }
    //lint -restore

	static Boolean isChildOf(const AIC_Activity* p, const AIC_Activity* child);
		// Return True of p is a decsendent of child

#ifdef DEBUG
	const char* description() const { return d_id; }
		// Return some kind of text to indicate which activity it is

	static const char* thingToDoStr(ThingsToDo what);

	const char* priorityStr(char* buffer) const;
#endif

	// void run(AIC_StrategyData* sDat);

 private:
	// void needUpdate();
		// Make activity as needing to update it's totals

 	// void makePriority();
		// Add up priorities[] to make priority

 	// void priority(int p);		  					// Set priority
	void addTotalPriority(int p);	  					// Add value to total priority
	void addPriority(int p);							// Add value to priority

#ifdef DEBUG
	void checkPriorities() const;
#else
	void checkPriorities() const { }
#endif

};

class AIC_ActivityList
{
	AIC_Activity* d_top;

	AIC_ActivityList(const AIC_ActivityList&);
	AIC_ActivityList& operator = (const AIC_ActivityList&);

 public:
 	AIC_ActivityList();
	~AIC_ActivityList();

	AIC_Activity* create(AIC_Activity* parent);
		// Create a new activity as child of parent

	void kill(AIC_Activity* activity);
		// Kill an activity, and all activities below it

	AIC_Activity* top() { return d_top; }
	const AIC_Activity* top() const { return d_top; }
		// Return the top of the activity tree

	int priority() { return d_top->totalPriority(); }

	AIC_Activity* pick(AIC_Activity::ThingsToDo* what, RandomNumber& randGen);

#ifdef DEBUG
	void log(LogWindow* logWin);
#endif
};

/*
 * Acivity Iterator, iterates through all nodes inclusive of and below
 * the starting node.
 * Child nodes are always returned before the parents.
 */

class AIC_ActivityIter
{
	AIC_Activity* d_top;
	AIC_Activity* d_current;
	Boolean d_topDown;
	Boolean d_finished;
	Boolean d_gotNext;			// Set if next item is already set up

	AIC_ActivityIter(const AIC_ActivityIter&);
	AIC_ActivityIter& operator = (const AIC_ActivityIter&);
 public:
 	AIC_ActivityIter(AIC_Activity* top, Boolean topDown = False);
		// Iterates from top down if topDown=True
		// Iterates from bottom up if topDown=False

   //lint -save -e1758 ... Should return a reference
	Boolean operator ++ ();
	//lint -restore

	AIC_Activity* current() 
	{
		ASSERT(!d_finished);
		ASSERT(!d_gotNext); 
		return d_current; 
	}

	AIC_Activity* operator->() { return current(); }

	void invalidate();	// Current item and children are about to be deleted
};


inline int AIC_Activity::priority() const
{
	checkPriorities(); 
	return d_priority;
}

inline int AIC_Activity::totalPriority() const
{
	checkPriorities();
	return d_totalPriority;
}

inline int AIC_Activity::priority(ThingsToDo which) const 
{
	ASSERT((which >= TDD_First) && (which < AIC_Activity::TDD_HowMany));

	return d_priorities[which]; 
}

#endif /* AIC_ACT_HPP */

