/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef AIC_STRA_HPP
#define AIC_STRA_HPP

#ifndef __cplusplus
#error aic_stra.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *	Campaign AI Strategy
 *
 *----------------------------------------------------------------------
 */


#include "gamedefs.hpp"
#include "aic_sdat.hpp"
#include "sched.hpp"
#include "codelet.hpp"
#include "codelist.hpp"

/*
 * Forward References
 */

class CampaignData;

class AIC_TownList;           // Master List of Towns
class AIC_ObjectiveList;      // Master List of Objectives
class AIC_UnitList;           // Master List of Units

class CampaignMessageInfo;
class AIC_MessageProcess;

#ifdef DEBUG
class DrawDIBDC;
class PixelPoint;
class IMapWindow;
#endif



/*
 * Strategy class contains data used to think about things
 */


class AIC_Strategy : public GameSchedule, private AIC_StrategyData
{
    public:
     	AIC_Strategy(Side s, const CampaignData* campData);
	    ~AIC_Strategy();

      bool readData(FileReader& f);
      bool writeData(FileWriter& f) const;


    	void init();
		    // Set up initial Activities, analyze, etc...


	   Side side() const { return AIC_StrategyData::side(); }   //lint !e1511 ... hides member
    	const CampaignData* campData() { return AIC_StrategyData::campData(); } //lint !e1511 ... Hides member

#ifdef DEBUG
    	void showTown(DrawDIBDC* dib, ITown itown, const PixelPoint& p);
      void drawAI(const IMapWindow& mw);
#endif

    	void process(TimeTick theTime);
	    	// Note that real time has passed

    	void think();
	    	// Have a thought

      // Process Incoming Game message
      void receiveGameMessage(const CampaignMessageInfo& msg);

      /*
       * Implement AIC_StrategyData Interface
       */

      virtual void addPriority(AIC_CODELET_ID id, unsigned int p);      // Add Priority and return new value
      virtual void subPriority(AIC_CODELET_ID id, unsigned int p);      // Add Priority and return new value
      virtual void priority(AIC_CODELET_ID id, unsigned int p);
      virtual unsigned int priorityRange() const;

    private:

      /*--------------
       * Private Functions
       */

      void maintain();
          // Do regular maintenance every thought
      void createData();
         // create empty town, objective and unitlists if not already created

      #ifdef DEBUG
      String name() const { return("AIC_Strategy"); }
      #endif

      /*--------------
       * Data
       */

      AIC_TownList*        d_townList;          // Master List of Towns
      AIC_ObjectiveList*   d_objectiveList;     // Master List of Objectives
      AIC_UnitList*        d_unitList;          // Master List of Units

      // Codelet Processor

      AIC_CodeletList  	   d_codeList;		      // Main list of codelets to run
      AIC_CodeletList::ID  d_codeID[AIC_CODE_HowMany];

      // Processors : Each of these is derived from AIC_Codelet

      AIC_MessageProcess*     d_procMessages;         // Game-Message Processor

      static TimeTick s_realTimeInterval;    // How often does it get notified of time changes
      static UWORD s_fileVersion;
};


#endif /* AIC_STRA_HPP */

