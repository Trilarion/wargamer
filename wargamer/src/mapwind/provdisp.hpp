/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef PROVDISP_HPP
#define PROVDISP_HPP

#ifndef __cplusplus
#error provdisp.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *	Province Display
 *
 *----------------------------------------------------------------------
 */

class ProvinceDisplayImp;
class DrawDIBDC;
class MapWindowData;

class ProvinceDisplay {
	ProvinceDisplayImp* d_provImp;
 public:
 	ProvinceDisplay();
	~ProvinceDisplay();

	void drawDynamic(DrawDIBDC* dib, const MapWindowData& mapData);
	void drawStatic(DrawDIBDC* dib, const MapWindowData& mapData);
};


#endif /* PROVDISP_HPP */


