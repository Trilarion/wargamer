/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
#ifndef MAPANIM_HPP
#define MAPANIM_HPP

#include "gamedefs.hpp"

class MW_Animation;
class MapWindowData;
class DrawDIBDC;

class MW_AnimationManager {
    enum {
      Battle,
      Animation_HowMany
    };

    MW_Animation* d_mwAnimations[Animation_HowMany];
  public:
    MW_AnimationManager();
    ~MW_AnimationManager();

    void init(const MapWindowData* mapData);
    void run(const DrawDIBDC* srcDib, HDC hdc, Boolean sameFrame = False);
};



#endif

/*
 *----------------------------------------------------------------------
 * $Log$
 *----------------------------------------------------------------------
 */
