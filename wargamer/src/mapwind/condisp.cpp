/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 * Connection Drawing
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "condisp.hpp"
#include "mw_data.hpp"
#include "dib.hpp"
#include "town.hpp"
#include "campdint.hpp"
#include "gengine.hpp"
#include "fractal.hpp"
#include "thicklin.hpp"
#include "locengin.hpp"
#include "gdiengin.hpp"
#include "palette.hpp"
#include "dib_util.hpp"


/*
 * Draw railway lines and other connections
 *
 * These needs improving...
 */

void ConnectionDisplay::draw(DrawDIBDC* dib, MapWindowData& mapData, const CampaignData& campData)
{
   CArrayIter<Connection> cl = campData.getConnections();

   const TownList& tl = campData.getTowns();

   /*
    * Use different colour depending on connection type
    */

   // HDC hdc = gApp.getDibDC();
   // HBITMAP oldBM = (HBITMAP) SelectObject(hdc, dib->getHandle());

#if 0
   ColourIndex colors[CT_Max];
   colors[CT_Road]  = dib->getColour(PALETTERGB(128,64,64));
   colors[CT_Rail]  = dib->getColour(PALETTERGB(0,0,0));
   colors[CT_River] = dib->getColour(PALETTERGB(0,0,128));
#endif

   const ColourIndex MaskColor = 0;
   const ColourIndex RoadIndex = 1;    // Index used for main road
   const ColourIndex RoadDark  = 2;    // Index used for Dark edge of road
   const ColourIndex RoadLight = 3;    // Index used for Light edge of road


   /*
    * Set up a temporary DIB
    */

   // DrawDIB* bufDIB = new DrawDIB(dib->getWidth(), dib->getHeight());
   DrawDIB* bufDIB = mapData.getFXDIB(dib);
   bufDIB->setMaskColour(MaskColor);
   bufDIB->fill(MaskColor);

   /*
    * Draw connections into temporary DIB
    */


   PixelPoint p0(0,0);
   PixelPoint p1(1,1);
   Location loc0;
   Location loc1;
   mapData.pixelToLocation(p0, loc0);
   mapData.pixelToLocation(p1, loc1);

   int res = minimum(loc1.getX() - loc0.getX(), loc1.getY() - loc0.getY());

   ThickLineEngine dibEngine(bufDIB);
   LocationEngine conEngine(dibEngine, mapData);
   FractalEngine fractalEngine(conEngine, FractalEngine::BendRange/8, res);

   // dibEngine.color(RGB(0,0,0));
   dibEngine.color(RoadIndex);         // Draw roads in colour index 1

   Rect<Distance> r;
   mapData.getViewRect(r);
   fractalEngine.setClip(FractalEngine::GRect(r.left(), r.top(), r.width(), r.height()));

   while(++cl)
   {
      const Connection& con = cl.current();

      const Location& l1 = tl[con.node1].getLocation();
      const Location& l2 = tl[con.node2].getLocation();


      /*
       * Set up rendering pipeline and engine based on:
       *    - Type of connection
       *    - Zoom level
       *    - Quality / Capacity
       *
       * Fractal engine must use Locations rather than pixels
       * or else they will move depending on the screen position.
       */

      // thickness of roads outof 10.
      // Value of 10, means 1 pixel on outermost zoom

      static int roadThick[CQ_Max] = {
#if 0
         5,    // Poor
         7,    // Average
         10    // Good
#else
         5,10,15     // Good is 1.5 pixels wide
#endif
      };

      int thickness = MulDiv(
         con.capacity * roadThick[con.quality] * mapData.getZoomLevel(),
         mapData.displayWidth(),
         1024 * 10 * (MaxConnectCapacity - 1));
      if(thickness < 1)
         thickness = 1;
      dibEngine.thickness(thickness);

      // High quality roads are straighter?

      static int roadBend[CQ_Max] = {
         FractalEngine::BendRange/4,   // Poor
         FractalEngine::BendRange/8,   // Average
         FractalEngine::BendRange/16,  // Good
      };

      fractalEngine.setValues(roadBend[con.quality], res);


      fractalEngine.addPoint(GraphicEngine::GPoint(l1.getX(), l1.getY()));
      fractalEngine.addPoint(GraphicEngine::GPoint(l2.getX(), l2.getY()));
      fractalEngine.close();

#if 0
      PixelPoint p1;
      PixelPoint p2;

      mapData.locationToPixel(l1, p1);
      mapData.locationToPixel(l2, p2);

      /*
       * Would be good to do a clipping test to see whether to draw or not
       */

      switch(con.how)
      {
        case CT_River:
          drawRiver(dib, p1, p2, colors[con.how], &con, mapData);
          break;
        case CT_Road:
          drawRoad(dib, p1, p2, colors[con.how], &con, mapData);
          break;
        case CT_Rail:
          drawRailLine(dib, p1, p2, colors[con.how], &con, mapData);
          break;
      }
#endif
   }

   /*
    * Apply edge effects to buffer DIB
    */

   DIB_Utility::colorEdge(bufDIB, -1,-1, RoadDark);   // Top Left get darkened
   DIB_Utility::colorEdge(bufDIB, +1,+1, RoadLight);  // Bottom Right gets lightened

   // const UBYTE* darkTable = Palette::getColorizeTable(RGB(0,0,0), 40);
   // const UBYTE* darkTable = Palette::getColorizeTable(RGB(0,0,0), 30);
   CPalette::RemapTablePtr darkTable = dib->getPalette()->getColorizeTable(RGB(0,0,0), 30);
   DIB_Utility::remapColor(dib, bufDIB, darkTable, RoadDark, 0,0,dib->getWidth(),dib->getHeight(),0,0);
   // Palette::releaseTable(darkTable);

   // const UBYTE* lightTable = Palette::getColorizeTable(RGB(0,0,0), 5);
   // const UBYTE* lightTable = Palette::getLightRemapTable(20);
   CPalette::RemapTablePtr lightTable = dib->getPalette()->getLightRemapTable(20);
   DIB_Utility::remapColor(dib, bufDIB, lightTable, RoadLight, 0,0,dib->getWidth(),dib->getHeight(),0,0);
   // Palette::releaseTable(lightTable);

   // const UBYTE* roadTable = Palette::getColorizeTable(RGB(0,0,0), 20);
   CPalette::RemapTablePtr roadTable = dib->getPalette()->getColorizeTable(RGB(0,0,0), 20);
   DIB_Utility::remapColor(dib, bufDIB, roadTable, RoadIndex, 0,0,dib->getWidth(),dib->getHeight(),0,0);
   // Palette::releaseTable(roadTable);

   // dib->blit(0,0,dib->getWidth(), dib->getHeight(), bufDIB, 0,0);

#if 0
   /*
    * Apply buffer DIB to main DIB
    */

   const UBYTE* remapTable = Palette::getColorizeTable(RGB(0,0,0), 20);

   // dib->blit(0,0,dib->getWidth(), dib->getHeight(), bufDIB, 0,0);
   dib->remapDIB(bufDIB, remapTable, 0,0, dib->getWidth(), dib->getHeight(), 0, 0);

   Palette::releaseTable(remapTable);
#endif

   /*
    * Delete DIB
    */

   // delete bufDIB;
   mapData.releaseFXDIB(bufDIB);

   // SelectObject(hdc, oldBM);
}

#if 0
void ConnectionDisplay::drawRiver(DrawDIBDC* dib, const PixelPoint& p1, const PixelPoint& p2, ColourIndex colour, const Connection* con, MapWindowData& mapData)
{
   dib->line(p1.getX(), p1.getY(), p2.getX(), p2.getY(), colour);
}

#if 0
static void thickLine(hdc, LONG x1, LONG y1, LONG x2, LONG y2, COLORREF color, int width)
{
   HPEN hNewPen = CreatePen(PS_SOLID, width, (COLORREF)color);
   HPEN hOldPen = (HPEN)SelectObject(hdc, hNewPen);

   MoveToEx(hdc, x1, y1, NULL);
   LineTo(hdc, x2, y2);
}
#endif


void ConnectionDisplay::drawRoad(DrawDIBDC* dib, const PixelPoint& p1, const PixelPoint& p2, ColourIndex colour, const Connection* con, MapWindowData& mapData)
{
  switch(mapData.getZoomEnum())
  {
    case 0:
      dib->line(p1.getX(), p1.getY(), p2.getX(), p2.getY(), colour);
      break;
    case 1:
      dib->line(p1.getX(), p1.getY(), p2.getX(), p2.getY(), colour, 2);
      break;
    case 2:
    case 3:
    case 4:
      dib->line(p1.getX(), p1.getY(), p2.getX(), p2.getY(), colour, 3);
      break;
  }
}

void ConnectionDisplay::drawRailLine(DrawDIBDC* dib, const PixelPoint& p1, const PixelPoint& p2, ColourIndex colour, const Connection* con, MapWindowData& mapData)
{

   const int space = 7;
   const int width = mapData.getZoomEnum() < 3?5:7;

   PixelPoint start(p1.getX(), p1.getY());
   PixelPoint end(p2.getX(), p2.getY());

   const  int y = p2.getY() - p1.getY();
   const  int x = p2.getX() - p1.getX();

   const  int dist = aproxDistance(x, y);

   if(dist > 0)
   {
     switch(mapData.getZoomEnum())
     {
       case 0:
       case 1:
       {
         dib->line(p1.getX(), p1.getY(), p2.getX(), p2.getY(), colour);
         break;
       }

       case 2:
       case 3:
       case 4:
       {

         {
           POINT point1;
           point1.x = p1.getX() + ((width * y) / (2 * dist));
           point1.y = p1.getY() - ((width * x) / (2 * dist));

           POINT point2;
           point2.x = p2.getX() + ((width * y) / (2 * dist));
           point2.y = p2.getY() - ((width * x) / (2 * dist));

           dib->line(point1.x, point1.y, point2.x, point2.y, colour, mapData.getZoomEnum() < 3?1:2);
         }

         {
           POINT point1;
           point1.x = p1.getX() - ((width * y) / (2 * dist));
           point1.y = p1.getY() + ((width * x) / (2 * dist));

           POINT point2;
           point2.x = p2.getX() - ((width * y) / (2 * dist));
           point2.y = p2.getY() + ((width * x) / (2 * dist));

           dib->line(point1.x, point1.y, point2.x, point2.y, colour, mapData.getZoomEnum() < 3?1:2);
         }
         break;
       }
     }
     if(mapData.getZoomEnum() > 1)
       colour = con->quality == CQ_Poor?dib->getColour(PALETTERGB(255, 255, 255)):
                con->quality == CQ_Average?dib->getColour(PALETTERGB(200, 0, 0)):
                dib->getColour(PALETTERGB(0, 0, 0));

     dib->rightAngleLines(width, space, start, end, colour);

   }
}
#endif

/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.2  2001/06/18 08:25:45  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
