/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 * graphic pipeline engine for converting physical coordinates to pixels
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "locengin.hpp"
#include "grtypes.hpp"
#include "measure.hpp"
#include "mw_data.hpp"

/*
 * Convert points to pixels and send to output
 */

LocationEngine::LocationEngine(GraphicEngine& output, const MapWindowData& mapData) :
   d_output(output),
   d_mapData(mapData)
{
}

LocationEngine::~LocationEngine()
{
}

void LocationEngine::addPoint(const Location& l)
{
   PixelPoint pix;
   d_mapData.locationToPixel(l, pix);
   d_output.addPoint(GPoint(pix.x(), pix.y()));
}

void LocationEngine::addPoint(const GPoint& p)
{
   addPoint(Location(p.x(), p.y()));
}

void LocationEngine::close()
{
   d_output.close();
}



/*
 *----------------------------------------------------------------------
 * $Log$
 *----------------------------------------------------------------------
 */
