/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef CONDISP_HPP
#define CONDISP_HPP

#ifndef __cplusplus
#error condisp.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *	Connection Drawing
 *
 *----------------------------------------------------------------------
 */

#include "gamedefs.hpp"
#include "grtypes.hpp"

class MapWindowData;
class CampaignData;
class DrawDIBDC;
class PixelPoint;
struct Connection;

class ConnectionDisplay 
{
	public:
 		ConnectionDisplay() { }
		~ConnectionDisplay() { }

		void draw(DrawDIBDC* dib, MapWindowData& mapData, const CampaignData& campData);
			// Draw all the connections onto the given DIB

	private:
		// void drawRiver(DrawDIBDC* dib, const PixelPoint& p1, const PixelPoint& p2, ColourIndex colour, const Connection* con, MapWindowData& mapData);
		// void drawRoad(DrawDIBDC* dib, const PixelPoint& p1, const PixelPoint& p2, ColourIndex colour, const Connection* con, MapWindowData& mapData);
		// void drawRailLine(DrawDIBDC* dib, const PixelPoint& p1, const PixelPoint& p2, ColourIndex colour, const Connection* con, MapWindowData& mapData);


};

#endif /* CONDISP_HPP */

