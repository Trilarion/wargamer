/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef MAPFILE_HPP
#define MAPFILE_HPP

#ifndef __cplusplus
#error mapfile.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1995, Steven Morle-Green, All Rights Reserved
 *----------------------------------------------------------------------
 *
 *	Map graphic Interface class
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 *
 *----------------------------------------------------------------------
 */


// #include "mapzoom.hpp"
#include "mappoint.hpp"
#include "array.hpp"

class DIB;
class DrawDIBDC;
class WinFileBinaryReader;

/*
 * One file on disk
 */

class MapFileDisk {
	friend class MapFileSystem;

	DWORD bfOffBits;
	LONG biWidth;
	LONG biHeight;
	UCHAR remap[256];
	LPCSTR fileName;
	WinFileBinaryReader* rFile;

public:
	MapFileDisk(LPCSTR name);
	~MapFileDisk();

	Boolean loadMapSection(DIB* dest, const RECT& r);
	void close();
	Boolean open();
};

/*
 * Class for handling display of map from a set of files
 */


class MapFileSystem {
	typedef ArrayBase::ArrayIndex MapIndex;
	enum { NoMap = ArrayBase::ArrayIndex_MAX };

	Array<MapFileDisk*> maps;			// Array of mapFiles
	MapIndex currentMap;
	RECT loadedRect;
	DIB* loadedDIB;
public:
	MapFileSystem();
	~MapFileSystem();

	Boolean displayMapSection(DrawDIBDC* dib, const MapPoint& p, MapCord fullWidth, LONG wantWidth);
};


#endif /* MAPFILE_HPP */

