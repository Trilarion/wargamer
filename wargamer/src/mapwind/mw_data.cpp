/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 * Map Window Data
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "mw_data.hpp"
#include "mw_user.hpp"     // MapSelect
// #include "grapple.hpp"
#include "gamectrl.hpp"    // For SysTick::
#include "campdint.hpp"
#include "dib.hpp"

/*
 * Constructor
 */

MapWindowData::MapWindowData(MapWindowUser* user, const CampaignData* campData) :
   d_fx(0),
   d_fxUsed(false),
   d_user(user),
   d_campData(campData),
   timedHighlight(false),
   highlightedTown(NoTown),
   highlightedUnit(NoCommandPosition),
   highlightTimer(0),
   trackedUnits(),
   currentUnit(NoUnitListID),
   currentGeneral(NoUnitListID),
   d_selectedTown(NoTown),
   d_selectedProvince(NoProvince),
   mouseLocation(0,0),
   d_ctrlKey(false),
   d_shiftKey(false),
   // drawSignal(),        // Removed 10Sep97
   // inUse(),             // Removed 10Sep97
#ifdef DEBUG
   d_gridMode(GridOff),
#endif
#if defined(CUSTOMIZE)
#if defined(EDITOR)
   editMode(PM_EditTown),
#else
   editMode(PM_Playing),
#endif
   editDial(0),
   edittingTown(NoTown),
#endif
   // d_grapples(new Grapple[MaxGrapples]),        // Removed 10Sep97
   // d_grapple(0),                                // Removed 10Sep97
   showAllRoutes(false),
   showAllAttribs(false)
//   d_showSupplyLines(false)
{
}

MapWindowData::~MapWindowData()
{
   // delete[] d_grapples;

   ASSERT(!d_fxUsed);
   delete d_fx;
   d_fx = 0;
}

/*
 * Interface to control dialog
 */

void MapWindowData::fillMapSelect(MapSelect& info) const
{
   info.selectedTown       = d_selectedTown;
   info.selectedProvince   = d_selectedProvince;
   info.trackedUnits       = &trackedUnits;
   info.currentUnit        = currentUnit;
   info.currentGeneral     = currentGeneral;
   info.mouseLocation      = mouseLocation;
   // info.grapple            = d_grapple;
   info.ctrlKey            = d_ctrlKey;
   info.shiftKey           = d_shiftKey;
}



ConstICommandPosition MapWindowData::getCurrentUnit() const
{
   ConstICommandPosition curTracked = NoCommandPosition;

   if(timedHighlight && (highlightedUnit != NoCommandPosition))
   {
      curTracked = highlightedUnit;
   }
   else if((trackedUnits.unitCount() > 0) && (currentUnit != NoUnitListID))
      curTracked = trackedUnits.getUnit(currentUnit);

   return curTracked;
}


/*
 * Timer for Highlighting units after being found
 */

void MapWindowData::setHighlightTimer()
{
   timedHighlight = true;
   highlightTimer = GameControl::getTick() + SysTick::TicksPerSecond * 5;
}

void MapWindowData::setHighlightedTown(ITown iTown)
{
   highlightedTown = iTown;
   setHighlightTimer();
}

void MapWindowData::setHighlightedUnit(ConstICommandPosition cpi)
{
   highlightedUnit = cpi;
   setHighlightTimer();
}

void MapWindowData::countTimedHighLight()
{
   if(timedHighlight && (highlightedUnit != NoCommandPosition))
   {
      if(GameControl::getTick() >= highlightTimer)
      {
         timedHighlight = false;
         highlightedTown = NoTown;
         highlightedUnit = NoCommandPosition;
      }
   }
   else
      timedHighlight = false;
}


ITown MapWindowData::getHighlightedTown() const
{
   if(timedHighlight && (highlightedTown != NoTown))
      return highlightedTown;
   else
      return d_selectedTown;
}




void MapWindowData::townSelected(ITown ob)
{
   if(ob != NoTown)
   {
      d_selectedTown = ob;
      d_selectedProvince = d_campData->getTownProvince(ob);
   }
   else
   {
      d_selectedTown = NoTown;
      d_selectedProvince = NoProvince;
   }
}

/*
 * Get and release temporary buffers for special effects
 * This allows a single buffer to be shared by different
 * display modules without frequent creation and deletion of DIBSections.
 */

DrawDIBDC* MapWindowData::getFXDIB(const DIB* dib)
{
   ASSERT(dib != 0);
   ASSERT(!d_fxUsed);

   if(dib == 0)
      return 0;

   if(d_fxUsed)
      return 0;

   d_fxUsed = true;

   if(d_fx == 0)
      d_fx = new DrawDIBDC(dib->getWidth(), dib->getHeight());
   else if( (dib->getWidth() != d_fx->getWidth()) ||
            (dib->getHeight() != d_fx->getHeight()) )
   {
      d_fx->resize(dib->getWidth(), dib->getHeight());
   }

   ASSERT(d_fx != 0);
   return d_fx;
}

void MapWindowData::releaseFXDIB(DIB* dib)
{
   ASSERT(d_fxUsed);
   ASSERT(dib == d_fx);
   d_fxUsed = false;
}


/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.2  2001/06/18 08:25:45  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
