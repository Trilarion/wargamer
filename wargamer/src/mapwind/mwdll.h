/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef MWDLL_H
#define MWDLL_H
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 * Mapwind DLL defines
 *
 *----------------------------------------------------------------------
 */

#if defined(NO_WG_DLL)
    #define MAPWIND_DLL
#elif defined(EXPORT_MAPWIND_DLL)
    #define MAPWIND_DLL __declspec(dllexport)
#else
    #define MAPWIND_DLL __declspec(dllimport)
#endif


#endif /* MWDLL_H */

