/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef MW_DATA_HPP
#define MW_DATA_HPP

#ifndef __cplusplus
#error mw_data.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *	Campaign Map Window Data
 *
 *----------------------------------------------------------------------
 */

#include <windows.h>			// For HWND, HCURSOR, etc...
#include "mapzoom.hpp"		// For MagZoom
#include "grtypes.hpp"		// For PixelPoint
#include "gamedefs.hpp"		// For ITown, ICommandPosition
#include "sync.hpp"		// Mutex, AutoEvent
#include "unitlist.hpp"		// StackedUnitList
#ifdef CUSTOMIZE
#include "mw_user.hpp"		// EditMode
#endif

class TownDisplay;
class ProvinceDisplay;
// class Grapple;
class CampaignData;
class MapWindowUser;
struct MapSelect;
class DIB;
class DrawDIBDC;

/*
 * Maximum number of grapples in MapWindow
 */

// #define MaxGrapples 4

/*
 * Common MapWindowData
 */

class MapWindowData :
	public MapZoom
{
		DrawDIBDC* d_fx;				// Effects DIB
		bool d_fxUsed;
   public:
		MapWindowUser*	d_user;
		const CampaignData*	d_campData;

		/*
	 	 * Highlighted Objects
	 	 *
	 	 * NOTE:
	 	 *		If highlightedTown was always the same as selectedTown then some
	 	 *		code could be simplifed.
	 	 *
	 	 *		Similarly highlightedUnit could be same as currentUnit
	 	 */

		bool timedHighlight;   // used for highlighing unit or town after using findTown\Leader dial
		ITown highlightedTown;
		ConstICommandPosition highlightedUnit;
		int highlightTimer;			// Counter for highlighting objects

		/*
	 	 * Current Objects
		 * Note: currentGeneral never seems to get set by anything
	 	 */
		StackedUnitList trackedUnits;
		UnitListID currentUnit;					// Entry into tracked Units
		UnitListID currentGeneral;				// Entry into trackedUnits
		ITown d_selectedTown;
		IProvince d_selectedProvince;
		Location mouseLocation;					// Physical Location of mouse pointer
		bool d_ctrlKey;
		bool d_shiftKey;

		/*
	 	 * Draw Data
	 	 */

#ifdef DEBUG
		enum GridMode {
			GridOff,					// No Grid
			GridVertical,			// Grid is orthogonal to display
			GridNorth, 				// Grid is relative to North

			GridMode_MAX = GridNorth,
			GridMode_MIN = GridOff
		} d_gridMode;				// Set to display grid
#endif

		bool showAllRoutes;
		bool showAllAttribs;
//      bool d_showSupplyLines;

		/*
	 	 * Stuff used by editor!
	 	 */

#ifdef CUSTOMIZE
		EditMode editMode;
		EditDialog* editDial;
		ITown edittingTown;
#endif

 	private:
 		MapWindowData(const MapWindowData&);
		MapWindowData& operator = (const MapWindowData&);
 	public:
 		MapWindowData(MapWindowUser* user, const CampaignData* campData);
 		virtual ~MapWindowData();

		void fillMapSelect(MapSelect& info) const;
			// Fill information for callback functions

   	ConstICommandPosition getCurrentUnit() const;
		void setHighlightTimer();
		void setHighlightedTown(ITown iTown);
		ITown getHighlightedTown() const;
		void setHighlightedUnit(ConstICommandPosition cpi);
		void countTimedHighLight();

		void toggleShowAllRoute() { showAllRoutes = !showAllRoutes; }

		void toggleShowAllAttribs() {	showAllAttribs = !showAllAttribs; }

//      void toggleShowSupplyLines() { d_showSupplyLines = !d_showSupplyLines; }
//      bool showSupplyLines() const { return d_showSupplyLines; }

#ifdef DEBUG
		void toggleGrid()
		{
			if(d_gridMode == GridMode_MAX)
				d_gridMode = GridMode_MIN;
			else
				d_gridMode = GridMode(d_gridMode + 1);
		}

		bool gridOn() const { return d_gridMode != GridOff; }
		GridMode gridMode() const { return d_gridMode; }
#endif

		void selectedProvince(IProvince ob) { d_selectedProvince = ob; }
		void townSelected(ITown ob);

		DrawDIBDC* getFXDIB(const DIB* dib);
			// Get DIB that is same size as passed DIB
			// to use for special effects
		void releaseFXDIB(DIB* dib);
			// Indicate that have finished with the FXDIB
};

#endif /* MW_DATA_HPP */

