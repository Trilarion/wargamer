/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef MW_ROUTE_HPP
#define MW_ROUTE_HPP

#ifndef __cplusplus
#error mw_route.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *	Arrows used to show unit routes
 *
 *----------------------------------------------------------------------
 */

#include "gamedefs.hpp"			// For ICommandPosition
#include "cpdef.hpp"

class DrawDIBDC;
class MapWindowData;
class Location;
class MapRouteDisplayImp;
class CampaignRoute;
class CampaignPosition;
class CampaignOrder;
class RouteList;

/*
 * Arrows
 */

struct ArrowInfo {
	int gap;
	int width;
	int height;
	int length;

	ArrowInfo(int g, int w, int h, int l)
	{
	  gap = g;
	  width = w;
	  height = h;
	  length = l;
	}
};

class MapRouteDisplay 
{
		MapRouteDisplayImp* d_imp;
	public:

		MapRouteDisplay();
		~MapRouteDisplay();

		// old method
		static void drawArrow(DrawDIBDC* dib, MapWindowData& mapData, ConstICommandPosition cpi, const Location& sLocation, const Location& eLocation, const ArrowInfo& ai);
		void drawRoute(DrawDIBDC* dib, MapWindowData& mapData, const CampaignPosition& from, const CampaignPosition& dest, Side side);
		void drawRoute(DrawDIBDC* dib, MapWindowData& mapData, const CampaignPosition& from, const CampaignOrder& order, Side side);

      // new method
		void drawRoute(DrawDIBDC* dib, MapWindowData& mapData, ConstICommandPosition& cpi);
		void drawRoute(DrawDIBDC* dib, MapWindowData& mapData, const CampaignPosition& from,
					const CampaignOrder& order, Side side, const RouteList& rl);
		void drawRoute(DrawDIBDC* dib, MapWindowData& mapData, const CampaignPosition& from,
					const CampaignPosition& dest, Side side, const RouteList& rl);
};




#endif /* MW_ROUTE_HPP */

