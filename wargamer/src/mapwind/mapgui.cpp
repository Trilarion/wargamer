/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 * Map Window User Interface
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "mapgui.hpp"
#include "mapzoom.hpp"        // For MagEnum
#include "mw_data.hpp"


#include "scenario.hpp"
#include "app.hpp"
#include "palette.hpp"
#include "resdef.h"              // For WID_Map
#include "registry.hpp"
#include "campint.hpp"           // for campaign->getMode()... should be able to remove this
#include "cscroll.hpp"
#include "dib.hpp"
#include "imglib.hpp"
#include "scn_res.h"
#include "wmisc.hpp"
#include "scn_img.hpp"
// #include "sync.hpp"
#ifdef CUSTOMIZE
#include "printmap.hpp"
#endif
#if defined(DEBUG)
#include "msgenum.hpp"
#include "todolog.hpp"
#include "logwin.hpp"
#endif

/*
 * Scroll Bar size
 */

static const int SB_Width = 15;
static const int SB_Height = 15;

/*
 * Weather Button constants
 */

static const int WB_Width = 15;
static const int WB_Height = 15;
static const int nWeatherButtons = 2;

/*
 * Constructor
 */

MapGUI::MapGUI(MapWindowData* mapData) :
   d_mapData(mapData),
   d_mapDraw(new MapDraw),
   d_mapTrack(mapData),
   d_hwndVScroll(NULL),
   d_hwndHScroll(NULL),
   d_mouseStartingLocation(0,0),
   d_wantedZoomLevel(0),
   d_zooming(False),
   d_buttonPressed(False),
   d_ctrlKey(False),
   d_mouseClickLocation(0,0),
   d_gotClickLocation(False)
{
   initCustomScrollBar(APP::instance());
}


/*--------------------------------------------------------------------
 * Class Stuff
 */

const char MapGUI::s_className[] = "MapWindow";

/*
 * registry Strings
 */

const char MapGUI::mapWindowRegName[] = "mapWindow";
const char MapGUI::regNameZoom[] = "Zoom";
const char MapGUI::regNamePosition[] = "Position";



ATOM MapGUI::classAtom = 0;

ATOM MapGUI::registerClass()
{
   if(!classAtom)
   {
      WNDCLASS wc;

      wc.style = CS_OWNDC | CS_NOCLOSE;
      wc.lpfnWndProc = baseWindProc;
      wc.cbClsExtra = 0;
      wc.cbWndExtra = sizeof(MapGUI*);
      wc.hInstance = APP::instance();
      wc.hIcon = NULL;
      wc.hCursor = LoadCursor(NULL, IDC_ARROW);
      wc.hbrBackground = NULL;
      wc.lpszMenuName = NULL;
      wc.lpszClassName = s_className;
      classAtom = RegisterClass(&wc);
   }

   ASSERT(classAtom != 0);

   return classAtom;
}



void MapGUI::makeWindow(HWND parent)
{
#ifdef DEBUG_MAPWIND_MESSAGES
   debugLog("+MapGUI::createWindow(%p)... start\n", parent);
#endif

   /*
    * Register the Window Class
    */

   registerClass();

   /*
    * Create the window
    */

   HWND hWnd = createWindow(
      WS_EX_CONTEXTHELP,
      // getClassName(),         // Class
      "Map Window",           // Name
         WS_CHILD    |
         WS_MAXIMIZE |        // Begin Maximized
         WS_CLIPSIBLINGS |
         WS_CLIPCHILDREN,     /* style */
      10,60,200,200,          // Initial Rectangle
      parent,                 /* parent window */
      (HMENU) WID_Map         /* menu handle (Actually it's a child Window ID) */
      // APP::instance()      /* program handle */
   );

   ASSERT(hWnd != NULL);

   MapPoint startPoint;
   if(!getRegistry(regNamePosition, &startPoint, sizeof(startPoint), mapWindowRegName))
   {
     startPoint.setX(d_mapData->getFullWidth() / 2);
     startPoint.setY(d_mapData->getFullHeight() / 2);
   }
   if(!getRegistry(regNameZoom, &d_wantedZoomLevel, sizeof(d_wantedZoomLevel), mapWindowRegName))
      d_wantedZoomLevel = 0;

   doZoom(MapPoint(startPoint.getX(), startPoint.getY()));

#ifdef DEBUG_MAPWIND_MESSAGES
   debugLog("-MapGUI::createWindow... finished\n");
#endif

}

MapGUI::~MapGUI()
{
}

/*
 * Message Processor
 */

LRESULT MapGUI::procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
#ifdef DEBUG_MAPWIND_MESSAGES
   debugLog("MapGUI::procMessage(%s)\n",
      getWMdescription(hWnd, msg, wParam, lParam));
#endif

   switch(msg)
   {
      HANDLE_MSG(hWnd, WM_CREATE,   onCreate);
      HANDLE_MSG(hWnd, WM_DESTROY,  onDestroy);
      HANDLE_MSG(hWnd, WM_PAINT,    onPaint);
      HANDLE_MSG(hWnd, WM_SIZE,     onSize);
      HANDLE_MSG(hWnd, WM_HSCROLL,  onHScroll);
      HANDLE_MSG(hWnd, WM_VSCROLL,  onVScroll);

      HANDLE_MSG(hWnd, WM_LBUTTONDOWN, onLButtonDown);
      HANDLE_MSG(hWnd, WM_RBUTTONDOWN, onRButtonDown);
      HANDLE_MSG(hWnd, WM_LBUTTONUP, onLButtonUp);

      HANDLE_MSG(hWnd, WM_MOUSEMOVE, onMouseMove);
      HANDLE_MSG(hWnd, WM_SETCURSOR, onSetCursor);

      HANDLE_MSG(hWnd, WM_COMMAND, onCommand);
#if !defined(EDITOR)
      HANDLE_MSG(hWnd, WM_TIMER, onTimer);  // animation timer
#endif

   default:
      return defProc(hWnd, msg, wParam, lParam);
   }
}

/*
 * In Create Message Handler
 */

BOOL MapGUI::onCreate(HWND hWnd, CREATESTRUCT* lpCreateStruct)
{
#ifdef DEBUG_MAPWIND_MESSAGES
   debugLog("+MapGUI::onCreate(%p)\n", hWnd);
#endif


   RECT r;
   GetClientRect(hWnd, &r);

   d_hwndVScroll = csb_create(
      0, WS_CHILD | SBS_VERT,
      r.right-SB_Width,                           /* horizontal position       */
      0,                           /* vertical position         */
      SB_Width,                         /* width of the scroll bar   */
      r.bottom-SB_Height,               /* default height            */
      hWnd,                   /* handle of main window          */
      APP::instance()                  /* instance owning this window    */
   );

   ASSERT(d_hwndVScroll != NULL);

   /*
    * Get imagelist for scroll buttons
    * set imagelist, scrollbk and thumbbk
    */

   csb_setButtonIcons(d_hwndVScroll, ScenarioImageLibrary::get(ScenarioImageLibrary::VScrollButtons));
   csb_setThumbBk(d_hwndVScroll, scenario->getScenarioBkDIB(Scenario::BackPatterns::VScrollThumbBackground));
   csb_setScrollBk(d_hwndVScroll, scenario->getScenarioBkDIB(Scenario::BackPatterns::VScrollBackground));
   csb_setBorder(d_hwndVScroll, &scenario->getBorderColors());

   d_hwndHScroll = csb_create(
      0, WS_CHILD | SBS_HORZ,
      0,                           /* horizontal position       */
      r.bottom-SB_Height,                           /* vertical position         */
      r.right-SB_Width,                         /* width of the scroll bar   */
      SB_Height,               /* default height            */
      hWnd,                   /* handle of main window          */
      APP::instance()                  /* instance owning this window    */
   );

   ASSERT(d_hwndHScroll != NULL);

   csb_setButtonIcons(d_hwndHScroll, ScenarioImageLibrary::get(ScenarioImageLibrary::HScrollButtons));
   csb_setThumbBk(d_hwndHScroll, scenario->getScenarioBkDIB(Scenario::BackPatterns::HScrollThumbBackground));
   csb_setScrollBk(d_hwndHScroll, scenario->getScenarioBkDIB(Scenario::BackPatterns::HScrollBackground));
   csb_setBorder(d_hwndHScroll, &scenario->getBorderColors());

   /*
    * initialize animations and set animation timer
    */

#if !defined(EDITOR)
   d_mapDraw->initAnimations(d_mapData);

   const UINT timeOut = 100;  //  10 per sec
   UINT result = SetTimer(hWnd, 0, timeOut, NULL);
   ASSERT(result);
#endif // EDITOR

#ifdef DEBUG_MAPWIND_MESSAGES
   debugLog("-MapGUI::onCreate()... End\n");
#endif

   return TRUE;
}

/*
 * Animation timer
 */

#if !defined(EDITOR)
void MapGUI::onTimer(HWND hwnd, UINT id)
{
  HDC hdc = GetWindowDC(hwnd);
  ASSERT(hdc);

  HPALETTE oldPal = SelectPalette(hdc, Palette::get(), TRUE);
  RealizePalette(hdc);

  d_mapDraw->animate(hdc);

  SelectPalette(hdc, oldPal, TRUE);
  ReleaseDC(hwnd, hdc);
}
#endif // EDITOR

/*
 * On Command Message Handler
 * Forwards message to Parent (MainWindow).
 */

void MapGUI::onCommand(HWND hWnd, int id, HWND hwndCtl, UINT codeNotify)
{
   // FORWARD_WM_COMMAND(GetParent(hWnd), id, hwndCtl, codeNotify, PostMessage);
   FORWARD_WM_COMMAND(GetParent(hWnd), id, hwndCtl, codeNotify, SendMessage);
}

/*
 * On Destroy Message Handler
 */

void MapGUI::onDestroy(HWND hWnd)
{
#ifdef DEBUG
   debugLog("+MapGUI::onDestroy()\n");
#endif

   /*
    * Save settings to registry
    */

   setRegistry(regNameZoom, &d_wantedZoomLevel, sizeof(d_wantedZoomLevel), mapWindowRegName);
   MapPoint mapPos(d_mapData->getMidX(), d_mapData->getMidY());
   setRegistry(regNamePosition, &mapPos, sizeof(mapPos), mapWindowRegName);


   HWND parent = GetParent(hWnd);
   ASSERT(parent != NULL);
   FORWARD_WM_PARENTNOTIFY(parent, WM_DESTROY, hWnd, 0, SendMessage);

   KillTimer(hWnd, 0);

#ifdef DEBUG
   debugLog("-MapGUI::onDestroy()... End\n");
#endif
}

void MapGUI::onLButtonDown(HWND hwnd, BOOL fDoubleClick, int x, int y, UINT keyFlags)
{
   PixelPoint p = PixelPoint(x, y);

   d_mouseStartingLocation = p;

   d_buttonPressed = True;
   d_mapTrack.d_draggingStatus = False;

#ifdef DEBUG_MAPWIND_MESSAGES
   debugLog("MapGUI::onLButtonDown(%d,%d)\n", x, y);
#endif

   if(d_zooming)
   {
      doZoom(p);
   }
   else
      d_mapTrack.onLButtonDown(hwnd, p, keyFlags);

}



void MapGUI::onLButtonUp(HWND hwnd, int x, int y, UINT keyFlags)
{
  d_buttonPressed = False;

#if !defined(EDITOR)
   if(d_mapTrack.d_draggingStatus)
   {
     MapSelect info;
     d_mapData->fillMapSelect(info);
     d_mapData->d_user->onEndDrag(info);
   }
   else
   {
     MapSelect info;
     d_mapData->fillMapSelect(info);
     d_mapData->d_user->onLClick(info);
   }

//    else if(campaign->getMode() == CampaignInterface::CM_UNITS)
//    {
//       PixelPoint p;        //???? Un-initialised point
//       d_mapTrack.clickUnit(p);
//    }
#endif

  d_mapTrack.d_draggingStatus = False;

//  ReleaseCapture();
}



void MapGUI::onHScroll(HWND hwnd, HWND hwndCtl, UINT code, int pos)
{
   int minP;
   int maxP;
#ifdef OLD_SCROLL
   GetScrollRange(hwnd, SB_HORZ, &minP, &maxP);
#else
   csb_getRange(hwndCtl, &minP, &maxP);
#endif

   MapCord newX = d_mapData->getMidX();
   MapCord pageWidth = d_mapData->getViewWidth();

   switch(code)
   {
   case SB_PAGELEFT:
      newX -= pageWidth;
      break;
   case SB_PAGERIGHT:
      newX += pageWidth;
      break;
   case SB_LINELEFT:
      newX -= pageWidth / 16;
      break;
   case SB_LINERIGHT:
      newX += pageWidth / 16;
      break;
   case SB_THUMBPOSITION:
      newX = pos + pageWidth / 2;
      break;
   default:
      return;
   }

   if(d_mapData->setMapX(newX))
   {
      setXScroll();
      // requestRedraw(TRUE);
      forceDraw();
      // d_mapDraw->forceDraw();
   }

   d_mapData->d_user->updateZoom();
}

void MapGUI::onVScroll(HWND hwnd, HWND hwndCtl, UINT code, int pos)
{
   int minP;
   int maxP;
#ifdef OLD_SCROLL
   GetScrollRange(hwnd, SB_VERT, &minP, &maxP);
#else
   csb_getRange(hwndCtl, &minP, &maxP);
#endif

   MapCord newY = d_mapData->getMidY();
   MapCord pageHeight = d_mapData->getViewHeight();

   switch(code)
   {
   case SB_PAGELEFT:
      newY -= pageHeight;
      break;
   case SB_PAGERIGHT:
      newY += pageHeight;
      break;
   case SB_LINELEFT:
      newY -= pageHeight / 16;
      break;
   case SB_LINERIGHT:
      newY += pageHeight / 16;
      break;
   case SB_THUMBPOSITION:
      newY = pos + pageHeight / 2;
      break;
   default:
      return;
   }

   if(d_mapData->setMapY(newY))
   {
      setYScroll();
      forceDraw();
      // requestRedraw(TRUE);
      // d_mapDraw->forceDraw();
   }

   d_mapData->d_user->updateZoom();
}

void MapGUI::doPopupMenu(HWND hwnd, PixelPoint& pt)
{
   HMENU hmenu;            // top-level menu
   HMENU hmenuTrackPopup;  // pop-up menu

   // d_mapData->locationToPixel(d_mouseClickLocation, pt);

   d_mapData->pixelToLocation(pt, d_mouseClickLocation);
   d_gotClickLocation = True;


   // Load the menu resource.

   hmenu = LoadMenu(APP::instance(), MAKEINTRESOURCE(MENU_TOWNEDITPOPUP));

   ASSERT(hmenu != NULL);

   // TrackPopupMenu cannot display the top-level menu, so get
   // the handle of the first pop-up menu.

   hmenuTrackPopup = GetSubMenu(hmenu, 0);

   ASSERT(hmenuTrackPopup != NULL);

   // Display the floating pop-up menu. Track the right mouse
   // button on the assumption that this function is called
   // during WM_CONTEXTMENU processing.

   POINT p = pt;
   ClientToScreen(hwnd, &p);

   int result = TrackPopupMenuEx(hmenuTrackPopup,
         TPM_LEFTALIGN | TPM_RIGHTBUTTON | TPM_RETURNCMD | TPM_NONOTIFY,
         p.x, p.y, hwnd, NULL);

   DestroyMenu(hmenu);

   if(result != 0)
   {
      FORWARD_WM_COMMAND(hwnd, result, 0, NULL, SendMessage);
      // SendMessage(hwnd, WM_COMMAND, result, 0);
   }

   d_gotClickLocation = False;
}



void MapGUI::onRButtonDown(HWND hwnd, BOOL fDoubleClick, int x, int y, UINT keyFlags)
{
#ifdef DEBUG_MAPWIND_MESSAGES
   debugLog("MapGUI::onRButtonDown()\n");
#endif

   if( (d_mapData->trackedUnits.unitCount() > 0 && d_mapData->currentUnit != NoUnitListID) ||
       (d_mapData->d_selectedTown != NoTown) )
   {
       MapSelect info;
       d_mapData->fillMapSelect(info);
       d_mapData->d_user->onRClick(info);
   }
   else
   {
     PixelPoint p(x, y);
     doPopupMenu(hwnd, p);
   }
}


BOOL MapGUI::onSetCursor(HWND hwnd, HWND hwndCursor, UINT codeHitTest, UINT msg)
{
   if(d_zooming)
   {
      ASSERT(scenario->getZoomCursor() != NULL);
      SetCursor(scenario->getZoomCursor());
      return TRUE;
   }
   else if(d_mapTrack.setCursor())
      return TRUE;
   else
      return FORWARD_WM_SETCURSOR(hwnd, hwndCursor, codeHitTest, msg, defProc);
}


void MapGUI::onPaint(HWND hWnd)
{
#ifdef DEBUG__MAPWIND_MESSAGES
   debugLog("+MapGUI::onPaint.. start\n");
#endif
#ifdef DEBUG_MAP_PAINTING
   logWindow->printf("Painting map");
#endif
   PAINTSTRUCT ps;
   HDC hdc = BeginPaint(hWnd, &ps);

   HPALETTE oldPal = SelectPalette(hdc, Palette::get(), TRUE);
   RealizePalette(hdc);

   /*
    * Display bitmap in centre of client
    */

   RECT cRect;
   GetClientRect(hWnd, &cRect);

   d_mapDraw->draw(*d_mapData, *d_mapData->d_campData);
   d_mapDraw->paint(hdc, cRect);

   SelectPalette(hdc, oldPal, TRUE);
   RealizePalette(hdc);
   EndPaint(hWnd, &ps);

   // redraw map to get rid of any animations
#if !defined(EDITOR)
   {
      // LockData lock(this);
      // d_mapDraw->draw(*d_mapData, *d_mapData->d_campData);
   }
#endif

#ifdef DEBUG_MAPWIND_MESSAGES
   debugLog("-MapGUI::onPaint.. finish\n");
#endif
}

#if defined(CUSTOMIZE)
void MapGUI::printMap()
{
  PrintMap::printMap(d_mapData, getHWND());
}
#endif

/*
 * onSize called so that tinyMap can have its rectangle adjusted
 */

void MapGUI::onSize(HWND hwnd, UINT state, int cx, int cy)
{
#ifdef DEBUG_MAPWIND_MESSAGES
   debugLog("+MapGUI::onSize(%d,%d)\n", cx, cy);
#endif

   /*
    * Work out where we go in relation to clientWindow
    */

   // campWind->positionMapWindow();

   if(state == SIZE_MAXIMIZED)
   {
#ifdef DEBUG_MAPWIND_MESSAGES
      debugLog("MapWindow is maximized\n");
#endif
   }
   else if(state == SIZE_RESTORED)
   {
#ifdef DEBUG_MAPWIND_MESSAGES
      debugLog("MapWindow is restored\n");
#endif
   }

   SetWindowPos(d_hwndVScroll, HWND_TOP, cx-SB_Width, 0, SB_Width, cy, SWP_SHOWWINDOW);
   SetWindowPos(d_hwndHScroll, HWND_TOP, 0, cy-SB_Height, cx-SB_Width, SB_Height, SWP_SHOWWINDOW);

#if 0
   HWND hButton = GetDlgItem(hWnd, IDM_WEATHERWINDOW);
   ASSERT(hButton != NULL);
   MoveWindow(hButton, cx-WB_Width, cy-WB_Height, WB_Width, WB_Height, TRUE);
#endif

   MapCord x = d_mapData->getMidX();
   MapCord y = d_mapData->getMidY();

   if(state != SIZE_MINIMIZED)
     setMapSize();
   setMapPos(x, y);


#ifdef DEBUG_MAPWIND_MESSAGES
   debugLog("-MapGUI::onSize... finished\n");
#endif
}


/*----------------------------------------------------------
 * Mouse Tracking
 */

void MapGUI::onMouseMove(HWND hwnd, int x, int y, UINT keyFlags)
{
   // set keyboard focus to check for cntrlkey

   //------- Disable this to see if it is causing the odd effects with log windows
   // SetFocus(APP::getMainHWND());

   d_mapData->d_ctrlKey = !!(keyFlags & MK_CONTROL);     // !! converts it to a bool
   d_mapData->d_shiftKey = !!(keyFlags & MK_SHIFT);

   PixelPoint p = PixelPoint(x, y);

   if(d_buttonPressed && !d_mapTrack.d_draggingStatus)// && d_mapTrack.isPlayerUnit())
   {
      if(d_mouseStartingLocation.getX() < x - 5 ||
        d_mouseStartingLocation.getX() > x + 5 ||
        d_mouseStartingLocation.getY() < y - 5 ||
        d_mouseStartingLocation.getY() > y + 5)
      {
         MapSelect info;
         d_mapData->fillMapSelect(info);
         if(d_mapData->d_user->onStartDrag(info))
            d_mapTrack.d_draggingStatus = True;
      }
   }

   d_mapData->pixelToLocation(p, d_mapData->mouseLocation);

   if(d_zooming)
   {
      d_mapData->d_selectedTown = NoTown;
      d_mapData->d_selectedProvince = NoProvince;
   }
   else
   {
      if(d_mapTrack.track(hwnd, x, y, keyFlags))
      {
         requestRedraw(FALSE);
      }
   }
}

void MapGUI::setXScroll()
{
   RECT r;
   GetClientRect(hWnd, &r);
   csb_setPosition(d_hwndHScroll, d_mapData->getTopX());
   csb_setPage(d_hwndHScroll, d_mapData->pixelToMap(r.right - r.left));

}

void MapGUI::setYScroll()
{
   RECT r;
   GetClientRect(hWnd, &r);
   csb_setPosition(d_hwndVScroll, d_mapData->getTopY());
   csb_setPage(d_hwndVScroll, d_mapData->pixelToMap(r.bottom - r.top));

}

void MapGUI::setScrollRange()
{
  RECT r;
  GetClientRect(hWnd, &r);
  csb_setRange(d_hwndHScroll, 0, d_mapData->getFullWidth()); //- pixelToMap(r.right - r.left));
  csb_setRange(d_hwndVScroll, 0, d_mapData->getFullHeight()); //- pixelToMap(r.bottom - r.top));
}



void MapGUI::setMapPos(MapCord x, MapCord y)
{
#ifdef DEBUG_MAPWIND_MESSAGES
   debugLog("MapWindow::setMapPos(%ld, %ld)\n", (long) x, (long) y);
#endif

   d_mapData->setMapX(x);
   setXScroll();
   d_mapData->setMapY(y);
   setYScroll();
   // d_mapDraw->forceDraw();
   forceDraw();
   // requestRedraw(TRUE);


#if !defined(EDITOR)
   d_mapData->d_user->updateZoom();
#endif   // !EDITOR
}

/*
 * Set up little map size and auto-update the size
 * Clip to window as neccessary
 *
 * x,y are the coordinates of the centre to display
 */

void MapGUI::setMapSize()
{
   RECT r;
   GetClientRect(hWnd, &r);

   /*
    * Subtract scroll bars (Added 13Feb97)
    */

   r.right -= SB_Width;
   r.bottom -= SB_Height;

   if(r.right > 0 && r.bottom > 0)
   {

     PixelPoint p = PixelPoint(r.right, r.bottom);
      d_mapData->setViewSize(p);

#ifdef DEBUG_MAPWIND_MESSAGES
     debugLog("MapWindow::setMapSize, %ld,%ld\n", (long) p.getX(), (long) p.getY());
#endif

     d_mapDraw->setSize(p);
   }
}

void MapGUI::requestRedraw(BOOL all)
{
   // RWLock lock;
   // lock.startWrite();

   // LockData lock(this);


   d_mapDraw->requestRedraw(all);

   //------ Do this in the onPaint function
   // d_mapDraw->draw(*d_mapData, *d_mapData->d_campData);

   RECT r;
   GetClientRect(getHWND(), &r);
   SetRect(&r, r.left, r.top, r.right-SB_Width, r.bottom-SB_Height);
   InvalidateRect(getHWND(), &r, FALSE);

   //------ Can not do this here or else we get deadlock!
   // UpdateWindow(getHWND());

   d_mapData->d_user->mapRedrawn();

   // lock.endWrite();
}

void MapGUI::forceDraw()
{
   // d_mapDraw->forceDraw();
   requestRedraw(TRUE);
}

#if defined(CUSTOMIZE)
void MapGUI::setTrackMode(TrackMode mode)
{
   d_zooming = FALSE;
   d_mapTrack.trackMode(mode);
}
#endif


Location MapGUI::getClickLocation() const
{
   if(d_gotClickLocation)
   {
      // d_gotClickLocation = False;
      return d_mouseClickLocation;
   }
   else
   {
      Location l;
      d_mapData->mapToLocation( MapPoint(d_mapData->getMidX(), d_mapData->getMidY()), l);
      return l;
   }
}


/*
 * Zoom to a Map Coordinate
 */

void MapGUI::doZoom(const MapPoint& mc)
{
   d_mapData->setZoomEnum(d_wantedZoomLevel);

   setMapSize();
   setScrollRange();
   setMapPos(mc.getX(), mc.getY());
   // d_mapDraw->forceDraw();
   forceDraw();
   // requestRedraw(TRUE);

   d_zooming = FALSE;

#if !defined(EDITOR)
   d_mapData->d_user->updateZoom();
#endif   // !EDITOR

   d_mapData->d_user->magnificationUpdated();
}


void MapGUI::doZoom(MagEnum magLevel, const MapPoint& mc)
{
   d_wantedZoomLevel = magLevel;
   doZoom(mc);
}

void MapGUI::doZoom(const PixelPoint& p)
{
   MapPoint mc;
   d_mapData->pixelToMap(p, mc);
   doZoom(mc);
}

void MapGUI::toggleZoomMode()
{
   d_zooming = !d_zooming;
   if(d_zooming)
      d_wantedZoomLevel = d_mapData->getZoomInEnum();
   d_mapData->d_user->magnificationUpdated();
}

void MapGUI::zoomOut()
{
   ASSERT(d_mapData->canZoomOut());
   zoomOut(d_mapData->getZoomOutEnum());
}

void MapGUI::zoomOut(MagEnum magLevel)
{
   d_wantedZoomLevel = magLevel;
   MapPoint mc = MapPoint(d_mapData->getMidX(), d_mapData->getMidY());
   doZoom(mc);
}

/*
 * Zoom to a specified zoom level
 */

void MapGUI::zoomTo(MagEnum magLevel)
{
   if(d_zooming)
   {
      d_zooming = FALSE;
   }
   else
   {
      /*
       * Zoom out
       */

      if(magLevel < d_mapData->getZoomEnum())
         zoomOut(magLevel);
      else
      {
         d_zooming = TRUE;
         d_wantedZoomLevel = magLevel;
      }
   }

   d_mapData->d_user->magnificationUpdated();
}

void MapGUI::centerOnMap(const Location& l, MagEnum magLevel)
{
  MapPoint mp;
  d_mapData->locationToMap(l, mp);

  if(magLevel > d_mapData->getZoomEnum())
    doZoom(magLevel, mp);

  else
    setMapPos(mp.getX(), mp.getY());
}


/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.2  2001/06/18 08:25:45  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
