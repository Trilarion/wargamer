/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
#ifndef MAPWIND_H
#define MAPWIND_H

#ifndef __cplusplus
#error mapwind.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1995, Steven Morle-Green, All Rights Reserved
 *----------------------------------------------------------------------
 *
 * Map Window
 *
 *----------------------------------------------------------------------
 */

#include "mwdll.h"
#include <windef.h>           // For HWND
#include "mappoint.hpp"
#include "gamedefs.hpp"
#include "mapzoom.hpp"        // for magenum
#include "mw_user.hpp"
// #include "mw_data.hpp"
// #include "mapgui.hpp"
#include "wind.hpp"

/*
 * Undefined classes
 */

class CampaignData;

class OrderTB;
class TinyMapWindow;
struct MapSelect;
class MapDisplayObject;

class MapGUI;
class MapWindowData;

class DrawDIBDC;
struct ArrowInfo;
class Location;

class MapWindowUser;

class CampaignPosition;
class CampaignOrder;
class RouteList;

/*==================================================================
 * Main Map Window... coordinates the different parts:
 *    mapzoom : zooming and file
 *    mapdraw : drawing
 *    mapgui  : User Interface
 */

class MAPWIND_DLL MapWindow : public Window
    // public WindowBaseND
   // public SuspendableWindow
   // private MapWindowData
{
   MapGUI* d_mapGui;                      // User Interface
   MapWindowData* d_mapData;


private:
   MapWindow(const MapWindow& map);                   // Unimplemented
   MapWindow& operator = (const MapWindow& map);      // Unimplemented

   MapWindow(MapWindowUser* user, const CampaignData* data);

   void init(HWND parent);

public:
   ~MapWindow();
   static MapWindow* make(MapWindowUser* user, const CampaignData* data, HWND parent);

       virtual HWND getHWND() const;
        virtual bool isVisible() const;
        virtual bool isEnabled() const;
        virtual void show(bool visible);
        virtual void enable(bool enable);

   // HWND getHWND() const;
   // HWND getHandle() const { return getHWND(); }

   void setHighlightedTown(ITown iTown);
   void setHighlightedUnit(ConstICommandPosition cpi);
   ITown getHighlightedTown() const;

#if defined(CUSTOMIZE)
   void printMap();
#endif

   void setMapPos(MapCord x, MapCord y);


   // MAPWIND_DLL static ATOM registerClass();


   /*
    * User Interface Functions
    */


   MagEnum getZoomEnum() const;
   Boolean canZoomOut() const;
   Boolean canZoomIn() const;
   BOOL isZooming() const;

   void initZoom(MagEnum magLevel);
      // Initialise zooming to specific zoom level
      // If already zooming, then cancel it
      // If zoom level lower than current level, then cancel

   void initZoomIn();
      // Initialise zoom to next zoom level

   void zoomOut();
      // Immediately zoom out to next level, at current coordinates

   void zoomTo(MagEnum magLevel, const MapPoint& mc);
      // Immediately zoom to given location and zoom level

   void zoomTo(MagEnum magLevel);
      // Immediately zoom to zoom level, centering on current coordinates

   void zoomTo(const MapPoint& mc);
      // Immediately zoom to given coordinates at current zoom level

   void centerOnMap(const Location& l, MagEnum level);
      // center map on this location

   void locationToMap(const Location& lc, MapPoint& mc) const;
   void locationToPixel(const Location& lc, PixelPoint& pc) const;
   MapCord getTopX() const;
   MapCord getTopY() const;
   MapCord getFullWidth() const;
   MapCord getFullHeight() const;
   MapCord getViewWidth() const;
   MapCord getViewHeight() const;
#ifdef DEBUG
   void toggleGrid();
#endif
   void toggleShowAllRoute();
   void toggleShowAllAttribs();


   /*
    * Draw Functions
    */

   void requestRedraw(BOOL all);
   void forceDraw();

#ifdef DEBUG
   BOOL isGridOn() const;
#endif

#if !defined(EDITOR)
   void drawRoute(DrawDIBDC* dib, const CampaignPosition& from, const CampaignOrder& order, Side side);
   void drawRoute(DrawDIBDC* dib, const CampaignPosition& from, const CampaignOrder& order, Side side, const RouteList& rl);
#endif

#if defined(CUSTOMIZE)
   /*
    * Interface to editor
    */

   void townSelected(ITown ob);
   void provinceSelected(IProvince ob);
   void editDestroyed();
   EditMode getEditMode() const;
   void setEditMode(EditMode newMode);
   EditDialog* getEditDial();
   void setEditDial(EditDialog* dial);
   ITown getEdittingTown() const;
   void setEdittingTown(ITown itown);

   void setTrackMode(TrackMode mode);
   Location getClickLocation() const;
#endif

   // const MapWindowData& mapData() const { return *this; }
   // MapWindowData& mapData() { return *this; }
   const MapWindowData& mapData() const { return *d_mapData; }
   MapWindowData& mapData() { return *d_mapData; }
};



#endif /* MAPWIND_H */

/*
 *----------------------------------------------------------------------
 * $Log$
 *----------------------------------------------------------------------
 */
