/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 * Draw Inserts
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "inserts.hpp"
#include "mytypes.h"
#include "grtypes.hpp"
#include "scenario.hpp"
#include "fonts.hpp"
#include "dib.hpp"
#include "palette.hpp"
#include "mw_data.hpp"
#include "campdint.hpp"
#include "town.hpp"
#include "scenario.hpp"
#include "bmp.hpp"
#include "colours.hpp"
#include "myassert.hpp"

#define PREDRAWN     // Offscreen locations are predrawn on the map so need to do anything

/*---------------------------------------------------------------
 * Private Implementation
 */

class InsertDisplayImp
{
   public:
      InsertDisplayImp();  // DrawDIBDC* dib, const MapWindowData& mapData);
      ~InsertDisplayImp();

      void init(DrawDIBDC* dib, const MapWindowData& mapData);
      void drawInsert(IProvince iProv);

   private:
#if !defined(PREDRAWN)
//      CPalette::RemapTablePtr d_insertRemapTable;
      CPalette::RemapTablePtr d_insertShadow;
      ColourIndex d_insertTopBorder;
      ColourIndex d_insertBottomBorder;
#endif
      Font d_insertFont;
      int d_fontHeight;

      DrawDIBDC* d_dib;
      const MapWindowData* d_mapData;

#if !defined(PREDRAWN)
      static DIB* s_texture;
      static int s_refCount;

      static const char s_offScreenTextureName[];
#endif
};

#if !defined(PREDRAWN)
const char InsertDisplayImp::s_offScreenTextureName[] = "OffSiteMaps.bmp";
DIB* InsertDisplayImp::s_texture = 0;
int InsertDisplayImp::s_refCount = 0;
#endif

InsertDisplayImp::~InsertDisplayImp()
{
   ASSERT(d_dib != 0);
//   d_dib->restoreDC();
   d_insertFont.reset();
   d_dib = 0;

#if !defined(PREDRAWN)
   ASSERT(s_refCount > 0);
   if (--s_refCount == 0)
   {
      delete s_texture;
      s_texture = 0;
   }
#endif
   d_mapData = 0;
}

InsertDisplayImp::InsertDisplayImp() :
   // d_insertRemapTable(0),
#if !defined(PREDRAWN)
   d_insertShadow(0),
   d_insertTopBorder(0),
   d_insertBottomBorder(0),
#endif
   d_insertFont(),
   d_fontHeight(0),
   d_dib(0),
   d_mapData(0)
   // d_texture(0)
{
#if !defined(PREDRAWN)
   ++s_refCount;
#endif
}

void InsertDisplayImp::init(DrawDIBDC* dib, const MapWindowData& mapData)
{
   d_mapData = &mapData;
   d_dib = dib;

   ASSERT(d_dib != 0);
   ASSERT(d_mapData != 0);

#if !defined(PREDRAWN)

   if (s_texture == 0)
   {
      CString fileName(scenario->makeScenarioFileName(s_offScreenTextureName));
      s_texture = BMP::newDIB(fileName, BMP::RBMP_Normal);
   }

   // d_dib->saveDC();

//    COLORREF cref;
//    if(!scenario->getColour("MapInsert", cref))
//       cref = RGB(255,255,255);
//
//    COLORREF dark = Palette::brightColour(cref, 0, 128);
//    COLORREF bright = Palette::brightColour(cref, 32, 255);
//
//    d_insertBottomBorder = d_dib->getColour(dark);
//    d_insertTopBorder = d_dib->getColour(bright);

   d_insertBottomBorder = d_dib->getColour(Colors::Black);
   d_insertTopBorder = d_dib->getColour(Colors::Black);

//   d_insertRemapTable = d_dib->getPalette()->getColorizeTable(cref, 80);
   d_insertShadow = d_dib->getPalette()->getDarkRemapTable(30);
#endif
}


void InsertDisplayImp::drawInsert(IProvince iProv)
{
   ASSERT(d_mapData);
   ASSERT(d_dib);

   const int InsertWidth = 90;
   const int InsertHeight = 60;

   LONG magLevel = d_mapData->getZoomLevel();
   int width = MulDiv(magLevel * InsertWidth, d_mapData->displayWidth(), BaseDisplayWidth);
   int height = MulDiv(magLevel * InsertHeight, d_mapData->displayWidth(), BaseDisplayWidth);

   const Province* prov = &d_mapData->d_campData->getProvince(iProv);

   const Location& l = prov->getLocation();
   PixelPoint p;
   d_mapData->locationToPixel(l, p);

   LONG x1 = p.x() - width/2;
   LONG y1 = p.y() - height/2;

#if !defined(PREDRAWN)
   /*
    * Changed 12Aug99 : Use texture instead of transparency
    */

   // Draw texture...

   ASSERT(s_texture);
   d_dib->rect(x1+1, y1+1, width-2, height-2, s_texture);

//   // dib->rect(x1, y1, width, height, dib->getColour(PALETTERGB(192, 192, 32)));
//   d_dib->remapRectangle(d_insertRemapTable, x1+1, y1+1, width-2, height-2);

   // Draw Shadow

   int shadowWidth = (magLevel * 2 * d_mapData->displayWidth()) / BaseDisplayWidth;
   int shadowHeight = shadowWidth;

   if(shadowWidth > 0)
   {
      d_dib->remapRectangle(d_insertShadow, x1+shadowWidth, y1+height, width, shadowHeight);
      d_dib->remapRectangle(d_insertShadow, x1+width, y1+shadowHeight, shadowWidth, height-shadowHeight);
   }

   // Draw Frame

   d_dib->frame(x1, y1, width, height, d_insertTopBorder, d_insertBottomBorder);

#else    // PREDRAWN
#ifdef DEBUG
//   d_dib->frame(x1, y1, width, height, d_dib->getColour(RGB(255,0,255)));
#endif   // DEBUG
#endif   // PREDRAWN

   const char* name = prov->getName();
   ASSERT(name != 0);
   if(name != 0)
   {
      LONG fontHeight = magLevel * 10;
      fontHeight = (fontHeight * d_mapData->displayWidth()) / BaseDisplayWidth;
      fontHeight = maximum(fontHeight, MinFontHeight);

      if((d_fontHeight != fontHeight) || (d_insertFont.getHandle() == NULL))
      {
         d_fontHeight = fontHeight;

         LogFont lf;
         lf.height(fontHeight);
         lf.weight(FW_MEDIUM);

         // const char* fontName;

         if(fontHeight > 20)
            lf.face(scenario->fontName(Font_Decorative));
         else
            lf.face(scenario->fontName(Font_Normal));

         lf.italic(true);
         lf.underline(true);

         d_insertFont.set(lf);
      }

      d_dib->setTextColor(PALETTERGB(0, 0, 0));
      // dib->setTextAlign(TA_TOP | TA_CENTER);
      d_dib->setTextAlign(TA_TOP | TA_LEFT);
      d_dib->selectObject(d_insertFont);
      d_dib->setBkMode(TRANSPARENT);

      PixelRect rect(x1+2, y1+2, x1 + width - 4, y1 + height - 4);

      DrawText(d_dib->getDC(), name, -1, &rect, DT_CENTER | DT_WORDBREAK);
      // wTextOut(dib->getDC(), x1 + width/2, y1 + 2, name);
   }
}

/*---------------------------------------------------------------
 * Public Functions
 */

InsertDisplay::InsertDisplay() :
    d_insertImp(new InsertDisplayImp)
{
    ASSERT(d_insertImp != 0);
}

InsertDisplay::~InsertDisplay()
{
   delete d_insertImp;
   d_insertImp = 0;
}

void InsertDisplay::drawInserts(DrawDIBDC* dib, const MapWindowData& mapData)
{
   // InsertDisplayImp imp(dib, mapData);
   ASSERT(d_insertImp);
   d_insertImp->init(dib, mapData);

   ProvinceIter pl = mapData.d_campData->getProvinces();

   dib->saveDC();

   while(++pl)
   {
      const Province& prov = pl.current();

      if(prov.isOffScreen())
         d_insertImp->drawInsert(pl.currentID());
   }

   dib->restoreDC();
}

/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.2  2001/06/18 08:25:45  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
