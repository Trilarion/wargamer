/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef SupplyDisp_HPP
#define SupplyDisp_HPP

/*
 * Display Supply Lines on map
 */

// class CampaignSupplyLine;
class DrawDIBDC;
class DrawDIB;
class MapWindowData;

class SupplyDisplay
{
   public:
      SupplyDisplay();
      ~SupplyDisplay();
      void draw(DrawDIBDC* dib, MapWindowData* mapData);
      void reset() { d_dirty = true; }
   private:
//      CampaignSupplyLine* d_supplyLines;
      bool d_dirty;              // Set if needs to be redrawn
      DrawDIB* d_overlayDIB;
};


#endif   // SupplyDisp_HPP
