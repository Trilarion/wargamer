/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 * Display Unit on Map Window
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "dispunit.hpp"
#include "mw_data.hpp"
// #include "mapwind.hpp"
#include "campdint.hpp"
#include "scenario.hpp"
#include "options.hpp"
#include "scn_res.h"
#include "unitinfo.hpp"
#include "cbattle.hpp"
#include "armies.hpp"
#include "town.hpp"
#include "imglib.hpp"
#include "dib.hpp"
#include "armyutil.hpp"
#if !defined(EDITOR)
#include "mw_route.hpp"
#endif
#include "fonts.hpp"

class UnitDisplayImp {
    // Disable Compiler generated copy constructor
    UnitDisplayImp(const UnitDisplayImp&);
    UnitDisplayImp& operator = (const UnitDisplayImp&);
    UnitDisplayImp();      // Prevent instantiation
    ~UnitDisplayImp();
  public:
    static void drawUnits(DrawDIBDC* dib, MapWindowData& mapData, MapRouteDisplay* routeDisplay);
 private:
    static void drawUnit(DrawDIBDC* dib, MapWindowData& mapData, MapRouteDisplay* routeDisplay, ConstICommandPosition cpi, Boolean highlight, Boolean route, Boolean attributes);
    static void highlightUnit(DrawDIBDC* dib, MapWindowData& mapData, ConstICommandPosition cpi);
#if !defined(EDITOR)
    static void displayRoute(DrawDIBDC* dib, MapWindowData& mapData, MapRouteDisplay* routeDisplay, ConstICommandPosition cpi, Boolean nextDestOnly = False);
#endif
//    static void putUnitAttributes(DrawDIBDC* dib, const MapWindowData& mapData, ConstICommandPosition cpi);
};


void UnitDisplay::drawUnits(DrawDIBDC* dib, MapWindowData& mapData, MapRouteDisplay* routeDisplay)
{
   // d_unitDisp->drawUnits(dib, mapData);
   UnitDisplayImp::drawUnits(dib, mapData, routeDisplay);
}


static int imageCXRankOffset[Rank_HowMany] = {
               FI_ArmyGroup_CX,           // God
               FI_ArmyGroup_CX,           // President
               FI_ArmyGroup_CX,           // Executive
               FI_Army_CX,          // Army
//             FI_ArmyWing_CX,      // Wing
               FI_Corps_CX,         // Corps
               FI_Division_CX    // Division
//             FI_Division_CX       // Brigade
};

static int imageCYRankOffset[Rank_HowMany] = {
               FI_ArmyGroup_CY,           // God
               FI_ArmyGroup_CY,           // President
               FI_ArmyGroup_CY,           // Executive
               FI_Army_CY,          // Army
//             FI_ArmyWing_CY,      // Wing
               FI_Corps_CY,         // Corps
               FI_Division_CY    // Division
//             FI_Division_CY       // Brigade
};


void UnitDisplayImp::drawUnit(DrawDIBDC* dib, MapWindowData& mapData, MapRouteDisplay* routeDisplay, ConstICommandPosition cp, Boolean highlight, Boolean route, Boolean attributes)
{
  const Armies* armies = &mapData.d_campData->getArmies();

  // const CommandPosition* cp = armies->getCommand(cpi);

#if !defined(EDITOR)
  /*
   * If it is a garrison, then town display takes care of it.
   */

  if(cp->isGarrison())
   return;
#endif

  Side side = cp->getSide();

  const CampaignPosition& pos = CampaignArmy_Util::getUnitDisplayPosition(cp);

  Location l;
  Location lTail;

  pos.getLocation(l);
  pos.getTailLocation(lTail);

  PixelPoint p;
  mapData.locationToPixel(l, p);
  PixelPoint pTail;
  mapData.locationToPixel(lTail, pTail);

  /*
   * Display a rectangle showing column of march
   */

  if(p != pTail)
  {
    COLORREF cRef = scenario->getSideColour(side);

    int width = mapData.getZoomLevel();
    if(width == 1)
    {
      ColourIndex colour = dib->getColour(cRef);
      dib->line(p.getX(), p.getY(), pTail.getX(), pTail.getY(), colour);
    }
    else
    {
      ColourIndex colour = dib->getColour(cRef);
      dib->rightAngleLines(10, 3, p, pTail, colour);
    }
  }

  Boolean hasNoInfo = False;
  Boolean isSeen = True;
  Boolean isPlayer = True;

  if( (p.getX() >= 0) &&
      (p.getY() >= 0) &&
      (p.getX() < dib->getWidth()) &&
      (p.getY() < dib->getHeight()) )
  {

    static int imageRankOffset[Rank_HowMany] = {
               FI_ArmyGroup,           // God
               FI_ArmyGroup,           // President
               FI_ArmyGroup,           // Executive
               FI_Army,          // Army
//             FI_ArmyWing,      // Wing
               FI_Corps,         // Corps
               FI_Division    // Division
//             FI_Division       // Brigade
    };

    /*
     * Display actual nation flag if
     * 1. Nation Display option is active, and
     * 2. This is a players unit or, a seen enemy with adequate info
     */

#if !defined(EDITOR)
    isPlayer = armies->isPlayerUnit(cp);
    Boolean actualNation = ( (Options::get(OPT_NationDisplay)) &&
                             ( (isPlayer) ||
                               (!CampaignOptions::get(OPT_FogOfWar)) ||
                               (cp->getInfoQuality() >= CommandPosition::VeryLimitedInfo) ) );
#else
    Boolean actualNation = (Options::get(OPT_NationDisplay));
#endif

    ImageLibrary* il = scenario->getNationFlag(cp->getNation(), actualNation);
    ASSERT(il != 0);

    /*
     * Display Rank Flag if
     * 1. This is a players unit, or
     * 2. This is a seen enemy with adequate info
     *
     * Otherwise, for the time being, display an Army marker with a Question mark
     */

    // UWORD imageID;
    RankEnum rank;

#if !defined(EDITOR)
    if( (isPlayer) ||
        ( (cp->isSeen()) && (cp->getInfoQuality() >= CommandPosition::VeryLimitedInfo) ) )
    {
      rank = armies->getRank(cp).getRankEnum();
    }
    else
    {
      rank = Rank_Army;
      hasNoInfo = True;
      isSeen = cp->isSeen();
    }
#else
    rank = armies->getRank(cp).getRankEnum();
#endif //(EDITOR)

    if(isSeen)
    {
      il->blit(dib, imageRankOffset[rank], p.getX()-(imageCXRankOffset[rank]/2),
               p.getY()-(imageCYRankOffset[rank]/2), True);
    }

    /*
     * if this is enemy and they are not seen then we are looking at their
     * last known location. Darken Image to temorarily indicate this
     *
     */

    else
    {
      il->darkenBlit(dib, imageRankOffset[rank], p.getX()-(imageCXRankOffset[rank]/2),
               p.getY()-(imageCYRankOffset[rank]/2), 40);

    }

#if 0
    /*
     * if we have no info then put some question marks for the time being
     */

    if(hasNoInfo)
    {
      static HFONT hFont = FontManager::getFont(12);

      HFONT oldFont = dib->setFont(hFont);

      dib->lightenedText("???", p.getX()-7, p.getY()-7, 50);

      dib->setFont(oldFont);
    }
#endif
  }

//  Boolean limitedInfo = (getOption(OPT_LimitedInfoUnits) && !d_campData->getArmies().isUnitOrderable(list.getUnit(i)));
  if(isPlayer || isSeen)
  {
    if(highlight)
      highlightUnit(dib, mapData, cp);
  }

#if !defined(EDITOR)
  if(isPlayer || cp->getInfoQuality() >= CommandPosition::LimitedInfo)
  {
    if(route || highlight)
    {
      displayRoute(dib, mapData, routeDisplay, cp, !isPlayer);
    }
  }
#endif

}


#if !defined(EDITOR)
void UnitDisplayImp::displayRoute(DrawDIBDC* dib, MapWindowData& mapData, MapRouteDisplay* routeDisp, ConstICommandPosition cpi, Boolean nextDestOnly)
{
#ifdef DEBUG_ALL
   OutputDebugString("displayRoute... start\r\n");
#endif
  ASSERT(cpi != NoCommandPosition);
  const Armies* armies = &mapData.d_campData->getArmies();

  {
//  const CommandPosition* cp = armies->getCommand(cpi);
//  const OrderValues* order = cp->getCurrentOrder();

    Location sl;
    cpi->getLocation(sl);
    static ArrowInfo ai(10, 7, 15, 15);

    if(nextDestOnly)
    {
      ITown town = cpi->getDestTown();
      ASSERT(town != NoTown);

      const Town& t = mapData.d_campData->getTown(town);
      const Location& el = t.getLocation();
      MapRouteDisplay::drawArrow(dib, mapData, cpi, sl, el, ai);
    }
    else
    {
      /*
       * New tidied Version
       */

      routeDisp->drawRoute(dib, mapData, cpi);
    }
  }
#ifdef DEBUG_ALL
   OutputDebugString("displayRoute... finish\r\n");
#endif
}
#endif

void UnitDisplayImp::drawUnits(DrawDIBDC* dib, MapWindowData& mapData, MapRouteDisplay* routeDisplay)
{
   /*
    * Work out who is highlighted
    */

   ArmyReadLocker lock(mapData.d_campData->getArmies());

   ConstICommandPosition curTracked = mapData.getCurrentUnit();

   const Armies& armies = mapData.d_campData->getArmies();
   NationIter nationIter = &armies;

   while(++nationIter)
   {
      Side side = nationIter.current();

      ConstUnitIter unitIter(&armies, armies.getFirstUnit(side));
      while(unitIter.sister())
      {
         ConstICommandPosition cpi = unitIter.current();
         // const CommandPosition* cp = unitIter.currentCommand();

#if defined(EDITOR)
         drawUnit(dib, mapData, routeDisplay, cpi,
            curTracked == cpi,
            mapData.showAllRoutes, mapData.showAllAttribs);
#else
         //--- Moved to cu_move
         // cp->runTimer();

         if(CampaignArmy_Util::isUnitVisible(mapData.d_campData, cpi))
           drawUnit(dib, mapData, routeDisplay, cpi, curTracked == cpi,
               mapData.showAllRoutes, mapData.showAllAttribs);
#endif
      }

      side++;
   }

#ifdef SORT_OUT_THEADED_MAPWINDOW
   if(tinyMap)
   {
     if(mapData.trackedUnits.unitCount() > 0 && mapData.currentUnit != NoUnitListID)
       tinyMap->draw(mapData.trackedUnits.getUnit(mapData.currentUnit));
     else
       tinyMap->draw(NoCommandPosition);
   }
#endif
}

void UnitDisplayImp::highlightUnit(DrawDIBDC* dib, MapWindowData& mapData, ConstICommandPosition cpi)
{
#ifdef DEBUG_HIGHLIGHT
  debugLog("UnitDisplayImp::highlightUnit() currentUnit = %d\n", (int)mapData.currentUnit);
#endif
#ifdef DEBUG_ALL
      OutputDebugString("highlightUnit... start\r\n");
#endif

    const CommandPosition *cp = mapData.d_campData->getCommand(cpi);
    ASSERT(cp != 0);

    const Armies* armies = &mapData.d_campData->getArmies();

    RankEnum rank = armies->getRank(cpi).getRankEnum();

    // CampaignPosition& pos = *cp;    // ->getPosition();
    const CampaignPosition& pos = CampaignArmy_Util::getUnitDisplayPosition(cpi);
    Location l;
    pos.getLocation(l);
    PixelPoint p;
    mapData.locationToPixel(l, p);

    ColourIndex color = dib->getColour(scenario->getSideColour(cp->getSide()));

    dib->frame(p.getX()-(imageCXRankOffset[rank]/2) - 1,
               p.getY()-(imageCYRankOffset[rank]/2) - 1,
               imageCXRankOffset[rank]+2, imageCYRankOffset[rank]+2,
               color);
#if 0
    Lines line;
    line.putRect(dib, p.getX()-(getCXRankOffset[rank]/2) - 1,
                 p.getY()-(getCYRankOffset[rank]/2) - 1,
                 OBI_CX, OBI_CX - 6, WHITE);
#endif
#ifdef DEBUG_ALL
      OutputDebugString("highlightUnit... finish\r\n");
#endif
}

#if 0
void UnitDisplayImp::putUnitAttributes(DrawDIBDC* dib, const MapWindowData& mapData, ConstICommandPosition cpi)
{
  ASSERT(cpi != NoCommandPosition);
  CommandPosition *cp = mapData.d_campData->getCommand(cpi);
  ASSERT(cp != 0);

  // CampaignPosition& pos = *cp;      // ->getPosition();
   const CampaignPosition& pos = mapData.d_campData->getArmies().getUnitDisplayPosition(cpi);
  Location l;
    // positionToLocation(mapData.d_campData, pos, l);
  pos.getLocation(l);
  PixelPoint p;
  mapData.locationToPixel(l, p);

  UnitInfo unitInfo;
  unitInfo.drawUnitInfo(dib, cpi, p.getX()-OBI_CX/2 - 1,
                          p.getY()-OBI_CX/2 - 17);
}
#endif
/*
 * Draw Battles
 */

/*
 * Battle Display Implementation
 */

class BattleDisplayImp {
   ImageList images;

    // Disable Compiler generated Copy constructor
    BattleDisplayImp(const BattleDisplayImp&);
    BattleDisplayImp& operator = (const BattleDisplayImp&);
  public:
    BattleDisplayImp() { }
    ~BattleDisplayImp() { }

   void loadImages();
   void releaseImages();
   void draw(DrawDIBDC* dib, const PixelPoint& p);
};

BattleDisplay::BattleDisplay() :
    d_batDisp(new BattleDisplayImp)
{
}

BattleDisplay::~BattleDisplay()
{
        delete d_batDisp;
}

void BattleDisplayImp::loadImages()
{
   if(!images.isInitialised())
   {
      images.loadBitmap(
         scenario->getScenarioResDLL(),
         MAKEINTRESOURCE(BM_BATTLEICONS),
         BATTLEICON_WIDTH,
         BATTLEICON_MASK);
   }
}

void BattleDisplayImp::releaseImages()
{
   if(images.isInitialised())
      images.unloadBitmap();
}


void BattleDisplayImp::draw(DrawDIBDC* dib, const PixelPoint& p)
{
   int w = images.getImgWidth();
   int h = images.getImgHeight();

   images.draw(dib->getDC(), BATTLEICON_NORMAL, p.getX() - w/2, p.getY() - h/2, w, h);
}

void BattleDisplay::drawBattles(DrawDIBDC* dib, const MapWindowData& mapData)
{
#if 0
#if !defined(EDITOR)
   Boolean drawn = False;

   CampaignBattleIterR iter = &mapData.d_campData->getBattleList();
   while(++iter)
   {

      const CampaignBattle* battle = iter.current();
      ASSERT(battle != 0);

      const CampaignPosition& pos = battle->getPosition();
      Location l;
      pos.getLocation(l);
      PixelPoint p;
      mapData.locationToPixel(l, p);

      if( (p.getX() >= 0) &&
            (p.getY() >= 0) &&
            (p.getX() < dib->getWidth()) &&
            (p.getY() < dib->getHeight()) )
      {
         if(!drawn)
         {
            drawn = True;
         }

            d_batDisp->loadImages();
            d_batDisp->draw(dib, p);
      }
   }

   if(drawn)
      ;
   else
        d_batDisp->releaseImages();
#endif
#endif
}




/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.2  2001/06/18 08:25:45  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
