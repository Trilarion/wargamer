/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef MW_TRACK_HPP
#define MW_TRACK_HPP

#ifndef __cplusplus
#error mw_track.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *	Map Window Tracking and Zooming
 *
 *----------------------------------------------------------------------
 */

#include <windef.h>
#include "mw_user.hpp"

class PixelPoint;
class MapWindowData;
class CampaignData;

// Expiremental template for auto-initialising variables

template<class T, int N>
class AutoSet {
	T value;
 public:
 	AutoSet() { value = N; }
	AutoSet(T n) { value = n; }
	AutoSet(AutoSet<T,N>& n) { value = n.value; }
	operator T&() { return value; }
	T& operator = (T n) { value = n; return *this; }

};



class MapWindowTrack {
	MapWindowData* d_mapData;

	// HCURSOR d_cursor;						// Cursor used when tracking
	AutoSet<HCURSOR, NULL> d_cursor;
#if defined(CUSTOMIZE)
	TrackMode d_trackMode;
#endif

	Boolean d_overPlayerItem;             // mouse if over player controled unit or town
   Boolean d_overObject;                 // is mouse over a unit or town
 public:
	Boolean d_draggingStatus;					// User is dragging

 private:
	MapWindowTrack(const MapWindowTrack&);
	MapWindowTrack& operator = (const MapWindowTrack&);
 public:
	MapWindowTrack(MapWindowData* mapData);
	~MapWindowTrack();

	void onLButtonDown(HWND hwnd, const PixelPoint& p, UINT keyFlags);
		// Process Left Button Down

	Boolean setCursor();
		// Set the cursor depending on the track mode
		// return True if cursor was set

//	Boolean isPlayerControl();
		// Is player in control of current selected unit

	Boolean isPlayerUnit();
		// is mouse over a player controled unit

	Boolean isPlayerTown();
		// is mouse over a player controled town

	Boolean track(HWND hwnd, int x, int y, UINT keyFlags);
		// mouse has moved, so track for objects under mouse

// 	void clickUnit(const PixelPoint& p);
// 		// unit has been clicked on
//
#if defined(CUSTOMIZE)
	void trackMode(TrackMode mode) { d_trackMode = mode; }
		// Set trackMode
#endif

 private:
//	void clickTown(const PixelPoint& p);
		// Town has been clicked on

	Boolean trackTowns(HWND hwnd, int x, int y, UINT keyFlags);
		// Return True if current town has changed

	Boolean trackUnits(HWND hwnd, int x, int y, UINT keyFlags);
		// Return True if current unit has changed

	Boolean trackProvinces(HWND hwnd, int x, int y, UINT keyFlags);
		// Return True if current province has changed

};


#endif /* MW_TRACK_HPP */

