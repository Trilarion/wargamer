/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef MAPZOOM_H
#define MAPZOOM_H

#ifndef __cplusplus
#error mapzoom.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1995, Steven Morle-Green, All Rights Reserved
 *----------------------------------------------------------------------
 *
 *	Manipulation of large zoomed maps
 *
 *----------------------------------------------------------------------
 *
 * Updated: 12th Feb 1997 by Steven:
 *		zoom levels are determined by the size of the display area
 *		such that level 0, always shows the entire world.
 *		Level 1, shows half the world, etc...
 *
 *----------------------------------------------------------------------
 */

// ------------------------------------------------------------------------

#include "mwdll.h"
#include "measure.hpp"
#include "mappoint.hpp"
#include "point.hpp"

class DrawDIBDC;
class MapFileSystem;
class PixelPoint;

/*
 * Constants used by various displayers
 */

const int BaseDisplayWidth = 1024;
const int MinFontHeight = 16;

/*
 * New version
 *
 * This combines what used to be in MAPADJ and MAPZOOM and all the
 * position values from MapWindow
 */

/*
 * We also use PixelPoint and Location
 */

typedef UBYTE MagEnum;
const MagEnum InvalidZoom = MagEnum(-1);

class MapZoom {
	MapFileSystem* mapFile;

	// The visible area

	MapPoint top;
	MapPoint size;
	MapPoint total;

	// The corners of the physical world in MapCord units

	MapPoint p1;
	MapPoint p2;
	MapPoint p3;
	MapPoint p4;
	Distance physicalWidth;		// what total.x represents in real world coordinates
	Distance physicalHeight;	// what total.x represents in real world coordinates
	LONG pixelWidth;				// Current magnification level... pixels for entire map width
	//------- Added 12Feb97
	LONG d_displayWidth;			// Size of display, equivalent to old magLevels[0]

	// Magnification level enumerated in levels

	MagEnum magEnum;

	//------- Removed 12Feb97
	// static const LONG magLevels[];

	static const MagEnum MaxMagLevel;

public:
	MapZoom();
	virtual ~MapZoom();

	/*
	 * Information Functions
	 */

	MapCord getFullWidth() const { return total.getX(); }
	MapCord getFullHeight() const { return total.getY(); }
	MapCord getViewWidth() const { return size.getX(); }
	MapCord getViewHeight() const { return size.getY(); }
	MapCord getTopX() const { return top.getX(); }
	MapCord getTopY() const { return top.getY(); }
	MapCord getMidX() const;
	MapCord getMidY() const;

	Distance getPhysicalHeight() const { return physicalHeight; }
	Distance getPhysicalWidth() const { return physicalWidth; }

	LONG displayWidth() const { return d_displayWidth; }

	void getViewRect(Rect<Distance>& r) const;
		// Return rectangle of physical coordinates


	/*
	 * Update/Set functions
	 */

	Boolean setMapX(MapCord x);
	Boolean setMapY(MapCord y);
	//------- Removed 12Feb97
	// void setViewSize(const MapPoint& m) { size = m; }
	void setViewSize(const PixelPoint& p);
		// Update the viewed size

	/*
	 * Coordinate conversion functions
	 */

	MapCord pixelToMap(LONG p) const;
	LONG mapToPixel(MapCord m) const;

	LONG distanceToPixel(Distance d) const;

	void mapToPixel(const MapPoint& mc, PixelPoint& pc) const;
	void pixelToMap(const PixelPoint& pc, MapPoint& mc) const;
	void pixelSizeToMap(const PixelPoint& pc, MapPoint& mc) const;
	void mapToLocation(const MapPoint& mc, Location& lc) const;
	void locationToMap(const Location& lc, MapPoint& mc) const;
	void pixelToLocation(const PixelPoint& pc, Location& lc) const;
	MAPWIND_DLL void locationToPixel(const Location& lc, PixelPoint& pc) const;

	Boolean onDisplay(const PixelPoint& p) const;
			// Is point on visible part of map?

	/*
	 * Magnification functions
	 */

	Boolean canZoomOut() const;
	Boolean canZoomIn() const;
	MagEnum getZoomEnum() const { return magEnum; }
	void setZoomEnum(MagEnum m);
	MagEnum getZoomOutEnum() const;
	MagEnum getZoomInEnum() const;
	LONG getZoomLevel() const;

	/*
	 * Draw map into DIB
	 */

	void drawStaticMap(DrawDIBDC* dib) const;

};

//--------------------------------------------------------------------

#endif /* MAPZOOM_H */

