/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef DISPUNIT_HPP
#define DISPUNIT_HPP

#ifndef __cplusplus
#error dispunit.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *	Information used for displaying Units and Battles
 *
 *----------------------------------------------------------------------
 */

#include "winctrl.hpp"

class DrawDIBDC;
class MapWindowData;
class UnitDisplayImp;
class MapRouteDisplay;

class UnitDisplay {
   // UnitDisplayImp* d_unitDisp;

	UnitDisplay(const UnitDisplay&);
	UnitDisplay& operator = (const UnitDisplay&);
 public:
 	UnitDisplay() { }
	~UnitDisplay() { }

	static void drawUnits(DrawDIBDC* dib, MapWindowData& mapData, MapRouteDisplay* routeDisplay);
};

class BattleDisplayImp;

class BattleDisplay {
        BattleDisplayImp* d_batDisp;

	BattleDisplay(const BattleDisplay&);
	BattleDisplay& operator = (const BattleDisplay&);
 public:
 	BattleDisplay();
	~BattleDisplay();

	void drawBattles(DrawDIBDC* dib, const MapWindowData& mapData);
};


#if 0           // Defined inside dispunit as BattleDisplayImp
class BattleDisplay {
	ImageList images;
public:
	BattleDisplay();
	~BattleDisplay();

	void loadImages();
	void releaseImages();
	void draw(DrawDIBDC* dib, const PixelPoint& p);
};
#endif

#endif /* DISPUNIT_HPP */
