# Microsoft Developer Studio Generated NMAKE File, Based on mapwind.dsp
!IF "$(CFG)" == ""
CFG=mapwind - Win32 Editor Debug
!MESSAGE No configuration specified. Defaulting to mapwind - Win32 Editor Debug.
!ENDIF 

!IF "$(CFG)" != "mapwind - Win32 Release" && "$(CFG)" != "mapwind - Win32 Debug" && "$(CFG)" != "mapwind - Win32 Editor Debug" && "$(CFG)" != "mapwind - Win32 Editor Release"
!MESSAGE Invalid configuration "$(CFG)" specified.
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "mapwind.mak" CFG="mapwind - Win32 Editor Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "mapwind - Win32 Release" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE "mapwind - Win32 Debug" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE "mapwind - Win32 Editor Debug" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE "mapwind - Win32 Editor Release" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE 
!ERROR An invalid configuration is specified.
!ENDIF 

!IF "$(OS)" == "Windows_NT"
NULL=
!ELSE 
NULL=nul
!ENDIF 

CPP=cl.exe
MTL=midl.exe
RSC=rc.exe

!IF  "$(CFG)" == "mapwind - Win32 Release"

OUTDIR=.\..\..\exe
INTDIR=.\o\release
# Begin Custom Macros
OutDir=.\..\..\exe
# End Custom Macros

!IF "$(RECURSE)" == "0" 

ALL : "$(OUTDIR)\mapwind.dll"

!ELSE 

ALL : "gamesup - Win32 Release" "ob - Win32 Release" "campdata - Win32 Release" "system - Win32 Release" "$(OUTDIR)\mapwind.dll"

!ENDIF 

!IF "$(RECURSE)" == "1" 
CLEAN :"system - Win32 ReleaseCLEAN" "campdata - Win32 ReleaseCLEAN" "ob - Win32 ReleaseCLEAN" "gamesup - Win32 ReleaseCLEAN" 
!ELSE 
CLEAN :
!ENDIF 
	-@erase "$(INTDIR)\condisp.obj"
	-@erase "$(INTDIR)\dispunit.obj"
	-@erase "$(INTDIR)\grapple.obj"
	-@erase "$(INTDIR)\inserts.obj"
	-@erase "$(INTDIR)\locengin.obj"
	-@erase "$(INTDIR)\mapanim.obj"
	-@erase "$(INTDIR)\mapdraw.obj"
	-@erase "$(INTDIR)\mapfile.obj"
	-@erase "$(INTDIR)\mapgui.obj"
	-@erase "$(INTDIR)\mapwind.obj"
	-@erase "$(INTDIR)\mapzoom.obj"
	-@erase "$(INTDIR)\mw_data.obj"
	-@erase "$(INTDIR)\mw_route.obj"
	-@erase "$(INTDIR)\mw_track.obj"
	-@erase "$(INTDIR)\mw_user.obj"
	-@erase "$(INTDIR)\provdisp.obj"
	-@erase "$(INTDIR)\supplydisp.obj"
	-@erase "$(INTDIR)\tinymap.obj"
	-@erase "$(INTDIR)\towndisp.obj"
	-@erase "$(INTDIR)\vc60.idb"
	-@erase "$(INTDIR)\vc60.pdb"
	-@erase "$(OUTDIR)\mapwind.dll"
	-@erase "$(OUTDIR)\mapwind.exp"
	-@erase "$(OUTDIR)\mapwind.lib"
	-@erase "$(OUTDIR)\mapwind.pdb"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

"$(INTDIR)" :
    if not exist "$(INTDIR)/$(NULL)" mkdir "$(INTDIR)"

CPP_PROJ=/nologo /MD /W3 /Gi /GR /GX /Zi /O2 /I "..\h" /I "..\..\res" /I "..\system" /I "..\gamesup" /I "..\ob" /I "..\campdata" /I "..\campwind" /D "NDEBUG" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "EXPORT_MAPWIND_DLL" /Fp"$(INTDIR)\mapwind.pch" /YX"stdinc.hpp" /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /c 
MTL_PROJ=/nologo /D "NDEBUG" /mktyplib203 /win32 
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\mapwind.bsc" 
BSC32_SBRS= \
	
LINK32=link.exe
LINK32_FLAGS=gdi32.lib user32.lib /nologo /dll /incremental:no /pdb:"$(OUTDIR)\mapwind.pdb" /debug /machine:I386 /out:"$(OUTDIR)\mapwind.dll" /implib:"$(OUTDIR)\mapwind.lib" 
LINK32_OBJS= \
	"$(INTDIR)\condisp.obj" \
	"$(INTDIR)\dispunit.obj" \
	"$(INTDIR)\grapple.obj" \
	"$(INTDIR)\inserts.obj" \
	"$(INTDIR)\locengin.obj" \
	"$(INTDIR)\mapanim.obj" \
	"$(INTDIR)\mapdraw.obj" \
	"$(INTDIR)\mapfile.obj" \
	"$(INTDIR)\mapgui.obj" \
	"$(INTDIR)\mapwind.obj" \
	"$(INTDIR)\mapzoom.obj" \
	"$(INTDIR)\mw_data.obj" \
	"$(INTDIR)\mw_route.obj" \
	"$(INTDIR)\mw_track.obj" \
	"$(INTDIR)\mw_user.obj" \
	"$(INTDIR)\provdisp.obj" \
	"$(INTDIR)\supplydisp.obj" \
	"$(INTDIR)\tinymap.obj" \
	"$(INTDIR)\towndisp.obj" \
	"$(OUTDIR)\system.lib" \
	"$(OUTDIR)\campdata.lib" \
	"$(OUTDIR)\ob.lib" \
	"$(OUTDIR)\gamesup.lib"

"$(OUTDIR)\mapwind.dll" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

!ELSEIF  "$(CFG)" == "mapwind - Win32 Debug"

OUTDIR=.\..\..\exe
INTDIR=.\o\debug
# Begin Custom Macros
OutDir=.\..\..\exe
# End Custom Macros

!IF "$(RECURSE)" == "0" 

ALL : "$(OUTDIR)\mapwindDB.dll"

!ELSE 

ALL : "gamesup - Win32 Debug" "ob - Win32 Debug" "campdata - Win32 Debug" "system - Win32 Debug" "$(OUTDIR)\mapwindDB.dll"

!ENDIF 

!IF "$(RECURSE)" == "1" 
CLEAN :"system - Win32 DebugCLEAN" "campdata - Win32 DebugCLEAN" "ob - Win32 DebugCLEAN" "gamesup - Win32 DebugCLEAN" 
!ELSE 
CLEAN :
!ENDIF 
	-@erase "$(INTDIR)\condisp.obj"
	-@erase "$(INTDIR)\dispunit.obj"
	-@erase "$(INTDIR)\grapple.obj"
	-@erase "$(INTDIR)\inserts.obj"
	-@erase "$(INTDIR)\locengin.obj"
	-@erase "$(INTDIR)\mapanim.obj"
	-@erase "$(INTDIR)\mapdraw.obj"
	-@erase "$(INTDIR)\mapfile.obj"
	-@erase "$(INTDIR)\mapgui.obj"
	-@erase "$(INTDIR)\mapwind.obj"
	-@erase "$(INTDIR)\mapzoom.obj"
	-@erase "$(INTDIR)\mw_data.obj"
	-@erase "$(INTDIR)\mw_route.obj"
	-@erase "$(INTDIR)\mw_track.obj"
	-@erase "$(INTDIR)\mw_user.obj"
	-@erase "$(INTDIR)\provdisp.obj"
	-@erase "$(INTDIR)\supplydisp.obj"
	-@erase "$(INTDIR)\tinymap.obj"
	-@erase "$(INTDIR)\towndisp.obj"
	-@erase "$(INTDIR)\vc60.idb"
	-@erase "$(INTDIR)\vc60.pdb"
	-@erase "$(OUTDIR)\mapwindDB.dll"
	-@erase "$(OUTDIR)\mapwindDB.exp"
	-@erase "$(OUTDIR)\mapwindDB.ilk"
	-@erase "$(OUTDIR)\mapwindDB.lib"
	-@erase "$(OUTDIR)\mapwindDB.pdb"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

"$(INTDIR)" :
    if not exist "$(INTDIR)/$(NULL)" mkdir "$(INTDIR)"

CPP_PROJ=/nologo /MDd /W3 /Gm /Gi /GR /GX /ZI /Od /I "..\h" /I "..\..\res" /I "..\system" /I "..\gamesup" /I "..\ob" /I "..\campdata" /I "..\campwind" /D "_DEBUG" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "EXPORT_MAPWIND_DLL" /Fp"$(INTDIR)\mapwind.pch" /YX"stdinc.hpp" /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /GZ /c 
MTL_PROJ=/nologo /D "_DEBUG" /mktyplib203 /win32 
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\mapwind.bsc" 
BSC32_SBRS= \
	
LINK32=link.exe
LINK32_FLAGS=gdi32.lib user32.lib /nologo /dll /incremental:yes /pdb:"$(OUTDIR)\mapwindDB.pdb" /debug /machine:I386 /out:"$(OUTDIR)\mapwindDB.dll" /implib:"$(OUTDIR)\mapwindDB.lib" /pdbtype:sept 
LINK32_OBJS= \
	"$(INTDIR)\condisp.obj" \
	"$(INTDIR)\dispunit.obj" \
	"$(INTDIR)\grapple.obj" \
	"$(INTDIR)\inserts.obj" \
	"$(INTDIR)\locengin.obj" \
	"$(INTDIR)\mapanim.obj" \
	"$(INTDIR)\mapdraw.obj" \
	"$(INTDIR)\mapfile.obj" \
	"$(INTDIR)\mapgui.obj" \
	"$(INTDIR)\mapwind.obj" \
	"$(INTDIR)\mapzoom.obj" \
	"$(INTDIR)\mw_data.obj" \
	"$(INTDIR)\mw_route.obj" \
	"$(INTDIR)\mw_track.obj" \
	"$(INTDIR)\mw_user.obj" \
	"$(INTDIR)\provdisp.obj" \
	"$(INTDIR)\supplydisp.obj" \
	"$(INTDIR)\tinymap.obj" \
	"$(INTDIR)\towndisp.obj" \
	"$(OUTDIR)\systemDB.lib" \
	"$(OUTDIR)\campdataDB.lib" \
	"$(OUTDIR)\obDB.lib" \
	"$(OUTDIR)\gamesupDB.lib"

"$(OUTDIR)\mapwindDB.dll" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

!ELSEIF  "$(CFG)" == "mapwind - Win32 Editor Debug"

OUTDIR=.\..\..\exe
INTDIR=.\o\Editor\Debug
# Begin Custom Macros
OutDir=.\..\..\exe
# End Custom Macros

!IF "$(RECURSE)" == "0" 

ALL : "$(OUTDIR)\mapwindEDDB.dll"

!ELSE 

ALL : "gamesup - Win32 Editor Debug" "ob - Win32 Editor Debug" "campdata - Win32 Editor Debug" "system - Win32 Editor Debug" "$(OUTDIR)\mapwindEDDB.dll"

!ENDIF 

!IF "$(RECURSE)" == "1" 
CLEAN :"system - Win32 Editor DebugCLEAN" "campdata - Win32 Editor DebugCLEAN" "ob - Win32 Editor DebugCLEAN" "gamesup - Win32 Editor DebugCLEAN" 
!ELSE 
CLEAN :
!ENDIF 
	-@erase "$(INTDIR)\condisp.obj"
	-@erase "$(INTDIR)\dispunit.obj"
	-@erase "$(INTDIR)\grapple.obj"
	-@erase "$(INTDIR)\inserts.obj"
	-@erase "$(INTDIR)\locengin.obj"
	-@erase "$(INTDIR)\mapdraw.obj"
	-@erase "$(INTDIR)\mapfile.obj"
	-@erase "$(INTDIR)\mapgui.obj"
	-@erase "$(INTDIR)\mapwind.obj"
	-@erase "$(INTDIR)\mapzoom.obj"
	-@erase "$(INTDIR)\mw_data.obj"
	-@erase "$(INTDIR)\mw_track.obj"
	-@erase "$(INTDIR)\mw_user.obj"
	-@erase "$(INTDIR)\printmap.obj"
	-@erase "$(INTDIR)\provdisp.obj"
	-@erase "$(INTDIR)\supplydisp.obj"
	-@erase "$(INTDIR)\tinymap.obj"
	-@erase "$(INTDIR)\towndisp.obj"
	-@erase "$(INTDIR)\vc60.idb"
	-@erase "$(INTDIR)\vc60.pdb"
	-@erase "$(OUTDIR)\mapwindEDDB.dll"
	-@erase "$(OUTDIR)\mapwindEDDB.exp"
	-@erase "$(OUTDIR)\mapwindEDDB.ilk"
	-@erase "$(OUTDIR)\mapwindEDDB.lib"
	-@erase "$(OUTDIR)\mapwindEDDB.pdb"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

"$(INTDIR)" :
    if not exist "$(INTDIR)/$(NULL)" mkdir "$(INTDIR)"

CPP_PROJ=/nologo /MDd /W3 /Gm /Gi /GR /GX /ZI /Od /I "..\h" /I "..\..\res" /I "..\system" /I "..\gamesup" /I "..\ob" /I "..\campdata" /I "..\campwind" /D "_USRDLL" /D "EXPORT_MAPWIND_DLL" /D "_DEBUG" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /D "EDITOR" /Fp"$(INTDIR)\mapwind.pch" /YX"stdinc.hpp" /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /GZ /c 
MTL_PROJ=/nologo /D "_DEBUG" /mktyplib203 /win32 
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\mapwind.bsc" 
BSC32_SBRS= \
	
LINK32=link.exe
LINK32_FLAGS=gdi32.lib user32.lib /nologo /dll /incremental:yes /pdb:"$(OUTDIR)\mapwindEDDB.pdb" /debug /machine:I386 /out:"$(OUTDIR)\mapwindEDDB.dll" /implib:"$(OUTDIR)\mapwindEDDB.lib" /pdbtype:sept 
LINK32_OBJS= \
	"$(INTDIR)\condisp.obj" \
	"$(INTDIR)\dispunit.obj" \
	"$(INTDIR)\grapple.obj" \
	"$(INTDIR)\inserts.obj" \
	"$(INTDIR)\locengin.obj" \
	"$(INTDIR)\mapdraw.obj" \
	"$(INTDIR)\mapfile.obj" \
	"$(INTDIR)\mapgui.obj" \
	"$(INTDIR)\mapwind.obj" \
	"$(INTDIR)\mapzoom.obj" \
	"$(INTDIR)\mw_data.obj" \
	"$(INTDIR)\mw_track.obj" \
	"$(INTDIR)\mw_user.obj" \
	"$(INTDIR)\printmap.obj" \
	"$(INTDIR)\provdisp.obj" \
	"$(INTDIR)\supplydisp.obj" \
	"$(INTDIR)\tinymap.obj" \
	"$(INTDIR)\towndisp.obj" \
	"$(OUTDIR)\systemDB.lib" \
	"$(OUTDIR)\campdataEDDB.lib" \
	"$(OUTDIR)\obEDDB.lib" \
	"$(OUTDIR)\gamesupDB.lib"

"$(OUTDIR)\mapwindEDDB.dll" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

!ELSEIF  "$(CFG)" == "mapwind - Win32 Editor Release"

OUTDIR=.\..\..\exe
INTDIR=.\o\Editor\Release
# Begin Custom Macros
OutDir=.\..\..\exe
# End Custom Macros

!IF "$(RECURSE)" == "0" 

ALL : "$(OUTDIR)\mapwindED.dll"

!ELSE 

ALL : "gamesup - Win32 Editor Release" "ob - Win32 Editor Release" "campdata - Win32 Editor Release" "system - Win32 Editor Release" "$(OUTDIR)\mapwindED.dll"

!ENDIF 

!IF "$(RECURSE)" == "1" 
CLEAN :"system - Win32 Editor ReleaseCLEAN" "campdata - Win32 Editor ReleaseCLEAN" "ob - Win32 Editor ReleaseCLEAN" "gamesup - Win32 Editor ReleaseCLEAN" 
!ELSE 
CLEAN :
!ENDIF 
	-@erase "$(INTDIR)\condisp.obj"
	-@erase "$(INTDIR)\dispunit.obj"
	-@erase "$(INTDIR)\grapple.obj"
	-@erase "$(INTDIR)\inserts.obj"
	-@erase "$(INTDIR)\locengin.obj"
	-@erase "$(INTDIR)\mapdraw.obj"
	-@erase "$(INTDIR)\mapfile.obj"
	-@erase "$(INTDIR)\mapgui.obj"
	-@erase "$(INTDIR)\mapwind.obj"
	-@erase "$(INTDIR)\mapzoom.obj"
	-@erase "$(INTDIR)\mw_data.obj"
	-@erase "$(INTDIR)\mw_track.obj"
	-@erase "$(INTDIR)\mw_user.obj"
	-@erase "$(INTDIR)\printmap.obj"
	-@erase "$(INTDIR)\provdisp.obj"
	-@erase "$(INTDIR)\supplydisp.obj"
	-@erase "$(INTDIR)\tinymap.obj"
	-@erase "$(INTDIR)\towndisp.obj"
	-@erase "$(INTDIR)\vc60.idb"
	-@erase "$(OUTDIR)\mapwindED.dll"
	-@erase "$(OUTDIR)\mapwindED.exp"
	-@erase "$(OUTDIR)\mapwindED.lib"
	-@erase "$(OUTDIR)\mapwindED.pdb"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

"$(INTDIR)" :
    if not exist "$(INTDIR)/$(NULL)" mkdir "$(INTDIR)"

CPP_PROJ=/nologo /MD /W3 /Gi /GR /GX /Zd /O2 /I "..\h" /I "..\..\res" /I "..\system" /I "..\gamesup" /I "..\ob" /I "..\campdata" /I "..\campwind" /D "NDEBUG" /D "_USRDLL" /D "EXPORT_MAPWIND_DLL" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /D "EDITOR" /Fp"$(INTDIR)\mapwind.pch" /YX"stdinc.hpp" /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /c 
MTL_PROJ=/nologo /D "NDEBUG" /mktyplib203 /win32 
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\mapwind.bsc" 
BSC32_SBRS= \
	
LINK32=link.exe
LINK32_FLAGS=gdi32.lib user32.lib /nologo /dll /incremental:no /pdb:"$(OUTDIR)\mapwindED.pdb" /debug /machine:I386 /out:"$(OUTDIR)\mapwindED.dll" /implib:"$(OUTDIR)\mapwindED.lib" 
LINK32_OBJS= \
	"$(INTDIR)\condisp.obj" \
	"$(INTDIR)\dispunit.obj" \
	"$(INTDIR)\grapple.obj" \
	"$(INTDIR)\inserts.obj" \
	"$(INTDIR)\locengin.obj" \
	"$(INTDIR)\mapdraw.obj" \
	"$(INTDIR)\mapfile.obj" \
	"$(INTDIR)\mapgui.obj" \
	"$(INTDIR)\mapwind.obj" \
	"$(INTDIR)\mapzoom.obj" \
	"$(INTDIR)\mw_data.obj" \
	"$(INTDIR)\mw_track.obj" \
	"$(INTDIR)\mw_user.obj" \
	"$(INTDIR)\printmap.obj" \
	"$(INTDIR)\provdisp.obj" \
	"$(INTDIR)\supplydisp.obj" \
	"$(INTDIR)\tinymap.obj" \
	"$(INTDIR)\towndisp.obj" \
	"$(OUTDIR)\system.lib" \
	"$(OUTDIR)\campdataED.lib" \
	"$(OUTDIR)\obED.lib" \
	"$(OUTDIR)\gamesup.lib"

"$(OUTDIR)\mapwindED.dll" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

!ENDIF 

.c{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.c{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<


!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("mapwind.dep")
!INCLUDE "mapwind.dep"
!ELSE 
!MESSAGE Warning: cannot find "mapwind.dep"
!ENDIF 
!ENDIF 


!IF "$(CFG)" == "mapwind - Win32 Release" || "$(CFG)" == "mapwind - Win32 Debug" || "$(CFG)" == "mapwind - Win32 Editor Debug" || "$(CFG)" == "mapwind - Win32 Editor Release"
SOURCE=.\condisp.cpp

"$(INTDIR)\condisp.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\dispunit.cpp

"$(INTDIR)\dispunit.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\grapple.cpp

"$(INTDIR)\grapple.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\inserts.cpp

"$(INTDIR)\inserts.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\locengin.cpp

"$(INTDIR)\locengin.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\mapanim.cpp

!IF  "$(CFG)" == "mapwind - Win32 Release"


"$(INTDIR)\mapanim.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "mapwind - Win32 Debug"


"$(INTDIR)\mapanim.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "mapwind - Win32 Editor Debug"

!ELSEIF  "$(CFG)" == "mapwind - Win32 Editor Release"

!ENDIF 

SOURCE=.\mapdraw.cpp

"$(INTDIR)\mapdraw.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\mapfile.cpp

"$(INTDIR)\mapfile.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\mapgui.cpp

"$(INTDIR)\mapgui.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\mapwind.cpp

"$(INTDIR)\mapwind.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\mapzoom.cpp

"$(INTDIR)\mapzoom.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\mw_data.cpp

"$(INTDIR)\mw_data.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\mw_route.cpp

!IF  "$(CFG)" == "mapwind - Win32 Release"


"$(INTDIR)\mw_route.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "mapwind - Win32 Debug"


"$(INTDIR)\mw_route.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "mapwind - Win32 Editor Debug"

!ELSEIF  "$(CFG)" == "mapwind - Win32 Editor Release"

!ENDIF 

SOURCE=.\mw_track.cpp

"$(INTDIR)\mw_track.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\mw_user.cpp

"$(INTDIR)\mw_user.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\printmap.cpp

!IF  "$(CFG)" == "mapwind - Win32 Release"

!ELSEIF  "$(CFG)" == "mapwind - Win32 Debug"

!ELSEIF  "$(CFG)" == "mapwind - Win32 Editor Debug"


"$(INTDIR)\printmap.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "mapwind - Win32 Editor Release"


"$(INTDIR)\printmap.obj" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\provdisp.cpp

"$(INTDIR)\provdisp.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\supplydisp.cpp

"$(INTDIR)\supplydisp.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\tinymap.cpp

"$(INTDIR)\tinymap.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\towndisp.cpp

"$(INTDIR)\towndisp.obj" : $(SOURCE) "$(INTDIR)"


!IF  "$(CFG)" == "mapwind - Win32 Release"

"system - Win32 Release" : 
   cd "\src\sf\wargamer\SRC\SYSTEM"
   $(MAKE) /$(MAKEFLAGS) /F .\system.mak CFG="system - Win32 Release" 
   cd "..\mapwind"

"system - Win32 ReleaseCLEAN" : 
   cd "\src\sf\wargamer\SRC\SYSTEM"
   $(MAKE) /$(MAKEFLAGS) /F .\system.mak CFG="system - Win32 Release" RECURSE=1 CLEAN 
   cd "..\mapwind"

!ELSEIF  "$(CFG)" == "mapwind - Win32 Debug"

"system - Win32 Debug" : 
   cd "\src\sf\wargamer\SRC\SYSTEM"
   $(MAKE) /$(MAKEFLAGS) /F .\system.mak CFG="system - Win32 Debug" 
   cd "..\mapwind"

"system - Win32 DebugCLEAN" : 
   cd "\src\sf\wargamer\SRC\SYSTEM"
   $(MAKE) /$(MAKEFLAGS) /F .\system.mak CFG="system - Win32 Debug" RECURSE=1 CLEAN 
   cd "..\mapwind"

!ELSEIF  "$(CFG)" == "mapwind - Win32 Editor Debug"

"system - Win32 Editor Debug" : 
   cd "\src\sf\wargamer\SRC\SYSTEM"
   $(MAKE) /$(MAKEFLAGS) /F .\system.mak CFG="system - Win32 Editor Debug" 
   cd "..\mapwind"

"system - Win32 Editor DebugCLEAN" : 
   cd "\src\sf\wargamer\SRC\SYSTEM"
   $(MAKE) /$(MAKEFLAGS) /F .\system.mak CFG="system - Win32 Editor Debug" RECURSE=1 CLEAN 
   cd "..\mapwind"

!ELSEIF  "$(CFG)" == "mapwind - Win32 Editor Release"

"system - Win32 Editor Release" : 
   cd "\src\sf\wargamer\SRC\SYSTEM"
   $(MAKE) /$(MAKEFLAGS) /F .\system.mak CFG="system - Win32 Editor Release" 
   cd "..\mapwind"

"system - Win32 Editor ReleaseCLEAN" : 
   cd "\src\sf\wargamer\SRC\SYSTEM"
   $(MAKE) /$(MAKEFLAGS) /F .\system.mak CFG="system - Win32 Editor Release" RECURSE=1 CLEAN 
   cd "..\mapwind"

!ENDIF 

!IF  "$(CFG)" == "mapwind - Win32 Release"

"campdata - Win32 Release" : 
   cd "\src\sf\wargamer\src\campdata"
   $(MAKE) /$(MAKEFLAGS) /F .\campdata.mak CFG="campdata - Win32 Release" 
   cd "..\mapwind"

"campdata - Win32 ReleaseCLEAN" : 
   cd "\src\sf\wargamer\src\campdata"
   $(MAKE) /$(MAKEFLAGS) /F .\campdata.mak CFG="campdata - Win32 Release" RECURSE=1 CLEAN 
   cd "..\mapwind"

!ELSEIF  "$(CFG)" == "mapwind - Win32 Debug"

"campdata - Win32 Debug" : 
   cd "\src\sf\wargamer\src\campdata"
   $(MAKE) /$(MAKEFLAGS) /F .\campdata.mak CFG="campdata - Win32 Debug" 
   cd "..\mapwind"

"campdata - Win32 DebugCLEAN" : 
   cd "\src\sf\wargamer\src\campdata"
   $(MAKE) /$(MAKEFLAGS) /F .\campdata.mak CFG="campdata - Win32 Debug" RECURSE=1 CLEAN 
   cd "..\mapwind"

!ELSEIF  "$(CFG)" == "mapwind - Win32 Editor Debug"

"campdata - Win32 Editor Debug" : 
   cd "\src\sf\wargamer\src\campdata"
   $(MAKE) /$(MAKEFLAGS) /F .\campdata.mak CFG="campdata - Win32 Editor Debug" 
   cd "..\mapwind"

"campdata - Win32 Editor DebugCLEAN" : 
   cd "\src\sf\wargamer\src\campdata"
   $(MAKE) /$(MAKEFLAGS) /F .\campdata.mak CFG="campdata - Win32 Editor Debug" RECURSE=1 CLEAN 
   cd "..\mapwind"

!ELSEIF  "$(CFG)" == "mapwind - Win32 Editor Release"

"campdata - Win32 Editor Release" : 
   cd "\src\sf\wargamer\src\campdata"
   $(MAKE) /$(MAKEFLAGS) /F .\campdata.mak CFG="campdata - Win32 Editor Release" 
   cd "..\mapwind"

"campdata - Win32 Editor ReleaseCLEAN" : 
   cd "\src\sf\wargamer\src\campdata"
   $(MAKE) /$(MAKEFLAGS) /F .\campdata.mak CFG="campdata - Win32 Editor Release" RECURSE=1 CLEAN 
   cd "..\mapwind"

!ENDIF 

!IF  "$(CFG)" == "mapwind - Win32 Release"

"ob - Win32 Release" : 
   cd "\src\sf\wargamer\SRC\OB"
   $(MAKE) /$(MAKEFLAGS) /F .\ob.mak CFG="ob - Win32 Release" 
   cd "..\mapwind"

"ob - Win32 ReleaseCLEAN" : 
   cd "\src\sf\wargamer\SRC\OB"
   $(MAKE) /$(MAKEFLAGS) /F .\ob.mak CFG="ob - Win32 Release" RECURSE=1 CLEAN 
   cd "..\mapwind"

!ELSEIF  "$(CFG)" == "mapwind - Win32 Debug"

"ob - Win32 Debug" : 
   cd "\src\sf\wargamer\SRC\OB"
   $(MAKE) /$(MAKEFLAGS) /F .\ob.mak CFG="ob - Win32 Debug" 
   cd "..\mapwind"

"ob - Win32 DebugCLEAN" : 
   cd "\src\sf\wargamer\SRC\OB"
   $(MAKE) /$(MAKEFLAGS) /F .\ob.mak CFG="ob - Win32 Debug" RECURSE=1 CLEAN 
   cd "..\mapwind"

!ELSEIF  "$(CFG)" == "mapwind - Win32 Editor Debug"

"ob - Win32 Editor Debug" : 
   cd "\src\sf\wargamer\SRC\OB"
   $(MAKE) /$(MAKEFLAGS) /F .\ob.mak CFG="ob - Win32 Editor Debug" 
   cd "..\mapwind"

"ob - Win32 Editor DebugCLEAN" : 
   cd "\src\sf\wargamer\SRC\OB"
   $(MAKE) /$(MAKEFLAGS) /F .\ob.mak CFG="ob - Win32 Editor Debug" RECURSE=1 CLEAN 
   cd "..\mapwind"

!ELSEIF  "$(CFG)" == "mapwind - Win32 Editor Release"

"ob - Win32 Editor Release" : 
   cd "\src\sf\wargamer\SRC\OB"
   $(MAKE) /$(MAKEFLAGS) /F .\ob.mak CFG="ob - Win32 Editor Release" 
   cd "..\mapwind"

"ob - Win32 Editor ReleaseCLEAN" : 
   cd "\src\sf\wargamer\SRC\OB"
   $(MAKE) /$(MAKEFLAGS) /F .\ob.mak CFG="ob - Win32 Editor Release" RECURSE=1 CLEAN 
   cd "..\mapwind"

!ENDIF 

!IF  "$(CFG)" == "mapwind - Win32 Release"

"gamesup - Win32 Release" : 
   cd "\src\sf\wargamer\SRC\GAMESUP"
   $(MAKE) /$(MAKEFLAGS) /F .\gamesup.mak CFG="gamesup - Win32 Release" 
   cd "..\mapwind"

"gamesup - Win32 ReleaseCLEAN" : 
   cd "\src\sf\wargamer\SRC\GAMESUP"
   $(MAKE) /$(MAKEFLAGS) /F .\gamesup.mak CFG="gamesup - Win32 Release" RECURSE=1 CLEAN 
   cd "..\mapwind"

!ELSEIF  "$(CFG)" == "mapwind - Win32 Debug"

"gamesup - Win32 Debug" : 
   cd "\src\sf\wargamer\SRC\GAMESUP"
   $(MAKE) /$(MAKEFLAGS) /F .\gamesup.mak CFG="gamesup - Win32 Debug" 
   cd "..\mapwind"

"gamesup - Win32 DebugCLEAN" : 
   cd "\src\sf\wargamer\SRC\GAMESUP"
   $(MAKE) /$(MAKEFLAGS) /F .\gamesup.mak CFG="gamesup - Win32 Debug" RECURSE=1 CLEAN 
   cd "..\mapwind"

!ELSEIF  "$(CFG)" == "mapwind - Win32 Editor Debug"

"gamesup - Win32 Editor Debug" : 
   cd "\src\sf\wargamer\SRC\GAMESUP"
   $(MAKE) /$(MAKEFLAGS) /F .\gamesup.mak CFG="gamesup - Win32 Editor Debug" 
   cd "..\mapwind"

"gamesup - Win32 Editor DebugCLEAN" : 
   cd "\src\sf\wargamer\SRC\GAMESUP"
   $(MAKE) /$(MAKEFLAGS) /F .\gamesup.mak CFG="gamesup - Win32 Editor Debug" RECURSE=1 CLEAN 
   cd "..\mapwind"

!ELSEIF  "$(CFG)" == "mapwind - Win32 Editor Release"

"gamesup - Win32 Editor Release" : 
   cd "\src\sf\wargamer\SRC\GAMESUP"
   $(MAKE) /$(MAKEFLAGS) /F .\gamesup.mak CFG="gamesup - Win32 Editor Release" 
   cd "..\mapwind"

"gamesup - Win32 Editor ReleaseCLEAN" : 
   cd "\src\sf\wargamer\SRC\GAMESUP"
   $(MAKE) /$(MAKEFLAGS) /F .\gamesup.mak CFG="gamesup - Win32 Editor Release" RECURSE=1 CLEAN 
   cd "..\mapwind"

!ENDIF 


!ENDIF 

