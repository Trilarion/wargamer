/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef MAPDRAW_HPP
#define MAPDRAW_HPP

#ifndef __cplusplus
#error mapdraw.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *	All things concerned with drawing the map window
 *
 *----------------------------------------------------------------------
 */

#include "sync.hpp"
#include "gamedefs.hpp"
#include "towndisp.hpp"
#include "provdisp.hpp"
#include "inserts.hpp"
#include "dispunit.hpp"
#include "condisp.hpp"
#include "supplyDisp.hpp"
#include "mapanim.hpp"
#if !defined(EDITOR)
#include "mw_route.hpp"
#endif

class DIB;
class DrawDIBDC;
class PixelPoint;
#if !defined(EDITOR)
class CampaignRoute;
#endif
struct Connection;
class RouteList;

class MapDIB {
	DrawDIBDC* d_static;			// Bits of the map that don't change very often
	DrawDIBDC* d_dynamic;		// Dynamic DIB
	Mutex staticLock;
	Mutex mainLock;
public:
	MapDIB();
	~MapDIB();

	DrawDIBDC* getStaticDIB();
	void releaseStaticDIB();
	DrawDIBDC* getMainDIB();
	void releaseMainDIB();

	Boolean setSize(const PixelPoint& p);
};

class MapDraw {
	MapDIB d_mapDibs;					// Would be better as a pointer to hide from callers
	Boolean staticDirty;			// flag that Static DIB needs recreating

	TownDisplay			d_townDisp;
	ProvinceDisplay	d_provDisp;
	InsertDisplay		d_inserts;
	UnitDisplay			d_unitDisp;
	ConnectionDisplay	d_conDisp;
   SupplyDisplay     d_supplyDisp;
#if !defined(EDITOR)
	MW_AnimationManager d_animationManager;
	MapRouteDisplay	d_routeDisp;
#endif

   static int s_instanceCount;
   static DIB* s_bmScale;

 public:
	MapDraw();
	~MapDraw();

#if !defined(EDITOR)
	void initAnimations(const MapWindowData* mapData);
		// initialize map animations
#endif

	void setSize(const PixelPoint& p);
		// Set the size of the map bitmap

	void requestRedraw(BOOL all);
		// Mark the map as needing to be redrawn
		// If all is TRUE, then the static map must be redrawn
		// otherwise only the dynamic parts

	void forceDraw();
		// More or less the same as requestRedraw(TRUE)

	void draw(MapWindowData& mapData, const CampaignData& campData);
		// Physically draw the map into the dib

	void paint(HDC hdc, const RECT& r);
		// Copy DIB to physical screen

#if !defined(EDITOR)
	void animate(HDC hdc);

	void drawRoute(DrawDIBDC* dib, MapWindowData& mapData, const CampaignPosition& from, const CampaignOrder& order, Side side)
	{
		d_routeDisp.drawRoute(dib, mapData, from, order, side);
	}
	void drawRoute(DrawDIBDC* dib, MapWindowData& mapData, const CampaignPosition& from, const CampaignOrder& order, Side side, const RouteList& rl)
	{
		d_routeDisp.drawRoute(dib, mapData, from, order, side, rl);
	}
#endif

 private:
	// these would be better implemented as stand-alone static functions
	// inside mapdraw.cpp

	void drawScale(DrawDIBDC* dib, const MapWindowData& mapData);
#ifdef DEBUG
	void drawGrid(DrawDIBDC* dib, const MapWindowData& mapData);
#endif
	// void drawRoute(DrawDIBDC* dib, const CampaignRoute* route, ITown start, ITown finish, const MapWindowData& mapData);

};

#endif /* MAPDRAW_HPP */

