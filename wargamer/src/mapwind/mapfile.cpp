/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1995, Steven Morle-Green, All Rights Reserved
 *----------------------------------------------------------------------
 *
 * Map Graphics Implementation
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/06/13 08:52:38  greenius
 * Initial Import
 *
 *
 *----------------------------------------------------------------------
 */


#include "stdinc.hpp"
#include "mapfile.hpp"
#include "dib.hpp"
#include "palette.hpp"
#include "scenario.hpp"
#include "winfile.hpp"

/*
 * Overall Map File system
 */

MapFileSystem::MapFileSystem()
{
   ASSERT(scenario != 0);

   currentMap = NoMap;
   loadedDIB = 0;
   SetRect(&loadedRect, 0,0,0,0);

   ArrayIndex nMaps = (ArrayIndex) scenario->getNumCampaignMaps();
   maps.init(nMaps);
   for(ArrayIndex i = 0; i < nMaps; i++)
   {
      maps[i] = new MapFileDisk(scenario->getCampaignMapName(i));
      ASSERT(maps[i] != 0);
   }
}

MapFileSystem::~MapFileSystem()
{
   for(ArrayIndex i = 0; i < maps.entries(); i++)
   {
      if(maps[i])
      {
         delete maps[i];
         maps[i] = 0;
      }
   }
   if(loadedDIB)
   {
      delete loadedDIB;
      loadedDIB = 0;
   }
}

/*
 * Does NOT round up
 */

inline int wMulDiv(int a, int b, int c)
{
   return (a * b) / c;
}

inline int wMulDivRound(int a, int b, int c)
{
   return (a * b + c/2) / c;
}



Boolean MapFileSystem::displayMapSection(DrawDIBDC* dib, const MapPoint& p, MapCord fullWidth, LONG wantWidth)
{
   /*
    * Find best matching map
    *
    * The best map, is the largest one that is smaller than the required size
    * If none are small enough, then just use the smallest
    *
    * Change this to use the one that is closest,
    * e.g. 900 will use map1024
    *      700 will use map512
    * I suspect that a more logarithmic method will be needed to bias
    * it towards picking smaller maps
    *
    * By dividing the lower diffs by 2, we have adjusted it so that the
    * cross over between 512 and 1024 will be at:
    *    
    */

   MapIndex bestMap = NoMap;
   int bestSize = 0;
   for(ArrayIndex i = 0; i < maps.entries(); i++)
   {
      int mSize = maps[i]->biWidth;

#if 0 // 12Feb97, get smallest available if dib is too small
      if(mSize <= wantWidth)
      {
         if((bestMap == NoMap) || (mSize > bestSize))
#elif 0
      if( (bestMap == NoMap) || 
         ( (mSize > bestSize) && (mSize <= wantWidth) ) )
#else
      int diff;
      if(mSize > wantWidth)
         diff = mSize - wantWidth;
      else
         diff = (wantWidth - mSize) / 2;     // bias it towards smaller maps

      if( (bestMap == NoMap) || (diff < bestSize) )
#endif

         {
            bestMap = i;
            // bestSize = mSize;
            bestSize = diff;
         }
#if 0 // 12Feb97
      }
#endif
   }

   ASSERT(bestMap != NoMap);
   if(bestMap == NoMap)
      return False;

   Boolean reload = False;       // Set this to force a reload

   if(bestMap != NoMap)
   {
      if(bestMap != currentMap)
      {
         if(currentMap != NoMap)
            maps[currentMap]->close();
         currentMap = bestMap;
         reload = True;
      }

      MapFileDisk* map = maps[currentMap];

      /*
       * Get desired rectangle in terms of graphic file pixels
       */

      RECT wantRect;
      wantRect.left   = wMulDiv(p.getX(),          map->biWidth, fullWidth);
      wantRect.top    = wMulDiv(p.getY(),          map->biWidth, fullWidth);
      wantRect.right  = wMulDiv(dib->getWidth(),      map->biWidth, wantWidth);
      wantRect.bottom = wMulDiv(dib->getHeight(),  map->biWidth, wantWidth);

      /*
       * Clip it...
       */

      if(wantRect.left < 0)
         wantRect.left = 0;
      if(wantRect.left >= map->biWidth)
         wantRect.left = map->biWidth - 1;
      if( (wantRect.left + wantRect.right) > map->biWidth)
         wantRect.right = map->biWidth - wantRect.left;

      if(wantRect.top < 0)
         wantRect.top = 0;
      if(wantRect.top >= map->biHeight)
         wantRect.top = map->biHeight - 1;
      if( (wantRect.top + wantRect.bottom) > map->biHeight)
         wantRect.bottom = map->biHeight - wantRect.top;

      int dibWidth = (wantRect.right + 3) & ~3;       // Round to a DWORD

      /*
       * Make sure we have a DIB big enough
       */

      if(!loadedDIB || (dibWidth > loadedDIB->getWidth()) || (wantRect.bottom > loadedDIB->getHeight()))
      {
         if(loadedDIB)
            delete loadedDIB;
         loadedDIB = new DIB(dibWidth, wantRect.bottom); // Top Down
         reload = True;
      }

      /*
       * See if desired area is already in loadedDIB
       */

      if(!reload)
      {
         if( (wantRect.left < loadedRect.left) ||
            ((wantRect.left + wantRect.right) > (loadedRect.left + loadedRect.right)) ||
            (wantRect.top < loadedRect.top) ||
            ((wantRect.top + wantRect.bottom) > (loadedRect.top + loadedRect.bottom)) )
         {
            reload = True;
         }
      }

      if(reload)
      {
         // Load it from disk

         loadedRect.left = wantRect.left;
         loadedRect.right = loadedDIB->getWidth();
         loadedRect.top = wantRect.top;
         loadedRect.bottom = loadedDIB->getHeight();

         /*
          * Clip loadedRect, but still try to load as much in as possible
          */

         if(loadedRect.left < 0)
            loadedRect.left = 0;
         if(loadedRect.right > map->biWidth)
            loadedRect.right = map->biWidth;
         if( (loadedRect.left + loadedRect.right) > map->biWidth)
         {
            loadedRect.left = map->biWidth - loadedRect.right;
         }
         if(loadedRect.top < 0)
            loadedRect.top = 0;
         if(loadedRect.bottom > map->biHeight)
            loadedRect.bottom = map->biHeight;
         if( (loadedRect.top + loadedRect.bottom) > map->biHeight)
            loadedRect.top = map->biHeight - loadedRect.bottom;

         map->loadMapSection(loadedDIB, loadedRect);
      }

      /*
       * Blit it
       */

      /*
       * Work out what to blit!
       */

      int xSrc = wantRect.left - loadedRect.left;
      int ySrc = wantRect.top - loadedRect.top;

      /*
       * Do some fine tuning
       */

      int xDest  = wMulDiv(wantRect.left, wantWidth, map->biWidth) - 
                   wMulDiv(p.getX(), wantWidth, fullWidth);
      int yDest  = wMulDiv(wantRect.top, wantWidth, map->biWidth) - 
                   wMulDiv(p.getY(), wantWidth, fullWidth);


      /*
       * Bodge for graphic that is too small for display!
       *
       * Need a better solution... whereby map gets centered in display
       * with a nice background pattern.
       *
       * Will need to fit in with MapZoom so that objects are offset properly
       */

      // int destWidth = wMulDiv(wantRect.right, wantWidth, map->biWidth);
      // int destHeight = wMulDiv(wantRect.bottom, wantWidth, map->biWidth);

      // Round up versions
      int destWidth = wMulDivRound(wantRect.right, wantWidth, map->biWidth);
      int destHeight = wMulDivRound(wantRect.bottom, wantWidth, map->biWidth);

      ASSERT(destWidth <= dib->getWidth());
      ASSERT(destHeight <= dib->getHeight());

      if(destWidth > dib->getWidth())
         destWidth = dib->getWidth();
      if(destHeight > dib->getHeight())
         destHeight = dib->getHeight();

      /*
       * Blit loadedDIB to given DIB
       */

      HDC hdcSrc = CreateCompatibleDC(NULL);
      // HDC hdcDest = CreateCompatibleDC(NULL);

      SelectObject(hdcSrc, loadedDIB->getHandle());
      // SelectObject(hdcDest, dib->getHandle());

      SetStretchBltMode(dib->getDC(), COLORONCOLOR);

      StretchBlt(dib->getDC(), xDest, yDest, destWidth - xDest, destHeight - yDest,
                  hdcSrc, xSrc, ySrc, wantRect.right, wantRect.bottom,
                  SRCCOPY);

      // DeleteDC(hdcDest);
      DeleteDC(hdcSrc);

   }

   return True;
}


/*
 * MapFileDisk Implementation
 */

MapFileDisk::MapFileDisk(LPCSTR name)
{
   fileName = name;
   bfOffBits = 0;
   biWidth = 0;
   biHeight = 0;
   rFile = 0;

   /*
    * Get the header, etc...
    */

   open();
   BITMAPFILEHEADER header;
   if(!rFile->read(&header, sizeof(header)))
      return;

   BITMAPINFOHEADER bmh;
   if(!rFile->read(&bmh, sizeof(bmh)))
      return;

   bfOffBits   = header.bfOffBits;
   biWidth     = bmh.biWidth;
   biHeight    = bmh.biHeight;

   if((bmh.biPlanes != 1) ||
      (bmh.biBitCount != 8) ||
      (bmh.biCompression != BI_RGB))
   {
#ifdef DEBUG
      debugMessage("%s is not 256 colour RGB Windows BMP", name);
#endif
      return;
   }

   /*
    * Read the Colour Table
    */

   RGBQUAD pal[256];

   if(!rFile->read(pal, sizeof(pal)))
      return;

   /*
    * Make ourselves a colour remap table
    */

   HPALETTE sysPal = Palette::get();

   for(int i = 0; i < 256; i++)
      remap[i] = (UCHAR) GetNearestPaletteIndex(sysPal, RGB(pal[i].rgbRed, pal[i].rgbGreen, pal[i].rgbBlue));

   close();
}

MapFileDisk::~MapFileDisk()
{
   close();
}

Boolean MapFileDisk::loadMapSection(DIB* dest, const RECT& r)
{
   ASSERT(r.top >= 0);
   ASSERT(r.left >= 0);
   ASSERT((r.left + r.right) <= biWidth);
   ASSERT((r.top + r.bottom) <= biHeight);

   open();
   ASSERT(rFile != 0);

   /*
    * Load area into DIB (at top left if DIB's size > image)
    *
    * Note that BMP is upside down, but we have made our DIB top down
    *
    * So we load the image from the bottom up so that disk reads
    * are in a forward direction.
    */

   PUCHAR pb = dest->getBits();
   ASSERT(pb != 0);

   pb += dest->getStorageWidth() * r.bottom;

   int count = r.bottom;
   int y = biHeight - (r.top + r.bottom);    // Images on disk are upside down!

   while(count--)
   {
      pb -= dest->getStorageWidth();

      rFile->seekTo(bfOffBits + y * biWidth + r.left);
      rFile->read(pb, r.right);

      // Remap to current palette

      PUCHAR pb1 = pb;
      int bCount = r.right;
      while(bCount--)
      {
         *pb1 = remap[*pb1];
         pb1++;
      }

      y++;
   }

   return False;
}

Boolean MapFileDisk::open()
{
   if(rFile == 0)
      rFile = new WinFileBinaryReader(fileName);

   return (rFile != 0);
}

void MapFileDisk::close()
{
   if(rFile)
   {
      delete rFile;
      rFile = 0;
   }
}


/*
 *----------------------------------------------------------------------
 * $Log$
 *----------------------------------------------------------------------
 */
