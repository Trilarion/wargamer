/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef INSERTS_HPP
#define INSERTS_HPP

#ifndef __cplusplus
#error inserts.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *	Draw Inserts
 *
 *----------------------------------------------------------------------
 */

class InsertDisplayImp;
class DrawDIBDC;
class MapWindowData;

class InsertDisplay
{
		InsertDisplayImp* d_insertImp;

      InsertDisplay(const InsertDisplay&);
      InsertDisplay& operator = (const InsertDisplay&);

	public:
		InsertDisplay();
		~InsertDisplay();

		void drawInserts(DrawDIBDC* dib, const MapWindowData& mapData);
};

#endif /* INSERTS_HPP */

