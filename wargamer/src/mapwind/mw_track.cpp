/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 * Map Window Tracking and Zooming
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "mw_track.hpp"
#include "mw_data.hpp"
#include "campdint.hpp"
#include "campint.hpp"
#include "armies.hpp"
#include "town.hpp"
#include "scenario.hpp"
#include "options.hpp"
#include "armyutil.hpp"

#include "resdef.h"   // for cursor
#include "app.hpp"    // for cursor


#ifdef DEBUG
#include "todolog.hpp"
#include "logwin.hpp"
#endif

/*--------------------------------------------------------------------
 * Constants
 */

static const ULONG MAX_TRACK_DISTANCE = 16;  // Max distance from object to be tracked

/*
 * Constructor
 */

MapWindowTrack::MapWindowTrack(MapWindowData* mapData) :
   d_mapData(mapData),
   d_cursor(NULL),
   d_draggingStatus(False),
   d_overPlayerItem(False),
   d_overObject(False)
{
#if defined(CUSTOMIZE)
   d_trackMode = MWTM_None;
#endif
}

MapWindowTrack::~MapWindowTrack()
{
}


void MapWindowTrack::onLButtonDown(HWND hwnd, const PixelPoint& p, UINT keyFlags)
{
#if !defined(EDITOR)
#if defined(CUSTOMIZE)
   if(d_mapData->editMode == PM_Playing)
#endif
   {
     static MapSelect info;
     d_mapData->fillMapSelect(info);
     d_mapData->d_user->onButtonDown(info);

   }
#if defined(CUSTOMIZE)
   else
#endif
#endif   // !EDITOR
#if defined(CUSTOMIZE)
   if(d_mapData->editDial == 0)
   {
         MapSelect info;
         d_mapData->fillMapSelect(info);
         d_mapData->d_user->editObject(info, p);
   }
   else  // d_mapData->editDial != 0
   {
      MapSelect info;
      d_mapData->fillMapSelect(info);
      d_trackMode = d_mapData->editDial->onSelect(d_trackMode, info);
   }
#endif   // !CUSTOMIZE
}



Boolean MapWindowTrack::setCursor()
{
#if defined(CUSTOMIZE)
   if(d_trackMode != MWTM_None)
   {
      if(d_cursor == NULL)
         d_cursor = LoadCursor(NULL, IDC_CROSS);
      ASSERT(d_cursor != NULL);
      SetCursor(d_cursor);
      return TRUE;
   }
#endif

   if(d_draggingStatus)
   {
#ifdef DEBUG
      g_logWindow.printf("Setting drag cursor");
#endif
      HCURSOR hCursor = scenario->getDragCursor();
      ASSERT(hCursor);

      SetCursor(hCursor);
      return True;
   }

   else if(d_overPlayerItem) //isPlayerControl())
   {
      HCURSOR hCursor = scenario->getGloveCursor();
      ASSERT(hCursor != 0);
      SetCursor(hCursor);
      return True;
   }

   return False;
}

#if 0
Boolean MapWindowTrack::isPlayerControl()
{
#if defined(EDITOR)
   return True;

#else // Normal game version...

   if(d_mapData->trackedUnits.unitCount() > 0)
   {
     ASSERT(d_mapData->currentUnit != NoUnitListID);
     const Armies& armies = d_mapData->d_campData->getArmies();
     return CampaignArmy_Util::isUnitOrderable(d_mapData->trackedUnits.getUnit(d_mapData->currentUnit));
   }

   if(d_mapData->d_selectedTown != NoTown)
   {
     return d_mapData->d_campData->isTownOrderable(d_mapData->d_selectedTown);
   }

   return False;

#endif

}
#endif

Boolean MapWindowTrack::isPlayerUnit()
{
#if defined(EDITOR)
   return True;

#else // Normal game version...
   if(d_mapData->trackedUnits.unitCount() > 0)
   {
     const Armies& armies = d_mapData->d_campData->getArmies();

     for(int i = 0; i < d_mapData->trackedUnits.unitCount(); i++)
     {
       if(CampaignArmy_Util::isUnitOrderable(d_mapData->trackedUnits.getUnit(i)))
         return True;
     }
   }

   return False;
#endif
}

Boolean MapWindowTrack::isPlayerTown()
{
#if defined(EDITOR)
   return True;

#else // Normal game version...
   if(d_mapData->d_selectedTown != NoTown)
   {
     return d_mapData->d_campData->isTownOrderable(d_mapData->d_selectedTown);
   }

   return False;
#endif
}

Boolean MapWindowTrack::track(HWND hwnd, int x, int y, UINT keyFlags)
{
   Boolean changed = False;

   Boolean wasOverObject = d_overObject;
   d_overObject = False;
   d_overPlayerItem = False;

#if defined(CUSTOMIZE)
   switch(d_mapData->editMode)
   {
#endif   // CUSTOMIZE
#if !defined(EDITOR)
#if defined(CUSTOMIZE)
   case PM_Playing:
#endif   // CUSTOMIZE
      {

         if(trackTowns(hwnd, x, y, keyFlags))
           changed = True;
         if(trackUnits(hwnd, x, y, keyFlags))
           changed = True;

//       if(!changed && wasOverObject)
         if(!d_overObject && wasOverObject)
         {
           d_mapData->d_user->notOverObject();
           changed = True;
         }

         if(d_draggingStatus || d_overObject)
         {
           static MapSelect info;
           d_mapData->fillMapSelect(info);

           if(d_draggingStatus)
             d_mapData->d_user->onDrag(info);
           else
           {
             d_mapData->d_user->overObject(info);
             d_overPlayerItem = (isPlayerTown() || isPlayerUnit());
           }
         }

      }  // Playing
#if defined(CUSTOMIZE)
      break;
#endif
#endif   // EDITOR
#if defined(CUSTOMIZE)

   default:
      if(d_mapData->editDial == 0)
      {
         switch(d_mapData->editMode)
         {
         case PM_EditTown:
            d_trackMode = MWTM_Town;
            break;
         case PM_EditProvince:
            d_trackMode = MWTM_Province;
            break;
         case PM_EditConnection:
            d_trackMode = MWTM_Town;
            break;
         case PM_EditOB:
            d_trackMode = MWTM_Unit;
            break;
         default:
            break;
         }
      }


      switch(d_trackMode)
      {
         case MWTM_Town:
            if(trackTowns(hwnd, x, y, keyFlags))
               changed = True;
            break;

         case MWTM_Unit:
            if(trackUnits(hwnd, x, y, keyFlags))
               changed = True;
            break;

         case MWTM_Province:
            if(trackProvinces(hwnd, x, y, keyFlags))
               changed = True;
            break;

         case MWTM_Position:
         default:
            break;
      }
   }
#endif   // CUSTOMIZE

// d_overPlayerItem = (isPlayerUnit() || isPlayerTown());

   return changed;
}

#if 0    // Sent to user with MapSelect filled in instead
/*
 * Not really sure where these functions should go
 * They manipulate data in mw_data
 * But are only ever used here.
 *
 * Really, mw_data should be seperated into seperate parts so that
 * objects on the display are virtual objects, each with their own
 * tracking, clicking, etc, functions.  That would allow new types of
 * display objects to be added without altering any of the functions
 * inside mapwindow.
 */

void MapWindowTrack::clickTown(const PixelPoint& p)
{
// if(isPlayerControl())
   {
     MapSelect info;
     d_mapData->fillMapSelect(info);
     d_mapData->d_user->onLClick(info);
   }
}

void MapWindowTrack::clickUnit(const PixelPoint& p)
{
// if(isPlayerControl())
   {
     MapSelect info;
     d_mapData->fillMapSelect(info);
     d_mapData->d_user->onLClick(info);
   }
}
#endif



/*
 * Track town underneath mouse.
 *
 * How shall we do this?
 *  1) Keep a list of everything on screen in pixel coordinates
 *  2) Convert to real world coordinates and find closest.
 *
 * - Method 2 could find town that is off screen.
 * - Method 1 requires memory.  Is there any other need for a list of
 *   displayed towns?
 *
 * The towns ought to be sorted into a tree, sorted alternately by X and
 * Y coordinates... see Sedgewick.
 *
 * To get things going, let's use Method 2.
 *
 * Problem of map perspective, means distanceToPixel not working
 */

Boolean MapWindowTrack::trackTowns(HWND hwnd, int x, int y, UINT keyFlags)
{
   Boolean changed = False;

   CArrayIter<Town> tl = d_mapData->d_campData->getTowns();

   ITown bestID = NoTown;
   ITown lastID = NoTown;
   const Town* bestTown = 0;
   const Town* lastTown = 0;
   ULONG bestDist = 0;

   while(++tl)
   {
      const Town& town = tl.current();

      PixelPoint p;
      d_mapData->locationToPixel(town.getLocation(), p);

      if(d_mapData->onDisplay(p))
      {
         ULONG d = aproxDistance(p.getX() - x, p.getY() - y);
         if(d < MAX_TRACK_DISTANCE)
         {
            Boolean track = False;
            if(bestTown)
            {
              if( (town.getSize() < bestTown->getSize()) ||
                  ( (d < bestDist) && (town.getSize() == bestTown->getSize()) ) )  // less than is actually greater than
                track = True;                                                   // in townsize enum largest town starts at 0
            }
            else
            {
               track = True;
            }

            if(track)
            {
              bestTown = &town;
              bestDist = d;
              bestID = tl.currentID();
            }
         }
      }
   }

   if(bestID != NoTown)
   {
     if(d_mapData->d_selectedTown != bestID)
     {
       changed = True;
       d_mapData->d_selectedTown = bestID;
     }
     d_overObject = True;
   }
   else
   {
     d_mapData->d_selectedTown = NoTown;
   }

   d_mapData->d_selectedProvince = NoProvince;

   return changed;
}

Boolean MapWindowTrack::trackUnits(HWND hwnd, int x, int y, UINT keyFlags)
{
   Boolean changed = False;

   ConstICommandPosition currentCPI = (d_mapData->trackedUnits.unitCount() > 0) ?
         d_mapData->trackedUnits.getUnit(0) : NoCommandPosition;

   d_mapData->trackedUnits.reset();

   const Armies& armies = d_mapData->d_campData->getArmies();
   NationIter nationIter = &armies;
   Side side = 0;
   while(++nationIter)
   {
      Side side = nationIter.current();

      ConstUnitIter unitIter(&armies, armies.getFirstUnit(side));
      while(unitIter.sister())
      {
         ConstICommandPosition cp = unitIter.current();
         // const CommandPosition* cp = armies.getCommand(cpi);
         const CampaignPosition& pos = cp->getPosition();
         Location l;
         // positionToLocation(d_mapData->d_campData, pos, l);
         pos.getLocation(l);
         PixelPoint p;
         d_mapData->locationToPixel(l, p);
         if(d_mapData->onDisplay(p))
         {
            ULONG d = aproxDistance(p.getX() - x, p.getY() - y);
            if(d < MAX_TRACK_DISTANCE)
            {
               if( (!CampaignOptions::get(OPT_FogOfWar)) ||
                   (CampaignArmy_Util::isUnitVisible(d_mapData->d_campData, cp)) )
                 d_mapData->trackedUnits.addUnit(cp);
            }
         }
      }

      side++;
   }


   if(d_mapData->trackedUnits.unitCount() > 0)
   {
      static MapSelect info;

      if(d_mapData->trackedUnits.getUnit(0) != currentCPI)
      {
        d_mapData->fillMapSelect(info);
        d_mapData->currentUnit = 0;
        changed = True;
      }

      d_overObject = True;
   }
   else
      d_mapData->currentUnit = NoUnitListID;

   return changed;
}


Boolean MapWindowTrack::trackProvinces(HWND hwnd, int x, int y, UINT keyFlags)
{
   Boolean changed = False;


   PixelPoint p = PixelPoint(x, y);

   Location l;
   d_mapData->pixelToLocation(p, l);

   // HWND hWndStatus = campWind->getStatusBarHandle();
   // ASSERT(hWndStatus != NULL);

   ProvinceIter pl = d_mapData->d_campData->getProvinces();

   IProvince bestProv = NoProvince;
   Distance bestDist = 5000;

   while(++pl)
   {
      const Province& prov = pl.current();

      Distance d = distanceLocation(prov.getLocation(), l);
      if( (bestProv == NoProvince) || (d < bestDist) )
      {
         bestProv = pl.currentID();
         // bestProv = &prov;
         bestDist = d;
      }
   }

#if 0
   if(bestProv != NoProvince)
   {
      const Province* pr = &d_mapData->d_campData->getProvince(bestProv);

      HWND hWndStatus = campWind->getStatusBarHandle();

      if(pr->getName())
         SendMessage(hWndStatus, SB_SETTEXT, 1, (LPARAM) pr->getName());
      else
         SendMessage(hWndStatus, SB_SETTEXT, 1, (LPARAM) "Unnamed");

      if(pr->getShortName())
         SendMessage(hWndStatus, SB_SETTEXT, 2, (LPARAM) pr->getShortName());
      else
         SendMessage(hWndStatus, SB_SETTEXT, 2, (LPARAM) "");
   }
   else
   {
      SendMessage(hWndStatus,  SB_SETTEXT, 1, (LPARAM) "");
      SendMessage(hWndStatus,  SB_SETTEXT, 2, (LPARAM) "");
   }
#endif

   if(d_mapData->d_selectedProvince != bestProv)
   {
      d_mapData->d_selectedProvince = bestProv;
      // requestRedraw(FALSE);
      changed = True;
   }

   return changed;
}




/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.2  2001/06/18 08:25:45  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
