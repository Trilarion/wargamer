# Microsoft Developer Studio Project File - Name="BattleEditor" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Application" 0x0101

CFG=BattleEditor - Win32 Debug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "BattleEditor.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "BattleEditor.mak" CFG="BattleEditor - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "BattleEditor - Win32 Release" (based on "Win32 (x86) Application")
!MESSAGE "BattleEditor - Win32 Debug" (based on "Win32 (x86) Application")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""
CPP=cl.exe
MTL=midl.exe
RSC=rc.exe

!IF  "$(CFG)" == "BattleEditor - Win32 Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "..\..\exe"
# PROP Intermediate_Dir "Release"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_MBCS" /YX /FD /c
# ADD CPP /nologo /MD /W3 /GR /GX /Zi /O2 /I "..\batwind" /I "..\batdisp" /I "..\h" /I "..\system" /I "..\batdata" /I "..\gamesup" /I "..\ob" /I "..\..\res" /I "..\batai" /I "..\batlogic" /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_MBCS" /YX /FD /c
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x809 /d "NDEBUG"
# ADD RSC /l 0x809 /i "..\.." /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:windows /machine:I386
# ADD LINK32 comctl32.lib User32.lib gdi32.lib ComDlg32.lib /nologo /subsystem:windows /debug /machine:I386

!ELSEIF  "$(CFG)" == "BattleEditor - Win32 Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "..\..\exe"
# PROP Intermediate_Dir "Debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_MBCS" /YX /FD /GZ /c
# ADD CPP /nologo /MDd /W3 /Gm /GR /GX /ZI /Od /I "..\batdisp" /I "..\h" /I "..\system" /I "..\batdata" /I "..\gamesup" /I "..\ob" /I "..\..\res" /I "..\batai" /I "..\batlogic" /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_MBCS" /YX /FD /GZ /c
# ADD BASE MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x809 /d "_DEBUG"
# ADD RSC /l 0x809 /i "..\.." /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:windows /debug /machine:I386 /pdbtype:sept
# ADD LINK32 comctl32.lib User32.lib gdi32.lib ComDlg32.lib /nologo /subsystem:windows /debug /machine:I386 /out:"..\..\exe/BattleEditorDB.exe" /pdbtype:sept

!ENDIF 

# Begin Target

# Name "BattleEditor - Win32 Release"
# Name "BattleEditor - Win32 Debug"
# Begin Group "Source Files"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat"
# Begin Source File

SOURCE=.\bat_oob.cpp
# End Source File
# Begin Source File

SOURCE=.\batedit.cpp
# End Source File
# Begin Source File

SOURCE=.\btool.cpp
# End Source File
# Begin Source File

SOURCE=.\dialogs.cpp
# End Source File
# Begin Source File

SOURCE=.\editgame.cpp
# End Source File
# Begin Source File

SOURCE=.\editwind.cpp
# End Source File
# Begin Source File

SOURCE=.\locator.cpp
# End Source File
# Begin Source File

SOURCE=.\sidebar.cpp
# End Source File
# End Group
# Begin Group "Header Files"

# PROP Default_Filter "h;hpp;hxx;hm;inl"
# Begin Source File

SOURCE=.\bat_oob.hpp
# End Source File
# Begin Source File

SOURCE=.\btool.hpp
# End Source File
# Begin Source File

SOURCE=.\dialogs.hpp
# End Source File
# Begin Source File

SOURCE=.\editgame.hpp
# End Source File
# Begin Source File

SOURCE=.\editwind.hpp
# End Source File
# Begin Source File

SOURCE=.\locator.hpp
# End Source File
# Begin Source File

SOURCE=.\sidebar.hpp
# End Source File
# Begin Source File

SOURCE=.\stdinc.hpp
# End Source File
# End Group
# Begin Group "Resource Files"

# PROP Default_Filter "ico;cur;bmp;dlg;rc2;rct;bin;rgs;gif;jpg;jpeg;jpe"
# Begin Source File

SOURCE=..\..\res\batedit.rc
# End Source File
# Begin Source File

SOURCE=..\..\res\batedres.h
# End Source File
# Begin Source File

SOURCE=..\..\res\batres.h
# End Source File
# Begin Source File

SOURCE=..\..\res\battle.rc
# PROP Exclude_From_Build 1
# End Source File
# Begin Source File

SOURCE=..\..\res\resource.rc
# PROP Exclude_From_Build 1
# End Source File
# End Group
# End Target
# End Project
