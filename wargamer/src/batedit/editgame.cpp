/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *      Battle Game
 *
 *----------------------------------------------------------------------
 */


#include "stdinc.hpp"
#include "editgame.hpp"
#include "batctrl.hpp"
#include "refptr.hpp"
#include "editwind.hpp"
#include "batdata.hpp"
#include "bcreate.hpp"
#include "btimctrl.hpp"
#include "sync.hpp"
#include "batmap.hpp"
#include "batdisp.hpp"
//#include "string.hpp"
#include "simpstr.hpp"
#include "savegame.hpp"
#include "filecnk.hpp"
#include "batarmy.hpp"
#include "control.hpp"
#include "batai.hpp"
#include "obNames.hpp"

#include <memory>           // Standard template

#if !defined(NOLOG)
#include "clog.hpp"
// LogFileFlush bgLog("batgame.log");
LogFile bgLog("batgame.log");
#endif

//--------- This locks up the compiler in debug mode.
// using namespace WG_BattleWindows;
using namespace BattleWindows_Internal;

/*
 * Game Control Data
 */

class BattleGameData :
	public BattleEditorInterface
{
        public:
                BattleGameData() :
                        d_windows(0),
                        d_battleData(new BattleData())
                        // d_threadManager(),
                        // d_timeControl()
//                      //  d_logicThread(0)
                {
//						d_battleData->map()->create(HexCord(128,128));
                }

                ~BattleGameData()
                {
//                        d_threadManager.terminate();
                        // Most things are destroyed automatically with RefPtr
                        // but seeing as there as several cross-references, it is
                        // important to delete things in the correct order
                        // e.g. BattleWindows contains a pointer to the BattleData

//                        delete d_logicThread;
//                        d_logicThread = 0;
                        if(d_windows) {
                            delete d_windows;
                            d_windows = 0; }

                        if(d_battleData) {
                            delete d_battleData;
                            d_battleData = 0; }

                }

                /*
                 * Functions Used by BattleGameImp
                 */

                // BattleData* battleData() { return d_battleData.get(); }
                BattleData* battleData() { return d_battleData; }
                // const BattleData* battleData() const { return d_battleData.get(); }
                const BattleData* battleData() const { return d_battleData; }

                void makeWindows()
                {
                        ASSERT(d_windows == 0);
                        ASSERT(d_battleData != 0);
                        // ASSERT(d_windows.get() == 0);
                        // ASSERT(d_battleData.get() != 0);
                        ASSERT(d_battleData->hexSize().x() != 0);
                        ASSERT(d_battleData->hexSize().y() != 0);
                        // d_windows = auto_ptr<BattleWindows>(new BattleWindows(d_battleData.get()));
                        d_windows = new BattleWindows(this);	// d_battleData);
                }


#if 0
/*
                void startLogic()
                {
                        // ASSERT(d_windows.get() != 0);
                        ASSERT(d_windows != 0);
                        // ASSERT(d_battleData.get() != 0);
                        ASSERT(d_battleData != 0);
                        ASSERT(d_battleData->hexSize().x() != 0);
                        ASSERT(d_battleData->hexSize().y() != 0);

                        // d_logicThread = auto_ptr<BattleLogic>(new BattleLogic(&d_threadManager, d_battleData.get()));
//                        d_logicThread = new BattleLogic(&d_threadManager, d_battleData);
//                        d_logicThread->start();

                        // d_timeThread->start();
                        // MMTimer::start();
                }
*/
                void pauseGame(bool pauseMode)
                {
                        if(pauseMode)
                                d_threadManager.pause();
                        else
                                d_threadManager.unpause();
                }
                /*
                 * Functions used by BattleTimeThread
                 */

                ThreadManager* threadManager() { return &d_threadManager; }

                BattleMeasure::BattleTime::Tick addTicks(int ticks)
                {
                        return d_timeControl.addTicks(ticks);
                }
/*
                bool runLogic(BattleMeasure::BattleTime::Tick batTicks)
                {
                        // if(d_logicThread.get() != 0)
                        if(d_logicThread != 0)
                        {
                                d_logicThread->updateTime(batTicks);
                                return true;
                        }
                        return false;
                }
*/
#endif
                bool redrawMaps()
                {
                        // if(d_windows.get() != 0)
                        if(d_windows != 0)
                        {
                                d_windows->timeChanged();
                                return true;
                        }
                        return false;
                }
#if 0
                bool processSound()
                {

                        if(d_battleData->soundSystem() != 0)
                        {
                                 // process audio, passing the viewers screen-centre hexcoord as a reference point for spatial positioning
                                 d_battleData->soundSystem()->ProcessSound(d_battleData->getHex(d_windows->getBattleMap()->getDisplay()->getCentre(), BattleData::Mid) );
                                 return true;
                        }
                        return false;
                }
#endif
		      void surrender() { }
             void endBattle() { }
      		BatTimeControl* timeControl() { return 0; }					// &d_timeControl; }
      		const BatTimeControl* timeControl() const { return 0; }	// &d_timeControl; }
				void sendPlayerMessage(const BattleMessageInfo&) { }

				void openBattle();
				void saveBattle(bool promptName);

                void setController(Side side, GamePlayerControl::Controller control) { }
            virtual void convertAIScriptToBinary();
		  private:
		  		void writeData();

        private:
                BattleWindows* d_windows;
                BattleData* d_battleData;
                // auto_ptr<BattleWindows> d_windows;
                // auto_ptr<BattleData>         d_battleData;

                // ThreadManager                           d_threadManager;
                // BatTimeControl                          d_timeControl;

//                BattleLogic*    d_logicThread;
                // auto_ptr<BattleLogic>        d_logicThread;

					 String d_fileName;
};

#if 0
/*
 * Thread used to control game
 *
 * Note that BattleTimeThread contains a pointer to BattleGameImp,
 * which contains a pointer back to BattleTimeThread.
 */

class BattleTimeThread : public Thread
{
        public:
                BattleTimeThread(BattleGameData* gameData);
                virtual ~BattleTimeThread();

        private:
                virtual void run();
                        // Implement Thread

        private:
                BattleGameData* d_gameData;
};
#endif

class BattleGameImp
        // public RefBaseDel
        // public BattleDataUser,
        // public BattleTimeUser
        // private MMTimer
{
                BattleGameData d_data;
                // auto_ptr<BattleTimeThread>      d_timeThread;

                // Unimplemented copy

                BattleGameImp(const BattleGame&);
                BattleGameImp& operator =(const BattleGameImp&);

        public:
                BattleGameImp();
                ~BattleGameImp();

                BattleData* battleData() { return d_data.battleData(); }
                const BattleData* battleData() const { return d_data.battleData(); }

                void makeWindows() { d_data.makeWindows(); }

//                void startLogic() { d_data.startLogic(); }

                // void pauseGame(bool pauseMode) { d_data.pauseGame(pauseMode); }

        private:
                // void addTime(SysTick::Value ticks);

                // virtual void addTick(int ticks);
                        // Implements BattleTimeUser

                /*
                 * Implements BattleDataUser
                 */

                // virtual void dataChanged();

                /*
                 * Implements MMTimer
                 */

                // virtual void proc() { addTime(1); }

                //----------- WindowOwner implementation
                // virtual void windowDestroyed(HWND hWnd);
};

#if 0
/*-------------------------------------------------------------------------
 * BattleTimeThread Functions
 */

BattleTimeThread::BattleTimeThread(BattleGameData* gameData) :
        Thread(gameData->threadManager()),
        d_gameData(gameData)
{
        ASSERT(gameData != 0);
}


BattleTimeThread::~BattleTimeThread()
{
}

virtual void BattleTimeThread::run()
{
        Greenius_System::WaitableCounter timer(1000/SysTick::TicksPerSecond, true);

        while(wait(timer) != TWV_Quit)
        {
                int ticks = timer.getAndClear();

                /*
                 * Convert SysTick into BattleTick using the current
                 * realTime to GameTime ratio.
                 * Tell Logic how much time has passed.
                 */

#if !defined(NOLOG)
                static int count = 0;
                bgLog << ">addTick " << count << ", ticks=" << ticks << ", " << sysTime << endl;
#endif

                BattleMeasure::BattleTime::Tick battleTicks = d_gameData->addTicks(ticks);

                if(battleTicks != 0)
                {
/*
                        if(d_gameData->runLogic(battleTicks))
                        {
                                d_gameData->redrawMaps();
                                d_gameData->processSound();
                        }
*/

                }

#if !defined(NOLOG)
                bgLog << "<addTick " << count << ", battleTicks=" << battleTicks << ", " << sysTime << endl;
                ++count;
#endif
        }
}
#endif

/*-----------------------------------------------------------------------
 * BattleGameData Functions
 */

/*--------------------------------------------------------------------------
 * BattleGameImp Functions
 */

BattleGameImp::BattleGameImp()
{
        // d_timeThread = auto_ptr<BattleTimeThread>(new BattleTimeThread(&d_data));
        // d_timeThread->start();
}

BattleGameImp::~BattleGameImp()
{
        // d_timeThread = 0;            // don't want recursive pointers
}


/*==========================================================================
 * Class Interface
 */

BattleGame::BattleGame()
{
        d_imp = new BattleGameImp;
}


BattleGame::~BattleGame()
{
        delete d_imp;
}

BattleData* BattleGame::battleData()
{
        ASSERT(d_imp != 0);
        return d_imp->battleData();
}

const BattleData* BattleGame::battleData() const
{
        ASSERT(d_imp != 0);
        return d_imp->battleData();
}

void BattleGame::makeWindows()
{
        ASSERT(d_imp != 0);
        d_imp->makeWindows();
}


#if 0
/*
void BattleGame::startLogic()
{
        ASSERT(d_imp != 0);
        d_imp->startLogic();
}
*/
void BattleGame::pauseGame(bool pauseMode)
{
        ASSERT(d_imp != 0);
        d_imp->pauseGame(pauseMode);
}

void BattleGame::addTime(SysTick::Value ticks)
{
        ASSERT(d_imp != 0);
        d_imp->addTime(ticks);
}
#endif



void BattleGameData::openBattle()
{
	// forward message to top in game
	// but editor can just do it here
	// because it is single threaded no need to worry about windows
	// trying to display data while data is being loaded

	/*
	 * Prompt for a filename
	 */

	SimpleString fileName;
	if(SaveGame::askLoadName(fileName, SaveGame::Battle))
	{
		d_fileName = fileName.toStr();

		FileWithMode<WinFileBinaryChunkReader> file(d_fileName.c_str(), SaveGame::Battle);

		// BattleOB* ob = d_battleData->ob();
		// d_battleData->ob(0);
		// ob->deleteGenericOB();
		// delete ob;
      d_battleData->deleteOB();


		d_battleData->clear();
		d_battleData->readData(file, 0, true);    // load Generic OB and intialise deployment map
	}


}

void BattleGameData::saveBattle(bool promptName)
{
	SimpleString fileName;

	if(promptName || (d_fileName.length() == 0))
	{
		if(SaveGame::askSaveName(fileName, SaveGame::Battle))
		{
			d_fileName = fileName.toStr();
		}
		else
			return;
	}

   OB_Names::writeFile();           // Must be done BEFORE saving the OB

	FileWithMode<WinFileBinaryChunkWriter> file(d_fileName.c_str(), SaveGame::Battle);

	FileTypeChunk ftype(SaveGame::Battle);
	ftype.writeData(file);

	d_battleData->writeData(file, true);    // Save generic OB as well as Battle
   BattleMap::writeNameTable();     // Must be done AFTER saving the hexmap
}

void BattleGameData::convertAIScriptToBinary()
{
   if(d_fileName.c_str())
   {
      BattleAI ai(d_battleData);
      ai.convertScript(d_fileName.c_str());
   }
}

