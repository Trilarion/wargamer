/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef BAT_OOB_HPP
#define BAT_OOB_HPP

/*

Dialog & controls for Battle OOB editing

*/

#include "batarmy.hpp"
// #include "bobiter.hpp"
#include "bobutil.hpp"
#include "batedres.h"
#include "batmap.hpp"                   // batdisp: Battlefield display window



extern BOOL unit_page_status;
extern BOOL leader_page_status;

extern HWND unit_hwnd;
extern HWND leader_hwnd;

extern BOOL oob_init_ok;


extern BattleMeasure::HexCord OOB_MouseSelectHex;
extern BattleMeasure::HexCord OOB_OldMouseSelectHex;
extern bool OOB_MouseSelected;

enum UnitTypeEnum {
    CP,
    SP
};


struct UnitDescriptor {

    UnitTypeEnum type;
    BattleCP * cp;
    BattleSP * sp;

    UnitDescriptor(BattleCP * unit) {
        type = CP;
        cp = unit;
        sp = NULL;
    }

    UnitDescriptor(BattleSP * unit) {
        type = SP;
        cp = NULL;
        sp = unit;
    }
        
};
    




class OBDeployUser {
public:

    OBDeployUser(void) { }
    virtual ~OBDeployUser(void) { }

    virtual void OnSelChange(BattleUnit * unit) = 0;
    virtual void OnDeployUnit(BattleCP * cp) = 0;
    virtual void OnRemoveUnit(BattleCP * cp) = 0;
    virtual void OnDragUnitFromTreeView(BattleCP * cp) = 0;

};





class BattleOOBClass {

public:
        /*
         * Data
         */


    // handle of main dialog
    HWND hwnd;
    HWND treeview_hwnd;
    HWND tabctrl_hwnd;

    OBDeployUser * m_DeployUser;

    // shortcut menus
    HMENU CP_menu; // main menu for CP
    HMENU CP_unittype_submenu; // popup for adding unit types
    HMENU CP_infantry_submenu; // list of infantry
    HMENU CP_cavalry_submenu; // list of cavalry
    HMENU CP_artillery_submenu; // list of artillery
    HMENU CP_special_submenu; // list of special

    HMENU SP_menu;

    // map window
    BattleMapWind * d_mapWind;

    // battle data
    PBattleData d_batData;

    // the order of battle
    BattleOB* d_ob;

    // which CP has the current selection in the TeeeView
    RefBattleCP CurrentSelectionCP;
    RefBattleSP CurrentSelectionSP;
    // treeitem which has current selection in TreeView
    HTREEITEM CurrentSelectionHItem;

    // CP which shortcut menu is acting upon
    RefBattleCP MenuSelectCP;
    RefBattleSP MenuSelectSP;
    // treeitem which shortcut menu is acting upon
    HTREEITEM MenuSelectHItem;

    bool tree_initialized;

    static const char s_defaultPortrait[];

public:

        /*
         * Public Functions
         */


    BattleOOBClass(HWND parent, HINSTANCE  instance, PBattleData batData, BattleMapWind * mapWind, OBDeployUser * deployuser);
    ~BattleOOBClass(void);

    void SetSelection(BattleCP * cp);
    void EnsureVisible(BattleCP * cp);
    void EnsureVisibleAndExpanded(BattleCP * cp);
    void FindItemNext(BattleCP * cp, HTREEITEM hitem, bool select_item, bool expand_item);
    void FindItemPrev(BattleCP * cp, HTREEITEM hitem, bool select_item, bool expand_item);

	void MoveCPUnit(BattleCP * detatching_unit, BattleCP * attatchto_unit);
    
         bool isVisible() const;
         void show();
         void hide();

         void reset(BattleOB* ob);
         void destroy();

public:
         /*
          * Private Functions
          */


    static BOOL APIENTRY DefaultDialogProc(HWND hWnd, UINT Msg, WPARAM wParam, LPARAM lParam);
    static BOOL APIENTRY UnitDialogProc(HWND hWnd, UINT Msg, WPARAM wParam, LPARAM lParam);
    static BOOL APIENTRY LeaderDialogProc(HWND hWnd, UINT Msg, WPARAM wParam, LPARAM lParam);

    void InitControls(void);
    void InitTreeView(void);

    void FillTreeFromOOB(Side side);
    void AddCPs(RefBattleCP cp, HTREEITEM h_parenttreeitem);
    void AddChildSPs(RefBattleCP parent_cp, HTREEITEM h_parenttreeitem);

    void RenameTreeItem(HTREEITEM hitem);

    void FillDialogsFromCP(RefBattleCP cp);
    void FillDialogsFromSP(RefBattleSP sp);

    void ClearCPDialogs(void);
    void ClearSPDialogs(void);

    bool FillReinforcementDetails(BattleCP * cp);
    bool IsReinforcement(BattleCP * cp);
    void AddToReinforcements(BattleCP * cp);
    bool RemoveFromReinforcements(BattleCP * cp);
    
    BOOL UpdateCPFromEditCtrl(int wID, HWND hwnd);
    BOOL UpdateCPFromComboCtrl(int wID, HWND hwnd);
    BOOL UpdateCPFromTrackbarCtrl(HWND hwnd);
    BOOL UpdateCPFromCheckBox(int wID, HWND hwnd);

    BOOL UpdateSPFromEditCtrl(int wID, HWND hwnd);
    BOOL UpdateSPFromComboCtrl(int wID, HWND hwnd);
    BOOL UpdateSPFromTrackbarCtrl(HWND hwnd);
    BOOL UpdateSPFromCheckBox(int wID, HWND hwnd);
    
    void CreateTabs(void);

    void ConstructCPShortcutMenu(RefBattleCP cp, int xpos, int ypos);
    void ConstructCPShortcutSubMenus(RefBattleCP cp);

    void ConstructSPShortcutMenu(RefBattleSP parent_sp, int xpos, int ypos);

    void OnCPMenu(int wID);
    void OnCPInfantrySubMenu(int wID);
    void OnCPCavalrySubMenu(int wID);
    void OnCPArtillerySubMenu(int wID);
    void OnCPSpecialSubMenu(int wID);

    void OnSPMenu(int wID);

    // bugfixed delete functions which comply with the generic OB problems
    void RemoveSPs(RefBattleCP battle_cp);
    void RemoveSP(RefBattleSP battle_sp);
        
    void DeleteCP(RefBattleCP MenuSelectCP);
    void DeleteSP(RefBattleSP MenuSelectSP);
    void RemoveUnit(const RefBattleCP cp);
        
    // helper functions
    char * GetFacingString(BattleMeasure::HexPosition::Facing facing);
    char * GetRankString(RankEnum rank);
    char * GetCPFormationString(BOB_Definitions::CPFormation formation);
    char * GetCPDeployString(BOB_Definitions::CPDeployHow deploy);
    char * GetCPLineString(BOB_Definitions::CPLineHow line);
    char * GetSPFormationString(BOB_Definitions::SPFormation formation);
    char * GetSPFacingString(int facing);
    char * GetSpecializationString(int s);
    BattleMeasure::HexPosition::Facing IndexToFacing(int index);
    int FacingToIndex(BattleMeasure::HexPosition::Facing facing);


};





#endif

/*
 * $Log$
 * Revision 1.3  2002/11/24 13:17:48  greenius
 * Added Tim Carne's editor changes including InitEditUnitTypeFlags.
 *
 * MTC patch 122.  ifndef BAT_OOB was a typo  - originally and wrongly BAT_OOP
 *
 * Revision 1.2  2001/07/30 09:27:23  greenius
 * Various bug fixes
 *
 */
