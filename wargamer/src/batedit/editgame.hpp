/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef BATGAME_HPP
#define BATGAME_HPP

#ifndef __cplusplus
#error batgame.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *      Battle Game
 *
 *----------------------------------------------------------------------
 */

#include "refptr.hpp"
// #include "systick.hpp"
// #include "soundsys.hpp"

class BattleGameImp;
class BattleData;

class BattleGame :
        public RefBaseDel
{
                BattleGameImp* d_imp;           // Implementation class

                // Unimplemented copy

                BattleGame(const BattleGame&);
                BattleGame& operator =(const BattleGame&);

        public:
                BattleGame();
                ~BattleGame();

                BattleData* battleData();
                const BattleData* battleData() const;
                
                void makeWindows();
                        // Create the windows


//                void startLogic();
                        // Start the game running!

                // void pauseGame(bool pauseMode);
                        // Pause or unpause the game

                // void addTime(SysTick::Value ticks);
                        // Add on some time
};


#endif /* BATGAME_HPP */

