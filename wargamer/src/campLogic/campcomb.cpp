/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1995, Steven Morle-Green, All Rights Reserved
 *----------------------------------------------------------------------
 *
 * Campaign Combat Mechanics
 *
 * Contains the following implementations:
 *    Aggression RealCampaignData::campaignEnemyAggressCheck(ICommandPosition cpi, Aggression agValue)
 *    void       RealCampaignData::findCloseUnits(ICommandPosition cpi, CloseUnits& closeUnits)
 *    void       RealCampaignData::doCampaignCheckEnemy(ICommandPosition cpi)
 *    ForceRatio CampaignMovement::calcForceRatio(SPCount ourSP, SPCount enemySP)
 *    AggressResultAction getAggressResult(const CommandPosition* cp, ForceRatio ratio, Aggression aggress)
 *
 *----------------------------------------------------------------------
 */


#include "stdinc.hpp"
#include "campcomb.hpp"
#include "cc_util.hpp"
#include "cu_data.hpp"
#include "campctrl.hpp"       // Campaign Interface
#include "campdint.hpp"       // Campaign Data Implementation
#include "cc_avoid.hpp"
#include "cbatproc.hpp"
#include "cbattle.hpp"
#include "armies.hpp"
#include "c_const.hpp"
#include "campmsg.hpp"
#include "cu_mode.hpp"
#include "route.hpp"
#include "terrain.hpp"
#include "options.hpp"
#include "misc.hpp"
#include "scenario.hpp"
#include "town.hpp"
#include "routelst.hpp"
#include "wg_rand.hpp"

#ifdef DEBUG
#include "todolog.hpp"
#include "unitlog.hpp"
#endif

class CampaignCombatUtil
{
 public:

#if 0
   enum AggressResultAction {
      ARA_ForceBattle,  // Force Battle
      ARA_Engage,       // Move to Engage
      ARA_Avoid8,       // Avoid at 8 Miles
      ARA_AvoidFM,      // Avoid using Force March if neccessary
      ARA_WaitBattle,   // Accept Battle
   };
#endif

   enum AggressResultAction {
     ForceBattle,
     AwaitBattle,
     Ignore,
     IgnoreOrForce,     // chance of one or the other
     AvoidContact,      // try to keep at least 8 miles away
     Retreat,           // try to keep at least 24 miles away
     IgnoreOrRetreat    // chance of one or the other
   };

   enum ForceSize {     // compared to the enemy
     Inconsequential,   // greater than 3/1
     Smaller,           // greater than 1.3/1
     Equal,             // between 1.3/1 to 1/1.5
     Larger             // greater than 1/1.5
   };

   enum PostureDistanceLevel {
     DirectOffensive24,     // Posture is Offensive and enemy is direct threat within 24 miles
     DirectOffensive8,      // Posture is Offensive and enemy is direct threat within 8 miles
     IndirectOffensive24,   // Posture is Offensive and enemy is indirect threat within 24 miles
     IndirectOffensive8,    // Posture is Offensive and enemy is indirect threat within 8 miles
     DirectDefensive24,     // Posture is Defensive and enemy is direct threat within 24 miles
     DirectDefensive8,      // Posture is Defensive and enemy is direct threat within 8 miles
     IndirectDefensive24,   // Posture is Defensive and enemy is indirect threat within 24 miles
     IndirectDefensive8     // Posture is Defensive and enemy is indirect threat within 8 miles
   };

   enum AggressionLevel {
     LessThan55,            // aggression is less than 55
     LessThan103,           // aggression is less than 103
     LessThan154Minus,      // aggression is less than 154, value is treated as -
     LessThan154Plus,       // aggression is less than 154, value is treated as +
     LessThan205,
     Over205
   };

   enum PostureAggressionLevel {
     Level0Offensive,
     Level0Defensive,
     Level1Offensive,
     Level1Defensive,
     Level2Offensive,
     Level2Defensive,
     Level3Offensive,
     Level3Defensive,
   };

   static void doCampaignNewEnemy(CampUnitMoveData* campData, ICommandPosition cpi, ForceRatio newRatio, ForceSize forceSize, ICommandPosition enemy);
   static Orders::Aggression::Value campaignEnemyAggressCheck(CampUnitMoveData* campData, ICommandPosition cpi, Orders::Aggression::Value agValue);
   static ForceRatio calcForceRatio(SPCount ourSP, SPCount enemySP);
   static AggressResultAction getAggressResult(Orders::Aggression::Value aggress, ForceSize ratio, PostureDistanceLevel pLevel);
// static AggressResultAction getAggressResult(const CommandPosition* cp, ForceRatio ratio, Orders::Aggression::Value aggress);
   static ForceSize calcForceSizeRatio(SPCount ourSP, SPCount enemySP);
   static PostureDistanceLevel getPostureDistanceLevel(Orders::Aggression::Value a, Orders::Posture::Type p, Distance d, Boolean directThreat);
   static Boolean battleIsClose(Orders::Aggression::Value ag, Distance d);
// static void adjustRatioForFOW(CampaignData* campData, ICommandPosition enemyCPI, SPCount& enemySP);
   static Boolean enemyInTheWay(CampUnitMoveData* combData, ICommandPosition cpi, ICommandPosition enemy);
};

CampaignCombatUtil::ForceSize CampaignCombatUtil::calcForceSizeRatio(SPCount ourSP, SPCount enemySP)
{
  if(ourSP >= enemySP*3)           // we are at least 3 times larger
    return Inconsequential;

  else if(ourSP >= enemySP*1.33)   // we are at least 1/3 larger
    return Smaller;

  else if(ourSP >= enemySP/1.5)    // we are between 1/3 larger and 1.5 smaller
    return Equal;

  else
    return Larger;                 // enemy is greater than  1.5 larger
}

CampaignCombatUtil::PostureDistanceLevel CampaignCombatUtil::getPostureDistanceLevel(Orders::Aggression::Value a, Orders::Posture::Type p, Distance d, Boolean directThreat)
{
  ASSERT(a < Orders::Aggression::HowMany);
  ASSERT(p < Orders::Posture::HowMany);

  ASSERT(d <= CampaignConst::enemyNearDistance);

  if(directThreat)   // if a direct threat
  {
    if(d <= CampaignConst::enemyBattleDistance)  // if within 8 miles
    {
      return (p == Orders::Posture::Offensive) ? DirectOffensive8 : DirectDefensive8;
    }
    else
    {
      return (p == Orders::Posture::Offensive) ? DirectOffensive24 : DirectDefensive24;
    }

  }
  else             // if an indirect threat
  {
    if(d <= CampaignConst::enemyBattleDistance)  // if within 8 miles
    {
      return (p == Orders::Posture::Offensive) ? IndirectOffensive8 : IndirectDefensive8;
    }
    else
    {
      return (p == Orders::Posture::Offensive) ? IndirectOffensive24 : IndirectDefensive24;
    }
  }


}



/*-------------------------------------------------------------------------
 * Convert SP into a ratio enum
 *
 * There must be a better way than this.
 * probably something along the lines of:
 *   if(ourSP > enemySP)
 *    {
 * resulting in a maximum of 3 tests instead of 7.
 */

ForceRatio CampaignCombatUtil::calcForceRatio(SPCount ourSP, SPCount enemySP)
{
   if(enemySP == 0)
      return FR_None;
   else if(ourSP > (enemySP * 3))
      return FR_Inconsequential;
   else if(ourSP > (enemySP * 2))
      return FR_Smaller2;
   else if(ourSP > ((enemySP * 3) / 2))
      return FR_Smaller;
   else if(ourSP > ((enemySP * 3) / 4))
      return FR_Equal;
   else if(ourSP > (enemySP / 2))
      return FR_Larger;
   else if(ourSP > (enemySP / 3))
      return FR_Larger2;
   else
      return FR_Outnumbered;
}

#ifdef DEBUG
static const char* ratioNames[FR_HowMany] = {
   "FR_None",
   "FR_Inconsequential",
   "FR_Smaller2",
   "FR_Smaller",
   "FR_Equal",
   "FR_Larger",
   "FR_Larger2",
   "FR_Outnumbered"
};
#endif


/*-------------------------------------------------------------------------
 * Class for tracking all close units
 *
 * For now it just remembers the single closest enemy unit
 * It really ought to build up a complete list of all visble units
 * within 24 miles, clumping together any that are close.
 */


/*-------------------------------------------------------------------------
 * Aggression Result Table
 */


CampaignCombatUtil::AggressResultAction CampaignCombatUtil::getAggressResult(Orders::Aggression::Value aggress, ForceSize ratio, PostureDistanceLevel pLevel)
{
  const Table3D<UBYTE>& table = scenario->getAggressionResultTable();

  return static_cast<CampaignCombatUtil::AggressResultAction>(table.getValue(aggress, ratio, pLevel));
}

#if 0
static CampaignCombatUtil::AggressResultAction CampaignCombatUtil::getAggressResult(const CommandPosition* cp, ForceRatio ratio, Orders::Aggression::Value aggress)
{
   /*
    * Work out whether force is smaller or larger
    *
    * Similar or smaller is when enemy < 1/3 larger than our force
    */
#if 0
   // [Aggression][enemySize][Move/Hold]

   static AggressResultAction actions[4][3][2] = {

      // Agression 0

      { { ARA_Avoid8,         ARA_Avoid8     },       // Against Small
        { ARA_AvoidFM,        ARA_AvoidFM    },       // Equal
        { ARA_AvoidFM,        ARA_AvoidFM    } },     // Against Large

      // Agression 1

      { { ARA_Engage,         ARA_WaitBattle },       // Against Small
        { ARA_Avoid8,         ARA_Avoid8     },       // Equal
        { ARA_AvoidFM,        ARA_AvoidFM    } },     // Against Large

      // Agression 2

      { { ARA_Engage,         ARA_WaitBattle },       // Against Small
        { ARA_Engage,         ARA_WaitBattle },       // Equal
        { ARA_Avoid8,         ARA_Avoid8     } },     // Against Large

      // Agression 3

      { { ARA_ForceBattle,    ARA_WaitBattle },       // Against Small
        { ARA_ForceBattle,    ARA_WaitBattle },       // Equal
        { ARA_ForceBattle,    ARA_WaitBattle } }      // Against Large
   };
#endif

   const Table3D<UBYTE>* table = scenario->getAggressionResultTable();
   ASSERT(table != 0);

   enum { MOVE, HOLD } moveHold = cp->getCurrentOrder()->isMoveOrder() ? MOVE : HOLD;
   enum { SM, EQ, LG } larger;
   if(ratio <= FR_Smaller)
      larger = SM;
   else if(ratio >= FR_Larger)
      larger = LG;
   else
      larger = EQ;

#ifdef DEBUG
   static const char* largerName[] = { "Smaller", "Equal", "Larger" };

   static const char* moveHoldName[] = { "HOLD", "MOVE" };

   cuLog.printf("larger=%d (%s), moveHold=%d (%s), ag=%d (%s)",
      (int) larger, largerName[larger],
      (int) moveHold, moveHoldName[moveHold],
      (int) aggress, (const char*) Orders::Aggression::getName(aggress));
#endif

   /*
    * NOTE:
    *   There are some extra modifiers and special cases to do here:
    *    - Cutoff to prevent large units mindlessy fighting tiny units
    *    - Cutoff to prevent tiny units attacking large ones.
    *    - Defender of Victory point less likely to move out
    *    - 'Dug-in' less likely to move out
    */

#ifdef DEBUG
   ToDo("doCampaignCheckEnemy.. cutoffs");
   ToDo("doCampaignCheckEnemy.. defender/dug-in");
#endif

   ASSERT(aggress < table->getNTables());
   ASSERT(moveHold < table->getWidth());
   ASSERT(larger < table->getHeight());

// return actions[aggress][larger][moveHold];

   return static_cast<AggressResultAction>(table->getValue(aggress, larger, moveHold));
}
#endif

/*------------------------------------------------------------------------
 * Is enemy a direct threat?
 *
 */


Boolean isAThreat(CampaignData* campData, ICommandPosition movingCPI, ICommandPosition enemyCPI, ITown startTown, ITown destTown)
{
  IConnection iCon = CampaignRouteUtil::commonConnection(campData, startTown, destTown);
  ASSERT(iCon != NoConnection);

  Boolean inTheWay = False;

  // if enemy is on this connection
  if(iCon == enemyCPI->getConnection())
  {
    // is he between us and our first connection
    if(!movingCPI->atTown() && movingCPI->getConnection() == iCon)
    {
      // if we're headed in the same direction
      if(movingCPI->getLastTown() == enemyCPI->getLastTown())
      {
        inTheWay = (movingCPI->getDistanceAlong() < enemyCPI->getDistanceAlong());
      }
      else
      {
        inTheWay = (movingCPI->getDistanceAlong() < enemyCPI->getRemainingDistance());
      }
    }
    else
      inTheWay = True;
  }

  // if he's not... but connected and distance is less than battle distance
  else if(enemyCPI->isConnected(destTown))
  {
    Distance d = (enemyCPI->getLastTown() == destTown) ? enemyCPI->getDistanceAlong() :
          enemyCPI->getRemainingDistance();

    inTheWay = (d < CampaignConst::enemyBattleDistance);
  }

  return inTheWay;
}

Boolean isAThreat(CampaignData* campData, ICommandPosition movingCPI, ICommandPosition enemyCPI)
{
  ASSERT(campData);
  ASSERT(movingCPI != NoCommandPosition);
  ASSERT(enemyCPI != NoCommandPosition);

  Boolean inTheWay = False;

  ITown lastTown = movingCPI->getLastTown();

#ifdef DEBUG
  const Town& t = campData->getTown(lastTown);
  cuLog.printf("See if %s is a threat to %s near %s",
     (const char*)campData->getUnitName(enemyCPI).toStr(), (const char*)campData->getUnitName(movingCPI).toStr(), t.getNameNotNull());
#endif

  /*
   * See if enemy is presently at one of our route nodes, or connection in between
   */


  if(movingCPI->isRetreating())
  {
     // add retreat town if we have one
     if(movingCPI->hasRetreatTown())
     {
       if(enemyCPI->atTown())
       {
         inTheWay = (movingCPI->retreatTown() == enemyCPI->getTown());
       }
       else
       {
         inTheWay = isAThreat(campData, movingCPI, enemyCPI, lastTown, movingCPI->retreatTown());
       }
     }
  }
  else
  {
    RouteListIterNLR iter(&movingCPI->currentRouteList());
    while(++iter)
    {
      const RouteNode* node = iter.current();

#ifdef DEBUG
      const Town& t1 = campData->getTown(lastTown);
      const Town& t2 = campData->getTown(node->d_town);
      cuLog.printf("Testing nodes %d (%s) -- %d (%s)",
         static_cast<int>(lastTown), t1.getNameNotNull(),
         static_cast<int>(node->d_town), t2.getNameNotNull());
#endif

      if(enemyCPI->atTown())
      {
        inTheWay = (node->d_town == enemyCPI->getTown());
      }
      else
      {
        inTheWay = isAThreat(campData, movingCPI, enemyCPI, lastTown, node->d_town);
      }

      if(inTheWay)
        break;

      lastTown = node->d_town;
    }
  }

  return inTheWay;
}

/*
 * Is enemy a Direct Threat?
 *
 */

Boolean CampaignCombatUtil::enemyInTheWay(CampUnitMoveData* combData, ICommandPosition cpi, ICommandPosition enemy)
{
  CampaignData* campData = combData->d_campData;
  ASSERT(cpi != NoCommandPosition);
  ASSERT(enemy != NoCommandPosition);

  Boolean inTheWay = False;

  /*
   * Get some useful values
   */

  CommandPosition* cp = campData->getCommand(cpi);
  CommandPosition* enemyCP = campData->getCommand(enemy);

  /*
   * First, a couple of easy special cases
   */

  // enemy force is inconsequential
  if(cp->enemyRatio() == FR_Inconsequential)
  {
#ifdef DEBUG
    cuLog.printf("Enemy not a direct threat. Enemy is inconsequential");
#endif
    inTheWay = False;
  }

  // if fog of war is active and we don't know if enemy is moving,
  // assume he is a threat
  else if( (CampaignOptions::get(OPT_FogOfWar)) &&
           (enemyCP->getInfoQuality() <= CommandPosition::VeryLimitedInfo) )
  {
#ifdef DEBUG
    cuLog.printf("Enemy activity is unknown. Assuming he is a direct threat.");
#endif
    inTheWay = True;
  }

  // If we're both just setting around
  else if(!cp->shouldMove() && !enemyCP->shouldMove())
  {
#ifdef DEBUG
    cuLog.printf("Enemy not a direct threat. We're both just setting around");
#endif
    inTheWay = False;
  }

  // We have a target unit and unit == enemy
  else if(cp->getTarget() == enemy)
  {
#ifdef DEBUG
    cuLog.printf("Enemy is a direct threat. Close enemy is our target");
#endif
    inTheWay = True;
  }

  // Enemy has a target unit and it is us
  else if(enemyCP->getTarget() == cpi)
  {
#ifdef DEBUG
    cuLog.printf("Enemy is a direct threat. We are his target");
#endif
    inTheWay = True;
  }

  else
  {
  // if we're moving
    if(cp->shouldMove())
    {
      inTheWay = isAThreat(campData, cpi, enemy);
#ifdef DEBUG
      cuLog.printf("%s is moving and %s %s a direct-threat. He is on or near our route",
        cp->getNameNotNull(), enemyCP->getNameNotNull(), (inTheWay) ? "is" : "is not");
#endif
    }

    if(!inTheWay)
    {
      // if enemy is moving
      if(enemyCP->shouldMove())
      {
        inTheWay = isAThreat(campData, enemy, cpi);
#ifdef DEBUG
        cuLog.printf("%s %s a direct-threat. He is on or near our route",
           enemyCP->getNameNotNull(), (inTheWay) ? "is" : "is not");
#endif
      }
    }
  }

  return inTheWay;
}

void CampaignCombat::surrenderUnit(CampaignLogicOwner* campGame, ICommandPosition hunit, CloseUnits& closeUnits)
{
   CampaignData* campData = campGame->campaignData();
   Armies& armies = campData->getArmies();

#ifdef DEBUG
   cuLog.printf(hunit, "%s is surrendering", (const char*)campData->getUnitName(hunit).toStr());
#endif
   CommandPosition* cp = armies.getCommand(hunit);

   if(cp->atTown())
   {
     ITown iTown = cp->getTown();
     ASSERT(iTown != NoTown);
     Town& town = campData->getTown(iTown);

     if(town.isSieged())
     {
       CampaignMessageInfo msgInfo(town.getSide(), CampaignMessages::TownSurrender);
       msgInfo.where(iTown);
       campGame->sendPlayerMessage(msgInfo);

       CPIter uIter(&armies);
       while(uIter.sister())
       {
         ICommandPosition cpi = uIter.current();

         if(cpi->atTown() && cpi->getTown() == iTown && cpi->isSieging())
         {
           CampaignMessageInfo msgInfo(town.getSide(), CampaignMessages::FortDestroyed);
           msgInfo.cp(cpi);
           msgInfo.where(iTown);
           campGame->sendPlayerMessage(msgInfo);

           CP_ModeUtil::setMode(campData, cpi, CampaignMovement::CPM_None);
         }
       }
     }

     town.setGarrison(False);
     town.setSiege(False);
     town.resetWeeksUnderSiege();
   }

   armies.removeUnitAndChildren(hunit);

}

/*-------------------------------------------------------------------------
 * Unit encounters new enemy units
 */

void forceBattle(CampUnitMoveData* combData, ICommandPosition cpi, ICommandPosition enemy)
{
  CommandPosition* cp = combData->d_campData->getCommand(cpi);

  cp->setTarget(enemy);
  Boolean newMode = CP_ModeUtil::setMode(combData->d_campData, cpi, CampaignMovement::CPM_IntoBattle);

#ifdef DEBUG
  cuLog.printf("%s wants to attack %s",
     (const char*)combData->d_campData->getUnitName(cpi).toStr(),
     (const char*)combData->d_campData->getUnitName(enemy).toStr());
#endif

  if(newMode)
  {
    CampaignMessageInfo msgInfo(cp->getSide(), CampaignMessages::MovingInForBattle);
    msgInfo.cp(cpi);
    msgInfo.cpTarget(enemy);
    combData->d_campaignGame->sendPlayerMessage(msgInfo);
  }
}

void awaitBattle(CampUnitMoveData* combData, ICommandPosition cpi, ICommandPosition enemy)
{
  CommandPosition* cp = combData->d_campData->getCommand(cpi);

#ifdef DEBUG
  cuLog.printf("Wait for Battle");
#endif

  Boolean newMode = CP_ModeUtil::setMode(combData->d_campData, cpi, CampaignMovement::CPM_AcceptBattle);
  if(newMode)
  {
    CampaignMessageInfo msgInfo(cp->getSide(), CampaignMessages::PreparingForBattle);
    msgInfo.cp(cpi);
    msgInfo.cpTarget(enemy);
    combData->d_campaignGame->sendPlayerMessage(msgInfo);
  }
}

void ignore(CampUnitMoveData* combData, ICommandPosition cpi, ICommandPosition enemy)
{
  CommandPosition* cp = combData->d_campData->getCommand(cpi);
#ifdef DEBUG
  cuLog.printf("Ingnoring enemy");
#endif

  CampaignMessageInfo msgInfo(cp->getSide(), CampaignMessages::EncounteredEnemy);
  msgInfo.cp(cpi);
  msgInfo.cpTarget(enemy);
  combData->d_campaignGame->sendPlayerMessage(msgInfo);
}

void avoidContact8(CampUnitMoveData* combData, ICommandPosition cpi, ICommandPosition enemy)
{
  CommandPosition* cp = combData->d_campData->getCommand(cpi);

#ifdef DEBUG
  cuLog.printf("Avoid at 8");
#endif

  Boolean newMode = CP_ModeUtil::setMode(combData->d_campData, cpi, CampaignMovement::CPM_AvoidContact);
  if(newMode)
  {
    CampaignMessageInfo msgInfo(cp->getSide(), CampaignMessages::HoldingBackAt8);
    msgInfo.cp(cpi);
    msgInfo.cpTarget(enemy);
    combData->d_campaignGame->sendPlayerMessage(msgInfo);
  }
}

void retreatTo24(CampUnitMoveData* combData, ICommandPosition cpi, ICommandPosition enemy)
{
  CommandPosition* cp = combData->d_campData->getCommand(cpi);

#ifdef DEBUG
  cuLog.printf("Retreat to 24");
#endif

  Boolean newMode = CP_ModeUtil::setMode(combData->d_campData, cpi, CampaignMovement::CPM_AvoidContactFast);

  if(newMode)
  {
    CampaignMessageInfo msgInfo(cp->getSide(), CampaignMessages::AvoidingContact);
    msgInfo.cp(cpi);
    msgInfo.cpTarget(enemy);
    combData->d_campaignGame->sendPlayerMessage(msgInfo);
  }
}



void CampaignCombatUtil::doCampaignNewEnemy(CampUnitMoveData* combData, ICommandPosition cpi, ForceRatio newRatio, ForceSize forceSize, ICommandPosition enemy)
{

   CampaignData* campData = combData->d_campData;
   Armies* armies = combData->d_armies;
   CampaignBattleList& battleList = campData->getBattleList();

   CommandPosition* cp = armies->getCommand(cpi);

   /*
    * Get starting aggression depending on whether ratio has
    * increased or decreased
    */

   Orders::Aggression::Value loAg = cp->getCurrentOrder()->getAggressLevel();
   Orders::Aggression::Value hiAg = cp->getActiveAggression();

   Orders::Aggression::Value ag;
   if(newRatio < cp->enemyRatio())     // Situation got better
      ag = maximum(loAg, hiAg);
   else
      ag = minimum(loAg, hiAg);

   cp->enemyRatio(newRatio);

#ifdef DEBUG
   cuLog.printf(cpi, "Doing close unit check against %s",
      (const char*) campData->getUnitName(enemy).toStr());

   cuLog.printf("Current Aggression = %s, Current Posture = %s",
     Orders::Aggression::getName(ag),
     Orders::Posture::postureName(cp->posture()));
#endif

   /*
    * Calculate new aggression
    */

   if(!cp->aggressionLocked())
   {
     Orders::Aggression::Value aggress = (cp->enemyRatio() == FR_Inconsequential) ? ag :
                                        campaignEnemyAggressCheck(combData, cpi, ag);

     cp->setAggressionValues(aggress, cp->posture());
   }

   Boolean inTheWay = enemyInTheWay(combData, cpi, enemy);

   PostureDistanceLevel postureLevel = getPostureDistanceLevel(cp->getActiveAggression(),
           cp->posture(), combData->d_closeUnits.getClosestSeenDist(), inTheWay);

   AggressResultAction action = getAggressResult(cp->getActiveAggression(),
           forceSize, postureLevel);

#ifdef DEBUG
   cuLog.printf("Aggression = %d, ForceSize = %d, PostureLevel = %d",
       static_cast<int>(cp->getActiveAggression()),
       static_cast<int>(forceSize),
       static_cast<int>(postureLevel));
#endif

   switch(action)
   {
      /*
       * Move To Attack
       */

      case ForceBattle:
        forceBattle(combData, cpi, enemy);
        break;

      /*
       * Hold In Place and Await the enemy
       */

      case AwaitBattle:
        awaitBattle(combData, cpi, enemy);
        break;

      /*
       * Ignoring the enmy
       */

      case Ignore:
        ignore(combData, cpi, enemy);
        break;

      case IgnoreOrForce:

        /*
         * If active aggression is greater than current orders aggression
         * then we force battle
         */

        if(cp->getActiveAggression() > cp->getCurrentOrder()->getAggressLevel())
        {
          forceBattle(combData, cpi, enemy);
        }

        /*
         *  Otherwise we ignore
         */

        else
        {
          ignore(combData, cpi, enemy);
        }

        break;

      case AvoidContact:
        avoidContact8(combData, cpi, enemy);
        break;

      case Retreat:
        retreatTo24(combData, cpi, enemy);
        break;

      case IgnoreOrRetreat:
        /*
         * If active aggression is less than current orders aggression
         * then we retreat to 24
         */

        if(cp->getActiveAggression() < cp->getCurrentOrder()->getAggressLevel())
        {
          retreatTo24(combData, cpi, enemy);
        }

        /*
         *  Otherwise we ignore
         */

        else
        {
          ignore(combData, cpi, enemy);
        }

        break;

#ifdef DEBUG
      default:
        FORCEASSERT("Improper Aggress Result Action");
#endif
   }

}

/*-------------------------------------------------------------------------
 * Find out if a battle should be played
 */

Boolean CampaignCombatUtil::battleIsClose(Orders::Aggression::Value ag, Distance d)
{
   ASSERT(ag >= 0);
   ASSERT(ag < AGGRESS_HowMany);

   /*
    * Battle should always be fought even if aggression is timid.
    * There is no way out of it if less than 2 miles
    */

   static Distance battleDistance[AGGRESS_HowMany] = {
      MilesToDistance(2),
      MilesToDistance(2),
      MilesToDistance(4),
      MilesToDistance(6)
   };

   return d <= battleDistance[ag];
}

/*-------------------------------------------------------------------------
 * Check for enemy units within 24 miles
 *
 * This is rather a large function... break it down!
 *
 * Return True if no longer near enemy
 */

Boolean CampaignCombat::checkEnemy(CampUnitMoveData* combData, ICommandPosition cpi)
{
   Boolean result = False;

   CampaignData* campData = combData->d_campData;
   Armies* armies = &(campData->getArmies());
   CommandPosition* cp = armies->getCommand(cpi);
   CampaignBattleList& battleList = campData->getBattleList();

   /*
    * Take Action if there are close enemy units
    */

   ICommandPosition enemy = combData->d_closeUnits.getClosestSeen();
#ifdef DEBUG
   ICommandPosition actualClosest = combData->d_closeUnits.getClosest();
#endif
   Distance closeDist = combData->d_closeUnits.getClosestSeenDist();
   if(enemy == NoCommandPosition)
   {
      /*
       * Not near enemy any more, so stop retreating, etc
       */

      if(cp->enemyRatio() != FR_None)
      {
#ifdef DEBUG
         cuLog.printf(cpi, "Clearing closeUnitCheck");
#endif

         /*
          * Get him back to doing what he was upto before encountering enemy.
          * This may need more subtle effects, e.g. if he was retreating
          * and is now out of range of enemy, there is no point in him
          * immediately trying to go back where he just retreated from!
          * He could be clever enough to look for a new route.
          *
          * On the other hand, if the enemy have just run away then
          * by all means continue with the original order.
          */

         result = True;
      }
   }

   /*
    * If it's night time, then do not try to do anything like
    * initiate battles
    *
    * This is a bit shaky, because if bump into enemy in the
    * night while retreating, should do some kind of mode change.
    * At the very least to stop moving.
    */

   else if(!combData->d_nightTime)
   {
      CampaignBattle* closeBattle = 0;

      if(cp->shouldCheckEnemy() &&
         closeDist < CampaignConst::enemyNearDistance)   // 24 miles
      {
#ifdef DEBUG
        cuLog.printf(cpi, "closest seen is %s, closest unseen is %s",
           (const char*) campData->getUnitName(enemy).toStr(),
           (const char*)campData->getUnitName(actualClosest).toStr());

        cuLog.printf("Distance=%ld (%d miles))",
           (long) closeDist,
           (int) DistanceToMile(closeDist));
#endif

        SPCount ourSP = 0;

        /*
         *  If close enemy is in battle add fighting friendly units to SPCount.
         *
         */

        CommandPosition* enemyCP = armies->getCommand(enemy);
        if(enemyCP->isInBattle())
        {
          closeBattle = battleList.findBattle(enemy);
          ASSERT(closeBattle != 0);

          ourSP = CampaignBattleProc::getFriendlySPCount(combData, closeBattle, cp->getSide());

          /*
           * use distance to the battle location if less than enemy distance
           */

          static CampaignRoute s_route(campData);

          Distance battleDistance = 0;
          if(s_route.getRouteDistance(&cp->getPosition(), closeBattle, closeDist, battleDistance))
          {
            ASSERT(battleDistance <= closeDist);
            closeDist = battleDistance;
          }
        }

        SPCount enemySP = combData->d_closeUnits.getSeenSP(CampaignConst::enemyLoseDistance);      // getUnitSPCount(enemy, True);
        ourSP += armies->getUnitSPValue(cpi);

        /*
         * Convert SP into ratio
         */


        ForceRatio newRatio = CampaignCombatUtil::calcForceRatio(ourSP, enemySP);

#ifdef DEBUG
        cuLog.printf("Ratio: ourSP=%ld, enemySP=%ld, new ratio=%s, old ratio=%s",
             static_cast<long>(ourSP),
             static_cast<long>(enemySP),
             const_cast<const char*>(ratioNames[newRatio]),
             const_cast<const char*>(ratioNames[cp->enemyRatio()]) );
#endif

        /*
         * If it's changed...
         */

        if(newRatio != cp->enemyRatio())
        {
          CampaignCombatUtil::ForceSize forceSize = CampaignCombatUtil::calcForceSizeRatio(ourSP, enemySP);
          CampaignCombatUtil::doCampaignNewEnemy(combData, cpi, newRatio, forceSize, enemy);
        }
      }

      /*
       * If enemy is within battle distance
       */

      if(!cp->isRouting() && closeDist <= CampaignConst::enemyBattleDistance)
      {
         CommandPosition* enemyCP = armies->getCommand(enemy);

         /*
          * If we are in the proper mode to do battle
          */

         if(cp->willDoBattle())
         {

           /*
            * If enemy is already in battle, join in
            */

           if(enemyCP->isInBattle())
           {
#ifdef DEBUG
             cuLog.printf(cpi, "joining battle with %s!", enemyCP->getName());
#endif

//           CampaignBattle* battle = battleList.findBattle(enemy);
             ASSERT(closeBattle != 0);

             CampaignBattleProc::addUnit(combData->d_campaignGame, closeBattle, cpi);

             CampaignMessageInfo msgInfo(cp->getSide(), CampaignMessages::JoiningBattle);
             msgInfo.cp(cpi);
             combData->d_campaignGame->sendPlayerMessage(msgInfo);
           }

           /*
            * Enemy must be in a suitable mode!
            */

           else if( (!enemyCP->isRouting()) &&
                    (enemyCP->willDoBattle() || CampaignCombatUtil::battleIsClose(enemyCP->getActiveAggression(), closeDist)) )
           {
#ifdef DEBUG
             cuLog.printf("%s initiating battle with %s!", cp->getName(), enemyCP->getName());
#endif

             CampaignBattle* battle = battleList.createBattle(cp->location());
             ASSERT(battle != 0);
             CampaignBattleProc::addUnit(combData->d_campaignGame, battle, cpi);
             CampaignBattleProc::addUnit(combData->d_campaignGame, battle, enemy);
             // battleList.startBattle(battle);
//                 CampaignBattleProc::startBattle(combData->d_campaignGame, battle);


             CampaignMessageInfo msgInfo1(cp->getSide(), CampaignMessages::EngagingInBattle);
             msgInfo1.cp(cpi);
             combData->d_campaignGame->sendPlayerMessage(msgInfo1);

             CampaignMessageInfo msgInfo2(enemyCP->getSide(), CampaignMessages::EngagingInBattle);
             msgInfo2.cp(enemy);
             combData->d_campaignGame->sendPlayerMessage(msgInfo2);

           }
#ifdef DEBUG
           else
           {
             cuLog.printf(cpi, "Trying to start battle, but enemy %s is not in the mood",
                       (const char*) campData->getUnitName(enemy).toStr());
           }
#endif
         }

         /*
          * Otherwise, see if we are in minimum battle range
          */

         else if(CampaignCombatUtil::battleIsClose(cp->getActiveAggression(), closeDist))
         {
           /*
            * if enemy is not in a battle, await battle
            */

            if(!enemyCP->isInBattle())
            {
              CP_ModeUtil::setMode(campData, cpi, CampaignMovement::CPM_AcceptBattle);
            }

#ifdef DEBUG
            cuLog.printf("%s is within close battle distance", cp->getName());
#endif
         }
      }
   }

   /*
    * Handle the avoid enemy cases
    */

   if(cp->isAvoiding())
   {
      if(CampaignAvoid::avoid(campData, cpi, combData->d_closeUnits))
         result = True;
   }

   return result;
}

/*
 * Process units actioning soundGuns order
 */

Boolean CampaignCombat::marchToBattle(ICommandPosition cpi, CampUnitMoveData* moveData)
{

  ASSERT(cpi != NoCommandPosition);
  ASSERT(moveData != 0);

  /*
   * return if we don't have soundguns advanced order,
   * we're already in or moving to battle,
   * or we're routing or withdrawing
   */

  if(!cpi->getCurrentOrder()->getSoundGuns() ||
     cpi->isMovingToBattle() ||
     cpi->isInBattle() ||
     cpi->isRouting() ||
     cpi->isWithdrawing())
  {
    return False;
  }

  /*
   * See if a battle is going nearby
   */

  ICommandPosition closeCPI = moveData->d_closeFriendlyUnits.getClosest();
  Distance closeDistance = moveData->d_closeFriendlyUnits.getClosestDist();

  ICommandPosition closeEnemyCPI = moveData->d_closeUnits.getClosestSeen();
  Distance closeEnemyDistance = moveData->d_closeUnits.getClosestSeenDist();

  ICommandPosition targetCPI = NoCommandPosition;

  if(closeCPI != NoCommandPosition &&
     closeCPI->isInBattle() &&
     closeDistance < MilesToDistance(24))
  {
    if(closeEnemyCPI == NoCommandPosition || closeDistance <= closeEnemyDistance)
      targetCPI = closeCPI;
    else
      targetCPI = closeEnemyCPI;
  }

  else if(closeEnemyCPI != NoCommandPosition &&
          closeEnemyCPI->isInBattle() &&
          closeEnemyDistance < MilesToDistance(24))
  {
    if(closeCPI == NoCommandPosition || closeEnemyDistance < closeDistance)
      targetCPI = closeEnemyCPI;
    else
      targetCPI = closeCPI;
  }

  /*
   * If battle is going nearby and unit has soundGuns option
   * test to see if he moves to the battle. Set to forcemarch if so.
   *
   * Test is %leader initiative*1.5 of 255
   */

  if(targetCPI != NoCommandPosition)
  {
    Leader* leader = moveData->d_campData->getArmies().getUnitLeader(cpi);

    int chance = MulDiv(leader->getInitiative()*1.5, 100, Attribute_Range);
    int dieRoll = CRandom::get(100);

#ifdef DEBUG
    cuLog.printf("----%s is testing for 'march to battle' orders------", cpi->getNameNotNull());
    cuLog.printf("chance of passing test is %d, die roll is %d", chance, dieRoll);
#endif

    if(dieRoll < chance)
    {
      /*
       * Unit marching to the sound of battle will forcemarch, have an
       * AttackSmall(level 2) aggression level, and an offensive posture
       */

      cpi->marchingToSoundOfBattle(True);

      cpi->setTarget(targetCPI);
      cpi->setForceMarch(True);
      cpi->setAggressionValues(Orders::Aggression::AttackSmall, Orders::Posture::Offensive);

//    CampaignRouteUtil::plotUnitRoute(moveData->d_campData, cpi);

      Boolean newMode = CP_ModeUtil::setMode(moveData->d_campData, cpi, CampaignMovement::CPM_IntoBattle);

#ifdef DEBUG
      cuLog.printf("%s is marching to %s's battle", cpi->getNameNotNull(), (const char*)moveData->d_campData->getUnitName(targetCPI).toStr());
#endif
      if(newMode)
      {
        CampaignMessageInfo msgInfo(cpi->getSide(), CampaignMessages::HearBattle);
        msgInfo.cp(cpi);
        msgInfo.cpTarget(targetCPI);
        moveData->d_campaignGame->sendPlayerMessage(msgInfo);
      }

      return True;
    }
  }

  return False;
}


/*-------------------------------------------------------------------------
 * Simple 2D Table class... will write this properly soon
 */
#if 0
template<class T>
class Table {
   UBYTE width;
   UBYTE height;
   unsigned int xRange;
   unsigned int yRange;
   T* entries;
public:
   Table(UBYTE w, UBYTE h, unsigned int x, unsigned int y, T* t)
   {
      width = w;
      height = h;
      xRange = x;
      yRange = y;
      entries= t;
   }


   T get(unsigned int x, unsigned int y) const
   {
      unsigned int x1 = (x * width) / xRange;
      unsigned int y1 = (y * height) / yRange;

      ASSERT(x1 < width);
      ASSERT(y1 < height);

      return entries[x1 + y1 * width];
   }
};

/*
 * Ben's table... should be in a data file
 * Values are % chance of changing agression
 * y value is leader's agression (in 5 ranges)
 * x value is order's agression level
 */

/*
 * Values are in quarters, i.e. +4 = 100%, +6 = 150%
 *
 * Use 3 tables to handle case of ? and ?? entries
 * Or use a special flag (e.g set bit 6 to indicate ?)
 */

static SBYTE aggressItems[5*4] = {
   0,  -4, -8, -12,
   0,  -2, -4, -8,
   0,  -2, -2, -4,      // -2 in this row is ?1/2
   0,   0, -1,  0,      // -1 in this row is ?1/4
   0,  +2, +2,  0
};

const int aggressTableDivisor = 4;

#if 0
static Table<SBYTE> aggressTable(4, 5, 4, Attribute_Range + 1, aggressItems);
#endif
#endif

/*-------------------------------------------------------------------------
 * Return the effective aggression to use when a unit spots an enemy
 * Called from the 24 mile check.
 *
 * See Ben's Design Document:
 *    "What Happens & How When Opposing Forces Come Into
 *     Contact in the Stategic Game"
 *       2) The Aggression Check.
 */

// first a couple helper functions
CampaignCombatUtil::AggressionLevel getAggressionLevel(Attribute value, Boolean plus)
{
  if(value < 55)
    return CampaignCombatUtil::LessThan55;

  else if(value < 103)
    return CampaignCombatUtil::LessThan103;

  else if(value < 154)
    return (plus) ? CampaignCombatUtil::LessThan154Plus : CampaignCombatUtil::LessThan154Minus;

  else if(value < 205)
    return CampaignCombatUtil::LessThan205;

  else
    return CampaignCombatUtil::Over205;
}

CampaignCombatUtil::PostureAggressionLevel getPostureAggressionLevel(Orders::Aggression::Value a, Orders::Posture::Type p)
{

  if(a == Orders::Aggression::Timid)
    return (p == Orders::Posture::Offensive) ? CampaignCombatUtil::Level0Offensive : CampaignCombatUtil::Level0Defensive;

  else if(a == Orders::Aggression::DefendSmall)
    return (p == Orders::Posture::Offensive) ? CampaignCombatUtil::Level1Offensive : CampaignCombatUtil::Level1Defensive;

  else if(a == Orders::Aggression::AttackSmall)
    return (p == Orders::Posture::Offensive) ? CampaignCombatUtil::Level2Offensive : CampaignCombatUtil::Level2Defensive;

  else
    return (p == Orders::Posture::Offensive) ? CampaignCombatUtil::Level3Offensive : CampaignCombatUtil::Level3Defensive;

}

Orders::Aggression::Value CampaignCombatUtil::campaignEnemyAggressCheck(CampUnitMoveData* combData, ICommandPosition cpi, Orders::Aggression::Value agValue)
{
   /*
    * This check only enabled if option is selected
    */

   if(CampaignOptions::get(OPT_AggressionChange))
   {
      Armies* armies = combData->d_armies;
      CampaignData* campData = combData->d_campData;

      CommandPosition* cp = armies->getCommand(cpi);

      ICommandPosition enemy = combData->d_closeUnits.getClosestSeen();
      ASSERT(enemy != NoCommandPosition);

      CommandPosition* enemyCP = armies->getCommand(enemy);

      // Orders::Aggression::Value agValue = cp->getCurrentOrder().aggressLevel;
      const Leader* l = armies->getLeader(cp->getLeader());

      const Table2D<SWORD>& table = scenario->getAggressionChangeTable();

#if 0 // replace with boolean expression
      bool plus = (cp->enemyRatio() < FR_Equal) ? True :
                     (cp->enemyRatio() == FR_Equal) ? static_cast<Boolean>(CRandom::get(2)) :
                     False;
#else
      bool plus = (cp->enemyRatio() < FR_Equal) ||
                  ( (cp->enemyRatio() == FR_Equal) && (CRandom::get(2) != 0) );
#endif


      /*
       * Get Aggression and posture levels
       *
       * and Modify aggression level
       * Modifier is a shift of 1 (either up or down) of aggression level
       */

      AggressionLevel aLevel = getAggressionLevel(l->getAggression(), plus);
      PostureAggressionLevel paLevel = getPostureAggressionLevel(agValue, cp->posture());

#ifdef DEBUG
      cuLog.printf("Current AggressionLevel = %d, Current PostureAggressionLevel = %d",
          static_cast<int>(aLevel),
          static_cast<int>(paLevel));
#endif

      /*
       * Hold orders in a major victory location
       *
       * TODO: Ben needs to define this
       */

      /*
       * Defensive Posture in chokepoint or field works (+1)
       */

      if(cp->posture() == Orders::Posture::Defensive)
      {
        Boolean shouldModify = False;

        if(cp->getFieldWorks() > 0)
        {
#ifdef DEBUG
          cuLog.printf("In field works");
#endif
          shouldModify = True;
        }

        else if(cp->atTown())
        {
//        Town& town = campData->getTown(cp->getTown());

//        const TerrainTypeItem& item = campData ->getTerrainType(town.getTerrain());

          if(campData->isChokePoint(cp->getTown()))
          {
#ifdef DEBUG
            cuLog.printf("In a chokepoint");
#endif
            shouldModify = True;
          }
        }


        if(shouldModify)
          aLevel = clipValue<AggressionLevel>(INCREMENT(aLevel), LessThan55, Over205);
      }

      /*
       * Offensive Posture and enemy in fieldworks (-1)
       */

      if(cp->posture() == Orders::Posture::Offensive && enemyCP->getFieldWorks() > 0)
      {
#ifdef DEBUG
        cuLog.printf("Enemy has fieldworks field works");
#endif
        aLevel = clipValue<AggressionLevel>(DECREMENT(aLevel), LessThan55, Over205);
      }

      /*
       * We are inconsequential to the enemy (-1)
       */

      if(cp->enemyRatio() == FR_Outnumbered) // enemy is 3 times greater than us
      {
#ifdef DEBUG
        cuLog.printf("Enemy outnumbers us by at least 3:1");
#endif
        aLevel = clipValue<AggressionLevel>(DECREMENT(aLevel), LessThan55, Over205);
      }

      /*
       * We have offensive posture and supply level is less than 50%
       */

      if(cp->posture() == Orders::Posture::Offensive && cp->getSupply() < Attribute_Range/2)
      {
#ifdef DEBUG
        cuLog.printf("Posture is offensive and we have less than 50%% supplies");
#endif
        aLevel = clipValue<AggressionLevel>(DECREMENT(aLevel), LessThan55, Over205);
      }

      /*
       * TODO: modifier for 'Fear of' factor (-1). Needs to be defined by Ben
       */

#ifdef DEBUG
      cuLog.printf("AggressionLevel after modifier = %d", static_cast<int>(aLevel));
#endif



      /*
       * If tableEntry isn't 0 then test to see if aggression will change
       *
       * The test is:
       *  1. Take leader initiative as a % of 255.
       *  2. Subtract this from 100.
       *  3. Multiply by absolute of table value
       *  4. This is the %chance of a leader changing his aggression
       *
       *  If chance is over 100 then test again using chance-100 as new chance
       */

      SWORD tableEntry = table.getValue(aLevel, paLevel);

      if(tableEntry != 0)
      {
         Orders::Aggression::Value oldAgValue = agValue;

         int chance = MulDiv((100 - MulDiv(l->getInitiative(), 100, Attribute_Range)), tableEntry, 10);

         int loops = 2;

         while(loops--) // Agression can change a max of 2 levels
         {
           int dieRoll = CRandom::get(100);

#ifdef DEBUG
           cuLog.printf("Chance of changing order = %d%%, die Roll = %d", chance, dieRoll);
#endif

           if(dieRoll < chance)
           {
             if(aLevel >= LessThan154Plus)
             {
               if(agValue < Orders::Aggression::Attack)
                  INCREMENT(agValue);
             }
             else
             {
               if(agValue > 0)
                  DECREMENT(agValue);
             }

#ifdef DEBUG
             cuLog.printf("Aggression changed to from %s to %s",
                Orders::Aggression::getName(oldAgValue),
                Orders::Aggression::getName(agValue));
#endif
           }

           chance -= 100;

           /*
            * If this is the second time around then test to see if posture changes
            *
            * Posture change test is:
            *   1. Same as above and
            *   2. Offensive will only change if there is a chance of
            *      aggression level dropping
            *   3. Defensive will only change if there is a chance of
            *      aggression level rising
            */

           if(!loops)
           {
             Orders::Posture::Type oldPosture = cp->posture();
             Boolean shouldTest = False;

             if(aLevel >= LessThan154Plus)
             {
               if(cp->posture() == Orders::Posture::Defensive)
                 shouldTest = True;
             }
             else
             {
               if(cp->posture() == Orders::Posture::Offensive)
                 shouldTest = True;
             }


             if(shouldTest)
             {
               int dieRoll = CRandom::get(100);

#ifdef DEBUG
               cuLog.printf("Chance of changing posture = %d%%, die roll = %d", chance, dieRoll);
#endif

               if(dieRoll < chance)
               {
                 Orders::Posture::Type newPosture = (cp->posture() == Orders::Posture::Defensive) ?
                                                     Orders::Posture::Offensive : Orders::Posture::Defensive;

                 ASSERT(oldPosture != newPosture);

                 cp->setAggressionValues(cp->getActiveAggression(), newPosture);
//               cp->posture(newPosture);

#ifdef DEBUG
                 cuLog.printf("Posture changed to from %s to %s",
                    Orders::Posture::postureName(oldPosture),
                    Orders::Posture::postureName(newPosture));
#endif
               }
             }

           }

           if(chance <= 0)
             break;

         }

         if(agValue != oldAgValue)
         {
            CampaignMessageInfo msgInfo(cp->getSide(), CampaignMessages::ChangedAggression);
            msgInfo.cp(cpi);
            msgInfo.text1(Orders::Aggression::getName(oldAgValue));
            msgInfo.text2(Orders::Aggression::getName(agValue));
            combData->d_campaignGame->sendPlayerMessage(msgInfo);
         }
      }
   }

   return agValue;
}

#if 0
/*
 * Functions called for each mode...
 * Do we really need these?
 *
 * Also... it would be better to set up a class with virtual functions
 */


// void RealCampaignData::doCampaignAcceptBattle(ICommandPosition cpi)
void CampaignCombat::acceptBattle(CampUnitMoveData* campData, ICommandPosition cpi)
{
#ifdef DEBUG
   ToDo("doCampaignAcceptBattle()");
#endif
}

void CampaignCombat::intoBattle(CampUnitMoveData* campData, ICommandPosition cpi)
{
#ifdef DEBUG
   ToDo("doCampaignIntoBattle()");
#endif
}

void CampaignCombat::inBattle(CampUnitMoveData* campData, ICommandPosition cpi)
{
#ifdef DEBUG
   ToDo("doCampaignInBattle()");
#endif
}
#endif

/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.2  2001/06/18 08:25:44  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
