/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef DS_UNIT_H
#define DS_UNIT_H

#ifndef __cplusplus
#error ds_unit.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1995, Steven Morle-Green, All Rights Reserved
 *----------------------------------------------------------------------
 *
 *	Structure for Despatch Message DSID_UNITORDER
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.2  2001/06/18 08:25:44  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 * Revision 1.1  2001/06/13 08:52:37  greenius
 * Initial Import
 *
 * Revision 1.1  1996/02/23 14:08:58  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#include "despatch.hpp"
#include "campord.hpp"
#include "armies.hpp"
#include "campdint.hpp"
#include "campint.hpp"
#include "options.hpp"
#include "cu_order.hpp"
#include "realord.hpp"		// getDescription
#include "c_obutil.hpp"

#include <windows.h>
// #include "StringPtr.hpp"

#include "MultiplayerMsg.hpp"

#include "clogic_dll.h"
#include "gamedefs.hpp"
#include "cpdef.hpp"

class OrderBase;
class CampaignData;

class CLOGIC_DLL DS_UnitOrder : public DespatchMessage {
	ICommandPosition d_unit;
	CampaignOrder d_order;
public:
	DS_UnitOrder(ICommandPosition unit, const CampaignOrder* order);
	DS_UnitOrder(void) { }
	void process(CampaignData* campData);

	virtual int pack(void * buffer, void * gamedata);
	virtual void unpack(void * buffer, void * gamedata);

#ifdef DEBUG
	String getName();
#endif

};

CLOGIC_DLL void sendUnitOrder(ConstParamCP unit, const OrderBase* order);

#endif /* DS_UNIT_H */

