/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef REALORD_HPP
#define REALORD_HPP

#ifndef __cplusplus
#error realord.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *	Actual Definitions of Real Campaign Orders
 * (Campord only contains virtual class)
 *
 *----------------------------------------------------------------------
 */

#include "clogic_dll.h"
#include "campord.hpp"
#include "simpstr.hpp"

class CampaignData;
class CampaignOrder;
class CampaignLogicOwner;

namespace CampaignOrderUtil
{
	CLOGIC_DLL SimpleString getAdvancedOrderText(const CampaignOrder* order);
	CLOGIC_DLL SimpleString getDestinationText(const CampaignData* campData, const CampaignOrder* order);
	CLOGIC_DLL SimpleString getDescription(const CampaignData* campData, const CampaignOrder* order, OD_TYPE how);
	CLOGIC_DLL SimpleString getOrderText(const CampaignData* campData, const CampaignOrder* order, OD_TYPE how);
	bool beginOrder(CampaignLogicOwner* campGame, const CampaignOrder* order, ParamCP cpi);
	CLOGIC_DLL Boolean allowed(Orders::Type::Value t, ConstParamCP cpi, const CampaignData* campData);
	CLOGIC_DLL Boolean canGiveOnArrival(const CampaignData* campData, ConstParamCP cpi,
        		const CampaignOrder& order, Orders::Type::Value o);
};




#endif /* REALORD_HPP */

