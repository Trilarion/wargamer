/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 * Close Unit List
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "cc_close.hpp"
#include "campdint.hpp"
#include "compos.hpp"
#include "fsalloc.hpp"

#ifdef DEBUG
//#define DEBUG_CLOSEUNITS
#ifdef DEBUG_CLOSEUNITS
#include "clog.hpp"
static LogFileFlush closeUnitLog("CloseUnits.log");
#endif
#endif

#ifdef DEBUG
static FixedSizeAlloc<CloseUnitItem> closeUnitItemAlloc("CloseUnitItem");
#else
static FixedSizeAlloc<CloseUnitItem> closeUnitItemAlloc;
#endif

void* CloseUnitItem::operator new(size_t size)
{
  return closeUnitItemAlloc.alloc(size);
}

#ifdef _MSC_VER
void CloseUnitItem::operator delete(void* deadObject)
{
  closeUnitItemAlloc.release(deadObject);
}
#else
void CloseUnitItem::operator delete(void* deadObject, size_t size)
{
  closeUnitItemAlloc.release(deadObject, size);
}
#endif

CloseUnits::CloseUnits()
{
   reset();
}

CloseUnits::~CloseUnits()
{
   cpList.reset();
}

void CloseUnits::reset()
{
#ifdef DEBUG
   currentUnit = NoCommandPosition;
#endif

   best = NoCommandPosition;
   bestDistance = Distance_MAX;

   bestSeen = NoCommandPosition;
   bestSeenDistance = Distance_MAX;

   spCount = 0;

   cpList.reset();
}

void CloseUnits::add(CampaignData* campData, ICommandPosition cpi, Distance d, SPCount sp)
{
   ASSERT(cpi != NoCommandPosition);
   CommandPosition* cp = campData->getCommand(cpi);

#ifdef DEBUG_CLOSEUNITS
   ASSERT(currentUnit != NoCommandPosition);

   closeUnitLog.printf("Adding %s to %s's CloseUnits",
      (const char*)campData->getUnitName(cpi).toStr(),
      (const char*)campData->getUnitName(currentUnit).toStr());

   closeUnitLog.printf("------ distance = %ld(%d miles), SPValue = %d",
         static_cast<long>(d),
         DistanceToMile(d),
         static_cast<int>(sp));

   closeUnitLog.printf("--------------- is seen = %d, is not a garrision = %d",
         static_cast<int>(cp->isSeen()),
         static_cast<int>(!cp->isGarrison()));

   closeUnitLog.printf("--------------- best distance is %ld(%d miles)",
         static_cast<long>(bestDistance),
         DistanceToMile(bestDistance));

   closeUnitLog.printf("--------------- best seen distance is %ld(%d miles)\n",
         static_cast<long>(bestSeenDistance),
         DistanceToMile(bestSeenDistance));

#endif

   ASSERT(!cpi->isDead());
   if(!cpi->isDead())
   {
      if(d < bestDistance)
      {
         best = cpi;
         bestDistance = d;

         /*
          *  if unit is seen
          */

         if(cp->isSeen() && !cp->isGarrison())
         {
           bestSeen = cpi;
           bestSeenDistance = d;
         }
      }


      spCount += sp;

      CloseUnitItem* item = new CloseUnitItem(cpi, d, sp, cp->isSeen());

      cpList.append(item);
   }
}

void CloseUnits::removeDead() const
{
   CloseUnits* ncThis = const_cast<CloseUnits*>(this);

   SListIter<CloseUnitItem> it(&ncThis->cpList);
   while (++it)
   {
      if(it->cpi->isDead())
         it.remove();
   }

   ncThis->best = NoCommandPosition;
   ncThis->bestDistance = Distance_MAX;

   ncThis->bestSeen = NoCommandPosition;
   ncThis->bestSeenDistance = Distance_MAX;

   ncThis->spCount = 0;

   it.rewind();
   while (++it)
   {
      if(it->distance < bestDistance)
      {
         ncThis->best = it->cpi;
         ncThis->bestDistance = it->distance;

         /*
          *  if unit is seen
          */

         if(it->cpi->isSeen() && !it->cpi->isGarrison())
         {
           ncThis->bestSeen = it->cpi;
           ncThis->bestSeenDistance = it->distance;
         }
      }


      ncThis->spCount += it->spCount;
   }
}

ICommandPosition CloseUnits::getClosest() const
{
   if((best != NoCommandPosition) && best->isDead())
      removeDead();

   return best;
}

ICommandPosition CloseUnits::getClosestSeen() const
{
   if((bestSeen != NoCommandPosition) && bestSeen->isDead())
      removeDead();

   return bestSeen;
}

Distance CloseUnits::getClosestDist() const
{
   removeDead();
   return bestDistance;
}

Distance CloseUnits::getClosestSeenDist() const
{
   removeDead();
   return bestSeenDistance;
}

SPCount CloseUnits::getSeenSP(Distance d) const
{
   removeDead();

   SPCount count = 0;

   SListIterR<CloseUnitItem> iter(&cpList);
   while(++iter)
   {
     const CloseUnitItem* item = iter.current();

     if(item->seen && item->distance <= d)
       count += item->spCount;
   }

   return count;
}


/*
 * Iterator Functions
 */

void CloseUnits::rewind()
{
   removeDead();
   cpList.rewind();
}

Boolean CloseUnits::operator ++()
{
   bool result;
   do
   {
      result = ++cpList;
   } while(result && cpList.getCurrent()->cpi->isDead());

   return result;
}

Boolean CloseUnits::seenOnly()
{
  Boolean notSeen;
  do
  {
    notSeen = False;

    if(operator++())  // ++cpList)
    {
      const CloseUnitItem& item = current();
      if(item.seen)
        return True;
      else
        notSeen = True;
    }
  } while(notSeen);

  return False;
}

const CloseUnitItem& CloseUnits::current()
{
   ASSERT(!cpList.getCurrent()->cpi->isDead());
   return *cpList.getCurrent();
}



/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.2  2001/06/18 08:25:44  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
