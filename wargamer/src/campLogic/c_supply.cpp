/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 * Campaign Supply Logic
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "c_supply.hpp"
// #include "campdimp.hpp"
#include "campctrl.hpp"
#include "campdint.hpp"
#include "town.hpp"
#include "armies.hpp"
#include "route.hpp"
#include "options.hpp"
#include "cc_close.hpp"
#include "camppos.hpp"
#include "scenario.hpp"
#include "weather.hpp"
#include "nations.hpp"
#include "tables.hpp"
#include "wg_rand.hpp"
#include "armyutil.hpp"
#include "terrain.hpp"

#ifdef DEBUG
#include "clog.hpp"
LogFileFlush supplyLog("supply.log");

#define TIME_FUNCTIONS

#ifdef TIME_FUNCTIONS
#include "mmtimer.hpp"

class FunctionTimer
{
   public:
      FunctionTimer(LogFile& log, const char* name) :
         d_log(&log),
         d_name(name),
         d_time(Greenius_System::MMTimer::getTime())
      {
      }

      ~FunctionTimer()
      {
         DWORD ms = Greenius_System::MMTimer::getTime() - d_time;

         d_log->printf("TimedFunction %s took %ld milliseconds",
               d_name, (long) ms);
      }

   private:
      LogFile* d_log;
      const char* d_name;
      DWORD d_time;
};
#endif

#endif

/*
 * Internal Functions
 */

class UsedTownList {
      bool* d_towns;
      int   d_nTowns;
   public:
      UsedTownList(int nTowns) :
         d_towns(new bool[nTowns]),
         d_nTowns(nTowns)
      {
         clear();
      }

      ~UsedTownList()
      {
         if(d_towns)
            delete[] d_towns;
      }

      void clear()
      {
         memset(d_towns, False, d_nTowns);
      }

      void setHasTown(ITown itown)
      {
         ASSERT(itown < d_nTowns);
         d_towns[itown] = True;
      }

      bool hasTown(ITown itown)
      {
         ASSERT(itown < d_nTowns);
         return d_towns[itown];
      }

      UsedTownList& operator = (const UsedTownList& tl)
      {
         ASSERT(tl.d_nTowns == d_nTowns);
         memcpy(d_towns, tl.d_towns, d_nTowns);
         return *this;
      }
};

class SupplyUtil {
 public:


   enum MiscMod {
     MaxSupplyNonFriendly,
     MaxForageNonFriendly,
     ForageReplenishMult
   };

   static void findCloseUnits(CampaignLogicOwner* campGame, CloseUnits& closeUnits, ITown itown, Distance maxDistance);
   static Boolean enemyInTheWay(const CampaignData* campData, ITown iTown, IConnection iCon, Side enemySide, Distance distAlong, UsedTownList& uList);
   static int findSupplyLine(const CampaignData* campData, ConstParamCP cpi, Side side,
      ITown iTown, IConnection lastConnection,
      const SPCount wantToDraw, const Distance distToFirstDepot,
      RouteList& bestList, UsedTownList& ulist, UsedTownList& eList);

   static int getSupply(
      const CampaignData* campData,
      RouteList& list,
      UsedTownList& usedList,
      UsedTownList& enemyAtTownList,
      const SPCount wantToDraw,
      Distance& distToFirstDepot,
      Side s,
      ITown iTown,
      IConnection ci,
      IConnection lastConnection,
      ConstParamCP cpi);

   // static Side getOtherSide(Side s) { return (s == 0) ? 1 : 0; }
   static Boolean adjustSupplySourcePercent(const CampaignData* campData,
      ConstParamCP cpi, ITown iTown, int& percent);
   static Boolean canPassInto(const CampaignData* campData, ConstParamCP cpi,
      Side side, ITown iTown);
   static Boolean canPassFrom(const CampaignData* campData, ConstParamCP cpi,
      Side side, ITown iTown, IConnection ic, IConnection lastConnection);
   static bool inList(RouteList& list, ITown iTown)
   {
      SListIterR<RouteNode> iter(&list);
      while(++iter)
      {
         if(iter.current()->d_town == iTown)
            return True;
      }
      return False;
   }
};

Boolean SupplyUtil::enemyInTheWay(const CampaignData* campData, ITown iTown,
   IConnection ci, Side enemySide, Distance distanceAlong, UsedTownList& uList)
{
  ConstUnitIter iter(&campData->getArmies(), campData->getArmies().getFirstUnit(enemySide));
  while(iter.sister())
  {
    ConstICommandPosition enemyCPI = iter.current();
    Boolean inTheWay = False;

    // at a town
    if(enemyCPI->atTown())
    {
      // he's in the way unless he is under siege
      if(enemyCPI->getTown() == iTown)
      {
        if(enemyCPI->isGarrison())
        {
          const Town& t = campData->getTown(iTown);
          inTheWay = !t.isSieged();
        }
        else
          inTheWay = True;
      }

      if(inTheWay)
         uList.setHasTown(iTown);
    }

    // not at a town
    else
    {
      if(enemyCPI->getConnection() == ci)
      {
        ASSERT(enemyCPI->getLastTown() == iTown || enemyCPI->getDestTown() == iTown);
        Distance enemyDistFromTown = (enemyCPI->getLastTown() == iTown) ?
           enemyCPI->getDistanceAlong() : enemyCPI->getRemainingDistance();

        inTheWay = (enemyDistFromTown <= distanceAlong);
      }
    }

    if(inTheWay)
    {
#ifdef DEBUG
      supplyLog.printf("%s is blocking our path", (const char*)campData->getUnitName(enemyCPI).toStr());
#endif
      return True;
    }
  }

  return False;
}

Boolean SupplyUtil::adjustSupplySourcePercent(const CampaignData* campData,
   ConstParamCP cpi, ITown iTown, int& percent)
{
  ASSERT(iTown != NoTown);
  const Town& t = campData->getTown(iTown);

  // If not a 'Friendly' source max percent can only be 75%
  // or if unit is from a major nation,
  // it can only have a max supply level of 75% if source is
  // not from the same nation

  const Table1D<UWORD>& table = scenario->getSupplyMiscTable();
  const int maxLevel = table.getValue(MaxSupplyNonFriendly);

  ASSERT(t.getProvince() != NoProvince);
  const Province& p = campData->getProvince(t.getProvince());
  if( (campData->getNations().getFriendlyTo(p.getNationality()) != cpi->getSide()) ||
      ( (scenario->getNationType(cpi->getNation()) == MajorNation) &&
        (cpi->getNation() != p.getNationality()) ) )
  {
    percent = minimum(maxLevel, percent);

#ifdef DEBUG
    supplyLog.printf("Supply Source (%s) adjusted to %d%%, max-level %d%%",
        t.getNameNotNull(), percent, maxLevel);
#endif
    return True;
  }

  return False;
}

/*
 * Warning! recursive function
 * Recursively finds the best supply route, adding nodes to the list
 */

int SupplyUtil::getSupply(
   const CampaignData* campData,
   RouteList& list,
   UsedTownList& usedList,
   UsedTownList& enemyAtTownList,
   const SPCount wantToDraw,
   Distance& distToFirstDepot,
   Side s,
   ITown iTown,
   IConnection ci,
   IConnection lastConnection,
   ConstParamCP cpi)
{
  ASSERT(iTown != NoTown);
  ASSERT(ci != NoConnection);

  // first, get next town
  const Connection& con = campData->getConnection(ci);
  const Distance maxRangeCapital = Town::getSupplyMaxRange(TOWN_Capital);

//  if(con.distance + distToFirstDepot > maxRangeCapital)
//  return 0;

  ITown otherTown = con.getOtherTown(iTown);
//  if(usedList.hasTown(otherTown))
//  return 0;
//  if(otherTown == startTown || otherTown == lastTown || inList(usedList, otherTown))
//  return 0;

  const Town& t = campData->getTown(iTown);
  const Town& nextTown = campData->getTown(otherTown);

#ifdef DEBUG_SUPPLYLOG
  supplyLog.printf("--->> Checking supply from %s to %s for %s",
     t.getName(), nextTown.getName(), (cpi != NoCommandPosition) ? cpi->getName() : "NULL");
#endif

  /*
   * make sure enemy isn't in the way, if he is return 0
   * Make sure we can pass from last town and into next town
   */

  ConstICommandPosition tc = NoCommandPosition;
  if(enemyInTheWay(campData, otherTown, ci, otherSide(s), con.distance, enemyAtTownList) ||
     !canPassFrom(campData, cpi, s, iTown, ci, lastConnection) ||
     !canPassInto(campData, tc, s, otherTown))
  {
    return 0;
  }


  Boolean shouldRecurse = False;
  int drawAmount = 0;

  /*
   * if town is a depot or supply source, we can add it if it is
   * friendly
   */

  if( ((nextTown.getIsDepot()) || (nextTown.getIsSupplySource() && nextTown.getSupplyLevel() > 0)) &&
      (nextTown.getSide() == s) )
  {
    Distance fullRange = 0;
    Distance maxRange = 0;
    if(nextTown.getSupplyRange(fullRange, maxRange))
    {
      /*
       * if here it means we have a depot or source(with supply available)
       */


      Distance lastTownMax = 0;
      Distance lastTownFull = 0;
      const Town& t = campData->getTown(iTown);
      if( (t.getIsDepot() || t.getIsSupplySource()) &&
          (t.getSupplyRange(lastTownFull, lastTownMax)) )
      {
        fullRange += lastTownFull;
        maxRange += lastTownMax;
      }

      // adjust supply ranges according to weather
      const Table1D<UWORD>& table = scenario->getSupplyWeatherTable();
      int base = table.getResolution();
      int mult = table.getValue(campaignData->getWeather().getGroundConditions());

      fullRange = MulDiv(fullRange, mult, base);
      maxRange = MulDiv(maxRange, mult, base);

      // adjust supply ranges according to connection quality
      // TODO: get table from scenario tables
      static const int s_table[CQ_Max] = {
         8, 10, 12
      };

      base = 10;
      mult = s_table[con.quality];
      fullRange = MulDiv(fullRange, mult, base);
      maxRange = MulDiv(maxRange, mult, base);

      // if connection distance is longer than max supply, return 0
      if((distToFirstDepot + con.distance) > maxRange)
        return 0;

      /*
       * If here, then we have a friendly depot or supply source
       * within at least max range
       */

      ASSERT(maxRange - fullRange > 0);

      drawAmount = ((con.distance + distToFirstDepot) <= fullRange) ? 100 :
        MulDiv(maxRange - (con.distance + distToFirstDepot), 100, maxRange - fullRange);

      // if a source then return amount we can draw
      if(nextTown.getIsSupplySource())
      {
        RouteNode* node = new RouteNode(otherTown);
        ASSERT(node);

        list.append(node);

        // adjust for 'non-friendly' source or 'non-nation' source, the later
        // only if cpi is from a 'Major' nation
        if(cpi != NoCommandPosition)
          adjustSupplySourcePercent(campData, cpi, otherTown, drawAmount);

        return MulDiv(wantToDraw, drawAmount, 100);
      }

      // if a depot,
      else if(nextTown.getIsDepot())
      {
        shouldRecurse = True;
      }
    }
  }

  else
  {
    distToFirstDepot += con.distance;

    // keep trying recursively if we passed maxSupplyRange
    if(distToFirstDepot <= maxRangeCapital)
    {
      drawAmount = 100;
      shouldRecurse = True;
    }
  }

  /*
   * continue on if we should recursively continue search
   */

  if(shouldRecurse)
  {
    int bestDraw = 0;
#if 0
    RouteNode* node = new RouteNode(otherTown);
    ASSERT(node);
    usedList.append(node);
#endif
    usedList.setHasTown(otherTown);

    /*
     * Continue on recursively
     */

    RouteList bestList;
//    RouteList worstList;
    UsedTownList uList(campData->getTowns().entries());
    uList = usedList;
    for(int i = 0; i < MaxConnections; i++)
    {
      IConnection ic = nextTown.getConnection(i);
      if(ic != NoTown)
      {
         const Connection& con = campData->getConnection(ic);
         if(con.distance + distToFirstDepot > maxRangeCapital)
            continue;

         ITown otherTown2 = con.getOtherTown(otherTown);
         if(uList.hasTown(otherTown2) || enemyAtTownList.hasTown(otherTown2))
            continue;

         Distance d = (nextTown.getIsDepot()) ? 0 : distToFirstDepot;

         RouteList list;
         int draw = getSupply(campData, list, uList, enemyAtTownList, (bestDraw == 0) ? wantToDraw : bestDraw, d,
                 s, otherTown, ic, ci, cpi);

#ifdef DEBUG_SUPPLYLOG
         supplyLog.printf("-->> (GetSupply) Node %s can draw %d points for %s",
               campData->getTownName(otherTown), draw, (cpi != NoCommandPosition) ? cpi->getName() : "NULL");
#endif
#if 1
         if(draw > 0)
         {
            bestDraw = draw;
            bestList = list;
            break;
         }
#else
         if(draw > bestDraw)
         {
            bestDraw = draw;
            bestList = list;
         }
#endif
      }
      else
        break;
    }

    bestDraw = MulDiv(drawAmount, bestDraw, 100);

    /*
     * if bestdraw is greater than 0
     * we have found a depot
     */

    if(bestDraw > 0)
    {
      RouteNode* node = new RouteNode(otherTown);
      ASSERT(node);

      list.insert(node);

      RouteListIterR iter(&bestList);
      while(++iter)
      {
        RouteNode* node = new RouteNode(iter.current()->d_town);
        ASSERT(node);

        list.append(node);
      }
    }

    return bestDraw;

  }

  return 0;
}

/*
 * Determine if supply line can pass through town
 */

Boolean SupplyUtil::canPassFrom(const CampaignData* campData, ConstParamCP cpi,
  Side side, ITown iTown, IConnection ic, IConnection lastConnection)
{
  ASSERT(campData);
  ASSERT(iTown != NoTown);

  const Town& t = campData->getTown(iTown);

  // we cannot pass supply if we own town, and it is sieged
  // or raided
  if(t.getSide() == side)
  {
    if(t.isSieged() || t.isRaided())
      return False;

    // if bridge is down we cannot pass supply to opposite sides
    // unless we have a bridge train;
    if(lastConnection == NoConnection)
      return True;
    if(!t.isBridgeUp() && !campData->isSameSideOfChokePoint(ic, lastConnection))
    {
      return CampaignArmy_Util::bridgeTrainAtTown(campData, iTown, side);
    }
  }

  else
  {
    const TerrainTypeItem& ti = campData->getTerrainType(t.getTerrain());

    // if unit is setting at the town, then we can pass supply to it
    if(cpi != NoCommandPosition &&
       cpi->atTown() &&
       cpi->getTown() == iTown)
    {
      // if bridge chokepoint we cannot pass supply to opposite sides
      // unless we have a bridge train;

      if(lastConnection != NoConnection &&
         !campData->isSameSideOfChokePoint(ic, lastConnection))
      {
        if(ti.isBridge() && t.isSieged())
          return CampaignArmy_Util::bridgeTrainAtTown(campData, iTown, side);
        else
          return False;
      }
      else
        return True;
    }

    // if we do not own town then we cannot pass unless
    // town is under siege, and a bridge train is present
    else if(t.isSieged())
    {
      return (ti.isBridge() && CampaignArmy_Util::bridgeTrainAtTown(campData, iTown, side));
    }

    return False;
  }

  return True;
}

/*
 * Determine if supply line can pass through town
 */

Boolean SupplyUtil::canPassInto(const CampaignData* campData, ConstParamCP cpi,
  Side side, ITown iTown)
{
  ASSERT(campData);
  ASSERT(iTown != NoTown);

  const Town& t = campData->getTown(iTown);


  /*
   * We cannot pass into if...
   */

  if(cpi == NoCommandPosition || !cpi->atTown())
  {
    // if town is not friendly, return False
    if( (t.getSide() == side) &&
        (t.isRaided() || t.isSieged()) )
    {
      return False;
    }
  }

  else if(cpi->atTown())
  {
    ASSERT(cpi->getTown() == iTown);
  }

  return True;
}

/*
 *    Find a supply route. returns %of needed supply that can be drawn
 *    Depending on distances at which units are supplied
 *    Between 0..FullSupplyDistance, units are fully supplied
 *    Between Full to Max, they are supplied proportionally to distance
 */

int SupplyUtil::findSupplyLine(const CampaignData* campData, ConstParamCP cpi, Side side,
   ITown iTown, IConnection lastConnection, const SPCount wantToDraw,
   const Distance distToFirstDepot, RouteList& bestList, UsedTownList& uList, UsedTownList& eList)
{
#ifdef DEBUG
   const char* townName = campData->getTownName(iTown);
   const char* cpName = (cpi == NoCommandPosition) ? "Null" : cpi->getName();
   const char* sideName = scenario->getSideName(side);

   supplyLog.printf("findSupplyLine(%s, %s, %s)",
      townName, cpName, sideName);
#endif


  ASSERT(iTown != NoTown);
  const Town& t = campData->getTown(iTown);
  int bestDraw = 0;    // best draw in SP
  Distance maxRange = 0;
  Distance fullRange = 0;
  Boolean addThisTown = False;
  const Distance maxRangeCapital = Town::getSupplyMaxRange(TOWN_Capital);

  /*
   * Special cases
   */

  // if we're out of maximum possible range return 0
  if(distToFirstDepot > maxRangeCapital)
    return 0;

  if(!SupplyUtil::canPassInto(campData, cpi, side, iTown))
    return 0;

  /*
   * If town is a supply source we have a line, if its a depot add it to the list
   */

  if( (t.getIsSupplySource() || t.getIsDepot()) &&
      (t.getSide() == side) )
  {
    // don't add if a depot and is under siege
    if(t.getIsDepot() && t.isSieged())
      ;
    else if(t.getSupplyRange(fullRange, maxRange))
    {
      if(distToFirstDepot > maxRange)
        return 0;

      ASSERT(maxRange - fullRange > 0);

      addThisTown = True;

      if(t.getIsSupplySource())
      {
        /*
         * Adjust draw amount, depending on range to first depot
         */

        ASSERT(distToFirstDepot <= maxRange);
        ASSERT(maxRange - fullRange > 0);

        int percent = (distToFirstDepot > fullRange) ?
          MulDiv(maxRange - distToFirstDepot, 100, maxRange - fullRange) : 100;

        // adjust for 'non-friendly' source or 'non-nation' source, the later
        // only if cpi is from a 'Major' nation
        if(cpi != NoCommandPosition)
          adjustSupplySourcePercent(campData, cpi, iTown, percent);

        bestDraw = MulDiv(wantToDraw, percent, 100);
      }
    }
  }

  /*
   * If not at a source find a supply line
   *
   * go through each connection and see if there is a depot,
   * or supply source on the other end
   */


  if( (bestDraw == 0) &&
      ((!t.getIsSupplySource()) || (t.getSide() != side)) )
  {
    uList.setHasTown(iTown);
    for(int i = 0; i < MaxConnections; i++)
    {
      IConnection ci = t.getConnection(i);

      if(ci != NoConnection)
      {
        //uList.clear();
        RouteList list;

        const Connection& con = campData->getConnection(ci);
        if(con.distance + distToFirstDepot > maxRangeCapital)
            continue;

        ITown otherTown = con.getOtherTown(iTown);
        if(uList.hasTown(otherTown) || eList.hasTown(otherTown))
            continue;

        Distance d = distToFirstDepot;
        int draw = getSupply(campData, list, uList, eList, wantToDraw, d, side, iTown,
          ci, lastConnection, cpi);

#ifdef DEBUG_SUPPLYLOG
        supplyLog.printf("-->> (FindSupplyLine) Node %s can draw %d points for %s",
               campData->getTownName(iTown), draw, (cpi != NoCommandPosition) ? cpi->getName() : "NULL");
#endif

#if 1
        if(draw > 0)
        {
          ASSERT(list.entries() > 0);
          const Town& supplyTown = campData->getTown(list.getLast()->d_town);
          ASSERT(supplyTown.getIsSupplySource());

          bestDraw = draw;
          bestList = list;
          break;
        }
#else
        if(draw > bestDraw)
        {
          ASSERT(list.entries() > 0);
          const Town& supplyTown = campData->getTown(list.getLast()->d_town);
          ASSERT(supplyTown.getIsSupplySource());

          bestDraw = draw;
          bestList = list;
        }
#endif
      }
      else
        break;
    }
  }

  if(bestDraw > 0 && addThisTown)
  {
    RouteNode* node = new RouteNode(iTown);
    ASSERT(node);

    bestList.insert(node);
  }

  return bestDraw;
}

int CampaignSupply::findSupplyLine(const CampaignData* campData, ITown iTown, SPCount wantToDraw, RouteList& bestList)
{
  SPCount bestDraw = 0;
  Distance distToFirstDepot = 0;

  bestList.reset();

  ConstICommandPosition cpi = NoCommandPosition;
  const Town& t = campData->getTown(iTown);

#ifdef DEBUG
  supplyLog.printf("-------- Finding supply for %s", t.getName());
#endif

  UsedTownList uList(campData->getTowns().entries());
  UsedTownList eList(campData->getTowns().entries());
  bestDraw = SupplyUtil::findSupplyLine(campData, cpi, t.getSide(), iTown,
     NoConnection, wantToDraw, distToFirstDepot, bestList, uList, eList);

  /*
   * if bestDraw is greater than 0 then we have a supply line
   */

#ifdef DEBUG
  if(bestDraw > 0)
  {
    ASSERT(bestList.entries() > 0);
  }
#endif

  return bestDraw;
}

int CampaignSupply::findSupplyLine(const CampaignData* campData, ParamCP cpi, const SPCount wantToDraw)
{
#ifdef TIME_FUNCTIONS
   FunctionTimer(supplyLog, "findSupplyLine::cpi");
#endif

#ifdef DEBUG
  if(lstrcmpi("4th Prussian Corps", cpi->getName()) == 0)
  {
    int i = 0;
  }

  supplyLog.printf("-------- Finding supply for %s", cpi->getName());
#endif

  SPCount bestDraw = 0;
  RouteList& supplyList = cpi->supplyRouteList();
  Distance distToFirstDepot = 0;

  cpi->resetSupplyRoute();
  UsedTownList uList(campData->getTowns().entries());
  UsedTownList eList(campData->getTowns().entries());

  // if at a town
  if(cpi->atTown())
  {
    bestDraw = SupplyUtil::findSupplyLine(campData, cpi, cpi->getSide(),
      cpi->getTown(), cpi->lastConnection(), wantToDraw, distToFirstDepot, supplyList, uList, eList);
  }

  // or on a connection
  else
  {
    // check both towns on either end of connection
    RouteList list;

    // make sure enemy isn't between us and the end town
    if(!SupplyUtil::enemyInTheWay(campData, cpi->getLastTown(), cpi->getConnection(), otherSide(cpi->getSide()), cpi->getDistanceAlong(), eList))
    {
      // last town
      distToFirstDepot = cpi->getDistanceAlong();
      int draw = SupplyUtil::findSupplyLine(campData, cpi, cpi->getSide(),
        cpi->getLastTown(), cpi->getConnection(), wantToDraw, distToFirstDepot, list, uList, eList);

      if(draw > bestDraw)
      {
        bestDraw = draw;
        supplyList = list;
      }
    }

    if(!SupplyUtil::enemyInTheWay(campData, cpi->getDestTown(), cpi->getConnection(), otherSide(cpi->getSide()), cpi->getRemainingDistance(), eList))
    {
      // next town
      list.reset();
      distToFirstDepot = cpi->getRemainingDistance();
      int draw = SupplyUtil::findSupplyLine(campData, cpi, cpi->getSide(),
         cpi->getDestTown(), cpi->getConnection(), wantToDraw, distToFirstDepot, list, uList, eList);

      if(draw > bestDraw)
      {
        bestDraw = draw;
        supplyList = list;
      }
    }
  }

#ifdef DEBUG

  /*
   * if bestDraw is greater than 0 then we have a supply line
   */

  if(bestDraw > 0)
  {
    ASSERT(supplyList.entries() > 0);
    ASSERT(supplyList.getLast()->d_town != NoTown);
    const Town& t = campData->getTown(supplyList.getLast()->d_town);
    ASSERT(t.getIsSupplySource());
  }
#endif

  return bestDraw;
}


void CampaignSupply::weeklySupply(CampaignLogicOwner* campGame)
{
   // set a thread lock while doing this
   // RWLock lock;
   // lock.startWrite();

   CampaignData* campData = campGame->campaignData();

#ifdef DEBUG
   supplyLog.printf("\nProcessing Weekly Supply on %s", campData->asciiTime());
   supplyLog.printf("--------------------------------");

   for(Nationality n = 0; n < 14; n++)
   {
      supplyLog.printf("%s is allowed %d depots",
         scenario->getNationName(n),
         campData->getNations().getNDepotsAllowed(n));
   }

   int depots[14] = {
      0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
   };

   for(ITown itown = 0; itown < campData->getTowns().entries(); itown++)
   {
      Town& t = campData->getTown(itown);
      if(t.getIsDepot())
      {
         Province& p = campData->getProvince(t.getProvince());
         depots[p.getNationality()]++;
      }
   }

   for(n = 0; n < 14; n++)
   {
      supplyLog.printf("%s has %d depots",
         scenario->getNationName(n),
         depots[n]);
   }

#endif

   /*
    * Skip if realism settings for supply is off
    */

   if(!CampaignOptions::get(OPT_Supply))
      return;

   TownList& towns = campData->getTowns();
   Armies& armies = campData->getArmies();

   /*
    * Step 1: find all supply sources and add eaches supply-rate
    * to its stockpile
    *
    * Then add forage rate to each towns forage stockpile
    */

   for(ITown iTown = 0; iTown < towns.entries(); iTown++)
   {
     Town& t = towns[iTown];

     // add supply to supply sources
     if(t.getIsSupplySource())
     {
       t.addSupplyLevel(t.getSupplyRate());
     }

     // add forage value to stockpile

     // first get modified replenishment rate
     // replinshishment rate is modified by depending on the month
     // const Date& date = const_cast<const CampaignData*>(campData)->getDate();
     const Date& date = campData->getDate();

     const Table1D<UWORD>& miscTable = scenario->getSupplyMiscTable();
     ASSERT(miscTable.getResolution() > 0);

     Attribute replenishBy = MulDiv(t.getForageBase(), miscTable.getValue(SupplyUtil::ForageReplenishMult), miscTable.getResolution());

     const Table1D<UWORD>& monthlyTable = scenario->getSupplyMonthlyTable();
     ASSERT(monthlyTable.getResolution() > 0);

     // modify for month
     replenishBy = MulDiv(replenishBy, monthlyTable.getValue(date.month), monthlyTable.getResolution());

     // max forage value is base / month modifier
     Attribute maxForage = MulDiv(t.getForageBase(), monthlyTable.getValue(date.month), monthlyTable.getResolution());

     t.setForageStock(static_cast<Attribute>(minimum(maxForage, t.getForageStock() + replenishBy)));

   }

   /*
    * Step 2: Now go through units a find a supply route for each
    * The supply line must be clear of enemy, or enemy-owned locations
    * Note: an enemy setting on a connection between supply lines, blocks supply
    */

#ifdef DEBUG
   FunctionTimer ft(supplyLog, "Weekly Supply - SupplyLines");
#endif
   for(Side s = 0; s < scenario->getNumSides(); s++)
   {
     UnitIter uiter(&armies, armies.getFirstUnit(s));
     while(uiter.sister())
     {
       ICommandPosition cpi = uiter.current();
       ASSERT(cpi->isLower(Rank_President));

#ifdef DEBUG
       if(lstrcmpi("6th Army", cpi->getName()) == 0)
       {
         int i = 0;
       }
#endif

#ifdef DEBUG
      supplyLog.printf("---");
      supplyLog.printf("Checking supplies for %s", (const char*)cpi->getName());
      supplyLog.printf("Initial supply = %d", (int) cpi->getSupply());
#endif

       armies.setCommandSupply(cpi, 0);

       // get total count of SPs
       // this is the number of supply points we want to draw
       SPCount wantToDraw = armies.getUnitSPCount(cpi, True);
       SPCount adjustedWantToDraw = wantToDraw;
       if(wantToDraw > 0)
       {
         // find route
         SPCount canDraw = findSupplyLine(campData, cpi, wantToDraw);
         ASSERT(canDraw <= wantToDraw);

         /*
          * if canDraw is greater than 0 then we have a supply line
          */

         if(canDraw > 0)
         {
           // we should have a last node and it should be a supply source
           RouteList& supplyRoute = cpi->supplyRouteList();
           ASSERT(supplyRoute.getLast());
           ASSERT(supplyRoute.getLast()->d_town != NoTown);

           Town& t = campData->getTown(supplyRoute.getLast()->d_town);
           ASSERT(t.getIsSupplySource());

           // deduct from stockpile
           // clip canDraw
           canDraw = minimum(canDraw, t.getSupplyLevel());
           t.setSupplyLevel(t.getSupplyLevel() - canDraw);

           if(cpi->isGarrison())
           {
             ASSERT(cpi->atTown());
             Town& gTown = campData->getTown(cpi->getTown());

             if(!gTown.isSieged())
             {
               if(gTown.getIsDepot())
               {
                 int garrisonStock = minimum(t.getSupplyLevel(), canDraw);
                 t.setSupplyLevel(t.getSupplyLevel() - garrisonStock);

                 gTown.setSupplyLevel(gTown.getSupplyLevel() + garrisonStock);
               }
             }
//-------------------- Commented out: SWG 26Aug99:
//-------------------- getLast()->d_town is the first town on the supply route not where the unit is now!
// #ifdef DEBUG
//              else
//                ASSERT(supplyRoute.getLast()->d_town == cpi->getTown());
// #endif
           }

#ifdef DEBUG
           supplyLog.printf("Supply Line for %s", (const char*)campData->getUnitName(cpi).toStr());
           supplyLog.printf("-- wants to draw %d points, is drawing %d points, %d points remaining)---",
               static_cast<int>(wantToDraw), static_cast<int>(canDraw), static_cast<int>(t.getSupplyLevel()));

           RouteListIterR iter(&supplyRoute);
           while(++iter)
           {
             const Town& town = campData->getTown(iter.current()->d_town);
             const char* type = (town.getIsSupplySource()) ? "Supply Source" : "Depot";

             supplyLog.printf("  %s (%s)", town.getName(), type);
           }
#endif
         }

         /*
          * If units are not 100% supplied they may try to forage from closest town.
          */

         if(canDraw < adjustedWantToDraw)
         {
           ASSERT(cpi->getCloseTown() != NoTown);
           Town& ct = campData->getTown(cpi->getCloseTown());

           if(cpi->getSide() == ct.getSide())
           {
             SPCount needToDraw = static_cast<SPCount>(adjustedWantToDraw - canDraw);
             ASSERT(needToDraw > 0);

             Attribute drawThisForage = minimum(ct.getForageStock(), needToDraw);

             /*
              * We need to account for being in 'Friendly territory'
              * this is distict from 'controled' territory
              */

             ASSERT(ct.getProvince() != NoProvince);
             const Province& p = campData->getProvince(ct.getProvince());

             // if in unfriendly terrority, modify draw amount by table value
             if(campData->getNations().getFriendlyTo(p.getNationality()) != cpi->getSide())
             {
               const Table1D<UWORD>& table = scenario->getSupplyMiscTable();
               ASSERT(table.getResolution() > 0);

               drawThisForage = MulDiv(drawThisForage, table.getValue(SupplyUtil::MaxForageNonFriendly), table.getResolution());
             }

             // if the unit has no supply line, i.e. it received all
             // its supply via forage, then the maximum supply percent
             // can only be 75%
             if(cpi->supplyRouteList().entries() == 0)
             {
               const Table1D<UWORD>& table = scenario->getSupplyMiscTable();
               drawThisForage = MulDiv(drawThisForage, table.getValue(SupplyUtil::MaxForageNonFriendly),
                   table.getResolution());
             }

             ct.setForageStock(ct.getForageStock() - drawThisForage);

             canDraw += drawThisForage;

#ifdef DEBUG
             supplyLog.printf("%s is attempting to forage from %s (%d points drawn, %d points remaining)",
               cpi->getNameNotNull(), ct.getNameNotNull(),
               static_cast<int>(drawThisForage), static_cast<int>(ct.getForageStock()));
#endif
           }

         }

         /*
          * Set unit supply attribute if we have drawn any supply
          */

         if(canDraw > 0)
         {
           // set unit supply as percentage of Attribute_Range
           int percentDrawn = MulDiv(canDraw, 100, wantToDraw);

           Attribute supply = MulDiv(percentDrawn, Attribute_Range, 100);

           // if unit is crossing a river (one with a blown bridge and no bridge train)
           // then we lose some of this supply
           // Note: this is done here because the supply is still drawn
           // from the source, but is in effect lost
           if(cpi->crossingRiver())
           {
             // we a lose a random % of supply between 30% - 70%
             int percent = CRandom::get(30, 70);
             supply = MulDiv(supply, 100 - percent, 100);
           }

           armies.setCommandSupply(cpi, supply);

#ifdef DEBUG
           supplyLog.printf("%s is %d%% supplied\n",
              cpi->getNameNotNull(), percentDrawn);
#endif
         }
       }
     }
   }

   // lock.endWrite();

}



/*-------------------------------------------------------------------------
 * Collect Information about close units on a given side to a given location
 *
 * This is used by towns
 */

void SupplyUtil::findCloseUnits(CampaignLogicOwner* campGame, CloseUnits& closeUnits, ITown itown, Distance maxDistance)
{
   CampaignData* campData = campGame->campaignData();
   TownList& towns = campData->getTowns();
   Armies& armies = campData->getArmies();

   closeUnits.reset();

   const Town& town = towns[itown];
   Location location = town.getLocation();
   Side side = town.getSide();

   /*
    * Iterate through top level of enemy side(s)
    */

   NationIter nIter(&armies);
   while(++nIter)
   {
      Side unitSide = nIter.current();

      if(side == unitSide)
      {
         ICommandPosition topUnit = armies.getFirstUnit(unitSide);

         UnitIter uIter(&armies, topUnit, false);
         while(uIter.sister())
         {
            ICommandPosition cpUnit = uIter.current();
            // CommandPosition* cpUnit = armies.getCommand(cpiUnit);

            /*
             * First do a quick check for physical proximity
             */

            Location ehLocation;
            Location etLocation;
            cpUnit->getHeadLocation(ehLocation);
            cpUnit->getTailLocation(etLocation);

            if( (distanceLocation(location, ehLocation) < maxDistance) ||
                (distanceLocation(location, etLocation) < maxDistance) )
            {
               /*
                * Now for the complicated task of finding the actual route
                * distance.
                */

               Distance routeDistance = 0;
               static CampaignRoute route(campData);
               CampaignPosition pos = itown;

               if(route.getRouteDistance(&pos, cpUnit->location(), maxDistance, routeDistance))
               {
                  /*
                   * Add unit to list of units...
                   */

                  // closeUnits.add(cpiUnit, routeDistance, armies.getUnitSPCount(cpiUnit, True, True));
                  closeUnits.add(campData, cpUnit, routeDistance, armies.getUnitSPCount(cpUnit, True));
               }
            }
         }
      }
   }
}



/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.1  2001/07/02 15:23:16  greenius
 * Battle, Campaign, Combined game and Campaign Editor now compile and run... mostly
 *
 * Revision 1.2  2001/06/18 08:25:44  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
