/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef C_CONST_HPP
#define C_CONST_HPP

#ifndef __cplusplus
#error c_const.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *	Campaign Unit Combat and Movement Constants
 *
 *----------------------------------------------------------------------
 */

#include "measure.hpp"

class CampaignConst {
 public:

	/*
	 * Constant Distances
	 */

	static const Distance enemyNearDistance;
	static const Distance enemyLoseDistance;
	static const Distance enemyBattleDistance;
   static const Distance instantBattleDistance;
	static const Distance enemyLoseBattleDistance;
	static const Distance enemySightingDistanceArmy;
	static const Distance enemySightingDistanceCorp;
	static const Distance enemySightingDistanceDivision;
	static const Distance trafficJamDistance;
   static const Distance enemyTownInfoDistance;
};




#endif /* C_CONST_HPP */

