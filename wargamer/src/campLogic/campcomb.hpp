/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef CAMPCOMB_H
#define CAMPCOMB_H

#ifndef __cplusplus
#error campcomb.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1995, Steven Morle-Green, All Rights Reserved
 *----------------------------------------------------------------------
 *
 *	Campaign Combat Mechanics
 *
 *----------------------------------------------------------------------
 */

#include "gamedefs.hpp"
#include "cpdef.hpp"
#include "measure.hpp"

struct CampUnitMoveData;
class CloseUnits;
class CampaignLogicOwner;
//class CampaignData;

class CampaignCombat {
 public:
	static Boolean checkEnemy(CampUnitMoveData* campData, ICommandPosition cpi);
		// Process actions for nearby enemy
		// Return True if no enemy were nearby

	static Boolean marchToBattle(ICommandPosition cpi, CampUnitMoveData* moveData);
		// process soundGuns orders

//	static void acceptBattle(CampUnitMoveData* campData, ICommandPosition cpi);
//	static void intoBattle(CampUnitMoveData* campData, ICommandPosition cpi);
//	static void inBattle(CampUnitMoveData* campData, ICommandPosition cpi);
	static void surrenderUnit(CampaignLogicOwner* campGame, ICommandPosition hunit, CloseUnits& closeUnits);
};

#endif /* CAMPCOMB_H */

