/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 * Campaign Combat and Movement: Avoid Contact / Withdraw
 *
 * This is not a trivial problem to solve, because it is not
 * as simple as simply moving away from enemy, or towards nearest
 * friendly town, because there may be more than 1 enemy unit in the
 * vicinity or moving towards a friendly town might involve initially
 * moving towards an enemy to the next node.
 *
 * We must be careful not to make the withdrawing unit look stupid by
 * bouncing backwards and forwards between enemies... e.g. get's close
 * to enemy 1, turns around and runs away, getting close to enemy 2, etc
 *
 *----------------------------------------------------------------------
 */


#include "stdinc.hpp"
#include "cc_avoid.hpp"
#include "campdint.hpp"
#include "scenario.hpp"
#include "route.hpp"
#include "c_const.hpp"
#include "cc_close.hpp"
#include "cc_util.hpp"
#include "armies.hpp"
#include "town.hpp"
#include "cu_mode.hpp"
#include "cbattle.hpp"
#include "cbatproc.hpp"
#include "wg_rand.hpp"
#include "weather.hpp"
#include "c_supply.hpp"
#include "armyutil.hpp"
#include "losses.hpp"
//#include "fsalloc.hpp"

#ifdef DEBUG
//#include <string.hpp>
// #include "campaign.hpp"
#include "todolog.hpp"
#include "unitlog.hpp"
static LogFileFlush retreatLog("Retreat.log");
#endif

/*------------------------------------------------------------------------
 * Local container class for holding a list of units that
 * may be blocking retreat path
 */

struct BlockingUnitItem : public SLink {

  ICommandPosition d_cpi;

  BlockingUnitItem(ICommandPosition cpi) : d_cpi(cpi) {}

  void* operator new(size_t size);
#ifdef _MSC_VER
  void operator delete(void* deadObject);
#else
  void operator delete(void* deadObject, size_t size);
#endif
  enum { ChunkSize = 10 };
};

#ifdef DEBUG
static FixedSizeAlloc<BlockingUnitItem> blockingUnitAlloc("BlockingUnit");
#else
static FixedSizeAlloc<BlockingUnitItem> blockingUnitAlloc;
#endif

void* BlockingUnitItem::operator new(size_t size)
{
  return blockingUnitAlloc.alloc(size);
}

#ifdef _MSC_VER
void BlockingUnitItem::operator delete(void* deadObject)
{
  blockingUnitAlloc.release(deadObject);
}
#else
void BlockingUnitItem::operator delete(void* deadObject, size_t size)
{
  blockingUnitAlloc.release(deadObject, size);
}
#endif


class BlockingUnits : public SList<BlockingUnitItem> {
public:
  void add(ICommandPosition cpi)
  {
    BlockingUnitItem* item = new BlockingUnitItem(cpi);
    ASSERT(item);
    append(item);
  }
};

class RetreatPriority {
public:
   enum Mode {
      BT_First = 0,
      BT_Blocked = BT_First,   // current location is physically blocked, i.e is an unfriendly chokepoint
      BT_ThroughEnemy,         // enemy is between us and freedom
      BT_BlownBridge,          // a blown bridge is between us and freedom
      BT_UnfriendlyChokePoint, // towards an unfriendly chokepoint
      BT_NearEnemy,            // enemy is within 12 miles of town
      BT_NotFriendly,          // towards unfriendly territory
      BT_Friendly,             // towards friendly territory
      BT_TowardsFriends,       // toward friendly units
      BT_AlongOriginalRoute,   // Along our original route
      BT_AlongSupply,          // Along a supply route
   };
};

/*------------------------------------------------------------------------
 * Local container class for holding a information about a retreat town
 *
 */

struct RetreatTownItem : public SLink {

  ITown d_town;
  IConnection d_con;
  RetreatPriority::Mode d_mode;
  Distance d_enemyDistance;
  SPCount d_enemySP;

  RetreatTownItem() { reset(); }

  RetreatTownItem(ITown town, IConnection con, RetreatPriority::Mode mode, Distance enemyDistance, SPCount enemySP) :
    d_town(town),
    d_con(con),
    d_mode(mode),
    d_enemyDistance(enemyDistance),
    d_enemySP(enemySP) {}

  void reset()
  {
    d_town = NoTown;
    d_con = NoConnection;
    d_mode = RetreatPriority::BT_First;
    d_enemyDistance = 0;
    d_enemySP = 0;
  }

  void* operator new(size_t size);
#ifdef _MSC_VER
  void operator delete(void* deadObject);
#else
  void operator delete(void* deadObject, size_t size);
#endif
  enum { ChunkSize = 10 };
};

#ifdef DEBUG
static FixedSizeAlloc<RetreatTownItem> retreatTownAlloc("RetreatTown");
#else
static FixedSizeAlloc<RetreatTownItem> retreatTownAlloc;
#endif

void* RetreatTownItem::operator new(size_t size)
{
  return retreatTownAlloc.alloc(size);
}

#ifdef _MSC_VER
void RetreatTownItem::operator delete(void* deadObject)
{
  retreatTownAlloc.release(deadObject);
}
#else
void RetreatTownItem::operator delete(void* deadObject, size_t size)
{
  retreatTownAlloc.release(deadObject, size);
}
#endif



class RetreatTowns : public SList<RetreatTownItem> {
public:
};

/*--------------------------------------------------------------------
 * Set up movement variables so unit retreats
 *
 * Algorithm to use is:
 *    FOR each connected node
 *       Calculate a mode and value
 *
 *    Move towards node with best mode and value
 *
 *    Pick node with highest mode.
 *    If modes are the same, then use value to choose.
 */

class BestTown {
   RetreatTownItem d_bestTown;       // the best town
   RetreatTowns d_retreatTowns;      // the list of other towns
public:
   BestTown() { reset(); }

   void reset()
   {
     d_bestTown.reset();
     d_retreatTowns.reset();
   }


   ITown getBestTown() const { ASSERT(d_bestTown.d_town != NoTown); return d_bestTown.d_town; }
   IConnection getBestConnection() const { ASSERT(d_bestTown.d_con != NoConnection); return d_bestTown.d_con; }

   Boolean add(ITown t, IConnection iCon, RetreatPriority::Mode m, Distance ed, SPCount enemySP, SPCount ourSPValue);

   RetreatPriority::Mode getMode() const { return d_bestTown.d_mode; }
   SPCount getEnemySPCount() const { return d_bestTown.d_enemySP; }
   Distance getEnemyDistance() const { return d_bestTown.d_enemyDistance; }

   void randomize(CampaignData* campData, ICommandPosition cpi);

#ifdef DEBUG
   void listChoices(CampaignData* campData);
   static const char* getModeName(RetreatPriority::Mode mode);
#endif
};

#ifdef DEBUG
const char* BestTown::getModeName(RetreatPriority::Mode mode)
{
   static const char* names[] = {
      "BT_Blocked",
      "BT_ThroughEnemy",
      "BT_BlownBridge",
      "BT_UnfriendlyChokePoint",
      "BT_NearEnemy",
      "BT_NotFriendly",
      "BT_Friendly",
      "BT_TowardsFriends",
      "BT_AlongOriginalRoute",
      "BT_AlongSupply"
   };

   ASSERT(mode <= RetreatPriority::BT_AlongSupply);
   ASSERT(mode >= 0);

   return names[mode];
}

void BestTown::listChoices(CampaignData* campData)
{
   SListIter<RetreatTownItem> iter(&d_retreatTowns);
   while(++iter)
   {
      Town& t = campData->getTown(iter.current()->d_town);
      retreatLog.printf("------ Town = %s, Mode = %s, Mode Value = %d",
           t.getName(), getModeName(iter.current()->d_mode), static_cast<int>(iter.current()->d_mode));
   }
}
#endif

Boolean BestTown::add(ITown t, IConnection iCon, RetreatPriority::Mode m, Distance ed, SPCount enemySP, SPCount ourSP)
{
   ASSERT(t != NoTown);
   ASSERT(iCon != NoConnection);

#ifdef DEBUG
   cuLog.printf("BestTown::add(%s, %s, %ld, %d, %d)",
      campaignData->getTownName(t),
      getModeName(m),
      (long) ed,
      (int) enemySP,
      (int) ourSP);
#endif

   Boolean newBest = False;

   if( (d_bestTown.d_town == NoTown) || (m > d_bestTown.d_mode) )
      newBest = True;

   else if(m == d_bestTown.d_mode)
   {
     /*
      * if we're going througn the enemy and have an
      * estimated 6:1 advantage then add
      */

     if( (d_bestTown.d_mode == RetreatPriority::BT_ThroughEnemy) &&
         (ourSP >= enemySP * 6) )
     {
         newBest = True;
     }
     else
       newBest = (ed > d_bestTown.d_enemyDistance);
   }

   // add to list regardless of whether its best town
   RetreatTownItem* rt = new RetreatTownItem(t, iCon, m, ed, enemySP);
   ASSERT(rt);

   d_retreatTowns.append(rt);;

   if(newBest)
   {
     d_bestTown = *rt;
   }

   return newBest;
}

/*
 * Put some randomization into picking retreat town
 */

void BestTown::randomize(CampaignData* campData, ICommandPosition cpi)
{
  /*
   * Note: Ben needs to properly devise a test for this
   */

  ILeader leader = cpi->getLeader();

  /*
   * for now I'll get leaders initiative and tactical ability as a percent of 512
   * this will be the chance of not randomizing pick
   */

  int percent = MulDiv(leader->getInitiative() + leader->getTactical(), 100, 512);

#ifdef DEBUG
  cuLog.printf("Testing to see if %s randomizes retreat route.", cpi->getName());
  cuLog.printf("--------Raw chance of not randomizing = %d", percent);
#endif

   /*
    * I'll throw in a few modifiers
    */

   // for having staff over 200 ( x .2)
   if(leader->getStaff() >= 200)
     percent += (percent * .2);

   // for having staff over 130 ( x .1)
   else if(leader->getStaff() >= 100)
     percent += (percent * .1);

   // for charisma over 200 (x .3)
   if(leader->getCharisma() >= 200)
     percent += (percent * .3);

   // for charisma over 150 (x .2)
   else if(leader->getCharisma() >= 150)
     percent += (percent * .2);

   // if leader is commanding over his ability ( - x.3)
   if(leader->getRankLevel().getRankEnum() < cpi->getRankEnum())
     percent -= (percent * .3);

   // if we're routing  ( - x .3)
   if(cpi->isRouting())
     percent -= (percent * .3);

   // clip it
   percent = clipValue(percent, 0, 100);

#ifdef DEBUG
   cuLog.printf("----------Modified chance of not randomizing = %d", percent);
#endif

   if(CRandom::get(100) > percent)
   {
#ifdef DEBUG
      cuLog.printf("----------Randomizing retreat route");
#endif

      // first, get the number of towns in our list
      int entries = d_retreatTowns.entries();

      // get a random pick
      int pick = CRandom::get(entries);
      ASSERT(pick < entries);

      // iterate through town list till we get to our pick
      SListIter<RetreatTownItem> iter(&d_retreatTowns);
      for(int i = 0; ++iter && i != pick; i++)
      {
        RetreatTownItem* item = iter.current();

        // don't pick a place where we have to break through
        if(item->d_mode > RetreatPriority::BT_ThroughEnemy)
          d_bestTown = *item;
      }
   }
}

/*------------------------------------------------------------------------
 * Retreat Data
 *
 * various info that the retreat logic uses
 */

struct RetreatData {
  CampaignData* d_campData;
  ITown d_town;
  IConnection d_con;
  ICommandPosition d_cpi;
  CloseUnits* d_closeUnits;
  BlockingUnits* d_blockingUnits;  // list of enemy who are blocking a given path
  SPCount d_enemySP;
  Distance d_enemyDistance;

  RetreatData() :
    d_campData(0),
    d_town(NoTown),
    d_con(NoConnection),
    d_cpi(NoCommandPosition),
    d_closeUnits(0),
    d_blockingUnits(0),
    d_enemySP(0),
    d_enemyDistance(0) {}
};

/*------------------------------------------------------------------------
 * Campaign Retreat logic
 *
 * Retreat priorities are:
 *   1. Fall back along line of supply
 *   2. Fall back along original route
 *   3. Fall back towards friends
 *   4. Move directly away from enemy & towards friendly territory
 *   5. Move directly away from enemy
 *
 */

class CampaignRetreatUtil {
public:
  static Boolean routeBlockedByChokePoint(RetreatData& data);
  static Boolean acrossBlownBridge(RetreatData& data);
  static Boolean unfriendlyChokePoint(RetreatData& data);
  static Boolean enemyBlockingPath(RetreatData& data);
  static Boolean enemyNear(RetreatData& data);
  static Boolean retreatToSupplyLine(RetreatData& data);
  static Boolean retreatToOriginalRoute(RetreatData& data);
  static Boolean retreatToFriendlyUnits(RetreatData& data);
  static Boolean retreatToUnfriendlyTown(RetreatData& data);
  static Distance getEnemyDistance(RetreatData& data);
  static Boolean shouldTryBreakOut(CampaignData* campData, ICommandPosition cpi, SPCount theirCount);

  static Boolean trappedUnit(CampaignData* campData, BestTown& bestTown, ICommandPosition cpi, BlockingUnits& bu, Distance retreatDistance);
  static void considerRetreatTown(CampaignData* campData, ICommandPosition cpi, IConnection iCon, ITown town, CloseUnits& closeUnits, BestTown& bestTown);
  static void retreatBack(CampaignData* campData, ICommandPosition cpi, CloseUnits& cl, Distance howFar);

  static Boolean dropOffGarrison(CampaignData* campData, const ICommandPosition& cpi, ITown town);
};

Boolean CampaignRetreatUtil::routeBlockedByChokePoint(RetreatData& data)
{
   ASSERT(data.d_campData != 0);
   ASSERT(data.d_con != NoConnection);
   ASSERT(data.d_cpi != NoCommandPosition);

   CommandPosition* cp = data.d_campData->getCommand(data.d_cpi);

   if(cp->atTown())
   {
     Town& t = data.d_campData->getTown(cp->getTown());

#ifdef DEBUG
     Boolean chokePoint = data.d_campData->isChokePoint(cp->getTown());
     Boolean lastConnection = (cp->lastConnection() != data.d_con);
     Boolean notOurSide = (t.getSide() != cp->getSide());

     static const char* boolName[] = {
       "False",
       "True"
     };

     cuLog.printf("At a chokepoint = %s, is last connection = %s(%d, %d), not our side = %s",
          boolName[chokePoint],
          boolName[lastConnection],
          static_cast<int>(cp->lastConnection()),
          static_cast<int>(data.d_con),
          boolName[notOurSide]);

#endif

     if(data.d_campData->isChokePoint(cp->getTown()))
     {
       if(cp->lastConnection() == NoConnection)
         return False;

       if( (t.getSide() != cp->getSide()) && (cp->lastConnection() != data.d_con) )
       {
          return !data.d_campData->isSameSideOfChokePoint(cp->lastConnection(), data.d_con);
       }
     }

   }

   return False;
}

Boolean CampaignRetreatUtil::acrossBlownBridge(RetreatData& data)
{
   ASSERT(data.d_campData != 0);
   ASSERT(data.d_town != NoTown);
   ASSERT(data.d_cpi != NoCommandPosition);
   ASSERT(data.d_con != NoConnection);

   if(data.d_cpi->atTown())
   {
     Town& t = data.d_campData->getTown(data.d_cpi->getTown());
     if(!t.isBridgeUp() && data.d_cpi->lastConnection() != NoConnection)
     {
       return !data.d_campData->isSameSideOfChokePoint(data.d_cpi->lastConnection(), data.d_con);
     }
   }

   return False;
}

Boolean CampaignRetreatUtil::unfriendlyChokePoint(RetreatData& data)
{
   ASSERT(data.d_campData != 0);
   ASSERT(data.d_town != NoTown);
   ASSERT(data.d_cpi != NoCommandPosition);

   Town& t = data.d_campData->getTown(data.d_town);

   return ( (data.d_campData->isChokePoint(data.d_town) && t.getSide() != data.d_cpi->getSide()) );
}

Boolean CampaignRetreatUtil::retreatToSupplyLine(RetreatData& data)
{
   ASSERT(data.d_campData != 0);
   ASSERT(data.d_town != NoTown);
   ASSERT(data.d_cpi != NoCommandPosition);

   RouteList& sRoute = data.d_cpi->supplyRouteList();
   SListIter<RouteNode> iter(&sRoute);
   while(++iter)
   {
      if(iter.current()->d_town == data.d_town)
         return True;
   }

   return False;
#if 0
   SPCount wantToDraw = data.d_campData->getArmies().getUnitSPCount(data.d_cpi, True);
   if( (CampaignSupply::findSupplyLine(data.d_campData, data.d_cpi, wantToDraw) > 0) )

   {
     RouteListIterR iter(&data.d_cpi->supplyRouteList());
     while(++iter)
     {
       if(iter.current()->d_town == data.d_town)
         return True;
     }
   }
#endif
}

Boolean CampaignRetreatUtil::retreatToOriginalRoute(RetreatData& data)
{
   ASSERT(data.d_campData != 0);
   ASSERT(data.d_town != NoTown);
   ASSERT(data.d_con != NoConnection);
   ASSERT(data.d_cpi != NoCommandPosition);

   data.d_cpi->rewindPreviousRoute();
   while(data.d_cpi->previousRouteIter())
   {
     ITown town = data.d_cpi->previousRouteNode();
     ASSERT(town != NoTown);

     if(data.d_town == town)
       return True;
   }

   return False;
}

Boolean CampaignRetreatUtil::retreatToFriendlyUnits(RetreatData& data)
{
   ASSERT(data.d_campData != 0);
   ASSERT(data.d_town != NoTown);
   ASSERT(data.d_cpi != NoCommandPosition);

   Armies& armies = data.d_campData->getArmies();

   /*
    * For now I'll see if a friendly unit is connected to retreat town
    */

   ICommandPosition first = armies.getFirstUnit(data.d_cpi->getSide());
   UnitIter iter(&armies, first);
   while(iter.sister())
   {
     if(iter.current() != data.d_cpi)
     {
       ICommandPosition friendCPI = iter.current();

       if(friendCPI->isConnected(data.d_town))
          return True;
     }
   }

   return False;
}


Boolean CampaignRetreatUtil::retreatToUnfriendlyTown(RetreatData& data)
{
   ASSERT(data.d_campData != 0);
   ASSERT(data.d_town != NoTown);
   ASSERT(data.d_cpi != NoCommandPosition);

   Town& t = data.d_campData->getTown(data.d_town);

   return (scenario->isEnemy(t.getSide(), data.d_cpi->getSide()));
}

Boolean CampaignRetreatUtil::enemyBlockingPath(RetreatData& data)
{
   ASSERT(data.d_campData != 0);
   ASSERT(data.d_town != NoTown);
   ASSERT(data.d_cpi != NoCommandPosition);
   ASSERT(data.d_con != NoConnection);
// ASSERT(data.d_blockingUnits);
   ASSERT(data.d_closeUnits);

   CommandPosition* cp = data.d_campData->getCommand(data.d_cpi);
   Boolean inTheWay = False;

   SPCount enemySP = 0;

   if(data.d_blockingUnits)
     data.d_blockingUnits->reset();

   data.d_closeUnits->rewind();
   while(data.d_closeUnits->seenOnly())
   {
      const CloseUnitItem& item = data.d_closeUnits->current();
      ASSERT(item.seen);

      CommandPosition* cpEnemy = data.d_campData->getCommand(item.cpi);
      Boolean blockingPath = False;

      // don't add garrison
      if(cpEnemy->isGarrison())
         continue;

      /*
       * Find any enemy units that might be in the Way
       *
       * Start out with the town
       */

      if(cpEnemy->atTown())
      {
        // if their at this town...
        if(cpEnemy->getTown() == data.d_town)
          blockingPath = True;

        // or at adjacent town that is less than enemy battle distance
        // this is unlikely but we better check anyway
        else
        {
          IConnection iCon = CampaignRouteUtil::commonConnection(data.d_campData, cpEnemy->getTown(), data.d_town);
          if(iCon != NoConnection)
          {
            const Connection& con = data.d_campData->getConnection(iCon);
            blockingPath = (con.distance < CampaignConst::enemyBattleDistance);
          }
        }
      }

      else
      {
        IConnection enemyCon = cpEnemy->getConnection();

        /*
         *  If enemy is on this connection see if he is between us and town
         */

        if(enemyCon == data.d_con)
        {

          /*
           * Find out who is closer to the town
           *
           * If we're at a town then enemy has to be between us
           */

          if(cp->atTown())
          {
            ASSERT(cp->getTown() != data.d_town);
            blockingPath = True;
          }

          else
          {
            ITown ourLastTown = cp->getLastTown();
            ITown theirLastTown = cpEnemy->getLastTown();

            if(ourLastTown == theirLastTown)
            {
              Distance ourDistance = cp->getDistanceAlong();
              Distance theirDistance = cpEnemy->getDistanceAlong();

              if(theirLastTown == data.d_town)
                blockingPath = (theirDistance <= ourDistance);
              else
                blockingPath = (ourDistance <= theirDistance);
            }
            else
            {
              Distance ourDistance = cp->getDistanceAlong();
              Distance theirDistance = cpEnemy->getRemainingDistance();

              if(theirLastTown == data.d_town)
                blockingPath = (theirDistance >= ourDistance);
              else
                blockingPath = (ourDistance >= theirDistance);
            }
          }
        }

        // or if he is connected to this town
        else if(cpEnemy->isConnected(data.d_town))
        {
          Distance d = (cpEnemy->getLastTown() == data.d_town) ? cpEnemy->getDistanceAlong() :
                cpEnemy->getRemainingDistance();

          // if he's less than battle distance, he's in the way
          blockingPath = (d < CampaignConst::enemyBattleDistance);
        }
      }

      if(blockingPath)
      {
        enemySP += item.spCount;

        if(data.d_blockingUnits)
          data.d_blockingUnits->add(item.cpi);

        if(data.d_enemyDistance == 0 ||
           item.distance < data.d_enemyDistance)
        {
          data.d_enemyDistance = item.distance;
        }

        inTheWay = blockingPath;
      }
   }

   data.d_enemySP = enemySP;
   return inTheWay;
}

Boolean CampaignRetreatUtil::enemyNear(RetreatData& data)
{
   ASSERT(data.d_campData != 0);
   ASSERT(data.d_cpi != NoCommandPosition);
   ASSERT(data.d_town != NoTown);

   Distance distance = Distance_MAX;

   data.d_closeUnits->rewind();
   while(data.d_closeUnits->seenOnly())
   {
      const CloseUnitItem& item = data.d_closeUnits->current();
      ASSERT(item.seen);

      // don't check if this is a garrison
      if(item.cpi->isGarrison())
        continue;

      if(!item.cpi->atTown() && item.cpi->isConnected(data.d_town))
      {
         if(item.cpi->getLastTown() == data.d_town)
         {
           if(item.cpi->getDistanceAlong() < distance)
           {
             distance = item.cpi->getDistanceAlong();
           }
         }
         else
         {
           ASSERT(item.cpi->getDestTown() == data.d_town);
           if(item.cpi->getRemainingDistance() < distance)
             distance = item.cpi->getRemainingDistance();
         }
      }

#ifdef DEBUG
      else if(item.cpi->atTown())
        ASSERT(item.cpi->getTown() != data.d_town);
#endif
   }

   if(distance != Distance_MAX)
   {
     data.d_enemyDistance = distance;
     return True;
   }
   else
     return False;
}

Distance CampaignRetreatUtil::getEnemyDistance(RetreatData& data)
{
   ASSERT(data.d_campData != 0);
   ASSERT(data.d_town != NoTown);
   ASSERT(data.d_closeUnits);

   Distance enemyDistance = Distance_MAX;

   static CampaignRoute route(data.d_campData);
   Distance routeDistance = 0;

   const CampaignPosition& townPos = data.d_town;

   data.d_closeUnits->rewind();
   while(data.d_closeUnits->seenOnly())
   {
      const CloseUnitItem& item = data.d_closeUnits->current();
      ASSERT(item.seen);

      CommandPosition* cpEnemy = data.d_campData->getCommand(item.cpi);

      if(route.getRouteDistance(&townPos, cpEnemy->location(), CampaignConst::enemyLoseDistance, routeDistance) )
      {
#ifdef DEBUG
         cuLog.printf("%s distance from %s = %ld (from unit=%ld)",
            (const char*) data.d_campData->getUnitName(item.cpi).toStr(),
            (const char*) data.d_campData->getTownName(data.d_town),
            (long) routeDistance,
            (long) item.distance);
#endif

         if(routeDistance < enemyDistance)
         {
            enemyDistance = routeDistance;
         }
      }
   }

   return enemyDistance;
}

Boolean CampaignRetreatUtil::shouldTryBreakOut(CampaignData* campData, ICommandPosition cpi, SPCount theirCount)
{
  SPCount ourValue = campData->getArmies().getUnitSPValue(cpi);

#ifdef DEBUG
  cuLog.printf("------------------- Our SPValue = %d, Their SPCount = %d",
      static_cast<int>(ourValue), static_cast<int>(theirCount));
#endif

  /*
   * Test to see if we break-out
   */

  ILeader leader = cpi->getLeader();
  ASSERT(leader != NoLeader);

  /*
   * The test is...
   * base chance = leaders aggression + charisma as a % of 512
   */

  int chance = MulDiv(leader->getAggression() + leader->getCharisma(), 100, 512);

#ifdef DEBUG
  cuLog.printf("Raw chance of breaking out = %d%%", chance);
#endif

  /*
   * Modifiers...
   */

  // if we outnumber them by 10 to 1 ( X .50) or..
  if(ourValue >= (theirCount * 10))
  {
    chance *= 1.5;
  }

  // if we outnumber them by 8 to 1 ( X .25)
  else if(ourValue >= (theirCount * 8))
  {
    chance *= 1.25;
  }

  // if it is raining ( X .10) or...
  if(campData->getWeather().getWeather() == CampaignWeather::LightRain)
  {
    chance *= 1.1;
  }

  // if it is heavy raining ( X.20)
  else if(campData->getWeather().getWeather() == CampaignWeather::HeavyRain)
  {
    chance *= 1.2;
  }

  // if it is very muddy (- X.20)
  else if(campData->getWeather().getGroundConditions() == CampaignWeather::VeryMuddy)
  {
    chance *= .8;
  }

  // -1% for each % below 50% of currentSupply
  int supplyPercent = MulDiv(cpi->getSupply(), 100, 256);
  int howManyBelow = clipValue(50 - supplyPercent, 0, 50);

  if(howManyBelow > 0)
  {
    chance -= (chance * (howManyBelow/100));
  }

  chance = clipValue(chance, 0, 100);

#ifdef DEBUG
  cuLog.printf("Modified chance of breaking out = %d%%", chance);
#endif

  return (CRandom::get(100) <= chance);
}

Boolean CampaignRetreatUtil::trappedUnit(CampaignData* campData, BestTown& bestTown, ICommandPosition cpi, BlockingUnits& blockingUnits, Distance retreatDistance)
{
  ASSERT(campData != 0);
  ASSERT(cpi != NoCommandPosition);

#ifdef DEBUG
  cuLog.printf("--------%s is trapped", (const char*)campData->getUnitName(cpi).toStr());
#endif

  enum TrappedUnitMode {
    BreakOut,
    HoldInPlace,
    Surrender,

    TrappedUnitMode_HowMany
  } tuMode;

  /*
   * See if we have an inconsequential unit
   * if enemy is inconsequential(or we think he is) then we try to break out
   *
   * Ratio is 6:1 Using our SPValue and their SPCount
   */

//  SPCount ourValue = campData->getArmies().getUnitSPValue(cpi);
//  SPCount theirCount = bestTown.getEnemySPCount();

#ifdef DEBUG
//  cuLog.printf("------------------- Our SPValue = %d, Their SPCount = %d",
//    static_cast<int>(ourValue), static_cast<int>(theirCount));
#endif

//  if(ourValue >= theirCount*6)
  if(shouldTryBreakOut(campData, cpi, bestTown.getEnemySPCount()))
    tuMode = BreakOut;
  else if(cpi->getMode() == CampaignMovement::CPM_StartingRetreat)
    tuMode = Surrender;
  else
    tuMode = HoldInPlace;


  switch(tuMode)
  {
    case BreakOut:
#ifdef DEBUG
      cuLog.printf("---------------- Trying to break out");
#endif

      ASSERT(blockingUnits.entries() > 0);

      if(cpi->isRouting() || cpi->isWithdrawing())
      {

        /*
         * If here we need to run a quick battle to determine if we
         * break out or not.
         */

        CampaignBattle battle(campData);

        // add our unit to the battle
        battle.addUnit(cpi, True);

        // add enemy units to battle
        SListIterR<BlockingUnitItem> iter(&blockingUnits);
        while(++iter)
        {
          battle.addUnit(iter.current()->d_cpi, True);
        }

        // run the battle
        if(!CampaignBattleProc::procBreakOutBattle(campData, &battle, cpi->getSide()))
        {
          if(cpi->getMode() == CampaignMovement::CPM_StartingRetreat)
          {
            // if here we did not break out and must surrender
#ifdef DEBUG
            cuLog.printf("Breakout Failed --- Surrendering");
#endif
            CP_ModeUtil::setMode(campData, cpi, CampaignMovement::CPM_Surrendering);
          }
          else
          {
            ASSERT(cpi->isWithdrawing() || cpi->getMode() == CampaignMovement::CPM_Retreating);
            // if here we did not break out and withdraw / retreat ends
            // and we will await our final glory
#ifdef DEBUG
            cuLog.printf("Breakout Failed --- Withdraw ends");
#endif
            CP_ModeUtil::setMode(campData, cpi, CampaignMovement::CPM_AcceptBattle);
            cpi->setAggressionValues(Orders::Aggression::AttackSmall, Orders::Posture::Defensive);
          }
        }

        // if we broke through
        else
        {
#ifdef DEBUG
          cuLog.printf("Breakout Succesful --- starting Retreat");
#endif
          SListIterR<BlockingUnitItem> iter(&blockingUnits);
          while(++iter)
          {
            ICommandPosition enemyCPI = iter.current()->d_cpi;

            /*
             * Enemy must retreat back 4
             */

            CloseUnits cl;

            CloseUnitData data;
            data.d_campData = campData;
            data.d_cpi = enemyCPI;
            data.d_from = enemyCPI->location();
            data.d_closeUnits = &cl;
            data.d_closeUnitSide = (enemyCPI->getSide() == 0) ? 1 : 0;
            data.d_flags |= CloseUnitData::AllAdjacent;
            data.d_findDistance = CampaignConst::enemyLoseDistance;
            CloseUnitUtil::findCloseUnits(data);
#ifdef DEBUG
            cl.currentUnit = enemyCPI;
#endif

//          enemyCPI->setAggressionValues(Orders::Aggression::DefendSmall, Orders::Posture::Defensive);
            CampaignRetreatUtil::retreatBack(campData, enemyCPI, cl, retreatDistance);

            /*
             * Enemy must be removed from any battle that he may be in.
             */

            CampaignBattleProc::removeFromBattle(campData, enemyCPI);
          }

          return True;
        }
      }
      else
      {
        /*
         * Otherwise, let the normal movement code take care of it
         */

        // Modified: SWG:22Jul99: setMode moved to after setTarget
        // because setMode actually does work plotting routes and things

        cpi->setTarget(blockingUnits.first()->d_cpi);
        cpi->setAggressionValues(Orders::Aggression::Attack, Orders::Posture::Offensive);
        CP_ModeUtil::setMode(campData, cpi, CampaignMovement::CPM_IntoBattle);
      }
      break;

    case HoldInPlace:
#ifdef DEBUG
      cuLog.printf("---------------- Gonna hold in place");
#endif

      CP_ModeUtil::setMode(campData, cpi, CampaignMovement::CPM_AcceptBattle);
      cpi->setAggressionValues(Orders::Aggression::AttackSmall, Orders::Posture::Defensive);
      break;

    case Surrender:
#ifdef DEBUG
      cuLog.printf("---------------- Surrendering");
#endif
      CP_ModeUtil::setMode(campData, cpi, CampaignMovement::CPM_Surrendering);
      break;

#ifdef DEBUG
    default:
      FORCEASSERT("Improper TrappedUnitMode");
#endif
  }

  return False;
}

void CampaignRetreatUtil::considerRetreatTown(CampaignData* campData, ICommandPosition cpi, IConnection iCon, ITown town, CloseUnits& closeUnits, BestTown& bestTown)
{
   ASSERT(iCon != NoConnection);

   // Ignore off-screen location

   const Town& rtown = campData->getTown(town);
   if(rtown.isOffScreen())
      return;

   RetreatData data;
   data.d_campData = campData;
   data.d_closeUnits = &closeUnits;
   data.d_blockingUnits = 0;
   data.d_town = town;
   data.d_con = iCon;
   data.d_cpi = cpi;

   RetreatPriority::Mode newMode = RetreatPriority::BT_Friendly;

// if(routeBlockedByChokePoint(data))
//   newMode = RetreatPriority::BT_Blocked;

   if(enemyBlockingPath(data))
     newMode = RetreatPriority::BT_ThroughEnemy;

   else if(!cpi->hasRetreatTown())
   {
      if(acrossBlownBridge(data))
         newMode = RetreatPriority::BT_BlownBridge;

      else if(unfriendlyChokePoint(data))
         newMode = RetreatPriority::BT_UnfriendlyChokePoint;

      else if(enemyNear(data))
         newMode = RetreatPriority::BT_NearEnemy;

      else if(retreatToSupplyLine(data))
         newMode = RetreatPriority::BT_AlongSupply;

      else if(retreatToOriginalRoute(data))
         newMode = RetreatPriority::BT_AlongOriginalRoute;

      else if(retreatToFriendlyUnits(data))
         newMode = RetreatPriority::BT_TowardsFriends;

      else if(retreatToUnfriendlyTown(data))
         newMode = RetreatPriority::BT_NotFriendly;
   }

   // add town
   SPCount ourValue = 0;

      // if this is through the enemy, get our
   if(newMode == RetreatPriority::BT_ThroughEnemy)
     ourValue = campData->getArmies().getUnitSPValue(cpi);

   bestTown.add(town, iCon, newMode, data.d_enemyDistance, data.d_enemySP, ourValue);
}


/*
 * Physically move unit back X miles from its previous opponent
 * Done immeadiatly after a losing battle
 */

void CampaignRetreatUtil::retreatBack(CampaignData* campData, ICommandPosition cpi, CloseUnits& closeUnits, Distance moveDistance)
{
  ASSERT(campData);
  ASSERT(cpi != NoCommandPosition);

  CommandPosition* cp = campData->getCommand(cpi);

  ASSERT(closeUnits.currentUnit == cpi);

#ifdef DEBUG
  retreatLog.printf("------%s is retreating back %ld miles",
    (const char*)campData->getUnitName(cpi).toStr(), DistanceToMile(moveDistance));

  String locName;
  campData->getCampaignPositionString(cp->location(), locName);

  retreatLog.printf("Location is %s", (const char*) locName.c_str());
#endif

  Distance distanceMoved = 0;

  BestTown bestTown;

  while( (distanceMoved < moveDistance) )
  {
    bestTown.reset();
    if(cp->atTown())
    {
      /*
       * if we have already moved and are at a town we need to recalculate
       * close units.
       */

      if(distanceMoved > 0)
      {
        CloseUnitData data;
        data.d_campData = campData;
        data.d_cpi = cpi;
        data.d_from = cpi->location();
        data.d_closeUnits = &closeUnits;
        data.d_closeUnitSide = (cpi->getSide() == 0) ? 1 : 0;
        data.d_findDistance = CampaignConst::enemyLoseDistance;
        data.d_flags |= CloseUnitData::AllAdjacent;
        CloseUnitUtil::findCloseUnits(data);
#ifdef DEBUG
        closeUnits.currentUnit = cpi;
#endif
      }

      ITown itown = cp->getTown();
      const Town& t = campData->getTown(itown);

      for(int i = 0; i < MaxConnections; i++)
      {
         IConnection ic = t.getConnection(i);
         if(ic == NoConnection)
            break;

         // if we've already moved some, we can't go back the way we came
         if(ic != cp->lastConnection() || distanceMoved == 0)
         {
           const Connection& con = campData->getConnection(ic);
           bool considerTown = True;

           if(campData->isChokePoint(itown) &&
              t.isGarrisoned() &&
              t.getSide() != cp->getSide() &&
              cp->lastConnection() != NoConnection)
           {
//             const Connection& lastCon = campData->getConnection(cp->lastConnection());
               considerTown = (campData->isSameSideOfChokePoint(ic, cp->lastConnection()));
           }

           if(considerTown)
           {
               ITown nextTown = con.getOtherTown(itown);
               CampaignRetreatUtil::considerRetreatTown(campData, cpi, ic, nextTown, closeUnits, bestTown);
           }
         }
      }

      // now test for randomize best town
#ifdef DEBUG
      bestTown.listChoices(campData);
#endif

      bestTown.randomize(campData, cpi);
    }
    else
    {
      CampaignRetreatUtil::considerRetreatTown(campData, cpi, cp->getConnection(), cp->getDestTown(), closeUnits, bestTown);
      CampaignRetreatUtil::considerRetreatTown(campData, cpi, cp->getConnection(), cp->getLastTown(), closeUnits, bestTown);
    }

    // if we're trapped
    if(bestTown.getMode() == RetreatPriority::BT_Blocked ||
       (bestTown.getMode() == RetreatPriority::BT_ThroughEnemy &&
        bestTown.getEnemyDistance() <= moveDistance + CampaignConst::enemyBattleDistance) )
    {
      BlockingUnits blockingUnits;

      // get list of units blocking our path
      RetreatData data;
      data.d_campData = campData;
      data.d_closeUnits = &closeUnits;
      data.d_blockingUnits = &blockingUnits;
      data.d_town = bestTown.getBestTown();
      data.d_con = bestTown.getBestConnection();
      data.d_cpi = cpi;

      Boolean result = enemyBlockingPath(data);
      ASSERT(result);

      cpi->trapped(True);

      if(!CampaignRetreatUtil::trappedUnit(campData, bestTown, cpi, blockingUnits, moveDistance))
        break;
    }

//  else
    {

      cpi->trapped(False);
      ITown iTown = bestTown.getBestTown();
      IConnection iCon = bestTown.getBestConnection();

      ASSERT(iTown != NoTown);
      ASSERT(iCon != NoConnection);

#ifdef DEBUG
      retreatLog.printf("Retreating towards %s down connection %d",
         campData->getTownName(iTown), static_cast<int>(iCon));
#endif

      /*
       * If at a town, put unit on its connection
       */

      Boolean stillAlive = True;

      if(cp->atTown())
      {
        cp->removePreviousRouteNode(cp->getTown());

        const Town& t = campData->getTown(cp->getTown());

//#if 0
        // if we're at a fortified friendly town, go into garrison
        if( (t.getSide() == cp->getSide()) &&
            (t.getFortifications() > 0) &&
            (dropOffGarrison(campData, cpi, cp->getTown())) )
        {
#ifdef DEBUG
//        retreatLog.printf("%s is retreating into Garrison", cpi->getName());
#endif
          break;
        }
        else
//#endif
        {
          // If we're retreating across a blown bridge, we lose some more stuff
          if(bestTown.getMode() == RetreatPriority::BT_BlownBridge)
          {
#ifdef DEBUG
            retreatLog.printf("%s is retreating across blown bridge", cpi->getNameNotNull());
#endif
            // first we lose all heavy equipment
            CampaignArmy_Util::loseEquipment(campData, cpi);

            SPCount count = campData->getArmies().getUnitSPCount(cpi, True);
            if(count > 0)
            {
              // for now, we'll lose a random % between 10% - 30% of our current SP count
              const int base = 100;
              const int lossPercent = CRandom::get(10, 30);

              int n = minimum(32767, count * lossPercent);
              SPLoss loss(0, static_cast<UWORD>(n), base);
              SPCount actualLoss = Losses::makeLosses(&campData->getArmies(), cpi, loss);
#ifdef DEBUG
              retreatLog.printf("%s is losing %d SP",
                 cpi->getNameNotNull(), static_cast<int>(actualLoss));
#endif
            }

            if(CampaignArmy_Util::clearDestroyedUnits(campData, cpi))
            {
#ifdef DEBUG
                retreatLog.printf(" %s has been completely wiped out", cpi->getName());
#endif
                stillAlive = False;
            }
          }

          if(stillAlive)
            cpi->startMoving(iCon, cpi->getTown());
        }
      }

      if(stillAlive)
      {
        /*
         * if our last town is our retreat town, reverse direction
         */

        if(cpi->getLastTown() == iTown)
          cpi->reverseDirection();

        /*
         * Now, move unit along its connection,
         */

        int distanceRemaining = moveDistance-distanceMoved;
        const int remainingDistanceBefore = cp->getRemainingDistance();

        const Distance tailLength = campData->getArmies().getColumnLength(cpi);
        cp->movePosition(distanceRemaining, tailLength);

        const int remainingDistanceAfter = cp->getRemainingDistance();
        ASSERT(remainingDistanceBefore >= remainingDistanceAfter);

        distanceMoved += (remainingDistanceBefore - remainingDistanceAfter);

        // set town as retreat town
        cp->retreatTown(iTown);

#ifdef DEBUG
        retreatLog.printf("distance moved = %ld(%d miles)", distanceMoved, DistanceToMile(distanceMoved));
#endif
      }
    }
  }
}

/*
 * Drop off Garrison if retreating / withdrawing
 * return True if whole unit goes into garrison,
 * otherwise return False (even if a sub-unit goes into garrison)
 */

Boolean CampaignRetreatUtil::dropOffGarrison(CampaignData* campData, const ICommandPosition& topCPI,
    ITown town)
{
  ASSERT(campData);
  ASSERT(topCPI != NoCommandPosition);
  ASSERT(town != NoTown);

  const Town& t = campData->getTown(town);
  ASSERT(t.getFortifications() > 0);

  // go through each unit in command and see if it should detach and go into garrison
  // We have to do it bottom up, so test XX's, then XXX's, etc.

  RankEnum rank = Rank_Division;
  Boolean droppedOff = False;

    /*
     * Go through each unit in command, from bottom up at
     */

  do
  {
    UnitIter uIter(&campData->getArmies(), topCPI);
    Boolean increment = True;

    while(uIter.next())
    {
      const ICommandPosition& cpi = uIter.current();

      if( (cpi->sameRank(rank)) &&
          (cpi->getCurrentOrder()->getDropOffGarrison())  )
      {

        /*
         * If entire unit will fit into the fort then entire unit
         * goes into the garrison, otherwise drop off no more than 1/3 of unit
         */

        // get number allowed in garrison
        SPCount allowedSP = static_cast<SPCount>(t.getStrength() * t.getFortifications());
        SPCount unitSP = campData->getArmies().getUnitSPCount(cpi, True);

        if(unitSP <= allowedSP)
        {
          if(cpi != topCPI)
            campData->getArmies().detachCommand(cpi);

          CP_ModeUtil::setMode(campData, cpi, CampaignMovement::CPM_Garrison);
          droppedOff = (cpi == topCPI);
        }

        // if XXX or higher, drop off 1/3 of our command
        else if(cpi->isHigher(Rank_Division))
        {
          /*
           * If here then we are dropping off no more than 1/3 of our infantry.
           */

          ICommandPosition dropOffCPI = NoCommandPosition;
          SPCount nTotalInfantry = campData->getArmies().getNType(cpi, BasicUnitType::Infantry, False);

          ICommandPosition iChild = cpi->getChild();

          // if we have children
          while(iChild != NoCommandPosition)
          {
            /*
             * To be eligable, a sub-unit must be no more than 1/3 of our infantry
             * the elibable force with lowest base-morale gets dropped-off
             *
             * If no sub-units are eligable the sub-unit with the lowest base-morale
             * may have one of its sub-units dropped-off (if any)
             */

            Attribute lowestMorale = Attribute_Range;
            SPCount bestNInfantry = 0;
            ICommandPosition bestCPI = NoCommandPosition;

            UnitIter iter(&campData->getArmies(), iChild);
            while(iter.sister())
            {
              const ICommandPosition& curCPI = iter.current();
              Attribute baseMorale = campData->getArmies().getBaseMorale(curCPI);

              const SPCount nInfantry = campData->getArmies().getNType(curCPI, BasicUnitType::Infantry, False);

              Boolean add = False;

              if(bestCPI == NoCommandPosition)
              {
                add = True;
              }
              else
              {
                if((nInfantry * 3) <= nTotalInfantry)
                {
                  add = ( (baseMorale < lowestMorale) || ((bestNInfantry * 3) > nTotalInfantry) );
                }
                else
                {
                  add = ( (baseMorale < lowestMorale) && ((bestNInfantry * 3) > nTotalInfantry) );
                }
              }

              if(add)
              {
                lowestMorale = baseMorale;
                bestCPI = curCPI;
                bestNInfantry = nInfantry;
              }
            }

            ASSERT(bestCPI != NoCommandPosition);
            if(bestCPI != NoCommandPosition)
            {
              // if best count is less than 1/3 dropp off this unit
              if((bestNInfantry * 3) <= nTotalInfantry)
              {
                iChild = NoCommandPosition;
                dropOffCPI = bestCPI;
              }
              else
                iChild = bestCPI->getChild();
            }
          }

          // if we have a drop-off unit
          if(dropOffCPI != NoCommandPosition)
          {
            campData->getArmies().detachCommand(dropOffCPI);
            CP_ModeUtil::setMode(campData, dropOffCPI, CampaignMovement::CPM_Garrison);
          }
          // otherwise do nothing, we can't drop off
        }
      }
    }

    // Note: to increment rank, we really need to decrement
    if(increment)
      DECREMENT(rank);

  } while (rank >= topCPI->getRankEnum());

  return droppedOff;
}

/*-----------------------------------------------------------------------
 * CampaignAvoid functions
 */

ITown CampaignAvoid::pickRetreatTown(CampaignData* campData, ICommandPosition cpi, CloseUnits& closeUnits)
{
   ITown destTown = NoTown;

   CommandPosition* cp = campData->getCommand(cpi);

   ASSERT(closeUnits.currentUnit == cpi);

#ifdef DEBUG
   cuLog.printf("%s is retreating", (const char*)campData->getUnitName(cpi).toStr());

   String locName;
   campData->getCampaignPositionString(cp->location(), locName);

   cuLog.printf("Location is %s", (const char*) locName.c_str());
#endif

   /*
    * Pick a town
    */

   static BestTown bestTown;

   bestTown.reset();


   /*
    */

   if(cp->atTown())
   {
      ITown itown = cp->getTown();
      const Town& t = campData->getTown(itown);

      for(int i = 0; i < MaxConnections; i++)
      {
         IConnection ic = t.getConnection(i);
         if(ic == NoConnection)
            break;

         // cannot go back the way we came if we're routing
         if(ic != cp->lastConnection() || !cp->isRouting())
         {
           const Connection& con = campData->getConnection(ic);
           ITown nextTown = con.getOtherTown(itown);
           CampaignRetreatUtil::considerRetreatTown(campData, cpi, ic, nextTown, closeUnits, bestTown);
         }
      }

      // now test to randomize best town
      bestTown.randomize(campData, cpi);
      destTown = bestTown.getBestTown();

#ifdef DEBUG
      cuLog.printf(cpi, "retreating from %s towards %s",
         campData->getTownName(itown),
         campData->getTownName(destTown));
#endif

   }
   else
   {
      /*
       * Consider town's starting with destination
       * Reason is that if town's both get equal priority
       * such as they are both friendly, away from enemy,
       * then unit will continue going where it was instead
       * repeatedly changing direction.
       */

      // if we already have a retreat town, that is the only way we can go
      if(cp->hasRetreatTown())
      {
        CampaignRetreatUtil::considerRetreatTown(campData, cpi, cp->getConnection(), cp->retreatTown(), closeUnits, bestTown);
      }
      else
      {
        CampaignRetreatUtil::considerRetreatTown(campData, cpi, cp->getConnection(), cp->getDestTown(), closeUnits, bestTown);
        CampaignRetreatUtil::considerRetreatTown(campData, cpi, cp->getConnection(), cp->getLastTown(), closeUnits, bestTown);
      }

      destTown = bestTown.getBestTown();

#ifdef DEBUG
      cuLog.printf(cpi, "retreating towards %s",
            campData->getTownName(destTown));
#endif
   }

   ASSERT(bestTown.getMode() != RetreatPriority::BT_Blocked);

   if( (bestTown.getMode() == RetreatPriority::BT_ThroughEnemy) &&
       (bestTown.getEnemyDistance() < CampaignConst::enemyBattleDistance) )
   {

     /*
      * If here then unit is trapped
      *
      * Unit should either:
      * 1. Halt and prepare for a last ditch stand or
      * 2. Try and break out (if against a supposedly inconsequential force)
      * 3. Surrender         (if we're routed and not trying to break-out)
      *
      */

     BlockingUnits blockingUnits;

     // get list of units blocking our path
     RetreatData data;
     data.d_campData = campData;
     data.d_closeUnits = &closeUnits;
     data.d_blockingUnits = &blockingUnits;
     data.d_town = bestTown.getBestTown();
     data.d_con = bestTown.getBestConnection();
     data.d_cpi = cpi;

     Boolean result = CampaignRetreatUtil::enemyBlockingPath(data);
     ASSERT(result);

     cpi->trapped(True);

     CampaignRetreatUtil::trappedUnit(campData, bestTown, cpi, blockingUnits, CampaignConst::enemyBattleDistance);
     destTown = NoTown;

   }
   else
   {
     cpi->trapped(False);
     cpi->retreatTown(destTown);
   }

   return destTown;
}

/*
 * Try to keep away from enemy
 * Return True if no longer avoiding
 */

Boolean CampaignAvoid::avoid(CampaignData* campData, ICommandPosition cpi, CloseUnits& closeUnits)
{
   ASSERT(campData != 0);
   Armies* armies = &campData->getArmies();

   Boolean result = False;

   CommandPosition* cp = armies->getCommand(cpi);

   /*
    * Work out a distance to remain
    */

   Distance closeDist = closeUnits.getClosestSeenDist();

   Distance nearDist;
   Distance farDist;
   Boolean fastMode;

   /*
    * Special case for after battle
    * If we just lost a battle, retreat back 4 miles
    */

   if(cp->getMode() == CampaignMovement::CPM_StartingRetreat)
   {
     const Distance tailLength = campData->getArmies().getColumnLength(cpi);
     const Distance moveDistance = tailLength+MilesToDistance(4);

     CampaignRetreatUtil::retreatBack(campData, cpi, closeUnits, moveDistance);

     // if we didn't surrender,  or go into garrison, set to retreating
     if(!cpi->isSurrendering() && !cpi->isGarrison())
       CP_ModeUtil::setMode(campData, cpi, CampaignMovement::CPM_Retreating);

     return False;
   }

   /*
    * If we are retreating (that is morale = 0)
    */

   else if(cp->getMode() == CampaignMovement::CPM_Retreating)
   {
     /*
      * if we have regained morale, let orderUpdated take over
      */

     if(cp->getMorale() > 0)
     {
       return True;
     }

     /*
      * else,
      * If enemy is no longer within 24 miles and we are
      * at a town then the retreat is over, otherwise it continues
      */

     else if(cp->atTown())
     {
       const Town& t = campData->getTown(cp->getTown());

       // if we're at a fortified friendly town, go into garrison
       // regardless of distance
       if(t.getSide() == cp->getSide() && t.getFortifications() > 0)
       {
//       CP_ModeUtil::setMode(campData, cpi, CampaignMovement::CPM_Garrison);
         CampaignRetreatUtil::dropOffGarrison(campData, cpi, cp->getTown());
         return False;
       }
       else
         return (closeDist > MilesToDistance(24));
     }

     return False;
   }

   else if(cp->getMode() == CampaignMovement::CPM_AfterWinningBattle)
   {
     // if pursuing, set to pursuit
     if(cp->hasPursuitTarget())
     {
        if(!cp->isForcedHold(campData->getTick()))
        {
          ASSERT(cp->getCurrentOrder()->getTarget() != NoCommandPosition);
          CP_ModeUtil::setMode(campData, cpi, CampaignMovement::CPM_Pursuing);
        }

        return False;
     }

     return True;
   }

   else if(cp->getMode() == CampaignMovement::CPM_StartingWithdraw)
   {
     // if withdrawing we immediatly jump back 8 miles
     const Distance tailLength = campData->getArmies().getColumnLength(cpi);
     const Distance moveDistance = tailLength+MilesToDistance(4);
     CampaignRetreatUtil::retreatBack(campData, cpi, closeUnits, moveDistance);

     // if we didn't go into garrison, or holding for last ditch battle, set to withdrawing
     if(!cpi->isGarrison() && !cpi->willDoBattle())
       CP_ModeUtil::setMode(campData, cpi, CampaignMovement::CPM_Withdrawing);

     return False;

   }

   else if(cp->getMode() == CampaignMovement::CPM_Withdrawing)
   {
     /*
      * Withdraw continues until we arrive at a town that is at least 12 miles from enemy
      *
      * if at a friendly garrison town then the withdraw is over and unit goes into
      * garrison
      */

     if(cp->atTown())
     {
       const Town& t = campData->getTown(cp->getTown());

       // if we're at a fortified friendly town, go into garrison
       // regardless of distance
       if(t.getSide() == cp->getSide() && t.getFortifications() > 0)
       {
//       CP_ModeUtil::setMode(campData, cpi, CampaignMovement::CPM_Garrison);
         CampaignRetreatUtil::dropOffGarrison(campData, cpi, cp->getTown());
         return False;
       }
     }

     return (closeDist >= MilesToDistance(12) && cp->atTown());
   }

   /*
    * Other cases
    */

   if( (cp->getMode() == CampaignMovement::CPM_AvoidContact) ||
       (cp->getMode() == CampaignMovement::CPM_AvoidContactHold) ||
       (cp->getMode() == CampaignMovement::CPM_AvoidContactRetreat) )
   {
      nearDist = CampaignConst::enemyBattleDistance;  // 8 Miles
      farDist = CampaignConst::enemyLoseBattleDistance;  // 12 Miles
      fastMode = False;
   }
   else
   if( (cp->getMode() == CampaignMovement::CPM_AvoidContactFast) ||
       (cp->getMode() == CampaignMovement::CPM_AvoidContactFastHold) ||
       (cp->getMode() == CampaignMovement::CPM_AvoidContactFastRetreat) )
   {
      nearDist = CampaignConst::enemyNearDistance;    // 24 Miles
      farDist = CampaignConst::enemyLoseDistance;     // 28 Miles
      fastMode = True;
   }
#ifdef DEBUG
   else
      FORCEASSERT("doCampaignAvoid was called without being in CPM_AVOID* mode");
#endif


   enum DistanceMode {
      DM_Near,          // closer than near distance
      DM_Middle,        // In between
      DM_Far            // further than far distance
   } distanceMode;

   /*
    * Table of mode changes:
    *
    *  [mode][distance mode][move order]
    *
    * mode= 0..5 (AvoidContact --> AvoidContactRetreatFast)
    * distance = Near/Middle/Far
    * mode = Move / Hold
    */

   static CampaignMovement::CP_Mode stateTable[3][2] = {
         { CampaignMovement::CPM_AvoidContactRetreat, CampaignMovement::CPM_AvoidContactRetreat },
         { CampaignMovement::CPM_AvoidContactHold,    CampaignMovement::CPM_AvoidContactHold    },
         { CampaignMovement::CPM_AvoidContact,        CampaignMovement::CPM_AvoidContactHold    }
   };

   static CampaignMovement::CP_Mode stateTableFast[3][2] = {
         { CampaignMovement::CPM_AvoidContactFastRetreat,   CampaignMovement::CPM_AvoidContactFastRetreat },
         { CampaignMovement::CPM_AvoidContactFastHold,      CampaignMovement::CPM_AvoidContactFastHold    },
         { CampaignMovement::CPM_AvoidContactFast,          CampaignMovement::CPM_AvoidContactFastHold    }
   };

   if(closeDist <= nearDist)
      distanceMode = DM_Near;
   else if(closeDist < farDist)
      distanceMode = DM_Middle;
   else
      distanceMode = DM_Far;

   enum { MOVE, HOLD } moveMode = cp->getCurrentOrder()->isMoveOrder() ? MOVE : HOLD;

   CampaignMovement::CP_Mode newMode;

   if(fastMode)
      newMode = stateTableFast[distanceMode][moveMode];
   else
      newMode = stateTable[distanceMode][moveMode];

   if(newMode != cp->getMode())
//    cp->setMode(newMode);
      CP_ModeUtil::setMode(campData, cpi, newMode);

   return result;
}


/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.1  2001/07/02 15:23:16  greenius
 * Battle, Campaign, Combined game and Campaign Editor now compile and run... mostly
 *
 * Revision 1.2  2001/06/18 08:25:44  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
