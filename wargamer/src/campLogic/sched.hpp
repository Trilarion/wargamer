/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef SCHED_H
#define SCHED_H

#ifndef __cplusplus
#error sched.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1995, Steven Morle-Green, All Rights Reserved
 *----------------------------------------------------------------------
 *
 *	Scheduler
 *
 * Makes sure routines are called at regular game time intervals
 *
 * Note that TimeTick has a 1 hour resolution
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/06/13 08:52:37  greenius
 * Initial Import
 *
 * Revision 1.1  1996/06/22 19:04:48  sgreen
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */


#include "clogic_dll.h"
#include "measure.hpp"
#include "sllist.hpp"

class FileReader;
class FileWriter;

class GameSchedule : public SLink {
	TimeTick nextProcess;		// Time to next process this function
	TimeTick interval;

   static UWORD s_fileVersion;

public:
	CLOGIC_DLL GameSchedule(TimeTick _interval);
	virtual ~GameSchedule() { }

	virtual void process(TimeTick interval) = 0;

	CLOGIC_DLL Boolean run(TimeTick now);

	/*
	 * File Interface
	 */

	CLOGIC_DLL bool readData(FileReader& f);
	CLOGIC_DLL bool writeData(FileWriter& f) const;
};

class GameScheduleList : public SList<GameSchedule> {
public:
	CLOGIC_DLL ~GameScheduleList();
	CLOGIC_DLL void add(GameSchedule* proc);
	CLOGIC_DLL void run(TimeTick now);

	/*
	 * File Interface
	 */

	CLOGIC_DLL Boolean read(FileReader& f);
	CLOGIC_DLL Boolean write(FileWriter& f) const;
};


#endif /* SCHED_H */

