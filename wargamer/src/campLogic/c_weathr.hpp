/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef C_WEATHR_HPP
#define C_WEATHR_HPP

class CampaignLogicOwner;

class CampaignWeatherUtil {
	public:
	  static void doTodaysWeather(CampaignLogicOwner* campGame);
};


#endif
