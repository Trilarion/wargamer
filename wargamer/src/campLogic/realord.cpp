/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 * Real Campaign Orders Implementation
 *
 *----------------------------------------------------------------------
 */


#include "stdinc.hpp"
#include "realord.hpp"
//#include "campord.hpp"
#include "campdint.hpp"
#include "campctrl.hpp"
#include "simpstr.hpp"
#include "town.hpp"
#include "compos.hpp"
#include "armies.hpp"
#include "cbattle.hpp"
#include "cbatproc.hpp"
#include "cu_mode.hpp"
#include "campmsg.hpp"
#include "resstr.hpp"
#include "res_str.h"
#include "armyutil.hpp"
#include "c_supply.hpp"
#include "c_obutil.hpp"
#include "wg_rand.hpp"
#include "townprop.hpp"
#include "terrain.hpp"
#include "campside.hpp"
#include "scenario.hpp"
//#include "route.hpp"
#include "siege.hpp"

namespace CampaignOrderUtil
{

enum {
  None,
  Via1,
  Via2,
  To,
  MoveTo,
  And,
  Hold,
  RestRally,
  AttachTo,
  AttachSP,
  DetachLeader,
  Garrison,
  InstallSupply,
  UpgradeFort,

  StringID_HowMany
};

// string id's for order types
static const int s_stringIDs[StringID_HowMany] = {
  IDS_None,
  IDS_MISC_ORDERS_VIA1,
  IDS_MISC_ORDERS_VIA2,
  IDS_MISC_ORDERS_TO,
  IDS_MISC_ORDERS_MOVETO,
  IDS_MISC_ORDERS_AND,
  IDS_ORDER_HOLD,
  IDS_ORDER_RESTRALLY,
  IDS_MISC_ORDERS_ATTACHTO,
  IDS_MISC_ORDERS_ATTACHSP,
  IDS_MISC_ORDERS_DETACHLEADER,
  IDS_ORDER_GARRISON,
  IDS_MISC_ORDERS_INSTALLSUPPLY,
  IDS_MISC_ORDERS_UPGRADEFORT
};

static ResourceStrings s_strings(s_stringIDs, StringID_HowMany);
static const char newLine[] = "\r\n";
static const char noDescription[] = "";

inline const char* getText(int id)
{
  ASSERT(id < StringID_HowMany);
  return s_strings.get(id);
}

SimpleString getAdvancedOrderText(const CampaignOrder* order)
{
  SimpleString text;

  Boolean hasAdvanced = False;

  /*
   * If pursue option is set
   */

  if(order->getPursue())
  {
    text = Orders::AdvancedOrders::pursueText();
    hasAdvanced = True;
  }

  if(order->getSoundGuns())
  {
    if(hasAdvanced)
    {
      text += newLine;
    }

    text += Orders::AdvancedOrders::soundGunsText();

    hasAdvanced = True;
  }

  if(order->getSiegeActive())
  {
    if(hasAdvanced)
    {
      text += newLine;
    }
    text += Orders::AdvancedOrders::siegeActiveText();
    hasAdvanced = True;
  }

  if(!hasAdvanced)
    text = getText(None);

  ASSERT(text.toStr());

  return text;
}

SimpleString getDestinationText(const CampaignData* campData, const CampaignOrder* order)
{
  SimpleString text;

  if(order->isMoveOrder())
  {
    if(order->getDestTown() != NoTown)
    {
      const Town& t = campData->getTown(order->getDestTown());
      text = t.getNameNotNull();
      text += newLine;
    }
    else if(order->getTarget() != NoCommandPosition)
    {
      ConstICommandPosition cpi = order->getTarget();
      text = cpi->getNameNotNull();
      text += newLine;
    }
    else
         text += "?";


    for(int i = 0; i < Orders::Vias::MaxVia; i++)
    {
      if(i < order->getNVias())
      {
        const Town& t = campData->getTown(order->getVia(i));
        text += getText(Via1);
        text += t.getNameNotNull();
        text += newLine;
      }
    }

  }
  else
    text = "";

  return text;
}

SimpleString getOrderText(const CampaignData* campData, const CampaignOrder* order, OD_TYPE how)
{
  return getDescription(campData, order, how);
}


/*
 * Order Description code
 */

class OrderImpBase
{
   public:
      virtual ~OrderImpBase() { }

      virtual SimpleString getDescription(const CampaignData* campData, const CampaignOrder* order, OD_TYPE how) const = 0;
#if !defined(EDITOR)
      virtual bool beginOrder(CampaignLogicOwner* campGame, const CampaignOrder* order, ParamCP cpi) const = 0;
#endif
      virtual Orders::Type::Value type() const = 0;
};

class OrderImp_MoveTo : public OrderImpBase {
   public:
      SimpleString getDescription(const CampaignData* campData, const CampaignOrder* order, OD_TYPE how) const;
#if !defined(EDITOR)
      bool beginOrder(CampaignLogicOwner* campGame, const CampaignOrder* order, ParamCP cpi) const;
#endif
      Orders::Type::Value type() const { return Orders::Type::MoveTo; }
};

class OrderImp_Hold : public OrderImpBase {
   public:
      SimpleString getDescription(const CampaignData* campData, const CampaignOrder* order, OD_TYPE how) const;
#if !defined(EDITOR)
      bool beginOrder(CampaignLogicOwner* campGame, const CampaignOrder* order, ParamCP cpi) const;
#endif
      Orders::Type::Value type() const { return Orders::Type::Hold; }
};

class OrderImp_RestRally : public OrderImpBase {
   public:
      SimpleString getDescription(const CampaignData* campData, const CampaignOrder* order, OD_TYPE how) const;
#if !defined(EDITOR)
      bool beginOrder(CampaignLogicOwner* campGame, const CampaignOrder* order, ParamCP cpi) const;
#endif
      Orders::Type::Value type() const { return Orders::Type::RestRally; }
};

class OrderImp_Attach : public OrderImpBase {
   public:
      SimpleString getDescription(const CampaignData* campData, const CampaignOrder* order, OD_TYPE how) const;
#if !defined(EDITOR)
      bool beginOrder(CampaignLogicOwner* campGame, const CampaignOrder* order, ParamCP cpi) const;
#endif
      Orders::Type::Value type() const { return Orders::Type::Attach; }
};

class OrderImp_AttachSP : public OrderImpBase {
   public:
      SimpleString getDescription(const CampaignData* campData, const CampaignOrder* order, OD_TYPE how) const;
#if !defined(EDITOR)
      bool beginOrder(CampaignLogicOwner* campGame, const CampaignOrder* order, ParamCP cpi) const;
#endif
      Orders::Type::Value type() const { return Orders::Type::AttachSP; }
};

class OrderImp_Leader : public OrderImpBase {
   public:
      SimpleString getDescription(const CampaignData* campData, const CampaignOrder* order, OD_TYPE how) const;
#if !defined(EDITOR)
      bool beginOrder(CampaignLogicOwner* campGame, const CampaignOrder* order, ParamCP cpi) const;
#endif
      Orders::Type::Value type() const { return Orders::Type::Leader; }
};

class OrderImp_Garrison : public OrderImpBase{
   public:
      SimpleString getDescription(const CampaignData* campData, const CampaignOrder* order, OD_TYPE how) const;
#if !defined(EDITOR)
      bool beginOrder(CampaignLogicOwner* campGame, const CampaignOrder* order, ParamCP cpi) const;
#endif
      Orders::Type::Value type() const { return Orders::Type::Garrison; }
};

class OrderImp_InstallSupply : public OrderImpBase {
   public:
      SimpleString getDescription(const CampaignData* campData, const CampaignOrder* order, OD_TYPE how) const;
#if !defined(EDITOR)
      bool beginOrder(CampaignLogicOwner* campGame, const CampaignOrder* order, ParamCP cpi) const;
#endif
      Orders::Type::Value type() const { return Orders::Type::InstallSupply; }
};

class OrderImp_UpgradeFort : public OrderImpBase {
   public:
      SimpleString getDescription(const CampaignData* campData, const CampaignOrder* order, OD_TYPE how) const;
#if !defined(EDITOR)
      bool beginOrder(CampaignLogicOwner* campGame, const CampaignOrder* order, ParamCP cpi) const;
#endif
      Orders::Type::Value type() const { return Orders::Type::UpgradeFort; }
};

class OrderImp_BlowBridge : public OrderImpBase {
   public:
      SimpleString getDescription(const CampaignData* campData, const CampaignOrder* order, OD_TYPE how) const;
#if !defined(EDITOR)
      bool beginOrder(CampaignLogicOwner* campGame, const CampaignOrder* order, ParamCP cpi) const;
#endif
      Orders::Type::Value type() const { return Orders::Type::BlowBridge; }
};

class OrderImp_RepairBridge : public OrderImpBase {
   public:
      SimpleString getDescription(const CampaignData* campData, const CampaignOrder* order, OD_TYPE how) const;
#if !defined(EDITOR)
      bool beginOrder(CampaignLogicOwner* campGame, const CampaignOrder* order, ParamCP cpi) const;
#endif
      Orders::Type::Value type() const { return Orders::Type::RepairBridge; }
};

class OrderImp_RemoveDepot : public OrderImpBase {
   public:
      SimpleString getDescription(const CampaignData* campData, const CampaignOrder* order, OD_TYPE how) const;
#if !defined(EDITOR)
      bool beginOrder(CampaignLogicOwner* campGame, const CampaignOrder* order, ParamCP cpi) const;
#endif
      Orders::Type::Value type() const { return Orders::Type::RemoveDepot; }
};

/*
 * Description Functions
 */

SimpleString OrderImp_MoveTo::getDescription(const CampaignData* campData, const CampaignOrder* order, OD_TYPE how) const
{
   SimpleString s;

   ConstICommandPosition destUnit = order->getTarget();
   ITown destTown = order->getDestTown();

   s = Orders::AdvancedOrders::getMoveHowName(order->getMoveHow());
   s += getText(To);

   if(how == OD_LONG)
   {
     s += getDestinationText(campData, order);
     // Order on Arrival text
     s += Orders::AdvancedOrders::getOrderOnArrivalName(order->getOrderOnArrival());
   }
   else
   {
     if(destUnit != NoCommandPosition)
       s += campData->getUnitName(destUnit);
     else if(destTown != NoTown)
       s += campData->getTownName(destTown);
   }

   return s;
}

SimpleString OrderImp_Hold::getDescription(const CampaignData* campData, const CampaignOrder* order, OD_TYPE how) const
{
   return getText(Hold);
}

SimpleString OrderImp_RestRally::getDescription(const CampaignData* campData, const CampaignOrder* order, OD_TYPE how) const
{
   return getText(RestRally);
}

SimpleString OrderImp_Attach::getDescription(const CampaignData* campData, const CampaignOrder* order, OD_TYPE how) const
{
   SimpleString s;

   s = getText(AttachTo);

   if(order->getTarget() != NoCommandPosition)
     s += campData->getUnitName(order->getTarget());

   return s;
}

SimpleString OrderImp_AttachSP::getDescription(const CampaignData* campData, const CampaignOrder* order, OD_TYPE how) const
{
   return getText(AttachSP);
}

SimpleString OrderImp_Leader::getDescription(const CampaignData* campData, const CampaignOrder* order, OD_TYPE how) const
{
   return getText(DetachLeader);
}

SimpleString OrderImp_Garrison::getDescription(const CampaignData* campData, const CampaignOrder* order, OD_TYPE how) const
{
   return getText(Garrison);
}

SimpleString OrderImp_InstallSupply::getDescription(const CampaignData* campData, const CampaignOrder* order, OD_TYPE how) const
{
   return getText(InstallSupply);
}

SimpleString OrderImp_UpgradeFort::getDescription(const CampaignData* campData, const CampaignOrder* order, OD_TYPE how) const
{
   return getText(UpgradeFort);
}

SimpleString OrderImp_BlowBridge::getDescription(const CampaignData* campData, const CampaignOrder* order, OD_TYPE how) const
{
   // TODO: get from string resource
   return InGameText::get(IDS_BlowingBridge); // getText(InstallSupply);
}

SimpleString OrderImp_RepairBridge::getDescription(const CampaignData* campData, const CampaignOrder* order, OD_TYPE how) const
{
   return InGameText::get(IDS_RepairingBridge); //getText(UpgradeFort);
}

SimpleString OrderImp_RemoveDepot::getDescription(const CampaignData* campData, const CampaignOrder* order, OD_TYPE how) const
{
   return InGameText::get(IDS_RemovingDepot); //getText(UpgradeFort);
}

#if !defined(EDITOR)

/*
 * BeginOrder functions
 */

bool OrderImp_MoveTo::beginOrder(CampaignLogicOwner* campGame, const CampaignOrder* order, ParamCP cpi) const
{
   // make sure we have a target town or unit
   if(order->hasDestTown() || order->hasDestUnit())
   {
     // set mode
     CP_ModeUtil::setMode(campGame->campaignData(), cpi, CampaignMovement::CPM_Moving);
//   CampaignRouteUtil::plotUnitRoute(campGame->campaignData(), cpi);
     return True;
   }
   else
     return False;
}

bool OrderImp_Hold::beginOrder(CampaignLogicOwner* campGame, const CampaignOrder* order, ParamCP cpi) const
{
   CampaignData* campData = campGame->campaignData();
   CP_ModeUtil::setMode(campData, cpi, CampaignMovement::CPM_Holding);

    if(cpi->atTown())
    {
        // if at an enemy town, start raid or siege
        Siege::arrivedAtTown(campGame, cpi->getTown(), cpi);
    }

   return True;
}

bool OrderImp_RestRally::beginOrder(CampaignLogicOwner* campGame, const CampaignOrder* order, ParamCP cpi) const
{
   CampaignData* campData = campGame->campaignData();
   CP_ModeUtil::setMode(campData, cpi, CampaignMovement::CPM_Rallying);
   return True;
}

bool OrderImp_Attach::beginOrder(CampaignLogicOwner* campGame, const CampaignOrder* order, ParamCP cpi) const
{
   CampaignData* campData = campGame->campaignData();

//   ASSERT(order->getTarget() != NoCommandPosition);
   ICommandPosition targetCPI = constCPtoCP(order->getTarget());

   if(targetCPI == NoCommandPosition)
      return false;

   if(cpi->isDead() || targetCPI->isDead())
      return false;

   // if not an independent leader, attach unit
   if(cpi->isRealUnit())
   {
     // don't attach if 2 armies
     if( (cpi->getRank().getRankEnum() == Rank_ArmyGroup)  &&
        (targetCPI->getRank().getRankEnum() == Rank_ArmyGroup) )
       ; // do nothing
     else
     {

       CampaignMessageInfo msgInfo(cpi->getSide(), CampaignMessages::MetAtAndAwait);
       msgInfo.cp(cpi);
       msgInfo.where(cpi->getCloseTown());
       msgInfo.cpTarget(targetCPI);
       campGame->sendPlayerMessage(msgInfo);
       // CampaignArmy_Util::attachUnits(campData, order->getTarget(), cpi);
       CampaignArmy_Util::attachUnits(campData, targetCPI, cpi);
     }
   }
   else
   {
     CampaignMessageInfo msgInfo(cpi->getSide(), CampaignMessages::TakenCommand);
     msgInfo.cp(cpi);
     msgInfo.cpTarget(targetCPI);
     msgInfo.leader(cpi->getLeader());
     campGame->sendPlayerMessage(msgInfo);

     CampaignArmy_Util::transferLeader(campData, cpi->getLeader(), targetCPI, True);
   }

   return False;
}

bool OrderImp_AttachSP::beginOrder(CampaignLogicOwner* campGame, const CampaignOrder* order, ParamCP cpi) const
{
   ASSERT(order->getTarget() != NoCommandPosition);
   ASSERT(order->getTargetSP() != NoStrengthPoint);

   CampaignData* campData = campGame->campaignData();


   campData->getArmies().transferStrengthPoint(constCPtoCP(order->getTarget()), cpi, order->getTargetSP());
   return False;     // Make caller call finishOrder
}

bool OrderImp_Leader::beginOrder(CampaignLogicOwner* campGame, const CampaignOrder* order, ParamCP cpi) const
{
   CampaignData* campData = campGame->campaignData();

   /*
    * Make leader independent
    */

   CampaignArmy_Util::transferLeader(campData, cpi->getLeader(), NoCommandPosition, True);
   return False;     // Make caller call finishOrder
}

bool OrderImp_Garrison::beginOrder(CampaignLogicOwner* campGame, const CampaignOrder* order, ParamCP cpi) const
{
   CampaignData* campData = campGame->campaignData();
   if(cpi->atTown())
   {
     Town& t = campData->getTown(cpi->getTown());
     if(t.getSide() == cpi->getSide())
         CP_ModeUtil::setMode(campData, cpi, CampaignMovement::CPM_Garrison);

     return True;
   }
   else
     return False;
}

bool OrderImp_InstallSupply::beginOrder(CampaignLogicOwner* campGame, const CampaignOrder* order, ParamCP cpi) const
{
   Boolean carryOut = False;
   CampaignData* campData = campGame->campaignData();
   Town& town = campData->getTown(cpi->getCloseTown());

   if(cpi->atTown())
   {
     OrderBattle* ob = campData->getArmies().ob();
     ASSERT(ob);

#if 0
     const Table1D<UBYTE>& table = scenario->getSideDepotLimitTable();
     const int nDepots = campData->getSideData().getSideDepotsBuilt(cpi->getSide());
     const int nDepotsUC = campData->getSideData().getSideDepotsUnderConstruction(cpi->getSide());
#endif

     Province& p = campData->getProvince(town.getProvince());
     Nationality n = cpi->getNation(); //p.getNationality();

     const int nDepotsAllowed = campData->getNations().getNDepotsAllowed(n);
     const int nDepotsBuilt = campData->getNations().getNDepotsBuilt(n);
     const int nDepotsUC = campData->getNations().getNDepotsUC(n);

     if( (!town.getIsDepot()) &&
         (!town.getIsSupplySource()) &&
         (town.getSide() == cpi->getSide()) &&
         (nDepotsBuilt + nDepotsUC < nDepotsAllowed) )
     {
       // time taken to build a depot is 256 / Supply source replenishment value
       // with a min of 3 days and a max of 10
       // only a unit with SP can begin a depot
       SPCount spCount = campData->getArmies().getUnitSPCount(cpi, True);
       if(spCount > 0)
       {
         // first find supply source
         TimeTick tillWhen = 0;

         const int maxDays = 10;
         const int minDays = 3;

         int canSupply = CampaignSupply::findSupplyLine(campData, cpi, spCount);
         if(canSupply > 0)
         {
           ASSERT(cpi->supplyRouteList().entries() > 0);
           ASSERT(cpi->supplyRouteList().getLast()->d_town != NoTown);
           const Town& supplySource = campData->getTown(cpi->supplyRouteList().getLast()->d_town);

           tillWhen = (supplySource.getSupplyRate() > 0) ?
             DaysToTicks(256 / supplySource.getSupplyRate()) : DaysToTicks(maxDays);
         }

         // if we dont have a supply line time taken is 256 / forageBase
         else
         {
           tillWhen = (town.getForageBase() > 0) ?
             DaysToTicks(256 / town.getForageBase()) : DaysToTicks(maxDays);
         }

         // clip it
         tillWhen = campData->getTick() + clipValue(tillWhen, DaysToTicks(minDays), DaysToTicks(maxDays));

//         Town_COrder* to = new Town_COrder(CampaignOBUtil::cpiToCPIndex(cpi, *ob), Town_COrder::BuildDepot, tillWhen);
//         ASSERT(to);

         cpi->addTownOrder(cpi->getCloseTown(), Town_COrder::BuildDepot, tillWhen);
//         town.addOrder(to);
         town.setisBuildingDepot(True);

         // unchecked ammended by Paul - 12/9/99
         town.setSupplyInstaller(cpi);
         // end unchecked ammended by Paul - 12/9/99


         //campData->getSideData().incSideDepotsUnderConstruction(cpi->getSide());
         campData->getNations().incNDepotsUC(n);
         carryOut = True;
       }
     }
   }

   if(!carryOut)
   {
     // tell player we couldn';t carry out order
     CampaignMessageInfo msgInfo(cpi->getSide(), CampaignMessages::NotInstallSupply);
     msgInfo.cp(cpi);
     msgInfo.where(cpi->getCloseTown());
     campGame->sendPlayerMessage(msgInfo);
   }

   return False;
}

bool OrderImp_RemoveDepot::beginOrder(CampaignLogicOwner* campGame, const CampaignOrder* order, ParamCP cpi) const
{
   Boolean carryOut = False;
   CampaignData* campData = campGame->campaignData();
   Town& town = campData->getTown(cpi->getCloseTown());

   if(cpi->atTown())
   {
     OrderBattle* ob = campData->getArmies().ob();
     ASSERT(ob);

     Province& p = campData->getProvince(town.getProvince());
     Nationality n = cpi->getNation(); //p.getNationality();

     const int nDepotsBuilt = campData->getNations().getNDepotsBuilt(n);

     if( (town.getIsDepot()) &&
         (town.getSide() == cpi->getSide()) &&
         (nDepotsBuilt > 0) )
     {
       // time taken to remove a depot is 256 / supply-source repo rate or forage value
       // with a min of 3 days and a max of 10
       // only a unit with SP can begin a depot
       SPCount spCount = campData->getArmies().getUnitSPCount(cpi, True);
       if(spCount > 0)
       {
         // first find supply source
         TimeTick tillWhen = 0;

         const int maxDays = 10;
         const int minDays = 3;

         int canSupply = CampaignSupply::findSupplyLine(campData, cpi, spCount);
         if(canSupply > 0)
         {
           ASSERT(cpi->supplyRouteList().entries() > 0);
           ASSERT(cpi->supplyRouteList().getLast()->d_town != NoTown);
           const Town& supplySource = campData->getTown(cpi->supplyRouteList().getLast()->d_town);

           tillWhen = (supplySource.getSupplyRate() > 0) ?
             DaysToTicks(256 / supplySource.getSupplyRate()) : DaysToTicks(maxDays);
         }

         // if we dont have a supply line time taken is 256 / forageBase
         else
         {
           tillWhen = (town.getForageBase() > 0) ?
             DaysToTicks(256 / town.getForageBase()) : DaysToTicks(maxDays);
         }

         // clip it
         tillWhen = campData->getTick() + clipValue(tillWhen, DaysToTicks(minDays), DaysToTicks(maxDays));

//         Town_COrder* to = new Town_COrder(CampaignOBUtil::cpiToCPIndex(cpi, *ob), Town_COrder::RemoveDepot, tillWhen);
//         ASSERT(to);
         cpi->addTownOrder(cpi->getCloseTown(), Town_COrder::RemoveDepot, tillWhen);

//         town.addOrder(to);
         town.setisBuildingDepot(False);

         // unchecked ammended by Paul - 12/9/99
         town.setSupplyInstaller(cpi);
         // end unchecked ammended by Paul - 12/9/99

         carryOut = True;
       }
     }
   }

   if(!carryOut)
   {
#if 0
     // tell player we couldn';t carry out order
     CampaignMessageInfo msgInfo(cpi->getSide(), CampaignMessages::NotInstallSupply);
     msgInfo.cp(cpi);
     msgInfo.where(cpi->getCloseTown());
     campGame->sendPlayerMessage(msgInfo);
#endif
   }

   return False;
}

bool OrderImp_UpgradeFort::beginOrder(CampaignLogicOwner* campGame, const CampaignOrder* order, ParamCP cpi) const
{
   CampaignData* campData = campGame->campaignData();
   Town& t = campData->getTown(cpi->getCloseTown());
   Boolean carryOut = False;

   if(cpi->atTown())
   {
     if( (cpi->getSide() == t.getSide()) &&
         (t.getFortifications() < Town::MaxFortifications) &&
         (campData->getArmies().hasEngineer(cpi)) )
     {

       /*
        * It takes 4 weeks + or - (0 - 20%) to build a fort.
        *
        */

       TimeTick tillWhen = WeeksToTicks(4);

       // adjust + or - (0 - 20%)
       TimeTick offset = MulDiv(tillWhen, 2, 10);

       tillWhen = campData->getTick() + CRandom::get(tillWhen - offset, tillWhen + offset);

       OrderBattle* ob = campData->getArmies().ob();
//       Town_COrder* to = new Town_COrder(CampaignOBUtil::cpiToCPIndex(cpi, *ob), Town_COrder::BuildFort, tillWhen);
//       ASSERT(to);
       cpi->addTownOrder(cpi->getCloseTown(), Town_COrder::BuildFort, tillWhen);

//       t.addOrder(to);
       t.setisBuildingFort(True);
       carryOut = True;

       // unchecked ammended by Paul - 12/9/99
       t.setFortUpgrader(cpi);
       // end unchecked ammended by Paul - 12/9/99
     }
   }


   if(!carryOut)
   {
     // tell player we couldn';t carry out order
     CampaignMessageInfo msgInfo(cpi->getSide(), CampaignMessages::NotInstallFort);
     msgInfo.cp(cpi);
     msgInfo.where(cpi->getCloseTown());
     campGame->sendPlayerMessage(msgInfo);
   }

   return False;
}

bool OrderImp_BlowBridge::beginOrder(CampaignLogicOwner* campGame, const CampaignOrder* order, ParamCP cpi) const
{
   CampaignData* campData = campGame->campaignData();
   if(cpi->atTown())
   {
     OrderBattle* ob = campData->getArmies().ob();
     ASSERT(ob);

     Town& t = campData->getTown(cpi->getTown());

     const TerrainTypeItem& terrain = campData->getTerrainType(t.getTerrain());

     if(terrain.isBridge() && t.isBridgeUp())
     {
       TimeTick tillWhen = campData->getTick();
//       Town_COrder* to = new Town_COrder(CampaignOBUtil::cpiToCPIndex(cpi, *ob), Town_COrder::BlowBridge, tillWhen);
//       ASSERT(to);
       cpi->addTownOrder(cpi->getCloseTown(), Town_COrder::BlowBridge, tillWhen);
       Town_COrder* to = cpi->townOrders().getLast();

       // we try to blow bridge right away
       if(TownProcess::processTownOrder(campGame, cpi, to, tillWhen))
       {
         cpi->townOrders().remove(to);
       }
     }
   }

   // TODO: send player message telling him if we couldn't complete
   return False;
}

bool OrderImp_RepairBridge::beginOrder(CampaignLogicOwner* campGame, const CampaignOrder* order, ParamCP cpi) const
{
   CampaignData* campData = campGame->campaignData();
   if(cpi->atTown())
   {
     OrderBattle* ob = campData->getArmies().ob();
     ASSERT(ob);

     Town& t = campData->getTown(cpi->getTown());

     const TerrainTypeItem& terrain = campData->getTerrainType(t.getTerrain());

     if(terrain.isBridge() && !t.isBridgeUp())
     {
       TimeTick tillWhen = campData->getTick() + HoursToTicks(12);

//       Town_COrder* to = new Town_COrder(CampaignOBUtil::cpiToCPIndex(cpi, *ob), Town_COrder::RepairBridge, tillWhen);
//       ASSERT(to);

//       t.addOrder(to);
       cpi->addTownOrder(cpi->getCloseTown(), Town_COrder::RepairBridge, tillWhen);
     }
   }

// TODO: send player message telling him if we couldn't complete
   return False;
}

#endif   // !defined(EDITOR)


static OrderImp_MoveTo        s_orderImp_MoveTo;
static OrderImp_Hold          s_orderImp_Hold;
static OrderImp_RestRally     s_orderImp_RestRally;
static OrderImp_Attach        s_orderImp_Attach;
static OrderImp_AttachSP      s_orderImp_AttachSP;
static OrderImp_Leader        s_orderImp_Leader;
static OrderImp_Garrison      s_orderImp_Garrison;
static OrderImp_InstallSupply s_orderImp_InstallSupply;
static OrderImp_UpgradeFort   s_orderImp_UpgradeFort;
static OrderImp_BlowBridge    s_orderImp_BlowBridge;
static OrderImp_RepairBridge  s_orderImp_RepairBridge;
static OrderImp_RemoveDepot   s_orderImp_RemoveDepot;

static const OrderImpBase* s_orderImp[] =
{
   &s_orderImp_MoveTo,
   &s_orderImp_Hold,
   &s_orderImp_RestRally,
   &s_orderImp_Attach,
   &s_orderImp_AttachSP,
   &s_orderImp_Leader,
   &s_orderImp_Garrison,
   &s_orderImp_InstallSupply,
   &s_orderImp_UpgradeFort,
   &s_orderImp_BlowBridge,
   &s_orderImp_RepairBridge,
   &s_orderImp_RemoveDepot
};

#ifdef DEBUG

class CheckOrderImpClass {
   public:
      CheckOrderImpClass()
      {

#ifdef __WATCOM_CPLUSPLUS__
#pragma warning 367 9;  // disable expression always true warning
#pragma warning 13 9;   // disable unreachable code warning
#endif

         ASSERT( (sizeof(s_orderImp) / sizeof(OrderImpBase*)) == Orders::Type::HowMany);

         for(int i = 0; i < Orders::Type::HowMany; ++i)
         {
            ASSERT(s_orderImp[i]->type() == i);
         }
      }
};

static CheckOrderImpClass check;

#endif



SimpleString CampaignOrderUtil::getDescription(const CampaignData* campData, const CampaignOrder* order, OD_TYPE how)
{
   return s_orderImp[order->getType()]->getDescription(campData, order, how);
}

#if !defined(EDITOR)
bool beginOrder(CampaignLogicOwner* campGame, const CampaignOrder* order, ParamCP cpi)
{
   return s_orderImp[order->getType()]->beginOrder(campGame, order, cpi);
}
#endif


Boolean allowed(Orders::Type::Value t, ConstParamCP cpi, const CampaignData* campData)
{
#if !defined(EDITOR)

  /*
   * Some useful values
   */

  const CommandPosition* cp = campData->getCommand(cpi);
  Boolean atTown = cp->atTown();
  ITown iTown = cp->getCloseTown();
  ASSERT(iTown != NoTown);

  const Town& town = campData->getTown(iTown);

  /*
   * Can always give current order
   */

  if(cp->getCurrentOrder()->getType() == t)
    return True;

  /*
   * If unit is under siege, it can't give any orders
   */

  if(cp->isGarrison() && town.isSieged())
    return False;

  /*
   * If Garrison order, unit must be at a town with fortifications
   */

  if(t == Orders::Type::Garrison)
  {
    return (atTown && town.getFortifications() > 0) ? True : False;
  }

  /*
   * If Install Supply Order, unit must be at a town
   * that is not already a depot or source, and it
   * must be adjacent to another friendly depot or source
   */

  if(t == Orders::Type::InstallSupply)
  {
    if(atTown)
    {
       const Province& p = campData->getProvince(town.getProvince());
       Nationality n = cpi->getNation(); //p.getNationality();

       const int nDepotsAllowed = campData->getNations().getNDepotsAllowed(n);
       const int nDepotsBuilt = campData->getNations().getNDepotsBuilt(n);
       const int nDepotsUC = campData->getNations().getNDepotsUC(n);

       if( (town.getSide() == cp->getSide()) &&
           (!town.getIsDepot()) &&
           (!town.getIsSupplySource()) &&
           (nDepotsBuilt + nDepotsUC < nDepotsAllowed) )

       {
         // make sure we are adjacent to a friendly depot or source
         for(int i = 0; i < MaxConnections; i++)
         {
            if(town.getConnection(i) != NoConnection)
            {
               const Connection& con = campData->getConnection(town.getConnection(i));
               const Town& ot = campData->getTown(con.getOtherTown(iTown));

               if( (ot.getSide() == cp->getSide()) &&
                  ((ot.getIsDepot()) || (ot.getIsSupplySource())) )
               {
                  return True;
               }
            }
            else
              break;
         }
       }
    }

    return False;
  }

  /*
   * If Remove Depot Order, unit must be at a town
   * that is already a depot, and side must have already built depots
   */

  if(t == Orders::Type::RemoveDepot)
  {
     if(atTown)
     {

         if(  (town.getSide() == cp->getSide()) &&
              (town.getIsDepot()) &&
              (campData->getNations().getNDepotsBuilt(cpi->getNation()) > 0) )

         {
            return True;
         }
     }

     return False;
  }

  /*
   * If Build Fortification
   */

  if(t == Orders::Type::UpgradeFort)
  {
    if(  (atTown) &&
         (town.getSide() == cp->getSide()) &&
         (town.getFortifications() < Town::MaxFortifications) )
    {
      return campData->getArmies().hasEngineer(cpi);
    }

    return False;
  }

  /*
   * If Blow Bridge
   */

  if(t == Orders::Type::BlowBridge)
  {
    if(atTown)
    {
      // town must be a bridge
      const TerrainTypeItem& ti = campData->getTerrainType(town.getTerrain());
      if(town.isBridgeUp() && ti.isBridge())
      {
        // first we must have an Inf, or Engineer SP
        return (campData->getArmies().hasInfantry(cpi) || campData->getArmies().hasEngineer(cpi));
      }
    }

    return False;
  }

  /*
   * If Repair Bridge
   */

  if(t == Orders::Type::RepairBridge)
  {
    if(atTown)
    {
      // town must be a bridge- town with the bridge down
      const TerrainTypeItem& ti = campData->getTerrainType(town.getTerrain());
      if(!town.isBridgeUp() &&  ti.isBridge())
      {
        // as long as we have SP we can try to repair
        SPCount spCount = campData->getArmies().getUnitSPCount(cpi, True);
        return (spCount > 0);
      }
    }

    return False;
  }
#endif

  return True;
}

Boolean CampaignOrderUtil::canGiveOnArrival(const CampaignData* campData, ConstParamCP cpi,
            const CampaignOrder& order, Orders::Type::Value o)
{
  /*
   * Hold and Rest\Rally we can always give, test only for Garrison, Attach
   */

  if(!Orders::Type::canGiveOnArrival(o))
    return False;

#if !defined(EDITOR)
  switch(o)
  {

    case Orders::Type::Garrison:
    case Orders::Type::UpgradeFort:
    {
      /*
       * To Give a Garrison on Arrival order
       * order must have a destination town that has fortifications
       */

      if(order.hasDestTown())
      {
        const Town& t = campData->getTown(order.getDestTown());

        if(o == Orders::Type::Garrison)
          return (t.getFortifications() > 0);
        else
          return (campData->getArmies().hasEngineer(cpi) && t.getFortifications() < Town::MaxFortifications);
//        return (t.getFortifications() < Town::MaxFortifications);
      }
      else
        return False;
    }

    case Orders::Type::Attach:
    {
      /*
       * If we have a target unit we can attach
       */

      if(order.hasTargetUnit())
        return True;

      /*
       * Otherwise, if we have a destination town we can
       * attach if town has a friendly unit
       */

      if(order.hasDestTown())
      {
        if(cpi != NoCommandPosition)
        {
          // CommandPosition* cp = campData->getCommand(cpi);

          ConstUnitIter iter(&campData->getArmies(), campData->getArmies().getFirstUnit(cpi->getSide()), False);
          while(iter.sister())
          {
            const CommandPosition* sisterCP = iter.currentCommand();

            if( (sisterCP->atTown()) && (sisterCP->getTown() == order.getDestTown()) )
            {
              return True;
            }
          }
        }
      }
      return False;
    }

    case Orders::Type::RepairBridge:
    {
      if(order.hasDestTown())
      {
        const Town& t = campData->getTown(order.getDestTown());
        const TerrainTypeItem& ui = campData->getTerrainType(t.getTerrain());

        // if bridge is blown we can repair it
        // as long as we have SP we can try to repair
        SPCount spCount = campData->getArmies().getUnitSPCount(cpi, True);
        return (spCount > 0 && !t.isBridgeUp());
      }

      return False;
    }

    case Orders::Type::BlowBridge:
    {
      if(order.hasDestTown())
      {
        const Town& t = campData->getTown(order.getDestTown());
        const TerrainTypeItem& ui = campData->getTerrainType(t.getTerrain());

        return ((campData->getArmies().hasInfantry(cpi) || campData->getArmies().hasEngineer(cpi)) && (ui.isBridge() && t.isBridgeUp()));
      }

      return False;
    }

    case Orders::Type::InstallSupply:
    {
#if 0
      const Table1D<UBYTE>& table = scenario->getSideDepotLimitTable();
      const int nDepots = campData->getSideData().getSideDepotsBuilt(cpi->getSide());
      const int nDepotsUC = campData->getSideData().getSideDepotsUnderConstruction(cpi->getSide());
#endif

      if( (order.hasDestTown()) )//&&
//          (nDepots + nDepotsUC < table.getValue(cpi->getSide())) )
      {
        const Town& t = campData->getTown(order.getDestTown());
        const Province& p = campData->getProvince(t.getProvince());
        Nationality n = cpi->getNation(); //p.getNationality();

        const int nDepotsAllowed = campData->getNations().getNDepotsAllowed(n);
        const int nDepotsBuilt = campData->getNations().getNDepotsBuilt(n);
        const int nDepotsUC = campData->getNations().getNDepotsUC(n);

        if(nDepotsBuilt + nDepotsUC >= nDepotsAllowed)
           return False;

        // make sure we are adjacent to a friendly depot or source
        for(int i = 0; i < MaxConnections; i++)
        {
          if(t.getConnection(i) != NoConnection)
          {
            const Connection& con = campData->getConnection(t.getConnection(i));
            const Town& ot = campData->getTown(con.getOtherTown(order.getDestTown()));

            if( (ot.getSide() == cpi->getSide()) &&
                ((ot.getIsDepot()) || (ot.getIsSupplySource())) )
            {
              return True;
            }
          }
          else
            break;
        }
      }

      return False;
    }

    case Orders::Type::RemoveDepot:
    {
      if( (order.hasDestTown()) )//&&
//          (campData->getSideData().getSideDepotsBuilt(cpi->getSide()) > 0) )
      {
        const Town& t = campData->getTown(order.getDestTown());
        const Province& p = campData->getProvince(t.getProvince());
        Nationality n = cpi->getNation(); //p.getNationality();

        return (campData->getNations().getNDepotsBuilt(n) > 0);
      }

      return False;
    }
  }
#endif

  return True;
}

}; // namespace CampaignOrderUtil




/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.2  2001/06/18 08:25:44  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
