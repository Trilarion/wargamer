/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1995, Steven Morle-Green, All Rights Reserved
 *----------------------------------------------------------------------
 *
 * Attrition and Straggler calculations
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/07/02 15:23:16  greenius
 * Battle, Campaign, Combined game and Campaign Editor now compile and run... mostly
 *
 * Revision 1.2  2001/06/18 08:25:44  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 * Revision 1.1  2001/06/13 08:52:37  greenius
 * Initial Import
 *
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "atrition.hpp"
#include "campdint.hpp"
#include "armies.hpp"
#include "losses.hpp"
#include "options.hpp"
#include "scenario.hpp"
#include "town.hpp"


#ifdef DEBUG
#include "clog.hpp"
extern LogFile lossLog;
#endif

class AttritionLevel {
public:
  enum Value {
    Excellent,
    VeryGood,
    Good,
    Average,
    Fair,
    Poor,
    HowMany,
    First = Excellent,
    Last = Poor
  };
};

class SizeRatio {
public:
  enum Value {
    Small,
    Average,
    Large,
    VeryLarge
  };
};

class AttritionModifiers {
public:
  enum Value {
    InFriendlyTerritory,
    CharismaAndStaffOver360,
    MoraleLessThan100,
    MoraleLessThan50,
    NationMoraleOver220,
    NationMoraleOver180,
    NationMoraleLessThan100,
    NationMoraleLessThan50
  };
};

AttritionLevel::Value getAttritionLevel(Attribute currentFatigue, int supplyPercent)
{
  ASSERT(supplyPercent <= 100);
  ASSERT(supplyPercent >= 0);

  /*
   * Calculate Attrition level. This is done by reducing current fatigue
   * 1% for every 2% supply shortage
   */

  int reduceBy = MulDiv(currentFatigue, (100-supplyPercent)/2, 100);
  int level = currentFatigue-reduceBy;

  if(level > 230)
    return AttritionLevel::Excellent;
  else if(level > 207)
    return AttritionLevel::VeryGood;
  else if(level > 158)
    return AttritionLevel::Good;
  else if(level > 109)
    return AttritionLevel::Average;
  else if(level > 61)
    return AttritionLevel::Fair;
  else
    return AttritionLevel::Poor;

}

int getAttritionLoss(AttritionLevel::Value aLevel, AttritionClass::value c)
{
  ASSERT(aLevel < AttritionLevel::HowMany);
  ASSERT(c < AttritionClass::HowMany);

  const Table2D<int>& table = scenario->getAttritionTable();

  return table.getValue(aLevel, c);
}

SizeRatio::Value getSizeRatio(SPCount count)
{
  if(count < 7)
    return SizeRatio::Small;
  else if(count < 49)
    return SizeRatio::Average;
  else if(count < 101)
    return SizeRatio::Large;
  else
    return SizeRatio::VeryLarge;
}

void dailyAttrition(ICommandPosition cpi, CampaignData* campData, int todaysFatigueCount)
{
  ASSERT(cpi != NoCommandPosition);
  Armies& armies = campData->getArmies();
  CommandPosition* cp = armies.getCommand(cpi);
  Leader* leader = armies.getUnitLeader(cpi);

#ifdef DEBUG
  lossLog.printf("===== Processing Daily Attrition for %s =====", (const char*) campData->getUnitName(cpi).toStr());
#endif

  // XX's with only 1 SP do not suffer attrition
  int spCount = armies.getUnitSPCount(cpi, True);
  if(spCount <= 1)
     return;   

  /*
   *  First calculate attrition level
   */

  Attribute supply = cp->getSupply();

  int supplyPercent = MulDiv(supply, 100, 255);

  /*
   * Current fatigue is current fatigue +/- current fatigue count
   */

  int fatigueLevel = cp->getFatigue() + todaysFatigueCount;
  fatigueLevel = maximum(0, minimum(fatigueLevel, Attribute_Range));

  AttritionLevel::Value aLevel = getAttritionLevel(fatigueLevel, supplyPercent);

#ifdef DEBUG
  lossLog.printf("%s -- fatigue = %d, supply = %d, supplyPercent = %d, attritionLevel = %d",
        cp->getNameNotNull(), fatigueLevel, (int)cp->getSupply(), supplyPercent, (int)aLevel);
#endif

  /*
   * get force size value
   */

  SizeRatio::Value size = getSizeRatio(spCount);

  /*
   * Test for attritionLevel modifiers
   *
   * First, if we're at a town get Town Modifier table
   *
   */

  int modifyBy = 0;

  if(cp->atTown())
  {
    const Table2D<SWORD>& table = scenario->getAttritionModifierForTownTable();
    const Town& town = campData->getTown(cp->getTown());

    modifyBy += table.getValue(size, town.getSize());

#ifdef DEBUG
    lossLog.printf("modifyBy(%d) after atTown modifier", modifyBy);
#endif
  }

  /*
   *  Otherwise get connection modifier table
   */

  else
  {
    const Table2D<SWORD>& table = scenario->getAttritionModifierForConnectionTable();
    const Connection& con = campData->getConnection(cp->getConnection());

    modifyBy += table.getValue(size, con.quality);
#ifdef DEBUG
    lossLog.printf("modifyBy(%d) after connection modifier", modifyBy);
#endif
  }

  /*
   *  Other miscellanous modifiers
   */

  const Table1D<SWORD>& table = scenario->getAttritionModifiersTable();

  /*
   * If force is in friendly territory
   */

  const Town& t = campData->getTown(cp->getCloseTown());
  const Province& p = campData->getProvince(t.getProvince());
  if(campData->getNations().getFriendlyTo(p.getNationality()) == cp->getSide())
  {
    modifyBy += table.getValue(AttritionModifiers::InFriendlyTerritory);

#ifdef DEBUG
    lossLog.printf("modifyBy(%d) after Friendly-territory modifier", modifyBy);
#endif
  }

  /*
   *  If leader's charisma + staff >= 360
   */

  if((leader->getCharisma()+leader->getStaff()) >= 360)
  {
    modifyBy += table.getValue(AttritionModifiers::CharismaAndStaffOver360);
#ifdef DEBUG
    lossLog.printf("modifyBy(%d) after charismaAndStaff modifier", modifyBy);
#endif
  }

  /*
   * If forces current morale is less than 50
   */

  if(cp->getMorale() < 50)
  {
    modifyBy += table.getValue(AttritionModifiers::MoraleLessThan50);
#ifdef DEBUG
    lossLog.printf("modifyBy(%d) after moraleLessThan50 modifier", modifyBy);
#endif
  }

  /*
   * If forces current morale is less than 100
   */

  if(cp->getMorale() < 100)
  {
    modifyBy += table.getValue(AttritionModifiers::MoraleLessThan100);
#ifdef DEBUG
    lossLog.printf("modifyBy(%d) after moraleLessThan100 modifier", modifyBy);
#endif
  }

  /*
   * Modify for national morale
   */

  Nationality n = (scenario->getNationType(cp->getNation()) == MajorNation) ?
    cp->getNation() : scenario->getDefaultNation(cp->getSide());

  Attribute nMorale = campData->getNationMorale(n);
  // if it is greater than 200, manpower * 20%
  if(nMorale >= 220)
    modifyBy += table.getValue(AttritionModifiers::NationMoraleOver220);
  else if(nMorale >= 180)
    modifyBy += table.getValue(AttritionModifiers::NationMoraleOver180);
  else if(nMorale < 50)
    modifyBy += table.getValue(AttritionModifiers::NationMoraleLessThan50);
  else if(nMorale < 100)
    modifyBy += table.getValue(AttritionModifiers::NationMoraleLessThan100);

  aLevel = clipValue<AttritionLevel::Value>(
     static_cast<AttritionLevel::Value>(aLevel + modifyBy),
     AttritionLevel::First, 
     AttritionLevel::Last);

  /*
   *  Find number SPs of given Attrition class. Reference table, and apply attrition
   */

  for(AttritionClass::value c = AttritionClass::Poor; c < AttritionClass::HowMany; INCREMENT(c))
  {

    SPCount count = armies.getNumClassSP(cpi, c);

    int loss = getAttritionLoss(aLevel, c);
    int n = minimum(32767, count * loss);
    SPLoss attrition(0, static_cast<UWORD>(n), 1000);

#ifdef DEBUG
    lossLog.printf(" Attrition loss for %s AttritionClass %d = %d.%04x",
          cp->getNameNotNull(), (int)c,
         (int)attrition.getInt(), (int)attrition.getFraction());
#endif

    SPCount actual = Losses::makeAttrition(&armies, cpi, attrition, c);

#ifdef DEBUG
    lossLog.printf("Actual losses for %s AttritionClass %d = %d",
          cp->getNameNotNull(), (int)c, (int)actual);
#endif
  }
}

void AttritionProc::procDailyAttrition(CampaignData* campData, ICommandPosition cpi)
{
  ASSERT(campData != 0);

#ifdef DEBUG
  lossLog.printf("============= Processing Daily Attrition for %s on %s =======",
       (const char*) campData->getUnitName(cpi).toStr(), campData->asciiTime());
#endif

//  CPIter iter(&campData->getArmies());

//  while(iter.sister())
//  {
     CommandPosition* cp = campData->getCommand(cpi);

     /*
      * If unit is on an offscreen location, don't apply attrition
      */

     Boolean noAttrition = False;
     if(cp->atTown())
     {
       Town& t = campData->getTown(cp->getTown());
       noAttrition = t.isOffScreen();
     }
     else
     {
       Connection& c = campData->getConnection(cp->getConnection());
       noAttrition = c.offScreen;
     }

     /*
      * If unit has an effective SPValue of 3 or less, don't apply
      */

     if(campData->getArmies().getUnitSPValue(cpi) <= 3)
       noAttrition = True;

     if(!noAttrition)
     {
       /*
        * Apply attrition to each subordinate unit individually
        */

       UnitIter uiter(&campData->getArmies(), cpi);
       while(uiter.next())
       {
         /*
          * Apply only to bottom level units
          */

         CommandPosition* subCP = uiter.currentCommand();

         if(subCP->getChild() == NoCommandPosition)
           dailyAttrition(uiter.current(), campData, cp->getFatigueCount());
       }

     }
#ifdef DEBUG
     else
       lossLog.printf("%s is offscreen", cp->getNameNotNull());
#endif
//  }

#ifdef DEBUG
  lossLog.printf("============ End of Daily Attrition ==========\n");
#endif
}


#if 0
Attrition::Attrition(CampaignData* campData) :
   d_campData(campData)
{
}

/*
 * Update attrition for organisation
 * Called once per day.
 *
 */

void Attrition::dailyAttrition(ICommandPosition cpi)
{

  ASSERT(cpi != NoCommandPosition);

  Armies& armies = d_campData->getArmies();

  CommandPosition* cp = armies.getCommand(cpi);

#ifdef DEBUG
  lossLog.printf("===== Processing Daily Attrition for %s =====", cp->getNameNotNull());
#endif

  /*
   *  If unit is at an offscreen location, no attrition is applied
   */

   Boolean offScreen = False;
   if(cp->atTown())
   {
     Town& t = d_campData->getTown(cp->getTown());

     offScreen = t.isOffScreen();
   }
   else
   {
     Connection& c = d_campData->getConnection(cp->getConnection());

     offScreen = c.offScreen;
   }

   if(offScreen)
   {
#ifdef DEBUG
     lossLog.printf("%s is offscreen", cp->getNameNotNull());
#endif
     return;
   }


  /*
   * If unit has 3 Effective SPs or less, no Attrition is applied
   */

  SPCount count = armies.getUnitSPValue(cpi);
  if(count < 3)
  {
    ASSERT(count > 0);
#ifdef DEBUG
    lossLog.printf("%s has less than 3 effective SP. Count = %d", cp->getNameNotNull(), (int)count);
#endif
    return;
  }


  const Table2D<int>& table = scenario->getAttritionTable();

  /*
   *  First calculate attrition level
   *  Attrition Level is one of 5 values (0-4)
   *
   *
   *  Multiply fatigue by %supply-level and convert value to (0 - 4)
   */


  Attribute supply = cp->getSupply();

  int supplyPercent = MulDiv(supply, 100, 255);

  Attribute attritionLevel = Attribute(MulDiv(supplyPercent, cp->getFatigue(), 100));

  int value;
  if(attritionLevel > 206)
    value = 0;
  else if(attritionLevel > 157)
    value = 1;
  else if(attritionLevel > 110)
    value = 2;
  else if(attritionLevel > 85)
    value = 3;
  else
    value = 4;

#ifdef DEBUG
  lossLog.printf("%s -- fatigue = %d, supply = %d, supplyPercent = %d, attritionLevel = %d(%d)",
        cp->getNameNotNull(), (int)cp->getFatigue(), (int)cp->getSupply(), supplyPercent, (int)attritionLevel, value);
#endif

  /*
   *  Find number SPs of given Attrition class. Reference table, and apply attrition
   */

  for(AttritionClass::value c = AttritionClass::Poor; c < AttritionClass::HowMany; INCREMENT(c))
  {

    SPCount count = armies.getNumClassSP(cpi, c);

    int loss = table.getValue(value, c);

    int n = minimum(32767, count * loss);
    SPLoss attrition(0, static_cast<UWORD>(n), 1000);

#ifdef DEBUG
    lossLog.printf(" Attrition loss for %s AttritionClass %d = %d.%04x",
          cp->getNameNotNull(), (int)c,
         (int)attrition.getInt(), (int)attrition.getFraction());
#endif

    SPCount actual = Losses::makeAttrition(&armies, cpi, attrition, c);

#ifdef DEBUG
    lossLog.printf("Actual losses for %s AttritionClass %d = %d",
          cp->getNameNotNull(), (int)c, (int)actual);
#endif
  }

//  Boolean wipedOut = armies.clearDestroyedUnits(cpi);
//  ASSERT(wipedOut != True);

}

//    no more stragglers!

/*
 * Update stragglers for organisation
 * Called once per day.
 *
 * All of the numbers ought to be scenario or unittype defined constants
 */

void Attrition::dailyStragglers(ICommandPosition hunit)
{
   Armies& armies = d_campData->getArmies();

   /*
    * Skip attrition if realism setting off
    */

   if(!getOption(OPT_Attrition))
      return;

   ASSERT(hunit != NoCommandPosition);
   CommandPosition* cp = armies.getCommand(hunit);
   const CampaignOrder* order = cp->getCurrentOrder();

#ifdef DEBUG
   lossLog.printf("------------------------------------------------------");
   lossLog.printf("dailyStragglers(%s)", cp->getName());
#endif

   /*
    * Calculate % stragglers and recoveries
    *
    * Note: % is actual 1/1024
    */

   SPLoss stragglers = 0;
   SPLoss recovery = 0;
   SPLoss attrition = 0;

   // attrition.set(0, 5, 100);        // 5%

   switch(cp->getMode())
   {
   case CampaignMovement::CPM_Moving:
      if(cp->doingForceMarch())
         stragglers += SPLoss(0, 20, 100);            // 20%
      else
         stragglers += SPLoss(0, 1, 100);          // 1%
      recovery += SPLoss(0, 1, 200);                  // 1/200;
      break;
   default:
      stragglers += SPLoss(0, 1, 512);
      recovery += SPLoss(0, 1, 10);
      break;
   }

   order->adjustStragglers(stragglers, recovery);

#if 0
   switch(order.getType())
   {
   case ORDER_ROUT:
      stragglers += SPLoss(0, 1, 10);
      recovery = 0;
      break;
   case ORDER_REST_RALLY:
      stragglers = 0;
      recovery = SPLoss(0, 1, 20);
      break;
   }
#endif

   /*
    * Apply Supply Level effect to make more stragglers
    */

   if(cp->getSupply() != Attribute_Range)
   {
      int n = minimum(32767, Attribute_Range - cp->getSupply());
      stragglers += SPLoss(0, UWORD(n), UWORD(5 * Attribute_Range)); // Up to 1/5th (20%) caused by low supply
   }


   // attrition.set(0, 5, 100);        // 5%

   attrition = stragglers / 10;        // Attrition level to be 10th of straggler level


#ifdef DEBUG
   if( (stragglers != 0) || (recovery != 0) || (attrition != 0))
   {
      lossLog.printf("  stragglers=%d.%04x, recovery=%d.%04x, attrition=%d.%04x",
         (int)stragglers.getInt(), (int)stragglers.getFraction(),
         (int)recovery.getInt(),   (int)recovery.getFraction(),
         (int)attrition.getInt(),  (int)attrition.getFraction());
   }
#endif

   /*
    * Convert Percentages into Strength Points
    * (or fraction of an SP
    */

   UnitCountInfo info;

   armies.getUnitCounts(hunit, info, True);

   stragglers *= info.effective;
   recovery *= info.stragglers;
   attrition *= info.total;

#ifdef DEBUG
   lossLog.printf("  TotalSP = %d, Stragglers = %d, Effective = %d",
      (int)info.total,
      (int)info.stragglers,
      (int)info.effective);

   if((stragglers != 0) || (recovery != 0))
   {
      lossLog.printf("  stragglers=%d.%04x, recovery=%d.%04x",
         (int)stragglers.getInt(), (int)stragglers.getFraction(),
         (int)recovery.getInt(),   (int)recovery.getFraction());
   }
#endif

   /*
    * Apply stragglers/recovery to stragglers...
    */

   if(stragglers > recovery)
   {
      stragglers -= recovery;

      if(stragglers != 0)
      {
#ifdef DEBUG
         lossLog.printf(" Creating %d.%04x Stragglers",
            (int)stragglers.getInt(), (int)stragglers.getFraction());
#endif
         armies.orgMakeStragglers(hunit, stragglers);

      }
   }
   else
   {
      recovery -= stragglers;

      if(recovery != 0)
      {
#ifdef DEBUG
         lossLog.printf(" Recovering %d.%04x Stragglers",
            (int)recovery.getInt(),   (int)recovery.getFraction());
#endif
         armies.orgRecoverStragglers(hunit, recovery);
      }
   }

   /*
    * Do attrition... based on new straggler count.
    * NO: Attrition based on ALL units
    */

   // armies.getUnitCounts(hunit, info, True);
   // attrition *= info.stragglers;

#ifdef DEBUG
   // lossLog.printf("  TotalSP = %d, Stragglers = %d, Effective = %d",
   // (int)info.total,
   // (int)info.stragglers,
   // (int)info.effective);

   if(attrition != 0)
   {
      lossLog.printf("  Losing %d.%04x to attrition",
         (int)attrition.getInt(),  (int)attrition.getFraction());
   }
#endif

   if(attrition != 0)
      armies.orgMakeAttrition(hunit, attrition);

#ifdef DEBUG
   lossLog.close();
#endif
}

#endif


/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.1  2001/07/02 15:23:16  greenius
 * Battle, Campaign, Combined game and Campaign Editor now compile and run... mostly
 *
 * Revision 1.2  2001/06/18 08:25:44  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
