##############################################################################
# Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)	 #
# This Software is subject to the GNU General Public License.  				 #
# For License information see the file COPYING in the project root directory #
# For more information see the file README.									 #
##############################################################################

#####################################
# Makefile Include for Campaign Logic
#
# This is included from wargamer.mk
#
# It might be better to have a stand-alone makefile for each
# library instead of including like this.  That would make
# compiling individual libraries easier and faster, and also
# encourage each part of the program to be more independant
#-------------------------------------------------------

ROOT=..\..

############ Comment the MAKEDLL=1 to not make a DLL version
!ifndef MAKEDLL
!ifndef %NO_WG_DLL
######################### Need to work on the user interface interaction first
MAKEDLL=1
!endif
!endif

!ifdef MAKEDLL
FNAME = clogic
!else
NAME = clogic
!endif

lnk_dependencies += clogic.mk

!include $(ROOT)\config\wgpaths.mif

OBJS =  realord.obj  &
	townprop.obj

!ifndef EDITOR
OBJS +=	siege.obj    &
        c_const.obj  &
	cbatproc.obj &
        cbatmsg.obj  &
	cc_town.obj  &
	cc_close.obj &
	cu_data.obj  &
	cu_order.obj &
	recover.obj  &
	condutil.obj &
	losses.obj   &
	cc_avoid.obj &
	cc_util.obj  &
	campcomb.obj &
	atrition.obj &
	cu_move.obj  &
	cu_speed.obj &
	cu_fog.obj   &
	cu_rand.obj  &
	c_supply.obj &
	c_weathr.obj &
	cu_ldr.obj   &
	cu_repo.obj  &
	defect.obj   &
        sched.obj    &
	despatch.obj &
	ds_reorg.obj &
	ds_town.obj  &
    ds_repoSP.obj &
	ds_unit.obj  &
	cmsgutil.obj
!endif

CFLAGS += -i=$(ROOT)\src\system;$(ROOT)\src\gamesup
CFLAGS += -i=$(ROOT)\res
CFLAGS += -i=$(ROOT)\src\ob

all :: $(TARGETS) .SYMBOLIC
	@%null

!ifdef MAKEDLL
###########
#Make DLL
###########

CFLAGS += -DEXPORT_CLOGIC_DLL
SYSLIBS += COMCTL32.LIB VFW32.LIB DSOUND.LIB
LIBS += system.lib
LIBS += gamesup.lib
LIBS += ob.lib
LIBS += campdata.lib

!include $(ROOT)\config\dll95.mif

!else

###########
# Make Library
###########

!include $(ROOT)\config\lib95.mif

!endif

#####################################################################
# $Log$
# Revision 1.1  2001/06/13 08:52:37  greenius
# Initial Import
#
#####################################################################
