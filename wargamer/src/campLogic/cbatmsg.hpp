/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef CBATMSG_HPP
#define CBATMSG_HPP

#ifndef __cplusplus
#error cbatmsg.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 * Messages from Battle Logic to User Interface
 *
 *----------------------------------------------------------------------
 */

#include "wg_msg.hpp"
#include "cbattle.hpp"

class CampaignData;
class CampaignBattle;
// class BattleInfoData;
class CampaignLogicOwner;

struct BattleInfoData {
	const CampaignBattleUnitList* d_units;      // list of both sides units
	Side d_whichSide;             // current side
	Side d_victor;                // victorious side
	ITown d_town;

	BattleInfoData(const CampaignBattleUnitList* units, Side side, Side victor, ITown town) :
	  d_units(units),
	  d_whichSide(side),
	  d_victor(victor),
	  d_town(town) {}
};

#if 0   // Moved to higher level
class ChooseBattleModeRequest : public WargameMessage::WaitableMessage
{
        CampaignData* d_campData;
		CampaignBattle* d_battle;

        BattleMode::Mode d_mode;        // Filled in by run()

	public:
		ChooseBattleModeRequest(CampaignData* campData, CampaignBattle* battle) :
            d_campData(campData),
            d_battle(battle),
            d_mode(BattleMode::Setup)
        {
        }

        // static void send(CampaignData* campData, CampaignBattle* battle);

		virtual void run();			// Implemented elsewhere
        // virtual void clear();

        void mode(BattleMode::Mode mode) { d_mode = mode; }
        BattleMode::Mode mode() const { return d_mode; }
        Location location() const;
};


class BattleDayDialMsg : public WargameMessage::WaitableMessage {
		const BattleInfoData& d_data;	// info for battle in progress
        EndOfDayMode::Mode d_result;

	public:
		BattleDayDialMsg(const BattleInfoData& data) : d_data(data), d_result() { }
		void run();			// Implemented elsewhere
        
        EndOfDayMode::Mode result() const { return d_result; }

        // Functions called by run()

        const CampaignBattleUnitList& unitList(Side s) const { return d_data.d_units[s]; }
        ITown town() const { return d_data.d_town; }
        Side currentSide() const { return d_data.d_whichSide; }
        Side victor() const { return d_data.d_victor; }

        void setResult(EndOfDayMode::Mode result) { d_result = result; }


};

class BattleResultMsg : public WargameMessage::WaitableMessage {
		const BattleInfoData& d_data;	// info for battle in progress
	public:
		BattleResultMsg(const BattleInfoData& data) : d_data(data) { }
		void run();			// Implemented elsewhere
};
#endif

#endif /* CBATMSG_HPP */

