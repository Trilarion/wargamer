/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
#include "stdinc.hpp"
#include "c_weathr.hpp"
#include "campdint.hpp"
#include "weather.hpp"
#include "wg_rand.hpp"
#include "misc.hpp"
#include "campctrl.hpp"
#include "scenario.hpp"

#ifdef DEBUG
//extern LogFileFlush weatherLog;
#endif


const int nMonths = 12;
const int nWeatherTypes = 3;


static int getWeatherProbability(int month, CampaignWeather::Weather w)
{
  ASSERT(month < nMonths);
  ASSERT(w < CampaignWeather::Weather_HowMany);

  const Table2D<UBYTE>& table = scenario->getWeatherTable();
  return table.getValue(month, w);
}

void CampaignWeatherUtil::doTodaysWeather(CampaignLogicOwner* campGame)
{
    CampaignData* campData = campGame->campaignData();

  /*
   * Get current month
   */

  const Date& date = campData->getDate();

#ifdef DEBUG
  weatherLog.printf("===== Doing Todays Weather on %s %d", getMonthName(date.month, FALSE), (int) date.day);
#endif

  /*
   * Get a random value between 0 - 99
   */

//  static RandomNumber rand;

  int dice = CRandom::get(100);
  int chance = 0;

#ifdef DEBUG
  Boolean found = False;

  // Check that table is set up correctly
  {
     int chance = 0;
     for(CampaignWeather::Weather w = CampaignWeather::Sunny; w < CampaignWeather::Weather_HowMany; INCREMENT(w))
     {
         chance += getWeatherProbability(date.month, w);
     }
     ASSERT(chance == 100);
  }
#endif

  for(CampaignWeather::Weather w = CampaignWeather::Sunny; w < CampaignWeather::Weather_HowMany; INCREMENT(w))
  {
    chance += getWeatherProbability(date.month, w);

    if(dice < chance)
    {
#ifdef DEBUG
      found = True;
#endif
      CampaignWeather& weather = campData->getWeather();
      weather.setWeather(w);
      break;
    }
  }

#ifdef DEBUG
  ASSERT(found);
  weatherLog.printf("Die Roll is %d, chance = %d", dice, chance);
  weatherLog.printf("Current Weather is %s", CampaignWeather::getWeatherText(campData->getWeather().getWeather()));
  weatherLog.printf("Current Conditions are %s\n", CampaignWeather::getConditionsText(campData->getWeather().getGroundConditions()));
#endif

  campGame->updateWeather();
}




/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.2  2001/06/18 08:25:44  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
