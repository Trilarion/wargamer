/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef CU_FOG_HPP
#define CU_FOG_HPP

/*
 * Fog of War Utility
 */

#include "gamedefs.hpp"
#include "cpdef.hpp"
#include "measure.hpp"

struct CampUnitMoveData;
class CampaignData;
class CloseUnits;

class FogOfWarUtil {
  public:
	 class SpottingEnemyModifiers {      // spotting enemy modifier table index values
		public:
		  enum Value {
			  EnemyTerritory,
			  SmallUnit,
			  LargeUnit,
			  VeryLargeUnit,
			  HeavyRain,
			  FrenchInFrance,
           AlreadySeen,

			  HowMany
		  };
	 };

	 class InformationModifiers {     // information modifier table index values
		public:
		  enum Value {
			  Cavalry3To1,
			  Cavalry2To1,
			  Cavalry3To2,
			  Cavalry1To3,
			  Cavalry1To2,
			  ChokePoint,
			  HeavyRain,
			  HeavyMud,
			  InitBelow100,
			  InitBelow130,
			  InitAbove200,
			  MoraleBelow50,

			  HowMany
		  };
	 };

	 static void processDailyUnits(CampUnitMoveData* moveData, ICommandPosition cpi);
	 static void processHourlyUnits(CampUnitMoveData* moveData, ICommandPosition cpi);
	 static void processDailyTowns(CampaignData* campData);
};



#endif
