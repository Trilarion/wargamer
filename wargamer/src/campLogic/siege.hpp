/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef SIEGE_HPP
#define SIEGE_HPP

#ifndef __cplusplus
#error siege.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1995, Steven Morle-Green, All Rights Reserved
 *----------------------------------------------------------------------
 *
 *	Sieges, etc...
 *
 *----------------------------------------------------------------------
 */

#include "clogic_dll.h"
#include "gamedefs.hpp"
#include "cpdef.hpp"

class CampaignLogicOwner;

/*
 * External Functions in siege
 */

class Siege {
 public:
	CLOGIC_DLL static void dailySiege(CampaignLogicOwner* campGame);
	CLOGIC_DLL static void weeklySiege(CampaignLogicOwner* campGame);
	static bool arrivedAtTown(CampaignLogicOwner* campGame, ITown itown, ICommandPosition hunit);
};

#endif /* SIEGE_HPP */

