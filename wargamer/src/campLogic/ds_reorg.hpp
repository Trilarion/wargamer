/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef DS_REORG_HPP
#define DS_REORG_HPP

#ifndef __cplusplus
#error ds_reorg.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *	Despatch Reorganization messages
 *
 *----------------------------------------------------------------------
 */

#include "clogic_dll.h"
#include "gamedefs.hpp"
#include "cpdef.hpp"
#include "rank.hpp"

#include "despatch.hpp"
#include "campdint.hpp"
#include "armies.hpp"
#include "compos.hpp"
#include "armyutil.hpp"
#include "c_obutil.hpp"
#include <vector>

namespace DS_Reorg {




class ReorgList
{
public:

		struct ReorgItem
		{
			enum WhatEnum
			{
				NewCP,				// Create a new CP at same location as s_cp
				NewLeader,        // Create a new leader
				AttachCP,			// Attach s_cp
				AttachSP,			// Attach s_cp,s_sp
				AttachLeader,		// Attach s_leader
				Push,					// Following items will be children of the last CP
				Pop,					// Pop back up to previous CP.

				HowMany
			} s_what;

			ICommandPosition  s_cp;			   // CommandPosition for NewCP and AttachCP order
			ILeader				s_leader;	   // Leader for AttachLeader
			ISP					s_sp;			   // StrengthPoint for AttachSP
			RankEnum          s_rank;        // rank for new cp/leader
			Nationality       s_nation;      // nationality of new cp/leader
			bool           s_independent; // is new cp unattached?

			ReorgItem()
			{
				s_what = HowMany;
				s_cp = NoCommandPosition;
				s_leader = NoLeader;
				s_sp = NoStrengthPoint;
				s_rank = Rank_Division;
				s_nation = NATION_Neutral;
				s_independent = False;
			}
		};

		ICommandPosition d_dest;		// Who is the message destined for
                typedef std::vector<ReorgItem> Container;
                Container d_items;		// STL vector List of items

	public:

		CLOGIC_DLL ReorgList(ConstParamCP cp);
		CLOGIC_DLL ReorgList(void);
		CLOGIC_DLL ~ReorgList();

		// Functions used when creating ReorgList

		void destCP(ConstParamCP cp) { d_dest = constCPtoCP(cp); }

		void addPush();
		void addPop();
		void addCreateCP(ConstParamCP cp, Nationality n, RankEnum rank, bool independent);
		void addCreateLeader(ConstParamCP cp, Nationality n, RankEnum rank);
		void addTransferCP(ConstParamCP cp);
		void addTransferSP(ConstParamCP cp, const ConstISP& isp);
		void addTransferLeader(const ConstILeader& iLeader, ConstParamCP pos);

		// Functions used to process the list

		CLOGIC_DLL void process(CampaignData* campData);
};








class DS_Reorganize : public DespatchMessage
{
		ReorgList d_orgList;
	public:
		DS_Reorganize(ConstParamCP cp) : d_orgList(cp) { }
		DS_Reorganize(void) : d_orgList() { }

		/*
		 * Implementation of DespatchMessage Interface
		 */

		CLOGIC_DLL virtual void process(CampaignData* campData);
		CLOGIC_DLL virtual int pack(void * buffer, void * gamedata);
		CLOGIC_DLL virtual void unpack(void * buffer, void * gamedata);

#ifdef DEBUG
		CLOGIC_DLL virtual String getName();
#endif

		ReorgList* orgList() { return &d_orgList; }
};





	/*
	 * Reorganization Messages are recursively defined
	 * Typical method is:
	 *			h = start(cpTop);
	 *			  addCP(h, cp)
	 *			  push(h);
	 *			  	 addCP(h, cp1);
	 *				 addCP(h, cp2);
	 *			  pop(h);
	 *         addCP(h, cp3);
	 *			end(h);
	 *
	 * This means that units are to be transferred underneath cpTop
	 * If cpTop is NoCommandPosition, then units will become
	 * detached.
	 * If createCP is called, then it will be created at the
	 * location of the unit given.
	 *
	 * This example will result in a hierarchy of:
	 *
	 *						+- cp1 ...
	 *				+- cp +
	 *				|		+- cp2 ...
	 *    cpTop +
	 *          |
	 *          +- cp3 ...
	 *
	 */

	typedef DS_Reorganize* HOrganize;
	const HOrganize hNULL = 0;

	CLOGIC_DLL HOrganize start(ConstParamCP dest);
	CLOGIC_DLL void push(HOrganize handle);
	CLOGIC_DLL void pop(HOrganize handle);
	CLOGIC_DLL void createCP(HOrganize handle, ConstParamCP cp, Nationality n, RankEnum rank, Boolean independent);
	CLOGIC_DLL void createLeader(HOrganize handle, ConstParamCP cp, Nationality n, RankEnum rank);
	CLOGIC_DLL void transferCP(HOrganize handle, ConstParamCP cp);
	CLOGIC_DLL void transferSP(HOrganize handle, ConstParamCP cp, const ConstISP& isp);
	CLOGIC_DLL void transferLeader(HOrganize handle, const ConstILeader& iLeader, ConstParamCP cpi);
	CLOGIC_DLL void end(HOrganize handle);
};

#endif /* DS_REORG_HPP */

