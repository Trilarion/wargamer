/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1995, Steven Morle-Green, All Rights Reserved
 *----------------------------------------------------------------------
 *
 * Message Despatcher
 *
 * Any action that a player does that is to affect the world data
 * is done by sending a message to this module.
 *
 * "player" means either the human player, computer AI or remote
 * computer player.
 *
 * Part of this module's function is to coordinate messages from
 * remote computers so that they all get actioned in the same
 * game tick.
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.2  2001/06/18 08:25:44  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 * Revision 1.1  2001/06/13 08:52:37  greenius
 * Initial Import
 *
 * Revision 1.3  1996/06/22 19:06:06  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.2  1996/02/29 11:01:53  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.1  1996/02/23 14:08:35  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "despatch.hpp"


/*
 * Global Instance: This really ought to be part of CampaignGame or CampaignData
 */

Despatcher* Despatcher::s_instance = 0;

Despatcher* Despatcher::instance()
{
   if(s_instance == 0)
   {
       s_instance = new Despatcher();
   }
   return s_instance;
}


/*
 * Called by game logic each tick
 */

void Despatcher::doProcess(CampaignData* campData)
{
   d_campData = campData;
   GDespatcher::process(d_campData->getCTime().toULONG() );
}

/*
 * Send a message to despatcher
 *
 * The contents of data depends on id.
 */

void Despatcher::doSendMessage(DespatchMessage* msg)
{
   ULONG activationTime;

   if(CMultiPlayerConnection::connectionType() != ConnectionNone) {

      // set activation time at least 1 interval ahead
      // (NOTE : 2*interval may ensure more reliable Slave->Master communication)
      activationTime = d_campData->getCTime().toULONG() + (maxSyncInterval * 2);

      // pack message
      // (NOTE :including filling out the 'order_type' field)
      int data_length = msg->pack(&d_MultiplayerCampaignOrder, (void *) d_campData);

      // send message to remote machine
      if(g_directPlay) {

         d_MultiplayerCampaignOrder.msg_type = MP_MSG_CampaignOrder;
         d_MultiplayerCampaignOrder.activation_time = activationTime;
         d_MultiplayerCampaignOrder.data_length = data_length;

         // message size is the base size, plus the size of the actual data
         unsigned int msg_size = 16 + data_length;

         // send to remote
         g_directPlay->sendMessage(
            (LPVOID) &d_MultiplayerCampaignOrder,
            msg_size
         );
      }
   }

   // for single-player activation time is ALWAYS zero
   else activationTime = 0;

   // send message internally
   GDespatcher::sendMessage(msg, activationTime);

}



void Despatcher::doSlaveSend(DespatchMessage * msg, ULONG activationTime) {

   // send message internally
   GDespatcher::sendMessage(msg, activationTime);
}



void Despatcher::clear()
{
    delete s_instance;
    s_instance = 0;
}

/*
 * Action a message
 */

void Despatcher::actionMessage(GDespatchMessage* msg)
{
   ASSERT(msg != 0);

#ifdef DEBUG
   debugLog("Despatcher::actionMessage(%s)\n", (const char*) msg->getName().c_str());
#endif

   DespatchMessage* cmsg = static_cast<DespatchMessage*>(msg);

   cmsg->process(d_campData);
}


/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.2  2001/06/18 08:25:44  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
