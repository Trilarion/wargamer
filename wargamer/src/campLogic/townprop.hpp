/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef TOWNPROP_HPP
#define TOWNPROP_HPP

#ifndef __cplusplus
#error townprop.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1995, Steven Morle-Green, All Rights Reserved
 *----------------------------------------------------------------------
 *
 *	Process Towns (Resources)
 *
 * Stand alone functions to process towns combined into
 * a handy class to act as a namespace.
 *
 *----------------------------------------------------------------------
 */

#include "clogic_dll.h"
#include "gamedefs.hpp"
#include "measure.hpp"
#include "cpdef.hpp"

class CampaignData;
class CampaignLogicOwner;
struct Town_COrder;
class BuildItem;

class TownProcess {
 public:
#if !defined(EDITOR)
 	/*
	 * Process Functions
	 */

	CLOGIC_DLL static void weeklyResources(CampaignLogicOwner* campGame, CampaignData* data);
		// Process weekly resources for all towns

	static void dailyTownOrders(CampaignLogicOwner* campGame);
		// Process Daily Town orders

	static Boolean processTownOrder(CampaignLogicOwner* campGame, ICommandPosition cpi, Town_COrder* to, TimeTick theTime);
		// Process a town order
#endif  // !defined(EDITOR)

	/*
	 * Information Functions
	 */

	static void resourcesWeeklyRate(const CampaignData* campData, IProvince iProv, ResourceSet& totalResources);
		// return Resources

	static void resourcesWeeklyNeed(const CampaignData* campData, IProvince iProv, ResourceSet& needResources);
		// return resources needed

	CLOGIC_DLL static void resourcesWeeklyNeed(const CampaignData* campData, const BuildItem* builds, ResourceSet& needResources);
		// return resources needed

	CLOGIC_DLL static void resourcesAvailable(const CampaignData* campData, IProvince iProv, ResourceSet& totalResources, ResourceSet& usedResources);
		// return resources available

    CLOGIC_DLL static void getTotalResources(const CampaignData* campData, IProvince iprov, ResourceSet& result);
    CLOGIC_DLL static void getAvailableResources(const CampaignData* campData, IProvince iprov, ResourceSet& result);

};




#endif /* TOWNPROP_HPP */

