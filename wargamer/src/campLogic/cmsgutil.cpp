/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 * Utility Class for converting messages into readable text
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "cmsgutil.hpp"
#include "campmsg.hpp"
#include "campdint.hpp"
#include "simpstr.hpp"
#include "unittype.hpp"
#include "compos.hpp"
#include "wmisc.hpp"

/*
 * Private Function to do the actual formatting
 */


static void formatMessage(SimpleString& str, const CampaignMessageInfo& msg, const char* fmt, const CampaignData* campData)
{
   str.allocSize(128);        // Let it allocate in larger sized chunks

   const char StartKey = '{';
   const char FinishKey = '}';

   const char* src = fmt;
   while(*src)
   {
      char c = *src++;

      if(c == StartKey)
      {
         char c1 = *src++;
         ASSERT(c1 != 0);
         char c2 = *src++;
         ASSERT(c2 != 0);

         c1 = (char) toupper(c1);
         c2 = (char) toupper(c2);

         /*
          * Brute force comparison!
          */

         if( (c1 == 'U') && (c2 == 'N') )
         {
            // Unit

            ICommandPosition cpi = msg.cp();
            ASSERT(cpi != NoCommandPosition);
            str += campData->getUnitName(cpi);
         }
         else if(c1 == 'T' && (c2 == 'A'))
         {
            // Target

            ConstICommandPosition cpi = msg.cpTarget();
            ASSERT(cpi != NoCommandPosition);
            str += campData->getUnitName(cpi);
         }
         else if(c1 == 'T' && (c2 == 'O'))
         {
            // Town

            ITown town = msg.where();
            ASSERT(town != NoTown);
            str += campData->getTownName(town);
         }
         else if(c1 == 'T' && (c2 == 'Y'))
         {
            // Type
            UnitType what = msg.what();
            ASSERT(what != NoUnitType);
            str += campData->getUnitType(what).getName();
         }
         else if(c1 == 'L' && (c2 == 'E'))
         {
            // Leader

            ILeader ileader = msg.leader();
            ASSERT(ileader != NoLeader);
            const Leader* leader = campData->getLeader(ileader);
            ASSERT(leader != 0);
            str += leader->getName();
         }
         else if(c1 == 'S' && (c2 == '1'))
         {
            // String1

            const char* s = msg.s1();
            ASSERT(s != 0);
            str += s;
         }
         else if(c1 == 'S' && (c2 == '2'))
         {
            // String2

            const char* s = msg.s2();
            ASSERT(s != 0);
            str += s;
         }
         else if(c1 == 'N' && (c2 == '1'))
         {
            // Number 1

            int n = msg.value1();
            char buffer[sizeof(int) * 3 + 2];      // This should be big enough
            _itoa(n, buffer, 10);
            str += buffer;
         }
         else
         {
            FORCEASSERT("Illegal formatting code in string");
            throw GeneralError("Illegal formatting code in campaign string %s", fmt);
         }

         /*
          * Skip to end of '}'
          */

         while(c2 != FinishKey)
         {
            c2 = *src++;
            ASSERT(c2 != 0);
            if(c2 == 0)
               throw GeneralError("Missing '}' in campaign string %s", fmt);
         }

      }
      else
      {
         ASSERT ((c != '%') || (*src != 's'));     // String probably contains C formatting

         str += c;
      }
   }
}



/*
 * exposed Function
 */


void CampaignMessageFormat::messageToText(SimpleString& s, const CampaignMessageInfo& msg, const CampaignData* campData)
{
   ASSERT(msg.id() < CampaignMessages::CMSG_HowMany);

   const char* text = msg.getText();

   if(text == 0)
   {
      int resID = msg.getResID();
      ResString msgText(resID);
      formatMessage(s, msg, msgText.c_str(), campData);
   }
   else
      formatMessage(s, msg, text, campData);
}


/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.1  2001/07/02 15:23:16  greenius
 * Battle, Campaign, Combined game and Campaign Editor now compile and run... mostly
 *
 * Revision 1.2  2001/06/18 08:25:44  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
