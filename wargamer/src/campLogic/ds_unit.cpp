/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1995, Steven Morle-Green, All Rights Reserved
 *----------------------------------------------------------------------
 *
 * Process Unit Order Despatcher Messages
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "ds_unit.hpp"





DS_UnitOrder::DS_UnitOrder(ICommandPosition unit, const CampaignOrder* order) :
   d_order(*order),
   d_unit(unit)
{
}

void DS_UnitOrder::process(CampaignData* campData)
{
#ifdef DEBUG
   debugLog("procUnitOrder(id=%d, order=%s)\n",
      static_cast<int>(d_unit->getSelf()),
      static_cast<const char*>(CampaignOrderUtil::getDescription(campData, &d_order, OD_BRIEF).toStr()));
#endif

   OrderListUtil::sendOrder(&d_order, d_unit, campData);
}

void sendUnitOrder(ConstParamCP cpi, const OrderBase* order)
{
   ASSERT(cpi->isLower(Rank_President));
#ifdef DEBUG
   debugLog("sendUnitOrder(%s, order=%s)\n",
      (const char*) cpi->getName(),
      static_cast<const char*>(CampaignOrderUtil::getDescription(campaignData, order, OD_BRIEF).toStr()));
      // (const char*) order->getDescription(OD_BRIEF));
#endif
   DS_UnitOrder* msg = new DS_UnitOrder(constCPtoCP(cpi), order);
   ASSERT(msg != 0);
   Despatcher::sendMessage(msg);
}


/*
Pack a multiplayer message into buffer
NOTE : buffer points to MP_MSG_CAMPAIGNORDER
*/
int DS_UnitOrder::pack(void * buffer, void * gamedata) {

   // cast to campaign data
   CampaignData * campData = (CampaignData *) gamedata;

   MP_MSG_CAMPAIGNORDER * msg = (MP_MSG_CAMPAIGNORDER *) buffer;

   // order type
   msg->order_type = (unsigned int) CMP_MSG_UNIT;

   int * data_buffer = (int *) msg->data;

   *data_buffer = d_unit->getSelf();
   data_buffer ++;

   // value is returned as bytes
   int len = d_order.pack(data_buffer, campData->getArmies().ob());

   return len+4;
}

/*
Unpack a multiplayer message from buffer
NOTE : buffer points to MP_MSG_CAMPAIGNORDER
*/
void DS_UnitOrder::unpack(void * buffer, void * gamedata) {

   // cast to campaign data
   CampaignData * campData = (CampaignData *) gamedata;

   MP_MSG_CAMPAIGNORDER * msg = (MP_MSG_CAMPAIGNORDER *) buffer;

   int * data_buffer = (int *) msg->data;

   d_unit = CampaignOBUtil::cpIndexToCPI(*data_buffer, *campData->getArmies().ob() );
   data_buffer++;

   d_order.unpack(data_buffer, campData->getArmies().ob() );

}



#ifdef DEBUG

String DS_UnitOrder::getName()
{
   String s;

   s = "Unit Order (";

   s += CampaignOrderUtil::getDescription(campaignData, &d_order, OD_BRIEF).toStr();

   s += ")";

   return s;
}

#endif


/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.2  2001/06/18 08:25:44  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
