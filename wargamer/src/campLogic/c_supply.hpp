/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef C_SUPPLY_HPP
#define C_SUPPLY_HPP

#ifndef __cplusplus
#error c_supply.hpp is foruse	with C++
#endif

/*
 *----------------------------------------------------------------------
 *	$Id$
 *----------------------------------------------------------------------
 *	Copyright (C) 1996, Steven	Morle-Green, All Rights	Reserved
 *	Parts	of	this code may have been	written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *	Campaign	Supply Logic
 *
 *----------------------------------------------------------------------
 */

#include	"clogic_dll.h"
#include	"gamedefs.hpp"
#include	"cpdef.hpp"

class	CampaignLogicOwner;
class	CampaignData;
class	RouteList;

/*
 *	Global Functions
 */

class	CampaignSupply	{
	public:
		CLOGIC_DLL static	void weeklySupply(CampaignLogicOwner* campGame);
		CLOGIC_DLL static	int findSupplyLine(const CampaignData*	campData, ParamCP cpi, const	SPCount wantToDraw);
		CLOGIC_DLL static	int findSupplyLine(const CampaignData*	campData, ITown iTown, SPCount wantToDraw, RouteList&	bestList);
};

#endif /* C_SUPPLY_HPP */


