/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef DEFECT_HPP
#define DEFECT_HPP

/*
 * Minor-Allies Defection routines
 */

#include "gamedefs.hpp"
class CampaignLogicOwner;

class NationDefectProc {
public:
  static void process(CampaignLogicOwner* campGame);
  static void nationDefects(CampaignLogicOwner* campGame, Nationality n);
};


#endif
