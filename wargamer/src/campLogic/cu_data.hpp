/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef CU_DATA_HPP
#define CU_DATA_HPP

#ifndef __cplusplus
#error cu_data.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *	Campaign Unit Movement Data
 *
 *----------------------------------------------------------------------
 */

#include "gamedefs.hpp"
#include "cc_close.hpp"
#include "measure.hpp"

class CampaignData;
class CampaignLogicOwner;
class Armies;

/*
 * Implemented as a struct so that all of combat can make 
 * quick efficient use of the data.
 */

struct CampUnitMoveData {
	CampaignData* d_campData;
	CampaignLogicOwner* d_campaignGame;
	Armies* d_armies;

	TimeTick		d_ticks;				// Ticks to process this turn
	CloseUnits	d_closeUnits;		// List of units close to current unit
   CloseUnits  d_closeFriendlyUnits;
	Boolean		d_nightTime;

 private:
	// Prevent Copy Constructor
	CampUnitMoveData(const CampUnitMoveData& cuMove);
	const CampUnitMoveData& operator = (const CampUnitMoveData& cuMove);

 public:
 	CampUnitMoveData(CampaignLogicOwner* campGame);
		// Constructor
	~CampUnitMoveData() { }
		// Destructor (does nothing)
};


#endif /* CU_DATA_HPP */

