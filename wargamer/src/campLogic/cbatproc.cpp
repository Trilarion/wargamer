/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 * Process Campaign Battles
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "cbatproc.hpp"
#include "cbattle.hpp"
#include "cbatmsg.hpp"
#include "campdint.hpp"
#include "armies.hpp"
#include "losses.hpp"
#include "random.hpp"
#include "town.hpp"
#include "campctrl.hpp"    // for sendPlayerMessage
#include "cu_data.hpp"
#include "campmsg.hpp"
#include "recover.hpp"
#include "cu_mode.hpp"
#include "terrain.hpp"
#include "weather.hpp"
#include "options.hpp"
#include "scenario.hpp"
#include "tables.hpp"
#include "fsalloc.hpp"
#include "wg_rand.hpp"
#include "armyutil.hpp"
#include "route.hpp"
#include "control.hpp"
#include "cc_close.hpp"
#include "cc_util.hpp"
#ifdef DEBUG
// #include <string.hpp>
 #include "clog.hpp"
 #include "logwin.hpp"
 static LogFileFlush cbLog("cbattle.log");
 extern LogFile lossLog;
#endif

/*
 * utility classes
 */

class ForceSize {
   public:
   enum Value
   {
      Small,
      Medium,
      Large,
      VeryLarge,

      HowMany
   };
};

class Divisors {
   public:
   enum Value
   {
      IneffectiveUnit,

      HowMany
   };
};

class CombatTableUtil {
   public:
   static const UBYTE getDivisor(Divisors::Value what);
   static const ForceSize::Value getForceSize(SPCount count);
};


const ForceSize::Value CombatTableUtil::getForceSize(SPCount count)
{
   ForceSize::Value size = ForceSize::Small;

   const Table1D<int>& table = scenario->getForceSizeTable();

   for(int i = 0; i < table.getWidth(); i++)
   {
      if(count <= table.getValue(i))
         return size;

      INCREMENT(size);
   }

   return size;
}

/*
 * Misc. divisors
 */

const UBYTE CombatTableUtil::getDivisor(Divisors::Value what)
{
   ASSERT(what < Divisors::HowMany);

   const Table1D<UBYTE>& table = scenario->getCombatDivisorTable();
   ASSERT(table.getWidth() == Divisors::HowMany);

   return table.getValue(what);
}


/*------------------------------------------------------------------
 * Battle Day
 * There are 4 rounds of battle fought each day.
 * Currently, Ben wants 4 rounds regardless of season (I believe)
 * As of now they are fought at 0900, 1200, 1500, and 1800 hrs
 */

class BattleDay {
   public:
   enum Value
   {
      Night,
      Morning,
      LateMorning,
      Noon,
      Afternoon,
      Evening,

      BattleDay_HowMany

   };
};


/*
 * Namespace for local functions
 */


/*----------------------------------------------------------------------
 *  IneffectiveUnit is unit that fights at only half value
 *  i.e if its parent leader is unable to effectively control it
 */

class IneffectiveUnit : public SLink {
   ICommandPosition d_cpi;
   public:
   IneffectiveUnit(ICommandPosition cpi) :
   d_cpi(cpi)
   {
   }

   ICommandPosition unit() const
   { return d_cpi;
   }

   void* operator new(size_t size);
#ifdef _MSC_VER
   void operator delete(void* deadObject);
#else
   void operator delete(void* deadObject, size_t size);
#endif
   enum { ChunkSize = 20 };
};

#ifdef DEBUG
static FixedSizeAlloc<IneffectiveUnit> ineffectiveUnitItemAlloc("IneffectiveUnit");
#else
static FixedSizeAlloc<IneffectiveUnit> ineffectiveUnitItemAlloc;
#endif

void* IneffectiveUnit::operator new(size_t size)
{
   return ineffectiveUnitItemAlloc.alloc(size);
}

#ifdef _MSC_VER
void IneffectiveUnit::operator delete(void* deadObject)
{
   ineffectiveUnitItemAlloc.release(deadObject);
}
#else
void IneffectiveUnit::operator delete(void* deadObject, size_t size)
{
   ineffectiveUnitItemAlloc.release(deadObject, size);
}
#endif

class IneffectiveUnitList : public SList<IneffectiveUnit> {
   public:
   Boolean hasUnit(ICommandPosition cpi);
};

Boolean IneffectiveUnitList::hasUnit(ICommandPosition cpi)
{
   SListIter<IneffectiveUnit> iter(this);
   while(++iter)
   {
      IneffectiveUnit* item = iter.current();

      if(item->unit() == cpi)
         return True;
   }

   return False;
}


/*-------------------- CampaignBattleUnitListUtil ------------------------------
 *  Utility for setting CampaignBattleUnitList values
 */

class CampaignBattleUnitListUtil {
   public:
   
      struct Data
      {
         CampaignData* d_campData;
         CampaignBattleUnit* d_unit;
         IneffectiveUnitList d_ineffectiveUnits;
         UBYTE d_battleRound;

         Data() :
         d_campData(0),
         d_unit(0),
         d_battleRound(0)
         {
         }

         void set(CampaignData* campData, CampaignBattleUnit* bu, UBYTE battleRound)
         {
            d_campData = campData;
            d_unit = bu;
            d_battleRound = battleRound;
            d_ineffectiveUnits.reset();
         }
      };

      static SPCount getUnitSPValue(Data& data);
      static void getIneffectiveUnits(Data& data);
};

/*
 * Get Unit SPValue
 *
 * This is not as simple as it seems. First, We factor in terrain,
 * then if round 1 or 2 of battle we have to factor in for effective
 * unit organization.
 *
 */

SPCount CampaignBattleUnitListUtil::getUnitSPValue(CampaignBattleUnitListUtil::Data& data)
{
   ASSERT(data.d_campData != 0);
   ASSERT(data.d_unit != 0);

   UWORD spValue = 0;

   UnitIter uiter(&data.d_campData->getArmies(), data.d_unit->getUnit());

   while(uiter.next())
   {
      CommandPosition* cp = uiter.currentCommand();
      UWORD uValue = 0;

      /*
      * Multiplier table
      */

      enum EffectiveUnitModifier
      {
         Terrain,
         Rain,
         AgainstRetreating,
         RR_Round1,
         RR_Round2,
         IneffectiveUnit,
         EUC_HowMany
      };

      // get table
      const Table1D<UWORD>& ucmTable = scenario->getUnitCountModifierTable();
      ASSERT(ucmTable.getWidth() == EUC_HowMany);
      ASSERT(ucmTable.getResolution() != 0);

      StrengthPointIter spIter(&data.d_campData->getArmies(), uiter.current());
      while(++spIter)
      {
         const StrengthPointItem* sp = spIter.current();

         CommandPosition* cpParent = data.d_campData->getCommand(data.d_unit->getUnit());

         Boolean enemyRetreating = data.d_unit->againstRetreating();
         Boolean hasRestRally = (cpParent->getCurrentOrder()->getType() == Orders::Type::RestRally);

         const UnitTypeItem& uti = data.d_campData->getUnitType(sp->getUnitType());

         UWORD value = uti.getEffectiveValue();

         /*
         * if unit type is Cavalry modify value for terrain, weather, etc.
         */

         if(uti.getBasicType() == BasicUnitType::Cavalry)
         {
            // if unit is in Wooded, Wooded-Hilly, or Marshy terrain value is halved
            const Town& t = data.d_campData->getTown(cp->getCloseTown());
            const TerrainTypeItem& item = data.d_campData->getTerrainType(t.getTerrain());

            if(item.getGroundType() == Terrain::Wooded ||
               item.getGroundType() == Terrain::WoodedHilly ||
               item.getGroundType() == Terrain::Marsh)
            {
               value = MulDiv(value, ucmTable.getValue(Terrain), ucmTable.getResolution());
            }

            // if weather is heavy rain value is multiplied by 1.5
            if(data.d_campData->getWeather().getWeather() == CampaignWeather::HeavyRain)
               value = MulDiv(value, ucmTable.getValue(Rain), ucmTable.getResolution());

            // if enemy is in retreat status then value is tripled
            if(enemyRetreating)
               value = MulDiv(value, ucmTable.getValue(AgainstRetreating), ucmTable.getResolution());
         }

         /*
         * if we have rest rally orders and we are in round 1 then value is halved
         * if we are in round 2 then value is * .75
         */

         if(hasRestRally)
         {
            value = (data.d_battleRound <= 1) ? MulDiv(value, ucmTable.getValue(RR_Round1), ucmTable.getResolution()) :
            MulDiv(value, ucmTable.getValue(RR_Round2), ucmTable.getResolution());

         }

         uValue += value;

      }


      /*
      * If battleRound 1 or 2 then check for Ineffecient organizations
      * Value is halved for each level of command that is Inefficiently organized
      */

      if(data.d_battleRound <= 2)
      {
         ICommandPosition cpi = uiter.current();
         CommandPosition* cp = uiter.currentCommand();
         while(cpi->isLower(Rank_President))
         {
            if(data.d_ineffectiveUnits.hasUnit(cpi))
            {
               uValue = MulDiv(uValue, ucmTable.getValue(IneffectiveUnit), ucmTable.getResolution());
            }

            cpi = cp->getParent();
            cp = data.d_campData->getCommand(cpi);
         }
      }

      spValue += uValue;
   }

   // round out to nearest int
   return static_cast<SPCount>((spValue + 4)/10);
}

/*
 * Ineffective units are units a leader cannot effectively control
 * they fight at only half value
 */

void CampaignBattleUnitListUtil::getIneffectiveUnits(CampaignBattleUnitListUtil::Data& data)
{
   ASSERT(data.d_campData != 0);
   ASSERT(data.d_unit != 0);
   ASSERT(data.d_battleRound <= 2);

   //  SPCount spValue = 0;

   if(data.d_battleRound > 2 || data.d_unit->outOfIt())
      return; // spValue;

   ICommandPosition cpi = data.d_unit->getUnit();

   UnitIter iter(&data.d_campData->getArmies(), cpi);
   while(iter.next())
   {
      CommandPosition* cp = iter.currentCommand();

      if(cp->isHigher(Rank_Division))
      {
         Leader* leader = data.d_campData->getLeader(cp->getLeader());

         /*
         * Get number of formations leader can 'effectively' control
         * Formula is leader's initiative+staff / 100. (Rounded out to nearest)
         */

         const int divisor = CombatTableUtil::getDivisor(Divisors::IneffectiveUnit); //100;
         ASSERT(divisor > 0);
         const int roundOutValue = (divisor / 2) - 1;

         int nCanControl = ((leader->getInitiative() + leader->getStaff() + roundOutValue) / divisor) + 1;

         /*
         * See how many he actualy controls
         */

         ICommandPosition cpiChild = cp->getChild();
//         ASSERT(cpiChild != NoCommandPosition);

         int nFormations = 0;
         if(cpiChild != NoCommandPosition)
         {
            UnitIter cIter(&data.d_campData->getArmies(), cpiChild);
            while(cIter.sister())
            {
               nFormations++;
            }

            /*
            * If he controls more than he should then excess units are deemed 'ineffective'
            * and SPValues are halved
            *
            * Prioity for units to be selected as 'ineffective' are
            *  1. Unit with lowest SPValue
            *  2. Unit with lowest morale
            *  3. Unit with lowest leader initiative
            *  4. Unit with lowest leader tactical ability
            *  5. If we still haven't found one, pick it at random
            *
            */

            if(nFormations > nCanControl)
            {
               int nIneffective = nFormations - nCanControl;

               while(nIneffective--)
               {
                  ICommandPosition worstCPI = NoCommandPosition;
                  SPCount worstSPValue = UWORD_MAX;
                  Attribute worstMorale = Attribute_Range;
                  Attribute worstInitiative = Attribute_Range;
                  Attribute worstTacticalAbility = Attribute_Range;

                  cIter.reset(cpiChild);
                  while(cIter.sister())
                  {
                     if(!data.d_ineffectiveUnits.hasUnit(cIter.current()))
                     {
                        CommandPosition* cp = cIter.currentCommand();
                        Leader* leader = data.d_campData->getLeader(cp->getLeader());
                        bool add = false;
                        SPValue value = data.d_campData->getArmies().getUnitSPValue(cIter.current());

                        if(worstCPI == NoCommandPosition)
                        {
                           add = True;
                        }
                        else if(value <= worstSPValue)
                        {
                           if(value < worstSPValue)
                              add = True;
                           else if(cp->getMorale() <= worstMorale)
                           {
                              if(cp->getMorale() < worstMorale)
                                 add = True;
                              else if(leader->getInitiative() <= worstInitiative)
                              {
                                 if(leader->getInitiative() < worstInitiative)
                                    add = True;
                                 else if(leader->getTactical() <= worstTacticalAbility)
                                 {
                                    add = (leader->getTactical() < worstTacticalAbility) ||
                                          (CRandom::get(2) != 0);
#if 0 // simplify expression
                                    if(leader->getTactical() < worstTacticalAbility)
                                       add = True;

                                    else
                                       add = static_cast<Boolean>(CRandom::get(2));
#endif
                                 }
                              }
                           }
                        }

                        if(add)
                        {
                           worstCPI = cIter.current();
                           worstSPValue = value;
                           worstMorale = cp->getMorale();
                           worstInitiative = leader->getInitiative();
                           worstTacticalAbility = leader->getTactical();
                        }
                     }
                  }
                  ASSERT(worstCPI != NoCommandPosition);

                  IneffectiveUnit* ieUnit = new IneffectiveUnit(worstCPI);
                  ASSERT(ieUnit != 0);

#ifdef DEBUG
                  cbLog.printf("%s(SPValue = %d) is ineffective",
                     (const char*)data.d_campData->getUnitName(worstCPI).toStr(),
                     static_cast<int>(worstSPValue));
#endif
                  data.d_ineffectiveUnits.append(ieUnit);
               }
            }
         }
      }
   }
}



class CampaignBattleUtil {
   // Store some useful values

   CampaignLogicOwner* d_campGame;
   CampaignData* d_campData;
   Armies* d_armies;
   // CampaignBattle* d_battle;
   CampaignBattleIter d_battle;

   BattleDay::Value d_timeOfDay;

   public:

   enum RoundOutcome
   { WonThisRound, LostThisRound, DrawThisRound
   };

   CampaignBattleUtil(CampaignLogicOwner* campGame, CampaignBattle* battle, BattleDay::Value timeOfDay);
   CampaignBattleUtil(CampaignData* campData, CampaignBattle* battle);
   CampaignBattleUtil(CampaignLogicOwner* campGame, CampaignBattleIter battle, BattleDay::Value timeOfDay);
   CampaignBattleUtil(CampaignData* campData, CampaignBattleIter battle);


   void startBattle();
   // sets up initial values for a battle

   void deployForces();
   // checks to see if deployment time has been reached

   void calculateResults();
   // do actual battle calculations

   Boolean calcBreakOutBattle(Side sideBreakingOut);
   // calculate a breakout battle (note: this is a special case battle)

   void doStoppedForNight();
   // do stopped for the evening routine.
   // Includes morale recovery, check for critical loss, etc.

   void doAfterBattle();
   // called after fighting has stopped
   // increments battle round while victor is still reforming
   // once reformed, it call doBattleOver()

   void doReinforced(const ICommandPosition& cpi);
   // reinforcements have arrived, recalucates average morale

   private:
   void initStartingPosture();
   // initialize posture

   void initSupply();
   // initialize supply values

   void initStartingMorale();
   // initialize starting morale for both sides

   void initStartingMorale(Side side);
   // initialize starting morale for side

   void setCampaignBattleUnitFlags();

   void setSpecialistFlags();

   void setFatigue();

   void setSPValues();
   // set SP Values for both sides

   void setUnitListFlags();
   // set VP location flags for each side

   void closeUpColumns();
   // temporary function to close up a columns tail while deploying

   void setBombValues();
   // set Bombardment values for both sides

   void setNonArtySPCounts();
   // set Non-Artillery SP counts for both sides

   Boolean checkForWithdraw();
   // see if one side or the other has elected to withdraw

   Boolean lostContact(Side ourSide);

   Boolean checkForVictor();
   // see if battle has been won

   void doBrushAside();

   void testForSortie();

   void doBombardment();
   // do bombarment calculations

   void doCasualty();
   // do casualty calcualations

   void doMorale();
   // do morale-loss calculations

   void doEndOfFighting();
   // processes end of fighting

   void doLostBattle(Side side);
   // processes lossing side

   void doWonBattle(Side side);
   // sets reconcentration time for winning side

   void doWithdrawing(Side side);
   // processes withdrawing side

   void doTacticalPursuit();
   // added loss calculations for the loser

   void doStrategicPursuit();
   // send units with pursuit orders pursuing

   void doBattleOver();
   // battle is actually over. processes winning side

   void setValues();
   // calls setSPValue(), setBombValues(), and setNonArtySPCount()

   Boolean  stillInBattle(Side side);
   // does this side have units still in battle

   Boolean implementLosses();
   // actually apply losses

   void spPromotionCheck(const ICommandPosition& cpi, Boolean victor);
   // test to see if any SP's are promoted due to battle experience
   // called by both victor and defeated

   void leaderImprovesCheck(const ICommandPosition& cpi);
   // test to see if Winner's CinC improves due to experience
   // called by victor only

   void doVictoriousUnit(const ICommandPosition& cpi);
   // set final morale, mode, etc. for winning unit

   Attribute calcVictorMorale();
   // calculate final morale for victor

   void checkForLeaderLoss();
   // see if leaders are hit. called at the end of each battle round

   void checkForEquipmentLoss(const ICommandPosition& cpi);
   // see if retreating unit involuntarily losses its heave equipment
   // (i.e. siege-trains, depots, engineers)

   RoundOutcome roundOutcome(Side side);
   // won, lost, or draw on the last round

   void recoverMorale();

   void applySideVP();

   void updatePosture();

   TimeTick timeToReconcentrate(Side s, SPCount spCount);

   void setSideNationality();

   static Side getOtherSide(Side side)
   {
      ASSERT(side == 0 || side == 1);
      return (side == 0) ? 1 : 0;
   }

   void showBattleResults(Side s);

#if 0
      Boolean atChokePoint(Side side);
   Boolean defendingChokePoint(Side side);
   Boolean haveEngineers(Side side);
#endif
};

// CampaignBattleUtil::CampaignBattleUtil(CampaignLogicOwner* campGame, CampaignBattle* battle, BattleDay::Value t) :
CampaignBattleUtil::CampaignBattleUtil(CampaignLogicOwner* campGame, CampaignBattleIter battle, BattleDay::Value t) :
    d_campGame(campGame),
   d_campData(campGame->campaignData()),
   d_armies(&(d_campData->getArmies())),
   d_battle(battle),
   d_timeOfDay(t)
{
}

// CampaignBattleUtil::CampaignBattleUtil(CampaignData* campData, CampaignBattle* battle) :
CampaignBattleUtil::CampaignBattleUtil(CampaignData* campData, CampaignBattleIter battle) :
   d_campGame(0),
   d_campData(campData),
   d_armies(&(d_campData->getArmies())),
   d_battle(battle),
   d_timeOfDay(BattleDay::Morning)
{
}

CampaignBattleUtil::CampaignBattleUtil(CampaignLogicOwner* campGame, CampaignBattle* battle, BattleDay::Value t) :
   d_campGame(campGame),
   d_campData(campGame->campaignData()),
   d_armies(&(d_campData->getArmies())),
   d_battle(&(campGame->campaignData()->getBattleList()), battle),
   d_timeOfDay(t)
{
}

CampaignBattleUtil::CampaignBattleUtil(CampaignData* campData, CampaignBattle* battle) :
   d_campGame(0),
   d_campData(campData),
   d_armies(&(d_campData->getArmies())),
    d_battle(&(campData->getBattleList()), battle),
   d_timeOfDay(BattleDay::Morning)
{
}



void CampaignBattleUtil::startBattle()
{
   const Side side0 = 0;
   const Side side1 = 1;

   {
      CampaignBattlePtr battlePtr = d_battle.current();      // sets write lock

      battlePtr->daysOfBattle(1);
      battlePtr->turnMode(CampaignBattle::BattleTurnMode::Deploying);
   }

#ifdef DEBUG
   cbLog.printf("Starting Battle -- %s vs %s",
      d_battle->cinc(0)->getNameNotNull(),
      d_battle->cinc(1)->getNameNotNull());

   Town& t = d_campData->getTown(d_battle->getCloseTown());
   const TerrainTypeItem& terrain = d_campData->getTerrainType(t.getTerrain());

   cbLog.printf("Terrain: %s, %s",
      Terrain::groundTypeName(terrain.getGroundType()),
      Terrain::riverTypeName(terrain.getRiverType()));
#endif

   initStartingPosture();
   initSupply();
   initStartingMorale();
   setUnitListFlags();
   setValues();

   // some useful values
   SPValue side0Value = d_battle->spValue(side0);
   SPValue side1Value = d_battle->spValue(side1);
   SPCount side0Count = d_battle->spCount(side0);
   SPCount side1Count = d_battle->spCount(side1);
   Side sSide = (side0Value > side1Value) ? side0 : side1;    // stronger side
   Side wSide = (sSide == side1) ? side0 : side1;             // weaker side

   /*
   * see if this is a brush aside battle
   * its a brush aside if ratio is at least 6 to 1,
   * the stronger side is moving,
   * the stronger side has an offensive posture or we're trying to break-out,
   * and weaker side is not in a fortress or chokepoint
   */

   if((d_battle->ratio() >= CampaignBattle::Ratio_6To1) &&
      (d_battle->isMoving(sSide)) &&
      (d_battle->posture(sSide) == Orders::Posture::Offensive || d_battle->breakingOut(sSide)) &&
      !d_battle->defendingChokePoint(wSide))
   {
      CampaignBattlePtr battlePtr = d_battle.current();      // sets write lock
      battlePtr->brushAside(True);
#ifdef DEBUG
      cbLog.printf("===========Brush aside Battle==============");
#endif
   }

   /*
   * See if this is a relief battle
   */

   if(!d_battle->brushAside())
   {
      for(Side s = 0; s < scenario->getNumSides(); s++)
      {
         CampaignBattlePtr battlePtr = d_battle.current();      // sets write lock
         CampaignBattleUnitListIterW iter(&battlePtr->units(s));
         while(++iter)
         {
            ICommandPosition cpi = iter.current()->getUnit();
            if(cpi->atTown())
            {
               const Town& t = d_campData->getTown(cpi->getTown());
               if(t.getSide() != cpi->getSide() && t.isGarrisoned())
               {
                  CampaignBattlePtr battlePtr = d_battle.current();      // sets write lock
                  battlePtr->siegeTown(cpi->getTown());
                  break;
               }
            }
         }
      }
   }

   /*
   * Set deployment time
   */

   int hours = 0;
   if(!d_battle->brushAside())
   {
      const Table1D<UBYTE>& table = scenario->getDeploymentTimeTable();

      // use largest side to calculate time
      SPCount count = CombatTableUtil::getForceSize((side0Value > side1Value) ? side0Count : side1Count);

      hours = table.getValue(count);
   }

   CampaignBattlePtr battlePtr = d_battle.current();      // sets write lock
   battlePtr->tillWhen(d_campData->getTick() + HoursToTicks(hours));

#ifdef DEBUG
   cbLog.printf("Time to deploy is %d hours", hours);
   cbLog.printf("Forces are in contact at: %s", d_campData->asciiTime());
#endif
}

void CampaignBattleUtil::deployForces()
{
   if(d_battle->timeElapsed(d_campData->getTick()))
   {
      d_battle.current()->turnMode(CampaignBattle::BattleTurnMode::Fighting);

#ifdef DEBUG
      cbLog.printf("Battle started at: %s", d_campData->asciiTime());
#endif
   }
   else
   {
      closeUpColumns();
   }
}

/*
 * Calculate one turn of battle casualties
 */

void CampaignBattleUtil::calculateResults()
{
   ASSERT(d_timeOfDay != BattleDay::Night);

#ifdef DEBUG
   String locName;
   d_campData->getCampaignPositionString(&d_battle->getPosition(), locName);
   lossLog.printf("==================================");
   lossLog.printf("Battle Losses at %s", locName.c_str());

   static const char* s_dayTimeText[] = {
      "Night",
      "Morning",
      "Late Morning",
      "Noon",
      "Afternoon",
      "Evening"
   };

   cbLog.printf("Time of Day = %s, battle round %d, Days of Battle = %d",
      s_dayTimeText[d_timeOfDay],
      static_cast<int>(d_battle->battleRound()),
      static_cast<int>(d_battle->daysOfBattle()));
#endif

   /*
   * Make sure both sides still have units in the battle
   */

   Boolean battleOver = False;
   for(Side s = 0; s < scenario->getNumSides(); s++)
   {
      if(!stillInBattle(s))
      {
#ifdef DEBUG
         cbLog.printf("%s is no longer in the battle", scenario->getSideName(s));
#endif


         Side otherSide = getOtherSide(s);
         if(stillInBattle(otherSide))
         {
            {
               CampaignBattlePtr battlePtr = d_battle.current();
               battlePtr->victor(otherSide);
            }
            doEndOfFighting();
         }
         else
         {
#ifdef DEBUG
            cbLog.printf("%s is no longer in the battle", scenario->getSideName(otherSide));
#endif
            {
               CampaignBattlePtr battlePtr = d_battle.current();
               battlePtr->victor(SIDE_Neutral);
               battlePtr->turnMode(CampaignBattle::BattleTurnMode::BattleOver);
            }

            // Added SWG: 30Aug99
            showBattleResults(GamePlayerControl::getSide(GamePlayerControl::Player));
         }

         battleOver = True;
      }
   }

   if(battleOver)
      ; // do nothing

   /*
   * If we're brushing aside, we calculate results immedialty
   */

   else if(d_battle->brushAside())
   {
      doBrushAside();

      // we still get to take some swipes at them
      //   doBombardment();
      doCasualty();
      implementLosses();
      doEndOfFighting();
   }

   // if its morning we just do the early morning routines
   else if(d_timeOfDay == BattleDay::Morning)
   {
      if(d_battle->isStoppedForNight())
      {
         {
            CampaignBattlePtr battlePtr = d_battle.current();      // sets write lock
            battlePtr->incrementDaysOfBattle();
         }

         // see if one side or the other has elected to withdraw
         Boolean stillFighting = (!d_battle->hasVictor() && !checkForWithdraw());

         if(!stillFighting)
            doEndOfFighting();
         else
            recoverMorale();

         d_battle.current()->setStoppedForNight(False);
      }
   }
   // don't calc battle if battleRound == 0, just increment battleRound.
   // Results don't get calculated until the end of a round
   else if(d_battle->battleRound() != 0)
   {
      // set cinc
      for(Side s = 0; s < scenario->getNumSides(); s++)
         d_battle.current()->setCinC(s);

      // see if we have a sieged garrison that may join in
      if(d_battle->testForSortieBattle())
      {
         testForSortie();
      }

      // reset posture
      updatePosture();

      // reset all relevant values
      setValues();

      // do the battle calculations
      doBombardment();
      doCasualty();
      doMorale();

      // apply losses
      implementLosses();

      // check for hit leaders
      checkForLeaderLoss();

      // check for end of fighting if rounds 1 to 3
      // if evening doStoppedForNight handles it
      if(d_timeOfDay <= BattleDay::Afternoon)
      {
         if(checkForVictor())
            doEndOfFighting();
      }
   }

   // increment battle round
   d_battle.current()->incBattleRound();
}

Boolean CampaignBattleUtil::calcBreakOutBattle(Side sideBreakingOut)
{
#ifdef DEBUG
   cbLog.printf("\n===========Break-out Battle==============");
#endif

   initSupply();
   setUnitListFlags();
   setCampaignBattleUnitFlags();
   setSpecialistFlags();
   setFatigue();
   setSPValues();
   setSideNationality();

   //  Side side0 = 0;
   //  Side side1 = 1;

   Side otherSide = getOtherSide(sideBreakingOut);

   // some useful values
   SPValue ourValue = d_battle->spValue(sideBreakingOut);
   SPValue theirValue = d_battle->spValue(otherSide);
   SPCount ourCount = d_battle->spCount(sideBreakingOut);
   SPCount theirCount = d_battle->spCount(otherSide);

   Boolean brokeThrough = False;

   /*
   * see if unit breaks-out
   * if unit has a 6:1 or greater advantage it automatically breaks-out,
   * if unit has a 3:1 or greater advantage it tests for break-out
   * if other side is not in a fortress or chokepoint
   */


   if(ourValue > theirValue)
   {
      // if >= 6:1
      if((d_battle->ratio() >= CampaignBattle::Ratio_6To1) &&
         (!d_battle->defendingChokePoint(otherSide)))
      {
         // automatic
         brokeThrough = True;
      }

      // if >= 3:1
      else if((d_battle->ratio() >= CampaignBattle::Ratio_3To1) &&
         (!d_battle->defendingChokePoint(otherSide)))
      {
         enum BreakOutBattleValues
         {
            BaseMultiplier,
            OurInit40,           // our initiative is 40% greater than enemies initiative
            TheirInit40,
            CharismaOver180,
               AggressOver180,
               CharismaLess90,
               AggressLess90,

               BOBV_HowMany
         };

         const Table1D<UWORD>& table = scenario->getBreakOutBattleTable();
         ASSERT(table.getWidth() == BOBV_HowMany);
         ASSERT(table.getResolution() != 0);

         /*
         * Test to see if unit breaks-out
         *
         * Base chance is 16% X odds ratio
         */

         const int baseM = (table.getValue(BaseMultiplier) / table.getResolution());
         ASSERT(theirValue > 0);
         int chance = baseM * (ourValue / theirValue);

#ifdef DEBUG
         cbLog.printf("Base chance of breaking out = %d", chance);
#endif

         /*
         * Modifiers
         */

         // our cinc initiative is 40% greater than their cinc ( X 1.2)
         if(d_battle->cinc(sideBreakingOut)->getInitiative() >= d_battle->cinc(otherSide)->getInitiative() * 1.4)
            chance = MulDiv(chance, table.getValue(OurInit40), table.getResolution());
         // their cinc initiative is 40% greater than ourr cinc ( X .8)
         else if(d_battle->cinc(otherSide)->getInitiative() >= d_battle->cinc(sideBreakingOut)->getInitiative() * 1.4)
            chance = MulDiv(chance, table.getValue(TheirInit40), table.getResolution());

         // our cinc charisma is greater than 180 (X 1.1)
         if(d_battle->cinc(sideBreakingOut)->getCharisma() >= 180)
            chance = MulDiv(chance, table.getValue(CharismaOver180), table.getResolution());
         // our cinc charisma is less than 90 (X .8)
         else if(d_battle->cinc(sideBreakingOut)->getCharisma() < 90)
            chance = MulDiv(chance, table.getValue(CharismaLess90), table.getResolution());


         // our cinc charisma is greater than 180 (X 1.1)
         if(d_battle->cinc(sideBreakingOut)->getAggression() >= 180)
            chance = MulDiv(chance, table.getValue(AggressOver180), table.getResolution());
         // our cinc charisma is less than 90 (X .8)
         else if(d_battle->cinc(sideBreakingOut)->getAggression() < 90)
            chance = MulDiv(chance, table.getValue(AggressLess90), table.getResolution());

         chance = maximum(0, 100);

#ifdef DEBUG
         cbLog.printf("Modified chance of breaking out = %d", chance);
#endif

         brokeThrough = (CRandom::get(100) < chance);
      }
   }

   if(brokeThrough)
   {
#ifdef DEBUG
      cbLog.printf("%s have broken through enemy", scenario->getSideName(sideBreakingOut));
#endif

      // do casualties
      doCasualty();
      implementLosses();

      // set broke-through enemy to retreat status

      CampaignBattlePtr battlePtr = d_battle.current();

      CampaignBattleUnitListIterW iter(&battlePtr->units(otherSide));
      while(++iter)
      {
         ICommandPosition cpi = iter.current()->getUnit();

         UnitIter uiter(&d_campData->getArmies(), cpi);
         while(uiter.next())
         {
            uiter.current()->setMorale(0);
            // Aggression is set to timid, and defensive posture
            uiter.current()->setAggressionValues(Orders::Aggression::Timid, Orders::Posture::Defensive);
            uiter.current()->aggressionLocked(True);
         }

         // a routing unit force marches
         cpi->setForceMarch(True);

         // reset order
         cpi->getOrders().getCurrentOrder()->makeHold();
         CP_ModeUtil::setMode(d_campData, cpi, CampaignMovement::CPM_Retreating);
      }

   }

   return brokeThrough;
}

/*
 * test to see if besieged unit joins in the battle
 */

void CampaignBattleUtil::testForSortie()
{
   ASSERT(d_battle->siegeTown() != NoTown);

   /*
   * find unit that is besieged
   */

   const Town& t =  d_campData->getTown(d_battle->siegeTown());
   ASSERT(t.isGarrisoned());

   Boolean joiningBattle = False;

#ifdef DEBUG
   Boolean found = False;
#endif

   ICommandPosition topCPI = d_campData->getArmies().getFirstUnit(t.getSide());
   UnitIter iter(&d_campData->getArmies(), topCPI);
   while(iter.sister())
   {
      ICommandPosition cpi = iter.current();

      if(cpi->isGarrison() && cpi->getTown() == d_battle->siegeTown())
      {
#ifdef DEBUG
         cbLog.printf("----Testing besieged %s for joining battle", (const char*)d_campData->getUnitName(cpi).toStr());
#endif

         ILeader leader = cpi->getLeader();
         ASSERT(leader != NoLeader);
         if(leader == NoLeader)
            continue;

         /*
         * We found besieged unit.
         *
         * Test to see if he joins battle. Test is...
         * raw chance = leader's aggression + initiative + unit morale as a % 768
         */

         const int combinedValue = leader->getAggression() + leader->getInitiative() + cpi->getMorale();
         int chance = MulDiv(combinedValue, 100, 768);

#ifdef DEBUG
         cbLog.printf("Raw chance of %s joining battle = %d",
            cpi->getNameNotNull(), chance);
#endif

         /*
         * Modifiers...
         */

         const Table1D<UWORD>& table = scenario->getSortieTestTable();
         ASSERT(table.getWidth() == BattleDay::BattleDay_HowMany);
         ASSERT(table.getResolution() != 0);

         // if noon or afternoon( X .5 )
         // if evening ( X .75 )
         chance = MulDiv(chance, table.getValue(d_timeOfDay), table.getResolution());

         chance = clipValue(chance, 0, 100);

#ifdef DEBUG
         cbLog.printf("Modified chance of joining battle = %d", chance);
#endif

         if(CRandom::get(100) <= chance)
         {
#ifdef DEBUG
            cbLog.printf("%s is going to sortie", cpi->getNameNotNull());
#endif
            /*
            * Add him to battle but keep him in Garrison mode
            */

            CampaignBattleUnit* bu = new CampaignBattleUnit(cpi);
            ASSERT(bu != 0);

            {
               CampaignBattlePtr battlePtr = d_battle.current();
               CampaignBattleUnitList& bl = battlePtr->units(cpi->getSide());
               bl.append(bu);
            }

            doReinforced(cpi);

            joiningBattle = True;
         }

#ifdef DEBUG
         found = True;
#endif
      }
   }

   if(joiningBattle)
      d_battle.current()->siegeTown(NoTown);

#ifdef DEBUG
   ASSERT(found);
#endif
}

void CampaignBattleUtil::setValues()
{
   setCampaignBattleUnitFlags();
   setSpecialistFlags();
   setFatigue();
   setSPValues();
   setBombValues();
   setSideNationality();
   d_battle.current()->resetLosses();
}

/*
 * Set battle unit flags.
 */

void CampaignBattleUtil::setCampaignBattleUnitFlags()
{
   CampaignBattlePtr battlePtr = d_battle.current();

   for(Side s = 0; s < scenario->getNumSides(); s++)
   {
      CampaignBattleUnitListIterW iter(&battlePtr->units(s));
      while(++iter)
      {
         // set against retreating flag
         CampaignBattleUnit* bu = iter.current();
         bu->againstRetreating((d_battle->currentMorale(getOtherSide(s)) == 0));
      }
   }
}

/*
 * Set specialist flags
 */

void CampaignBattleUtil::setSpecialistFlags()
{
   if(d_battle->deploying() || d_battle->fighting())
   {
      for(Side s = 0; s < scenario->getNumSides(); s++)
      {
         UWORD nArtySpecialist = 0;      // count of artillery specialist
         UWORD nCavalrySpecialist = 0;      // count of cavalery specialist

         CampaignBattleUnitListIterR iter(&d_battle->units(s));
         while(++iter)
         {
            const CampaignBattleUnit* bu = iter.current();
            if(bu->outOfIt())
               continue;

            // update specialist flags for entire organization
            d_campData->getArmies().setCommandSpecialistFlags(bu->getUnit());

            UnitIter iter(&d_campData->getArmies(), bu->getUnit());
            while(iter.next())
            {
               const Leader* leader = iter.current()->getLeader();

               if(leader->isOutOfIt())
                  ; // do nothing
               else if(leader->isSpecialistType(Specialist::Artillery))
                  nArtySpecialist++;
               else if(leader->isSpecialistType(Specialist::Cavalry))
                  nCavalrySpecialist++;
            }
         }

         /*
         * set flags
         */

         CampaignBattlePtr battlePtr = d_battle.current();

         battlePtr->nArtillerySpecialist(s, nArtySpecialist);
         battlePtr->nCavalrySpecialist(s, nCavalrySpecialist);

#ifdef DEBUG
         cbLog.printf("%s have %d Artillery Specialist, and %d Cavalry Specialist",
            scenario->getSideName(s),
            static_cast<int>(nArtySpecialist),
            static_cast<int>(nCavalrySpecialist));
#endif

      }
   }
}

/*
 * Set pre-battle posture
 */

void CampaignBattleUtil::initStartingPosture()
{
   CampaignBattlePtr battlePtr = d_battle.current();

   updatePosture();
   for(Side s = 0; s < scenario->getNumSides(); s++)
   {
      if(d_battle->posture(s) == Orders::Posture::Offensive)
      {
         battlePtr->preBattlePosture(s, (d_battle->atTown()) ? PreBattlePosture::AttackingAt :
         PreBattlePosture::AttackingNear);
      }
      else
      {
         battlePtr->preBattlePosture(s, (d_battle->atTown()) ? PreBattlePosture::DefendingAt :
         PreBattlePosture::DefendingNear);
      }
   }
}

/*
 * Determine posture
 *
 * Posture of majority of sp's indicates posture
 */

void CampaignBattleUtil::updatePosture()
{
   for(Side s = 0; s < scenario->getNumSides(); s++)
   {
#ifdef DEBUG
      cbLog.printf("---- Updating posture for %s", scenario->getSideName(s));
#endif

      SPCount defendCount = 0;
      SPCount attackCount = 0;

      CampaignBattleUnitListIterR iter(&d_battle->units(s));
      while(++iter)
      {
         const CampaignBattleUnit* bu = iter.current();

         if(bu->getUnit()->posture() == Orders::Posture::Offensive)
         {
            attackCount += d_campData->getArmies().getUnitSPCount(bu->getUnit(), True);
         }
         else
         {
            defendCount += d_campData->getArmies().getUnitSPCount(bu->getUnit(), True);
         }
      }

      Orders::Posture::Type posture = (attackCount >= defendCount) ? Orders::Posture::Offensive :
      Orders::Posture::Defensive;

      d_battle.current()->posture(s, posture);

#ifdef DEBUG
      cbLog.printf("------ Posture for %s is %s",
         scenario->getSideName(s), Orders::Posture::postureName(posture));
#endif
   }
}

/*
 * Init supply levels for each side
 */

void CampaignBattleUtil::initSupply()
{
   CampaignBattlePtr battlePtr = d_battle.current();

   for(Side s = 0; s < scenario->getNumSides(); s++)
   {
      int cSupply = 0;
      SPCount spCount = 0;

      CampaignBattleUnitList* units = &battlePtr->units(s);
      CampaignBattleUnitListIterR iter(units);

      while(++iter)
      {
         const CampaignBattleUnit* bu = iter.current();
         CommandPosition* cp = d_campData->getCommand(bu->getUnit());

         SPCount count = d_campData->getArmies().getUnitSPCount(bu->getUnit(), True);
         cSupply += (cp->getSupply() * count);

         spCount += count;

      }

      if(spCount > 0)
      {
         Attribute supply = clipValue<Attribute>(cSupply/spCount, 0, Attribute_Range);
         battlePtr->supply(s, supply);
      }

#ifdef DEBUG
      cbLog.printf("Supply level for %s is %ld%%",
         scenario->getSideName(s),
         MulDiv(d_battle->supply(s), 100, 256));
#endif
   }
}

/*
 * set starting morale for units in list
 */

void CampaignBattleUtil::initStartingMorale()
{
   for(Side s = 0; s < scenario->getNumSides(); s++)
   {
#ifdef DEBUG
      cbLog.printf("---Processing Starting Morale for %s", scenario->getSideName(s));
#endif

      initStartingMorale(s);
   }
}

void CampaignBattleUtil::initStartingMorale(Side s)
{
   CampaignBattleUnitListIterR iter(&d_battle->units(s));

   ULONG combinedMorale = 0;
   SPCount spCount = 0;         // actual sp count
   SPCount spValue = 0;         // effective sp count

   while(++iter)
   {
      const CampaignBattleUnit* bu = iter.current();


      CommandPosition* cp = d_campData->getCommand(bu->getUnit());
      Attribute morale = cp->getMorale();

#ifdef DEBUG
      cbLog.printf("Start morale before modifiers for %s = %d",
         cp->getNameNotNull(), static_cast<int>(morale));
#endif

      SPCount count = d_campData->getArmies().getUnitSPCount(bu->getUnit(), True);
      spValue += d_campData->getArmies().getUnitSPValue(bu->getUnit());


      // modifier for Combined arms
      // Note: applies if forces infantry is not at least 50% of its strength

      enum StartMoraleModifiers
      {
         CombinedArms,
         NMoraleAbove225,
         NMoraleBelow150,
         NMoraleBelow100,
         NMoraleBelow50,

         SMM_HowMany
      };

      const Table1D<UWORD>& table = scenario->getStartMoraleModifierTable();
      ASSERT(table.getWidth() == SMM_HowMany);
      ASSERT(table.getResolution() != 0);

      SPCount nInfantry = d_campData->getArmies().getNType(bu->getUnit(), BasicUnitType::Infantry, False);

      if(nInfantry <= count / 2)
         morale = minimum<int>(Attribute_Range, MulDiv(morale, table.getValue(CombinedArms), table.getResolution()));

#ifdef DEBUG
      cbLog.printf("morale after combined-arms modifier = %d",
         static_cast<int>(morale));
#endif

      // modifier for national morale
      Nationality n = (scenario->getNationType(cp->getNation()) == MajorNation) ?
         cp->getNation() : scenario->getDefaultNation(cp->getSide());

      Attribute nMorale = d_campData->getArmies().getNationMorale(cp->getNation());
      if(nMorale >= 225)
         morale = minimum<int>(Attribute_Range, MulDiv(morale, table.getValue(NMoraleAbove225), table.getResolution()));
      else if(nMorale <= 50)
         morale = minimum<int>(Attribute_Range, MulDiv(morale, table.getValue(NMoraleBelow50), table.getResolution()));
      else if(nMorale <= 100)
         morale = minimum<int>(Attribute_Range, MulDiv(morale, table.getValue(NMoraleBelow100), table.getResolution()));
      else if(nMorale <= 150)
         morale = minimum<int>(Attribute_Range, MulDiv(morale, table.getValue(NMoraleBelow150), table.getResolution()));

      // modifier for supply level
      // For every 1% a force's supply level is below 50% its
      // morale is reduced by 1%

      const UBYTE minSupplyPercent = 50;
      Attribute supply = cp->getSupply();
      int supplyPercent = MulDiv(supply, 100, Attribute_Range);

      if(supplyPercent < minSupplyPercent)
      {
         int missingPercent = minSupplyPercent - supplyPercent;
         morale = MulDiv(morale, 100-missingPercent, 100);
      }

#ifdef DEBUG
      cbLog.printf("morale after supply modifier = %d",
         static_cast<int>(morale));
#endif

      combinedMorale += (morale * count);
      spCount += count;
   }

   if(spCount > 0)
   {
      Attribute startMorale = clipValue<Attribute>(combinedMorale/spCount, 0, Attribute_Range);

#ifdef DEBUG
      cbLog.printf("Combined starting morale for %s before size modifier = %d",
         scenario->getSideName(s), static_cast<int>(startMorale));
#endif

      /*
      * Starting morale modifiers
      */

      // modifier for force size
      ForceSize::Value forceSize = CombatTableUtil::getForceSize(spValue);

      const Table1D<int>& table = scenario->getSizeModifierTable();
      ASSERT(table.getWidth() == ForceSize::HowMany);
      ASSERT(table.getResolution() != 0);

      int modifier =  table.getValue(forceSize);
      startMorale = clipValue<Attribute>(MulDiv(startMorale, modifier, table.getResolution()), 0, Attribute_Range);

#ifdef DEBUG
      cbLog.printf("Combined starting morale for %s after size modifier = %d",
         scenario->getSideName(s), static_cast<int>(startMorale));
#endif

      CampaignBattlePtr battlePtr = d_battle.current();

      battlePtr->startMorale(s, startMorale);
      battlePtr->currentMorale(s, startMorale);
   }
}

/*
 * set both sides current sp value, and sp counts
 */


void CampaignBattleUtil::setSPValues()
{
   static CampaignBattleUnitListUtil::Data data;

   CampaignBattlePtr battlePtr = d_battle.current();

   for(Side s = 0; s < scenario->getNumSides(); s++)
   {
#ifdef DEBUG
      cbLog.printf("------------- Setting Effective SP Values for %s", scenario->getSideName(s));
#endif
      CampaignBattleUnitListIterW iter(&battlePtr->units(s));

      SPCount spValue = 0;
      SPCount nInfantry = 0;
      SPCount nArtillery = 0;
      SPCount nCavalry = 0;
      SPCount nOther = 0;
      while(++iter)
      {
         CampaignBattleUnit* bu = iter.current();
         CommandPosition* cp = d_campData->getCommand(bu->getUnit());

#ifdef DEBUG
         cbLog.printf("------------- Setting Effective SP Values for %s",
            (const char*)d_campData->getUnitName(bu->getUnit()).toStr());
#endif

         data.set(d_campData, bu, d_battle->battleRound());

         /*
         *  If round 1 or 2 get any ineffective organizations
         */

         if(d_battle->battleRound() <= 2 && d_battle->daysOfBattle() <= 1)
            CampaignBattleUnitListUtil::getIneffectiveUnits(data);

         /*
         * Get Unit SP Value
         */

         spValue += CampaignBattleUnitListUtil::getUnitSPValue(data);
         nInfantry += d_campData->getArmies().getNType(bu->getUnit(), BasicUnitType::Infantry, False);
         nCavalry += d_campData->getArmies().getNType(bu->getUnit(), BasicUnitType::Cavalry, False);
         nArtillery += d_campData->getArmies().getNType(bu->getUnit(), BasicUnitType::Artillery, False);
         nOther += d_campData->getArmies().getNType(bu->getUnit(), BasicUnitType::Special, False);

      }

      // SP value is halved if we are attacking across a blown bridge
      if(d_battle->defendingBlownBridge(getOtherSide(s)))
      {
#ifdef DEBUG
         cbLog.printf("%s are attacking across blown bridge", scenario->getSideName(s));
#endif

         // unless we have a bridge train, if so it is halved only on the first fround
         if((d_battle->hasBridgeTrain(s)) &&
            (d_battle->battleRound() >= 2 || d_battle->daysOfBattle() > 1))
         {
            // do nothing
#ifdef DEBUG
            cbLog.printf("%s has a bridge train, no penalty", scenario->getSideName(s));
#endif
         }
         else
            spValue /= 2;
      }

      battlePtr->spValue(s, spValue);
      battlePtr->infantryCount(s, nInfantry);
      battlePtr->artilleryCount(s, nArtillery);
      battlePtr->cavalryCount(s, nCavalry);
      battlePtr->otherCount(s, nOther);

#ifdef DEBUG
      cbLog.printf("SPValue = %d, Infantry = %d, Artillery = %d, Cavalry = %d, Other = %d, Total = %d",
         static_cast<int>(d_battle->spValue(s)),
         static_cast<int>(d_battle->infantryCount(s)),
         static_cast<int>(d_battle->artilleryCount(s)),
         static_cast<int>(d_battle->cavalryCount(s)),
         static_cast<int>(d_battle->otherCount(s)),
         static_cast<int>(d_battle->spCount(s)));

#endif
   }

   battlePtr->setRatio();
}

/*
 * Set artillery bombardment levels for each side
 */

void CampaignBattleUtil::setBombValues()
{
   CampaignBattlePtr battlePtr = d_battle.current();

   for(Side s = 0; s < scenario->getNumSides(); s++)
   {
      BombValue bValue = 0;

      CampaignBattleUnitListIterR iter(&d_battle->units(s));

      while(++iter)
      {
         const CampaignBattleUnit* bu = iter.current();
         bValue += d_armies->getBombardmentValue(bu->getUnit());
      }

      battlePtr->bombValue(s, bValue);
   }
}

/*
 * Set fatigue levels for each side
 */

void CampaignBattleUtil::setFatigue()
{
   CampaignBattlePtr battlePtr = d_battle.current();

   for(Side s = 0; s < scenario->getNumSides(); s++)
   {
      int cFatigue = 0;
      SPCount spCount = 0;

      CampaignBattleUnitList* units = &battlePtr->units(s);
      CampaignBattleUnitListIterR iter(units);

      while(++iter)
      {
         const CampaignBattleUnit* bu = iter.current();
         CommandPosition* cp = d_campData->getCommand(bu->getUnit());

         SPCount count = d_campData->getArmies().getUnitSPCount(bu->getUnit(), True);
         cFatigue += (cp->getFatigue() * count);

         spCount += count;

      }

      if(spCount > 0)
      {
         Attribute fatigue = clipValue<Attribute>(cFatigue/spCount, 0, Attribute_Range);
         battlePtr->fatigue(s, fatigue);
      }
   }
}

/*
 *  See if defending force is defending a victory location
 *  or if they are retreating from another battle
 */

void CampaignBattleUtil::setUnitListFlags()
{
   CampaignBattlePtr battlePtr = d_battle.current();

   for(Side side = 0; side < scenario->getNumSides(); side++)
   {
      battlePtr->defendingVictoryLocation(side, False);
      battlePtr->defendingChokePoint(side, False);
      battlePtr->defendingBlownBridge(side, False);
      battlePtr->hasEngineers(side, False);
      battlePtr->hasBridgeTrain(side, False);

      CampaignBattleUnitListIterR iter(&battlePtr->units(side));
      while(++iter)
      {
         const CampaignBattleUnit* bu = iter.current();
         ICommandPosition cpi = bu->getUnit();
         ASSERT(side == cpi->getSide());

         if(cpi->isRetreating())
            battlePtr->breakingOut(side, True);

         // defensive flags
         if(cpi->atTown() && (cpi->posture() == Orders::Posture::Defensive))
         {
            Town& town = d_campData->getTown(cpi->getTown());

            // set defending chokepoint flag
            if(!d_battle->defendingChokePoint(side) &&
               d_campData->isChokePoint(cpi->getTown()))
            {
               battlePtr->defendingChokePoint(side, True);
#ifdef DEBUG
               cbLog.printf("%s is defending chokepoint", scenario->getSideName(side));
#endif
            }

            // set defending blown bridge
            if(!d_battle->defendingBlownBridge(side) &&
               !town.isBridgeUp())
            {
               battlePtr->defendingBlownBridge(side, True);
#ifdef DEBUG
               cbLog.printf("%s is defending blown bridge", scenario->getSideName(side));
#endif
            }

            if(!d_battle->defendingVictoryLocation(side) &&
               town.getVictory(cpi->getSide()) > 0)
            {
               battlePtr->defendingVictoryLocation(side, True);
#ifdef DEBUG
               cbLog.printf("%s is defending victory location", scenario->getSideName(side));
#endif
            }
         }

         // if we have engineers
         if(!d_battle->hasEngineers(side) &&
            d_campData->getArmies().hasEngineer(cpi))
         {
            battlePtr->hasEngineers(side, True);
#ifdef DEBUG
            cbLog.printf("%s has Engineers", scenario->getSideName(side));
#endif
         }

         // if we have a bridge-train
         if(!d_battle->hasBridgeTrain(side) &&
            d_campData->getArmies().hasBridgeTrain(cpi))
         {
            battlePtr->hasBridgeTrain(side, True);
#ifdef DEBUG
            cbLog.printf("%s has Bridge Train", scenario->getSideName(side));
#endif
         }
      }
   }
}

/*
 * Set Side nationality settings
 *
 */

void CampaignBattleUtil::setSideNationality()
{
   // container for unitcount values
   static UINT s_nationSPCounts[Nationality_MAX];
   // container for unit artillery values
   static UINT s_nationArtilleryValues[Nationality_MAX];
   ASSERT(scenario->getNumNations() <= Nationality_MAX);


   for(Side side = 0; side < scenario->getNumSides(); side++)
   {
#ifdef DEBUG
      cbLog.printf("Setting nationality for %s", scenario->getSideName(side));
#endif

      /*
      * initialize containers
      */

      for(int n = 0; n < scenario->getNumNations(); n++)
      {
         s_nationSPCounts[n] = 0;
         s_nationArtilleryValues[n] = 0;
      }

      /*
      * fill container values
      */

      CampaignBattleUnitListIterR iter(&d_battle->units(side));
      while(++iter)
      {
         const CampaignBattleUnit* bu = iter.current();

         UnitIter uiter(&d_campData->getArmies(), bu->getUnit());
         while(uiter.next())
         {
            ICommandPosition cpi = uiter.current();
            ASSERT(cpi->getSide() == side);
            ASSERT(cpi->getNation() < scenario->getNumNations());

            /*
            * Get bombardment strength
            */

            BombValue bValue = 0;
            ConstStrengthPointIter iter(&d_campData->getArmies(), cpi);
            while(++iter)
            {
               ConstISP sp = iter.current();

               const UnitTypeItem& uti = d_campData->getUnitType(sp->getUnitType());
               bValue += uti.getBombardmentValue();
            }

            s_nationArtilleryValues[cpi->getNation()] += bValue;

            /*
            * Get unit count
            */

            SPCount spCount = d_campData->getArmies().getUnitSPCount(cpi, False);
            s_nationSPCounts[cpi->getNation()] += spCount;
         }
      }

      /*
      * Set unit nation values
      */

      // find national artillery majority
      UINT bestValue = 0;
      Nationality bestNation = Nationality_MAX;

      for(n = 0; n < scenario->getNumNations(); n++)
      {
         if(scenario->nationToSide(n) == side)
         {
            if(bestNation == Nationality_MAX ||
               s_nationArtilleryValues[n] > bestValue)
            {
               bestValue = s_nationArtilleryValues[n];
               bestNation = n;
            }
         }
      }

      ASSERT(bestNation != Nationality_MAX);
      d_battle.current()->sideArtilleryNation(side, bestNation);

#ifdef DEBUG
      cbLog.printf("Artillery nationality is %s", scenario->getNationName(bestNation));
#endif

      // find national sp majority
      bestValue = 0;
      bestNation = Nationality_MAX;

      for(n = 0; n < scenario->getNumNations(); n++)
      {
         if(scenario->nationToSide(n) == side)
         {
            if(bestNation == Nationality_MAX ||
               s_nationSPCounts[n] > bestValue)
            {
               bestValue = s_nationSPCounts[n];
               bestNation = n;
            }
         }
      }

      ASSERT(bestNation != Nationality_MAX);
      d_battle.current()->sideNation(side, bestNation);

#ifdef DEBUG
      cbLog.printf("Side nationality is %s", scenario->getNationName(bestNation));
#endif
   }
}

/*
 *  Close up columns when deploying into battle
 *  A rather crude way of doing it
 */

void CampaignBattleUtil::closeUpColumns()
{
   CampaignBattlePtr battlePtr = d_battle.current();

   for(Side side = 0; side < scenario->getNumSides(); side++)
   {
      CampaignBattleUnitListIterW iter(&battlePtr->units(side));
      while(++iter)
      {
         CampaignBattleUnit* bu = iter.current();
         ICommandPosition cpi = bu->getUnit();
         CampaignPosition* pos = cpi->location();

         Distance head = pos->getHeadDistance();
         Distance tail = pos->getTailDistance();
         const Distance columnLength = (head >= tail) ? head - tail : tail - head;

#ifdef DEBUG
         Location l;
         pos->getLocation(l);
         cbLog.printf("Location for %s is (%ld, %ld)", cpi->getName(), l.getX(), l.getY());
         cbLog.printf("Column length is %ld, Head Distance is  %ld, Tail distance is %ld",
            columnLength, head, tail);
#endif

         if(d_battle->timeElapsed(d_campData->getTick()))
            tail = head;

         else
         {
            if(head > tail)
            {
               tail += (columnLength/2);
            }
            else if(tail > head)
            {
               tail -= (columnLength/2);
            }

         }

         pos->setTailDistance(tail);
#ifdef DEBUG
         cbLog.printf("Head Distance is  %ld, Tail distance is %ld", head, tail);
#endif
      }
   }
}

void CampaignBattleUtil::doBrushAside()
{
   ASSERT(d_battle->brushAside());
   Side winningSide = (d_battle->spValue(0) > d_battle->spValue(1)) ? 0 : 1;
   Side losingSide = getOtherSide(winningSide);

   // set larger side as victor
   CampaignBattlePtr battlePtr = d_battle.current();
   battlePtr->victor(winningSide);
   battlePtr->currentMorale(losingSide, 0);
}

void CampaignBattleUtil::doBombardment()
{
   // some useful constants
   const int dieRollMin = 2;
   const int dieRollMax = 11;
   const int modifierMin = -6;
   const int modifierMax = 4;

   for(Side side = 0; side < scenario->getNumSides(); side++)
   {
#ifdef DEBUG
      cbLog.printf("-------- Applying Bombardment for %s", scenario->getSideName(side));
#endif

      /*
      * If our current morale is 0, then this is a retreating unit
      * a retreating unit cannot inflict casualties
      */

      if(d_battle->currentMorale(side) <= 0)
         continue;

      Side otherSide = getOtherSide(side);

      /*
      * Get our bombardment value
      */

      BombValue bombValue = d_battle->bombValue(side);
      SPCount nonArtySPCount = d_battle->nonArtySPCount(side);
      SPCount theirSPCount = d_battle->spCount(otherSide);

#ifdef DEBUG
      cbLog.printf("Bombardment value before modifier = %d", static_cast<int>(bombValue));
      cbLog.printf("Number non-arty SP's = %d", static_cast<int>(nonArtySPCount));
      cbLog.printf("Enemy SP count = %d", static_cast<int>(theirSPCount));
#endif

      /*
      *   limit bombardment values to no greater than half enemy total sp
      *   and half friendly non-arty
      */

      bombValue = static_cast<BombValue>(minimum<int>(bombValue, theirSPCount/2));
      bombValue = static_cast<BombValue>(minimum<int>(bombValue, nonArtySPCount/2));

#ifdef DEBUG
      cbLog.printf("Bombardment value after enemySize/nonArtySP modifier = %d", static_cast<int>(bombValue));
#endif

      /*
      * Get die roll between 2 and 12. Die roll can be modified to a min
      * of 0 and a max of 13
      */


      int dieRoll = CRandom::get(dieRollMin, dieRollMax);
      ASSERT(dieRoll >= dieRollMin && dieRoll <= dieRollMax);

#ifdef DEBUG
      cbLog.printf("Bombardment die roll before modifiers = %d", dieRoll);
#endif

      /*
      * Apply die Roll modifiers
      */

      int modifyBy = 0;

      /*
      * modifier for nationality (needs scenario table)
      */

      const Table1D<SWORD>& nTable = scenario->getBombardmentNationModifierTable();
      ASSERT(nTable.getWidth() == scenario->getNumNations());
      ASSERT(d_battle->sideArtilleryNation(side) < scenario->getNumNations());

      modifyBy += nTable.getValue(d_battle->sideArtilleryNation(side));

#ifdef DEBUG
      cbLog.printf("Die Roll modifier after nation modifier = %d", modifyBy);
#endif

      /*
      * Modifiers for terrain
      */

      ASSERT(d_battle->getCloseTown() != NoTown);
      Town& t = d_campData->getTown(d_battle->getCloseTown());
      const TerrainTypeItem& terrain = d_campData->getTerrainType(t.getTerrain());

      // get terrain table
      const Table2D<int>& gTable = scenario->getBombardmentTerrainModifierTable();
      modifyBy += gTable.getValue(terrain.getGroundType(), d_battle->posture(side));;

#ifdef DEBUG
      cbLog.printf("Die Roll modifier after terrain modifier = %d", modifyBy);
#endif

      enum BombardmentModifier
      {
         SupplyLessThan25,
         SupplyLessThan50,
         SupplyLessThan75,
         SupplyOver75,
         Has3ArtillerySpecialist,
         HasArtillerySpecialist,
         NoSpecialist,
         DefendingBlownBridge,
         EnemyDefendingBlownBridge,

         BM_HowMany
      };

      const Table1D<SWORD>& aTable = scenario->getBombardmentModifierTable();
      ASSERT(aTable.getWidth() == BM_HowMany);

      /*
      * modifier for supply
      */

      int supplyPercent = MulDiv(d_battle->supply(side), 100, Attribute_Range);

      // supply is less than 25% (-2)
      // supply is less than 50% (-1)
      BombardmentModifier bm = (supplyPercent < 25) ? SupplyLessThan25 :
      (supplyPercent < 50) ? SupplyLessThan50 :
      (supplyPercent < 75) ? SupplyLessThan75 : SupplyOver75;

      modifyBy += aTable.getValue(bm);

#ifdef DEBUG
      cbLog.printf("Die Roll modifier after supply modifier = %d", modifyBy);
#endif

      /*
      * Modifier for Having specialist
      */

      // for having at least 1 artillery specialist (+1)
      // for having at least 3 artillery specialist (+2)
      bm = (d_battle->has3ArtillerySpecialist(side)) ? Has3ArtillerySpecialist :
      (d_battle->hasArtillerySpecialist(side)) ? HasArtillerySpecialist : NoSpecialist;

      modifyBy += aTable.getValue(bm);

#ifdef DEBUG
      cbLog.printf("Die Roll modifier after Artillery Specialist modifier = %d", modifyBy);
#endif

      /*
      * Modifier for Weather
      */

      CampaignWeather::Weather weather = d_campData->getWeather().getWeather();
      CampaignWeather::GroundConditions ground = d_campData->getWeather().getGroundConditions();

      const Table2D<SWORD>& wTable = scenario->getBombardmentWeatherTable();
      ASSERT(wTable.getWidth() == CampaignWeather::GroundConditions_HowMany);
      ASSERT(wTable.getHeight() == CampaignWeather::Weather_HowMany);

      modifyBy += wTable.getValue(weather, ground);

#ifdef DEBUG
      cbLog.printf("Die Roll modifier after weather modifier = %d", modifyBy);
#endif

      // modify for blown bridge
      if(d_battle->defendingBlownBridge(side))
         modifyBy += aTable.getValue(DefendingBlownBridge);
      else if(d_battle->defendingBlownBridge(otherSide))
         modifyBy += aTable.getValue(EnemyDefendingBlownBridge);

#ifdef DEBUG
      cbLog.printf("Die Roll modifier after blown bridge modifier = %d", modifyBy);
#endif

      /*
      * Clip modifier to min/max values (-6, 4)
      */

      modifyBy = static_cast<int>(clipValue(modifyBy, modifierMin, modifierMax));

      /*
      * Clip dieRoll between 0 and nBombardmentEffect-1
      */

      const Table1D<int>& bTable = scenario->getBombardmentEffectTable();
      ASSERT(bTable.getResolution() != 0);

      dieRoll = clipValue(dieRoll + modifyBy, 0, bTable.getWidth() - 1);
      ASSERT(dieRoll < bTable.getWidth());

#ifdef DEBUG
      cbLog.printf("Die Roll after modifiers = %d", dieRoll);
#endif

      int lossPercent = bTable.getValue(dieRoll);

      // actual loss % is table value / 1000
      int n = minimum(bombValue * lossPercent, 32767);
      SPLoss loss(0, static_cast<UWORD>(n), 100 * bTable.getResolution());

#ifdef DEBUG
      cbLog.printf("other side loses %d.%x",
         loss.getInt(), loss.getFraction());
#endif

      d_battle.current()->loss(otherSide, loss);
   }
}

void CampaignBattleUtil::doCasualty()
{
   // some useful constants
   const int dieRollMin = 2;
   const int dieRollMax = 11;
   const int modifierMin = -6;
   const int modifierMax = 4;

   const Table1D<int>& ceTable = scenario->getCasualityEffectTable();

   for(Side side = 0; side < scenario->getNumSides(); side++)
   {

#ifdef DEBUG
      cbLog.printf("-------- Applying Casualty for %s", scenario->getSideName(side));
#endif

      /*
      * if current morale is 0 then we cannot inflict casualties
      */

      if(d_battle->currentMorale(side) <= 0)
         continue;

      Side otherSide = getOtherSide(side);

      // some useful values
      ILeader iOurCinC = d_battle->cinc(side);
      ASSERT(iOurCinC != NoLeader);
      ILeader iTheirCinC = d_battle->cinc(otherSide);
      ASSERT(iTheirCinC != NoLeader);

      if(iOurCinC == NoLeader || iTheirCinC == NoLeader)
         return;

      Leader* ourCinC = d_campData->getLeader(iOurCinC);
      Leader* theirCinC = d_campData->getLeader(iTheirCinC);

      /*
      *  roll the dice
      */

      int dieRoll = CRandom::get(dieRollMin, dieRollMax);
      ASSERT(dieRoll >= dieRollMin && dieRoll <= dieRollMax);

#ifdef DEBUG
      cbLog.printf("Casualty die roll before modifiers = %d", dieRoll);
#endif

      int modifyBy = 0;

      /*
      * Apply die Roll modifiers
      */

      enum CombatModifiers
      {
         OurAbility100Greater,
         OurAbility50Greater,
         OurAbilityGreater,
         TheirAbility50Greater,
         TheirAbilityGreater,
         OurMorale100Greater,
         OurMoraleGreater,
         TheirMorale50Greater,
         TheirMoraleGreater,
         MoraleLessThan65,
         MoraleOver65,
         Ratio1To4,
         SupplyOver75,
         SupplyLessThan75,
         SupplyLessThan50,
         EnemySupplyLessThan25,
         Fatigue2To1,
         FatigueLessThan65,
         Reinforced,
         EnemyDefendingChokePoint,
         EngineersAtChokePoint,
         WonLastRound,
         LostLastRound,
         OneTypeSP,
         TwoTypeSP,
         EnemyOneTypeSP,
         DefendingChokePoint,

         CM_HowMany
      };

      const Table1D<SWORD>& mTable = scenario->getCombatDieRollModifier();
      ASSERT(mTable.getWidth() == CM_HowMany);

      /*
      * modifier for leader's tactical ablility
      */

      // if 50% greater than enemies (+2)
      // if greater than enemies (+1)
      // if enemy is 50% greater (-1)
      CombatModifiers cm = (ourCinC->getTactical() >= (theirCinC->getTactical()*2)) ? OurAbility100Greater :
      (ourCinC->getTactical() >= (theirCinC->getTactical()*1.5)) ? OurAbility50Greater :
      (ourCinC->getTactical() > theirCinC->getTactical()) ? OurAbilityGreater :
      (theirCinC->getTactical() >= (ourCinC->getTactical()*1.5)) ? TheirAbility50Greater : TheirAbilityGreater;

      modifyBy += mTable.getValue(cm);

#ifdef DEBUG
      cbLog.printf("Die Roll Modifier = %d, after leader ability modifier", modifyBy);
#endif

      /*
      * modifiers for terrain
      */

      ASSERT(d_battle->getCloseTown() != NoTown);
      Town& t = d_campData->getTown(d_battle->getCloseTown());
      const TerrainTypeItem& terrain = d_campData->getTerrainType(t.getTerrain());

      // get terrain table
      const Table3D<int>& gTable = scenario->getCasualtyTerrainModifierTable();
      modifyBy += gTable.getValue(terrain.getGroundType(), d_battle->posture(side), d_battle->posture(otherSide));;

      // get river table
      const Table3D<int>& rTable = scenario->getCasualtyRiverTypeModifierTable();
      modifyBy += rTable.getValue(terrain.getRiverType(), d_battle->posture(side), d_battle->posture(otherSide));

      // if we;re defending a chokepoint
      modifyBy += mTable.getValue(DefendingChokePoint);

#ifdef DEBUG
      cbLog.printf("Die Roll Modifier = %d, after terrain modifier", modifyBy);
#endif

      /*
      * modifiers for morale
      */

      // our morale is double enemies  (+1)
      // else if enemy is at least 50% larger (-1)
      cm = (d_battle->currentMorale(side) > d_battle->currentMorale(otherSide)*2) ? OurMorale100Greater :
      (d_battle->currentMorale(side) > d_battle->currentMorale(otherSide)) ? OurMoraleGreater :
      (d_battle->currentMorale(otherSide) >= d_battle->currentMorale(side)*1.5) ? TheirMorale50Greater : TheirMoraleGreater;

      modifyBy += mTable.getValue(cm);

      // our morale is 65 or less (-1)
      if(d_battle->currentMorale(side) <= 65)
         modifyBy += mTable.getValue(MoraleLessThan65);
      // else if over 65 and we against a retreating foe (+1)
      else if(d_battle->currentMorale(otherSide) == 0)
      {
         modifyBy += mTable.getValue(MoraleOver65);
      }

#ifdef DEBUG
      cbLog.printf("Die Roll Modifier = %d, after morale modifier", modifyBy);
#endif

      /*
      *  modifiers for Cavalry
      */

      // not applicable if enemy is defending blown bridge

      enum CavalryRatio
      {
         Over4To1,
         Over2To1,
         Over1To1,
         Less1To1,
         Less1To2,

         CR_HowMany
      };

      enum SpecialistRatio
      {
         MoreSpecialist,
         LessSpecialist,

         SR_HowMany
      };

      const Table3D<SWORD>& cTable = scenario->getCasualtyCavalryModifierTable();
      ASSERT(cTable.getNTables() == SR_HowMany);
      ASSERT(cTable.getHeight() == CR_HowMany);
      ASSERT(cTable.getWidth() == Terrain::GroundType_HowMany);

      // calc cavalry ratio
      SPCount ourCav = d_battle->cavalryCount(side);
      SPCount theirCav = d_battle->cavalryCount(otherSide);

      CavalryRatio cr = (ourCav >= theirCav  * 4) ? Over4To1 :
      (ourCav >= theirCav  * 2) ? Over2To1 :
      (ourCav >= theirCav) ? Over1To1 :
      (theirCav >= ourCav  * 2) ? Less1To2 : Less1To1;

      // calc specialist ratio
      int ourSpecialist = d_battle->nCavalrySpecialist(side);
      int theirSpecialist = d_battle->nCavalrySpecialist(otherSide);

      SpecialistRatio sr = (ourSpecialist >= theirSpecialist) ? MoreSpecialist : LessSpecialist;

      // get table value
      int v = cTable.getValue(sr, cr, terrain.getGroundType());

      // if we have a positive value, we can only add it if enemy is not
      // defending a blown bridge
      if(v > 0 && d_battle->defendingBlownBridge(otherSide))
         v = 0;

      modifyBy += v;

#ifdef DEBUG
      cbLog.printf("Die Roll Modifier = %d, after terrain modifier", modifyBy);
#endif

      /*
      * modifiers for Odds
      */

      // if we are outnumbered by at least 4:1 (-1)
      if(d_battle->units(otherSide).combatManpower() >= d_battle->units(side).combatManpower()*4)
         modifyBy += mTable.getValue(Ratio1To4);

#ifdef DEBUG
      cbLog.printf("Die Roll Modifier = %d, after odds modifier", modifyBy);
#endif

      /*
      * modifiers for supply
      */

      int ourSupplyPercent = MulDiv(d_battle->supply(side), 100, Attribute_Range);
      int theirSupplyPercent = MulDiv(d_battle->supply(otherSide), 100, Attribute_Range);

      // we have less than 50%  (-2)
      // we have less than 75% (-1)
      cm = (ourSupplyPercent <= 50) ? SupplyLessThan50 :
      (ourSupplyPercent <= 75) ? SupplyLessThan75 : SupplyOver75;

      modifyBy += mTable.getValue(cm);

      // enemy supply is less than 25% (+1)
      if(theirSupplyPercent <= 25)
         modifyBy += mTable.getValue(EnemySupplyLessThan25);

#ifdef DEBUG
      cbLog.printf("Die Roll Modifier = %d, after supply modifier", modifyBy);
#endif

      /*
      * modifiers for fatigue
      */

      // if our fatigue is double enemies (+1)
      if(d_battle->fatigue(side) >= d_battle->fatigue(otherSide)*2)
         modifyBy += mTable.getValue(Fatigue2To1);

      // if our fatigue is 65 or less (-1)
      if(d_battle->fatigue(side) <= 65)
         modifyBy += mTable.getValue(FatigueLessThan65);

#ifdef DEBUG
      cbLog.printf("Die Roll Modifier = %d, after fatigue modifier", modifyBy);
#endif

      /*
      * Modifier for tactical situation
      */

      // if we we're reinforced
      if(d_battle->reinforced(side))
         modifyBy += mTable.getValue(Reinforced);

      // if enemy is defending a chokepoint
      if(d_battle->defendingChokePoint(otherSide))
         modifyBy += mTable.getValue(EnemyDefendingChokePoint);

      // if we have engineers and are attacking or defending a chokepoint
      if((d_battle->hasEngineers(side)) &&
         ( (d_battle->posture(side) == Orders::Posture::Offensive &&
         d_battle->defendingChokePoint(otherSide)) ||
         (d_battle->posture(side) == Orders::Posture::Defensive &&
         d_battle->defendingChokePoint(side)) ))
      {
         modifyBy += mTable.getValue(EngineersAtChokePoint);
      }

      // you won previous round and have offensive posture
      if(d_battle->posture(side) == Orders::Posture::Offensive &&
         roundOutcome(side) == WonThisRound)
      {
         modifyBy += mTable.getValue(WonLastRound);
      }

      // or lost last round and enemy has offensive posture
      else if(d_battle->posture(otherSide) == Orders::Posture::Offensive &&
         roundOutcome(otherSide) == WonThisRound)
      {
         modifyBy += mTable.getValue(LostLastRound);
      }

#ifdef DEBUG
      cbLog.printf("Die Roll Modifier = %d, after tactical situation modifier", modifyBy);
#endif

      /*
      * Modifier for combined arms
      */

      // we have only 1 type of SP
      if((d_battle->infantryCount(side) == 0 && d_battle->cavalryCount(side) == 0) ||
         (d_battle->artilleryCount(side) == 0 && d_battle->cavalryCount(side) == 0) ||
         (d_battle->infantryCount(side) == 0 && d_battle->artilleryCount(side) == 0))
      {
         modifyBy += mTable.getValue(OneTypeSP);
      }

      // we have only 2 type of SP
      if((d_battle->infantryCount(side) == 0) ||
         (d_battle->artilleryCount(side) == 0) ||
         (d_battle->cavalryCount(side) == 0))
      {
         modifyBy += mTable.getValue(TwoTypeSP);
      }

      // enemy have only 1 type of SP
      if((d_battle->infantryCount(otherSide) == 0 && d_battle->cavalryCount(otherSide) == 0) ||
         (d_battle->artilleryCount(otherSide) == 0 && d_battle->cavalryCount(otherSide) == 0) ||
         (d_battle->infantryCount(otherSide) == 0 && d_battle->artilleryCount(otherSide) == 0))
      {
         modifyBy += mTable.getValue(EnemyOneTypeSP);
      }

#ifdef DEBUG
      cbLog.printf("Die Roll Modifier = %d, after combined-arms modifier", modifyBy);
#endif

      /*
      * Nationality modifier
      */

      const Province& prov = d_campData->getProvince(t.getProvince());

      const Table2D<SWORD>& nTable = scenario->getCasualtyNationTable();
      modifyBy += nTable.getValue(d_battle->sideNation(side), prov.getNationality());

#ifdef DEBUG
      cbLog.printf("Die Roll Modifier = %d, after combined-arms modifier", modifyBy);
#endif

      // clip modifier
      modifyBy = clipValue(modifyBy, modifierMin, modifierMax);

      /*
      * Clip dieRoll between 0 and nBombardmentEffect-1
      */

      dieRoll = clipValue(dieRoll+modifyBy, 0, ceTable.getWidth() - 1);

#ifdef DEBUG
      cbLog.printf("Die Roll after modifiers = %d", dieRoll);
#endif

      d_battle.current()->dieRoll(side, static_cast<UBYTE>(dieRoll));

   }

   /*
   * Apply actual losses
   */

   for(side = 0; side < scenario->getNumSides(); side++)
   {
      Side otherSide = getOtherSide(side);

      int lossPercent = ceTable.getValue(d_battle->dieRoll(side));

      /*
      * Modifiers to Loss percent
      */

#ifdef DEBUG
      cbLog.printf("Applying other side losses for %s", scenario->getSideName(side));
      cbLog.printf("Unmodified loss percent = %d.%d",
         lossPercent / ceTable.getResolution(),
         lossPercent % ceTable.getResolution());
#endif

      /*
      * Against Retreating enemy
      */

      enum LossModifiers
      {
         AgainstRetreating,

         LM_HowMany
      };

      const Table1D<UWORD>& lTable = scenario->getLossModifierTable();
      ASSERT(lTable.getWidth() == LM_HowMany);
      ASSERT(lTable.getResolution() != 0);

      if(d_battle->currentMorale(otherSide) == 0)
      {
         UWORD tableValue = lTable.getValue(AgainstRetreating);
         lossPercent = MulDiv(lossPercent, tableValue, lTable.getResolution());

#ifdef DEBUG
         cbLog.printf("Modified (For Against Retreating) loss percent = %d.%d",
            lossPercent / ceTable.getResolution(),
            lossPercent % ceTable.getResolution());
#endif
      }

      /*
      * Posture / Battle Result Modifiers
      */

      const Table3D<UWORD>& poTable = scenario->getLossPostureModifierTable();
      ASSERT(poTable.getResolution() != 0);

      RoundOutcome ro = roundOutcome(side);
      Orders::Posture::Type ourPosture = d_battle->posture(side);
      Orders::Posture::Type theirPosture = d_battle->posture(otherSide);

      UWORD tableValue = poTable.getValue(ro, ourPosture, theirPosture);
      lossPercent = MulDiv(lossPercent, tableValue, poTable.getResolution());

#ifdef DEBUG
      cbLog.printf("Modified (For Posture and Battle Result) loss percent = %d.%d",
         lossPercent / ceTable.getResolution(),
         lossPercent % ceTable.getResolution());
#endif

      int n = minimum(d_battle->spValue(side) * lossPercent, 32767);
      SPLoss loss(0, static_cast<UWORD>(n), 1000);

#ifdef DEBUG
      UWORD n1 = 32767;
      SPLoss testLoss(0, n1, 1000);
      SPLoss testLoss2(0, n1, 1000);
      testLoss += testLoss2;
      int lossInt = testLoss.getInt();
      testLoss *= SPLoss(0, 6, 10);
      lossInt = testLoss.getInt();
      cbLog.printf("other side loses %d.%x",
         loss.getInt(), loss.getFraction());
#endif
      d_battle.current()->loss(otherSide, loss);
   }
}

#if 0
Boolean CampaignBattleUtil::defendingChokePoint(Side s)
{
   Boolean result = False;
   if(d_battle->posture(s) == Orders::Posture::Defensive)
   {
      CampaignBattleUnitListIterR iter(&d_battle->units(s));
      while(++iter)
      {
         const CampaignBattleUnit* bu = iter.current();
         const CommandPosition* cp = d_campData->getCommand(bu->getUnit());

         if(cp->atTown())
         {
            result = d_campData->isChokePoint(cp->getTown());

            if(result)
               break;
         }
      }
   }

   return result;
}

Boolean CampaignBattleUtil::atChokePoint(Side s)
{
   Boolean result = False;

   CampaignBattleUnitListIterR iter(&d_battle->units(s));
   while(++iter)
   {
      const CampaignBattleUnit* bu = iter.current();
      const CommandPosition* cp = d_campData->getCommand(bu->getUnit());

      if(cp->atTown())
      {
         result = d_campData->isChokePoint(cp->getTown());

         if(result)
            break;
      }
   }

   return result;
}

Boolean CampaignBattleUtil::haveEngineers(Side s)
{
   Boolean result = False;

   CampaignBattleUnitListIterR iter(&d_battle->units(s));
   while(++iter)
   {
      const CampaignBattleUnit* bu = iter.current();

      result = d_campData->getArmies().hasEngineer(bu->getUnit());

      if(result)
         break;
   }

   return result;
}
#endif

/*
 * What happened on last round
 */

CampaignBattleUtil::RoundOutcome CampaignBattleUtil::roundOutcome(Side side)
{
#ifdef DEBUG
   static const char* resultText[] = {
      "Won Round",
      "Lost Round",
      "Draw"
    };
#endif

   Side otherSide = getOtherSide(side);

   UBYTE ourRoll = d_battle->dieRoll(side);
   UBYTE theirRoll = d_battle->dieRoll(otherSide);

   RoundOutcome thisRound;
   if(ourRoll >= 5 && ourRoll >= theirRoll+3)
      thisRound = WonThisRound;
   else if(theirRoll >= 5 && theirRoll >= ourRoll+3)
      thisRound = LostThisRound;
   else
      thisRound = DrawThisRound;

#ifdef DEBUG
   cbLog.printf("Outcome = (%s)", resultText[thisRound]);
#endif

   return thisRound;
}

void CampaignBattleUtil::doMorale()
{
   // some useful constants
   const int dieRollMin = 0;
   const int dieRollMax = 9;

   for(Side side = 0; side < scenario->getNumSides(); side++)
   {

#ifdef DEBUG
      cbLog.printf("-------- Applying Morale for %s", scenario->getSideName(side));
#endif

      /*
      * if current morale is 0 then we cannot inflict morale loss
      */

      if(d_battle->currentMorale(side) <= 0)
         continue;

      Side otherSide = getOtherSide(side);

      /*
      *  roll the dice
      */

      int dieRoll = CRandom::get(dieRollMin, dieRollMax);
      ASSERT(dieRoll >= dieRollMin && dieRoll <= dieRollMax);

#ifdef DEBUG
      cbLog.printf("Morale die roll before modifiers = %d", dieRoll);
#endif

      int modifyBy = 0;

      /*
      * Apply die Roll modifiers
      *
      * First, see who 'won' this round of battle
      * To win a sides dieRoll must be 5 or more AND 3 or more greater than
      * the other sides dieRoll
      *
      */

      RoundOutcome thisRound = roundOutcome(side);
      // first increment status counter. this should probably be elsewhere

      d_battle.current()->incStatusCount(side, (thisRound == WonThisRound) ? 1 : (thisRound == LostThisRound) ? -1 : 0);

      const Table3D<SWORD>& mdTable = scenario->getMoraleDieRollModifierTable();
      ASSERT(thisRound < mdTable.getNTables());
      ASSERT(d_battle->posture(side) < mdTable.getHeight());
      ASSERT(d_timeOfDay < mdTable.getWidth());

      modifyBy += mdTable.getValue(thisRound, d_battle->posture(side), d_timeOfDay);

#ifdef DEBUG
      cbLog.printf("ModifyBy after status modifiers = %d", modifyBy);
#endif

      /*
      * Nationality modifiers
      */

      const Town& t = d_campData->getTown(d_battle->getCloseTown());
      const Province& prov = d_campData->getProvince(t.getProvince());

      const Table2D<SWORD>& nTable = scenario->getMoraleNationModifierTable();
      ASSERT(nTable.getWidth() == scenario->getNumNations());
      ASSERT(nTable.getHeight() == scenario->getNumNations());
      ASSERT(d_battle->sideNation(otherSide) < scenario->getNumNations());
      ASSERT(prov.getNationality() < scenario->getNumNations());

      modifyBy += nTable.getValue(d_battle->sideNation(otherSide), prov.getNationality());

#ifdef DEBUG
      cbLog.printf("ModifyBy after nation modifiers = %d", modifyBy);
#endif


      // get morale effect table
      const Table2D<Attribute>& table = scenario->getMoraleEffectTable();

      dieRoll = clipValue(dieRoll + modifyBy, 0, table.getWidth() - 1);

#ifdef DEBUG
      cbLog.printf("Modify by = %d, final die roll = %d", modifyBy, dieRoll);
#endif

      /*
      * apply morale table
      *
      * Note: morale loss applies to enemy and not this side
      */

      UWORD moraleLoss = table.getValue(d_battle->dieRoll(side), dieRoll);

#ifdef DEBUG
      cbLog.printf("Morale loss for %s before modifiers = %d", scenario->getSideName(otherSide), static_cast<int>(moraleLoss));
      cbLog.printf("Morale for %s before loss = %d", scenario->getSideName(otherSide), static_cast<int>(d_battle->currentMorale(otherSide)));
#endif

      /*
      * Apply modifiers to morale loss
      */

      const Table3D<UWORD>& mlTable = scenario->getMoraleLossModifierTable();
      ASSERT(mlTable.getResolution() != 0);
      ASSERT(thisRound < mlTable.getNTables());
      ASSERT(d_battle->posture(side) < mlTable.getHeight());
      ASSERT(d_battle->posture(otherSide) < mlTable.getWidth());

      SWORD tableValue = mlTable.getValue(thisRound, d_battle->posture(side), d_battle->posture(otherSide));

      moraleLoss = MulDiv(moraleLoss, tableValue, mlTable.getResolution());

#ifdef DEBUG
      cbLog.printf("Morale loss for %s after modifiers = %d", scenario->getSideName(otherSide), static_cast<int>(moraleLoss));
#endif

      // Modify for force ratio
      CampaignBattlePtr battlePtr = d_battle.current();
      const int ourSize = battlePtr->units(side).combatManpower();
      const int theirSize = battlePtr->units(otherSide).combatManpower();
      if(theirSize >= 8 * ourSize)
         moraleLoss /= 4.5;
      else if(theirSize >= 7 * ourSize)
         moraleLoss /= 4;
      else if(theirSize >= 6 * ourSize)
         moraleLoss /= 3.5;
      else if(theirSize >= 5 * ourSize)
         moraleLoss /= 3;
      else if(theirSize >= 4 * ourSize)
         moraleLoss /= 2.5;
      else if(theirSize >= 3.5 * ourSize)
         moraleLoss /= 2.25;
      else if(theirSize >= 3 * ourSize)
         moraleLoss /= 2;
      else if(theirSize >= 2.5 * ourSize)
         moraleLoss /= 1.75;
      else if(theirSize >= 2 * ourSize)
         moraleLoss /= 1.5;


#ifdef DEBUG
      cbLog.printf("Morale loss after ratio modifiers = %d", static_cast<int>(moraleLoss));
#endif

      moraleLoss = clipValue<Attribute>(moraleLoss, 0, Attribute_Range - 1);
      d_battle.current()->moraleLoss(otherSide, static_cast<Attribute>(moraleLoss));

#ifdef DEBUG
      cbLog.printf("Morale for %s after loss = %d", scenario->getSideName(otherSide), static_cast<int>(d_battle->currentMorale(otherSide)));
#endif
   }
}

Boolean CampaignBattleUtil::implementLosses()
{
   CampaignBattlePtr battlePtr = d_battle.current();

   for(Side side = 0; side < scenario->getNumSides(); side++)
   {

#ifdef DEBUG
      cbLog.printf("-------- Applying Losses for %s", scenario->getSideName(side));
#endif

      /*
      *  Apply damage
      */

      CampaignBattleUnitListIterW iter(&battlePtr->units(side));
      SPLoss loss = d_battle->loss(side);

      while(++iter)
      {
         CampaignBattleUnit* bu = iter.current();
         ICommandPosition cpi = bu->getUnit();
         CommandPosition* cp = d_armies->getCommand(cpi);

         SPCount spCount = d_armies->getUnitSPCount(cpi, True);   // Active SPs in organization

         SPCount ourSideSP = d_battle->spCount(side);
         SPLoss ourSideLoss = loss;

         /*
         * What will this unit actually lose?
         * e.g. if total side's SP is 10, and this unit has 2 SP
         *      then it will have 2/10 of the total damage applied to it.
         */

         if(ourSideSP > 0)
            ourSideLoss *= SPLoss(0, spCount, ourSideSP);


#ifdef DEBUG
         cbLog.printf("Battle Losses for %s:",
            (const char*)d_campData->getUnitName(cpi).toStr());
         cbLog.printf(" casualties = %u.%04x, SP=%d",
            (unsigned int) ourSideLoss.getInt(),
            (unsigned int) ourSideLoss.getFraction(),
            (int) spCount);

         char name[100];
         lstrcpy(name, cp->getNameNotNull());

#endif

         SPCount actualLoss = Losses::makeLosses(d_armies, cpi, ourSideLoss);
         Boolean wipedOut = CampaignArmy_Util::clearDestroyedUnits(d_campData, cpi);

         if(wipedOut)
         {
#ifdef DEBUG
            cbLog.printf(" %s has been completely wiped out", name);
#endif

            bu->outOfIt(True);

            // if we no longer have units in the battle set other side as winner
            if(!stillInBattle(side))
            {
               // make sure they're still in battle
               Side otherSide = getOtherSide(side);
               if(stillInBattle(otherSide))
                  battlePtr->victor(otherSide);
               else
               {
                  battlePtr->victor(SIDE_Neutral);
                  battlePtr->turnMode(CampaignBattle::BattleTurnMode::BattleOver);
               }
            }
            else
               battlePtr->setCinC(side);

            return True;
         }

         battlePtr->addLoss(side, actualLoss);

#ifdef DEBUG
         cbLog.printf(" Actual Losses = %d ==> %d",
            static_cast<int>(actualLoss),
            static_cast<int>(d_battle->getLosses(side)));
#endif

      }

   }

   return False;
}

/*
 * See if a victory has been achieved
 * A victory is achieved when one side's morale reaches 0
 */

Boolean CampaignBattleUtil::checkForVictor()
{
   // if we already have one
   if(d_battle->hasVictor())
      return True;

   // otherwise...
   for(Side side = 0; side < scenario->getNumSides(); side++)
   {
      if(d_battle->currentMorale(side) <= 0)
      {
         /*
         * if both sides have a morale of 0 then the one with the least
         * negative morale is the victor
         */

         Side otherSide = getOtherSide(side);

         if(d_battle->currentMorale(otherSide) <= 0)
         {
            Attribute ourNegative = d_battle->negativeMorale(0);
            Attribute theirNegative = d_battle->negativeMorale(1);

            if(ourNegative > theirNegative)
            {
               d_battle.current()->victor(otherSide);
            }
            else if(ourNegative < theirNegative)
            {
               d_battle.current()->victor(side);
            }

            // if negative morale is even then pick a random to win or lose
            else
            {
               d_battle.current()->victor((CRandom::get(2) == 1) ? side : otherSide);
            }
         }
         else
         {
            d_battle.current()->victor(otherSide);
         }

         break;
      }
   }

   return(d_battle->victor() != SIDE_Neutral);
}

void CampaignBattleUtil::doEndOfFighting()
{
   // if this is a new battle day
   if(d_battle->isStoppedForNight())
   {
      //#ifdef DEBUG
      setSPValues();
      //#endif

      // this battle was won during round 4 of the previous day
      if(d_battle->hasVictor())
         doWonBattle(d_battle->victor());

      // set withdrawing side(s) on the way
      for(Side s = 0; s < scenario->getNumSides(); s++)
      {
         if(stillInBattle(s) && d_battle->endOfDayMode(s) == EndOfDayMode::Withdraw)
         {
            doWithdrawing(s);
         }
      }
   }

   // or we are in rounds 1 to 4
   else
   {
      ASSERT(d_battle->hasVictor());
      ASSERT(stillInBattle(d_battle->victor()));

      // do won battle routine only in rounds 1 to 3 or this is a brushaside
      if(d_timeOfDay <= BattleDay::Afternoon || d_battle->brushAside())
         doWonBattle(d_battle->victor());

      Side losingSide = getOtherSide(d_battle->victor());
      // make sure loser still has units in battle (i.e. he may have been wiped-out)
      //--- Modified SWG: 30Aug99 : Still want a results display
      // if(stillInBattle(losingSide))
         doLostBattle(losingSide);
   }
}

/*
 * See if one or the other (or both) sides is withdrawing
 */

Boolean CampaignBattleUtil::checkForWithdraw()
{
   for(Side side = 0; side < scenario->getNumSides(); side++)
   {
      if(d_battle->endOfDayMode(side) == EndOfDayMode::Withdraw)
      {
         Side otherSide = getOtherSide(side);
         // set other side as victor unless he is also withdrawing
         if(d_battle->endOfDayMode(otherSide) == EndOfDayMode::Continue)
            d_battle.current()->victor(otherSide);

         return True;
      }
   }

   return False;
}

/*
 * test to break contact with victorious enemy if unit is withdrawing or retreating
 * in the 4 round of the day.
 * If so victorious enemy may temporarly lose all contact with beaten foe
 * Test is: for every morale below zero, there is a 5% chance of not losing contact
 */

Boolean CampaignBattleUtil::lostContact(Side ourSide)
{
   if(CampaignOptions::get(OPT_FogOfWar))
   {
      int percent = minimum(100, d_battle->negativeMorale(ourSide) * 5);

#ifdef DEBUG
      cbLog.printf("Testing for losing contact with enemy. Chance of not losing contact is %d", percent);
#endif
      return (CRandom::get(100) >= percent);
   }

   return False;
}

/*
 * This side has lost the battle.
 * All units morale gets set to 0
 */

void CampaignBattleUtil::doLostBattle(Side side)
{

  //--- Added SWG: 30Aug99
  // If all units dead then skip the calculations

  if (stillInBattle(side))
  {

   /*
   * Apply tactical pursuit. This is a freebie for the victor
   * to inflict more casaulties on the loser
   */

   doTacticalPursuit();

   //#ifdef DEBUG   // so we can get final sp counts
   //#endif

   // set cinc's unit as possible pursuit unit
   ASSERT(d_battle->cinc(side) != NoLeader);
   d_battle.current()->pursuitTarget(d_battle->cinc(side)->getCommand());

   // lower nation morale. If Cinc is not from a major nation
   // lower morale of default nation
   /*
   * Note: Ben has not designed proper modifiers for this yet
   */

   applySideVP();

   /*
   * Go through and set all units morale to 0
   */

   {
      CampaignBattlePtr battlePtr = d_battle.current();

      CampaignBattleUnitListIterW iter(&battlePtr->units(side));

      while(++iter)
      {
         CampaignBattleUnit* bu = iter.current();
         //   bu->outOfIt(True);

         CommandPosition* cp = d_campData->getCommand(bu->getUnit());

         UnitIter uiter(&d_campData->getArmies(), bu->getUnit());
         while(uiter.next())
         {
            CommandPosition* cpChild = uiter.currentCommand();
            cpChild->setMorale(0);

            // Aggression is set to timid, and defensive posture
            cpChild->setAggressionValues(Orders::Aggression::Timid, Orders::Posture::Defensive);
            cpChild->aggressionLocked(True);
         }

         // if unit is a garrison, then don;t do anything, it goes back into its fortress
         if(cp->isGarrison())
            continue;

         // Aggression is set to timid, and defensive posture
         cp->setAggressionValues(Orders::Aggression::Timid, Orders::Posture::Defensive);

         // a routing unit force marches
         cp->setForceMarch(True);

         // test to see if any SP's are promoted due to battle experience
         spPromotionCheck(bu->getUnit(), False);

         // test to see if any heavy equipment is lost
         checkForEquipmentLoss(bu->getUnit());

         // test to see if Top-level leader improves any
         //   leaderImprovesCheck(

         // reset order
         cp->getOrders().getCurrentOrder()->makeHold();
         CP_ModeUtil::setMode(d_campData, bu->getUnit(), CampaignMovement::CPM_StartingRetreat);
      }

      // for final tally
      setSPValues();

      // rewind and set all units to out of battle
      iter.rewind();
      while(++iter)
      {
         CampaignBattleUnit* bu = iter.current();
         bu->outOfIt(True);
      }
   }

   // do pursuit
   ICommandPosition pCPI = d_battle->pursuitTarget();
   if((d_battle->posture(d_battle->victor()) == Orders::Posture::Offensive) &&
      (pCPI != NoCommandPosition) &&
      (pCPI->isSeen()))
   {
      doStrategicPursuit();
   }
  }   // Added SWG: 30Aug99

   showBattleResults(side);
}

void CampaignBattleUtil::applySideVP()
{
   for(Side side = 0; side < scenario->getNumSides(); side++)
   {
      //    Nationality n = d_battle->cinc(side)->getNation();
      if(side != d_battle->victor())
         continue;

      // adjust according to start size
      // if this is the victor we use other side
      Side s = getOtherSide(side);
      SPCount startSP = (d_battle->units(s).nStartInfantry() +
         d_battle->units(s).nStartCavalry() +
         d_battle->units(s).nStartArtillery() +
         d_battle->units(s).nStartOther());

      SPCount nowSP = (d_battle->units(s).nInfantry() +
         d_battle->units(s).nCavalry() +
         d_battle->units(s).nArtillery() +
         d_battle->units(s).nOther());

      // get start size
      ForceSize::Value fs = CombatTableUtil::getForceSize(startSP);
      // temporary table. TODO: get from scenario tables

      // get our loss percentage
      int lossPercent = (startSP > 0) ? MulDiv(startSP - nowSP, 100, startSP) : 100;
      enum
      {
         Light,
         Significant,
         Heavy,
         VeryHeavy,
         Losses_HowMany
      } losses;

      if(lossPercent <= 5)
         losses = Light;
      else if(lossPercent <= 20)
         losses = Significant;
      else if(lossPercent <= 40)
         losses = Heavy;
      else
         losses = VeryHeavy;

      // victory points achieved for winning side
      static UBYTE s_table[ForceSize::HowMany][Losses_HowMany] = {
      //   LC   SC  HC  VHC
          {  0, 0, 0, 0 },        // Small Force
          {  0, 0, 1, 2 },        // Medium Force
          {  0, 1, 2, 3 },        // Large Force
          {  1, 2, 3, 4 }         // VeryLarge Force
      };

      d_campData->addVictoryLevel(s_table[fs][losses], side);
#ifdef DEBUG
      cbLog.printf("%s get %d victory points", scenario->getSideName(side), s_table[fs][losses]);
#endif
#if 0
         // apply moraleChange / 2 to all allied nations
         for(Nationality an = 0; an < scenario->getNumNations(); an++)
      {
         if((d_campData->getArmies().getNationAllegiance(an) == side) &&
            (scenario->getNationType(an) == MajorNation))
         {
            Attribute nMorale = d_campData->getNationMorale(an);

            // adjust attribute
            Attribute moraleChange = MulDiv(nMorale, s_table[fs][losses], base * 100);// / 2;

            // if allied then / by 2
            if(an != n)
               moraleChange /= 2;

            nMorale = (victor) ? minimum(Attribute_Range, nMorale + moraleChange) :
            maximum(0, nMorale - moraleChange);

            d_campData->setNationMorale(an, nMorale);
         }
      }
#endif
   }

   // if(d_campGame)
   //       d_campGame->moraleUpdated();
}

/*
 * Set reconcentration times for winning side
 */

TimeTick CampaignBattleUtil::timeToReconcentrate(Side s, SPCount spCount)
{
   int hours = 0;

   if(!d_battle->brushAside())
   {
      const Table1D<UBYTE>& dTable = scenario->getDeploymentTimeTable();
      SPCount count = CombatTableUtil::getForceSize(spCount);
      hours = dTable.getValue(count);

#ifdef DEBUG
      cbLog.printf("Time to reconcentrate (unmodified) is %d hours", hours);
#endif

      // if we have a defensive posture, reconcentrating takes 2 hours longer
      if(d_battle->posture(s) == Orders::Posture::Defensive)
         hours += 2;
      // else it only takes half as long
      else
         hours /= 2;

      // if subordinates rating greater than leader staff adds 2 hours
      ILeader leader = d_battle->cinc(s);
      // if(leader->getStaff() < d_campData->getArmies().getCommandSubordination(leader->getCommand()))
      if(leader->getStaff() < d_campData->getArmies().getCommandSubordination(leader))
         hours += 2;
   }

   return hours;
}

void CampaignBattleUtil::doWonBattle(Side side)
{
   int hours = 0;

   if(!d_battle->brushAside())
   {
      hours = timeToReconcentrate(side, d_battle->spCount(side) - d_battle->getLosses(side));

   }

   CampaignBattlePtr battlePtr = d_battle.current();

   battlePtr->tillWhen(d_campData->getTick() + HoursToTicks(hours));

#ifdef DEBUG
   cbLog.printf("Time to reconcentrate (modified) is %d hours", hours);
#endif
   battlePtr->turnMode(CampaignBattle::BattleTurnMode::Reconcentrating);
}

void CampaignBattleUtil::doWithdrawing(Side side)
{
   /*
   * A force withdrawing recovers morale to 55% of starting total
   * If already over 55% then new morale is the average between starting morale and 55%
   */

   {
      CampaignBattlePtr battlePtr = d_battle.current();

      CampaignBattleUnitListIterW iter(&battlePtr->units(side));

      while(++iter)
      {
         CampaignBattleUnit* bu = iter.current();

         //   bu->outOfIt(True);
         CommandPosition* cp = d_campData->getCommand(bu->getUnit());

         UnitIter uiter(&d_campData->getArmies(), bu->getUnit());

         // get % currentMorale of startMorale
         Attribute newMorale = 0;
         Attribute startMorale = d_battle->startMorale(side);
         Attribute currentMorale = d_battle->currentMorale(side);
         ASSERT(currentMorale <= startMorale);

         int baseRecovery = 55;
         ILeader cinc = d_battle->cinc(side);

         /*
         * if cinc has charisma over 200, base recovery is +5
         */

         if(cinc->getCharisma() >= 200)
            baseRecovery += 5;

         // if cinc staff is less than units subordination (baseRecovery is -5)
         // if(cinc->getStaff() < d_campData->getArmies().getCommandSubordination(cinc->getCommand()))
         if(cinc->getStaff() < d_campData->getArmies().getCommandSubordination(cinc))
            baseRecovery -= 5;

         // 30% chance of -5, or 30% chance of +5 to baseRecovery
         int dieRoll = CRandom::get(10);
         if(dieRoll < 3)
            baseRecovery += 5;
         else if(dieRoll < 6)
            baseRecovery -= 5;

         int percent = MulDiv(currentMorale, 100, startMorale);
         if(percent < 56)
         {
            newMorale = static_cast<Attribute>(MulDiv(baseRecovery, startMorale, 100));
         }
         else
         {
            newMorale = static_cast<Attribute>(MulDiv(baseRecovery, startMorale, 100));
            newMorale = static_cast<Attribute>((newMorale + startMorale) / 2);
         }

         while(uiter.next())
         {
            CommandPosition* cpChild = uiter.currentCommand();
            cpChild->setMorale(newMorale);
         }

#ifdef DEBUG
         cbLog.printf("%s is withdrawing. Morale is %d",
            (const char*)d_campData->getUnitName(bu->getUnit()).toStr(), static_cast<int>(newMorale));
#endif

         // test to see if any SP's are promoted due to battle experience
         spPromotionCheck(bu->getUnit(), False);

         // if unit is a garrison, then don;t do anything, it goes back into its fortress
         if(cp->isGarrison())
            continue;

         // reset order
         cp->getOrders().getCurrentOrder()->makeHold();
         CP_ModeUtil::setMode(d_campData, bu->getUnit(), CampaignMovement::CPM_StartingWithdraw);
      }

      // for final tally
      setSPValues();

      // rewind and set all units to out of battle
      iter.rewind();
      while(++iter)
      {
         CampaignBattleUnit* bu = iter.current();
         bu->outOfIt(True);
      }
   }

   /*
   * There must NOT be a write lock by here!
   */

   showBattleResults(side);
}

/*
 * Calculate tactical pursuit losses
 * Note: Tactical Pursuit is not to be confused with Pursuit after battle
 *       Tactical Pursuit is merely a way for the victor to inflict additional losses
 *
 */

void CampaignBattleUtil::doTacticalPursuit()
{
#ifdef DEBUG
   cbLog.printf("============== Doing Tactical Pursuit ===========");
#endif
   ASSERT(d_battle->hasVictor());

   CampaignBattlePtr battlePtr = d_battle.current();

   battlePtr->resetLosses();

   Side winningSide = d_battle->victor();
   Side losingSide = getOtherSide(winningSide);
   ILeader cinc = d_battle->cinc(winningSide);
   CampaignWeather& weather = d_campData->getWeather();
   ASSERT(cinc != NoLeader);
   ASSERT(d_battle->currentMorale(losingSide) <= 0);

   /*
   * Calculate pursuit points:
   *       each light cavalry sp(of the winning side) is worth 1 point
   *       each heavy cavalry sp is worth .5 points
   *       each mounted artillery sp is worth .3 points
   *       all other sp are worth .1 points
   *
   * use a base of 10
   */

   ULONG pursuitPoints = 0;
   CampaignBattleUnitListIterW iter(&battlePtr->units(winningSide));
   while(++iter)
   {
      CampaignBattleUnit* bu = iter.current();
      const ICommandPosition& cpi = bu->getUnit();

      /*
      * Test each unit in command
      */

      UnitIter uIter(&d_campData->getArmies(), cpi);
      while(uIter.next())
      {
         // add up pursuit points
         const ICommandPosition& supCPI = uIter.current();
         ULONG lPoints = 0;
         StrengthPointIter spIter(&d_campData->getArmies(), supCPI);
         while(++spIter)
         {
            StrengthPoint* sp = d_armies->getStrengthPoint(spIter.currentISP());
            const UnitTypeItem& item = d_campData->getUnitType(sp->getUnitType());

            // if light cavalry
            if(item.isLightCavalry())
               lPoints += 10;
            // if other cavalry
            else if(item.getBasicType() == BasicUnitType::Cavalry)
               lPoints += 5;
            // if mounted artillery
            else if(item.isMounted() && item.getBasicType() == BasicUnitType::Artillery)
               lPoints += 3;
            // all others
            else
               lPoints += 1;
         }  // ENDWHILE spIter

         // #if defined(NOT_DEMO_VERSION)
         // if unit or any in its chain of command have
         // pursuit orders and a fatigue-level of over 110, pursuit points X 1.5
         if(supCPI->getFatigue() >= 65)
         {
            Boolean isPursuing = False;

            // if top-level unit has pursue they all do
            if(supCPI->getCurrentOrder()->getPursue() ||
               cpi->getCurrentOrder()->getPursue())
            {
               isPursuing = True;
            }

            // otherwise, see if us or any of our higher commands have pursue
            else
            {
               ICommandPosition cpParent = supCPI->getParent();
               while(cpParent != cpi && !cpParent->sameRank(Rank_President))
               {
                  if(cpParent->getCurrentOrder()->getPursue())
                  {
                     isPursuing = True;
                     break;
                  }

                  cpParent = cpParent->getParent();
               }  // ENDWHILE
            } // ENDIF

            // if pursuing, X 1.5
            if(isPursuing)
            {
               if(supCPI->getFatigue() > 110)
                  lPoints *= 1.5;

               // reduce fatigue attribute by (number of rounds fought X 10) - 10
               // can reduce to no less than 65
               const int roundsPerDay = 4;
               const int nRounds = ((maximum(0, d_battle->daysOfBattle() - 1)) * roundsPerDay) + d_battle->battleRound();
               //        Attribute fatigue = maximum(65, supCPI->getFatigue() - ((nRounds * 10) - 10));
               //          supCPI->setFatigue(fatigue);
               d_campData->getArmies().applyFatigue(supCPI, -((nRounds * 10) - 10));
            } // ENDIF pursuing
         } // ENDIF fatigue > 65
         // #endif // defined(NOT_DEMO_VERSION)

         pursuitPoints += lPoints;
      } // ENDWHILE uiter
   } // ENDWHILE unitlistIter

   pursuitPoints /= 10;

#ifdef DEBUG
   cbLog.printf("Pursuit Points for %s = %ld",
      scenario->getSideName(winningSide), pursuitPoints);
#endif

   /*
   * Roll dice. Get a value from 0 - 12
   */

   const int dieRollValues = 12;
   int dieRoll = CRandom::get(dieRollValues);

#ifdef DEBUG
   cbLog.printf("Raw die roll = %d", dieRoll);
#endif

   /*
   * Die roll modifiers
   */

   int modifyBy = 0;

   // if winner has a cavalry specialist (+2)
   if(d_battle->hasCavalrySpecialist(winningSide))
   {
      modifyBy += 2;
   }

   // winners fatigue is 170 or higher (+2)
   if(d_battle->fatigue(winningSide) >= 170)
   {
      modifyBy += 2;
   }

   // ourCavalry outnumbers theirs by 2 to 1 or more (+2)
   if(d_battle->cavalryCount(winningSide) >= (d_battle->cavalryCount(losingSide) * 2))
   {
      modifyBy += 2;
   }

   // Winning cinc aggression is 170+ (+1)
   if(cinc->getAggression() >= 170)
   {
      modifyBy++;
   }

   // cinc initiativ is 170+ (+1)
   if(cinc->getInitiative() >= 170)
   {
      modifyBy++;
   }

   // cinc charisma is 170+ (+1)
   if(cinc->getCharisma() >= 170)
   {
      modifyBy++;
   }

   // for heavy mud (-1)
   if(weather.getGroundConditions() == CampaignWeather::VeryMuddy)
   {
      modifyBy--;
   }

   // for heavy rain (-1)
   if(weather.getWeather() == CampaignWeather::HeavyRain)
   {
      modifyBy--;
   }

   // enemy retreating from chokepoint, wooded, or wooded hilly terrain (-2)
   ICommandPosition loser = d_battle->cinc(losingSide)->getCommand();
   const Town& t = d_campData->getTown(loser->getCloseTown());
   const TerrainTypeItem& terrain = d_campData->getTerrainType(t.getTerrain());

   if((loser->atTown() && terrain.isChokePoint()) ||
      (terrain.getGroundType() == Terrain::Wooded) ||
      (terrain.getGroundType() == Terrain::WoodedHilly))
   {
      modifyBy -= 2;
   }

   // winners fatigue is less than 90 (-2)
   if(d_battle->fatigue(winningSide) < 90)
   {
      modifyBy -= 2;
   }

   // winners morale is less than 65 (-2)
   if(d_battle->currentMorale(winningSide) < 65)
   {
      modifyBy -= 2;
   }

   // winners cavalry outnumbered 2 to 1 by enemy (-2)
   if(d_battle->cavalryCount(winningSide) <= d_battle->cavalryCount(losingSide) / 2)
   {
      modifyBy -= 2;
   }

   // apply modifier to die roll
   dieRoll = clipValue(dieRoll + modifyBy, 0, (dieRollValues-1));
#ifdef DEBUG
   cbLog.printf("Modify die roll by %d, new die Roll = %d", modifyBy, dieRoll);
#endif

   // get loss % (as a percent of pursuitPoints)
   const Table1D<int>& table = scenario->getTacticalPursuitTable();
   int percentLoss = table.getValue(dieRoll);

#ifdef DEBUG
   cbLog.printf("%%loss of pursuit points before modifier = %d", percentLoss);
#endif

   /*
   * Modifiers to percent loss
   */

   // if the winner finishes in a defensive posture ( /3 or /4 )
   if(d_battle->posture(winningSide) == Orders::Posture::Defensive)
   {
      if(d_timeOfDay == BattleDay::Evening || d_timeOfDay == BattleDay::Night)
         percentLoss /= 4;
      else
         percentLoss /= 3;
   }

   // if its round 4 and offensive posture ( /1.5 )
   else if(d_timeOfDay == BattleDay::Evening || d_timeOfDay == BattleDay::Night)
   {
      percentLoss /= 1.5;
   }

#ifdef DEBUG
   cbLog.printf("Final %%loss of pursuit points = %d", percentLoss);
#endif

   /*
   * Apply losses
   */

   if(percentLoss > 0)
   {
      int n = minimum(pursuitPoints * percentLoss, 32767);
      SPLoss loss(0, static_cast<UWORD>(n), 100);

#ifdef DEBUG
      cbLog.printf("other side loses before %d.%x",
         loss.getInt(), loss.getFraction());
#endif

      d_battle.current()->loss(losingSide, loss);
      implementLosses();
   }
}

void CampaignBattleUtil::doStrategicPursuit()
{
#ifdef DEBUG
   cbLog.printf("============== Doing Strategic Pursuit ===========");
#endif

   ICommandPosition pCpi = d_battle->pursuitTarget();
   ASSERT(pCpi != NoCommandPosition);
   ASSERT(pCpi->isSeen());

   CampaignBattlePtr battlePtr = d_battle.current();

   CampaignBattleUnitListIterW iter(&battlePtr->units(d_battle->victor()));
   while(++iter)
   {
      const ICommandPosition& topCPI = iter.current()->getUnit();

      ASSERT(d_battle->posture(topCPI->getSide()) == Orders::Posture::Offensive);
      ASSERT(topCPI->getSide() == d_battle->victor());

#ifdef DEBUG
      cbLog.printf("Testing %s and its units for Pursuit orders against %s",
         topCPI->getName(), pCpi->getName());
#endif

      /*
      * Go through each unit in command, from bottom up at
      * set pursuit for any unit with orders
      */

      RankEnum rank = Rank_Division;
      do
      {
         UnitIter uIter(&d_campData->getArmies(), topCPI);
         Boolean increment = True;

         while(uIter.next())
         {
            const ICommandPosition& cpi = uIter.current();
            if((cpi->sameRank(rank)) &&
               (cpi->getCurrentOrder()->getPursue()))
            {
               // set to pursuing
               cpi->hasPursuitTarget(True);

               // if this is not top unit, then detach
               if(cpi != topCPI)
               {
#ifdef DEBUG
                  cbLog.printf("Detaching %s to pursue", cpi->getName());
#endif
                  d_campData->getArmies().detachCommand(cpi);

                  // reset morale etc.
                  doVictoriousUnit(cpi);
                  cpi->setAggressionValues(Orders::Aggression::Attack, Orders::Posture::Offensive);
                  cpi->getCurrentOrder()->setTarget(pCpi);
                  cpi->getCurrentOrder()->setAggressLevel(Orders::Aggression::Attack);
                  cpi->setForceMarch(True);

                  // plot route to enemy
                  CampaignRouteUtil::plotUnitRoute(d_campData, cpi);

                  // set time to reconcentrate
                  SPCount c = d_campData->getArmies().getUnitSPCount(cpi, True);
                  int hours = timeToReconcentrate(cpi->getSide(), c);
                  cpi->forceHold(d_campData->getTick() + HoursToTicks(hours));

#ifdef DEBUG
                  cbLog.printf("will take %d hours to form up", hours);
#endif
                  // don't inc rank, we must go through rank again to reset iter
                  increment = False;
               }
               else
                  break;
            }
         }

         // Note: to increment rank, we really need to decrement
         if(increment)
            DECREMENT(rank);

      } while(rank >= topCPI->getRankEnum());
   }
}

/*
 *  This is called while victor is reforming
 */

void CampaignBattleUtil::doAfterBattle()
{
   if(d_battle->timeElapsed(d_campData->getTick()))
   {
      d_battle.current()->turnMode(CampaignBattle::BattleTurnMode::BattleOver);
      doBattleOver();
   }
}

Attribute CampaignBattleUtil::calcVictorMorale()
{
   ASSERT(d_battle->victor() != SIDE_Neutral);
   ILeader cinc = d_battle->cinc(d_battle->victor());
   int multiplier = (d_battle->currentMorale(d_battle->victor()) > 65) ? 90 : 80;

   // modifier to multiplier

   // if cinc charisma is over 200   (+5)
   if(cinc->getCharisma() >= 200)
      multiplier += 5;

   // if combined subordination is greater than leaders staff (-5)
   // if(cinc->getStaff() < d_campData->getArmies().getCommandSubordination(cinc->getCommand()))
   if(cinc->getStaff() < d_campData->getArmies().getCommandSubordination(cinc))
      multiplier -= 5;

   // a 30% chance of +5 and a 30% chance of -5
   int dieRoll = CRandom::get(10);

   if(dieRoll < 3)
      multiplier += 5;
   else if(dieRoll < 6)
      multiplier -= 5;

   return static_cast<Attribute>(MulDiv(d_battle->startMorale(d_battle->victor()), multiplier, 100));
}

void CampaignBattleUtil::doVictoriousUnit(const ICommandPosition& cpi)
{
   Attribute newMorale = calcVictorMorale();

   UnitIter uiter(&d_campData->getArmies(), cpi);
   while(uiter.next())
   {
      const ICommandPosition& cpi = uiter.current();
      cpi->setMorale(newMorale);

      // test to see if any leader's improve due to battle experience, done for victors only
      leaderImprovesCheck(cpi);
   }

   d_campData->getArmies().averageCommandMorale(cpi);

#ifdef DEBUG
   CommandPosition* cp = d_campData->getCommand(cpi);

   cbLog.printf("Morale for %s ==> %d",
      (const char*)d_campData->getUnitName(cpi).toStr(),
      cpi->getMorale());
#endif


   // test to see if any SP's are promoted due to battle experience
   spPromotionCheck(cpi, True);

   // if unit is a garrison, then don;t do anything, it goes back into its fortress
   if(!cpi->isGarrison())
   {
      CP_ModeUtil::setMode(d_campData, cpi, CampaignMovement::CPM_AfterWinningBattle);

      // Check for fatigue limits, done for victors only and if not pursuing
      if(!cpi->hasPursuitTarget())
         FatigueProc::procUnitFatigueLimit(d_campGame, cpi);
   }
}

/*
 * Battle really is over
 */

void CampaignBattleUtil::doBattleOver()
{
   ASSERT(d_battle->battleOver());
   ASSERT(d_battle->victor() != SIDE_Neutral);
   ASSERT(d_battle->victor() < scenario->getNumSides());

   ASSERT(d_battle->cinc(d_battle->victor()) != NoLeader);
   Leader* cinc = d_campData->getLeader(d_battle->cinc(d_battle->victor()));

   /*
   * Recover morale. If current morale is > 65 we recover 90% of days starting total
   * otherwise we recover 80%
   */

   {
      CampaignBattlePtr battlePtr = d_battle.current();

      CampaignBattleUnitListIterW iter(&battlePtr->units(d_battle->victor()));
      while(++iter)
      {
         const ICommandPosition& cpi = iter.current()->getUnit();
         doVictoriousUnit(cpi);

         // plot route to enemy if we're pursuing

         //---- Modified SWG: 14Jul99
         //---- Modified PJS: 2Sept99
         // Note: We must check for unit->hasPursuitTarget() not battle->hasPursuitTarget()
         // as a test is performed in doStrategicPursuit() which sets the
         // unit->hasPursuitTarget to true ONLY if the test is passed
         // (i.e. checking for pursuit advanced orders). By checking only the battle->hasPursuitTarget()
         // units will ALWAYS pursue regardless of orders
         if(cpi->hasPursuitTarget())
//         if(d_battle->hasPursuitTarget())
         {
            if(d_battle->pursuitTarget() == NoCommandPosition)
               cpi->hasPursuitTarget(False);
            else
            {
               cpi->setAggressionValues(Orders::Aggression::Attack, Orders::Posture::Offensive);

               // reset morale etc.
//               doVictoriousUnit(cpi);
               cpi->setAggressionValues(Orders::Aggression::Attack, Orders::Posture::Offensive);
               cpi->getCurrentOrder()->setTarget(d_battle->pursuitTarget());
               cpi->getCurrentOrder()->setAggressLevel(Orders::Aggression::Attack);
               cpi->setForceMarch(True);

               // plot route to enemy
               CampaignRouteUtil::plotUnitRoute(d_campData, cpi);
            }
         }

#ifdef DEBUG
         cbLog.printf("%s is Victorious", (const char*)d_campData->getUnitName(iter.current()->getUnit()).toStr());
         cbLog.printf("Victor reformed at %s", d_campData->asciiTime());
#endif
      }
   }

   showBattleResults(d_battle->victor());
   d_battle.current()->resetUnits();

   ASSERT(d_battle->units(d_battle->victor()).entries() == 0);
   ASSERT(d_battle->units(getOtherSide(d_battle->victor())).entries() == 0);
}

/*
 * See if any sp's get promoted due to battle experience
 */

void CampaignBattleUtil::spPromotionCheck(const ICommandPosition& cpi, Boolean victor)
{
   ASSERT(cpi != NoCommandPosition);

   /*
   * Go through every SP and test for promotion
   *
   * If victorious, promotable SP's have a 25% chance of being promoted
   * If not, then 15% chance
   */

#ifdef DEBUG
   cbLog.printf("----- Testing for SP Promotion for %s", (const char*)d_campData->getUnitName(cpi).toStr());
#endif


   SPIter iter(d_armies, cpi);
   while(++iter)
   {
      // const StrengthPoint* sp = iter.current();
      StrengthPoint* sp = d_armies->getStrengthPoint(iter.currentISP());

      const UnitTypeItem& item = d_campData->getUnitType(sp->getUnitType());

      if(item.canPromote())
      {
         ASSERT(item.promoteTo() != NoUnitType);
         ASSERT(item.promoteTo() < d_campData->getMaxUnitType());

         int chance = (victor) ? 25 : 15;

         if(CRandom::get(100) <= chance)
         {
            /*
            * If here then our SP has been promoted
            */

#ifdef DEBUG
            ICommandPosition cpi = iter.currentICP();
            CommandPosition* cp = d_armies->getCommand(cpi);

            const UnitTypeItem& newItem = d_campData->getUnitType(item.promoteTo());

            cbLog.printf("SP %d(%s) belonging to %s is promoted to %d(%s)",
               static_cast<int>(sp->getUnitType()),
               item.getName(),
               cp->getNameNotNull(),
               static_cast<int>(item.promoteTo()),
               newItem.getName());
#endif

            sp->setUnitType(item.promoteTo());

         }
      }
   }
}

/*
 * See if leader improves after a winning battle
 *
 * A leader has a %Attribute chance of NOT having that attribute improve.
 * For now improvment is 5
 */

//const int improveBy = 5;

class AlterLeaderModifier {
   public:
   enum Type
   {
      Initiative,
      Staff,
      Charisma,
      TacticalAbility,

      HowMany
   };
};

Boolean adjustAttribute(int& value, AlterLeaderModifier::Type whichAttrib, Boolean topLevel)
{
   int chance = maximum(0, 100 - (MulDiv(value, 100, Attribute_Range)));

   /*
   * chance gets halved if this is not a top-level unit
   */

   if(!topLevel)
      chance *= .5;

   if(CRandom::get(Attribute_Range) < chance)
   {
      /*
      * Leader improves by %(table value) of 255-value
      */

      const Table1D<UWORD>& table = scenario->getAlterLeaderTable();

      int improveBy = MulDiv(Attribute_Range-value, table.getValue(whichAttrib), 1000);

      value += improveBy;
      value = clipValue<Attribute>(value, 0, Attribute_Range);
      return True;
   }

   return False;
}

void CampaignBattleUtil::leaderImprovesCheck(const ICommandPosition& cpi)
{
   ASSERT(cpi != NoCommandPosition);

   ILeader leader = d_campData->getArmies().getUnitLeader(cpi);
   Boolean topLevel = (cpi->getParent()->getRankEnum() == Rank_President);

#ifdef DEBUG
   cbLog.printf("=======Adjusting leader abilities for %s", (const char*)d_campData->getUnitName(cpi).toStr());
#endif

   int newValue = leader->getCharisma();
   if(adjustAttribute(newValue, AlterLeaderModifier::Charisma, topLevel))
   {
#ifdef DEBUG
      cbLog.printf("Charisma has improved from %d to %d",
         static_cast<int>(leader->getCharisma()), newValue);
#endif
      leader->setCharisma(static_cast<Attribute>(newValue));
   }

   newValue = leader->getInitiative();
   if(adjustAttribute(newValue, AlterLeaderModifier::Initiative, topLevel))
   {
#ifdef DEBUG
      cbLog.printf("Initiative has improved from %d to %d",
         static_cast<int>(leader->getInitiative()), newValue);
#endif

      /*
      * if initiative improves, there is a chance that leaders'
      * rank-ceiling (Command Ability) will be incremented
      *
      * Note, all subordinates check also
      */

      enum OutCome
      { Won, Lost, OutCome_HowMany
      };
      OutCome outCome = (d_battle->victor() == leader->getSide()) ? Won : Lost;

      // Note: move table to scenario file
      static const int s_table[Rank_HowMany][OutCome_HowMany] = {
        {  0,  0 },
        {  0,  0 },
        {  0,  0 },
        { 16, 24 },
        { 12, 18 },
        { 10, 15 }
    };

      const int d = s_table[leader->getRankLevel().getRankEnum()][outCome];

      if(d > 0)
      {
         /*
         * Chance of improving is initiative / d if CinC
         * otherwise its (initiative / 2) / d
         */

         int chance = (d_battle->cinc(leader->getSide()) == leader) ?
            leader->getInitiative() / d : (leader->getInitiative() / 2) / d;

         if(CRandom::get(100) < chance)
         {
            leader->getRankLevel().promote();

            /*
            * If he is promoted, then subordination gets multiplied
            * by table value
            */

            // Note: move table to scenario file
            static const int s_mTable[Rank_HowMany] = {
            10,
            10,
            10,
            12,
            15,
            20
        };

            const int m = s_mTable[leader->getRankLevel().getRankEnum()];

            Attribute newSub = static_cast<Attribute>(minimum(Attribute_Range, MulDiv(leader->getSubordination(), m, 10)));
            leader->setSubordination(newSub);

#ifdef DEBUG
            cbLog.printf("%s has been promoted to %s",
               leader->getName(), leader->getRankLevel().getRankName());
#endif
         }
      }

      leader->setInitiative(static_cast<Attribute>(newValue));
   }

   newValue = leader->getStaff();
   if(adjustAttribute(newValue, AlterLeaderModifier::Staff, topLevel))
   {
#ifdef DEBUG
      cbLog.printf("Staff has improved from %d to %d",
         static_cast<int>(leader->getStaff()), newValue);
#endif
      leader->setStaff(static_cast<Attribute>(newValue));
   }

   newValue = leader->getTactical();
   if(adjustAttribute(newValue, AlterLeaderModifier::TacticalAbility, topLevel))
   {
#ifdef DEBUG
      cbLog.printf("Tactical Ability has improved from %d to %d",
         static_cast<int>(leader->getTactical()), newValue);
#endif
      leader->setTactical(static_cast<Attribute>(newValue));
   }

}

/*
 * Test to see if retreating unit loses any heavy equipment
 */

void CampaignBattleUtil::checkForEquipmentLoss(const ICommandPosition& cpi)
{
   ASSERT(cpi != NoCommandPosition);
   CommandPosition* cp = d_campData->getCommand(cpi);

#ifdef DEBUG
   cbLog.printf("======= Testing for equipment loss for %s", (const char*)d_campData->getUnitName(cpi).toStr());
#endif

   /*
   * First, we need to see if unit has any heavy equipment
   */

   SPCount nOther = d_campData->getArmies().getNType(cpi, BasicUnitType::Special, False);
   Attribute negativeMorale = d_battle->negativeMorale(cp->getSide());

   if(nOther > 0 && negativeMorale > 0)
   {
#ifdef DEBUG
      cbLog.printf("Unit has %d special-type SP's, negativeMorale = %d",
         static_cast<int>(nOther), static_cast<int>(negativeMorale));
#endif

      /*
      * Get chance of something happening
      *
      * If round 1-3, loss is automatic, and this is the chance
      * of the enemy capturing the equipment
      *
      * If round 4, then this is the chance of losing the equipment. If lost
      * then there is a 50% chance of the enemy capturing it
      *
      * chance = negativeMorale * 2
      */

      Boolean lost = False;
      Boolean captured = False;

      int chance = minimum((negativeMorale*2), 100);

      if(d_timeOfDay == BattleDay::Evening)
      {
#ifdef DEBUG
         cbLog.printf("chance of losing equipment = %d", chance);
#endif

         lost = (CRandom::get(100) < chance);
         if(lost)
            captured = (CRandom::get(2) == 1);
      }
      else
      {
#ifdef DEBUG
         cbLog.printf("chance of enemy capturing equipment = %d", chance);
#endif

         lost = True;
         captured = (CRandom::get(100) < chance);
      }

      if(lost)
      {
#ifdef DEBUG
         cbLog.printf("Equipment will be %s", (captured) ? "captured" : "lost");
#endif

         /*
         * If here, all heavy equipment will be lost, either captured or destroyed
         * Go throught each Special-Type SP and test to see if captured or destroyed
         */

         while(nOther--)
         {
            ICommandPosition specialCPI = NoCommandPosition;
            ISP isp = NoStrengthPoint;

            Boolean found = False;

            /*
            * First find a special type
            */

            for(int t = 0; t < SpecialUnitType::HowMany; t++)
            {
               found = d_campData->getArmies().findSpecialType(cpi, specialCPI, isp, static_cast<SpecialUnitType::value>(t));
               if(found)
                  break;
            }

            ASSERT(found);
            ASSERT(isp != NoStrengthPoint);
            ASSERT(specialCPI != NoCommandPosition);

            /*
            * if captured create new sps of the same type and attach to
            * other side
            */

            if(captured)
            {
               const StrengthPoint* sp = d_campData->getArmies().getStrengthPoint(isp);
               const UnitTypeItem& unitType = d_campData->getUnitType(sp->getUnitType());

               // if the other side has won then create a new sp of the same type
               // (if allowed by nationality)
               Side otherSide = getOtherSide(cpi->getSide());

               if(stillInBattle(otherSide))
               {
                  ILeader enemyLeader = d_battle->cinc(otherSide);
                  Nationality n = enemyLeader->getNation();

                  if((d_battle->victor() == otherSide) &&
                     (unitType.isNationality(n)) &&
                     (enemyLeader->getCommand() != NoCommandPosition))
                  {
                     ISP newISP = d_campData->getArmies().createStrengthPoint();
                     newISP->setUnitType(sp->getUnitType());

                     d_campData->getArmies().attachStrengthPoint(enemyLeader->getCommand(), newISP);
                  }
               }
            }

            /*
            * Detach and delete sp
            */

            d_campData->getArmies().detachStrengthPoint(specialCPI, isp);
            d_campData->getArmies().deleteStrengthPoint(isp);

         }
      }
#ifdef DEBUG
      else
         cbLog.printf("Equipment not lost");
#endif
   }
#ifdef DEBUG
   else
      cbLog.printf("Unit has no heavy equipment");
#endif
}

/*
 * Battle has stopped for the evening. Check for victory, critical loss, retreat, etc.
 */

void CampaignBattleUtil::doStoppedForNight()
{
   // see if we have a victor
   Boolean hasVictor = checkForVictor();

   // if so, see if retreating side breaks contact(info contact that is)
   if(hasVictor)
   {
      ASSERT(d_battle->victor() != SIDE_Neutral);
      Side otherSide = getOtherSide(d_battle->victor());

      if(stillInBattle(otherSide))
         d_battle.current()->lostContact(otherSide, lostContact(otherSide));
      else
      {
         doEndOfFighting();
         d_battle.current()->setStoppedForNight(True);
      }
   }

   /*
   * Let player decide if he wants to withdraw or not, or...
   *
   * Even if player decides to continue, there is still a chance
   * unit leader will decide to withdraw on his own due to critical loss.
   * if current morale is below 65 check for critical loss. If so
   * then side withdraws. Only one side will withdraw in any event
   * Priority for side that withdraws is
   *  1. Least %chance of staying
   *  2. Lowest Morale
   *  3. Pick a random
   */

   int lowestChance = 100;
   int highestChance = 100;
   Attribute lowestMorale = Attribute_Range;
   Side lowestSide = SIDE_Neutral;

   for(Side s = 0; s < scenario->getNumSides(); s++)
   {
      ASSERT(d_battle->cinc(s) != NoLeader);
      Leader* cinc = d_campData->getLeader(d_battle->cinc(s));
      CommandPosition* cp = d_campData->getCommand(cinc->getCommand());

      // if side has won we prompt player for withdraw if retreating unit broke contact
      // i.e. we don't know that we won
      if(!hasVictor ||
         s != d_battle->victor() ||
         d_battle->lostContact(getOtherSide(s)))
      {

         // let player decide what to do

         if(!hasVictor || s == d_battle->victor())
         {
            // EndOfDayMode::Mode mode = d_battle->askEndOfDayMode(s);

            // EndOfDayMode::Mode mode = d_battle->endOfDayMode(s);

            /*
            * Player chooses whether to withdraw or not
            *
            */

            if(d_battle->fighting())
            {
               // ILeader leader = d_units[s].cinc();
               // ASSERT(leader != NoLeader);

               /*
               * Do only player units:
               * This should also be sent for AI sides so AI can have choice!
               */

               // if(d_campData->getArmies().isPlayerUnit(leader->getCommand()))
               if(GamePlayerControl::getControl(s) == GamePlayerControl::Player)
               {
                  ASSERT(d_battle->getCloseTown() != NoTown);
                  BattleInfoData bd(d_battle->units(), s, d_battle->victor(), d_battle->getCloseTown());
                  EndOfDayMode::Mode mode = d_campGame->endBattleDay(s, bd);
                  d_battle.current()->endOfDayMode(mode);
               }
            }

         }

         /*
         * Now test for withdraw because of critical loss
         * Even if a unit is retreating, we still run his test since
         * a victorious enemy may still withdraw if he has no knowledge that
         * the enemy is defeated and we should still test under the same mechanic
         */

         // Current morale must be 65 or less to test for withdraw
         // Also, SHQ should never test for withdraw(if option is set)
         if(d_battle->currentMorale(s) <= 65)
         {
            /*
            * Get chance of staying in battle
            * Raw chance is leader's (aggression + (charsma/2)) / 4
            */

            int chance = clipValue((cinc->getAggression()+(cinc->getCharisma()/2))/4, 0, 100);

#ifdef DEBUG
            cbLog.printf("Chance of attacker not withdrawing before modifiers is %d", chance);
#endif

            Boolean add = False;

            if(lowestSide == SIDE_Neutral)
               add = True;
            else if(chance <= lowestChance)
            {
               if(chance == lowestChance)
               {
                  if(d_battle->currentMorale(s) <= lowestMorale)
                  {
                     if(d_battle->currentMorale(s) == lowestMorale)
                        add = CRandom::get(2) != 0;    // static_cast<Boolean>(CRandom::get(2));
                     else
                        add = True;
                  }
               }
               else
                  add = True;
            }

            highestChance = (chance < lowestChance) ? lowestChance : chance;

            if(add)
            {
               lowestChance = chance;
               lowestMorale = d_battle->currentMorale(s);
               lowestSide = s;
            }
         }
      }
   }

   /*
   * Test for withdrawal if we have units that might withdraw
   */

   if(lowestSide != SIDE_Neutral)
   {
      Side withdrawingSide = SIDE_Neutral;

      if(CRandom::get(100) > lowestChance)
      {
         withdrawingSide = lowestSide;
      }

      // if lowest doesn't withdraw and highest is still less than 65 it tests also
      else if(highestChance < 100)
      {
         if(CRandom::get(100) > highestChance)
         {
            withdrawingSide = getOtherSide(lowestSide);
         }
      }

      if((!hasVictor || s == d_battle->victor()) && withdrawingSide != SIDE_Neutral)
      {
         CampaignBattlePtr battlePtr = d_battle.current();

         if(withdrawingSide == d_battle->victor())
            battlePtr->victor(SIDE_Neutral);

         battlePtr->endOfDayMode(withdrawingSide, EndOfDayMode::Withdraw);

         // withdrawing units automatically break contact
         battlePtr->lostContact(withdrawingSide, True);

#ifdef DEBUG
         cbLog.printf("%s is withdrawing due to critical loss", scenario->getSideName(lowestSide));
#endif
      }
   }

   /*
   * if side is withdrawing or retreating then see if we break contact
   * Note: in this context breaking contact means enemy loses sight
   *       of withdrawing side
   *
   */

   {
      CampaignBattlePtr battlePtr = d_battle.current();

      for(s = 0; s < scenario->getNumSides(); s++)
      {
         if(stillInBattle(s) && d_battle->lostContact(s))
         {
            CampaignBattleUnitListIterW iter(&battlePtr->units(s));
            while(++iter)
            {
               CampaignBattleUnit* bu = iter.current();

               // if we lost contact set info level to NoInfo
#ifdef DEBUG
               cbLog.printf("Broke Contact with enemy");
#endif
               bu->getUnit()->setNotSeen();
               bu->getUnit()->setLastSeenLocation();
            }
         }
      }
   }

   /*
   * If we have a victory do end of fighting routine
   *
   * There must NOT be a write lock at this point!
   */

   if(hasVictor)
      doEndOfFighting();

   d_battle.current()->setStoppedForNight(True);
}

/*
 * Recover morale if a multi-day battle
 */

void CampaignBattleUtil::recoverMorale()
{
   for(Side s = 0; s < scenario->getNumSides(); s++)
   {
#ifdef DEBUG
      cbLog.printf("=======Recovering %s's morale for another day of Battle",
         scenario->getSideName(s));
      cbLog.printf("Old Start Morale = %d, Current Morale = %d",
         static_cast<int>(d_battle->startMorale(s)),
         static_cast<int>(d_battle->currentMorale(s)));
#endif
      // some useful values
      ASSERT(d_battle->cinc(s) != NoLeader);
      Leader* cinc = d_campData->getLeader(d_battle->cinc(s));

      // base recovery value is 80% of starting total
      int value = 80;

      // modifiers to value

      // if cinc has charisma over 200  (+5)
      if(cinc->getCharisma() >= 200)
         value += 5;

      RoundOutcome lastRound = roundOutcome(s);
      // if we won round 4 of previous days fight (+5)
      if(lastRound == WonThisRound)
         value += 5;
      // if we lost (-5)
      else if(lastRound == LostThisRound)
         value -= 5;

      // if leaders staff is less than units subordination (-5)
      // if(cinc->getStaff() < d_campData->getArmies().getCommandSubordination(cinc->getCommand()))
      if(cinc->getStaff() < d_campData->getArmies().getCommandSubordination(cinc))
         value -= 5;

      value = clipValue(value, 0, 100);
      Attribute newMorale = static_cast<Attribute>(MulDiv(d_battle->startMorale(s), value, 100));

      newMorale = maximum(d_battle->currentMorale(s), newMorale);

      d_battle.current()->startMorale(s, newMorale);
      d_battle.current()->currentMorale(s, newMorale);

#ifdef DEBUG
      cbLog.printf("New Start Morale = %d, Current Morale = %d",
         static_cast<int>(d_battle->startMorale(s)),
         static_cast<int>(d_battle->currentMorale(s)));
#endif
   }
}

/*
 * Reinforced
 */

void CampaignBattleUtil::doReinforced(const ICommandPosition& cpi)
{
   ASSERT(cpi != NoCommandPosition);
   const CommandPosition* cp = d_campData->getCommand(cpi);
   Side side = cp->getSide();

#ifdef DEBUG
   cbLog.printf("---------- %s reinforced by %s",
      scenario->getSideName(side), (const char*)d_campData->getUnitName(cpi).toStr());

   cbLog.printf("Battle morale before reinforcement = %d",
      static_cast<int>(d_battle->currentMorale(side)));
#endif

   /*
   * if fighting is over, then it starts again if this is an enemy
   */

   Boolean reinforced = True;

   if(d_battle->reconcentrating())
   {
      ASSERT(d_battle->hasVictor());

      if(d_battle->victor() != side)
      {
#ifdef DEBUG
         cbLog.printf("Fighting has restarted");
#endif
         d_battle.current()->victor(SIDE_Neutral);
         d_battle.current()->turnMode(CampaignBattle::BattleTurnMode::Fighting);
         reinforced = False;
      }
   }

   /*
   * If fighting has already started, then we have to take
   * battle morale into account
   */

   if(d_battle->fighting())
   {
      CampaignBattlePtr battlePtr = d_battle.current();

      CampaignBattleUnitListIterW iter(&battlePtr->units(side));

      while(++iter)
      {
         CampaignBattleUnit* bu = iter.current();

         if(bu->getUnit() != cpi)
         {
            UnitIter uIter(&d_campData->getArmies(), bu->getUnit());
            while(uIter.next())
            {
               CommandPosition* cp = uIter.currentCommand();
               cp->setMorale(d_battle->currentMorale(side));
            }

            d_campData->getArmies().averageCommandMorale(bu->getUnit());
         }
      }
   }

   /*
   * Re-initialize starting morale
   */

   d_battle.current()->reinforced(side, reinforced);
   initStartingMorale(side);

#ifdef DEBUG
   cbLog.printf("Battle morale after reinforcement = %d",
      static_cast<int>(d_battle->currentMorale(side)));
#endif
}

/*
 * Determine if any leaders have been hit
 * Run at the end of each battle round.
 *
 */

void CampaignBattleUtil::checkForLeaderLoss()
{
   CampaignBattlePtr battlePtr = d_battle.current();

   for(Side s = 0; s < scenario->getNumSides(); s++)
   {
#ifdef DEBUG
      cbLog.printf("-------------- Testing %s for leader loss", scenario->getSideName(s));
#endif
      CampaignBattleUnitListIterW iter(&battlePtr->units(s));
      while(++iter)
      {
         CampaignBattleUnit* bu = iter.current();

         /*
         * go through each subordinate leader and test for loss
         */

         UnitIter iter(&d_campData->getArmies(), bu->getUnit());
         while(iter.next())
         {
            ICommandPosition cpi = iter.current();
            ILeader leader = cpi->getLeader();

            // Supreme leader cannot be hit
            if(leader->isSupremeLeader())
               continue;

            /*
            * If he is already slightly wounded, reset flag
            * effect of slight wound only lasts 1 round
            */

            if(leader->isLightlyWounded() && leader->isOutOfIt())
               leader->isOutOfIt(False);

            /*
            * Chance of being hit is leader's charisma / 100
            * modified by what he is commanding (i.e. division, corps, etc.)
            */

            // use a base of 100
            LONG chance = leader->getCharisma();

            /*
            * modify for rank;
            */

            const Table1D<SWORD>& rTable = scenario->getLeaderHitRankModifierTable();
            ASSERT(rTable.getWidth() == Rank_HowMany);
            ASSERT(rTable.getResolution() != 0);
            ASSERT(cpi->getRank().getRankEnum() < rTable.getWidth());

            // if a division (X 2)
            // if a corps (X 1.5)
            SWORD value = rTable.getValue(cpi->getRank().getRankEnum());
            chance = MulDiv(value, chance, rTable.getResolution());

            /*
            * Modify for nationality
            */

            // if an Austrian then halve
            const Table1D<SWORD>& nTable = scenario->getLeaderHitNationModifierTable();
            ASSERT(nTable.getResolution() != 0);
            ASSERT(leader->getNation() < nTable.getWidth());

            value = nTable.getValue(leader->getNation());
            chance = MulDiv(value, chance, nTable.getResolution());

            chance /= 100;

            /*
            * see if he is hit
            */

            if(CRandom::get(100) <= chance)
            {
#ifdef DEBUG
               cbLog.printf("%s's %s has been hit",
                  cpi->getNameNotNull(), leader->getNameNotNull());
#endif
               /*
               * He's been hit. Now determine how he's been hit
               */

               // get die roll of between 0 - 11
               const int maxRoll = 12;

               int dieRoll = CRandom::get(maxRoll);
               ASSERT(dieRoll < maxRoll);

               /*
               *  For lucky leader. A 50 / 50 chance of this being only a 'near miss',
               *  or 'horse killed'
               */

               HitLeader::HitHow hitHow;

               if((leader->isLucky()) &&
                  (CRandom::get(100) <= 50))
               {
#ifdef DEBUG
                  cbLog.printf("Lucky Leader escapes!");
#endif
                  hitHow = (CRandom::get(100) <= 50) ? HitLeader::Missed : HitLeader::HorseKilled;
               }
               else
               {

                  /*
                  * Die roll modifiers
                  */

                  /*
                  * Get value from scenario table
                  */

                  const Table2D<UBYTE>& table = scenario->getLeaderHitTable();

                  RoundOutcome thisRound = roundOutcome(s);
                  enum
                  {
                     WonDraw,
                     Lost
                  } what = (thisRound == WonThisRound || thisRound == DrawThisRound) ? WonDraw : Lost;

                  hitHow = static_cast<HitLeader::HitHow>(table.getValue(dieRoll, what));

               }

               switch(hitHow)
               {
               case HitLeader::Missed:
#ifdef DEBUG
                  cbLog.printf("------- Lucky escape! Close call");
#endif
                  break;   // do nothing

               case HitLeader::HorseKilled:
                  leader->isHorseKilled(True);
#ifdef DEBUG
                  cbLog.printf("------- Horse Killed");
#endif
                  break;

               case HitLeader::SlightWound:
                  leader->isLightlyWounded(True);
                  leader->isOutOfIt(True);

                  // reduce health by 5 (permant reduction)
                  leader->setHealth(static_cast<Attribute>(maximum(0, leader->getHealth() - 5)));
#ifdef DEBUG
                  cbLog.printf("------- Slighty Wounded");
#endif
                  break;

               case HitLeader::SeriousWound:
                  leader->isSeriouslyWounded(True);
                  leader->isOutOfIt(True);

                  // reduce health by 50 (permant reduction)
                  leader->setHealth(static_cast<Attribute>(maximum(0, leader->getHealth() - 50)));
#ifdef DEBUG
                  cbLog.printf("------- Seriously Wounded");
#endif
                  break;

               case HitLeader::Captured:
                  {
                     // unattach leader and add to captured list
                     Boolean cincHit = (leader == d_battle->cinc(s));
                     CampaignArmy_Util::captureLeader(d_campData, leader);

                     if(cincHit)
                        battlePtr->setCinC(s);
#ifdef DEBUG
                     cbLog.printf("------- Captured");
#endif
                  }
                  break;

               case HitLeader::Killed:
                  {
                     // unattach leader
                     Boolean cincHit = (leader == d_battle->cinc(s));
                     CampaignArmy_Util::killLeader(d_campData, leader);

                     if(cincHit)
                        battlePtr->setCinC(s);
#ifdef DEBUG
                     cbLog.printf("------- Killed");
#endif
                  }
                  break;
               }

               battlePtr->addHitLeader(s, leader, cpi, hitHow);
            }
         }
      }
   }
}

Boolean CampaignBattleUtil::stillInBattle(Side s)
{
   CampaignBattlePtr battlePtr = d_battle.current();

   CampaignBattleUnitListIterW iter(&battlePtr->units(s));
   while(++iter)
   {
      CampaignBattleUnit* bu = iter.current();
      if(!bu->outOfIt())
         return True;
   }

   return False;
}

void CampaignBattleUtil::showBattleResults(Side s)
{
#if 0
      /*
   * Do only player units
   *
   * Actually... this should be sent on to the AI as well for consideration
   */

      if(GamePlayerControl::getControl(s) == GamePlayerControl::Player)
   {
      ASSERT(d_battle->getCloseTown() != NoTown);

      BattleInfoData bd(d_battle->units(), s, d_battle->victor(), d_battle->getCloseTown());

      d_campData->stopTime();
      BattleResultMsg brWindow(bd);
      d_campGame->getUserResponse(&brWindow);
      d_campData->startTime();
   }
#endif

   ASSERT(d_battle->getCloseTown() != NoTown);

   BattleInfoData bd(d_battle->units(), s, d_battle->victor(), d_battle->getCloseTown());
   d_campGame->battleResults(s, bd);

}

/*---------------------------------------------------------------------------
 * Process battles
 */

void CampaignBattleProc::procBattles(CampaignLogicOwner* campGame, CampaignBattleList* battleList)
{
   if(battleList->entries() > 0)
   {
      CampaignData* campData = campGame->campaignData();

#ifdef DEBUG
      cbLog.printf("\n------------- procBattles: %s", campData->asciiTime());
      // g_logWindow->printf("-------- procBattles: %s", campData->asciiTime());
#endif

      GameTick theTime = campData->getCTime().getTime();

      /*
      * There are 4 rounds of battle fought each day.
      * Currently, Ben wants 4 rounds regardless of season (I believe)
      * As of now they are fought at 0900, 1200, 1500, and 1800 hrs, or
      *
      */

      const Time earlyMorning = Time(7, 0);
      const GameTick earlyMorningTick = earlyMorning.get();

      const Time morning = Time(9, 0);    // 8-9 am
      const GameTick morningTick = morning.get();

      const Time lateMorning = Time(11, 0);     // 10-11 pm
      const GameTick lateMorningTick = lateMorning.get();

      const Time noon = Time(13, 0);      // 12-1 pm
      const GameTick noonTick = noon.get();

      const Time afternoon = Time(15, 0);    // 2-3 pm
      const GameTick afternoonTick = afternoon.get();

      const Time evening = Time(17, 0);      // 4-5pm
      const GameTick eveningTick = evening.get();

      const Time night = Time(18, 0);     // 8pm
      const GameTick nightTick = night.get();

      BattleDay::Value dayTime;

      if(theTime <= earlyMorningTick || theTime >= nightTick)
         dayTime = BattleDay::Night;
      else if(theTime <= morningTick)
         dayTime = BattleDay::Morning;
      else if(theTime <= lateMorningTick)
         dayTime = BattleDay::LateMorning;
      else if(theTime <= noonTick)
         dayTime = BattleDay::Noon;
      else if(theTime <= afternoonTick)
         dayTime = BattleDay::Afternoon;
      else
         dayTime = BattleDay::Evening;

      /*
      * If it is the first night turn, run end of day battle routine.
      * This consists of informing the player of the days progress of
      * all ongoing battles, and allows the player to continue or withdraw from battle
      *
      * Note: Use the BattleList NoLock iteration classes here. Otherwise, they put a lock on the
      *       logic thread and we can't send our message to the interface thread
      *
      * Note SWG 3Dec98 : Must be locked or else the display can crash
      *       because battles are deleted half way through displaying
      *       unless we were to write some really clever iterators.
      */

      if((theTime == nightTick))
      {
         // CampaignBattleIterNLW iter = battleList;
         // CampaignBattleIterW iter = battleList;
         CampaignBattleIter iter = battleList;

         while(++iter)
         {
            // CampaignBattle* battle = iter.current();


#ifdef DEBUG
            {
               ConstCampaignBattlePtr battle = iter.constCurrent();
               String locName;
               campData->getCampaignPositionString(&battle->getPosition(), locName);
               cbLog.printf("\n================================");
               cbLog.printf("Processing stoppedForNight() at %s", locName.c_str());
            }
#endif
            // if(battle->fighting())
            if(iter->fighting())
            {
               // CampaignBattleUtil util(campGame, battle, dayTime);
               CampaignBattleUtil util(campGame, iter, dayTime);
               util.doStoppedForNight();
            }
         }
      }

      // CampaignBattleIterW iter = battleList;
      CampaignBattleIter iter = battleList;
      while(++iter)
      {
         // CampaignBattle* battle = iter.current();
         // CampaignBattleIter battle = iter;

         if(iter->isCalculated() && !iter->isSettingUp() && iter->playThisRound(campData->getTick()))
         {
#ifdef DEBUG
            String locName;
            campData->getCampaignPositionString(&iter->getPosition(), locName);
            cbLog.printf("\n================================");
            cbLog.printf("Processing Battle at %s", (const char*) locName.c_str());
#endif

            // increment next round time
            iter.current()->nextRoundAt(campData->getTick() + HoursToTicks(2));
            CampaignBattleUtil util(campGame, iter, dayTime);

            // starting the battle
            if(iter->starting())
            {
#ifdef DEBUG
               cbLog.printf("Starting Battle");
#endif
               util.startBattle();
            }

            // deploying our armies
            if(iter->deploying())
            {
#ifdef DEBUG
               cbLog.printf("Deploying for Battle");
#endif
               util.deployForces();
            }

            // fighting our armies
            if(iter->fighting())
            {
               // daytime only
               if(dayTime != BattleDay::Night)
               {
#ifdef DEBUG
                  cbLog.printf("Turn of battle being calculated");
#endif

                  if(iter->isCalculated()) // || iter->isInteractive())
                  {
                     // Calculate Results
                     util.calculateResults();
                  }

               }
#ifdef DEBUG
               else      // Stop battle at night
               {
#ifdef DEBUG
                  cbLog.printf("Battle being stopped for night");
#endif
               }
#endif
            }

            // if reconcentrating
            if(iter->reconcentrating())
            {
               util.doAfterBattle();
            }

         }
      }

      iter.rewind();
      while(++iter)
      {
         // CampaignBattle* battle = iter.current();
         // if battle is over then remove it
         // if(battle->battleOver())
         if(iter->battleOver())
         {
            battleList->finishBattle(iter.current());
            iter.removeBattle();
            // iter.rewind();
         }
      }

#ifdef DEBUG
      cbLog.close();
#endif
   }
}


void CampaignBattleProc::prepareToWithdraw(Armies* armies, CampaignBattle* battle, ICommandPosition hunit)
{
   ASSERT(hunit != NoCommandPosition);
}

/*
 *  Following two function's called by RealCampaignData::doCampaignCheckEnemy()
 */

SPCount CampaignBattleProc::getFriendlySPCount(CampUnitMoveData* combData, CampaignBattle* battle, Side side)
{
   return battle->spValue(side);
}

ICommandPosition CampaignBattleProc::getFriendlyUnit(CampUnitMoveData* combData, CampaignBattle* battle, Side side)
{
   CampaignBattleUnitList& bl = battle->units(side);

   if(bl.entries() > 0)
   {
      ASSERT(bl.cinc() != NoLeader);

      Leader* leader = combData->d_campData->getLeader(bl.cinc());

      return leader->getCommand();
   }

   return NoCommandPosition;
}




/*
 * Add a unit to battle
 */

void CampaignBattleProc::addUnit(CampaignLogicOwner* campGame, CampaignBattle* battle, ICommandPosition cpi)
{
   // if this battle is over, don't add
   if(!battle->battleOver())
   {
      CampaignBattleUnit* bu = battle->addUnit(cpi);
      if(bu)
      {
         switch(battle->mode())
         {
         case BattleMode::Calculated:
            {
               if(battle->turnMode() >= CampaignBattle::BattleTurnMode::Deploying)
               {
                  CampaignBattleUtil util(campGame, battle, BattleDay::Night);
                  util.doReinforced(cpi);
               }
               break;
            }

         case BattleMode::Tactical:
            {
#ifdef DEBUG
               cbLog.printf("%s is reinforcing tactical battle", cpi->getName());
               // logWindow->printf("%s is reinforcing tactical battle", cpi->getName());
#endif
               bu->reinforcement(True);
               campGame->reinforceTactical();
               break;
            }
         }
      }
   }
}

/*
 * Run a break-out battle
 *
 */

Boolean CampaignBattleProc::procBreakOutBattle(CampaignData* campData, CampaignBattle* battle, Side sideBreakingOut)
{
   CampaignBattleUtil util(campData, battle);
   return util.calcBreakOutBattle(sideBreakingOut);
}

void CampaignBattleProc::removeFromBattle(CampaignData* campData, ICommandPosition cpi)
{
   CampaignBattleList& bl = campData->getBattleList();
   if(bl.isInABattle(cpi))
   {
#ifdef DEBUG
      cbLog.printf("\n---------- Removing %s from battle\n", (const char*)campData->getUnitName(cpi).toStr());
#endif
      bl.removeFromBattle(cpi);
   }
}

// can we play a tactical battle?
bool CampaignBattleProc::canPlayTactical(CampaignData* campData, CampaignBattle* battle)
{
#ifdef DEBUG
   cbLog.printf("\n-------------- Testing for possible Tactical Battle");
#endif

   // Not if one already exists
   if(campData->getBattleList().hasPlayerBattle())
   {
#ifdef DEBUG
      cbLog.printf("Cannot play. One already exists!");
#endif
      return False;
   }

   const Armies* army = &campData->getArmies();

   SPCount spCounts[2] = { 0,0 };
   IConnection con[2] = { NoConnection, NoConnection };

   // get force sizes
//    SPCount side0 = 0;
//    SPCount side1 = 0;
//    SPCount* s_size[2] = {
//       &side0, &side1
//    };
//
//    IConnection conSide0 = NoConnection;
//    IConnection conSide1 = NoConnection;
//    IConnection* s_con[2] = {
//       &conSide0, &conSide1
//    };

   for(Side s = 0; s < 2; s++)
   {
      //      bool defendingChokePoint = True;
      bool allGarrison = True;
      CampaignBattleUnitList& units = battle->units(s);
      SListIter<CampaignBattleUnit> iter(&units);
      while(++iter)
      {
#ifdef DEBUG
         cbLog.printf("%s is in the battle", iter.current()->getUnit()->getName());
#endif

         // we cannot play if one of the forces is a garrison
         // shouldn't happen but weill check anyway
         if(iter.current()->getUnit()->isGarrison())
         {
#ifdef DEBUG
            cbLog.printf("%s is a garrison", iter.current()->getUnit()->getName());
#endif
         }
         else
         {
            allGarrison = False;

//          spCounts[s] += campData->getArmies().getUnitSPCount(iter.current()->getUnit(), True);

            // Count SPs that are not engineers

#ifdef DEBUG
            cbLog.printf("Counting SPs in %s", iter.current()->getUnit()->getName());
#endif

            ConstSPIter spIter(army, iter.current()->getUnit());
            while (++spIter)
            {
                ConstISP sp = spIter.current();

                // Ignore if Engineer

                UnitType ut = sp->getUnitType();
                const UnitTypeItem& uti = army->getUnitType(ut);


                if (uti.getBasicType() != BasicUnitType::Special)
                {
#ifdef DEBUG
                  cbLog.printf("Adding SP %s", uti.getName());
#endif
                  ++spCounts[s];
                }
                else
                {
#ifdef DEBUG
                  cbLog.printf("Skipping SP %s", uti.getName());
#endif
                }
            }


            if(con[s] == NoConnection &&
               !iter.current()->getUnit()->atTown())
            {
               con[s] = iter.current()->getUnit()->getConnection();
            }
         }
      }

      // if defending chokepoint we cannot play
      if(allGarrison)
      {
#ifdef DEBUG
         cbLog.printf("Cannot play. %s is all Garrison", scenario->getSideName(s));
#endif
         return False;
      }


#if 0
   /*
    * Close Units are not bein considered any more
    * The problem is you can have a very small unit initiating a battle
    * and then lots of reinforcements at 24 miles away.
    * Then the battle starts with hardly any units on the battlefield
    */


      // add any friendly units within 24 miles who are moving into battle
      CloseUnits cl;

      CloseUnitData data;
      data.d_campData = campData;
      data.d_from = battle;
      data.d_closeUnits = &cl;
      data.d_closeUnitSide = s;
      data.d_findDistance = MilesToDistance(24);
      data.d_flags |= CloseUnitData::MovingIntoBattle;

      CloseUnitUtil::findCloseUnits(data);
      spCounts[s] += cl.getSP();
#endif


      // cannot play if either side is defined as 'Small'
      ForceSize::Value fSize = CombatTableUtil::getForceSize(spCounts[s]);
      if(fSize == ForceSize::Small)
      {
#ifdef DEBUG
         cbLog.printf("Cannot play. %s is defined as 'Small'", scenario->getSideName(s));
#endif
         return False;
      }
   }

   // we cannot play if we have a chokepoint between us
   if(con[0] != NoConnection &&
      con[1] != NoConnection &&
      con[0] != con[1])
   {
      // find shared town (if any, though there should be)
      Connection& con0 = campData->getConnection(con[0]);
      Connection& con1 = campData->getConnection(con[1]);

      ITown town = (con0.node1 == con1.node1 || con0.node1 == con1.node2) ? con0.node1 :
      (con0.node2 == con1.node1 || con0.node2 == con1.node2) ? con0.node2 : NoTown;

      Town& t = campData->getTown(town);
      if(campData->isChokePoint(town) &&
         !campData->isSameSideOfChokePoint(con[0], con[1]) &&
         !t.isBridgeUp())
      {
#ifdef DEBUG
         Town& t = campData->getTown(town);
         cbLog.printf("Cannot play. On opposite sides of blown bridge - %s", t.getName());
#endif
         return False;
      }
   }

   // we cannot play if one force outmumbers the other by 3:1 or more
#ifdef DEBUG
   cbLog.printf("French sp-count = %d, Allied sp-count = %d",
      static_cast<int>(spCounts[0]), static_cast<int>(spCounts[1]));
#endif
   if((spCounts[0] >= (3 * spCounts[1])) || (spCounts[1] >= (3 * spCounts[0])))
   {
#ifdef DEBUG
      cbLog.printf("Cannot play. One side outnumbers other by 3:1 or greater");
#endif
      return False;
   }

   return True;
}

void CampaignBattleProc::startBattle(CampaignLogicOwner* campGame, CampaignBattle* battle)
{
   ASSERT(battle != 0);
   ASSERT(battle->isSettingUp());

   CampaignData* campData = campGame->campaignData();
   CampaignBattleList& bl = campData->getBattleList();

   BattleMode::Mode mode = BattleMode::Calculated;

   // If player is not controlling a battle already, then prompt
   // bool timeStopped = False;
   if(canPlayTactical(campData, battle))
   {
      // Send a message to the game to get a result...
      // ChooseBattleModeRequest::send(d_campData, battle);
      // timeStopped = True;
      // campData->stopTime();
      mode = campGame->askBattleMode(battle);
      ASSERT((mode == BattleMode::Calculated) || (mode == BattleMode::Tactical));
   }

   // initBattleMode(battle, mode);

   if(mode != BattleMode::Calculated)
      bl.playersBattle(battle);

   // Put it in calculated mode

   battle->setMode(mode);

   /*
   * If tactical... let the game now about it.
   */

   if(mode == BattleMode::Tactical)
   {
      campGame->requestTactical(battle);
   }
   // else if(timeStopped)
   //    campData->startTime();
}

// start any battles not started
void CampaignBattleProc::startBattles(CampaignLogicOwner* campGame)
{
   CampaignBattleList& battleList = campGame->campaignData()->getBattleList();

   SListIter<CampaignBattle> iter(&battleList);
   while(++iter)
   {
      if(iter.current()->isSettingUp())
      {
         startBattle(campGame, iter.current());
      }
   }
}



/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.1  2001/07/02 15:23:16  greenius
 * Battle, Campaign, Combined game and Campaign Editor now compile and run... mostly
 *
 * Revision 1.2  2001/06/18 08:25:44  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
