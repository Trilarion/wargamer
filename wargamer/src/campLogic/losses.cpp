/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1995, Steven Morle-Green, All Rights Reserved
 *----------------------------------------------------------------------
 *
 * Campaign Losses, Stragglers and Attrition support functions
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.2  2001/06/18 08:25:44  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 * Revision 1.1  2001/06/13 08:52:37  greenius
 * Initial Import
 *
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "losses.hpp"
#include "armies.hpp"
#include "unittype.hpp"
#include "campdint.hpp"
#include "random.hpp"
#include "victory.hpp"
#include "wg_rand.hpp"
#include "campside.hpp"

#ifdef DEBUG
#include "clog.hpp"
LogFile lossLog = "losses.log";
#endif

/*-----------------------------------------------------------
 * Class to share code between losses and stragglers
 */

class UnitLoss {
protected:
   Armies* army;
   ICommandPosition cpi;
   // CommandPosition* cp;

   ISP best;
   ICommandPosition bestCPI;
   int bestVal;


public:
   UnitLoss(Armies* a, ICommandPosition c);
   SPCount apply(SPLoss l, Boolean combatUnitsOnly = False);
   SPCount apply(SPLoss l, BasicUnitType::value basicType, SpecialUnitType::value specialType);

private:
   virtual void applySP(ICommandPosition cpi, ISP sp) = 0;
   virtual void considerCP(ICommandPosition cpi) = 0;
   virtual void considerCP(ICommandPosition cpi, BasicUnitType::value basicType, SpecialUnitType::value specialType) = 0;
protected:
   int calcSPValue(const StrengthPoint* sp);
   void setBest(ICommandPosition i, ISP isp, int value);
};

UnitLoss::UnitLoss(Armies* a, ICommandPosition c)
{
   army = a;
   cpi = c;
   // count = 0;

   // cp = 0;

   best = NoStrengthPoint;
   bestCPI = NoCommandPosition;
   bestVal = 0;
}

const int CombatTypeLoss = 100;

const BasicUnitType::value getCombatTypeLoss(int roll)
{
  ASSERT(roll >= 0);
  ASSERT(roll < CombatTypeLoss);

  if(roll < 50)
    return BasicUnitType::Infantry;
  else if(roll < 75)
    return BasicUnitType::Artillery;
  else
    return BasicUnitType::Cavalry;
}

#ifdef DEBUG
static const char* BasicTypeName[] = {
  "Infantry",
  "Artillery",
  "Cavalry"
};

const char* getBasicTypeName(BasicUnitType::value basicType)
{
  ASSERT(basicType >= BasicUnitType::Infantry);
  ASSERT(basicType <= BasicUnitType::Artillery);

  return BasicTypeName[(int)basicType];
}
#endif


/*
 * Apply losses. If combatUnitsOnly is True then losses are applied as follows:
 * 50% chance of losing Infantry, 25% Artillery, 25% Cavalry
 * Returns the actual number of losses caused
 */

SPCount UnitLoss::apply(SPLoss losses, Boolean combatUnitsOnly)
{
   SPCount count = 0;


   while(losses != 0)
   {
      /*
       * If it is less than 1, then apply probability
       */

      if(losses.getInt() == 0)
      {
         UWORD r = (UWORD) CRandom::get(UWORD_MAX + 1);

#ifdef DEBUG
         lossLog.printf("    Probability of loss: %x, picked %d",
            (int) losses.getValue(), (int) r);
#endif

         if(r >= losses.getFraction())
         {
#ifdef DEBUG
            lossLog.printf("    Failed");
#endif
            break;
         }

#ifdef DEBUG
            lossLog.printf("    Passed");
#endif

         losses = 0;
      }
      else
         losses -= 1;

      /*
       * Iterate through unit, looking for suitable unit to remove!
       */

      best = NoStrengthPoint;
      bestCPI = NoCommandPosition;
      bestVal = 0;

      if(!combatUnitsOnly)
      {

         UnitIter ui(army, cpi);
         while(ui.next())
         {
            CommandPosition* u = ui.currentCommand();

            if(u->getSPEntry() != NoStrengthPoint)
            {
               considerCP(ui.current());
            }
         }

         if(best != NoStrengthPoint)
         {
            applySP(bestCPI, best);
            count++;
         }
#ifdef DEBUG
         else
            lossLog.printf("    No suitable SP was found");
#endif
      }

      /*
       *  Find suitable combat unit to remove. If chosen type is not
       *  available try with another type
       */

      else
      {
         // select type to remove
         const int nValues = 100;
         int dieRoll = CRandom::get(nValues);
         BasicUnitType::value basicType = getCombatTypeLoss(dieRoll);

         Boolean found = False;
         int tries = 0;

#ifdef DEBUG
         lossLog.printf("%s will be lost -- die roll = %d",
             getBasicTypeName(basicType), dieRoll);
#endif
         while(!found)
         {
            tries++;
            UnitIter ui(army, cpi);
            while(ui.next())
            {
               CommandPosition* u = ui.currentCommand();

               if(u->getSPEntry() != NoStrengthPoint)
               {
                  considerCP(ui.current(), basicType, SpecialUnitType::NotSpecial);
               }
            }

            if(best != NoStrengthPoint)
            {
               applySP(bestCPI, best);
               found = True;
               count++;
            }
            else
            {
               // try with another type

               if((basicType + 1) <= BasicUnitType::Artillery)
                 INCREMENT(basicType);
               else
                 basicType = BasicUnitType::Infantry;

#ifdef DEBUG
               lossLog.printf("No suitable SP was found. Trying %s", getBasicTypeName(basicType));
#endif

               if(tries >= 3)
               {
#ifdef DEBUG
                  lossLog.printf("    No suitable SP was found");
#endif
                  break;

               }
            }
         }
      }
   }

#ifdef DEBUG
   lossLog.printf(" #### Losses applied = %d", (int) count);
#endif

   return count;
}

/*
 * Apply losses for specific BasicUnitType or Special Type
 * Returns the actual number of losses caused
 */

SPCount UnitLoss::apply(SPLoss losses, BasicUnitType::value basicType, SpecialUnitType::value specialType)
{
   SPCount count = 0;

   while(losses != 0)
   {
      /*
       * If it is less than 1, then apply probability
       */

      if(losses.getInt() == 0)
      {
         UWORD r = (UWORD) CRandom::get(UWORD_MAX + 1);

#ifdef DEBUG
         lossLog.printf("    Probability of loss: %x, picked %d",
            (int) losses.getValue(), (int) r);
#endif

         if(r >= losses.getFraction())
         {
#ifdef DEBUG
            lossLog.printf("    Failed");
#endif
            break;
         }

#ifdef DEBUG
            lossLog.printf("    Passed");
#endif

         losses = 0;
      }
      else
         losses -= 1;

      /*
       * Make a single loss
       */

#if 0
      if(cp == 0)
         cp = army->getCommand(cpi);

      ASSERT(cp != 0);
#endif

      /*
       * Iterate through unit, looking for suitable unit to remove!
       */

      best = NoStrengthPoint;
      bestCPI = NoCommandPosition;
      bestVal = 0;

      UnitIter ui(army, cpi);

      while(ui.next())
      {
         CommandPosition* u = ui.currentCommand();

         if(u->getSPEntry() != NoStrengthPoint)
         {
           considerCP(ui.current(), basicType, specialType);
         }
      }

      if(best != NoStrengthPoint)
      {
         applySP(bestCPI, best);
         count++;
      }
#ifdef DEBUG
      else
         lossLog.printf("    No suitable SP was found");
#endif
   }

#ifdef DEBUG
   lossLog.printf(" #### Losses applied = %d", (int) count);
#endif

   return count;
}


/*
 * Calculate a quality value for SP
 * Low values are bad, high values are good.
 */

int UnitLoss::calcSPValue(const StrengthPoint* sp)
{
   const UnitTypeItem& ut = campaignData->getUnitType(sp->getUnitType());

   int value = CRandom::get(0x100) + ut.getBaseMorale();

#ifdef DEBUG_LOSSES
   lossLog.printf("    SP %s: base morale=%d, value=%d",
      ut.getName(),
      (int) ut.getBaseMorale(),
      value);
#endif

   return value;
}

void UnitLoss::setBest(ICommandPosition i, ISP isp, int value)
{
   best = isp;
   bestCPI = i;
   bestVal = value;
}

/*-----------------------------------------------------------
 * These functions apply to entire organisations
 */

/*-----------------------------------------------------------
 * Make Losses... Takes Units from non-straggling units
 */

class MakeCasualties : public UnitLoss {
public:
   MakeCasualties(Armies* a, ICommandPosition c) : UnitLoss(a, c) { }

private:
   void applySP(ICommandPosition cpi, ISP sp);
   void considerCP(ICommandPosition cpi);
   void considerCP(ICommandPosition cpi, BasicUnitType::value basicType, SpecialUnitType::value specialType);

};

void MakeCasualties::considerCP(ICommandPosition cpi)
{
   CommandPosition* u = army->getCommand(cpi);

   StrengthPointIter spiter(army, u);
   while(++spiter)
   {
      const StrengthPointItem* sp = spiter.current();

      int value = calcSPValue(sp);

#ifdef DEBUG_LOSSES
      lossLog.printf("    SP %d, value=%d", (int) spiter.currentISP(), value);
#endif

      if((best == NoStrengthPoint) || (value < bestVal))
      {
         setBest(cpi, spiter.currentISP(), value);
      }
   }
}

void MakeCasualties::considerCP(ICommandPosition cpi, BasicUnitType::value basicType, SpecialUnitType::value specialType)
{
   CommandPosition* u = army->getCommand(cpi);

   StrengthPointIter spiter(army, u);
   while(++spiter)
   {
      const StrengthPointItem* sp = spiter.current();

      const UnitTypeTable& table = campaignData->getUnitTypes();
      ASSERT(sp->getUnitType() < table.entries());

      const UnitTypeItem& uti = table[sp->getUnitType()];

      if(uti.getBasicType() == basicType && uti.getSpecialType() == specialType)
      {

         int value = calcSPValue(sp);

#ifdef DEBUG_LOSSES
         lossLog.printf("    SP %d, value=%d", (int) spiter.currentISP(), value);
#endif

         if((best == NoStrengthPoint) || (value < bestVal))
         {
           setBest(cpi, spiter.currentISP(), value);
         }
      }
   }

}

void MakeCasualties::applySP(ICommandPosition cpi, ISP isp)
{
   // ASSERT(cp != 0);
   ASSERT(cpi != NoCommandPosition);
   ASSERT(isp != NoStrengthPoint);

   StrengthPoint* sp = army->getStrengthPoint(isp);
   CommandPosition* cp = army->getCommand(cpi);

#ifdef DEBUG
   lossLog.printf("  Remove SP %d from %s",
      (int) army->getIndex(isp),
      cp->getName());
#endif

   const UnitTypeItem& typeItem = campaignData->getUnitType(sp->getUnitType());
   campaignData->getSideData().addSideLosses(cp->getSide(), 1, typeItem.getBasicType());

   army->detachStrengthPoint(cpi, isp);
   army->deleteStrengthPoint(isp);
}

#if 0    // there are no stragglers any more
/*-----------------------------------------------------------
 * Create stragglers...
 */

class MakeStragglers : public UnitLoss {
public:
   MakeStragglers(Armies* a, ICommandPosition c) : UnitLoss(a, c) { }

private:
   void applySP(ICommandPosition cpi, ISP sp);
   void considerCP(ICommandPosition cpi);
   void considerCP(ICommandPosition cpi, BasicUnitType::value basicType, SpecialUnitType::value specialType)
   {}
};

void MakeStragglers::considerCP(ICommandPosition cpi)
{
   // int nSP = army->getSPListCount(cpi, True);         // Number excluding stragglers
   int nSP = army->getSPListCount(cpi);         // Number excluding stragglers

   if(nSP)
   {
      StrengthPointIter spiter(army, cpi);
      while(++spiter)
      {
         const StrengthPointItem* sp = spiter.current();

#if 0 // There are no stragglers anymore
         if(!sp->isStraggling())
#endif
         {
            int value = calcSPValue(sp);

#ifdef DEBUG_LOSSES
            lossLog.printf("    SP %d, value=%d", (int) spiter.currentISP(), value);
#endif

            if((best == NoStrengthPoint) || (value < bestVal))
            {
               setBest(cpi, spiter.currentISP(), value);
            }
         }
      }
   }
}


void MakeStragglers::applySP(ICommandPosition cpi, ISP sp)
{
   ASSERT(cpi != NoCommandPosition);
   ASSERT(sp != NoStrengthPoint);

#ifdef DEBUG
   lossLog.printf("  Turning SP %d from %s into a straggler",
      (int) sp,
      army->getCommand(cpi)->getName());
#endif

   army->setStraggler(cpi, sp);
}

/*-----------------------------------------------------------
 * Recover stragglers...
 */


class RecoverStragglers : public UnitLoss {
public:
   RecoverStragglers(Armies* a, ICommandPosition c) : UnitLoss(a, c) { }

private:
   void applySP(ICommandPosition cpi, ISP sp);
   void considerCP(ICommandPosition cpi);
   void considerCP(ICommandPosition cpi, BasicUnitType::value basicType, SpecialUnitType::value specialType)
   {}
};


void RecoverStragglers::considerCP(ICommandPosition cpi)
{
   UnitCountInfo info;

   army->getUnitCounts(cpi, info, False);

   if(info.stragglers)
   {
      StrengthPointIter spiter(army, cpi);
      while(++spiter)
      {
         const StrengthPointItem* sp = spiter.current();

         if(sp->isStraggling())
         {
            int value = calcSPValue(sp);

#ifdef DEBUG_LOSSES
            lossLog.printf("    SP %d, value=%d", (int) spiter.currentISP(), value);
#endif

            if((best == NoStrengthPoint) || (value > bestVal))
            {
               setBest(cpi, spiter.currentISP(), value);
            }
         }
      }
   }
}

void RecoverStragglers::applySP(ICommandPosition cpi, ISP sp)
{
   ASSERT(cpi != NoCommandPosition);
   ASSERT(sp != NoStrengthPoint);

#ifdef DEBUG
   lossLog.printf("  Recovering straggling SP %d to %s",
      (int) sp,
      army->getCommand(cpi)->getName());
#endif

   army->clearStraggler(cpi, sp);
}

#endif

/*-----------------------------------------------------------
 * Attrition: Take losses from All /---straggling--/ units...
 */

class MakeAttrition : public UnitLoss {
   AttritionClass::value d_class;
public:
   MakeAttrition(Armies* a, ICommandPosition c, AttritionClass::value cl) : UnitLoss(a, c) { d_class = cl; }

private:
   void applySP(ICommandPosition cpi, ISP sp);
   void considerCP(ICommandPosition cpi);
   void considerCP(ICommandPosition cpi, BasicUnitType::value basicType, SpecialUnitType::value specialType)
   {}
};


void MakeAttrition::considerCP(ICommandPosition cpi)
{

   CommandPosition* u = army->getCommand(cpi);

   StrengthPointIter spiter(army, u);
   while(++spiter)
   {
      const StrengthPointItem* sp = spiter.current();

      const UnitTypeTable& table = campaignData->getUnitTypes();
      ASSERT(sp->getUnitType() < table.entries());

      const UnitTypeItem& uti = table[sp->getUnitType()];

      if(uti.getAttritionClass() == d_class)
      {

         int value = calcSPValue(sp);

#ifdef DEBUG
         lossLog.printf("    SP %d, value=%d ", (int) army->getIndex(spiter.currentISP()), value);
#endif

         if((best == NoStrengthPoint) || (value < bestVal))
         {
           setBest(cpi, spiter.currentISP(), value);
         }
      }
   }


#if 0
   int nSP = army->getSPListCount(cpi, False);  // Number including stragglers

   if(nSP)
   {
      StrengthPointIter spiter(army, cpi);
      while(++spiter)
      {
         const StrengthPointItem* sp = spiter.current();

         Un
         if(sp->isStraggling())
         {
            int value = calcSPValue(sp);

            // Stragglers to have greater chance of being lost

            if(!sp->isStraggling())
               value += value;

#ifdef DEBUG_LOSSES
            lossLog.printf("    SP %d, value=%d", (int) spiter.currentISP(), value);
#endif

            if((best == NoStrengthPoint) || (value < bestVal))
            {
               setBest(cpi, spiter.currentISP(), value);
            }
         }
      }
   }
#endif
}

void MakeAttrition::applySP(ICommandPosition cpi, ISP isp)
{
   // ASSERT(cp != 0);
   ASSERT(cpi != NoCommandPosition);
   ASSERT(isp != NoStrengthPoint);

   StrengthPoint* sp = army->getStrengthPoint(isp);
   CommandPosition* cp = army->getCommand(cpi);

#ifdef DEBUG
   lossLog.printf("  Attrition: Removing SP %d from %s",
      (int) army->getIndex(isp),
      cp->getName());
#endif

   const UnitTypeItem& typeItem = campaignData->getUnitType(sp->getUnitType());
   campaignData->getSideData().addSideLosses(cp->getSide(), 1, typeItem.getBasicType());

   army->detachStrengthPoint(cpi, isp);
   army->deleteStrengthPoint(isp);
}


/*===========================================================
 * External Functions
 */


// SPCount Armies::orgMakeLosses(ICommandPosition cpi, SPLoss losses)
SPCount Losses::makeLosses(Armies* army, ICommandPosition cpi, SPLoss losses)
{
   ASSERT(losses.getInt() < 0x8000);

#ifdef DEBUG
   lossLog.printf("makeLosses(%d,%x)", (int) army->getIndex(cpi), (int) losses.getValue());
#endif

   MakeCasualties casualty(army, cpi);
   return casualty.apply(losses, True);
}

// SPCount Armies::makeLosses(ICommandPosition cpi, SPLoss losses, BasicUnitType::value basicType, SpecialUnitType::value specialType)
SPCount Losses::makeLosses(Armies* army, ICommandPosition cpi, SPLoss losses, BasicUnitType::value basicType, SpecialUnitType::value specialType)
{
   ASSERT(losses.getInt() < 0x8000);

#ifdef DEBUG
   lossLog.printf("makeLosses(%d,%x)", (int) army->getIndex(cpi), (int) losses.getValue());
#endif

   MakeCasualties casualty(army, cpi);
   return casualty.apply(losses, basicType, specialType);
}

#if 0    // There are no stragglers any more
// SPCount Armies::makeStragglers(ICommandPosition cpi, SPLoss losses)
static SPCount Losses::makeStragglers(Armies* army, ICommandPosition cpi, SPLoss losses)
{
   ASSERT(losses.getInt() < 0x8000);

#ifdef DEBUG
   lossLog.printf("makeStragglers(%d,%x)", (int) cpi, losses.getValue());
#endif

   MakeStragglers casualty(army, cpi);
   return casualty.apply(losses);
}

// SPCount Armies::recoverStragglers(ICommandPosition cpi, SPLoss gains)
static SPCount Losses::recoverStragglers(Armies* army, ICommandPosition cpi, SPLoss gains)
{
   ASSERT(gains.getInt() < 0x8000);

#ifdef DEBUG
   lossLog.printf("    recoverStragglers(%d,%x)", (int) cpi, (int) gains.getValue());
#endif

   RecoverStragglers casualty(army, cpi);
   return casualty.apply(gains);
}
#endif   // There are no stragglers

// SPCount Armies::makeAttrition(ICommandPosition cpi, SPLoss losses)
SPCount Losses::makeAttrition(Armies* army, ICommandPosition cpi, SPLoss losses, AttritionClass::value c)
{
   ASSERT(losses.getInt() < 0x8000);

#ifdef DEBUG
   lossLog.printf("makeAttrition(%d,%x)", (int) army->getIndex(cpi), (int) losses.getValue());
#endif

   MakeAttrition casualty(army, cpi, c);
   return casualty.apply(losses);
}



/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.2  2001/06/18 08:25:44  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
