/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 * Campaign Combat: Taking over towns
 *
 *----------------------------------------------------------------------
 */



#include "stdinc.hpp"
#include "cc_town.hpp"
#include "campctrl.hpp"
#include "campdint.hpp"
#include "campmsg.hpp"
#include "armies.hpp"
#include "town.hpp"
#include "scenario.hpp"
#include "random.hpp"
#include "condutil.hpp"
#include "terrain.hpp"
#include "cu_order.hpp"
#include "wg_rand.hpp"
#include "c_supply.hpp"
#ifdef DEBUG
#include "misc.hpp"
#include "unitlog.hpp"
LogFileFlush townLog("Town.log");

#endif
#include "todolog.hpp"


/*
 * Just take over a town
 *
 * Change the town's side
 * Update any victory condition counters
 * Update Province ownership... may be better done in resource phase
 */

void CampUnitTown::takeTown(CampaignLogicOwner* campGame, ITown itown, ICommandPosition hunit)
{
   ASSERT(itown != NoTown);
   ASSERT(hunit != NoCommandPosition);
   ASSERT(campGame != 0);

   CampaignData* campData = campGame->campaignData();
   ASSERT(campData != 0);

   CommandPosition* cp = campData->getCommand(hunit);
   ASSERT(cp != 0);
   ASSERT(cp->takingOverTown());
   Town& town = campData->getTown(itown);
   cp->notTakingTown();

   Side side = cp->getSide();
   Side tSide = town.getSide();

   if(tSide != side)
   {
#ifdef DEBUG
      cuLog.printf("%s changing side from %s to %s",
         campData->getTownName(itown),
         scenario->getSideName(town.getSide()),
         scenario->getSideName(side) );
#endif

      /*
       * Tell the player
       */

      {
         CampaignMessageInfo msgInfo(side, CampaignMessages::TownTaken);
         msgInfo.cp(hunit);
         msgInfo.where(itown);
         campGame->sendPlayerMessage(msgInfo);
      }

      {
         CampaignMessageInfo msgInfo(tSide, CampaignMessages::TownLost);
         msgInfo.where(itown);
         campGame->sendPlayerMessage(msgInfo);
      }

      town.setSide(side);

      // add remove victory
      campData->addVictoryLevel(town.getVictory(side), side);

      //--- Bugfix: Steven, 25Jun99 : avoid error in removeVictoryLevel when taking neutral towns
      if(tSide != SIDE_Neutral)
         campData->removeVictoryLevel(town.getVictory(tSide), tSide);

      ConditionTypeData data;
      data.d_town = itown;
      CampaignConditionUtil::instantUpdate(campGame, ConditionType::ID_TownChangesSide, &data);

      /*
       *  If town taken is an opposing capital cancel all build items
       */

      IProvince iProv = town.getProvince();
      ASSERT(iProv != NoProvince);
      Province& prov = campData->getProvince(iProv);

      if(town.getIsCapital())
      {
         prov.cancelBuilds();
#ifdef DEBUG
         cuLog.printf("%s's capital(%s) taken. All builds canceled",
            prov.getNameNotNull(), town.getNameNotNull());
#endif
      }

      /*
       * Losing / Winning a town effects national morale
       */

      // TODO: get table from scenario tables
      // value is % increase / decrease in national morale (using a base of 10)
      // i.e. % = value / base
      static int base = 10;
      static UBYTE s_table[NationType_MAX][TownSize_MAX] = {
         // Capital,  City,  Town,  Other
         {    200,     100,    0,      0  },  // Major Nation
         {    100,      50,    0,      0  },  // Minor Nation
         {     50,      25,    0,      0  }   // Generic
     };

      enum {
         IsSupplySource,
         IsDepot,
         IsChokePoint
      };

      static UBYTE s_modTable[] = {
        100, 50, 100
     };

      Nationality n = prov.getNationality();
      UBYTE tValue = s_table[scenario->getNationType(n)][town.getSize()];

      // if town is a supply source, value is increased by 10%
      if(town.getIsSupplySource())
         tValue += s_modTable[IsSupplySource];
      // if town is a depot, value is increased by 10%
      else if(town.getIsDepot())
         tValue += s_modTable[IsDepot];

      // if town is a chokepoint, value is increased by 10%
      if(campData->isChokePoint(itown))
         tValue += s_modTable[IsChokePoint];

      if(tValue > 0)
      {
         for(Side s = 0; s < scenario->getNumSides(); s++)
         {
            Boolean victor = (s == side);

            // apply moraleChange / 2 to all allied nations
            for(Nationality an = 0; an < scenario->getNumNations(); an++)
            {
               if( (campData->getArmies().getNationAllegiance(an) == s) &&
                  (scenario->getNationType(an) == MajorNation) )
               {
                  Attribute nMorale = campData->getNationMorale(an);

                  // adjust attribute
                  Attribute moraleChange = MulDiv(nMorale, tValue, base * 100);// / 2;

                  // if allied then / by 2
                  if(an != n)
                     moraleChange /= 2;

                  nMorale = (victor) ? minimum<int>(Attribute_Range, nMorale + moraleChange) :
                  maximum(0, nMorale - moraleChange);

                  campData->setNationMorale(an, nMorale);
               }
            }
         }

         campGame->moraleUpdated();
      }

      campGame->redrawMap();
   }
}

#ifdef DEBUG
static const char* text[UnitToTownRatio::HowMany] = {
  "Less Than",
  "Less Than 3/2",
  "Less Than 2/1",
  "Less Than 3/1",
  "Less Than 4/1",
  "Over 4"
};

const char* UnitToTownRatio::getText(Value v)
{
   ASSERT(v < HowMany);
   return text[v];
}
#endif

UnitToTownRatio::Value unitToTownRatio(SPCount unitSP, SPCount townSP)
{

   if( (unitSP == 0 && townSP > 0) || (unitSP < townSP) )
      return UnitToTownRatio::LessThan;

   else if( (unitSP == townSP) || (unitSP < (townSP*3)/2) )
      return UnitToTownRatio::LessThan3To2;

   else if(unitSP < (townSP*2))
      return UnitToTownRatio::LessThan2To1;

   else if(unitSP < (townSP*3))
      return UnitToTownRatio::LessThan3To1;

   else if(unitSP < (townSP*4))
      return UnitToTownRatio::LessThan4To1;

   else
      return UnitToTownRatio::Over4;

}

/*
 * Get units allowable sp count for taking a town
 */

SPCount spsTakingTown(CampaignData* campData, ICommandPosition cpi)
{
   /*
    * For this test SPCount is as follows:
    *
    *  1. Infantry SP's count as 1 SP
    *  2. Artillery SP's count as 1/4 SP
    *   3. Cavalry SP's count as 1/2 SP
    *  4. Ben doesn't say, but presumably Special-Types are not counted?
    *
    * First, get our SP Counts
    */


   SPCount unitSP = campData->getArmies().getNType(cpi, BasicUnitType::Infantry, False);
   int nArt = campData->getArmies().getNType(cpi, BasicUnitType::Artillery, False);
   int nCav = campData->getArmies().getNType(cpi, BasicUnitType::Cavalry, False);
   unitSP += nArt / 4;
   unitSP += nCav / 2;
   unitSP += (((nArt % 4) + (2 * (nCav % 2))) + 1) / 4;

   return unitSP;
}

UnitToTownRatio::Value CampUnitTown::getUnitToTownRatio(CampaignData* campData, ITown iTown, Side side)
{
   /*
    * To take a town unit SPCount must be greater than town strength
    *
    * Note we are using SPCounts and not SPValues.
    */

   // if any friendly units are at this town, add their counts also
   ICommandPosition topCPI = campData->getArmies().getFirstUnit(side);
   SPCount unitSP = 0;

   UnitIter iter(&campData->getArmies(), topCPI);
   while(iter.sister())
   {
      ICommandPosition cpi = iter.current();
      if(cpi->atTown() && cpi->getTown() == iTown)
         unitSP += spsTakingTown(campData, cpi);
   }

   SPCount townSP = campData->getTownStrength(iTown, side);  // town.getStrength(side);

   /*
    * Get our ratio
    */

   return unitToTownRatio(unitSP, townSP);
}

/*
 * A Unit is entering a new town
 *
 * Check to see whether it should:
 *    Continue moving
 *    Siege town
 *    Attack town
 *
 * This function only sets mode flags.  The actual combat and siege
 * calculations will be done later.
 */

void CampUnitTown::enterTown(CampaignLogicOwner* campGame, ITown itown, ICommandPosition hunit)
{
   ASSERT(itown != NoTown);
   ASSERT(hunit != NoCommandPosition);
   ASSERT(campGame != 0);

   CampaignData* campData = campGame->campaignData();
   ASSERT(campData != 0);

   CommandPosition* cp = campData->getCommand(hunit);

   ASSERT(cp != 0);
   Side side = cp->getSide();

   const Town& town = campData->getTown(itown);

#ifdef DEBUG
   const char* szFriend;

   if(town.getSide() == side)
      szFriend = "friendly";
   else if(town.getSide() == SIDE_Neutral)
      szFriend = "neutral";
   else
      szFriend = "enemy";

   static const char* townSizeName[TownSize_MAX] = {
     "Capital",
     "City",
     "Town",
     "Other"
   };

   ASSERT(town.getSize() < TownSize_MAX);

   cuLog.printf("%s entering %s %s(%s)",
      cp->getName(),
      szFriend,
      town.getNameNotNull(),
      townSizeName[town.getSize()]);
#endif

   /*
    * Update conditions
    */

   Province& prov = campData->getProvince(town.getProvince());
   ConditionTypeData data;
   data.d_nation = prov.getNationality();
   data.d_side = side;
   CampaignConditionUtil::instantUpdate(campGame, ConditionType::ID_MovesIntoNeutral, &data);


   TimeTick holdTilWhen = 0;

   /*
    * If town is currently suffering a catastrophe, then add another day to hold time
    */

   if(town.hasCatastrophe(campData->getTick()))
   {
      holdTilWhen += DaysToTicks(1);
#ifdef DEBUG
      cuLog.printf("%s is holding an extra day because %s is currently suffering from a catastrophe",
         (const char*)campData->getUnitName(hunit).toStr(), town.getNameNotNull());
#endif
   }

   /*
    * If we don't own this town
    */

   if(town.getSide() != side)
   {
      if(town.isGarrisoned() || town.isSieged() || town.isOccupied())
      {
#ifdef DEBUG
         cuLog.printf("%s is not unoccupied", town.getNameNotNull());
#endif
      }
      else
      {

         /*
          * Get tables
          */

         const Table2D<UBYTE>& delayTable = scenario->getTownDelayTable();
         const Table1D<UBYTE>& townSizeMultiplierTable = scenario->getTownSizeMultiplierTable();

         UnitToTownRatio::Value ratio = getUnitToTownRatio(campData, itown, hunit->getSide());

#ifdef DEBUG
         cuLog.printf("Unit to Town ratio is '%s'", UnitToTownRatio::getText(ratio));
#endif

         /*
          * Now get die roll and raw delay time
          */

         int dieRoll = CRandom::get(delayTable.getWidth());

#ifdef DEBUG
         cuLog.printf("Raw Die Roll is %d", dieRoll);
#endif

         /*
          *  Do die roll modifers
          *
          *  First one is for leader aggression
          */

         Leader* leader = campData->getLeader(cp->getLeader());

         if(leader->getAggression() < 101)
            dieRoll -= 1;
         else if(leader->getAggression() > 179)
            dieRoll += 1;

#ifdef DEBUG
         cuLog.printf("Die Roll after aggression modifier is %d", dieRoll);
#endif

         dieRoll = clipValue(dieRoll, 0, delayTable.getWidth() - 1);

         /*
          * TODO: modifier for Marching to Sound of Guns
          */

         /*
          * Get our raw delay time
          * If we are at a choke point, ratio value is always LessThan
          */

         //      const TerrainTypeItem& terrain = campData->getTerrainType(town.getTerrain());
         Boolean isChokePoint = campData->isChokePoint(itown);
         int hoursDelay = delayTable.getValue((isChokePoint) ? UnitToTownRatio::LessThan : ratio, dieRoll);

#ifdef DEBUG

         if(isChokePoint)
         {
            cuLog.printf("%s is a chokepoint", town.getName());
         }

         cuLog.printf("Raw delay time is %d hours. Ratio value is %d, Die Roll is %d",
            hoursDelay,
            (isChokePoint) ? UnitToTownRatio::LessThan : static_cast<int>(ratio),
            dieRoll);
#endif

         /*
          * Get our town size multiplier, if town is a chokepoint, add 2 to multiplier
          */

         int multiplier = townSizeMultiplierTable.getValue(town.getSize());


         if(isChokePoint)
         {
            multiplier += 2;
         }

         hoursDelay *= multiplier;

#ifdef DEBUG
         cuLog.printf("Delay time after TownSizeModifier is %d hours", hoursDelay);
#endif

         /*
          * Convert our delay time to TimeTicks
          */

         holdTilWhen += HoursToTicks(hoursDelay);

         /*
          * If our ratio is greater than LessThan then we can take the town
          * at the end of our delay time
          */

         if(ratio > UnitToTownRatio::LessThan)
         {
            cp->takingThisTown(itown);

            /*
             * if any friendly units are in town and they are raiding
             * have them finish up
             */

            ICommandPosition topCPI = campData->getArmies().getFirstUnit(side);
            UnitIter iter(&campData->getArmies(), topCPI);
            while(iter.sister())
            {
               ICommandPosition cpi = iter.current();
               if(cpi != hunit && cpi->atTown() && cpi->getTown() == itown && cpi->isRaiding())
                  CampaignOrderUtil::finishOrder(campGame, cpi);

            }


#if 0
            /*
             * Now, if hold time is 0 take over town right away
             */

               if(holdTilWhen == 0)
            {
               destroySupply(campGame, itown, hunit);
               takeTown(campGame, itown, hunit);
            }
#endif
         }
      }
   }

   /*
    * force unit to hold for given amount of time
    */

   cp->forceHold(holdTilWhen+campData->getTick());
}

/*
 * Test to see if depot is destroyed
 * Obviously only applies to depots and supply-sources
 */

void CampUnitTown::destroySupply(CampaignLogicOwner* campGame, ITown itown, ICommandPosition cpi)
{
   ASSERT(itown != NoTown);
   ASSERT(cpi != NoCommandPosition);
   ASSERT(campGame);

   CampaignData* campData = campGame->campaignData();
   Town& t = campData->getTown(itown);

   ASSERT(t.getIsDepot() || t.getIsSupplySource());
   ASSERT(t.getSide() != cpi->getSide());
   ASSERT(!t.isGarrisoned());

#ifdef DEBUG
   townLog.printf("\nTest %s to see if supply is destroyed or captured",
      t.getNameNotNull());
   townLog.printf("---------------------------------------------------");
#endif

   /*
    * First, see if we destroy anything
    * Chance is a random % between 30 - 70
    */

   int chance = CRandom::get(30, 60);

#ifdef DEBUG
   townLog.printf("------- Chance of supply being destroyed = %d", chance);
#endif

   if(CRandom::get(100) < chance)
   {
      /*
       * If a depot, then depot is destroyed
       */

      if(t.getIsDepot())
      {
#ifdef DEBUG
         townLog.printf("------- Depot Destroyed");
#endif

         t.setIsDepot(False);
      }

      /*
       * If a supply source, then destroy a random % of supply
       */

      else
      {
         UWORD supplyLevel = t.getSupplyLevel();

         // get percent to destroy. a random value between 0 - 100
         int percent = CRandom::get(100);

#ifdef DEBUG
         townLog.printf("------- Source has %d of its supply destroyed", percent);
#endif

         // adjust supply level
         supplyLevel = MulDiv(supplyLevel, percent, 100);
         t.setSupplyLevel(supplyLevel);
      }
   }

   // else, capture supply
   else
   {
      // first, add any available supply to cpi's supply level
      SPCount spCount = campData->getArmies().getUnitSPCount(cpi, True);
      if(spCount > 0)
      {
         int capturedSupply = 0;

         // get percent unit is supplied, and how many sp we need to supply
         int percentUnitSupplied = MulDiv(cpi->getSupply(), 100, Attribute_Range);
         SPCount suppliedSP = MulDiv(spCount, percentUnitSupplied, 100);
         SPCount needToDraw = spCount - suppliedSP;

#ifdef DEBUG
         townLog.printf("------- %s needs to draw for %d SP",
            cpi->getNameNotNull(), static_cast<int>(needToDraw));
#endif

         // if a depot, then we have to trace a supply line to a source
         if(t.getIsDepot() && (t.getSide() != SIDE_Neutral))
         {
            RouteList supplyRoute;
            int canDraw = CampaignSupply::findSupplyLine(campData, itown, spCount, supplyRoute);
            if(canDraw > 0)
            {
               ASSERT(supplyRoute.entries() > 0);
               ASSERT(supplyRoute.getLast()->d_town != NoTown);

               Town& st = campData->getTown(supplyRoute.getLast()->d_town);
               ASSERT(st.getIsSupplySource());

               int capturePercent = CRandom::get(50);

               /*
               *  We capture a random amount of supplies
               */

               capturedSupply = minimum(canDraw, MulDiv(st.getSupplyLevel(), capturePercent, 100));

#ifdef DEBUG
               townLog.printf("------- %s captures %d%% of sources supply",
                  cpi->getNameNotNull(), capturePercent);
#endif

               st.setSupplyLevel(st.getSupplyLevel() - capturedSupply);

            }
         }
         // otherwise, get it directly from stockpile
         else
         {
            /*
            *  We capture amount needed to top off unit supplie
            */

            capturedSupply = minimum<int>(t.getSupplyLevel(), needToDraw);
            t.setSupplyLevel(t.getSupplyLevel() - capturedSupply);
         }

         int actuallyDrew = minimum<int>(capturedSupply, needToDraw);

#ifdef DEBUG
         townLog.printf("------- %d points actually captured",
            capturedSupply);

         townLog.printf("------- %d points goes directly to unit",
            actuallyDrew);

#endif

         // get new percent supplied and set unit supply
         percentUnitSupplied = minimum(100, MulDiv(suppliedSP + actuallyDrew, 100, spCount));
         Attribute supply = MulDiv(percentUnitSupplied, Attribute_Range, 100);
         campData->getArmies().setCommandSupply(cpi, supply);

         capturedSupply -= actuallyDrew;

         // now remaining supply (if any) is sent to units supply-source (if any)
         // not to exceed units canDraw value
         if(capturedSupply > 0)
         {
            int canDraw = CampaignSupply::findSupplyLine(campData, cpi, spCount);
            if(canDraw > 0)
            {
               ASSERT(cpi->supplyRouteList().entries() > 0);
               Town& st = campData->getTown(cpi->supplyRouteList().getLast()->d_town);

               int supplySent = minimum(canDraw, capturedSupply);
               st.setSupplyLevel(st.getSupplyLevel() + supplySent);
            }
         }
      }
   }
}


/*
 * Test to see if how much supply we may capture
 * Obviously only applies to depots and supply-sources
 */

// void CampUnitTown::captureSupply(CampaignLogicOwner* campGame, ITown itown, ICommandPosition hunit)
// {
//    ASSERT(itown != NoTown);
//    ASSERT(hunit != NoCommandPosition);
//    ASSERT(campGame);
//
//    CampaignData* campData = campGame->campaignData();
//    Town& t = campData->getTown(itown);
//
// }

/*
 * Update Town's Unit Mode
 *
 * Go through units and set up town's siege/garrison/occupy flags
 */

// void RealCampaignData::updateTownUnitMode()
void CampUnitTown::updateTownUnitMode(CampaignData* campData)
{
   /*
   * Clear all flags in town
   * (better to keep a local copy, otherwise need to lock out towns)
   */

   struct TownFlag {
      Boolean garrison:1;        // Is there a garrison here?
      Boolean siege:1;           // Is there a siege here?
      Boolean raid:1;
      Boolean occupied:1;        // Are there units here?

      TownFlag()
      {
         garrison = False;
         siege = False;
         raid = False;
         occupied = False;
      }
   };

   Armies& armies = campData->getArmies();
   TownList& towns = campData->getTowns();


   ASSERT(towns.entries() > 0);
   TownFlag* flags = new TownFlag[towns.entries()];
   ASSERT(flags != 0);

   /*
   * Set flags from where units are, and what they are doing
   */

   CPIter uIter(&armies);

   while(uIter.sister())
   {
      ICommandPosition cpi = uIter.current();

      const CommandPosition* cp = armies.getCommand(cpi);

      if(cp->atTown())
      {
         ITown t = cp->getTown();

         if(cp->isSieging())
            flags[t].siege = True;
         else if(cp->isGarrison())
            flags[t].garrison = True;
         else if(cp->isRaiding())
            flags[t].raid = True;
         else
            flags[t].occupied = True;

#ifdef DEBUG_ALL
         cuLog.printf("%30s %6s,%6s,%6s,%6s",
            towns[t].getName(),
            boolToAscii(flags[t].siege),
            boolToAscii(flags[t].garrison),
            boolToAscii(flags[t].raid),
            boolToAscii(flags[t].occupied));
#endif

      }
   }

   /*
   * Copy the new flags to the towns
   */

   for(ITown t = 0; t < towns.entries(); t++)
   {
      Town* town = &towns[t];

#ifdef DEBUG
      if( (town->isGarrisoned() != flags[t].garrison) ||
         (town->isSieged()     != flags[t].siege   ) ||
         (town->isRaided()     != flags[t].raid    ) ||
         (town->isOccupied()   != flags[t].occupied) )
      {
         cuLog.printf("%30s %6s,%6s,%6s,%6s",
            towns[t].getName(),
            boolToAscii(flags[t].siege),
            boolToAscii(flags[t].garrison),
            boolToAscii(flags[t].raid),
            boolToAscii(flags[t].occupied));
      }
#endif


      town->setGarrison(flags[t].garrison);
      town->setSiege(flags[t].siege);
      town->setRaid(flags[t].raid);
      town->setOccupied(flags[t].occupied);

      // if not under siege reset weeks under siege counter to 0
      if(!town->isSieged())
         town->resetWeeksUnderSiege();
   }

   delete[] flags;

}



/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.1  2001/07/02 15:23:16  greenius
 * Battle, Campaign, Combined game and Campaign Editor now compile and run... mostly
 *
 * Revision 1.2  2001/06/18 08:25:44  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
