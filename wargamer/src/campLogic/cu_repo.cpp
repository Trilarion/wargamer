/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
#include "stdinc.hpp"
#include "cu_repo.hpp"
#include "campdint.hpp"
#include "armies.hpp"
#include "campctrl.hpp"
#include "campmsg.hpp"
// #include "wg_rand.hpp"
#include "scenario.hpp"
#include "armyutil.hpp"
#include "route.hpp"
#include "town.hpp"
#include "c_supply.hpp"

#ifdef DEBUG
#include "clog.hpp"
static LogFileFlush rLog("RepoSPProc.log");
#endif


/*
 * Local utility class
 */

class RepoSPProcUtil {
   public:
   static void procRepoSPOrders(CampaignLogicOwner* campGame);
   static Boolean canGetThrough(CampaignData* campData, const ICommandPosition& cpi);
};

// Can sp get through to unit to attach?
Boolean RepoSPProcUtil::canGetThrough(CampaignData* campData, const ICommandPosition& cpi)
{
   /*
   * Can't get through...
   */

   // if unit is dead
   if(cpi->isDead() || cpi->trapped())
      return False;

   // if unit is under siege
   if(cpi->isGarrison())
   {
      ASSERT(cpi->atTown());
      const Town& t = campData->getTown(cpi->getTown());
      if(t.isSieged())
         return False;
   }

   // if unit doesn't have a supply line
   if((CampaignSupply::findSupplyLine(campData, cpi, 100) == 0))
      return False;

#ifdef DEBUG
   else
      ASSERT(cpi->supplyRouteList().entries() > 0);
#endif

   return True;
}

void RepoSPProcUtil::procRepoSPOrders(CampaignLogicOwner* campGame)
{
   ASSERT(campGame);

   CampaignData* campData = campGame->campaignData();
   Armies& armies = campData->getArmies();

   ArmyWriteLocker lock(armies);

   SListIter<RepoSP> iter(&armies.repoSPList());
   while(++iter)
   {
      RepoSP* rsp = iter.current();

      if(rsp->isCompleted())
      {
         if(rsp->refCount() == 0)
            iter.remove();

         continue;
      }

      /*
       * Find attaching AP and see if they have arrived
       */

      if(rsp->travelTime() <= campData->getTick())
      {

         if(rsp->attachTo() != NoCommandPosition &&
            !rsp->attachTo()->trapped())
         {
            /*
            * SP has arrived. Assign it to unit
            */

            // make sure we can still assign sp
            const UnitTypeItem& uti = campData->getUnitType(rsp->sp()->getUnitType());

#ifdef DEBUG
            rLog.printf("%s wants to attach to %s",
               uti.getName(), rsp->attachTo()->getNameNotNull());
#endif

            Boolean gotThrough = False;
            Boolean canAttachTo = CampaignArmy_Util::canAttachTo(campData, rsp->attachTo(), uti);

            if(canAttachTo)
            {
               gotThrough = canGetThrough(campData, rsp->attachTo());
               if(gotThrough)
               {
#if 0       // TODO: Need a player message for this

                     // let player know
                     CampaignMessageInfo msgInfo(il->attachTo()->getSide(), CampaignMessages::TakenCommand);
                  msgInfo.cp(il->attachTo());
                  msgInfo.cpTarget(il->attachTo());
                  msgInfo.leader(il->leader());
                  campGame->sendPlayerMessage(msgInfo);
#endif

#ifdef DEBUG
                  rLog.printf("%s attaching", uti.getName());
#endif

                  // transfer
                  armies.attachStrengthPoint(rsp->attachTo(), rsp->sp());
                  armies.setCommandSpecialistFlags(armies.getTopParent(rsp->attachTo()));

                  // remove from list
                  // iter.remove();
                  if(rsp->refCount() == 0)
                     iter.remove();
                  else
                     rsp->setCompleted();
                  continue;
               }
            }

            // otherwise it goes back to the pool or keeps trying
            if((!gotThrough && !canAttachTo) ||
               rsp->attachTo()->isDead())
            {


               /*
                * Figure out travel time back to pool
                *
                * Note: Temporary code
                * this needs more proper design.
                *
                */

               // temporary. we'll need to get this from a data file
               ASSERT(rsp->town() != NoTown);
               CampaignPosition toLoc;
               toLoc.setPosition(rsp->town());

               TimeTick travelTime = 0;
               Distance routeDistance = 0;
               static CampaignRoute route(campData);

               if(route.getRouteDistance(&toLoc, rsp->attachTo()->location(), Distance_MAX, routeDistance, False))
               {
                  // int miles = DistanceToMile(routeDistance);

                  /*
                   *  Travel time is 12 mph for 8 hours per day(96 mpd).
                   *  timeToTravel Function caculates raw time,
                   *   so set MPH macro to MPH(4) to account for courier rest time
                   */

                  travelTime = timeToTravel(routeDistance, MPH(4));
               }

#ifdef DEBUG
               rLog.printf("%s cannot attach, going back to pool (in %ld hrs)",
                  uti.getName(), (travelTime / TicksPerHour));
#endif

               rsp->travelTime(travelTime + campData->getTick());
               rsp->attachTo(NoCommandPosition);
            }
         }
      }
   }
}


/*--------------------------------------------------------
 * Access functions
 */

void RepoSPProc::processHourly(CampaignLogicOwner* campGame)
{
   ASSERT(campGame);

   /*
    * Process independent leader orders
    */

   RepoSPProcUtil::procRepoSPOrders(campGame);
}




/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.2  2001/06/18 08:25:44  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
