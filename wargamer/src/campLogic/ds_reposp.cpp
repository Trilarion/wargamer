/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
 * RepoSP Despatcher
 */


#include "stdinc.hpp"
#include "ds_repoSP.hpp"
#include "armies.hpp"
#ifdef DEBUG
#include "unitlog.hpp"
#endif


void RepoDespatch::process(CampaignData* campData)
{
   const RepoSP* const_rsp = d_rsp;
   RepoSP* rsp = const_cast<RepoSP*>(const_rsp);

   if(rsp->isCompleted())
      return;

   ICommandPosition cp = constCPtoCP(d_cp);

   rsp->attachTo(cp);
   ISP sp = d_rsp->sp();

   /*
   * Figure out travel time to unit
   *
   * Note: Temporary code
   * this needs more proper design.
   *
   */

   // temporary. we'll need to get this from a data file
   ASSERT(d_rsp->town() != NoTown);
   CampaignPosition fromLoc;
   fromLoc.setPosition(d_rsp->town());

   TimeTick travelTime = 0;
   Distance routeDistance = 0;

   //--- Modified 16Aug99 : SWG : Can't be static because when you restart a campaign
   // it contains pointers to deleted data

   // static CampaignRoute route(campData);
   CampaignRoute route(campData);

   if(route.getRouteDistance(&fromLoc, cp->location(), Distance_MAX, routeDistance, True))
   {
      int miles = DistanceToMile(routeDistance);

      /*
        *  Travel time is 12 mpd
        *  timeToTravel Function caculates raw time,
        */

      travelTime = timeToTravel(routeDistance, MPH(1)) / 2;
   }

   rsp->travelTime(travelTime + campData->getTick());

#ifdef DEBUG
   const UnitTypeItem& uti = campData->getUnitType(sp->getUnitType());

   cuLog.printf("%s taking over %s, travel time = %ld hours",
      uti.getName(), cp->getNameNotNull(), (travelTime / TicksPerHour));
//   logWindow->printf("%s taking over %s, travel time = %ld hours",
//      uti.getName(), cp->getNameNotNull(), (travelTime / TicksPerHour));
#endif
}







/*
Pack a multiplayer message into buffer
NOTE : buffer points to MP_MSG_CAMPAIGNORDER
*/
int RepoDespatch::pack(void * buffer, void * gamedata) {

   CampaignData * campData = (CampaignData *) gamedata;

   MP_MSG_CAMPAIGNORDER * msg = (MP_MSG_CAMPAIGNORDER *) buffer;

   // order type
   msg->order_type = (unsigned int) CMP_MSG_REPOSP;

   int * dataptr = (int *) msg->data;
   int bytes=0;

   *dataptr = (int) CampaignOBUtil::cpiToCPIndex(d_cp, *campData->getArmies().ob() );
   dataptr++;
   bytes+=4;

   /*
   Do RepoSP
   */

   *dataptr = (int) CampaignOBUtil::spiToSPIndex(d_rsp->d_sp, *campData->getArmies().ob() );
   dataptr++;
   bytes+=4;
      
   *dataptr = (int) d_rsp->d_town;
   dataptr++;
   bytes+=4;

   *dataptr = (int) CampaignOBUtil::cpiToCPIndex(d_rsp->d_attachTo, *campData->getArmies().ob() );
   dataptr++;
   bytes+=4;

   *dataptr = (int) d_rsp->d_travelTime;
   dataptr++;
   bytes+=4;

   *dataptr = (int) d_rsp->d_active;
   dataptr++;
   bytes+=4;

   return bytes;
}



/*
Unpack a multiplayer message from buffer
NOTE : buffer points to MP_MSG_CAMPAIGNORDER
*/
void RepoDespatch::unpack(void * buffer, void * gamedata) {

   CampaignData * campData = (CampaignData *) gamedata;
   Armies& armies = campData->getArmies();

   MP_MSG_CAMPAIGNORDER * msg = (MP_MSG_CAMPAIGNORDER *) buffer;

   int * dataptr = (int *) msg->data;

   ICommandPosition cp = CampaignOBUtil::cpIndexToCPI(*dataptr, *campData->getArmies().ob() );
   dataptr++;

   ISP sp = CampaignOBUtil::spIndexToISP(*dataptr, *campData->getArmies().ob() );
   dataptr++;

   /*
   Locate the ReposSP entry in the CampData->RepoList.. this is the d_rsp we must be altering
   */
   RepoSP* rsp;
    SListIter<RepoSP> iter(&armies.repoSPList());
    while(++iter) {
      
      rsp = iter.current();
      if(rsp->d_sp == sp) goto got_sp;
   }

   FORCEASSERT("ERROR - Can't find SP in RepoSPList...\n");
   return;

got_sp:

   d_rsp = rsp;

   d_cp = cp;
   
   rsp->d_town = (ITown) *dataptr;
   dataptr++;

   rsp->d_attachTo = CampaignOBUtil::cpIndexToCPI(*dataptr, *campData->getArmies().ob() );
   dataptr++;

   rsp->d_travelTime = (TimeTick) *dataptr;
   dataptr++;

   rsp->d_active = *dataptr != 0;
   dataptr++;
}




void RepoSPOrder::send(ConstParamCP cp, const RepoSP* rsp)
{
   if (!rsp->isCompleted())
   {
      RepoDespatch* msg = new RepoDespatch(cp, rsp);
      Despatcher::sendMessage(msg);
   }
}

/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.2  2001/06/18 08:25:44  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
