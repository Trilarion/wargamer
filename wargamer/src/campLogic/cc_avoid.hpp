/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef CC_AVOID_HPP
#define CC_AVOID_HPP

#ifndef __cplusplus
#error cc_avoid.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *	Campaign Combat and Movement: Avoid Contact / Withdraw
 *
 *----------------------------------------------------------------------
 */

#include "gamedefs.hpp"
#include "cpdef.hpp"
//#include "measure.hpp"

class CampaignData;
//class Armies;
class CloseUnits;

struct RetreatingUnitData {
  ICommandPosition d_cpi;

  RetreatingUnitData(ICommandPosition cpi) :
	 d_cpi(cpi) {}
};

class CampaignAvoid {
 public:
	static ITown pickRetreatTown(CampaignData* campData, ICommandPosition cpi, CloseUnits& closeUnits);
	static Boolean avoid(CampaignData* campData, ICommandPosition cpi, CloseUnits& closeUnits);
		// Return True if we got out of avoid mode
};

#endif /* CC_AVOID_HPP */

