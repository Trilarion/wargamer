# Microsoft Developer Studio Project File - Name="clogic" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Dynamic-Link Library" 0x0102

CFG=clogic - Win32 Debug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "clogic.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "clogic.mak" CFG="clogic - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "clogic - Win32 Release" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE "clogic - Win32 Debug" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""
CPP=cl.exe
MTL=midl.exe
RSC=rc.exe

!IF  "$(CFG)" == "clogic - Win32 Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "..\..\exe"
# PROP Intermediate_Dir "o\release"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MT /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "CLOGIC_EXPORTS" /YX /FD /c
# ADD CPP /nologo /MD /W3 /Gi /GR /GX /Zi /O2 /I "..\h" /I "..\..\res" /I "..\system" /I "..\gamesup" /I "..\ob" /I "..\campdata" /D "NDEBUG" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "EXPORT_CLOGIC_DLL" /YX"stdinc.hpp" /FD /c
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x809 /d "NDEBUG"
# ADD RSC /l 0x809 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /dll /machine:I386
# ADD LINK32 /nologo /dll /debug /machine:I386

!ELSEIF  "$(CFG)" == "clogic - Win32 Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "..\..\exe"
# PROP Intermediate_Dir "o\debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MTd /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "CLOGIC_EXPORTS" /YX /FD /GZ /c
# ADD CPP /nologo /MDd /W3 /Gm /Gi /GR /GX /ZI /Od /I "..\h" /I "..\..\res" /I "..\system" /I "..\gamesup" /I "..\ob" /I "..\campdata" /D "_DEBUG" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "EXPORT_CLOGIC_DLL" /YX"stdinc.hpp" /FD /GZ /c
# ADD BASE MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x809 /d "_DEBUG"
# ADD RSC /l 0x809 /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /dll /debug /machine:I386 /pdbtype:sept
# ADD LINK32 /nologo /dll /debug /machine:I386 /out:"..\..\exe/clogicDB.dll" /pdbtype:sept

!ENDIF 

# Begin Target

# Name "clogic - Win32 Release"
# Name "clogic - Win32 Debug"
# Begin Group "Source Files"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat"
# Begin Source File

SOURCE=.\atrition.cpp
# End Source File
# Begin Source File

SOURCE=.\c_const.cpp
# End Source File
# Begin Source File

SOURCE=.\c_supply.cpp
# End Source File
# Begin Source File

SOURCE=.\c_weathr.cpp
# End Source File
# Begin Source File

SOURCE=.\campcomb.cpp
# End Source File
# Begin Source File

SOURCE=.\cbatmsg.cpp
# End Source File
# Begin Source File

SOURCE=.\cbatproc.cpp
# End Source File
# Begin Source File

SOURCE=.\cc_avoid.cpp
# End Source File
# Begin Source File

SOURCE=.\cc_close.cpp
# End Source File
# Begin Source File

SOURCE=.\cc_town.cpp
# End Source File
# Begin Source File

SOURCE=.\cc_util.cpp
# End Source File
# Begin Source File

SOURCE=.\cmsgutil.cpp
# End Source File
# Begin Source File

SOURCE=.\condutil.cpp
# End Source File
# Begin Source File

SOURCE=.\cu_data.cpp
# End Source File
# Begin Source File

SOURCE=.\cu_fog.cpp
# End Source File
# Begin Source File

SOURCE=.\cu_ldr.cpp
# End Source File
# Begin Source File

SOURCE=.\cu_move.cpp
# End Source File
# Begin Source File

SOURCE=.\cu_order.cpp
# End Source File
# Begin Source File

SOURCE=.\cu_rand.cpp
# End Source File
# Begin Source File

SOURCE=.\cu_repo.cpp
# End Source File
# Begin Source File

SOURCE=.\cu_speed.cpp
# End Source File
# Begin Source File

SOURCE=.\defect.cpp
# End Source File
# Begin Source File

SOURCE=.\despatch.cpp
# End Source File
# Begin Source File

SOURCE=.\ds_reorg.cpp
# End Source File
# Begin Source File

SOURCE=.\ds_reposp.cpp
# End Source File
# Begin Source File

SOURCE=.\ds_town.cpp
# End Source File
# Begin Source File

SOURCE=.\ds_unit.cpp
# End Source File
# Begin Source File

SOURCE=.\losses.cpp
# End Source File
# Begin Source File

SOURCE=.\realord.cpp
# End Source File
# Begin Source File

SOURCE=.\recover.cpp
# End Source File
# Begin Source File

SOURCE=.\sched.cpp
# End Source File
# Begin Source File

SOURCE=.\siege.cpp
# End Source File
# Begin Source File

SOURCE=.\townprop.cpp
# End Source File
# End Group
# Begin Group "Header Files"

# PROP Default_Filter "h;hpp;hxx;hm;inl"
# Begin Source File

SOURCE=.\atrition.hpp
# End Source File
# Begin Source File

SOURCE=.\c_const.hpp
# End Source File
# Begin Source File

SOURCE=.\c_supply.hpp
# End Source File
# Begin Source File

SOURCE=.\c_weathr.hpp
# End Source File
# Begin Source File

SOURCE=.\campcomb.hpp
# End Source File
# Begin Source File

SOURCE=.\cbatmsg.hpp
# End Source File
# Begin Source File

SOURCE=.\cbatproc.hpp
# End Source File
# Begin Source File

SOURCE=.\cc_avoid.hpp
# End Source File
# Begin Source File

SOURCE=.\cc_close.hpp
# End Source File
# Begin Source File

SOURCE=.\cc_town.hpp
# End Source File
# Begin Source File

SOURCE=.\cc_util.hpp
# End Source File
# Begin Source File

SOURCE=.\clogic_dll.h
# End Source File
# Begin Source File

SOURCE=.\cmsgutil.hpp
# End Source File
# Begin Source File

SOURCE=.\condutil.hpp
# End Source File
# Begin Source File

SOURCE=.\cu_data.hpp
# End Source File
# Begin Source File

SOURCE=.\cu_fog.hpp
# End Source File
# Begin Source File

SOURCE=.\cu_ldr.hpp
# End Source File
# Begin Source File

SOURCE=.\cu_move.hpp
# End Source File
# Begin Source File

SOURCE=.\cu_order.hpp
# End Source File
# Begin Source File

SOURCE=.\cu_rand.hpp
# End Source File
# Begin Source File

SOURCE=.\cu_repo.hpp
# End Source File
# Begin Source File

SOURCE=.\cu_speed.hpp
# End Source File
# Begin Source File

SOURCE=.\defect.hpp
# End Source File
# Begin Source File

SOURCE=.\despatch.hpp
# End Source File
# Begin Source File

SOURCE=.\ds_reorg.hpp
# End Source File
# Begin Source File

SOURCE=.\ds_reposp.hpp
# End Source File
# Begin Source File

SOURCE=.\ds_town.hpp
# End Source File
# Begin Source File

SOURCE=.\ds_unit.hpp
# End Source File
# Begin Source File

SOURCE=.\losses.hpp
# End Source File
# Begin Source File

SOURCE=.\realord.hpp
# End Source File
# Begin Source File

SOURCE=.\recover.hpp
# End Source File
# Begin Source File

SOURCE=.\sched.hpp
# End Source File
# Begin Source File

SOURCE=.\siege.hpp
# End Source File
# Begin Source File

SOURCE=.\stdinc.hpp
# End Source File
# Begin Source File

SOURCE=.\townprop.hpp
# End Source File
# End Group
# Begin Group "Resource Files"

# PROP Default_Filter "ico;cur;bmp;dlg;rc2;rct;bin;rgs;gif;jpg;jpeg;jpe"
# End Group
# End Target
# End Project
