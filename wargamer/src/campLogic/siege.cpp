/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1995, Steven Morle-Green, All Rights Reserved
 *----------------------------------------------------------------------
 *
 * Sieges, Raids, Storms, etc...
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/07/02 15:23:16  greenius
 * Battle, Campaign, Combined game and Campaign Editor now compile and run... mostly
 *
 * Revision 1.2  2001/06/18 08:25:44  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 * Revision 1.1  2001/06/13 08:52:37  greenius
 * Initial Import
 *
 *
 *----------------------------------------------------------------------
 */


#include "stdinc.hpp"
#include "siege.hpp"
#include "siegeint.hpp"
#include "campdint.hpp"
#include "campctrl.hpp"
#include "cc_town.hpp"
#include "cu_order.hpp"
#include "armies.hpp"
#include "town.hpp"
#include "scenario.hpp"
#include "campmsg.hpp"
#include "losses.hpp"
#include "cu_mode.hpp"
#include "fsalloc.hpp"
#include "wg_rand.hpp"
#include "armyutil.hpp"
#include "gamectrl.hpp"
#include "weather.hpp"
#include "control.hpp"

#ifdef DEBUG
#include "clog.hpp"
#include "unitlog.hpp"
LogFile siegeLog("siege.log");
#endif

using namespace WG_CampaignSiege;


#ifdef DEBUG
static FixedSizeAlloc<CPListItem> cpItemAlloc("CPListItem");
#else
static FixedSizeAlloc<CPListItem> cpItemAlloc;
#endif

void* CPListItem::operator new(size_t size)
{
  return cpItemAlloc.alloc(size);
}

#ifdef _MSC_VER
void CPListItem::operator delete(void* deadObject)
{
  cpItemAlloc.release(deadObject);
}
#else
void CPListItem::operator delete(void* deadObject, size_t size)
{
  cpItemAlloc.release(deadObject, size);
}
#endif




/*
 * Private Data for siege calculations
 */

class SiegeUtil {
 public:
   static void dailyTownSiege(CampaignLogicOwner* campGame, ITown t);
   static void weeklyTownSiege(CampaignLogicOwner* campGame, ITown t);
   static SPCount getInfantrySPValue(const ICommandPosition& cpi);
};

class SiegeAction {
    CampaignLogicOwner* d_campGame;
    CampaignData* d_campData;

    CPList d_attackList;
    CPList d_defendList;

    ITown d_iTown;

    ActionType d_actionType;

    /*
     * Die roll modifier array indexes
     * For accesing Die Roll modifier table
     * Enumeration must match SiegeDieRollModifierTable in tables.dat
     */

    enum {
      FortLevelMod,      //  index for Fort Level modifier
      EngineerMod,       //  index for Engineer present modifier
      SiegeTrainMod,     //  index for Siege Train present modifier
      SurrenderMinMod,   //  index for Surrender Minimum reached modifier
      MoraleDoubleMod,   //  index for Morale double opponents modifier
      Morale50Mod,       //  index for Morale 50% greater than opponents
      CharismaMod,       //  index for Charisma is 50% greater than opponents
      DevBombMod,        //  index for Devastating Bombardment modifier
      DefenderSupplyMod, //  index for Defender's supply is gone modifier
      HeavyMud
    };

  public:
    SiegeAction(CampaignLogicOwner* campGame, ITown i);

    static void startSiege(CampaignLogicOwner* campGame, ITown iTown, ICommandPosition hunit);
    static Boolean startRaid(CampaignLogicOwner* campGame, ITown iTown, ICommandPosition hunit);
    static Boolean canRaid(CampaignData* campData, ITown iTown, Side side);
    static void retreatRaiders(CampaignData* campData, ITown iTown, Side side);

    void beginAction();  // begins weekly check actions
    void beginStorm();   // begins storm actions
    void beginSally();   // begins Sally actions
    Boolean doDailyAction();
    Boolean canSiege();

    CPList& attackList() { return d_attackList; }
    CPList& defendList() { return d_defendList; }
    void sendResult(ITown t);

  private:

    void doAction(ActionType actionType);
    void doStorm();
    void doSally();
    void doSortie();
    void doMinorOperations();
    void doBreach();
    void doBreachAndDB();
    void doRandomEvent();
    void doDefenderEvent();
    void doAttackerEvent();
    SPCount implementLosses(CPList& list, SPLoss& losses, BasicUnitType::value basicType = BasicUnitType::HowMany);
    void doRetreat(CPList& list);
    void doRaid(CPList& list);
    void doSurrenderCheck();
    void doDestroyGarrison();
    void takeTown();
    Boolean defendersRemain() { return d_defendList.entries() > 0; }
    Boolean attackersRemain() { return d_attackList.entries() > 0; }

    // support functions
    void fillUnitLists(Boolean noStartValues = False);
    void setFlags();

    ICommandPosition getLeaderUnit(CPList& list);
    SPCount getTotalSPCount(CPList& list);
//  SPCount getNInfantry(CPList& list);
    Attribute getAverageMorale(CPList& list);
    Attribute getCharisma(CPList& list);
    Attribute getSupply(CPList& list);

    Boolean isAttackerGreater() { return (d_attackList.infSP() >= d_defendList.infSP()); }
};

SiegeAction::SiegeAction(CampaignLogicOwner* campGame, ITown i) :
      d_campGame(campGame),
      d_campData(campGame->campaignData()),
      d_iTown(i)
{
}



#ifdef DEBUG
void CPList::print(const Armies* armies, const char* title)
{
  if(title)
    siegeLog.printf("%s", title);

  CPListItem* item = first();
  while(item)
  {
    siegeLog.printf("%d: %s", (int) armies->getIndex(item->d_cpi), item->d_cpi->getName());

    item = next();
  }
}
#endif

void SiegeAction::fillUnitLists(Boolean noStartValues)
{
   ASSERT(d_iTown != NoTown);

   Armies* armies = &d_campData->getArmies();

   ULONG   attackMP = 0;
   ULONG   defendMP = 0;
   SPCount attackCount = 0;
   SPCount defendCount = 0;
   SPCount attackInf = 0;
   SPCount defendInf = 0;

   d_defendList.reset();
   d_attackList.reset();

   const Town& town = d_campData->getTown(d_iTown);

   CPIter uIter(armies);
   while(uIter.sister())
   {
      ICommandPosition cpi = uIter.current();

      if(cpi->atTown() && (cpi->getTown() == d_iTown))
      {
         // Modified SWG: 12Sep99 : All units belonging to town are defenders
         if(cpi->getSide() == town.getSide())
         {

            if(cpi->isGarrison())// && !d_defendList.hasSurrendered())
            {
               d_defendList.append(new CPListItem(cpi));

               if(d_defendList.hasSurrendered())
               {
#ifdef DEBUG
                  siegeLog.printf("%s is due to surrender", cpi->getName());
#endif
               }
               else
               {
#ifdef DEBUG
                  siegeLog.printf("%s is under siege", cpi->getName());
#endif
                  defendCount += armies->getUnitSPValue(cpi);
                  defendInf += armies->getUnitSPValue(cpi, BasicUnitType::Infantry);
                  defendMP += CampaignArmy_Util::totalManpower(d_campData, cpi);
               }
            }

            if(!noStartValues)
            {
              d_defendList.side(cpi->getSide());
            }
         }

         // SWG: 12Sep99 : Added isRaiding() so that if several small units
         //                arrive seperately it will take over a town.
         // Modified again: Remove all conditions so even tiny units are added in
         else  // if(cpi->isSieging() || cpi->isRaiding())
         {
            d_attackList.append(new CPListItem(cpi));

            attackCount += armies->getUnitSPValue(cpi);
            attackInf += armies->getUnitSPValue(cpi, BasicUnitType::Infantry);
            attackMP += CampaignArmy_Util::totalManpower(d_campData, cpi);

            if(!noStartValues)
            {
              d_attackList.side(cpi->getSide());
            }
#ifdef DEBUG
            siegeLog.printf("%s is sieging", cpi->getName());
#endif
         }
// #ifdef DEBUG
//          else
//             siegeLog.printf("%s is hanging around!", cpi->getName());
// #endif
      }
   }

#ifdef DEBUG
   d_attackList.print(armies, "AttackList contains:");
   d_defendList.print(armies, "DefendList contains:");

   // get fortification level and determine number of sp allowed in garrison
   UBYTE fortLevel = town.getFortifications();
   siegeLog.printf("fortLevel = %d", static_cast<int>(fortLevel));
#endif

   // add spcounts.
   if(!noStartValues)
   {
     d_attackList.startManpower(attackMP);
     d_attackList.startSPCount(attackCount);

     d_defendList.startManpower(defendMP);
     d_defendList.startSPCount(defendCount);
   }

   d_attackList.spCount(attackCount);
   d_defendList.spCount(defendCount);

   d_attackList.infSP(attackInf);
   d_defendList.infSP(defendInf);

   d_attackList.endManpower(attackMP);
   d_defendList.endManpower(defendMP);

#ifdef DEBUG
   SPCount nTotalAllowed = static_cast<SPCount>(town.getStrength() * fortLevel);
   siegeLog.printf("Total SP's allowed in garrison = %d", (int)nTotalAllowed);
#endif
}

Boolean SiegeAction::canSiege()
{
  ASSERT(d_iTown != NoTown);

  fillUnitLists();
  return isAttackerGreater();
}

Boolean SiegeAction::doDailyAction()
{
   // increment siege counter
   ASSERT(d_iTown != NoTown);
   Town& town = d_campData->getTown(d_iTown);
   ASSERT(town.getFortifications() == 1);

   /*
    * If active siege
    */

   if(canSiege())
   {
     setFlags();
     if(d_attackList.autoStorm())
     {
       d_attackList.actionType(SA_Storm);
       d_defendList.actionType(SA_Storm);

       doStorm();

       if(d_defendList.hasSurrendered())
       {
         doDestroyGarrison();
         takeTown();
       }

       else if(!isAttackerGreater())
       {
         doRetreat(d_attackList);
       }

       return True;
     }

     return False;

   }
   else
     doRetreat(d_attackList);

   return False;
}

void SiegeAction::beginAction()
{
   // increment siege counter
   ASSERT(d_iTown != NoTown);
   Town& town = d_campData->getTown(d_iTown);

#ifdef DEBUG
   siegeLog.printf("\n----------------------------------------------------------");
   siegeLog.printf("%s size is %d", town.getNameNotNull(), (int)town.getStrength());
#endif

   fillUnitLists();

   // make sure attacker can still siege
   if(!isAttackerGreater())
   {
      doRetreat(d_attackList);
      return;
   }
   if(CampUnitTown::getUnitToTownRatio(d_campData, d_iTown, d_attackList.side()) <= UnitToTownRatio::LessThan)
   {
      doRaid(d_attackList);
      return;
   }

   town.incrementWeeksUnderSiege();
   setFlags();

   if(town.surrenderMinimumReached() &&
      !d_attackList.siegeActive())
   {
     doSurrenderCheck();
   }

   if(!d_defendList.hasSurrendered() &&
       d_attackList.siegeActive() &&
       town.getFortifications() > 0)
   {
     doRandomEvent();

     int n = CRandom::get(100);
#ifdef DEBUG
     siegeLog.printf("beginAction random = %d", n);
#endif
     // add modifiers

     /*
      * Get die roll modifier table
      */

     const Table1D<SWORD>* dieRollModifier = scenario->getSiegeDieRollModifier();
     ASSERT(dieRollModifier != 0);

     /*
      *  modifier for fortification level
      */

     n += (town.getFortifications() * dieRollModifier->getValue(FortLevelMod));

#ifdef DEBUG
     siegeLog.printf("beginAction after fort modifier = %d", n);
#endif

     /*
      *  modifier for engineer if one present.
      */

     if(d_attackList.hasEngineer())
       n += dieRollModifier->getValue(EngineerMod);

     /*
      *  modifier for siege train if one present.
      */

     if(d_attackList.hasSiegeTrain())
       n += dieRollModifier->getValue(SiegeTrainMod);


#ifdef DEBUG
     siegeLog.printf("beginAction after engineer modifier = %d", n);
#endif

     /*
      *  modifier for heavy mud.
      */


     if(d_campData->getWeather().getGroundConditions() == CampaignWeather::VeryMuddy)
       n += dieRollModifier->getValue(HeavyMud);

#ifdef DEBUG
     siegeLog.printf("beginAction after ground-conditions modifier = %d", n);
#endif

     /*
      *  modifier for surrender minimum.  if so add 5.
      */

     if(town.surrenderMinimumReached())
       n += dieRollModifier->getValue(SurrenderMinMod);

#ifdef DEBUG
     siegeLog.printf("beginAction after surrender minimun modifier = %d", n);
#endif

    /*
     *  Divide roll by 10 for table array, make sure n >= 0 && n < 100
     */

     n = maximum(0, minimum(99, n));
     n /= 10;

     const Table1D<UBYTE>* table = (d_attackList.hasSiegeTrain()) ?
        scenario->getActiveSiegeTableTrain() :
        scenario->getActiveSiegeTableNoTrain();

     ASSERT(table != 0);
     ActionType actionType = static_cast<ActionType>(table->getValue(n));
     d_attackList.actionType(actionType);
     d_defendList.actionType(actionType);

     doAction(actionType);

     /*
      * do surrender check if defenders still have SP's
      */

     if(!d_defendList.hasSurrendered() && defendersRemain())
     {
       doSurrenderCheck();
     }
   }

   if(!defendersRemain())
   {
     d_attackList.wonSiege(True);
     d_defendList.hasSurrendered(False);
     takeTown();
   }
   else if(d_defendList.hasSurrendered())
   {
     doDestroyGarrison();
     takeTown();
   }

   else if(!isAttackerGreater())
   {
     doRetreat(d_attackList);
   }

   else if(CampUnitTown::getUnitToTownRatio(d_campData, d_iTown, d_attackList.side()) <= UnitToTownRatio::LessThan)
   {
      doRaid(d_attackList);
   }
}


void SiegeAction::beginStorm()
{
}

void SiegeAction::setFlags()
{
  Armies* armies = &d_campData->getArmies();

  /*
   * Attacker flags
   */

  d_attackList.isAttacker(True);

  CPListItem* item = d_attackList.first();
  while(item)
  {
    /*
     * Set engineer flag
     */

    ICommandPosition cpi;
    ISP isp;
    if(armies->findSpecialType(item->d_cpi, cpi, isp, SpecialUnitType::Engineer))
      d_attackList.hasEngineer(True);

    /*
     * Set siege train flag
     */

    if(armies->findSpecialType(item->d_cpi, cpi, isp, SpecialUnitType::SiegeTrain))
      d_attackList.hasSiegeTrain(True);

    /*
     * Active / passive Siege
     * Attacker must also have more effective infantry than defenders
     * total effective count
     */

    if( (item->d_cpi->isSiegeActive()) &&
        (d_attackList.infSP() >= d_defendList.infSP()) )
    {
      d_attackList.siegeActive(True);

      /*
       * Have auto siege
       */

      if(item->d_cpi->getCurrentOrder()->getAutoStorm())
        d_attackList.autoStorm(True);
    }


    item = d_attackList.next();
  }
}


void SiegeAction::doAction(ActionType actionType)
{
  switch(actionType)
  {
    case SA_Sortie:
#ifdef DEBUG
      siegeLog.printf("Action is Sortie");
#endif
      doSortie();
      break;
    case SA_MinorOperations:
#ifdef DEBUG
      siegeLog.printf("Action is Minor Operations");
#endif
      doMinorOperations();
      break;
    case SA_NoEffect:
#ifdef DEBUG
      siegeLog.printf("Action is No Effect");
#endif
      break;
    case SA_Breach:
#ifdef DEBUG
      siegeLog.printf("Action is Breach");
#endif
      doBreach();
      break;
    case SA_BreachAndDB:
#ifdef DEBUG
      siegeLog.printf("Action is Breach and Devastating Bombardment");
#endif
      doBreachAndDB();
      break;
#ifdef DEBUG
    default:
      GeneralError error("Improper actionType in SiegeAction::doAction()");
#endif
  }
}

void SiegeAction::doRandomEvent()
{
  int n = CRandom::get(100);
#ifdef DEBUG
  siegeLog.printf("randomEvent: random = %d", n);
#endif


  if(n < 5)
    doDefenderEvent();

  else if(n >= 95)
    doAttackerEvent();
}


void SiegeAction::doDefenderEvent()
{
  int n = CRandom::get(100);

#ifdef DEBUG
  siegeLog.printf("defenderEvent: random = %d", n);
#endif


  if(n < 33)
  {
#ifdef DEBUG
    siegeLog.printf("defenderEvent: Supply Magazine explodes");
#endif
    ASSERT(d_iTown != NoTown);
    Town& town = d_campData->getTown(d_iTown);
#ifdef DEBUG
    siegeLog.printf("defenderEvent: Supply level before = %d", (int)town.getSupplyLevel());
#endif

    town.setSupplyLevel(town.getSupplyLevel() / 2);

    {
      CampaignMessageInfo msgInfo(town.getSide(), CampaignMessages::MortarShellExploded);
      msgInfo.where(d_iTown);
      d_campGame->sendPlayerMessage(msgInfo);
    }

#ifdef DEBUG
    siegeLog.printf("defenderEvent: Supply level after = %d", (int)town.getSupplyLevel());
#endif

  }
  else if(n < 66)
  {
    ICommandPosition cpi = getLeaderUnit(d_defendList);

#ifdef DEBUG
    {
      siegeLog.printf("defenderEvent: Leader killed. Dead Leader is %s",
          cpi->getLeader()->getNameNotNull());
    }
#endif

    CampaignMessageInfo msg(cpi->getSide(), CampaignMessages::GeneralKilled);
    msg.cp(cpi);
    msg.leader(cpi->getLeader());
    d_campGame->sendPlayerMessage(msg);

    CampaignArmy_Util::killLeader(d_campData, cpi->getLeader());

#ifdef DEBUG
    {
      siegeLog.printf("defenderEvent: Leader killed. New Leader is %s",
          cpi->getLeader()->getNameNotNull());
    }
#endif
  }
  else if(n < 100)
  {
#ifdef DEBUG
    siegeLog.printf("defenderEvent: Poor construction revealed");
#endif

    ASSERT(d_iTown != NoTown);
    Town& town = d_campData->getTown(d_iTown);

#ifdef DEBUG
    siegeLog.printf("defenderEvent: Fort level before = %d", (int)town.getFortifications());
#endif

    if(town.getFortifications() > 1)
      town.setFortifications(town.getFortifications() - 1);

#ifdef DEBUG
    siegeLog.printf("defenderEvent: Fort level after = %d", (int)town.getFortifications());
#endif
    {
      CampaignMessageInfo msgInfo(town.getSide(), CampaignMessages::PoorConstruction);
      msgInfo.where(d_iTown);
      d_campGame->sendPlayerMessage(msgInfo);
    }
  }
}

void SiegeAction::doAttackerEvent()
{
  int n = CRandom::get(100);
#ifdef DEBUG
  siegeLog.printf("attackerEvent: random = %d", n);
#endif
  if(n < 33)
  {


  }
  else if(n < 66)
  {
    ICommandPosition cpi = getLeaderUnit(d_attackList);

#ifdef DEBUG
    {
      siegeLog.printf("defenderEvent: Leader killed. Dead Leader is %s",
          cpi->getLeader()->getNameNotNull());
    }
#endif

    CampaignMessageInfo msg(cpi->getSide(), CampaignMessages::GeneralKilled);
    msg.cp(cpi);
    msg.leader(cpi->getLeader());
    d_campGame->sendPlayerMessage(msg);

    CampaignArmy_Util::killLeader(d_campData, cpi->getLeader());

#ifdef DEBUG
    {
      siegeLog.printf("defenderEvent: Leader killed. New Leader is %s",
          cpi->getLeader()->getNameNotNull());
    }
#endif
  }
  else if(n < 100)
  {


  }
}

void SiegeAction::doStorm()
{

  SPCount nAttackInfantry = d_attackList.infSP(); // getNInfantry(d_attackList);
  SPCount nDefendInfantry = d_defendList.infSP(); // getNInfantry(d_defendList);

#ifdef DEBUG
  siegeLog.printf("doStorm nAttackInfantry = %d, nDefendInfantry = %d", (int)nAttackInfantry, (int)nDefendInfantry);
#endif

  ASSERT(d_iTown);
  const Town& t = d_campData->getTown(d_iTown);

  /*
   * Determine percentage of defending force allowed in garrison and
   * adjust nDefendInfantry accordingly.
   */

  SPCount nTotalAllowed = static_cast<SPCount>(t.getStrength() * t.getFortifications());

  if(d_defendList.spCount() > 0 &&
     d_defendList.spCount() > nTotalAllowed)
  {
    ASSERT(nTotalAllowed > 0);
    int percent = MulDiv(100, nTotalAllowed, d_defendList.spCount());

    nDefendInfantry = static_cast<SPCount>(MulDiv(nDefendInfantry, percent, 100));
  }

#ifdef DEBUG
  siegeLog.printf("doStorm nDefendInfantry after size adj = %d", (int)nDefendInfantry);
#endif

  Attribute attackMorale = getAverageMorale(d_attackList);
  Attribute defendMorale = getAverageMorale(d_defendList);
#ifdef DEBUG
  siegeLog.printf("doStorm attackMorale = %d, defendMorale = %d", (int)attackMorale, (int)defendMorale);
#endif

  Attribute attackCharisma = getCharisma(d_attackList);
  Attribute defendCharisma = getCharisma(d_defendList);
#ifdef DEBUG
  siegeLog.printf("doStorm attackCharisma = %d, defendCharisma = %d", (int)attackCharisma, (int)defendCharisma);
#endif

  int n = CRandom::get(100);

#ifdef DEBUG
  siegeLog.printf("doStorm random = %d", n);
#endif

  // add modifiers

  /*
   * Get Die  roll modifier table
   */

  const Table1D<SWORD>* dieRollModifier = scenario->getSiegeDieRollModifier();
  ASSERT(dieRollModifier != 0);

  /*
   *  modifiers for morale. If attacker 50% greater than defender add 15.
   *  If attacker double defender add 30. Inverse if defender has higher morale
   */

  int moreThanDouble = dieRollModifier->getValue(MoraleDoubleMod);
  int moreThan50 = dieRollModifier->getValue(Morale50Mod);

  if((attackMorale - defendMorale) >= defendMorale)
    n += moreThanDouble;
  else if((attackMorale - defendMorale) >= (defendMorale/2))
    n += moreThan50;
  else if((defendMorale - attackMorale) >= attackMorale)
    n -= moreThanDouble;
  else if((defendMorale - attackMorale) >= (attackMorale/2))
    n -= moreThan50;

#ifdef DEBUG
  siegeLog.printf("doStorm after morale modifier = %d", n);
#endif


  /*
   *  modifiers for leader charisma. if attacker 50% greater add 10. Inverse
   *  if defender greater by 50%.
   */

  int charismaMod = dieRollModifier->getValue(CharismaMod);

  if((attackCharisma - defendCharisma) >= (defendCharisma/2))
    n += charismaMod;
  else if((defendCharisma - attackCharisma) >= (attackCharisma/2))
    n -= charismaMod;

#ifdef DEBUG
  siegeLog.printf("doStorm after charisma modifier = %d", n);
#endif

  // if following devastating bombardment add 10
  ASSERT(d_iTown != NoTown);
  Town& town = d_campData->getTown(d_iTown);
  if(town.isDevBomb())
    n += dieRollModifier->getValue(DevBombMod);

#ifdef DEBUG
  siegeLog.printf("doStorm after DevBomb modifier = %d", n);
#endif

  // modifier for defenders supply. If 0 add 10
  if(town.getSupplyLevel() == 0)
    n += dieRollModifier->getValue(DefenderSupplyMod);

#ifdef DEBUG
  siegeLog.printf("doStorm after supply modifier = %d", n);
#endif

  enum StormResult {
      SR_Fails,
      SR_Succeeds
  } stormResult;

  /*
   *  Make sure n >= 0 and n < 100. Divide die roll by ten for table array
   */

  n = maximum(0, minimum(n, 99));
  n /= 10;

  /*
   * Fatigue is reduced by 10
   */

  CPListItem* item = d_attackList.first();
  while(item)
  {
    d_campData->getArmies().applyFatigue(item->d_cpi, -10);
    item = d_attackList.next();
  }

  /*
   *  Get attackers(besieger) and defenders(besieged) losses
   *  Losses are in % losses
   *
   */

  const Table1D<int>* attackerTable = scenario->getStormLossTableAttacker();
  ASSERT(attackerTable != 0);

  int attackPercent = attackerTable->getValue(n);

  /*
   *  Get result
   */

  const Table1D<UBYTE>* resultTable = scenario->getStormResultTable();
  ASSERT(resultTable != 0);

  stormResult = (StormResult)resultTable->getValue(n);

  int num = minimum(32767, nDefendInfantry * attackPercent);
  SPLoss attackLosses(0, static_cast<UWORD>(num), 1000);
#ifdef DEBUG
  siegeLog.printf("Attacker Storm's. losses = %d.%x SP's", attackLosses.getInt(), attackLosses.getFraction());
#endif

  implementLosses(d_attackList, attackLosses, BasicUnitType::Infantry);

  if(stormResult == SR_Fails)
  {
    const Table1D<int>* defenderTable = scenario->getStormLossTableDefender();
    ASSERT(defenderTable != 0);

    int defendPercent = defenderTable->getValue(n);

    // halv losses if no breach has been blown
    if(!d_attackList.breachBlown())
      defendPercent /= 2;

    int num = minimum(32767, nAttackInfantry * defendPercent);
    SPLoss defendLosses(0, static_cast<UWORD>(num), 1000);

#ifdef DEBUG
    siegeLog.printf("Defend Storm. Before fort mod, losses = %d.%x SP's", defendLosses.getInt(), defendLosses.getFraction());
#endif

    defendLosses /= t.getFortifications();

#ifdef DEBUG
    siegeLog.printf("Fortification level = %d", static_cast<int>(t.getFortifications()));
    siegeLog.printf("Defend Storm. After fort mod, losses = %d.%x SP's", defendLosses.getInt(), defendLosses.getFraction());
#endif

    implementLosses(d_defendList, defendLosses, BasicUnitType::Infantry);
  }
  else
    d_defendList.hasSurrendered(True);

  fillUnitLists(True);
}


void SiegeAction::doSortie()
{
  /*
   *  Get loss tables.
   *  Losses are in SP's.
   */

  const Table1D<UBYTE>* attackerTable = scenario->getSortieTableAttacker();
  const Table1D<UBYTE>* defenderTable = scenario->getSortieTableDefender();

  /*
   *  20 possible values in table
   */

  const int nTableValues = attackerTable->getWidth();
  ASSERT(nTableValues == defenderTable->getWidth());

  int n = CRandom::get(nTableValues);
  ASSERT(n < nTableValues);


  /*
   * Get loss results
   */


  SPLoss attackerLosses = static_cast<UWORD>(attackerTable->getValue(n));
  SPLoss defenderLosses = static_cast<UWORD>(defenderTable->getValue(n));

#ifdef DEBUG
  siegeLog.printf("Besieger Losses in Sortie are %d SP's", attackerLosses.getInt());
  siegeLog.printf("Besieged Losses in Sortie are %d SP's", defenderLosses.getInt());
#endif

  implementLosses(d_attackList, attackerLosses);
  implementLosses(d_defendList, defenderLosses);

  fillUnitLists(True);
}

void SiegeAction::doMinorOperations()
{
  ASSERT(d_iTown != NoTown);
  const Town& t = d_campData->getTown(d_iTown);
  SPCount nInfantry = d_attackList.infSP(); // getNInfantry(d_attackList);

#ifdef DEBUG
  siegeLog.printf("Besieger has %d infantry", nInfantry);
#endif

  /*
   *  Calculate results.
   *
   */

  int chance = minimum(100, t.getFortifications() * nInfantry);

  int n = CRandom::get(100);

#ifdef DEBUG
  siegeLog.printf("Minor Op. chance = %d, random = %d", chance, n);
#endif

  if(n <= chance)
  {
    SPLoss attackerLosses = 1;
#ifdef DEBUG
    siegeLog.printf("Besieger losses %d infantry", attackerLosses.getInt());
    siegeLog.printf("nInfantry before losses  = %d", (int)nInfantry);
#endif

    implementLosses(d_attackList, attackerLosses);
    fillUnitLists(True);

#ifdef DEBUG
    if(attackersRemain())
    {
      nInfantry = d_attackList.infSP();
      siegeLog.printf("nInfantry after losses  = %d", (int)nInfantry);
    }
#endif
  }
}

void SiegeAction::doBreach()
{
  /*
   *  Send Message indicating breach has occured.
   *  For now will send it from first attacking unit in list
   */

  ASSERT(d_iTown != NoTown);
  const Town& t = d_campData->getTown(d_iTown);

  CPListItem* item = d_attackList.first();
  ASSERT(item != 0);

  d_attackList.breachBlown(True);

  {
    CampaignMessageInfo msgInfo(item->d_cpi->getSide(), CampaignMessages::BreachedFort);
    msgInfo.where(d_iTown);
    msgInfo.cp(item->d_cpi);
    d_campGame->sendPlayerMessage(msgInfo);
  }
  {
    CampaignMessageInfo msgInfo(t.getSide(), CampaignMessages::FortBreached);
    msgInfo.where(d_iTown);
    d_campGame->sendPlayerMessage(msgInfo);
  }

  doStorm();
}


void SiegeAction::doBreachAndDB()
{
  ASSERT(d_iTown);
  Town& t = d_campData->getTown(d_iTown);

  /*
   * defender suffers % losses = to 5 - d_fortLevel
   */

  int percent = (5 - t.getFortifications());

  // if siegetrain present double chance of losses
  int chance = (d_attackList.hasSiegeTrain()) ?
      (percent * d_defendList.spCount()) * 2 : percent * d_defendList.spCount();

  int n = CRandom::get(100);

#ifdef DEBUG
  siegeLog.printf("Devastating Bombardment. chance = %d, random = %d", chance, n);
#endif

  if(n <= chance)
  {
    int num = minimum(32767, d_attackList.spCount() * percent);
    SPLoss defendLosses(0, static_cast<UWORD>(num), 100);

#ifdef DEBUG
    siegeLog.printf("Devastating Bombardment. losses = %d.%x SP's", defendLosses.getInt(), defendLosses.getFraction());
#endif

    /*
     * if engineer or siegetrain unit present with attackers and fortlevel is
     * greater than 1, reduce fortlevel by one
     */

    if( (d_attackList.hasEngineer() || d_attackList.hasSiegeTrain()) &&
        t.getFortifications() > 1)
    {
      t.setFortifications(t.getFortifications() - 1);
    }

#ifdef DEBUG
    siegeLog.printf("Devastating Bombardment. fortLevel = %d",
        static_cast<int>(t.getFortifications()));
#endif

    implementLosses(d_defendList, defendLosses);
    fillUnitLists(True);
  }

  doBreach();
}

/*
 * Ben indicates there should be 2 surrender tables, active/inactive.
 * So far he has only provided one
 */

void SiegeAction::doSurrenderCheck()
{
  ASSERT(d_iTown != NoTown);
  Town& town = d_campData->getTown(d_iTown);

  if(town.surrenderMinimumReached())
  {
    Attribute charisma = getCharisma(d_defendList);
    Attribute supply = getSupply(d_defendList);

#ifdef DEBUG
    siegeLog.printf("Surrender Check: charisma = %d, supply = %d", (int)charisma, (int)supply);
#endif

    int charismaPercent = MulDiv(static_cast<int>(charisma), 100, Attribute_Range);
    int supplyPercent = MulDiv(static_cast<int>(supply), 100, Attribute_Range);

#ifdef DEBUG
    siegeLog.printf("Surrender Check: charismaPercent = %d, supplyPercent = %d",
          charismaPercent, supplyPercent);
#endif

    // chance of not surrendering
    int chance = charismaPercent + ((charismaPercent * supplyPercent)/100);
#ifdef DEBUG
    siegeLog.printf("Surrender Check: chance = %d", chance);
#endif

    int n = CRandom::get(100);

#ifdef DEBUG
    siegeLog.printf("Surrender Check: random = %d", n);
#endif
    if(n > chance)
    {
      d_defendList.hasSurrendered(True);
    }
  }
}



void SiegeAction::doDestroyGarrison()
{
#ifdef DEBUG
  siegeLog.printf("Besieged has surrendered");
#endif

  CPListItem* defender = d_defendList.first();
  while(defender)
  {
    CP_ModeUtil::setMode(d_campData, defender->d_cpi, CampaignMovement::CPM_Surrendering);
    defender = d_defendList.next();
  }

  d_attackList.wonSiege(True);
}


void SiegeAction::takeTown()
{
  CPListItem* attacker = d_attackList.first();
  ASSERT(attacker != 0);
  ASSERT(d_iTown != NoTown);
  Town& town = d_campData->getTown(d_iTown);
  town.setGarrison(False);
  town.setSiege(False);
  town.resetWeeksUnderSiege();

  attacker->d_cpi->takingThisTown(d_iTown);
  CampUnitTown::takeTown(d_campGame, d_iTown, attacker->d_cpi);

  while(attacker)
  {
    CP_ModeUtil::setMode(d_campData, attacker->d_cpi, CampaignMovement::CPM_Holding);
    attacker = d_attackList.next();
  }

}

SPCount SiegeAction::implementLosses(CPList& list, SPLoss& losses, BasicUnitType::value basicType)
{
  /*
   */

  SPCount ourSideSP = getTotalSPCount(list);
  if(ourSideSP == 1)
    return 0;

  SPCount nLosses = 0;

  Armies& armies = d_campData->getArmies();

  CPListItem* cpList = list.first();
  while(cpList)
  {
    SPCount spCount = armies.getUnitSPCount(cpList->d_cpi, True); // Active SPs in organization

    SPLoss ourSideLoss = losses;


    /*
     * What will this unit actually lose?
     * e.g. if total side's SP is 10, and this unit has 2 SP
     *      then it will have 2/10 of the total damage applied to it.
     */

    ourSideLoss *= SPLoss(0, spCount, ourSideSP);


#ifdef DEBUG
    siegeLog.printf("Battle Losses for %s:",
         (const char*) d_campData->getUnitName(cpList->d_cpi).toStr());
    siegeLog.printf(" casualties = %u.%04x, SP=%d",
         (unsigned int) ourSideLoss.getInt(),
         (unsigned int) ourSideLoss.getFraction(),
         (int) spCount);

    char name[100];
    lstrcpy(name, cpList->d_cpi->getNameNotNull());

#endif

   SPCount nInfCount = armies.getNType(cpList->d_cpi, BasicUnitType::Infantry, False);
    bool clearAll = False;

    if(nInfCount > 0 && basicType == BasicUnitType::Infantry)
    {
      nLosses = Losses::makeLosses(&armies, cpList->d_cpi, losses, basicType, SpecialUnitType::NotSpecial);
      clearAll = ((nInfCount - nLosses) == 0);
    }
    else
      nLosses = Losses::makeLosses(&armies, cpList->d_cpi, losses);

    CampaignArmy_Util::clearDestroyedUnits(d_campData, cpList->d_cpi, clearAll);

#ifdef DEBUG
    siegeLog.printf("Losses for %s are %d", name, nLosses);
#endif

    cpList = list.next();
  }

  return nLosses;
}

SPCount SiegeAction::getTotalSPCount(CPList& list)
{
  SPCount total = 0;

  CPListItem* cpList = list.first();
  while(cpList)
  {
    total += d_campData->getArmies().getUnitSPCount(cpList->d_cpi, True);
    cpList = list.next();
  }
  return total;
}

Attribute SiegeAction::getAverageMorale(CPList& list)
{
  ULONG morale = 0;
  int nUnits = 0;

  const Armies* armies = &d_campData->getArmies();

  CPListItem* item = list.first();
  while(item)
  {

    morale += item->d_cpi->getMorale();
    nUnits++;

    item = list.next();
  }

  ASSERT(nUnits > 0);
  return static_cast<Attribute>(nUnits > 0 ? morale/nUnits : 0);
}

/*
 *  if more than one unit returns charisma level of leader with highest rating
 *  also sets supply level to chosen leaders unit level
 */

Attribute SiegeAction::getCharisma(CPList& list)
{
  const ICommandPosition& cpi = getLeaderUnit(list);

  ASSERT(cpi->getLeader() != NoLeader);
  return cpi->getLeader()->getCharisma();
}

Attribute SiegeAction::getSupply(CPList& list)
{
  const ICommandPosition& cpi = getLeaderUnit(list);
  ASSERT(cpi != NoCommandPosition);
  return cpi->getSupply();
}

// returns unit with siege or sieged leader with highest rank rating if more than one unit in list
ICommandPosition SiegeAction::getLeaderUnit(CPList& list)
{
  RankEnum rank = Rank_HowMany;
  ICommandPosition cpi = NoCommandPosition;

  CPListItem* item = list.first();
  ASSERT(item != 0);
  while(item)
  {
    if(rank == Rank_HowMany ||
       item->d_cpi->getLeader()->getRankLevel().getRankEnum() > rank ||
       item->d_cpi->getLeader()->isSupremeLeader())
    {
      rank = item->d_cpi->getLeader()->getRankLevel().getRankEnum();
      cpi = item->d_cpi;
      if(item->d_cpi->getLeader()->isSupremeLeader())
        break;
    }
    item = list.next();
  }

  ASSERT(cpi != NoCommandPosition);

  return cpi;
}


void SiegeAction::doRetreat(CPList& list)
{
   ASSERT(d_iTown != NoTown);
   Town& town = d_campData->getTown(d_iTown);

   //----- Modified SWG: 8Jul99 : infinite loop because not advancing through list
   // CPListItem* cpList = list.first();
   // while(cpList)
   for(CPListItem* cpList = list.first(); cpList; cpList = list.next())
   {
      ASSERT(cpList->d_cpi != 0);

      if(CP_ModeUtil::setMode(d_campData, cpList->d_cpi, CampaignMovement::CPM_StartingWithdraw))
      {
         {
            CampaignMessageInfo msgInfo(cpList->d_cpi->getSide(), CampaignMessages::BrokenSiege);
            msgInfo.where(d_iTown);
            msgInfo.cp(cpList->d_cpi);
            d_campGame->sendPlayerMessage(msgInfo);
         }
         {
            CampaignMessageInfo msgInfo(town.getSide(), CampaignMessages::SiegeEnded);
            msgInfo.where(d_iTown);
            d_campGame->sendPlayerMessage(msgInfo);
         }
      }
   }

   list.retreating(True);
}

void SiegeAction::doRaid(CPList& list)
{
   ASSERT(d_iTown != NoTown);
   Town& town = d_campData->getTown(d_iTown);

   SPCount count = getTotalSPCount(list);
   if(count >= (d_campData->getTownStrength(d_iTown, list.side()) / 2))
   {
      for(CPListItem* cpList = list.first(); cpList; cpList = list.next())
      {
         ASSERT(cpList->d_cpi != 0);

         if(CP_ModeUtil::setMode(d_campData, cpList->d_cpi, CampaignMovement::CPM_Raiding))
         {
            {
               CampaignMessageInfo msgInfo(list.side(), CampaignMessages::RaidingTown);
               msgInfo.where(d_iTown);
               msgInfo.cp(cpList->d_cpi);
               d_campGame->sendPlayerMessage(msgInfo);
            }
            {
               CampaignMessageInfo msgInfo(town.getSide(), CampaignMessages::TownRaided);
               msgInfo.where(d_iTown);
               d_campGame->sendPlayerMessage(msgInfo);
            }
         }
      }

      list.raiding(True);
   }
   else
      doRetreat(list);
}

/*
 * Global Function to begin a new siege
 */

void SiegeAction::startSiege(CampaignLogicOwner* campGame, ITown iTown, ICommandPosition hunit)
{
   ASSERT(iTown != NoTown);
   ASSERT(hunit != NoCommandPosition);

   CampaignData* campData = campGame->campaignData();

   CommandPosition* cp = campData->getCommand(hunit);
   ASSERT(cp != 0);
   Town& town = campData->getTown(iTown);

   Side uSide = cp->getSide();
   Side tSide = town.getSide();

    //--- Update: 3feb99 : startSiege is now done as part of hold order
   // CampaignOrderUtil::finishOrder(campGame, hunit);

   // find all friendly units at town at set to siegeing
   ICommandPosition topCPI = campData->getArmies().getFirstUnit(hunit->getSide());
   UnitIter iter(&campData->getArmies(), topCPI);
   while(iter.sister())
   {
      if(iter.current()->atTown() && iter.current()->getTown() == iTown)
            CP_ModeUtil::setMode(campData, iter.current(), CampaignMovement::CPM_Sieging);
   }

   SiegeAction siegeAction(campGame, iTown);
   if(siegeAction.canSiege())
   {
      UnitToTownRatio::Value ratio = CampUnitTown::getUnitToTownRatio(campData, iTown, hunit->getSide());
      if(ratio > UnitToTownRatio::LessThan)
      {
         if(!town.isSieged())
         {
            /*
             * Display some messages to player
             */

            {
               CampaignMessageInfo msgInfo(uSide, CampaignMessages::StartSiege);
               msgInfo.where(iTown);
               msgInfo.cp(hunit);
               campGame->sendPlayerMessage(msgInfo);
            }
            {
               CampaignMessageInfo msgInfo(tSide, CampaignMessages::SiegeStarted);
               msgInfo.where(iTown);
               campGame->sendPlayerMessage(msgInfo);
            }
         }
      }
      else
         siegeAction.doRaid(siegeAction.attackList());
   }
   else
      siegeAction.doRetreat(siegeAction.attackList());

#if 0
   if(CP_ModeUtil::setMode(campData, hunit, CampaignMovement::CPM_Sieging))
   {
       SiegeAction siegeAction(campGame, iTown);
       if(siegeAction.canSiege())
       {
          /*
           * Display some messages to player
           */

          {
             CampaignMessageInfo msgInfo(uSide, CampaignMessages::StartSiege);
             msgInfo.where(iTown);
             msgInfo.cp(hunit);
             campGame->sendPlayerMessage(msgInfo);
          }
          {
             CampaignMessageInfo msgInfo(tSide, CampaignMessages::SiegeStarted);
             msgInfo.where(iTown);
             campGame->sendPlayerMessage(msgInfo);
          }
       }
       else
       {
          if(CP_ModeUtil::setMode(campData, hunit, CampaignMovement::CPM_AvoidContactRetreat))
            {
              {
                 CampaignMessageInfo msgInfo(uSide, CampaignMessages::AttemptedSiege);
                 msgInfo.where(iTown);
                 msgInfo.cp(hunit);
                 campGame->sendPlayerMessage(msgInfo);
              }
              {
                 CampaignMessageInfo msgInfo(tSide, CampaignMessages::SiegeAttempted);
                 msgInfo.where(iTown);
                 campGame->sendPlayerMessage(msgInfo);
              }
            }
       }
    }
#endif
}

Boolean SiegeAction::canRaid(CampaignData* campData, ITown iTown, Side side)
{
   ICommandPosition topCPI = campData->getArmies().getFirstUnit(side);
   SPCount unitSP = 0;

   UnitIter iter(&campData->getArmies(), topCPI);
   while(iter.sister())
   {
      ICommandPosition cpi = iter.current();
      if(cpi->atTown() && cpi->getTown() == iTown)
         unitSP += campData->getArmies().getUnitSPCount(cpi, True);
   }

   return (unitSP >= (campData->getTownStrength(iTown, side) / 2));
}

void SiegeAction::retreatRaiders(CampaignData* campData, ITown iTown, Side side)
{
   ICommandPosition topCPI = campData->getArmies().getFirstUnit(side);
   SPCount unitSP = 0;

   UnitIter iter(&campData->getArmies(), topCPI);
   while(iter.sister())
   {
      ICommandPosition cpi = iter.current();
      if(cpi->atTown() && cpi->getTown() == iTown)
      {
         if(CP_ModeUtil::setMode(campData, cpi, CampaignMovement::CPM_Raiding))
         {
            ;
         }
      }
   }
}

/*
 * Global Function to begin a Raid
 */

Boolean SiegeAction::startRaid(CampaignLogicOwner* campGame, ITown iTown, ICommandPosition hunit)
{
   ASSERT(iTown != NoTown);
   ASSERT(hunit != NoCommandPosition);

   CampaignData* campData = campGame->campaignData();
   Armies& armies = campData->getArmies();

   CommandPosition* cp = campData->getCommand(hunit);
   ASSERT(cp != 0);

   Town& town = campData->getTown(iTown);


   Side uSide = cp->getSide();
   Side tSide = town.getSide();

   // we can raid if we have at least 50% of town strength
   if(canRaid(campData, iTown, hunit->getSide()))//unitSP >= (campData->getTownStrength(iTown, uSide) / 2) )
   {
     //--- Update: 3feb99 : startSiege is now done as part of hold order
     // CampaignOrderUtil::finishOrder(campGame, hunit);
     if(CP_ModeUtil::setMode(campData, hunit, CampaignMovement::CPM_Raiding))
     {
       {
             CampaignMessageInfo msgInfo(uSide, CampaignMessages::RaidingTown);
             msgInfo.where(iTown);
             msgInfo.cp(hunit);
             campGame->sendPlayerMessage(msgInfo);
       }
       {
             CampaignMessageInfo msgInfo(tSide, CampaignMessages::TownRaided);
             msgInfo.where(iTown);
             campGame->sendPlayerMessage(msgInfo);
       }
     }

     return True;
   }
   else
   {
      retreatRaiders(campData, iTown, hunit->getSide());
   }

   return False;
}

void SiegeAction::sendResult(ITown town)
{
   if( (d_attackList.siegeActive() && d_actionType != SA_NoEffect) ||
       (d_attackList.wonSiege()) ||
       (d_attackList.retreating()) )
   {
      for(Side s = 0; s < scenario->getNumSides(); s++)
      {
         // display siege result window for player
         CPList* ourList = (d_attackList.side() == s) ?
           &d_attackList : &d_defendList;

         CPList* theirList = (d_attackList.side() != s) ?
           &d_attackList : &d_defendList;

         SiegeInfoData sd(town, *ourList, *theirList);

         d_campGame->siegeResults(s, sd);
      }
   }
}

/*-------------------------------------------------------
 * Daily / Weekly Siege Calculations for a town
 * Process a town.
 */

void SiegeUtil::dailyTownSiege(CampaignLogicOwner* campGame, ITown t)
{
   CampaignData* campData = campGame->campaignData();
   TownList& towns = campData->getTowns();

   Town* town = &towns[t];

#ifdef DEBUG
   char timeBuffer[200];
   campData->asciiTime(timeBuffer);
   siegeLog.printf("\nProcessing daily Siege for %s at %s",
      town->getNameNotNull("Unnamed"),
      timeBuffer);
#endif

   /*
    * Only level-1 forts are run the daily routine
    */

   ASSERT(town->isSieged());
   if(town->isGarrisoned() && town->getFortifications() == 1)
   {
      // RWLock lock;
      // lock.startWrite();

      SiegeAction siegeAction(campGame, t);
      Boolean result = siegeAction.doDailyAction();

      // lock.endWrite();

      if(result)
      {
        siegeAction.sendResult(t);
      }
   }
}

void SiegeUtil::weeklyTownSiege(CampaignLogicOwner* campGame, ITown t)
{
   CampaignData* campData = campGame->campaignData();
   TownList& towns = campData->getTowns();

   Town* town = &towns[t];

#ifdef DEBUG
   char timeBuffer[200];
   campData->asciiTime(timeBuffer);
   siegeLog.printf("\nProcessing weekly Siege for %s at %s",
      town->getNameNotNull("Unnamed"),
      timeBuffer);
#endif

   ASSERT(town->isSieged());
   if(town->isGarrisoned())
   {
      // RWLock lock;
      // lock.startWrite();

      SiegeAction siegeAction(campGame, t);
      siegeAction.beginAction();

      // lock.endWrite();

      siegeAction.sendResult(t);
   }
}

/*------------------------ Outside access ------------------------------
 * A unit has finished it's orders at a town, and want to know
 * what to do.
 *
 * Should it siege?
 *
 * return True if sieging or raiding
 */


Boolean Siege::arrivedAtTown(CampaignLogicOwner* campGame, ITown itown, ICommandPosition hunit)
{
   ASSERT(itown != NoTown);
   ASSERT(hunit != NoCommandPosition);

   CampaignData* campData = campGame->campaignData();
   CommandPosition* cp = campData->getCommand(hunit);
   ASSERT(cp != 0);

   Town& town = campData->getTown(itown);

   Side uSide = cp->getSide();
   Side tSide = town.getSide();

   if(uSide != tSide)
   {
      /*
       * For now, we'll just set it to siege...
       */

      if(town.isGarrisoned())
      {
         SiegeAction::startSiege(campGame, itown, hunit);
         return True;
      }
      else
      {
         return SiegeAction::startRaid(campGame, itown, hunit);
      }

   }

   return True;
}

/*===============================================================
 * Daily / Weekly Siege Calculations
 */

void Siege::dailySiege(CampaignLogicOwner* campGame)
{
   CampaignData* campData = campGame->campaignData();
   TownList& towns = campData->getTowns();

   for(ITown t = 0; t < towns.entries(); t++)
   {
      const Town& town = towns[t];
      if(town.isSieged())
      {
         SiegeUtil::dailyTownSiege(campGame, t);
      }
   }
}

void Siege::weeklySiege(CampaignLogicOwner* campGame)
{
   CampaignData* campData = campGame->campaignData();
   TownList& towns = campData->getTowns();

   for(ITown t = 0; t < towns.entries(); t++)
   {
      Town* town = &towns[t];

      if(town->isRaided())
      {
         for(Side s = 0; s < 2; s++)
         {
            if(s != town->getSide() && !SiegeAction::canRaid(campData, t, s))
               SiegeAction::retreatRaiders(campData, t, s);
         }
      }

      if(town->isSieged())
      {
         SiegeUtil::weeklyTownSiege(campGame, t);
      }
   }
}


/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.1  2001/07/02 15:23:16  greenius
 * Battle, Campaign, Combined game and Campaign Editor now compile and run... mostly
 *
 * Revision 1.2  2001/06/18 08:25:44  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
