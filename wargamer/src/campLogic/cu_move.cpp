/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 * Campaign Unit Movement
 *
 * This was originally muddled up as a mixture of functions from
 * CommandPosition, CampaignMovement and RealCampaignData
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "cu_move.hpp"
#include "cu_speed.hpp"
#include "cu_data.hpp"
#include "campctrl.hpp"
#include "campdint.hpp"
#include "armies.hpp"
#include "atrition.hpp"
#include "cc_town.hpp"
#include "siege.hpp"
#include "campcomb.hpp"
#include "cc_avoid.hpp"
#include "cu_fog.hpp"
#include "cu_order.hpp"
#include "cc_util.hpp"
#include "options.hpp"
#include "route.hpp"
#include "town.hpp"
#include "except.hpp"
#include "campmsg.hpp"
#include "recover.hpp"
#include "scenario.hpp"
#include "c_const.hpp"
#include "terrain.hpp"
#include "c_weathr.hpp"
#include "condutil.hpp"
#include "cu_rand.hpp"
#include "cbattle.hpp"
#include "cbatproc.hpp"
#include "wg_rand.hpp"
#include "cu_ldr.hpp"
#include "townprop.hpp"
#include "armyutil.hpp"
#include "cu_repo.hpp"

#ifdef DEBUG
#include "unitlog.hpp"
#include "todolog.hpp"
#include "logwin.hpp"

//static LogFileFlush mLog("UnitMove.log");

#endif

/*
 * Constructor
 */

CampUnitMovement::CampUnitMovement(CampaignLogicOwner* campGame) :
   d_moveData(new CampUnitMoveData(campGame)),
   d_shouldRedrawMap(False)
{
   ASSERT(d_moveData != 0);
   if(d_moveData == 0)
      throw GeneralError("Unable to allocate memory for CampUnitMoveData");
}

CampUnitMovement::~CampUnitMovement()
{
   delete d_moveData;
}

/*
 * Move Units each Hour
 */


void CampUnitMovement::hourlyUnits(TimeTick _ticks)
{
#ifdef DEBUG
//   logWindow->printf("Processing Hourly Units");
   cuLog.printf("Processing Hourly Units");
#endif
   d_moveData->d_ticks = _ticks;

   // process independent leaders
   LeaderProc::processHourly(d_moveData->d_campaignGame);

   // process replacement SP's
   RepoSPProc::processHourly(d_moveData->d_campaignGame);

   // update town-unit mode
   CampUnitTown::updateTownUnitMode(d_moveData->d_campData);
   moveCampaignUnits();

   if(d_shouldRedrawMap)
   {
#ifdef DEBUG_TOLOGWINDOW
      logWindow->printf("Hourly Units. Redrawing map!");
#endif
      d_moveData->d_campaignGame->repaintMap();
      d_shouldRedrawMap = False;
   }
}

/*
 * Process Units each day:
 *    Calculates Stragglers/Attrition
 */

void CampUnitMovement::dailyUnits()
{
#ifdef DEBUG
   cuLog.printf("dailyUnits");
#endif

   /*
   * Do town orders
   */

   TownProcess::dailyTownOrders(d_moveData->d_campaignGame);

   /*
   * Do todays weather
   */

   CampaignWeatherUtil::doTodaysWeather(d_moveData->d_campaignGame);

   /*
   * Do Random Events
   */

   CampaignRandomEventUtil::process(d_moveData->d_campaignGame);

   /*
   * Do Conditions update
   */

   CampaignConditionUtil::dailyUpdate(d_moveData->d_campaignGame);

   /*
   * Town fog of war
   */

   FogOfWarUtil::processDailyTowns(d_moveData->d_campData);

   /*
   * leaders
   */

   LeaderProc::processDaily(d_moveData->d_campaignGame);

   /*
   * Process daily unit routines
   */

   CPIter iter(&d_moveData->d_campData->getArmies());

   while(iter.sister())
   {
      ICommandPosition cpi = iter.current();

      /*
      * Find close units
      */

      findCloseUnits(cpi);

      /*
      * Apply Fog of War for units
      */

      FogOfWarUtil::processDailyUnits(d_moveData, cpi);

      /*
      * Do attrition
      */

      if(CampaignOptions::get(OPT_Attrition))
      {
         AttritionProc::procDailyAttrition(d_moveData->d_campData, cpi);
      }

      /*
      * Get todays fatigue level for all units
      */

      FatigueProc::procDailyFatigue(d_moveData->d_campData, cpi);

      /*
      * Check for auto rest
      */

      FatigueProc::procDailyFatigueLimits(d_moveData->d_campaignGame, cpi);

      /*
      * Recover Morale
      */

      RecoverMoraleProc::recoverDailyMorale(d_moveData, cpi);
   }
}


/*
 * Global Move Units function
 */

void CampUnitMovement::moveCampaignUnits()
{
#ifdef DEBUG_TIME
   cuLog.printf("moveUnits");
#endif

   /*
   * Battle!
   *
   * Called from here so any units forced to retreat can do so
   * right away
   */

   CampaignBattleList& batList = d_moveData->d_campData->getBattleList();
   CampaignBattleProc::procBattles(d_moveData->d_campaignGame, &batList);

   /*
   * If it is night time do not move
   *
   * Exception:
   *    Retreating Units can move away from enemy at night
   *    Units with their head in town, can allow the tail to catch up
   */

   const CampaignTime& ctime = d_moveData->d_campData->getCTime();
   const Date& date = d_moveData->d_campData->getDate();

   GameTick gtick = ctime.getTime();
   GameTick sunrise = getSunRise(date, 0);
   GameTick sunset = getSunSet(date, 0);

   d_moveData->d_nightTime = ( (gtick < sunrise) || (gtick > sunset) );

   /*
    * SWG: 5Aug99:
    * Action any new Orders first so that orders issued simultaneously are
    * processed properly.
    *
    * e.g. if a corps was issued a move order in the same time as one of its decisions was
    * ordererd to garrison, the corps had already moved before the division's order was actioned
    * and we ended up with a Garrison not at a town
    */

   {
      UnitIter uiter(d_moveData->d_armies, d_moveData->d_armies->getTop());
      while(uiter.next())
      {
         ICommandPosition cpi = uiter.current();

         if(!cpi->isParalized(d_moveData->d_campData->getTick()))
         {
            CampaignOrderUtil::processCampaignOrders(d_moveData, cpi);
         }
      }
   }

   /*
    * Phase 1: Do checks for enemy, etc...
    */


   UnitIter uiter(d_moveData->d_armies, d_moveData->d_armies->getTop());
   while(uiter.next())
   {
      ICommandPosition cpi = uiter.current();
      // CommandPosition* cp = uiter.currentCommand();

      /*
      *  No need to do President or God
      */

      if(cpi->isLower(Rank_President))
      {
         /*
         *  If a top level unit do battle, get Close Units, and do fog of war
         */

         ICommandPosition parent = cpi->getParent();
         ASSERT(parent != NoCommandPosition);
         // ICommandPosition pi = cpi->getParent();
         // ASSERT(pi != NoCommandPosition);
         // CommandPosition* parent = d_moveData->d_armies->getCommand(pi);
         if(parent->sameRank(Rank_President))
         {

#ifdef DEBUG
            if(!d_moveData->d_nightTime || cpi->canMoveAtNight())
               cuLog.printf("\n\n---------- Processing %s", (const char*)d_moveData->d_campData->getUnitName(cpi).toStr());
#endif

            /*
            * get close friendly and enemy units
            */

            findCloseUnits(cpi);

            /*
            * Do hourly fog of war, fatigue loss/recovery
            */

            if(!d_moveData->d_nightTime || cpi->canMoveAtNight())
            {
               FogOfWarUtil::processHourlyUnits(d_moveData, cpi);
               FatigueProc::procHourlyFatigue(cpi, d_moveData->d_campData);
            }
         }

         /*
          * If unit is paralized due to squabbling leaders, it can't do anything
          * including processing orders
          */

         if(!cpi->isParalized(d_moveData->d_campData->getTick()))
         {

            //--- Moved to loop above
            // CampaignOrderUtil::processCampaignOrders(d_moveData, cpi);

            /*
             * move only if top level unit
             */

            if(parent->sameRank(Rank_President))
            {
               moveCampaignUnit(cpi);

               CampaignOrderUtil::processTimedOrders(d_moveData->d_campaignGame, cpi);

               if(!d_moveData->d_nightTime || cpi->canMoveAtNight())
               {
                  if(cpi->shouldMove())
                     doCampaignMoveUnit(cpi);
               }
            }
            else
            {
               /*
                * Move attached unit... just copy parent's position and order
                */

               cpi->setPosition(parent->getPosition());
            }
         }

#if 0
            /*
         * Finally do fog of war for next turn if top-level unit
         */
            if((parent->sameRank(Rank_President)) &&
            (!d_moveData->d_nightTime || cpi->canMoveAtNight()))
         {
            FogOfWarUtil::processHourlyUnits(d_moveData, cpi);
         }
#endif
      }
   }

#ifdef DEBUG
   cuLog.close();
#endif

   // start any battles (that is determine if tactical or calculated)
   // called from here so all units added to a battle this process will
   // be added to initial deployment (rather than any except the first from each side)
   // This should solve the timing problems experienced in battlegame deployment
   // do too reinforcements arriving in the same hour as the battle starts
   CampaignBattleProc::startBattles(d_moveData->d_campaignGame);
}

/*
 * Move a campaign Unit
 *
 * The scheduler will call this once per game hour
 * The routine still checks the time anyway.
 */

void CampUnitMovement::moveCampaignUnit(ICommandPosition hunit)
{
   // CommandPosition* cp = d_moveData->d_armies->getCommand(hunit);

   /*
   * Return if night-time or we can't move
   */

   if(!d_moveData->d_nightTime || hunit->canMoveAtNight())
   {
      // check activity
      if(!hunit->isNormalActivity())
         doCampaignAbnormalActivity(hunit);

      if(hunit->canBeInBattle())   // if unit can participate in a battle
      {
         if(CampaignCombat::checkEnemy(d_moveData, hunit))
            CampaignOrderUtil::orderUpdated(d_moveData->d_campaignGame, hunit);
      }

      /*
      * Special cases...
      */


      if(hunit->isSurrendering())
         CampaignCombat::surrenderUnit(d_moveData->d_campaignGame, hunit, d_moveData->d_closeUnits);
      else
      {
#if 0
            // taking over a town
            if(hunit->takingOverTown() && !hunit->isForcedHold(d_moveData->d_campData->getTick()))
            CampUnitTown::takeTown(d_moveData->d_campaignGame, hunit->takingThisTown(), hunit);
#endif
         // see if we should 'march to the sound of battle'
         if(CampaignCombat::marchToBattle(hunit, d_moveData))
         {
            ; // do nothing
         }
      }

      /*
      * increment or decrement unit activity counter
      */

      hunit->adjustActivityCount(d_moveData->d_campData->getTick());

   }
}

/*
 * Process a unit that is in move mode
 */

void CampUnitMovement::doCampaignMoveUnit(ICommandPosition hunit)
{
   // CommandPosition* cp = d_moveData->d_armies->getCommand(hunit);
   OrderBase* cOrder = hunit->getCurrentOrder();

   /*
   * If unit has been forced to hold due to town catastrophe, return
   */

   if(hunit->isForcedHold(d_moveData->d_campData->getTick()))
   {
      return;
   }

   if(hunit->crossingRiver())
      hunit->crossingRiver(False);

   /*
   *  If unit has fieldworks they are no longer valid
   */
#if 0     // field works not used for Nap1813
      if(hunit->hasFieldWorks())
      hunit->invalidateFieldWorks();
#endif
#ifdef DEBUG_ALL
   cuLog.printf(hunit, "doCampaignMoveUnit()");
#endif

   /*
   * If an Armistice is in effect, make sure unit moves only in friendly territory
   */

   if(d_moveData->d_campData->armisticeInEffect())
   {
      ITown destTown = hunit->getDestTown();
      ASSERT(destTown != NoTown);

      Town& town = d_moveData->d_campData->getTown(destTown);
      if(scenario->isEnemy(hunit->getSide(), town.getSide()))
      {
         // if we're in or going to enemy territory, go to hold orders
         cOrder->setOrderOnArrival(Orders::Type::Hold);
         CampaignOrderUtil::finishOrder(d_moveData->d_campaignGame, hunit);
         return;
      }
   }

   if(hunit->isRetreating())
   {
      /*
      * doCampaignRetreat will set up:
      *    setNextDest
      *    setUnitOnRoute
      */

      // make sure we have a valid retreat town
      if(hunit->hasRetreatTown())
      {
         if(hunit->atTown())
         {
            const Town& t = d_moveData->d_campData->getTown(hunit->getTown());

            bool legalRetreatTown = False;
            for(int i = 0; i < MaxConnections; i++)
            {
               IConnection ic = t.getConnection(i);
               if(ic == NoConnection)
                  break;

               Connection& con = d_moveData->d_campData->getConnection(ic);
               ITown otherTown = con.getOtherTown(hunit->getTown());

               if(otherTown == hunit->retreatTown())
               {
                  legalRetreatTown = True;
                  break;
               }
            }

            if(!legalRetreatTown)
               hunit->clearRetreatTown();
         }
         else
         {
            Connection& con = d_moveData->d_campData->getConnection(hunit->getConnection());
            if(con.node1 != hunit->retreatTown() && con.node2 != hunit->retreatTown())
               hunit->clearRetreatTown();
         }
      }

      ITown destTown = CampaignAvoid::pickRetreatTown(d_moveData->d_campData, hunit, d_moveData->d_closeUnits);
      if(destTown != NoTown)
      {
         ASSERT(destTown == hunit->retreatTown());
         if(destTown != NoTown)
         {
            if(hunit->atTown())
               setUnitOnRoute(hunit, destTown);
            else
               hunit->moveTowards(destTown);
         }
      }
      // if we have no dest town then unit is more or less trapped
      // and has changed its mode(either holding, waiting for battle, or surrendering
      // depending on the circumstances
      else
         return;
   }
   else     // Not retreating
   {

      /*
      * If there is no destination, then get one
      */

      if((hunit->getNextDest() == NoTown) && (hunit->getTarget() == NoCommandPosition))
      {
         ITown destTown = cOrder->getNextDest();
         if(destTown == NoTown)
            hunit->setTarget(constCPtoCP(cOrder->getTarget()));
         else
            hunit->setNextDest(destTown);

         /*
         * If unit is at a town, the normal atTown movement code will work out
         * where to go.  Otherwise we need to some special case code here
         * so that they don't mindlessy try to march to the location they
         * are supposed to retreat from/
         */

         ASSERT(destTown != NoTown || cOrder->getTarget() != NoCommandPosition);
      }

      /*
      * If unit is not on a link between towns, then it must work out where
      * to go to.
      */

      // CampaignPosition& cpos = cp->getPosition();

      if(hunit->atTown())
      {
         /*
         * Is unit trying to move?
         */

         ITown cTown = hunit->getTown();
         Town& t = d_moveData->d_campData->getTown(cTown);

         /*
         * see if we capture or destroy anything
         * i.e. depots, supply sources, supply
         */

         if((t.getSide() != hunit->getSide()) &&
            !(t.isGarrisoned()) &&
            (t.getIsDepot() || t.getIsSupplySource()))
         {
            CampUnitTown::destroySupply(d_moveData->d_campaignGame, hunit->getTown(), hunit);
         }

         // taking over a town, take it
         if(hunit->takingOverTown())
            CampUnitTown::takeTown(d_moveData->d_campaignGame, hunit->takingThisTown(), hunit);

         /*
         * If got to an end point, then get next part of order
         */

         if(hunit->getTarget() == NoCommandPosition)
         {
            if((hunit->getNextDest() == NoTown) || (cTown == hunit->getNextDest()))
            {
               cOrder->advanceNextDest();
               ITown orderTown = cOrder->getNextDest();
               if(orderTown == NoTown)
                  hunit->setTarget(constCPtoCP(cOrder->getTarget()));
               else
                  hunit->setNextDest(orderTown);
            }
         }

         /*
         * Set up to move to destination
         */

         if(hunit->getTarget() != NoCommandPosition)
         {
            /*
            * Moving towards unit
            */

            // const CommandPosition* cpt = d_moveData->d_armies->getCommand(hunit->getTarget());
            ConstICommandPosition cpt = hunit->getTarget();

            /*
            * If target at a town, then move towards that town
            * unless it's the same town we're in, in which case attach and
            * finish the order
            */

            if(cpt->atTown())
            {
               ITown fTown = cpt->getTown();
               if(fTown == cTown)
               {
                  CampaignOrderUtil::finishOrder(d_moveData->d_campaignGame, hunit);
               }
               else
                  setUnitOnRoute(hunit);
            }
            else
            {
               /*
               * If target's connection is connected to our town
               * then select the route directly
               * Otherwise move to whicever end of the route he is
               * closest to.
               */

               setUnitOnRoute(hunit);
            }
         }
         else if(hunit->getNextDest() != NoTown)
         {
            setUnitOnRoute(hunit);
         }
         else     // Nowehere to go.. must have completed order
         {
            /*
            * This is our final destination...
            * Decide what to do with the town:
            *    Siege it?
            *
            * Update: Do not do siege because orderOnArrival should
            *         take priority.  The hold on arrival will automatically
            *         start the siege if appropriate.
            */

            // if(!Siege::arrivedAtTown(d_moveData->d_campaignGame, cTown, hunit))
            CampaignOrderUtil::finishOrder(d_moveData->d_campaignGame, hunit);

            CampaignMessageInfo msgInfo(hunit->getSide(), CampaignMessages::ArrivedAtTown);
            msgInfo.cp(hunit);
            msgInfo.where(cTown);
            d_moveData->d_campaignGame->sendPlayerMessage(msgInfo);
            return;
         }
      }
   }     // if(retreating)

   /*
   * Removed else, so that unit's can do one turns movement immediately
   * instead of wasting an hour deciding which way to go.
   */

   if(!hunit->atTown())
   {
      /*
      * Get our next destination town
      * and consider attacking, etc...
      */

      RouteList& rl = hunit->currentRouteList();

      ITown itown = NoTown;

      if(hunit->isRetreating())
      {
         ASSERT(hunit->retreatTown() != NoTown);
         itown = hunit->retreatTown();
      }
      else
      {
//         ASSERT(rl.entries() > 0);
         if(rl.entries() == 0)            // Defensive coding:
            itown = hunit->getDestTown();
         else
            itown = rl.first()->d_town;
      }

      ASSERT(itown != NoTown);
      const Town& town = d_moveData->d_campData->getTown(itown);

      // make sure we're headed the right way
      if(hunit->getDestTown() != itown)
         hunit->reverseDirection();

      /*
      * Move along link
      */

#ifdef DEBUG_ALL
      cuLog.printf(hunit, "Moving along connection to %s",
         town.getNameNotNull());
#endif

      /*
      * Calculate how far to move
      *
      */

      Speed speed = StrategicSpeed::getStrategicSpeed(hunit, hunit->getConnection(), d_moveData);
      Distance distToMove = distanceTravelled(speed, d_moveData->d_ticks);

      adjustDistance(hunit, distToMove);

      if(hunit->movePosition(distToMove, d_moveData->d_armies->getColumnLength(hunit)))
      {
#ifdef DEBUG
         cuLog.printf("Arrived at %s", d_moveData->d_campData->getTownName(itown));
#endif
         /*
         * Mark town as our own!
         *
         * If we got here, then we've already done the sieging and whatnot
         */

         CampUnitTown::enterTown(d_moveData->d_campaignGame, itown, hunit);
      }
   }

   d_shouldRedrawMap = True;
   //#endif
}

/*
 * Pick a route towards given town
 *
 * Only called if a unit is already at a town.
 */

void CampUnitMovement::setUnitOnRoute(ICommandPosition cpi, ITown dest)
{
   // ASSERT(dest != NoTown);
   // RWLock lock;
   // lock.startWrite();

   // CommandPosition* cp = d_moveData->d_armies->getCommand(cpi);

   ASSERT(cpi->atTown());
   ITown cTown = cpi->getTown();
   Town& t = d_moveData->d_campData->getTown(cTown);

   /*
   * Add town to units's previous route list if not retreating,
   * otherwise remove it
   */

   if(!cpi->isRetreating())
      cpi->previousRouteNode(cTown);
   else
      cpi->removePreviousRouteNode(cTown);
   {

      ITown nTown = NoTown;
      IConnection con = NoConnection;

      if(cpi->isRetreating())
      {
         ASSERT(dest != NoTown);
         nTown = dest;
      }
      else
      {
         RouteList& rl = cpi->currentRouteList();
         CampaignRouteUtil::plotUnitRoute(d_moveData->d_campData, cpi);

         if(rl.entries() > 0)
            nTown = rl.first()->d_town;
      }

      /*
      * Get common connection
      */

      if(nTown != NoTown)
      {
         con = CampaignRouteUtil::commonConnection(d_moveData->d_campData, cTown, nTown);
         ASSERT(con != NoConnection);
      }

      if(con == NoConnection)
      {
         // Help... what do we do?
#ifdef DEBUG
         cuLog.printf(cpi, "has no route from %s to %s",
            d_moveData->d_campData->getTownName(cTown),
            d_moveData->d_campData->getTownName(nTown));
#endif


         CampaignMessageInfo msgInfo(cpi->getSide(), CampaignMessages::Stranded);
         msgInfo.cp(cpi);
         msgInfo.where(cTown);
         d_moveData->d_campaignGame->sendPlayerMessage(msgInfo);

         CampaignOrderUtil::finishOrder(d_moveData->d_campaignGame, cpi);
      }
      else
      {
         // Set to starting to move from current town along connection
#ifdef DEBUG
         const Connection& connect = campaignData->getConnection(con);
         ITown conTown = connect.getOtherTown(cTown);
         ASSERT(conTown != NoTown);
         RouteList& rl = cpi->currentRouteList();

         cuLog.printf("\n\n%s wants to move along link %d from %s to %s\n",
            (const char*)d_moveData->d_campData->getUnitName(cpi).toStr(),
            static_cast<int>(con),
            d_moveData->d_campData->getTownName(cTown),
            d_moveData->d_campData->getTownName(conTown));
         cuLog.printf("-------------------------------------------\n");
#endif

         Boolean moveUnit = True;

         if(d_moveData->d_campData->isChokePoint(cTown) && cpi->lastConnection() != NoConnection)
         {
            const Town& t = d_moveData->d_campData->getTown(cTown);
            const TerrainTypeItem& ti = d_moveData->d_campData->getTerrainType(t.getTerrain());

            /*
            * If new connection is not on the same side as last connection
            * we cannot pass chokepoint if enemy garrisons town
            *
            * unless this is a bridge chokepoint, the town is under siege, and our unit
            * or a frienldly unit at the town has a bridge train
            *
            */

            if(!d_moveData->d_campData->isSameSideOfChokePoint(con, cpi->lastConnection()))
            {
               Boolean hasBridgeTrain = False;
               if(ti.isBridge())
               {
                  hasBridgeTrain = (d_moveData->d_campData->getArmies().hasBridgeTrain(cpi) ||
                     CampaignArmy_Util::bridgeTrainAtTown(d_moveData->d_campData, cTown, cpi->getSide()));
#ifdef DEBUG
                  if(hasBridgeTrain)
                     cuLog.printf("--- Has bridge-train\n");
#endif
               }

               if((con != cpi->lastConnection()) &&
                  (t.isGarrisoned() && t.getSide() != cpi->getSide()) &&
                  (!hasBridgeTrain || !t.isSieged()))
               {

#ifdef DEBUG
                  if(t.isGarrisoned() && t.getSide() != cpi->getSide())
                     cuLog.printf("--- Garrisoned by enemy. Cannot pass\n");
#endif
                  moveUnit = False;
               }

               /*
               * It takes time if we are moving across a blown bridge
               * Hold unit up for awhile
               */

               if(moveUnit && !t.isBridgeUp())
               {
                  cpi->crossingRiver(True);

                  // if we're retreating, we lose our heavy equipment
                  if(cpi->isRouting() || cpi->isWithdrawing())
                     CampaignArmy_Util::loseEquipment(d_moveData->d_campData, cpi);

                  enum UnitSize
                  {
                     SmallUnit,     // less than 15 SPs
                     MediumUnit,    // less than 30 SPs
                     LargeUnit,     // less than 45 SPs
                     VeryLargeUnit, // over 45 Sps

                     UnitSize_HowMany
                  } unitSize;

                  SPCount spCount = d_moveData->d_campData->getArmies().getUnitSPCount(cpi, True);
                  if(spCount <= 15)
                     unitSize = SmallUnit;
                  else if(spCount <= 30)
                     unitSize = MediumUnit;
                  else if(spCount <= 60)
                     unitSize = LargeUnit;
                  else
                     unitSize = VeryLargeUnit;

                  int hoursToMove = 0;

                  if(!hasBridgeTrain)
                  {
                     // we a lose a random % of supply between 30% - 70%
                     int percent = CRandom::get(30, 70);
                     Attribute supply = MulDiv(cpi->getSupply(), 100 - percent, 100);
                     d_moveData->d_campData->getArmies().setCommandSupply(cpi, supply);

                     // TODO: get table from scenario tables
                     static UBYTE s_table[UnitSize_HowMany] = {
                    12, 24, 30, 36
                 };

                     // get raw time to move (in hours)
                     hoursToMove = s_table[unitSize];

                     // if we have heavy units, multiply by 20%
                     if(d_moveData->d_campData->getArmies().hasSiegeTrain(cpi) ||
                        d_moveData->d_campData->getArmies().hasBridgeTrain(cpi))
                     {
                        hoursToMove *= 1.2;
                     }

                     // ajdust +- 20%
                     int adjustBy = MulDiv(hoursToMove, 2, 10);
                     hoursToMove = CRandom::get(hoursToMove - adjustBy, hoursToMove + adjustBy);

                  }
                  else // we have a bridge train
                  {
                     // chance of crossing in 2 hours
                     int chance = 40;

                     // modify chance for nation
                     // TODO: move to scenario tables
                     static UBYTE s_table[] = {
                    10,  0, 0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0
                 };

                     chance += s_table[cpi->getNation()];

                     // modify for enemy holding town
                     if(t.isSieged())
                        chance -= 15;

#ifdef DEBUG
                     int i = 0;
#endif
                     for(;;)
                     {
#ifdef DEBUG
                        i++;
#endif
                        const int increaseBy = 10;

                        hoursToMove += 2;

                        if(CRandom::get(100) > chance)
                           chance += increaseBy;
                        else
                           break;
                     };
#ifdef DEBUG
                     cuLog.printf("------- Iterations = %d\n", i);
#endif

                  }

                  cpi->forceHold(d_moveData->d_campData->getTick() + HoursToTicks(hoursToMove));
#ifdef DEBUG
                  cuLog.printf("----- Crossing river. Will take %d hours\n", hoursToMove);
#endif
               }
            }
         }

         if(moveUnit)
            cpi->startMoving(con, cTown);
         else
            CampaignOrderUtil::finishOrder(d_moveData->d_campaignGame, cpi);
      }



#if 0   // old way
         static CampaignRoute route(d_moveData->d_campData);
      CampaignRoute::RouteFlag flags = (!retreating) ? CampaignRoute::UseCosts : 0;
      if(!retreating && Options::get(OPT_FullRoute))
         flags |= CampaignRoute::FindBest;

      // route.setup(flags, cpi->getSide(), SysTicks::TicksPerHour * 8);
      route.setup(flags, cpi->getSide(), TicksPerHour * 8);

      IConnection con = route.planRoute(cTown, iTown);
      if(con == NoConnection)
      {
         // Help... what do we do?

#ifdef DEBUG
         cuLog.printf(cpi, "has no route from %s to %s",
            d_moveData->d_campData->getTownName(cTown),
            d_moveData->d_campData->getTownName(iTown));
#endif

         CampaignMessageInfo msgInfo(cpi->getSide(), CampaignMessages::Stranded);
         msgInfo.cp(cpi);
         msgInfo.where(cTown);
         d_moveData->d_campaignGame->sendPlayerMessage(msgInfo);

         CampaignOrderUtil::finishOrder(d_moveData->d_campaignGame, cpi);

         // cOrder.type = ORDER_NONE;
         // cOrder.destTown = NoTown;
         // orderUpdated(cpi);
      }
      else
      {
         // Set to starting to move from current town along connection
#ifdef DEBUG

         const Connection& connect = campaignData->getConnection(con);
         ITown conTown = connect.getOtherTown(cTown);
         ASSERT(conTown != NoTown);

         cuLog.printf(cpi, "moving along link %d from %s to %s on way to %s",
            con,
            d_moveData->d_campData->getTownName(cTown),
            d_moveData->d_campData->getTownName(conTown),
            d_moveData->d_campData->getTownName(iTown));
#endif

         Boolean moveUnit = True;

         if(d_moveData->d_campData->isChokePoint(cTown) && cpi->lastConnection() != NoConnection)
         {
            if(cpi->getSide() != t.getSide() && con != cpi->lastConnection())
               moveUnit = d_moveData->d_campData->isSameSideOfChokePoint(con, cpi->lastConnection());
         }

         if(moveUnit)
            cpi->startMoving(con, cTown);
         else
            CampaignOrderUtil::finishOrder(d_moveData->d_campaignGame, cpi);
      }
#endif
   }

   // lock.endWrite();

}


/*
 * We're in between locations, and need to decide whether to
 * continue along or to double back.
 *
 * If one end is our final destination, then head towards it.
 *
 * If target is a unit, and on same connection, then head towards it.
 *
 * If one town is friendly and the other enemy, then head towards
 * the friendly one (unless agression is high?)
 *
 * If both are friendly, then work out which is closest to destination
 *
 * If both are enemy... go to closest?
 */

void CampUnitMovement::moveOffConnection(ICommandPosition cpi)
{
   CommandPosition* cp = d_moveData->d_armies->getCommand(cpi);

   ASSERT(!cp->atTown());

   /*
   * If destination is a unit
   * If it is on same connection then set up to move towards it
   */

   ITown destTown = NoTown;
   ITown finalTown = cp->getNextDest();

   if(cp->getTarget() != NoCommandPosition)
   {
      const CommandPosition* cpt = d_moveData->d_armies->getCommand(cp->getTarget());

      if(cpt->atTown())
      {
         ITown fTown = cpt->getTown();
         if(cp->isConnected(fTown))
            destTown = fTown;
         else
            finalTown = fTown;
      }
      else
      {
         if(cpt->getConnection() == cp->getConnection())
         {
            /*
            * on same connection...
            * set up position values to move towards it
            */

            cp->moveTowards(cpt->location());
            return;
         }
         else
            finalTown = cpt->getCloseTown();
      }
   }

   if((destTown == NoTown) && (finalTown != NoTown))
   {
      if(cp->isConnected(finalTown))
         destTown = finalTown;
      else     // pick end closest to finalTown
      {
         /*
         * Ask for a route from the next town
         * If the answer is our current connection, then we should
         * double back on ourselves.
         */

         static CampaignRoute route(d_moveData->d_campData);

         CampaignRoute::RouteFlag flags = CampaignRoute::UseCosts;
//         if(Options::get(OPT_FullRoute))
//            flags |= CampaignRoute::FindBest;

         // route.setup(flags, cp->getSide(), SysTicks::TicksPerHour * 8);
         route.setup(flags, cp->getSide(), TicksPerHour * 8);

         IConnection con = route.planRoute(cp->getDestTown(), finalTown);
         if(con == cp->getConnection())
            destTown = cp->getLastTown();
         else
            destTown = cp->getDestTown();
      }
   }

   if(destTown != NoTown)
      cp->moveTowards(destTown);

   // else, nowhere to go!
}

#if 0
void CampUnitMovement::doCampaignInstallSupply(ICommandPosition hunit)
{
   CommandPosition* cp = d_moveData->d_armies->getCommand(hunit);

   /*
   * Check if still valid
   * e.g.
   *    Town might have been taken over
   *    SupplyUnit SP might have been killed
   */

   ASSERT(cp->atTown());
   const Town& town = d_moveData->d_campData->getTown(cp->getTown());

   ASSERT(town.getSupplyInstaller() == hunit);

   if(town.getSide() != cp->getSide())
   {
      CampaignMessageInfo msgInfo(cp->getSide(), CampaignMessages::NotInstallSupply);
      msgInfo.cp(hunit);
      msgInfo.where(cp->getTown());
      d_moveData->d_campaignGame->sendPlayerMessage(msgInfo);
      CampaignOrderUtil::finishOrder(d_moveData->d_campaignGame, hunit);
      return;
   }

   ICommandPosition supplyCP = NoCommandPosition;
   ISP supplySP = NoStrengthPoint;

   if(!d_moveData->d_armies->findSupplyUnit(hunit, supplyCP, supplySP))
   {
      CampaignMessageInfo msgInfo(cp->getSide(), CampaignMessages::NotInstallSupplyDead);
      msgInfo.cp(hunit);
      msgInfo.where(cp->getTown());
      d_moveData->d_campaignGame->sendPlayerMessage(msgInfo);
      CampaignOrderUtil::finishOrder(d_moveData->d_campaignGame, hunit);
      return;
   }


   if(cp->modeTimeFinished(d_moveData->d_campData->getTick()))
   {
      ASSERT(cp->atTown());

      Town& town = d_moveData->d_campData->getTown(cp->getTown());

      ASSERT(!town.getIsDepot());
      ASSERT(!town.getIsSupplySource());
      ASSERT(town.getSide() == cp->getSide());
      ASSERT(supplyCP != NoCommandPosition);
      ASSERT(supplySP != NoStrengthPoint);

      /*
      * Do the build...
      */

      /*
      * Remove Supply Unit Strength Point
      */

      d_moveData->d_armies->detachStrengthPoint(supplyCP, supplySP);
      d_moveData->d_armies->deleteStrengthPoint(supplySP);

      /*
      * Create Depot
      */

      town.setIsDepot(True);
      town.setSupplyInstaller(NoCommandPosition);

      /*
      * Tell the player
      */

      CampaignMessageInfo msgInfo(town.getSide(), CampaignMessages::SupplyDepotInstalled);
      msgInfo.cp(hunit);
      msgInfo.where(cp->getTown());
      d_moveData->d_campaignGame->sendPlayerMessage(msgInfo);

      /*
      * Change mode
      */

      CampaignOrderUtil::finishOrder(d_moveData->d_campaignGame, hunit);
   }
}

/*
 * Do Build Fortification
 *
 * Notes:
 *   1. Town needs to keep track of whether an upgrade is in progress
 *      and if so, then the order should not be available.
 *
 *   2. The checking code could be moved to the places where the
 *      actions take place (e.g. takeOverTown, causeLosses).  This
 *      may be neccesary when 1. is implemented, so that a town's
 *      inProgress flag is cleared if an engineer is killed, or the
 *      unit is deleted.
 *
 *   3. Player should be shown the status of the action in the Town
 *      and Unit Information Windows.
 *
 * These notes, also apply toSupply Depots
 */


void CampUnitMovement::doCampaignInstallFort(ICommandPosition hunit)
{
   CommandPosition* cp = d_moveData->d_armies->getCommand(hunit);

   /*
   * Check if still valid
   * e.g.
   *    Town might have been taken over
   *    Engineer might have been killed
   */

   ASSERT(cp->atTown());
   const Town& town = d_moveData->d_campData->getTown(cp->getTown());

   ASSERT(town.getFortUpgrader() == hunit);

   if(town.getSide() != cp->getSide())
   {
      CampaignMessageInfo msgInfo(town.getSide(), CampaignMessages::NotInstallFort);
      msgInfo.cp(hunit);
      msgInfo.where(cp->getTown());
      d_moveData->d_campaignGame->sendPlayerMessage(msgInfo);

      CampaignOrderUtil::finishOrder(d_moveData->d_campaignGame, hunit);
      return;
   }

   ICommandPosition engCP = NoCommandPosition;
   ISP engSP = NoStrengthPoint;

   if(!d_moveData->d_armies->findEngineer(hunit, engCP, engSP))
   {
      CampaignMessageInfo msgInfo(town.getSide(), CampaignMessages::NotInstallFortDead);
      msgInfo.cp(hunit);
      msgInfo.where(cp->getTown());
      d_moveData->d_campaignGame->sendPlayerMessage(msgInfo);

      CampaignOrderUtil::finishOrder(d_moveData->d_campaignGame, hunit);
      return;
   }


   if(cp->modeTimeFinished(d_moveData->d_campData->getTick()))
   {
      ASSERT(cp->atTown());

      Town& town = d_moveData->d_campData->getTown(cp->getTown());

      ASSERT(town.getFortifications() < Town::MaxFortifications);
      ASSERT(town.getSide() == cp->getSide());
      ASSERT(engCP != NoCommandPosition);
      ASSERT(engSP != NoStrengthPoint);

      /*
      * Do the build...
      */

      /*
      * Create Fort
      */

      if(town.upgradeFortifications())
      {
         /*
         * Tell the player
         */

         CampaignMessageInfo msgInfo(town.getSide(), CampaignMessages::FortUpgraded);
         msgInfo.cp(hunit);
         msgInfo.where(cp->getTown());
         msgInfo.value1(town.getFortifications());
         d_moveData->d_campaignGame->sendPlayerMessage(msgInfo);
      }

      /*
      * Change mode
      */

      CampaignOrderUtil::finishOrder(d_moveData->d_campaignGame, hunit);

   }
}

/*
 *  Unit is digging field works
 */

// fieldworks not included in Nap1813
void CampUnitMovement::doCampaignDigIn(ICommandPosition cpi)
{
   CommandPosition* cp = d_moveData->d_armies->getCommand(cpi);
   ASSERT(cp->isDigging() == True);

   /*
   *  Make sure engineers are available
   */

   ICommandPosition engCP = NoCommandPosition;
   ISP engSP = NoStrengthPoint;

   if(!d_moveData->d_armies->findSpecialType(cpi, engCP, engSP, SpecialUnitType::Engineer))
   {
#if 0
         d_moveData->d_campaignGame->sendPlayerMessage(cpi, NoTown, "Engineers not available for fieldworks");
#else
      CampaignMessageInfo msgInfo(cp->getSide(), CampaignMessages::EngineerNotFieldWork);
      msgInfo.cp(cpi);
      d_moveData->d_campaignGame->sendPlayerMessage(msgInfo);
#endif
      OrderBase* order = cp->getCurrentOrder();
      order->setDigIn(False);
      CampaignOrderUtil::finishOrder(d_moveData->d_campaignGame, cpi);
   }

   /*
   *  If time has elapsed then 1 level of field works has been completed
   */

   else if(cp->modeTimeFinished(d_moveData->d_campData->getTick()))
   {

      if(d_moveData->d_armies->doDiggingFieldWorks(cpi))
      {
         /*
         *  Tell player one level of fieldworks has been completed
         */

#if 0
            d_moveData->d_campaignGame->sendPlayerMessage(cpi, NoTown, "Fieldworks have been upgraded to level %d.", (int)cp->getFieldWorks());
#else
         CampaignMessageInfo msgInfo(cp->getSide(), CampaignMessages::FieldWorkUpgraded);
         msgInfo.cp(cpi);
         msgInfo.value1(cp->getFieldWorks());
         d_moveData->d_campaignGame->sendPlayerMessage(msgInfo);
#endif
         cp->setModeTime(d_moveData->d_campData->getTick(), DaysToTicks(7));
      }
      else
      {
         /*
         *  Tell player fieldworks have been completed
         */

#if 0
            d_moveData->d_campaignGame->sendPlayerMessage(cpi, NoTown, "We have finished digging in. Fieldworks have been upgraded to level %d.",
            (int)cp->getFieldWorks());
#else
         CampaignMessageInfo msgInfo(cp->getSide(), CampaignMessages::FieldWorkComplete);
         msgInfo.cp(cpi);
         msgInfo.value1(cp->getFieldWorks());
         d_moveData->d_campaignGame->sendPlayerMessage(msgInfo);
#endif
      }
   }
}
#endif

void CampUnitMovement::doCampaignAbnormalActivity(ICommandPosition cpi)
{
   ASSERT(cpi != NoCommandPosition);
   CommandPosition* cp = d_moveData->d_campData->getCommand(cpi);

#ifdef DEBUG
   cuLog.printf("%s is involved in abnormal activity(%s)",
      (const char*)d_moveData->d_campData->getUnitName(cpi).toStr(),
      CampaignMovement::getModeDescription(cp->getMode()));
#endif

   Boolean breakOffActivity = False;

   /*
   * Special case of counter being 0
   */

   if(cp->activityCount() == 0)                  // if 0, do nothing
      return;

   /*
   *  chance of breaking off activity is activityCount as a % of 255
   *
   *  Modified by the following:
   *  1. If aggression is Timid then divide chance by 1
   *  2. Else if aggression is Defensive then divide chance by 2
   *  3. Else if aggression is Offensive then divide chance by 3
   *  4. Else if aggression is Aggressive then divide chance by 4
   *  5. If pursuit option is set divide by 2 again
   */

   else
   {

      static int aggressionMultiplier[Orders::Aggression::HowMany] = {
      1,  // Timid
      2,  // Defensive
      3,  // Offensive
      4   // Aggressive
    };

      /*
      * First, get chance
      */

      int chance = MulDiv(cp->activityCount(), 100, Attribute_Range);

#ifdef DEBUG
      cuLog.printf("Raw chance for abandoning activity is %d", chance);
#endif

      ASSERT(cp->getActiveAggression() < Orders::Aggression::HowMany);

      chance /= aggressionMultiplier[cp->getActiveAggression()];

      if(cp->getCurrentOrder()->getPursue())
         chance /= 2;

      int dieRoll = CRandom::get(100);

#ifdef DEBUG
      cuLog.printf("modified chance is %d, Die Roll is %d", chance, dieRoll);
#endif

      if(dieRoll < chance)
      {
         breakOffActivity = True;
      }
   }


   if(breakOffActivity)
   {
      // clear out target if chasing enemy
      if(cpi->getTarget() != NoCommandPosition && cpi->getTarget()->getSide() != cpi->getSide())
      {
//         cpi->setTarget(NoCommandPosition);
         cpi->getCurrentOrder()->clearTargetUnit();
      }

      CampaignOrderUtil::orderUpdated(d_moveData->d_campaignGame, cpi);
#ifdef DEBUG
      cuLog.printf("Activity stopped. New mode is (%s)",
         CampaignMovement::getModeDescription(cp->getMode()));
#endif
   }
}


/*
 * Adjust distance if forces will overlap
 */

#ifdef DEBUG
#define DEBUG_ADJUSTDISTANCE
#endif

Boolean CampUnitMovement::adjustDistance(ICommandPosition cp1, ICommandPosition cp2,
             Distance& distance, Distance distanceToTravel)
{
   ASSERT(cp1->shouldMove());
   ASSERT(!cp1->atTown());

   if(cp2->atTown())
   {
      if(cp1->getDestTown() == cp2->getTown())
      {
         distance = cp1->getRemainingDistance();

#ifdef DEBUG_ADJUSTDISTANCE
         Town& t = d_moveData->d_campData->getTown(cp1->getDestTown());

         cuLog.printf("%s is at our next node(%s) at a range of %d miles",
            cp2->getName(),
            t.getName(),
            DistanceToMile(distance));
#endif
         return (distance < distanceToTravel);
      }
   }
   else
   {
      ASSERT(cp1->getConnection() != NoConnection);
      ASSERT(cp2->getConnection() != NoConnection);

      /*
      * See if we're on the same connection
      *
      */

      if(cp1->getConnection() == cp2->getConnection())
      {

         /*
         * Find out who is closer to the town
         */

         ITown ourLastTown = cp1->getLastTown();
         ITown theirLastTown = cp2->getLastTown();

         Distance ourDistance = cp1->getDistanceAlong();
         Distance theirDistance = (ourLastTown == theirLastTown) ? cp2->getDistanceAlong() : cp2->getRemainingDistance();

         if(ourDistance <= theirDistance)
         {
            distance = theirDistance - ourDistance;
#ifdef DEBUG_ADJUSTDISTANCE
            cuLog.printf("%s is on our connection(%d) at a range of %d miles",
               cp2->getName(),
               static_cast<int>(cp1->getConnection()),
               DistanceToMile(distance));
#endif
            return (distance < distanceToTravel);
         }
      }
   }

   return False;
}

/*
 * Make sure we don't move past target unit or enemy. Returns true if
 * we have met up with friendly target unit
 */

void CampUnitMovement::adjustDistance(ICommandPosition cpi, Distance& distance)
{
   ASSERT(cpi != NoCommandPosition);

   CommandPosition* cp = d_moveData->d_campData->getCommand(cpi);

   ASSERT(!cp->atTown());

#ifdef DEBUG_ADJUSTDISTANCE
   cuLog.printf("adjusting distance for %s(wants to move %d miles(actual = %ld)",
      (const char*)d_moveData->d_campData->getUnitName(cpi).toStr(),
      DistanceToMile(distance),
      distance);
#endif

   d_moveData->d_closeUnits.rewind();

   Distance closestDistance = Distance_MAX;
   const Distance autoBattleDistance = MilesToDistance(2);

   /*
   * See if we will overlap any enemy
   */

   while(++d_moveData->d_closeUnits)
   {
      const CloseUnitItem& item = d_moveData->d_closeUnits.current();
      ICommandPosition enemyCPI = item.cpi;

      // we can overlap garrison units
      if(enemyCPI->isGarrison())
         continue;

      Distance dist = Distance_MAX;
      if(adjustDistance(cpi, enemyCPI, dist, autoBattleDistance + distance) && dist < closestDistance)
         closestDistance = dist;
   }

   // if so adjust distance
   if(closestDistance < (autoBattleDistance + distance))
      distance = maximum(0, (closestDistance - autoBattleDistance) + 1);


   /*
   * Now see if we will overlap any friendly units who may be our target, or
   * we are their target
   */

   Boolean overLappedTarget = False;
   while(++d_moveData->d_closeFriendlyUnits)
   {
      const CloseUnitItem& item = d_moveData->d_closeFriendlyUnits.current();

      if(cpi->getTarget() == item.cpi || item.cpi->getTarget() == cpi)
      {
         Distance dist = Distance_MAX;
         if(adjustDistance(cpi, item.cpi, dist, distance) && dist < closestDistance)
         {
            closestDistance = dist;
            overLappedTarget = (cpi->getTarget() == item.cpi);
         }
      }
   }

   // if so adjust distance
   if(closestDistance < distance)
   {
      distance = closestDistance;
   }

   if(overLappedTarget)
   {
      CampaignOrderUtil::finishOrder(d_moveData->d_campaignGame, cpi);
#ifdef DEBUG_ADJUSTDISTANCE
      cuLog.printf("%s has arrived friendly target", (const char*)d_moveData->d_campData->getUnitName(cpi).toStr());
#endif
   }

#ifdef DEBUG_ADJUSTDISTANCE
   cuLog.printf("Adjusted distance: actual = %ld, miles = %d",  distance, DistanceToMile(distance));
#endif
}


void CampUnitMovement::findCloseUnits(ICommandPosition cpi)
{
   /*
   * Find enemy units within 28 miles, otherwise get the closest one
   */

   CloseUnitData data;
   data.d_campData = d_moveData->d_campData;
   data.d_cpi = cpi;
   data.d_from = cpi->location();
   data.d_closeUnits = &d_moveData->d_closeUnits;
   data.d_closeUnitSide = (cpi->getSide() == 0) ? 1 : 0;
   data.d_findDistance = CampaignConst::enemyLoseDistance;
   if(cpi->isRetreating())
      data.d_flags |= CloseUnitData::AllAdjacent;

   CloseUnitUtil::findCloseUnits(data);

#ifdef DEBUG
   d_moveData->d_closeUnits.currentUnit = cpi;
#endif

   /*
   * Find friendly units within 24 miles
   */

   data.d_closeUnits = &d_moveData->d_closeFriendlyUnits;
   data.d_closeUnitSide = cpi->getSide();
   data.d_flags = 0;
   data.d_findDistance = MilesToDistance(24);
   CloseUnitUtil::findCloseUnits(data);
#ifdef DEBUG
   d_moveData->d_closeFriendlyUnits.currentUnit = cpi;
#endif
}


/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.1  2001/07/02 15:23:16  greenius
 * Battle, Campaign, Combined game and Campaign Editor now compile and run... mostly
 *
 * Revision 1.2  2001/06/18 08:25:44  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
