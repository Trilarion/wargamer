/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef MULTIPLAYERMSG_HPP
#define MULTIPLAYERMSG_HPP


/*

  Filename : MultiPlayerMsg.hpp

  Description : structures to define messages passed between local & remote for multiplayer games

*/

#include "gamedefs.hpp"


// this is a temporary define -
// see notes accompanying the MP_MSG_CAMPAIGNORDER & MP_MSG_BATLEORDER
#define MAX_PACKED_ORDER_SIZE 1024

#define MP_CRC_CHECK

/*
* After casting void * to MP_MSG_GENERIC, switch on the 'msg_type' field with MultiPlayerMessageEnum
* and then cast the pointer to the appropriate MP_MSG_XXXXX structure
*/

enum MultiPlayerMessageEnum {

	MP_MSG_Generic,
	MP_MSG_SlaveReady,
	MP_MSG_Sync,
	MP_MSG_SyncAcknowledgement,
	MP_MSG_CampaignOrder,
	MP_MSG_BattleOrder,
	MP_MSG_RequestDownload,
	MP_MSG_Download,
	MP_MSG_SetGame,
	MP_MSG_SetGameResponse,
	MP_MSG_SetSide,
	MP_MSG_SetOptions,
	MP_MSG_StartGame,
	MP_MSG_SetSeed,
	MP_MSG_ValidateVersions,
	MP_MSG_SaveGame,
	MP_MSG_CampaignCheckSum,
	MP_MSG_BattleCheckSum
};



enum CampaignMessageTypeEnum {

	CMP_MSG_UNIT,
	CMP_MSG_TOWN,
	CMP_MSG_REORGANIZE,
	CMP_MSG_REPOSP
};


/*
* Generic message to switch on
*/

struct MP_MSG_GENERIC {

	unsigned int msg_type;
};


/*
* Message for Slave machine to tell Master that game-loading is complete
* and it is ready to commence time syncing
*/

struct MP_MSG_SLAVEREADY {

	unsigned int msg_type;
};


/*
* Sync message, sent by Master to advance Slave's time
*/

struct MP_MSG_SYNC {

	unsigned int msg_type;

	unsigned int tick;

#ifdef MP_CRC_CHECK
	unsigned int checksum;
#endif
};


/*
* Sync acknowledgement message, sent by Slave to tell Master that synchronisation is proceeding OK
*/

struct MP_MSG_SYNCACKNOWLEDGEMENT {

	unsigned int msg_type;

	unsigned int tick;
};


/*
* Campaign order message
* It will probably be necessary to split the campaign orders up into their separate types -
* to preserve bandwith due to the varying sizes of different orders
*/

struct MP_MSG_CAMPAIGNORDER {

	unsigned int msg_type;

	unsigned int activation_time;
	unsigned int order_type;
	unsigned int data_length;

	char data[MAX_PACKED_ORDER_SIZE];
};

/*
* Battle order message
* It will probably be necessary to split the battle orders up into their separate types -
* to preserve bandwith due to the varying sizes of different orders
*/

struct MP_MSG_BATTLEORDER {

	unsigned int msg_type;

	unsigned int activation_time;
	unsigned int order_type;
	unsigned int data_length;

	char data[MAX_PACKED_ORDER_SIZE];
};

/*
* Request for a file download
*/

struct MP_MSG_REQUESTDOWNLOAD {

	unsigned int msg_type;

	char filename[128];
};


/*
* Download
* the actual data immediately follows the filename[128] data
*/

struct MP_MSG_DOWNLOAD {

	unsigned int msg_type;

	char filename[128];
};


/*
* Details of game selected
* ie. the WGB / WGC filename & checksum
* If there is a missing file, or there is a checksum mismatch
* Then the client machine must request the relevant file(s) for download
*/

struct MP_MSG_SETGAME {

	unsigned int msg_type;

	char gamefilename[128];
	unsigned int checksum;

	int format; // cast to SaveGame::FileFormat enum
};


/*
* the Slave machine may reply to the above SETGAME message
* either indicates success, or
* a) file wasn't found
* b) checksum was wrong
*/

struct MP_MSG_SETGAMERESPONSE {

	unsigned int msg_type;

	enum SetGameResponseEnum {
		OK,
		FileNotFound,	// file wasn't found
		ChecksumError	// file was found, but was different version
	};

	SetGameResponseEnum response_type;
};


/*
* Set player side
*/

struct MP_MSG_SETSIDE {

	unsigned int msg_type;

	Side opponent_side;
};

/*
* Set options
*/

struct MP_MSG_SETOPTIONS {

	unsigned int msg_type;

	// these correspond to the bitfields stored in the GlabalSettings registry keys (& the OptionBase class)
	unsigned int options;
	unsigned int campaign_options;
};


/*
* Trigger the remote game to start
*/

struct MP_MSG_STARTGAME {

	unsigned int msg_type;

};

/*
* Set random number seed
*/

struct MP_MSG_SETSEED {

	unsigned int msg_type;

	unsigned int seed;
};


/*
* Used to validate that remote is running same game version as local
*/

struct MP_MSG_VALIDATEVERSIONS {

	unsigned int msg_type;

	unsigned int version;
	unsigned int checksum;
};


/*
* Used to tell Slave to save the current game with the specified filename
*/

struct MP_MSG_SAVEGAME {

	unsigned int msg_type;
	char savefilename[128];
};





#endif














