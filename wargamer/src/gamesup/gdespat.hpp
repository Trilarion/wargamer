/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef GDESPAT_HPP
#define GDESPAT_HPP

#ifndef __cplusplus
#error gdespat.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *	Generic Despatcher
 *
 *----------------------------------------------------------------------
 */

#include "mytypes.h"
#include "gamesup.h"
// #include "Sllist.hpp"
#include "critical.hpp"

#include <list>

/*
 * List of messages to be processed next time frame
 */

//swg:27jul2001, Use std::list instead of intrusive list
struct GDespatchMessage // : public SLink 
{
public:
	virtual ~GDespatchMessage() = 0;

	/*
	Pack a multiplayer message into buffer
	NOTE : buffer points to either MP_MSG_CAMPAIGNORDER or MP_MSG_BATTLEORDER accordingly
	*/
	virtual int pack(void * buffer, void * gamedata) = 0;

	/*
	Unpack a multiplayer message from buffer
	NOTE : buffer points to either MP_MSG_CAMPAIGNORDER or MP_MSG_BATTLEORDER accordingly
	*/
	virtual void unpack(void * buffer, void * gamedata) = 0;

	ULONG activationTime;

#ifdef DEBUG
	virtual String getName() = 0;
#endif
};

inline GDespatchMessage::~GDespatchMessage()
{
}

#if 0 //swg:28july2001, replace with std::queue
class GDespatchList : public SList<GDespatchMessage> {
public:
};
#endif

/*
 * Despatcher Class
 */


class GAMESUP_DLL_EXPORT GDespatcher : public SharedData {
public:
	GDespatcher() { }
	~GDespatcher() { }

	void process(ULONG currentTime);			// Called by game logic each tick
	void sendMessage(GDespatchMessage* msg, ULONG activationTime);
private:
//	GDespatchMessage* get();
	virtual void actionMessage(GDespatchMessage* msg) = 0;

   typedef std::list<GDespatchMessage*> GDespatchList;
	GDespatchList d_list;
};


#endif /* GDESPAT_HPP */

