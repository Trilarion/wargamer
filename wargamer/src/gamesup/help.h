/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef HELP_H
#define HELP_H


#ifdef __cplusplus
extern "C" {
#endif

/*
 * Help Topic IDs for Context Help. All ids must be unique.
 * ID cannot be 0.
 * Values must be hard coded. No Arithmitic allowed due too Help-Compiler limitations
 *
 */


/*
 * defines for UnitOrderTB controls
 */

#define IDH_UTB_OrderLeader      11            // Order-Leader Button
#define IDH_UTB_OrderUnit        12            // Order-Unit Button
#define IDH_UTB_DestTown         13            // Town-destination Button
#define IDH_UTB_DestUnit         14            // Unit-destination Button
#define IDH_UTB_Detach           15            // Detach Unit\Leader Button
#define IDH_UTB_SPTransfer       16            // Strength Point transfer Button
#define IDH_UTB_Orders           17            // Order control
#define IDH_UTB_Aggress          18            // Agression-level control
#define IDH_UTB_Send             19            // Send order Button
#define IDH_UTB_Restore          20            // Restore previous order Button
#define IDH_UTB_AdvSettings      21            // Advance settings window
#define IDH_UTB_UnitInfoWind     22            // Unit-info window
#define IDH_UTB_SoundGuns        23            // Sound of Guns Button
#define IDH_UTB_Pursue           24            // Pursue Button
#define IDH_UTB_AllSpeed         25            // March at all speed Button
#define IDH_UTB_ForceMarch       26            // Force March Button
#define IDH_UTB_DigIn            27            // Dig Field-works Button
#define IDH_UTB_SiegeActive      28            // Siege Active Button
#define IDH_UTB_AutoStorm        29            // Auto-Storm Button
#define IDH_UTB_NameDisplay      30
#define IDH_UTB_LeaderHelp       31
#define IDH_UTB_Scroll           32
#define IDH_UTB_InfoPanel        33
/*
 *defines for TownOrderTB controls
 */

#define IDH_TTB_BuildInfantry    41            // Build Infantry Button
#define IDH_TTB_BuildCavalry     42            // Build Cavalry Button
#define IDH_TTB_BuildArtillery   43            // Build Artillery Button
#define IDH_TTB_BuildSpecial     44            // Build Special-Type Button
#define IDH_TTB_EnableBuild      45            // Enable Build Button
#define IDH_TTB_AutoBuild        46            // Auto Build Button
#define IDH_TTB_Priority         47            // Priority-Level Control
#define IDH_TTB_UnitType         48            // Unit-Type Combo
#define IDH_TTB_SendBuild        49            // Send Build-Order Button
#define IDH_TTB_TownInfoWind     50            // Town-Info Window
#define IDH_TTB_NationDisplay    51
#define IDH_TTB_TownHelp         52
#define IDH_TTB_Scroll           53
#define IDH_TTB_InfoPanel        54


/*
 * defines for camptool
 */

#define IDH_ZoomOut          61            // Zoom Out Button
#define IDH_ZoomIn           62            // Zoom In Buttons

/*
 * defines for clock window
 */

#define IDH_GameSpeed        71            // Game-speed display
#define IDH_Pause            72            // Pause Button
#define IDH_GameDate         73            // Game-date display
#define IDH_GameSpeedSlider  74            // Game-speed track bar
#define IDH_AdvTimeDay       75            // Advance 1 day Button
#define IDH_AdvTimeWeek      76            // Advance 1 week Button
#define IDH_AdvTimeFortnight 77            // Advance 1 fortnight Button

/*
 * defines for Tool Bar
 */

#define IDH_TB_Grid          81
#define IDH_TB_TinyMap       82
#define IDH_TB_Palette       83
#define IDH_TB_ZoomIn        84
#define IDH_TB_ZoomOut       85
#define IDH_TB_Menu          86

/*
 * defines for Message Window
 */

#define IDH_MW_PreviousMsg   100
#define IDH_MW_NextMsg       101
#define IDH_MW_DeleteMsg     102
#define IDH_MW_DeleteAllMsg  103
#define IDH_MW_Order         104
#define IDH_MW_Settings      105
#define IDH_MW_DontShow      106
#define IDH_MW_ShowList      107

/*
 * defines for SideBar Info Windows
 */

#define IDH_SBI_MoreInfo     120

/*
 * generic defines for Game Editors
 */

#define IDH_EDIT_HelpIDEdit    130
#define IDH_EDIT_DefaultHelpID 131

/*
 * defines for Town Editor
 */

#define IDH_TE_TownName       140
#define IDH_TE_Delete         141
#define IDH_TE_Cancel         142
#define IDH_TE_Ok             143
#define IDH_TE_Province       144
#define IDH_TE_Set            145
#define IDH_TE_OffScreen      146
#define IDH_TE_Owner          147
#define IDH_TE_TownType       148
#define IDH_TE_Siegeable      149
#define IDH_TE_Victory        150
#define IDH_TE_Strength       151
#define IDH_TE_Stacking       152
#define IDH_TE_Fortifications 153
#define IDH_TE_Political      154
#define IDH_TE_Align          155
#define IDH_TE_XPosition      156
#define IDH_TE_YPosition      157
#define IDH_TE_Terrain        158
#define IDH_TE_MoveTown       159
#define IDH_TE_SupplyRate     160
#define IDH_TE_Forage                   -1              // Needs defining
#define IDH_TE_SupplySource   161
#define IDH_TE_SupplyDepot    162
#define IDH_TE_ManpowerRate   163
#define IDH_TE_Transferable   164
#define IDH_TE_ResourceRate   165

/*
 * defines for OB Editor
 */

#define IDH_OBE_UnitsListBox     200
#define IDH_OBE_OK               201
#define IDH_OBE_Cancel           202
#define IDH_OBE_UnitRankLevel    203
#define IDH_OBE_UnitSide         204
#define IDH_OBE_UnitNation       205
#define IDH_OBE_SPList           206
#define IDH_OBE_UnitType         207
#define IDH_OBE_BasicTypes       208
#define IDH_OBE_EditSP           209
#define IDH_OBE_NewSP            210
#define IDH_OBE_CurrentPosition  211
#define IDH_OBE_NewPosition      212
#define IDH_OBE_TownList         213
#define IDH_OBE_Move             214
#define IDH_OBE_UnitName         215
#define IDH_OBE_Morale           216
#define IDH_OBE_Fatigue          217
#define IDH_OBE_Supply           218
#define IDH_OBE_LeaderName       219
#define IDH_OBE_LeaderNation     220
#define IDH_OBE_Initiative       221
#define IDH_OBE_Staff            222
#define IDH_OBE_Subordination    223
#define IDH_OBE_Aggression       224
#define IDH_OBE_Charisma         225
#define IDH_OBE_Ability          226
#define IDH_OBE_Radius           227
#define IDH_OBE_RankRating       228
#define IDH_OBE_Health           229
#define IDH_OBE_RankLevel        230

/*
 * defines for Connection Editor
 */

#define IDH_CE_List              250
#define IDH_CE_OffScreen         251
#define IDH_CE_Distance          252
#define IDH_CE_Type              253
#define IDH_CE_Quality           254
#define IDH_CE_Capacity          255
#define IDH_CE_Delete            256
#define IDH_CE_Close             257

/*
 * defines for Province Editor
 */

#define IDH_PE_ProvinceName      280
#define IDH_PE_Abreviated        281
#define IDH_PE_OffScreen         282
#define IDH_PE_Owner             283
#define IDH_PE_Allegiance        284
#define IDH_PE_SetCapital        285
#define IDH_PE_SetTown           286
#define IDH_PE_Nationality       287
#define IDH_PE_Move              288
#define IDH_PE_Delete            289

/*
 *  defines for Find-Town Dial
 */

#define IDH_FTD_Search           300
#define IDH_FTD_TownList         301
#define IDH_FTD_FindTown         302

/*
 * defines for Find-Leader Dial
 */

#define IDH_FLD_Search           310
#define IDH_FLD_LeaderList       311
#define IDH_FLD_FindLeader       312

/*
 * defines for Player-Settings Dial
 */

#define IDH_PSD_Player           320
#define IDH_PSD_AI               321
#define IDH_PSD_Remote           322

/*
 * defines for Options Dial
 */

#define IDH_POD_InstantOrders    340
#define IDH_POD_InstantMove      341
#define IDH_POD_FullRoute        342
#define IDH_POD_NoOrderDelay     343
#define IDH_POD_DelayOrder5      344
#define IDH_POD_DelayOrder10     345
#define IDH_POD_ViewEnemy        346
#define IDH_POD_OrderEnemy       347
#define IDH_POD_AggressChange    348
//#define IDH_POD_LimitedUnitInfo  349
//#define IDH_POD_LimitedTownInfo  350
#define IDH_POD_SupplyRules      351
#define IDH_POD_Attrition        352
#define IDH_POD_NationRules      353
#define IDH_POD_ToolTips         354
#define IDH_POD_ColorCursor      355
#define IDH_POD_AlertBox         356
#define IDH_POD_TownNames        357
#define IDH_POD_NationDisplay    358
#define IDH_POD_ShowMsg          359

#define IDH_POD_QuickMove           360         // Added by Steven
#define IDH_POD_TownIcons                       361
#define IDH_POD_TownHilightIcons        362
#define IDH_POD_DrawMapScale            363

#define IDH_POD_DeadBodies              364     // added by Jim
#define IDH_POD_FogOfWar                365
#define IDH_POD_Labels                  366

#define IDH_POD_ShowSupply              367
#define IDH_POD_ShowChokePoints              368
#define IDH_POD_ShowSupplyLines              369

/*
 * defines for Window Topic ids
 */

#define IDH_Wargamer             500
#define IDH_UnitOrderWindow      501
#define IDH_TownOrderWindow      502

/*
 * Test defines for merging help with new campaign window
 */

#define IDH_Opening_Test         550
#define IDH_Opening_TestJump     551

#ifdef __cplusplus
};
#endif

#endif
