##############################################################################
# Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)	 #
# This Software is subject to the GNU General Public License.  				 #
# For License information see the file COPYING in the project root directory #
# For more information see the file README.									 #
##############################################################################

###########################################
# Battle Datae Control Library Makefile
###########################################

FNAME = gameSup

lnk_dependencies += makedll.mk

!include ..\wgpaths.mif

CFLAGS += -DEXPORT_GAMESUP_DLL
# CFLAGS += -fi=dll.h

OBJS =  scenario.obj	&
	name.obj	&
	control.obj	&
	options.obj	&
	wg_msg.obj	&
	wldfile.obj  	&
	savegame.obj  	&
	scn_img.obj  	&
	print.obj 	&
	wg_rand.obj	&
	gdespat.obj	&
	b_tables.obj	&
	wg_main.obj	&
	about.obj	&
	gameinfo.obj	&
	victory.obj	&
	gmsgwin.obj     &
	plyrdlg.obj  	&
        gthread.obj

!ifndef NODEBUG
OBJS += ai_panel.obj
!endif

CFLAGS += -i=$(ROOT)\res
CFLAGS += -i=$(ROOT)\system

SYSLIBS += COMCTL32.LIB VFW32.LIB DSOUND.LIB
# SYSLIBS += USER32.LIB KERNEL32.LIB GDI32.LIB WINMM.LIB SHELL32.LIB

LIBS += system.lib


!include ..\dll95.mif

.before:
        @echo Making $(TARGETS)

