/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef FILENAME_HPP
#define FILENAME_HPP

#ifndef __cplusplus
#error filename.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1995, Steven Morle-Green, All Rights Reserved
 *----------------------------------------------------------------------
 *
 *	Specific Filenames used in Wargamer
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 *
 *----------------------------------------------------------------------
 */

#define SCENARIO_EXT "SWG"
#define CAMPAIGN_EXT "WGC"
#define BATTLE_EXT 	"WGB"
#define SAVE_EXT 		"WGS"

#endif /* FILENAME_HPP */

