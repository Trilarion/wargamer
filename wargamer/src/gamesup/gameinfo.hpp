/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef GAMEINFO_HPP
#define GAMEINFO_HPP

#include "StringPtr.hpp"
#include "victory.hpp"
#include "gamesup.h"

class FileReader;
class FileWriter;

class GAMESUP_DLL_EXPORT MissionInfo {
	 CString d_description;            // description of the Mission
	 CString d_rtfFileName;            // Mission briefing .RTF file
	 CString d_graphicFileName;        // Mission briefing .BMP file
	 CString d_side0AVIFileName;       // Side 0 Victory .AVI file
	 CString d_side1AVIFileName;       // Side 1 Victory .AVI file
	 CString d_drawAVIFileName;        // Draw .AVI file

  public:
	 static const char NoDescription[];
	 static const char NoRTFFileName[];
	 static const char NoGraphicFileName[];
	 static const char NoAVIFileName[];

	 MissionInfo(); // {}
	 MissionInfo(const MissionInfo& info) { copy(info); } // copy constructor
	 MissionInfo& operator=(const MissionInfo& info) { return copy(info); }
	 ~MissionInfo() {}

	 void setDescription(const char* description);
	 const char* getDescription() const { return d_description; }

	 void setRTFFileName(const char* fileName);
	 const char* getRTFFileName() const { return d_rtfFileName; }

	 void setGraphicFileName(const char* fileName);
	 const char* getGraphicFileName() const { return d_graphicFileName; }

	 const char* getAVIFileName(GameVictory::WhoWon who) const;

	 void setSide0AVIFileName(const char* fileName);
	 const char* getSide0AVIFileName() const { return d_side0AVIFileName; }

	 void setSide1AVIFileName(const char* fileName);
	 const char* getSide1AVIFileName() const { return d_side1AVIFileName; }

	 void setDrawAVIFileName(const char* fileName);
	 const char* getDrawAVIFileName() const { return d_drawAVIFileName; }


	 Boolean read(FileReader& f);
	 Boolean write(FileWriter& f) const;

  private:
	 MissionInfo& copy(const MissionInfo& info);
};


#endif
