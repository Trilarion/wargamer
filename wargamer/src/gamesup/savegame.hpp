/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef SAVEGAME_HPP
#define SAVEGAME_HPP

#ifndef __cplusplus
#error savegame.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *	Load and Save Game Utility Class
 *
 *----------------------------------------------------------------------
 */

#include "mytypes.h"
#include "gamesup.h"
#include "except.hpp"

class SimpleString;

class GAMESUP_DLL_EXPORT OpenFileDialog 
{
	char d_fileName[MAX_PATH];
	char d_nameNoPath[MAX_PATH];
 public:
	OpenFileDialog();
	bool ask(const char* title, const char* filter, const char* defName, const char* defDir);
	const char* copyFileName() const;
	const char* fileName() const;
	const char* fileNameNoPath() const;
	const char* copyFileNameNoPath() const;
};

class GAMESUP_DLL_EXPORT SaveFileDialog 
{
	char d_fileName[MAX_PATH];
 public:
	SaveFileDialog();
	bool ask(const char* title, const char* filter, const char* defName, const char* defDir);
	const char* fileName() const;
	void addExtension(const char* ext);
};



namespace SaveGame {

	/*
	 * Enumeration used for the mode in the file format
	 */

	enum FileFormat {
		Campaign,			// Scenario
		Battle,
		SaveGame,		// Player's saved game
	};

	GAMESUP_DLL_EXPORT bool askLoadName(SimpleString& s, FileFormat mode);
	GAMESUP_DLL_EXPORT bool askSaveName(SimpleString& s, FileFormat mode);
   GAMESUP_DLL_EXPORT const char* getDefFolder(FileFormat mode);

	// Boolean askSaveWorld(SimpleString& s);
	// Boolean askLoadWorld(SimpleString& s);
	// Boolean askSaveGame(SimpleString& s);
	// Boolean askLoadGame(SimpleString& s);
};

namespace BrowseForFile {
   GAMESUP_DLL_EXPORT Boolean load(char* fileName, char* filter);
};


/*
 * Useful Chunk type for writing a file version
 */

class FileReader;
class FileWriter;


class GAMESUP_DLL_EXPORT FileTypeChunk
{
	public:
        typedef UWORD Version;


		FileTypeChunk(SaveGame::FileFormat mode) : d_mode(mode), d_version(s_version) { }

		Boolean readData(FileReader& f);
		Boolean writeData(FileWriter& f) const;

		SaveGame::FileFormat mode() const { return d_mode; }
        Version version() const { return d_version; }

        struct WrongVersionError : public GeneralError
        {
            WrongVersionError(Version v) :
                GeneralError("File has illegal version (%d.%d)", int(v >> 16), int(v & 0xffff))
                {
                }
        };

	private:
		SaveGame::FileFormat d_mode;
        Version d_version;

		static char s_chunkID[];
		static Version s_version;
};



#endif /* SAVEGAME_HPP */

