/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#include "stdinc.hpp"
#include "gameinfo.hpp"
#include "misc.hpp"
#include "wldfile.hpp"
#include "filebase.hpp"
#include "filecnk.hpp"



const char MissionInfo::NoDescription[] = "No Description Available";
const char MissionInfo::NoRTFFileName[] = "No RTF File";
const char MissionInfo::NoGraphicFileName[] = "No Graphic File";
const char MissionInfo::NoAVIFileName[] = "No AVI File";

const char descriptionChunkName[] = "CampaignDescription";

MissionInfo::MissionInfo() :
  d_description(copyString(NoDescription)),
  d_rtfFileName(copyString(NoRTFFileName)),
  d_graphicFileName(copyString(NoGraphicFileName)),
  d_side0AVIFileName(copyString(NoAVIFileName)),
  d_side1AVIFileName(copyString(NoAVIFileName)),
  d_drawAVIFileName(copyString(NoAVIFileName))
{
}

void MissionInfo::setDescription(const char* description)
{
  ASSERT(description != 0);

  d_description = copyString(description);
  ASSERT(d_description != 0);
}

void MissionInfo::setRTFFileName(const char* fileName)
{
  ASSERT(fileName != 0);

  d_rtfFileName = copyString(fileName);
  ASSERT(d_rtfFileName != 0);
}

void MissionInfo::setGraphicFileName(const char* fileName)
{
  ASSERT(fileName != 0);

  d_graphicFileName = copyString(fileName);
  ASSERT(d_graphicFileName != 0);
}

void MissionInfo::setSide0AVIFileName(const char* fileName)
{
  ASSERT(fileName != 0);

  d_side0AVIFileName = copyString(fileName);
  ASSERT(d_side0AVIFileName != 0);
}

void MissionInfo::setSide1AVIFileName(const char* fileName)
{
  ASSERT(fileName != 0);

  d_side1AVIFileName = copyString(fileName);
  ASSERT(d_side1AVIFileName != 0);
}

void MissionInfo::setDrawAVIFileName(const char* fileName)
{
  ASSERT(fileName != 0);

  d_drawAVIFileName = copyString(fileName);
  ASSERT(d_drawAVIFileName != 0);
}

const char* MissionInfo::getAVIFileName(GameVictory::WhoWon who) const
{
  ASSERT(who != GameVictory::GameInProgress);

  if(who == GameVictory::Side0)
	 return d_side0AVIFileName;
  else if(who == GameVictory::Side1)
	 return d_side1AVIFileName;
  else
	 return d_drawAVIFileName;

}



/*
 * file interface
 */

const UWORD DVersion = 0x0001;

Boolean MissionInfo::write(FileWriter& f) const
{
  FileChunkWriter fc(f, descriptionChunkName);

  if(f.isAscii())
  {


  }
  else
  {
	 f.putUWord(DVersion);

	 f.putString(d_description);
	 f.putString(d_rtfFileName);
	 f.putString(d_graphicFileName);
	 f.putString(d_side0AVIFileName);
	 f.putString(d_side1AVIFileName);
	 f.putString(d_drawAVIFileName);

  }
  return f.isOK();
}

Boolean MissionInfo::read(FileReader& f)
{
  FileChunkReader fc(f, descriptionChunkName);
  if(!fc.isOK())
	 return False;

  if(f.isAscii())
  {

  }
  else
  {
	 UWORD version;
	 f.getUWord(version);
	 ASSERT(version <= DVersion);

	 char* buf = 0;
	 buf = f.getString();
	 if(buf)
		d_description = copyString(buf);

	 buf = f.getString();
	 if(buf)
		d_rtfFileName = copyString(buf);

	 buf = f.getString();
	 if(buf)
		d_graphicFileName = copyString(buf);

	 if(version >= 0x0001)
	 {
		buf = f.getString();
		if(buf)
		  d_side0AVIFileName = copyString(buf);

		buf = f.getString();
		if(buf)
		  d_side1AVIFileName = copyString(buf);

		buf = f.getString();
		if(buf)
		  d_drawAVIFileName = copyString(buf);
	 }
  }

  return f.isOK();
}

MissionInfo& MissionInfo::copy(const MissionInfo& info)
{
  setDescription(info.getDescription());
  setRTFFileName(info.getRTFFileName());
  setGraphicFileName(info.getGraphicFileName());
  setSide0AVIFileName(info.getSide0AVIFileName());
  setSide1AVIFileName(info.getSide1AVIFileName());
  setDrawAVIFileName(info.getDrawAVIFileName());
  return *this;
}
