/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#include "stdinc.hpp"
#include "print.hpp"
#include "myassert.hpp"
#include <commdlg.h>

namespace Print {

BOOL print(HWND hParent, HDC& hdc, int nPages)
{
  PRINTDLG pd;

  pd.lStructSize = sizeof(PRINTDLG);
  pd.hDevMode = NULL;
  pd.hDevNames = NULL;
  pd.Flags = PD_PRINTSETUP | PD_RETURNDC;
  pd.hwndOwner = hParent;
  pd.hDC = NULL;
  pd.nFromPage = 1;
  pd.nToPage = nPages;
  pd.nMinPage = 0;
  pd.nMaxPage = 0;
  pd.nCopies = 1;
  pd.hInstance = NULL;
  pd.lCustData = 0L;
  pd.lpfnPrintHook = NULL;
  pd.lpfnSetupHook = NULL;
  pd.lpPrintTemplateName = NULL;
  pd.lpSetupTemplateName = NULL;
  pd.hPrintTemplate = NULL;
  pd.hSetupTemplate = NULL;

  BOOL result = PrintDlg(&pd);
  if(result)
  {
	 ASSERT(pd.hDC);
	 hdc = pd.hDC;
  }

  return result;
}

}; // namespace Print
