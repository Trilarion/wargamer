/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef REFCP_HPP
#define REFCP_HPP

#ifndef __cplusplus
#error refcp.hpp is for use with C++
#endif

#pragma error "Obsolete Use obdefs.hpp or cpdefs.hpp"

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 * Include file for users of
 *	Reference Counters for CommandPositions, Leaders and SPs
 *
 * Even though these are in gamesup\ they are really campaign specific
 *
 *----------------------------------------------------------------------
 */

#endif /* REFCP_HPP */

