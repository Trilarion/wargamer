/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef GTHREAD_HPP
#define GTHREAD_HPP

#ifndef __cplusplus
#error gthread.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 * Game Threads
 *
 *----------------------------------------------------------------------
 */

#include "gamesup.h"

namespace Greenius_System
{
class ThreadManager;
}

/*
 * Singleton for GameLogic ThreadManager
 *
 * Not strictly a singleton because it returns a different type
 * than itself... but this avoids having to include ThreadManager
 * in this header file.
 */

class GAMESUP_DLL_EXPORT GameThreads
{
    public:
        static Greenius_System::ThreadManager* instance();

        static void pause(bool pauseMode);

    private:
        GameThreads(const GameThreads&);
        GameThreads& operator=(const GameThreads&);
        GameThreads();
        ~GameThreads();

    private:
        static Greenius_System::ThreadManager* s_instance;
};




#endif /* GTHREAD_HPP */

/*
 *----------------------------------------------------------------------
 * $Log$
 *----------------------------------------------------------------------
 */
