/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef SCENARIO_H
#define SCENARIO_H

#ifndef __cplusplus
#error scenario.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1995, Steven Morle-Green, All Rights Reserved
 *----------------------------------------------------------------------
 *
 *      Interface to scenario specific data
 *
 *----------------------------------------------------------------------
 */

#include <wtypes.h>                     // Needed for COLORREF
#include "gamedefs.hpp"
#include "StringPtr.hpp"
#include "array.hpp"
#include "tables.hpp"
#include "mappoint.hpp"
// #include "palwind.hpp"
#include "custbord.hpp"
// #include "palette.hpp"

#include "gamesup.h"

/*
 * Forward References
 */

class ConfigFile;
class DrawDIB;
class ImageLibrary;
struct ImagePos;

const int MaxCampaignMaps = 4;

struct MapScaleInfo
{
        unsigned int s_widthMiles;      // Width between p1->p2 and p3->p4
        unsigned int s_heightMiles;     // Distance between p1..p3 and p2..p4
        MapPoint s_total;               // Logical Map
        MapPoint s_p[4];                // TopLeft, TopRight, BottomLeft, BottomRight
        int s_viewAngle;                // Angle in degrees that viewpoint is facing, e.g. 0=North, 90=East
};

enum FontStyle
{
        Font_Normal,
        Font_Italic,
        Font_Bold,
        Font_BoldItalic,
        Font_Small,
        Font_SmallItalic,
        Font_SmallBold,
        Font_SmallBoldItalic,
        Font_Decorative,

        Font_HowMany
};

class GAMESUP_DLL_EXPORT  MissingColor : public GeneralError
{
        public:
                MissingColor(const char* name) : GeneralError("Missing Color: %s", name) { }
};

class GAMESUP_DLL_EXPORT Scenario_Base
{
   public:

      /*
      * Misc. dib fillers
      */

      class BackPatterns
      {
         public:
         enum Value
         {
            First = 0,

               PaperBackground = First,
               MenuBarBackground,
               HScrollBackground,
               VScrollBackground,
               HScrollThumbBackground,
               VScrollThumbBackground,
               BarChartBackground,
               BarChartForeground,
               CherryWoodBackground,
               // ScrollBarBackground,
               ButtonBackground,

               HowMany,

               // For backward compatibility

               WoodBackground=MenuBarBackground,
         };
      };

      class ScenarioFileException : public GeneralError {
         public:
         ScenarioFileException() : GeneralError("Error Reading Scenario File") { }
         ScenarioFileException(const char* s, ...);
      };


      struct KeywordNotFound : public GeneralError
      {
         KeywordNotFound(const char* fileName, const char* keyword) :
         GeneralError("Keyword \"%s\" not found in file %s",
            keyword, fileName)
         {
         }
      };
};

/*
 * Scenario Class Definition
 */

class GAMESUP_DLL_EXPORT Scenario : public Scenario_Base
{
   public:
        static void create(const char* name);
        static void kill();
        static char* defaultFileName();

        virtual int getInt(const char* keyWord) = 0;

        virtual void lockConfig() = 0;
        virtual void unlockConfig() = 0;

        virtual int getNumSides() const = 0;
        virtual const char* getSideName(Side side) const = 0;
        virtual COLORREF getSideColour(Side side) const = 0;

        virtual COLORREF getBorderColour(IBorderColour i) const = 0;
        virtual const CustomBorderInfo& getBorderColors() const = 0;

        virtual const char* getFolderName() const = 0;
//        virtual const char* getAnimationFolderName() const = 0;

        virtual const char* getTinyMapFileName() const = 0;
        virtual const char* getPaletteFileName() const = 0;
        virtual const char* getUnitTypeFileName() const = 0;
        virtual const char* getTerrainTypeFileName() const = 0;
        virtual const char* getHelpFileName() const = 0;
        virtual const char* getHistoryFileName() const = 0;
        virtual char* makeScenarioFileName(const char* fileName, bool mustExist = true) const = 0;

		  virtual StringPtr getPortraitPath() const = 0;
		  virtual StringPtr getProvincePicturePath() const = 0;

        virtual HINSTANCE getScenarioResDLL() const = 0;

        virtual const DrawDIB* getSideBkDIB(Side side) = 0;
        virtual const DrawDIB* getScenarioBkDIB(BackPatterns::Value v) = 0;

        virtual const char* getFileName() const = 0;

        /*
         *  Scenario combat tables
         */

        virtual const Table1D<UBYTE>& getCombatDivisorTable() = 0;
        virtual const Table1D<UWORD>& getUnitCountModifierTable() = 0;
        virtual const Table1D<UWORD>& getBreakOutBattleTable() = 0;
        virtual const Table1D<UWORD>& getSortieTestTable() = 0;
        virtual const Table1D<UWORD>& getStartMoraleModifierTable() = 0;
        virtual const Table1D<SWORD>& getBombardmentNationModifierTable() = 0;
        virtual const Table1D<SWORD>& getBombardmentModifierTable() = 0;
        virtual const Table2D<SWORD>& getBombardmentWeatherTable() = 0;
        virtual const Table3D<SWORD>& getCasualtyCavalryModifierTable() = 0;
        virtual const Table2D<SWORD>& getCasualtyNationTable() = 0;
        virtual const Table3D<UWORD>& getLossPostureModifierTable() = 0;
        virtual const Table1D<UWORD>& getLossModifierTable() = 0;
        virtual const Table2D<SWORD>& getMoraleNationModifierTable() = 0;
        virtual const Table3D<SWORD>& getMoraleDieRollModifierTable() = 0;
        virtual const Table3D<UWORD>& getMoraleLossModifierTable() = 0;
        virtual const Table1D<SWORD>& getLeaderHitNationModifierTable() = 0;
        virtual const Table1D<SWORD>& getLeaderHitRankModifierTable() = 0;
        virtual const Table1D<int>& getForceSizeTable() = 0;
        virtual const Table1D<int>& getSizeModifierTable() = 0;
        virtual const Table1D<UBYTE>& getDeploymentTimeTable() = 0;
        virtual const Table1D<UBYTE>& getSupplyModifierTable() = 0;
        virtual const Table1D<SWORD>& getCombatDieRollModifier() = 0;
        virtual const Table1D<int>& getCasualityEffectTable() = 0;
        virtual const Table1D<int>& getBombardmentEffectTable() = 0;
        virtual const Table2D<Attribute>& getMoraleEffectTable() = 0;
        virtual const Table1D<UWORD>& getAlterLeaderTable() = 0;
        virtual const Table1D<int>& getTacticalPursuitTable() = 0;
        virtual const Table3D<int>& getCasualtyTerrainModifierTable() = 0;
        virtual const Table3D<int>& getCasualtyRiverTypeModifierTable() = 0;
        virtual const Table2D<int>& getBombardmentTerrainModifierTable() = 0;
        virtual const Table2D<UBYTE>& getLeaderHitTable() = 0;

        /*
         * Scenario Siege tables
         */
        virtual const Table1D<UBYTE>* getActiveSiegeTableNoTrain() = 0;
        virtual const Table1D<UBYTE>* getActiveSiegeTableTrain() = 0;
        virtual const Table1D<UBYTE>* getSortieTableAttacker() = 0;
        virtual const Table1D<UBYTE>* getSortieTableDefender() = 0;
        virtual const Table1D<UBYTE>* getStormResultTable() = 0;
        virtual const Table1D<int>* getStormLossTableAttacker() = 0;
        virtual const Table1D<int>* getStormLossTableDefender() = 0;
        virtual const Table1D<UBYTE>* getSallyResultTable() = 0;
        virtual const Table1D<int>* getSallyLossTableAttacker() = 0;
        virtual const Table1D<int>* getSallyLossTableDefender() = 0;
        virtual const Table1D<SWORD>* getSiegeDieRollModifier() = 0;

        /*
         *  Scenario Aggression tables
         */

        virtual const Table3D<UBYTE>& getAggressionResultTable() = 0;
        virtual const Table2D<SWORD>& getAggressionChangeTable() = 0;

        /*
         * Attrition tables
         */

        virtual const Table2D<int>& getAttritionTable() = 0;
        virtual const Table2D<SWORD>& getAttritionModifierForConnectionTable() = 0;
        virtual const Table2D<SWORD>& getAttritionModifierForTownTable() = 0;
        virtual const Table1D<SWORD>& getAttritionModifiersTable() = 0;

        /*
         *  Delayed order tables
         */

        virtual const Table2D<int>& getCourierSpeedAdjustmentTable() = 0;
        virtual const Table1D<UBYTE>& getActionTimeTable() = 0;
        virtual const Table2D<int>& getActionTimeAdjustmentTable() = 0;

        /*
         * Movement-Speed Tables
         */

        virtual const Table2D<int>& getBasicSpeedTable() = 0;
        virtual const Table1D<int>& getConnectQualityTable() = 0;

        /*
         * Weather Tables
         */

        virtual const Table2D<UBYTE>& getWeatherTable() = 0;
        virtual const Table1D<SWORD>& getConditionsAdjustmentTable() = 0;
        virtual const Table2D<int>& getConditionToSpeedTable() = 0;

        /*
         * Nations Defecting tables
         */

        virtual const Table1D<UBYTE>& getNationDefectsTable() = 0;
        virtual const Table1D<UBYTE>& getNationDefectsSideTable() = 0;
        virtual const Table1D<SWORD>& getNationDefectsModifierTable() = 0;
        virtual const Table1D<UBYTE>& getNationDefectsEnemyNationsTable() = 0;
        virtual const Table1D<UBYTE>& getSideDepotLimitTable() = 0;

        /*
         * Fog Of War Tables
         */

        virtual const Table2D<UBYTE>& getSpottingEnemyTable() = 0;
        virtual const Table1D<SWORD>&  getSpottingEnemyModifierTable() = 0;
        virtual const Table2D<UBYTE>& getInformationQualityTable() = 0;
        virtual const Table1D<SWORD>& getInfoQualityModifierTable() = 0;

        /*
         * Random Event Tables
         */

        virtual const Table2D<UBYTE>& getRandomEventTable() = 0;
        virtual const Table1D<UBYTE>& getLeaderHealthCutoffTable() = 0;

        /*
         * Town-Delay table
         */

        virtual const Table2D<UBYTE>& getTownDelayTable() = 0;
        virtual const Table1D<UBYTE>& getTownSizeMultiplierTable() = 0;

        /*
         * Random Leader tables
         */

        virtual const Table2D<UBYTE>& getLeaderBaseValueTable() = 0;
        virtual const Table1D<UBYTE>& getLeaderDecreaseAbilityTable() = 0;
        virtual const StringTable& getSideLeaderNameTable(Side side) = 0;

        /*
         * Recover Morale tables
         */

        virtual const Table2D<UBYTE>& getRecoverMoraleFromRestingTable() = 0;
        virtual const Table1D<UBYTE>& getRecoverMoraleRetreatingTable() = 0;
        virtual const Table2D<int>& getRecoverMoraleRestingNationTable() = 0;
        virtual const Table2D<int>& getRecoverMoraleRetreatingNationTable() = 0;

        /*
         * Supply tables
         */

        virtual const Table1D<UWORD>& getSupplyMiscTable() = 0;
        virtual const Table1D<UWORD>& getSupplyMonthlyTable() = 0;
        virtual const Table1D<UWORD>& getSupplyWeatherTable() = 0;

        virtual int getNumNations() const = 0;
        virtual Side nationToSide(Nationality n) const = 0;
        virtual const char* getNationName(Nationality n) const = 0;
        virtual const char* getNationAdjective(Nationality n) const = 0;
        virtual COLORREF getNationColour(Side side) const = 0;

        virtual NationType getNationType(Nationality n) const = 0;

        virtual Nationality getGenericNation(Side s) const = 0;
        virtual Nationality getDefaultNation(Side s) const = 0;
        virtual void setAlliance(Nationality n, Side s) = 0;
        virtual ImageLibrary* getNationFlag(Nationality n, Boolean actual) = 0;
        virtual unsigned int getNationFlagSpriteIndex(Nationality n) = 0;

        virtual Boolean isEnemy(Side s1, Side s2) = 0;

        virtual unsigned int getNumCampaignMaps() const = 0;
        virtual const char* getCampaignMapName(unsigned int i) const = 0;

        virtual ImageLibrary* getImageLibrary(LPCTSTR bmpResName, UWORD n, const ImagePos* pos, Boolean dc = False) = 0;

        virtual HCURSOR getZoomCursor() const = 0;
        virtual HCURSOR getGloveCursor() const = 0;
        virtual HCURSOR getDragCursor() const = 0;

        virtual const MapScaleInfo* getMapScaleInfo() = 0;
                // Get Information about Map Scale size

        virtual const char* fontName(FontStyle i) const = 0;

        virtual bool getColour(const char* name, COLORREF& cref) const = 0;
                // Get Scenario Colour associated with "name"

        virtual COLORREF getColour(const char* name) const = 0;

		  virtual const char* defaultPortrait(Side s) const = 0;

        struct LockConfig
        {
            LockConfig();
            ~LockConfig();
        };
};

/*
 * Global instance
 */

extern GAMESUP_DLL_EXPORT Scenario* scenario;


inline Scenario::LockConfig::LockConfig()
{
   scenario->lockConfig();
}

inline Scenario::LockConfig::~LockConfig()
{
   scenario->unlockConfig();
}

#endif /* SCENARIO_H */

