/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1995, Steven Morle-Green, All Rights Reserved
 *----------------------------------------------------------------------
 *
 *	Game Options
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/06/13 08:52:38  greenius
 * Initial Import
 *
 * Revision 1.1  1996/02/29 11:01:53  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "options.hpp"
#include "registry.hpp"
#include "myassert.hpp"
#include "cmsg_id.hpp"
#include "filebase.hpp"
#include "misc.hpp"
#include "resdef.h"
#include "res_str.h"
#include "help.h"
#include "alertbox.hpp"


#define OptionSize ((OPT_GameOption_HowMany + 31) / 32)
#define CampOptionSize ((OPT_CampaignOption_HowMany + 31) / 32)
#define MsgOptionSize ((CampaignMessages::CMSG_HowMany + 31) / 32)
#define AlertBoxOptionSize ((AB_HowMany + 31) / 32)

/*
 * Registry Strings
 */

static const char globRegName[] = "GlobalSettings";
static const char regOptions[] = "Options";
static const char regCampOptions[] = "CampaignOptions";
static const char regMessages[] = "MsgShow";
static const char regMessageCount[] = "MsgCount";
static const char regAlert[] = "AlertShow";
static const char regVersion[] = "Version";

/*
 * Options Groups & Default Values for Difficulty Settings
 */


/*
 * Generic Game Options
 */

OptionInfo OptionSettingsInfo::gameOptionsSettings[] = {

//	{ OPT_FullRoute,		GameOptionsPage, 	IDS_OPT_FullRoute,			IDH_POD_FullRoute, 			true,  true,  true  },
	{ OPT_ColorCursor,		GameOptionsPage, 	IDS_OPT_ColorCursor,		IDH_POD_ColorCursor, 		false, false, false },
	{ OPT_ToolTips,			GameOptionsPage, 	IDS_OPT_ToolTips,			IDH_POD_ToolTips, 			true,  true,  true  },
	{ OPT_AlertBox,			GameOptionsPage, 	IDS_OPT_AlertBox,			IDH_POD_AlertBox, 			true,  true,  true  },
	{ OPT_TownNames,		GameOptionsPage, 	IDS_OPT_TownNames,			IDH_POD_TownNames, 			true,  true,  true  },
	{ OPT_NationDisplay,	GameOptionsPage, 	IDS_OPT_NationDisplay,		IDH_POD_NationDisplay, 		false, true,  true  },
	{ OPT_TownIcons,		GameOptionsPage, 	IDS_OPT_TownIcons,			IDH_POD_TownIcons, 			true,  true,  true  },
	{ OPT_TownHilightIcons,	GameOptionsPage, 	IDS_OPT_TownHilightIcons,	IDH_POD_TownHilightIcons, 	true,  true,  true  },
	{ OPT_DrawMapScale,		GameOptionsPage, 	IDS_OPT_DrawMapScale,	    IDH_POD_DrawMapScale, 	    true,  true,  true  },
	{ OPT_ShowSupply,		GameOptionsPage, 	IDS_OPT_ShowSupply,			IDH_POD_ShowSupply, 	    false,  true,  true  },
	{ OPT_ShowChokePoints,	GameOptionsPage, 	IDS_OPT_ShowChokePoints,	IDH_POD_ShowChokePoints, 	false,  true,  true  },
	{ OPT_ShowSupplyLines,		GameOptionsPage, 	IDS_OPT_ShowSupplyLines,			IDH_POD_ShowSupplyLines, 	    false,  false,  true  },

	{ OPT_DeadBodies,			GameOptionsPage, 	IDS_OPT_DeadBodies,			IDH_POD_DeadBodies, 		true,	true,  true },
	{ OPT_Labels,				GameOptionsPage, 	IDS_OPT_Labels,				IDH_POD_Labels, 			true,	true,  true },

#ifdef DEBUG
	{ OPT_InstantMove,		GameOptionsPage, 	IDS_OPT_InstantMove,		IDH_POD_InstantMove, 		false, false, false },
	{ OPT_OrderEnemy,			GameOptionsPage, 	IDS_OPT_OrderEnemy,			IDH_POD_OrderEnemy, 		false, false, false },
	{ OPT_ShowAI,				GameOptionsPage, 	IDS_OPT_ShowAI,				IDH_POD_ShowAI, 			false, false, false },
	{ OPT_QuickMove,			GameOptionsPage, 	IDS_OPT_QuickMove,			IDH_POD_QuickMove, 			false, false, false },
#endif

	{ OPT_GameOption_HowMany }		// Mark end of table
};

/*
 * Campaign-Specific Options
 */

#if 0
#ifdef DEBUG
OptionInfo OptionSettingsInfo::campaignOptionsSettings[] = {
	{ OPT_FogOfWar,				GameOptionsPage, 	IDS_OPT_FogOfWar,			IDH_POD_FogOfWar, 			false,	true,  true },
//	{ OPT_LimitedUnitInfo,		RealismPage,	IDS_OPT_LimitedInfoUnits,	IDH_POD_LimitedUnitInfo, 	false,  true,	true },
//	{ OPT_LimitedTownInfo,		RealismPage,	IDS_OPT_LimitedInfoTowns,	IDH_POD_LimitedTownInfo, 	false,	false,  true },
	{ OPT_InstantOrders,		RealismPage, 	IDS_OPT_InstantOrders, 		IDH_POD_InstantOrders, 		true,	true,	false },
	{ OPT_AggressionChange,		RealismPage, 	IDS_OPT_AgressionChange,	IDH_POD_AggressChange, 		false,	false,  true },
	{ OPT_Supply,				RealismPage, 	IDS_OPT_Supply,				IDH_POD_SupplyRules,		false,	true,   true },
	{ OPT_Attrition,			RealismPage, 	IDS_OPT_Attrition,			IDH_POD_Attrition, 			false,	true,   true },
	{ OPT_NationRules,			RealismPage, 	IDS_OPT_NationRules,		IDH_POD_NationRules, 		false,	false,  true },

	{ OPT_CampaignOption_HowMany }		// Mark end of table
};
#else
OptionInfo OptionSettingsInfo::campaignOptionsSettings[] = {
	{ OPT_FogOfWar,				GameOptionsPage, 	IDS_OPT_FogOfWar,			IDH_POD_FogOfWar, 			false,	true,  true },
//	{ OPT_LimitedUnitInfo,		RealismPage,	IDS_OPT_LimitedInfoUnits,	IDH_POD_LimitedUnitInfo, 	false,	true,	true },
//	{ OPT_LimitedTownInfo,		RealismPage, 	IDS_OPT_LimitedInfoTowns,	IDH_POD_LimitedTownInfo, 	false,	false,  true },
	{ OPT_InstantOrders,		RealismPage, 	IDS_OPT_InstantOrders, 		IDH_POD_InstantOrders, 		true,	true,	false },
	{ OPT_AggressionChange,		RealismPage, 	IDS_OPT_AgressionChange,	IDH_POD_AggressChange, 		false,	false,  true },
	{ OPT_Supply,				RealismPage, 	IDS_OPT_Supply,				IDH_POD_SupplyRules,		false,	true,   true },
	{ OPT_Attrition,			RealismPage, 	IDS_OPT_Attrition,			IDH_POD_Attrition, 			false,	true,   true },
	{ OPT_NationRules,			RealismPage,	IDS_OPT_NationRules,		IDH_POD_NationRules, 		false,	false,  true },
	{ OPT_DeadBodies,			RealismPage, 	IDS_OPT_DeadBodies,			IDH_POD_DeadBodies, 		false,	false,  true },
	{ OPT_FogOfWar,				RealismPage, 	IDS_OPT_FogOfWar,			IDH_POD_FogOfWar, 			false,	false,  true },
	{ OPT_Labels,				RealismPage, 	IDS_OPT_Labels,				IDH_POD_Labels, 			false,	false,  true },

	{ OPT_CampaignOption_HowMany }		// Mark end of table
};
#endif
#else


OptionInfo OptionSettingsInfo::campaignOptionsSettings[] = {
	{ OPT_FogOfWar,			RealismPage, 	IDS_OPT_FogOfWar,			IDH_POD_FogOfWar, 			false,	true,  true },
	{ OPT_InstantOrders,		RealismPage, 	IDS_OPT_InstantOrders, 	IDH_POD_InstantOrders, 		true,	true,	false },
	{ OPT_AggressionChange,	RealismPage, 	IDS_OPT_AgressionChange,IDH_POD_AggressChange, 		false,	false,  true },
	{ OPT_Supply,				RealismPage, 	IDS_OPT_Supply,			IDH_POD_SupplyRules,		false,	true,   true },
	{ OPT_Attrition,			RealismPage, 	IDS_OPT_Attrition,		IDH_POD_Attrition, 			false,	true,   true },
	{ OPT_NationRules,		RealismPage,	IDS_OPT_NationRules,		IDH_POD_NationRules, 		false,	false,  true },
//	{ OPT_DeadBodies,			RealismPage, 	IDS_OPT_DeadBodies,		IDH_POD_DeadBodies, 		false,	false,  true },
//	{ OPT_FogOfWar,			RealismPage, 	IDS_OPT_FogOfWar,			IDH_POD_FogOfWar, 			false,	false,  true },
//	{ OPT_Labels,				RealismPage, 	IDS_OPT_Labels,			IDH_POD_Labels, 			false,	false,  true },

	{ OPT_CampaignOption_HowMany }		// Mark end of table
};
#endif



/*----------------------------------------------------------------
 *  Options Base class
 */


const UWORD OptionBase::s_fileVersion = 0x0005;

OptionBase::OptionBase(size_t size, const char* regName) :
  d_flags(0),
  d_size(size),
  d_regName(regName)
{
  ASSERT(d_size > 0);
  ASSERT(d_regName);

  d_flags = new ULONG[d_size];
  ASSERT(d_flags);

  reset();
}

#define getMask(b)	\
	size_t i = b / 32;	\
	ULONG mask = 1 << (b & 31);

void OptionBase::flip(int b)
{
	getMask(b);
	ASSERT(i < d_size);

	d_flags[i] ^= mask;
	updateRegistry();
}

void OptionBase::set(int b)
{
	getMask(b);
	ASSERT(i < d_size);

	d_flags[i] |= mask;
	updateRegistry();
}

void OptionBase::clear(int b)
{
	getMask(b);
	ASSERT(i < d_size);

	d_flags[i] &= ~mask;
	updateRegistry();
}

bool OptionBase::get(int b)
{
	getMask(b);
	ASSERT(i < d_size);

	return ((d_flags[i] & mask) != 0);
}

void OptionBase::updateRegistry()
{
	bool result = setRegistry(d_regName, d_flags, sizeof(ULONG)*d_size, globRegName);
    if(result)
        result = setRegistry(regVersion, &s_fileVersion, sizeof(s_fileVersion), globRegName);

	ASSERT(result);
}



bool OptionBase::readRegistry()
{

    bool result = getRegistry(d_regName, d_flags, sizeof(ULONG)*d_size, globRegName);
    if(result)
    {
        UWORD version = 0;
        result = getRegistry(regVersion, &version, sizeof(version), globRegName);

        if(!result || (version != s_fileVersion))
            fiddleVersion(version);
//         if(result)
//         {
//             ASSERT(version <= s_fileVersion);
//
//             if(version != s_fileVersion)
//                fiddleVersion(version);
//         }
    }
    return result;
}

void OptionBase::reset()
{
  for(int i = 0; i < d_size; i++)
	 d_flags[i] = 0;
}


bool OptionBase::readData(FileReader& f)
{
	UWORD version;
	f.getUWord(version);
	ASSERT(version <= s_fileVersion);

	UWORD arraySize;
	f.getUWord(arraySize);

	for(int i = 0; i < d_size; i++)
	{
	  if(i < arraySize)
		 f.getULong(d_flags[i]);
	  else
		 d_flags[i] = 0;
	}

    if(version != s_fileVersion)
       fiddleVersion(version);

	updateRegistry();

	return f.isOK();
}

bool OptionBase::writeData(FileWriter& f) const
{
	f.putUWord(s_fileVersion);

	UWORD arraySize = static_cast<UWORD>(d_size);
	f.putUWord(arraySize);

	for(int i = 0; i < arraySize; i++)
		f.putULong(d_flags[i]);

	return f.isOK();
}


/*----------------------------------------------------------
 *  Game Options
 */

class GameOptionsImp : public OptionBase {
  public:
	 GameOptionsImp();
	 ~GameOptionsImp() {}

	 void setDefault();

    private:
        void fiddleVersion(UWORD version);
};

/*
 * static access functions
 */

GameOptionsImp::GameOptionsImp() :
  OptionBase(OptionSize, regOptions)
{
  if(!readRegistry())
  {
	 setDefault();
  }
}

static GameOptionEnum s_defaultGameOptions[] = {
	OPT_ToolTips,
	OPT_AlertBox,
	OPT_TownIcons,
	OPT_TownHilightIcons,
	OPT_NationDisplay,
    OPT_DrawMapScale,
    OPT_TownNames,

	OPT_GameOption_HowMany
};

void GameOptionsImp::setDefault()
{
  reset();

  for(int i = 0; s_defaultGameOptions[i] != OPT_GameOption_HowMany; i++)
	 set(s_defaultGameOptions[i]);
}

/*
 * Update options to match old or changed versions
 */

void GameOptionsImp::fiddleVersion(UWORD version)
{
   /*
    * If version is wrong then reset them all to default values
    */

   setDefault();

#if 0
    if(version < 0x0001)
    {
#ifdef DEBUG
        for(int i = OPT_QuickMove; i >= OPT_InstantMove; --i)
        {
            if(get(i-1))
                set(i);
            else
                clear(i);
        }
#endif
        set(OPT_DrawMapScale);
    }


/*
	if(version < 0x0002) {
#ifdef DEBUG
		for(int i=OPT_QuickMove; i >= OPT_InstantMove; i--) {
            if(get(i-1))
                set(i);
            else
                clear(i);
        }
#endif
	}
*/
#endif


}



static GameOptionsImp s_gameOptions;

void Options::set(GameOptionEnum what, bool how)
{
	ASSERT(what  < OPT_GameOption_HowMany);

	if(how)
		s_gameOptions.set(what);
	else
		s_gameOptions.clear(what);
}

void Options::toggle(GameOptionEnum what)
{
	ASSERT(what  < OPT_GameOption_HowMany);
	s_gameOptions.flip(what);
}

bool Options::get(GameOptionEnum b)
{
	ASSERT(b < OPT_GameOption_HowMany);
	return s_gameOptions.get(b);
}

void Options::setDefault()
{
	s_gameOptions.setDefault();
}

ULONG Options::getBitfield(void) {

	return s_gameOptions.getBitfield();
}

void Options::setBitfield(ULONG flags) {

	s_gameOptions.setBitfield(flags);
}

/*-----------------------------------------------------------
 * Campaign Options
 * These are options that can only be set at the beginning of a game
 * Mainly these are realism settings
 */

class CampaignOptionsImp : public OptionBase {
	 CampaignOptions::SkillLevel d_skillLevel;

  public:
	 CampaignOptionsImp();
	 ~CampaignOptionsImp() {}

	 void setDefault() { setSkillLevel(CampaignOptions::Novice); }

	 void setSkillLevel(CampaignOptions::SkillLevel l);
	 CampaignOptions::SkillLevel getSkillLevel() const { return d_skillLevel; }

  private:
	 void setBeginner();
	 void setIntermediate();
	 void setAdvanced();

};

CampaignOptionsImp::CampaignOptionsImp() :
  OptionBase(CampOptionSize, regCampOptions),
  d_skillLevel(CampaignOptions::Normal)
{
  if(!readRegistry())
  {
	 setDefault();
  }
}

void CampaignOptionsImp::setSkillLevel(CampaignOptions::SkillLevel l)
{
  ASSERT(l < CampaignOptions::SkillLevel_HowMany);

  d_skillLevel = l;

  // set the game options (have to access d_gameOptions class for these)
  for(GameOptionEnum g_opt = OPT_GameOptionFirst; g_opt<OPT_GameOption_HowMany; INCREMENT(g_opt)) {

	  OptionInfo & opt_inf = OptionSettingsInfo::gameOptionsSettings[static_cast<int>(g_opt)];
	  if(d_skillLevel == CampaignOptions::Novice) { if(opt_inf.s_beginner) s_gameOptions.set(g_opt); else s_gameOptions.clear(g_opt); }
	  if(d_skillLevel == CampaignOptions::Normal) { if(opt_inf.s_normal) s_gameOptions.set(g_opt); else s_gameOptions.clear(g_opt); }
	  if(d_skillLevel == CampaignOptions::Expert) { if(opt_inf.s_advanced) s_gameOptions.set(g_opt); else s_gameOptions.clear(g_opt); }
  }

  // set the campaign options (these are members)
  for(CampaignOptionEnum c_opt = OPT_CampaignOptionFirst; c_opt<OPT_CampaignOption_HowMany; INCREMENT(c_opt)) {

	  OptionInfo & opt_inf = OptionSettingsInfo::campaignOptionsSettings[static_cast<int>(c_opt)];
	  if(d_skillLevel == CampaignOptions::Novice) { if(opt_inf.s_beginner) set(c_opt); else clear(c_opt); }
	  if(d_skillLevel == CampaignOptions::Normal) { if(opt_inf.s_normal) set(c_opt); else clear(c_opt); }
	  if(d_skillLevel == CampaignOptions::Expert) { if(opt_inf.s_advanced) set(c_opt); else clear(c_opt); }
  }
}



static CampaignOptionEnum s_beginnerOptions[] = {
  OPT_InstantOrders,

  OPT_CampaignOption_HowMany
};

static CampaignOptionEnum s_intermediateOptions[] = {
  OPT_InstantOrders,
  OPT_Supply,
  OPT_NationRules,

  OPT_CampaignOption_HowMany
};

static CampaignOptionEnum s_advancedOptions[] = {
  OPT_FogOfWar,
//  OPT_LimitedUnitInfo,
//  OPT_LimitedTownInfo,
  OPT_AggressionChange,
  OPT_Supply,
  OPT_Attrition,
  OPT_NationRules,

  OPT_CampaignOption_HowMany
};

void CampaignOptionsImp::setBeginner()
{
  reset();

  for(int i = 0; s_beginnerOptions[i] != OPT_CampaignOption_HowMany; i++)
	 set(s_beginnerOptions[i]);
}

void CampaignOptionsImp::setIntermediate()
{
  reset();

  for(int i = 0; s_intermediateOptions[i] != OPT_CampaignOption_HowMany; i++)
	 set(s_intermediateOptions[i]);
}

void CampaignOptionsImp::setAdvanced()
{
  reset();

  for(int i = 0; s_advancedOptions[i] != OPT_CampaignOption_HowMany; i++)
	 set(s_advancedOptions[i]);
}

static CampaignOptionsImp s_campOptions;

/*
 * static access functions
 */

void CampaignOptions::set(CampaignOptionEnum what, bool how)
{
	ASSERT(what < OPT_CampaignOption_HowMany);

	if(how)
		s_campOptions.set(what);
	else
		s_campOptions.clear(what);
}

void CampaignOptions::toggle(CampaignOptionEnum what)
{
	ASSERT(what < OPT_CampaignOption_HowMany);
	s_campOptions.flip(what);
}

bool CampaignOptions::get(CampaignOptionEnum b)
{
	ASSERT(b < OPT_CampaignOption_HowMany);
	return s_campOptions.get(b);
}

bool CampaignOptions::readData(FileReader& f)
{
	return s_campOptions.readData(f);
}

bool CampaignOptions::writeData(FileWriter& f)
{
	return s_campOptions.writeData(f);
}

void CampaignOptions::setDefault()
{
  s_campOptions.setDefault();
}

void CampaignOptions::setSkillLevel(CampaignOptions::SkillLevel l)
{
  s_campOptions.setSkillLevel(l);
}

CampaignOptions::SkillLevel CampaignOptions::getSkillLevel()
{
  return s_campOptions.getSkillLevel();
}

ULONG CampaignOptions::getBitfield(void) {

	return s_campOptions.getBitfield();
}

void CampaignOptions::setBitfield(ULONG flags) {

	s_campOptions.setBitfield(flags);
}


/*----------------------------------------------------------
 *  Message Options
 */

class MessageOptionsImp : public OptionBase {
  public:
	 MessageOptionsImp();
	 ~MessageOptionsImp() {}

	 void setDefault();
};

MessageOptionsImp::MessageOptionsImp() :
  OptionBase(MsgOptionSize, regMessages)
{
   UWORD msgCount;
   if(!getRegistry(regMessageCount, &msgCount, sizeof(msgCount), globRegName) ||
     (msgCount != CampaignMessages::CMSG_HowMany) ||
     !readRegistry())
   {
      setDefault();
   }
}


void MessageOptionsImp::setDefault()
{
  reset();
  for(CampaignMessages::CMSG_ID id = CampaignMessages::CMSG_First;
		id < CampaignMessages::CMSG_HowMany;
		INCREMENT(id))
  {
	 set(id);
  }

  UWORD msgCount = CampaignMessages::CMSG_HowMany;
  setRegistry(regMessageCount, &msgCount, sizeof(msgCount), globRegName);
}

static MessageOptionsImp s_msgOptions;

/*
 * static access functions
 */

void MessageOptions::set(CampaignMessages::CMSG_ID what, bool how)
{
	ASSERT(what < CampaignMessages::CMSG_HowMany);

	if(how)
		s_msgOptions.set(what);
	else
		s_msgOptions.clear(what);
}

void MessageOptions::toggle(CampaignMessages::CMSG_ID what)
{
	ASSERT(what < CampaignMessages::CMSG_HowMany);
	s_msgOptions.flip(what);
}

bool MessageOptions::get(CampaignMessages::CMSG_ID what)
{
	ASSERT(what < CampaignMessages::CMSG_HowMany);
	return s_msgOptions.get(what);
}

void MessageOptions::setDefault()
{
  s_msgOptions.setDefault();
}

/*----------------------------------------------------------
 *  AlertBox
 */

class AlertBoxOptionsImp : public OptionBase {
  public:
	 AlertBoxOptionsImp();
	 ~AlertBoxOptionsImp() {}
};

AlertBoxOptionsImp::AlertBoxOptionsImp() :
  OptionBase(AlertBoxOptionSize, regAlert)
{
  if(!readRegistry())
  {
   for(AlertBoxEnum i = AB_First; i < AB_HowMany; INCREMENT(i))
      set(i);
  }
}

static AlertBoxOptionsImp s_alertOptions;

/*
 * static access functions
 */

void AlertBoxOptions::set(AlertBoxEnum what, bool how)
{
	ASSERT(what < AB_HowMany);

	if(how)
		s_alertOptions.set(what);
	else
		s_alertOptions.clear(what);
}

void AlertBoxOptions::toggle(AlertBoxEnum what)
{
	ASSERT(what < AB_HowMany);
	s_alertOptions.flip(what);
}

bool AlertBoxOptions::get(AlertBoxEnum b)
{
	ASSERT(b < AB_HowMany);
	return s_alertOptions.get(b);
}



int alertBox(HWND h, LPCTSTR lpText, LPCTSTR lpCaption,	UINT uType, AlertBoxEnum id, int defValue)
{
   if (Options::get(OPT_AlertBox) && AlertBoxOptions::get(id))
   {
      long val = alertBox(h, lpText, lpCaption, uType);
      AlertBoxOptions::set(id, HIWORD(val) != 0);
      return LOWORD(val);
   }
   else
      return defValue;

}
