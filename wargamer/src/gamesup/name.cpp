/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *	Named Item functions
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "name.hpp"
#include "wldfile.hpp"
#include "filebase.hpp"		// FileReader, FileWriter
#include "todolog.hpp"

NamedItem::NamedItem(NamedItem& ni) : CString()
{
	const char* s = ni.getName();
	if(s)
		set(copyString(s));
}

NamedItem& NamedItem::operator = (NamedItem& ni)
{
	const char* s = ni.getName();
	if(s)
		setName(copyString(s));
	else
		setName(0);
	return *this;
}

/*
 * File handling
 */


const UWORD NamedItem::fileVersion = 0x0000;

Boolean NamedItem::readData(FileReader& f)
{
	if(f.isAscii())
	{
#ifdef DEBUG
		todoLog.printf("TODO: NamedItem::readData().ascii unwritten\n");
#endif
		return False;
	}
	else
	{
		UWORD version;
		f.getUWord(version);
		ASSERT(version <= fileVersion);

		setName(f.getString());
	}

	return f.isOK();
}

Boolean NamedItem::writeData(FileWriter& f) const
{
	if(f.isAscii())
	{
		f.printf(fmt_ssn, nameToken, getName());
	}
	else
	{
		f.putUWord(fileVersion);
		f.putString(getName());
	}

	return f.isOK();
}


