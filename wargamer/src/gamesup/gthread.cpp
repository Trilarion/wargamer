/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 * Singleton ThreadManager
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "gthread.hpp"
#include "thread.hpp"

using Greenius_System::ThreadManager;


ThreadManager* GameThreads::s_instance = 0;

ThreadManager* GameThreads::instance()
{
    if(!s_instance)
        s_instance = new ThreadManager;
    return s_instance;
}

void GameThreads::pause(bool pauseMode)
{
    if(pauseMode)
        instance()->pause();
    else
        instance()->unpause();
}

/*
 *----------------------------------------------------------------------
 * $Log$
 *----------------------------------------------------------------------
 */
