/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#include "stdinc.hpp"
#include "wg_rand.hpp"
#include "random.hpp"

#ifdef DEBUG
#include "clog.hpp"
LogFile wg_randLog("wg_rand.log");
#endif


static RandomNumber s_CRandom;

void CRandom::seed(ULONG start)
{
  s_CRandom.seed(start);

#ifdef DEBUG
  wg_randLog.printf("Setting CRandom seed to %i\n", start);
#endif
}

ULONG CRandom::getSeed()
{
  return s_CRandom.getSeed();
}

int CRandom::get(int from, int to)
{
#ifdef DEBUG
  int n = s_CRandom(from, to);
  wg_randLog.printf("CRandom get from %i to %i = %i\n", from, to, n);
  return n;
#else
  return s_CRandom(from, to);
#endif
}

int CRandom::get(int range)
{
#ifdef DEBUG
  int n = s_CRandom.get(range);
  wg_randLog.printf("CRandom get in range %i = %i\n", range, n);
  return n;
#else
  return s_CRandom.get(range);
#endif
}

RandomNumber& CRandom::object()
{
   return s_CRandom;
}





