# Microsoft Developer Studio Generated NMAKE File, Based on gamesup.dsp
!IF "$(CFG)" == ""
CFG=gamesup - Win32 Editor Debug
!MESSAGE No configuration specified. Defaulting to gamesup - Win32 Editor Debug.
!ENDIF 

!IF "$(CFG)" != "gamesup - Win32 Release" && "$(CFG)" != "gamesup - Win32 Debug" && "$(CFG)" != "gamesup - Win32 Editor Debug" && "$(CFG)" != "gamesup - Win32 Editor Release"
!MESSAGE Invalid configuration "$(CFG)" specified.
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "gamesup.mak" CFG="gamesup - Win32 Editor Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "gamesup - Win32 Release" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE "gamesup - Win32 Debug" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE "gamesup - Win32 Editor Debug" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE "gamesup - Win32 Editor Release" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE 
!ERROR An invalid configuration is specified.
!ENDIF 

!IF "$(OS)" == "Windows_NT"
NULL=
!ELSE 
NULL=nul
!ENDIF 

CPP=cl.exe
MTL=midl.exe
RSC=rc.exe

!IF  "$(CFG)" == "gamesup - Win32 Release"

OUTDIR=.\..\..\exe
INTDIR=.\o\release
# Begin Custom Macros
OutDir=.\..\..\exe
# End Custom Macros

!IF "$(RECURSE)" == "0" 

ALL : "$(OUTDIR)\gamesup.dll"

!ELSE 

ALL : "system - Win32 Release" "$(OUTDIR)\gamesup.dll"

!ENDIF 

!IF "$(RECURSE)" == "1" 
CLEAN :"system - Win32 ReleaseCLEAN" 
!ELSE 
CLEAN :
!ENDIF 
	-@erase "$(INTDIR)\control.obj"
	-@erase "$(INTDIR)\gameinfo.obj"
	-@erase "$(INTDIR)\gdespat.obj"
	-@erase "$(INTDIR)\gthread.obj"
	-@erase "$(INTDIR)\multiplayerconnection.obj"
	-@erase "$(INTDIR)\multiplayermsg.obj"
	-@erase "$(INTDIR)\name.obj"
	-@erase "$(INTDIR)\options.obj"
	-@erase "$(INTDIR)\print.obj"
	-@erase "$(INTDIR)\savegame.obj"
	-@erase "$(INTDIR)\scenario.obj"
	-@erase "$(INTDIR)\scn_img.obj"
	-@erase "$(INTDIR)\vc60.idb"
	-@erase "$(INTDIR)\vc60.pdb"
	-@erase "$(INTDIR)\victory.obj"
	-@erase "$(INTDIR)\wg_msg.obj"
	-@erase "$(INTDIR)\wg_rand.obj"
	-@erase "$(INTDIR)\wldfile.obj"
	-@erase "$(OUTDIR)\gamesup.dll"
	-@erase "$(OUTDIR)\gamesup.exp"
	-@erase "$(OUTDIR)\gamesup.lib"
	-@erase "$(OUTDIR)\gamesup.pdb"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

"$(INTDIR)" :
    if not exist "$(INTDIR)/$(NULL)" mkdir "$(INTDIR)"

CPP_PROJ=/nologo /MD /W3 /Gi /GR /GX /Zi /O2 /I "..\h" /I "..\..\res" /I "..\system" /D "NDEBUG" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "EXPORT_GAMESUP_DLL" /Fp"$(INTDIR)\gamesup.pch" /YX"stdinc.hpp" /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /c 
MTL_PROJ=/nologo /D "NDEBUG" /mktyplib203 /win32 
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\gamesup.bsc" 
BSC32_SBRS= \
	
LINK32=link.exe
LINK32_FLAGS=ComDlg32.lib user32.lib /nologo /dll /incremental:no /pdb:"$(OUTDIR)\gamesup.pdb" /debug /machine:I386 /out:"$(OUTDIR)\gamesup.dll" /implib:"$(OUTDIR)\gamesup.lib" 
LINK32_OBJS= \
	"$(INTDIR)\control.obj" \
	"$(INTDIR)\gameinfo.obj" \
	"$(INTDIR)\gdespat.obj" \
	"$(INTDIR)\gthread.obj" \
	"$(INTDIR)\multiplayerconnection.obj" \
	"$(INTDIR)\multiplayermsg.obj" \
	"$(INTDIR)\name.obj" \
	"$(INTDIR)\options.obj" \
	"$(INTDIR)\print.obj" \
	"$(INTDIR)\savegame.obj" \
	"$(INTDIR)\scenario.obj" \
	"$(INTDIR)\scn_img.obj" \
	"$(INTDIR)\victory.obj" \
	"$(INTDIR)\wg_msg.obj" \
	"$(INTDIR)\wg_rand.obj" \
	"$(INTDIR)\wldfile.obj" \
	"$(OUTDIR)\system.lib"

"$(OUTDIR)\gamesup.dll" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

!ELSEIF  "$(CFG)" == "gamesup - Win32 Debug"

OUTDIR=.\..\..\exe
INTDIR=.\o\debug
# Begin Custom Macros
OutDir=.\..\..\exe
# End Custom Macros

!IF "$(RECURSE)" == "0" 

ALL : "$(OUTDIR)\gamesupDB.dll"

!ELSE 

ALL : "system - Win32 Debug" "$(OUTDIR)\gamesupDB.dll"

!ENDIF 

!IF "$(RECURSE)" == "1" 
CLEAN :"system - Win32 DebugCLEAN" 
!ELSE 
CLEAN :
!ENDIF 
	-@erase "$(INTDIR)\control.obj"
	-@erase "$(INTDIR)\gameinfo.obj"
	-@erase "$(INTDIR)\gdespat.obj"
	-@erase "$(INTDIR)\gthread.obj"
	-@erase "$(INTDIR)\multiplayerconnection.obj"
	-@erase "$(INTDIR)\multiplayermsg.obj"
	-@erase "$(INTDIR)\name.obj"
	-@erase "$(INTDIR)\options.obj"
	-@erase "$(INTDIR)\print.obj"
	-@erase "$(INTDIR)\savegame.obj"
	-@erase "$(INTDIR)\scenario.obj"
	-@erase "$(INTDIR)\scn_img.obj"
	-@erase "$(INTDIR)\vc60.idb"
	-@erase "$(INTDIR)\vc60.pdb"
	-@erase "$(INTDIR)\victory.obj"
	-@erase "$(INTDIR)\wg_msg.obj"
	-@erase "$(INTDIR)\wg_rand.obj"
	-@erase "$(INTDIR)\wldfile.obj"
	-@erase "$(OUTDIR)\gamesupDB.dll"
	-@erase "$(OUTDIR)\gamesupDB.exp"
	-@erase "$(OUTDIR)\gamesupDB.ilk"
	-@erase "$(OUTDIR)\gamesupDB.lib"
	-@erase "$(OUTDIR)\gamesupDB.pdb"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

"$(INTDIR)" :
    if not exist "$(INTDIR)/$(NULL)" mkdir "$(INTDIR)"

CPP_PROJ=/nologo /MDd /W3 /Gm /Gi /GR /GX /ZI /Od /I "..\h" /I "..\..\res" /I "..\system" /D "_DEBUG" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "EXPORT_GAMESUP_DLL" /Fp"$(INTDIR)\gamesup.pch" /YX"stdinc.hpp" /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /GZ /c 
MTL_PROJ=/nologo /D "_DEBUG" /mktyplib203 /win32 
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\gamesup.bsc" 
BSC32_SBRS= \
	
LINK32=link.exe
LINK32_FLAGS=ComDlg32.lib user32.lib /nologo /dll /incremental:yes /pdb:"$(OUTDIR)\gamesupDB.pdb" /debug /machine:I386 /out:"$(OUTDIR)\gamesupDB.dll" /implib:"$(OUTDIR)\gamesupDB.lib" /pdbtype:sept 
LINK32_OBJS= \
	"$(INTDIR)\control.obj" \
	"$(INTDIR)\gameinfo.obj" \
	"$(INTDIR)\gdespat.obj" \
	"$(INTDIR)\gthread.obj" \
	"$(INTDIR)\multiplayerconnection.obj" \
	"$(INTDIR)\multiplayermsg.obj" \
	"$(INTDIR)\name.obj" \
	"$(INTDIR)\options.obj" \
	"$(INTDIR)\print.obj" \
	"$(INTDIR)\savegame.obj" \
	"$(INTDIR)\scenario.obj" \
	"$(INTDIR)\scn_img.obj" \
	"$(INTDIR)\victory.obj" \
	"$(INTDIR)\wg_msg.obj" \
	"$(INTDIR)\wg_rand.obj" \
	"$(INTDIR)\wldfile.obj" \
	"$(OUTDIR)\systemDB.lib"

"$(OUTDIR)\gamesupDB.dll" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

!ELSEIF  "$(CFG)" == "gamesup - Win32 Editor Debug"

OUTDIR=.\..\..\exe
INTDIR=.\o\debug
# Begin Custom Macros
OutDir=.\..\..\exe
# End Custom Macros

!IF "$(RECURSE)" == "0" 

ALL : "$(OUTDIR)\gamesupDB.dll"

!ELSE 

ALL : "system - Win32 Editor Debug" "$(OUTDIR)\gamesupDB.dll"

!ENDIF 

!IF "$(RECURSE)" == "1" 
CLEAN :"system - Win32 Editor DebugCLEAN" 
!ELSE 
CLEAN :
!ENDIF 
	-@erase "$(INTDIR)\control.obj"
	-@erase "$(INTDIR)\gameinfo.obj"
	-@erase "$(INTDIR)\gdespat.obj"
	-@erase "$(INTDIR)\gthread.obj"
	-@erase "$(INTDIR)\multiplayerconnection.obj"
	-@erase "$(INTDIR)\multiplayermsg.obj"
	-@erase "$(INTDIR)\name.obj"
	-@erase "$(INTDIR)\options.obj"
	-@erase "$(INTDIR)\print.obj"
	-@erase "$(INTDIR)\savegame.obj"
	-@erase "$(INTDIR)\scenario.obj"
	-@erase "$(INTDIR)\scn_img.obj"
	-@erase "$(INTDIR)\vc60.idb"
	-@erase "$(INTDIR)\vc60.pdb"
	-@erase "$(INTDIR)\victory.obj"
	-@erase "$(INTDIR)\wg_msg.obj"
	-@erase "$(INTDIR)\wg_rand.obj"
	-@erase "$(INTDIR)\wldfile.obj"
	-@erase "$(OUTDIR)\gamesupDB.dll"
	-@erase "$(OUTDIR)\gamesupDB.exp"
	-@erase "$(OUTDIR)\gamesupDB.ilk"
	-@erase "$(OUTDIR)\gamesupDB.lib"
	-@erase "$(OUTDIR)\gamesupDB.pdb"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

"$(INTDIR)" :
    if not exist "$(INTDIR)/$(NULL)" mkdir "$(INTDIR)"

CPP_PROJ=/nologo /MDd /W3 /Gm /Gi /GR /GX /ZI /Od /I "..\h" /I "..\..\res" /I "..\system" /D "_USRDLL" /D "EXPORT_GAMESUP_DLL" /D "_DEBUG" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /D "EDITOR" /Fp"$(INTDIR)\gamesup.pch" /YX"stdinc.hpp" /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /GZ /c 
MTL_PROJ=/nologo /D "_DEBUG" /mktyplib203 /win32 
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\gamesup.bsc" 
BSC32_SBRS= \
	
LINK32=link.exe
LINK32_FLAGS=ComDlg32.lib user32.lib /nologo /dll /incremental:yes /pdb:"$(OUTDIR)\gamesupDB.pdb" /debug /machine:I386 /out:"$(OUTDIR)\gamesupDB.dll" /implib:"$(OUTDIR)\gamesupDB.lib" /pdbtype:sept 
LINK32_OBJS= \
	"$(INTDIR)\control.obj" \
	"$(INTDIR)\gameinfo.obj" \
	"$(INTDIR)\gdespat.obj" \
	"$(INTDIR)\gthread.obj" \
	"$(INTDIR)\multiplayerconnection.obj" \
	"$(INTDIR)\multiplayermsg.obj" \
	"$(INTDIR)\name.obj" \
	"$(INTDIR)\options.obj" \
	"$(INTDIR)\print.obj" \
	"$(INTDIR)\savegame.obj" \
	"$(INTDIR)\scenario.obj" \
	"$(INTDIR)\scn_img.obj" \
	"$(INTDIR)\victory.obj" \
	"$(INTDIR)\wg_msg.obj" \
	"$(INTDIR)\wg_rand.obj" \
	"$(INTDIR)\wldfile.obj" \
	"$(OUTDIR)\systemDB.lib"

"$(OUTDIR)\gamesupDB.dll" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

!ELSEIF  "$(CFG)" == "gamesup - Win32 Editor Release"

OUTDIR=.\..\..\exe
INTDIR=.\o\release
# Begin Custom Macros
OutDir=.\..\..\exe
# End Custom Macros

!IF "$(RECURSE)" == "0" 

ALL : "$(OUTDIR)\gamesup.dll"

!ELSE 

ALL : "system - Win32 Editor Release" "$(OUTDIR)\gamesup.dll"

!ENDIF 

!IF "$(RECURSE)" == "1" 
CLEAN :"system - Win32 Editor ReleaseCLEAN" 
!ELSE 
CLEAN :
!ENDIF 
	-@erase "$(INTDIR)\control.obj"
	-@erase "$(INTDIR)\gameinfo.obj"
	-@erase "$(INTDIR)\gdespat.obj"
	-@erase "$(INTDIR)\gthread.obj"
	-@erase "$(INTDIR)\multiplayerconnection.obj"
	-@erase "$(INTDIR)\multiplayermsg.obj"
	-@erase "$(INTDIR)\name.obj"
	-@erase "$(INTDIR)\options.obj"
	-@erase "$(INTDIR)\print.obj"
	-@erase "$(INTDIR)\savegame.obj"
	-@erase "$(INTDIR)\scenario.obj"
	-@erase "$(INTDIR)\scn_img.obj"
	-@erase "$(INTDIR)\vc60.idb"
	-@erase "$(INTDIR)\victory.obj"
	-@erase "$(INTDIR)\wg_msg.obj"
	-@erase "$(INTDIR)\wg_rand.obj"
	-@erase "$(INTDIR)\wldfile.obj"
	-@erase "$(OUTDIR)\gamesup.dll"
	-@erase "$(OUTDIR)\gamesup.exp"
	-@erase "$(OUTDIR)\gamesup.lib"
	-@erase "$(OUTDIR)\gamesup.pdb"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

"$(INTDIR)" :
    if not exist "$(INTDIR)/$(NULL)" mkdir "$(INTDIR)"

CPP_PROJ=/nologo /MD /W3 /Gi /GR /GX /Zd /O2 /I "..\h" /I "..\..\res" /I "..\system" /D "NDEBUG" /D "_USRDLL" /D "EXPORT_GAMESUP_DLL" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /D "EDITOR" /Fp"$(INTDIR)\gamesup.pch" /YX"stdinc.hpp" /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /c 
MTL_PROJ=/nologo /D "NDEBUG" /mktyplib203 /win32 
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\gamesup.bsc" 
BSC32_SBRS= \
	
LINK32=link.exe
LINK32_FLAGS=ComDlg32.lib user32.lib /nologo /dll /incremental:no /pdb:"$(OUTDIR)\gamesup.pdb" /debug /machine:I386 /out:"$(OUTDIR)\gamesup.dll" /implib:"$(OUTDIR)\gamesup.lib" 
LINK32_OBJS= \
	"$(INTDIR)\control.obj" \
	"$(INTDIR)\gameinfo.obj" \
	"$(INTDIR)\gdespat.obj" \
	"$(INTDIR)\gthread.obj" \
	"$(INTDIR)\multiplayerconnection.obj" \
	"$(INTDIR)\multiplayermsg.obj" \
	"$(INTDIR)\name.obj" \
	"$(INTDIR)\options.obj" \
	"$(INTDIR)\print.obj" \
	"$(INTDIR)\savegame.obj" \
	"$(INTDIR)\scenario.obj" \
	"$(INTDIR)\scn_img.obj" \
	"$(INTDIR)\victory.obj" \
	"$(INTDIR)\wg_msg.obj" \
	"$(INTDIR)\wg_rand.obj" \
	"$(INTDIR)\wldfile.obj" \
	"$(OUTDIR)\system.lib"

"$(OUTDIR)\gamesup.dll" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

!ENDIF 

.c{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.c{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<


!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("gamesup.dep")
!INCLUDE "gamesup.dep"
!ELSE 
!MESSAGE Warning: cannot find "gamesup.dep"
!ENDIF 
!ENDIF 


!IF "$(CFG)" == "gamesup - Win32 Release" || "$(CFG)" == "gamesup - Win32 Debug" || "$(CFG)" == "gamesup - Win32 Editor Debug" || "$(CFG)" == "gamesup - Win32 Editor Release"
SOURCE=.\control.cpp

"$(INTDIR)\control.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\gameinfo.cpp

"$(INTDIR)\gameinfo.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\gdespat.cpp

"$(INTDIR)\gdespat.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\gthread.cpp

"$(INTDIR)\gthread.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\multiplayerconnection.cpp

"$(INTDIR)\multiplayerconnection.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\multiplayermsg.cpp

"$(INTDIR)\multiplayermsg.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\name.cpp

"$(INTDIR)\name.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\options.cpp

"$(INTDIR)\options.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\print.cpp

"$(INTDIR)\print.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\savegame.cpp

"$(INTDIR)\savegame.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\scenario.cpp

"$(INTDIR)\scenario.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\scn_img.cpp

"$(INTDIR)\scn_img.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\victory.cpp

"$(INTDIR)\victory.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\wg_msg.cpp

"$(INTDIR)\wg_msg.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\wg_rand.cpp

"$(INTDIR)\wg_rand.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\wldfile.cpp

"$(INTDIR)\wldfile.obj" : $(SOURCE) "$(INTDIR)"


!IF  "$(CFG)" == "gamesup - Win32 Release"

"system - Win32 Release" : 
   cd "\src\sf\wargamer\SRC\SYSTEM"
   $(MAKE) /$(MAKEFLAGS) /F .\system.mak CFG="system - Win32 Release" 
   cd "..\GAMESUP"

"system - Win32 ReleaseCLEAN" : 
   cd "\src\sf\wargamer\SRC\SYSTEM"
   $(MAKE) /$(MAKEFLAGS) /F .\system.mak CFG="system - Win32 Release" RECURSE=1 CLEAN 
   cd "..\GAMESUP"

!ELSEIF  "$(CFG)" == "gamesup - Win32 Debug"

"system - Win32 Debug" : 
   cd "\src\sf\wargamer\SRC\SYSTEM"
   $(MAKE) /$(MAKEFLAGS) /F .\system.mak CFG="system - Win32 Debug" 
   cd "..\GAMESUP"

"system - Win32 DebugCLEAN" : 
   cd "\src\sf\wargamer\SRC\SYSTEM"
   $(MAKE) /$(MAKEFLAGS) /F .\system.mak CFG="system - Win32 Debug" RECURSE=1 CLEAN 
   cd "..\GAMESUP"

!ELSEIF  "$(CFG)" == "gamesup - Win32 Editor Debug"

"system - Win32 Editor Debug" : 
   cd "\src\sf\wargamer\SRC\SYSTEM"
   $(MAKE) /$(MAKEFLAGS) /F .\system.mak CFG="system - Win32 Editor Debug" 
   cd "..\GAMESUP"

"system - Win32 Editor DebugCLEAN" : 
   cd "\src\sf\wargamer\SRC\SYSTEM"
   $(MAKE) /$(MAKEFLAGS) /F .\system.mak CFG="system - Win32 Editor Debug" RECURSE=1 CLEAN 
   cd "..\GAMESUP"

!ELSEIF  "$(CFG)" == "gamesup - Win32 Editor Release"

"system - Win32 Editor Release" : 
   cd "\src\sf\wargamer\SRC\SYSTEM"
   $(MAKE) /$(MAKEFLAGS) /F .\system.mak CFG="system - Win32 Editor Release" 
   cd "..\GAMESUP"

"system - Win32 Editor ReleaseCLEAN" : 
   cd "\src\sf\wargamer\SRC\SYSTEM"
   $(MAKE) /$(MAKEFLAGS) /F .\system.mak CFG="system - Win32 Editor Release" RECURSE=1 CLEAN 
   cd "..\GAMESUP"

!ENDIF 


!ENDIF 

