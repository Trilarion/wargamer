/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef DPLYUSER_HPP
#define DPLYUSER_HPP

#include "batinfo.hpp"

/*------------------------------------------------------------------------------------------------

        Base class to notify battle game of deployment phase

--------------------------------------------------------------------------------------------------*/



class DeploymentUser {

public:

    virtual ~DeploymentUser(void) = 0;
    // virtual void startDeployment(BattleInfo * battle_info) =0;
    virtual void endDeployment() = 0;
    virtual void deployUnits(Side s) = 0;

};

inline DeploymentUser::~DeploymentUser(void) { }







class WindowChangeUser {

public:

    virtual ~WindowChangeUser(void)= 0;
    virtual void NextScreen(void) = 0;
    virtual void LastScreen(void) = 0;

};

inline WindowChangeUser::~WindowChangeUser(void) { }






#endif

