/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef CMSG_ID_HPP
#define CMSG_ID_HPP

#ifndef __cplusplus
#error cmsg_id.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *	IDs of Campaign Messages
 *
 * Seperated from campmsg.hpp so can be included by options.hpp and
 * anything else that does not need to know about CampaignMessageInfo
 *
 *----------------------------------------------------------------------
 */

#include "gamesup.h"

class SimpleString;

namespace CampaignMessages
{

 	/*
	 * The table in campmsg.cpp must also be updated
	 * whenever this table is updated
	 */

	enum CMSG_ID
	{
		CMSG_First = 0,

		ArrivedAtTown = CMSG_First,
		NewUnitBuilt,
		MetAtAndAwait,
		TakenCommand,
		MetAndAwait,
		Stranded,
		MovingInForBattle,
		HoldingBackAt8,
		AvoidingContact,
		PreparingForBattle,
		JoiningBattle,
		EngagingInBattle,
		GeneralKilled,
		UnitDestroyed,
		CannotCommand,
		NotNoble,
		TownTaken,
		TownLost,
		HearBattle,
		EncounteredEnemy,
		ChangedAggression,
		MortarShellExploded,
		PoorConstruction,
		BreachedFort,
		FortBreached,
		TownSurrender,
		FortDestroyed,
		BrokenSiege,
		SiegeEnded,
		StartSiege,
		SiegeStarted,
		AttemptedSiege,
		SiegeAttempted,
		RaidingTown,
		TownRaided,
		NotInstallSupply,
		NotInstallSupplyDead,
		SupplyDepotInstalled,
		NotInstallFort,
		NotInstallFortDead,
		FortUpgraded,
		EngineerNotFieldWork,
		FieldWorkUpgraded,
		FieldWorkComplete,
		BridgeCollapse,
		TownFire,
		LeaderSick,
		LeaderRecovered,
		AlliedDisillusion,
		SHQInChaos,
		EnemyPlansCaptured,
		SquabblingLeaders,
		PopularEnthusiasm,
      ArmisticeStart,
      ArmisticeEnd,

		CMSG_HowMany,
		NoMessage = -1

	};

	enum MessageGroup {
		Group0,
		Group1,
		Group2,
		Group3,

		Group_HowMany
	};


};		// namespace CampaignMessages

#endif /* CMSG_ID_HPP */

