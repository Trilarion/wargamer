/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef WG_MSG_HPP
#define WG_MSG_HPP

#ifndef __cplusplus
#error wg_msg.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *	Private Messages to ourself
 *
 *----------------------------------------------------------------------
 */

#include "gamesup.h"
#include "app.hpp"
#include "StringPtr.hpp"
// #include "sync.hpp"
#include "savegame.hpp"
#include "threadMsg.hpp"
#include "myassert.hpp"

class WargameApp;

namespace WargameMessage
{
	enum MessageType
	{
		// WGM_Message = WM_USER,
		WGM_Load = WM_USER + 1,
		WGM_Abandon,
		// WGM_FinishBattle
	};

	inline BOOL postMessage(MessageType id, LPARAM msg)
	{
		LONG result = PostThreadMessage(APP::getInterfaceThreadID(), id, 0, msg);
		ASSERT(result != 0);
		return (result != 0);
	}

	class LoadGame
	{
		public:
			// enum Format { Campaign, Battle, SaveGame, None=-1 };
			typedef SaveGame::FileFormat Format;

		public:
			LoadGame() : d_format() { }
			~LoadGame() { }


			static BOOL post(const char* fname, Format format)
			{
                LoadGame* msg = getStaticInstance(fname, format);
				return postMessage(WGM_Load, reinterpret_cast<LPARAM>(msg));
			}

			const char* fileName() const { return d_fileName; }
			Format fileFormat() const { return d_format; }

		private:
			GAMESUP_DLL_EXPORT static LoadGame* getStaticInstance(const char* fname, Format format);

			CString d_fileName;
			Format d_format;
	};


	inline BOOL postLoadGame(const char* fileName = 0)
	{
		return LoadGame::post(fileName, SaveGame::SaveGame);
	}

	inline BOOL postNewCampaign(const char* fileName = 0)
	{
		return LoadGame::post(fileName, SaveGame::Campaign);
	}

	inline BOOL postNewBattle(const char* fileName = 0)
	{
		return LoadGame::post(fileName, SaveGame::Battle);
	}

	inline BOOL postNewGame(const char* fileName, LoadGame::Format format)
	{
		return LoadGame::post(fileName, format);
	}

	class Abandon
	{
		public:
            static BOOL post()
            {
        		return postMessage(WGM_Abandon, 0);
        		// return postMessage(WGM_Abandon, reinterpret_cast<LPARAM>(Abandon::getStaticInstance()));
            }

        private:
			// static Abandon* getStaticInstance();
			Abandon() { }
			~Abandon() { }

            // static Abandon s_instance;
	};

	inline BOOL postAbandon()
	{
		return Abandon::post();
	}


#if 0    // Old version
  	class MessageBase
	{
		public:
			MessageBase() { }
			virtual ~MessageBase() { }

			virtual void run() = 0;
			virtual void clear() { delete this; }
  	};

	class GAMESUP_DLL_EXPORT WaitableMessage : public MessageBase
	{
		protected:
			Event d_event;						// for caller to wait for
		public:
#ifdef DEBUG
			int wait() { return d_event.wait(100000); }
#else
			int wait() { return d_event.wait(); }
#endif
            bool send() { return postMessage(this); }
            void signal() { d_event.set(); }

            SyncObject& syncObject() { return d_event; }

			virtual void clear() { }
	};

	inline BOOL postMessage(MessageBase* msg)
	{
		return postMessage(WGM_Message, reinterpret_cast<LPARAM>(msg));
	}
#endif



};	// namespace WargameMessage


#endif /* WG_MSG_HPP */

