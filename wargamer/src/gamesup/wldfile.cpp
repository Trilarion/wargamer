/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1995, Steven Morle-Green, All Rights Reserved
 *----------------------------------------------------------------------
 *
 *	Read and Write Scenario and World Data
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "wldfile.hpp"
#include "myassert.hpp"
#include <string.h>
// #include "filecnk.hpp"
// #include "filename.hpp"

// #include "campimp.hpp"
// #include "campdimp.hpp"
// #include "town.hpp"
// #include "camppos.hpp"

// #include "scenario.hpp"
// #include "idstr.hpp"
#ifdef DEBUG
// #include "todolog.hpp"
#endif


/*
 * Chunk Names
 */

const char campaignChunkName[]			= "Campaign";
const char townChunkName[]				= "Locations";
const char connectionChunkName[]		= "Connections";
const char provinceChunkName[]			= "Provinces";
const char armyChunkName[]				= "Army";
const char nationChunkName[]			= "Nations";
const char campaignCPChunkName[]		= "CampaignUnits";
const char commandChunkName[]			= "Units";
const char leaderChunkName[]			= "Leaders";
const char spChunkName[]				= "StrengthPoints";
const char iLeaderChunkName[]			= "IndependantLeader";
const char conditionsChunkName[] 		= "Conditions";
const char allegianceChunkName[] 		= "Allegiance";
const char weatherChunkName[] 			= "Weather";
const char victoryLevelChunkName[] 		= "VictoryLevel";
const char totalLossesChunkName[] 		= "TotalLosses";
const char sideRandomEventsChunkName[]  = "SideRandomEvents";
const char campaignTacticalChunkName[]  = "CampaignTactical";

/*
 * Keywords
 */

const char scenarioToken[] 			= "Scenario";
const char entriesToken[] 			= "Number";
const char nameToken[] 				= "Name";
const char sideToken[] 				= "Side";
const char locationToken[] 			= "Location";
const char connectionToken[] 		= "Connections";
const char provinceToken[] 			= "Province";
const char nameAlignToken[] 			= "nameAlign";
const char sizeToken[] 				= "size";
const char victoryToken[] 			= "victory";
const char politicalToken[] 			= "political";
const char manPowerToken[] 			= "manPower";
const char resourceToken[] 			= "resource";
const char supplyToken[] 				= "supplyRate";
const char forageToken[] 				= "forage";
const char stackingToken[] 			= "stacking";
const char terrainToken[] 			= "terrain";
const char flagsToken[] 				= "flags";
const char fortificationsToken[] 	= "fortifications";
const char capitalToken[]				= "Capital";
const char abrevToken[]				= "Abrev";
const char nodeToken[]					= "Nodes";
const char howToken[]					= "How";
const char qualityToken[]				= "Quality";
const char capacityToken[]			= "Capacity";
const char townsToken[]				= "Towns";
const char presidentToken[]			= "President";
const char controlToken[]				= "Controller";
const char rankCountToken[]			= "RankCount";
const char unusedToken[]				= "Unused";
const char leaderToken[]				= "Leader";
const char moraleToken[]				= "Morale";
const char fatigueToken[]				= "Fatigue";
const char initiativeToken[]			= "Initiative";
const char staffToken[]				= "Staff";
const char subordinationToken[]		= "Subordination";
const char aggressionToken[]			= "Aggression";
const char charismaToken[]			= "Charisma";
const char tacticalToken[]			= "Tactical";
const char commandRadiusToken[]		= "CommandRadius";
const char rankRatingToken[]			= "RankRating";
const char healthToken[]				= "Health";
const char nationToken[]           = "Nation";
const char commandToken[]				= "Command";
const char positionToken[]			= "Position";
const char rankToken[]					= "Rank";
const char linkToken[]					= "Link";
const char nextToken[]					= "Next";
const char strengthPointToken[]		= "SP";
const char offScreenToken[]        = "OffScreen";
const char distanceToken[]         = "Distance";

const char startItemString[] = "{\r\n";
const char endItemString[] = "}\r\n";
const char eolString[] = "\r\n";

const char fmt_sddddn[] = "%s %d %d %d %d\r\n";
const char fmt_sdddn[] = "%s %d %d %d\r\n";
const char fmt_sddn[] = "%s %d %d\r\n";
const char fmt_sdn[] = "%s %d\r\n";
const char fmt_ssn[] = "%s \"%s\"\r\n";
const char fmt_slln[] = "%s %ld %ld\r\n";
const char fmt_sln[] = "%s %ld\r\n";
const char fmt_sn[] = "%s\r\n";


/*
 * Make a filename with an extension
 */

void addExtension(char* buf, const char* src, const char* ext)
{
	strcpy(buf, src);

	char* s = strrchr(buf, '.');
	if(!s)
		s = buf + strlen(buf);

	*s++ = '.';
	strcpy(s, ext);

#ifdef DEBUG
	debugLog("addExtension(%s, %s) -> %s\n", src, ext, buf);
#endif
}

/*------------------------------------------------------------------
 * NamedItem
 */


/*------------------------------------------------------------------
 * CampaignPosition
 */

