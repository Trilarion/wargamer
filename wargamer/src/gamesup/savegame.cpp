/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *	Load and Save Game Utility
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "savegame.hpp"
#include "filename.hpp"
// #include "wg_msg.hpp"
// #include "gamectrl.hpp"
// #include "campint.hpp"
#include "misc.hpp"
#include "app.hpp"
#include "registry.hpp"
#include "scenario.hpp"
#include "wmisc.hpp"
#include "simpstr.hpp"
#include "res_str.h"
#include "filecnk.hpp"
#include "winfile.hpp"
#include "filebase.hpp"
#include <windows.h>
#include <commdlg.h>


OpenFileDialog::OpenFileDialog() 
{ 
   d_fileName[0] = 0; 
   d_nameNoPath[0] = 0; 
}

const char* OpenFileDialog::copyFileName() const
{ 
   return copyString(d_fileName); 
}

const char* OpenFileDialog::fileName() const 
{
   return d_fileName; 
}

const char* OpenFileDialog::fileNameNoPath() const 
{
   return d_nameNoPath; 
}

const char* OpenFileDialog::copyFileNameNoPath() const
{
   return copyString(d_nameNoPath); 
}
bool OpenFileDialog::ask(const char* title, const char* filter, const char* defName, const char* defDir)
{
	OPENFILENAME info;

	if(defName != 0)
		strcpy(d_fileName, defName);

	info.lStructSize = sizeof(info);
	info.hwndOwner = APP::getMainHWND();	// NULL;		// globWin.mainWind->getHWND();
	info.hInstance = 0; 							// Ignored
	info.lpstrFilter = filter;
	info.lpstrCustomFilter = NULL; 				// Unused
	info.nMaxCustFilter = 0; 						// Unused
	info.nFilterIndex = 0; 						// Not sure about this
	info.lpstrFile = d_fileName;
	info.nMaxFile = MAX_PATH;
	info.lpstrFileTitle = d_nameNoPath; //NULL; 					// ?
	info.nMaxFileTitle = MAX_PATH; //0;
	info.lpstrInitialDir = defDir;
	info.lpstrTitle = title;	// "Load World";
	info.Flags =
			OFN_EXPLORER |
			OFN_FILEMUSTEXIST |
			OFN_HIDEREADONLY |
			OFN_LONGNAMES |
			OFN_NOCHANGEDIR |
			// OFN_NONETWORKBUTTON |
			// OFN_OVERWRITEPROMPT |
			OFN_PATHMUSTEXIST |
			OFN_SHOWHELP;
	info.nFileOffset = 0;
	info.nFileExtension = 0;
	info.lpstrDefExt = NULL;
	info.lCustData = NULL;
	info.lpfnHook = NULL;
	info.lpTemplateName = NULL;

	return (GetOpenFileName(&info) != 0);
}


SaveFileDialog::SaveFileDialog() 
{
   d_fileName[0] = 0; 
}

const char* SaveFileDialog::fileName() const 
{ 
   return d_fileName; 
}

void SaveFileDialog::addExtension(const char* ext)
{
	char* s = strrchr(d_fileName, '.');
	if(!s)
	{
		s = d_fileName + strlen(d_fileName);
		*s++ = '.';
		strcpy(s, ext);
	}
}

bool SaveFileDialog::ask(const char* title, const char* filter, const char* defName, const char* defDir)
{
	OPENFILENAME info;

	if(defName != 0)
		strcpy(d_fileName, defName);

	/*
	 * Make sure destination folder exists
	 */

	if(defDir != 0)
	{
		if(!FileSystem::dirExists(defDir))
			if(CreateDirectory(defDir, NULL) == 0)
				defDir = NULL;
	}


	info.lStructSize = sizeof(info);
	info.hwndOwner = APP::getMainHWND();	// NULL;		// globWin.mainWind->getHWND();
	info.hInstance = 0; 							// Ignored
	info.lpstrFilter = filter;
	info.lpstrCustomFilter = NULL; 				// Unused
	info.nMaxCustFilter = 0; 						// Unused
	info.nFilterIndex = 0; 						// Not sure about this
	info.lpstrFile = d_fileName;
	info.nMaxFile = MAX_PATH;
	info.lpstrFileTitle = NULL; 					// ?
	info.nMaxFileTitle = 0;
	info.lpstrInitialDir = defDir;
	info.lpstrTitle = title;	// "Save World";
	info.Flags =
			OFN_EXPLORER |
			// OFN_FILEMUSTEXIST |
			OFN_HIDEREADONLY |
			OFN_LONGNAMES |
			OFN_NOCHANGEDIR |
			// OFN_NONETWORKBUTTON |
			OFN_OVERWRITEPROMPT |
			OFN_PATHMUSTEXIST |
			OFN_SHOWHELP;
	info.nFileOffset = 0;
	info.nFileExtension = 0;
	info.lpstrDefExt = NULL;
	info.lCustData = NULL;
	info.lpfnHook = NULL;
	info.lpTemplateName = NULL;

	return (GetSaveFileName(&info) != 0);
}






namespace SaveGame {

const char registryName[] = "\\SavedGames";


// static const char worldFilter[] = "Binary\0*." CAMPAIGN_EXT "\0Ascii\0*.asc\0";
// static const char saveFilter[] = "Saved Game\0*." SAVE_EXT "\0";

static const char defWorldName[] = "campaign." CAMPAIGN_EXT;
static const char* defBattleName = 0;
static const char* defSaveName = 0;
static const char* saveFolder = "SavedGames";

// static StringPtr s_defFolder[3];
static StringPtr s_defFile[3];


static void getFilter(SimpleString& filter, FileFormat mode)
{
	switch(mode)
	{
		case Campaign:
			idsToString(filter, IDS_SAVE_SCENARIO_BIN);
			filter += '\0';
			filter += "*." CAMPAIGN_EXT;
			filter += '\0';
			idsToString(filter, IDS_SAVE_SCENARIO_ASC);
			filter += '\0';
			filter += "*.asc";
			filter += '\0';
			filter += '\0';
			break;
		case Battle:
			idsToString(filter, IDS_Battle);
			filter += '\0';
			filter += "*." BATTLE_EXT;
			filter += '\0';
			filter += '\0';
			break;
		case SaveGame:
			idsToString(filter, IDS_SAVE_SAVEDGAME);
			filter += '\0';
			filter += "*." SAVE_EXT;
			filter += '\0';
			filter += '\0';
			break;

		default:
			FORCEASSERT("Unknown File Format");
			break;
	}
}

static const char* getFilter(FileFormat mode)
{
	switch(mode)
	{
		case Campaign:
			return CAMPAIGN_EXT;
		case Battle:
			return BATTLE_EXT;
		case SaveGame:
			return SAVE_EXT;
		default:
			FORCEASSERT("Unknown File Format");
			break;
	}
	return 0;
}

void setDefFile(FileFormat mode, const char* fName)
{
//   s_defFolder[mode] = 0;
   s_defFile[mode] = fName;
}

const char* getDefFolder(FileFormat mode)
{
   if(s_defFile[mode].toStr() == 0)
   {
	   switch(mode)
	   {
		   case Campaign:
		   case Battle:
			   if(scenario != 0)
				   return scenario->getFolderName();
			   else
				   return ".";

		   case SaveGame:
			   return saveFolder;

		   default:
			   FORCEASSERT("Unknown File Format");
            return 0;
	   }
   }
//   else
//      return 0;      // s_defFolder[mode];

	return 0;
}

static const char* getDefName(FileFormat mode)
{
   if (s_defFile[mode].toStr() == 0)
   {
	   switch(mode)
	   {
		   case Campaign:
			   return defWorldName;
		   case Battle:
			   return defBattleName;
		   case SaveGame:
			   return defSaveName;

		   default:
			   FORCEASSERT("Unknown File Format");
            return 0;
	   }
   }
   else
      return s_defFile[mode];
//	return 0;
}


bool askLoadName(SimpleString& s, FileFormat mode)
{
	OpenFileDialog dial;

	UINT titleID;

	switch(mode)
	{
		case Campaign:
			titleID = IDS_SAVE_LOADWORLD;
			break;
		case Battle:
			titleID = IDS_SAVE_LOADWORLD;	// wrong... should be battle
			break;
		case SaveGame:
			titleID = IDS_SAVE_LOADGAME;
			break;

		default:
			titleID = IDS_SAVE_LOADWORLD;
			FORCEASSERT("Unknown File Format");
			break;
	}

	ResString title(titleID);

	SimpleString filter;
	getFilter(filter, mode);

	const char* defFolder = getDefFolder(mode);
	const char* defName = getDefName(mode);

	bool result = dial.ask(title.c_str(), filter.toStr(), defName, defFolder);

	if(result)
	{
		if(mode == SaveGame)
		{
			RegistryFile registry(registryName);
			registry.setFileName(dial.fileName());
		}

		s += dial.fileName();

      setDefFile(mode, dial.fileName());
	}

	return result;
}

bool askSaveName(SimpleString& s, FileFormat mode)
{
	SaveFileDialog dial;

	UINT titleID;

	switch(mode)
	{
		case Campaign:
			titleID = IDS_SAVE_SAVEWORLD;
			break;
		case Battle:
			titleID = IDS_SAVE_SAVEWORLD;	// wrong... should be battle
			break;
		case SaveGame:
			titleID = IDS_SAVE_SAVEGAME;
			break;

		default:
			titleID = IDS_SAVE_LOADWORLD;
			FORCEASSERT("Unknown File Format");
			break;
	}

	ResString title(titleID);

	SimpleString filter;
	getFilter(filter, mode);

	const char* defFolder = getDefFolder(mode);
	const char* defName = getDefName(mode);

	bool result = dial.ask(title.c_str(), filter.toStr(), defName, defFolder);
	if(result)
	{
		dial.addExtension(getFilter(mode));

		if(mode == SaveGame)
		{
			RegistryFile registry(registryName);
			registry.setFileName(dial.fileName());
		}

		s += dial.fileName();

      setDefFile(mode, dial.fileName());
	}

	return result;
}


};		// namespace SaveGame



bool BrowseForFile::load(char* fileName, char* filter)
{
	using namespace SaveGame;

	ASSERT(fileName != 0);

	OpenFileDialog dial;

	ResString title(IDS_SAVE_FINDFILE);

	const char* defFolder = 0;
	if(scenario != 0)
		defFolder = scenario->getFolderName();

	bool result = dial.ask(title.c_str(), filter, NULL, defFolder);

	lstrcpy(fileName, dial.fileNameNoPath());

	return result;
}



char FileTypeChunk::s_chunkID[] = "FileType";
UWORD FileTypeChunk::s_version = 0x0002;

bool FileTypeChunk::readData(FileReader& f)
{
	FileChunkReader fr(f, s_chunkID);

	if(!fr.isOK())
		return false;

   if (f.isAscii())
   {
      // todo:
   }
   else
   {
	   f >> d_version;

	   if(!fr.isOK())
           return false;

       if(d_version > s_version)
           throw WrongVersionError(d_version);

	   UWORD mode;
	   f >> mode;
	   d_mode = static_cast<SaveGame::FileFormat>(mode);

       // Get Scenario

       CString scenName;
       if (d_version >= 0x0002)
       {
         scenName = f.getString();
       }
       else
       {
         scenName = Scenario::defaultFileName();
       }

       Scenario::create(scenName);
   }
	return fr.isOK();
}

bool FileTypeChunk::writeData(FileWriter& f) const
{
	FileChunkWriter fr(f, s_chunkID);

   if (f.isAscii())
   {
      f.printf("Version %d\n", s_version);
      f.printf("Mode %d\n", d_mode);
   }
   else
   {
   	f << s_version;
   	f.putUWord(d_mode);
      f.putString(scenario->getFileName());     // New in version 0x0002
   }
	return f.isOK();

}


