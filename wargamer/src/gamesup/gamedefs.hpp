/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef GAMEDEFS_H
#define GAMEDEFS_H

#ifndef __cplusplus
#error gamedefs.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1995, Steven Morle-Green, All Rights Reserved
 *----------------------------------------------------------------------
 *
 * Definitions used in War game
 *
 *----------------------------------------------------------------------
 */


#include "mytypes.h"
#include "BitSet.hpp"
// #include "cpdef.hpp"             // needed for new ICommandPosition definition
// #include "refcp.hpp"             // needed for new ICommandPosition definition
// #include "iComPos.hpp"
// #include "typedef.hpp"

#define DATA_BEING_CHANGED    // For temporary commenting out of code

/*
 * forward references
 */

class FileReader;
class FileWriter;

/*
 * Definition of Sides
 */

typedef UBYTE Side;
#define SIDE_Neutral Side(UBYTE_MAX)
#define Side_MAX     Side(SIDE_Neutral - 1)

inline Side otherSide(Side s)
{
    if(s == SIDE_Neutral)
        return s;
    else
        return s ^ 1;
}

/*
 * Definition of Nationality
 * Each nationality can be matched to a side using scenario
 */

typedef UBYTE Nationality;
typedef Set32 NationMask;     // Bit map of nationalities
#define NATION_Neutral Nationality(UBYTE_MAX)
#define Nationality_MAX Nationality(31)

/*
 *  Definition of Nation Type.
 *  Presently they are 1 of 3 types:
 *  MajorNation, MinorNation, GenericNation
 */

typedef UBYTE NationType;
enum {
  MajorNation,
  MinorNation,
  GenericNation
};
#define NationType_MAX NationType(3)

/*
 * Definition of TerrainType
 */

typedef UBYTE TerrainType;
#define NoTerrainType TerrainType(UBYTE_MAX)

/*
 * Definition of Border Colors
 */

typedef UBYTE IBorderColour;
#define BorderColour_MAX IBorderColour(4)

/*
 * HelpID definition
 */

typedef UWORD HelpID;
#define NoHelpID HelpID(UWORD_MAX)

/*
 * Some basic index types
 */

typedef UWORD IGAMEOB;
const IGAMEOB NoGameOb = IGAMEOB(UWORD_MAX);

typedef IGAMEOB ITown;
// DTypeDef(IGAMEOB, ITown);
const ITown             NoTown            = ITown(NoGameOb);

typedef IGAMEOB IProvince;
const IProvince         NoProvince        = IProvince(NoGameOb);

typedef IGAMEOB IConnection;
const IConnection       NoConnection      = IConnection(NoGameOb);

enum GameObType {
   GT_CommandPosition,
   GT_DetachedLeader,
   GT_StrengthPoint,
   GT_Leader,

   GT_Max
};

/*
 * Handle to a game object
 * Set up so can be stored as a LONG
 */

struct HGAMEOB {
   UBYTE type;       // CommandPosition, Independant Leader, Town, Independant SP
   UBYTE reserved;
   UWORD index;
};

/*
 * Strength Points and Unit Types
 */

typedef UBYTE UnitType;
#define NoUnitType UnitType(UBYTE_MAX)
typedef UWORD SPCount;        // Strength Point count

typedef SPCount SPValue;        // Effective SP value
typedef UWORD BombValue;      // Bombardment value

/*
 * Attributes are stored as numbers between 0..255
 * In general, 0 is bad, 255 is good.
 *
 * The player will normally be shown enumerated values based on
 * this range, e.g. poor, bad, medium, good, excellent
 */

typedef UBYTE Attribute;

#define Attribute_Range Attribute(UBYTE_MAX)

/*
 * Attribute points are accumulated values used for resource and manpower
 */

typedef UWORD AttributePoints;
#define AttributePoint_Range AttributePoints(UWORD_MAX)

struct ResourceSet {
   AttributePoints manPower;
   AttributePoints resource;

   ResourceSet() { manPower = 0; resource = 0; }
};


#endif /* GAMEDEFS_H */

