/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *	Who controls what side
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "control.hpp"
#include "myassert.hpp"
#include "registry.hpp"
#include "filebase.hpp"
#include "options.hpp"

namespace {

const int nSides = 2;



class SideController {
	 static GamePlayerControl::Controller d_sideController[nSides];
	 static const char d_globRegName[];


	 static const UWORD FileVersion;


  public:
	 SideController();

	 GamePlayerControl::Controller getControl(Side side) const
	 {
	 	ASSERT(side < nSides);
	  	return d_sideController[side];
	 }

	 void setControl(Side s, GamePlayerControl::Controller c)
	 {
	 	ASSERT(s < nSides);
		d_sideController[s] = c;
		updateRegistry();
	 }

	 Boolean readData(FileReader& f);
	 Boolean writeData(FileWriter& f) const;

    static Side getDefaultSide();

  private:
	 void updateRegistry();
};

const char SideController::d_globRegName[] = "GlobalSettings";

GamePlayerControl::Controller SideController::d_sideController[nSides] = {
	 GamePlayerControl::Player,
	 GamePlayerControl::AI
};

SideController::SideController()
{
	if(!getRegistry("Control", d_sideController, sizeof(d_sideController), d_globRegName))
   {
      Side defSide = getDefaultSide();

      for (int i = 0; i < nSides; ++i)
      {
         if(i == defSide)
            setControl(i, GamePlayerControl::Player);
         else
            setControl(i, GamePlayerControl::AI);
      }


   }
}

Side SideController::getDefaultSide()
{
   Side defSide = 0;

   if(!getRegistry("defSide", &defSide, sizeof(defSide), d_globRegName))
   {
      defSide = 0;
   }

   return defSide;
}

void SideController::updateRegistry()
{
	setRegistry("Control", d_sideController, sizeof(d_sideController), d_globRegName);
}

const UWORD SideController::FileVersion = 0x0000;

Boolean SideController::readData(FileReader& f)
{

	UWORD version;
	f.getUWord(version);
	ASSERT(version <= FileVersion);

	UBYTE sideCount;
	f.getUByte(sideCount);
	ASSERT(sideCount == nSides);

	for(int i = 0; i < nSides; i++)
	{
		UBYTE control;
		f.getUByte(control);

		ASSERT( (control == GamePlayerControl::Player) || (control == GamePlayerControl::AI) || (control == GamePlayerControl::Remote) );

		if( (control != GamePlayerControl::Player) && (control != GamePlayerControl::AI) && (control != GamePlayerControl::Remote) )
			control = GamePlayerControl::Player;

		setControl(i, static_cast<GamePlayerControl::Controller>(control));
	}

	return f.isOK();
}

Boolean SideController::writeData(FileWriter& f) const
{
	f.putUWord(FileVersion);
	f.putUByte(nSides);
	for(int i = 0; i < nSides; i++)
		f.putUByte(getControl(i));

	return f.isOK();
}


static SideController sideController;

};	// namespace

/*
 * Global functions
 */

GamePlayerControl::Controller GamePlayerControl::getControl(Side side)
{
  ASSERT(side != SIDE_Neutral);
  ASSERT(side < nSides);

  if(side != SIDE_Neutral)
  {
	 return sideController.getControl(side);
  }

  return  Player;

}

void GamePlayerControl::setControl(Side side, Controller value)
{
  ASSERT(side != SIDE_Neutral);
  ASSERT(side < nSides);
  ASSERT(value >= Player);
  ASSERT(value <= Remote);

  if(side != SIDE_Neutral)
	 sideController.setControl(side, value);
}

Side GamePlayerControl::getSide(Controller control)
{
    ASSERT(control < ControllerCount);

    for(Side s = 0; s < nSides; ++s)
    {
        if(sideController.getControl(s) == control)
            return s;
    }
    return SIDE_Neutral;

}



Boolean GamePlayerControl::readData(FileReader& f)
{
	return sideController.readData(f);
}

Boolean GamePlayerControl::writeData(FileWriter& f)
{
	return sideController.writeData(f);
}


bool GamePlayerControl::canControl(Side side)
{
#if !defined(EDITOR)
#ifdef DEBUG
   if(Options::get(OPT_OrderEnemy))
      return True;
   else
#endif
   {
      if(side == SIDE_Neutral)
         return false;
      else
      	return (sideController.getControl(side) == Player);
   }
#else
   return true;
#endif   // EDITOR
}


Side GamePlayerControl::getDefaultSide()
{
   return SideController::getDefaultSide();
}

/*
 * $Log$
 */
