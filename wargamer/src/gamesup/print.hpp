/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef PRINT_HPP
#define PRINT_HPP

#include "gamesup.h"

namespace Print {
	GAMESUP_DLL_EXPORT BOOL print(HWND hParent, HDC& hdc, int nPages);
};


#endif
