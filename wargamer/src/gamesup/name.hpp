/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef NAME_HPP
#define NAME_HPP

#ifndef __cplusplus
#error name.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *	Named Items
 *
 *----------------------------------------------------------------------
 */

#include "StringPtr.hpp"
#include "gamesup.h"

class FileReader;
class FileWriter;

/*
 * Base class for objects with names
 */

class NamedItem : private CString {
	static const UWORD fileVersion;

public:
	NamedItem() : CString() { }
	GAMESUP_DLL_EXPORT NamedItem(NamedItem& ni);

	virtual ~NamedItem()	{ }

	void setName(char* s) {	set(s); }

	const char* getName() const
	{
		return get();
	}

	const char* getNameNotNull(const char* def = 0) const 
	{
		return get() ? get() : (def ? def : "");
	}

	GAMESUP_DLL_EXPORT NamedItem& operator = (NamedItem& ni);

	/*
	 * File Interface
	 */

	GAMESUP_DLL_EXPORT Boolean readData(FileReader& f);
	GAMESUP_DLL_EXPORT Boolean writeData(FileWriter& f) const;
};


#endif /* NAME_HPP */

