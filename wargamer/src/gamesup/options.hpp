/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef OPTIONS_H
#define OPTIONS_H

#ifndef __cplusplus
#error options.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1995, Steven Morle-Green, All Rights Reserved
 *----------------------------------------------------------------------
 *
 *	Game Options
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.3  1996/03/01 17:29:50  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.2  1996/02/29 18:09:32  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.1  1996/02/28 09:33:33  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#include "mytypes.h"
#include "cmsg_id.hpp"
#include "gamesup.h"


/*
 * Game Options, combined in an unsigned word
 *
 * Note: There will probably be more than 16 (or 32) different
 *       options by the time we finish.
 *       We should either just use ordinary Boolean values in
 *       the options class, or use the Set class (see BitSet.hpp)
 *			with seperate values for each type of option.
 *
 *			Using the set class will be best, because it will make
 *			saving and restoring from the registry easier.
 */

class FileReader;
class FileWriter;

class GAMESUP_DLL_EXPORT OptionBase {
	 ULONG* d_flags;
	 size_t d_size;
	 const char* d_regName;

     static const UWORD s_fileVersion;

  public:
	 OptionBase(size_t size, const char* regName);
	 ~OptionBase()
	 {
		if(d_flags)
		  delete[] d_flags;
	 }

	 void flip(int b);
	 void set(int b);
	 void clear(int b);
	 bool get(int b);

	 ULONG getBitfield(void) { return *d_flags; }
	 void setBitfield(ULONG flags) { *d_flags = flags; }

	 bool readData(FileReader& f);
	 bool writeData(FileWriter& f) const;

  protected:
	 void updateRegistry();
	 bool readRegistry();
	 void reset();

    private:
        virtual void fiddleVersion(UWORD version) { }

};


enum GameOptionEnum {
	OPT_GameOptionFirst = 0,
//	OPT_FullRoute = OPT_GameOptionFirst,
	OPT_ColorCursor = OPT_GameOptionFirst,
	OPT_ToolTips,
	OPT_AlertBox,
	OPT_TownNames,
	OPT_NationDisplay,
	OPT_TownIcons,								// Display Towns on Map using Graphics at appropriate level
	OPT_TownHilightIcons,					// Always Display Hilighted towns using graphics
   OPT_DrawMapScale,                          // Set to Draw MapScale
	OPT_ShowSupply,
	OPT_ShowChokePoints,
   OPT_ShowSupplyLines,

	OPT_DeadBodies,                                   // Display of dead bodies on battlefield
	OPT_Labels,										  // Show battle text labels

#ifdef DEBUG
	OPT_InstantMove,
   OPT_OrderEnemy,
	OPT_ShowAI,									// Debug Option
	OPT_QuickMove,								// Quickly move between locations
#endif

	OPT_GameOption_HowMany

};

/*
 * These are campaign specific options that cannot be changed once set
 * These are mainly realism settings
 */

enum CampaignOptionEnum {
	OPT_CampaignOptionFirst = 0,
	OPT_FogOfWar = OPT_CampaignOptionFirst,
//	OPT_LimitedUnitInfo = OPT_CampaignOptionFirst,    // limited info available on enemy units
//	OPT_LimitedTownInfo,                              // limited info available on enemy towns
	OPT_InstantOrders,                                // order messenger times are ignored
	OPT_AggressionChange,                             // leader's may change aggression level on own
	OPT_Supply,                                       // Supply rules inforced
	OPT_Attrition,                                    // Attrition is calculated
	OPT_NationRules,                                  // Nationality rules inforced

	OPT_CampaignOption_HowMany
};

enum AlertBoxEnum {
	AB_First = 0,
	AB_ExitGame = AB_First,
	AB_SendOrder,
	AB_DetachUnit,
   AB_SurrenderBattle,
   AB_AbandonGame,
	AB_HowMany
};


/*
 * Options Groups & Default Values for Difficulty Settings
 */




enum OptionPageEnum {

	FirstPage = 0,

// #ifdef DEBUG
	RealismPage = FirstPage,
	GameOptionsPage,
// #else
//		GameOptionsPage = FirstPage,
// #endif
	ShowMessagePage,
	LastMessagePage = ShowMessagePage + CampaignMessages::Group_HowMany - 1,

	LastPage = LastMessagePage,
	OptionPage_HowMany,

	DefaultPage = 0
};



struct OptionInfo {
	int            s_option;	// What option does this refer to
	OptionPageEnum 		s_page;		// Which Options Page does it belong to
	int		  		s_resID;	// ID in resource file for text to display in dialog
	int		  		s_helpID;	// ID in help file
	bool	  		s_beginner;	// Default starting value for beginner level
	bool			s_normal;	// Default for normal play
	bool			s_advanced; // Default for advanced play
};

#define IDH_POD_ShowAI -1

class GAMESUP_DLL_EXPORT OptionSettingsInfo {

public:

	static OptionInfo gameOptionsSettings[];
	static OptionInfo campaignOptionsSettings[];

};

/*
 * Functions, packed away in Options::
 */

class GAMESUP_DLL_EXPORT Options {
 public:
	static bool get(GameOptionEnum b);
	static void set(GameOptionEnum what, bool how);
	static void toggle(GameOptionEnum what);
	static void setDefault();

	static ULONG getBitfield(void);
	static void setBitfield(ULONG flags);
};

class GAMESUP_DLL_EXPORT CampaignOptions {
 public:
	enum SkillLevel {
		Novice,
		Normal,
		Expert,
      Custom,

		SkillLevel_HowMany
	};

	static bool get(CampaignOptionEnum what);
	static void set(CampaignOptionEnum what, bool flag);
	static void toggle(CampaignOptionEnum what);
	static void setDefault();
	static void setSkillLevel(SkillLevel l);
   static SkillLevel getSkillLevel();

#if 0
	static void setBeginner();
	static void setIntermediate();
	static void setAdvanced();
#endif

	static ULONG getBitfield(void);
	static void setBitfield(ULONG flags);

	static bool readData(FileReader& f);
	static bool writeData(FileWriter& f);
};

class GAMESUP_DLL_EXPORT AlertBoxOptions {
 public:
	static bool get(AlertBoxEnum id);
	static void set(AlertBoxEnum id, bool flag);
	static void toggle(AlertBoxEnum id);
};

class GAMESUP_DLL_EXPORT MessageOptions {
 public:
	static bool get(CampaignMessages::CMSG_ID id);
	static void set(CampaignMessages::CMSG_ID id, bool flag);
	static void toggle(CampaignMessages::CMSG_ID id);
   static void setDefault();
};

GAMESUP_DLL_EXPORT int alertBox(HWND h, LPCTSTR lpText, LPCTSTR lpCaption,	UINT uType, AlertBoxEnum id, int defValue);


#endif /* OPTIONS_H */

