/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
#include "stdinc.hpp"
#include "wg_main.hpp"
#include "wind.hpp"
#include "app.hpp"
#include "resdef.h"
#include "timer.hpp"
#include "gamectrl.hpp"

#include "dib.hpp"
#include "dib_util.hpp"
#include "scenario.hpp"
#include "scrnbase.hpp"
#include "wmisc.hpp"
#include "fonts.hpp"
#include "palwind.hpp"

#include "about.hpp"
#include "msgbox.hpp"

#include"options.hpp"
#include "alertbox.hpp"

// #include "keycode.hpp"
#include "wg_msg.hpp"

#include "registry.hpp"
#include "resstr.hpp"
#include <ShellAPI.h>

// #define CAPTION_BUFSIZE 50
static const int CAPTION_BUFSIZE = 50;

char szMainCaption[CAPTION_BUFSIZE];



/*
 * Custom Caption Code
 */



struct SystemButton {
   enum Type
   {
      Min,
      Max,
      Close,

      HowMany
   } d_type;

   RECT d_rect;
   Boolean d_selected;

   SystemButton() :
   d_type(Min),
   d_selected(False)
   {
      SetRect(&d_rect, 0, 0, 0, 0);
   }
};


class RealMainWindow : public WindowBaseND {
   WMTimer d_wmTimer;

   static ATOM classAtom;
   static const char s_className[];
//   static const char s_positionRegName[];
   static const char s_mainWindowRegDir[];
   static WSAV_FLAGS s_saveFlags;

   // For Custom Caption

   DrawDIBDC* d_captionDib;
   Palette::PaletteID d_palID;
   // const DrawDIB* d_fillDib;

   SystemButton d_sysButtons[SystemButton::HowMany];


   public:
   RealMainWindow();
   ~RealMainWindow(); // {}


   virtual const char* className() const
   { return s_className;
   }

   private:
   static ATOM registerClass();


   LRESULT procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
   BOOL onCreate(HWND hWnd, CREATESTRUCT* lpCreateStruct);
   void onDestroy(HWND hwnd);
   void onGetMinMaxInfo(HWND hWnd, LPMINMAXINFO lpMinMaxInfo);
   void onTimer(HWND hwnd, UINT id);

   void onCommand(HWND hWnd, int id, HWND hwndCtl, UINT codeNotify);

   // For Custom Caption

   void onNCPaint(HWND hwnd, HRGN hrgn);
   BOOL onNCActivate(HWND hwnd, BOOL fActive, HWND hwndActDeact, BOOL fMinimized);
   void onNCLButtonDown(HWND hwnd, BOOL fDoubleClick, int x, int y, UINT codeHitTest);
   void onNCLButtonUp(HWND hwnd, int x, int y, UINT codeHitTest);
   UINT onNCHitTest(HWND hwnd, int x, int y);
   void onSize(HWND hwnd, UINT state, int cx, int cy);
   void onMove(HWND hwnd, int cx, int cy);
   void onClose(HWND hWnd);

   void sysButtonClicked(SystemButton::Type t);
   void setSysButtonPos();
   void drawMinButton(int x, int y, int cx, int cy, ColourIndex color);
   void drawMaxButton(int x, int y, int cx, int cy, ColourIndex color);
   void drawCloseButton(int x, int y, int cx, int cy, COLORREF color);

   void redrawCaption();

   int getSystemButtonID(int cx, int cy);

};

const char RealMainWindow::s_className[] = "WarGamer";
ATOM RealMainWindow::classAtom = 0;

// static const char RealMainWindow::s_positionRegName[] = "Position";
const char RealMainWindow::s_mainWindowRegDir[] = "MainWindow";
WSAV_FLAGS RealMainWindow::s_saveFlags = WSAV_POSITION | WSAV_SIZE | WSAV_RESTORE;


/*
 * Register Main Class
 */

ATOM RealMainWindow::registerClass()
{
   if(!classAtom)
   {
      WNDCLASS wc;

      wc.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
      wc.lpfnWndProc = baseWindProc;
      wc.cbClsExtra = 0;
      wc.cbWndExtra = sizeof(RealMainWindow*);
      wc.hInstance = APP::instance();
      wc.hIcon = LoadIcon(APP::instance(), MAKEINTRESOURCE(ICON_WARGAMER));
      wc.hCursor = LoadCursor(NULL, IDC_ARROW);
      //    wc.hbrBackground = (HBRUSH) (COLOR_BACKGROUND + 1);
      wc.hbrBackground = NULL;   // (HBRUSH) (COLOR_BACKGROUND + 1);
      //    wc.lpszMenuName = MAKEINTRESOURCE(MENU_START);
      wc.lpszMenuName = NULL;
      wc.lpszClassName = s_className;  // szMainClass;   // szAppName;
      classAtom = RegisterClass(&wc);
   }

   ASSERT(classAtom != 0);

   return classAtom;
}

RealMainWindow::RealMainWindow() :
   d_captionDib(0),
   d_palID(0)
   // d_fillDib(scenario->getScenarioBkDIB(Scenario::BackPatterns::MenuBarBackground))
{
#ifdef DEBUG
   debugLog("RealMainWindow::RealMainWindow() -- constructor called\n");
#endif


   LoadString(APP::instance(), IDS_Caption, szMainCaption, sizeof(szMainCaption));

   registerClass();

   HWND hWnd = createWindow(0,
      // className,        // szMainClass,
      szMainCaption,
      WS_OVERLAPPEDWINDOW |
      WS_MAXIMIZE |           // Start off maximized by default
      WS_CLIPCHILDREN,
      CW_USEDEFAULT,          /* init. x pos */
      CW_USEDEFAULT,          /* init. y pos */
      CW_USEDEFAULT,          /* init. x size */
      CW_USEDEFAULT,          /* init. y size */
      NULL,                   /* parent window */
      NULL                    /* menu handle */
      // APP::instance()      /* program handle */
      );

   ASSERT(hWnd != NULL);
   if(hWnd == NULL)
      return;

   SetWindowText(hWnd, szMainCaption);








#ifdef DEBUG
   DWORD id = GetCurrentThreadId();
   ASSERT(id == APP::getInterfaceThreadID());
#endif

#if 0
      // May be nicer to not show the window... until after all children are positioned

//      if(!restoreWindowState(mainWindowRegName, hWnd, WSAV_ALL, "\\mainwind"))
//      ShowWindow(hWnd, SW_MAXIMIZE);
   // ShowWindow(hWnd, nCmdShow);
#endif

   if(!getState(s_mainWindowRegDir, s_saveFlags))
      ShowWindow(hWnd, SW_MAXIMIZE);

//   if(!restoreWindowState(s_positionRegName, hWnd, WSAV_ALL, s_mainWindowRegDir))
//      ShowWindow(hWnd, SW_SHOWDEFAULT);

   /*
   * initialize caption/border subclass
   */

   setSysButtonPos();
   redrawCaption();
}

RealMainWindow::~RealMainWindow()
{
#ifdef DEBUG
   debugLog("RealMainWindow::~RealMainWindow() -- destructor called\n");
#endif
   selfDestruct();

   if(d_captionDib)
      delete d_captionDib;
}

LRESULT RealMainWindow::procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
   switch(msg)
   {
   HANDLE_MSG(hWnd, WM_CREATE, onCreate);
   HANDLE_MSG(hWnd, WM_GETMINMAXINFO,  onGetMinMaxInfo);
   HANDLE_MSG(hWnd, WM_DESTROY, onDestroy);
   HANDLE_MSG(hWnd, WM_TIMER,          onTimer);
   HANDLE_MSG(hWnd, WM_COMMAND,           onCommand);

   // For Custom Caption

   HANDLE_MSG(hWnd, WM_NCPAINT,        onNCPaint);
   HANDLE_MSG(hWnd, WM_NCACTIVATE,     onNCActivate);
   HANDLE_MSG(hWnd, WM_NCLBUTTONDOWN,  onNCLButtonDown);
   HANDLE_MSG(hWnd, WM_NCLBUTTONUP,    onNCLButtonUp);
   HANDLE_MSG(hWnd, WM_NCHITTEST,      onNCHitTest);
   HANDLE_MSG(hWnd, WM_SIZE,           onSize);
   HANDLE_MSG(hWnd, WM_MOVE,           onMove);
   HANDLE_MSG(hWnd, WM_CLOSE,          onClose);

   default:
      return defProc(hWnd, msg, wParam, lParam);
   }
}

void RealMainWindow::onGetMinMaxInfo(HWND hWnd, LPMINMAXINFO lpMinMaxInfo)
{
   const int minimumWidth = 640;
   const int minimumHeight = 480;

   lpMinMaxInfo->ptMinTrackSize.x = minimumWidth;
   lpMinMaxInfo->ptMinTrackSize.y = minimumHeight;
}


BOOL RealMainWindow::onCreate(HWND hWnd, CREATESTRUCT* lpCreateStruct)
{
#ifdef DEBUG
   debugLog("RealMainWindow::onCreate()\n");
#endif

   ASSERT(APP::getMainHWND() == NULL);
   APP::setMainHWND(hWnd);

   d_wmTimer.initWMTimer(hWnd);
   return True;
}

void RealMainWindow::onDestroy(HWND hwnd)
{
#ifdef DEBUG
   debugLog("RealMainWindow::onDestroy()\n");
#endif

//   saveWindowState(s_positionRegName, hwnd, WSAV_ALL, s_mainWindowRegDir);
   saveState(s_mainWindowRegDir, s_saveFlags);

   d_wmTimer.stopWMTimer();
   APP::setMainHWND(0);
}

void RealMainWindow::onTimer(HWND hwnd, UINT id)
{
   GameControl::onTimer(); // gApp.runGame();
}


/*
 * Custom Caption Stuff
 */

void RealMainWindow::redrawCaption()
{
   RECT r;
   GetWindowRect(getHWND(), &r);
   int capCY = GetSystemMetrics(SM_CYCAPTION);

   POINT p1;
   p1.x = r.left;
   p1.y = r.top;
   ScreenToClient(getHWND(), &p1);

   r.right = p1.x + r.right - r.left;
   r.bottom = p1.y + r.bottom - r.top;
   r.left = p1.x;
   r.top = p1.y;

   InvalidateRect(getHWND(), &r, FALSE);

   SendMessage(getHWND(), WM_NCPAINT, 0, 0);
}

/*
 * Setup the system button positions
 */


void RealMainWindow::setSysButtonPos()
{
   RECT r;
   GetWindowRect(getHWND(), &r);

   const int capCY = GetSystemMetrics(SM_CYCAPTION);
   const int yOffset = 2;
   const int rxOffset = 1;  // (3 * ScreenBase::dbX()) / ScreenBase::baseX();
   const int spaceCX = 1;
   const int buttonCX = capCY - (2 * yOffset);

   // int x = r.right - ((SystemButton::HowMany * buttonCX) + CustomBorderWindow::ThickBorder + (2 * spaceCX) + rxOffset);
   int x = r.right - ((SystemButton::HowMany * buttonCX) + CustomBorderWindow::ThickBorder + rxOffset);
   int y = r.top + CustomBorderWindow::ThickBorder + yOffset;
   for(SystemButton::Type t = SystemButton::Min; t < SystemButton::HowMany; INCREMENT(t))
   {
      d_sysButtons[t].d_type = t;
      SetRect(&d_sysButtons[t].d_rect, x, y, x + buttonCX, y + buttonCX);

      x += buttonCX + spaceCX;
   }
}



void RealMainWindow::onSize(HWND hwnd, UINT state, int cx, int cy)
{
#ifdef DEBUG
   GameControl::pause((state == SIZE_MINIMIZED) ? True : False);
#endif
   setSysButtonPos();
   // SendMessage(hwnd, WM_NCPAINT, 0, 0);
   redrawCaption();
}

void RealMainWindow::onMove(HWND hwnd, int cx, int cy)
{
   setSysButtonPos();
   redrawCaption();
   // SendMessage(hwnd, WM_NCPAINT, 0, 0);
}

void RealMainWindow::drawMinButton(int x, int y, int cx, int cy, ColourIndex color)
{
   ASSERT(d_captionDib);

   // we are drawing a -

   const int xOffset = 3;
   int yOffset = y + (cy - 7);
   d_captionDib->hLine(x + xOffset, y + yOffset++, cx - (2 * xOffset), color);
   d_captionDib->hLine(x + xOffset, y + yOffset, cx - (2 * xOffset), color);
}

void RealMainWindow::drawMaxButton(int x, int y, int cx, int cy, ColourIndex color)
{
   ASSERT(d_captionDib);

   /*
   * See if we are maximized or not
   */

   if(IsZoomed(getHWND()))
   {
      /*
      * Draw 2 overlapping rects
      */

      const int xOffset = 3;
      const int yOffset = 2;
      const int frameCX = 5;
      const int frameCY = 5;

      d_captionDib->frame(x + xOffset, y + yOffset, frameCX, frameCY, color);
      d_captionDib->frame((x + xOffset) - 1, (y + yOffset) + 3, frameCX, frameCY, color);
   }
   else
   {
      /*
      * Draw a line and a single rect
      */

      const int xOffset = 3;
      const int yOffset = 3;
      const int frameCX = 6;
      const int frameCY = 5;

      d_captionDib->hLine(x + xOffset, y + yOffset, frameCX, color);
      d_captionDib->frame((x + xOffset), (y + yOffset) + 1, frameCX, frameCY, color);
   }
}

void RealMainWindow::drawCloseButton(int x, int y, int cx, int cy, COLORREF color)
{
   /*
   * Draw a x
   */

   static const char* c = "x";

   SIZE s;
   GetTextExtentPoint32(d_captionDib->getDC(), c, lstrlen(c), &s);

   const int xOffset = 2;
   const int yOffset = ((cy - s.cy) / 2) - 1;

   COLORREF oldColor = d_captionDib->setTextColor(color);
   wTextOut(d_captionDib->getDC(), x + xOffset, y + yOffset, c);

   d_captionDib->setTextColor(oldColor);
}

void RealMainWindow::onNCPaint(HWND hwnd, HRGN hrgn)
{
   HDC hdc = GetWindowDC(hwnd);
   ASSERT(hdc != 0);

   HPALETTE oldPal = SelectPalette(hdc, Palette::get(), FALSE);
   RealizePalette(hdc);

   /*
   * Draw Borders
   */

   RECT r;
   GetWindowRect(hwnd, &r);
   // SetRect(&r, 0, 0, windowRect.right-windowRect.left, windowRect.bottom-windowRect.top);
   OffsetRect(&r, -r.left, -r.top);

   if(scenario != 0)
   {
      const CustomBorderInfo& bi = scenario->getBorderColors();
      static CustomBorderWindow bw(bi);
      bw.drawThickBorder(hdc, r);
   }

   /*
   * Allocate caption dib if needed
   */

   const int capCX = r.right - (CustomBorderWindow::ThickBorder*2);
   const int capCY = GetSystemMetrics(SM_CYCAPTION);

   if(d_palID != Palette::paletteID())
   {
      delete d_captionDib;
      d_captionDib = 0;
   }
   d_palID = Palette::paletteID();

   DIB_Utility::allocateDib(&d_captionDib, capCX, capCY);
   ASSERT(d_captionDib);

   // fill it
   // ASSERT(d_fillDib);

   if(scenario != 0)
   {
      const DrawDIB* fillDIB = scenario->getScenarioBkDIB(Scenario::BackPatterns::MenuBarBackground);
      d_captionDib->rect(0, 0, capCX, capCY, fillDIB);
   }
   else
      d_captionDib->rect(0,0,capCX,capCY, d_captionDib->getColour(GetSysColor(COLOR_ACTIVECAPTION)));

   d_captionDib->setBkMode(TRANSPARENT);

   // draw a thin black border around caption
   ColourIndex ci = d_captionDib->getColour(PALETTERGB(0, 0, 0));
   d_captionDib->frame(0, 0, capCX, capCY, ci);

   /*
   * draw buttons
   */

   // setSysButtonPos();

   ColourIndex c1;
   ColourIndex c2;

   if(scenario != 0)
   {
      const CustomBorderInfo& bi = scenario->getBorderColors();
      c1 = d_captionDib->getColour(bi.colours[CustomBorderInfo::OffWhite]);
      c2 = d_captionDib->getColour(bi.colours[CustomBorderInfo::DarkBrown]);
   }
   else
   {
      c1 = d_captionDib->getColour(PALETTERGB(255,247,215));
      c2 = d_captionDib->getColour(PALETTERGB(115,90,66));
   }

   COLORREF imgColorRef = PALETTERGB(0, 0, 0);
   ColourIndex  imgColor = d_captionDib->getColour(imgColorRef);

#if 0
      const int yOffset = 2;
   const int rxOffset = (3 * ScreenBase::dbX()) / ScreenBase::baseX();
   const int spaceCX = 1;
   const int buttonCX = capCY - (2 * yOffset);

   int x = windowRect.right - ((SystemButton::HowMany * buttonCX) + CustomBorderWindow::ThickBorder + (2 * spaceCX) + rxOffset);
   int y = windowRect.top + CustomBorderWindow::ThickBorder + yOffset;

   POINT p;
   p.x = r.right - (SystemButton::HowMany * buttonCX + (2 * spaceCX) + rxOffset);
   p.y = yOffset;
#endif

   int captionRightEdge = capCX;

   for(SystemButton::Type i = SystemButton::Min; i < SystemButton::HowMany; INCREMENT(i))
      // for(int i = 0; i < SystemButton::HowMany; i++)
   {
#if 0
         d_sysButtons[i].d_type = i;
      SetRect(&d_sysButtons[i].d_rect, x, y, x + buttonCX, y + buttonCX);
#endif

      const int cx = d_sysButtons[i].d_rect.right - d_sysButtons[i].d_rect.left;
      const int cy = d_sysButtons[i].d_rect.bottom - d_sysButtons[i].d_rect.top;
      // int cx = buttonCX;
      // int cy = buttonCY;

      POINT p;
      p.x = d_sysButtons[i].d_rect.left;
      p.y = 0;

      // convert x from screen

      ScreenToClient(hwnd, &p);

      p.y = (capCY - cy) / 2;

      if (i == SystemButton::Min)
      {
         drawMinButton(p.x, p.y, cx, cy, imgColor);
         captionRightEdge = p.x;
      }
      else if(i == SystemButton::Max)
         drawMaxButton(p.x, p.y, cx, cy, imgColor);
      else
         drawCloseButton(p.x, p.y, cx, cy, imgColorRef);

      if(d_sysButtons[i].d_selected)
         d_captionDib->frame(p.x, p.y, cx, cy, c2, c1);
      else
         d_captionDib->frame(p.x, p.y, cx, cy, c1, c2);

      // x += buttonCX + spaceCX;
      // p.x += buttonCX;
   }

   /*
   * get font
   */

   const int fontHeight = capCY - 2;
   ASSERT(fontHeight >= 3);    // check for silly size?

   LogFont lf;
   if(scenario != 0)
      lf.face(scenario->fontName(Font_Bold));
   else
      lf.face("");
   lf.height(fontHeight);
   lf.weight(FW_MEDIUM);
   lf.charset(Greenius_System::ANSI);      // otherwise we may get symbols!

   Font font;
   font.set(lf);

   HFONT oldFont = d_captionDib->setFont(font);

   /*
   *  Draw Caption Text
   */

   const size_t MaxCaptionLength = 200;
   char buf[MaxCaptionLength];
   GetWindowText(hwnd, buf, MaxCaptionLength-1);

#if 0
      const KeyCode* key = KeyCode::instance();
   if(key->shouldDisplayMessage())
   {
      const char* message = key->description();
      static const char* header = " ... ";
      ASSERT(message);
      size_t msgSize = strlen(message) + strlen(header);
      if(msgSize > (MaxCaptionLength-1))
         msgSize = (MaxCaptionLength-1);
      size_t msgEnd = strlen(buf);
      size_t msgStart = MaxCaptionLength - msgSize;
      if(msgStart > msgEnd)
         msgStart = msgEnd;
      wsprintf(&buf[msgStart], "%s%s", header, message);
   }
#endif


   // set text color
   COLORREF color;
   // if(!scenario->getColour("DarkBrown", color))
   if(!scenario || !scenario->getColour("CaptionText", color))
      color = PALETTERGB(0, 0, 0);

   COLORREF oldColor = d_captionDib->setTextColor(color);

   // get text height
   TEXTMETRIC tm;
   GetTextMetrics(d_captionDib->getDC(), &tm);

   const int x = 15;
   const int y = (capCY - (tm.tmHeight+2)) / 2;

   wTextOut(d_captionDib->getDC(), x, y, buf);

#if 0 // Removed from OpenSource version
   /*
   * Display Special KeyCode message
   */

   const KeyCode* key = KeyCode::instance();
   if(key->shouldDisplayMessage())
   {
      const char* message = key->description();
      ASSERT(message);
      static const char* header = " ... ";
      wsprintf(buf, "%s%s", header, message);
      int w = textWidth(d_captionDib->getDC(), buf);
      int x = captionRightEdge - w - 4;
      if(x < 0)
         x = 0;
      wTextOut(d_captionDib->getDC(), x, y, buf);
   }
#endif

   BitBlt(hdc, CustomBorderWindow::ThickBorder, CustomBorderWindow::ThickBorder, capCX, capCY,
      d_captionDib->getDC(), 0, 0, SRCCOPY);

   // clean up
   d_captionDib->setFont(oldFont);
   d_captionDib->setTextColor(oldColor);

   SelectPalette(hdc, oldPal, FALSE);
   ReleaseDC(hwnd, hdc);
}

BOOL RealMainWindow::onNCActivate(HWND hwnd, BOOL fActive, HWND hwndActDeact, BOOL fMinimized)
{
   return TRUE;
}

UINT RealMainWindow::onNCHitTest(HWND hwnd, int x, int y)
{
#if 0
   for(int i = 0; i < SystemButton::HowMany; i++)
   {
      POINT p;
      p.x = x;
      p.y = y;

      if(PtInRect(&d_sysButtons[i].d_rect, p))
      {
         // return s_hitTest[i];
         return HTCAPTION;
      }
   }
#endif
   RECT r;
   GetWindowRect(getHWND(), &r);
   int capCY = GetSystemMetrics(SM_CYCAPTION);
   r.bottom = r.top + capCY;
   if(PtInRect(&r, PixelPoint(x, y)))
      return HTCAPTION;
   else
      return defProc(hwnd, WM_NCHITTEST, 0, MAKELPARAM(x, y));

}

int RealMainWindow::getSystemButtonID(int x, int y)
{
   int id = -1;

   for(int i = 0; i < SystemButton::HowMany; i++)
   {
      POINT p;
      p.x = x;
      p.y = y;

      if(PtInRect(&d_sysButtons[i].d_rect, p))
      {
         id = i;
         break;
      }
   }

   return id;
}

void RealMainWindow::onNCLButtonDown(HWND hwnd, BOOL fDoubleClick, int x, int y, UINT codeHitTest)
{
#if 0
      Boolean overButton = False;
   for(int i = 0; i < SystemButton::HowMany; i++)
   {
      POINT p;
      p.x = x;
      p.y = y;

      if(PtInRect(&d_sysButtons[i].d_rect, p))
      {
         d_sysButtons[i].d_selected = True;
         SendMessage(hwnd, WM_NCPAINT, 0, 0);
         break;
      }
   }
#endif

   int id = getSystemButtonID(x, y);

//    if(codeHitTest == HTREDUCE)
//    {
//       id = SystemButton::Min;
//    }
//    else if(codeHitTest == HTZOOM)
//    {
//       id = SystemButton::Max;
//    }
//    else if(codeHitTest == HTSYSMENU)
//    {
//       id = SystemButton::Close;
//    }

   ASSERT(id == -1 || id < SystemButton::HowMany);

   if(id != -1)
   {
      d_sysButtons[id].d_selected = True;
      redrawCaption();
      // SendMessage(hwnd, WM_NCPAINT, 0, 0);
   }
   else
      FORWARD_WM_NCLBUTTONDOWN(hwnd, fDoubleClick, x, y, codeHitTest, defProc);
   // defProc(hwnd, WM_NCLBUTTONDOWN, static_cast<WPARAM>(codeHitTest), MAKELPARAM(x, y));
}

void RealMainWindow::sysButtonClicked(SystemButton::Type t)
{
   switch(t)
   {
   case SystemButton::Min:
      //    ShowWindow(getHWND(), SW_MINIMIZE);
      CloseWindow(getHWND());
      break;

   case SystemButton::Max:
      {
         ShowWindow(getHWND(), (IsZoomed(getHWND())) ? SW_SHOWNORMAL : SW_MAXIMIZE);
         break;
      }

   case SystemButton::Close:
      SendMessage(getHWND(), WM_CLOSE, 0, 0);
      break;

#ifdef DEBUG
   default:
      FORCEASSERT("Improper type");
#endif
   }
}

void RealMainWindow::onNCLButtonUp(HWND hwnd, int x, int y, UINT codeHitTest)
{
#if 0
      Boolean overButton = False;

   for(SystemButton::Type i = SystemButton::Min; i < SystemButton::HowMany; INCREMENT(i))
   {
      if(d_sysButtons[i].d_selected)
      {
#ifdef DEBUG
         logWindow->printf("NCButtonUp over button");
#endif
         d_sysButtons[i].d_selected = False;
         redrawCaption();
         // SendMessage(hwnd, WM_NCPAINT, 0, 0);
         overButton = True;
         break;
      }
   }

   if(overButton)
   {
      sysButtonClicked(i);
   }
#endif

   int id = getSystemButtonID(x, y);

//    int id = -1;
//    if(codeHitTest == HTREDUCE)
//    {
//       id = SystemButton::Min;
//    }
//    else if(codeHitTest == HTZOOM)
//    {
//       id = SystemButton::Max;
//    }
//    else if(codeHitTest == HTSYSMENU)
//    {
//       id = SystemButton::Close;
//    }

   ASSERT(id == -1 || id < SystemButton::HowMany);

   if(id != -1)
   {
      d_sysButtons[id].d_selected = False;
      redrawCaption();
      // SendMessage(hwnd, WM_NCPAINT, 0, 0);
      sysButtonClicked(static_cast<SystemButton::Type>(id));
   }
   else
      FORWARD_WM_NCLBUTTONUP(hwnd, x, y, codeHitTest, defProc);
   // defProc(hwnd, WM_NCLBUTTONDOWN, static_cast<WPARAM>(codeHitTest), MAKELPARAM(x, y));
}

/*
 * Default onClose prompts user and posts Quit Message
 */

void RealMainWindow::onClose(HWND hWnd)
{
// Boolean doQuit = False;
   Boolean oldPause = GameControl::pause(True);

//    if(Options::get(OPT_AlertBox) && AlertBoxOptions::get(AB_ExitGame))
//    {
//      long val = alertBox(APP::getMainHWND(), InGameText::get(IDS_SureExitGame),
//                        InGameText::get(IDS_ExitGame), MB_OKCANCEL);
//
//      AlertBoxOptions::set(AB_ExitGame, (Boolean)HIWORD(val));
//      doQuit = (LOWORD(val) == IDOK);
//    }
//    else
//      doQuit = True;

     int val = alertBox(APP::getMainHWND(), InGameText::get(IDS_SureExitGame),
                        InGameText::get(IDS_ExitGame), MB_OKCANCEL,
                        AB_ExitGame,
                        IDOK);

// if(doQuit)
   if(val == IDOK)
   {
#ifdef DEBUG
      debugLog("really quitting\n");
#endif
      PostQuitMessage(0);
   }
   else
   {
      GameControl::pause(oldPause);
#ifdef DEBUG
      debugLog("Not quitting\n");
#endif
   }
}

/*
 * onCommand Message
 * If subclassed window doesn't process it, it sends it on down to here
 */

void RealMainWindow::onCommand(HWND hWnd, int id, HWND hwndCtl, UINT codeNotify)
{
   switch(id)
   {
   case IDM_EXIT:
      SendMessage(hWnd, WM_CLOSE, 0, 0);
      break;

   case IDM_ABOUT:
      aboutDialog();
      break;

   case IDM_LOADGAME: {
      WargameMessage::postLoadGame();
     break;
      }

   case IDM_HELP_CONTENTS:
      {
         if(scenario->getHelpFileName())
         {
//            BOOL bResult = WinHelp(hWnd, scenario->getHelpFileName(), HELP_FINDER, 0L);
//          ASSERT(bResult != FALSE);
         }
         else
         {
            messageBox(InGameText::get(IDS_MissingFile), InGameText::get(IDS_HelpFileNotAvailable), MB_ICONINFORMATION | MB_OK);
         }
      }
      break;

   case IDM_HELP_HISTORY:
      {
         if(scenario->getHistoryFileName())
         {
//            BOOL bResult = WinHelp(hWnd, scenario->getHistoryFileName(), HELP_FINDER, 0L);
//            ASSERT(bResult != FALSE);
         }
         else
         {
            messageBox(InGameText::get(IDS_MissingFile), InGameText::get(IDS_ReferenceNotAvailable), MB_ICONINFORMATION | MB_OK);
         }
      }
     break;

     /*
     * Link user to publisher's Wargamer Web page
     */
   case IDM_WEB: {
      char win_dir[MAX_PATH];
      GetWindowsDirectory(win_dir, MAX_PATH);

      SHELLEXECUTEINFO info;
      info.cbSize = sizeof(SHELLEXECUTEINFO);

      info.fMask = 0;
      info.hwnd = getHWND();
      info.lpVerb = "Open";
      info.lpFile = "http://www.empire.co.uk/wargamer";
      info.lpParameters = NULL;
      info.lpDirectory = win_dir;
      info.nShow = SW_SHOW;

      BOOL retval = ShellExecuteEx(&info);
      break;
   }

#ifdef DEBUG
   case IDM_DEBUG:
      throw GeneralError("Forced Error\n");
#endif
   }
}

/*-------------------------------------------------------------
 * Access to Main Window
 */

MainWindow::MainWindow() :
  d_mw(0),
  d_hMain(0)
{
   d_mw = new RealMainWindow;
   ASSERT(d_mw);

   d_hMain = d_mw->getHWND();
   ASSERT(d_hMain);
}

MainWindow::~MainWindow()
{
   ASSERT(d_mw);
   ASSERT(d_hMain);

   // DestroyWindow(d_hMain);

   d_hMain = 0;
   delete d_mw;
   d_mw = 0;
}

/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.2  2001/06/18 08:25:45  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */

