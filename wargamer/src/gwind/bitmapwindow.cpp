/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			      *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								            *
 *----------------------------------------------------------------------------*/
#include "stdinc.hpp"
#include "BitmapWindow.hpp"

/*

  Filename : BitmapWindow.cpp

  Description : Implements a simple window w. H & V scrollbars, to display a given bitmap in

*/


// #include "wind.hpp"
#include "fonts.hpp"
// #include "generic.hpp"
#include "app.hpp"
#include "scenario.hpp"
#include "bmp.hpp"
#include "DIB.hpp"
// #include "DIB_util.hpp"
#include "DIB_blt.hpp"
#include "palette.hpp"
#include <memory>
// #include "cbutton.hpp"
#include "cscroll.hpp"
#include "scn_img.hpp"


BitmapWindow::BitmapWindow(HWND hparent, RECT * rect, DIB * bitmap) {

	ASSERT(hparent);
	ASSERT(bitmap);

	d_hparent = hparent;

	d_bkDIB = bitmap;
	d_fillDIB = NULL;
	d_fillCol = RGB(0,0,0);
	d_screenDIB = NULL;

	d_bitmapWidth = bitmap->getWidth();
	d_bitmapHeight= bitmap->getHeight();

    createWindow(
        WS_EX_LEFT,
        "BitmapWindow",
        WS_CHILD | WS_CLIPSIBLINGS,
        rect->left, rect->top, rect->right, rect->bottom,
        hparent,
        NULL); //APP::instance() );

	ASSERT(getHWND());

    SetWindowText(getHWND(),"BitmapWindow");

	// init custom scrollbar
	initCustomScrollBar(APP::instance());

	d_scrollBarWidth = GetSystemMetrics(SM_CXVSCROLL);
	d_scrollBarHeight = GetSystemMetrics(SM_CXHSCROLL);

	// create scroll bars
	d_VScrollBar = csb_create(
		0,
		WS_CHILD | SBS_VERT,
		(rect->right - d_scrollBarWidth), 0,
		d_scrollBarWidth, (rect->bottom - d_scrollBarHeight),
		getHWND(),
		APP::instance()
	);
	ASSERT(d_VScrollBar);

	d_HScrollBar = csb_create(
		0,
		WS_CHILD | SBS_HORZ,
		0, (rect->bottom - d_scrollBarHeight),
		(rect->right - d_scrollBarWidth), d_scrollBarHeight,
		getHWND(),
		APP::instance()
	);
	ASSERT(d_HScrollBar);

	// set scroll bar attributes
	csb_setButtonIcons(d_VScrollBar, ScenarioImageLibrary::get(ScenarioImageLibrary::VScrollButtons));
	csb_setThumbBk(d_VScrollBar, scenario->getScenarioBkDIB(Scenario::BackPatterns::VScrollThumbBackground));
	csb_setScrollBk(d_VScrollBar, scenario->getScenarioBkDIB(Scenario::BackPatterns::VScrollBackground));
	csb_setBorder(d_VScrollBar, &scenario->getBorderColors());

	csb_setButtonIcons(d_HScrollBar, ScenarioImageLibrary::get(ScenarioImageLibrary::HScrollButtons));
	csb_setThumbBk(d_HScrollBar, scenario->getScenarioBkDIB(Scenario::BackPatterns::HScrollThumbBackground));
	csb_setScrollBk(d_HScrollBar, scenario->getScenarioBkDIB(Scenario::BackPatterns::HScrollBackground));
	csb_setBorder(d_HScrollBar, &scenario->getBorderColors());

	// set scroll bar parameters
	csb_setPosition(d_VScrollBar, 0);//d_bitmapHeight/2);
	csb_setPosition(d_HScrollBar, 0);//d_bitmapWidth/2);

	d_VScrollActive = false;
	d_HScrollActive = false;

	setupScrollBars();
	drawScreenDIB();
}



BitmapWindow::~BitmapWindow(void) {

	selfDestruct();

	if(d_screenDIB) delete d_screenDIB;
	// we don't delete the bkDIB, cause that was supplied
}



void
BitmapWindow::setSize(RECT * rect) {


	MoveWindow(getHWND(), rect->left, rect->top, rect->right, rect->bottom, FALSE);

	// set scroll bar parameters
	csb_setPosition(d_VScrollBar, 0);//d_bitmapHeight/2);
	csb_setPosition(d_HScrollBar, 0);//d_bitmapWidth/2);

	setupScrollBars();

    drawScreenDIB();

	InvalidateRect(getHWND(), NULL, FALSE);
}



void
BitmapWindow::setBitmap(DIB * bitmap) {

	ASSERT(bitmap);

	d_bkDIB = bitmap;

	d_bitmapWidth = bitmap->getWidth();
	d_bitmapHeight= bitmap->getHeight();

	// set scroll bar parameters
//	csb_setPosition(d_VScrollBar, 0);//d_bitmapHeight/2);
//	csb_setPosition(d_HScrollBar, 0);//d_bitmapWidth/2);

	setupScrollBars();

//	drawScreenDIB();

//	InvalidateRect(getHWND(), NULL, FALSE);
}

void
BitmapWindow::setFillCol(COLORREF col) {

	d_fillCol = col;

	drawScreenDIB();

	InvalidateRect(getHWND(), NULL, FALSE);
}

void
BitmapWindow::setFillDIB(DrawDIB * dib) {

	d_fillDIB = dib;

	drawScreenDIB();

	InvalidateRect(getHWND(), NULL, FALSE);
}


void
BitmapWindow::showWindow(bool show) {

	if(show) ShowWindow(getHWND(), SW_SHOW);
	else ShowWindow(getHWND(), SW_HIDE);
}


void BitmapWindow::centerBitmap(void) {

//	int min,max,mid;

	RECT rect;
	GetClientRect(getHWND(), &rect);
	int window_width = rect.right - rect.left;
	int window_height = rect.bottom - rect.top;

	int x_offset = d_bitmapWidth - window_width;
	if(x_offset > 0) x_offset /= 2;
	else x_offset = 0;

	int y_offset = d_bitmapHeight - window_height;
	if(y_offset > 0) y_offset /= 2;
	else y_offset = 0;

	csb_setPosition(d_HScrollBar, x_offset);
	csb_setPosition(d_VScrollBar, y_offset);

	drawScreenDIB();
	InvalidateRect(getHWND(), NULL, FALSE);
}


void
BitmapWindow::positionBitmap(int x, int y) {

	csb_setPosition(d_VScrollBar, x);
	csb_setPosition(d_HScrollBar, y);

	drawScreenDIB();
	InvalidateRect(getHWND(), NULL, FALSE);
}


void
BitmapWindow::setupScrollBars(void) {

	ASSERT(d_VScrollBar);
	ASSERT(d_HScrollBar);


	RECT clientRect;
	GetClientRect(getHWND(), &clientRect);


	// do VScrollBar
	if(clientRect.bottom < d_bitmapHeight) {

		d_VScrollActive = true;

		csb_setRange(d_VScrollBar, 0, d_bitmapHeight);
		csb_setPage(d_VScrollBar, (clientRect.bottom - d_scrollBarHeight));

		MoveWindow(
			d_VScrollBar,
			(clientRect.right - d_scrollBarWidth), 0,
			d_scrollBarWidth, (clientRect.bottom - d_scrollBarHeight),
			TRUE
		);

		ShowWindow(d_VScrollBar, SW_SHOW);
	}
	else {
		d_VScrollActive = false;
		ShowWindow(d_VScrollBar, SW_HIDE);
	}

	// do HScrollBar
	if(clientRect.right < d_bitmapWidth) {

		d_HScrollActive = true;

		csb_setRange(d_HScrollBar, 0, d_bitmapWidth);
		csb_setPage(d_HScrollBar, (clientRect.right - d_scrollBarWidth));

		MoveWindow(
			d_HScrollBar,
			0, (clientRect.bottom - d_scrollBarHeight),
			(clientRect.right - d_scrollBarWidth), d_scrollBarHeight,
			TRUE
		);

		ShowWindow(d_HScrollBar, SW_SHOW);
	}
	else {
		d_HScrollActive = false;
		ShowWindow(d_HScrollBar, SW_HIDE);
	}


	// readjust if one scroll-bar is missing
	if(d_VScrollActive && (!d_HScrollActive)) {

		csb_setPage(d_VScrollBar, clientRect.bottom);

		MoveWindow(
			d_VScrollBar,
			(clientRect.right - d_scrollBarWidth), 0,
			d_scrollBarWidth, (clientRect.bottom),
			TRUE
		);
	}

	if(d_HScrollActive && (!d_VScrollActive)) {

		csb_setPage(d_HScrollBar, clientRect.right);

		MoveWindow(
			d_HScrollBar,
			0, (clientRect.bottom - d_scrollBarHeight),
			(clientRect.right), d_scrollBarHeight,
			TRUE
		);
	}

	// if both are active, we gotta fill in that annoying little bit between the two bars
	if(d_HScrollActive && d_VScrollActive) {

		d_scrollBitRect.left = clientRect.right - d_scrollBarWidth;
		d_scrollBitRect.top = clientRect.bottom - d_scrollBarHeight;
		d_scrollBitRect.right = clientRect.right;
		d_scrollBitRect.bottom = clientRect.bottom;
	}



}



void
BitmapWindow::drawScreenDIB(void) {

	if(!d_bkDIB) return;

	RECT clientRect;
	GetClientRect(getHWND(), &clientRect);

	int clientWidth = clientRect.right;
	if(d_VScrollActive) clientWidth -= d_scrollBarWidth;

	int clientHeight = clientRect.bottom;
	if(d_HScrollActive) clientHeight -= d_scrollBarHeight;

	if(d_screenDIB) delete d_screenDIB;
	d_screenDIB = new DrawDIBDC(clientWidth, clientHeight);

	if(d_bitmapWidth < clientWidth || d_bitmapHeight < clientHeight) {
		if(d_fillDIB) d_screenDIB->fill(d_fillDIB);
		else d_screenDIB->fill(d_screenDIB->getColour(d_fillCol));
	}

	int xDest;
	if(d_bitmapWidth < clientWidth) xDest = (clientWidth/2) - (d_bitmapWidth/2);
	else xDest = 0;

	int yDest;
	if(d_bitmapHeight < clientHeight) yDest = (clientHeight/2) - (d_bitmapHeight/2);
	else yDest = 0;

	int xSrc;
	if(d_HScrollActive) xSrc = csb_getPosition(d_HScrollBar);
	else xSrc = 0;

	int ySrc;
	if(d_VScrollActive) ySrc = csb_getPosition(d_VScrollBar);
	else ySrc = 0;

	int blitWidth;
	if(d_bitmapWidth < clientWidth) blitWidth = d_bitmapWidth;
	else blitWidth = clientWidth;

	int blitHeight;
	if(d_bitmapHeight < clientHeight) blitHeight = d_bitmapHeight;
	else blitHeight = clientHeight;

	d_screenDIB->blit(
		xDest, yDest,
		blitWidth, blitHeight,
		d_bkDIB,
		xSrc, ySrc
	);

}


LRESULT
BitmapWindow::procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam) {

    switch(msg) {

        HANDLE_MSG(hWnd, WM_SIZE, onSize);
        HANDLE_MSG(hWnd, WM_PAINT, onPaint);
		HANDLE_MSG(hWnd, WM_HSCROLL, onHScroll);
		HANDLE_MSG(hWnd, WM_VSCROLL, onVScroll);

        default:
            return defProc(hWnd, msg, wParam, lParam);
    }
}



void
BitmapWindow::onSize(HWND hwnd, UINT state, int cx, int cy) {

}


void
BitmapWindow::onPaint(HWND hwnd) {

    PAINTSTRUCT ps;
    HDC hdc = BeginPaint(hwnd, &ps);
    HPALETTE oldPal = SelectPalette(hdc, Palette::get(), FALSE);
    RealizePalette(hdc);

    // blit screen DIB to window
	if(d_screenDIB) {
		BitBlt(hdc,0,0,d_screenDIB->getWidth(),d_screenDIB->getHeight(),d_screenDIB->getDC(),0,0,SRCCOPY);
	}

	// fill in that annoying bit between the two scroolbars
	if(d_HScrollActive && d_VScrollActive) {
		FillRect(
			hdc,
			&d_scrollBitRect,
			(HBRUSH) GetStockObject(DKGRAY_BRUSH)
		);
	}

    SelectPalette(hdc, oldPal, FALSE);
    EndPaint(hwnd, &ps);

}


void
BitmapWindow::onVScroll(HWND hwnd, HWND hwndCtl, UINT code, int pos) {

	RECT clientRect;
	GetClientRect(getHWND(), &clientRect);

	switch(code) {

		case SB_PAGEUP : {
			int inc = csb_getPage(d_VScrollBar);
			pos -= inc;
			if(pos<0) pos=0;
			csb_setPosition(d_VScrollBar, pos);

			drawScreenDIB();
			InvalidateRect(getHWND(), NULL, FALSE);
			break;
		}
		case SB_PAGEDOWN : {
			int inc = csb_getPage(d_VScrollBar);
			pos += inc;

			int max = d_bitmapHeight - clientRect.bottom;
			if(d_HScrollActive) max += d_scrollBarHeight;
			if(pos>max) pos = max;

			csb_setPosition(d_VScrollBar, pos);

			drawScreenDIB();
			InvalidateRect(getHWND(), NULL, FALSE);
			break;
		}
		case SB_LINEUP : {
			int inc = d_bitmapHeight / 64;
			pos -= inc;
			if(pos<0) pos=0;
			csb_setPosition(d_VScrollBar, pos);

			drawScreenDIB();
			InvalidateRect(getHWND(), NULL, FALSE);
			break;
		}
		case SB_LINEDOWN : {
			int inc = d_bitmapHeight / 64;
			pos += inc;

			int max = d_bitmapHeight - clientRect.bottom;
			if(d_HScrollActive) max += d_scrollBarHeight;
			if(pos>max) pos = max;

			csb_setPosition(d_VScrollBar, pos);

			drawScreenDIB();
			InvalidateRect(getHWND(), NULL, FALSE);
			break;
		}
		case SB_THUMBPOSITION : {
			csb_setPosition(d_VScrollBar, pos);
			drawScreenDIB();
			InvalidateRect(getHWND(), NULL, FALSE);
			break;
		}
		default : {
			return;
		}
	}
}


void
BitmapWindow::onHScroll(HWND hwnd, HWND hwndCtl, UINT code, int pos) {

	RECT clientRect;
	GetClientRect(getHWND(), &clientRect);

	switch(code) {

		case SB_PAGEUP : {
			int inc = csb_getPage(d_HScrollBar);
			pos -= inc;
			if(pos<0) pos=0;
			csb_setPosition(d_HScrollBar, pos);

			drawScreenDIB();
			InvalidateRect(getHWND(), NULL, FALSE);
			break;
		}
		case SB_PAGEDOWN : {
			int inc = csb_getPage(d_HScrollBar);
			pos += inc;

			int max = d_bitmapWidth - clientRect.right;
			if(d_VScrollActive) max += d_scrollBarWidth;
			if(pos>max) pos = max;

			csb_setPosition(d_HScrollBar, pos);

			drawScreenDIB();
			InvalidateRect(getHWND(), NULL, FALSE);
			break;
		}
		case SB_LINEUP : {
			int inc = d_bitmapWidth / 64;
			pos -= inc;
			if(pos<0) pos=0;
			csb_setPosition(d_HScrollBar, pos);

			drawScreenDIB();
			InvalidateRect(getHWND(), NULL, FALSE);
			break;
		}
		case SB_LINEDOWN : {
			int inc = d_bitmapWidth / 64;
			pos += inc;
			
			int max = d_bitmapWidth - clientRect.right;
			if(d_VScrollActive) max += d_scrollBarWidth;
			if(pos>max) pos = max;

			csb_setPosition(d_HScrollBar, pos);

			drawScreenDIB();
			InvalidateRect(getHWND(), NULL, FALSE);
			break;
		}
		case SB_THUMBPOSITION : {
			csb_setPosition(d_HScrollBar, pos);
			drawScreenDIB();
			InvalidateRect(getHWND(), NULL, FALSE);
			break;
		}
		default : {
			return;
		}
	}
}




/*----------------------------------------------------------------------------
 * $Log$
 *----------------------------------------------------------------------------
 */

