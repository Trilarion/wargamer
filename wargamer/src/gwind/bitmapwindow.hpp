/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef BITMAPWINDOW_HPP
#define BITMAPWINDOW_HPP

#include "wind.hpp"
#include "gwind.h"

/*

  Filename : BitmapWindow.hpp

  Description : Implements a simple window w. H & V scrollbars, to display a given bitmap in

*/

class DIB;
class DrawDIBDC;


class GWIND_DLL BitmapWindow : public WindowBaseND {

public:

	BitmapWindow(HWND hparent, RECT * rect, DIB * bitmap);
	~BitmapWindow(void);

	void setSize(RECT * rect);
	void setBitmap(DIB * bitmap);
	void setFillCol(COLORREF col);
	void setFillDIB(DrawDIB * dib);
	void showWindow(bool show);
	void centerBitmap(void);
	void positionBitmap(int x, int y);

private:

	HWND d_hparent;
    DIB * d_bkDIB;
    DrawDIBDC * d_screenDIB;
	DrawDIB * d_fillDIB;
	COLORREF d_fillCol;

	int d_bitmapWidth;
	int d_bitmapHeight;

	int d_scrollBarWidth;
	int d_scrollBarHeight;

	HWND d_VScrollBar;
	int d_VScrollPos;
	bool d_VScrollActive;

	HWND d_HScrollBar;
	int d_HScrollPos;
	bool d_HScrollActive;

	RECT d_scrollBitRect;
	
	void setupScrollBars(void);
	void drawScreenDIB(void);

	LRESULT procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
	void onSize(HWND hwnd, UINT state, int cx, int cy);
	void onPaint(HWND hwnd);
	void onVScroll(HWND hwnd, HWND hwndCtl, UINT code, int pos);
	void onHScroll(HWND hwnd, HWND hwndCtl, UINT code, int pos);

};



#endif
