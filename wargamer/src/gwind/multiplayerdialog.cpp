/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#include "stdinc.hpp"
#include "MultiPlayerDialog.hpp"


/*

  Filename : MultiPlayerDialog.hpp

  Description : Dialog class for setting up a multiplayer connection.

*/

#include <stdio.h>

#include "app.hpp"
#include "resource.h"
#include "MultiplayerConnection.hpp"




CMultiPlayerDialog::CMultiPlayerDialog(HWND parent) {

	d_user = 0;

	g_directPlay->enumerateConnections();

	d_mode = Host;
	d_hostGameParams.GameName = 0;
	d_hostGameParams.MaxPlayers = 0;
	d_hostGameParams.Password = 0;

	initDialog(parent);
}


CMultiPlayerDialog::~CMultiPlayerDialog(void) {

	destroyDialog();

}


void
CMultiPlayerDialog::reinitDirectPlay(void) {

	g_directPlay->destroyObject();
	g_directPlay->createObject();
}




bool
CMultiPlayerDialog::initDialog(HWND parent) {

	/*
	Start DirectPlay Main Dialog
	*/
	d_mainDialogHwnd = CreateDialog(
		APP::instance(),
		MAKEINTRESOURCE(IDD_MULTIPLAYERDIALOG),
		parent,
		(DLGPROC) DPlayDialogProc
	);
	if(!d_mainDialogHwnd) return false;
	SetWindowLong(d_mainDialogHwnd, GWL_USERDATA, (LONG)this);
	ShowWindow(d_mainDialogHwnd, SW_SHOW);

	/*
	Start DirectPlay Host Dialog, as child of main dialog
	*/
	d_hostDialogHwnd = CreateDialog(
		APP::instance(),
		MAKEINTRESOURCE(IDD_HOSTDIALOG),
		d_mainDialogHwnd,
		(DLGPROC) DPlayDialogProc
	);
	if(!d_hostDialogHwnd) return false;
	SetWindowLong(d_hostDialogHwnd, GWL_USERDATA, (LONG)this);
	ShowWindow(d_hostDialogHwnd, SW_SHOW);

	/*
	Start DirectPlay Join Dialog, as child of main dialog
	*/
	d_joinDialogHwnd = CreateDialog(
		APP::instance(),
		MAKEINTRESOURCE(IDD_JOINDIALOG),
		d_mainDialogHwnd,
		(DLGPROC) DPlayDialogProc
	);
	if(!d_joinDialogHwnd) return false;
	SetWindowLong(d_joinDialogHwnd, GWL_USERDATA, (LONG)this);
	ShowWindow(d_joinDialogHwnd, SW_HIDE);

	/*
	Set default state to HOST
	*/
	HWND hbutton = GetDlgItem(d_mainDialogHwnd, IDC_RADIO_HOSTGAME);
	SendMessage(hbutton, BM_SETCHECK, (WPARAM) BST_CHECKED, 0);

	hbutton = GetDlgItem(d_mainDialogHwnd, IDOK);
	EnableWindow(hbutton, FALSE);

	/*
	Init combos
	*/

	d_lastConnectionIndex = -1;
	reinitCombos();

	/*
	Set text limits on edit controls
	*/
	HWND hedit = GetDlgItem(d_hostDialogHwnd, IDC_EDIT_GAMENAME);
	SendMessage(hedit, EM_SETLIMITTEXT, (WPARAM) 64, (LPARAM) 0);
//	hedit = GetDlgItem(d_hostDialogHwnd, IDC_EDIT_MAXPLAYERS);
//	SendMessage(hedit, EM_SETLIMITTEXT, (WPARAM) 2, (LPARAM) 0);
//	hedit = GetDlgItem(d_hostDialogHwnd, IDC_EDIT_HOSTPASSWORD);
//	SendMessage(hedit, EM_SETLIMITTEXT, (WPARAM) 16, (LPARAM) 0);

	/*
	Show dialogs
	*/
	ShowWindow(d_mainDialogHwnd, SW_SHOW);
	ShowWindow(d_hostDialogHwnd, SW_SHOW);
	ShowWindow(d_joinDialogHwnd, SW_HIDE);

	return TRUE;
}



void
CMultiPlayerDialog::destroyDialog(void) {

	ShowWindow(d_mainDialogHwnd, SW_HIDE);
	ShowWindow(d_hostDialogHwnd, SW_HIDE);
	ShowWindow(d_joinDialogHwnd, SW_HIDE);

	DestroyWindow(d_hostDialogHwnd);
	d_hostDialogHwnd = 0;
	DestroyWindow(d_joinDialogHwnd);
	d_joinDialogHwnd = 0;
	DestroyWindow(d_mainDialogHwnd);
	d_mainDialogHwnd = 0;

}


void
CMultiPlayerDialog::reinitCombos(void) {

	HWND hcombo = GetDlgItem(d_mainDialogHwnd, IDC_COMBO_CONNECTION);
	SendMessage(hcombo, CB_RESETCONTENT, 0, 0);

	unsigned int index=0;

	if(CDirectPlay::connectionsList().first()) {

		do {

			DPlayConnectionDesc * desc = CDirectPlay::connectionsList().getCurrent();
			SendMessage(hcombo, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) desc->Name);
			SendMessage(hcombo, CB_SETITEMDATA, (WPARAM) index, (LPARAM) (DWORD) desc);
			index++;

		} while(CDirectPlay::connectionsList().next());
	}

	if(d_lastConnectionIndex >= 0) SendMessage(hcombo, CB_SETCURSEL, (WPARAM) d_lastConnectionIndex, (LPARAM) 0);
	else SendMessage(hcombo, CB_SETCURSEL, (WPARAM) 0, (LPARAM) 0);

	int n = SendMessage(hcombo, CB_GETCURSEL, (WPARAM) 0, 0);
	DPlayConnectionDesc * desc = (DPlayConnectionDesc *) SendMessage(hcombo, CB_GETITEMDATA, (WPARAM) n, 0);
	CDirectPlay::selectedConnection(desc);
}



bool
CMultiPlayerDialog::validateDialogEntries(void) {

	if(d_mode == Host) {

		HWND hCtrl = GetDlgItem(d_hostDialogHwnd, IDC_EDIT_GAMENAME);
		int numch_name = SendMessage(hCtrl, EM_LINELENGTH, (WPARAM) 0, (LPARAM) 0);

//		hCtrl = GetDlgItem(d_hostDialogHwnd, IDC_EDIT_MAXPLAYERS);
//		int numch_maxpl = SendMessage(hCtrl, EM_LINELENGTH, (WPARAM) 0, (LPARAM) 0);

//		char tmp[1024];
//		tmp[0] = 0xFF;
//		SendMessage(hCtrl, EM_GETLINE, (WPARAM) 0, (LPARAM) tmp);
//		tmp[numch_maxpl] = 0;
//		int val = atoi(tmp);
//		int maxpl = val;

//		if(numch_name > 0 && maxpl > 1) return true;
//		else return false;
      return (numch_name > 0);
	}

	else {

		HWND hCtrl = GetDlgItem(d_joinDialogHwnd, IDC_LIST_GAMESFOUND);
		int list_num = SendMessage(hCtrl, LB_GETCURSEL, (WPARAM) 0, (LPARAM) 0);
		return(list_num >= 0);
	}
}



void
CMultiPlayerDialog::refreshSessionsList(void) {

	HWND hcombo = GetDlgItem(d_mainDialogHwnd, IDC_COMBO_CONNECTION);
	unsigned int n = SendMessage(hcombo, CB_GETCURSEL, (WPARAM) 0, 0);

	DPlayConnectionDesc * connection_desc = (DPlayConnectionDesc *) SendMessage(hcombo, CB_GETITEMDATA, (WPARAM) n, 0);

	g_directPlay->selectedConnection(connection_desc);
	g_directPlay->connect();

	HWND hlistbox = GetDlgItem(d_joinDialogHwnd, IDC_LIST_GAMESFOUND);
	SendMessage(hlistbox, LB_RESETCONTENT, 0, 0);

	g_directPlay->enumerateSessions();

	unsigned int index = 0;

	if(CDirectPlay::sessionsList().first()) {

		do {

			DPlaySessionDesc * desc = CDirectPlay::sessionsList().getCurrent();

			SendMessage(hlistbox, LB_ADDSTRING, 0, (LPARAM) (LPCTSTR) desc->Desc);
			SendMessage(hlistbox, LB_SETITEMDATA, (WPARAM) index, (LPARAM) (DWORD) desc);

			index++;

		} while(CDirectPlay::sessionsList().next());

		SendMessage(hlistbox, LB_SETCURSEL, (WPARAM) 0, (LPARAM) 0);

		// fill in caps details
		n = 0;
		DPlaySessionDesc * session_desc = (DPlaySessionDesc *) SendMessage(hlistbox, LB_GETITEMDATA, (WPARAM) n, 0);
		CDirectPlay::selectedSession(session_desc);
		getSessionCaps(session_desc);
	}

	else {

		CDirectPlay::selectedSession(NULL);
		SetDlgItemText(d_joinDialogHwnd, IDC_EDIT_GAMEINFO, NULL);
	}
}


void
CMultiPlayerDialog::getSessionCaps(DPlaySessionDesc * desc) {

	SessionCaps caps(desc->Flags);

	char players[64];
	sprintf(players, "Players : (%i/%i)", desc->NumPlayers, desc->MaxPlayers);

	char password[64];
	if(caps.PasswordRequired) sprintf(password, "PASSWORD REQUIRED");
	else sprintf(password, "NO PASSWORD");

	char join[64];
	if(caps.JoinDisabled) sprintf(join, "JOIN DISABLED");
	else sprintf(join, "JOINABLE");

	char newplayers[64];
	if(caps.NewPlayersDisabled) sprintf(newplayers, "NO NEW PLAYERS");
	else sprintf(newplayers, "PLAYERS ALLOWED");

	sprintf(session_text, "%s, %s, %s, %s", players, password, join, newplayers);

	SetDlgItemText(d_joinDialogHwnd, IDC_EDIT_GAMEINFO, session_text);
}



BOOL CALLBACK
DPlayDialogProc(HWND hwndDlg, UINT uMsg, WPARAM wParam, LPARAM lParam) {

	CMultiPlayerDialog * objectptr = (CMultiPlayerDialog *) GetWindowLong(hwndDlg, GWL_USERDATA);

	switch(uMsg) {

		case WM_INITDIALOG : {
			return TRUE;
		}

		case WM_ACTIVATE : {
			UINT state = LOWORD(wParam);
			if((state == WA_ACTIVE) || (state == WA_CLICKACTIVE))
				APP::setActiveDial(hwndDlg);
			else if(state == WA_INACTIVE)
				APP::clearActiveDial(hwndDlg);
			return TRUE;
		}

		case WM_COMMAND : {

			int wNotifyCode = HIWORD(wParam);
			int wID = LOWORD(wParam);
			HWND hCtrl = (HWND) lParam;

			/*
			Notifiy Code
			*/
			switch (wNotifyCode) {

				/*
				Edit controls
				*/
				case EN_CHANGE : {

					switch(wID) {

						case IDC_EDIT_GAMENAME : {
							if(objectptr) {
								int numch = SendMessage(hCtrl, EM_LINELENGTH, (WPARAM) 0, (LPARAM) 0);
								if(objectptr->d_hostGameParams.GameName) {
									free(objectptr->d_hostGameParams.GameName);
									objectptr->d_hostGameParams.GameName = 0;
								}
								char tmp[1024];  
                                                                tmp[0] = -1; // static_cast<char>(0xff);
								SendMessage(hCtrl, EM_GETLINE, (WPARAM) 0, (LPARAM) tmp);
								tmp[numch] = 0;
								objectptr->d_hostGameParams.GameName = strdup(tmp);
							}
							break;
						}
#if 0
						case IDC_EDIT_MAXPLAYERS : {
							if(objectptr) {
								int numch = SendMessage(hCtrl, EM_LINELENGTH, (WPARAM) 0, (LPARAM) 0);
								char tmp[1024];  tmp[0] = 0xFF;
								SendMessage(hCtrl, EM_GETLINE, (WPARAM) 0, (LPARAM) tmp);
								tmp[numch] = 0;
								int val = atoi(tmp);
								objectptr->d_hostGameParams.MaxPlayers = val;
							}
							break;
						}
						case IDC_EDIT_HOSTPASSWORD : {
							if(objectptr) {
								int numch = SendMessage(hCtrl, EM_LINELENGTH, (WPARAM) 0, (LPARAM) 0);
								if(objectptr->d_hostGameParams.Password) {
									free(objectptr->d_hostGameParams.Password);
									objectptr->d_hostGameParams.Password = 0;
								}
								char tmp[1024];  tmp[0] = 0xFF;
								SendMessage(hCtrl, EM_GETLINE, (WPARAM) 0, (LPARAM) tmp);
								tmp[numch] = 0;
								objectptr->d_hostGameParams.Password = strdup(tmp);
							}
							break;
						}
#endif
					}
					if(objectptr) {
						EnableWindow(GetDlgItem(objectptr->d_mainDialogHwnd, IDOK), objectptr->validateDialogEntries());
					}
					return TRUE;
				}

				/*
				Buttons
				*/
				case BN_CLICKED : {

					switch(wID) {

						case IDC_RADIO_HOSTGAME : {
							if(objectptr) {
								objectptr->d_mode = CMultiPlayerDialog::Host;
								ShowWindow(objectptr->d_hostDialogHwnd, SW_SHOW);
								ShowWindow(objectptr->d_joinDialogHwnd, SW_HIDE);
							}
							break;
						}
						case IDC_RADIO_JOINGAME : {
							if(objectptr) {
								objectptr->d_mode = CMultiPlayerDialog::Join;
								ShowWindow(objectptr->d_joinDialogHwnd, SW_SHOW);
								ShowWindow(objectptr->d_hostDialogHwnd, SW_HIDE);
							}
							break;
						}
						case IDC_BUTTON_REFRESH : {
							if(objectptr) {
								objectptr->refreshSessionsList();
							}
							break;
						}
						case IDOK : {
							if(objectptr) {
								bool result = objectptr->startSession();
								//if(!result) PostQuitMessage(0);
							}
							break;
						}
						case IDCANCEL : {
							if(objectptr->d_user) {
								objectptr->d_user->connectionCancelled();
							}
							break;
						}
					}
					if(objectptr) {
						EnableWindow(GetDlgItem(objectptr->d_mainDialogHwnd, IDOK), objectptr->validateDialogEntries());
					}
					return TRUE;
				}

				/*
				Combo box
				*/
				case CBN_SELCHANGE : {

					switch(wID) {

						/*
						If the connection type is changed we have to
						DELETE & RECREATE THE ENTIRE DIRECT-PLAY OBJECT
						Somehow the SDK completely overlooks this little 'feature'
						*/
						case IDC_COMBO_CONNECTION : {
							if(objectptr) {
								int n = SendMessage(hCtrl, CB_GETCURSEL, (WPARAM) 0, 0);
								objectptr->d_lastConnectionIndex = n;
								objectptr->reinitDirectPlay();
								g_directPlay->enumerateConnections();
								objectptr->reinitCombos();
							}
							break;
						}

						case IDC_LIST_GAMESFOUND : {

							if(objectptr) {
								int n = SendMessage(hCtrl, LB_GETCURSEL, (WPARAM) 0, 0);
								// set this as selected session
								DPlaySessionDesc * session_desc = (DPlaySessionDesc *) SendMessage(hCtrl, LB_GETITEMDATA, (WPARAM) n, 0);
								CDirectPlay::selectedSession(session_desc);
								objectptr->getSessionCaps(session_desc);

							}
							break;
						}
					}
					return TRUE;
				}
			}
		}
	}
	return FALSE;
}






bool
CMultiPlayerDialog::startSession(void) {

	bool result;

	if(d_mode == Host) {

		HWND hcombo = GetDlgItem(d_mainDialogHwnd, IDC_COMBO_CONNECTION);
		unsigned int n = SendMessage(hcombo, CB_GETCURSEL, (WPARAM) 0, 0);
		DPlayConnectionDesc * connection_desc = (DPlayConnectionDesc *) SendMessage(hcombo, CB_GETITEMDATA, (WPARAM) n, 0);

		result = hostSession(connection_desc);
		if(!result) return false;

		result = g_directPlay->createMsgThread();
		if(!result) return false;

		g_directPlay->createPlayer();
		if(!result) return false;

		if(d_user) d_user->connectionHosting();
	}

	else {

		HWND hlistbox = GetDlgItem(d_joinDialogHwnd, IDC_LIST_GAMESFOUND);
		int n = SendMessage(hlistbox, LB_GETCURSEL, (WPARAM) 0, (LPARAM) 0);

		DPlaySessionDesc * session_desc;

		if(n >= 0) session_desc = (DPlaySessionDesc *) SendMessage(hlistbox, LB_GETITEMDATA, (WPARAM) n, (LPARAM) 0);

		else {
			session_desc = new DPlaySessionDesc;
 			ZeroMemory(&session_desc->SessionGuid, sizeof(GUID));
		}

		result = joinSession(session_desc);
		if(!result) return false;

		result = g_directPlay->createMsgThread();
		if(!result) return false;

		g_directPlay->createPlayer();
		if(!result) return false;

		g_directPlay->enumeratePlayers();
		if(!result) return false;

		if(d_user) d_user->connectionJoined();
	}

	return true;
}





bool
CMultiPlayerDialog::hostSession(DPlayConnectionDesc * connection_desc) {

	bool result;

	CDirectPlay::selectedConnection(connection_desc);

	result = g_directPlay->connect();
	if(!result) return false;

	DPlaySessionDesc session_desc;
	session_desc.MaxPlayers = d_hostGameParams.MaxPlayers;
	session_desc.Desc = d_hostGameParams.GameName;
	session_desc.Password = d_hostGameParams.Password;

	SessionCaps session_caps;
	session_caps.DirectPlayProtocol = true;
	session_caps.PasswordRequired = (session_desc.Password != 0);

	result = g_directPlay->createSession(&session_desc, &session_caps);
	if(!result) return false;

	return true;
}


bool
CMultiPlayerDialog::joinSession(DPlaySessionDesc * session_desc) {

	CDirectPlay::selectedSession(session_desc);

	bool result;

	result = g_directPlay->joinSession();

	return result;
}












