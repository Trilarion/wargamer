/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
#ifndef CHATDIALOG_HPP
#define CHATDIALOG_HPP

#include "gwind.h"

/*

  Filename : ChatDialog.hpp

  Description : multiplayer chat dialog, and connection status window

*/


class CChatDialogUser {

public:

   GWIND_DLL CChatDialogUser(void) { }
   GWIND_DLL virtual ~CChatDialogUser(void) { }

   GWIND_DLL virtual void requestDisconnect(void) = 0;
   GWIND_DLL virtual void sendChatMessage(char * text) = 0;
};





class CChatDialog {

public:

   GWIND_DLL CChatDialog(HWND hparent);
   GWIND_DLL ~CChatDialog(void);

   GWIND_DLL void registerUser(CChatDialogUser * user) { d_user = user; }
   GWIND_DLL void show(void);
   GWIND_DLL void hide(void);
   GWIND_DLL bool addMessage(char * text, bool isLocal);
   GWIND_DLL void gotoTextEnd(void);
   GWIND_DLL void setTextColours(COLORREF local_col, COLORREF remote_col);
   GWIND_DLL void setTextNames(char * local_name, char * remote_name);
   GWIND_DLL void sendMessage(char * text);


private:

   HWND d_HWND;
   CChatDialogUser * d_user;
   COLORREF d_localTextCol;
   COLORREF d_remoteTextCol;
   char * d_localName;
   char * d_remoteName;
};



BOOL CALLBACK ChatDialogProc(HWND hwndDlg, UINT uMsg, WPARAM wParam, LPARAM lParam);


#endif

/*
 *----------------------------------------------------------------------
 * $Log$
 *----------------------------------------------------------------------
 */
