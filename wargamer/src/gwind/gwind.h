/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
#ifndef GWIND_DLL_H
#define GWIND_DLL_H
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 * Included by all files in GWIND DLL
 *
 * Defines 
 *
 *
 *----------------------------------------------------------------------
 */


#if defined(NO_WG_DLL)
    #define GWIND_DLL
#elif defined(EXPORT_GWIND_DLL)
    #define GWIND_DLL __declspec(dllexport)
#else
    #define GWIND_DLL __declspec(dllimport)
#endif

#endif /* GWIND_DLL_H */

/*
 *----------------------------------------------------------------------
 * $Log$
 *----------------------------------------------------------------------
 */
