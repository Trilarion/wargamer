/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef TIMEWIND_HPP
#define TIMEWIND_HPP

#ifndef __cplusplus
#error timewind.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 * Time Window:
 * Shared between Campaign and Battle Game using abstract classes
 * to exchange Data
 *
 *----------------------------------------------------------------------
 */

#include "gwind.h"
// #include "string.hpp"
#include "mytypes.h"
#include "wind.hpp"

class PixelRect;
class CustomTrackBar;
class ImageLibrary;
class DrawDIBDC;
class DrawDIB;

class TimeControlWindow : public WindowBaseND
{
        // disable constructors and copiers

        TimeControlWindow(const TimeControlWindow&);
        TimeControlWindow& operator=(const TimeControlWindow&);

    public:
        GWIND_DLL TimeControlWindow();
        GWIND_DLL void init(HWND parent);
        GWIND_DLL virtual ~TimeControlWindow();

        GWIND_DLL int getWidth() const;
        GWIND_DLL void setPosition(LONG x, LONG y, LONG w, LONG h);
        GWIND_DLL void update(bool force);

        // void show(bool visible) { WindowBaseND::show(visible); }
        // void show() { WindowBaseND::show(); }
        // void hide() { WindowBaseND::hide(); }

        typedef int RateValue;
        typedef ULONG TimeValue;
        typedef int JumpValue;

        GWIND_DLL virtual bool isTimeFrozen() const;
        GWIND_DLL virtual void freezeTime(bool freeze);

        GWIND_DLL virtual RateValue getTimeRange() const;               // How many different rates are there?
        GWIND_DLL virtual void setRate(RateValue n);              // Set rate to value 0..getTimeRange()
        GWIND_DLL virtual RateValue getRate() const;

        GWIND_DLL virtual void setJump(JumpValue n);               // Jump time, e.g. 0=day, 1=week, 2=fortnight
        GWIND_DLL virtual bool isTimeJumping();
        GWIND_DLL virtual void stopJump();
        virtual bool hasJumps() const = 0;

        // GWIND_DLL virtual int getValue() const;                   // Value to display to user for given value
        // GWIND_DLL virtual int getValue(RateValue rate) const;     // Value to display to user for given value
        GWIND_DLL virtual TimeValue getTime() const;              // get the current date/time
        GWIND_DLL virtual String displayTime() const;             // convert time to string
        GWIND_DLL virtual String displayRate() const;               // e.g. "seconds per day"
private:
	   GWIND_DLL LRESULT procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
	   BOOL onCreate(HWND hWnd, CREATESTRUCT* lpCreateStruct);
	   void onPaint(HWND hWnd);
	   void onDestroy(HWND hWnd);
	   void onDrawItem(HWND hwnd, const DRAWITEMSTRUCT* lpDrawItem);
	   void onHScroll(HWND hwnd, HWND hwndCtl, UINT code, int pos);
	   void onCommand(HWND hwnd, int id, HWND hwndCtl, UINT codeNotify);
	   void onContextMenu(HWND hwnd, HWND hwndCtl, int x, int y);
	   LRESULT onNotify(HWND hWnd, int id, NMHDR* lpNMHDR);
	   void onSize(HWND hWnd, UINT state, int cx, int cy);
	   BOOL onEraseBk(HWND hwnd, HDC hdc);

	   void drawDate(const DRAWITEMSTRUCT* lpDrawItem);
	   void drawRate(const DRAWITEMSTRUCT* lpDrawItem);
	   void updateFreeze();

	   void setJump(JumpValue value, int id);

#ifdef DEBUG
    	void checkPalette() { }
#endif

    private:
	   HWND d_parent;
    	CustomTrackBar* d_trackBar;
	   const ImageLibrary* d_images;
	   DrawDIBDC* d_dib;
	   const DrawDIB* d_fillDib;

	   UWORD d_jumping;
	   enum { NotJumping = UWORD_MAX };
	   TimeValue d_lastDay;

};

#endif /* TIMEWIND_HPP */

