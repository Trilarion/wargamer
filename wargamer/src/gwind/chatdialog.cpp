/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#include "stdinc.hpp"
#include "ChatDialog.hpp"

/*

  Filename : ChatDialog.cpp

  Description : multiplayer chat dialog, and connection status window

*/

#include "app.hpp"
#include "resource.h"
#include "directplay.hpp"


CChatDialog::CChatDialog(HWND hparent) {

	d_HWND = 0;
	d_user = 0;
	d_localTextCol = RGB(0,0,0);
	d_localTextCol = RGB(128,128,128);
	d_localName = 0;
	d_remoteName = 0;

	d_HWND = CreateDialog(
		APP::instance(),
		MAKEINTRESOURCE(IDD_MUTLIPLAYERMSG),
		hparent,
		(DLGPROC) ChatDialogProc
	);

	SetWindowLong(d_HWND, GWL_USERDATA, (LONG)this);
	ShowWindow(d_HWND, SW_HIDE);
}


CChatDialog::~CChatDialog(void) {

	DestroyWindow(d_HWND);
	d_HWND = 0;

	if(d_localName) {
		free(d_localName);
		d_localName = 0;
	}
	if(d_remoteName) {
		free(d_remoteName);
		d_remoteName = 0;
	}
}

void
CChatDialog::show(void) {

	ShowWindow(d_HWND, SW_SHOW);
}

void
CChatDialog::hide(void) {

	ShowWindow(d_HWND, SW_HIDE);
}

bool
CChatDialog::addMessage(char * text, bool isLocal) {

	char * sender;
	if(isLocal) sender = d_localName;
	else sender = d_remoteName;

	// add sender's name to front
	int sz = strlen(sender) + strlen(text);
	char * final_text = new char[sz+1];
	strcpy(final_text, sender);
	strcat(final_text, text);

	int n = SendMessage(
		GetDlgItem(d_HWND, IDC_EDIT_HISTORY),
		WM_GETTEXTLENGTH,
		0, 0
	);

	int newsize = n + sz + 1;

	char * buffer = new char[newsize];
	GetDlgItemText(d_HWND, IDC_EDIT_HISTORY, buffer, newsize);


	strcat(buffer, final_text);
	SetDlgItemText(d_HWND, IDC_EDIT_HISTORY, buffer);

	delete[] buffer;

	gotoTextEnd();

	return true;
}


void
CChatDialog::gotoTextEnd(void) {

	int n = SendMessage(
		GetDlgItem(d_HWND, IDC_EDIT_HISTORY),
		WM_GETTEXTLENGTH,
		0, 0
	);

	SendMessage(
		GetDlgItem(d_HWND, IDC_EDIT_HISTORY),
		EM_SETSEL,
		(WPARAM) n,
		(LPARAM) n
	);

	SendMessage(
		GetDlgItem(d_HWND, IDC_EDIT_HISTORY),
		EM_SCROLLCARET,
		0, 0
	);
}


void
CChatDialog::setTextColours(COLORREF local_col, COLORREF remote_col) {

	d_localTextCol = local_col;
	d_remoteTextCol = remote_col;
}


void
CChatDialog::setTextNames(char * local_name, char * remote_name) {

	if(d_localName) {
		free(d_localName);
		d_localName = 0;
	}
	if(d_remoteName) {
		free(d_remoteName);
		d_remoteName = 0;
	}
	d_localName = strdup(local_name);
	d_remoteName = strdup(remote_name);
}


void
CChatDialog::sendMessage(char * text) {

	if(g_directPlay) {

		if(g_directPlay->getRemotePlayer() != 0) {
			g_directPlay->sendChatMessage(
				g_directPlay->getRemotePlayer(),
				text
			);
		}
	}

}



BOOL CALLBACK
ChatDialogProc(HWND hwndDlg, UINT uMsg, WPARAM wParam, LPARAM lParam) {

	CChatDialog * objectptr = (CChatDialog *) GetWindowLong(hwndDlg, GWL_USERDATA);

	switch(uMsg) {

		case WM_INITDIALOG : {
			return TRUE;
		}

		case WM_ACTIVATE : {
			UINT state = LOWORD(wParam);
			if((state == WA_ACTIVE) || (state == WA_CLICKACTIVE))
				APP::setActiveDial(hwndDlg);
			else if(state == WA_INACTIVE)
				APP::clearActiveDial(hwndDlg);
			return TRUE;
		}

		case WM_COMMAND : {

			int wNotifyCode = HIWORD(wParam);
			int wID = LOWORD(wParam);
			HWND hCtrl = (HWND) lParam;

			/*
			Notifiy Code
			*/
			switch(wNotifyCode) {

				/*
				Edit controls
				*/
				case EN_UPDATE : {

					switch(wID) {

						case IDC_EDIT_MESSAGE : {
							char buffer[1024];
							int num = GetDlgItemText(hwndDlg, IDC_EDIT_MESSAGE, buffer, 1024);
							// return pressed (LF + CR), so send string & clear buffer
							if(buffer[num-1] == 10 || buffer[num-1] == 13) {
								objectptr->sendMessage(buffer);
								objectptr->addMessage(buffer, true);
								SetDlgItemText(hwndDlg, IDC_EDIT_MESSAGE, "");
							}
							// ESC pressed, so clear buffer
							if(buffer[num-1] == 27) {
								SetDlgItemText(hwndDlg, IDC_EDIT_MESSAGE, "");
							}
							break;
						}
						case IDC_EDIT_HISTORY : {
							break;
						}
					}
				}
				/*
				Buttons
				*/
				case BN_CLICKED : {

					switch(wID) {

						case IDC_BUTTON_DISCONNECT : {
							break;
						}
					}
				}

			} // switch(wNotifyCode)

		} // WM_COMMAND

	} // switch(uMsg)

	return FALSE;
}





