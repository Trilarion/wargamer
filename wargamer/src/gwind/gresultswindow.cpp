/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 * Generic Battle Results Window
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "GResultsWindow.hpp"
#include "victory.hpp"
#include "palwind.hpp"
#include "dib.hpp"
#include "wmisc.hpp"
#include "cbutton.hpp"
#include "scrnbase.hpp"
#include "scenario.hpp"
#include "control.hpp"
#include "fonts.hpp"
#include "bmp.hpp"
#include "dib_blt.hpp"
#include "wmisc.hpp"
#include "app.hpp"
#include "colours.hpp"
#include "dc_hold.hpp"
#include "scn_res.h"
#include "imglib.hpp"
#include "gLeaderInfo.hpp"
#include "options.hpp"
#include "simpstr.hpp"
#include "resstr.hpp"
#include "random.hpp"




namespace { // private namespace

class CResultWindow :
    public CGenericResultsWindow,
    private PaletteWindow,
    private WindowBaseND
{
        enum ButtonID
        {
            ID_Continue,
            ID_Withdraw,
            #ifdef DEBUG
                ID_ShowAll
            #endif
        };

        enum
        {
            IconBorder = 8
        };

    public:
        CResultWindow(ResultWindowUser* owner);
        virtual ~CResultWindow();

        void show(bool visible) { WindowBaseND::show(visible); }

        virtual void show();
        virtual void hide();
        virtual void setPosition(LONG x, LONG y, LONG w, LONG h, DeferWindowPosition* def);
    private:
        void createWindow();
        void makeDIB();
        CustomButton createButton(const char* text, ButtonID id);

        LRESULT procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
        BOOL onCreate(HWND hwnd, CREATESTRUCT* lpCreateStruct);
        void onDestroy(HWND hwnd);
        void onSize(HWND hwnd, UINT state, int cx, int cy);
        void onPaint(HWND hwnd);
        BOOL onEraseBk(HWND hwnd, HDC hdc);
	    void onCommand(HWND hwnd, int id, HWND hwndCtl, UINT codeNotify);

        ResultWindowUser* d_owner;
        DrawDIBDC* d_dib;
        DIB* d_bkPic;
        ImageLibrary* d_dotImages;
        ImageLibrary* d_flagImages;

        CustomButton d_button1;
        CustomButton d_button2;

#ifdef DEBUG
        CustomButton d_buttonShow;
        bool d_showAll;
#endif
};


CResultWindow::CResultWindow(ResultWindowUser* owner) :
    d_owner(owner),
    d_dib(0),
    d_bkPic(0),
    d_button1(0),
    d_button2(0),
    #ifdef DEBUG
    d_buttonShow(0),
    d_showAll(false),
    #endif
    d_dotImages(0),
    d_flagImages(0)
{
    // TODO: Get names from scenario file and choose random

   /*
    * Choose a picture
    *       <scenario>\Victory\drawVicResults?.bmp
    *       <scenario>\Victory\FrenchVicResults?.bmp
    *       <scenario>\Victory\AlliedVicResults?.bmp
    */

   const BattleResults* results = d_owner->results();
   Side winner = results->winner();

    d_bkPic = GameVictory::getPicture(winner);

    Side playerSide = GamePlayerControl::getSide(GamePlayerControl::Player);
    if(playerSide == SIDE_Neutral)
      playerSide = 0;

	ImagePos* imagePos = reinterpret_cast<ImagePos*>(loadResource(scenario->getScenarioResDLL(), MAKEINTRESOURCE(PR_DOTMODEIMAGES), RT_IMAGEPOS));
	d_dotImages = scenario->getImageLibrary(MAKEINTRESOURCE(BM_DOTMODEIMAGES), DotMode_HowMany, imagePos);
    d_flagImages = scenario->getNationFlag(scenario->getGenericNation(playerSide), true);
}

CResultWindow::~CResultWindow()
{
    selfDestruct();
    delete d_dib;
    delete d_bkPic;
	delete d_dotImages;
}

void CResultWindow::show()
{
    createWindow();
    ASSERT(getHWND());
    if(getHWND())
    {
        makeDIB();
        show(true);
    }
}

void CResultWindow::hide()
{
    // createWindow();
    // ASSERT(getHWND());
    show(false);
    // if(getHWND())
    //     ShowWindow(getHWND(), SW_HIDE);

    delete d_dib;
    d_dib = 0;
}

void CResultWindow::setPosition(LONG x, LONG y, LONG w, LONG h, DeferWindowPosition* def)
{
    createWindow();

    ASSERT(getHWND());

    if(getHWND())
    {
       if(def)
       {
          def->setPos(getHWND(), HWND_TOP, x, y, w, h, SWP_NOZORDER | SWP_NOACTIVATE);
       }
       else
       {
        SetWindowPos(getHWND(), HWND_TOP,
            x, y, w, h,
            SWP_NOACTIVATE | SWP_NOZORDER);

            // SWP_SHOWWINDOW);
       }
    }
}

void CResultWindow::createWindow()
{
    if(getHWND() == NULL)
    {
        HWND hParent = d_owner->hwnd();
        // ASSERT(hParent);

	    // CustomButton::initCustomButton(wndInstance(hParent));
	    CustomButton::initCustomButton(APP::instance());

        CommonWindowBase::createWindow(
            0,                               // exStyle
            // className(),                     // className
            NULL,                            // WindowNAme
            WS_CHILD | WS_CLIPSIBLINGS,      // Style
            0,0,0,0,                         // Position
            hParent,                         // parent
            NULL                            // Menu
            // wndInstance(hParent)             // Instance
            // APP::instance()
        );
    }
}

LRESULT CResultWindow::procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
    LRESULT result;

    if(!handlePalette(hWnd, msg, wParam, lParam, result))
    {
        switch(msg)
        {
            HANDLE_MSG(hWnd, WM_CREATE,     onCreate);
            HANDLE_MSG(hWnd, WM_PAINT,      onPaint);
            HANDLE_MSG(hWnd, WM_ERASEBKGND, onEraseBk);
            HANDLE_MSG(hWnd, WM_COMMAND,    onCommand);
            HANDLE_MSG(hWnd, WM_DESTROY,    onDestroy);
            HANDLE_MSG(hWnd, WM_SIZE,		onSize);

            default:
                return defProc(hWnd, msg, wParam, lParam);
        }
    }

    return result;
}

CustomButton CResultWindow::createButton(const char* text, ButtonID id)
{
    CustomButton cb = CreateWindow(
            CUSTOMBUTTONCLASS,
            text,
            WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON,
            ScreenBase::x(0),
            ScreenBase::y(0),
            ScreenBase::x(64),
            ScreenBase::y(12),
            getHWND(),
            HMENU(id),
            wndInstance(getHWND()),
            NULL);

    cb.setFillDib(scenario->getScenarioBkDIB(Scenario::BackPatterns::ButtonBackground));
    cb.setBorderColours(scenario->getBorderColors());

    return cb;
}

BOOL CResultWindow::onCreate(HWND hwnd, CREATESTRUCT* lpCreateStruct)
{

    d_button1 = createButton(InGameText::get(IDS_Withdraw), ID_Withdraw);

    if(d_owner->results()->willContinue() == BattleResults::Continue)
    {
        d_button2 = createButton(InGameText::get(IDS_Continue), ID_Continue);
    }

    #ifdef DEBUG
        d_buttonShow = createButton("Show", ID_ShowAll);
    #endif

    return TRUE;
}

void CResultWindow::onDestroy(HWND hwnd)
{
}

void CResultWindow::onSize(HWND hwnd, UINT state, int cx, int cy)
{
    if(d_dib)
    {
        if( (cx != d_dib->getWidth()) || (cy != d_dib->getHeight()) )
        {
            makeDIB();
        }
    }

    int border = (IconBorder * cx) / 1024;

    LONG x = cx - border;
    LONG y = cy - border;

    if(d_button1.getHWND())
    {
        PixelRect r;
        GetClientRect(d_button1.getHWND(), &r);

        PixelRect r1;
        r1.right(x);
        r1.bottom(y);
        r1.left(r1.right() - r.width());
        r1.top(r1.bottom() - r.height());

        d_button1.move(r1);

        x = r1.left() - border;
    }

    if(d_button2.getHWND())
    {
        PixelRect r;
        GetClientRect(d_button2.getHWND(), &r);

        PixelRect r1;
        r1.right(x);
        r1.bottom(y);
        r1.left(r1.right() - r.width());
        r1.top(r1.bottom() - r.height());

        d_button2.move(r1);
    }
#ifdef DEBUG
    if(d_buttonShow.getHWND())
    {
        PixelRect r;
        GetClientRect(d_buttonShow.getHWND(), &r);

        PixelRect r1;
        r1.left(border);
        r1.bottom(y);
        r1.right(r1.left() + r.width());
        r1.top(r1.bottom() - r.height());

        d_buttonShow.move(r1);
    }
#endif

}

void CResultWindow::onPaint(HWND hwnd)
{
  PAINTSTRUCT ps;
  HDC hdc = BeginPaint(hwnd, &ps);

  ASSERT(d_dib);

  // HPALETTE oldPal = SelectPalette(hdc, Palette::get(), False);
  // RealizePalette(hdc);

  if(d_dib)
    BitBlt(hdc, 0, 0, d_dib->getWidth(), d_dib->getHeight(), d_dib->getDC(), 0, 0, SRCCOPY);

  // SelectPalette(hdc, oldPal, False);

  EndPaint(hwnd, &ps);
}

BOOL CResultWindow::onEraseBk(HWND hwnd, HDC hdc)
{
    return TRUE;
}

void CResultWindow::onCommand(HWND hwnd, int id, HWND hwndCtl, UINT codeNotify)
{
    switch(id)
    {
        case ID_Continue:
            d_owner->onButton(BattleResults::Continue);
            break;
        case ID_Withdraw:
            d_owner->onButton(BattleResults::Withdraw);
            break;
#ifdef DEBUG
        case ID_ShowAll:
            d_showAll = !d_showAll;
            makeDIB();
            InvalidateRect(hwnd, NULL, TRUE);
            break;
#endif
    }
}


void CResultWindow::makeDIB()
{
    ASSERT(getHWND());

    const BattleResults* results = d_owner->results();

    PixelRect r;
    HRESULT success = GetClientRect(getHWND(), &r);
    ASSERT(success);

    if((r.width() == 0) || (r.height() == 0))
       return;

    if(d_dib)
        d_dib->resize(r.width(), r.height());
    else
        d_dib = new DrawDIBDC(r.width(), r.height());

    /*
     * Fill Background
     */

    Side playerSide = GamePlayerControl::getSide(GamePlayerControl::Player);
    if(playerSide == SIDE_Neutral)
      playerSide = 0;

    Side otherSide = ::otherSide(playerSide);

    COLORREF playerColor = scenario->getSideColour(playerSide);
    COLORREF otherColor = scenario->getSideColour(otherSide);
    COLORREF Black = Colours::Black;


    ASSERT(d_bkPic != 0);

    DIB_Utility::stretchFit(d_dib, 0,0,d_dib->getWidth(),d_dib->getHeight(), d_bkPic);

    /*
     * Work out some font sizes and positions
     */

    class CNiceHeight
    {
            enum
            {
                VirtualHeight = 768     // Height that design was drawn at
            };

        public:
            CNiceHeight(int actualHeight) : d_height(actualHeight) { }
            int convert(int y)
            {
                return (y * d_height) / VirtualHeight;
            }

        private:
            int d_height;
    };

    CNiceHeight height(d_dib->getHeight());

    int titleHeight = maximum(8, height.convert(48));
    int headHeight = maximum(8, height.convert(32));
    int itemHeight = maximum(8, height.convert(24));

    LogFont lf;
    lf.height(titleHeight);
    lf.weight(FW_BOLD);
    lf.face(scenario->fontName(Font_Bold));
    lf.italic(false);
    lf.underline(false);

    Font titleFont;
    Font headFont;
    Font itemFont;
    Font totalFont;

    titleFont.set(lf);

    lf.height(headHeight);
    lf.weight(FW_NORMAL);
    lf.face(scenario->fontName(Font_Normal));
    headFont.set(lf);

    lf.height(itemHeight);
    itemFont.set(lf);

    lf.weight(FW_BOLD);
    totalFont.set(lf);

    DCFontHolder font(d_dib->getDC(), headFont);

    int centreX = d_dib->getWidth() / 2;

    /*
     * Draw Results
     */

    d_dib->setBkMode(TRANSPARENT);

    // Title

    int y = height.convert(32);

    char buffer[100];

    const char* str_BattleResults = InGameText::get(IDS_BattleResults);  // "Battle Results";   // TODO: needs to go in string resource
    SimpleString str_Leader = InGameText::get(IDS_UI_LEADER);  // "Leader :";
   str_Leader += " :";
   SimpleString str_Unit = InGameText::get(IDS_UI_UNIT);    // "Unit :";
   str_Unit += " :";
   SimpleString str_Inf = InGameText::get(IDS_UI_INFANTRY);  // "Infantry :";
   str_Inf += " :";
   SimpleString str_Cav = InGameText::get(IDS_UI_CAVALRY) ;  // "Cavalry :";
   str_Cav += " :";
   SimpleString str_Art = InGameText::get(IDS_UI_ARTY) ;  // "Artillery :";
   str_Art += " :";
   SimpleString str_Other = InGameText::get(IDS_Engineers) ;  // "Engineers :";
   str_Other += " :";
   SimpleString str_Total = InGameText::get(IDS_Total) ;  // "Total :";
   str_Total += " :";
    const char* str_Losses = InGameText::get(IDS_Losses);  // "Losses";
    const char* str_men = InGameText::get(IDS_Men);  // "men";
    const char* str_guns = InGameText::get(IDS_Guns);  // "guns";
    static const char* str_outOf = "/";


    wsprintf(buffer, "%s %s",
        scenario->getSideName(playerSide),
        str_BattleResults);

    font.set(titleFont);
    d_dib->setTextColor(playerColor);
    d_dib->setTextAlign(TA_TOP | TA_CENTER);
    wTextOut(d_dib->getDC(), centreX, y, buffer);

    // Draw Battle Icons either side
    int titleWidth = textWidth(d_dib->getDC(), buffer);
    int x = (d_dib->getWidth() - titleWidth) / 2;

    int xFlag1 = x/2;
    int xFlag2 = d_dib->getWidth() - x/2;
    int yBattle = y + titleHeight/2;
    d_dotImages->blit(d_dib, DotMode_Battle, xFlag1, yBattle);
    d_dotImages->blit(d_dib, DotMode_Battle, xFlag2, yBattle);

    y = height.convert(80);

    // Draw flags underneath

    int w;
    int h;
    d_flagImages->getImageSize(FI_Army, w, h);

    int xh;
    int yh;
    d_flagImages->getHotSpots(FI_Army, xh, yh);

    x = d_dib->getWidth() / 2 - w/2 + xh;    // centre adjusted for hotspot
    int x1 = x;
    y += yh;        // adjust Y to ignore hotspot

    d_flagImages->blit(d_dib, FI_Army, x, y);
    while(x > xFlag1)
    {
        x -= w + 1;
        x1 += w + 1;
        d_flagImages->blit(d_dib, FI_Army, x, y);
        d_flagImages->blit(d_dib, FI_Army, x1, y);
    }

    // Draw BattleName

    y = height.convert(128);
    font.set(headFont);
    d_dib->setTextColor(Black);
    d_dib->setTextAlign(TA_TOP | TA_CENTER);

    wTextOut(d_dib->getDC(), centreX, y, results->battleName().c_str());

    // Victory Style

    // We could display this using 2 tables, so result is always
    // relative to player
    //
    // e.g. French Minor Victory <==> Allied Minor Defeat


    y = height.convert(192);
    font.set(headFont);
    d_dib->setTextColor(Black);
    d_dib->setTextAlign(TA_TOP | TA_CENTER);

    // static const char* whyNames[] =
    // {
    //     "Unknown",
    //     "End of Day",
    //     "Surrender",
    //     "Defeated"
    // };

    static const InGameText::ID strVictory[] = {
        IDS_Draw,
        IDS_WinningDraw,
        IDS_MinorVictory,
        IDS_MajorVictory,
        IDS_CrushingVictory,
    };

    SimpleString whyStr;     // = whyNames[why];
    if(results->level() != BattleResults::Draw)
    {
        whyStr = scenario->getSideName(results->winner());
        whyStr += " ";
    }
    whyStr += InGameText::get(strVictory[results->level()]);

    wTextOut(d_dib->getDC(), centreX, y, whyStr.toStr());

    /*
     * Player's losses
     */

    for(int column = 0; column < 2; ++column)
    {
        Side cSide;
        COLORREF cColor;
        int xLeft;

        const int xMargin = 32;

        if(column == 0)
        {
            cSide = playerSide;
            cColor = playerColor;
            xLeft = (d_dib->getWidth() * xMargin) / 1024;
        }
        else
        {
            cSide = otherSide;
            cColor = otherColor;
            xLeft = (d_dib->getWidth() * (512 + xMargin) ) / 1024;
        }

        const BattleLosses* losses = results->losses(cSide);


        y = height.convert(256);

        font.set(itemFont);
        int leadWidth = textWidth(d_dib->getDC(), str_Leader.toStr());
        leadWidth = maximum(leadWidth, textWidth(d_dib->getDC(), str_Unit.toStr()));
        leadWidth = maximum(leadWidth, textWidth(d_dib->getDC(), str_Inf.toStr()));
        leadWidth = maximum(leadWidth, textWidth(d_dib->getDC(), str_Cav.toStr()));
        leadWidth = maximum(leadWidth, textWidth(d_dib->getDC(), str_Art.toStr()));
        leadWidth = maximum(leadWidth, textWidth(d_dib->getDC(), str_Other.toStr()));
        leadWidth = maximum(leadWidth, textWidth(d_dib->getDC(), str_Total.toStr()));
        int wSpace = textWidth(d_dib->getDC(), " ");

        int xAlign = xLeft + leadWidth; // (d_dib->getWidth() * 128) / 1024;
        int xAlign1 = xAlign + wSpace;
        // int xRight = xLeft + (d_dib->getWidth() * (512-64*2)) / 1024;

        // Display Side Name

        class KeepX
        {
            public:
                KeepX() : d_max(0) { }
                void textOut(HDC dc, int x, int y, const char* s)
                {
                    wTextOut(dc, x, y, s);
                    int x1 = x + textWidth(dc, s);
                    d_max = maximum(d_max, x1);
                }

                int get() const { return d_max; }

            private:
                int d_max;

        };

        KeepX xKeeper;


        d_dib->setTextColor(cColor);
        d_dib->setTextAlign(TA_TOP | TA_LEFT);
        font.set(headFont);
        xKeeper.textOut(d_dib->getDC(), xLeft, y, scenario->getSideName(cSide));

        // Display Leader & Org

        y = height.convert(288);

        int y1 = y + headHeight;
        y = height.convert(320);
        int y2 = y + headHeight;

        d_dib->setTextColor(Black);
        d_dib->setTextAlign(TA_BASELINE | TA_RIGHT);

        font.set(itemFont);
        wTextOut(d_dib->getDC(), xAlign, y1, str_Leader.toStr());
        wTextOut(d_dib->getDC(), xAlign, y2, str_Unit.toStr());

        ConstRefGLeader leader = results->leader(cSide);
        ConstRefGenericCP cp = results->cp(cSide);

        ASSERT(leader != NoGLeader);
        ASSERT(cp != NoGenericCP);

        d_dib->setTextAlign(TA_BASELINE | TA_LEFT);
        font.set(headFont);
        if(leader != NoGLeader)
            xKeeper.textOut(d_dib->getDC(), xAlign1, y1, leader->getName());
        font.set(itemFont);
        if(cp != NoGenericCP)
            xKeeper.textOut(d_dib->getDC(), xAlign1, y2, cp->getName());


        /*
         * Leader Portrait
         */

        if(leader != NoGLeader)
        {
            PicLibrary::Picture portrait = GLeaderInfo::getPortrait(leader);

            const int picBorder = 1;

            int picHeight = height.convert(128);
            int portHeight = picHeight - picBorder * 2;
            int portWidth = (portHeight * portrait->getWidth()) / portrait->getHeight();
            int picWidth = portWidth + picBorder * 2;

            y = height.convert(256);
            x = xKeeper.get() + wSpace;

            COLORREF borderColor = RGB(0,0,0);

            d_dib->frame(x, y, picWidth, picHeight, d_dib->getColour(borderColor));

            x += picBorder;
            y += picBorder;

            DIB_Utility::stretchDIBNoMask(
                    d_dib,
                    x, y, portWidth, portHeight,
                    portrait,
                    0, 0, portrait->getWidth(), portrait->getHeight());
        }

        /*
         * Details
         */

        if(
#ifdef DEBUG
            d_showAll ||
#endif
            d_owner->willShowEnemyResults(cSide))
        {
            y = height.convert(384);

            // Display <side> "losses"

            font.set(headFont);
            d_dib->setTextAlign(TA_TOP | TA_LEFT);
            d_dib->setTextColor(cColor);
            wTextPrintf(d_dib->getDC(), xLeft, y, "%s %s",
                (const char*) scenario->getSideName(cSide),
                (const char*) str_Losses);


            font.set(itemFont);
            d_dib->setTextColor(Black);

            const char* str_UnitTypes[BasicUnitType::HowMany + 1] =
            {
                str_Inf.toStr(),
                str_Cav.toStr(),
                str_Art.toStr(),
                str_Other.toStr(),
                str_Total.toStr()
            };

            int wDigit = textWidth(d_dib->getDC(), "9");

            int y = height.convert(416);
            int rowHeight = itemHeight + 2;

            int xNum = xAlign1 + wDigit;
            int maxLoss = losses->losses();     // find width of losses
            while(maxLoss >= 10)
            {
                xNum += wDigit;
                maxLoss /= 10;
            }

            int xNum1 = xNum + wDigit + textWidth(d_dib->getDC(), str_outOf);
            maxLoss = losses->startStrength();  // width of startStrength
            while(maxLoss >= 10)
            {
                xNum1 += wDigit;
                maxLoss /= 10;
            }

            int xMen = xNum1 + wSpace;
            int xGuns = xMen + textWidth(d_dib->getDC(), str_men) +
                               textWidth(d_dib->getDC(), " (100%)");

            int xRight = xGuns;

            for(int t = 0; t <= BasicUnitType::HowMany; ++t)
            {
                BattleResults::ManCount manLost;
                int lossPercent;
                BattleResults::ManCount manStart;

                if(t == BasicUnitType::HowMany)
                {
                    d_dib->hLine(xLeft, y, xRight - xLeft, d_dib->getColour(Black));
                    d_dib->hLine(xLeft, y+1, xRight - xLeft, d_dib->getColour(Black));
                    y += 2;
                    d_dib->hLine(xLeft, y + itemHeight, xRight - xLeft, d_dib->getColour(Black));
                    d_dib->hLine(xLeft, y+1 + itemHeight, xRight - xLeft, d_dib->getColour(Black));

                    font.set(totalFont);

                    manLost = losses->losses();
                    lossPercent = losses->lossPercent();
                    manStart = losses->startStrength();
                }
                else
                {
                    ASSERT(t < BasicUnitType::HowMany);
                    BasicUnitType::value ut = static_cast<BasicUnitType::value>(t);
                    manLost = losses->losses(ut);
                    lossPercent = losses->lossPercent(ut);
                    manStart = losses->startStrength(ut);
                }

                d_dib->setTextAlign(TA_TOP | TA_RIGHT);
                wTextOut(d_dib->getDC(), xAlign, y, str_UnitTypes[t]);

                if(manStart != 0)
                {
                    d_dib->setTextAlign(TA_TOP | TA_RIGHT);
                    wTextPrintf(d_dib->getDC(), xNum, y, "%d",
                        (int) manLost);

                    d_dib->setTextAlign(TA_TOP | TA_LEFT);
                    wTextOut(d_dib->getDC(), xNum, y, str_outOf);

                    d_dib->setTextAlign(TA_TOP | TA_RIGHT);
                    wTextPrintf(d_dib->getDC(), xNum1, y, "%d",
                        (int) manStart);

                    // percentages
                    d_dib->setTextAlign(TA_TOP | TA_LEFT);

                    wTextPrintf(d_dib->getDC(), xMen, y, "%s (%d%%)",
                        str_men, lossPercent);

                    if(t == BasicUnitType::Artillery)
                    {
                        int nGuns = (manLost * UnitTypeConst::GunsPerSP) / UnitTypeConst::ArtilleryPerSP;
                        wTextPrintf(d_dib->getDC(), xGuns, y, "[%d %s]",
                            nGuns, str_guns);
                    }
                }

                y += rowHeight;
            }

        }

    }
}


};  // private namespace

CGenericResultsWindow* CGenericResultsWindow::create(ResultWindowUser* owner)
{
    return new CResultWindow(owner);
}


bool ResultWindowUser::willShowEnemyResults(Side s) const
{
   return( (GamePlayerControl::getControl(s) == GamePlayerControl::Player) ||
            !CampaignOptions::get(OPT_FogOfWar) );
}

