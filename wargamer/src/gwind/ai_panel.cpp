/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *	Control Panel for AI
 *
 * used for debugging... allows AI to be paused, rate changed, etc
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "ai_panel.hpp"
#include "grtypes.hpp"
#include "wind.hpp"
#include "winctrl.hpp"
#include "scenario.hpp"
#include "app.hpp"
#include "fonts.hpp"
#include "wmisc.hpp"
// #include "windsave.hpp"
#include "registry.hpp"
// #include <string.hpp>
#include <stdio.h>
#include <stdlib.h>

class AI_ControlPanelImp :
	public WindowBaseND
{
		static const char s_className[];
		static ATOM s_classAtom;
//		static char s_windowRegKey[];
		static char s_pauseRegKey[];
		static char s_rateRegKey[];
      static WSAV_FLAGS s_saveFlags;

        // enum
		// {
		// 	MinRate = 1,
		// 	MaxRate = 5000,
		// 	TickFreq = 500,
		// };

	public:
		AI_ControlPanelImp(AI_ControlInterface* ai);
		~AI_ControlPanelImp();

        virtual const char* className() const { return s_className; }

	private:
		static ATOM registerClass();

		LRESULT procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
		void onDestroy(HWND hWnd);
		BOOL onCreate(HWND hWnd, CREATESTRUCT* lpCreateStruct);
		void onCommand(HWND hWnd, int id, HWND hwndCtl, UINT codeNotify);
		LRESULT onNotify(HWND hWnd, int id, NMHDR* lpNMHDR);
		void onDrawItem(HWND hwnd, const DRAWITEMSTRUCT* lpDrawItem);
		void onHScroll(HWND hwnd, HWND hwndCtl, UINT code, int pos);

		void drawRate(HDC hdc, const RECT& r);
		void updateRate();

        int tickFreq() const;

		enum
		{
			ID_PauseButton = 1,
			ID_RateSlider,
			ID_RateText,
			ID_LeftText,
			ID_RightText
		};

	private:
		AI_ControlInterface* d_ai;

		Button d_pauseButton;
		StaticWindow d_rateText;
		TrackBar d_rateSlider;
		StaticWindow d_leftText;
		StaticWindow d_rightText;
		String d_title;

		int d_fontHeight;
};


const char AI_ControlPanelImp::s_className[] = "AI_ControlPanel";
ATOM AI_ControlPanelImp::s_classAtom = NULL;

// static char AI_ControlPanelImp::s_windowRegKey[] = "position";
char AI_ControlPanelImp::s_pauseRegKey[] = "paused";
char AI_ControlPanelImp::s_rateRegKey[] = "rate";
WSAV_FLAGS AI_ControlPanelImp::s_saveFlags = WSAV_POSITION | WSAV_SIZE | WSAV_RESTORE | WSAV_MINIMIZE;

AI_ControlPanelImp::AI_ControlPanelImp(AI_ControlInterface* ai) :
	d_ai(ai),
	d_pauseButton(),
	d_rateText(),
	d_rateSlider()
{
	registerClass();

    d_title = ai->getTitle();

	createWindow(
		WS_EX_TOOLWINDOW | WS_EX_WINDOWEDGE,	 	// ex-style
		// s_className,
		d_title.c_str(),
		WS_POPUP | WS_CAPTION,				// Style
		50,50,200,120,			// x,y,w,h
		APP::getMainHWND(),	// parent
		NULL);						// Menu
		// APP::instance());		// Instance

	SetWindowText(getHWND(), d_title.c_str());

//	restoreWindowState(s_windowRegKey, hWnd, WSAV_POSITION, d_title.c_str());
//	ShowWindow(getHWND(), SW_SHOW);
   getState(d_title.c_str(), s_saveFlags);

	bool paused;
	if(getRegistry(s_pauseRegKey, &paused, sizeof(paused), d_title.c_str()))
	{
		if(paused)
		{
			d_ai->pause(paused);
			d_pauseButton.setCheck(paused);
		}
	}

	int rate;
	if(getRegistry(s_rateRegKey, &rate, sizeof(rate), d_title.c_str()))
	{
		if(rate != ai->getRate())
		{
//			ASSERT(rate >= d_ai->getMinRate());
//			ASSERT(rate <= d_ai->getMaxRate());

			if( (rate >= d_ai->getMinRate()) && (rate <= d_ai->getMaxRate()) )
			{
				d_ai->setRate(rate);
				updateRate();
			}
		}
	}
}

AI_ControlPanelImp::~AI_ControlPanelImp()
{
	selfDestruct();
}

ATOM AI_ControlPanelImp::registerClass()
{
	if(!s_classAtom)
	{
		WNDCLASS wc;

		wc.style = CS_OWNDC | CS_DBLCLKS;
		wc.lpfnWndProc = baseWindProc;
		wc.cbClsExtra = 0;
		wc.cbWndExtra = sizeof(AI_ControlPanelImp*);
		wc.hInstance = APP::instance();
		wc.hIcon = NULL;
		wc.hCursor = LoadCursor(NULL, IDC_ARROW);
		wc.hbrBackground = (HBRUSH) (1 + COLOR_3DFACE);
		wc.lpszMenuName = NULL;
		wc.lpszClassName = s_className;
		s_classAtom = RegisterClass(&wc);
	}

	ASSERT(s_classAtom != 0);

	return s_classAtom;
}

LRESULT AI_ControlPanelImp::procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	switch(msg)
	{
		HANDLE_MSG(hWnd, WM_CREATE,	onCreate);
		HANDLE_MSG(hWnd, WM_DESTROY,	onDestroy);
		HANDLE_MSG(hWnd, WM_NOTIFY,	onNotify);
		HANDLE_MSG(hWnd, WM_COMMAND, onCommand);
		HANDLE_MSG(hWnd, WM_DRAWITEM, onDrawItem);
		HANDLE_MSG(hWnd, WM_HSCROLL, onHScroll);
	default:
		return defProc(hWnd, msg, wParam, lParam);
	}
}

void AI_ControlPanelImp::onDestroy(HWND hWnd)
{
	bool paused = d_pauseButton.getCheck();
	setRegistry(s_pauseRegKey, &paused, sizeof(paused), d_title.c_str());
	int rate = d_ai->getRate();
	setRegistry(s_rateRegKey, &rate, sizeof(rate), d_title.c_str());

//	saveWindowState(s_windowRegKey, hWnd, WSAV_POSITION, d_title.c_str());
   saveState(d_title.c_str(), s_saveFlags);
}

void AI_ControlPanelImp::updateRate()
{
	d_rateSlider.setValue(d_ai->getRate());
	InvalidateRect(d_rateText.getHWND(), NULL, TRUE);
}

void AI_ControlPanelImp::drawRate(HDC hdc, const RECT& r)
{
	float rate = static_cast<float>(d_ai->getRate()) / 1000;

	char buf[30];
	sprintf(buf, "%.2f seconds:", (float) rate);

	// d_rateText.setText(buf);

	// int height = maximum(10, d_fontHeight - 2);
	int height = maximum(10, r.bottom - r.top - 2);

	LogFont lf;
	lf.height(height);
	lf.weight(FW_MEDIUM);
	lf.face(scenario->fontName(Font_Bold));

	Fonts font;
	font.set(hdc, lf);

	wTextOut(hdc, r.left, r.bottom-height, buf);
}

BOOL AI_ControlPanelImp::onCreate(HWND hWnd, CREATESTRUCT* lpCreateStruct)
{
	HDC dc = GetWindowDC(hWnd);
	TEXTMETRIC metrics;
	GetTextMetrics(dc, &metrics);
	ReleaseDC(hWnd, dc);

	d_fontHeight = metrics.tmHeight;
	int spacing = metrics.tmHeight + 2;

	int y = 10;

	d_pauseButton.create(
		ID_PauseButton,
		"Pause",
		hWnd,
		0,
		WS_VISIBLE | WS_CHILD | BS_AUTOCHECKBOX,
		PixelRect(10, y, 90, y+d_fontHeight));
	y += spacing;

	ASSERT(d_pauseButton.getHWND() != NULL);

	d_rateText.create(
		ID_RateText,
		hWnd,
		0,
		WS_VISIBLE | WS_CHILD | SS_OWNERDRAW,
		PixelRect(50, y, 150, y+d_fontHeight));

    ASSERT( (d_ai->getMinRate() / 1000) < 9999);
    ASSERT( (d_ai->getMaxRate() / 1000) < 9999);
    char strNumber[5];

	d_leftText.create(
		ID_LeftText,
		hWnd,
		0,
		WS_VISIBLE | WS_CHILD | SS_LEFT,
		PixelRect(10, y, 50, y+d_fontHeight));
    itoa(d_ai->getMinRate() / 1000, strNumber, 10);
	d_leftText.setText(strNumber);

	d_rightText.create(
		ID_RightText,
		hWnd,
		0,
		WS_VISIBLE | WS_CHILD | SS_RIGHT,
		PixelRect(150, y, 190, y+d_fontHeight));
    itoa(d_ai->getMaxRate() / 1000, strNumber, 10);
	d_rightText.setText(strNumber);
	y += spacing;

	d_rateSlider.create(
		ID_RateSlider,
		"Rate:",
		hWnd,
		0,
		WS_VISIBLE | WS_CHILD | TBS_HORZ | TBS_AUTOTICKS,
		PixelRect(10, y, 190, y+d_fontHeight));
	d_rateSlider.setRange(d_ai->getMinRate(), d_ai->getMaxRate());

	d_rateSlider.setTickFreq(tickFreq());
	y += spacing;

	updateRate();

	PixelRect r(0,0,200,y+10);
	AdjustWindowRectEx(&r, GetWindowLong(hWnd, GWL_STYLE), GetMenu(hWnd) ? TRUE : FALSE, GetWindowLong(hWnd, GWL_EXSTYLE));

	SetWindowPos(hWnd, HWND_TOP, 0,0, r.width(), r.height(), SWP_NOMOVE | SWP_NOZORDER);

	return TRUE;
}

void AI_ControlPanelImp::onCommand(HWND hWnd, int id, HWND hwndCtl, UINT codeNotify)
{
	switch(id)
	{
		case ID_PauseButton:
		{
			if(codeNotify == BN_CLICKED)
			{
				d_ai->pause(Button(hwndCtl).getCheck());
			}
		}
		break;
	}
}

void AI_ControlPanelImp::onHScroll(HWND hwnd, HWND hwndCtl, UINT code, int pos)
{
	switch(code)
	{
	case TB_ENDTRACK:
	case TB_BOTTOM:
	case TB_LINEDOWN:
	case TB_LINEUP:
	case TB_PAGEDOWN:
	case TB_PAGEUP:
	case TB_TOP:
	case TB_THUMBPOSITION:
	case TB_THUMBTRACK:
		{
			int newRate = d_rateSlider.getValue();
			d_ai->setRate(newRate);
			updateRate();
		}
		break;
	}
}


LRESULT AI_ControlPanelImp::onNotify(HWND hWnd, int id, NMHDR* lpNMHDR)
{
	return 0;
}

void AI_ControlPanelImp::onDrawItem(HWND hwnd, const DRAWITEMSTRUCT* lpDrawItem)
{
	switch(lpDrawItem->CtlID)
	{
		case ID_RateText:
			drawRate(lpDrawItem->hDC, lpDrawItem->rcItem);
			break;
	}
}

/*
 * Return tick intervals for the slide bar
 */

int AI_ControlPanelImp::tickFreq() const
{
    static int s_closest[10] =
    {
        1,1,2,2,5,5,5,5,10,10
    };

    static const int DesiredTicks = 10;

    int range = d_ai->getMaxRate() - d_ai->getMinRate();
    range /= DesiredTicks;

    ASSERT(range >= 0);
    int multiplier = 1;
    while(range > 10)
    {
        range /= 10;
        multiplier *= 10;
    }
    ASSERT(range >= 0);
    ASSERT(range < 10);

    return s_closest[range] * multiplier;
}

/*
 * Interface Implementation
 */

AI_ControlPanel::AI_ControlPanel() :
	d_imp(0)
{
}

AI_ControlPanel::~AI_ControlPanel()
{
	delete d_imp;
}

void AI_ControlPanel::init(AI_ControlInterface* ai)
{
	ASSERT(d_imp == 0);

	d_imp = new AI_ControlPanelImp(ai);
}

void AI_ControlPanel::clear()
{
	delete d_imp;
	d_imp = 0;
}

