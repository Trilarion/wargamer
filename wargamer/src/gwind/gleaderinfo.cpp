/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 * Generic Leader Info Support Functions
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "gLeaderInfo.hpp"
#include "scenario.hpp"
#include "simpstr.hpp"
#include "winfile.hpp"

/*
 * draw a portrait into given rectangle maintaining aspect ratio
 */

PicLibrary::Picture GLeaderInfo::getPortrait(ConstRefGLeader leader)
{
	static const char defName[] = "<Default>";

	const char* picName = leader->portrait();

	if( (picName == 0) || (strcmp(picName, defName) == 0) )
		picName = scenario->defaultPortrait(leader->getSide());

	/*
	 * Make up filename as:
	 *		<scenario>\portraits\name.bmp
	 */

	SimpleString fileNameStr = scenario->getPortraitPath();	// "Portraits\\";
	fileNameStr << "\\" << picName << ".BMP";

#ifdef DEBUG
	bool exists = FileSystem::fileExists(fileNameStr.toStr());
	ASSERT(exists);
	if(!exists)
		return 0;
#endif

	// CString fileName = scenario->makeScenarioFileName(fileNameStr.toStr());

	return PicLibrary::get(fileNameStr.toStr());
}

