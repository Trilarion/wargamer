# Microsoft Developer Studio Generated NMAKE File, Based on gwind.dsp
!IF "$(CFG)" == ""
CFG=gwind - Win32 Editor Debug
!MESSAGE No configuration specified. Defaulting to gwind - Win32 Editor Debug.
!ENDIF 

!IF "$(CFG)" != "gwind - Win32 Release" && "$(CFG)" != "gwind - Win32 Debug" && "$(CFG)" != "gwind - Win32 Editor Debug" && "$(CFG)" != "gwind - Win32 Editor Release"
!MESSAGE Invalid configuration "$(CFG)" specified.
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "gwind.mak" CFG="gwind - Win32 Editor Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "gwind - Win32 Release" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE "gwind - Win32 Debug" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE "gwind - Win32 Editor Debug" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE "gwind - Win32 Editor Release" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE 
!ERROR An invalid configuration is specified.
!ENDIF 

!IF "$(OS)" == "Windows_NT"
NULL=
!ELSE 
NULL=nul
!ENDIF 

CPP=cl.exe
MTL=midl.exe
RSC=rc.exe

!IF  "$(CFG)" == "gwind - Win32 Release"

OUTDIR=.\..\..\exe
INTDIR=.\o\release
# Begin Custom Macros
OutDir=.\..\..\exe
# End Custom Macros

!IF "$(RECURSE)" == "0" 

ALL : "$(OUTDIR)\gwind.dll"

!ELSE 

ALL : "ob - Win32 Release" "system - Win32 Release" "gamesup - Win32 Release" "$(OUTDIR)\gwind.dll"

!ENDIF 

!IF "$(RECURSE)" == "1" 
CLEAN :"gamesup - Win32 ReleaseCLEAN" "system - Win32 ReleaseCLEAN" "ob - Win32 ReleaseCLEAN" 
!ELSE 
CLEAN :
!ENDIF 
	-@erase "$(INTDIR)\about.obj"
	-@erase "$(INTDIR)\ai_panel.obj"
	-@erase "$(INTDIR)\bitmapwindow.obj"
	-@erase "$(INTDIR)\chatdialog.obj"
	-@erase "$(INTDIR)\gleaderinfo.obj"
	-@erase "$(INTDIR)\gmsgwin.obj"
	-@erase "$(INTDIR)\goptdial.obj"
	-@erase "$(INTDIR)\gresultswindow.obj"
	-@erase "$(INTDIR)\multiplayerdialog.obj"
	-@erase "$(INTDIR)\optwind.obj"
	-@erase "$(INTDIR)\plyrdlg.obj"
	-@erase "$(INTDIR)\timewind.obj"
	-@erase "$(INTDIR)\titlebar.obj"
	-@erase "$(INTDIR)\vc60.idb"
	-@erase "$(INTDIR)\vc60.pdb"
	-@erase "$(INTDIR)\wg_main.obj"
	-@erase "$(OUTDIR)\gwind.dll"
	-@erase "$(OUTDIR)\gwind.exp"
	-@erase "$(OUTDIR)\gwind.lib"
	-@erase "$(OUTDIR)\gwind.pdb"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

"$(INTDIR)" :
    if not exist "$(INTDIR)/$(NULL)" mkdir "$(INTDIR)"

CPP_PROJ=/nologo /MD /W3 /Gi /GR /GX /Zi /O2 /I "..\h" /I "..\..\res" /I "..\system" /I "..\gamesup" /I "..\ob" /D "NDEBUG" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "EXPORT_GWIND_DLL" /Fp"$(INTDIR)\gwind.pch" /YX"stdinc.hpp" /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /c 
MTL_PROJ=/nologo /D "NDEBUG" /mktyplib203 /win32 
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\gwind.bsc" 
BSC32_SBRS= \
	
LINK32=link.exe
LINK32_FLAGS=User32.lib gdi32.lib advapi32.lib Shell32.lib /nologo /dll /incremental:no /pdb:"$(OUTDIR)\gwind.pdb" /debug /machine:I386 /out:"$(OUTDIR)\gwind.dll" /implib:"$(OUTDIR)\gwind.lib" 
LINK32_OBJS= \
	"$(INTDIR)\about.obj" \
	"$(INTDIR)\ai_panel.obj" \
	"$(INTDIR)\bitmapwindow.obj" \
	"$(INTDIR)\chatdialog.obj" \
	"$(INTDIR)\gleaderinfo.obj" \
	"$(INTDIR)\gmsgwin.obj" \
	"$(INTDIR)\goptdial.obj" \
	"$(INTDIR)\gresultswindow.obj" \
	"$(INTDIR)\multiplayerdialog.obj" \
	"$(INTDIR)\optwind.obj" \
	"$(INTDIR)\plyrdlg.obj" \
	"$(INTDIR)\timewind.obj" \
	"$(INTDIR)\titlebar.obj" \
	"$(INTDIR)\wg_main.obj" \
	"$(OUTDIR)\gamesup.lib" \
	"$(OUTDIR)\system.lib" \
	"$(OUTDIR)\ob.lib"

"$(OUTDIR)\gwind.dll" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

!ELSEIF  "$(CFG)" == "gwind - Win32 Debug"

OUTDIR=.\..\..\exe
INTDIR=.\o\debug
# Begin Custom Macros
OutDir=.\..\..\exe
# End Custom Macros

!IF "$(RECURSE)" == "0" 

ALL : "$(OUTDIR)\gwindDB.dll"

!ELSE 

ALL : "ob - Win32 Debug" "system - Win32 Debug" "gamesup - Win32 Debug" "$(OUTDIR)\gwindDB.dll"

!ENDIF 

!IF "$(RECURSE)" == "1" 
CLEAN :"gamesup - Win32 DebugCLEAN" "system - Win32 DebugCLEAN" "ob - Win32 DebugCLEAN" 
!ELSE 
CLEAN :
!ENDIF 
	-@erase "$(INTDIR)\about.obj"
	-@erase "$(INTDIR)\ai_panel.obj"
	-@erase "$(INTDIR)\bitmapwindow.obj"
	-@erase "$(INTDIR)\chatdialog.obj"
	-@erase "$(INTDIR)\gleaderinfo.obj"
	-@erase "$(INTDIR)\gmsgwin.obj"
	-@erase "$(INTDIR)\goptdial.obj"
	-@erase "$(INTDIR)\gresultswindow.obj"
	-@erase "$(INTDIR)\multiplayerdialog.obj"
	-@erase "$(INTDIR)\optwind.obj"
	-@erase "$(INTDIR)\plyrdlg.obj"
	-@erase "$(INTDIR)\timewind.obj"
	-@erase "$(INTDIR)\titlebar.obj"
	-@erase "$(INTDIR)\vc60.idb"
	-@erase "$(INTDIR)\vc60.pdb"
	-@erase "$(INTDIR)\wg_main.obj"
	-@erase "$(OUTDIR)\gwindDB.dll"
	-@erase "$(OUTDIR)\gwindDB.exp"
	-@erase "$(OUTDIR)\gwindDB.ilk"
	-@erase "$(OUTDIR)\gwindDB.lib"
	-@erase "$(OUTDIR)\gwindDB.pdb"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

"$(INTDIR)" :
    if not exist "$(INTDIR)/$(NULL)" mkdir "$(INTDIR)"

CPP_PROJ=/nologo /MDd /W3 /Gm /Gi /GR /GX /ZI /Od /I "..\h" /I "..\..\res" /I "..\system" /I "..\gamesup" /I "..\ob" /D "_DEBUG" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "EXPORT_GWIND_DLL" /Fp"$(INTDIR)\gwind.pch" /YX"stdinc.hpp" /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /GZ /c 
MTL_PROJ=/nologo /D "_DEBUG" /mktyplib203 /win32 
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\gwind.bsc" 
BSC32_SBRS= \
	
LINK32=link.exe
LINK32_FLAGS=User32.lib gdi32.lib advapi32.lib Shell32.lib /nologo /dll /incremental:yes /pdb:"$(OUTDIR)\gwindDB.pdb" /debug /machine:I386 /out:"$(OUTDIR)\gwindDB.dll" /implib:"$(OUTDIR)\gwindDB.lib" /pdbtype:sept 
LINK32_OBJS= \
	"$(INTDIR)\about.obj" \
	"$(INTDIR)\ai_panel.obj" \
	"$(INTDIR)\bitmapwindow.obj" \
	"$(INTDIR)\chatdialog.obj" \
	"$(INTDIR)\gleaderinfo.obj" \
	"$(INTDIR)\gmsgwin.obj" \
	"$(INTDIR)\goptdial.obj" \
	"$(INTDIR)\gresultswindow.obj" \
	"$(INTDIR)\multiplayerdialog.obj" \
	"$(INTDIR)\optwind.obj" \
	"$(INTDIR)\plyrdlg.obj" \
	"$(INTDIR)\timewind.obj" \
	"$(INTDIR)\titlebar.obj" \
	"$(INTDIR)\wg_main.obj" \
	"$(OUTDIR)\gamesupDB.lib" \
	"$(OUTDIR)\systemDB.lib" \
	"$(OUTDIR)\obDB.lib"

"$(OUTDIR)\gwindDB.dll" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

!ELSEIF  "$(CFG)" == "gwind - Win32 Editor Debug"

OUTDIR=.\..\..\exe
INTDIR=.\o\Editor\Debug
# Begin Custom Macros
OutDir=.\..\..\exe
# End Custom Macros

!IF "$(RECURSE)" == "0" 

ALL : "$(OUTDIR)\gwindEDDB.dll"

!ELSE 

ALL : "ob - Win32 Editor Debug" "system - Win32 Editor Debug" "gamesup - Win32 Editor Debug" "$(OUTDIR)\gwindEDDB.dll"

!ENDIF 

!IF "$(RECURSE)" == "1" 
CLEAN :"gamesup - Win32 Editor DebugCLEAN" "system - Win32 Editor DebugCLEAN" "ob - Win32 Editor DebugCLEAN" 
!ELSE 
CLEAN :
!ENDIF 
	-@erase "$(INTDIR)\about.obj"
	-@erase "$(INTDIR)\ai_panel.obj"
	-@erase "$(INTDIR)\bitmapwindow.obj"
	-@erase "$(INTDIR)\chatdialog.obj"
	-@erase "$(INTDIR)\gleaderinfo.obj"
	-@erase "$(INTDIR)\gmsgwin.obj"
	-@erase "$(INTDIR)\goptdial.obj"
	-@erase "$(INTDIR)\gresultswindow.obj"
	-@erase "$(INTDIR)\multiplayerdialog.obj"
	-@erase "$(INTDIR)\optwind.obj"
	-@erase "$(INTDIR)\plyrdlg.obj"
	-@erase "$(INTDIR)\timewind.obj"
	-@erase "$(INTDIR)\titlebar.obj"
	-@erase "$(INTDIR)\vc60.idb"
	-@erase "$(INTDIR)\vc60.pdb"
	-@erase "$(INTDIR)\wg_main.obj"
	-@erase "$(OUTDIR)\gwindEDDB.dll"
	-@erase "$(OUTDIR)\gwindEDDB.exp"
	-@erase "$(OUTDIR)\gwindEDDB.ilk"
	-@erase "$(OUTDIR)\gwindEDDB.lib"
	-@erase "$(OUTDIR)\gwindEDDB.pdb"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

"$(INTDIR)" :
    if not exist "$(INTDIR)/$(NULL)" mkdir "$(INTDIR)"

CPP_PROJ=/nologo /MDd /W3 /Gm /Gi /GR /GX /ZI /Od /I "..\h" /I "..\..\res" /I "..\system" /I "..\gamesup" /I "..\ob" /D "EXPORT_GWIND_DLL" /D "_USRDLL" /D "EDITOR" /D "_DEBUG" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /Fp"$(INTDIR)\gwind.pch" /YX"stdinc.hpp" /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /GZ /c 
MTL_PROJ=/nologo /D "_DEBUG" /mktyplib203 /win32 
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\gwind.bsc" 
BSC32_SBRS= \
	
LINK32=link.exe
LINK32_FLAGS=User32.lib gdi32.lib advapi32.lib Shell32.lib /nologo /dll /incremental:yes /pdb:"$(OUTDIR)\gwindEDDB.pdb" /debug /machine:I386 /out:"$(OUTDIR)\gwindEDDB.dll" /implib:"$(OUTDIR)\gwindEDDB.lib" /pdbtype:sept 
LINK32_OBJS= \
	"$(INTDIR)\about.obj" \
	"$(INTDIR)\ai_panel.obj" \
	"$(INTDIR)\bitmapwindow.obj" \
	"$(INTDIR)\chatdialog.obj" \
	"$(INTDIR)\gleaderinfo.obj" \
	"$(INTDIR)\gmsgwin.obj" \
	"$(INTDIR)\goptdial.obj" \
	"$(INTDIR)\gresultswindow.obj" \
	"$(INTDIR)\multiplayerdialog.obj" \
	"$(INTDIR)\optwind.obj" \
	"$(INTDIR)\plyrdlg.obj" \
	"$(INTDIR)\timewind.obj" \
	"$(INTDIR)\titlebar.obj" \
	"$(INTDIR)\wg_main.obj" \
	"$(OUTDIR)\gamesupDB.lib" \
	"$(OUTDIR)\systemDB.lib" \
	"$(OUTDIR)\obEDDB.lib"

"$(OUTDIR)\gwindEDDB.dll" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

!ELSEIF  "$(CFG)" == "gwind - Win32 Editor Release"

OUTDIR=.\..\..\exe
INTDIR=.\o\Editor\Release
# Begin Custom Macros
OutDir=.\..\..\exe
# End Custom Macros

!IF "$(RECURSE)" == "0" 

ALL : "$(OUTDIR)\gwindED.dll"

!ELSE 

ALL : "ob - Win32 Editor Release" "system - Win32 Editor Release" "gamesup - Win32 Editor Release" "$(OUTDIR)\gwindED.dll"

!ENDIF 

!IF "$(RECURSE)" == "1" 
CLEAN :"gamesup - Win32 Editor ReleaseCLEAN" "system - Win32 Editor ReleaseCLEAN" "ob - Win32 Editor ReleaseCLEAN" 
!ELSE 
CLEAN :
!ENDIF 
	-@erase "$(INTDIR)\about.obj"
	-@erase "$(INTDIR)\ai_panel.obj"
	-@erase "$(INTDIR)\bitmapwindow.obj"
	-@erase "$(INTDIR)\chatdialog.obj"
	-@erase "$(INTDIR)\gleaderinfo.obj"
	-@erase "$(INTDIR)\gmsgwin.obj"
	-@erase "$(INTDIR)\goptdial.obj"
	-@erase "$(INTDIR)\gresultswindow.obj"
	-@erase "$(INTDIR)\multiplayerdialog.obj"
	-@erase "$(INTDIR)\optwind.obj"
	-@erase "$(INTDIR)\plyrdlg.obj"
	-@erase "$(INTDIR)\timewind.obj"
	-@erase "$(INTDIR)\titlebar.obj"
	-@erase "$(INTDIR)\vc60.idb"
	-@erase "$(INTDIR)\wg_main.obj"
	-@erase "$(OUTDIR)\gwindED.dll"
	-@erase "$(OUTDIR)\gwindED.exp"
	-@erase "$(OUTDIR)\gwindED.lib"
	-@erase "$(OUTDIR)\gwindED.pdb"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

"$(INTDIR)" :
    if not exist "$(INTDIR)/$(NULL)" mkdir "$(INTDIR)"

CPP_PROJ=/nologo /MD /W3 /Gi /GR /GX /Zd /O2 /I "..\h" /I "..\..\res" /I "..\system" /I "..\gamesup" /I "..\ob" /D "EXPORT_GWIND_DLL" /D "NDEBUG" /D "_USRDLL" /D "EDITOR" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /Fp"$(INTDIR)\gwind.pch" /YX"stdinc.hpp" /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /c 
MTL_PROJ=/nologo /D "NDEBUG" /mktyplib203 /win32 
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\gwind.bsc" 
BSC32_SBRS= \
	
LINK32=link.exe
LINK32_FLAGS=User32.lib gdi32.lib advapi32.lib Shell32.lib /nologo /dll /incremental:no /pdb:"$(OUTDIR)\gwindED.pdb" /debug /machine:I386 /out:"$(OUTDIR)\gwindED.dll" /implib:"$(OUTDIR)\gwindED.lib" 
LINK32_OBJS= \
	"$(INTDIR)\about.obj" \
	"$(INTDIR)\ai_panel.obj" \
	"$(INTDIR)\bitmapwindow.obj" \
	"$(INTDIR)\chatdialog.obj" \
	"$(INTDIR)\gleaderinfo.obj" \
	"$(INTDIR)\gmsgwin.obj" \
	"$(INTDIR)\goptdial.obj" \
	"$(INTDIR)\gresultswindow.obj" \
	"$(INTDIR)\multiplayerdialog.obj" \
	"$(INTDIR)\optwind.obj" \
	"$(INTDIR)\plyrdlg.obj" \
	"$(INTDIR)\timewind.obj" \
	"$(INTDIR)\titlebar.obj" \
	"$(INTDIR)\wg_main.obj" \
	"$(OUTDIR)\gamesup.lib" \
	"$(OUTDIR)\system.lib" \
	"$(OUTDIR)\obED.lib"

"$(OUTDIR)\gwindED.dll" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

!ENDIF 

.c{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.c{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<


!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("gwind.dep")
!INCLUDE "gwind.dep"
!ELSE 
!MESSAGE Warning: cannot find "gwind.dep"
!ENDIF 
!ENDIF 


!IF "$(CFG)" == "gwind - Win32 Release" || "$(CFG)" == "gwind - Win32 Debug" || "$(CFG)" == "gwind - Win32 Editor Debug" || "$(CFG)" == "gwind - Win32 Editor Release"
SOURCE=.\about.cpp

"$(INTDIR)\about.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\ai_panel.cpp

"$(INTDIR)\ai_panel.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\bitmapwindow.cpp

"$(INTDIR)\bitmapwindow.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\chatdialog.cpp

"$(INTDIR)\chatdialog.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\gleaderinfo.cpp

"$(INTDIR)\gleaderinfo.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\gmsgwin.cpp

"$(INTDIR)\gmsgwin.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\goptdial.cpp

"$(INTDIR)\goptdial.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\gresultswindow.cpp

"$(INTDIR)\gresultswindow.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\multiplayerdialog.cpp

"$(INTDIR)\multiplayerdialog.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\optwind.cpp

"$(INTDIR)\optwind.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\plyrdlg.cpp

"$(INTDIR)\plyrdlg.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\timewind.cpp

"$(INTDIR)\timewind.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\titlebar.cpp

"$(INTDIR)\titlebar.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\wg_main.cpp

"$(INTDIR)\wg_main.obj" : $(SOURCE) "$(INTDIR)"


!IF  "$(CFG)" == "gwind - Win32 Release"

"gamesup - Win32 Release" : 
   cd "\src\sf\wargamer\SRC\GAMESUP"
   $(MAKE) /$(MAKEFLAGS) /F .\gamesup.mak CFG="gamesup - Win32 Release" 
   cd "..\gwind"

"gamesup - Win32 ReleaseCLEAN" : 
   cd "\src\sf\wargamer\SRC\GAMESUP"
   $(MAKE) /$(MAKEFLAGS) /F .\gamesup.mak CFG="gamesup - Win32 Release" RECURSE=1 CLEAN 
   cd "..\gwind"

!ELSEIF  "$(CFG)" == "gwind - Win32 Debug"

"gamesup - Win32 Debug" : 
   cd "\src\sf\wargamer\SRC\GAMESUP"
   $(MAKE) /$(MAKEFLAGS) /F .\gamesup.mak CFG="gamesup - Win32 Debug" 
   cd "..\gwind"

"gamesup - Win32 DebugCLEAN" : 
   cd "\src\sf\wargamer\SRC\GAMESUP"
   $(MAKE) /$(MAKEFLAGS) /F .\gamesup.mak CFG="gamesup - Win32 Debug" RECURSE=1 CLEAN 
   cd "..\gwind"

!ELSEIF  "$(CFG)" == "gwind - Win32 Editor Debug"

"gamesup - Win32 Editor Debug" : 
   cd "\src\sf\wargamer\SRC\GAMESUP"
   $(MAKE) /$(MAKEFLAGS) /F .\gamesup.mak CFG="gamesup - Win32 Editor Debug" 
   cd "..\gwind"

"gamesup - Win32 Editor DebugCLEAN" : 
   cd "\src\sf\wargamer\SRC\GAMESUP"
   $(MAKE) /$(MAKEFLAGS) /F .\gamesup.mak CFG="gamesup - Win32 Editor Debug" RECURSE=1 CLEAN 
   cd "..\gwind"

!ELSEIF  "$(CFG)" == "gwind - Win32 Editor Release"

"gamesup - Win32 Editor Release" : 
   cd "\src\sf\wargamer\SRC\GAMESUP"
   $(MAKE) /$(MAKEFLAGS) /F .\gamesup.mak CFG="gamesup - Win32 Editor Release" 
   cd "..\gwind"

"gamesup - Win32 Editor ReleaseCLEAN" : 
   cd "\src\sf\wargamer\SRC\GAMESUP"
   $(MAKE) /$(MAKEFLAGS) /F .\gamesup.mak CFG="gamesup - Win32 Editor Release" RECURSE=1 CLEAN 
   cd "..\gwind"

!ENDIF 

!IF  "$(CFG)" == "gwind - Win32 Release"

"system - Win32 Release" : 
   cd "\src\sf\wargamer\SRC\SYSTEM"
   $(MAKE) /$(MAKEFLAGS) /F .\system.mak CFG="system - Win32 Release" 
   cd "..\gwind"

"system - Win32 ReleaseCLEAN" : 
   cd "\src\sf\wargamer\SRC\SYSTEM"
   $(MAKE) /$(MAKEFLAGS) /F .\system.mak CFG="system - Win32 Release" RECURSE=1 CLEAN 
   cd "..\gwind"

!ELSEIF  "$(CFG)" == "gwind - Win32 Debug"

"system - Win32 Debug" : 
   cd "\src\sf\wargamer\SRC\SYSTEM"
   $(MAKE) /$(MAKEFLAGS) /F .\system.mak CFG="system - Win32 Debug" 
   cd "..\gwind"

"system - Win32 DebugCLEAN" : 
   cd "\src\sf\wargamer\SRC\SYSTEM"
   $(MAKE) /$(MAKEFLAGS) /F .\system.mak CFG="system - Win32 Debug" RECURSE=1 CLEAN 
   cd "..\gwind"

!ELSEIF  "$(CFG)" == "gwind - Win32 Editor Debug"

"system - Win32 Editor Debug" : 
   cd "\src\sf\wargamer\SRC\SYSTEM"
   $(MAKE) /$(MAKEFLAGS) /F .\system.mak CFG="system - Win32 Editor Debug" 
   cd "..\gwind"

"system - Win32 Editor DebugCLEAN" : 
   cd "\src\sf\wargamer\SRC\SYSTEM"
   $(MAKE) /$(MAKEFLAGS) /F .\system.mak CFG="system - Win32 Editor Debug" RECURSE=1 CLEAN 
   cd "..\gwind"

!ELSEIF  "$(CFG)" == "gwind - Win32 Editor Release"

"system - Win32 Editor Release" : 
   cd "\src\sf\wargamer\SRC\SYSTEM"
   $(MAKE) /$(MAKEFLAGS) /F .\system.mak CFG="system - Win32 Editor Release" 
   cd "..\gwind"

"system - Win32 Editor ReleaseCLEAN" : 
   cd "\src\sf\wargamer\SRC\SYSTEM"
   $(MAKE) /$(MAKEFLAGS) /F .\system.mak CFG="system - Win32 Editor Release" RECURSE=1 CLEAN 
   cd "..\gwind"

!ENDIF 

!IF  "$(CFG)" == "gwind - Win32 Release"

"ob - Win32 Release" : 
   cd "\src\sf\wargamer\SRC\OB"
   $(MAKE) /$(MAKEFLAGS) /F .\ob.mak CFG="ob - Win32 Release" 
   cd "..\gwind"

"ob - Win32 ReleaseCLEAN" : 
   cd "\src\sf\wargamer\SRC\OB"
   $(MAKE) /$(MAKEFLAGS) /F .\ob.mak CFG="ob - Win32 Release" RECURSE=1 CLEAN 
   cd "..\gwind"

!ELSEIF  "$(CFG)" == "gwind - Win32 Debug"

"ob - Win32 Debug" : 
   cd "\src\sf\wargamer\SRC\OB"
   $(MAKE) /$(MAKEFLAGS) /F .\ob.mak CFG="ob - Win32 Debug" 
   cd "..\gwind"

"ob - Win32 DebugCLEAN" : 
   cd "\src\sf\wargamer\SRC\OB"
   $(MAKE) /$(MAKEFLAGS) /F .\ob.mak CFG="ob - Win32 Debug" RECURSE=1 CLEAN 
   cd "..\gwind"

!ELSEIF  "$(CFG)" == "gwind - Win32 Editor Debug"

"ob - Win32 Editor Debug" : 
   cd "\src\sf\wargamer\SRC\OB"
   $(MAKE) /$(MAKEFLAGS) /F .\ob.mak CFG="ob - Win32 Editor Debug" 
   cd "..\gwind"

"ob - Win32 Editor DebugCLEAN" : 
   cd "\src\sf\wargamer\SRC\OB"
   $(MAKE) /$(MAKEFLAGS) /F .\ob.mak CFG="ob - Win32 Editor Debug" RECURSE=1 CLEAN 
   cd "..\gwind"

!ELSEIF  "$(CFG)" == "gwind - Win32 Editor Release"

"ob - Win32 Editor Release" : 
   cd "\src\sf\wargamer\SRC\OB"
   $(MAKE) /$(MAKEFLAGS) /F .\ob.mak CFG="ob - Win32 Editor Release" 
   cd "..\gwind"

"ob - Win32 Editor ReleaseCLEAN" : 
   cd "\src\sf\wargamer\SRC\OB"
   $(MAKE) /$(MAKEFLAGS) /F .\ob.mak CFG="ob - Win32 Editor Release" RECURSE=1 CLEAN 
   cd "..\gwind"

!ENDIF 


!ENDIF 

