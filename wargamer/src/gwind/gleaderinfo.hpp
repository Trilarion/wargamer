/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef GLEADERINFO_HPP
#define GLEADERINFO_HPP

#ifndef __cplusplus
#error gLeaderInfo.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 * Generic Leader Info Support Functions
 *
 *----------------------------------------------------------------------
 */

#include "gwind.h"
#include "piclib.hpp"
#include "obdefs.hpp"                         

/*
 * It is intended that GUnitInfo be extended to take on the
 * funcionality of some of campwind\unitinfo.cpp\\UnitInfoData::drawLeaderDib()
 * so that the Campaign and Battle can share common code.
 */


class GLeaderInfo
{
    public:
        GWIND_DLL static PicLibrary::Picture getPortrait(ConstRefGLeader leader);
};





#endif /* GLEADERINFO_HPP */

