/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
#include "stdinc.hpp"
#include "optwind.hpp"

#include "bmp.hpp"
#include "DIB.hpp"
#include "DIB_blt.hpp"
#include "cscroll.hpp"
#include "fonts.hpp"
#include "simpstr.hpp"
#include "wmisc.hpp"
#include "scn_img.hpp"
#include <memory>
#include "app.hpp"
#include "scenario.hpp"
#include "options.hpp"
#include "resstr.hpp"


/*

  Filename : Optwind.cpp

  Description : Window class for FrontEnd, using custom windows, to allow selection of game options

*/




OptionsWindow::OptionsWindow(HWND parent, OptionWindFlags modeflags, int nrows, RECT * rect, OptionsWindowInterface * user) {

   d_user = user;

   d_modeflags = modeflags;

   d_isEnabled = true;

   ASSERT( (d_modeflags & GENERAL_OPTIONS) || (d_modeflags & CAMPAIGN_OPTIONS) || (d_modeflags & BATTLE_OPTIONS) );

   d_nCols = nrows;

   ASSERT(d_nCols > 0);

   d_scrollBar = NULL;

   d_bkDIB = NULL;
   d_boxCheckedDIB = NULL;
   d_boxUncheckedDIB = NULL;
   d_staticDIB = NULL;
   d_displayDIB = NULL;

   d_bkDIB = loadBMPDIB("frontend\\paper.bmp");
   ASSERT(d_bkDIB);
   d_boxCheckedDIB = loadBMPDIB("frontend\\BoxChecked.bmp");
   ASSERT(d_boxCheckedDIB);
   d_boxUncheckedDIB = loadBMPDIB("frontend\\BoxUnchecked.bmp");
   ASSERT(d_boxUncheckedDIB);

   d_optionItems = NULL;
   d_numOptionItems = 0;

   d_yScrollPos = 0;
   d_scrollBarWidth = GetSystemMetrics(SM_CXVSCROLL);

   setWindowSize(rect);

   getOptions();
   setupStaticDIB();

   drawScreenDIB();

    createWindow(
        WS_EX_LEFT,
        // GenericNoBackClass::className(),
        "OptionsWindow",
        WS_CHILD | WS_CLIPSIBLINGS,
        d_displayRect.left, d_displayRect.top, d_displayRect.right + d_scrollBarWidth, d_displayRect.bottom,
      parent,
        NULL
        // APP::instance()
   );

   ASSERT(getHWND());

    SetWindowText(getHWND(),"OptionsWindow");


   // create child scroll bar
   initCustomScrollBar(APP::instance());

   d_scrollBar = csb_create(
      0,
      WS_CHILD | SBS_VERT,
      d_displayRect.right,
      0,
      d_scrollBarWidth,
      d_displayRect.bottom,
      getHWND(),
      APP::instance()
   );

   ASSERT(d_scrollBar);

   csb_setButtonIcons(d_scrollBar, ScenarioImageLibrary::get(ScenarioImageLibrary::VScrollButtons));
   csb_setThumbBk(d_scrollBar, scenario->getScenarioBkDIB(Scenario::BackPatterns::VScrollThumbBackground));
   csb_setScrollBk(d_scrollBar, scenario->getScenarioBkDIB(Scenario::BackPatterns::VScrollBackground));
   csb_setBorder(d_scrollBar, &scenario->getBorderColors());

   csb_setPosition(d_scrollBar, 0);
   csb_setRange(d_scrollBar, 0, d_staticHeight);
   csb_setPage(d_scrollBar, d_displayRect.bottom);


   ShowWindow(d_scrollBar, SW_SHOW);
   ShowWindow(getHWND(), SW_SHOW);

   InvalidateRect(d_scrollBar, NULL, FALSE);
   UpdateWindow(d_scrollBar);
}




OptionsWindow::~OptionsWindow()
{
    selfDestruct();
   if(d_bkDIB) delete d_bkDIB;
   if(d_boxCheckedDIB) delete d_boxCheckedDIB;
   if(d_boxUncheckedDIB) delete d_boxUncheckedDIB;
   if(d_staticDIB) delete d_staticDIB;
   if(d_displayDIB) delete d_displayDIB;

   if(d_optionItems) delete[] d_optionItems;
}



DIB* OptionsWindow::loadBMPDIB(const char* name) {

   std::auto_ptr<char> fName(scenario->makeScenarioFileName(name));
   DIB* dib = BMP::newDIB(fName.get(), BMP::RBMP_Normal);
   return dib;
}



void
OptionsWindow::setWindowSize(RECT * rect) {

   // set display DIB rect
   d_displayRect.left = rect->left;
   d_displayRect.top = rect->top;
   d_displayRect.right = rect->right - d_scrollBarWidth;
   d_displayRect.bottom = rect->bottom;

   // move
    if(getHWND()) {
      MoveWindow(
         getHWND(),
         d_displayRect.left,
         d_displayRect.top,
         d_displayRect.right + d_scrollBarWidth,
         d_displayRect.bottom,
         TRUE
      );
   }

// csb_setPage(d_scrollBar, d_displayRect.bottom);

   if(d_yScrollPos >= (d_staticHeight - d_displayRect.bottom)) d_yScrollPos = (d_staticHeight - d_displayRect.bottom);
   if(d_yScrollPos < 0) d_yScrollPos = 0;

   csb_setPosition(d_scrollBar, d_yScrollPos);

   if(d_scrollBar) {
      MoveWindow(
         d_scrollBar,
         d_displayRect.right,
         0,
         d_scrollBarWidth,
         d_displayRect.bottom,
         TRUE
      );
   }

}


void
OptionsWindow::resize(RECT * rect) {

   setWindowSize(rect);
   setupStaticDIB();
   drawScreenDIB();
}



void
OptionsWindow::getOptions(void) {

   if(d_optionItems) delete[] d_optionItems;

   // find out how many
   d_numOptionItems = 0;

   if(d_modeflags & GENERAL_OPTIONS) { d_numOptionItems += OPT_GameOption_HowMany; }
   if(d_modeflags & CAMPAIGN_OPTIONS) { d_numOptionItems += OPT_CampaignOption_HowMany; }
   if(d_modeflags & BATTLE_OPTIONS) { }

   // dimension array
   d_optionItems = new OptionItem[d_numOptionItems];

   // when drawing - if we reach these vals, we know to add aheader
   d_generalStartIndex = -1;
   d_campaignStartIndex = -1;
   d_battleStartIndex = -1;

   d_numCategoryHeaders = 0;

   int index = 0;

   // fill in details
   if(d_modeflags & GENERAL_OPTIONS) {

      d_numCategoryHeaders++;
      d_generalStartIndex = index;

      for(GameOptionEnum o=OPT_GameOptionFirst; o<OPT_GameOption_HowMany; INCREMENT(o)) {

         int val = static_cast<int>(o);
         OptionItem & item = d_optionItems[index];

         item.flag = GENERAL_OPTIONS;
         item.enum_val = val;
         item.isChecked = Options::get(o);

         index++;
      }
   }

   if(d_modeflags & CAMPAIGN_OPTIONS) {

      d_numCategoryHeaders++;
      d_campaignStartIndex = index;

      for(CampaignOptionEnum o=OPT_CampaignOptionFirst; o<OPT_CampaignOption_HowMany; INCREMENT(o)) {

         int val = static_cast<int>(o);
         OptionItem & item = d_optionItems[index];

         item.flag = CAMPAIGN_OPTIONS;
         item.enum_val = val;
         item.isChecked = CampaignOptions::get(o);

         index++;
      }
   }

   if(d_modeflags & BATTLE_OPTIONS) {

      d_numCategoryHeaders++;
      d_battleStartIndex = index;
   }


}




void
OptionsWindow::setupStaticDIB(void) {

   d_itemHeight = d_displayRect.bottom / 12;
   d_headerHeight = d_itemHeight * 2;

   // get needed DIB dimensions
   d_staticHeight = (d_itemHeight * (d_numOptionItems / d_nCols)) + (d_headerHeight * d_numCategoryHeaders);
   if(d_staticHeight < d_displayRect.bottom) d_staticHeight = d_displayRect.bottom;
   d_staticWidth = d_displayRect.right + d_displayRect.left;

   // make DIB
   if(d_staticDIB) delete d_staticDIB;
   d_staticDIB = new DrawDIBDC(d_staticWidth, d_staticHeight);

   // copy bkgnd paper into DIB
   DrawDIB * tmpDIB = new DrawDIB(d_bkDIB->getWidth(), d_bkDIB->getHeight() );
   tmpDIB->blitFrom(d_bkDIB);
   d_staticDIB->fill(tmpDIB);
   delete tmpDIB;


   // width of each column
   int columnWidth = d_staticWidth / d_nCols;

   /*
   get check-box dimensions
   */
#if 0
   int checkWidth = columnWidth / 16;
   int checkHeight = d_itemHeight / 2;
   // ensure they're square
   if(checkHeight < checkWidth) checkWidth = checkHeight;
   else checkHeight = checkWidth;
   // get offsets from top,left of item
   int checkHOffset = columnWidth / 32;
   int checkYOffset = (d_itemHeight / 2) - (checkHeight / 2);
#endif
   int checkWidth = columnWidth / 16;
   int checkHeight = d_itemHeight / 2;
   // ensure they're square
   if(checkHeight < checkWidth) checkHeight = checkWidth;
   else checkHeight = checkWidth;
   // get offsets from top,left of item
   int checkHOffset = columnWidth / 32;
   int checkYOffset = (d_itemHeight / 2) - (checkHeight / 2);
   /*
   get text-rect dimensions
   */
#if 0
   int textWidth = (columnWidth / 4) * 3;
   int textHeight = d_itemHeight / 2;
   // get offsets from top,left of item
   int textHOffset = (columnWidth / 8);
   int textYOffset = (d_itemHeight / 2) - (textHeight / 2);
#endif

   int textWidth = (columnWidth / 4) * 3;
   int textHeight = d_itemHeight;
   // get offsets from top,left of item
   int textHOffset = (columnWidth / 8);
   int textYOffset = 0;//(d_itemHeight / 2) - (textHeight / 2);

   /*
   setup a font for use
   */
   HDC hdc = d_staticDIB->getDC();

   const int minFontHeight = 16;
   int fontHeight = 16;
   fontHeight = (fontHeight * (d_itemHeight)) / 1024;
   int h = maximum(fontHeight, minFontHeight);

   LogFont logFont;
   logFont.height(h);
   logFont.weight(FW_MEDIUM);
   logFont.face(scenario->fontName(Font_Bold));

   Fonts font;
   font.set(hdc, logFont);

   COLORREF color = RGB(0,0,0);//RGB(148,115,33);
   COLORREF oldColor = SetTextColor(hdc, color);

   SetBkMode(hdc, TRANSPARENT);




   /*
   main loop to go through options
   */
   int yPos = 0;
   int currentColumn = 0;

   for(int opt=0; opt<d_numOptionItems; opt++) {

      /*
      Note : Had to change these around cos, annoyingly, campaignOptionsSettings & gameOptionsSettings are the wrong way round
      */

      // if this is the general category start
      if(opt == d_generalStartIndex) {
         drawHeaderSection(yPos, InGameText::get(IDS_CampaignOptions));
         yPos += d_headerHeight;
         currentColumn = 0;
      }
      // if this is the campaign category start
      if(opt == d_campaignStartIndex) {
         drawHeaderSection(yPos, InGameText::get(IDS_GeneralOptions));
         yPos += d_headerHeight;
         currentColumn = 0;
      }
      // if this is the battle category start
      if(opt == d_battleStartIndex) {
         drawHeaderSection(yPos, InGameText::get(IDS_BattleOptions));
         yPos += d_headerHeight;
         currentColumn = 0;
      }


      OptionItem & item = d_optionItems[opt];

      int xPos = currentColumn * columnWidth;

      // set up item rects
      item.checkBoxRect.left = xPos + checkHOffset;
      item.checkBoxRect.right = checkWidth;
      item.checkBoxRect.top = yPos + checkYOffset;
      item.checkBoxRect.bottom = checkHeight;

      item.textRect.left = xPos + textHOffset;
      item.textRect.right = textWidth;
      item.textRect.top = yPos + textYOffset;
      item.textRect.bottom = textHeight;

      // draw check box
      DIB * chkDIB;
      if(item.isChecked) chkDIB = d_boxCheckedDIB;
      else chkDIB = d_boxUncheckedDIB;

      DIB_Utility::stretchDIB(
         d_staticDIB,
         item.checkBoxRect.left,
         item.checkBoxRect.top,
         item.checkBoxRect.right,
         item.checkBoxRect.bottom,
         chkDIB,
         0, 0, chkDIB->getWidth(), chkDIB->getHeight()
      );

      // draw text
      RECT textRect;
      textRect.left = item.textRect.left;
      textRect.right = item.textRect.left + item.textRect.right;
      textRect.top = item.textRect.top;
      textRect.bottom = item.textRect.top + item.textRect.bottom;

      int resID;
      if(item.flag & GENERAL_OPTIONS) resID = OptionSettingsInfo::gameOptionsSettings[item.enum_val].s_resID;
      else if(item.flag & CAMPAIGN_OPTIONS) resID = OptionSettingsInfo::campaignOptionsSettings[item.enum_val].s_resID;
      else if(item.flag & BATTLE_OPTIONS) { }
      else FORCEASSERT("No Options Type (General, Campaign, Battle, ...) ! ");

      SimpleString optionText;
      idsToString(optionText, resID);

      DrawText(hdc, optionText.toStr(), lstrlen(optionText.toStr()), &textRect, DT_SINGLELINE | DT_LEFT | DT_VCENTER);


      /*
      move onto next column
      */
      currentColumn++;
      if(currentColumn == d_nCols) {
         currentColumn = 0;
         yPos += d_itemHeight;
      }

   }

}



void
OptionsWindow::drawHeaderSection(int yPos, const char * text) {

   HDC hdc = d_staticDIB->getDC();

   /*
   get a font to use
   */
   const int minFontHeight = 20;
   int fontHeight = 25;
   fontHeight = (fontHeight * (d_itemHeight)) / 1024;
   int h = maximum(fontHeight, minFontHeight) + 5;

   LogFont logFont;
   logFont.height(h);
   logFont.weight(FW_HEAVY);
   logFont.face(scenario->fontName(Font_Bold));
   logFont.underline(true);

   Fonts font;
   font.set(hdc, logFont);

   COLORREF color = RGB(0,0,0);//RGB(148,115,33);
   COLORREF oldColor = SetTextColor(hdc, color);

   SetBkMode(hdc, TRANSPARENT);

   RECT textRect;
   textRect.left = 0;
   textRect.right = d_staticDIB->getWidth();
   textRect.top = yPos;
   textRect.bottom = yPos + d_headerHeight;

   DrawText(hdc, text, lstrlen(text), &textRect, DT_SINGLELINE | DT_CENTER | DT_VCENTER);
}





void
OptionsWindow::drawScreenDIB(void) {

   int width = d_displayRect.right;// - d_displayRect.left;
   int height = d_displayRect.bottom;

   if(!d_displayDIB) {
      d_displayDIB = new DrawDIBDC(width, height);
   }

   else if(d_displayDIB->getWidth() != width || d_displayDIB->getHeight() != height) {
      delete d_displayDIB;
      d_displayDIB = new DrawDIBDC(width, height);
   }

   // blit section of staticDIB to displayDIB
   if((height + d_yScrollPos) > d_staticHeight) height = d_staticHeight - d_yScrollPos;
   if(width > d_staticWidth) width = d_staticWidth;


   d_displayDIB->blit(
      0,0,width,height,
      d_staticDIB,
      0,d_yScrollPos
   );

   // if screen isn't active, then darken options area
   if(!d_isEnabled) {

      d_displayDIB->darken(25);
   }

   d_displayDIB->frame(
      0,
      0,
      d_displayDIB->getWidth(),
      d_displayDIB->getHeight(),
      0
   );

}







LRESULT
OptionsWindow::procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam) {

        switch(msg) {

//            HANDLE_MSG(hWnd, WM_CREATE, onCreate);
//            HANDLE_MSG(hWnd, WM_DESTROY, onDestroy);
//            HANDLE_MSG(hWnd, WM_COMMAND, onCommand);
            HANDLE_MSG(hWnd, WM_SIZE, onSize);
            HANDLE_MSG(hWnd, WM_PAINT, onPaint);
         HANDLE_MSG(hWnd, WM_VSCROLL, onVScroll);
//            HANDLE_MSG(hWnd, WM_MOUSEMOVE, onMouseMove);
//            HANDLE_MSG(hWnd, WM_DRAWITEM, onDrawItem);
            HANDLE_MSG(hWnd, WM_LBUTTONDOWN, onLButtonDown);
            HANDLE_MSG(hWnd, WM_LBUTTONUP, onLButtonUp);
//            HANDLE_MSG(hWnd, WM_SETCURSOR, onSetCursor);

            default:
                return defProc(hWnd, msg, wParam, lParam);
        }
}



void
OptionsWindow::onSize(HWND hwnd, UINT state, int cx, int cy) {

}





void
OptionsWindow::onPaint(HWND handle) {

   if(!d_displayDIB) return;


    PAINTSTRUCT ps;
    HDC hdc = BeginPaint(handle, &ps);
    HPALETTE oldPal = SelectPalette(hdc, Palette::get(), FALSE);
    RealizePalette(hdc);


    BitBlt(hdc,0,0,d_displayDIB->getWidth(),d_displayDIB->getHeight(),d_displayDIB->getDC(),0,0,SRCCOPY);


    SelectPalette(hdc, oldPal, FALSE);
    EndPaint(handle, &ps);

}











void
OptionsWindow::onVScroll(HWND hwnd, HWND hwndCtl, UINT code, int pos) {

   switch(code) {

      case SB_PAGEUP : {
         int inc = csb_getPage(d_scrollBar);;
         d_yScrollPos-=inc;
         if(d_yScrollPos<0) d_yScrollPos=0;
         csb_setPosition(d_scrollBar, d_yScrollPos);
         drawScreenDIB();
         InvalidateRect(getHWND(), NULL, FALSE);
         break;
      }
      case SB_PAGEDOWN : {
         int inc = csb_getPage(d_scrollBar);;
         d_yScrollPos+=inc;
         if(d_yScrollPos >= (d_staticHeight - d_displayRect.bottom)) d_yScrollPos = d_staticHeight - d_displayRect.bottom;
         csb_setPosition(d_scrollBar, d_yScrollPos);
         drawScreenDIB();
         InvalidateRect(getHWND(), NULL, FALSE);
         break;
      }
      case SB_LINEUP : {
         int inc = d_itemHeight;
         d_yScrollPos-=inc;
         if(d_yScrollPos<0) d_yScrollPos=0;
         csb_setPosition(d_scrollBar, d_yScrollPos);
         drawScreenDIB();
         InvalidateRect(getHWND(), NULL, FALSE);
         break;
      }
      case SB_LINEDOWN : {
         int inc = d_itemHeight;
         d_yScrollPos+=inc;
         if(d_yScrollPos >= (d_staticHeight - d_displayRect.bottom)) d_yScrollPos = d_staticHeight - d_displayRect.bottom;
         csb_setPosition(d_scrollBar, d_yScrollPos);
         drawScreenDIB();
         InvalidateRect(getHWND(), NULL, FALSE);
         break;
      }
      case SB_THUMBPOSITION : {
         d_yScrollPos=pos;
         if(d_yScrollPos<0) d_yScrollPos = 0;
         if(d_yScrollPos >= (d_staticHeight - d_displayRect.bottom)) d_yScrollPos = d_staticHeight - d_displayRect.bottom;
         csb_setPosition(d_scrollBar, d_yScrollPos);
         drawScreenDIB();
         InvalidateRect(getHWND(), NULL, FALSE);
         break;
      }
      default : {
         return;
      }
   }
}





void
OptionsWindow::onLButtonDown(HWND hwnd, BOOL fDoubleClick, int x, int y, UINT keyFlags) {

   POINT pt;
   pt.x = x;
   pt.y = y;

   for(int opt=0; opt<d_numOptionItems; opt++) {

      OptionItem & item = d_optionItems[opt];

      RECT rect;
      // set up item rect
      rect.left = item.checkBoxRect.left;
      rect.right = item.checkBoxRect.left + item.checkBoxRect.right;
      rect.top = item.checkBoxRect.top - d_yScrollPos;
      rect.bottom = (item.checkBoxRect.top + item.checkBoxRect.bottom) - d_yScrollPos;

      if(PtInRect(&rect, pt)) {

         d_mouseDownOptionItem = opt;
         return;
      }

   }
}



void
OptionsWindow::onLButtonUp(HWND hwnd, int x, int y, UINT keyFlags) {

   POINT pt;
   pt.x = x;
   pt.y = y;

   for(int opt=0; opt<d_numOptionItems; opt++) {

      OptionItem & item = d_optionItems[opt];

      RECT rect;
      // set up item rect
      rect.left = item.checkBoxRect.left;
      rect.right = item.checkBoxRect.left + item.checkBoxRect.right;
      rect.top = item.checkBoxRect.top - d_yScrollPos;
      rect.bottom = (item.checkBoxRect.top + item.checkBoxRect.bottom) - d_yScrollPos;

      if(PtInRect(&rect, pt)) {

         // see if this is the same check-box we clicked down in
         if(d_mouseDownOptionItem == opt) {

            // we are now on Custom skill setting
            CampaignOptions::setSkillLevel(CampaignOptions::Custom);
            d_user->SettingsChanged();

            // General
            if(item.flag == GENERAL_OPTIONS) {
               Options::toggle(static_cast<GameOptionEnum>(item.enum_val));
               reinit();
               InvalidateRect(getHWND(), NULL, FALSE);
            }
            // Campaign
            if(item.flag == CAMPAIGN_OPTIONS) {
               CampaignOptions::toggle(static_cast<CampaignOptionEnum>(item.enum_val));
               reinit();
               InvalidateRect(getHWND(), NULL, FALSE);
            }
            // Battle
            if(item.flag == BATTLE_OPTIONS) {

            }

            return;
         }
      }
   }
}

/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.2  2001/06/18 08:25:45  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
