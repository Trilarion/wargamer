/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *
 * Started by Paul Sample
 *----------------------------------------------------------------------
 *
 * Dialog for Adjusting Game Options
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "goptdial.hpp"
#include "grtypes.hpp"
#include "resstr.hpp"
#include "wmisc.hpp"
#include "winctrl.hpp"
#include "resdef.h"
#include "app.hpp"

namespace {

/*
 * Titles of pages
 */

static int s_pageTitles[] =
{
   IDS_OPT_RealismPage,    // RealismPage
   IDS_OPT_MiscPage,       // GameOptionsPage
};

/*
 * Positions and Sizes
 */

const int ItemWidth = 96;  // 74;
const int ItemHeight = 12; // 13
const int ItemXMargin = 4;
const int ItemYMargin = 10;

const int TabWidth = ItemWidth * 2 + ItemXMargin * 2;
const int TabHeight = ItemHeight * 11 + ItemYMargin;
const int MinimumTabWidth = TabWidth;
const int MinimumTabHeight = TabHeight;


class PlayerOptionPage;

/*
 * Main Options Class
 */

class OptDial :
   public GameOptionsDialog,
   public ModelessDialog
{
   public:
      OptDial(OptionsOwner* owner, OptionPageEnum page);
      ~OptDial();

       HWND getHWND() const { return ModelessDialog::getHWND(); }
       const PixelRect& getDialogRect() const { return d_tdRect; }
       void onCommand(HWND hWnd, int id, HWND hwndCtl, UINT codeNotify);
       void onOK();

        bool isVisible() const { return ModelessDialog::isVisible(); }
        bool isEnabled() const { return ModelessDialog::isEnabled(); }
        void enable(bool enable) { ModelessDialog::enable(enable); }
        void show(bool visible) { ModelessDialog::show(visible); }



  private:

       void setPage(OptionPageEnum page);

       ModelessDialog* getPage(OptionPageEnum page) const { return (ModelessDialog*)d_pages[page]; }

       // const char* getPageResName(OptionPageEnum page) const { return 0; }
       const DLGTEMPLATE* getPageTemplate(OptionPageEnum page) const;

       BOOL procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
       BOOL onInitDialog(HWND hwnd, HWND hwndFocus, LPARAM lParam);
       void onDestroy(HWND hwnd);
       void onClose(HWND hwnd);
       void onContextMenu(HWND hwnd, HWND hwndCtl, int x, int y);
       LRESULT onNotify(HWND hWnd, int id, NMHDR* lpNMHDR);

       void onSelChanged();
       void onOK(HWND hwnd);

       void setOption(GameOptionEnum what, Boolean how);

       void makePages();
		      // Create the pages


       enum
       {
         PageCount = 2
       };




   private:
      OptionsOwner* d_owner;
      PlayerOptionPage** d_pages;
   	HWND d_tabHwnd;						// Tab Dialog handle
	   int d_curPage;						// Current tabbed dialog Page
	   PixelRect d_tdRect;						// Area for tabbed dialogues to fit into

	   struct HelpIDList
	   {
		   DWORD s_control;
		   DWORD s_topic;
	   }* d_helpIDs;				// Array of Help IDs
};


/*
 * Base class for a page
 */

class PlayerOptionPage : public ModelessDialog {
   DLGTEMPLATE*   d_dialog;         // Dialog Template
   OptDial*   d_owner;
   OptionPageEnum d_page;
public:
   PlayerOptionPage(OptDial* owner, OptionPageEnum page)
   {
      d_owner = owner;
      d_dialog = 0;
      d_page = page;
   }
   virtual ~PlayerOptionPage() { d_dialog = 0; d_owner = 0; }

   // void enable();
   //    // Enable window
    //
   // void disable();
   //    // Disable window

   void setPosition();
      // Set window position

   const DLGTEMPLATE* dlgTemplate() const { return d_dialog; }

   DLGTEMPLATE* dlgTemplate(DLGTEMPLATE* dlgTemplate)
   {
      DLGTEMPLATE* old = d_dialog;
      d_dialog = dlgTemplate;
      return old;
   }  // Get and set Dialog Template

   OptDial* owner() const { return d_owner; }

   OptionPageEnum page() const { return d_page; }

   HWND create() { return create(d_owner->getHWND()); }
   HWND create(HWND hParent);
      // Create a window


   virtual void makeTemplate() = 0;
   virtual UINT getTitleID() = 0;

   virtual void saveSettings() = 0;
   virtual void initSettings() = 0;
private:
   virtual BOOL procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
   virtual BOOL onInitDialog(HWND hwnd, HWND hwndFocus, LPARAM lParam);
   // virtual void onDestroy(HWND hwnd) { }
   // virtual void onNotify(HWND hwnd, int id, NMHDR* lpNMHDR);

};

/*
 * Class for options pages created dynamically
 */

class AutoOptionPage : public PlayerOptionPage {
   public:
      AutoOptionPage(OptDial* parent, OptionPageEnum page);
      ~AutoOptionPage();

      void makeTemplate();

      UINT getTitleID() { return s_pageTitles[page()]; }

      void saveSettings();
      void initSettings();
   private:
      // BOOL procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
      // BOOL onInitDialog(HWND hwnd, HWND hwndFocus, LPARAM lParam);
      // void onDestroy(HWND hwnd) { }
      // void onNotify(HWND hwnd, int id, NMHDR* lpNMHDR);

      const OptionInfo* d_options;
      int d_maxOption;
};

/*
 * Helper Functions for creating Dialog templates
 */

namespace DTemplate
{
   inline int getResStringW(int resID, WCHAR* dest)
   {
   #if 1
      ResString title(resID);
      int nChars=  MultiByteToWideChar(CP_ACP, 0, title.c_str(), -1, dest, 100);
      ASSERT(nChars != 0);
   #else
      int nChars = LoadStringW(APP::instance(), pageTitles[d_page], lpwstr, 100);
      ASSERT(nChars != 0);
      nChars++;         // Count the 0.
   #endif

      return nChars;
   }

   inline void* align4(void* ptr)
   {
      return reinterpret_cast<void*>((reinterpret_cast<ULONG>(ptr) + 3) & ~3);
   }

   UWORD* makeHeader(DLGTEMPLATE* pTemplate, UINT titleID, int w, int h)
   {
      WCHAR* lpwstr;
      UWORD* lpw;

      pTemplate->style = WS_CHILD | DS_3DLOOK | DS_CONTROL | WS_DLGFRAME | DS_SETFONT;
      pTemplate->dwExtendedStyle = 0;
      pTemplate->cdit = 0;       // Initially there are no items
      pTemplate->x = 0;
      pTemplate->y = 0;
      pTemplate->cx = w;
      pTemplate->cy = h;

      lpw = reinterpret_cast<UWORD*>(pTemplate + 1);

      *lpw++ = 0; // No menu
      *lpw++ = 0; // Default to dialog

      lpwstr = reinterpret_cast<WCHAR*>(lpw);

      int nChars = getResStringW(titleID, lpwstr);
      lpwstr += nChars;
      lpw = reinterpret_cast<UWORD*>(lpwstr);

      /*
       * Set up Font... note that DS_FONT must be in the style for this to work
       */

      *lpw++ = 8;    // Height of font
      lpwstr = reinterpret_cast<WCHAR*>(lpw);
      nChars=  MultiByteToWideChar(CP_ACP, 0, "Helv", -1, lpwstr, 100);
      lpwstr += nChars;
      lpw = reinterpret_cast<UWORD*>(lpwstr);

      return lpw;
   }

   UWORD* addGroupBox(UWORD* lpw, UINT titleID, int x, int y, int w, int h)
   {
      DLGITEMTEMPLATE* item = reinterpret_cast<DLGITEMTEMPLATE*>(align4(lpw));

      item->style = BS_GROUPBOX | BS_LEFT | WS_CHILD | WS_VISIBLE;
      item->dwExtendedStyle = 0;
      item->x = x;
      item->y = y;
      item->cx = w;
      item->cy = h;
      item->id = static_cast<WORD>(-1);

      lpw = reinterpret_cast<UWORD*>(item + 1);
      *lpw++ = 0xffff;
      *lpw++ = 0x0080;        // Button

      WCHAR* lpwstr = reinterpret_cast<WCHAR*>(lpw);
      int nChars = getResStringW(titleID, lpwstr);
      lpwstr += nChars;

      lpw = reinterpret_cast<UWORD*>(lpwstr);
      *lpw++ = 0;          // No creation data

      return lpw;
   }


   UWORD* addButton(UWORD* lpw, int x, int y, int w, int h, int buttonID, UINT nameID, DWORD style)
   {
      DLGITEMTEMPLATE* item = reinterpret_cast<DLGITEMTEMPLATE*>(align4(lpw));

      item->style = style;
      item->dwExtendedStyle = 0;
      item->x = x;
      item->y = y;
      item->cx = w;  // - 2;
      item->cy = h;  // - 2;
      item->id = buttonID;

      lpw = reinterpret_cast<UWORD*>(item + 1);
      *lpw++ = 0xffff;
      *lpw++ = 0x0080;        // Button

      WCHAR* lpwstr = reinterpret_cast<WCHAR*>(lpw);
      int nChars = getResStringW(nameID, lpwstr);
      lpwstr += nChars;

      lpw = reinterpret_cast<UWORD*>(lpwstr);
      *lpw++ = 0;          // No creation data

      return lpw;
   }

   UWORD* addButton(UWORD* lpw, int x, int y, int w, int h, int buttonID, UINT nameID)
   {
     return addButton(lpw, x, y, w, h, buttonID, nameID, BS_AUTOCHECKBOX | WS_CHILD | WS_VISIBLE | WS_TABSTOP);
   }
};

/*
 * Inline functions
 */

const int RealismButtonID = 2000;
const int ButtonID = 1000;


inline int campaignOptionToButton(int opt)
{
   return opt + RealismButtonID;
}


inline int optionToButton(int opt)
{
   return opt + ButtonID;
}

/*--------------------------------------------------------------
 * PlayerOptionPage Implemention
 */

HWND PlayerOptionPage::create(HWND hParent)
{
    ASSERT(d_dialog != 0);
    HWND hDialog = createDialogIndirect(d_dialog, hParent);
    ASSERT(hDialog != NULL);
    return hDialog;
}


// void PlayerOptionPage::enable()
// {
//    ASSERT(hWnd != NULL);
//    ShowWindow(hWnd, SW_SHOW);
// }
//
// void PlayerOptionPage::disable()
// {
//    ASSERT(hWnd != NULL);
//    ShowWindow(hWnd, SW_HIDE);
// }


void PlayerOptionPage::setPosition()
{
   const RECT& r = d_owner->getDialogRect();

#ifdef DEBUG
   debugLog("setPosition(%ld,%ld,%ld,%ld)\n",
      r.left, r.top, r.right, r.bottom);
#endif

   SetWindowPos(getHWND(), HWND_TOP, r.left, r.top, r.right-r.left, r.bottom-r.top, 0);
}

#ifdef __WATCOM_CPLUSPLUS__
#pragma warning 387 9   // Disable Side effect warning
#endif

//lint -emacro(522,HANDLE_WM_INITDIALOG)
//lint -emacro(522,HANDLE_WM_COMMAND)

BOOL PlayerOptionPage::procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
   switch(msg)
   {
      case WM_INITDIALOG:
         HANDLE_WM_INITDIALOG(hWnd, wParam, lParam, onInitDialog);
         break;
#if 0
      case WM_DESTROY:
         HANDLE_WM_DESTROY(hWnd, wParam, lParam, onDestroy);
         break;
#endif
      case WM_COMMAND:
         HANDLE_WM_COMMAND(hWnd, wParam, lParam, d_owner->onCommand);
         break;

//      case WM_NOTIFY:
//         HANDLE_WM_NOTIFY(hWnd, wParam, lParam, onNotify);
//         break;

      default:
         return FALSE;
   }

   return TRUE;
}

#ifdef __WATCOM_CPLUSPLUS__
#pragma warning 387 3   // Re-enable Side effect warning
#endif

BOOL PlayerOptionPage::onInitDialog(HWND hwnd, HWND hwndFocus, LPARAM lParam)
{
   setPosition();
   initSettings();
   return TRUE;
}

#if 0
void PlayerOptionPage::onNotify(HWND hwnd, int id, NMHDR* lpNMHDR)
{
  switch(lpNMHDR->code)
  {

    case PSN_SETACTIVE:
    {
      DWORD flags;
      if(d_page == LastPage)
         flags = PSWIZB_BACK | PSWIZB_FINISH;
      else
         flags = PSWIZB_BACK | PSWIZB_NEXT;

      HWND hPropSheet = GetParent(hwnd);
      PostMessage(hPropSheet, PSM_SETWIZBUTTONS, 0, (LPARAM)flags);
      break;
    }

    case PSN_WIZFINISH:
      owner()->onOK();
      break;

    default:
      break;
  }
}
#endif

//====================== Auto-Created implementation =================

AutoOptionPage::AutoOptionPage(OptDial* parent, OptionPageEnum page) :
   PlayerOptionPage(parent, page),
   d_options(0),
   d_maxOption(0)
{
   //lint -save -e788 ... enum not used in switch
   switch(page)
   {
      case RealismPage:
         d_options = OptionSettingsInfo::campaignOptionsSettings;
         d_maxOption = OPT_CampaignOption_HowMany;
         break;
      case GameOptionsPage:
         d_options = OptionSettingsInfo::gameOptionsSettings;
         d_maxOption = OPT_GameOption_HowMany;
         break;
      default:
         FORCEASSERT("Illegal Page for AutoOptionPage");
   }
   //lint -restore
}

AutoOptionPage::~AutoOptionPage()
{
   ::operator delete(dlgTemplate(0));
   d_options = 0;
}


void AutoOptionPage::makeTemplate()
{
   static UBYTE buffer[2048];

   DLGTEMPLATE* pTemplate = reinterpret_cast<DLGTEMPLATE*>(buffer);
   UWORD* lpw;

   /*
    * Set up header
    */

   lpw = DTemplate::makeHeader(pTemplate, s_pageTitles[page()], TabWidth, TabHeight);

   /*
    * Set up outer box
    */

   lpw = DTemplate::addGroupBox(lpw, s_pageTitles[page()], 0, 0, TabWidth, TabHeight);
   pTemplate->cdit++;      // Increment number of controls

   /*
    * set up each item
    */

   const int maxY = pTemplate->cy - ItemHeight;
   const int maxX = pTemplate->cx - ItemWidth;

   int x = ItemXMargin;
   int y = ItemYMargin;

   // for(const OptionInfo* info = OptionSettingsInfo::gameOptionsSettings;
   //   info->s_option != OPT_GameOption_HowMany;
   //   info++)
   for(const OptionInfo* info = d_options;
      info->s_option != d_maxOption;
      info++)
   {
      if(info->s_page == page())
      {
         // Add button

// #ifdef DEBUG
         int id = (page() == RealismPage) ? campaignOptionToButton(info->s_option) :
           optionToButton(info->s_option);
// #else
//         int id = optionToButton(info->s_option);
// #endif
         lpw = DTemplate::addButton(lpw, x, y, ItemWidth, ItemHeight, id, info->s_resID);
         pTemplate->cdit++;      // Increment number of controls

         // Update coordinates

         y += ItemHeight;
         if(y >= maxY)
         {
            y = ItemYMargin;
            x += ItemWidth;
            ASSERT(x < maxX);
         }

      }
   }

   size_t used = reinterpret_cast<size_t>(lpw) - reinterpret_cast<size_t>(buffer);

   DLGTEMPLATE* templateMemory = reinterpret_cast<DLGTEMPLATE*>(operator new(used));
   ASSERT(templateMemory != 0);
   memcpy(templateMemory, buffer, used);

   dlgTemplate(templateMemory);
}

void AutoOptionPage::initSettings()
{
   HWND hwnd = getHWND();

   /*
    * Set buttons to correct state.
    */

   // for(const OptionInfo* info = OptionSettingsInfo::gameOptionsSettings;
   //   info->s_option != OPT_GameOption_HowMany;
   //   info++)
   for(const OptionInfo* info = d_options;
      info->s_option != d_maxOption;
      info++)
   {
      if(info->s_page == page())
      {
// #ifdef DEBUG
         int buttonID = (page() == RealismPage) ? campaignOptionToButton(info->s_option) :
            optionToButton(info->s_option);

         Boolean set = (page() == RealismPage) ? CampaignOptions::get(static_cast<CampaignOptionEnum>(info->s_option)) :
            Options::get(static_cast<GameOptionEnum>(info->s_option));
// #else
//         int buttonID = optionToButton(info->s_option);
//         Boolean set = Options::get(static_cast<GameOptionEnum>(info->s_option));
// #endif

         setButtonCheck(hwnd, buttonID, set);
      }
   }
}

void AutoOptionPage::saveSettings()
{
   HWND hwnd = getHWND();

   /*
    * Set buttons to correct state.
    */

   // for(const OptionInfo* info = OptionSettingsInfo::gameOptionsSettings;
   //   info->s_option != OPT_GameOption_HowMany;
   //   info++)
   for(const OptionInfo* info = d_options;
      info->s_option != d_maxOption;
      info++)
   {
      if(info->s_page == page())
      {
// #ifdef DEBUG
         if(page() == RealismPage)
         {
           Button b(hwnd, campaignOptionToButton(info->s_option));
           CampaignOptions::set(static_cast<CampaignOptionEnum>(info->s_option), b.getCheck());
         }
         else
// #endif
         {
           Button b(hwnd, optionToButton(info->s_option));
           Options::set(static_cast<GameOptionEnum>(info->s_option), b.getCheck());
         }
      }
   }
}



OptDial::OptDial(OptionsOwner* owner, OptionPageEnum page) :
   d_owner(owner),
   d_pages(0),
   d_tabHwnd(NULL),
   d_curPage(page),
   d_tdRect(),
   d_helpIDs(0)
{
   setDeleteSelfMode(false);
   makePages();
   d_curPage = page;

   HWND hwnd = createDialog(playerOptionsDlgName, owner->getHWND());
   ASSERT(hwnd);
}


OptDial::~OptDial()
{
	DestroyWindow(getHWND());

   d_owner = 0;
   delete[] d_pages; d_pages = 0;
   delete[] d_helpIDs; d_helpIDs = 0;

   
}

/*
 * Create pages
 */

void OptDial::makePages()
{
   d_pages = new PlayerOptionPage*[PageCount];
   ASSERT(d_pages != 0);

   for(int i = 0; i < PageCount; i++)
      d_pages[i] = 0;

   for(OptionPageEnum p = FirstPage; p < PageCount; INCREMENT(p))
   {
      d_pages[p] = new AutoOptionPage(this, p);
      ASSERT(d_pages[p] != 0);
      d_pages[p]->makeTemplate();
   }

   /*
    * Make HelpIDs
    */

   unsigned int nItems = OPT_GameOption_HowMany + OPT_CampaignOption_HowMany;
   const OptionInfo* pInfo;

   d_helpIDs = new HelpIDList[nItems + 1];      // leave space for empty one at end
   ASSERT(d_helpIDs != 0);
   HelpIDList* pID = d_helpIDs;

   for(pInfo = OptionSettingsInfo::gameOptionsSettings;
      pInfo->s_option != OPT_GameOption_HowMany;
      pInfo++, pID++)
   {
      pID->s_control = optionToButton(pInfo->s_option);
      pID->s_topic = pInfo->s_helpID;
   }
   for(pInfo = OptionSettingsInfo::campaignOptionsSettings;
      pInfo->s_option != OPT_CampaignOption_HowMany;
      pInfo++, pID++)
   {
      pID->s_control = campaignOptionToButton(pInfo->s_option);
      pID->s_topic = pInfo->s_helpID;
   }

   pID->s_control = 0;
   pID->s_topic = 0;
}

#ifdef __WATCOM_CPLUSPLUS__
#pragma warning 387 9   // Disable Side effect warning
#endif

BOOL OptDial::procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
   switch(msg)
   {
      case WM_INITDIALOG:
         HANDLE_WM_INITDIALOG(hWnd, wParam, lParam, onInitDialog);
         break;

      case WM_DESTROY:
         HANDLE_WM_DESTROY(hWnd, wParam, lParam, onDestroy);
         break;
      case WM_COMMAND:
         HANDLE_WM_COMMAND(hWnd, wParam, lParam, onCommand);
         break;
      case WM_CLOSE:
         HANDLE_WM_CLOSE(hWnd, wParam, lParam, onClose);
         break;
      case WM_CONTEXTMENU:
         HANDLE_WM_CONTEXTMENU(hWnd, wParam, lParam, onContextMenu);
         break;
      case WM_NOTIFY:
         HANDLE_WM_NOTIFY(hWnd, wParam, lParam, onNotify);
         break;
      default:
         return FALSE;
   }

   return TRUE;
}

#ifdef __WATCOM_CPLUSPLUS__
#pragma warning 387 3   // Re-enable Side effect warning
#endif

void OptDial::onContextMenu(HWND hwnd, HWND hwndCtl, int x, int y)
{
   ASSERT(d_helpIDs != 0);
//   WinHelp(hwndCtl, "help\\Wg95Help.hlp", HELP_CONTEXTMENU, reinterpret_cast<DWORD>(d_helpIDs));
}

BOOL OptDial::onInitDialog(HWND hwnd, HWND hwndFocus, LPARAM lParam)
{

   RECT rcTab;
   rcTab.left = 0;
   rcTab.top = 0;
   rcTab.right = MinimumTabWidth;      // Minimum size in dialog units
   rcTab.bottom = MinimumTabHeight;


   int i;
   for(i = 0; i < PageCount; i++)
   {
      // DLGTEMPLATE* dialog = pages[i]->getDialog();
      const DLGTEMPLATE* dialog = d_pages[i]->dlgTemplate();

      ASSERT(dialog != NULL);

      /*
       * Adjust for maximum size
       */

      if(dialog->cx > rcTab.right)
         rcTab.right = dialog->cx;
      if(dialog->cy > rcTab.bottom)
         rcTab.bottom = dialog->cy;
   }

#ifdef DEBUG
   debugLog("Largest Dialog (Dialog Units): %ld,%ld,%ld,%ld\n",
      rcTab.left, rcTab.top, rcTab.right, rcTab.bottom);
#endif

   // Get the position of the box (convert from dialog units)

   LONG dbUnits = GetDialogBaseUnits();
   LONG dbX = LOWORD(dbUnits);
   LONG dbY = HIWORD(dbUnits);
   // LONG xMargin = (3 * dbX) / 4;
   LONG xMargin = (2 * dbX) / 4;
   // LONG yTop = (20 * dbY) / 8;
   LONG yTop = 0; // ( * dbY) / 8;
   LONG yMargin = (4 * dbY) / 8;

   // Convert to pixel coordinates

   MapDialogRect(hwnd, &rcTab);

#ifdef DEBUG
   debugLog("Largest Dialog (Pixels): %ld,%ld,%ld,%ld\n",
      rcTab.left, rcTab.top, rcTab.right, rcTab.bottom);
#endif

   /*
    * Create a tabbed window
    * Don't be concerned with the size, because that will
    * be set up in a little while, but we need a handle to
    * a tabbed control to use AdjustRect
    *
    * Do need to set up width, so that AdjustRect doesn't think
    * it needs to do several rows of buttons
    */

   d_tabHwnd = CreateWindow(
      WC_TABCONTROL, "",
      WS_CHILD | WS_CLIPSIBLINGS | WS_VISIBLE |
         TCS_TOOLTIPS | TCS_RIGHTJUSTIFY | TCS_MULTILINE,
      0, 0, rcTab.right, 100,
      hwnd,
      (HMENU) POD_TABBED,
      APP::instance(),
      NULL);

   ASSERT(d_tabHwnd != NULL);

#ifdef DEBUG
   debugLog("TabHwnd = %08lx\n", (ULONG) d_tabHwnd);
#endif

   /*
    * Set up the tabbed titles
    * If possible try and get the title from the Dialog's CAPTION
    *
    * This must be done before using AdjustRect
    */

   TC_ITEM tie;
   tie.mask = TCIF_TEXT | TCIF_IMAGE;
   tie.iImage = -1;

   for(i = 0; i < PageCount; i++)
   {
      // ResString title(pageTitles[i]);
      ResString title(d_pages[i]->getTitleID());

      tie.pszText = const_cast<LPSTR>(title.c_str()); // (char*) pages[i]->getTitle();

      TabCtrl_InsertItem(d_tabHwnd, i, &tie);
   }

   /*
    * Convert coordinates to pixel coordinates
    * and Move the window
    */

   // Work out complete size with tabs, and shift to desired location

   TabCtrl_AdjustRect(d_tabHwnd, TRUE, &rcTab);
#ifdef DEBUG
   debugLog("Tab Size: %ld,%ld,%ld,%ld\n",
      rcTab.left, rcTab.top, rcTab.right, rcTab.bottom);
#endif
   OffsetRect(&rcTab, xMargin - rcTab.left, yTop - rcTab.top);
#ifdef DEBUG
   debugLog("Offset Tab: %ld,%ld,%ld,%ld\n",
      rcTab.left, rcTab.top, rcTab.right, rcTab.bottom);
#endif

   // Move tabbed Window

   SetWindowPos(d_tabHwnd, NULL,
      rcTab.left, rcTab.top,
      rcTab.right - rcTab.left, rcTab.bottom - rcTab.top,
      SWP_NOZORDER);

   // Get display area

   CopyRect(&d_tdRect, &rcTab);
   TabCtrl_AdjustRect(d_tabHwnd, FALSE, &d_tdRect);    // Get display area
#ifdef DEBUG
   debugLog("Display Area: %ld,%ld,%ld,%ld\n",
      d_tdRect.left(), d_tdRect.top(), d_tdRect.right(), d_tdRect.bottom());
#endif

   /*
    * Move the lower buttons (to rcTab.bottom + a bit)
    */

   RECT rcButton;
   SetRect(&rcButton, 0, 0, 0, 0);
   LONG y = rcTab.bottom + yMargin;

     LONG x = rcTab.right;
//   y = rcTab.bottom + yMargin;

     HWND hwndButton = GetDlgItem(hwnd, POD_OK);
     ASSERT(hwndButton != 0);
     GetWindowRect(hwndButton, &rcButton);
     x -= rcButton.right - rcButton.left;
     SetWindowPos(hwndButton, NULL, x, y, 0, 0, SWP_NOSIZE | SWP_NOZORDER);

#ifdef DEBUG
     debugLog("OK: %ld,%ld,%ld,%ld\n",
        x, y, rcButton.right-rcButton.left, rcButton.bottom-rcButton.top);
#endif


     hwndButton = GetDlgItem(hwnd, POD_CANCEL);
     ASSERT(hwndButton != 0);
     GetWindowRect(hwndButton, &rcButton);
     x -= rcButton.right - rcButton.left + xMargin;
     SetWindowPos(hwndButton, NULL, x, y, 0, 0, SWP_NOSIZE | SWP_NOZORDER);
#ifdef DEBUG
     debugLog("Cancel: %ld,%ld,%ld,%ld\n",
        x, y, rcButton.right-rcButton.left, rcButton.bottom-rcButton.top);
#endif

   /*
    * Adjust size of overall dialogue box
    *
    * Would be a good idea to adjust coordinates
    * so that box is on screen.
    *
    * e.g. if off right, adjust coords to be left of given position
    * if off bottom, adjust to be above.
    */

   rcTab.bottom = y + yMargin + rcButton.bottom - rcButton.top;
   rcTab.bottom += GetSystemMetrics(SM_CYCAPTION);
   rcTab.bottom += GetSystemMetrics(SM_CYDLGFRAME) * 2;
   // rcTab.bottom -= rcTab.top;

   rcTab.right += xMargin * 2;
   rcTab.right += GetSystemMetrics(SM_CXDLGFRAME) * 2;
   rcTab.right -= rcTab.left;

#ifdef DEBUG
   debugLog("Overall Window: %ld,%ld,%ld,%ld\n",
      rcTab.left, rcTab.top, rcTab.right, rcTab.bottom);
#endif

   PixelPoint position(100, 100);

   SetWindowPos(hwnd, NULL,
      position.getX(),
      position.getY(),
      rcTab.right,
      rcTab.bottom,
      SWP_NOZORDER);

   /*
    * Initialise pages
    */

   for(i = 0; i < PageCount; i++)
      d_pages[i]->create();

   /*
    * For testing: create an initial dialog
    */

   if(d_curPage > 0)
     TabCtrl_SetCurSel(d_tabHwnd, d_curPage);


   onSelChanged();

   /*
    * End of tabbed dialog setup
    * Now get on with positioning ourselves and setting the controls
    */

   /*===============================================================
    * Set the position
    * Ought to do a bit of checking to see if it will fit on screen
    */

  return TRUE;
}

void OptDial::onDestroy(HWND hwnd)
{
  SendMessage(d_owner->getHWND(), WM_PARENTNOTIFY, (WPARAM)WM_DESTROY, (LPARAM)hwnd);
}

void OptDial::onCommand(HWND hwnd, int id, HWND hwndCtl, UINT codeNotify)
{
   if(id == POD_OK)
      onOK();
   else if(id == POD_CANCEL)
      onClose(hwnd);
}

LRESULT OptDial::onNotify(HWND hWnd, int id, NMHDR* lpNMHDR)
{
#ifdef DEBUG
   debugLog("OptDial::onNotify(%08lx %d %08lx %d %d)\n",
      (ULONG) hWnd,
      (int) id,
      (ULONG) lpNMHDR->hwndFrom,
      (int) lpNMHDR->idFrom,
      (int) lpNMHDR->code);
#endif

   // Assume its from the tabbed dialog

   if(lpNMHDR->idFrom == POD_TABBED)
   {
      switch(lpNMHDR->code)
      {
      case TCN_SELCHANGING:
         return FALSE;

      case TCN_SELCHANGE:
         onSelChanged();
         break;

      default:
         break;
      }
   }

   return TRUE;
}

void OptDial::onSelChanged()
{
   int iSel = TabCtrl_GetCurSel(d_tabHwnd);
   if(d_curPage != iSel)
      d_pages[d_curPage]->show(false);  // disable();

   d_pages[iSel]->show(true);  // enable();
   d_curPage = iSel;
}


void OptDial::onClose(HWND hwnd)
{
    DestroyWindow(hwnd);
}

void OptDial::onOK()
{
   for(int page = 0; page < PageCount; page++)
   {
      d_pages[page]->saveSettings();
   }

   d_owner->optionsChanged();

   for(AlertBoxEnum i = AB_First; i < AB_HowMany; INCREMENT(i))
      AlertBoxOptions::set(i, Options::get(OPT_AlertBox));



    onClose(getHWND());
}



};    // private namespave


GameOptionsDialog* GameOptionsDialog::create(OptionsOwner* owner, OptionPageEnum page)
{
   return new OptDial(owner, page);
}

