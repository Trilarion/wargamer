/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef TBLDIMG_HPP
#define TBLDIMG_HPP

#ifndef __cplusplus
#error tbldimg.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 * Town Build Page Images
 *
 *----------------------------------------------------------------------
 */

#include "scn_img.hpp"
#include "imglib.hpp"
#include "scn_res.h"
#include "unittype.hpp"

class DrawDIB;

/*
 * ImageLibrary index's
 */

class TownBuildImages {
    public:
        enum Value {
	        InfantryImage = BuildPageImageEnum::InfantryImage,
	        CavalryImage = BuildPageImageEnum::CavalryImage,
	        ArtilleryImage = BuildPageImageEnum::ArtilleryImage,
	        OtherImage = BuildPageImageEnum::OtherImage,

            UnpressedPlus = BuildPageImageEnum::UnpressedPlus,
            PressedPlus = BuildPageImageEnum::PressedPlus,
            UnpressedMinus = BuildPageImageEnum::UnpressedMinus,
            PressedMinus = BuildPageImageEnum::PressedMinus,
            UnpressedAuto = BuildPageImageEnum::UnpressedAuto,
            PressedAuto = BuildPageImageEnum::PressedAuto,
            UnpressedPool = BuildPageImageEnum::UnpressedPool,
            PressedPool = BuildPageImageEnum::PressedPool,

	        // ClockImage = BuildPageImageEnum::ClockImage,

	        // CapitalImage = BuildPageImageEnum::CapitalImage,
	        ResourceImage = BuildPageImageEnum::ResourceImage,
	        ManpowerImage = BuildPageImageEnum::ManpowerImage,
	        BridgeImage = BuildPageImageEnum::BridgeImage,
	        BarrelImage = BuildPageImageEnum::BarrelImage,
	        CornImage = BuildPageImageEnum::CornImage,
	        UnderConstructionImage = BuildPageImageEnum::UnderConstructionImage,
	        AllowedInGarrisonImage = BuildPageImageEnum::AllowedInGarrisonImage,

	        Image_HowMany = BuildPageImageEnum::ImageCount,

            NoImage = -1
        };

        TownBuildImages() :
            d_lib(ScenarioImageLibrary::get(ScenarioImageLibrary::TownBuildPageImages))
        {
            ASSERT(d_lib != 0);
        }

        ~TownBuildImages()
        {
        }

        void getImageSize(Value index, int& w, int& h)
        {
            d_lib->getImageSize(index, w, h);
        }

        void blit(DrawDIB* dest, Value imgNum, LONG x, LONG y, bool shadow, UINT shadowCX) const
        {
            d_lib->blit(dest, imgNum, x, y, shadow, shadowCX);
        }

    	void blit(DrawDIB* dest, Value imgNum, LONG x, LONG y, Boolean darken = False) const
        {
            d_lib->blit(dest, imgNum, x, y, darken);
        }

        void stretchBlit(DrawDIB* dest, Value imgNum, LONG x, LONG y, LONG w, LONG h) const
        {
            d_lib->stretchBlit(dest, imgNum, x, y, w, h);
        }

        const ImageLibrary* lib() const
        {
            return d_lib;
        }

        Value getUnitTypeIndex(BasicUnitType::value t) const
        {
            ASSERT(t < BasicUnitType::HowMany);

            return static_cast<Value>(InfantryImage + t);
        }

    private:
        const ImageLibrary* d_lib;
};


#endif /* TBLDIMG_HPP */

