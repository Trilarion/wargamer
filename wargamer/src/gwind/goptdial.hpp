/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef _OPTDIAL_HPP
#define _OPTDIAL_HPP

#include "gwind.h"
#include "dialog.hpp"
#include "options.hpp"		// for GameOptionEnum

class OptionsOwner
{
   public:
      virtual void optionsChanged() = 0;
      virtual HWND getHWND() const = 0;
};


/*
 * Class that remainder of game can see
 */

class GameOptionsDialog : virtual public Window
{
   public:
      static GWIND_DLL GameOptionsDialog* create(OptionsOwner* owner, OptionPageEnum page);
      virtual ~GameOptionsDialog() { }
};


#endif
