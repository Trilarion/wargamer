/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *	About Dialog
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "about.hpp"
#include "app.hpp"
#include "version.h"
#include "myassert.hpp"
#include "simpstr.hpp"
#include "resstr.hpp"
#include <sstream>

namespace {

/*
 * aboutProc - processes messages for the about dialogue.
 */

// static const char s_version[] = "Version 0.1";
// static const char s_version[] = "Version " VER_FILEVERSION;
// static const char s_date[] = "Compiled on " VER_DATE " at " VER_TIME;
// static const char s_title[] = VER_PRODNAME;
// static const char s_copyright[] = "Copyright " VER_COPYRIGHT;

/*
 * Message Handler for DIALOG can't use message crackers because
 * it must return TRUE or FALSE depending on whether message
 * was processed or not.
 */


static BOOL CALLBACK aboutProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam )
{
	switch( msg )
	{
	case WM_INITDIALOG:
		{
            const DWORD UserLen = 100;
            char user[UserLen];
            DWORD userLen = UserLen;
            BOOL result = GetUserName(user, &userLen);
            ASSERT(result);

            std::ostringstream byText;
            byText << "Compiled by " << user; // << '\0';

            SimpleString txtVersion = InGameText::get(IDS_Version);
            txtVersion += " ";
            txtVersion += VER_FILEVERSION;

            SimpleString txtDate = InGameText::get(IDS_CompiledOn);
            txtDate += " ";
            txtDate += VER_DATE;
            txtDate += " ";
            txtDate += InGameText::get(IDS_At);
            txtDate += " ";
            txtDate += VER_TIME;

            SimpleString txtTitle = VER_PRODNAME;

            SimpleString txtCopyright = InGameText::get(IDS_Copyright);
            txtCopyright += " ";
            txtCopyright += VER_COPYRIGHT;

			SendDlgItemMessage(hwnd, ID_ABOUT_VERSION,   WM_SETTEXT, 0, reinterpret_cast<LPARAM>(txtVersion.toStr()));
			SendDlgItemMessage(hwnd, ID_ABOUT_DATE, 	 WM_SETTEXT, 0, reinterpret_cast<LPARAM>(txtDate.toStr()));
			SendDlgItemMessage(hwnd, ID_ABOUT_TITLE, 	 WM_SETTEXT, 0, reinterpret_cast<LPARAM>(txtTitle.toStr()));
			SendDlgItemMessage(hwnd, ID_ABOUT_COPYRIGHT, WM_SETTEXT, 0, reinterpret_cast<LPARAM>(txtCopyright.toStr()));
		}
		return TRUE;

	case WM_COMMAND:
		if(LOWORD(wParam) == ID_ABOUT_OK)
			EndDialog(hwnd, TRUE);
		break;

	case WM_CLOSE:
		EndDialog(hwnd, TRUE);
		break;

	default:
		return FALSE;
	}

	return TRUE;
}

};		// Namespace


void aboutDialog()
{
	DialogBox(APP::instance(), DLG_ABOUTBOX_RESOURCE, APP::getMainHWND(), (DLGPROC) aboutProc);
}


