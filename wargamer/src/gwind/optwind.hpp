/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef OPTWIND_HPP
#define OPTWIND_HPP

/*

  Filename : Optwind.hpp

  Description : Window class for FrontEnd, using custom windows, to allow selection of game options

*/

#include "wind.hpp"
#include "gwind.h"

class OptionsWindowInterface {

public:

	OptionsWindowInterface(void) { }
	virtual ~OptionsWindowInterface(void) { }

	virtual void SettingsChanged(void) = 0;
};


// mode flags
enum OptionWindFlagsEnum
{
   GENERAL_OPTIONS =	0x01,
   CAMPAIGN_OPTIONS = 0x02,
   BATTLE_OPTIONS	= 0x04
};

typedef UWORD OptionWindFlags;


// structure for display of an individual option
struct OptionItem {

	RECT checkBoxRect;
	RECT textRect;
	bool isChecked;

	// general, campaign or battle
	unsigned int flag;
	// options are held as an enumeration for each category
	unsigned int enum_val;
};

class DIB;
class DrawDIBDC;


class GWIND_DLL OptionsWindow : public WindowBaseND {

public:

	OptionsWindow(HWND parent, OptionWindFlags modeflags, int nrows, RECT * rect, OptionsWindowInterface * user);
	~OptionsWindow();

	DIB * loadBMPDIB(const char* name);

	void resize(RECT * rect);
	void enable() { d_isEnabled = true; }
	void disable() { d_isEnabled = false; }
	bool isEnabled() const { return d_isEnabled; }
	void redraw(void) { drawScreenDIB(); }
	void reinit(void) {
		getOptions();
		setupStaticDIB();
		drawScreenDIB();
	}

private:
    //--- Disable this by making it private
    void enable(bool visible) { WindowBaseND::enable(visible); }

	OptionsWindowInterface * d_user;
	OptionWindFlags d_modeflags;

	bool d_isEnabled;

	// the array of options
	OptionItem * d_optionItems;
	int d_numOptionItems;

	// num cols required
	int d_nCols;

	// start index of each category
	int d_generalStartIndex;
	int d_campaignStartIndex;
	int d_battleStartIndex;

	// number of categories
	int d_numCategoryHeaders;


	DIB * d_bkDIB;
	DIB * d_boxCheckedDIB;
	DIB * d_boxUncheckedDIB;

	int d_mouseDownOptionItem;


	// the full options dialog
	DrawDIBDC * d_staticDIB;
	// the part which is displayed
	DrawDIBDC * d_displayDIB;

	// pixel height of each header
	int d_headerHeight;
	// pixel height of each header
	int d_itemHeight;
	// total height of diapay bitmap
	int d_staticWidth;
	int d_staticHeight;

	// vertical scrolling position of display
	HWND d_scrollBar;
	int d_yScrollPos;
	int d_scrollBarWidth;

//	const ImageLibrary* d_scrollButtons;
//	const DrawDIB* d_scrollFillDIB;
//	const DrawDIB* d_scrollThumbDIB;

	// whole rectangle of static DIB
	RECT d_staticRect;
	// rectangle of staticDIB which is show in displayDIB
	RECT d_displayRect;


	void getOptions(void);
	void setupStaticDIB(void);
	void drawHeaderSection(int yPos, const char * text);

	void drawScreenDIB(void);

	void setWindowSize(RECT * rect);

	LRESULT procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
	void onSize(HWND hwnd, UINT state, int cx, int cy);
	void onPaint(HWND hwnd);
	void onVScroll(HWND hwnd, HWND hwndCtl, UINT code, int pos);
	void onLButtonDown(HWND hwnd, BOOL fDoubleClick, int x, int y, UINT keyFlags);
	void onLButtonUp(HWND hwnd, int x, int y, UINT keyFlags);

};



#endif
