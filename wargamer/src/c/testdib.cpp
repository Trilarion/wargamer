/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1995, Steven Morle-Green, All Rights Reserved
 *----------------------------------------------------------------------
 *
 * Test DIB program
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/06/13 08:52:37  greenius
 * Initial Import
 *
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "app.hpp"
#include "myassert.hpp"
#include "wind.hpp"
#include "palwin.hpp"
#include "testdib.h"
#include "dib.hpp"
#include "bmp.hpp"
#include "dib_blt.hpp"
// #include "generic.hpp"
#include "palwind.hpp"

static const char s_pic1[] = "nap1813\\frontend\\startscreen.bmp";
static const char s_pic2[] = "nap1813\\victory\\FrenchVicResults1.bmp";
static const char s_pic3[] = "nap1813\\frontend\\OpeningScreen.bmp";
static const char s_pic4[] = "nap1813\\frontend\\Flagswithborder.bmp";

class PicWindow : public WindowBaseND, public PaletteWindow
{
    public:
       PicWindow(HWND parent, const char* picName, const PixelRect& r);
       ~PicWindow();

    private:
       LRESULT procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
       BOOL onCreate(HWND hWnd, CREATESTRUCT* lpCreateStruct);
       void onDestroy(HWND hwnd);
       void onClose(HWND hwnd);
       void onPaint(HWND hwnd);
       BOOL onEraseBk(HWND hwnd, HDC hdc);
       void onSize(HWND hwnd, UINT state, int cx, int cy);
       
       void makeDIB();

    private:
       DIB* d_pic;
       DrawDIBDC* d_mainDIB;
};

PicWindow::PicWindow(HWND hParent, const char* picName, const PixelRect& r) : d_pic(0), d_mainDIB(0)
{
    d_pic = BMP::newDIB(picName, BMP::RBMP_ForcePalette);

    createWindow(0,
        // className(),
        picName,
         WS_POPUP |
         WS_CAPTION |
         WS_BORDER |
         WS_SYSMENU |
          WS_SIZEBOX |
         WS_VISIBLE |
         // WS_MINIMIZEBOX |
         WS_CLIPSIBLINGS,
        r.left(), r.top(), r.width(), r.height(),
        hParent,
        NULL);
        // APP::instance());

       SetWindowText(getHWND(), picName);

}

PicWindow::~PicWindow()
{
    delete d_pic;
    delete d_mainDIB;
}

LRESULT PicWindow::procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
    LRESULT result;
    if(handlePalette(hWnd, msg, wParam, lParam, result))
       return result;

   switch(msg)
   {
       HANDLE_MSG(hWnd, WM_CREATE,         onCreate);
      HANDLE_MSG(hWnd, WM_DESTROY,        onDestroy);
       HANDLE_MSG(hWnd, WM_CLOSE,            onClose);
       HANDLE_MSG(hWnd, WM_PAINT,          onPaint);
       HANDLE_MSG(hWnd, WM_ERASEBKGND,     onEraseBk);
      HANDLE_MSG(hWnd, WM_SIZE,           onSize);

   default:
      return defProc(hWnd, msg, wParam, lParam);
   }
}

BOOL PicWindow::onCreate(HWND hWnd, CREATESTRUCT* lpCreateStruct)
{
    makeDIB();
    return TRUE;
}

void PicWindow::onDestroy(HWND hwnd)
{
}

void PicWindow::onClose(HWND hwnd)
{
}

void PicWindow::onPaint(HWND hwnd)
{
    PAINTSTRUCT ps;
    HDC hdc = BeginPaint(hwnd, &ps);

    if(d_mainDIB)
        BitBlt(hdc, 0, 0, d_mainDIB->getWidth(), d_mainDIB->getHeight(), d_mainDIB->getDC(), 0, 0, SRCCOPY);

    EndPaint(hwnd, &ps);
}

BOOL PicWindow::onEraseBk(HWND hwnd, HDC hdc)
{
    return TRUE;
}

void PicWindow::onSize(HWND hwnd, UINT state, int cx, int cy)
{
    makeDIB();
    InvalidateRect(hwnd, NULL, FALSE);
}

void PicWindow::makeDIB()
{
    RECT r;
    GetClientRect(getHWND(), &r);

    int w = r.right - r.left;
    int h = r.bottom - r.top;

    if(d_mainDIB)
       d_mainDIB->resize(w, h);
    else  
       d_mainDIB = new DrawDIBDC(w, h);

    DIB_Utility::stretchFit(d_mainDIB, 0,0,w,h, d_pic);
}


class MainWindow : public WindowBaseND, public PaletteWindow
{
    public:
        MainWindow();
        ~MainWindow();
    private:
       LRESULT procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
       BOOL onCreate(HWND hWnd, CREATESTRUCT* lpCreateStruct);
       void onDestroy(HWND hwnd);
       void onClose(HWND hwnd);
       void onCommand(HWND hWnd, int id, HWND hwndCtl, UINT codeNotify);
       void onPaint(HWND hwnd);
       BOOL onEraseBk(HWND hwnd, HDC hdc);
       void onSize(HWND hwnd, UINT state, int cx, int cy);
       bool onUserWindowDestroyed(HWND hwnd, HWND hChild, int idChild);
      // void onParentNotify(HWND hwnd, UINT msg, HWND hwndChild, int idChild);

       void checkMenu(int id, BOOL flag);
       void enableMenu(int id, BOOL flag);

       void makeDIB();
       virtual const char* className() const { return s_className; }

       static ATOM registerClass();

    private:
       PalWindow* d_palWind;
       DIB* d_pic1;
       DIB* d_pic2;
       DrawDIBDC* d_mainDIB;
       PicWindow* d_picWind1;
       PicWindow* d_picWind2;

       static ATOM s_classAtom;
       static const char s_className[];
       static const char s_title[];
};

class TestDIBApp :
    public APP
{
    public:
        TestDIBApp();
        ~TestDIBApp();

        BOOL initApplication();
        BOOL initInstance();
        void endApplication();
        BOOL doCmdLine(LPCSTR cmd);

        Boolean processMessage(MSG* msg);
                // Process Message.
                //   Return True if application dealt with it
                //   False to continue as usual

    private:
        MainWindow* d_mainWind;
};


/*========================================================================
 * Main Windows Function
 */

int PASCAL WinMain (HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int nCmdShow)
{
        int result = 0;
        try
        {
                TestDIBApp application;
                result = application.execute(hInstance, hPrevInstance, lpszCmdLine, nCmdShow);
        }
#if defined(DEBUG)
        catch(AssertError e)
        {
                // gApp.pauseGame(TRUE);
                MessageBox(0, "Exception", "Quit Program", MB_ICONSTOP | MB_TASKMODAL | MB_SETFOREGROUND);
        }
#endif
        catch(GeneralError e)
        {
                // gApp.pauseGame(TRUE);
                MessageBox(0, "General Error Exception", e.get(), MB_ICONSTOP | MB_TASKMODAL | MB_SETFOREGROUND);
        }
        catch(...)
        {
                // gApp.pauseGame(TRUE);
                MessageBox(0, "Exception", "Unknown", MB_ICONSTOP | MB_TASKMODAL | MB_SETFOREGROUND);
        }

        return result;
}

/*
 * Test Window class
 */

ATOM MainWindow::registerClass()
{
   if(!s_classAtom)
   {
      WNDCLASS wc;

      wc.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
      wc.lpfnWndProc = baseWindProc;
      wc.cbClsExtra = 0;
      wc.cbWndExtra = sizeof(MainWindow*);
      wc.hInstance = APP::instance();
      wc.hIcon = LoadIcon(APP::instance(), MAKEINTRESOURCE(MAIN_ICON));
      wc.hCursor = LoadCursor(NULL, IDC_ARROW);
      wc.hbrBackground = NULL;   // (HBRUSH) (COLOR_BACKGROUND + 1);
      wc.lpszMenuName = MAKEINTRESOURCE(MAIN_MENU);
      wc.lpszClassName = s_className;  // szMainClass;   // szAppName;
      s_classAtom = RegisterClass(&wc);
   }

   ASSERT(s_classAtom != 0);

   return s_classAtom;
}

static ATOM MainWindow::s_classAtom = 0;
static const char MainWindow::s_className[] = "TestDIB";
static const char MainWindow::s_title[] = "Test DIB Palettes";

MainWindow::MainWindow() :
    d_palWind(0),
    d_pic1(0),
    d_pic2(0),
    d_mainDIB(0),
    d_picWind1(0),
    d_picWind2(0)
{
    registerClass();

    createWindow(0,
        // s_className,
        s_title,
        WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_VISIBLE,
        CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT,
        NULL,
        NULL);
        // APP::instance());
    SetWindowText(getHWND(), s_title);

    d_palWind = new PalWindow(getHWND());
    d_picWind1 = new PicWindow(getHWND(), s_pic3, PixelRect(100,100,420,300));
    d_picWind2 = new PicWindow(getHWND(), s_pic4, PixelRect(500,100,820,300));
}

MainWindow::~MainWindow()
{
    delete d_palWind;
    delete d_picWind1;
    delete d_picWind2;
    delete d_pic1;
    delete d_pic2;
    delete d_mainDIB;
}


LRESULT MainWindow::procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
#ifdef DEBUG_MAINWIND_MESSAGES
   debugLog("MainWindow_Imp::procMessage(%s)\n",
      getWMdescription(hWnd, msg, wParam, lParam));
#endif

    LRESULT result;
    if(handlePalette(hWnd, msg, wParam, lParam, result))
       return result;

   switch(msg)
   {
       HANDLE_MSG(hWnd, WM_CREATE,         onCreate);
      HANDLE_MSG(hWnd, WM_DESTROY,        onDestroy);
       HANDLE_MSG(hWnd, WM_CLOSE,            onClose);
       HANDLE_MSG(hWnd, WM_COMMAND,       onCommand);
       HANDLE_MSG(hWnd, WM_PAINT,          onPaint);
       HANDLE_MSG(hWnd, WM_ERASEBKGND,     onEraseBk);
      HANDLE_MSG(hWnd, WM_SIZE,           onSize);
      // HANDLE_MSG(hWnd, WM_PARENTNOTIFY,   onParentNotify);
        HANDLE_MSG(hWnd, WM_USER_WINDOWDESTROYED, onUserWindowDestroyed);

   default:
      return defProc(hWnd, msg, wParam, lParam);
   }
}

BOOL MainWindow::onCreate(HWND hWnd, CREATESTRUCT* lpCreateStruct)
{
    d_pic1 = BMP::newDIB(s_pic1, BMP::RBMP_ForcePalette);
    d_pic2 = BMP::newDIB(s_pic2, BMP::RBMP_ForcePalette);
    // d_mainDIB = new DrawDIBDC(lpCreateStruct->cx, lpCreateStruct->cy);

    makeDIB();

    return TRUE;
}

void MainWindow::onDestroy(HWND hwnd)
{
}

void MainWindow::onClose(HWND hWnd)
{
    PostQuitMessage(0);
}

void MainWindow::onCommand(HWND hWnd, int id, HWND hwndCtl, UINT codeNotify)
{
    switch(id)
    {
       case IDM_PALETTEWINDOW:
          if(d_palWind == 0)
          {
             d_palWind = new PalWindow(hWnd);
          }
          else
          {
             delete d_palWind;
             d_palWind = 0;
          }
          checkMenu(IDM_PALETTEWINDOW, d_palWind != 0);
          break;

       case IDM_EXIT:
          FORWARD_WM_CLOSE(hWnd, SendMessage);
          break;

       default:
          break;
    }
}


void MainWindow::onPaint(HWND hwnd)
{
    PAINTSTRUCT ps;
    HDC hdc = BeginPaint(hwnd, &ps);

    if(d_mainDIB)
        BitBlt(hdc, 0, 0, d_mainDIB->getWidth(), d_mainDIB->getHeight(), d_mainDIB->getDC(), 0, 0, SRCCOPY);

    EndPaint(hwnd, &ps);
}

BOOL MainWindow::onEraseBk(HWND hwnd, HDC hdc)
{
    return TRUE;
}

void MainWindow::onSize(HWND hwnd, UINT state, int cx, int cy)
{
    makeDIB();
    InvalidateRect(hwnd, NULL, FALSE);
}

bool MainWindow::onUserWindowDestroyed(HWND hwnd, HWND hChild, int idChild)
{
    if(d_palWind && (hChild == d_palWind->getHWND()))
    {
        d_palWind = 0;
        checkMenu(IDM_PALETTEWINDOW, false);
        return true;
    }

    return false;
}

#if 0
void MainWindow::onParentNotify(HWND hwnd, UINT msg, HWND hwndChild, int idChild)
{
    if(msg == WM_CLOSE)
    {
       if(d_palWind && (hwndChild == d_palWind->getHWND()))
       {
            delete d_palWind;
            d_palWind = 0;
            checkMenu(IDM_PALETTEWINDOW, false);
       }
    }

   if(msg == WM_DESTROY)
    {
       if(d_palWind && (hwndChild == d_palWind->getHWND()))
       {
          checkMenu(IDM_PALETTEWINDOW, false);
       }
    }
}
#endif

void MainWindow::checkMenu(int id, BOOL flag)
{
        HMENU menu = GetMenu(getHWND());
        ASSERT(menu != NULL);

        if(menu)
                CheckMenuItem(menu, id, MF_BYCOMMAND | (flag ? MF_CHECKED : MF_UNCHECKED));
}

void MainWindow::enableMenu(int id, BOOL flag)
{
        ASSERT((flag == TRUE) || (flag == FALSE));

        HMENU menu = GetMenu(getHWND());
        ASSERT(menu != NULL);

        if(menu)
                EnableMenuItem(menu, id, MF_BYCOMMAND | (flag ? MF_ENABLED : MF_GRAYED));
}

void MainWindow::makeDIB()
{
    RECT r;
    GetClientRect(getHWND(), &r);

    int w = r.right - r.left;
    int h = r.bottom - r.top;

    if(d_mainDIB)
       d_mainDIB->resize(w, h);
    else  
       d_mainDIB = new DrawDIBDC(w, h);

    DIB_Utility::stretchFit(d_mainDIB, 0,0,w,h, d_pic1);
    DIB_Utility::stretchFit(d_mainDIB, 0,0,w/2,h/2, d_pic2);
}


TestDIBApp::TestDIBApp() :
    d_mainWind(0)
{
}

TestDIBApp::~TestDIBApp()
{
    endApplication();
}

BOOL TestDIBApp::initApplication()
{
    return TRUE;
}

BOOL TestDIBApp::initInstance()
{
    // Create a window with a custom button in

    d_mainWind = new MainWindow;
    return true;
}

void TestDIBApp::endApplication()
{
    // Delete window

    delete d_mainWind;
    d_mainWind = 0;
}

BOOL TestDIBApp::doCmdLine(LPCSTR cmd)
{
    return true;
}


Boolean TestDIBApp::processMessage(MSG* msg)
{
    return false;
}




/*
 *----------------------------------------------------------------------
 * $Log$
 *----------------------------------------------------------------------
 */
