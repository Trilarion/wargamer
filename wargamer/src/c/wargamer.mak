# Microsoft Developer Studio Generated NMAKE File, Based on Wargamer.dsp
!IF "$(CFG)" == ""
CFG=Wargamer - Win32 Debug
!MESSAGE No configuration specified. Defaulting to Wargamer - Win32 Debug.
!ENDIF 

!IF "$(CFG)" != "Wargamer - Win32 Release" && "$(CFG)" != "Wargamer - Win32 Debug"
!MESSAGE Invalid configuration "$(CFG)" specified.
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "Wargamer.mak" CFG="Wargamer - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "Wargamer - Win32 Release" (based on "Win32 (x86) Application")
!MESSAGE "Wargamer - Win32 Debug" (based on "Win32 (x86) Application")
!MESSAGE 
!ERROR An invalid configuration is specified.
!ENDIF 

!IF "$(OS)" == "Windows_NT"
NULL=
!ELSE 
NULL=nul
!ENDIF 

CPP=cl.exe
MTL=midl.exe
RSC=rc.exe

!IF  "$(CFG)" == "Wargamer - Win32 Release"

OUTDIR=.\..\..\exe
INTDIR=.\o\Release
# Begin Custom Macros
OutDir=.\..\..\exe
# End Custom Macros

!IF "$(RECURSE)" == "0" 

ALL : "$(OUTDIR)\Wargamer.exe"

!ELSE 

ALL : "frontend - Win32 Release" "gwind - Win32 Release" "campgame - Win32 Release" "batgame - Win32 Release" "ob - Win32 Release" "gamesup - Win32 Release" "system - Win32 Release" "$(OUTDIR)\Wargamer.exe"

!ENDIF 

!IF "$(RECURSE)" == "1" 
CLEAN :"system - Win32 ReleaseCLEAN" "gamesup - Win32 ReleaseCLEAN" "ob - Win32 ReleaseCLEAN" "batgame - Win32 ReleaseCLEAN" "campgame - Win32 ReleaseCLEAN" "gwind - Win32 ReleaseCLEAN" "frontend - Win32 ReleaseCLEAN" 
!ELSE 
CLEAN :
!ENDIF 
	-@erase "$(INTDIR)\vc60.idb"
	-@erase "$(INTDIR)\vc60.pdb"
	-@erase "$(INTDIR)\wargamer.obj"
	-@erase "$(INTDIR)\wargamer.res"
	-@erase "$(OUTDIR)\Wargamer.exe"
	-@erase "$(OUTDIR)\Wargamer.pdb"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

"$(INTDIR)" :
    if not exist "$(INTDIR)/$(NULL)" mkdir "$(INTDIR)"

CPP_PROJ=/nologo /MD /W3 /Gi /GR /GX /Zi /O2 /I "..\h" /I "..\system" /I "..\gamesup" /I "..\..\res" /I "..\gwind" /I "..\campgame" /I "..\campdata" /I "..\camp_ai" /I "..\campwind" /I "..\ob" /I "..\campLogic" /I "..\mapwind" /I "..\frontend" /I "..\batgame" /I "..\batdata" /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_MBCS" /Fp"$(INTDIR)\Wargamer.pch" /YX"stdinc.hpp" /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /c 
MTL_PROJ=/nologo /D "NDEBUG" /mktyplib203 /win32 
RSC_PROJ=/l 0x809 /fo"$(INTDIR)\wargamer.res" /i "..\.." /d "NDEBUG" 
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\Wargamer.bsc" 
BSC32_SBRS= \
	
LINK32=link.exe
LINK32_FLAGS=comctl32.lib gdi32.lib USer32.lib winmm.lib /nologo /subsystem:windows /incremental:no /pdb:"$(OUTDIR)\Wargamer.pdb" /debug /machine:I386 /out:"$(OUTDIR)\Wargamer.exe" 
LINK32_OBJS= \
	"$(INTDIR)\wargamer.obj" \
	"$(INTDIR)\wargamer.res" \
	"$(OUTDIR)\system.lib" \
	"$(OUTDIR)\gamesup.lib" \
	"$(OUTDIR)\ob.lib" \
	"$(OUTDIR)\batgame.lib" \
	"$(OUTDIR)\campgame.lib" \
	"$(OUTDIR)\gwind.lib" \
	"$(OUTDIR)\frontend.lib"

"$(OUTDIR)\Wargamer.exe" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

!ELSEIF  "$(CFG)" == "Wargamer - Win32 Debug"

OUTDIR=.\..\..\exe
INTDIR=.\o\Debug
# Begin Custom Macros
OutDir=.\..\..\exe
# End Custom Macros

!IF "$(RECURSE)" == "0" 

ALL : "$(OUTDIR)\WargamerDB.exe"

!ELSE 

ALL : "frontend - Win32 Debug" "gwind - Win32 Debug" "campgame - Win32 Debug" "batgame - Win32 Debug" "ob - Win32 Debug" "gamesup - Win32 Debug" "system - Win32 Debug" "$(OUTDIR)\WargamerDB.exe"

!ENDIF 

!IF "$(RECURSE)" == "1" 
CLEAN :"system - Win32 DebugCLEAN" "gamesup - Win32 DebugCLEAN" "ob - Win32 DebugCLEAN" "batgame - Win32 DebugCLEAN" "campgame - Win32 DebugCLEAN" "gwind - Win32 DebugCLEAN" "frontend - Win32 DebugCLEAN" 
!ELSE 
CLEAN :
!ENDIF 
	-@erase "$(INTDIR)\vc60.idb"
	-@erase "$(INTDIR)\vc60.pdb"
	-@erase "$(INTDIR)\wargamer.obj"
	-@erase "$(INTDIR)\wargamer.res"
	-@erase "$(OUTDIR)\WargamerDB.exe"
	-@erase "$(OUTDIR)\WargamerDB.ilk"
	-@erase "$(OUTDIR)\WargamerDB.pdb"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

"$(INTDIR)" :
    if not exist "$(INTDIR)/$(NULL)" mkdir "$(INTDIR)"

CPP_PROJ=/nologo /MDd /W3 /Gm /Gi /GR /GX /ZI /Od /I "..\h" /I "..\system" /I "..\gamesup" /I "..\..\res" /I "..\gwind" /I "..\campgame" /I "..\campdata" /I "..\camp_ai" /I "..\campwind" /I "..\ob" /I "..\campLogic" /I "..\mapwind" /I "..\frontend" /I "..\batgame" /I "..\batdata" /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_MBCS" /Fp"$(INTDIR)\Wargamer.pch" /YX"stdinc.hpp" /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /GZ /c 
MTL_PROJ=/nologo /D "_DEBUG" /mktyplib203 /win32 
RSC_PROJ=/l 0x809 /fo"$(INTDIR)\wargamer.res" /i "..\.." /d "_DEBUG" 
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\Wargamer.bsc" 
BSC32_SBRS= \
	
LINK32=link.exe
LINK32_FLAGS=comctl32.lib gdi32.lib USer32.lib winmm.lib /nologo /subsystem:windows /incremental:yes /pdb:"$(OUTDIR)\WargamerDB.pdb" /debug /machine:I386 /out:"$(OUTDIR)\WargamerDB.exe" /pdbtype:sept 
LINK32_OBJS= \
	"$(INTDIR)\wargamer.obj" \
	"$(INTDIR)\wargamer.res" \
	"$(OUTDIR)\systemDB.lib" \
	"$(OUTDIR)\gamesupDB.lib" \
	"$(OUTDIR)\obDB.lib" \
	"$(OUTDIR)\batgameDB.lib" \
	"$(OUTDIR)\campgameDB.lib" \
	"$(OUTDIR)\gwindDB.lib" \
	"$(OUTDIR)\frontendDB.lib"

"$(OUTDIR)\WargamerDB.exe" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

!ENDIF 

.c{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.c{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<


!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("Wargamer.dep")
!INCLUDE "Wargamer.dep"
!ELSE 
!MESSAGE Warning: cannot find "Wargamer.dep"
!ENDIF 
!ENDIF 


!IF "$(CFG)" == "Wargamer - Win32 Release" || "$(CFG)" == "Wargamer - Win32 Debug"
SOURCE=..\..\res\english.rc
SOURCE=..\..\res\german.rc
SOURCE=..\..\res\multiplayer.rc
SOURCE=..\..\res\resource.rc
SOURCE=..\..\res\string.rc
SOURCE=..\..\res\version.rc
SOURCE=.\wargamer.cpp

"$(INTDIR)\wargamer.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=..\..\res\wargamer.rc

!IF  "$(CFG)" == "Wargamer - Win32 Release"


"$(INTDIR)\wargamer.res" : $(SOURCE) "$(INTDIR)"
	$(RSC) /l 0x809 /fo"$(INTDIR)\wargamer.res" /i "..\.." /i "\src\sf\wargamer\res" /d "NDEBUG" $(SOURCE)


!ELSEIF  "$(CFG)" == "Wargamer - Win32 Debug"


"$(INTDIR)\wargamer.res" : $(SOURCE) "$(INTDIR)"
	$(RSC) /l 0x809 /fo"$(INTDIR)\wargamer.res" /i "..\.." /i "\src\sf\wargamer\res" /d "_DEBUG" $(SOURCE)


!ENDIF 

!IF  "$(CFG)" == "Wargamer - Win32 Release"

"system - Win32 Release" : 
   cd "\src\sf\wargamer\SRC\SYSTEM"
   $(MAKE) /$(MAKEFLAGS) /F .\system.mak CFG="system - Win32 Release" 
   cd "..\c"

"system - Win32 ReleaseCLEAN" : 
   cd "\src\sf\wargamer\SRC\SYSTEM"
   $(MAKE) /$(MAKEFLAGS) /F .\system.mak CFG="system - Win32 Release" RECURSE=1 CLEAN 
   cd "..\c"

!ELSEIF  "$(CFG)" == "Wargamer - Win32 Debug"

"system - Win32 Debug" : 
   cd "\src\sf\wargamer\SRC\SYSTEM"
   $(MAKE) /$(MAKEFLAGS) /F .\system.mak CFG="system - Win32 Debug" 
   cd "..\c"

"system - Win32 DebugCLEAN" : 
   cd "\src\sf\wargamer\SRC\SYSTEM"
   $(MAKE) /$(MAKEFLAGS) /F .\system.mak CFG="system - Win32 Debug" RECURSE=1 CLEAN 
   cd "..\c"

!ENDIF 

!IF  "$(CFG)" == "Wargamer - Win32 Release"

"gamesup - Win32 Release" : 
   cd "\src\sf\wargamer\SRC\GAMESUP"
   $(MAKE) /$(MAKEFLAGS) /F .\gamesup.mak CFG="gamesup - Win32 Release" 
   cd "..\c"

"gamesup - Win32 ReleaseCLEAN" : 
   cd "\src\sf\wargamer\SRC\GAMESUP"
   $(MAKE) /$(MAKEFLAGS) /F .\gamesup.mak CFG="gamesup - Win32 Release" RECURSE=1 CLEAN 
   cd "..\c"

!ELSEIF  "$(CFG)" == "Wargamer - Win32 Debug"

"gamesup - Win32 Debug" : 
   cd "\src\sf\wargamer\SRC\GAMESUP"
   $(MAKE) /$(MAKEFLAGS) /F .\gamesup.mak CFG="gamesup - Win32 Debug" 
   cd "..\c"

"gamesup - Win32 DebugCLEAN" : 
   cd "\src\sf\wargamer\SRC\GAMESUP"
   $(MAKE) /$(MAKEFLAGS) /F .\gamesup.mak CFG="gamesup - Win32 Debug" RECURSE=1 CLEAN 
   cd "..\c"

!ENDIF 

!IF  "$(CFG)" == "Wargamer - Win32 Release"

"ob - Win32 Release" : 
   cd "\src\sf\wargamer\SRC\OB"
   $(MAKE) /$(MAKEFLAGS) /F .\ob.mak CFG="ob - Win32 Release" 
   cd "..\c"

"ob - Win32 ReleaseCLEAN" : 
   cd "\src\sf\wargamer\SRC\OB"
   $(MAKE) /$(MAKEFLAGS) /F .\ob.mak CFG="ob - Win32 Release" RECURSE=1 CLEAN 
   cd "..\c"

!ELSEIF  "$(CFG)" == "Wargamer - Win32 Debug"

"ob - Win32 Debug" : 
   cd "\src\sf\wargamer\SRC\OB"
   $(MAKE) /$(MAKEFLAGS) /F .\ob.mak CFG="ob - Win32 Debug" 
   cd "..\c"

"ob - Win32 DebugCLEAN" : 
   cd "\src\sf\wargamer\SRC\OB"
   $(MAKE) /$(MAKEFLAGS) /F .\ob.mak CFG="ob - Win32 Debug" RECURSE=1 CLEAN 
   cd "..\c"

!ENDIF 

!IF  "$(CFG)" == "Wargamer - Win32 Release"

"batgame - Win32 Release" : 
   cd "\src\sf\wargamer\src\batgame"
   $(MAKE) /$(MAKEFLAGS) /F .\batgame.mak CFG="batgame - Win32 Release" 
   cd "..\c"

"batgame - Win32 ReleaseCLEAN" : 
   cd "\src\sf\wargamer\src\batgame"
   $(MAKE) /$(MAKEFLAGS) /F .\batgame.mak CFG="batgame - Win32 Release" RECURSE=1 CLEAN 
   cd "..\c"

!ELSEIF  "$(CFG)" == "Wargamer - Win32 Debug"

"batgame - Win32 Debug" : 
   cd "\src\sf\wargamer\src\batgame"
   $(MAKE) /$(MAKEFLAGS) /F .\batgame.mak CFG="batgame - Win32 Debug" 
   cd "..\c"

"batgame - Win32 DebugCLEAN" : 
   cd "\src\sf\wargamer\src\batgame"
   $(MAKE) /$(MAKEFLAGS) /F .\batgame.mak CFG="batgame - Win32 Debug" RECURSE=1 CLEAN 
   cd "..\c"

!ENDIF 

!IF  "$(CFG)" == "Wargamer - Win32 Release"

"campgame - Win32 Release" : 
   cd "\src\sf\wargamer\src\campgame"
   $(MAKE) /$(MAKEFLAGS) /F .\campgame.mak CFG="campgame - Win32 Release" 
   cd "..\c"

"campgame - Win32 ReleaseCLEAN" : 
   cd "\src\sf\wargamer\src\campgame"
   $(MAKE) /$(MAKEFLAGS) /F .\campgame.mak CFG="campgame - Win32 Release" RECURSE=1 CLEAN 
   cd "..\c"

!ELSEIF  "$(CFG)" == "Wargamer - Win32 Debug"

"campgame - Win32 Debug" : 
   cd "\src\sf\wargamer\src\campgame"
   $(MAKE) /$(MAKEFLAGS) /F .\campgame.mak CFG="campgame - Win32 Debug" 
   cd "..\c"

"campgame - Win32 DebugCLEAN" : 
   cd "\src\sf\wargamer\src\campgame"
   $(MAKE) /$(MAKEFLAGS) /F .\campgame.mak CFG="campgame - Win32 Debug" RECURSE=1 CLEAN 
   cd "..\c"

!ENDIF 

!IF  "$(CFG)" == "Wargamer - Win32 Release"

"gwind - Win32 Release" : 
   cd "\src\sf\wargamer\src\gwind"
   $(MAKE) /$(MAKEFLAGS) /F .\gwind.mak CFG="gwind - Win32 Release" 
   cd "..\c"

"gwind - Win32 ReleaseCLEAN" : 
   cd "\src\sf\wargamer\src\gwind"
   $(MAKE) /$(MAKEFLAGS) /F .\gwind.mak CFG="gwind - Win32 Release" RECURSE=1 CLEAN 
   cd "..\c"

!ELSEIF  "$(CFG)" == "Wargamer - Win32 Debug"

"gwind - Win32 Debug" : 
   cd "\src\sf\wargamer\src\gwind"
   $(MAKE) /$(MAKEFLAGS) /F .\gwind.mak CFG="gwind - Win32 Debug" 
   cd "..\c"

"gwind - Win32 DebugCLEAN" : 
   cd "\src\sf\wargamer\src\gwind"
   $(MAKE) /$(MAKEFLAGS) /F .\gwind.mak CFG="gwind - Win32 Debug" RECURSE=1 CLEAN 
   cd "..\c"

!ENDIF 

!IF  "$(CFG)" == "Wargamer - Win32 Release"

"frontend - Win32 Release" : 
   cd "\src\sf\wargamer\src\frontend"
   $(MAKE) /$(MAKEFLAGS) /F .\frontend.mak CFG="frontend - Win32 Release" 
   cd "..\c"

"frontend - Win32 ReleaseCLEAN" : 
   cd "\src\sf\wargamer\src\frontend"
   $(MAKE) /$(MAKEFLAGS) /F .\frontend.mak CFG="frontend - Win32 Release" RECURSE=1 CLEAN 
   cd "..\c"

!ELSEIF  "$(CFG)" == "Wargamer - Win32 Debug"

"frontend - Win32 Debug" : 
   cd "\src\sf\wargamer\src\frontend"
   $(MAKE) /$(MAKEFLAGS) /F .\frontend.mak CFG="frontend - Win32 Debug" 
   cd "..\c"

"frontend - Win32 DebugCLEAN" : 
   cd "\src\sf\wargamer\src\frontend"
   $(MAKE) /$(MAKEFLAGS) /F .\frontend.mak CFG="frontend - Win32 Debug" RECURSE=1 CLEAN 
   cd "..\c"

!ENDIF 


!ENDIF 

