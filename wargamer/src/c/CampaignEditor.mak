# Microsoft Developer Studio Generated NMAKE File, Based on CampaignEditor.dsp
!IF "$(CFG)" == ""
CFG=CampaignEditor - Win32 Editor Release
!MESSAGE No configuration specified. Defaulting to CampaignEditor - Win32 Editor Release.
!ENDIF 

!IF "$(CFG)" != "CampaignEditor - Win32 Editor Debug" && "$(CFG)" != "CampaignEditor - Win32 Editor Release"
!MESSAGE Invalid configuration "$(CFG)" specified.
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "CampaignEditor.mak" CFG="CampaignEditor - Win32 Editor Release"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "CampaignEditor - Win32 Editor Debug" (based on "Win32 (x86) Application")
!MESSAGE "CampaignEditor - Win32 Editor Release" (based on "Win32 (x86) Application")
!MESSAGE 
!ERROR An invalid configuration is specified.
!ENDIF 

!IF "$(OS)" == "Windows_NT"
NULL=
!ELSE 
NULL=nul
!ENDIF 

CPP=cl.exe
MTL=midl.exe
RSC=rc.exe

!IF  "$(CFG)" == "CampaignEditor - Win32 Editor Debug"

OUTDIR=.\..\..\exe
INTDIR=.\o\Editor\Debug
# Begin Custom Macros
OutDir=.\..\..\exe
# End Custom Macros

!IF "$(RECURSE)" == "0" 

ALL : "$(OUTDIR)\CampaignEditorDB.exe"

!ELSE 

ALL : "gwind - Win32 Editor Debug" "campwind - Win32 Editor Debug" "gamesup - Win32 Editor Debug" "campgameNoBattle - Win32 Editor Debug" "system - Win32 Editor Debug" "$(OUTDIR)\CampaignEditorDB.exe"

!ENDIF 

!IF "$(RECURSE)" == "1" 
CLEAN :"system - Win32 Editor DebugCLEAN" "campgameNoBattle - Win32 Editor DebugCLEAN" "gamesup - Win32 Editor DebugCLEAN" "campwind - Win32 Editor DebugCLEAN" "gwind - Win32 Editor DebugCLEAN" 
!ELSE 
CLEAN :
!ENDIF 
	-@erase "$(INTDIR)\campaign.res"
	-@erase "$(INTDIR)\vc60.idb"
	-@erase "$(INTDIR)\vc60.pdb"
	-@erase "$(INTDIR)\wgedit.obj"
	-@erase "$(OUTDIR)\CampaignEditorDB.exe"
	-@erase "$(OUTDIR)\CampaignEditorDB.ilk"
	-@erase "$(OUTDIR)\CampaignEditorDB.pdb"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

"$(INTDIR)" :
    if not exist "$(INTDIR)/$(NULL)" mkdir "$(INTDIR)"

CPP_PROJ=/nologo /MDd /W3 /Gm /GR /GX /ZI /Od /I "..\h" /I "..\system" /I "..\gamesup" /I "..\..\res" /I "..\gwind" /I "..\campgame" /I "..\campdata" /I "..\camp_ai" /I "..\campwind" /I "..\ob" /I "..\campLogic" /I "..\mapwind" /I "..\frontend" /I "..\campedit" /D "EDITOR" /D "_DEBUG" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /Fp"$(INTDIR)\CampaignEditor.pch" /YX"stdinc.hpp" /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /GZ /c 
RSC_PROJ=/l 0x809 /fo"$(INTDIR)\campaign.res" /i "..\.." /d "_DEBUG" /d "EDITOR" /d "CUSTOMIZE" /d "DEBUG" 
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\CampaignEditor.bsc" 
BSC32_SBRS= \
	
LINK32=link.exe
LINK32_FLAGS=gdi32.lib comctl32.lib User32.lib winmm.lib /nologo /subsystem:windows /incremental:yes /pdb:"$(OUTDIR)\CampaignEditorDB.pdb" /debug /machine:I386 /out:"$(OUTDIR)\CampaignEditorDB.exe" /pdbtype:sept 
LINK32_OBJS= \
	"$(INTDIR)\wgedit.obj" \
	"$(INTDIR)\campaign.res" \
	"$(OUTDIR)\systemDB.lib" \
	"$(OUTDIR)\campgameNoBattleEDDB.lib" \
	"$(OUTDIR)\gamesupDB.lib" \
	"$(OUTDIR)\campwindEDDB.lib" \
	"$(OUTDIR)\gwindEDDB.lib"

"$(OUTDIR)\CampaignEditorDB.exe" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

!ELSEIF  "$(CFG)" == "CampaignEditor - Win32 Editor Release"

OUTDIR=.\..\..\exe
INTDIR=.\o\Editor\Release
# Begin Custom Macros
OutDir=.\..\..\exe
# End Custom Macros

!IF "$(RECURSE)" == "0" 

ALL : "$(OUTDIR)\CampaignEditor.exe"

!ELSE 

ALL : "gwind - Win32 Editor Release" "campwind - Win32 Editor Release" "gamesup - Win32 Editor Release" "campgameNoBattle - Win32 Editor Release" "system - Win32 Editor Release" "$(OUTDIR)\CampaignEditor.exe"

!ENDIF 

!IF "$(RECURSE)" == "1" 
CLEAN :"system - Win32 Editor ReleaseCLEAN" "campgameNoBattle - Win32 Editor ReleaseCLEAN" "gamesup - Win32 Editor ReleaseCLEAN" "campwind - Win32 Editor ReleaseCLEAN" "gwind - Win32 Editor ReleaseCLEAN" 
!ELSE 
CLEAN :
!ENDIF 
	-@erase "$(INTDIR)\campaign.res"
	-@erase "$(INTDIR)\vc60.idb"
	-@erase "$(INTDIR)\vc60.pdb"
	-@erase "$(INTDIR)\wgedit.obj"
	-@erase "$(OUTDIR)\CampaignEditor.exe"
	-@erase "$(OUTDIR)\CampaignEditor.ilk"
	-@erase "$(OUTDIR)\CampaignEditor.pdb"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

"$(INTDIR)" :
    if not exist "$(INTDIR)/$(NULL)" mkdir "$(INTDIR)"

CPP_PROJ=/nologo /MD /W3 /Gm /GR /GX /ZI /Od /I "..\h" /I "..\system" /I "..\gamesup" /I "..\..\res" /I "..\gwind" /I "..\campgame" /I "..\campdata" /I "..\camp_ai" /I "..\campwind" /I "..\ob" /I "..\campLogic" /I "..\mapwind" /I "..\frontend" /I "..\campedit" /D "NDEBUG" /D "EDITOR" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /Fp"$(INTDIR)\CampaignEditor.pch" /YX"stdinc.hpp" /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /GZ /c 
RSC_PROJ=/l 0x809 /fo"$(INTDIR)\campaign.res" /i "..\.." /d "NDEBUG" /d "EDITOR" /d "CUSTOMIZE" 
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\CampaignEditor.bsc" 
BSC32_SBRS= \
	
LINK32=link.exe
LINK32_FLAGS=kernel32.lib user32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib gdi32.lib comctl32.lib User32.lib winmm.lib /nologo /subsystem:windows /incremental:yes /pdb:"$(OUTDIR)\CampaignEditor.pdb" /debug /machine:I386 /out:"$(OUTDIR)\CampaignEditor.exe" /pdbtype:sept 
LINK32_OBJS= \
	"$(INTDIR)\wgedit.obj" \
	"$(INTDIR)\campaign.res" \
	"$(OUTDIR)\system.lib" \
	"$(OUTDIR)\campgameNoBattleED.lib" \
	"$(OUTDIR)\gamesup.lib" \
	"$(OUTDIR)\campwindED.lib" \
	"$(OUTDIR)\gwindED.lib"

"$(OUTDIR)\CampaignEditor.exe" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

!ENDIF 

.c{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.c{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

MTL_PROJ=/nologo /D "_DEBUG" /mktyplib203 /win32 

!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("CampaignEditor.dep")
!INCLUDE "CampaignEditor.dep"
!ELSE 
!MESSAGE Warning: cannot find "CampaignEditor.dep"
!ENDIF 
!ENDIF 


!IF "$(CFG)" == "CampaignEditor - Win32 Editor Debug" || "$(CFG)" == "CampaignEditor - Win32 Editor Release"
SOURCE=..\..\res\campaign.rc

!IF  "$(CFG)" == "CampaignEditor - Win32 Editor Debug"


"$(INTDIR)\campaign.res" : $(SOURCE) "$(INTDIR)"
	$(RSC) /l 0x809 /fo"$(INTDIR)\campaign.res" /i "..\.." /i "D:\src\sourceforge\wargamer\res" /d "_DEBUG" /d "EDITOR" /d "CUSTOMIZE" /d "DEBUG" $(SOURCE)


!ELSEIF  "$(CFG)" == "CampaignEditor - Win32 Editor Release"


"$(INTDIR)\campaign.res" : $(SOURCE) "$(INTDIR)"
	$(RSC) /l 0x809 /fo"$(INTDIR)\campaign.res" /i "..\.." /i "D:\src\sourceforge\wargamer\res" /d "NDEBUG" /d "EDITOR" /d "CUSTOMIZE" $(SOURCE)


!ENDIF 

SOURCE=..\..\res\english.rc
SOURCE=..\..\res\german.rc
SOURCE=..\..\res\multiplayer.rc
SOURCE=..\..\res\resource.rc
SOURCE=..\..\res\version.rc
SOURCE=.\wargamer.cpp
SOURCE=.\wgedit.cpp

"$(INTDIR)\wgedit.obj" : $(SOURCE) "$(INTDIR)"


!IF  "$(CFG)" == "CampaignEditor - Win32 Editor Debug"

"system - Win32 Editor Debug" : 
   cd "\SRC\SOURCEFORGE\WARGAMER\SRC\SYSTEM"
   $(MAKE) /$(MAKEFLAGS) /F .\system.mak CFG="system - Win32 Editor Debug" 
   cd "..\c"

"system - Win32 Editor DebugCLEAN" : 
   cd "\SRC\SOURCEFORGE\WARGAMER\SRC\SYSTEM"
   $(MAKE) /$(MAKEFLAGS) /F .\system.mak CFG="system - Win32 Editor Debug" RECURSE=1 CLEAN 
   cd "..\c"

!ELSEIF  "$(CFG)" == "CampaignEditor - Win32 Editor Release"

"system - Win32 Editor Release" : 
   cd "\SRC\SOURCEFORGE\WARGAMER\SRC\SYSTEM"
   $(MAKE) /$(MAKEFLAGS) /F .\system.mak CFG="system - Win32 Editor Release" 
   cd "..\c"

"system - Win32 Editor ReleaseCLEAN" : 
   cd "\SRC\SOURCEFORGE\WARGAMER\SRC\SYSTEM"
   $(MAKE) /$(MAKEFLAGS) /F .\system.mak CFG="system - Win32 Editor Release" RECURSE=1 CLEAN 
   cd "..\c"

!ENDIF 

!IF  "$(CFG)" == "CampaignEditor - Win32 Editor Debug"

"campgameNoBattle - Win32 Editor Debug" : 
   cd "\SRC\SOURCEFORGE\WARGAMER\src\campgame"
   $(MAKE) /$(MAKEFLAGS) /F .\campgameNoBattle.mak CFG="campgameNoBattle - Win32 Editor Debug" 
   cd "..\c"

"campgameNoBattle - Win32 Editor DebugCLEAN" : 
   cd "\SRC\SOURCEFORGE\WARGAMER\src\campgame"
   $(MAKE) /$(MAKEFLAGS) /F .\campgameNoBattle.mak CFG="campgameNoBattle - Win32 Editor Debug" RECURSE=1 CLEAN 
   cd "..\c"

!ELSEIF  "$(CFG)" == "CampaignEditor - Win32 Editor Release"

"campgameNoBattle - Win32 Editor Release" : 
   cd "\SRC\SOURCEFORGE\WARGAMER\src\campgame"
   $(MAKE) /$(MAKEFLAGS) /F .\campgameNoBattle.mak CFG="campgameNoBattle - Win32 Editor Release" 
   cd "..\c"

"campgameNoBattle - Win32 Editor ReleaseCLEAN" : 
   cd "\SRC\SOURCEFORGE\WARGAMER\src\campgame"
   $(MAKE) /$(MAKEFLAGS) /F .\campgameNoBattle.mak CFG="campgameNoBattle - Win32 Editor Release" RECURSE=1 CLEAN 
   cd "..\c"

!ENDIF 

!IF  "$(CFG)" == "CampaignEditor - Win32 Editor Debug"

"gamesup - Win32 Editor Debug" : 
   cd "\SRC\SOURCEFORGE\WARGAMER\SRC\GAMESUP"
   $(MAKE) /$(MAKEFLAGS) /F .\gamesup.mak CFG="gamesup - Win32 Editor Debug" 
   cd "..\c"

"gamesup - Win32 Editor DebugCLEAN" : 
   cd "\SRC\SOURCEFORGE\WARGAMER\SRC\GAMESUP"
   $(MAKE) /$(MAKEFLAGS) /F .\gamesup.mak CFG="gamesup - Win32 Editor Debug" RECURSE=1 CLEAN 
   cd "..\c"

!ELSEIF  "$(CFG)" == "CampaignEditor - Win32 Editor Release"

"gamesup - Win32 Editor Release" : 
   cd "\SRC\SOURCEFORGE\WARGAMER\SRC\GAMESUP"
   $(MAKE) /$(MAKEFLAGS) /F .\gamesup.mak CFG="gamesup - Win32 Editor Release" 
   cd "..\c"

"gamesup - Win32 Editor ReleaseCLEAN" : 
   cd "\SRC\SOURCEFORGE\WARGAMER\SRC\GAMESUP"
   $(MAKE) /$(MAKEFLAGS) /F .\gamesup.mak CFG="gamesup - Win32 Editor Release" RECURSE=1 CLEAN 
   cd "..\c"

!ENDIF 

!IF  "$(CFG)" == "CampaignEditor - Win32 Editor Debug"

"campwind - Win32 Editor Debug" : 
   cd "\SRC\SOURCEFORGE\WARGAMER\src\campwind"
   $(MAKE) /$(MAKEFLAGS) /F .\campwind.mak CFG="campwind - Win32 Editor Debug" 
   cd "..\c"

"campwind - Win32 Editor DebugCLEAN" : 
   cd "\SRC\SOURCEFORGE\WARGAMER\src\campwind"
   $(MAKE) /$(MAKEFLAGS) /F .\campwind.mak CFG="campwind - Win32 Editor Debug" RECURSE=1 CLEAN 
   cd "..\c"

!ELSEIF  "$(CFG)" == "CampaignEditor - Win32 Editor Release"

"campwind - Win32 Editor Release" : 
   cd "\SRC\SOURCEFORGE\WARGAMER\src\campwind"
   $(MAKE) /$(MAKEFLAGS) /F .\campwind.mak CFG="campwind - Win32 Editor Release" 
   cd "..\c"

"campwind - Win32 Editor ReleaseCLEAN" : 
   cd "\SRC\SOURCEFORGE\WARGAMER\src\campwind"
   $(MAKE) /$(MAKEFLAGS) /F .\campwind.mak CFG="campwind - Win32 Editor Release" RECURSE=1 CLEAN 
   cd "..\c"

!ENDIF 

!IF  "$(CFG)" == "CampaignEditor - Win32 Editor Debug"

"gwind - Win32 Editor Debug" : 
   cd "\SRC\SOURCEFORGE\WARGAMER\src\gwind"
   $(MAKE) /$(MAKEFLAGS) /F .\gwind.mak CFG="gwind - Win32 Editor Debug" 
   cd "..\c"

"gwind - Win32 Editor DebugCLEAN" : 
   cd "\SRC\SOURCEFORGE\WARGAMER\src\gwind"
   $(MAKE) /$(MAKEFLAGS) /F .\gwind.mak CFG="gwind - Win32 Editor Debug" RECURSE=1 CLEAN 
   cd "..\c"

!ELSEIF  "$(CFG)" == "CampaignEditor - Win32 Editor Release"

"gwind - Win32 Editor Release" : 
   cd "\SRC\SOURCEFORGE\WARGAMER\src\gwind"
   $(MAKE) /$(MAKEFLAGS) /F .\gwind.mak CFG="gwind - Win32 Editor Release" 
   cd "..\c"

"gwind - Win32 Editor ReleaseCLEAN" : 
   cd "\SRC\SOURCEFORGE\WARGAMER\src\gwind"
   $(MAKE) /$(MAKEFLAGS) /F .\gwind.mak CFG="gwind - Win32 Editor Release" RECURSE=1 CLEAN 
   cd "..\c"

!ENDIF 


!ENDIF 

