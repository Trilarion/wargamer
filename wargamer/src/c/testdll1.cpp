/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 * Testing System DLL...
 *
 * This goes into a lib
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "wmisc.hpp"
#include "simpstr.hpp"

class BuildString {
public:
  enum ID {
    Description,
    Time,
    MP,
    Res,
    Eff,
    Morale,

    NotBuilding,

    HowMany
  };

private:
  static Boolean s_initialized;
  static const int s_resourceIDs[HowMany];
  static SimpleString s_strings[];
public:
  static const char* text(ID id);

};

Boolean BuildString::s_initialized = False;
SimpleString BuildString::s_strings[BuildString::HowMany];

const int BuildString::s_resourceIDs[BuildString::HowMany] = {
    1,2,3,4,5,6,7
#if 0
   IDS_BID_DESCRIPTION,
   IDS_BID_TIME,
   IDS_BID_MP,
   IDS_BID_RES,
   IDS_BID_EFF,
   IDS_BID_MORALE,
   IDS_BID_NOTBUILDING
#endif
};

const char* BuildString::text(BuildString::ID id)
{
  ASSERT(id < HowMany);

  if(!s_initialized)
  {
    for(int i = 0; i < HowMany; i++)
    {
      idsToString(s_strings[i], s_resourceIDs[i]);
      ASSERT(s_strings[i].toStr());
    }

    s_initialized = True;
  }

  return s_strings[id].toStr();
}



const char* testBuild()
{
    return BuildString::text(BuildString::Time);
}

/*
 *----------------------------------------------------------------------
 * $Log$
 *----------------------------------------------------------------------
 */
