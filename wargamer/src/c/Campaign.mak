# Microsoft Developer Studio Generated NMAKE File, Based on Campaign.dsp
!IF "$(CFG)" == ""
CFG=Campaign - Win32 Debug
!MESSAGE No configuration specified. Defaulting to Campaign - Win32 Debug.
!ENDIF 

!IF "$(CFG)" != "Campaign - Win32 Release" && "$(CFG)" != "Campaign - Win32 Debug"
!MESSAGE Invalid configuration "$(CFG)" specified.
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "Campaign.mak" CFG="Campaign - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "Campaign - Win32 Release" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE "Campaign - Win32 Debug" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE 
!ERROR An invalid configuration is specified.
!ENDIF 

!IF "$(OS)" == "Windows_NT"
NULL=
!ELSE 
NULL=nul
!ENDIF 

CPP=cl.exe
MTL=midl.exe
RSC=rc.exe

!IF  "$(CFG)" == "Campaign - Win32 Release"

OUTDIR=.\..\..\exe
INTDIR=.\o\release
# Begin Custom Macros
OutDir=.\..\..\exe
# End Custom Macros

ALL : "$(OUTDIR)\Campaign.dll"


CLEAN :
	-@erase "$(INTDIR)\testcamp.obj"
	-@erase "$(INTDIR)\vc60.idb"
	-@erase "$(OUTDIR)\Campaign.dll"
	-@erase "$(OUTDIR)\Campaign.exp"
	-@erase "$(OUTDIR)\Campaign.lib"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

"$(INTDIR)" :
    if not exist "$(INTDIR)/$(NULL)" mkdir "$(INTDIR)"

CPP_PROJ=/nologo /MD /W3 /GR /GX /O2 /I "..\h" /I "..\..\res" /I "..\system" /I "..\gamesup" /I "..\ob" /D "NDEBUG" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "EXPORT_CAMPAIGN_DLL" /Fp"$(INTDIR)\Campaign.pch" /YX"stdinc.hpp" /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /c 
MTL_PROJ=/nologo /D "NDEBUG" /mktyplib203 /win32 
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\Campaign.bsc" 
BSC32_SBRS= \
	
LINK32=link.exe
LINK32_FLAGS=/nologo /dll /incremental:no /pdb:"$(OUTDIR)\Campaign.pdb" /machine:I386 /out:"$(OUTDIR)\Campaign.dll" /implib:"$(OUTDIR)\Campaign.lib" 
LINK32_OBJS= \
	"$(INTDIR)\testcamp.obj"

"$(OUTDIR)\Campaign.dll" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

!ELSEIF  "$(CFG)" == "Campaign - Win32 Debug"

OUTDIR=.\..\..\exe
INTDIR=.\o\debug
# Begin Custom Macros
OutDir=.\..\..\exe
# End Custom Macros

ALL : "$(OUTDIR)\CampaignDB.dll"


CLEAN :
	-@erase "$(INTDIR)\testcamp.obj"
	-@erase "$(INTDIR)\vc60.idb"
	-@erase "$(INTDIR)\vc60.pdb"
	-@erase "$(OUTDIR)\CampaignDB.dll"
	-@erase "$(OUTDIR)\CampaignDB.exp"
	-@erase "$(OUTDIR)\CampaignDB.ilk"
	-@erase "$(OUTDIR)\CampaignDB.lib"
	-@erase "$(OUTDIR)\CampaignDB.pdb"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

"$(INTDIR)" :
    if not exist "$(INTDIR)/$(NULL)" mkdir "$(INTDIR)"

CPP_PROJ=/nologo /MDd /W3 /Gm /GR /GX /ZI /Od /I "..\h" /I "..\..\res" /I "..\system" /I "..\gamesup" /I "..\ob" /D "_DEBUG" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "EXPORT_CAMPAIGN_DLL" /Fp"$(INTDIR)\Campaign.pch" /YX"stdinc.hpp" /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /GZ /c 
MTL_PROJ=/nologo /D "_DEBUG" /mktyplib203 /win32 
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\Campaign.bsc" 
BSC32_SBRS= \
	
LINK32=link.exe
LINK32_FLAGS=/nologo /dll /incremental:yes /pdb:"$(OUTDIR)\CampaignDB.pdb" /debug /machine:I386 /out:"$(OUTDIR)\CampaignDB.dll" /implib:"$(OUTDIR)\CampaignDB.lib" /pdbtype:sept 
LINK32_OBJS= \
	"$(INTDIR)\testcamp.obj"

"$(OUTDIR)\CampaignDB.dll" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

!ENDIF 

.c{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.c{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<


!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("Campaign.dep")
!INCLUDE "Campaign.dep"
!ELSE 
!MESSAGE Warning: cannot find "Campaign.dep"
!ENDIF 
!ENDIF 


!IF "$(CFG)" == "Campaign - Win32 Release" || "$(CFG)" == "Campaign - Win32 Debug"
SOURCE=.\testcamp.cpp

"$(INTDIR)\testcamp.obj" : $(SOURCE) "$(INTDIR)"



!ENDIF 

