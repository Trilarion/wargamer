/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
 * Test NameTable Application
 */

#include "stdinc.hpp"
#include "nameTable.hpp"

void main()
{
   StringFileTable testTable("testNameTable.dat");

   const char* testNames[] =
   {
      "1st Infantry Division",
      "Steven Green",
      "Greenius",
      "Steven Green",
      "James Broad",
      "2nd Infantry Division",
      "1st Infantry Division",
      "1st Swedish Light Infantry Division",
      "Steven Green",
      "Abingdon Heavy Artillery",
      "Abingdon Heavy Artillery"
   };

   const int nTestNames = sizeof(testNames) / sizeof(const char*);

   StringFileTable::ID ids[nTestNames];


   for (int i = 0; i < nTestNames; ++i)
   {
      ids[i] = testTable.add(testNames[i]);
      printf("add(%s) = %d\n", testNames[i], (int) ids[i]);
   }

   for (i = 0; i < nTestNames; ++i)
   {
      printf("get(%d) = %s\n", (int) ids[i], testTable.get(ids[i]));
   }

   testTable.writeFile();

}
/*
 *----------------------------------------------------------------------
 * $Log$
 *----------------------------------------------------------------------
 */
