/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 * Display Chunks in a Chunk File
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include <iostream.h>
#include "filecnk.hpp"
#include "except.hpp"


struct Usage { };

// static const char endChunkName[] = "End";


void dumpChunkFile(const char* s)
{
    WinFileBinaryReader f(s);
    if(!f.isOK())
        throw GeneralError("Couldn't open file %s", s);

    ULONG headLength;
    UWORD version;

    f.getULong(headLength);
    f.getUWord(version);

    cout << "HeaderLength:" << headLength << endl;
    cout << "Version:" << (version >> 16) << "." << (version & 0xffff) << endl;

    cout << "ID=";
    UBYTE c;
    do
    {
        f.getUByte(c);
        cout << c;
    } while(c != 0);
    cout << endl;

    char* token;

    SeekPos pos = f.getPos();

    do
    {
        ULONG len;
        f.getULong(len);
        token = f.getString();

        cout << "Chunk:" << token << ", " << len << endl;

        if(!f.isOK())
            throw GeneralError("Error reading %s", s);

        pos += len;
        f.seekTo(pos);

    } while(strcmp(token, endChunkName) != 0);


}










void main(int argc, char* argv[])
{
    try
    {

        if(argc != 2)
            throw Usage();

        dumpChunkFile(argv[1]);

    }
    catch(Usage u)
    {
        cout << "Usage: " << endl;
        cout << "  dumpchnk filename" << endl;
    }
    catch(GeneralError e)
    {
        cout << "GeneralError: " << e.get() << endl;
    }
    catch(...)
    {
        cout << "Unknown Exception" << endl;
    }

}

/*
 *----------------------------------------------------------------------
 * $Log$
 *----------------------------------------------------------------------
 */
