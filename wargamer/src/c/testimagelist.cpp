/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1995, Steven Morle-Green, All Rights Reserved
 *----------------------------------------------------------------------
 *
 * Test ImageList program
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/06/13 08:52:37  greenius
 * Initial Import
 *
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "app.hpp"
#include "myassert.hpp"
#include "wind.hpp"
#include "palwin.hpp"
// #include "dib.hpp"
#include "bmp.hpp"
#include "palwind.hpp"
#include "palette.hpp"
#include "fonts.hpp"
#include "wmisc.hpp"

/*
 * My Replacement DIBDC
 */

class RawDIB {
   UBYTE* d_bits;
   HBITMAP d_handle;
   BITMAPINFO* d_binfo;

   Boolean d_hasMask;
   ColourIndex d_mask;

    //--- Disallow copying
   RawDIB(const RawDIB& dib);
   RawDIB& operator =(const RawDIB& dib);
 public:
   RawDIB();
   RawDIB(int width, int height);
   ~RawDIB();
      // COnstructor does nothing, because this structure is only
      // used internally by DIB and it's derivatives

   /*
    * Manipulator Functions
    */

   void release();
      // Deletes bitmap (used by ~DIB())

   void makeBitmapInfo(UINT width, UINT height);
      // Setup bitmapInfo for given size
      // NOTE: Does not clear out old values!

   bool operator ! () const { return d_handle == NULL; }

   int getWidth() const
   {
       if(d_binfo)
         return d_binfo->bmiHeader.biWidth; 
       else
          return 0;
   }

   int getStorageWidth() const { return (getWidth() + 3) & ~3; }

   int getHeight() const
   {
      if(d_binfo)
       {
         if(d_binfo->bmiHeader.biHeight < 0)
            return -d_binfo->bmiHeader.biHeight;
         else
            return d_binfo->bmiHeader.biHeight;
       }
       else
          return 0;
   }

   int getPlanes() const
   {
      ASSERT(d_binfo != 0);
      return d_binfo->bmiHeader.biPlanes; 
   }

   int getBitCount() const 
   {
      ASSERT(d_binfo != 0);
      return d_binfo->bmiHeader.biBitCount; 
   }
   
   int getClrUsed() const
   {
      ASSERT(d_binfo != 0);
      return d_binfo->bmiHeader.biClrUsed; 
   }

   int getColours() const     // Get number of colours
   {
      if(getClrUsed() == 0)
         return 1 << getBitCount();
      else
         return getClrUsed();
   }

   const LPRGBQUAD getColourTable() const // Get the Colour Table
   {
      ASSERT(d_binfo != 0);
      return d_binfo->bmiColors; 
   }

   HBITMAP getHandle() const     // Get the DIB Handle
   {
      ASSERT(d_handle != NULL);
      return d_handle; 
   }

   BITMAPINFO * getBitmapInfo(void) const 
    {
      ASSERT(d_binfo != 0);
      return d_binfo;
   }
};

class DIB : public RawDIB {
   // Unimplemented Constructor
    DIB(const DIB& dib);
   DIB& operator = (const DIB& dib);
public:
   DIB() : RawDIB() { }
   DIB(int width, int height) : RawDIB(width, height) { }
   ~DIB() { }

   void resize(int width, int height) { RawDIB::makeBitmapInfo(width, height); }
};

/*
 * HDC that can be used to draw into a DrawDIB
 */

class DCObject {
protected:
   HDC hdc;
public:
   DCObject();
   ~DCObject();

   //------ automatic conversion complicates other modules
   // operator HDC() const { return hdc; }
   HDC getDC() const { return hdc; }

   /*
    * functions to wrap around the windows Select functions
    */

   HGDIOBJ selectObject(HGDIOBJ obj);
   int setBkMode(int bkMode);
   COLORREF setTextColor(COLORREF col);
   COLORREF setBkColor(COLORREF col);
   UINT setTextAlign(UINT textAlign);
   HFONT setFont(HFONT font);
   HPEN setPen(HPEN pen);
   HBRUSH setBrush(HBRUSH brush);

protected:
   void init(HBITMAP bm);
};


class DIBDC : public DIB, public DCObject
{
   typedef DrawDIB super;

    DIBDC& operator=(const DIBDC&);
   DIBDC(const RawDIB& dib);  // : DIB(dib) { init(getHandle()); }
public:
   DIBDC() : DIB() { }
   DIBDC(int width, int height) : DIB(width, height) { init(getHandle()); }

   void resize(int width, int height);
};

/*
 * DIB Functions
 */

RawDIB::RawDIB() :
    d_bits(0),
    d_handle(NULL),
    d_binfo(0), 
    d_hasMask(False),
    d_mask(0) 
{
}

RawDIB::RawDIB(int width, int height) :
   d_bits(0),
   d_handle(NULL),
   d_binfo(0),
   d_hasMask(False),
   d_mask(0)
{
   makeBitmapInfo(width, height);
}

#if 0
RawDIB::RawDIB(const RawDIB& dib) :
   d_bits(dib.d_bits),
   d_handle(dib.d_handle),
   d_binfo(dib.d_binfo),
   d_hasMask(dib.d_hasMask),
   d_mask(dib.d_mask)
{
}
#endif

RawDIB::~RawDIB()
{
   release();
}

#ifdef DEBUG_DIB
static int nDestroyed = 0;
#endif

void RawDIB::release()
{
   if(d_handle != NULL)
   {
#ifdef DIB_LOG
        s_dibLog.logDelete(this);
#endif

      DeleteObject(d_handle);
      d_handle = NULL;
      d_bits = 0;
   }
   if(d_binfo != NULL)
   {
      GlobalFreePtr(d_binfo);
      d_binfo = 0;
   }
}

/*
 * To make it top down, set height negative.
 */

void RawDIB::makeBitmapInfo(UINT width, UINT height)
{
   ASSERT(width > 0);
   ASSERT(height > 0);

   if( (d_binfo != 0) && (getWidth() == width) && (getHeight() == height))
      return;

    //--------- Not inverted
   // height = -height;

   /*
    * release old bitmap
    */

   // release();
   if(d_handle != NULL)
   {
#ifdef DIB_LOG
        s_dibLog.logDelete(this);
#endif

      DeleteObject(d_handle);
      d_handle = NULL;
      d_bits = 0;
   }

   /*
    * Set up bitmapinfo
    */

   if(d_binfo == 0)
   {
      long storage = sizeof(BITMAPINFOHEADER) + 256 * sizeof(RGBQUAD);
      d_binfo = (BITMAPINFO*) GlobalAllocPtr(GMEM_MOVEABLE, storage);
   }

   ASSERT(d_binfo != 0);

   if(!d_binfo)
      return;

   d_binfo->bmiHeader.biSize = sizeof(BITMAPINFOHEADER);
   d_binfo->bmiHeader.biWidth = width;
   d_binfo->bmiHeader.biHeight = height;
   d_binfo->bmiHeader.biPlanes = 1;
   d_binfo->bmiHeader.biBitCount = 8;
   d_binfo->bmiHeader.biCompression = BI_RGB;
   d_binfo->bmiHeader.biSizeImage = 0;
   d_binfo->bmiHeader.biXPelsPerMeter = 0;
   d_binfo->bmiHeader.biYPelsPerMeter = 0;
   d_binfo->bmiHeader.biClrUsed = 0;
   d_binfo->bmiHeader.biClrImportant = 0;

#if 0
   /*
    * Set up identity table (0..256)
    */

   PUSHORT col = (PUSHORT) d_binfo->bmiColors;
   for(int i = 0; i < 256; i++)
      *col++ = (USHORT) i;
#else
    PALETTEENTRY pe[256];
    GetPaletteEntries(Palette::get(), 0, 256, pe);

    for(int i = 0; i < 256; ++i)
    {
       d_binfo->bmiColors[i].rgbRed = pe[i].peRed;
       d_binfo->bmiColors[i].rgbGreen = pe[i].peGreen;
       d_binfo->bmiColors[i].rgbBlue = pe[i].peBlue;
       d_binfo->bmiColors[i].rgbReserved = 0;
    }
#endif

   /*
    * Get a d_handle
    */

   HDC hdc = GetDC(NULL);
   ASSERT(hdc != NULL);
   HPALETTE oldPal = SelectPalette(hdc, Palette::get(), FALSE);
   RealizePalette(hdc);
#if 0
   d_handle = CreateDIBSection(hdc, d_binfo, DIB_PAL_COLORS, (VOID**)&d_bits, NULL, 0);
#else
   d_handle = CreateDIBSection(hdc, d_binfo, DIB_RGB_COLORS, (VOID**)&d_bits, NULL, 0);
#endif
   SelectPalette(hdc, oldPal, FALSE);
   ReleaseDC(NULL, hdc);

   ASSERT(d_handle != NULL);
   ASSERT(d_bits != NULL);

#ifdef DIB_LOG
    s_dibLog.logNew(this);
#endif

}


DCObject::DCObject()
{
   hdc = CreateCompatibleDC(NULL);
   if(hdc == NULL)
      throw WinError("Couldn't create DC for DrawDIB");

#ifdef DEBUG_DIBDC
    s_dcLog.logNew(hdc);
#endif
}

DCObject::~DCObject()
{
   if(hdc)
   {
      DeleteDC(hdc);
#ifdef DEBUG_DIBDC
       s_dcLog.logDelete(hdc);
#endif
      hdc = NULL;
   }
}

void DCObject::init(HBITMAP bm)
{
   ASSERT(hdc != NULL);
   ASSERT(bm != NULL);

   if(SelectObject(hdc, bm) == NULL)
      throw WinError("couldn't select DIB into DC");
}


HGDIOBJ DCObject::selectObject(HGDIOBJ obj)
{
   ASSERT(hdc != NULL);
   HGDIOBJ ob = SelectObject(hdc, obj);
   if(ob == NULL)
      throw WinError("Could not select Object into DIB");
   return ob;
}

int DCObject::setBkMode(int bkMode)
{
   ASSERT(hdc != NULL);
   int bk = SetBkMode(hdc, bkMode);
   return bk;
}

COLORREF 
DCObject::setTextColor(COLORREF col)
{
   ASSERT(hdc != NULL);
   COLORREF c = SetTextColor(hdc, col);
   return c;
}

COLORREF
DCObject::setBkColor(COLORREF col)
{
   ASSERT(hdc != NULL);
   COLORREF c = SetBkColor(hdc, col);
   return c;
}

UINT 
DCObject::setTextAlign(UINT textAlign)
{
   ASSERT(hdc != NULL);
   UINT align = SetTextAlign(hdc, textAlign);
   return align;
}

HFONT
DCObject::setFont(HFONT font)
{
   ASSERT(hdc != NULL);
   return (HFONT) selectObject(font);
}

HPEN
DCObject::setPen(HPEN pen)
{
   ASSERT(hdc != NULL);
   return (HPEN) selectObject(pen);
}

HBRUSH
DCObject::setBrush(HBRUSH brush)
{
   ASSERT(hdc != NULL);
   return (HBRUSH) selectObject(brush);
}



void DIBDC::resize(int width, int height) 
{
   DIB::resize(width, height); 
   DCObject::init(getHandle());
}







/*
 * Main Window
 */

class MainWindow : public WindowBaseND, public PaletteWindow
{
    public:
        MainWindow();
        ~MainWindow();
    private:
       LRESULT procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
       BOOL onCreate(HWND hWnd, CREATESTRUCT* lpCreateStruct);
       void onDestroy(HWND hwnd);
       void onClose(HWND hwnd);
       void onCommand(HWND hWnd, int id, HWND hwndCtl, UINT codeNotify);
       void onPaint(HWND hwnd);
       // BOOL onEraseBk(HWND hwnd, HDC hdc);
       void onSize(HWND hwnd, UINT state, int cx, int cy);

       virtual const char* className() const { return s_className; }

       static ATOM registerClass();

    private:
       static ATOM s_classAtom;
       static const char s_className[];
       static const char s_title[];
};

class TestDIBApp :
    public APP
{
    public:
        TestDIBApp();
        ~TestDIBApp();

        BOOL initApplication();
        BOOL initInstance();
        void endApplication();
        BOOL doCmdLine(LPCSTR cmd);

        Boolean processMessage(MSG* msg);
                // Process Message.
                //   Return True if application dealt with it
                //   False to continue as usual

    private:
        MainWindow* d_mainWind;
};


/*========================================================================
 * Main Windows Function
 */

int PASCAL WinMain (HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int nCmdShow)
{
        int result = 0;
        try
        {
                TestDIBApp application;
                result = application.execute(hInstance, hPrevInstance, lpszCmdLine, nCmdShow);
        }
#if defined(DEBUG)
        catch(AssertError e)
        {
                // gApp.pauseGame(TRUE);
                MessageBox(0, "Exception", "Quit Program", MB_ICONSTOP | MB_TASKMODAL | MB_SETFOREGROUND);
        }
#endif
        catch(GeneralError e)
        {
                // gApp.pauseGame(TRUE);
                MessageBox(0, "General Error Exception", e.get(), MB_ICONSTOP | MB_TASKMODAL | MB_SETFOREGROUND);
        }
        catch(...)
        {
                // gApp.pauseGame(TRUE);
                MessageBox(0, "Exception", "Unknown", MB_ICONSTOP | MB_TASKMODAL | MB_SETFOREGROUND);
        }

        return result;
}

/*
 * Test Window class
 */

ATOM MainWindow::registerClass()
{
   if(!s_classAtom)
   {
      WNDCLASS wc;

      wc.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
      wc.lpfnWndProc = baseWindProc;
      wc.cbClsExtra = 0;
      wc.cbWndExtra = sizeof(MainWindow*);
      wc.hInstance = APP::instance();
      wc.hIcon = NULL;     // LoadIcon(APP::instance(), MAKEINTRESOURCE(MAIN_ICON));
      wc.hCursor = LoadCursor(NULL, IDC_ARROW);
      wc.hbrBackground = (HBRUSH) (COLOR_BACKGROUND);
      wc.lpszMenuName = NULL;    // MAKEINTRESOURCE(MAIN_MENU);
      wc.lpszClassName = s_className;  // szMainClass;   // szAppName;
      s_classAtom = RegisterClass(&wc);
   }

   ASSERT(s_classAtom != 0);

   return s_classAtom;
}

static ATOM MainWindow::s_classAtom = 0;
static const char MainWindow::s_className[] = "TestImageList";
static const char MainWindow::s_title[] = "Test ImageList with DIBs";

MainWindow::MainWindow()
{
    registerClass();

    createWindow(0,
        // s_className,
        s_title,
        WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_VISIBLE,
        CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT,
        NULL,
        NULL);
        // APP::instance());
    SetWindowText(getHWND(), s_title);
}

MainWindow::~MainWindow()
{
}


LRESULT MainWindow::procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
#ifdef DEBUG_MAINWIND_MESSAGES
   debugLog("MainWindow_Imp::procMessage(%s)\n",
      getWMdescription(hWnd, msg, wParam, lParam));
#endif

    LRESULT result;
    if(handlePalette(hWnd, msg, wParam, lParam, result))
       return result;

   switch(msg)
   {
       HANDLE_MSG(hWnd, WM_CREATE,         onCreate);
      HANDLE_MSG(hWnd, WM_DESTROY,        onDestroy);
       HANDLE_MSG(hWnd, WM_CLOSE,            onClose);
       HANDLE_MSG(hWnd, WM_COMMAND,       onCommand);
       HANDLE_MSG(hWnd, WM_PAINT,          onPaint);
       // HANDLE_MSG(hWnd, WM_ERASEBKGND,     onEraseBk);
      HANDLE_MSG(hWnd, WM_SIZE,           onSize);

   default:
      return defProc(hWnd, msg, wParam, lParam);
   }
}

BOOL MainWindow::onCreate(HWND hWnd, CREATESTRUCT* lpCreateStruct)
{
    HDC hdc = GetWindowDC(hWnd);

    HPALETTE hPal = CreateHalftonePalette(hdc);
    Palette::set(hPal);

    ReleaseDC(hWnd, hdc);

    return TRUE;
}

void MainWindow::onDestroy(HWND hwnd)
{
}

void MainWindow::onClose(HWND hWnd)
{
    PostQuitMessage(0);
}

void MainWindow::onCommand(HWND hWnd, int id, HWND hwndCtl, UINT codeNotify)
{
}


void MainWindow::onPaint(HWND hwnd)
{
    PAINTSTRUCT ps;
    HDC hdc = BeginPaint(hwnd, &ps);

    /*
     * Make a DIB
     */

    const int w = 64;
    const int h = 32;

    DIBDC* dib = new DIBDC(w, h);

    HBRUSH hBrush = CreateSolidBrush(RGB(255,255,255));
    PixelRect r(0,0,w,h);
    FillRect(dib->getDC(), &r, hBrush);
    DeleteObject(hBrush);

    int fontHeight = h/2;

    Fonts font;
    font.setFont(dib->getDC(), fontHeight, 0, FW_NORMAL);
    dib->setTextColor(RGB(0,0,0));
    wTextOut(dib->getDC(), 2, (h - fontHeight) / 2, "Testing");

    /*
     * Copy DIB to screen
     */

    int x = 10;
    int y = 10;

    BitBlt(hdc, x, y, w, h, dib->getDC(), 0, 0, SRCCOPY);

    y += h + 4;

    /*
     * Make an ImageList
     */

    HBITMAP hbm = dib->getHandle();
    // HBITMAP hbm = CreateCompatibleBitmap(dib->getDC(), dib->getWidth(), dib->getHeight());

    DIBSECTION dibInfo;
    GetObject(hbm, sizeof(dibInfo), &dibInfo);

    HBITMAP hTempBM = CreateCompatibleBitmap(dib->getDC(), 1,1);
    SelectObject(dib->getDC(), hTempBM);


    {
       HIMAGELIST hList = ImageList_Create(w, h, ILC_COLOR, 1, 0);
       ASSERT(hList);

       ImageList_SetBkColor(hList, CLR_NONE);

       int imgIndex = ImageList_Add(hList, hbm, 0);
       ASSERT(imgIndex == 0);
       ASSERT(ImageList_GetImageCount(hList) == 1);

       ImageList_Draw(hList, imgIndex, hdc, x, y, ILD_NORMAL);
       y += h + 4;

       IMAGEINFO imgInfo;
       BOOL result = ImageList_GetImageInfo(hList, 0, &imgInfo);

       GetObject(imgInfo.hbmImage, sizeof(dibInfo), &dibInfo);
#if 0
       HDC memDC = CreateCompatibleDC(NULL);
       SelectObject(memDC, imgInfo.hbmImage);

       BitBlt(hdc, x, y, w, h, memDC, 0, 0, SRCCOPY);
       y += h + 4;

       BitBlt(memDC, 
        imgInfo.rcImage.left, 
        imgInfo.rcImage.top, 
        imgInfo.rcImage.right - imgInfo.rcImage.left,
        imgInfo.rcImage.bottom - imgInfo.rcImage.top,
        dib->getDC(), 0, 0, SRCCOPY);

       DeleteDC(memDC);
#endif

       ImageList_Draw(hList, imgIndex, hdc, x, y, ILD_NORMAL);
       y += h + 4;


       ImageList_Destroy(hList);
    }

    {
       HIMAGELIST hList = ImageList_Create(w/2, h, ILC_COLOR8 | ILC_MASK, 4, 0);
       ASSERT(hList);

       ImageList_SetBkColor(hList, CLR_NONE);

       int imgIndex = ImageList_AddMasked(hList, hbm, RGB(255,255,255));
       // ASSERT(imgIndex == 0);
       imgIndex = ImageList_AddMasked(hList, hbm, RGB(0,0,0));
       // ASSERT(imgIndex == 1);

       // ASSERT(ImageList_GetImageCount(hList) == 2);

       for(int i = 0; i < ImageList_GetImageCount(hList); ++i)
       {
           ImageList_Draw(hList, i, hdc, x, y, ILD_MASK);
           y += 40;
       }

       ImageList_Destroy(hList);
    }

    SelectObject(dib->getDC(), dib->getHandle());
    DeleteObject(hTempBM);

    /*
     * Copy DIB to screen to ensure it wasn't changed
     */

    BitBlt(hdc, 10, y, w, h, dib->getDC(), 0, 0, SRCCOPY);

    // DeleteObject(hbm);

    delete dib;

    EndPaint(hwnd, &ps);
}

// BOOL MainWindow::onEraseBk(HWND hwnd, HDC hdc)
// {
//     return TRUE;
// }

void MainWindow::onSize(HWND hwnd, UINT state, int cx, int cy)
{
}



TestDIBApp::TestDIBApp() :
    d_mainWind(0)
{
}

TestDIBApp::~TestDIBApp()
{
    endApplication();
}

BOOL TestDIBApp::initApplication()
{
    InitCommonControls();
    return TRUE;
}

BOOL TestDIBApp::initInstance()
{
    // Create a window with a custom button in

    d_mainWind = new MainWindow;
    return true;
}

void TestDIBApp::endApplication()
{
    // Delete window

    delete d_mainWind;
    d_mainWind = 0;
}

BOOL TestDIBApp::doCmdLine(LPCSTR cmd)
{
    return true;
}


Boolean TestDIBApp::processMessage(MSG* msg)
{
    return false;
}




/*
 *----------------------------------------------------------------------
 * $Log$
 *----------------------------------------------------------------------
 */
