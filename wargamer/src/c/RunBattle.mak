# Microsoft Developer Studio Generated NMAKE File, Based on RunBattle.dsp
!IF "$(CFG)" == ""
CFG=RunBattle - Win32 Debug
!MESSAGE No configuration specified. Defaulting to RunBattle - Win32 Debug.
!ENDIF 

!IF "$(CFG)" != "RunBattle - Win32 Release" && "$(CFG)" != "RunBattle - Win32 Debug"
!MESSAGE Invalid configuration "$(CFG)" specified.
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "RunBattle.mak" CFG="RunBattle - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "RunBattle - Win32 Release" (based on "Win32 (x86) Application")
!MESSAGE "RunBattle - Win32 Debug" (based on "Win32 (x86) Application")
!MESSAGE 
!ERROR An invalid configuration is specified.
!ENDIF 

!IF "$(OS)" == "Windows_NT"
NULL=
!ELSE 
NULL=nul
!ENDIF 

CPP=cl.exe
MTL=midl.exe
RSC=rc.exe

!IF  "$(CFG)" == "RunBattle - Win32 Release"

OUTDIR=.\..\..\exe
INTDIR=.\o\Release
# Begin Custom Macros
OutDir=.\..\..\exe
# End Custom Macros

!IF "$(RECURSE)" == "0" 

ALL : "$(OUTDIR)\RunBattle.exe"

!ELSE 

ALL : "ResourceNap1813 - Win32 Release" "ob - Win32 Release" "gwind - Win32 Release" "gamesup - Win32 Release" "system - Win32 Release" "batgame - Win32 Release" "$(OUTDIR)\RunBattle.exe"

!ENDIF 

!IF "$(RECURSE)" == "1" 
CLEAN :"batgame - Win32 ReleaseCLEAN" "system - Win32 ReleaseCLEAN" "gamesup - Win32 ReleaseCLEAN" "gwind - Win32 ReleaseCLEAN" "ob - Win32 ReleaseCLEAN" "ResourceNap1813 - Win32 ReleaseCLEAN" 
!ELSE 
CLEAN :
!ENDIF 
	-@erase "$(INTDIR)\battle.res"
	-@erase "$(INTDIR)\testbat.obj"
	-@erase "$(INTDIR)\vc60.idb"
	-@erase "$(INTDIR)\vc60.pdb"
	-@erase "$(OUTDIR)\RunBattle.exe"
	-@erase "$(OUTDIR)\RunBattle.pdb"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

"$(INTDIR)" :
    if not exist "$(INTDIR)/$(NULL)" mkdir "$(INTDIR)"

CPP_PROJ=/nologo /MD /W3 /Gi /GR /GX /Zi /O2 /I "..\h" /I "..\system" /I "..\gamesup" /I "..\..\res" /I "..\gwind" /I "..\batgame" /I "..\batdata" /I "..\ob" /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_MBCS" /Fp"$(INTDIR)\RunBattle.pch" /YX"stdinc.hpp" /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /c 
MTL_PROJ=/nologo /D "NDEBUG" /mktyplib203 /win32 
RSC_PROJ=/l 0x809 /fo"$(INTDIR)\battle.res" /i "..\.." /d "NDEBUG" 
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\RunBattle.bsc" 
BSC32_SBRS= \
	
LINK32=link.exe
LINK32_FLAGS=comctl32.lib gdi32.lib USer32.lib winmm.lib /nologo /subsystem:windows /incremental:no /pdb:"$(OUTDIR)\RunBattle.pdb" /debug /machine:I386 /out:"$(OUTDIR)\RunBattle.exe" 
LINK32_OBJS= \
	"$(INTDIR)\testbat.obj" \
	"$(INTDIR)\battle.res" \
	"$(OUTDIR)\batgame.lib" \
	"$(OUTDIR)\system.lib" \
	"$(OUTDIR)\gamesup.lib" \
	"$(OUTDIR)\gwind.lib" \
	"$(OUTDIR)\ob.lib" \
	"$(OUTDIR)\Nap1813res.lib"

"$(OUTDIR)\RunBattle.exe" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

!ELSEIF  "$(CFG)" == "RunBattle - Win32 Debug"

OUTDIR=.\..\..\exe
INTDIR=.\o\Debug
# Begin Custom Macros
OutDir=.\..\..\exe
# End Custom Macros

!IF "$(RECURSE)" == "0" 

ALL : "$(OUTDIR)\RunBattleDB.exe"

!ELSE 

ALL : "ResourceNap1813 - Win32 Debug" "ob - Win32 Debug" "gwind - Win32 Debug" "gamesup - Win32 Debug" "system - Win32 Debug" "batgame - Win32 Debug" "$(OUTDIR)\RunBattleDB.exe"

!ENDIF 

!IF "$(RECURSE)" == "1" 
CLEAN :"batgame - Win32 DebugCLEAN" "system - Win32 DebugCLEAN" "gamesup - Win32 DebugCLEAN" "gwind - Win32 DebugCLEAN" "ob - Win32 DebugCLEAN" "ResourceNap1813 - Win32 DebugCLEAN" 
!ELSE 
CLEAN :
!ENDIF 
	-@erase "$(INTDIR)\battle.res"
	-@erase "$(INTDIR)\testbat.obj"
	-@erase "$(INTDIR)\vc60.idb"
	-@erase "$(INTDIR)\vc60.pdb"
	-@erase "$(OUTDIR)\RunBattleDB.exe"
	-@erase "$(OUTDIR)\RunBattleDB.ilk"
	-@erase "$(OUTDIR)\RunBattleDB.pdb"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

"$(INTDIR)" :
    if not exist "$(INTDIR)/$(NULL)" mkdir "$(INTDIR)"

CPP_PROJ=/nologo /MDd /W3 /Gm /Gi /GR /GX /ZI /Od /I "..\h" /I "..\system" /I "..\gamesup" /I "..\..\res" /I "..\gwind" /I "..\batgame" /I "..\batdata" /I "..\ob" /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_MBCS" /Fp"$(INTDIR)\RunBattle.pch" /YX"stdinc.hpp" /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /GZ /c 
MTL_PROJ=/nologo /D "_DEBUG" /mktyplib203 /win32 
RSC_PROJ=/l 0x809 /fo"$(INTDIR)\battle.res" /i "..\.." /d "_DEBUG" 
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\RunBattle.bsc" 
BSC32_SBRS= \
	
LINK32=link.exe
LINK32_FLAGS=comctl32.lib gdi32.lib USer32.lib winmm.lib /nologo /subsystem:windows /incremental:yes /pdb:"$(OUTDIR)\RunBattleDB.pdb" /debug /machine:I386 /out:"$(OUTDIR)\RunBattleDB.exe" /pdbtype:sept 
LINK32_OBJS= \
	"$(INTDIR)\testbat.obj" \
	"$(INTDIR)\battle.res" \
	"$(OUTDIR)\batgameDB.lib" \
	"$(OUTDIR)\systemDB.lib" \
	"$(OUTDIR)\gamesupDB.lib" \
	"$(OUTDIR)\gwindDB.lib" \
	"$(OUTDIR)\obDB.lib" \
	"$(OUTDIR)\Nap1813res.lib"

"$(OUTDIR)\RunBattleDB.exe" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

!ENDIF 

.c{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.c{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<


!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("RunBattle.dep")
!INCLUDE "RunBattle.dep"
!ELSE 
!MESSAGE Warning: cannot find "RunBattle.dep"
!ENDIF 
!ENDIF 


!IF "$(CFG)" == "RunBattle - Win32 Release" || "$(CFG)" == "RunBattle - Win32 Debug"
SOURCE=..\..\res\battle.rc

!IF  "$(CFG)" == "RunBattle - Win32 Release"


"$(INTDIR)\battle.res" : $(SOURCE) "$(INTDIR)"
	$(RSC) /l 0x809 /fo"$(INTDIR)\battle.res" /i "..\.." /i "\src\sf\wargamer\res" /d "NDEBUG" $(SOURCE)


!ELSEIF  "$(CFG)" == "RunBattle - Win32 Debug"


"$(INTDIR)\battle.res" : $(SOURCE) "$(INTDIR)"
	$(RSC) /l 0x809 /fo"$(INTDIR)\battle.res" /i "..\.." /i "\src\sf\wargamer\res" /d "_DEBUG" $(SOURCE)


!ENDIF 

SOURCE=..\..\res\english.rc
SOURCE=..\..\res\german.rc
SOURCE=..\..\res\multiplayer.rc
SOURCE=..\..\res\resource.rc
SOURCE=..\..\res\string.rc
SOURCE=.\testbat.cpp

"$(INTDIR)\testbat.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=..\..\res\version.rc
SOURCE=.\wargamer.cpp

!IF  "$(CFG)" == "RunBattle - Win32 Release"

"batgame - Win32 Release" : 
   cd "\src\sf\wargamer\src\batgame"
   $(MAKE) /$(MAKEFLAGS) /F .\batgame.mak CFG="batgame - Win32 Release" 
   cd "..\c"

"batgame - Win32 ReleaseCLEAN" : 
   cd "\src\sf\wargamer\src\batgame"
   $(MAKE) /$(MAKEFLAGS) /F .\batgame.mak CFG="batgame - Win32 Release" RECURSE=1 CLEAN 
   cd "..\c"

!ELSEIF  "$(CFG)" == "RunBattle - Win32 Debug"

"batgame - Win32 Debug" : 
   cd "\src\sf\wargamer\src\batgame"
   $(MAKE) /$(MAKEFLAGS) /F .\batgame.mak CFG="batgame - Win32 Debug" 
   cd "..\c"

"batgame - Win32 DebugCLEAN" : 
   cd "\src\sf\wargamer\src\batgame"
   $(MAKE) /$(MAKEFLAGS) /F .\batgame.mak CFG="batgame - Win32 Debug" RECURSE=1 CLEAN 
   cd "..\c"

!ENDIF 

!IF  "$(CFG)" == "RunBattle - Win32 Release"

"system - Win32 Release" : 
   cd "\src\sf\wargamer\SRC\SYSTEM"
   $(MAKE) /$(MAKEFLAGS) /F .\system.mak CFG="system - Win32 Release" 
   cd "..\c"

"system - Win32 ReleaseCLEAN" : 
   cd "\src\sf\wargamer\SRC\SYSTEM"
   $(MAKE) /$(MAKEFLAGS) /F .\system.mak CFG="system - Win32 Release" RECURSE=1 CLEAN 
   cd "..\c"

!ELSEIF  "$(CFG)" == "RunBattle - Win32 Debug"

"system - Win32 Debug" : 
   cd "\src\sf\wargamer\SRC\SYSTEM"
   $(MAKE) /$(MAKEFLAGS) /F .\system.mak CFG="system - Win32 Debug" 
   cd "..\c"

"system - Win32 DebugCLEAN" : 
   cd "\src\sf\wargamer\SRC\SYSTEM"
   $(MAKE) /$(MAKEFLAGS) /F .\system.mak CFG="system - Win32 Debug" RECURSE=1 CLEAN 
   cd "..\c"

!ENDIF 

!IF  "$(CFG)" == "RunBattle - Win32 Release"

"gamesup - Win32 Release" : 
   cd "\src\sf\wargamer\SRC\GAMESUP"
   $(MAKE) /$(MAKEFLAGS) /F .\gamesup.mak CFG="gamesup - Win32 Release" 
   cd "..\c"

"gamesup - Win32 ReleaseCLEAN" : 
   cd "\src\sf\wargamer\SRC\GAMESUP"
   $(MAKE) /$(MAKEFLAGS) /F .\gamesup.mak CFG="gamesup - Win32 Release" RECURSE=1 CLEAN 
   cd "..\c"

!ELSEIF  "$(CFG)" == "RunBattle - Win32 Debug"

"gamesup - Win32 Debug" : 
   cd "\src\sf\wargamer\SRC\GAMESUP"
   $(MAKE) /$(MAKEFLAGS) /F .\gamesup.mak CFG="gamesup - Win32 Debug" 
   cd "..\c"

"gamesup - Win32 DebugCLEAN" : 
   cd "\src\sf\wargamer\SRC\GAMESUP"
   $(MAKE) /$(MAKEFLAGS) /F .\gamesup.mak CFG="gamesup - Win32 Debug" RECURSE=1 CLEAN 
   cd "..\c"

!ENDIF 

!IF  "$(CFG)" == "RunBattle - Win32 Release"

"gwind - Win32 Release" : 
   cd "\src\sf\wargamer\src\gwind"
   $(MAKE) /$(MAKEFLAGS) /F .\gwind.mak CFG="gwind - Win32 Release" 
   cd "..\c"

"gwind - Win32 ReleaseCLEAN" : 
   cd "\src\sf\wargamer\src\gwind"
   $(MAKE) /$(MAKEFLAGS) /F .\gwind.mak CFG="gwind - Win32 Release" RECURSE=1 CLEAN 
   cd "..\c"

!ELSEIF  "$(CFG)" == "RunBattle - Win32 Debug"

"gwind - Win32 Debug" : 
   cd "\src\sf\wargamer\src\gwind"
   $(MAKE) /$(MAKEFLAGS) /F .\gwind.mak CFG="gwind - Win32 Debug" 
   cd "..\c"

"gwind - Win32 DebugCLEAN" : 
   cd "\src\sf\wargamer\src\gwind"
   $(MAKE) /$(MAKEFLAGS) /F .\gwind.mak CFG="gwind - Win32 Debug" RECURSE=1 CLEAN 
   cd "..\c"

!ENDIF 

!IF  "$(CFG)" == "RunBattle - Win32 Release"

"ob - Win32 Release" : 
   cd "\src\sf\wargamer\SRC\OB"
   $(MAKE) /$(MAKEFLAGS) /F .\ob.mak CFG="ob - Win32 Release" 
   cd "..\c"

"ob - Win32 ReleaseCLEAN" : 
   cd "\src\sf\wargamer\SRC\OB"
   $(MAKE) /$(MAKEFLAGS) /F .\ob.mak CFG="ob - Win32 Release" RECURSE=1 CLEAN 
   cd "..\c"

!ELSEIF  "$(CFG)" == "RunBattle - Win32 Debug"

"ob - Win32 Debug" : 
   cd "\src\sf\wargamer\SRC\OB"
   $(MAKE) /$(MAKEFLAGS) /F .\ob.mak CFG="ob - Win32 Debug" 
   cd "..\c"

"ob - Win32 DebugCLEAN" : 
   cd "\src\sf\wargamer\SRC\OB"
   $(MAKE) /$(MAKEFLAGS) /F .\ob.mak CFG="ob - Win32 Debug" RECURSE=1 CLEAN 
   cd "..\c"

!ENDIF 

!IF  "$(CFG)" == "RunBattle - Win32 Release"

"ResourceNap1813 - Win32 Release" : 
   cd "\src\sf\wargamer\res"
   $(MAKE) /$(MAKEFLAGS) /F .\ResourceNap1813.mak CFG="ResourceNap1813 - Win32 Release" 
   cd "..\src\c"

"ResourceNap1813 - Win32 ReleaseCLEAN" : 
   cd "\src\sf\wargamer\res"
   $(MAKE) /$(MAKEFLAGS) /F .\ResourceNap1813.mak CFG="ResourceNap1813 - Win32 Release" RECURSE=1 CLEAN 
   cd "..\src\c"

!ELSEIF  "$(CFG)" == "RunBattle - Win32 Debug"

"ResourceNap1813 - Win32 Debug" : 
   cd "\src\sf\wargamer\res"
   $(MAKE) /$(MAKEFLAGS) /F .\ResourceNap1813.mak CFG="ResourceNap1813 - Win32 Debug" 
   cd "..\src\c"

"ResourceNap1813 - Win32 DebugCLEAN" : 
   cd "\src\sf\wargamer\res"
   $(MAKE) /$(MAKEFLAGS) /F .\ResourceNap1813.mak CFG="ResourceNap1813 - Win32 Debug" RECURSE=1 CLEAN 
   cd "..\src\c"

!ENDIF 


!ENDIF 

