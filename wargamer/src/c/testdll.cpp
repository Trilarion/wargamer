/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 * Test the system DLL
 *
 *----------------------------------------------------------------------
 */

#if 1       // try to find problem with undefined constructor
#include "stdinc.hpp"
#include "ANIMATE.HPP"
#include "APP.HPP"
#include "ARRAY.HPP"
#include "ARY_SORT.HPP"
#include "AUTOPTR.HPP"
#include "BARGRAPH.HPP"
#include "BEZIER.HPP"
#include "BMP.HPP"
#include "BRES.HPP"
#include "BUTTON.HPP"
#include "BW_INT.HPP"
#include "CBUTTON.HPP"
#include "CEDIT.HPP"
// #include "clistbox.hpp"
#include "CLOG.HPP"
#include "CMENU.HPP"
#include "COLOURS.HPP"
#include "config.hpp"
#include "CRITICAL.HPP"
#include "crossref.hpp"
#include "CSCROLL.HPP"
#include "StringPtr.hpp"
#include "CTAB.HPP"
#include "CTBAR.HPP"
#include "CUSTBORD.HPP"
#include "cust_dlg.hpp"
#include "C_COMBO.HPP"
#include "date.hpp"
#include "DATETIME.HPP"
#include "DC_HOLD.HPP"
#include "DC_MAN.HPP"
#include "DIALOG.HPP"
#include "DIB.HPP"
#include "DIBREF.HPP"
#include "DIB_BLT.HPP"
#include "DIB_POLY.HPP"
#include "DIB_UTIL.HPP"
#include "DLIST.HPP"
#include "DYNARRAY.HPP"
#include "EXCEPT.HPP"
#include "FILEBASE.HPP"
#include "FILECNK.HPP"
#include "FILEDATA.HPP"
#include "FILEITER.HPP"
#include "FILEPACK.HPP"
#include "FILLWIND.HPP"
#include "FIXPOINT.HPP"
#include "FONTS.HPP"
#include "FRACTAL.HPP"
#include "FRAMEWIN.HPP"
#include "FSALLOC.HPP"
#include "GAMECTRL.HPP"
#include "GDIENGIN.HPP"
#include "GENERIC.HPP"
#include "GENGINE.HPP"
#include "GRTYPES.HPP"
#include "GSECWIND.HPP"
#include "GTOOLBAR.HPP"
#include "IDSTR.HPP"
#include "IMGLIB.HPP"
#include "ITEMWIND.HPP"
#include "LLIST.HPP"
#include "LOGWIN.HPP"
#include "logw_imp.hpp"
#include "makename.hpp"
#include "MCIWIND.HPP"
#include "MEMPTR.HPP"
#include "MINMAX.HPP"
#include "MISC.HPP"
#include "MMTIMER.HPP"
#include "MSGBOX.HPP"
#include "MSGENUM.HPP"
#include "MYASSERT.HPP"
#include "PALETTE.HPP"
#include "PALWIN.HPP"
#include "PALWIND.HPP"
#include "PATHDIAL.HPP"
#include "PERMBUF.HPP"
#include "PICLIB.HPP"
#include "POINT.HPP"
#include "POOLARRY.HPP"
#include "POPICON.HPP"
#include "PTRLIST.HPP"
#include "PT_UTIL.HPP"
#include "RANDOM.HPP"
#include "RANGE.HPP"
#include "REFPTR.HPP"
#include "REFPTRIM.HPP"
#include "REGISTRY.HPP"
#include "RESSTR.HPP"
#include "SCRNBASE.HPP"
#include "BitSet.hpp"
#include "SIMPSTK.HPP"
#include "SIMPSTR.HPP"
#include "SLLIST.HPP"
#include "SOUNDS.HPP"
#include "SoundSys.hpp"
#include "SPACING.HPP"
#include "SPLASH.HPP"
#include "SPRLIB.HPP"
#include "SWGSPR.HPP"
#include "SYNC.HPP"
#include "SYSTICK.HPP"
#include "TABLES.HPP"
#include "TABWIN.HPP"
#include "THICKLIN.HPP"
#include "THREAD.HPP"
#include "TIMER.HPP"
#include "TODOLOG.HPP"
#include "TOOLTIP.HPP"
#include "TRACKWIN.HPP"
#include "TRANSWIN.HPP"
#include "TRIG.HPP"
#include "TYPEDEF.HPP"
#include "W95EXCPT.HPP"
#include "wavefile.hpp"
#include "WINCTRL.HPP"
#include "WIND.HPP"
#include "WINDSAVE.HPP"
#include "WINERROR.HPP"
#include "WINFILE.HPP"
#include "WMISC.HPP"
#endif

#include "except.hpp"
#include "myassert.hpp"
#include <iostream.h>

// #include "scenario.hpp"

class MyClass {
    public:
        MyClass() : d_a(0) { }
        virtual ~MyClass() { }

        int a() const { return d_a; }
        void a(int n) { d_a = n; }
    private:
        int d_a;
};


class BigClass : public MyClass, public RWLock
{
    public:    
        BigClass() : MyClass(), RWLock() { }
        virtual ~BigClass() { }

        int b() const { return d_b; }
        void b(int n) { d_b = n; }
    private:
        int d_b;
        CString d_strings[10];
        static SimpleString s_strings[10];
};

SimpleString BigClass::s_strings[10];


// static BigClass s_bigClass;

void test(int a, int b)
{
    // ASSERT(a == b);

    // throw GeneralError("Testing GeneralError in a DLL");
}

extern const char* testBuild();    // in testdll1.cpp

void main()
{
    testBuild();

    BigClass t;
    t.startWrite();

    t.a(10);
    t.b(15);

    t.endWrite();

    int i;
    int j;

    BigClass* bPtr = new BigClass;

    // s_bigClass.startRead();
    i = t.a();
    j = t.b();
    // s_bigClass.endRead();

    // CString* cstringArray = new CString[10];
    // cstringArray[2] = copyString("Hello");
    // delete[] cstringArray;

    delete bPtr;

    ConfigFile* config = new ConfigFile("nap1813.swg");
    delete config;

    try
    {
        test(i, j);
    }
    catch(GeneralError e)
    {
        cout << e.get() << endl;
    }
    catch(...)
    {
        cout << "Caught unknown interrupt" << endl;
    }
}

/*
 *----------------------------------------------------------------------
 * $Log$
 *----------------------------------------------------------------------
 */
