# Microsoft Developer Studio Generated NMAKE File, Based on runCampaign.dsp
!IF "$(CFG)" == ""
CFG=runCampaign - Win32 Debug
!MESSAGE No configuration specified. Defaulting to runCampaign - Win32 Debug.
!ENDIF 

!IF "$(CFG)" != "runCampaign - Win32 Release" && "$(CFG)" != "runCampaign - Win32 Debug"
!MESSAGE Invalid configuration "$(CFG)" specified.
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "runCampaign.mak" CFG="runCampaign - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "runCampaign - Win32 Release" (based on "Win32 (x86) Application")
!MESSAGE "runCampaign - Win32 Debug" (based on "Win32 (x86) Application")
!MESSAGE 
!ERROR An invalid configuration is specified.
!ENDIF 

!IF "$(OS)" == "Windows_NT"
NULL=
!ELSE 
NULL=nul
!ENDIF 

CPP=cl.exe
MTL=midl.exe
RSC=rc.exe

!IF  "$(CFG)" == "runCampaign - Win32 Release"

OUTDIR=.\..\..\exe
INTDIR=.\o\Release
# Begin Custom Macros
OutDir=.\..\..\exe
# End Custom Macros

!IF "$(RECURSE)" == "0" 

ALL : "$(OUTDIR)\runCampaign.exe"

!ELSE 

ALL : "gwind - Win32 Release" "frontend - Win32 Release" "gamesup - Win32 Release" "ob - Win32 Release" "system - Win32 Release" "campgameNoBattle - Win32 Release" "campdata - Win32 Release" "ResourceNap1813 - Win32 Release" "$(OUTDIR)\runCampaign.exe"

!ENDIF 

!IF "$(RECURSE)" == "1" 
CLEAN :"ResourceNap1813 - Win32 ReleaseCLEAN" "campdata - Win32 ReleaseCLEAN" "campgameNoBattle - Win32 ReleaseCLEAN" "system - Win32 ReleaseCLEAN" "ob - Win32 ReleaseCLEAN" "gamesup - Win32 ReleaseCLEAN" "frontend - Win32 ReleaseCLEAN" "gwind - Win32 ReleaseCLEAN" 
!ELSE 
CLEAN :
!ENDIF 
	-@erase "$(INTDIR)\campaign.res"
	-@erase "$(INTDIR)\testcamp.obj"
	-@erase "$(INTDIR)\vc60.idb"
	-@erase "$(INTDIR)\vc60.pdb"
	-@erase "$(OUTDIR)\runCampaign.exe"
	-@erase "$(OUTDIR)\runCampaign.pdb"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

"$(INTDIR)" :
    if not exist "$(INTDIR)/$(NULL)" mkdir "$(INTDIR)"

CPP_PROJ=/nologo /MD /W3 /Gi /GR /GX /Zi /O2 /I "..\h" /I "..\system" /I "..\gamesup" /I "..\..\res" /I "..\gwind" /I "..\campgame" /I "..\campdata" /I "..\camp_ai" /I "..\campwind" /I "..\ob" /I "..\campLogic" /I "..\mapwind" /I "..\frontend" /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_MBCS" /Fp"$(INTDIR)\runCampaign.pch" /YX"stdinc.hpp" /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /c 
MTL_PROJ=/nologo /D "NDEBUG" /mktyplib203 /win32 
RSC_PROJ=/l 0x809 /fo"$(INTDIR)\campaign.res" /i "..\.." /d "NDEBUG" 
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\runCampaign.bsc" 
BSC32_SBRS= \
	
LINK32=link.exe
LINK32_FLAGS=comctl32.lib gdi32.lib USer32.lib winmm.lib /nologo /subsystem:windows /incremental:no /pdb:"$(OUTDIR)\runCampaign.pdb" /debug /machine:I386 /out:"$(OUTDIR)\runCampaign.exe" 
LINK32_OBJS= \
	"$(INTDIR)\testcamp.obj" \
	"$(INTDIR)\campaign.res" \
	"$(OUTDIR)\Nap1813res.lib" \
	"$(OUTDIR)\campdata.lib" \
	"$(OUTDIR)\campgameNoBattle.lib" \
	"$(OUTDIR)\system.lib" \
	"$(OUTDIR)\ob.lib" \
	"$(OUTDIR)\gamesup.lib" \
	"$(OUTDIR)\frontend.lib" \
	"$(OUTDIR)\gwind.lib"

"$(OUTDIR)\runCampaign.exe" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

!ELSEIF  "$(CFG)" == "runCampaign - Win32 Debug"

OUTDIR=.\..\..\exe
INTDIR=.\o\Debug
# Begin Custom Macros
OutDir=.\..\..\exe
# End Custom Macros

!IF "$(RECURSE)" == "0" 

ALL : "$(OUTDIR)\runCampaignDB.exe"

!ELSE 

ALL : "gwind - Win32 Debug" "frontend - Win32 Debug" "gamesup - Win32 Debug" "ob - Win32 Debug" "system - Win32 Debug" "campgameNoBattle - Win32 Debug" "campdata - Win32 Debug" "ResourceNap1813 - Win32 Debug" "$(OUTDIR)\runCampaignDB.exe"

!ENDIF 

!IF "$(RECURSE)" == "1" 
CLEAN :"ResourceNap1813 - Win32 DebugCLEAN" "campdata - Win32 DebugCLEAN" "campgameNoBattle - Win32 DebugCLEAN" "system - Win32 DebugCLEAN" "ob - Win32 DebugCLEAN" "gamesup - Win32 DebugCLEAN" "frontend - Win32 DebugCLEAN" "gwind - Win32 DebugCLEAN" 
!ELSE 
CLEAN :
!ENDIF 
	-@erase "$(INTDIR)\campaign.res"
	-@erase "$(INTDIR)\testcamp.obj"
	-@erase "$(INTDIR)\vc60.idb"
	-@erase "$(INTDIR)\vc60.pdb"
	-@erase "$(OUTDIR)\runCampaignDB.exe"
	-@erase "$(OUTDIR)\runCampaignDB.ilk"
	-@erase "$(OUTDIR)\runCampaignDB.pdb"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

"$(INTDIR)" :
    if not exist "$(INTDIR)/$(NULL)" mkdir "$(INTDIR)"

CPP_PROJ=/nologo /MDd /W3 /Gm /Gi /GR /GX /ZI /Od /I "..\h" /I "..\system" /I "..\gamesup" /I "..\..\res" /I "..\gwind" /I "..\campgame" /I "..\campdata" /I "..\camp_ai" /I "..\campwind" /I "..\ob" /I "..\campLogic" /I "..\mapwind" /I "..\frontend" /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_MBCS" /Fp"$(INTDIR)\runCampaign.pch" /YX"stdinc.hpp" /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /GZ /c 
MTL_PROJ=/nologo /D "_DEBUG" /mktyplib203 /win32 
RSC_PROJ=/l 0x809 /fo"$(INTDIR)\campaign.res" /i "..\.." /d "_DEBUG" 
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\runCampaign.bsc" 
BSC32_SBRS= \
	
LINK32=link.exe
LINK32_FLAGS=comctl32.lib gdi32.lib USer32.lib winmm.lib /nologo /subsystem:windows /incremental:yes /pdb:"$(OUTDIR)\runCampaignDB.pdb" /debug /machine:I386 /out:"$(OUTDIR)\runCampaignDB.exe" /pdbtype:sept 
LINK32_OBJS= \
	"$(INTDIR)\testcamp.obj" \
	"$(INTDIR)\campaign.res" \
	"$(OUTDIR)\Nap1813res.lib" \
	"$(OUTDIR)\campdataDB.lib" \
	"$(OUTDIR)\campgameNoBattleDB.lib" \
	"$(OUTDIR)\systemDB.lib" \
	"$(OUTDIR)\obDB.lib" \
	"$(OUTDIR)\gamesupDB.lib" \
	"$(OUTDIR)\frontendDB.lib" \
	"$(OUTDIR)\gwindDB.lib"

"$(OUTDIR)\runCampaignDB.exe" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

!ENDIF 

.c{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.c{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<


!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("runCampaign.dep")
!INCLUDE "runCampaign.dep"
!ELSE 
!MESSAGE Warning: cannot find "runCampaign.dep"
!ENDIF 
!ENDIF 


!IF "$(CFG)" == "runCampaign - Win32 Release" || "$(CFG)" == "runCampaign - Win32 Debug"
SOURCE=..\..\res\campaign.rc

!IF  "$(CFG)" == "runCampaign - Win32 Release"


"$(INTDIR)\campaign.res" : $(SOURCE) "$(INTDIR)"
	$(RSC) /l 0x809 /fo"$(INTDIR)\campaign.res" /i "..\.." /i "\src\sf\wargamer\res" /d "NDEBUG" $(SOURCE)


!ELSEIF  "$(CFG)" == "runCampaign - Win32 Debug"


"$(INTDIR)\campaign.res" : $(SOURCE) "$(INTDIR)"
	$(RSC) /l 0x809 /fo"$(INTDIR)\campaign.res" /i "..\.." /i "\src\sf\wargamer\res" /d "_DEBUG" $(SOURCE)


!ENDIF 

SOURCE=..\..\res\english.rc
SOURCE=..\..\res\german.rc
SOURCE=..\..\res\multiplayer.rc
SOURCE=..\..\res\resource.rc
SOURCE=..\..\res\string.rc
SOURCE=.\testcamp.cpp

"$(INTDIR)\testcamp.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=..\..\res\version.rc
SOURCE=.\wargamer.cpp

!IF  "$(CFG)" == "runCampaign - Win32 Release"

"ResourceNap1813 - Win32 Release" : 
   cd "\src\sf\wargamer\res"
   $(MAKE) /$(MAKEFLAGS) /F .\ResourceNap1813.mak CFG="ResourceNap1813 - Win32 Release" 
   cd "..\src\c"

"ResourceNap1813 - Win32 ReleaseCLEAN" : 
   cd "\src\sf\wargamer\res"
   $(MAKE) /$(MAKEFLAGS) /F .\ResourceNap1813.mak CFG="ResourceNap1813 - Win32 Release" RECURSE=1 CLEAN 
   cd "..\src\c"

!ELSEIF  "$(CFG)" == "runCampaign - Win32 Debug"

"ResourceNap1813 - Win32 Debug" : 
   cd "\src\sf\wargamer\res"
   $(MAKE) /$(MAKEFLAGS) /F .\ResourceNap1813.mak CFG="ResourceNap1813 - Win32 Debug" 
   cd "..\src\c"

"ResourceNap1813 - Win32 DebugCLEAN" : 
   cd "\src\sf\wargamer\res"
   $(MAKE) /$(MAKEFLAGS) /F .\ResourceNap1813.mak CFG="ResourceNap1813 - Win32 Debug" RECURSE=1 CLEAN 
   cd "..\src\c"

!ENDIF 

!IF  "$(CFG)" == "runCampaign - Win32 Release"

"campdata - Win32 Release" : 
   cd "\src\sf\wargamer\src\campdata"
   $(MAKE) /$(MAKEFLAGS) /F .\campdata.mak CFG="campdata - Win32 Release" 
   cd "..\c"

"campdata - Win32 ReleaseCLEAN" : 
   cd "\src\sf\wargamer\src\campdata"
   $(MAKE) /$(MAKEFLAGS) /F .\campdata.mak CFG="campdata - Win32 Release" RECURSE=1 CLEAN 
   cd "..\c"

!ELSEIF  "$(CFG)" == "runCampaign - Win32 Debug"

"campdata - Win32 Debug" : 
   cd "\src\sf\wargamer\src\campdata"
   $(MAKE) /$(MAKEFLAGS) /F .\campdata.mak CFG="campdata - Win32 Debug" 
   cd "..\c"

"campdata - Win32 DebugCLEAN" : 
   cd "\src\sf\wargamer\src\campdata"
   $(MAKE) /$(MAKEFLAGS) /F .\campdata.mak CFG="campdata - Win32 Debug" RECURSE=1 CLEAN 
   cd "..\c"

!ENDIF 

!IF  "$(CFG)" == "runCampaign - Win32 Release"

!ELSEIF  "$(CFG)" == "runCampaign - Win32 Debug"

!ENDIF 

!IF  "$(CFG)" == "runCampaign - Win32 Release"

"campgameNoBattle - Win32 Release" : 
   cd "\src\sf\wargamer\src\campgame"
   $(MAKE) /$(MAKEFLAGS) /F .\campgameNoBattle.mak CFG="campgameNoBattle - Win32 Release" 
   cd "..\c"

"campgameNoBattle - Win32 ReleaseCLEAN" : 
   cd "\src\sf\wargamer\src\campgame"
   $(MAKE) /$(MAKEFLAGS) /F .\campgameNoBattle.mak CFG="campgameNoBattle - Win32 Release" RECURSE=1 CLEAN 
   cd "..\c"

!ELSEIF  "$(CFG)" == "runCampaign - Win32 Debug"

"campgameNoBattle - Win32 Debug" : 
   cd "\src\sf\wargamer\src\campgame"
   $(MAKE) /$(MAKEFLAGS) /F .\campgameNoBattle.mak CFG="campgameNoBattle - Win32 Debug" 
   cd "..\c"

"campgameNoBattle - Win32 DebugCLEAN" : 
   cd "\src\sf\wargamer\src\campgame"
   $(MAKE) /$(MAKEFLAGS) /F .\campgameNoBattle.mak CFG="campgameNoBattle - Win32 Debug" RECURSE=1 CLEAN 
   cd "..\c"

!ENDIF 

!IF  "$(CFG)" == "runCampaign - Win32 Release"

"system - Win32 Release" : 
   cd "\src\sf\wargamer\SRC\SYSTEM"
   $(MAKE) /$(MAKEFLAGS) /F .\system.mak CFG="system - Win32 Release" 
   cd "..\c"

"system - Win32 ReleaseCLEAN" : 
   cd "\src\sf\wargamer\SRC\SYSTEM"
   $(MAKE) /$(MAKEFLAGS) /F .\system.mak CFG="system - Win32 Release" RECURSE=1 CLEAN 
   cd "..\c"

!ELSEIF  "$(CFG)" == "runCampaign - Win32 Debug"

"system - Win32 Debug" : 
   cd "\src\sf\wargamer\SRC\SYSTEM"
   $(MAKE) /$(MAKEFLAGS) /F .\system.mak CFG="system - Win32 Debug" 
   cd "..\c"

"system - Win32 DebugCLEAN" : 
   cd "\src\sf\wargamer\SRC\SYSTEM"
   $(MAKE) /$(MAKEFLAGS) /F .\system.mak CFG="system - Win32 Debug" RECURSE=1 CLEAN 
   cd "..\c"

!ENDIF 

!IF  "$(CFG)" == "runCampaign - Win32 Release"

"ob - Win32 Release" : 
   cd "\src\sf\wargamer\SRC\OB"
   $(MAKE) /$(MAKEFLAGS) /F .\ob.mak CFG="ob - Win32 Release" 
   cd "..\c"

"ob - Win32 ReleaseCLEAN" : 
   cd "\src\sf\wargamer\SRC\OB"
   $(MAKE) /$(MAKEFLAGS) /F .\ob.mak CFG="ob - Win32 Release" RECURSE=1 CLEAN 
   cd "..\c"

!ELSEIF  "$(CFG)" == "runCampaign - Win32 Debug"

"ob - Win32 Debug" : 
   cd "\src\sf\wargamer\SRC\OB"
   $(MAKE) /$(MAKEFLAGS) /F .\ob.mak CFG="ob - Win32 Debug" 
   cd "..\c"

"ob - Win32 DebugCLEAN" : 
   cd "\src\sf\wargamer\SRC\OB"
   $(MAKE) /$(MAKEFLAGS) /F .\ob.mak CFG="ob - Win32 Debug" RECURSE=1 CLEAN 
   cd "..\c"

!ENDIF 

!IF  "$(CFG)" == "runCampaign - Win32 Release"

"gamesup - Win32 Release" : 
   cd "\src\sf\wargamer\SRC\GAMESUP"
   $(MAKE) /$(MAKEFLAGS) /F .\gamesup.mak CFG="gamesup - Win32 Release" 
   cd "..\c"

"gamesup - Win32 ReleaseCLEAN" : 
   cd "\src\sf\wargamer\SRC\GAMESUP"
   $(MAKE) /$(MAKEFLAGS) /F .\gamesup.mak CFG="gamesup - Win32 Release" RECURSE=1 CLEAN 
   cd "..\c"

!ELSEIF  "$(CFG)" == "runCampaign - Win32 Debug"

"gamesup - Win32 Debug" : 
   cd "\src\sf\wargamer\SRC\GAMESUP"
   $(MAKE) /$(MAKEFLAGS) /F .\gamesup.mak CFG="gamesup - Win32 Debug" 
   cd "..\c"

"gamesup - Win32 DebugCLEAN" : 
   cd "\src\sf\wargamer\SRC\GAMESUP"
   $(MAKE) /$(MAKEFLAGS) /F .\gamesup.mak CFG="gamesup - Win32 Debug" RECURSE=1 CLEAN 
   cd "..\c"

!ENDIF 

!IF  "$(CFG)" == "runCampaign - Win32 Release"

"frontend - Win32 Release" : 
   cd "\src\sf\wargamer\src\frontend"
   $(MAKE) /$(MAKEFLAGS) /F .\frontend.mak CFG="frontend - Win32 Release" 
   cd "..\c"

"frontend - Win32 ReleaseCLEAN" : 
   cd "\src\sf\wargamer\src\frontend"
   $(MAKE) /$(MAKEFLAGS) /F .\frontend.mak CFG="frontend - Win32 Release" RECURSE=1 CLEAN 
   cd "..\c"

!ELSEIF  "$(CFG)" == "runCampaign - Win32 Debug"

"frontend - Win32 Debug" : 
   cd "\src\sf\wargamer\src\frontend"
   $(MAKE) /$(MAKEFLAGS) /F .\frontend.mak CFG="frontend - Win32 Debug" 
   cd "..\c"

"frontend - Win32 DebugCLEAN" : 
   cd "\src\sf\wargamer\src\frontend"
   $(MAKE) /$(MAKEFLAGS) /F .\frontend.mak CFG="frontend - Win32 Debug" RECURSE=1 CLEAN 
   cd "..\c"

!ENDIF 

!IF  "$(CFG)" == "runCampaign - Win32 Release"

"gwind - Win32 Release" : 
   cd "\src\sf\wargamer\src\gwind"
   $(MAKE) /$(MAKEFLAGS) /F .\gwind.mak CFG="gwind - Win32 Release" 
   cd "..\c"

"gwind - Win32 ReleaseCLEAN" : 
   cd "\src\sf\wargamer\src\gwind"
   $(MAKE) /$(MAKEFLAGS) /F .\gwind.mak CFG="gwind - Win32 Release" RECURSE=1 CLEAN 
   cd "..\c"

!ELSEIF  "$(CFG)" == "runCampaign - Win32 Debug"

"gwind - Win32 Debug" : 
   cd "\src\sf\wargamer\src\gwind"
   $(MAKE) /$(MAKEFLAGS) /F .\gwind.mak CFG="gwind - Win32 Debug" 
   cd "..\c"

"gwind - Win32 DebugCLEAN" : 
   cd "\src\sf\wargamer\src\gwind"
   $(MAKE) /$(MAKEFLAGS) /F .\gwind.mak CFG="gwind - Win32 Debug" RECURSE=1 CLEAN 
   cd "..\c"

!ENDIF 


!ENDIF 

