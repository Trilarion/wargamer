/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1995, Steven Morle-Green, All Rights Reserved
 *----------------------------------------------------------------------
 *
 *      Wargamer Application implementation
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "wargame.hpp"
#include "app.hpp"
#include "soundsys.hpp"
#include "gamectrl.hpp"
#include "savegame.hpp"
#include "registry.hpp"
#include "resstr.hpp"
#if !defined(TEST_BATTLE)
#include "campaign.hpp"
#endif
#include "wg_msg.hpp"
#include "simpstr.hpp"
#include "savegame.hpp"
#include "filename.hpp"
#include "wg_main.hpp"    // Main Window
#include "winfile.hpp"
#include "filecnk.hpp"
#if !defined(TEST_CAMPAIGN) && !defined(CAMPAIGN_EDITOR)
#include "batgame.hpp"
#include "batowner.hpp"
#endif
#if defined(CAMPAIGN_EDITOR) || defined(TEST_BATTLE)
#include "registry.hpp"
#endif
#include "scenario.hpp"
#if !defined(CAMPAIGN_EDITOR) && !defined(TEST_CAMPAIGN)
#include "tresult.hpp"
#endif
#ifdef DEBUG
#include "logwin.hpp"
#include "msgenum.hpp"
#endif

#include "options.hpp"
#include "control.hpp"

#if 0
#if !defined(TEST_BATTLE)
#include "ds_unit.hpp"
#include "ds_town.hpp"
#include "ds_repoSP.hpp"
#include "Ds_reorg.hpp"

#include "despatch.hpp"
#endif

#if !defined(CAMPAIGN_EDITOR) && !defined(TEST_CAMPAIGN)
#include "b_send.hpp"
#endif
#endif

#if !defined(CAMPAIGN_EDITOR)
#include "DirectPlay.hpp"
#include "ChatDialog.hpp"
#include "MultiPlayerMsg.hpp"
#include "MultiplayerConnection.hpp"
#include "wg_rand.hpp"
#endif

#if !defined(TEST_BATTLE) && !defined(CAMPAIGN_EDITOR)
#include "clogic.hpp"
#include "campimp.hpp"
#endif


/*
 * The following are needed for registering classes
 */

// #if defined(TEST_BATTLE)
// #include "palwind.hpp"
// #endif
// #if !defined(TEST_BATTLE)
// #include "mainwind.hpp"
// #include "mapwind.hpp"
// #include "obwin.hpp"
// #endif
#include "cbutton.hpp"
// #include "generic.hpp"
// #if !defined(TEST_BATTLE)
// #include "splash.hpp"
// #endif
// #ifdef DEBUG
// #include "palwin.hpp"
// #endif
#if !defined(TEST_BATTLE) && !defined(CAMPAIGN_EDITOR)
#include "front.hpp"
#endif
#include "memptr.hpp"
#include "misc.hpp"
#include "except.hpp"
#include "myassert.hpp"

#ifdef DEBUG
#include "msgbox.hpp"
#endif

// #define DEBUG_MESSAGES

/*
 * Global Application Data
 */

/*
 * Forward References
 */


#ifdef DEBUG_MESSAGES
#include "clog.hpp"
#include "msgenum.hpp"
LogFile mLog("messages.log");
#endif

#ifdef DEBUG
#include "clog.hpp"
LogFile startupLog("startup.log");
#endif

/*
 * Application class to make it work with system
 */

class WargameApp :
        public APP,
        public GameControlBase,
#if !defined(TEST_CAMPAIGN) && !defined(CAMPAIGN_EDITOR)
        public virtual BattleOwner,
#endif
#if !defined(CAMPAIGN_EDITOR)
      public CDirectPlayUser,
      public CChatDialogUser,
#endif
        public GameBasePtr

{
        // CampaignGame* d_campaignGame;                        // Current Campaign Game
        // BattleGame* d_battleGame;                            // Current Battle Game
        // GameBase* d_game;
        MainWindow* d_mainWind;

        // FrontEndWindow* d_frontEndWindow;  // Starting window

        const char* startFileName;      // Name of File to load

        // Autosave variables
        // they should really be in options

         SysTick::Value d_autoSaveTimer;  // Autosave counter
         SysTick::Value d_autoSaveRate;   // Autosave rate, 0=disabled
         StringPtr d_autoSaveName;   // Name, 0=use date/time

         static const char s_regAutoSave[];
         static const char s_regAutoSaveName[];

public:
        WargameApp();
        ~WargameApp();

        /*
         * Implementation of APP virtual functions
         */

        BOOL initApplication();
        BOOL initInstance();
        void endApplication();
        BOOL doCmdLine(LPCSTR cmd);

        Boolean processMessage(MSG* msg);
                // Process Message.
                //   Return True if application dealt with it
                //   False to continue as usual

        /*
         * Implementation of GameControl virtual functions
         */

        void pauseGame(Boolean paused);
                // Do whatever is neccessary to pause or unpause the game
                // For example in a multi-threaded game, it may need to send
                // a signal to each thread and wait until it has responded

        void updateTime(SysTick::Value ticks);
                // Do whatever is needed when some time has passed

        // virtuals from BatOwner
        void startCampaignTime() {}
        void stopCampaignTime() {}

 private:
        void doLoadGame(const char* fileName, SaveGame::FileFormat mode);

        void stopGame();
                // Stop any game in progress

        void restartGame(const char* startName = 0);
                // Stop current game and start with front end again.

#if !defined(CAMPAIGN_EDITOR)
        // void finishBattle();
#endif

        /*
         * Implement GameOwner
         */

        virtual HWND hwnd() const
        {
            return d_mainWind->getHWND();
        }

        virtual void requestSaveGame(bool prompt)
        {
            GameBasePtr::requestSaveGame(prompt);
        }

#if !defined(CAMPAIGN_EDITOR) && !defined(TEST_CAMPAIGN)
        virtual bool tacticalOver(const TacticalResults& results)
        {
            if(results.willContinue() == BattleResults::Continue)
            {
#ifdef DEBUG
                messageBox("Battle", "Battle continues overnight", MB_ICONINFORMATION | MB_OK);
#endif
                return true;
            }
            else
            {
                WargameMessage::postAbandon();
                // restartGame();
                return false;
            }
        }
#endif


   void doAutoSave();

#if !defined(CAMPAIGN_EDITOR)
   CChatDialog * d_chatDialog;
#endif

   // filename of game to start in multiplayer mode
   char d_multiplayerGameName[128];
   SaveGame::FileFormat d_multiplayerGameFormat;


#if !defined(CAMPAIGN_EDITOR)
   /*
   * DirectPlay interface functions
   */

   // sent when a new message has arrived in the DirectPlay message queue from the remote player
   // the message is removed from the DirectPlay message queue, and passed to the app via this function
   virtual void onNewMessage(LPVOID data, DWORD data_length);

   // the remote opponent has joined a locally hosted game
   virtual void onOpponentJoin(void);
   // the opponent has left the game
   virtual void onOpponentQuit(void);
   // the connection has somehow terminated
   virtual void onConnectionLost(void);
   // chat message received
   virtual void onChatMessage(LPSTR text);

   // some internal error has occured
   virtual void onError(char * context, char * desc);

   /*
   * ChatDialog interface functions
   */

   virtual void requestDisconnect(void);

   virtual void sendChatMessage(char * text);


   /*
   * GameOwner
   */
   virtual void multiplayerConnectionComplete(bool isMaster);


   void sendTimeSync(SysTick::Value ticks, unsigned int checksum);
   void acknowledgeTimeSync(MP_MSG_SYNC * sync_msg);
   void sendBattleTimeSync(SysTick::Value ticks, unsigned int checksum);
   void sendSlaveReady();
#else
   virtual void multiplayerConnectionComplete(bool isMaster) { }
   void sendTimeSync(SysTick::Value ticks, unsigned int checksum) { }
   void sendSlaveReady(void) { }
#endif   // CAMPAIGN_EDITOR

#if 0 // Moved into GAME
#if !defined(TEST_BATTLE) && !defined(CAMPAIGN_EDITOR)
   void processCampaignOrder(MP_MSG_CAMPAIGNORDER * msg);
#endif
#if !defined(TEST_CAMPAIGN) && !defined(CAMPAIGN_EDITOR)
   void processBattleOrder(MP_MSG_BATTLEORDER * msg);
#endif
#endif

};

const char WargameApp::s_regAutoSave[] = "AutoSaveRate";
const char WargameApp::s_regAutoSaveName[] = "AutoSaveName";

/*========================================================================
 * Main Windows Function
 */

int PASCAL WinMain (HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int nCmdShow)
{
        int result = 0;
        try
        {
                WargameApp application;
                result = application.execute(hInstance, hPrevInstance, lpszCmdLine, nCmdShow);
        }
#if defined(DEBUG)
        catch(const AssertError& e)
        {
                // gApp.pauseGame(TRUE);
                MessageBox(0, "Quit Program", "Exception", MB_ICONSTOP | MB_TASKMODAL | MB_SETFOREGROUND);
        }
#endif
        catch(const GeneralError& e)
        {
                // gApp.pauseGame(TRUE);
                MessageBox(0, e.get(), "General Error Exception", MB_ICONSTOP | MB_TASKMODAL | MB_SETFOREGROUND);
        }
        catch(...)
        {
                // gApp.pauseGame(TRUE);
                MessageBox(0, "Unknown", "Exception", MB_ICONSTOP | MB_TASKMODAL | MB_SETFOREGROUND);
        }

        return result;
}

/*===========================================================
 * Wargame Application Functions
 */

WargameApp::WargameApp() :
        GameBasePtr(0),
#if !defined(CAMPAIGN_EDITOR)
        d_chatDialog(0),
#endif
        d_mainWind(0),
        d_autoSaveTimer(0),
        d_autoSaveRate(0),
        d_autoSaveName(),
        startFileName(0)
{

   int saveRate;
   if(getRegistry(s_regAutoSave, &saveRate, sizeof(saveRate), ""))
   {
      // convert to ticks

      if (saveRate > 0)
      {
         d_autoSaveRate = saveRate * SysTick::TicksPerSecond;

         char buffer[500];
         strcpy(buffer, SaveGame::getDefFolder(SaveGame::SaveGame));
         char* nameStr = buffer + strlen(buffer);
         if(nameStr[-1] != '\\')
            *nameStr++ = '\\';

         if(getRegistryString(s_regAutoSaveName, nameStr, ""))
         {
            d_autoSaveName = buffer;
         }

         d_autoSaveTimer = d_autoSaveRate;
      }
   }

   startFileName = 0;


}

WargameApp::~WargameApp()
{
#if !defined(CAMPAIGN_EDITOR)
   if(d_chatDialog) {
      delete d_chatDialog;
      d_chatDialog = NULL;
   }
   if(g_directPlay) {
      delete g_directPlay;
      g_directPlay = NULL;
   }
#endif

   endApplication();
}

BOOL WargameApp::initApplication()
{
        return TRUE;
}


BOOL WargameApp::initInstance()
{

   /*
    * Bodge it so that threadLocale is set to German
    */

#ifdef TEST_GERMAN
//   DWORD clid = GetUserDefaultLCID();
   LANGID langID = MAKELANGID(LANG_GERMAN,SUBLANG_GERMAN);
   DWORD clid = MAKELCID(langID, SORT_DEFAULT);
   SetThreadLocale(clid);
#elif defined(TEST_FRENCH)
//   DWORD clid = GetUserDefaultLCID();
   LANGID langID = MAKELANGID(LANG_FRENCH,SUBLANG_FRENCH);
   DWORD clid = MAKELCID(langID, SORT_DEFAULT);
   SetThreadLocale(clid);
#endif



   /*
   * Make sure the we are in a decent resolution to run the app
   */

   {
      HDC hdc = CreateDC("DISPLAY", NULL, NULL, NULL);
      int width = GetDeviceCaps(hdc, HORZRES);
      int height = GetDeviceCaps(hdc, VERTRES);
      int bpp = GetDeviceCaps(hdc, BITSPIXEL);
      DeleteDC(hdc);

      if(width < 800 || height < 600 || bpp < 8) {
         MessageBox(
            NULL,
            InGameText::get(IDS_BadDisplaySetting),
            InGameText::get(IDS_ChangeDisplaySettings),
//            "Your current Display Settings are insufficent to run Napoleon 1813.\nPlease switch to at least 800 by 600 screen resolution\nand 256 colour (or higher), before running the game.",
//            "Please Change Display Settings",
            MB_OK | MB_ICONSTOP | MB_APPLMODAL
         );
         PostQuitMessage(0);
         return false;
      }
   }

   /*
   * Make sure that either Wargamer.exe is local
   * or set current directory to the InstallPath directory
   */
#if 0
   const char * wg_filename = "Wargamer.exe";
   char current_dir[MAX_PATH];
   GetCurrentDirectory(MAX_PATH, current_dir);

#ifdef DEBUG
   startupLog.printf("Startup Directory : %s\n", current_dir);
#endif

   char find_filename[MAX_PATH];
   sprintf(find_filename, "%s\\%s", current_dir, wg_filename);

   WIN32_FIND_DATA w32fd;
   HANDLE find_retval = FindFirstFile(find_filename, &w32fd);

#ifdef DEBUG
   if(find_retval != INVALID_HANDLE_VALUE) startupLog.printf("App found Wargamer.exe in this directory\n");
   else startupLog.printf("App couldn't find Wargamer.exe in this directory\n");
#endif

   // file not found locally
   if(find_retval == INVALID_HANDLE_VALUE) {

      char wg_dir[MAX_PATH];
      getRegistryString("InstallPath", wg_dir, "");

      SetCurrentDirectory(wg_dir);
      GetCurrentDirectory(MAX_PATH, current_dir);

#ifdef DEBUG
      startupLog.printf("Directory has been set to : %s\n", wg_dir);
      startupLog.printf("Get Current Dir says : %s\n", current_dir);
#endif

//    sprintf(find_filename, "%s\\%s", wg_dir, wg_filename);
//    find_retval = FindFirstFile(find_filename, &w32fd);
//    sprintf(find_filename, "%s\\%s", wg_dir, wg_filename);
      find_retval = FindFirstFile(wg_filename, &w32fd);

#ifdef DEBUG
   if(find_retval != INVALID_HANDLE_VALUE) startupLog.printf("App found Wargamer.exe in this directory\n");
   else startupLog.printf("App couldn't find Wargamer.exe in this directory\n");
#endif

      if(find_retval == INVALID_HANDLE_VALUE) FORCEASSERT("ERROR - Can't find where Wargamer.exe is located\n");
   }
#endif



   InitCommonControls();
   CustomButton::initCustomButton(APP::instance());

   // Make User Interface more responsive
   SetThreadPriority(GetCurrentThread(), THREAD_PRIORITY_ABOVE_NORMAL);

   d_mainWind = new MainWindow;



#if defined(CAMPAIGN_EDITOR) || defined(TEST_BATTLE)
   {
      CString buf = Scenario::defaultFileName();
      Scenario::create(buf);
   }
#endif


#if defined(CAMPAIGN_EDITOR)

        restartGame(startFileName);

#else

   /*
   * Init DSound
   */

        GlobalSoundSystemObj.Init(d_mainWind->getHWND());

   /*
   * register the class with g_directPlay to receive
   * multiplayer messages as they arrive
   */

   g_directPlay = new CDirectPlay;
   g_directPlay->registerUser(this);

   d_chatDialog = new CChatDialog(d_mainWind->getHWND());
   d_chatDialog->registerUser(this);
   d_chatDialog->setTextNames("[LOCAL] : ", "[REMOTE] : ");


        // Examine startFileName for an extension
        //
        // do front end if:
        //              startFileName==0 OR No extension
        // do start scenario if extension = wgc
        // do load game if extension = wgs

        const char* s = startFileName;
        if(s)
                s = strrchr(startFileName, '.');
        if(s)
                ++s;
        if(s && (stricmp(s, CAMPAIGN_EXT) == 0))
                doLoadGame(startFileName, SaveGame::Campaign);
        else if(s && (stricmp(s, BATTLE_EXT) == 0))
                doLoadGame(startFileName, SaveGame::Battle);
        else if(s && (stricmp(s, SAVE_EXT) == 0))
                doLoadGame(startFileName, SaveGame::SaveGame);
        else
                restartGame(startFileName);
#endif

        return TRUE;
}


/*
 * Parse command line for name of game to use
 *
 * Return TRUE if OK
 * Return FALSE to abort the program
 */

BOOL WargameApp::doCmdLine(LPCSTR cmd)
{
        debugLog("Command Line = %s\n", cmd);

        if(cmd && cmd[0])
        {
                BOOL inQuotes = FALSE;
                MemPtr<char> buf(MAX_PATH);

                const char* s = cmd;
                char* s1 = buf.get();

                while(*s)
                {
                        char c = *s++;

                        if(c == '\"')
                        {
                                inQuotes = !inQuotes;
                                if(!inQuotes)
                                        break;
                        }
                        else if(!inQuotes && isspace(c))
                        {
                                break;
                        }
                        else
                                *s1++ = c;
                }
                *s1 = 0;

                startFileName = copyString(buf.get());
        }

        return TRUE;
}

void WargameApp::endApplication()
{
    /*
     * Must set game to 0 so that it is destructed before the
     * main window is deleted.
     *
     * Otherwise deleting mainWind causes all child windows to be
     * automatically destroyed by windows leaving dangling pointers
     * to deleted memory and lots of system crashes.
     */

    setGame(0);

    if(d_mainWind)
    {
        delete d_mainWind;
        d_mainWind = 0;
    }

//    if(scenario())
    Scenario::kill();

    ASSERT(!scenario);
}


/*
 * Process Messages to ourself
 * if hwnd is NULL, then it is a private message to ourself.
 */

Boolean WargameApp::processMessage(MSG* msg)
{
   if(msg->hwnd == NULL)
   {
#ifdef DEBUG
      // if(logWindow)
      {
         g_logWindow.printf("processMessage(%s)",
            getWMdescription(msg->hwnd, msg->message, msg->wParam, msg->lParam));
      }
#endif

      if(msg->message == WargameMessage::WGM_Load)
      {
         WargameMessage::LoadGame* message = reinterpret_cast<WargameMessage::LoadGame*>(msg->lParam);

         CString fileName = copyString(message->fileName());

         if(!fileName)
         {
            SimpleString fName;

            if(SaveGame::askLoadName(fName, message->fileFormat()))
            {
               fileName = copyString(fName.toStr());
            }
         }

         if(fileName != 0)
         {
            doLoadGame(fileName, static_cast<SaveGame::FileFormat>(message->fileFormat()));
         }
      }
      else if(msg->message == WargameMessage::WGM_Abandon)
      {
         restartGame();
      }
#if 0
      else if(msg->message == WargameMessage::WGM_Message)
      {
         WargameMessage::MessageBase* message = reinterpret_cast<WargameMessage::MessageBase*>(msg->lParam);

         message->run();
         message->clear();               // This may delete it!

         return True;
      }
#endif
   }

   return False;
}

/*
 * Do whatever is neccessary to pause or unpause the game
 * For example in a multi-threaded game, it may need to send
 * a signal to each thread and wait until it has responded
 */

void WargameApp::pauseGame(Boolean paused)
{
    GameBasePtr::pauseGame(paused);
}

/*
 * Update the Time when some ticks have occured
 */

void WargameApp::updateTime(SysTick::Value ticks)
{
   /*
    * Update autosave timer
    */

   if(d_autoSaveRate)
   {
      if (d_autoSaveTimer < ticks)
      {
         doAutoSave();
         d_autoSaveTimer += d_autoSaveRate;
      }
      else
         d_autoSaveTimer -= ticks;
   }

   GameBasePtr::addTime(ticks);
}

void WargameApp::stopGame()
{
        GameControl::pause(True);
        setGame(0);
}

void WargameApp::restartGame(const char* startName)
{
        setGame(0);

#if defined(CAMPAIGN_EDITOR)
        if(startFileName != 0)
        {
                doLoadGame(startFileName, SaveGame::Campaign);
        }
        else
        {
                SimpleString fileName;
                if(SaveGame::askLoadName(fileName, SaveGame::Campaign))
                        doLoadGame(fileName.toStr(), SaveGame::Campaign);
        }
#elif defined(TEST_BATTLE)
        doLoadGame(0, SaveGame::Battle);
#else
        setGame(new FrontEnd(this));
#endif
}

void WargameApp::doLoadGame(const char* fileName, SaveGame::FileFormat mode)
{
#if !defined(CAMPAIGN_EDITOR)
      /*
      If this is a multiplayer game & we are Master
      then post SetGame & StartGame messages to Slave
      */
      if(CMultiPlayerConnection::connectionType() == ConnectionMaster) {

         // setgame
         MP_MSG_SETGAME setgame_msg;
         setgame_msg.msg_type = MP_MSG_SetGame;

         // copy the filename - without the path
         char * fname = const_cast<char *>(fileName);
         char * strptr = &fname[strlen(fname)-1];
         while(*strptr != '\\') strptr--;
         strptr++;
         strcpy(setgame_msg.gamefilename, "SavedGames\\");
         strcat(setgame_msg.gamefilename, strptr);

         setgame_msg.checksum = 0;
         setgame_msg.format = (int) mode;

         if(g_directPlay) {
            g_directPlay->sendMessage(
               (LPVOID) &setgame_msg,
               sizeof(MP_MSG_SETGAME)
            );
         }

         // startgame
         MP_MSG_STARTGAME startgame_msg;
         startgame_msg.msg_type = MP_MSG_StartGame;

         if(g_directPlay) {
            g_directPlay->sendMessage(
               (LPVOID) &startgame_msg,
               sizeof(MP_MSG_STARTGAME)
            );
         }
      }
#endif   // !defined(CAMPAIGN_EDITOR)


      /*
      If this is a multiplayer game & we are Slave
      then append filename onto local SavedGames path
      */


        setGame(0);

        SaveGame::FileFormat actualMode = mode;

        if(fileName)
        {
                WinFileBinaryChunkReader fr(fileName);
                FileTypeChunk fType(SaveGame::Campaign);
                fType.readData(fr);
                actualMode = fType.mode();
                ASSERT((actualMode == SaveGame::Campaign) || (actualMode == SaveGame::Battle));

            Scenario::create(Scenario::defaultFileName());
        }
        else
        {
            Scenario::create(Scenario::defaultFileName());
        }


#if !defined(TEST_BATTLE)
        if(actualMode == SaveGame::Campaign)
        {
            setGame(new CampaignGame(this, fileName, mode));
        }
        else
#endif
#if !defined(TEST_CAMPAIGN) && !defined(CAMPAIGN_EDITOR)
        if(actualMode == SaveGame::Battle)
        {
                setGame(new BattleGame(this, fileName, mode));
        }
        else
#endif
        {
                FORCEASSERT("Unknown Mode");
                throw GeneralError("Unknown Mode");
        }

        // It would be nice to show some kind of loading window
        // while the campaign initialises itself...
}




void WargameApp::doAutoSave()
{
   /*
    * Autosave feature
    */

   const char* fileName = d_autoSaveName;

   char dateFilename[MAX_PATH];

   if (fileName == 0)
   {
      char date[256];
      GetDateFormat(
         NULL,
         0,
         NULL,
         "dd MMM",
         date,
         255
      );

      char time[256];
      GetTimeFormat(
         NULL,
         TIME_FORCE24HOURFORMAT,
         NULL,
         "HH'-'mm'-'ss",
         time,
         255
      );

      sprintf(dateFilename, "%s\\AutoSave %s - %s",
            SaveGame::getDefFolder(SaveGame::SaveGame),
         time, date);
      fileName = dateFilename;
   }

   saveGame(fileName);

}


#if !defined(CAMPAIGN_EDITOR)

/*

  Once the front-end has established the connection & been notified that the remote machine
  has joined through it's CDirectPlayUser interface, g_directPlay must rediriect its messages
  up to WargameApp, by switching it's CDirectPlayUser interface to this class

*/

void
WargameApp::multiplayerConnectionComplete(bool isMaster) {

   /*
   Are we the Master or Slave machine
   */
   if(isMaster) CMultiPlayerConnection::connectionType(ConnectionMaster);
   else CMultiPlayerConnection::connectionType(ConnectionSlave);

   g_directPlay->registerUser(this);
   d_chatDialog->show();

   MP_MSG_VALIDATEVERSIONS v_msg;
   v_msg.msg_type = MP_MSG_ValidateVersions;
   v_msg.version = 12345;
   v_msg.checksum = 67890;

   g_directPlay->sendMessage(
      (LPVOID) &v_msg,
      sizeof(MP_MSG_VALIDATEVERSIONS)
   );

   onOpponentJoin();

}


/*

  MultiPlayer messages from the remote machine will arrive here.  Any DirectPlay system messages
  (such as CHAT messages) will have been caught by the CDirectPlay class & processed there - they will
  subsequently arrive at the onChatMessage() function below.

  'LPVOID data' points to the DPlay receive buffer.  Data should be copied out of here into a new buffer
  before processing.  As soon as this function returns, the 'LPVOID data' pointer will filled with the
  next pending message.

  Note that this function blocks DPlay's recieve thread until it returns.  Whilst there is no risk of losing
  messages whilst blocked (DirectPlay has its own internal msg thread & buffer) - spending time here
  does mean that messages will be accumulating in the input queue & could potentially cause a stall.

*/

void
WargameApp::onNewMessage(LPVOID data, DWORD data_length) {

   MP_MSG_GENERIC * generic_msg = (MP_MSG_GENERIC *) data;

   switch(generic_msg->msg_type) {

      case MP_MSG_SlaveReady : {
            /*
            Only the Master machine recieves this message
            */
            ASSERT(CMultiPlayerConnection::connectionType() == ConnectionMaster);

            //MP_MSG_SLAVEREADY * msg = (MP_MSG_SLAVEREADY *) generic_msg;
            slaveReady();

            break;
      }

      case MP_MSG_Sync : {
         /*
         Only the Slave machine recieves this message
         */
         ASSERT(CMultiPlayerConnection::connectionType() == ConnectionSlave);

         MP_MSG_SYNC * msg = (MP_MSG_SYNC *) generic_msg;

#if !defined(TEST_BATTLE)
#ifdef MP_CRC_CHECK
         // g_masterCRC = msg->checksum;
#endif
#endif

         // reply to say we got this time sync
         acknowledgeTimeSync(msg);

         // do something to let game code know we can advance time to the specified number of ticks
         // 'cause "the Master tells us so" !!

         break;
      }

      case MP_MSG_SyncAcknowledgement : {
         /*
         Only the Master machine recieves this message
         */
         ASSERT(CMultiPlayerConnection::connectionType() == ConnectionMaster);

         MP_MSG_SYNCACKNOWLEDGEMENT * msg = (MP_MSG_SYNCACKNOWLEDGEMENT *) generic_msg;

         // do something to let game code know we can carry on & advance time by the next installment -
         // as the Slave is keeping up Ok...
         timeSyncAcknowledged(msg->tick);

         break;
      }
#if !defined(TEST_BATTLE) && !defined(CAMPAIGN_EDITOR)
      case MP_MSG_CampaignOrder : {
         MP_MSG_CAMPAIGNORDER * msg = (MP_MSG_CAMPAIGNORDER *) generic_msg;
         processCampaignOrder(msg);
         break;
      }
#endif
#if !defined(TEST_CAMPAIGN) && !defined(CAMPAIGN_EDITOR)
      case MP_MSG_BattleOrder : {
         MP_MSG_BATTLEORDER * msg = (MP_MSG_BATTLEORDER *) generic_msg;
         processBattleOrder(msg);
         break;
      }
#endif
      case MP_MSG_RequestDownload : {
         MP_MSG_REQUESTDOWNLOAD * msg = (MP_MSG_REQUESTDOWNLOAD *) generic_msg;
         break;
      }
      case MP_MSG_Download : {
         MP_MSG_DOWNLOAD * msg = (MP_MSG_DOWNLOAD *) generic_msg;
         break;
      }
      case MP_MSG_SetGame : {
         MP_MSG_SETGAME * msg = (MP_MSG_SETGAME *) generic_msg;
         strcpy(d_multiplayerGameName, msg->gamefilename);
         d_multiplayerGameFormat = (SaveGame::FileFormat) msg->format;
         break;
      }
      case MP_MSG_SetGameResponse : {
         MP_MSG_SETGAMERESPONSE * msg = (MP_MSG_SETGAMERESPONSE *) generic_msg;
         break;
      }
      case MP_MSG_SetSide : {
         MP_MSG_SETSIDE * msg = (MP_MSG_SETSIDE *) generic_msg;
         GamePlayerControl::setControl(msg->opponent_side, GamePlayerControl::Remote);
         if(msg->opponent_side == 0)
            GamePlayerControl::setControl(1, GamePlayerControl::Player);
         else GamePlayerControl::setControl(0, GamePlayerControl::Player);
         break;
      }
      case MP_MSG_SetOptions : {
         MP_MSG_SETOPTIONS * msg = (MP_MSG_SETOPTIONS *) generic_msg;
         Options::setBitfield(msg->options);
         CampaignOptions::setBitfield(msg->campaign_options);
         break;
      }
      case MP_MSG_StartGame : {
         MP_MSG_STARTGAME * msg = (MP_MSG_STARTGAME *) generic_msg;
         if(d_multiplayerGameFormat == SaveGame::SaveGame) {
            // Load Game
            WargameMessage::postLoadGame(d_multiplayerGameName);
            //doLoadGame(d_multiplayerGameName, d_multiplayerGameFormat);
         }
         else {
            // Start Game
            doNewGame(d_multiplayerGameName, d_multiplayerGameFormat);
         }
         break;
      }
      case MP_MSG_SetSeed : {
         MP_MSG_SETSEED * msg = (MP_MSG_SETSEED *) generic_msg;
         /*
         Set random number seed for the logic
         */
         CRandom::seed(msg->seed);
         break;
      }
      case MP_MSG_ValidateVersions : {
         MP_MSG_VALIDATEVERSIONS * msg = (MP_MSG_VALIDATEVERSIONS *) generic_msg;
         break;
      }
      case MP_MSG_SaveGame : {
         MP_MSG_SAVEGAME * msg = (MP_MSG_SAVEGAME *) generic_msg;
         GameBasePtr::saveGame(msg->savefilename);
         break;
      }
      default : {
         FORCEASSERT("ERROR - Unknown MultiPlayer message received\n");
         break;
      }
   }
}

/*
   The following functions comw from CDirectPlayUser and CChatDialogUser,
   and are used for connection management & chat messages (which may occur from
   the moment the connection is established) and bypass the normal message queue
*/

// the remote opponent has joined a locally hosted game
void
WargameApp::onOpponentJoin(void) {

   /*
   This is designed for the Master to set up certain variables on the Slave machine
   such as the random number seed for the logic
   */
   if(CMultiPlayerConnection::connectionType() == ConnectionMaster) {
      /*
      Pick random number seed
      */

   //#ifdef DEBUG
   // unsigned int seed = 0x12345678;
   //#else
      unsigned int seed = (unsigned int) timeGetTime();
   //#endif


      /*
      Send seed to remote
      */
      MP_MSG_SETSEED seed_msg;
      seed_msg.msg_type = MP_MSG_SetSeed;
      seed_msg.seed = seed;

      if(g_directPlay) {

         g_directPlay->sendMessage(
            (LPVOID) &seed_msg,
            sizeof(MP_MSG_SETSEED)
         );
      }

      /*
      Set the local seed
      */
      CRandom::seed(seed);
   }

}


// the opponent has left the game
void
WargameApp::onOpponentQuit(void) {

}

// the connection has somehow terminated
void
WargameApp::onConnectionLost(void) {

}

// chat message received
void
WargameApp::onChatMessage(LPSTR text) {

   d_chatDialog->addMessage(text, false);

}

// some internal error has occured
void
WargameApp::onError(char * context, char * desc) {

}

void
WargameApp::requestDisconnect(void) {

}

void
WargameApp::sendChatMessage(char * text) {

}


/*

  This function is used by the Master to sync the Slave machine to it
  The 'tick' paramater is the absoulte value of the time-frame which the
  Master machine is instructing the Slave to advance to (ie. advance TO this time)

  Once the SYNC message is sent, the Master machine CANNOT advance beyond this time
  until a SYNCACKNOWLEDGED message is recieved from the Slave, thus indicating that
  the Slave is running its logic to the specified time

*/



void
WargameApp::sendTimeSync(SysTick::Value ticks, unsigned int checksum) {

   MP_MSG_SYNC sync_msg;
   sync_msg.msg_type = MP_MSG_Sync;
   sync_msg.tick = ticks;

#ifdef MP_CRC_CHECK
   sync_msg.checksum = checksum;
#endif

   if(g_directPlay) {

      g_directPlay->sendMessage(
         (LPVOID) &sync_msg,
         sizeof(MP_MSG_SYNC)
      );
   }

}




/*

  NOTE : ONLY the Slave machine uses this function to acknowledge to the Master that is received the Sync message

*/

void
WargameApp::acknowledgeTimeSync(MP_MSG_SYNC * sync_msg) {

   while(CMultiPlayerConnection::dialogPause()) { }

   /*
   Tell logic we are Ok to advance to the specified time
   */
   syncSlave(sync_msg->tick);

   /*
   Send acknowledgement message back to Master
   */
   MP_MSG_SYNCACKNOWLEDGEMENT acknowledgement_msg;
   acknowledgement_msg.msg_type = MP_MSG_SyncAcknowledgement;
   acknowledgement_msg.tick = sync_msg->tick;

   if(g_directPlay) {

      g_directPlay->sendMessage(
         (LPVOID) &acknowledgement_msg,
         sizeof(MP_MSG_SYNCACKNOWLEDGEMENT)
      );
   }

}



/*

  from BattleOwner

*/

void
WargameApp::sendBattleTimeSync(SysTick::Value ticks, unsigned int checksum) {

   sendTimeSync(ticks, checksum);

}




void
WargameApp::sendSlaveReady(void) {

   MP_MSG_SLAVEREADY slaveready_msg;
   slaveready_msg.msg_type = MP_MSG_SlaveReady;

   if(g_directPlay) {

      g_directPlay->sendMessage(
         (LPVOID) &slaveready_msg,
         sizeof(MP_MSG_SLAVEREADY)
      );
   }

}

#endif   // !CAMPAIGN_EDITOR

#if 0    // MOved to game
#if !defined(TEST_BATTLE) && !defined(CAMPAIGN_EDITOR)

/*

  These functions should maybe be within the Game
  The 'msg' should be passed down to Campaign of Battle respectively

*/

void
WargameApp::processCampaignOrder(MP_MSG_CAMPAIGNORDER * msg) {

   switch(msg->order_type) {

      case CMP_MSG_UNIT : {

         DS_UnitOrder * order = new DS_UnitOrder;
         order->unpack(msg, campaignData);

         Despatcher::slaveSend(order, msg->activation_time);
         break;
      }

      case CMP_MSG_TOWN : {

         DS_TownOrder * order = new DS_TownOrder;
         order->unpack(msg, campaignData);

         Despatcher::slaveSend(order, msg->activation_time);
         break;
      }

      case CMP_MSG_REORGANIZE : {

         DS_Reorg::DS_Reorganize * order = new DS_Reorg::DS_Reorganize;
         order->unpack(msg, campaignData);

         Despatcher::slaveSend(order, msg->activation_time);
         break;
      }

      case CMP_MSG_REPOSP : {

         RepoDespatch * order = new RepoDespatch;
         order->unpack(msg, campaignData);

         Despatcher::slaveSend(order, msg->activation_time);
         break;
      }

      default : {
         FORCEASSERT("ERROR - invalid campaign order type from remote machine\n");
         break;
      }
   }



}

#endif   // !defined(TEST_BATTLE)
#endif

#if 0    // MOved to game
#if !defined(TEST_CAMPAIGN) && !defined(CAMPAIGN_EDITOR)

void
WargameApp::processBattleOrder(MP_MSG_BATTLEORDER * msg) {

   DS_BattleUnitOrder * order = new DS_BattleUnitOrder;

   order->unpack(msg, batData);

   slaveSendBattleOrder(order, msg->activation_time);
}

#endif
#endif















/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.3  2001/07/02 15:20:38  greenius
 * Battle, Campaign, Combined game and Campaign Editor now compile and run... mostly
 *
 * Revision 1.2  2001/06/18 08:25:44  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
