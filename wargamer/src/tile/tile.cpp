/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1995, Steven Morle-Green, All Rights Reserved
 *----------------------------------------------------------------------
 *
 *	Utility to convert bitmap into tiles
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.2  1995/11/14 11:25:54  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.1  1995/10/25 09:54:28  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#define WIN32_LEAN_AND_MEAN		// Only include basic Windows stuff
#include <windows.h>

#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>
#include <ctype.h>
#include "mytypes.h"
#include "myassert.hpp"
#include "misc.hpp"

/*
 * Enumerations
 */

enum Verbosity {
	VB_SILENT,
	VB_NORMAL,
	VB_VERBOSE,
	VB_DEBUG1,
	VB_DEBUG2,
	VB_DEBUGALL,
	VB_MAX
};

/*
 * Global Variables
 */

static int tileWidth = 8;				// Size of tiles
static int tileHeight = 8;
static int maxTiles = 256;				// Maximum number of tiles to create
static int verbosity = VB_NORMAL;

static LPRGBQUAD palette = 0;
static ULONG palDiff[256][256];
static UBYTE palRemap[256];			// Table for remapping duplicated colours

/*
 * Helper Functions
 */

void error(const char* fmt, ...)
{
	char* buffer = new char[1000];

	va_list vaList;
	va_start(vaList, fmt);
	vsprintf(buffer, fmt, vaList);
	va_end(vaList);

	printf("\nError:\n%s\n\n", buffer);

	delete[] buffer;
}


void print(Verbosity level, const char* fmt, ...)
{
	if(level <= verbosity)
	{
		va_list vaList;
		va_start(vaList, fmt);
		vprintf(fmt, vaList);
		va_end(vaList);
	}
}


void usage()
{
	puts("\nUsage: tile [options] srcName");
	puts("Options:");
	puts("\t-w4 : Use4x4 pixel tiles");
	puts("\t-m4096 : Create maximum of 4096 tiles");
	puts("");
}

/*
 * Convert ascii string to integer
 */

Boolean aToInt(const char* s, int& n)
{
	/*
	 * Check for s being digits
	 */

	const char* s1 = s;
	while(*s1)
	{
		if(!isdigit(*s1))
			return False;
		s1++;
	}

	n = atoi(s);

	return True;
}

/*
 * My Assertion function
 */

void _myAssert(const char* expr, const char* file, int line)
{
	error("Assertion failed in\nFile: %s Line: %d\n`%s'", file, line, expr);
	exit(-1);
}

static Boolean GeneralError::doingError = False;
static Boolean GeneralError::firstError = False;

GeneralError::GeneralError(const char* fmt, ...)
{
	char buffer[500];

	va_list vaList;
	va_start(vaList, fmt);
	_vbprintf(buffer, 500, fmt, vaList);
	va_end(vaList);

	setMessage(buffer);
}

void GeneralError::setMessage(const char* s)
{
	firstError = doingError;
	doingError = True;
	describe = strdup(s);
}



/*
 * Class to assist File reading
 */

class FileReader {
	FILE* h;
	BOOL gotError;
	LPCSTR fName;

public:
	FileReader(LPCSTR name)
	{
		gotError = FALSE;

		fName = name;

		h = fopen(name, "rb");

		if(h == 0)
		{
			gotError = TRUE;
			error("Can't open %s", fName);
		}
	}

	~FileReader()
	{
		if(h != 0)
			fclose(h);
	}

	BOOL getError()
	{
		return gotError;
	}

	BOOL read(LPVOID ad, DWORD length)
	{
		if(fread(ad, length, 1, h) != 1)
		{
			gotError = TRUE;
			error("Error reading from %s", fName);
			return FALSE;
		}
		return TRUE;
	}
};


class FileWriter {
	FILE* h;
	BOOL gotError;
	LPCSTR fName;

public:
	FileWriter(LPCSTR name)
	{
		gotError = FALSE;

		fName = name;

		h = fopen(name, "wb");

		if(h == 0)
		{
			gotError = TRUE;
			error("Can't create %s", fName);
		}
	}

	~FileWriter()
	{
		if(h != 0)
			fclose(h);
	}

	BOOL getError()
	{
		return gotError;
	}

	BOOL write(LPVOID ad, DWORD length)
	{
		if(fwrite(ad, length, 1, h) != 1)
		{
			gotError = TRUE;
			error("Error writing to %s", fName);
			return FALSE;
		}
		return TRUE;
	}
};

/*
 * Find close colour in global palette
 */

UBYTE findCloseColour(UBYTE red, UBYTE green, UBYTE blue)
{
	LPRGBQUAD lc = palette;

	UBYTE bestCol = 0;
	int bestDist;

	for(int i = 0; i < 256; i++, lc++)
	{
		int dRed = lc->rgbRed - red;
		int dGreen = lc->rgbGreen - green;
		int dBlue = lc->rgbBlue - blue;

		if( (dRed == 0) && (dGreen == 0) && (dBlue == 0))
		{
			bestCol = (UBYTE) i;
			break;
		}

		int dist = dRed*dRed + dGreen*dGreen + dBlue*dBlue;

		if( (i == 0) || (dist < bestDist))
		{
			bestCol = (UBYTE) i;
			bestDist = dist;
		}
	}

#if 0
	printf("\nClosest colour to %d,%d,%d = %d [%d,%d,%d]\n",
		(int) red,
		(int) green,
		(int) blue,
		(int) bestCol,
		(int) palette[bestCol].rgbRed,
		(int) palette[bestCol].rgbGreen,
		(int) palette[bestCol].rgbBlue);
#endif

	return bestCol;
}

/*
 * Round a value up to next multiple of gran
 */

int roundSize(int val, int gran)
{
	val += gran - 1;
	val -= val % gran;
	return val;
}

/*
 * List of best Tile matches
 */

struct BestEntry {
	UWORD tile1;
	UWORD tile2;
	ULONG diff;
};

class BestList {
	BestEntry* items;
	int size;
	int next;
public:
	BestList(int s);
	~BestList();

	void add(UWORD t1, UWORD t2, ULONG diff);
	
	ULONG getMax() const;
	ULONG getMaxConsider() const;

	BOOL isEmpty() const { return (next == 0); }
	BOOL get(BestEntry& data);
	void adjust(UWORD t1, UWORD t2);

	void show();

private:
	void shuffle(int i);
};

/*
 * Initialise BestList
 */

BestList::BestList(int s)
{
	size = s;
	next = 0;
	items = new BestEntry[s];
}

BestList::~BestList()
{
	delete[] items;
}

void BestList::shuffle(int i)
{
	ASSERT(i < next);

	next--;
	for(; i < next; i++)
		items[i] = items[i+1];
}

void BestList::add(UWORD t1, UWORD t2, ULONG diff)
{
	/*
	 * Don't bother if already >= the biggest and the list is full
	 */

	if( (next == size) && (diff >= items[next-1].diff) )
		return;

	if(t1 > t2)
		swap(t1, t2);

	print(VB_DEBUGALL, "Adding %d,%d (%ld)\n",
		(int) t1, (int) t2, diff);


	/*
	 * Scan through list to find place to insert
	 */

#if 0
	BestEntry* item = items;
	for(int i = 0; i < next; i++, item++)
	{
		if(diff < item->diff)
			break;
	}
#else		// Binary search

	int i = 0;			// the value we are looking for

	if( (next == 0) || (diff <= items[0].diff) )
		i = 0;
	else if(diff >= items[next-1].diff)
		i = next;
	else
	{
		int l = 0;
		int r = next - 1;
		while(r >= l)
		{
			i = (l + r) / 2;

			if(diff == items[i].diff)
				break;

			if(diff < items[i].diff)
			{
				r = i - 1;

				if(diff >= items[r].diff)
					break;
			}
			else	
			{
				l = i + 1;

				if(diff <= items[l].diff)
				{
					i = l;
					break;
				}
			}
		}
	}
	
	BestEntry* item = &items[i];

#endif

	if(i < size)
	{
		print(VB_DEBUGALL, "Inserting %d,%d (%ld) into slot %d\n",
			(int) t1,
			(int) t2,
			diff,
			i);

		if(next < size)
			next++;

		for(int j = next - 2; j >= i; j--)
			items[j+1] = items[j];

		item->tile1 = t1;
		item->tile2 = t2;
		item->diff = diff;
	}

	/*
	 * Check for being sorted and print if not
	 */

	if(verbosity >= VB_DEBUGALL)
	{
		int low = 0;
		for(i = 0; i < next; i++)
		{
			if(items[i].diff < low)
			{
				error("BestList is not sorted (item %d)", i);
				show();
				exit(-1);
			}
			low = items[i].diff;
		}
	}
}

ULONG BestList::getMax() const
{
	if(next)
		return items[next-1].diff;
	else
		return 0;
}

ULONG BestList::getMaxConsider() const
{
	if(next == size)
		return items[next-1].diff;
	else
		return 0xffffffff;
}

BOOL BestList::get(BestEntry& data)
{
	if(next)
	{
		data = items[0];
		shuffle(0);
		return TRUE;
	}
	else
		return FALSE;
}

void BestList::adjust(UWORD t1, UWORD t2)
{
	if(t1 > t2)
		swap(t1, t2);

	BestEntry* item = items;

	for(int i = 0; i < next;)
	{
		if((item->tile1 == t1) && (item->tile2 == t2))
			shuffle(i);
		else
		{
			if(item->tile1 == t2)
				item->tile1 = t1;
			else if(item->tile1 > t2)
				item->tile1--;

			if(item->tile2 == t2)
				item->tile2 = t1;
			else if(item->tile2 > t2)
				item->tile2--;

			if(item->tile1 == item->tile2)
				shuffle(i);
			else
			{
				i++;
				item++;
			}
		}
	}
}

void BestList::show()
{
	if(verbosity >= VB_DEBUG2)
	{
		BestEntry* item = items;
		for(int i = 0; i < next; i++, item++)
		{
			print(VB_DEBUG2, "%3d: %3d,%3d, %5ld\n",
				i,
				(int) item->tile1,
				(int) item->tile2,
				item->diff);
		}
	}
}

/*
 * Tile Manager
 */

class TileSet {
	UBYTE** bits;
	int nextTile;
	int maxTiles;

	UWORD* map;
	UWORD* nextMap;

	int merged;

	// ULONG lastBest;
	// int lastMerge;

	BestList bestList;

public:
	TileSet(int maxTiles, int mapWidth, int mapHeight);
	~TileSet();

	void addTile(UBYTE* buffer);

	int getCount() const { return nextTile; }
	int getMerge() const { return merged; }

	UWORD* getMap() const { return map; }
	UBYTE* getTile(UWORD i) const { return bits[i]; }

private:
	void merge();
	void merge(int close1, int close2, ULONG bestDist);
	ULONG compareTile(UBYTE* tile1, UBYTE* tile2, ULONG currentBest);
	void fillBestList();
};

TileSet::TileSet(int mTile, int mapWidth, int mapHeight) :
	bestList(mTile)
{
	maxTiles = mTile;
	nextTile = 0;
	bits = new UBYTE*[maxTiles + 1];

	for(int i = 0; i < maxTiles; i++)
		bits[i] = 0;

	map = new UWORD[mapWidth * mapHeight];
	nextMap = map;

	merged = 0;
	// lastBest = 0;
	// lastMerge = maxTiles - 1;
}

TileSet::~TileSet()
{
	for(int i = 0; i < nextTile; i++)
	{
		delete[] bits[i];
		bits[i] = 0;
	}
	delete[] bits;
	delete[] map;
}

/*
 * Add a tile to the tile list
 *
 * Check existing tiles for identical tile
 * If found return its id
 * otherwide add a new one
 */

void TileSet::addTile(UBYTE* buffer)
{
	int tileSize = tileWidth * tileHeight;

	int i = 0;
	while(i < nextTile)
	{
		if(memcmp(buffer, bits[i], tileSize) == 0)
		{
			*nextMap++ = (UWORD) i;
			return;
		}
		i++;
	}

	/*
	 * It wasn't found, so make a new one
	 */

	UBYTE* newTile = new UBYTE[tileSize];
	if(!newTile)
	{
		error("Not enough memory to allocate tile %d", nextTile);
		return;
	}

	memcpy(newTile, buffer, tileSize);

	*nextMap++ = (UWORD) nextTile;
	bits[nextTile++] = newTile;

	if(nextTile > maxTiles)		// > because we have space for an extra one
	{
		/*
		 * Code to be written that merges two tiles
		 */

		merge();
		return;
	}

	return;
}

/*
 * Compare 2 tiles, returning colour difference
 *
 * This one works by taking the biggest colour difference of any
 * pixels.
 */

inline ULONG TileSet::compareTile(UBYTE* tile1, UBYTE* tile2, ULONG currentBest)
{
	ULONG best = 0;

#if 1
	int count = tileWidth * tileHeight;
	while(count--)
	{
		ULONG val = palDiff[*tile1++][*tile2++];

		if(val > currentBest)		// Shortcut exit
			return val;

		if(val > best)
			best = val;
	}
#else

	/*
	 * Average up 2 by 2 blocks instead
	 */


	for(int y = 0; y < (tileHeight - 1); y++)
	{
		for(int x = 0; x < (tileWidth - 1); x++)
		{
			UBYTE* t1 = tile1 + x + y * tileWidth;
			UBYTE* t2 = tile2 + x + y * tileWidth;

#if 0
			int r1 = palette[t1[0]].rgbRed + palette[t1[1]].rgbRed + 
						 palette[t1[tileWidth]].rgbRed + palette[t1[tileWidth+1]].rgbRed;
			int r2 = palette[t2[0]].rgbRed + palette[t2[1]].rgbRed + 
						 palette[t2[tileWidth]].rgbRed + palette[t2[tileWidth+1]].rgbRed;

			int g1 = palette[t1[0]].rgbGreen + palette[t1[1]].rgbGreen + 
						 palette[t1[tileWidth]].rgbGreen + palette[t1[tileWidth+1]].rgbGreen;
			int g2 = palette[t2[0]].rgbGreen + palette[t2[1]].rgbGreen + 
						 palette[t2[tileWidth]].rgbGreen + palette[t2[tileWidth+1]].rgbGreen;

			int b1 = palette[t1[0]].rgbBlue + palette[t1[1]].rgbBlue + 
						 palette[t1[tileWidth]].rgbBlue + palette[t1[tileWidth+1]].rgbBlue;
			int b2 = palette[t2[0]].rgbBlue + palette[t2[1]].rgbBlue + 
						 palette[t2[tileWidth]].rgbBlue + palette[t2[tileWidth+1]].rgbBlue;

			r1 -= r2;
			g1 -= g2;
			b1 -= b2;

			ULONG val = r1*r1 + g1*g1 + b1*b1;
#else

			ULONG val = palDiff[t1[0]][t2[0]] +
						   palDiff[t1[1]][t2[1]] +
							palDiff[t1[tileWidth]][t2[tileWidth]] +
							palDiff[t1[tileWidth+1]][t2[tileWidth+1]];

#endif


			if(val > currentBest)		// Shortcut exit
				return val;

			if(val > best)
				best = val;

		}
	}


#endif

	return best;
}

void TileSet::fillBestList()
{
	print(VB_DEBUG1, "Filling BestList\n");

	for(int i = 0; i < (nextTile - 1); i++)
	{
		for(int j = i + 1; j < nextTile; j++)
		{
			ULONG consider = bestList.getMaxConsider();

			ULONG diff = compareTile(bits[i], bits[j], consider);
			if(diff < consider)
				bestList.add((UWORD)i, (UWORD)j, diff);
		}
	}

	if(verbosity >= VB_DEBUG2)
		bestList.show();
}

/*
 * Merge 2 tiles
 */

void TileSet::merge(int close1, int close2, ULONG bestDist)
{
	print(VB_DEBUG1, "Merging %d and %d (diff=%ld)\n", close1, close2, bestDist);

	ASSERT(close1 != close2);

	/*
	 * Change close1's tile to be a colour mid-way between close1 and close2
	 */

	/*
	 * Remove close2 from tileList
	 */

	if(close1 > close2)
		swap(close1, close2);

	nextTile--;

	for(int i = close2; i < nextTile; i++)
		bits[i] = bits[i+1];

	/*
	 * Adjust map
	 */

	UWORD* mPtr = map;
	while(mPtr < nextMap)
	{
		if(*mPtr == close2)
			*mPtr = (UWORD) close1;
		else if(*mPtr > close2)
			*mPtr = (UWORD) (*mPtr - 1);
		mPtr++;
	}

	bestList.adjust((UWORD)close1, (UWORD)close2);

#if 0
	if(bestDist > lastBest)
		lastBest = bestDist;
#endif

	merged++;
}

/*
 * Find 2 tiles that are closest and merge them together
 */

void TileSet::merge()
{
	print(VB_DEBUG1, "Merging... \r");
	fflush(stdout);

	int close1 = 0;
	int close2 = 0;
	ULONG bestDist = 0xffffffff;		// MaxLong

	/*
	 * Pass 1:
	 *		Compare most recent tile with all tiles
	 *		because this is quite likely to have a close match
	 *		once all the original close matches are done
	 */

	int i = nextTile - 1;

	UBYTE* bitsI = bits[i];

	for(int j = 0; j < (nextTile - 1); j++)
	{
#if 0
		ULONG diff = compareTile(bitsI, bits[j], bestDist);
		if(diff < bestDist)
		{
			close1 = i;
			close2 = j;
			bestDist = diff;

			if(bestDist == 0)
				break;

		}

		if(diff < bestList.getMax())
			bestList.add((UWORD)i, (UWORD)j, diff);
#else
		ULONG consider = bestList.getMax();

		ULONG diff = compareTile(bitsI, bits[j], consider);
		if(diff < consider)
			bestList.add((UWORD)i, (UWORD)j, diff);
#endif
	}

#if 0
	if(bestDist <= lastBest)
	{
		merge(close1, close2, bestDist);
		return;
	}
#endif

	/*
	 * Get from best List
	 */

	if(bestList.isEmpty())
		fillBestList();

	BestEntry item;
	bestList.get(item);
	merge(item.tile1, item.tile2, item.diff);

#if 0
	/*
	 * Pass 2:
	 *		Continue from where we left off
	 */

	for(i = lastMerge; i < (nextTile - 1); i++)
	{
		for(int j = i + 1; j < nextTile; j++)
		{
			ULONG diff = compareTile(bits[i], bits[j], bestDist);
			if(diff < bestDist)
			{
				close1 = i;
				close2 = j;
				bestDist = diff;
				if(bestDist == 0)
					goto gotOne;

				if(bestDist <= lastBest)
					goto gotOne;
			}
		}
	}

	/*
	 * Process all
	 */

	for(i = 0; i < lastMerge; i++)
	{
		for(int j = i + 1; j < nextTile; j++)
		{
			ULONG diff = compareTile(bits[i], bits[j], bestDist);
			if(diff < bestDist)
			{
				close1 = i;
				close2 = j;
				bestDist = diff;
				if(bestDist == 0)
					goto gotOne;

				if(bestDist <= lastBest)
					goto gotOne;
			}
		}
	}

	gotOne:

	lastMerge = i;
#endif

}


/*
 * Make a table of palette differences
 */

void makePalDiffTable()
{
	print(VB_DEBUG1, "Making Palette difference table\n");

	for(int i = 0; i < 255; i++)
	{
		palDiff[i][i] = 0;		// Same colour!

		LPRGBQUAD c1 = &palette[i];

		for(int j = i + 1; j < 256; j++)
		{
			LPRGBQUAD c2 = &palette[j];

			int dR = c1->rgbRed - c2->rgbRed;
			int dG = c1->rgbGreen - c2->rgbGreen;
			int dB = c1->rgbBlue - c2->rgbBlue;

			ULONG val = dR * dR + dG * dG + dB * dB;

			palDiff[i][j] = val;
			palDiff[j][i] = val;
		}
	}
}

void makePalRemapTable()
{
	for(int i = 255; i >= 0; i--)
	{
		palRemap[i] = (UBYTE) i;

		LPRGBQUAD c1 = &palette[i];

		for(int j = i + 1; j < 256; j++)
		{
			LPRGBQUAD c2 = &palette[j];

			if( (c1->rgbRed == c2->rgbRed) &&
				 (c1->rgbGreen == c2->rgbGreen) &&
				 (c1->rgbBlue == c2->rgbBlue) )
					palRemap[j] = (UBYTE) i;
		}
	}
}

void remapBits(UBYTE* data, int size)
{
	while(size--)
	{
		*data = palRemap[*data];
		data++;
	}
}

/*
 * Do the actual work
 *
 * Return TRUE if succesful
 */

BOOL tile(LPCSTR srcName)
{
	/*
	 * Read in the file
	 */

	FileReader fr(srcName);

	if(fr.getError())
	{
		error("Can't open %s", srcName);
		return FALSE;
	}

	/*
	 * Read BITMAPFILEHEADER
	 */

	BITMAPFILEHEADER header;
	if(!fr.read(&header, sizeof(header)))
		return FALSE;

	print(VB_NORMAL, "\nBITMAPFILEHEADER:\n");
	print(VB_NORMAL, "%10s = `%c%c'\n", "Type", (char) header.bfType & 0xff, (char) (header.bfType >> 8));
	print(VB_NORMAL, "%10s = %ld\n", "Size",		header.bfSize);
	print(VB_NORMAL, "%10s = %hd\n", "Reserved1", header.bfReserved1);
	print(VB_NORMAL, "%10s = %hd\n", "Reserved2", header.bfReserved2);
	print(VB_NORMAL, "%10s = %ld\n", "OffBits",	header.bfOffBits);

	/*
	 * Read BITMAPINFO
	 */

	BITMAPINFOHEADER bmh;
	if(!fr.read(&bmh, sizeof(bmh)))
		return FALSE;

	print(VB_NORMAL, "\nBITMAPINFOHEADER:\n");
	print(VB_NORMAL, "%15s = %ld\n", "Size",				bmh.biSize);
	print(VB_NORMAL, "%15s = %ld\n", "Width",				bmh.biWidth);
	print(VB_NORMAL, "%15s = %ld\n", "Height",				bmh.biHeight);
	print(VB_NORMAL, "%15s = %hd\n", "Planes",				bmh.biPlanes);
	print(VB_NORMAL, "%15s = %hd\n", "BitCount",			bmh.biBitCount);
	print(VB_NORMAL, "%15s = %ld\n", "Compression",		bmh.biCompression);
	print(VB_NORMAL, "%15s = %ld\n", "SizeImage",			bmh.biSizeImage);
	print(VB_NORMAL, "%15s = %ld\n", "XPelsPerMeter",	bmh.biXPelsPerMeter);
	print(VB_NORMAL, "%15s = %ld\n", "YPelsPerMeter",	bmh.biYPelsPerMeter);
	print(VB_NORMAL, "%15s = %ld\n", "ClrUsed",			bmh.biClrUsed);
	print(VB_NORMAL, "%15s = %ld\n", "ClrImportant", 	bmh.biClrImportant);

	/*
	 * Check for valid type
	 */

	if((bmh.biPlanes != 1) ||
	   (bmh.biBitCount != 8) ||
		(bmh.biCompression != BI_RGB))
	{
		error("%s is not 256 colour RGB Windows BMP", srcName);
		return FALSE;
	}

	/*
	 * Read and Copy Colour Table
	 */

	RGBQUAD pal[256];

	if(!fr.read(pal, sizeof(pal)))
		return FALSE;

	if(verbosity >= VB_VERBOSE)
	{
		print(VB_VERBOSE, "\nColours:\n");
		for(int i = 0; i < 256; i++)
		{
			if( (i & 7) == 0)
				print(VB_VERBOSE, "\n%3d:", i);
			print(VB_VERBOSE, " %02x,%02x,%02x", pal[i].rgbRed, pal[i].rgbGreen, pal[i].rgbBlue);
		}
		print(VB_VERBOSE, "\n");
	}

	palette = pal;

	makePalRemapTable();
	makePalDiffTable();

	/*
	 * Read the bitmap tileHeight lines at a time.
	 *
	 * Remember bmp files are upside down
	 *
	 * Pad out bottom of image with colour 0
	 */

	int storageWidth = roundSize(bmh.biWidth, tileWidth);
	int storageHeight = roundSize(bmh.biHeight, tileHeight);

	print(VB_VERBOSE, "StorageWidth=%d, StorageHeight=%d\n",
		storageWidth, storageHeight);

	MemPtr<UBYTE> srcBuffer(storageWidth * tileHeight);

	int mapWidth = storageWidth / tileWidth;
	int mapHeight = storageHeight / tileHeight;

	print(VB_VERBOSE, "Map is %d by %d words = %d bytes\n",
		mapWidth, mapHeight, mapWidth * mapHeight * sizeof(UWORD));

	int yCount = storageHeight;

	TileSet tileSet(maxTiles, mapWidth, mapHeight);

	int tileProcCount = 0;

	MemPtr<UBYTE> tileBuf(tileWidth * tileHeight);

	while(yCount > 0)
	{
		print(VB_VERBOSE, "Reading %5d, tiles=%5d, processed=%5d, merged=%5d\n",
			yCount,
			tileSet.getCount(),
			tileProcCount,
			tileSet.getMerge());

		/*
		 * Fill up buffer
		 */

		UBYTE* p = srcBuffer + storageWidth * tileHeight;

		int yBuf = tileHeight;
		while(yBuf--)
		{
			p -= storageWidth;

			if(yCount > bmh.biHeight)
				memset(p, 0, storageWidth);
			else
			{
				if(!fr.read(p, bmh.biWidth))
					return FALSE;
				if(bmh.biWidth < storageWidth)
					memset(p + bmh.biWidth, 0, storageWidth - bmh.biWidth);
			}

			yCount--;
		}

		remapBits(srcBuffer, storageWidth * tileHeight);

		/*
		 * Add to current tile list
		 */

		p = srcBuffer;
		int xCount = mapWidth;
		while(xCount--)
		{
			/*
			 * Copy into tileBuf and add
			 */

			UBYTE* tBuf = tileBuf;
			UBYTE* sBuf = p;

			yBuf = tileHeight;
			while(yBuf--)
			{
				memcpy(tBuf, sBuf, tileWidth);
				tBuf += tileWidth;
				sBuf += storageWidth;
			}

			tileProcCount++;
			tileSet.addTile(tileBuf);
			p += tileWidth;
		}
	}

	print(VB_VERBOSE, "\n");

	/*
	 * For debugging purposes, save out the tile set as a bmp to compare
	 * what we've got
	 */

	FileWriter fw("tiletest.bmp");

	if(fw.getError())
		return FALSE;

	BITMAPFILEHEADER destHeader;
	destHeader = header;

	BITMAPINFOHEADER destbmh;
	destbmh = bmh;

	destbmh.biWidth = storageWidth;
	destbmh.biHeight = storageHeight;
	destbmh.biSizeImage = storageWidth * storageHeight;
	destHeader.bfSize = destbmh.biSizeImage + sizeof(BITMAPFILEHEADER) + sizeof(BITMAPINFOHEADER) + sizeof(pal);

	if(!fw.write(&destHeader, sizeof(destHeader)))
		return FALSE;
	if(!fw.write(&destbmh,	 sizeof(destbmh)))
		return FALSE;
	if(!fw.write(pal,			 sizeof(pal)))
		return FALSE;

	/*
	 * Write out the bits (remembering that bmps are upside down!)
	 */

	yCount = storageHeight;
	UWORD* mapBuf = tileSet.getMap();

	while(yCount > 0)
	{
		UBYTE* tb = srcBuffer;

		int xCount = mapWidth;
		while(xCount--)
		{
			UWORD tile = *mapBuf++;
			UBYTE* tPtr = tileSet.getTile(tile);

			UBYTE* tb1 = tb + storageWidth * tileHeight;
			int ty = tileHeight;
			while(ty--)
			{
				tb1 -= storageWidth;
				memcpy(tb1, tPtr, tileWidth);
				tPtr += tileWidth;
			}

			tb += tileWidth;
		}

		if(!fw.write(srcBuffer, storageWidth * tileHeight))
			return FALSE;

		yCount -= tileHeight;
	}


	return TRUE;
}


/*
 * Main Function
 */

void main(int argc, char* argv[])
{
	char* srcName = 0;

	int i = 0;
	while(++i < argc)
	{
		char* arg = argv[i];

		if( (arg[0] == '-') || (arg[0] == '/') )
		{
			switch(toupper(arg[1]))
			{
			case 'W':			// Width
				if(!aToInt(&arg[2], tileWidth))
				{
					error("-W must be followed by a number '%s'", arg);
					usage();
					return;
				}
				tileHeight = tileWidth;
				break;

			case 'M':			// Maximum Tiles
				if(!aToInt(&arg[2], maxTiles))
				{
					error("-M must be followed by a number '%s'", arg);
					usage();
					return;
				}
				break;

			case 'V':			// Verbosity Level
				if(isdigit(arg[2]))
				{
					if(!aToInt(&arg[2], verbosity))
					{
						error("-V was not followed by a number '%s'", arg);
						usage();
						return;
					}
				}
				else
					verbosity = VB_MAX;
				break;

			case '?':
			case 'H':
				usage();
				return;
			}
		}
		else
		if(srcName == 0)
			srcName = arg;
		else
		{
			error("Too Many parameters given");
			usage();
			return;
		}
	}

 	if(!srcName)
	{
		error("Not enough parameters on command line");
		usage();
		return;
	}

	if( (tileWidth <= 1) || (tileWidth >= (1 << 16)) )
	{
		error("Tile size must be between 2 and %ld", (1 << 16) - 1);
		usage();
		return;
	}

	if( (maxTiles < 1) || (maxTiles >= (1<<16)) )
	{
		error("Maximum Number of Tiles must between 1 and %ld", (1 << 16) - 1);
		usage();
		return;
	}

	print(VB_NORMAL, "Converting %s\n", srcName);
	print(VB_NORMAL, "Maximum of %d tiles\n", maxTiles);
	print(VB_NORMAL, "Tile size: %d by %d\n", tileWidth, tileHeight);

	if(tile(srcName))
		print(VB_NORMAL, "Successful\n");
	else
		error("Error tiling %s", srcName);
}
