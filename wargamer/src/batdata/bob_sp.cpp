/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 * Battle Strength Points
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "bob_sp.hpp"
// #include "bob_cp.hpp"
#include "bu_draw.hpp"
#include "filebase.hpp"
#include "ob.hpp"
#ifdef DEBUG
#include "boblog.hpp"
#endif

using namespace BOB_Definitions;

/*
 * Strength Point functions
 */

BattleSP::~BattleSP()
{
/*================================================================
 * Steven Changed Code without checking Out
 *================================================================
 */

 if(d_generic != NoStrengthPoint)
    d_generic->battleInfo(0);

/*================================================================
 * End Steven Changed Code without checking Out
 *================================================================
 */
}

BattleSP::BattleSP() :
   d_sister(NoBattleSP),
   d_parent(NoBattleCP),
   d_generic(NoStrengthPoint),
   d_formation(SP_Default),
   d_newFormation(SP_Default),
   d_formationTween(0),
   d_strength(SPStrength::MaxValue)
// d_whichSide(SP_LeftSide),
// d_colPos(0),
//   d_rowPos(0)
{
}

BattleSP::BattleSP(const BattleSP& sp) :
   d_sister(sp.d_sister),
   d_parent(sp.d_parent),
   d_generic(sp.d_generic),
   d_formation(sp.d_formation),
   d_newFormation(sp.d_newFormation),
   d_formationTween(sp.d_formationTween),
   d_strength(sp.d_strength)
{
}

BattleSP& BattleSP::operator = (const BattleSP& sp)
{
   d_sister = sp.d_sister;
   d_parent = sp.d_parent;
   d_generic = sp.d_generic;
   d_formation = sp.d_formation;
   d_newFormation= sp.d_newFormation;
   d_formationTween = sp.d_formationTween;
   d_strength = sp.d_strength;
   return *this;
}

void BattleSP::draw(UnitDrawer* drawer) const
{
   drawer->draw(this);
}

/*
 * Immediately set formation
 */

void BattleSP::setFormation(SPFormation form)
{
   ASSERT(form < SP_Formation_HowMany);
   d_formation = d_newFormation = form;
   d_formationTween = 0;
}

/*
 * Start a new formation change
 */

void BattleSP::changeFormation(SPFormation form)
{
   ASSERT(form < SP_Formation_HowMany);
   d_newFormation = form;
   if(d_newFormation != d_formation)
      d_formationTween = 2;
}

/*
 * Get current logical formation (including Changing)
 */

SPFormation BattleSP::formation() const
{
   if(d_formationTween.value() == 0)
      return d_formation;
   else
      return SP_ChangingFormation;
}

/*
 * Update tween value
 */

void BattleSP::addTween(FormationTween::Value change)
{
// ASSERT(d_formationTween.value() != 0);

   // If change makes it go over the range
   // then finish the formation change
   if(d_formationTween.add(change))
   {
      d_formation = d_newFormation;
      d_formationTween = 0;
   }
}

/*
 * File Interface
 */

inline bool operator >> (FileReader& f, SPFormation& spf)
{
   UBYTE b;
   f >> b;

   spf = static_cast<SPFormation>(b);
   return true;
}

inline bool operator << (FileWriter& f, const SPFormation& spf)
{
   return f.putUByte(spf);
}

const UWORD BattleSP::s_fileVersion = 0x0001;

Boolean BattleSP::readData(FileReader& f, OrderBattle& ob)
{
  UWORD version;
  f >> version;
  ASSERT(version <= s_fileVersion);

  if(version < 0x0001)
      ASSERT(d_generic == 0);
  else
  {
      ASSERT(d_generic != 0);
      ASSERT(d_generic->battleInfo() == this);
  }

  SPIndex spi;
  f >> spi;
  ISP sp = ob.sp(spi);
  d_sister = (spi != NoSPIndex) ? battleSP(sp) : NoBattleSP;

  if(version < 0x0001)
  {
     CPIndex cpi;
     f >> cpi;
      // d_parent = (cpi != NoCPIndex) ? battleCP(ob.command(cpi)) : NoBattleCP;
   }

  if(version < 0x0001)
  {
   f >> spi;
   d_generic = (spi != NoSPIndex) ? ob.sp(spi) : NoStrengthPoint;
   ASSERT(d_generic != NoStrengthPoint);
   d_generic->battleInfo(this);
  }

  f >> d_formation;
  f >> d_newFormation;

  UBYTE v;
  f >> v;
  d_formationTween = v;
  f >> v;
  d_strength = v;        // 0 = Dead, 100=Full strength

  if(!BattleUnit::readData(f, ob))
    return False;

  return f.isOK();
}

Boolean BattleSP::writeData(FileWriter& f, OrderBattle& ob) const
{
  f << s_fileVersion;

  SPIndex spi = (d_sister != NoBattleSP) ? ob.getIndex(d_sister->generic()) : NoSPIndex; //ob.getIndex(d_destSP);
  f << spi;

#if 0
  CPIndex cpi = (d_parent != NoBattleCP) ? ob.getIndex(d_parent->generic()) : NoCPIndex; //ob.getIndex(d_destSP);
  f << cpi;
#endif

#if 0
  spi = (d_generic != NoStrengthPoint) ? ob.getIndex(d_generic) : NoSPIndex; //ob.getIndex(d_destSP);
  f << spi;
#endif

  f << d_formation;
  f << d_newFormation;
  f << d_formationTween.value();
  f << d_strength.value();        // 0 = Dead, 100=Full strength

  if(!BattleUnit::writeData(f, ob))
    return False;

  return f.isOK();
}

/*
 * StrengthPoint container
 */

/*
 * Constructor
 */

BattleSPContainer::BattleSPContainer() :
   d_items()
{
#ifdef DEBUG
   bobLog.printf("BattleSPContainer constructor");
#endif
}

BattleSPContainer::~BattleSPContainer()
{
#ifdef DEBUG
   bobLog.printf("BattleSPContainer destructor");
#endif
}

/*
 * Create a new blank item
 */

ReturnRefBattleSP BattleSPContainer::create(ISP& isp)
{
   return create(BattleSP(), isp);
}

/*
 * Create a new item using given SP
 */

ReturnRefBattleSP BattleSPContainer::create(const BattleSP& sp, ISP& isp)
{
   PoolIndex i = d_items.add();
   // RefBattleSP newSP = &d_items[i];
   // *newSP = sp;
   d_items[i] = sp;
#ifdef DEBUG
   bobLog.printf("Created BattleSP %d", static_cast<int>(i));
#endif
   RefBattleSP nSP = &d_items[i];
   // return newSP;
   nSP->generic(isp);
   isp->battleInfo(nSP);

   return nSP;
}

void BattleSPContainer::reset()
{
#ifdef DEBUG
   bobLog.printf("BattleSPContainer reset");
#endif
   d_items.reset();
}

/*
 * File Interface
 */

const UWORD BattleSPContainer::s_fileVersion = 0x0001;


Boolean BattleSPContainer::readData(FileReader& f, OrderBattle& ob)
{
   UWORD version;
   f >> version;
   ASSERT(version <= s_fileVersion);

   UWORD nItems;
   f >> nItems;
   ASSERT(nItems >= 0);

   if(nItems != 0)
   {  
      d_items.reserve(nItems, true);
   }

   if(version < 0x0001)
   {
      /*
       * Pass 1 to get the generic IDs
       */

      SeekPos pos = f.getPos();

      for(PoolIndex i = 0; i < nItems; ++i)
      {
         UBYTE used;
         f >> used;
         if(used)
         {
            d_items[i].readData(f, ob);
         }
         else
            d_items.remove(i);
      }

      /*
       * Pass 2 to make sure sisters are set up correctly
       */

      f.seekTo(pos);

      for(i = 0; i < nItems; ++i)
      {
         UBYTE used;
         f >> used;
         if(used)
         {
            d_items[i].generic(0);     // to prevent assertion    
            d_items[i].readData(f, ob);
         }
      }
   }
   else  // version 1 reader
   {
      /*
       * read generic IDs
       */

      for(PoolIndex i = 0; i < nItems; ++i)
      {
         SPIndex spi;
         f >> spi;

                        if(spi != NoSPIndex)
                        {
                        // ASSERT(spi != NoSPIndex);

                           BattleSP* sp = &d_items[i];
                           ISP generic = ob.sp(spi);

                           sp->generic(generic);
                           generic->battleInfo(sp);
                        }
                        else
                        {
                           d_items.remove(i);
                        }

      }

      /*
       * Read rest of data
       */

      for(i = 0; i < nItems; ++i)
      {
                     if(d_items.isUsed(i))
         d_items[i].readData(f, ob);
                     else
                     {
                         BattleSP dummy;
                         StrengthPoint generic;
                         generic.battleInfo(&dummy);
                         dummy.generic(&generic);
                         dummy.readData(f,ob);
                         dummy.generic(0);
                         generic.battleInfo(0);
                     }
      }
   }

   return f.isOK();
}

Boolean BattleSPContainer::writeData(FileWriter& f, OrderBattle& ob) const
{
  f << s_fileVersion;

#if 0 /// old version 0
  UWORD nItems = d_items.entries();
  f << nItems;

  for(PoolIndex i = 0; i < nItems; i++)
  {
    UBYTE used = d_items.isUsed(i);
    f << used;

    if(used)
    {
       const BattleSP* sp = &d_items[i];

      if(!sp->writeData(f, ob))
         return False;
    }
  }
#else    // version 1

   /*
    * Count used
    */

   UWORD poolItems = d_items.entries();
   UWORD nItems = 0;

   for(PoolIndex i = 0; i < poolItems; i++)
   {
      if(d_items.isUsed(i))
         ++nItems;
   }

   f << nItems;

  /*
   * Write Generic IDs
   */

        for(i = 0; i < poolItems; i++)
   {
      if(d_items.isUsed(i))
      {
         const ISP& isp = d_items[i].generic();
         ASSERT(isp != NoStrengthPoint);
         SPIndex spi = ob.getIndex(isp);
         f << spi;
      }
   }

   /*
    * write data without generic ID
    */

        for(i = 0; i < poolItems; i++)
   {
      if(d_items.isUsed(i))
      {
         const BattleSP* sp = &d_items[i];

         if(!sp->writeData(f, ob))
            return False;
      }
   }
#endif

   return f.isOK();
}


/*
 *----------------------------------------------------------------------
 * $Log$
 *----------------------------------------------------------------------
 */
