/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
#ifndef PDEPLOY_HPP
#define PDEPLOY_HPP

#include "bd_dll.h"
// #include "batdata.hpp"
#include "BattleDataFwd.hpp"
#include "batunit.hpp"
#include "batord.hpp"
#include "bob_cp.hpp"

// Forward reference
namespace BattleMeasure { class HexCord; }
   class HexList;

namespace PlayerDeploy {
   

enum {
  INITIALIZE        = 0x01,
  REORGANIZE        = 0x02,
  FILLHEXLIST       = 0x04,
  NEVERFAIL         = 0x08,
  NOCOUNTZEROSP     = 0x10,
  TERRAINONLY       = 0x20,
  ENEMYONLY         = 0x40
};

/*

   Main PlayerDeploy function to handle the placing of units on battlefield

*/

BATDATA_DLL bool
playerDeploy(
   RPBattleData batData,
   const RefBattleCP & cp,
   const BattleMeasure::HexCord & startHex,
   const BattleOrderInfo* oi,
   HexList* hl,
   unsigned int flags);

/*

   User entry-point for PlayerDeploy, which uses CP's current order as an input parameter

*/

inline bool playerDeploy(
   RPBattleData batData,
   const RefBattleCP & cp,
   const BattleMeasure::HexCord & startHex,
   HexList* hl,
   unsigned int flags) {

    const BattleOrderInfo* oi = (cp->getCurrentOrder().nextWayPoint()) ? &(cp->getCurrentOrder().wayPoints().getLast()->d_order) : &(cp->getCurrentOrder().order() );
    return playerDeploy(batData, cp, startHex, oi, hl, flags);
}

inline bool playerDeploy(
   RCPBattleData batData,
   const CRefBattleCP & cp,
   const BattleMeasure::HexCord & startHex,
//   const BattleOrderInfo* oi,
   HexList* hl,
   unsigned int flags) 
{
   ASSERT(hl);
   ASSERT(flags & PlayerDeploy::FILLHEXLIST);

   const BattleData* bd = batData;
   RPBattleData ncBatData = const_cast<BattleData*>(bd);

   RefBattleCP ncCP = const_cast<BattleCP*>(cp);

//    return playerDeploy(ncBatData, ncCP, startHex, oi, hl, flags);
    return playerDeploy(ncBatData, ncCP, startHex, hl, flags);
}

inline bool playerDeploy(
   RCPBattleData batData,
   const CRefBattleCP & cp,
   const BattleMeasure::HexCord & startHex,
   const BattleOrderInfo* oi,
   HexList* hl,
   unsigned int flags) 
{
   ASSERT(hl);
   ASSERT(flags & PlayerDeploy::FILLHEXLIST);

   const BattleData* bd = batData;
   RPBattleData ncBatData = const_cast<BattleData*>(bd);

   RefBattleCP ncCP = const_cast<BattleCP*>(cp);

    return playerDeploy(ncBatData, ncCP, startHex, oi, hl, flags);
}








#if 0
enum DeployFlags {
   Initializing,
   Reorganizing,
    None
};

BATDATA_DLL bool playerDeploy(
                RPBattleData batData,
                const RefBattleCP& cp,
                const HexCord& startHex,
                        BattleOrderInfo* oi,
                HexList* hl,
                DeployFlags flags);

inline bool playerDeploy(
                RPBattleData batData,
                const RefBattleCP& cp,
                const HexCord& startHex,
                HexList* hl,
                DeployFlags flags)
{
    BattleOrderInfo* oi = (cp->getCurrentOrder().nextWayPoint()) ?
                &(cp->getCurrentOrder().wayPoints().getLast()->d_order) : &(cp->getCurrentOrder().order() );

   return playerDeploy(batData, cp, startHex, oi, hl, flags);
}


inline bool playerDeployFromOrder(
                RPBattleData batData,
                const RefBattleCP& cp,
                const HexCord& startHex,
                BattleOrder * order,
                HexList* hl,
                        DeployFlags flags)
{
    BattleOrderInfo* oi = (order->nextWayPoint()) ?
                &order->wayPoints().getLast()->d_order : &order->order();

   return playerDeploy(batData, cp, startHex, oi, hl, flags);
}

inline bool playerDeployFromOrder(
                RPBattleData batData,
                const RefBattleCP& cp,
                const HexCord& startHex,
                BattleOrderInfo* oi,
                HexList* hl,
                DeployFlags flags)
{
   return playerDeploy(batData, cp, startHex, oi, hl, flags);
}

bool BATDATA_DLL getPlayerDeployHexes(
   RPBattleData batData,
   const RefBattleCP& cp,
   const HexCord& startHex,
   BattleOrder * order,
   HexList* hl,
   DeployFlags flags);

#endif

               

BATDATA_DLL bool canHexBeOccupied(
   RCPBattleData bd,
   const RefBattleCP& cp,
   const BattleMeasure::HexCord& hex,
   bool terrainOnly,
   bool ourUnitsToo,
   bool currentFormation,
   bool allowForBridge);
BATDATA_DLL void placeUnit(RPBattleData bd, const RefBattleUnit& unit, const BattleMeasure::HexCord& hex);

}     // namespace



#endif
/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.3  2001/07/02 13:18:24  greenius
 * Battle, Campaign, Combined game and Campaign Editor now compile and run... mostly
 *
 * Revision 1.2  2001/06/18 08:25:43  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
