/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef BATCTRL_HPP
#define BATCTRL_HPP

#ifndef __cplusplus
#error batctrl.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *	Battle Game Controller
 *
 *----------------------------------------------------------------------
 */

// #include "string.hpp"
// #include "b_result.hpp"

class BattleData;
// class BatTimeControl;
class BattleMessageInfo;
class BattleFinish;

class BattleGameInterface
{
	public:
        virtual ~BattleGameInterface() = 0;

        virtual BattleData* battleData() = 0;
        virtual const BattleData* battleData() const = 0;
        
        // virtual BatTimeControl* timeControl() = 0;
        // virtual const BatTimeControl* timeControl() const = 0;
        // virtual String battleName() const = 0;

        virtual void sendPlayerMessage(const BattleMessageInfo&) = 0;

        // virtual void openBattle() = 0;
        // virtual void saveBattle(bool promptName) = 0;

        virtual void requestBattleOver(const BattleFinish& result) = 0;
		  virtual bool isOver() = 0;

		  virtual void stopCampaignTime() = 0;
        virtual void startCampaignTime() = 0;
						// Battle finishes somehow.. this can be called from another thread

        // virtual void requestOpenBattle() = 0;
            // Open selected from File Menu

        // virtual void requestSaveBattle(bool prompt) = 0;
            // Save or Save-As selected from File Menu

        // virtual void setController(Side side, GamePlayerControl::Controller control) = 0;
};

inline BattleGameInterface::~BattleGameInterface() { }

#endif /* BATCTRL_HPP */

