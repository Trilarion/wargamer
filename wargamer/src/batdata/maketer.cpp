/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *      Random Battlefield creation
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "maketer.hpp"
#include "batdata.hpp"
#include "batinfo.hpp"
#include "hexdata.hpp"
#include "building.hpp"
#include "wg_rand.hpp"
#include "memptr.hpp"
#include "b_terr.hpp"
#include <stack>                      // STL container
#include "sllist.hpp"
#include "fractmap.hpp"
#include "pathfind.hpp"
#include "key_terr.hpp"

using namespace BattleBuildings;
using namespace BattleMeasure;


#define TERRAIN_GENERATOR_VERSION 1.2f

/*
* Define this to make generator create burning / smouldering buildings
*/

//#define RANDOM_BUILDING_STATUS


/*
This table holds the default parameters for generating height fields
*/

FractalMapParameters TerrainHeightFieldParameters[] = {

    // HEIGHT_FLAT
    FractalMapParameters(
        32,
        0,
        64,
        8,
        1,
        BattleTerrainHex::MaxHeight /2 ),

    // HEIGHT_ROLLING
    FractalMapParameters(
        32,
        0,
        128,
        28,
        2,
        (BattleTerrainHex::MaxHeight *2)/3),

    // HEIGHT_HILLY
    FractalMapParameters(
        64,
        0,
        128,
        32,
        6,
        BattleTerrainHex::MaxHeight),

    // HEIGHT_STEEP
    FractalMapParameters(
        64,
        0,
        128,
        64,
        2,
        BattleTerrainHex::MaxHeight)

};

/*
    // HEIGHT_FLAT
    FractalMapParameters(
        64,
        0,
        128,
        8,
        1,
        BattleTerrainHex::MaxHeight /2 ),

    // HEIGHT_ROLLING
    FractalMapParameters(
        64,
        0,
        128,
        64,
        8,
        BattleTerrainHex::MaxHeight),

    // HEIGHT_HILLY
    FractalMapParameters(
        64,
        0,
        128,
        128,
        18,  // 20
        BattleTerrainHex::MaxHeight),

    // HEIGHT_STEEP
    FractalMapParameters(
        64,
        0,
        128,
        64,
        0,
        BattleTerrainHex::MaxHeight)
*/


class BattleCreateImp {

        public:



        private:
                TerrainGenerationParameters * parameters;

                int MinimumHeight;
                int MaximumHeight;


                enum SettlementType {
                    Normal,
                    Farm,
                    Special
                };

                typedef struct SeedPoint : public SLink {
                    HexCord hex;
                    SettlementType type;
                    int size;
                } SeedPoint;

                SList <SeedPoint> SettlementSeedPoints;

                typedef struct SpecialListNode : public SLink {
                    int special;
                } SpecialListNode;

                bool d_paramsAlloced;


                KeyTerrainList * d_keyTerrainList;

        public:

                BattleCreateImp(TerrainGenerationParameters* params);
                BattleCreateImp(void);
                ~BattleCreateImp();

                void make(BattleData* batData);


                /* Return a random number from (0...range) inclusive */
                /* Return a random number from (min...max) inclusive */


                void SetDefaultParameters(void);
                int ReadParametersFile(char * filename);

                /* New terrain generation functions */

                void MakeEmptyMap(BattleMap * map, const HexCord& size);
                void ClearWholeMap(BattleMap * map, TerrainType type);
                void CreateTerrainHex(BattleMap * map, const HexCord& hex, TerrainType type);
                void CreateTerrainClump(BattleMap * map, const HexCord& centre_hex, TerrainType terrain_type, int size, int density);
                void CreateTerrainClump(
                    BattleMap * map,
                    const HexCord& centre_hex,
                    TerrainType terrain_type,
                    int size, int density,
                    int use_sprites,
                    int small_sprite_index,
                    int large_sprite_index,
                    const BuildingTable* buildingTable,
                    BuildingList* buildings);

                void CreateTerrainArea(BattleMap * map, const HexCord& centre_hex, TerrainType terrain_type, int size, int spread_chance);
                void EditorCreateTerrainArea(BattleMap * map, const HexCord& centre_hex, TerrainType terrain_type, int size, int spread_chance, int mode);
                void CreateTerrainScattering(BattleMap * map, const HexCord& centre_hex, TerrainType terrain_type, int size, int density);

                void CreateHeightMap(BattleMap * map);
                void SetAllHeights(BattleMap * map, int height);
                void AlterHexHeight(BattleMap * map, const HexCord& hex, int ammount);
                void AlterHexAreaHeight(BattleMap * map, const HexCord& centre_hex, int size, int spread_chance, int ammount);
                void AlterHexClumpHeight(BattleMap * map, const HexCord& centre_hex, int size, int density, int ammount);

                void CreateBasicTerrain(BattleMap * map, const TerrainTable & terrain);
                void CreateComplexTerrain(BattleMap * map, const TerrainTable & terrain);
                void CreateTerrainPatch(BattleMap * map, TerrainType terrain_type, const HexCord& centre_hex, int size, int density);

                void CreateLakes(BattleMap * map);
                void ClearCoasts(BattleMap * map);
                void makeCoasts(BattleMap* map, const TerrainTable& terrain);
                void SetEdges(BattleMap * map, const HexCord& hex, EdgeInfo edge_info);
                void ClearAllEdges(BattleMap * map);
                void CreateWoods(BattleMap * map, const BuildingTable * buildingTable, BuildingList * buildings);



                void SetRandomEdges(BattleMap * map, HexCord * A, HexCord * B);
                int DoesPathCrossWater(BattleMap * map, const TerrainTable & terrain, SList<PathPoint> * Path);
                int DoesPathCrossPathType(BattleMap * map, SList<PathPoint> * Path, LogicalPathEnum path_type, int path_number);
                int DoesPathCrossPathTypeAtDifferentAngle(BattleMap * map, SList<PathPoint> * Path, LogicalPathEnum path_type);
                int DoesPathCrossRiversConstantly(BattleMap * map, SList<PathPoint> * Path, int frequency);

                void ClearAllPaths(BattleMap * map);
                void CreateRivers(BattleData * batdata, BattleMap * map, const TerrainTable & terrain);
                void CreateStreams(BattleData * batdata, BattleMap * map, const TerrainTable & terrain);
                void ConstructRiverPath(BattleMap * map, const TerrainTable & terrain, SList<PathPoint> * RiverPath, LogicalPathEnum RiverType);

                void CreateRoads(BattleData * batdata, BattleMap * map, const TerrainTable & terrain);
                void CreateTracks(BattleData * batdata, BattleMap * map, const TerrainTable & terrain);
                void ConstructRoadPath(BattleMap * map, const TerrainTable & terrain, SList<PathPoint> * RoadPath, LogicalPathEnum RoadType);

            void ProcessPathsExitingMap(BattleData * batdata, BattleMap * map);

                void AddSettlementSeedingPoint(const HexCord& hexpos, SettlementType type);
                void CreateRandomSettlements(BattleMap * map, const TerrainTable & terrain);
                void CreateSingleSettlement(const BuildingTable * buildingTable, BuildingList * buildings, const HexCord& hex, BuildingListEntry entry);
                void CreateSettlements(BattleMap * map, const TerrainTable & terrain, const BuildingTable * buildingTable, BuildingList * buildings);
                void ScatterVillageSettlements(BattleMap * map, const TerrainTable & terrain, SeedPoint * seed_point, const BuildingTable* buildingTable, BuildingList* buildings);
                void ScatterTownSettlements(BattleMap * map, const TerrainTable & terrain, SeedPoint * seed_point, const BuildingTable* buildingTable, BuildingList* buildings);

                void CreateFarm(BattleMap * map, const TerrainTable & terrain, SeedPoint * seed_point, const BuildingTable* buildingTable, BuildingList* buildings);

                void ReplaceBridgesWithBuildings(BattleMap * map, const TerrainTable & terrain, const BuildingTable* buildingTable, BuildingList* buildings);
                int GetBridgeOrientation(EdgeMap edges);

                void FixDodgyPaths(BattleMap * map);

                void CreateTestBuildings(BattleMap * map, const TerrainTable & terrain, const BuildingTable* buildingTable, BuildingList* buildings);
    private:
        HexCord::HexDirection randomDirection() const
        {
//            return static_cast<HexCord::HexDirection>(CRandom::get(HexCord::First, HexCord::End));
            return static_cast<HexCord::HexDirection>(CRandom::get(HexCord::First, HexCord::End-1));
        }

};



BattleCreateImp::BattleCreateImp(TerrainGenerationParameters* params) :
    parameters(params),
    d_paramsAlloced(false)
{
    if(params == 0)
    {
        parameters = new TerrainGenerationParameters;
        if(ReadParametersFile("bfield.dat") == 0)
            SetDefaultParameters();

        d_paramsAlloced = true;
    }

}

BattleCreateImp::BattleCreateImp(void) {

    d_keyTerrainList = NULL;

    d_paramsAlloced = false;

}



BattleCreateImp::~BattleCreateImp()
{
    if(d_paramsAlloced)
        delete parameters;
}

void BattleCreateImp::make(BattleData* batdata)
{
    ASSERT(parameters != 0);

    BattleMap * map = batdata->map();
    BuildingList * buildings = batdata->buildingList();
    TerrainTable & terrain = batdata->terrainTable();
    BuildingTable * buildingTable = batdata->buildingTable();

    d_keyTerrainList = batdata->keyTerrainList();
    d_keyTerrainList->get()->reset();

   parameters->d_numVillagesCreated = 0;
   parameters->d_numTownsCreated = 0;
   parameters->d_numFarmsCreated = 0;

    // create an empty map
    HexCord hexSize;
    hexSize.x(parameters->MapWidth);
    hexSize.y(parameters->MapHeight);
    MakeEmptyMap(map, hexSize);

   // set boundary hexes
   batdata->maximizePlayingArea();



    CreateHeightMap(map);
    CreateBasicTerrain(map, terrain);

    CreateLakes(map);
    makeCoasts(map, terrain);

    //                CreateComplexTerrain(map, terrain);

    CreateRivers(batdata, map, terrain);
    CreateStreams(batdata, map, terrain);
    CreateRoads(batdata, map, terrain);
    CreateTracks(batdata, map, terrain);

    // place bridges as buildings, in positions where paths indicate bridges
    ReplaceBridgesWithBuildings(map, terrain, buildingTable, buildings);

   CreateRandomSettlements(map, terrain);
    CreateSettlements(map, terrain, buildingTable, buildings);
    CreateWoods(map, buildingTable, buildings);


#ifdef DEBUG
    CreateTestBuildings(map, terrain, buildingTable, buildings);
   d_keyTerrainList->check();
#endif


}



/*
Sets a default set of parameters for the terrain generation
For use in testing
*/

void
BattleCreateImp::SetDefaultParameters(void) {



        parameters->HeightType = HEIGHT_FLAT;

        parameters->FirstTerrainType = GT_Grass;
        parameters->SecondTerrainType = GT_Rough;
        parameters->PercentageSecondTerrain = 15;
      parameters->PercentageThirdTerrain = 0;
      parameters->PercentageFourthTerrain = 0;
      parameters->PercentageFifthTerrain = 0;


        parameters->NumberOfRivers = 2;
        parameters->NumberOfStreams = 4;
        parameters->NumberOfRoads = 3;
        parameters->NumberOfTracks =5;

        parameters->ChanceRoadsJoin = 50;
        parameters->ChanceCreateBridge = 100;

        parameters->MinimumSettlementSize = 4;
        parameters->MaximumSettlementSize = 10;
        parameters->MaximumVillageSize = 6;
        parameters->MaximumTownSize = 10;
        parameters->VillageDensity = 3;
        parameters->TownDensity = 12;
        parameters->CombineSettlementsDistance = 8;

        parameters->MinimumFarmSize = 2;
        parameters->MaximumFarmSize = 4;

        parameters->ChanceSettlementAtCrossroads = 50;
        parameters->ChanceSettlementAtJunction = 25;
        parameters->ChanceSettlementAtBridge = 50;
        parameters->ChanceSettlementAtFord = 25;

        parameters->NumberOfRandomSettlements = 5;
        parameters->ChanceRandomSettlementIsSpecial = 10;
        parameters->ChanceRandomSettlementIsSolitary = 10;
        parameters->ChanceRandomSettlementIsFarm = 100;

        parameters->ChanceFarmsCreateFields = 50;
        parameters->AverageFarmFieldsSize = 3;

        parameters->NumberOfLightWoods = 5;
        parameters->NumberOfDenseWoods = 5;
        parameters->MinimumWoodSize = 5;
        parameters->MaximumWoodSize = 10;
        parameters->MinimumWoodDensity = 4;
        parameters->MaximumWoodDensity = 8;

        parameters->NumberOfLakes = 2;
        parameters->MinimumLakeSize = 2;
        parameters->MaximumLakeSize = 6;

      parameters->d_numVillagesCreated = 0;
      parameters->d_numTownsCreated = 0;
      parameters->d_numFarmsCreated = 0;

}

struct ClumpItem {

    HexCord hexpos;
    int counter;

    bool operator < (const ClumpItem& i) const { return counter < i.counter; }
    bool operator == (const ClumpItem& i) const { return (counter == i.counter) && (hexpos == i.hexpos); }

};

typedef std::queue <ClumpItem, std::deque<ClumpItem> > ClumpQueue;




void
BattleCreateImp::MakeEmptyMap(BattleMap * map, const HexCord& size) {
    // create an empty map
    map->create(size);
}


void
BattleCreateImp::ClearWholeMap(BattleMap * map, TerrainType type) {

HexCord MapSize = map->getSize();
int width = MapSize.x();
int height = MapSize.y();
HexCord hex;

        for(int y=0; y<height; y++) {

            for(int x=0; x<width; x++) {

                hex.x(x);
                hex.y(y);

                BattleTerrainHex& maphex = map->get(hex);
                maphex.d_terrainType = type;
            }
        }
}


/*
Create a single hex of given type
*/

void
BattleCreateImp::CreateTerrainHex(BattleMap * map, const HexCord& hex, TerrainType type) {

    BattleTerrainHex& maphex = map->get(hex);
    maphex.d_terrainType = type;

}


/*
Creates a 'clump' of a certain terrain-type, spreading out from the centre hex
Size & Density define the proportions of the clump
*/

void
BattleCreateImp::CreateTerrainClump(
        BattleMap * map,
        const HexCord& centre_hex,
        TerrainType terrain_type,
        int size, int density) {


    ClumpItem clump_hex;
    HexCord move_hex;
    int current_terrain;
    int current_counter;

    // first clump in queue is at the centre pos
    clump_hex.hexpos.x(centre_hex.x());
    clump_hex.hexpos.y(centre_hex.y());
    clump_hex.counter = 1;

    // return if the initial hex isn't clear
    BattleTerrainHex& maphex = map->get(clump_hex.hexpos);
    current_terrain = maphex.d_terrainType;
    if(current_terrain != GT_None && current_terrain != parameters->FirstTerrainType && current_terrain != parameters->SecondTerrainType) return;
    // set the terrain at the initial hex
    maphex.d_terrainType = terrain_type;

    // initialize the queue
    ClumpQueue clump_queue;
    clump_queue.push(clump_hex);

    // repeat until queue is empty
    while(! clump_queue.empty() ) {

        // get the first item in queue
        clump_hex = clump_queue.front();
        // remove this clump from queue
        clump_queue.pop();

        // make sure the counter hasn't reached the maximum clump size
        current_counter = clump_hex.counter;
        if(current_counter < size) {

            // add random surrounding hexes, up to counter * density & set their terrain types
            for(int n=0; n < density; n++) {

                // choose a random direction
                // int rand_dir = Random(1,7);
                HexCord::HexDirection rand_dir = randomDirection();
                // if on map, then add
                if(map->moveHex(clump_hex.hexpos, rand_dir, move_hex)) {
                    // get the terrain type at this hex
                    BattleTerrainHex& maphex = map->get(move_hex);
                    current_terrain = maphex.d_terrainType;
                    // only add this to the queue if the terrain here is clear
                    if(current_terrain == GT_None || current_terrain == parameters->FirstTerrainType || current_terrain == parameters->SecondTerrainType) {
                        // set the terrain type here
                        maphex.d_terrainType = terrain_type;
                        // fill out the new clump structure
                        clump_hex.hexpos = move_hex;
                        clump_hex.counter = current_counter+1;
                        // push in at end of queue
                        clump_queue.push(clump_hex);
                    }

                }  // if this was a valid map hex

            }  // repeat until the required density

        } // if counter hadn't expired

    }  // when the queue is empty, quit

}

/*
Creates a 'clump' of a certain terrain-type, spreading out from the centre hex
Size & Density define the proportions of the clump
if UseSprites is set then sprites clusters are placed on each hex created
SmallSpriteIndex is the bottom of range & LargeSpriteIndex is the top of the range
LargeSprites clusters are placed at centre of clump, and Smaller ones at the outside

const BuildingTable* buildingTable, BuildingList* buildings
if(building_type > BattleBuildings::TOWN_LARGE) building_type = BattleBuildings::TOWN_LARGE;
                                buildings->add(hexpos, building_type);
*/

void
BattleCreateImp::CreateTerrainClump(
        BattleMap * map,
        const HexCord& centre_hex,
        TerrainType terrain_type,
        int size, int density,
        int use_sprites,
        int small_sprite_index,
        int large_sprite_index,
        const BuildingTable* buildingTable,
        BuildingList* buildings) {

   BuildingListEntry entry;
   entry.status(BUILDING_NORMAL);
   entry.frame(0);
   entry.timer(0);
   entry.effectCount(0);

    int building_type;

    ClumpItem clump_hex;
    HexCord move_hex;
    int current_terrain;
    int current_counter;

    // first clump in queue is at the centre pos
    clump_hex.hexpos.x(centre_hex.x());
    clump_hex.hexpos.y(centre_hex.y());
    clump_hex.counter = 1;

    // return if the initial hex isn't clear
    BattleTerrainHex& maphex = map->get(clump_hex.hexpos);
    current_terrain = maphex.d_terrainType;
    if(current_terrain != GT_None && current_terrain != parameters->FirstTerrainType && current_terrain != parameters->SecondTerrainType) return;
    // set the terrain at the initial hex
    maphex.d_terrainType = terrain_type;

    // if we're using sprites, add the largest one at the centre
    if(use_sprites != 0) {
        building_type = large_sprite_index;

      entry.index(building_type);

        buildings->add(clump_hex.hexpos, entry); }

    // initialize the queue
    ClumpQueue clump_queue;
    clump_queue.push(clump_hex);

    // repeat until queue is empty
    while(! clump_queue.empty() ) {

        // get the first item in queue
        clump_hex = clump_queue.front();
        // remove this clump from queue
        clump_queue.pop();

        // make sure the counter hasn't reached the maximum clump size
        current_counter = clump_hex.counter;
        if(current_counter < size) {

            // add random surrounding hexes, up to counter * density & set their terrain types
            for(int n=0; n < density; n++) {

                // choose a random direction
                // int rand_dir = Random(1,7);
                HexCord::HexDirection rand_dir = randomDirection();
                // if on map, then add
                if(map->moveHex(clump_hex.hexpos, rand_dir, move_hex)) {
                    // get the terrain type at this hex
                    BattleTerrainHex& maphex = map->get(move_hex);
                    current_terrain = maphex.d_terrainType;
                    // only add this to the queue if the terrain here is clear
                    if(current_terrain == GT_None || current_terrain == parameters->FirstTerrainType || current_terrain == parameters->SecondTerrainType) {
                        // set the terrain type here
                        maphex.d_terrainType = terrain_type;
                        // if we're using sprites, place a cluster here
                        if(use_sprites != 0) {
//                            building_type = CRandom::get(small_sprite_index, large_sprite_index);
                            building_type = CRandom::get(small_sprite_index, large_sprite_index-1);
//                            building_type -= current_counter;
//                            if(building_type < small_sprite_index) building_type = small_sprite_index;

                     entry.index(building_type);

                            buildings->add(move_hex, entry); }
                        // fill out the new clump structure
                        clump_hex.hexpos = move_hex;
                        clump_hex.counter = current_counter+1;
                        // push in at end of queue
                        clump_queue.push(clump_hex);
                    }

                }  // if this was a valid map hex

            }  // repeat until the required density

        } // if counter hadn't expired

    }  // when the queue is empty, quit

}











void
BattleCreateImp::CreateTerrainArea(BattleMap * map, const HexCord& centre_hex, TerrainType terrain_type, int size, int spread_chance) {

    ClumpItem clump_hex;
    HexCord move_hex;
    int current_terrain;
    int current_counter;

    // first clump in queue is at the centre pos
    clump_hex.hexpos.x(centre_hex.x());
    clump_hex.hexpos.y(centre_hex.y());
    clump_hex.counter = 1;

    // return if the initial hex isn't clear
    BattleTerrainHex& maphex = map->get(clump_hex.hexpos);
    current_terrain = maphex.d_terrainType;
    if(current_terrain != GT_None && current_terrain != parameters->FirstTerrainType && current_terrain != parameters->SecondTerrainType) return;
    // set the terrain at the initial hex
    maphex.d_terrainType = terrain_type;

    // initialize the queue
    ClumpQueue clump_queue;
    clump_queue.push(clump_hex);

    // repeat until queue is empty
    while(! clump_queue.empty() ) {

        // get the first item in queue
        clump_hex = clump_queue.front();
        // remove this clump from queue
        clump_queue.pop();

        // make sure the counter hasn't reached the maximum clump size
        current_counter = clump_hex.counter;
        if(current_counter < size) {

            // add all surrounding hexes if clear
            for(int dir=1; dir < 7; dir++) {

                // if on map, then add
                if(map->moveHex(clump_hex.hexpos, static_cast <HexCord::HexDirection> (dir), move_hex)) {
                    // get the terrain type at this hex
                    BattleTerrainHex& maphex = map->get(move_hex);
                    current_terrain = maphex.d_terrainType;
                    // only add this to the queue if the terrain here is clear
                    if(current_terrain == GT_None || current_terrain == parameters->FirstTerrainType || current_terrain == parameters->SecondTerrainType) {
                        // set the terrain type here
                        maphex.d_terrainType = terrain_type;
                        // fill out the new clump structure
                        clump_hex.hexpos = move_hex;
                        // if random chance is within range, then this hex is added without an increment to the counter
                        if(CRandom::get(100) < spread_chance) clump_hex.counter = current_counter;
                        // otherwise the counter is increased as normal
                        else clump_hex.counter = current_counter+1;
                        // push in at end of queue
                        clump_queue.push(clump_hex);
                    }

                }  // if this was a valid map hex

            }  // repeat until all surrounding hexes are added

        } // if counter hadn't expired

    }  // when the queue is empty, quit

}




/*
A duplicate of the above function for the editor
the MODE parameter determines wether to
        0) overwrite all hexes, or to
        1) 'flood fill' overwriting only hexes of the type in the starting hex
*/

void
BattleCreateImp::EditorCreateTerrainArea(BattleMap * map, const HexCord& centre_hex, TerrainType terrain_type, int size, int spread_chance, int mode) {

    ClumpItem clump_hex;
    HexCord move_hex;
    int current_terrain;
    int current_counter;

    BattleTerrainHex& maphex = map->get(centre_hex);
    TerrainType initial_type = maphex.d_terrainType;

    // first clump in queue is at the centre pos
    clump_hex.hexpos.x(centre_hex.x());
    clump_hex.hexpos.y(centre_hex.y());
    clump_hex.counter = 1;

    // initialize the queue
    ClumpQueue clump_queue;
    clump_queue.push(clump_hex);

    // repeat until queue is empty
    while(! clump_queue.empty() ) {

        // get the first item in queue
        clump_hex = clump_queue.front();
        // remove this clump from queue
        clump_queue.pop();

        // make sure the counter hasn't reached the maximum clump size
        current_counter = clump_hex.counter;
        if(current_counter < size) {

            // add all surrounding hexes if clear
            for(int dir=1; dir < 7; dir++) {

                // if on map, then add
                if(map->moveHex(clump_hex.hexpos, static_cast <HexCord::HexDirection> (dir), move_hex)) {
                    // get the terrain type at this hex
                    BattleTerrainHex& maphex = map->get(move_hex);
                    current_terrain = maphex.d_terrainType;
                    // if mode == 1 only add this to the queue if the terrain here is same as initial type
                    if(mode == 1) {
                        if(current_terrain == initial_type) {
                        // set the terrain type here
                        maphex.d_terrainType = terrain_type;
                        // fill out the new clump structure
                        clump_hex.hexpos = move_hex;
                        // if random chance is within range, then this hex is added without an increment to the counter
                        if(CRandom::get(100) < spread_chance) clump_hex.counter = current_counter;
                        // otherwise the counter is increased as normal
                        else clump_hex.counter = current_counter+1;
                        // push in at end of queue
                        clump_queue.push(clump_hex);
                        }
                    }
                    // if mode == 0, add regardless of terrain type
                    else if(mode == 0) {
                        // set the terrain type here
                        maphex.d_terrainType = terrain_type;
                        // fill out the new clump structure
                        clump_hex.hexpos = move_hex;
                        // if random chance is within range, then this hex is added without an increment to the counter
                        if(CRandom::get(100) < spread_chance) clump_hex.counter = current_counter;
                        // otherwise the counter is increased as normal
                        else clump_hex.counter = current_counter+1;
                        // push in at end of queue
                        clump_queue.push(clump_hex);
                    }

                }  // if this was a valid map hex

            }  // repeat until all surrounding hexes are added

        } // if counter hadn't expired

    }  // when the queue is empty, quit

}




void
BattleCreateImp::CreateTerrainScattering(BattleMap * map, const HexCord& centre_hex, TerrainType terrain_type, int size, int density) {

HexCord MapSize = map->getSize();
int width = MapSize.x();
int height = MapSize.y();

int xoffset, yoffset;
HexCord hexpos;
int current_terrain;

    // repeat up to desired size
    for (int n=0; n<size; n++) {

        // repeat up to size * density
        for(int f=0; f< (n*density); f++) {

            xoffset = CRandom::get(n) - (n/2);
            yoffset = CRandom::get(n) - (n/2);
            hexpos.x(centre_hex.x() + xoffset);
            hexpos.y(centre_hex.y() + yoffset);

            // make sure our coordinate is on the map
            if(hexpos.x() >= 0 && hexpos.x() < width && hexpos.y() >= 0 && hexpos.y() < height) {

                BattleTerrainHex& maphex = map->get(hexpos);
                current_terrain = maphex.d_terrainType;

                // if this hex is clear
                if(current_terrain == GT_None || current_terrain == parameters->FirstTerrainType || current_terrain == parameters->SecondTerrainType)
                    maphex.d_terrainType = terrain_type;
            }

        }  // repeat up to density * size

    }  // repeat up to max size

}







/*
Uses FractalMap class to create a heightfield, then reads values into BattleMap
Every other line will appear shifted vertically due to hex system - but should not be noticable
*/

void
BattleCreateImp::CreateHeightMap(BattleMap * map) {

    // get the map dimensions
    const HexCord & hexSize = map->getSize();
    int size_x = hexSize.x();
    int size_y = hexSize.y();

    // create a fractal map of size (x,y)
    FractalMap heightfield(size_x,size_y, &TerrainHeightFieldParameters[parameters->HeightType]);
    // resample the heights to go from (0...resample_range)
    heightfield.ResampleHeightsRelative();
    heightfield.SmoothHeights();

    // loop through battlemap, setting height values
    int x,y;
    HexCord coord;
    // set the minimum & maximum heights of battlefield to opposite extremes
    MinimumHeight = BattleTerrainHex::MaxHeight;
    MaximumHeight = 0;

    for(y=0; y<size_y; y++) {

        for(x=0; x<size_x; x++) {

            coord.x(x);
            coord.y(y);

            BattleTerrainHex & hex = map->get(coord);
            int height_val = heightfield.GetData(x,y);
            if(height_val > BattleTerrainHex::MaxHeight) height_val = BattleTerrainHex::MaxHeight;
            if(height_val < 0) height_val = 0;
            hex.d_height = height_val;
            // set min & max variables
            if(hex.d_height < MinimumHeight) MinimumHeight = hex.d_height;
            if(hex.d_height > MaximumHeight) MaximumHeight = hex.d_height;
        }
    }

}




void
BattleCreateImp::SetAllHeights(BattleMap * map, int hexheight) {

if(hexheight < 0) hexheight = 0;
if(hexheight > BattleTerrainHex::MaxHeight) hexheight = BattleTerrainHex::MaxHeight;

HexCord MapSize = map->getSize();
int width = MapSize.x();
int height = MapSize.y();
HexCord hex;

        for(int y=0; y<height; y++) {

            for(int x=0; x<width; x++) {

                hex.x(x);
                hex.y(y);

                BattleTerrainHex& maphex = map->get(hex);
                maphex.d_height = hexheight;
            }
        }
}


/*
Alters hex heights by the given ammount
*/

void
BattleCreateImp::AlterHexHeight(BattleMap * map, const HexCord& hex, int ammount) {

    BattleTerrainHex & maphex = map->get(hex);
    if((maphex.d_height + ammount) < 0) { maphex.d_height = 0; return; }
    if((maphex.d_height + ammount) > BattleTerrainHex::MaxHeight) { maphex.d_height = BattleTerrainHex::MaxHeight; return; }
    else maphex.d_height += ammount;

}





void
BattleCreateImp::AlterHexAreaHeight(BattleMap * map, const HexCord& centre_hex, int size, int spread_chance, int ammount) {

    ClumpItem clump_hex;
    HexCord move_hex;
//    int current_terrain;
    int current_counter;

    // first clump in queue is at the centre pos
    clump_hex.hexpos.x(centre_hex.x());
    clump_hex.hexpos.y(centre_hex.y());
    clump_hex.counter = 1;

    // set the terrain at the initial hex
    AlterHexHeight(map, clump_hex.hexpos, ammount);

    // initialize the queue
    ClumpQueue clump_queue;
    clump_queue.push(clump_hex);

    // repeat until queue is empty
    while(! clump_queue.empty() ) {

        // get the first item in queue
        clump_hex = clump_queue.front();
        // remove this clump from queue
        clump_queue.pop();

        // make sure the counter hasn't reached the maximum clump size
        current_counter = clump_hex.counter;
        if(current_counter < size) {

            // add all surrounding hexes if clear
            for(int dir=1; dir < 7; dir++) {

                // if on map, then add
                if(map->moveHex(clump_hex.hexpos, static_cast <HexCord::HexDirection> (dir), move_hex)) {

                    // decay height modifier
                    if(ammount < -1) ammount++;
                    if(ammount > 1) ammount--;
                    // set the height here
                    AlterHexHeight(map, move_hex, ammount);
                    // fill out the new clump structure
                    clump_hex.hexpos = move_hex;
                    // if random chance is within range, then this hex is added without an increment to the counter
                    if(CRandom::get(100) < spread_chance) clump_hex.counter = current_counter;
                    // otherwise the counter is increased as normal
                    else clump_hex.counter = current_counter+1;
                    // push in at end of queue
                    clump_queue.push(clump_hex);

                }  // if this was a valid map hex

            }  // repeat until all surrounding hexes are added

        } // if counter hadn't expired

    }  // when the queue is empty, quit

}










void
BattleCreateImp::AlterHexClumpHeight(
        BattleMap * map,
        const HexCord& centre_hex,
        int size, int density, int ammount) {


    ClumpItem clump_hex;
    HexCord move_hex;
//    int current_terrain;
    int current_counter;

    // first clump in queue is at the centre pos
    clump_hex.hexpos.x(centre_hex.x());
    clump_hex.hexpos.y(centre_hex.y());
    clump_hex.counter = 1;

    // set the terrain at the initial hex
    AlterHexHeight(map, clump_hex.hexpos, ammount);

    // initialize the queue
    ClumpQueue clump_queue;
    clump_queue.push(clump_hex);

    // repeat until queue is empty
    while(! clump_queue.empty() ) {

        // get the first item in queue
        clump_hex = clump_queue.front();
        // remove this clump from queue
        clump_queue.pop();

        // make sure the counter hasn't reached the maximum clump size
        current_counter = clump_hex.counter;
        if(current_counter < size) {

            // add random surrounding hexes, up to counter * density & set their terrain types
            for(int n=0; n < density; n++) {

                // choose a random direction
                // int rand_dir = Random(1,7);
                HexCord::HexDirection rand_dir = randomDirection();
                // if on map, then add
                if(map->moveHex(clump_hex.hexpos, rand_dir, move_hex)) {
                    // decay height modifier
                    if(ammount < -1) ammount++;
                    if(ammount > 1) ammount--;
                    // set the height here
                    AlterHexHeight(map, clump_hex.hexpos, ammount);
                    // fill out the new clump structure
                    clump_hex.hexpos = move_hex;
                    clump_hex.counter = current_counter+1;
                    // push in at end of queue
                    clump_queue.push(clump_hex);

                }  // if this was a valid map hex

            }  // repeat until the required density

        } // if counter hadn't expired

    }  // when the queue is empty, quit

}









/*
Makes the primary & secondary basic terrain
Primary terrain initially occupies the whole map
Secondary & Tertiary terrains are added to occupy a percentage of the map after
*/

void
BattleCreateImp:: CreateBasicTerrain(BattleMap * map, const TerrainTable & terrain) {

    // get the size of the map
    const HexCord & mapsize = map->getSize();
    int width = mapsize.x();    // -1;
    int height = mapsize.y();   // -1;

    // set all hexes to be primary type
//    TerrainType current_type;
    HexCord tempcoord;

    for(int y=0; y<height; y++) {
        for(int x=0; x<width; x++) {

            tempcoord.x(x);
            tempcoord.y(y);

            BattleTerrainHex& hex = map->get(tempcoord);
            hex.d_terrainType = parameters->FirstTerrainType;
        }
    }

   int rand_size;
   int rand_density;
   int rand_x;
   int rand_y;
   float average_size;
   int num_passes;
   int f;


    // set a percentage of hexes to be second type
   if(parameters->PercentageSecondTerrain > 0) {

      average_size = (width + height) / 2;
      num_passes = ((average_size / 100.0) * parameters->PercentageSecondTerrain);

      for(int f=0; f < num_passes; f++) {

         rand_x = CRandom::get(width);
         rand_y = CRandom::get(height);
         tempcoord.x(rand_x);
         tempcoord.y(rand_y);
//       rand_size = CRandom::get(4,8);
         rand_size = CRandom::get(4,7);
//       rand_density = CRandom::get(2,6);
         rand_density = CRandom::get(2,5);

         CreateTerrainClump(map, tempcoord, parameters->SecondTerrainType, rand_size, rand_density);
      }
   }

    // set a percentage of hexes to be third type
    if(parameters->PercentageThirdTerrain > 0) {

      average_size = (width + height) / 2;
      num_passes = ((average_size / 100.0) * parameters->PercentageThirdTerrain);

      for(f=0; f < num_passes; f++) {

         rand_x = CRandom::get(width);
         rand_y = CRandom::get(height);
         tempcoord.x(rand_x);
         tempcoord.y(rand_y);
//       rand_size = CRandom::get(4,8);
         rand_size = CRandom::get(4,7);
//       rand_density = CRandom::get(2,6);
         rand_density = CRandom::get(2,5);

         CreateTerrainClump(map, tempcoord, parameters->ThirdTerrainType, rand_size, rand_density);
      }
    }

    // set a percentage of hexes to be fourth type
    if(parameters->PercentageFourthTerrain > 0) {

      average_size = (width + height) / 2;
      num_passes = ((average_size / 100.0) * parameters->PercentageFourthTerrain);

      for(f=0; f < num_passes; f++) {

         rand_x = CRandom::get(width);
         rand_y = CRandom::get(height);
         tempcoord.x(rand_x);
         tempcoord.y(rand_y);
//       rand_size = CRandom::get(4,8);
         rand_size = CRandom::get(4,7);
//       rand_density = CRandom::get(2,6);
         rand_density = CRandom::get(2,5);

         CreateTerrainClump(map, tempcoord, parameters->FourthTerrainType, rand_size, rand_density);
      }
    }

    // set a percentage of hexes to be fifth type
    if(parameters->PercentageFifthTerrain > 0) {

      average_size = (width + height) / 2;
      num_passes = ((average_size / 100.0) * parameters->PercentageFifthTerrain);

      for(f=0; f < num_passes; f++) {

         rand_x = CRandom::get(width);
         rand_y = CRandom::get(height);
         tempcoord.x(rand_x);
         tempcoord.y(rand_y);
//       rand_size = CRandom::get(4,8);
         rand_size = CRandom::get(4,7);
//       rand_density = CRandom::get(2,6);
         rand_density = CRandom::get(2,5);

         CreateTerrainClump(map, tempcoord, parameters->FifthTerrainType, rand_size, rand_density);
      }
    }


}


void
BattleCreateImp::CreateComplexTerrain(BattleMap * map, const TerrainTable & terrain) {

HexCord hexpos;
hexpos.x(0);
hexpos.y(0);
CreateTerrainClump(map, hexpos, GT_Orchard, 4, 8);

hexpos.x(63);
hexpos.y(63);
CreateTerrainClump(map, hexpos, GT_PloughedField, 4, 8);

}







/*
Creates a number of random lakes
*/

void
BattleCreateImp::CreateLakes(BattleMap * map) {

int f;
int x;
int y;
int size;

    // get the size of the map
    const HexCord & mapsize = map->getSize();
    int width = mapsize.x();    // -1;
    int height = mapsize.y();   // -1;

    HexCord pos;

    for(f=0; f<parameters->NumberOfLakes; f++) {

        x = CRandom::get(width);
        y = CRandom::get(height);
        pos.x(x);
        pos.y(y);
//        size = CRandom::get(parameters->MinimumLakeSize, parameters->MaximumLakeSize);
        size = CRandom::get(parameters->MinimumLakeSize, parameters->MaximumLakeSize-1);

        CreateTerrainArea(map, pos, GT_Water, size, 10);

    }

    // now level all areas of water to MinimumHeight on map
    TerrainType terrain_type;

    for(y=0; y<height; y++) {

        for(x=0; x<width; x++) {

            pos.x(x);
            pos.y(y);

            // get hex at this coord from map
            BattleTerrainHex & maphex = map->get(pos);
            // get the terrain type at this hex
            terrain_type = maphex.d_terrainType;
            // if there is water here, then set the height to zero
            if(terrain_type == GT_Water) maphex.d_height = MinimumHeight;
        }
    }


}




void
BattleCreateImp::ClearCoasts(BattleMap * map) {

HexCord MapSize = map->getSize();
int width = MapSize.x();
int height = MapSize.y();
HexCord hex;

        for(int y=0; y<height; y++) {

            for(int x=0; x<width; x++) {

                hex.x(x);
                hex.y(y);

                BattleTerrainHex& maphex = map->get(hex);
                maphex.d_coast = 0;

            }
        }
}



/*
 * Fill in coast bits
 */

void BattleCreateImp::makeCoasts(BattleMap* map, const TerrainTable& terrain) {
/*
Set coast bits from table
*/

const HexCord& hexSize = map->getSize();

    for(HexCord hex(0,0); hex.y() < hexSize.y(); hex.y(hex.y() + 1)) {

        for(hex.x(0); hex.x() < hexSize.x(); hex.x(hex.x() + 1)) {

            BattleTerrainHex& t = map->get(hex);
            if(!terrain.logical(t.d_terrainType).d_isWater) {

                UBYTE bits = 0;
                UBYTE mask = 1;

                for(HexCord::HexDirection d = HexCord::First; d < HexCord::End; INCREMENT(d), mask <<= 1) {

                    HexCord edgeHex;
                    if(map->moveHex(hex, d, edgeHex)) {

                        BattleTerrainHex& t1 = map->get(edgeHex);
                        if(terrain.logical(t1.d_terrainType).d_isWater) {
                            bits |= mask;
                        }
                    }
                }

            t.d_coast = bits;
            // Force coasts to be 0 height
            if(bits != 0)
            t.d_height = 0;
            }
        }
    }
}



void
BattleCreateImp::SetEdges(BattleMap * map, const HexCord& hex, EdgeInfo edge_info) {

    BattleTerrainHex& maphex = map->get(hex);

    if(edge_info.d_edges != 0) {

        maphex.d_edge.d_type = edge_info.d_type;
        maphex.d_edge.d_edges = edge_info.d_edges;

    }

    else {

        maphex.d_edge.d_type = 0;
        maphex.d_edge.d_edges = 0;
    }

}


void
BattleCreateImp::ClearAllEdges(BattleMap * map) {

HexCord MapSize = map->getSize();
int width = MapSize.x();
int height = MapSize.y();
HexCord hex;

        for(int y=0; y<height; y++) {

            for(int x=0; x<width; x++) {

                hex.x(x);
                hex.y(y);

                BattleTerrainHex& maphex = map->get(hex);
                maphex.d_edge.d_type = 0;
                maphex.d_edge.d_edges = 0;
            }
        }
}


/*
Creates a number of light to dense wooded patches on map
*/

void
BattleCreateImp::CreateWoods(BattleMap * map, const BuildingTable* buildingTable, BuildingList* buildings) {

    // get the size of the map
    const HexCord & mapsize = map->getSize();
    int width = mapsize.x();    // -1;
    int height = mapsize.y();   // -1;

    int size, density;
    int rand_x, rand_y, count;
    HexCord hexpos;

    /* Create Light Woods */

    for(count=0; count < parameters->NumberOfLightWoods; count++) {
        rand_x = CRandom::get(width);
        rand_y = CRandom::get(height);
        hexpos.x(rand_x); hexpos.y(rand_y);
//        size = CRandom::get(parameters->MinimumWoodSize, parameters->MaximumWoodSize);
        size = CRandom::get(parameters->MinimumWoodSize, parameters->MaximumWoodSize-1);
//        density = CRandom::get(parameters->MinimumWoodDensity, parameters->MaximumWoodDensity);
        density = CRandom::get(parameters->MinimumWoodDensity, parameters->MaximumWoodDensity-1);
        CreateTerrainClump(map, hexpos, GT_LightWood, size, density, 1, BattleBuildings::LIGHT_WOOD_SMALL, BattleBuildings::LIGHT_WOOD_LARGE, buildingTable, buildings);
//        CreateTerrainClump(map, hexpos, GT_Grass, size, density, 1, BattleBuildings::LIGHT_WOOD_SMALL, BattleBuildings::LIGHT_WOOD_LARGE, buildingTable, buildings);
    }

    /* CreateDense Woods */

    for(count=0; count < parameters->NumberOfDenseWoods; count++) {
        rand_x = CRandom::get(width);
        rand_y = CRandom::get(height);
        hexpos.x(rand_x); hexpos.y(rand_y);
//        size = CRandom::get(parameters->MinimumWoodSize, parameters->MaximumWoodSize);
        size = CRandom::get(parameters->MinimumWoodSize, parameters->MaximumWoodSize-1);
//        density = CRandom::get(parameters->MinimumWoodDensity, parameters->MaximumWoodDensity);
        density = CRandom::get(parameters->MinimumWoodDensity, parameters->MaximumWoodDensity-1);
        CreateTerrainClump(map, hexpos, GT_DenseWood, size, density, 1, BattleBuildings::DENSE_WOOD_SMALL, BattleBuildings::DENSE_WOOD_LARGE, buildingTable, buildings);
    }

}





/*
Finds two random hexes, on different edge of the battle map
NOTE : it may be nicer to allow hexes to occur on the same edge
*/

void
BattleCreateImp::SetRandomEdges(BattleMap * map, HexCord * A, HexCord * B) {

    // get the size of the map
    const HexCord & mapsize = map->getSize();
    int width = mapsize.x();    // -1;
    int height = mapsize.y();   // -1;

    // pick a random edge for hex A
    int rand_edgeA = CRandom::get(4);
    ASSERT(rand_edgeA < 4);
    // pick a random point on the edge
    switch(rand_edgeA) {
        case 0 : { A->x(0);             A->y(CRandom::get(height));   break; }  // left edge
        case 1 : { A->x(CRandom::get(width)); A->y(height-1);         break; }  // bottom edge
        case 2 : { A->x(width-1);       A->y(CRandom::get(height));   break; }  // right edge
        case 3 : { A->x(CRandom::get(width)); A->y(0);                break; }  // top edge
    }
    // pick a random edge from hex B
    int rand_edgeB = CRandom::get(4);
    ASSERT(rand_edgeB < 4);

    // make hex B  is on a different edge from hex A
    // NOTE : it may be nicer to allow hexes to occur on the same edge
    while(rand_edgeA == rand_edgeB) { rand_edgeB = CRandom::get(4); }
    ASSERT(rand_edgeB < 4);

    // pick a random point on the edge
    switch(rand_edgeB) {
        case 0 : { B->x(0);             B->y(CRandom::get(height));   break; }  // left edge
        case 1 : { B->x(CRandom::get(width)); B->y(height-1);         break; }  // bottom edge
        case 2 : { B->x(width-1);       B->y(CRandom::get(height));   break; }  // right edge
        case 3 : { B->x(CRandom::get(width)); B->y(0);                break; }  // top edge
    }

}





/*
Checks a path to see wether, at any point, it crosses a hex containing water.  Returns TRUE is it does.
*/

int
BattleCreateImp::DoesPathCrossWater(BattleMap * map, const TerrainTable & terrain, SList<PathPoint> * Path) {

    // terrain type at hex
    TerrainType terrain_type;
    // logical terrain descriptor
    LTerrainTable::Info logical_type;

    PathPoint * tmp_path = Path->first();

    // traverse the path
    while(tmp_path != NULL) {

        // get hex at this coord from map
        BattleTerrainHex & maphex = map->get(tmp_path->hex);
        // get the terrain type at this hex
        terrain_type = maphex.d_terrainType;

        // if water is found at this hex, return TRUE
        logical_type = terrain.logical(terrain_type);
        if(logical_type.d_isWater) return 1;

        tmp_path = Path->next();
    }

    // FALSE, no water found along this path
    return 0;
}



/*
Checks to see wether a path crosses another specified path_1 type, at any point
*/

int
BattleCreateImp::DoesPathCrossPathType(BattleMap * map, SList<PathPoint> * Path, LogicalPathEnum path_type, int path_number) {

if(path_number != 1 && path_number != 2) return 0;

    PathType path;

    PathPoint * tmp_path = Path->first();

    // traverse the patrh
    while(tmp_path != NULL) {

        // get hex at this coord from map
        BattleTerrainHex & maphex = map->get(tmp_path->hex);
        // get the path_1 type (always rivers) at this hex
        if(path_number == 1) path = maphex.d_path1.d_type;
        if(path_number == 2) path = maphex.d_path2.d_type;

        if(path == path_type) return 1;

        tmp_path = Path->next();
    }

return 0;
}






/*

Travels along a the given path & check to see wether it crosses the given 'path_type' at any point.
If the entry & exit points are the same, then continue & return FALSE at end
Otherwise return TRUE immediately, indicating that the paths cross at diffeerent angles
*/

int
BattleCreateImp::DoesPathCrossPathTypeAtDifferentAngle(BattleMap * map, SList<PathPoint> * Path, LogicalPathEnum path_type) {

    PathType path1;
    PathType path2;

    PathPoint * tmp_path = Path->first();

    // edge bit fields
    UBYTE prev_bits;
    UBYTE next_bits;
//    UBYTE hex_edgebits;
    UBYTE path_edgebits;

    // traverse the patrh
    while(tmp_path != NULL) {

        // get hex at this coord from map
        BattleTerrainHex & maphex = map->get(tmp_path->hex);
        // get the path_1 type (always rivers) at this hex
        path1 = maphex.d_path1.d_type;
        // get the path_2 type (roads, tracks, railways, etc)
        path2 = maphex.d_path2.d_type;

        // path one at this hex is of the specified path_type
        if(path1 == path_type) {
            // create the bit field for the current path
            if(tmp_path->dir_prev == HexCord::Stationary) prev_bits = 0;
            else prev_bits = 1 << tmp_path->dir_prev;
            if(tmp_path->dir_next == HexCord::Stationary) next_bits = 0;
            else next_bits = 1 << tmp_path->dir_next;
            path_edgebits = prev_bits | next_bits;
            // compare pathbits with mexhex edgebits, return TRUE if not same
            if(maphex.d_path1.d_edges != path_edgebits) return 1;
        }

        // path two at this hex is of the specified path_type
        if(path2 == path_type) {
            // create the bit field for the current path
            if(tmp_path->dir_prev == HexCord::Stationary) prev_bits = 0;
            else prev_bits = 1 << tmp_path->dir_prev;
            if(tmp_path->dir_next == HexCord::Stationary) next_bits = 0;
            else next_bits = 1 << tmp_path->dir_next;
            path_edgebits = prev_bits | next_bits;
            // compare pathbits with mexhex edgebits, return TRUE if not same
            if(maphex.d_path2.d_edges != path_edgebits) return 1;
        }

        // go to next point in path
        tmp_path = Path->next();
    }

// return FALSE since either no paths of the type were found
// or they were & used the same edges
return 0;
}




/*
Checks a path to see wether is crosses a river continually
The input value 'frequency' is how far apart (in hexes) two river crossings should occur, to be deemed too frequent
A return value of TRUE indicates that the path crosses two rivers within the frequency specified
*/

int
BattleCreateImp::DoesPathCrossRiversConstantly(BattleMap * map, SList<PathPoint> * Path, int frequency) {

    // path descriptor
    PathType path_type;
    // holds how far apart (in hexes) two river crossings are
    int river_crossed_counter = 0;

    PathPoint * tmp_path = Path->first();

    // traverse the path
    while(tmp_path != NULL) {

        // get hex at this coord from map
        BattleTerrainHex & maphex = map->get(tmp_path->hex);
        // get the path_1 type (always rivers) at this hex
        path_type = maphex.d_path1.d_type;

        // if we've started counting, then increment the distance for each hex
        if(river_crossed_counter != 0) river_crossed_counter++;

        // if the path_1 at this hex is a river
        if(path_type == LP_River || path_type == LP_SunkenRiver || path_type == LP_Stream || path_type == LP_SunkenStream) {
            // if the counter has already been started
            if(river_crossed_counter != 0) {
                // if the two crossings are too close together, return TRUE
                if(river_crossed_counter <= frequency) return 1;
                // otherwise start counting again, from this crossing
                else river_crossed_counter = 1;
            }
            // otherwise this is the first river crossed, so start the counter
            else river_crossed_counter = 1;
        }

        tmp_path = Path->next();
    }

    // FALSE, no river crossings were within the frequency parameter
    return 0;
}




void
BattleCreateImp::ClearAllPaths(BattleMap * map) {

HexCord MapSize = map->getSize();
int width = MapSize.x();
int height = MapSize.y();
HexCord hex;

        for(int y=0; y<height; y++) {

            for(int x=0; x<width; x++) {

                hex.x(x);
                hex.y(y);

                BattleTerrainHex& maphex = map->get(hex);
                maphex.d_path1.d_type = PathTable::None;
                maphex.d_path1.d_edges = 0;
                maphex.d_path2.d_type = PathTable::None;
                maphex.d_path2.d_edges = 0;
            }
        }
}


/*
Connects paths 1 & 2 separately within a given hex
*/
/*
void
BattleCreateImp::ResolvePathConnections(BattleMap * Map, HexCord hex) {

    UBYTE edge_bits;
    UBYTE prev_bits;
    UBYTE next_bits;

        path1_type = maphex.d_path1.d_type;

    // get hex at this coord from map
    BattleTerrainHex & maphex = map->get(hex);

                // if coming from edge of map, don't bother combining
                if(tmp_path->dir_prev == HexCord::Stationary) return;
                else prev_bits = 1 << tmp_path->dir_prev;
                // combine hex bit pattern, & river hex-entry bit pattern, to join rivers
                maphex.d_path1.d_edges |= prev_bits;
                return;
*/


/*
Creates paths for each river on map, and combines them with existing rivers
*/

void
BattleCreateImp::CreateRivers(BattleData * batdata, BattleMap * map, const TerrainTable & terrain) {

    PathFinder pathfinder(batdata);

    HexCord start;
    HexCord end;

    // list for holding pathpoints
    SList<PathPoint> * Path;

    for(int f=0; f < parameters->NumberOfRivers; f++) {

    // find path
    SetRandomEdges(map, &start, &end);

    Path = new SList<PathPoint>;
    pathfinder.FindPath(start, end, PATHFLAG_AVOIDUPHILL, Path);

    // apply path to map
   LogicalPathEnum path_type;
// int r = CRandom::get(0,100);
   int r = CRandom::get(100);
   int c = parameters->ChanceRiversSunken;
   if(r < c) path_type = LP_SunkenRiver;
   else path_type = LP_River;

    ConstructRiverPath(map, terrain, Path, path_type);

    delete(Path);

    }

}




/*
Creates paths for each stream on map, and combines them with existing rivers / streams
*/

void
BattleCreateImp::CreateStreams(BattleData * batdata, BattleMap * map, const TerrainTable & terrain) {

    PathFinder pathfinder(batdata);

    HexCord start;
    HexCord end;

    // list for holding pathpoints
    SList<PathPoint> * Path;

    for(int f=0; f < parameters->NumberOfStreams; f++) {

    // find path
    SetRandomEdges(map, &start, &end);

    Path = new SList<PathPoint>;;
    pathfinder.FindPath(start, end, PATHFLAG_AVOIDUPHILL, Path);

    // apply path to map
   LogicalPathEnum path_type;
   int r = CRandom::get(0,100);
   int c = parameters->ChanceStreamsSunken;
   if(r < c) path_type = LP_SunkenStream;
   else path_type = LP_Stream;

    ConstructRiverPath(map, terrain, Path, path_type);

    delete(Path);

    }

}




/*
Follows a pathlist, setting terrain to be river / stream & processing the following cases :

    1) rivers may not cross - instead they will join
    2) rivers may not exits within water, they will start terminate upon entering a body of water

Uses only path_1, since at this stage only rivers have been defined on map
*/

void
BattleCreateImp::ConstructRiverPath(BattleMap * map, const TerrainTable & terrain, SList<PathPoint> * RiverPath, LogicalPathEnum RiverType) {


    PathPoint * tmp_path = RiverPath->first();

    UBYTE edge_bits;
    UBYTE prev_bits;
    UBYTE next_bits;

    // terrain type at hex
    TerrainType terrain_type;
    // logical terrain descriptor
    LTerrainTable::Info logical_type;

    // path type at hex (only path_1 need be used)
    PathType path1_type;

    // traverse the path
    while(tmp_path != NULL) {

        // get hex at this coord from map
        BattleTerrainHex & maphex = map->get(tmp_path->hex);

        // get the terrain type at this hex
        terrain_type = maphex.d_terrainType;
        // get the path type defined in this hex
        path1_type = maphex.d_path1.d_type;


        /* if a river already exists in this hex, make the river/stream path join it & terminate here */
        if(path1_type == LP_River || path1_type == LP_SunkenRiver) {

            // if we are making a river path, join the bit patterns
            if(RiverType == LP_River || RiverType == LP_SunkenRiver) {
                // if coming from edge of map, don't bother combining
                if(tmp_path->dir_prev == HexCord::Stationary) return;
                else prev_bits = 1 << tmp_path->dir_prev;
                // combine hex bit pattern, & river hex-entry bit pattern, to join rivers
                maphex.d_path1.d_edges |= prev_bits;
                return;
            }
            // if we are making a stream, the entry bit patterns must be put in path2 & path2 set as a stream
            if(RiverType == LP_Stream || RiverType == LP_SunkenStream) {
                // if coming from edge of map, don't bother combining
                if(tmp_path->dir_prev == HexCord::Stationary) return;
                else prev_bits = 1 << tmp_path->dir_prev;
                maphex.d_path2.d_type = RiverType;
                maphex.d_path2.d_edges = prev_bits;
                return;
            }

        }

        /* if a stream already exists in this hex we must be in the stream generating phase, so make this stream join as normal */
        if(path1_type == LP_Stream || path1_type == LP_SunkenStream) {
            // if coming from edge of map, don't bother combining
            if(tmp_path->dir_prev == HexCord::Stationary) return;
            else prev_bits = 1 << tmp_path->dir_prev;
            // combine hex bit pattern, & river hex-entry bit pattern, to join rivers
            maphex.d_path1.d_edges |= prev_bits;
            return;
        }



        /* if this hex is a coast / body of water, then finish path here */
        logical_type = terrain.logical(terrain_type);
        if(logical_type.d_isWater) {
            return;
        }  // do nothing

        /* if no river / water, then construct path through hex as normal */
        else {

            // set path1 type to the specified river type
            maphex.d_path1.d_type = RiverType;

            // if direction is 'stationary' set the shift index as 0
            if(tmp_path->dir_prev == HexCord::Stationary)
                prev_bits = 0;
            else prev_bits = 1 << tmp_path->dir_prev;

            // if direction is 'stationary' set the shift index as 0
            if(tmp_path->dir_next == HexCord::Stationary)
                next_bits = 0;
            else next_bits = 1 << tmp_path->dir_next;

            // combine bits to form edgemap
            edge_bits = prev_bits | next_bits;
            // if this path is zero length, return here
            if(edge_bits == 0) return;
            maphex.d_path1.d_edges = edge_bits;

        }


    tmp_path = RiverPath->next();

    }  // traverse pathpoints list

}




/*
Creates paths for each road on map, and combines them with existing roads & rivers
*/

void
BattleCreateImp::CreateRoads(BattleData * batdata, BattleMap * map, const TerrainTable & terrain) {

    PathFinder pathfinder(batdata);

    HexCord start;
    HexCord end;

    // list for holding pathpoints
    SList<PathPoint> * Path;

    int num_roads = 10;
    int crosses_water = 0;
    int crosses_rivers = 0;
    int crosses_rivers_max_frequency = 3;
    int crosses_bridges = 0;
    int crosses_river_stream_junction =0;

    for(int f=0; f < parameters->NumberOfRoads; f++) {

    int num_attempts = 5;

    do {
        // find path
        crosses_water = 0;
        SetRandomEdges(map, &start, &end);
        Path = new SList<PathPoint>;
        pathfinder.FindPath(start, end, PATHFLAG_AVOIDUPHILL | PATHFLAG_AVOIDDOWNHILL, Path);

        // see wether this path crosses any water on map
        crosses_water = DoesPathCrossWater(map, terrain, Path);
        // see wether this path crosses rivers too frequently
        crosses_rivers = DoesPathCrossRiversConstantly(map, Path, crosses_rivers_max_frequency);
        // see wether this path crosses any already defined bridges (NOTE: it should allow aligned at the same angle as the path is trying to cross at ! )
        crosses_bridges = DoesPathCrossPathTypeAtDifferentAngle(map, Path, LP_RoadBridge);
        // see wether this path crosses a river & stream junction (in which case the stream section will appear in path2)
        crosses_river_stream_junction = (DoesPathCrossPathType(map, Path, LP_Stream, 2) | DoesPathCrossPathType(map, Path, LP_SunkenStream, 2) );

    // repeat creating random paths until one passes the criteria
    num_attempts--;

    } while( num_attempts > 0 && (crosses_water || crosses_rivers || crosses_bridges || crosses_river_stream_junction) );

    // apply path to map
   LogicalPathEnum path_type;
// int r = CRandom::get(0,100);
   int r = CRandom::get(100);
   int c = parameters->ChanceRoadsSunken;
   if(r < c) path_type = LP_SunkenRoad;
   else path_type = LP_Road;

   if(num_attempts > 0) ConstructRoadPath(map, terrain, Path, path_type);

    delete(Path);

    }

}





/*
Creates paths for each track on map, and combines them with existing roads & rivers & streams
*/

void
BattleCreateImp::CreateTracks(BattleData * batdata, BattleMap * map, const TerrainTable & terrain) {

    PathFinder pathfinder(batdata);

    HexCord start;
    HexCord end;

    // list for holding pathpoints
    SList<PathPoint> * Path;

    int num_roads = 10;
    int crosses_water = 0;
    int crosses_rivers = 0;
    int crosses_rivers_max_frequency = 3;
    int crosses_road_bridges = 0;
    int crosses_track_bridges = 0;
    int crosses_river_stream_junction = 0;
    int crosses_road_track_junction = 0;

    for(int f=0; f < parameters->NumberOfRoads; f++) {

    int num_attempts = 5;

    do {
        // find path
        crosses_water = 0;
        SetRandomEdges(map, &start, &end);
        Path = new SList<PathPoint>;
        pathfinder.FindPath(start, end, PATHFLAG_AVOIDUPHILL | PATHFLAG_AVOIDDOWNHILL, Path);

        // see wether this path crosses any water on map
        crosses_water = DoesPathCrossWater(map, terrain, Path);
        // see wether this path crosses rivers too frequently
        crosses_rivers = DoesPathCrossRiversConstantly(map, Path, crosses_rivers_max_frequency);
        // see wether this path crosses any already defined bridges (NOTE: it should allow aligned at the same angle as the path is trying to cross at ! )
        crosses_road_bridges = DoesPathCrossPathTypeAtDifferentAngle(map, Path, LP_RoadBridge);
        // see wether this path crosses any already defined bridges (NOTE: it should allow aligned at the same angle as the path is trying to cross at ! )
        crosses_track_bridges = DoesPathCrossPathTypeAtDifferentAngle(map, Path, LP_TrackBridge);
        // see wether this path crosses a river & stream junction (in which case the stream section will appear in path2)
        crosses_river_stream_junction = (DoesPathCrossPathType(map, Path, LP_Stream, 2) | DoesPathCrossPathType(map, Path, LP_SunkenStream, 2) );
        // see wether this path crosses a road & track junction (in which case the track section will appear in path2)
        crosses_road_track_junction = (DoesPathCrossPathType(map, Path, LP_Track, 1) | DoesPathCrossPathType(map, Path, LP_SunkenTrack, 1) );

    // repeat creating random paths until one passes the criteria
    num_attempts--;

    } while( (num_attempts>0) && crosses_water || crosses_rivers || crosses_road_bridges || crosses_track_bridges || crosses_river_stream_junction || crosses_road_track_junction );

    // apply path to map
   LogicalPathEnum path_type;
// int r = CRandom::get(0,100);
   int r = CRandom::get(100);
   int c = parameters->ChanceTracksSunken;
   if(r < c) path_type = LP_SunkenTrack;
   else path_type = LP_Track;

    ConstructRoadPath(map, terrain, Path, path_type);

    delete(Path);

    }

}





/*
Follows a pathlist, setting terrain to be road & processing the following cases :

    1) roads have a percentage chance of crossing rather than forming junctions
    2) roads will either form a bridge or a ford whien crossing a river path
    3) roads may not exits within water - a path crossing water will never be passed to this function

Path_2 is used for roads & tracks, since path_1 has already been defined for rivers
*/

void
BattleCreateImp::ConstructRoadPath(BattleMap * map, const TerrainTable & terrain, SList<PathPoint> * RoadPath, LogicalPathEnum RoadType) {


    PathPoint * tmp_path = RoadPath->first();

    UBYTE edge_bits;
    UBYTE prev_bits;
    UBYTE next_bits;

    // terrain type at hex
    TerrainType terrain_type;
    // logical terrain descriptor
//    LTerrainTable::Info logical_type;

    // path types at hex (path1 = rivers, path2 = roads / tracks)
    PathType path1_type;
    PathType path2_type;

    // traverse the path
    while(tmp_path != NULL) {

        // get hex at this coord from map
        BattleTerrainHex & maphex = map->get(tmp_path->hex);

        // get the terrain type at this hex
        terrain_type = maphex.d_terrainType;
        // get the path type defined in this hex
        path1_type = maphex.d_path1.d_type;
        path2_type = maphex.d_path2.d_type;


        /*
        If a River exists in this hex, create either
        a) a bridge
        b) a ford
        */

        if(path1_type == LP_River || path1_type == LP_SunkenRiver || path1_type == LP_Stream || path1_type == LP_SunkenStream) { //PathTable::River) {
            // create bit fields for paths
            if(tmp_path->dir_prev == HexCord::Stationary) prev_bits = 0;
            else prev_bits = 1 << tmp_path->dir_prev;
            if(tmp_path->dir_next == HexCord::Stationary) next_bits = 0;
            else next_bits = 1 << tmp_path->dir_next;
            // combine bits
            edge_bits = prev_bits | next_bits;
            // if this path is zero length, return here
            if(edge_bits == 0) return;
            maphex.d_path2.d_edges |= edge_bits;
            // if we are making a road path, create the appropriate bridge & process settlement creation
            if(RoadType == LP_Road || RoadType == LP_SunkenRoad) {
                maphex.d_path2.d_type = LP_RoadBridge;
                // maybe create a settlement point here
                if(CRandom::get(100) < parameters->ChanceSettlementAtBridge) AddSettlementSeedingPoint(tmp_path->hex, Normal);
            }
            // if we are making a track path, create the appropriate bridge & process settlement creation
            else if(RoadType == LP_Track || RoadType == LP_SunkenTrack) {
                maphex.d_path2.d_type = LP_TrackBridge;
                // maybe create a settlement point here
                if(CRandom::get(100) < parameters->ChanceSettlementAtBridge) AddSettlementSeedingPoint(tmp_path->hex, Normal);
            }

            // add this to our key-points list
            else if(d_keyTerrainList) {
                KeyTerrainPoint * p = new KeyTerrainPoint;
                p->hex(tmp_path->hex);
                p->type(KeyTerrainPoint::Bridge);
                if(!d_keyTerrainList->add(p)) delete p;
            }

        }  /* end of road & river intersection */


        /*
        If a Road, Path or Track exists in this hex, create either
        a) a crossroads
        b) a junction
        */

        /* If we are meeting a Road */

        else if(path2_type == LP_Road || path2_type == LP_SunkenRoad) {

            /* Form a Crossroads */

            if(CRandom::get(100) > parameters->ChanceRoadsJoin) {
                // create bit fields for paths
                if(tmp_path->dir_prev == HexCord::Stationary) prev_bits = 0;
                else prev_bits = 1 << tmp_path->dir_prev;
                if(tmp_path->dir_next == HexCord::Stationary) next_bits = 0;
                else next_bits = 1 << tmp_path->dir_next;
                // combine bits
                edge_bits = prev_bits | next_bits;
                // if this path is zero length then return now
                if(edge_bits == 0) return;

                // if we're making a road cross a road, used path_1 & combine bit patterns
                if(RoadType == LP_Road || RoadType == LP_SunkenRoad) maphex.d_path2.d_edges |= edge_bits;
                // if we're making a track cross a road, we must use path_1 fo the crossing (it will be clear since a river will have been already detected)
                if(RoadType == LP_Track || RoadType == LP_SunkenTrack) {
                    maphex.d_path1.d_type = RoadType;
                    maphex.d_path1.d_edges = edge_bits;
                }
                // maybe create a settlement point here
                if(CRandom::get(100) < parameters->ChanceSettlementAtCrossroads) AddSettlementSeedingPoint(tmp_path->hex, Normal);

                // add this to our key-points list
                else if(d_keyTerrainList) {
                    KeyTerrainPoint * p = new KeyTerrainPoint;
                    p->hex(tmp_path->hex);
                    p->type(KeyTerrainPoint::CrossRoads);
                    if(!d_keyTerrainList->add(p)) delete p;
                }

            }

            /* Form a Junction */

            else {
                // create bit fields for paths
                // if this path is coming from edge of map, then return here
                if(tmp_path->dir_prev == HexCord::Stationary) return;
                else prev_bits = 1 << tmp_path->dir_prev;
                // combine bits
                edge_bits = prev_bits;

                // if we're making a road join a road, used path_1 & combine bit patterns
                if(RoadType == LP_Road || RoadType == LP_SunkenRoad) maphex.d_path2.d_edges |= edge_bits;
                // if we're making a track join a road, we must use path_1 fo the crossing (it will be clear since a river will have been already detected)
                if(RoadType == LP_Track || RoadType == LP_SunkenTrack) {
                    maphex.d_path1.d_type = RoadType;
                    maphex.d_path1.d_edges = edge_bits;
                }
                // maybe create a settlement point here
                if(CRandom::get(100) < parameters->ChanceSettlementAtJunction) AddSettlementSeedingPoint(tmp_path->hex, Normal);

                // add this to our key-points list
                else if(d_keyTerrainList) {
                    KeyTerrainPoint * p = new KeyTerrainPoint;
                    p->hex(tmp_path->hex);
                    p->type(KeyTerrainPoint::Junction);
                    if(!d_keyTerrainList->add(p)) delete p;
                }

                // returns since path ends here
                return;
            }

        }  /* End of a path crossing / joining a road path */



        /* If we are meeting a Track, we must be on the Track-making phase */

        else if(path2_type == LP_Track || path2_type == LP_SunkenTrack) {

            /* Form a Crossroads */

            if(CRandom::get(100) > parameters->ChanceRoadsJoin) {
                // create bit fields for paths
                if(tmp_path->dir_prev == HexCord::Stationary) prev_bits = 0;
                else prev_bits = 1 << tmp_path->dir_prev;
                if(tmp_path->dir_next == HexCord::Stationary) next_bits = 0;
                else next_bits = 1 << tmp_path->dir_next;
                // combine bits
                edge_bits = prev_bits | next_bits;
                // if this path is zero length, then return here
                if(edge_bits == 0) return;

                    // set the path_1 edge bits so tracks cross (path 1 is normally used for rivers)
                    maphex.d_path1.d_type = path2_type;
                    maphex.d_path1.d_edges = edge_bits;

                // maybe create a settlement point here
                if(CRandom::get(100) < parameters->ChanceSettlementAtCrossroads) AddSettlementSeedingPoint(tmp_path->hex, Normal);

                // add this to our key-points list
                else if(d_keyTerrainList) {
                    KeyTerrainPoint * p = new KeyTerrainPoint;
                    p->hex(tmp_path->hex);
                    p->type(KeyTerrainPoint::CrossRoads);
                    if(!d_keyTerrainList->add(p)) delete p;
                }
            }

            /* Form a Junction */

            else {
                // create bit fields for paths
                // if this path is coming from edge of map, then return here
                if(tmp_path->dir_prev == HexCord::Stationary) return;
                else prev_bits = 1 << tmp_path->dir_prev;
                // combine bits
                edge_bits = prev_bits;

                // if we're making a track join a track, we must use path_1 fo the crossing (it will be clear since a river will have been already detected)
                    maphex.d_path1.d_type = path2_type;
                    maphex.d_path1.d_edges = edge_bits;

                // maybe create a settlement point here
                if(CRandom::get(100) < parameters->ChanceSettlementAtJunction) AddSettlementSeedingPoint(tmp_path->hex, Normal);

                // add this to our key-points list
                else if(d_keyTerrainList) {
                    KeyTerrainPoint * p = new KeyTerrainPoint;
                    p->hex(tmp_path->hex);
                    p->type(KeyTerrainPoint::Junction);
                    if(!d_keyTerrainList->add(p)) delete p;
                }
                // returns since path ends here
                return;
            }

        }  /* end of road & road intersection */



        /*
        If no River/Stream or Road/Track exists in this hex then create path as normal
        */

        else if(path2_type == 0) {
            // set path as road
            maphex.d_path2.d_type = RoadType;
            // create bit fields for baths
            if(tmp_path->dir_prev == HexCord::Stationary) prev_bits = 0;
            else prev_bits = 1 << tmp_path->dir_prev;
            if(tmp_path->dir_next == HexCord::Stationary) next_bits = 0;
            else next_bits = 1 << tmp_path->dir_next;
            // combine bits
            edge_bits = prev_bits | next_bits;
            // if this path is zero length, then return here
            if(edge_bits == 0) return;
            maphex.d_path2.d_edges = edge_bits;

        }  /* end of normal hex path */


    tmp_path = RoadPath->next();

    }  // traverse pathpoints list

}












/*
Adds a new settlement seeding point onto the map
If within a set distance from any other settlements, they are combined & their size increased
*/

void
BattleCreateImp::AddSettlementSeedingPoint(const HexCord& hexpos, SettlementType type) {

    int new_settlement_size;
    // work out the size of this settlement dependent upon type
//    if(type ==Normal) new_settlement_size = CRandom::get(parameters->MinimumSettlementSize, parameters->MaximumSettlementSize);
    if(type ==Normal) new_settlement_size = CRandom::get(parameters->MinimumSettlementSize, parameters->MaximumSettlementSize-1);
//    if(type == Farm) new_settlement_size = CRandom::get(parameters->MinimumFarmSize, parameters->MaximumFarmSize);
    if(type == Farm) new_settlement_size = CRandom::get(parameters->MinimumFarmSize, parameters->MaximumFarmSize-1);

    // first make sure there are no other settlements within the combining distance
    SeedPoint * settlement_point = SettlementSeedPoints.first();

    while(settlement_point != NULL) {

        // if settlements are too close then combine
        if((abs(settlement_point->hex.x() - hexpos.x()) < parameters->CombineSettlementsDistance) &&
            (abs(settlement_point->hex.y() - hexpos.y()) < parameters->CombineSettlementsDistance)) {

            // the existing settlement has the new settlement's size added to it
            settlement_point->size += new_settlement_size;
            // keep settlement's size within bounds for its type

            // villages & towns
            if(settlement_point->type == Normal)
                if(settlement_point->size > parameters->MaximumSettlementSize) settlement_point->size = parameters->MaximumSettlementSize;
            // farms
            if(settlement_point->type == Farm)
                if(settlement_point->size > parameters->MaximumFarmSize) settlement_point->size = parameters->MaximumFarmSize;

        return; }

    settlement_point = SettlementSeedPoints.next();
    }

    // if no settlements are within distance, add this settlement as normal
    SeedPoint * seed_point = new SeedPoint;
    seed_point->hex = hexpos;
    seed_point->size = new_settlement_size;
    seed_point->type = type;

    SettlementSeedPoints.insert(seed_point);

}



/*
Creates a number of randomly places settlements
These are either solitary buildings, special buildings, or farms dependingupon a percentage chance
The SettlementsSeedingPoints array is scanned, to ensure that none are placed too close to already established settlements
*/
void
BattleCreateImp::CreateRandomSettlements(BattleMap * map, const TerrainTable & terrain) {

HexCord MapSize = map->getSize();
int width = MapSize.x();
int height = MapSize.y();

int rand_x;
int rand_y;
HexCord hexpos;

    for(int f=0; f < parameters->NumberOfRandomSettlements; f++) {

        rand_x = CRandom::get(width);
        rand_y = CRandom::get(height);
        hexpos.x(rand_x);
        hexpos.y(rand_y);

    AddSettlementSeedingPoint(hexpos, Farm);
    }

}





void
BattleCreateImp::CreateSingleSettlement(const BuildingTable * buildingTable, BuildingList * buildings, const HexCord& hex, BuildingListEntry entry) {

    buildings->add(hex, entry);

}



void
BattleCreateImp::CreateSettlements(BattleMap * map, const TerrainTable & terrain, const BuildingTable* buildingTable, BuildingList* buildings) {

    SeedPoint * seed_point = SettlementSeedPoints.first();

    while(seed_point != NULL) {

        if(seed_point->type == Normal) {

            // scatter appropriate settlements around this point
            if(seed_point->size <= parameters->MaximumVillageSize) {
                // add this to our key-points list
                if(d_keyTerrainList) {
                    KeyTerrainPoint * p = new KeyTerrainPoint;
                    p->hex(seed_point->hex);
                    p->type(KeyTerrainPoint::Village);
                    if(!d_keyTerrainList->add(p)) delete p;
                }
            parameters->d_numVillagesCreated++;
                ScatterVillageSettlements(map, terrain, seed_point, buildingTable, buildings); }

            else if(seed_point->size <= parameters->MaximumTownSize) {
                // add this to our key-points list
                if(d_keyTerrainList) {
                    KeyTerrainPoint * p = new KeyTerrainPoint;
                    p->hex(seed_point->hex);
                    p->type(KeyTerrainPoint::Town);
                    if(!d_keyTerrainList->add(p)) delete p;
                }
            parameters->d_numTownsCreated++;
                ScatterTownSettlements(map, terrain, seed_point, buildingTable, buildings); }

        }

        if(seed_point->type == Farm) {
                // add this to our key-points list
                if(d_keyTerrainList) {
                    KeyTerrainPoint * p = new KeyTerrainPoint;
                    p->hex(seed_point->hex);
                    p->type(KeyTerrainPoint::Farm);
                    if(!d_keyTerrainList->add(p)) delete p;
                }
            parameters->d_numFarmsCreated++;
            CreateFarm(map, terrain, seed_point, buildingTable, buildings);
        }


    seed_point = SettlementSeedPoints.next();
    }

}








/*
Scatters village-type settlements around a given seed-point, and sets the hex type to village
*/

void
BattleCreateImp::ScatterVillageSettlements(BattleMap * map, const TerrainTable & terrain, SeedPoint * seed_point, const BuildingTable* buildingTable, BuildingList* buildings) {

   BuildingStatus building_status = BUILDING_NORMAL;
   unsigned int start_time = 0;

   HexCord MapSize = map->getSize();
   int mapwidth = MapSize.x();
   int mapheight = MapSize.y();

   int xoffset;
   int yoffset;
   int x=seed_point->hex.x();
   int y=seed_point->hex.y();
   int size = seed_point->size;
   HexCord hexpos;


    SList <SpecialListNode> SpecialTypesCreated;
    SpecialTypesCreated.reset();

    // increase the distance within which buildings are created
    int index_start = 4;

    for(int spread=1; spread<size; spread++) {

        // create a number of buildings within this range
        for(int density=0; density < (parameters->VillageDensity * spread); density++) {


         /*
         * Debugging aid : creates buildings in varying states
         */
         #ifdef RANDOM_BUILDING_STATUS
//          int r = CRandom::get(0,100);
            int r = CRandom::get(100);
            if(r<25) building_status = BUILDING_NORMAL;
            else if(r<50) {
               building_status = BUILDING_BURNING;
//             start_time = random(0,BUILDING_BURNING_INTERVAL);
            }
            else if(r<75) {
               building_status = BUILDING_SMOULDERING;
//             start_time = random(0,BUILDING_SMOULDERING_INTERVAL);
            }
            else building_status = BUILDING_DESTROYED;
         #endif

            xoffset = CRandom::get(spread) - (spread/2);
            yoffset = CRandom::get(spread) - (spread/2);

            hexpos.x(x+xoffset);
            hexpos.y(y+yoffset);

            // only add a settlement here if this hex is on the map
            if(hexpos.x() >= 0 && hexpos.x() < mapwidth && hexpos.y() >= 0 && hexpos.y() < mapheight) {
                // get the terrain type at this hex
                BattleTerrainHex& maphex = map->get(hexpos);

                    // make sure terrain here is valid for placing settlements
                    if(maphex.d_terrainType == GT_None || maphex.d_terrainType == parameters->FirstTerrainType || maphex.d_terrainType == parameters->SecondTerrainType) {

                        // set this hex to the town texture
                        maphex.d_terrainType = GT_Village;

                        // if there is a path here, then maybe add a roadside cluster
                  int path_index = -1;
                  if(maphex.d_path1.d_type != 0) path_index = GetBridgeOrientation(maphex.d_path1.d_edges);
                  if(path_index == -1 && maphex.d_path2.d_type != 0) path_index = GetBridgeOrientation(maphex.d_path2.d_edges);

                  // if a valid path, & we meet a percentage chance..
                  if((path_index != -1) && (CRandom::get(100)>60) ) {

                            // only add buildings, if there are none already defined for this hex
                            if(buildings->get(hexpos) == NULL) {

                        int bank = 0;
                        if(CRandom::get(100) > 50) bank = 1;

                        int building_type = VILLAGE_ROADSIDE_BANK1 + (bank*9) + path_index;

                        BuildingListEntry entry;
                        entry.status(building_status);
                        entry.effectCount(start_time);
                        entry.index(building_type);

                        buildings->add(hexpos, entry);
                     }
                  }

                        // else add a normal cluster, if no paths here
                        else if((maphex.d_path1.d_type == 0) && (maphex.d_path2.d_type == 0)) {

                            // only add buildings, if there are none already defined for this hex
                            if(buildings->get(hexpos) == NULL) {

                        BuildingListEntry entry;
                        entry.status(building_status);
                        entry.effectCount(start_time);

                                // now add the building
                                int building_type;
                                int special_factor = CRandom::get(100);
                                if(special_factor > 90) {
                                    building_type = BattleBuildings::VILLAGE_SPECIAL_SMALL +
                                        (CRandom::get(BattleBuildings::VILLAGE_SPECIAL_LARGE-BattleBuildings::VILLAGE_SPECIAL_SMALL) + index_start);
                                    if(building_type < BattleBuildings::VILLAGE_SPECIAL_SMALL) building_type = BattleBuildings::VILLAGE_SPECIAL_SMALL;
                                    if(building_type > BattleBuildings::VILLAGE_SPECIAL_LARGE) building_type = BattleBuildings::VILLAGE_SPECIAL_LARGE;

                                    bool alreadycreated = false;

                                    SpecialListNode * node = SpecialTypesCreated.first();
                                    while(node != NULL) {
                                        if(building_type == node->special) alreadycreated = true;
                                        node = SpecialTypesCreated.next();
                                    }
                                    if(!alreadycreated) {
                                        SpecialListNode * newnode = new SpecialListNode;
                                        newnode->special = building_type;
                                        SpecialTypesCreated.insert(newnode);

                              entry.index(building_type);

                                        buildings->add(hexpos, entry);
                                    }
                                }
                                else {
                                    building_type = BattleBuildings::VILLAGE_SMALL +
                                        (CRandom::get(BattleBuildings::VILLAGE_LARGE - BattleBuildings::VILLAGE_SMALL)+index_start);
                                    if(building_type < BattleBuildings::VILLAGE_SMALL) building_type = BattleBuildings::VILLAGE_SMALL;
                                    if(building_type > BattleBuildings::VILLAGE_LARGE) building_type = BattleBuildings::VILLAGE_LARGE;

                           entry.index(building_type);

                                    buildings->add(hexpos, entry);
                                }

                            }
                        }
                    }

            } // end of adding a building


        }// add a number of buildings at this range

    if(index_start > 0) index_start--;
    }// move out from centre of settlement


}




/*
Scatters town-type settlements around a given seed-point, and sets the hex type to town
*/

void
BattleCreateImp::ScatterTownSettlements(BattleMap * map, const TerrainTable & terrain, SeedPoint * seed_point, const BuildingTable* buildingTable, BuildingList* buildings) {

   BuildingStatus building_status = BUILDING_NORMAL;
   unsigned int start_time = 0;

   HexCord MapSize = map->getSize();
   int mapwidth = MapSize.x();
   int mapheight = MapSize.y();

   int xoffset;
   int yoffset;
   int x=seed_point->hex.x();
   int y=seed_point->hex.y();
   int size = seed_point->size;
   HexCord hexpos;

    SList <SpecialListNode> SpecialTypesCreated;
    SpecialTypesCreated.reset();

    // increase the distance within which buildings are created
    int index_start = 7;

    for(int spread=1; spread<size; spread++) {

        // create a number of buildings within this range
        for(int density=0; density < parameters->TownDensity; density++) {

         /*
         * Debugging aid : creates buildings in varying states
         */
         #ifdef RANDOM_BUILDING_STATUS
//          int r = CRandom::get(0,100);
            int r = CRandom::get(100);
            if(r<25) building_status = BUILDING_NORMAL;
            else if(r<50) {
               building_status = BUILDING_BURNING;
//             start_time = random(0,BUILDING_BURNING_INTERVAL);
            }
            else if(r<75) {
               building_status = BUILDING_SMOULDERING;
//             start_time = random(0,BUILDING_SMOULDERING_INTERVAL);
            }
            else building_status = BUILDING_DESTROYED;
         #endif


            xoffset = CRandom::get(spread) - (spread/2);
            yoffset = CRandom::get(spread) - (spread/2);

            hexpos.x(x+xoffset);
            hexpos.y(y+yoffset);

            // only add a settlement here if this hex is on the map
            if(hexpos.x() >= 0 && hexpos.x() < mapwidth && hexpos.y() >= 0 && hexpos.y() < mapheight) {
                // get the terrain type at this hex
                BattleTerrainHex& maphex = map->get(hexpos);

                    // make sure terrain here is valid for placing settlements
                    if(maphex.d_terrainType == GT_None || maphex.d_terrainType == parameters->FirstTerrainType || maphex.d_terrainType == parameters->SecondTerrainType) {

                        // set this hex to the town texture
                        maphex.d_terrainType = GT_Town;

                        // if there is a path here, then maybe add a roadside cluster
                  int path_index = -1;
                  if(maphex.d_path1.d_type != 0) path_index = GetBridgeOrientation(maphex.d_path1.d_edges);
                  if(path_index == -1 && maphex.d_path2.d_type != 0) path_index = GetBridgeOrientation(maphex.d_path2.d_edges);

                  // if a valid path, & we meet a percentage chance..
                  if((path_index != -1) && (CRandom::get(100)>60) ) {

                            // only add buildings, if there are none already defined for this hex
                            if(buildings->get(hexpos) == NULL) {

                        int bank = 0;
                        if(CRandom::get(100) > 50) bank = 1;

                        int building_type = TOWN_ROADSIDE_BANK1 + (bank*9) + path_index;

                        BuildingListEntry entry;
                        entry.status(building_status);
                        entry.effectCount(start_time);
                        entry.index(building_type);

                        buildings->add(hexpos, entry);
                     }
                  }

                        // else add a normal cluster, if no paths here
                        else if((maphex.d_path1.d_type == 0) && (maphex.d_path2.d_type == 0)) {

                            // only add buildings, if there are none already defined for this hex
                            if(buildings->get(hexpos) == NULL) {

                        BuildingListEntry entry;
                        entry.status(building_status);
                        entry.effectCount(start_time);

                                // now add the building
                                int building_type;
                                int special_factor = CRandom::get(100);
                                if(special_factor > 90) {
                                    building_type = BattleBuildings::TOWN_SPECIAL_SMALL +
                                        (CRandom::get(BattleBuildings::TOWN_SPECIAL_LARGE - BattleBuildings::TOWN_SPECIAL_SMALL)+index_start);
                                    if(building_type < BattleBuildings::TOWN_SPECIAL_SMALL) building_type = BattleBuildings::TOWN_SPECIAL_SMALL;
                                    if(building_type > BattleBuildings::TOWN_SPECIAL_LARGE) building_type = BattleBuildings::TOWN_SPECIAL_LARGE;

                                    bool alreadycreated = false;

                                    SpecialListNode * node = SpecialTypesCreated.first();
                                    while(node != NULL) {
                                        if(building_type == node->special) alreadycreated = true;
                                        node = SpecialTypesCreated.next();
                                    }
                                    if(!alreadycreated) {
                                        SpecialListNode * newnode = new SpecialListNode;
                                        newnode->special = building_type;
                                        SpecialTypesCreated.insert(newnode);

                              entry.index(building_type);

                                        buildings->add(hexpos, entry);
                                    }

                                }
                                else {
                                    building_type = BattleBuildings::TOWN_SMALL +
                                        (CRandom::get(BattleBuildings::TOWN_LARGE - BattleBuildings::TOWN_SMALL)+index_start);
                                    if(building_type < BattleBuildings::TOWN_SMALL) building_type = BattleBuildings::TOWN_SMALL;
                                    if(building_type > BattleBuildings::TOWN_LARGE) building_type = BattleBuildings::TOWN_LARGE;

                           entry.index(building_type);

                                    buildings->add(hexpos, entry);
                                }


/*
                                        // set a random status for this building ( test only ! )
                                        BuildingInfo const * binfo = buildings->get(hexpos);
                                        const BuildingInfo::DisplayInfo& dispInfo = binfo->displayInfo();
                                        for(BuildingInfo::DisplayInfo::const_iterator buildingIter = dispInfo.begin(); buildingIter != dispInfo.end(); ++buildingIter) {

                                            const BuildingDisplayInfo* bdInfo = buildingIter;
                                            bdInfo->status = static_cast<BattleBuildings::BuildingStatus> (Random(BattleBuildings::BUILDING_NORMAL, BattleBuildings::BUILDING_DESTROYED+1));
                                            if(bdInfo->status == BattleBuildings::BUILDING_BURNING) bdInfo->effect = BattleBuildings::BUILDING_FIRE;
                                            else if(bdInfo->status == BattleBuildings::BUILDING_SMOULDERING) bdInfo->effect = BattleBuildings::BUILDING_SMOKE;
                                            else bdInfo->effect = BattleBuildings::NONE;
                                            bdInfo->frame_no = Random(10);
                                            bdInfo->frame_timer = Random(10);
                                        }
*/

                            }
                        }
                    }

            } // end of adding a building


        }// add a number of buildings at this range

    if(index_start > 0) index_start--;
    }// move out from centre of settlement


}




void
BattleCreateImp::CreateFarm(BattleMap * map, const TerrainTable & terrain, SeedPoint * seed_point, const BuildingTable* buildingTable, BuildingList* buildings) {

   BuildingStatus building_status = BUILDING_NORMAL;
   unsigned int start_time = 0;


   const HexCord & MapSize = map->getSize();
   int width = MapSize.x();
   int height = MapSize.y();

   int xoffset;
   int yoffset;
   int centre_x=seed_point->hex.x();
   int centre_y=seed_point->hex.y();
   int size = seed_point->size;
   HexCord hexpos;
   TerrainType current_type;
   LogicalPathEnum current_path1;
   LogicalPathEnum current_path2;
   int building_type;

    // first place a main farm building in or near the centre of the farm settlement
    int index_start = 7;
    int n=0;

    hexpos = seed_point->hex;
    current_type = GT_Farm;
    current_path1 = LP_None;
    current_path2 = LP_None;

    int positioned_ok =0;
    int max_attempts = 10;

        // until we've found a suitable hex
        while(!positioned_ok) {

            // finish when hex is clear & there are no paths within it
            if(current_type == GT_None || current_type == parameters->FirstTerrainType || current_type == parameters->SecondTerrainType) {
                if(current_path1 == LP_None && current_path2 == LP_None) positioned_ok = 1; }

            // until we've found a valid hex on the map
            while(hexpos.x() < 0 || hexpos.x() > width || hexpos.y() < 0 || hexpos.y() > height) {
                xoffset = CRandom::get(n) - (n/2);
                yoffset = CRandom::get(n) - (n/2);
                hexpos.x(centre_x+xoffset);
                hexpos.y(centre_y+yoffset);
            }

            BattleTerrainHex & maphex = map->get(hexpos);
            current_type = maphex.d_terrainType;
            current_path1 = static_cast<LogicalPathEnum> (maphex.d_path1.d_type);
            current_path2 = static_cast<LogicalPathEnum> (maphex.d_path2.d_type);

            n++;
            max_attempts--;
            if(max_attempts == 0) return;
        }

        // now set this hex to be the main farm building
        BattleTerrainHex & maphex = map->get(hexpos);
        maphex.d_terrainType = GT_Farm;
        // make graphic size dependent on settlement size
        building_type = BattleBuildings::FARM_SMALL +
                CRandom::get(BattleBuildings::FARM_LARGE - BattleBuildings::FARM_SMALL) + seed_point->size;
        if(building_type > BattleBuildings::FARM_LARGE) building_type = BattleBuildings::FARM_LARGE;

         /*
         * Debugging aid : creates buildings in varying states
         */
         #ifdef RANDOM_BUILDING_STATUS
//          int r = CRandom::get(0,100);
            int r = CRandom::get(100);
            if(r<25) building_status = BUILDING_NORMAL;
            else if(r<50) {
               building_status = BUILDING_BURNING;
//             start_time = random(0,BUILDING_BURNING_INTERVAL);
            }
            else if(r<75) {
               building_status = BUILDING_SMOULDERING;
//             start_time = random(0,BUILDING_SMOULDERING_INTERVAL);
            }
            else building_status = BUILDING_DESTROYED;
         #endif

      BuildingListEntry entry;
      entry.index(building_type);
      entry.status(building_status);
      entry.effectCount(start_time);

        buildings->add(hexpos, entry);


        // now add a few extra buildings in the adjacent hexes, proportional to farm size
        centre_x = hexpos.x();
        centre_y = hexpos.y();

        int f;
        for(f=0; f<seed_point->size; f++) {

         /*
         * Debugging aid : creates buildings in varying states
         */
         #ifdef RANDOM_BUILDING_STATUS
//          int r = CRandom::get(0,100);
            int r = CRandom::get(100);
            if(r<25) building_status = BUILDING_NORMAL;
            else if(r<50) {
               building_status = BUILDING_BURNING;
//             start_time = random(0,BUILDING_BURNING_INTERVAL);
            }
            else if(r<75) {
               building_status = BUILDING_SMOULDERING;
//             start_time = random(0,BUILDING_SMOULDERING_INTERVAL);
            }
            else building_status = BUILDING_DESTROYED;
         #endif

            xoffset = CRandom::get(2) -1;
            yoffset = CRandom::get(2) -1;
            hexpos.x(centre_x + xoffset);
            hexpos.y(centre_y + yoffset);

            // make sure out coordinate is on the map
            if(hexpos.x() >= 0 && hexpos.x() < width && hexpos.y() >= 0 && hexpos.y() < height) {

                BattleTerrainHex & maphex = map->get(hexpos);
                current_type = maphex.d_terrainType;
                current_path1 = static_cast<LogicalPathEnum> (maphex.d_path1.d_type);
                current_path2 = static_cast<LogicalPathEnum> (maphex.d_path2.d_type);
                // if this hex is clear, add some farm buildings
                if(
                    (current_type == GT_None ||
                    current_type == parameters->FirstTerrainType ||
                    current_type == parameters->SecondTerrainType) &&
                    ((current_path1 | current_path2) == LP_None) ) {
                    maphex.d_terrainType = GT_Farm;
//                    building_type = CRandom::get(BattleBuildings::FARM_SPECIAL_SMALL, BattleBuildings::FARM_SPECIAL_LARGE);
                    building_type = CRandom::get(BattleBuildings::FARM_SPECIAL_SMALL, BattleBuildings::FARM_SPECIAL_LARGE-1);
               entry.index(building_type);
                    buildings->add(hexpos, entry);
                }
            }

        }


        int first_field_type;
        int second_field_type;
        int randnum;

        // decide which are the first & second crop types being grown
//        randnum = CRandom::get(1,5);
        randnum = CRandom::get(1,4);
        ASSERT((randnum >= 1) && (randnum <= 4));
        switch(randnum) {
            case 1 : { first_field_type = GT_PloughedField; break; }
            case 2 : { first_field_type = GT_CornField; break; }
            case 3 : { first_field_type = GT_WheatField; break; }
            case 4 : { first_field_type = GT_Vineyard; break; }
        }

//        randnum = CRandom::get(1,5);
        randnum = CRandom::get(1,4);
        ASSERT((randnum >= 1) && (randnum <= 4));
        switch(randnum) {
            case 1 : { second_field_type = GT_PloughedField; break; }
            case 2 : { second_field_type = GT_CornField; break; }
            case 3 : { second_field_type = GT_WheatField; break; }
            case 4 : { second_field_type = GT_Vineyard; break; }
        }

        HexCord centre_hex;
        centre_hex.x(centre_x);
        centre_hex.y(centre_y);

        CreateTerrainScattering(map,centre_hex, first_field_type, seed_point->size+2, 3);
        CreateTerrainScattering(map,centre_hex, second_field_type, seed_point->size+2, 3);



}




void
BattleCreateImp::ReplaceBridgesWithBuildings(BattleMap * map, const TerrainTable & terrain, const BuildingTable* buildingTable, BuildingList* buildings) {

HexCord MapSize = map->getSize();
int width = MapSize.x();
int height = MapSize.y();
HexCord hex;


        for(int y=0; y<height; y++) {

            for(int x=0; x<width; x++) {

                hex.x(x);
                hex.y(y);

                BattleTerrainHex & maphex = map->get(hex);
                PathInfo & path1 = maphex.d_path1;
                PathInfo & path2 = maphex.d_path2;

                if(path1.d_type == LP_TrackBridge) {
               if(buildings->get(hex) == NULL) {
                  int index = GetBridgeOrientation(path1.d_edges);
                  if(index != -1) {
                     BuildingListEntry entry;
                     entry.index(BattleBuildings::TRACK_BRIDGE + index);
                     entry.status(BUILDING_NORMAL);

                     buildings->add(hex, entry);
                  }
               }
                }

                if(path1.d_type == LP_RoadBridge) {
               if(buildings->get(hex) == NULL) {
                  int index = GetBridgeOrientation(path1.d_edges);
                  if(index != -1) {
                     BuildingListEntry entry;
                     entry.index(BattleBuildings::ROAD_BRIDGE + index);
                     entry.status(BUILDING_NORMAL);

                     buildings->add(hex, entry);
                  }
               }
                }

                if(path2.d_type == LP_TrackBridge) {
               if(buildings->get(hex) == NULL) {
                  int index = GetBridgeOrientation(path2.d_edges);
                  if(index != -1) {
                     BuildingListEntry entry;
                     entry.index(BattleBuildings::TRACK_BRIDGE + index);
                     entry.status(BUILDING_NORMAL);

                     buildings->add(hex, entry);
                  }
               }
                }


                if(path2.d_type == LP_RoadBridge) {
               if(buildings->get(hex) == NULL) {
                  int index = GetBridgeOrientation(path2.d_edges);
                  if(index != -1) {
                     BuildingListEntry entry;
                     entry.index(BattleBuildings::ROAD_BRIDGE + index);
                     entry.status(BUILDING_NORMAL);

                     buildings->add(hex, entry);
                  }
               }
                }


            }

        }

}




int
BattleCreateImp::GetBridgeOrientation(EdgeMap edges) {

//    int val;
    if(edges == 5) return 0;
    if(edges == 9) return 1;
    if(edges == 10) return 2;
    if(edges == 17) return 3;
    if(edges == 18) return 4;
    if(edges == 20) return 5;
    if(edges == 34) return 6;
    if(edges == 36) return 7;
    if(edges == 40) return 8;

    return -1;
}







void
BattleCreateImp::FixDodgyPaths(BattleMap * map) {

HexCord MapSize = map->getSize();
int width = MapSize.x();
int height = MapSize.y();
HexCord hex;


        for(int y=0; y<height; y++) {

            for(int x=0; x<width; x++) {

                hex.x(x);
                hex.y(y);

                BattleTerrainHex & maphex = map->get(hex);
                PathInfo & path1 = maphex.d_path1;
                PathInfo & path2 = maphex.d_path2;

                // first dodgy situation is where two paths of the same type are in the same hex on different paths
                // here, simply delete path1, & join up path 2
                if(
                (path1.d_type == LP_Track || path1.d_type == LP_SunkenTrack) &&
                (path2.d_type == LP_Track || path2.d_type == LP_SunkenTrack) ) {

                    // join up into path2
                    path2.d_edges |= path1.d_edges;
                    // remove path 1
                    path1.d_type = LP_None;
                    path1.d_edges = 0;
                }


            } // go through whole map
        }


}







void
BattleCreateImp::CreateTestBuildings(BattleMap * map, const TerrainTable & terrain, const BuildingTable* buildingTable, BuildingList* buildings) {

HexCord hexpos;
int x;

BuildingListEntry entry;
entry.status(BUILDING_NORMAL);
entry.frame(0);
entry.timer(0);
entry.effectCount(0);


    for(x = 0; x < 8; x++) {

        hexpos.x(x);
        hexpos.y(1);
      entry.index(BattleBuildings::TOWN_SMALL + x);
        buildings->add(hexpos, entry);

        hexpos.x(x);
        hexpos.y(3);
      entry.index(BattleBuildings::TOWN_SPECIAL_SMALL + x);
        buildings->add(hexpos, entry);

        hexpos.x(x);
        hexpos.y(5);
      entry.index(BattleBuildings::VILLAGE_SMALL + x);
        buildings->add(hexpos, entry);

        hexpos.x(x);
        hexpos.y(7);
      entry.index(BattleBuildings::VILLAGE_SPECIAL_SMALL + x);
        buildings->add(hexpos, entry);

        hexpos.x(x);
        hexpos.y(9);
      entry.index(BattleBuildings::FARM_SMALL + x);
        buildings->add(hexpos, entry);

        hexpos.x(x);
        hexpos.y(11);
      entry.index(BattleBuildings::FARM_SPECIAL_SMALL + x);
        buildings->add(hexpos, entry);

        hexpos.x(x);
        hexpos.y(13);
      entry.index(BattleBuildings::LIGHT_WOOD_SMALL + x);
        buildings->add(hexpos, entry);

        hexpos.x(x);
        hexpos.y(15);
      entry.index(BattleBuildings::DENSE_WOOD_SMALL + x);
        buildings->add(hexpos, entry);
    }



}








/*
Read in a terrain generator parameters ascii file
If there is a problem with loading / reading the file, returns FLASE
Otherwise fills in the parameters & returns TRUE
*/

int
BattleCreateImp::ReadParametersFile(char * filename) {

    HANDLE filehandle;
    char * buffer = NULL;
    int filesize;
    unsigned long bytesread;

    // attempt to open file
    filehandle = CreateFile(
        filename,
        GENERIC_READ,
        FILE_SHARE_READ,
        NULL,
        OPEN_EXISTING,
        FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN,
        NULL);

    // return FLASE if there was an error opening file
    if(filehandle == INVALID_HANDLE_VALUE) { return 0; }

    // get file size
    filesize = GetFileSize(filehandle, NULL);

    // allocate space for file
    buffer = new char[filesize];

    // rewind to start of file
    SetFilePointer(filehandle, NULL, NULL, FILE_BEGIN);

    // read file into buffer
    BOOL readresult = ReadFile(filehandle, buffer, filesize, &bytesread, NULL);

    // close file
    CloseHandle(filehandle);

    // return FALSE if there was a read error
    if(!readresult) { delete buffer; return 0; }
    // return FASLE if file was not of expected size
    if(bytesread != filesize) { delete buffer; return 0; }


    /*
    Start parsing the file
    */

    float version_number;
    char tempstring[256];
    char * pos = buffer;

    // find the file header string
    pos = strstr(pos, "TERRAIN_GENERATOR_PARAMETERS");
    if(!pos) { delete buffer; return 0; }
    sscanf(pos, "%s", tempstring);
    pos += strlen(tempstring);

    // find the version number string
    pos = strstr(pos, "VERSION");
    if(!pos) { delete buffer; return 0; }
    sscanf(pos, "%s", tempstring);
    pos += strlen(tempstring);

    // read the version number
    sscanf(pos, "%f", &version_number);
    // ensure that file version is within program version
    // NOTE : this doesn't work due to floating-point precision problems
    // if(version_number > TERRAIN_GENERATOR_VERSION) { delete buffer; return 0; }

    // find the random seed string
    pos = strstr(pos, "RANDOM_SEED");
    if(!pos) { delete buffer; return 0; }
    sscanf(pos, "%s", tempstring);
    pos += strlen(tempstring);
    // read in the value
    // sscanf(pos, "%i", &parameters->RandomSeed);

    // find the map width string
    pos = strstr(pos, "MAP_WIDTH");
    if(!pos) { delete buffer; return 0; }
    sscanf(pos, "%s", tempstring);
    pos += strlen(tempstring);
    // read in the value
    sscanf(pos, "%i", &parameters->MapWidth);

    // find the map width string
    pos = strstr(pos, "MAP_HEIGHT");
    if(!pos) { delete buffer; return 0; }
    sscanf(pos, "%s", tempstring);
    pos += strlen(tempstring);
    // read in the value
    sscanf(pos, "%i", &parameters->MapHeight);

    // find the height-type string
    pos = strstr(pos, "HEIGHT_TYPE");
    if(!pos) { delete buffer; return 0; }
    sscanf(pos, "%s", tempstring);
    pos += strlen(tempstring);
    // read in the value
    sscanf(pos, "%i", &parameters->HeightType);

    // find the primary terrain type string
    pos = strstr(pos, "PRIMARY_TERRAIN_TYPE");
    if(!pos) { delete buffer; return 0; }
    sscanf(pos, "%s", tempstring);
    pos += strlen(tempstring);
    // read in the value
    sscanf(pos, "%i", &parameters->FirstTerrainType);

    // find the secondary terrain type string
    pos = strstr(pos, "SECONDARY_TERRAIN_TYPE");
    if(!pos) { delete buffer; return 0; }
    sscanf(pos, "%s", tempstring);
    pos += strlen(tempstring);
    // read in the value
    sscanf(pos, "%i", &parameters->SecondTerrainType);

    // find the percentage secondary terrain string
    pos = strstr(pos, "PERCENTAGE_SECONDARY_TERRAIN");
    if(!pos) { delete buffer; return 0; }
    sscanf(pos, "%s", tempstring);
    pos += strlen(tempstring);
    // read in the value
    sscanf(pos, "%i", &parameters->PercentageSecondTerrain);

    // find the number of lakes string
    pos = strstr(pos, "NUMBER_OF_LAKES");
    if(!pos) { delete buffer; return 0; }
    sscanf(pos, "%s", tempstring);
    pos += strlen(tempstring);
    // read in the value
    sscanf(pos, "%i", &parameters->NumberOfLakes);

    // find the minimum lake size string
    pos = strstr(pos, "MINIMUM_LAKE_SIZE");
    if(!pos) { delete buffer; return 0; }
    sscanf(pos, "%s", tempstring);
    pos += strlen(tempstring);
    // read in the value
    sscanf(pos, "%i", &parameters->MinimumLakeSize);

    // find the maximum lake size string
    pos = strstr(pos, "MAXIMUM_LAKE_SIZE");
    if(!pos) { delete buffer; return 0; }
    sscanf(pos, "%s", tempstring);
    pos += strlen(tempstring);
    // read in the value
    sscanf(pos, "%i", &parameters->MaximumLakeSize);

    // find the number of rivers string
    pos = strstr(pos, "NUMBER_OF_RIVERS");
    if(!pos) { delete buffer; return 0; }
    sscanf(pos, "%s", tempstring);
    pos += strlen(tempstring);
    // read in the value
    sscanf(pos, "%i", &parameters->NumberOfRivers);

    // find the number of streams string
    pos = strstr(pos, "NUMBER_OF_STREAMS");
    if(!pos) { delete buffer; return 0; }
    sscanf(pos, "%s", tempstring);
    pos += strlen(tempstring);
    // read in the value
    sscanf(pos, "%i", &parameters->NumberOfStreams);

    // find the chance create bridge string
    pos = strstr(pos, "CHANCE_CREATE_BRIDGE");
    if(!pos) { delete buffer; return 0; }
    sscanf(pos, "%s", tempstring);
    pos += strlen(tempstring);
    // read in the value
    sscanf(pos, "%i", &parameters->ChanceCreateBridge);

    // find the number of roads string
    pos = strstr(pos, "NUMBER_OF_ROADS");
    if(!pos) { delete buffer; return 0; }
    sscanf(pos, "%s", tempstring);
    pos += strlen(tempstring);
    // read in the value
    sscanf(pos, "%i", &parameters->NumberOfRoads);

    // find the number of tracks string
    pos = strstr(pos, "NUMBER_OF_TRACKS");
    if(!pos) { delete buffer; return 0; }
    sscanf(pos, "%s", tempstring);
    pos += strlen(tempstring);
    // read in the value
    sscanf(pos, "%i", &parameters->NumberOfTracks);

    // find the chance roads join string
    pos = strstr(pos, "CHANCE_ROADS_JOIN");
    if(!pos) { delete buffer; return 0; }
    sscanf(pos, "%s", tempstring);
    pos += strlen(tempstring);
    // read in the value
    sscanf(pos, "%i", &parameters->ChanceRoadsJoin);

    // find the chance settlement at crossroads string
    pos = strstr(pos, "CHANCE_SETTLEMENT_AT_CROSSROADS");
    if(!pos) { delete buffer; return 0; }
    sscanf(pos, "%s", tempstring);
    pos += strlen(tempstring);
    // read in the value
    sscanf(pos, "%i", &parameters->ChanceSettlementAtCrossroads);

    // find the chance settlement at junction string
    pos = strstr(pos, "CHANCE_SETTLEMENT_AT_JUNCTION");
    if(!pos) { delete buffer; return 0; }
    sscanf(pos, "%s", tempstring);
    pos += strlen(tempstring);
    // read in the value
    sscanf(pos, "%i", &parameters->ChanceSettlementAtJunction);

    // find the chance settlement at bridge string
    pos = strstr(pos, "CHANCE_SETTLEMENT_AT_BRIDGE");
    if(!pos) { delete buffer; return 0; }
    sscanf(pos, "%s", tempstring);
    pos += strlen(tempstring);
    // read in the value
    sscanf(pos, "%i", &parameters->ChanceSettlementAtBridge);

    // find the chance settlement at ford string
    pos = strstr(pos, "CHANCE_SETTLEMENT_AT_FORD");
    if(!pos) { delete buffer; return 0; }
    sscanf(pos, "%s", tempstring);
    pos += strlen(tempstring);
    // read in the value
    sscanf(pos, "%i", &parameters->ChanceSettlementAtFord);

    // find the minimum settlement size string
    pos = strstr(pos, "MINIMUM_SETTLEMENT_SIZE");
    if(!pos) { delete buffer; return 0; }
    sscanf(pos, "%s", tempstring);
    pos += strlen(tempstring);
    // read in the value
    sscanf(pos, "%i", &parameters->MinimumSettlementSize);

    // find the maximum settlement size string
    pos = strstr(pos, "MAXIMUM_SETTLEMENT_SIZE");
    if(!pos) { delete buffer; return 0; }
    sscanf(pos, "%s", tempstring);
    pos += strlen(tempstring);
    // read in the value
    sscanf(pos, "%i", &parameters->MaximumSettlementSize);

    // find the maximum village size string
    pos = strstr(pos, "MAXIMUM_VILLAGE_SIZE");
    if(!pos) { delete buffer; return 0; }
    sscanf(pos, "%s", tempstring);
    pos += strlen(tempstring);
    // read in the value
    sscanf(pos, "%i", &parameters->MaximumVillageSize);

    // find the maximum town size string
    pos = strstr(pos, "MAXIMUM_TOWN_SIZE");
    if(!pos) { delete buffer; return 0; }
    sscanf(pos, "%s", tempstring);
    pos += strlen(tempstring);
    // read in the value
    sscanf(pos, "%i", &parameters->MaximumTownSize);

    // find the village density string
    pos = strstr(pos, "VILLAGE_DENSITY");
    if(!pos) { delete buffer; return 0; }
    sscanf(pos, "%s", tempstring);
    pos += strlen(tempstring);
    // read in the value
    sscanf(pos, "%i", &parameters->VillageDensity);

    // find the town density string
    pos = strstr(pos, "TOWN_DENSITY");
    if(!pos) { delete buffer; return 0; }
    sscanf(pos, "%s", tempstring);
    pos += strlen(tempstring);
    // read in the value
    sscanf(pos, "%i", &parameters->TownDensity);

    // find the combine settlements distance string
    pos = strstr(pos, "COMBINE_SETTLEMENTS_DISTANCE");
    if(!pos) { delete buffer; return 0; }
    sscanf(pos, "%s", tempstring);
    pos += strlen(tempstring);
    // read in the value
    sscanf(pos, "%i", &parameters->CombineSettlementsDistance);

    // find the minimum farm size string
    pos = strstr(pos, "MINIMUM_FARM_SIZE");
    if(!pos) { delete buffer; return 0; }
    sscanf(pos, "%s", tempstring);
    pos += strlen(tempstring);
    // read in the value
    sscanf(pos, "%i", &parameters->MinimumFarmSize);

    // find the maximum farm size string
    pos = strstr(pos, "MAXIMUM_FARM_SIZE");
    if(!pos) { delete buffer; return 0; }
    sscanf(pos, "%s", tempstring);
    pos += strlen(tempstring);
    // read in the value
    sscanf(pos, "%i", &parameters->MaximumFarmSize);

    // find the number of random settlements string
    pos = strstr(pos, "NUMBER_OF_RANDOM_SETTLEMENTS");
    if(!pos) { delete buffer; return 0; }
    sscanf(pos, "%s", tempstring);
    pos += strlen(tempstring);
    // read in the value
    sscanf(pos, "%i", &parameters->NumberOfRandomSettlements);

    // find the chance random settlement is special string
    pos = strstr(pos, "CHANCE_RANDOM_SETTLEMENT_IS_SPECIAL");
    if(!pos) { delete buffer; return 0; }
    sscanf(pos, "%s", tempstring);
    pos += strlen(tempstring);
    // read in the value
    sscanf(pos, "%i", &parameters->ChanceRandomSettlementIsSpecial);

    // find the chance random settlement is solitary string
    pos = strstr(pos, "CHANCE_RANDOM_SETTLEMENT_IS_SOLITARY");
    if(!pos) { delete buffer; return 0; }
    sscanf(pos, "%s", tempstring);
    pos += strlen(tempstring);
    // read in the value
    sscanf(pos, "%i", &parameters->ChanceRandomSettlementIsSolitary);

    // find the chance random settlement is farm string
    pos = strstr(pos, "CHANCE_RANDOM_SETTLEMENT_IS_FARM");
    if(!pos) { delete buffer; return 0; }
    sscanf(pos, "%s", tempstring);
    pos += strlen(tempstring);
    // read in the value
    sscanf(pos, "%i", &parameters->ChanceRandomSettlementIsFarm);

    // find the chance farms create fields string
    pos = strstr(pos, "CHANCE_FARMS_CREATE_FIELDS");
    if(!pos) { delete buffer; return 0; }
    sscanf(pos, "%s", tempstring);
    pos += strlen(tempstring);
    // read in the value
    sscanf(pos, "%i", &parameters->ChanceFarmsCreateFields);

    // find the average farm fields size string
    pos = strstr(pos, "AVERAGE_FARM_FIELDS_SIZE");
    if(!pos) { delete buffer; return 0; }
    sscanf(pos, "%s", tempstring);
    pos += strlen(tempstring);
    // read in the value
    sscanf(pos, "%i", &parameters->AverageFarmFieldsSize);

    // find the number of light woods string
    pos = strstr(pos, "NUMBER_OF_LIGHT_WOODS");
    if(!pos) { delete buffer; return 0; }
    sscanf(pos, "%s", tempstring);
    pos += strlen(tempstring);
    // read in the value
    sscanf(pos, "%i", &parameters->NumberOfLightWoods);

    // find the number of dense woods string
    pos = strstr(pos, "NUMBER_OF_DENSE_WOODS");
    if(!pos) { delete buffer; return 0; }
    sscanf(pos, "%s", tempstring);
    pos += strlen(tempstring);
    // read in the value
    sscanf(pos, "%i", &parameters->NumberOfDenseWoods);

    // find the minimum wood size string
    pos = strstr(pos, "MINIMUM_WOOD_SIZE");
    if(!pos) { delete buffer; return 0; }
    sscanf(pos, "%s", tempstring);
    pos += strlen(tempstring);
    // read in the value
    sscanf(pos, "%i", &parameters->MinimumWoodSize);

    // find the maximum wood size string
    pos = strstr(pos, "MAXIMUM_WOOD_SIZE");
    if(!pos) { delete buffer; return 0; }
    sscanf(pos, "%s", tempstring);
    pos += strlen(tempstring);
    // read in the value
    sscanf(pos, "%i", &parameters->MaximumWoodSize);

    // find the minimum wood density string
    pos = strstr(pos, "MINIMUM_WOOD_DENSITY");
    if(!pos) { delete buffer; return 0; }
    sscanf(pos, "%s", tempstring);
    pos += strlen(tempstring);
    // read in the value
    sscanf(pos, "%i", &parameters->MinimumWoodDensity);

    // find the maximum wood density string
    pos = strstr(pos, "MAXIMUM_WOOD_DENSITY");
    if(!pos) { delete buffer; return 0; }
    sscanf(pos, "%s", tempstring);
    pos += strlen(tempstring);
    // read in the value
    sscanf(pos, "%i", &parameters->MaximumWoodDensity);

    // free up the buffer, & return TRUE
    delete buffer;
    return 1;

}



// Create a random battlefield

namespace BattleCreate
{

void createRandomBattlefield (
        TerrainGenerationParameters * params,
        BattleData * batdata)
{
    BattleCreateImp gen(params);
    gen.make(batdata);
}

void MakeEmptyMap(BattleMap * map, const HexCord& size) {

    BattleCreateImp gen;
    gen.MakeEmptyMap(map, size);
}

void ClearWholeMap(BattleMap * map, TerrainType type) {

    BattleCreateImp gen;
    gen.ClearWholeMap(map, type);
}

void ClearAllPaths(BattleMap * map) {
    BattleCreateImp gen;
    gen.ClearAllPaths(map);
}

void ClearCoasts(BattleMap * map) {
    BattleCreateImp gen;
    gen.ClearCoasts(map);
}

void ClearAllEdges(BattleMap *map) {
    BattleCreateImp gen;
    gen.ClearAllEdges(map);
}

void SetAllHeights(BattleMap * map, int height) {
    BattleCreateImp gen;
    gen.SetAllHeights(map, height);
}

void makeCoasts(BattleMap* map, const TerrainTable& terrain) {
    BattleCreateImp gen;
    gen.makeCoasts(map, terrain);
}


void FixDodgyPaths(BattleMap * map) {
    BattleCreateImp gen;
    gen.FixDodgyPaths(map);
}


void ConstructRoadPath(BattleMap * map, const TerrainTable & terrain, SList<PathPoint> * RoadPath, LogicalPathEnum RoadType) {
    BattleCreateImp gen;
    gen.ConstructRoadPath(map, terrain, RoadPath, RoadType);
}


void ConstructRiverPath(BattleMap * map, const TerrainTable & terrain, SList<PathPoint> * RiverPath, LogicalPathEnum RiverType) {
    BattleCreateImp gen;
    gen.ConstructRiverPath(map, terrain, RiverPath, RiverType);
}


void CreateTerrainHex(BattleMap * map, const HexCord& hex, TerrainType type) {
    BattleCreateImp gen;
    gen.CreateTerrainHex(map, hex, type);
}


void EditorCreateTerrainArea(BattleMap * map, const HexCord& centre_hex, TerrainType terrain_type, int size, int spread_chance, int mode) {
    BattleCreateImp gen;
    gen.EditorCreateTerrainArea(map, centre_hex, terrain_type, size, spread_chance, mode);
}


void AlterHexHeight(BattleMap * map, const HexCord& hex, int ammount) {
    BattleCreateImp gen;
    gen.AlterHexHeight(map, hex, ammount);
}


void AlterHexAreaHeight(BattleMap * map, const HexCord& centre_hex, int size, int spread_chance, int ammount) {
    BattleCreateImp gen;
    gen.AlterHexAreaHeight(map, centre_hex, size, spread_chance, ammount);
}

void AlterHexClumpHeight(BattleMap * map, const HexCord& centre_hex, int size, int density, int ammount) {
    BattleCreateImp gen;
    gen.AlterHexClumpHeight(map, centre_hex, size, density, ammount);
}


void CreateSingleSettlement(const BuildingTable * buildingTable, BuildingList * buildings, const HexCord& hex, BuildingListEntry entry) {
    BattleCreateImp gen;
    gen.CreateSingleSettlement(buildingTable, buildings, hex, entry);
}


void SetEdges(BattleMap * map, const HexCord& hex, EdgeInfo edge_info) {
    BattleCreateImp gen;
    gen.SetEdges(map, hex, edge_info);
}

void ReplaceBridgesWithBuildings(BattleMap * map, const TerrainTable & terrain, const BuildingTable* buildingTable, BuildingList* buildings) {
   BattleCreateImp gen;
   gen.ReplaceBridgesWithBuildings(map, terrain, buildingTable, buildings);
}


};      // namespace BattleCreate




/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.2  2001/06/18 08:25:43  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
