/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
#ifndef BATUNIT_HPP
#define BATUNIT_HPP

#ifndef __cplusplus
#error batunit.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *      Battle Unit virtual class
 *
 *----------------------------------------------------------------------
 */

#include "refptr.hpp"
#include "hexlist.hpp"
#include "gamedefs.hpp"
#include "bobdef.hpp"
#include "animinfo.hpp"
#include "range.hpp"
#include "hexdata.hpp"
//namespace BattleMeasure { class HexCord; };

class UnitDrawer;               // see bu_draw.hpp

/*
 * Battleground object
 * base class for units on the battle display
 */

#define NOHEXMOVE // Note: this is temporary while we sort out movement within a hex

class OrderBattle;
class FileReader;
class FileWriter;




// unit base class. Both BattleCP and BattleSP derive from this class
class BattleUnit :
            public RefBaseCount
{
            public:
                typedef Range<UBYTE, 100>  VisibilityPercent;

                        BattleUnit() :
#ifdef NOHEXMOVE
                           d_nextMove(0),
#endif
                  d_rFacing(BattleMeasure::HexPosition::Facing_Default),
                  d_facing(BattleMeasure::HexPosition::Facing_Default),
                  d_wantFace(BattleMeasure::HexPosition::Facing_Default),
                  d_nextFace(BattleMeasure::HexPosition::Facing_Default),
                  d_finalPos(BattleMeasure::HexPosition::MoveTo_Default),
                  d_startSPChange(0),
                  d_endSPChange(0),
                  d_visibility(Visibility::Default),
                  d_visibilityPercent(VisibilityPercent::MaxValue),
                  d_speed(0),
                  d_moveFlags(AllSPAtCenter),
                  d_combatFlags(0),
            d_miscFlags(0),
                  d_FrameCounter(0) {}
                virtual ~BattleUnit() { }

                // set functions
                void position(const BattleMeasure::BattlePosition& p) { d_position = p; }
                void hex(const BattleMeasure::HexCord& hc) { d_position = hc; }

                // set flags
                void setDeploying(bool f)           { setUWORDFlag(d_moveFlags, f, Deploying); }
                void setMoving(bool f)              { setUWORDFlag(d_moveFlags, f, Moving); }
                void setChangingSPFormation(bool f) { setUWORDFlag(d_moveFlags, f, ChangingSPFormation); }
                void allSPAtCenter(bool f)          { setUWORDFlag(d_moveFlags, f, AllSPAtCenter); }
                void shootingGuns(bool f)           { setUBYTEFlag(d_combatFlags, f, ShootingGuns); }
                void shootingMuskets(bool f)        { setUBYTEFlag(d_combatFlags, f, ShootingMuskets); }
                void takingGunHits(bool f)          { setUBYTEFlag(d_combatFlags, f, TakingGunHits); }
                void takingMusketHits(bool f)       { setUBYTEFlag(d_combatFlags, f, TakingMusketHits); }
                void markingTime(bool f)            { setUWORDFlag(d_moveFlags, f, MarkingTime); }
                void moveStarted(bool f)            { setUWORDFlag(d_moveFlags, f, MoveStarted); }
               // void inCombat(bool f)               { setFlag(f, InCombat); }
                void inCloseCombat(bool f)          { setUBYTEFlag(d_combatFlags, f, CloseCombat); }
                void charging(bool f)               { setUWORDFlag(d_moveFlags, f, Charging); }
                void mouseOverUnit(bool f)          { setUBYTEFlag(d_miscFlags, f, MouseOverUnit); }
                void unitSelected(bool f)           { setUBYTEFlag(d_miscFlags, f, UnitSelected); }
                void movingBackwards(bool f)        { setUWORDFlag(d_moveFlags, f, MovingBackwards); }
                void shouldReorganize(bool f)       { setUBYTEFlag(d_miscFlags, f, ShouldReorganize); }
                void movingLeft(bool f)             { setUWORDFlag(d_moveFlags, f, MovingLeft); }
                void movingRight(bool f)            { setUWORDFlag(d_moveFlags, f, MovingRight); }
                void clearMove(bool f)              { setUWORDFlag(d_moveFlags, f, ClearMove); }
                void firstOrderSent(bool f)         { setUBYTEFlag(d_miscFlags, f, FirstOrderSent); }
                void noRoadMove(bool f)                { setUWORDFlag(d_moveFlags, f, NoRoadMove); }

                void setDead() { d_miscFlags |= IsDead; }
                void setFled() { d_miscFlags |= HasFled; }

#ifdef NOHEXMOVE
                void nextMove(BattleMeasure::BattleTime::Tick t) { d_nextMove = t; }
#endif

                // accessors
                const BattleMeasure::BattlePosition& position() const { return d_position; }
                BattleMeasure::BattlePosition& position() { return d_position; }
                const BattleMeasure::HexCord& hex() const { return d_position.hex(); }
                BattleMeasure::HexPosition& hexPosition() { return d_position.hexPosition(); }
                const BattleMeasure::HexPosition& hexPosition() const { return d_position.hexPosition(); }

                // get flags
                bool isDeploying()            const { return (d_moveFlags & Deploying) != 0; }
                bool isMoving()               const { return (d_moveFlags & Moving) != 0; }
                bool isChangingSPFormation()  const { return (d_moveFlags & ChangingSPFormation) != 0; }
                bool allSPAtCenter()          const { return (d_moveFlags & AllSPAtCenter) != 0; }
                bool shootingGuns()           const { return (d_combatFlags & ShootingGuns) != 0; }
                bool shootingMuskets()        const { return (d_combatFlags & ShootingMuskets) != 0; }
                bool takingGunHits()          const { return (d_combatFlags & TakingGunHits) != 0; }
                bool takingMusketHits()       const { return (d_combatFlags & TakingMusketHits) != 0; }
                bool markingTime()            const { return (d_moveFlags & MarkingTime) != 0; }
                bool moveStarted()            const { return (d_moveFlags & MoveStarted) != 0; }
                bool inCombat()               const { return (inCloseCombat() || takingGunHits() || takingMusketHits() || shootingGuns() || shootingMuskets()); } //(d_flags & InCombat); }
                bool inCloseCombat()          const { return (d_combatFlags & CloseCombat) != 0; }
                bool charging()               const { return (d_moveFlags & Charging) != 0; }
                bool mouseOverUnit()          const { return (d_miscFlags & MouseOverUnit) != 0; }
                bool unitSelected()           const { return (d_miscFlags & UnitSelected) != 0; }
                bool movingBackwards()        const { return (d_moveFlags & MovingBackwards) != 0; }
                bool movingLeft()             const { return (d_moveFlags & MovingLeft) != 0; }
                bool movingRight()            const { return (d_moveFlags & MovingRight) != 0; }
                bool clearMove()              const { return (d_moveFlags & ClearMove) != 0; }
                bool shouldReorganize()       const { return (d_miscFlags & ShouldReorganize) != 0; }
                bool isDead() const { return (d_miscFlags & IsDead) != 0; }
                bool hasFled() const { return (d_miscFlags & HasFled) != 0; }
                bool hasQuitBattle() const { return (d_miscFlags & (IsDead | HasFled)) != 0; }
                bool firstOrderSent() const { return (d_miscFlags & FirstOrderSent) != 0; }
                bool noRoadMove() const { return (d_moveFlags & NoRoadMove) != 0; }

                HexList& routeList() { return d_routeList; }
                const HexList& routeList() const { return d_routeList; }
                // const HexItem* nextHex() const { return (d_routeList.entries() > 0) ? d_routeList.first() : 0; }
                const HexItem* nextHex() const { return (d_routeList.entries() > 0) ? d_routeList.head() : 0; }
                // void removeRouteNode() { d_routeList.remove(d_routeList.first()); }
                void removeRouteNode() { d_routeList.remove(d_routeList.head()); }

                BattleMeasure::HexPosition::Facing rFacing() const { return d_rFacing; }
                BattleMeasure::HexPosition::Facing facing() const { return d_facing; }
                BattleMeasure::HexPosition::Facing wantFacing() const { return d_wantFace; }
                BattleMeasure::HexPosition::Facing nextFacing() const { return d_nextFace; }
                void setRFacing(BattleMeasure::HexPosition::Facing face) { d_rFacing = face; }
                void setFacing(BattleMeasure::HexPosition::Facing face) { d_wantFace = d_facing = face; }
                void changeFacing(BattleMeasure::HexPosition::Facing face) { d_wantFace = face; }
                bool updateFacing(BattleMeasure::HexPosition::Facing change);
                void nextFacing(BattleMeasure::HexPosition::Facing face) { d_nextFace = face; }
                void startSPChange(BattleMeasure::BattleTime::Tick t) { d_startSPChange = t; }
                void endSPChange(BattleMeasure::BattleTime::Tick t) { d_endSPChange = t; }
                BattleMeasure::BattleTime::Tick startSPChange() const { return d_startSPChange; }
                BattleMeasure::BattleTime::Tick endSPChange() const { return d_endSPChange; }

                void speed(BattleMeasure::Speed s) { d_speed = s; }
                BattleMeasure::Speed speed() const { return d_speed; }

                void finalPos(BattleMeasure::HexPosition::MoveTo mt) { d_finalPos = mt; }
                BattleMeasure::HexPosition::MoveTo finalPos() const { return d_finalPos; }

                void visibility(Visibility::Value v) { d_visibility = v; }
                Visibility::Value visibility() const { return d_visibility; }

                void visibilityPercent(VisibilityPercent::Value val) { d_visibilityPercent = val; }
                UBYTE visibilityPercent()     const { return static_cast<UBYTE>(d_visibilityPercent.value(100)); }

#ifdef NOHEXMOVE
                BattleMeasure::BattleTime::Tick nextMove() const { return d_nextMove; }
#endif
                virtual void draw(UnitDrawer* drawer) const = 0;

                // animation stuff
                mutable AnimationInfo d_AnimInfo;
                AnimationInfo& GetAnimInfo(void) const { return d_AnimInfo; }

                mutable int d_FrameCounter;
                inline int FrameCounter(void) const { return d_FrameCounter; }
                inline void FrameCounter(int newval) const { d_FrameCounter = newval; }
                inline void IncFrameCounter(void) const { d_FrameCounter++; }

                /*
                  * File Interface
                  */

                bool readData(FileReader& f, OrderBattle& ob);
                bool writeData(FileWriter& f, OrderBattle& ob) const;

         private:
                static const UWORD s_fileVersion;

                void setUWORDFlag(UWORD& flag, bool f, UWORD mask)
                {
                  if(f)
                         flag |= mask;
                  else
                         flag &= ~mask;
                }

                void setUBYTEFlag(UBYTE& flag, bool f, UBYTE mask)
                {
                  if(f)
                         flag |= mask;
                  else
                         flag &= ~mask;
                }

                BattleMeasure::BattlePosition      d_position;             // Where it is
                HexList                            d_routeList;      // route hexes
                BattleMeasure::HexPosition::Facing d_rFacing;        // Which direction is the SP facing (Hex Points only) ?
                BattleMeasure::HexPosition::Facing d_facing;         // Which direction is the SP actually facing (Hex Points or Faces)?
                BattleMeasure::HexPosition::Facing d_wantFace;       // What direction does it want to face?
                BattleMeasure::HexPosition::Facing d_nextFace;       // What direction does it face next

//              BattleMeasure::HexCord::HexDirection d_wantDir;
                BattleMeasure::HexPosition::MoveTo d_finalPos;

                BattleMeasure::BattleTime::Tick d_startSPChange;   // time started an SP Formantion change
                BattleMeasure::BattleTime::Tick d_endSPChange;

                Visibility::Value d_visibility;
                VisibilityPercent d_visibilityPercent;
#ifdef NOHEXMOVE        // Note: this is temporary while we sort out movement within a hex
                BattleMeasure::BattleTime::Tick  d_nextMove;
#endif
                BattleMeasure::Speed d_speed;

                enum MoveFlags {
                     Deploying           = 0x0001,
                     Moving              = 0x0002,
                     ChangingSPFormation = 0x0004,
                     AllSPAtCenter       = 0x0008,
                     MarkingTime         = 0x0010,
                     MoveStarted         = 0x0020,
                     MovingBackwards     = 0x0040,
                     MovingLeft          = 0x0080,
                     MovingRight         = 0x0100,
                     Charging            = 0x0200,
                     ClearMove           = 0x0400,
                     NoRoadMove          = 0x1000
                };

                UWORD d_moveFlags;

                enum CombatFlags {
                     ShootingGuns        = 0x01,
                     ShootingMuskets     = 0x02,
                     TakingGunHits       = 0x04,
                     TakingMusketHits    = 0x08,
                     CloseCombat         = 0x10
                };

                UBYTE d_combatFlags;

                enum MiscFlags {
                     MouseOverUnit       = 0x01,
                     UnitSelected        = 0x02,
                     ShouldReorganize    = 0x04,
                     IsDead              = 0x08,
                     // Unchecked
                     HasFled             = 0x10,
                     FirstOrderSent      = 0x20
                };

                UBYTE d_miscFlags;        // various flags
};


typedef        RefPtr<BattleUnit>  RefBattleUnit;
typedef       CRefPtr<BattleUnit>  CRefBattleUnit;
typedef const  RefPtr<BattleUnit>& ParamRefBattleUnit;
typedef const CRefPtr<BattleUnit>& ParamCRefBattleUnit;
typedef        RefPtr<BattleUnit>  ReturnRefBattleUnit;
typedef       CRefPtr<BattleUnit>  ReturnCRefBattleUnit;

static const CRefBattleUnit NoBattleUnit = 0;

// inline BattleUnit::~BattleUnit() { }

#endif /* BATUNIT_HPP */

