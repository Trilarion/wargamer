/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			        *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								                  *
 *----------------------------------------------------------------------------*/
#ifndef BOB_SP_HPP
#define BOB_SP_HPP

#ifndef __cplusplus
#error bob_sp.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *	Battle Strength Points
 *
 *----------------------------------------------------------------------
 */

#include "bd_dll.h"
#include "batunit.hpp"
//#include "unittype.hpp"
// #include "bobdef.hpp"
#include "obdefs.hpp"		// Generic OB Definitions
// #include "sp.hpp"				// Generic Strength Point
//#include "poolarry.hpp"
//#include "range.hpp"

//using BattleMeasure::BattlePosition;
//using BattleMeasure::HexPosition;

//using namespace BOB_Definitions;

class OrderBattle;
class FileReader;
class FileWriter;

/*-------------------------------------------------------------
 * Battle StrengthPoint
 *
 * These are stored in a container as part of BattleArmy
 * Each division contains a pointer to the start of a chain
 */

class BattleSP :
	public BattleUnit
{
	public:
		typedef Range<UBYTE, UBYTE_MAX> FormationTween;
		typedef Range<UBYTE, 100> SPStrength;

		virtual ~BattleSP();
		BATDATA_DLL BattleSP();
		BattleSP(const BattleSP&);
		BattleSP& operator = (const BattleSP&);

/******************************************************
Start of James's change
******************************************************/

                void generic(const ISP& g) {
                    if(d_generic != NoStrengthPoint)
                        d_generic->battleInfo(0);
                        
                    d_generic = g;
                    
                     if(d_generic != NoStrengthPoint)
                        d_generic->battleInfo(this);   
                }

/******************************************************
End of James's change
******************************************************/
		ReturnRefBattleSP sister() { return d_sister; }
		ReturnCRefBattleSP sister() const { return d_sister; }
		ReturnRefBattleCP parent() { return d_parent; }
		ReturnCRefBattleCP parent() const { return d_parent; }
		const ISP& generic() const { return d_generic; }
		const ISP& genericCheck() const { ASSERT(d_generic != NoStrengthPoint); return d_generic; }

		void parent(ParamRefBattleCP p) { d_parent = p; }
		void sister(RefBattleSP sp) { d_sister = sp; }
//		void generic(const ISP& g) { d_generic = g; }

		void setUnitType(UnitType t) { genericCheck()->setUnitType(t); }
		UnitType getUnitType() const { return genericCheck()->getUnitType(); }

		BATDATA_DLL void setFormation(BOB_Definitions::SPFormation form);
			// Immediately set formation
		BATDATA_DLL void changeFormation(BOB_Definitions::SPFormation form);
			// Start a new formation change
		BATDATA_DLL BOB_Definitions::SPFormation formation() const;
			// Get current logical formation (including Changing)
		FormationTween formationTween() const { return d_formationTween; }
			// Get tween value
		BOB_Definitions::SPFormation fromFormation() const { return d_formation; }
			// Get original formation
		BOB_Definitions::SPFormation destFormation() const { return d_newFormation; }
			// Get desired formation
		BATDATA_DLL void addTween(FormationTween::Value change);
			// Update tween value
		void setTween(FormationTween::Value value) { d_formationTween = value; }

		int strength(int range) const { return d_strength.value(range); }
		int strength() const { return d_strength.value(); }
		bool applyLoss(UBYTE loss) { return d_strength.remove(loss); }
		void setStrength(UBYTE str) { d_strength = str; }
		/*
		 * Implement BattleUnit
		 */

		virtual void draw(UnitDrawer* drawer) const;

		/*
		 * File Interface
		 */

		Boolean readData(FileReader& f, OrderBattle& ob);
		Boolean writeData(FileWriter& f, OrderBattle& ob) const;

	private:
		static const UWORD s_fileVersion;

		RefBattleSP		         d_sister;
		RefBattleCP		         d_parent;
		ISP				         d_generic;

		/*
		 * SP specific information
		 *
		 * Some of this could be moved up to the division
		 * because all of an SP act the same
		 * e.g.
		 *		they all have the same facings
		 * 	they all have the same formations
		 *
		 * Is this true?  What about mixed divisions, where infantry are
		 * in square formation, but cavalry can not form square.
		 * Ben says mixed divisions are illegal.
		 *
		 * What if a division is attacked from several sides?  Do all
		 * SPs fight the same attacker, or can they individually turn to
		 * fire at neighbours?
		 *
		 * Leaving all this info here makes it more flexible should we
		 * ever decide to allow SPs to be mixed or individually controlled
		 */

		/*
		 * SP Formation:
		 *		tween value is non zero if changing formation
		 *		1=just started changing, 255=almost finished
		 *		128 = half way between values
		 */

		BOB_Definitions::SPFormation		d_formation;
		BOB_Definitions::SPFormation		d_newFormation;
		FormationTween	                d_formationTween;
		SPStrength		                  d_strength;		    // 0 = Dead, 100=Full strength

};

class BattleSPContainer
{
		static const UWORD s_fileVersion;
		// Private functions to disable copying

		BattleSPContainer(const BattleSPContainer&);
		BattleSPContainer& operator = (const BattleSPContainer&);

	public:
		BattleSPContainer();
		~BattleSPContainer();

		ReturnRefBattleSP create(ISP& isp);
		ReturnRefBattleSP create(const BattleSP& sp, ISP& isp);
			// Create a new item

		void reset();
			// Delete All Items

		void removeSP(SPIndex spi) { d_items.remove(spi); }
		SPIndex getIndex(BattleSP * sp) { return d_items.getIndex(sp); }

		int entries() { return d_items.entries(); }
			// Return how many items there are

		/*
		 * File Interface
		 */

		Boolean readData(FileReader& f, OrderBattle& ob);
		Boolean writeData(FileWriter& f, OrderBattle& ob) const;

	private:
		PoolArray<BattleSP> d_items;
};

#endif /* BOB_SP_HPP */

/*
 *----------------------------------------------------------------------
 * $Log$
 *----------------------------------------------------------------------
 */
