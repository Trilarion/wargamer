# Microsoft Developer Studio Project File - Name="batdata" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Dynamic-Link Library" 0x0102

CFG=batdata - Win32 Editor Debug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "batdata.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "batdata.mak" CFG="batdata - Win32 Editor Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "batdata - Win32 Release" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE "batdata - Win32 Debug" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE "batdata - Win32 Editor Debug" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE "batdata - Win32 Editor Release" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""
CPP=cl.exe
MTL=midl.exe
RSC=rc.exe

!IF  "$(CFG)" == "batdata - Win32 Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "..\..\exe"
# PROP Intermediate_Dir "o\release"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MT /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "BATDATA_EXPORTS" /YX /FD /c
# ADD CPP /nologo /MD /W3 /Gi /GR /GX /Zi /O2 /I "..\h" /I "..\..\res" /I "..\system" /I "..\gamesup" /I "..\ob" /D "NDEBUG" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "EXPORT_BATDATA_DLL" /YX"stdinc.hpp" /FD /c
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x809 /d "NDEBUG"
# ADD RSC /l 0x809 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /dll /machine:I386
# ADD LINK32 User32.lib winmm.lib /nologo /dll /debug /machine:I386

!ELSEIF  "$(CFG)" == "batdata - Win32 Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "..\..\exe"
# PROP Intermediate_Dir "o\debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MTd /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "BATDATA_EXPORTS" /YX /FD /GZ /c
# ADD CPP /nologo /MDd /W3 /Gm /Gi /GR /GX /ZI /Od /I "..\h" /I "..\..\res" /I "..\system" /I "..\gamesup" /I "..\ob" /D "_DEBUG" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "EXPORT_BATDATA_DLL" /YX"stdinc.hpp" /FD /GZ /c
# ADD BASE MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x809 /d "_DEBUG"
# ADD RSC /l 0x809 /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /dll /debug /machine:I386 /pdbtype:sept
# ADD LINK32 User32.lib winmm.lib /nologo /dll /debug /machine:I386 /out:"..\..\exe/batdataDB.dll" /pdbtype:sept

!ELSEIF  "$(CFG)" == "batdata - Win32 Editor Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "batdata___Win32_Editor_Debug"
# PROP BASE Intermediate_Dir "batdata___Win32_Editor_Debug"
# PROP BASE Ignore_Export_Lib 0
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "..\..\exe"
# PROP Intermediate_Dir "o\Editor\Debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MDd /W3 /Gm /Gi /GR /GX /ZI /Od /I "..\h" /I "..\..\res" /I "..\system" /I "..\gamesup" /I "..\ob" /D "_DEBUG" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "EXPORT_BATDATA_DLL" /YX"stdinc.hpp" /FD /GZ /c
# ADD CPP /nologo /MDd /W3 /Gm /Gi /GR /GX /ZI /Od /I "..\h" /I "..\..\res" /I "..\system" /I "..\gamesup" /I "..\ob" /D "_USRDLL" /D "EXPORT_BATDATA_DLL" /D "_DEBUG" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /D "EDITOR" /YX"stdinc.hpp" /FD /GZ /c
# ADD BASE MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x809 /d "_DEBUG"
# ADD RSC /l 0x809 /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 User32.lib winmm.lib /nologo /dll /debug /machine:I386 /out:"..\..\exe/batdataDB.dll" /pdbtype:sept
# ADD LINK32 User32.lib winmm.lib /nologo /dll /debug /machine:I386 /out:"..\..\exe/batdataEDDB.dll" /pdbtype:sept

!ELSEIF  "$(CFG)" == "batdata - Win32 Editor Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "batdata___Win32_Editor_Release"
# PROP BASE Intermediate_Dir "batdata___Win32_Editor_Release"
# PROP BASE Ignore_Export_Lib 0
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "..\..\exe"
# PROP Intermediate_Dir "o\Editor\Release"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MD /W3 /Gi /GR /GX /Zd /O2 /I "..\h" /I "..\..\res" /I "..\system" /I "..\gamesup" /I "..\ob" /D "NDEBUG" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "EXPORT_BATDATA_DLL" /YX"stdinc.hpp" /FD /c
# ADD CPP /nologo /MD /W3 /Gi /GR /GX /Zd /O2 /I "..\h" /I "..\..\res" /I "..\system" /I "..\gamesup" /I "..\ob" /D "NDEBUG" /D "_USRDLL" /D "EXPORT_BATDATA_DLL" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /D "EDITOR" /YX"stdinc.hpp" /FD /c
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x809 /d "NDEBUG"
# ADD RSC /l 0x809 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 User32.lib winmm.lib /nologo /dll /debug /machine:I386
# ADD LINK32 User32.lib winmm.lib /nologo /dll /debug /machine:I386 /out:"..\..\exe/batdataED.dll"

!ENDIF 

# Begin Target

# Name "batdata - Win32 Release"
# Name "batdata - Win32 Debug"
# Name "batdata - Win32 Editor Debug"
# Name "batdata - Win32 Editor Release"
# Begin Group "Source Files"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat"
# Begin Source File

SOURCE=.\b_los.cpp
# End Source File
# Begin Source File

SOURCE=.\b_morale.cpp
# End Source File
# Begin Source File

SOURCE=.\b_ob_ran.cpp
# End Source File
# Begin Source File

SOURCE=.\b_send.cpp
# End Source File
# Begin Source File

SOURCE=.\b_sounds.cpp
# End Source File
# Begin Source File

SOURCE=.\b_tables.cpp
# End Source File
# Begin Source File

SOURCE=.\b_terr.cpp
# End Source File
# Begin Source File

SOURCE=.\batarmy.cpp
# End Source File
# Begin Source File

SOURCE=.\batcord.cpp
# End Source File
# Begin Source File

SOURCE=.\batdata.cpp
# End Source File
# Begin Source File

SOURCE=.\batlist.cpp
# End Source File
# Begin Source File

SOURCE=.\batmsg.cpp
# End Source File
# Begin Source File

SOURCE=.\batord.cpp
# End Source File
# Begin Source File

SOURCE=.\batsound.cpp
# End Source File
# Begin Source File

SOURCE=.\battime.cpp
# End Source File
# Begin Source File

SOURCE=.\batunit.cpp
# End Source File
# Begin Source File

SOURCE=.\bcreate.cpp
# End Source File
# Begin Source File

SOURCE=.\bob_cp.cpp
# End Source File
# Begin Source File

SOURCE=.\bob_sp.cpp
# End Source File
# Begin Source File

SOURCE=.\bob_vis.cpp
# End Source File
# Begin Source File

SOURCE=.\bobiter.cpp
# End Source File
# Begin Source File

SOURCE=.\boblog.cpp
# End Source File
# Begin Source File

SOURCE=.\bobutil.cpp
# End Source File
# Begin Source File

SOURCE=.\bordutil.cpp
# End Source File
# Begin Source File

SOURCE=.\breinfor.cpp
# End Source File
# Begin Source File

SOURCE=.\bshockl.cpp
# End Source File
# Begin Source File

SOURCE=.\btimctrl.cpp
# End Source File
# Begin Source File

SOURCE=.\building.cpp
# End Source File
# Begin Source File

SOURCE=.\corpdepl.cpp
# End Source File
# Begin Source File

SOURCE=.\fractmap.cpp
# End Source File
# Begin Source File

SOURCE=.\fx.cpp
# End Source File
# Begin Source File

SOURCE=.\hexdata.cpp
# End Source File
# Begin Source File

SOURCE=.\hexlist.cpp
# End Source File
# Begin Source File

SOURCE=.\hexmap.cpp
# End Source File
# Begin Source File

SOURCE=.\initunit.cpp
# End Source File
# Begin Source File

SOURCE=.\key_terr.cpp
# End Source File
# Begin Source File

SOURCE=.\maketer.cpp
# End Source File
# Begin Source File

SOURCE=.\pathfind.cpp
# End Source File
# Begin Source File

SOURCE=.\pdeploy.cpp
# End Source File
# Begin Source File

SOURCE=.\sqrt.cpp
# End Source File
# Begin Source File

SOURCE=.\tresult.cpp
# End Source File
# Begin Source File

SOURCE=.\wavefile.cpp
# End Source File
# End Group
# Begin Group "Header Files"

# PROP Default_Filter "h;hpp;hxx;hm;inl"
# Begin Source File

SOURCE=.\animinfo.hpp
# End Source File
# Begin Source File

SOURCE=.\b_gindex.hpp
# End Source File
# Begin Source File

SOURCE=.\b_los.hpp
# End Source File
# Begin Source File

SOURCE=.\b_morale.hpp
# End Source File
# Begin Source File

SOURCE=.\b_ob_ran.hpp
# End Source File
# Begin Source File

SOURCE=.\b_send.hpp
# End Source File
# Begin Source File

SOURCE=.\b_sounds.hpp
# End Source File
# Begin Source File

SOURCE=.\b_tables.hpp
# End Source File
# Begin Source File

SOURCE=.\b_terr.hpp
# End Source File
# Begin Source File

SOURCE=.\batarmy.hpp
# End Source File
# Begin Source File

SOURCE=.\batcord.hpp
# End Source File
# Begin Source File

SOURCE=.\batctrl.hpp
# End Source File
# Begin Source File

SOURCE=.\batdata.hpp
# End Source File
# Begin Source File

SOURCE=.\batlist.hpp
# End Source File
# Begin Source File

SOURCE=.\batmsg.hpp
# End Source File
# Begin Source File

SOURCE=.\batord.hpp
# End Source File
# Begin Source File

SOURCE=.\batsound.hpp
# End Source File
# Begin Source File

SOURCE=.\battime.hpp
# End Source File
# Begin Source File

SOURCE=.\BattleDataFwd.hpp
# End Source File
# Begin Source File

SOURCE=.\batunit.hpp
# End Source File
# Begin Source File

SOURCE=.\bcreate.hpp
# End Source File
# Begin Source File

SOURCE=.\bd_dll.h
# End Source File
# Begin Source File

SOURCE=.\bob_cp.hpp
# End Source File
# Begin Source File

SOURCE=.\bob_sp.hpp
# End Source File
# Begin Source File

SOURCE=.\bob_vis.hpp
# End Source File
# Begin Source File

SOURCE=.\bobdef.hpp
# End Source File
# Begin Source File

SOURCE=.\bobiter.hpp
# End Source File
# Begin Source File

SOURCE=.\boblog.hpp
# End Source File
# Begin Source File

SOURCE=.\bobutil.hpp
# End Source File
# Begin Source File

SOURCE=.\bordutil.hpp
# End Source File
# Begin Source File

SOURCE=.\breinfor.hpp
# End Source File
# Begin Source File

SOURCE=.\bshockl.hpp
# End Source File
# Begin Source File

SOURCE=.\btimctrl.hpp
# End Source File
# Begin Source File

SOURCE=.\bu_draw.hpp
# End Source File
# Begin Source File

SOURCE=.\building.hpp
# End Source File
# Begin Source File

SOURCE=.\corpdepl.hpp
# End Source File
# Begin Source File

SOURCE=.\fractmap.hpp
# End Source File
# Begin Source File

SOURCE=.\fx.hpp
# End Source File
# Begin Source File

SOURCE=.\g_build.hpp
# End Source File
# Begin Source File

SOURCE=.\g_terr.hpp
# End Source File
# Begin Source File

SOURCE=.\g_troops.hpp
# End Source File
# Begin Source File

SOURCE=.\hexdata.hpp
# End Source File
# Begin Source File

SOURCE=.\hexlist.hpp
# End Source File
# Begin Source File

SOURCE=.\hexmap.hpp
# End Source File
# Begin Source File

SOURCE=.\initunit.hpp
# End Source File
# Begin Source File

SOURCE=.\key_terr.hpp
# End Source File
# Begin Source File

SOURCE=.\maketer.hpp
# End Source File
# Begin Source File

SOURCE=.\pathfind.hpp
# End Source File
# Begin Source File

SOURCE=.\pdeploy.hpp
# End Source File
# Begin Source File

SOURCE=.\sqrt.hpp
# End Source File
# Begin Source File

SOURCE=.\stdinc.hpp
# End Source File
# Begin Source File

SOURCE=.\tresult.hpp
# End Source File
# Begin Source File

SOURCE=.\wavefile.hpp
# End Source File
# End Group
# Begin Group "Resource Files"

# PROP Default_Filter "ico;cur;bmp;dlg;rc2;rct;bin;rgs;gif;jpg;jpeg;jpe"
# End Group
# End Target
# End Project
