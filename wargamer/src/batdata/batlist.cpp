/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
#include "stdinc.hpp"
#include "batlist.hpp"
#include "fsalloc.hpp"
#include "filebase.hpp"
#include "ob.hpp"
#include "obdefs.hpp"
#include "bob_cp.hpp"
#include "resstr.hpp"
#include "res_str.h"

/*-------------------------------------------------------------------
 * HexList class, used for storing routes, deployment hexes, etc.
 */

const int chunkSize = 20;

#ifdef DEBUG
static FixedSize_Allocator itemAlloc(sizeof(BattleItem), chunkSize, "BattleItem");
#else
static FixedSize_Allocator itemAlloc(sizeof(BattleItem), chunkSize);
#endif

void* BattleItem::operator new(size_t size)
{
   ASSERT(size == sizeof(BattleItem));
  return itemAlloc.alloc(size);
}

#ifdef _MSC_VER
void BattleItem::operator delete(void* deadObject)
{
//   ASSERT(size == sizeof(BattleItem));
  itemAlloc.free(deadObject, sizeof(BattleItem));
}
#else
void BattleItem::operator delete(void* deadObject, size_t size)
{
   ASSERT(size == sizeof(BattleItem));
  itemAlloc.free(deadObject, size);
}
#endif

/*
 * File Interface
 */

const UWORD BattleItem::s_fileVersion = 0x0003;
Boolean BattleItem::readData(FileReader& f, OrderBattle* ob)
{
  UWORD version;
  f >> version;
  ASSERT(version <= s_fileVersion);

  CPIndex cpi;
  f >> cpi;

  d_cp = (cpi != NoCPIndex) ? battleCP(ob->command(cpi)) : NoBattleCP;

  f >> d_range;
  f >> d_fireValue;
  f >> d_spsFiring;

  if(version >= 0x0001)
  {
    UBYTE b;
    f >> b;
    d_position = static_cast<Position>(b);
  }

  if(version >= 0x0002)
  {
    f >> d_flags;
    f >> d_lastFireValue;
  }

  if(version >= 0x0003)
  {
    UBYTE b;
    f >> b;
    d_lastPosition = static_cast<Position>(b);
  }

   return f.isOK();
}

Boolean BattleItem::writeData(FileWriter& f, OrderBattle* ob) const
{
   f << s_fileVersion;

   CPIndex cpi = (d_cp != NoBattleCP) ? ob->getIndex(d_cp->generic()) : NoCPIndex;
   f << cpi;

   f << d_range;
   f << d_fireValue;
   f << d_spsFiring;

   UBYTE b = d_position;
   f << b;

   f << d_flags;
   f << d_lastFireValue;

   b = d_lastPosition;
   f << b;
   return f.isOK();
}

const TCHAR* BattleItem::positionText(BattleItem::Position pos)
{
    ASSERT(pos < Position_HowMany);

//    // TODO: get from resource
//    static const TCHAR* s_posTxt[Position_HowMany] = {
//       "Front",
//       "Right Front",
//       "Left Front",
//       "Right Flank",
//       "Left Flank",
//       "Rear",
//       "Right Rear",
//       "Left Rear"
//    };
//
//    return s_posTxt[pos];

   static const int s_posID[Position_HowMany] = {
      IDS_Front,
      IDS_RightFront,
      IDS_LeftFront,
      IDS_RightFlank,
      IDS_LeftFlank,
      IDS_Rear,
      IDS_RightRear,
      IDS_LeftRear
   };

   static ResourceStrings s_posStr(s_posID, Position_HowMany);

   return s_posStr.get(pos);
}

//-------------------------------------------------------------------------
BattleList& BattleList::operator = (const BattleList& hl)
{
   // reset list
   reset();

   // copy
   SListIterR<BattleItem> iter(&hl);
   while(++iter)
   {
    BattleItem* hi = new BattleItem(*iter.current());
    ASSERT(hi);

    append(hi);
    }

   d_closeRange = hl.d_closeRange;
   d_closeRangeFront = hl.d_closeRangeFront;
   d_closeRangeLeftFlank = hl.d_closeRangeLeftFlank;
    d_closeRangeRightFlank = hl.d_closeRangeRightFlank;
   d_closeRangeRear = hl.d_closeRangeRear;
   d_flags = hl.d_flags;

   return *this;
}


BattleItem* BattleList::find(const RefBattleCP& cp)
{
   SListIter<BattleItem> iter(this);
   while(++iter)
   {
    if(iter.current()->d_cp == cp)
      return iter.current();
   }

   return 0;
}

BattleItem* BattleList::newItem(const RefBattleCP& cp, const UWORD& r, BattleItem::Position pos)
{
      BattleItem* bi = new BattleItem(cp, r, pos);
      ASSERT(bi);

      if(r < d_closeRange)
         {
         d_closeRange = r;
         insert(bi);
      }
      else
             append(bi);

      switch(pos)
      {
         case BattleItem::Front:
         {
            if(r < d_closeRangeFront)
               d_closeRangeFront = r;

            break;
         }
         case BattleItem::RightFront:
         {
            if(r < d_closeRangeRightFront)
               d_closeRangeRightFront = r;

            break;
         }
         case BattleItem::LeftFront:
         {
            if(r < d_closeRangeLeftFront)
               d_closeRangeLeftFront = r;

            break;
         }
         case BattleItem::LeftFlank:
             {
            if(r < d_closeRangeLeftFlank)
               d_closeRangeLeftFlank = r;

            break;
             }
         case BattleItem::RightFlank:
         {
            if(r < d_closeRangeRightFlank)
               d_closeRangeRightFlank = r;

            break;
         }
         case BattleItem::Rear:
         {
            if(r < d_closeRangeRear)
               d_closeRangeRear = r;

            break;
         }
         case BattleItem::RightRear:
         {
            if(r < d_closeRangeRightRear)
               d_closeRangeRightRear = r;

            break;
         }
         case BattleItem::LeftRear:
         {
            if(r < d_closeRangeLeftRear)
               d_closeRangeLeftRear = r;

            break;
         }
#ifdef DEBUG
         default:
                  FORCEASSERT("Improper type in BattleList::newItem()");
#endif
         }

         return bi;
}

bool BattleList::addFireValue(const RefBattleCP& cp, const UWORD& r, const UWORD& fv, BattleItem::Position pos, bool countSP)
{
#if 0
    bool newCloseRange = False;
    if(r != UWORD_MAX && r < d_closeRange)
    {
         d_closeRange = r;
         d_flags |= Changed;
         newCloseRange = True;
    }
#endif
    switch(pos)
    {
             case BattleItem::Front:
             {
                  if(r < d_closeRangeFront)
                      d_closeRangeFront = r;

                  break;
             }
             case BattleItem::RightFront:
             {
                  if(r < d_closeRangeRightFront)
                      d_closeRangeRightFront = r;

                  break;
             }
             case BattleItem::LeftFront:
             {
                  if(r < d_closeRangeLeftFront)
                      d_closeRangeLeftFront = r;

                  break;
             }
             case BattleItem::LeftFlank:
             {
                  if(r < d_closeRangeLeftFlank)
                      d_closeRangeLeftFlank = r;

                  break;
             }
             case BattleItem::RightFlank:
             {
                  if(r < d_closeRangeRightFlank)
                      d_closeRangeRightFlank = r;

                  break;
             }
             case BattleItem::Rear:
             {
                  if(r < d_closeRangeRear)
                      d_closeRangeRear = r;

                  break;
             }
             case BattleItem::RightRear:
             {
                  if(r < d_closeRangeRightRear)
                      d_closeRangeRightRear = r;

                  break;
             }
             case BattleItem::LeftRear:
             {
                  if(r < d_closeRangeLeftRear)
                      d_closeRangeLeftRear = r;

                  break;
             }
#ifdef DEBUG
//         default:
 //           FORCEASSERT("Improper type in BattleList::newItem()");
#endif
   }

   bool found = False;
// bool added = False;

   SListIter<BattleItem> iter(this);
   while(++iter)
   {
      if(iter.current()->d_cp == cp)
      {
         BattleItem* bi = iter.current();

//       if( !(bi->d_flags & BattleItem::Added) )
//          added = True;

         bi->d_fireValue += fv;
         bi->d_flags |= BattleItem::Added;

#if 0
         if(pos != BattleItem::Position_HowMany &&
             bi->d_position != BattleItem::Front &&
             bi->d_position != pos)// || bi->d_position == BattleItem::Position_HowMany))
#else
         if(pos != BattleItem::Position_HowMany &&
            (bi->d_position <= BattleItem::LeftFront || bi->d_position == BattleItem::Position_HowMany) &&
            (pos > BattleItem::LeftFront || ( bi->d_position != BattleItem::Front && bi->d_position != pos) ) )// || bi->d_position == BattleItem::Position_HowMany))
#endif
         {
//          if(bi->d_position != BattleItem::Position_HowMany)
//             d_flags |= Changed; // Unchecked ammended

//                if(bi->d_position != BattleItem::Front)
               bi->d_position = pos;
         }

         if(r != UWORD_MAX && (bi->d_range == UWORD_MAX || r < bi->d_range))// != r)
         {
            bi->d_range = r;
//          d_flags |= Changed;
         }

//    if(fv > 0)
         if(bi->d_position == BattleItem::Front && countSP)
            bi->d_spsFiring++;

            if(bi->d_position != bi->d_lastPosition || bi->d_fireValue != bi->d_lastFireValue)
               d_flags |= Changed;

         found = True;
      }
   }

   if(!found)
   {
      // if not already in list, add to list
      ASSERT(r != UWORD_MAX);
      BattleItem* bi = new BattleItem(cp, r, fv, pos);
      ASSERT(bi);

      if(pos == BattleItem::Front && countSP)
         bi->d_spsFiring++;

      bi->d_flags |= BattleItem::Added;
      append(bi);
#if 0
      if(newCloseRange)
         insert(bi);
      else
         append(bi);

      d_flags |= Changed;
#endif
//    added = True;
   }

   return found;
//   return !added;
}

void BattleList::cleanUpList()
{
    // if fireValue does not equal last fire value
    // set changed flag
//  if(!changed() && d_lastFireValue != d_fireValue)
//  d_flags |= Changed;

    // remove any items that where not added this time
    SListIter<BattleItem> iter(this);
    while(++iter)
    {
         if(!iter.current()->added() ||
             iter.current()->d_cp->hasQuitBattle())
         {
             iter.remove();
         }
    }

    if(entries() == 0)
       d_closeRange = UWORD_MAX;
}

void BattleList::resetItems()
{
    d_flags &= ~Changed;
//   d_closeRange = UWORD_MAX;
    d_closeRangeFront = UWORD_MAX;
    d_closeRangeRightFront = UWORD_MAX;
    d_closeRangeLeftFront = UWORD_MAX;
    d_closeRangeLeftFlank = UWORD_MAX;
    d_closeRangeRightFlank = UWORD_MAX;
    d_closeRangeRear = UWORD_MAX;
    d_closeRangeRightRear = UWORD_MAX;
    d_closeRangeLeftRear = UWORD_MAX;

    SListIter<BattleItem> iter(this);
    while(++iter)
    {
         BattleItem* bi = iter.current();
         bi->d_flags &= ~BattleItem::Added;
         bi->d_lastFireValue = bi->d_fireValue;
            bi->d_lastPosition = bi->d_position;
         bi->d_fireValue = 0;
         bi->d_spsFiring = 0;
         bi->d_position = BattleItem::Position_HowMany;
            bi->d_range = UWORD_MAX;
    }
}

void BattleList::moveUpCloseUnit()
{
    // find closest front item in list and and move it to front of list
    UWORD bestRange = UWORD_MAX;
    BattleItem* bestItem = 0;
    SListIter<BattleItem> iter(this);
    while(++iter)
    {
         if(iter.current()->d_position == BattleItem::Front && iter.current()->d_range < bestRange)
         {
             bestItem = iter.current();
             bestRange = bestItem->d_range;
         }
    }

    // if we don;t have a front item, move up close item
    if(!bestItem)
    {
       iter.rewind();
       while(++iter)
       {
          if(iter.current()->d_range < bestRange)
          {
             bestItem = iter.current();
             bestRange = bestItem->d_range;
          }
       }
    }

    if(bestRange != d_closeRange)
    {
       d_closeRange = bestRange;
       d_flags |= Changed;
    }

    if(bestItem && bestItem != first())
    {
         BattleItem* bi = new BattleItem(*bestItem);
         ASSERT(bi);

         insert(bi);
         remove(bestItem);
    }
}

/*
 * File Interface
 */

const UWORD BattleList::s_fileVersion = 0x0001;
Boolean BattleList::readData(FileReader& f, OrderBattle* ob)
{
    UWORD version;
    f >> version;
    ASSERT(version <= s_fileVersion);

    if(version >= 0x0001)
   {
      f >> d_closeRange;
      f >> d_closeRangeFront;
      f >> d_closeRangeRightFront;
      f >> d_closeRangeLeftFront;
      f >> d_closeRangeRightFlank;
      f >> d_closeRangeLeftFlank;
      f >> d_closeRangeRear;
      f >> d_closeRangeRightRear;
      f >> d_closeRangeLeftRear;
   }

   int nItems;
   f >> nItems;
   ASSERT(nItems >= 0);
   while(nItems--)
   {
    BattleItem* bi = new BattleItem;
    ASSERT(bi);

    append(bi);

    if(!bi->readData(f, ob))
         return False;
   }

   return f.isOK();
}

Boolean BattleList::writeData(FileWriter& f, OrderBattle* ob) const
{
   f << s_fileVersion;

   f << d_closeRange;
   f << d_closeRangeFront;
   f << d_closeRangeRightFront;
   f << d_closeRangeLeftFront;
   f << d_closeRangeRightFlank;
   f << d_closeRangeLeftFlank;
   f << d_closeRangeRear;
   f << d_closeRangeRightRear;
   f << d_closeRangeLeftRear;

   int nItems = entries();
   f << nItems;

   BattleListIterR iter(this);
   while(++iter)
   {
    if(!iter.current()->writeData(f, ob))
      return False;
  }

  return f.isOK();
}



/*
 *----------------------------------------------------------------------
 * $Log$
 *----------------------------------------------------------------------
 */
