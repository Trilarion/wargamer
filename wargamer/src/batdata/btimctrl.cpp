/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 * Battle Time Controller class
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "btimctrl.hpp"
#include "myassert.hpp"
#include "registry.hpp"
#include "misc.hpp"
#include "scenario.hpp"

using BattleMeasure::BattleTime;

static const char battleRegName[] = "Battle";
static const char regTimeRate[] = "Timerate";
static const char regFreeze[] = "Freeze";

// static const int s_defaultRatio = 1;

BatTimeControl::BatTimeControl() :
   d_overflow(0),
   // d_totalTicks(0),
   d_ratio(0), // s_defaultRatio),
    d_frozen(false),
    d_jumping(0),
    d_tick(0)
{
    if (!getRegistry(regTimeRate, &d_ratio, sizeof(d_ratio), battleRegName))
    {
      d_ratio = scenario->getInt("BattleTimeRatioDefault");
    }

    // Force Ratio to legal value

   int minRate = scenario->getInt("BattleTimeRatioMinimum");
   int maxRate = scenario->getInt("BattleTimeRatioMaximum");
   if(d_ratio < minRate)
      d_ratio = minRate;
   if(d_ratio > maxRate)
      d_ratio = maxRate;


   if(!getRegistry(regFreeze, &d_frozen, sizeof(d_frozen), battleRegName))
      d_frozen = False;
}

BatTimeControl::~BatTimeControl()
{
   setRegistry(regTimeRate, &d_ratio,  sizeof(d_ratio),  battleRegName);
   setRegistry(regFreeze,   &d_frozen, sizeof(d_frozen), battleRegName);
}

BattleTime::Tick BatTimeControl::addTicks(SysTick::Value sysTicks)
{
    if(!isJumping() && d_frozen)
        return 0;
    else
    {
        BattleTime::Tick bTicks;
        if(isJumping())
        {
            // bTicks = BattleMeasure::seconds(10);
            bTicks = maximum(d_jumping - d_tick, BattleMeasure::minutes(1));


        }
        else
        {
           // d_totalTicks += sysTicks;
           SysTick::Value gameSysTicks = sysTicks + d_overflow;
           long numerator = gameSysTicks * BattleTime::TicksPerSecond;
           bTicks = numerator / SysTick::TicksPerSecond;
           d_overflow = numerator - bTicks * SysTick::TicksPerSecond;
           bTicks *= d_ratio;
        }

        d_tick += bTicks;
        return bTicks;
    }
}


void BatTimeControl::setRatio(int ratio)
{
   ASSERT(ratio > 0);
   d_ratio = ratio;
   d_overflow = 0;
}




/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.2  2001/06/18 08:25:43  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
