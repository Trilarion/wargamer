/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef BATARMY_HPP
#define BATARMY_HPP

#ifndef __cplusplus
#error batarmy.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *      Battle Game: Order of Battle
 *
 * Builds on top of Generic Order of Battle
 *
 *----------------------------------------------------------------------
 */

#include "bd_dll.h"
#include "ob.hpp"
#include "bob_sp.hpp"
#include "bob_cp.hpp"
#include "breinfor.hpp"

/*
 * Overall Order of Battle
 */

class BattleOB
{
        // Private functions to prevent copying
        BattleOB(const BattleOB&);
        const BattleOB& operator = (const BattleOB&);
    public:

        /*
        * Public Functions
        */


        BATDATA_DLL BattleOB(OrderBattle* ob);
        BATDATA_DLL ~BattleOB();

        BATDATA_DLL bool readData(FileReader& f);
        BATDATA_DLL bool writeData(FileWriter& f) const;


        void startRead() const { d_ob->startRead(); }
        void endRead() const { d_ob->endRead(); }

        void startWrite() { d_ob->startWrite(); }
        void endWrite() { d_ob->endWrite(); }

        class ReadLock : public OrderBattle::ReadLock
        {
            public:
                ReadLock(const BattleOB* ob) : OrderBattle::ReadLock(ob->ob()) { }
        };
        
        class WriteLock : public OrderBattle::WriteLock
        {
            public:
                WriteLock(BattleOB* ob) : OrderBattle::WriteLock(ob->ob()) { }
        };



        BATDATA_DLL void deleteGenericOB();
        // delete d_ob;


        BATDATA_DLL ReturnRefBattleCP createUnit();
        // Create a new Unit
        BATDATA_DLL void createLeader(ParamRefBattleCP cp);
        // Make a leader and assign to a unit
        BATDATA_DLL void makeUnitName(ParamRefBattleCP cp);
        // Set up units name

        Side sideCount() const { return d_nSides; }

        BATDATA_DLL void addChild(ParamRefBattleCP parent, ParamRefBattleCP child);

        BATDATA_DLL ReturnRefBattleSP createStrengthPoint();
        BATDATA_DLL void attachStrengthPoint(ParamRefBattleCP cpi, ParamRefBattleSP sp);
        SPIndex getIndex(const ParamCRefBattleSP sp) const { return d_ob->getIndex(sp->generic()); }
        const char* spName(const ParamCRefBattleSP sp) const { return d_ob->spName(sp->generic()); }

        const UnitTypeTable& getUnitTypes() const { return d_ob->getUnitTypes(); }
        int getMaxUnitType() const { return d_ob->getMaxUnitType(); }
        const UnitTypeItem& getUnitType(UnitType n) const { return d_ob->getUnitType(n); }
        const UnitTypeItem& getUnitType(const ConstISP& isp) const { return d_ob->getUnitType(isp->getUnitType()); }
        const UnitTypeItem& getUnitType(const ParamCRefBattleSP sp) const { return d_ob->getUnitType(sp->generic()->getUnitType()); }

        BATDATA_DLL ReturnRefBattleCP getTop(Side side);
        BATDATA_DLL ReturnCRefBattleCP getTop(Side side) const;
				        // Get first unit of given side

        BATDATA_DLL ReturnRefBattleCP makeCP(RefGenericCP gcp);
        BATDATA_DLL ReturnRefBattleSP makeSP(ISP gSP);
        OrderBattle*  ob() { return d_ob; }
        const OrderBattle* ob() const { return d_ob; }
        BATDATA_DLL void removeBattleUnits(bool ownsGeneric = false);
        BATDATA_DLL void removeBattleUnits(ParamRefBattleCP cp, bool ownsGeneric = false);
        BATDATA_DLL void removeStrengthPoints(RefBattleCP cp);
        BATDATA_DLL void removeStrengthPoint(RefBattleSP sp);

        B_ReinforceList& reinforceList() { return d_reinforceList; }
        const B_ReinforceList& reinforceList() const { return d_reinforceList; }

        void addReinforcement(const RefBattleCP& cp, BattleMeasure::BattleTime::Tick tick, BattleMeasure::HexCord hex)
        {
            d_reinforceList.addItem(cp, tick, hex);
        }

		/*
		 * Private Functions
		 */

		void setTop(Side side, ParamRefBattleCP cp);
						// Set first unit of a given side

		BATDATA_DLL void unlinkUnitAndChildren(ParamRefBattleCP cp);
		BATDATA_DLL void unlinkSister(ParamRefBattleCP sister, ParamRefBattleCP cp);
		BATDATA_DLL void unlinkUnit(ParamRefBattleCP cp);
		BATDATA_DLL void deleteAllUnits();


      BATDATA_DLL void removeOrphans();

    private:

        /*
         * Private Data
         */


        class BattleSideInfo
        {
	        public:
			    BattleSideInfo();
			    ~BattleSideInfo();

			    void set(ParamRefBattleCP cp) { d_top = cp; }
			    ReturnRefBattleCP get() { return d_top; }
			    ReturnCRefBattleCP get() const { return d_top; }

			    bool readData(FileReader& f, OrderBattle* cpList);
			    bool writeData(FileWriter& f) const;

	        private:
				RefBattleCP     d_top;
        };


        OrderBattle*            d_ob;               // Generic Order of Battle
        BattleCPList            d_cpList;           // Battle CommandPosition Information
        BattleSPContainer       d_spList;           // StrengthPoints
        B_ReinforceList         d_reinforceList;    // Unchecked by Paul

        Side                    d_nSides;
        BattleSideInfo*         d_sides;

        static const UWORD      s_fileVersion;
        static const char       s_fileID[];
};


#endif /* BATARMY_HPP */

/*-------------------------------------------------------------------------------
 * $Log$
 * Revision 1.1  2001/06/13 08:52:36  greenius
 * Initial Import
 *
 *-------------------------------------------------------------------------------
 */