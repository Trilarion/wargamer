/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *      Battle Command Positions
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "bob_cp.hpp"
#include "bob_sp.hpp"
#include "bu_draw.hpp"
#include "ob.hpp"
#include "filebase.hpp"

#ifdef DEBUG
#include "boblog.hpp"
#endif

/*------------------------------------------------------
 * File interface for DeployItem
 */

const UWORD DeployItem::s_fileVersion = 0x0000;

Boolean DeployItem::readData(FileReader& f, OrderBattle& ob)
{
  UWORD version;
  f >> version;
  ASSERT(version <= s_fileVersion);

  SPIndex spi;
  f >> spi;
  ISP sp = ob.sp(spi);
  d_sp = (spi != NoSPIndex) ? battleSP(sp) : NoBattleSP;

  f >> spi;
  sp = ob.sp(spi);
  d_wantSP = (spi != NoSPIndex) ? battleSP(sp) : NoBattleSP;

  if(!d_hex.readData(f))
         return False;

  if(!d_wantHex.readData(f))
         return False;

  return f.isOK();
}

Boolean DeployItem::writeData(FileWriter& f, OrderBattle& ob) const
{
  f << s_fileVersion;


  SPIndex spi;
  if(d_sp) {
      if(d_sp->generic() != NoStrengthPoint) spi = ob.getIndex(d_sp->generic() );
      else spi = NoSPIndex;
  }
  else spi = NoSPIndex;
  f << spi;

  if(d_wantSP) {
      if(d_wantSP->generic() != NoStrengthPoint) spi = ob.getIndex(d_wantSP->generic() );
      else spi = NoSPIndex;
  }
  else spi = NoSPIndex;
  f << spi;

   if(!d_hex.writeData(f))
             return False;

   if(!d_wantHex.writeData(f))
             return False;

   return f.isOK();
}

/*------------------------------------------------------
 * File interface for DeployMap
 */

const UWORD BattleCP::DeployMap::s_fileVersion = 0x0001;

Boolean BattleCP::DeployMap::readData(FileReader& f, OrderBattle& ob)
{
  UWORD version;
  f >> version;
  ASSERT(version <= s_fileVersion);

  int nItems;
  f >> nItems;

#ifdef LEIPZIG_FIX
  if(version < 0x0001)              // version 0 saved deployment wrong
    nItems = (nItems + 1) & ~1;     // round up to even value
#endif

   d_map.resize(nItems);

  // read map
   for(std::vector<DeployItem>::iterator di = d_map.begin();
                di != d_map.end();
                di++)
  {
         di->readData(f, ob);
  }

  // read other data
  if(!d_wantCPHex.readData(f))
         return False;

  f >> d_columns;
  f >> d_rows;
  f >> d_wantColumns;
  f >> d_wantRows;
  f >> d_nArtillery;

  return f.isOK();
}

Boolean BattleCP::DeployMap::writeData(FileWriter& f, OrderBattle& ob) const
{
  f << s_fileVersion;

  int nItems = d_map.size();   // d_columns * d_rows;
  f << nItems;

  // write map
  for(std::vector<DeployItem>::const_iterator di = d_map.begin();
                di != d_map.end();
                di++)
  {
         di->writeData(f, ob);
  }

  // write other data
  if(!d_wantCPHex.writeData(f))
         return False;

  f << d_columns;
  f << d_rows;
  f << d_wantColumns;
  f << d_wantRows;
  f << d_nArtillery;

  return f.isOK();
}

/*
 * Command Position Functions
 */


using namespace BOB_Definitions;
using namespace BattleMeasure;

/*
 * Default Constructor
 */

BattleCP::BattleCP() :
        d_generic(NoGenericCP),
        d_used(false),
        d_parent(NoBattleCP),
        d_sister(NoBattleCP),
        d_child(NoBattleCP),
        d_sp(NoBattleSP),
        d_startMorale(Morale::MaxValue),
        d_startStrength(0),
        d_lastStrength(0),
        d_ammoSupply(AmmoSupply::MaxValue),
        d_disorder(Disorder::MaxValue),
        d_status(Active),
        d_nearEnemyMode(NE_Default),
        d_lockMode(False),
        d_formation(CPF_March),
        d_nextFormation(CPF_March),
        d_aggression(BattleOrderInfo::Aggress_Default),
        d_posture(BattleOrderInfo::Posture_Default),
        d_orderMode(BattleOrderInfo::OM_Default),
        d_currentOrder(),
        d_actingOrder(),
        d_orderList(),
        d_moveMode(MoveMode_Default),
        d_nextFire(0),
        d_nextShockCheck(0),
        d_lockUntil(0),
        d_nextOrderCheck(0),
        d_targets(),
        d_targetCP(NoBattleCP),
        d_companionSP(NoBattleSP),
        d_moveDest(),
        d_entryPoint(REP_None),
        d_deployMap(),
        d_hexOffset(HO_Left),
        d_attachStatus(HQ_AttachStatus_Default)
{
#ifdef DEBUG
        bobLog.printf("BattleCP Constructor");
#endif
}

/*
 * Destructor
 */

BattleCP::~BattleCP()
{
#ifdef DEBUG
        bobLog.printf("BattleCP Destructor");
#endif
        if(d_generic != NoGenericCP)
                d_generic->battleInfo(0);
}

#if 0
/*
 * Copy Constructor...
 */

BattleCP::BattleCP(const BattleCP& cp) :
   d_generic(cp.d_generic),
   d_used(cp.d_used),
   d_parent(cp.d_parent),
   d_sister(cp.d_sister),
   d_child(cp.d_child),
   d_sp(cp.d_sp),
   // d_morale(cp.d_morale),
   d_startMorale(cp.d_startMorale),
   d_startStrength(cp.d_startStrength),
   d_lastStrength(cp.d_lastStrength),
   // d_fatigue(cp.d_fatigue),
   d_ammoSupply(cp.d_ammoSupply),
   d_disorder(cp.d_disorder),
   d_status(cp.d_status),
   d_nearEnemyMode(cp.d_nearEnemyMode),
   d_lockMode(cp.d_lockMode),
   d_formation(cp.d_formation),
   d_nextFormation(cp.d_nextFormation),
   d_aggression(cp.d_aggression),
   d_posture(cp.d_posture),
   d_targetCP(NoBattleCP),
   d_orderMode(cp.d_orderMode),

   d_currentOrder(cp.d_currentOrder),
   d_actingOrder(cp.d_actingOrder),
   d_orderList(cp.d_orderList),

   d_moveMode(cp.d_moveMode),
   d_nextFire(cp.d_nextFire),
   d_nextShockCheck(cp.d_nextShockCheck),
   d_lockUntil(cp.d_lockUntil),
   d_nextOrderCheck(cp.d_nextOrderCheck),

   d_targets(cp.d_targets),
   d_companionSP(cp.d_companionSP),
   d_moveDest(cp.d_moveDest),
   d_entryPoint(cp.d_entryPoint),

   d_deployMap(cp.d_deployMap),
   d_hexOffset(cp.d_hexOffset),
   d_attachStatus(cp.d_attachStatus)
{
#ifdef DEBUG
            bobLog.printf("BattleCP Copy Constructor");
#endif
}

/*
 * Assignment...
 */

const BattleCP& BattleCP::operator = (const BattleCP& cp)
{
#ifdef DEBUG
            bobLog.printf("BattleCP := BattleCP");
#endif

   // Should only be used to set new items, not overwriting

   ASSERT(d_generic() == 0);
   ASSERT(!d_used);

   d_generic =cp.d_generic;
   d_used = cp.d_used;
   d_parent = cp.d_parent;
   d_sister = cp.d_sister;
   d_child = cp.d_child;
   d_sp = cp.d_sp;
   d_startMorale = cp.d_startMorale;
   d_startStrength = cp.d_startStrength;
   d_lastStrength = cp.d_lastStrength;
   d_ammoSupply = cp.d_ammoSupply;
   d_disorder = cp.d_disorder;
   d_status = cp.d_status;
   d_nearEnemyMode = cp.d_nearEnemyMode;
   d_lockMode = cp.d_lockMode;
   d_formation = cp.d_formation;
   d_nextFormation = cp.d_nextFormation;
   d_aggression = cp.d_aggression;
   d_posture = cp.d_posture;
   d_targetCP = NoBattleCP;
   d_orderMode = cp.d_orderMode;

   d_currentOrder = cp.d_currentOrder;
   d_actingOrder = cp.d_actingOrder;
   d_orderList = cp.d_orderList;

   d_moveMode = cp.d_moveMode;
   d_nextFire = cp.d_nextFire;
   d_nextShockCheck = cp.d_nextShockCheck;
   d_lockUntil = cp.d_lockUntil;
   d_nextOrderCheck = cp.d_nextOrderCheck;

   d_targets = cp.d_targets;
   d_companionSP = cp.d_companionSP;
   d_moveDest = cp.d_moveDest;
   d_entryPoint = cp.d_entryPoint;

   d_deployMap = cp.d_deployMap;
   d_hexOffset = cp.d_hexOffset;
   d_attachStatus = cp.d_attachStatus;

         
#if 0 // old version         
         d_generic = cp.d_generic;
            d_used = cp.d_used;
            d_parent = cp.d_parent;
            d_sister = cp.d_sister;
            d_child = cp.d_child;

            d_sp = cp.d_sp;
            d_formation = cp.d_formation;
            d_nextFormation = cp.d_nextFormation;
            d_targetCP = cp.d_targetCP;

//      d_deployWhichWay = cp.d_deployWhichWay;
            d_hexOffset = cp.d_hexOffset;
#endif
            return *this;
}
#endif

bool operator < (const BattleCP& l, const BattleCP& r)
{
            return l.d_generic() < r.d_generic();
}

bool operator == (const BattleCP& l, const BattleCP& r)
{
            return l.d_generic() == r.d_generic();
}

void BattleCP::init(const RefGenericCP& cp)
{
            generic(cp);
}

void BattleCP::parent(ParamRefBattleCP cp)
{
            d_parent = cp;
}

void BattleCP::sister(ParamRefBattleCP cp)
{
            d_sister = cp;
}

void BattleCP::child(ParamRefBattleCP cp)
{
            d_child = cp;
}

CPIndex BattleCP::self() const { return generic()->getSelf(); }

ReturnRefBattleCP BattleCP::parent() { return d_parent; }
ReturnRefBattleCP BattleCP::sister() { return d_sister; }
ReturnRefBattleCP BattleCP::child() { return d_child; }

ReturnCRefBattleCP BattleCP::parent() const { return d_parent; }
ReturnCRefBattleCP BattleCP::sister() const { return d_sister; }
ReturnCRefBattleCP BattleCP::child() const { return d_child; }

const RefGenericCP& BattleCP::generic() { return d_generic; }
const ConstRefGenericCP& BattleCP::generic() const { return d_generic; }

void BattleCP::generic(const RefGenericCP& cp) { d_generic = cp; }



void BattleCP::setStatusFlag(UWORD mask, bool f)
{
   if(f)
      d_status |= mask;
   else
      d_status &= ~mask;
}



void BattleCP::setDead()
{
    BattleUnit::setDead();
    BattleCP* cp = child();
    while(cp != NoBattleCP)
    {
        cp->setDead();
        cp = cp->sister();
    }

    BattleSP* sPoint = sp();
    while(sPoint != NoBattleSP)
    {
        sPoint->setDead();
        sPoint = sPoint->sister();
    }
}

void BattleCP::setFled()
{
    BattleUnit::setFled();
    BattleCP* cp = child();
    while(cp != NoBattleCP)
    {
        cp->setFled();
        cp = cp->sister();
    }

    BattleSP* sPoint = sp();
    while(sPoint != NoBattleSP)
    {
        sPoint->setFled();
        sPoint = sPoint->sister();
    }
}

/*
 * Stuff from here was just copied from inline header
 * to reduce amount of code generation needed just to include
 * the header file!
 */



void BattleCP::morale(Morale::Value val) { d_generic->morale(val); }
void BattleCP::fatigue(Fatigue::Value val) { d_generic->fatigue(val); }

void BattleCP::ammoSupply(AmmoSupply::Value val) { d_ammoSupply = val; }

// Morale::Value     morale()     const { return d_morale; }
// Fatigue::Value    fatigue()    const { return d_fatigue; }
BattleCP::Morale::Value     BattleCP::morale()     const { return d_generic->morale(); }
BattleCP::Fatigue::Value    BattleCP::fatigue()    const { return d_generic->fatigue(); }

BattleCP::AmmoSupply::Value BattleCP::ammoSupply() const { return d_ammoSupply; }
BattleCP::Disorder::Value   BattleCP::disorder()   const { return d_disorder; }

// UBYTE moralePercent()     const { return static_cast<UBYTE>(d_morale.value(100)); }
// UBYTE fatiguePercent()    const { return static_cast<UBYTE>(d_fatigue.value(100)); }
UBYTE BattleCP::moralePercent()     const
{
   return static_cast<UBYTE>((morale() * 100) / Attribute_Range);
}

UBYTE BattleCP::fatiguePercent()    const
{
   return static_cast<UBYTE>((fatigue() * 100) / Attribute_Range);
}

UBYTE BattleCP::ammoSupplyPercent() const { return static_cast<UBYTE>(d_ammoSupply.value(100)); }
UBYTE BattleCP::startMorale() const { return static_cast<UBYTE>(d_startMorale); }

void BattleCP::startMorale(Morale::Value sm)       { d_startMorale = sm; }
void BattleCP::addMorale(Morale::Value m)
{
   morale((morale() + m > Attribute_Range) ? static_cast<UBYTE>(Attribute_Range) : static_cast<UBYTE>(morale() + m));
}
void BattleCP::addFatigue(Fatigue::Value f)
{
   fatigue((fatigue() - f < 0) ? 0 : fatigue() - f);
}

void BattleCP::addAmmoSupply(AmmoSupply::Value a) { d_ammoSupply.add(a); }

void BattleCP::removeMorale(Morale::Value m)
{
   morale((morale() - m < 0) ? 0 : morale() - m);
}

void BattleCP::removeFatigue(Fatigue::Value f)
{
   fatigue((fatigue() + f > Attribute_Range) ? static_cast<UBYTE>(Attribute_Range) : static_cast<UBYTE>(fatigue() + f));
}

void BattleCP::removeAmmoSupply(AmmoSupply::Value a) { d_ammoSupply.remove(a); }

UINT BattleCP::startStrength() const { return d_startStrength; }
void BattleCP::startStrength(UINT s) { d_startStrength = s; }

UINT BattleCP::lastStrength() const { return d_lastStrength; }
void BattleCP::lastStrength(UINT s) { d_lastStrength = s; }

void BattleCP::disorder(Disorder::Value v) { d_disorder = v; }
void BattleCP::decreaseDisorder()          { d_disorder.add(1); }
void BattleCP::increaseDisorder()          { d_disorder.remove(1); }


void BattleCP::criticalLossReached(bool f)   { setStatusFlag(CriticalLossReached, f); }
void BattleCP::noEffectCutoffReached(bool f) { setStatusFlag(NoEffectCutoffReached, f); }
void BattleCP::shaken(bool f)                { setStatusFlag(Shaken, f); }
void BattleCP::routing(bool f)               { setStatusFlag(Routing, f); }
void BattleCP::rallying(bool f)              { setStatusFlag(Rallying, f); }
void BattleCP::pursuing(bool f)              { setStatusFlag(Pursuing, f); }
void BattleCP::wavering(bool f)              { setStatusFlag(Wavering, f); }
void BattleCP::fleeingTheField(bool f)       { setStatusFlag(FleeingTheField, f); }
void BattleCP::active(bool f)                { setStatusFlag(Active, f); }
void BattleCP::aggressionLocked(bool f)      { setStatusFlag(AggressionLocked, f); }
void BattleCP::postureLocked(bool f)         { setStatusFlag(PostureLocked, f); }
void BattleCP::retreating(bool f)            { setStatusFlag(Retreating, f); }
void BattleCP::movingOverBridge(bool f)      { setStatusFlag(MovingOverBridge, f); }
void BattleCP::needsReadjustment(bool f)     { setStatusFlag(NeedsReadjustment, f); }
void BattleCP::offMap(bool f)                { setStatusFlag(OffMap, f); }
void BattleCP::historicalBattleReinforcent(bool f) { setStatusFlag(HistoricalBattleReinforcement, f); }

bool BattleCP::criticalLossReached()   const { return (d_status & CriticalLossReached) != 0; }
bool BattleCP::noEffectCutoffReached() const { return (d_status & NoEffectCutoffReached) != 0; }
bool BattleCP::shaken()                const { return (d_status & Shaken) != 0; }
bool BattleCP::routing()               const { return (d_status & Routing) != 0; }
bool BattleCP::rallying()              const { return (d_status & Rallying) != 0; }
bool BattleCP::pursuing()              const { return (d_status & Pursuing) != 0; }
bool BattleCP::wavering()              const { return (d_status & Wavering) != 0; }
bool BattleCP::fleeingTheField()       const { return (d_status & FleeingTheField) != 0; }
bool BattleCP::active()                const { return (d_status & Active) != 0; }
bool BattleCP::aggressionLocked()      const { return (d_status & AggressionLocked) != 0; }
bool BattleCP::postureLocked()         const { return (d_status & PostureLocked) != 0; }
bool BattleCP::retreating()            const { return (d_status & Retreating) != 0; }
bool BattleCP::movingOverBridge()      const { return (d_status & MovingOverBridge) != 0; }
bool BattleCP::needsReadjustment()     const { return (d_status & NeedsReadjustment) != 0; }
bool BattleCP::offMap()                const { return (d_status & OffMap) != 0; }
bool BattleCP::historicalBattleReinforcent() const { return (d_status & HistoricalBattleReinforcement) != 0; }

void BattleCP::moveMode(MoveMode m) { d_moveMode = m; }
BattleCP::MoveMode BattleCP::moveMode() const { return d_moveMode; }
Boolean BattleCP::moving()     const { return (d_moveMode == Moving); }
Boolean BattleCP::deploying()  const { return (d_moveMode == Deploying); }
Boolean BattleCP::liningUp()   const { return (d_moveMode == LiningUp); }
Boolean BattleCP::onManuever() const { return (d_moveMode == OnManuever); }
Boolean BattleCP::holding()    const { return (d_moveMode == Holding); }

void BattleCP::clearCompanion() { d_companionSP = NoBattleSP; }
void BattleCP::setCompanion(BattleSP* sp) { d_companionSP = sp; }
BattleSP* BattleCP::companionSP() const { return d_companionSP; }

bool BattleCP::wantsAttach()             const { return (!attached() && d_currentOrder.attachTo() != NoBattleCP); }
bool BattleCP::attached()                const { return (d_attachStatus == HQ_Attached); }
void BattleCP::setAttached()                   { d_attachStatus = HQ_Attached; }
void BattleCP::setUnattached()                 { d_attachStatus = HQ_Unattached; clearCompanion(); }
void BattleCP::setAttached(HQ_AttachStatus s)  { d_attachStatus = s; }

bool BattleCP::showHQ() const { return !(getRank().sameRank(Rank_Division) && d_sp != NoBattleSP); }

void BattleCP::moveDest(const BattleMeasure::HexCord& h) { d_moveDest = h; }
BattleMeasure::HexCord BattleCP::moveDest() const { return d_moveDest; }

void BattleCP::entryPoint(ReinforcementEntryPoint ep) { d_entryPoint = ep; }
ReinforcementEntryPoint BattleCP::entryPoint() const  { return d_entryPoint; }



void BattleCP::nearEnemyMode(NearEnemyMode m)
{
   if(!d_lockMode)
      d_nearEnemyMode = m;
}
BattleCP::NearEnemyMode BattleCP::nearEnemyMode() const { return d_nearEnemyMode; }

void BattleCP::lockMode() { d_lockMode = True; }
void BattleCP::unlockMode() { d_lockMode = False; }

bool BattleCP::isActingOnOrder(void) const { return d_actingOrder.ticks() != BattleSentOrder::Invalid; }

Side BattleCP::getSide() const { return generic()->side(); }
void BattleCP::setSide(Side side) { generic()->side(side); }

void BattleCP::setNation(Nationality n) { d_generic->nation(n); }
Nationality BattleCP::getNation() const { return d_generic->nation(); }

void BattleCP::setRank(RankEnum rank) { generic()->setRank(rank); }
const Rank& BattleCP::getRank() const { return generic()->getRank(); }

const char* BattleCP::getName() const { return generic()->getNameNotNull("<un-named Unit>"); }

RefGLeader BattleCP::leader() const { return generic()->leader(); }

ReturnRefBattleSP BattleCP::sp() { return d_sp; }
ReturnCRefBattleSP BattleCP::sp() const { return d_sp; }

bool BattleCP::isUsed() const { return d_used; }

void BattleCP::formation(BOB_Definitions::CPFormation f) { d_formation = f; }
BOB_Definitions::CPFormation BattleCP::formation() const { return d_formation; }
// Return current Divisional formation

void BattleCP::nextFormation(BOB_Definitions::CPFormation f) { d_nextFormation = f; }
BOB_Definitions::CPFormation BattleCP::nextFormation() const { return d_nextFormation; }

void BattleCP::increaseAggression()
{
   d_aggression = static_cast<BattleOrderInfo::Aggression>(minimum(3, aggression() + 1));
}

void BattleCP::decreaseAggression()
{
   d_aggression = static_cast<BattleOrderInfo::Aggression>(maximum(0, aggression() - 1));
   aggressionLocked(True);
}

void BattleCP::aggression(BattleOrderInfo::Aggression a)
{
   if(!aggressionLocked())
   {
      d_aggression = a;
   }
}
BattleOrder::Aggression BattleCP::aggression() const { return d_aggression; }

void BattleCP::posture(BattleOrderInfo::Posture p)
{
   if(!postureLocked())
   {
      d_posture = p;
   }
}
void BattleCP::setDefensive()
{
   d_posture = BattleOrderInfo::Defensive;
   postureLocked(True);
}

BattleOrder::Posture BattleCP::posture() const { return d_posture; }

void BattleCP::orderMode(BattleOrder::OrderMode m) { d_orderMode = m; }
BattleOrder::OrderMode BattleCP::orderMode() const { return d_orderMode; }

void BattleCP::targetCP(RefBattleCP thisUnit)  { d_targetCP = thisUnit; }
ReturnRefBattleCP BattleCP::targetCP() const { return d_targetCP; }

BOB_Definitions::CPDeployHow BattleCP::deployWhichWay() const { return d_currentOrder.deployHow(); }

BattleOrder& BattleCP::getCurrentOrder() { return d_currentOrder; }
const BattleOrder& BattleCP::getCurrentOrder() const { return d_currentOrder; }
// Get current Order

void BattleCP::setCurrentOrder(const BattleOrder& order) { d_currentOrder = order; }
// Set the cirrent Order


/*
 * Add SP to end of SP chain
 * This is not done very often, so just follow links to the end
 */

void BattleCP::addSP(ParamRefBattleSP sp)
{
            ASSERT(sp != NoBattleSP);
            ASSERT(sp->sister() == NoBattleSP);
            ASSERT(sp->parent() == NoBattleCP);
            ASSERT(sp->generic() != NoStrengthPoint);

            sp->parent(this);

            RefBattleSP lastSP = d_sp;

            if(lastSP == NoBattleSP)
                        d_sp = sp;
            else
            {
                        IFDEBUG(int safetyCounter = 0);
                        IFDEBUG(const int MaxSafety = 100);

                while(lastSP->sister() != NoBattleSP)
                {
                        lastSP = lastSP->sister();
                        ASSERT(++safetyCounter < MaxSafety);
                }
                lastSP->sister(sp);
        }
}

void BattleCP::removeStrengthPoints()
{
        while(d_sp != NoBattleSP)
        {
                RefBattleSP sp = d_sp;
                d_sp = d_sp->sister();
                sp->sister(NoBattleSP);
                sp->generic(NoStrengthPoint);
                sp->parent(NoBattleCP);
        }
}

void BattleCP::removeStrengthPoint(RefBattleSP& thisSP)
{
  ASSERT(d_sp != NoBattleSP);
  RefBattleSP sp = d_sp;
//  d_sp = d_sp->sister();

  RefBattleSP lastSP = NoBattleSP;

  while(sp != NoBattleSP)
  {
    if(sp == thisSP)
    {
      if(lastSP != NoBattleSP)
      {
        lastSP->sister(sp->sister());
      }
      else
      {
        d_sp = sp->sister();
      }

      sp->sister(NoBattleSP);
      sp->generic(NoStrengthPoint);
      sp->parent(NoBattleCP);
    }

    lastSP = sp;
    sp = sp->sister();
  }
}

#if 0
void BattleCP::removeStrengthPoint(int c, int r)
{
  if(d_sp != NoBattleSP)
  {
    DeployItem& di = deployItem(c, r);
    removeStrengthPoint(di.d_sp);
  }
}
#endif

void BattleCP::draw(UnitDrawer* drawer) const
{
        drawer->draw(this);
}

/*
 * Get the most common SP formation
 */
#if 1
SPFormation BattleCP::spFormation() const
{
//        CRefBattleSP sp = d_sp;
        if(d_sp != NoBattleSP && d_deployMap.d_map.size() > 0)
        {
         const DeployItem& di = d_deployMap.d_map[0];//deployItem(0, 0);
            return (di.active()) ? di.d_sp->fromFormation() : SP_MarchFormation;

//                return sp->fromFormation();
        }
        else
                return SP_MarchFormation;
}

SPFormation BattleCP::destSPFormation() const
{
        if(d_sp != NoBattleSP && d_deployMap.d_map.size() > 0)
        {
         const DeployItem& di =  d_deployMap.d_map[0];//deployItem(0, 0);
            return (di.active()) ? di.d_sp->destFormation() : SP_MarchFormation;

//                return sp->fromFormation();
        }
        else
                return SP_MarchFormation;
}

BattleTime::Tick BattleCP::orderTicks() const
{
   return d_orderList.ticks();
}

bool BattleCP::orderTicks(BattleTime::Tick ticks)
{
   return d_orderList.ticks(ticks);
}

bool BattleCP::lastOrderSent(BattleOrder* order) const
{
   return d_orderList.getLastOrder(order);
}
bool BattleCP::getNewOrder(BattleOrder* order)
{
        return d_orderList.getOrder(order);
}

#else
SPFormation BattleCP::spFormation() const
{
//        CRefBattleSP sp = d_sp;
        if(d_sp != NoBattleSP && deployMap().size() > 0)
        {
         const DeployItem& di = (deployMap())[0];//deployItem(0, 0);
            return (di.active()) ? di.d_sp->fromFormation() : SP_UnknownFormation;

//                return sp->fromFormation();
        }
        else
                return SP_UnknownFormation;
}

SPFormation BattleCP::destSPFormation() const
{
        if(d_sp != NoBattleSP && deployMap().size() > 0)
        {
         const DeployItem& di =  (deployMap())[0];//deployItem(0, 0);
            return (di.active()) ? di.d_sp->destFormation() : SP_UnknownFormation;

//                return sp->fromFormation();
        }
        else
                return SP_UnknownFormation;
#if 0
        CRefBattleSP sp = d_sp;
        if(sp != NoBattleSP)
                return sp->destFormation();
        else
                return SP_UnknownFormation;
#endif
}
#endif
void BattleCP::lastOrder(BattleOrder* order) const
{
        if(d_orderList.getLastOrder(order))
        {
        }
        else if(d_actingOrder.isValid())
        {
                *order = d_actingOrder;
        }
        else
        {
                *order = d_currentOrder;
        }
}


bool BattleCP::getNewOrder(BattleTime::Tick ticks, BattleOrder* order)
{
        return d_orderList.getOrder(ticks, order);
}

// Set the current acting order, with a time for when it is ready

void BattleCP::setActingOrder(BattleTime::Tick when, const BattleOrder& order)
{
        d_actingOrder.set(when, order);
}

// Get acting order if time is up

bool BattleCP::getActingOrder(BattleTime::Tick when, BattleOrder* order)
{
        if(d_actingOrder.isTime(when))
        {
                *order = d_actingOrder;
                d_actingOrder.reset();
                return true;
        }
        return false;
}

// TODO: get strings from resource
const char* BattleCP::nearEnemyName(NearEnemyMode m)
{
  ASSERT(m < NE_HowMany);
  static const char* s_text[NE_HowMany] = {
         "Not Near",
         "Holding",
         "Charge",
         "Charge At 1",
         "Charge At 2",
         "Charge At 3",
         "Halt At 1",
         "Halt At 2",
         "Halt At 3",
         "Halt At 4",
         "Halt At 5",
         "Halt At 6",
         "Keep Away 2",
         "Keep Away 3",
         "Keep Away 4",
         "Keep Away 5",
         "Keep Away 6",
         "Retreat 2",
         "Retreat 3",
         "Retreat 4",
         "Retreat 5",
         "Retreat 6"
  };

  return s_text[m];
}

const char* BattleCP::moveModeName(MoveMode m)
{
  ASSERT(m < MoveMode_Last);

   static const char* s_text[MoveMode_Last] = {
             "Moving",
             "On Manuever",
             "Lining up",
             "Deploying",
             "Holding"
   };

   return s_text[m];
}
#if 0
// in which direction is unit moving relative to facing
BattleCP::MovingHow BattleCP::movingHow() const
{
   // first average SP facings
   UWORD total = 0;
   UBYTE count = 0;
   for(std::vector<DeployItem>::const_iterator di = deployMap().begin();
       di != deployMap().end();
       ++di)
   {
      if(di->active())
      {
         total += di->sp->facing();
         count++;
      }
   }

   if(count == 0)
      return MH_Forward;

   HexPosition::Facing facing = cp->facing();
   HexPosition::Facing spFace = total / count;

   const HexPosition::Facing base = 64;
   int dif = base - facing;

   if(spFace + dif >= HexPosition::FacingResolution || spFace + dif < 0)
   {
      spFace = (spFace + dif >= HexPosition::FacingResolution) ?
          (spFace + dif) - HexPosition::FacingResolution :
          HexPosition::FacingResolution + (spFace + dif);
   }
   else
      spFace += dif;

   return (spFace >= 0 && spFace <= 128);
}
#endif

bool BattleCP::canMove() const
{
   ASSERT(nearEnemyMode() < NE_HowMany);

// if(movingBackwards())
//  return True;

   const int maxRange = 8;
   enum { NotMoving, Moving, M_HowMany } what;
   static const bool s_canMove[M_HowMany][NE_HowMany][maxRange] = {
             {  // not moving
                        { False,  True,  True,  True,  True,  True, True, True  },    // "Not Near",
                        { False,  True,  True,  True,  True,  True, True, True  },    // "Holding",
                        { False,  True,  True,  True,  True,  True, True, True  },    // "Charge",
                        { False,  False, False, False, False, False, True, True },    // "Charge At 1",
                        { False,  True,  False, False, False, False, True, True },    // "Charge At 2",
                        { False,  True,  True,  False, False, False, True, True },    // "Charge At 3",
                        { False, True,  True,  True,  True,  True, True, True  },   // "Halt At 1",
                        { False, False, True,  True,  True,  True, True, True },   // "Halt At 2",
                        { False, False, False, True,  True,  True, True, True   },   // "Halt At 3",
                        { False, False, False, False, True,  True, True, True  },   // "Halt At 4",
                        { False, False, False, False, False, True, True, True },   // "Halt At 5",
                        { False, False, False, False, False, False, True, True },   // "Halt At 6",
                        { False, False, True,  True,  True,  True, True, True },   // "Keep away At 2",
                        { False, False, False, True,  True,  True, True, True   },   // "Keep away At 3",
                        { False, False, False, False, True,  True, True, True  },   // "Keep away At 4",
                        { False, False, False, False, False, True, True, True },   // "Keep away At 5",
                        { False, False, False, False, False, False, True, True },   // "Keep away At 6",
                        { False, False, True,  True,  True,  True, True, True },   // "Retreat 2",
                        { False, False, False, True,  True,  True, True, True   },   // "Retreat 3",
                        { False, False, False, False, True,  True, True, True  },   // "Retreat 4",
                        { False, False, False, False, False, True, True, True },   // "Retreat 5",
                        { False, False, False, False, False, False, True, True },   // "Retreat 6",
             },

             { // moving
                        { False,  True,  True,  True,  True,  True, True, True },    // "Not Near",
                        { False,  True,  True,  True,  True,  True, True, True },    // "Holding",
                        { False,  True,  True,  True,  True,  True, True, True },    // "Charge",
                        { False,  True,  True,  True,  True,  True, True, True },    // "Charge at 1",
                        { False,  True,  True,  True,  True,  True, True, True },    // "Charge at 2",
                        { False,  True,  True,  True,  True,  True, True, True },    // "Charge at 3",
                        { False,  True,  True,  True,  True,  True, True, True  },   // "Halt At 1",
                        { False,  False, True,  True,  True,  True, True, True },   // "Halt At 2",
                        { False,  False, False, True,  True,  True, True, True   },   // "Halt At 3",
                        { False,  False, False, False, True,  True, True, True  },   // "Halt At 4",
                        { False,  False, False, False, False, True, True, True },   // "Halt At 5",
                        { False,  False, False, False, False, False, True, True },   // "Halt At 6",
                        { False,  False, True,  True,  True,  True, True, True },   // "Retreat 2",
                        { False,  False, False, True,  True,  True, True, True   },   // "Retreat 3",
                        { False,  False, False, False, True,  True, True, True  },   // "Retreat 4",
                        { False,  False, False, False, False, True, True, True },   // "Retreat 5",
                        { False,  False, False, False, False, False, True, True },   // "Retreat 6",
             }
   };

//   MovingHow mh = movingHow();
   int closeRange = (movingBackwards()) ? targetList().closeRangeRear() :
                    (movingRight()) ? targetList().closeRangeRightFlank() :
                    (movingLeft()) ? targetList().closeRangeLeftFlank() : targetList().closeRangeFront();

   const int r = (closeRange / BattleMeasure::XYardsPerHex) - 1;
   ASSERT(r >= 0);
   if(r > 7)
      return True;
   else
   {
      what = (holding() && d_currentOrder.wayPoints().entries() == 0) ? NotMoving : Moving;
      return s_canMove[what][nearEnemyMode()][r];
   }
}

void BattleCP::addToMap(RefBattleSP sp, const HexCord* hex, const int column, const int row)
{
   DeployItem* di = wantDeployItem(column, row);
   if(di)
   {
      di->d_wantSP = sp;

      if(hex)
         di->d_wantHex = *hex;
   }
}

#if 1
DeployItem* BattleCP::currentDeployItem(int c, int r) const
{
   ASSERT(c < d_deployMap.d_columns);
   ASSERT(r < d_deployMap.d_rows);
   if(c < d_deployMap.d_columns &&
      r < d_deployMap.d_rows)
   {
      int index = (d_deployMap.d_columns * r) + c;
      ASSERT(index < d_deployMap.d_map.size());
      if(index < d_deployMap.d_map.size())
         return const_cast<DeployItem*>(&d_deployMap.d_map[index]);
   }

   return 0;
}

DeployItem* BattleCP::wantDeployItem(int c, int r) const
{
   int cols = (d_deployMap.d_wantColumns % 2 != 0 && formation() == CPF_Extended) ?
      d_deployMap.d_wantColumns + 1 : d_deployMap.d_wantColumns;

   ASSERT(c < cols);//d_deployMap.d_wantColumns);
   ASSERT(r < d_deployMap.d_wantRows);
   if(c < cols && //d_deployMap.d_wantColumns &&
      r < d_deployMap.d_wantRows)
   {
      int index = (d_deployMap.d_wantColumns * r) + c;
      ASSERT(index < d_deployMap.d_map.size());
      if(index < d_deployMap.d_map.size())
         return const_cast<DeployItem*>(&d_deployMap.d_map[index]);
   }

   return 0;
}

#else
DeployItem& BattleCP::deployItem(const int c, const int r)
{
   int column = c;
   int row = r;

   ASSERT(column >= 0);
   ASSERT(row >= 0);


#ifdef DEBUG
   int cols = (formation() == CPF_Extended && d_deployMap.d_wantColumns % 2 != 0) ?  d_deployMap.d_wantColumns + 1 :  d_deployMap.d_wantColumns;
   ASSERT(column < cols);
   ASSERT(row < d_deployMap.d_wantRows);
#endif

   if(column < 0) column = 0;
   if(row < 0) row = 0;
   column = minimum(column, (d_deployMap.d_wantColumns + 1) & ~2);
   row = minimum(row, d_deployMap.d_wantRows);

   int index = (d_deployMap.d_wantColumns * row) + column;
   ASSERT(index < d_deployMap.d_map.size());

   return d_deployMap.d_map[index];
}
#endif

void BattleCP::lockMapItem(int i)
{
   ASSERT(i < d_deployMap.d_map.size());
   if(i < d_deployMap.d_map.size())
      d_deployMap.d_map[i].sync();
}

HexCord BattleCP::centerHex() const
{
   if(sp() != NoBattleSP)
   {
      const int r = 0;
      const int c = ((d_deployMap.d_wantColumns % 2) == 0) ?
          maximum(0, (d_deployMap.d_wantColumns / 2) - 1) : d_deployMap.d_wantColumns / 2;

      DeployItem* di = wantDeployItem(c, r);
      ASSERT(di);
      if(di)
      {
         ASSERT(di->active());
         if(di->active())
            return di->d_sp->hex();
      }
   }

   return hex();
}

/*

  As above but returns in paramater & check if DeployItem is active

*/

BATDATA_DLL bool BattleCP::centerHexIfActive(HexCord * centerHex) const
{
   const int r = 0;
   const int c = ((d_deployMap.d_wantColumns % 2) == 0) ?
          maximum(0, (d_deployMap.d_wantColumns / 2) - 1) : d_deployMap.d_wantColumns / 2;
#if 1
   DeployItem* di = wantDeployItem(c, r);
   if(di && di->active())
   {
      *centerHex = di->d_sp->hex();
      return True;
   }

   return False;

#else
   DeployItem& di = deployItem(c, r);
   if(!di.active()) return false;

   *centerHex = di.d_sp->hex();
   return true;
#endif
}


HexCord BattleCP::rightHex() const
{
#if 1
   if(sp() != NoBattleSP)
   {
      int r = 0;
      int c =  maximum(0, (d_deployMap.d_wantColumns - 1));

      DeployItem* di = wantDeployItem(c, r);
      if(di)
      {
         /*
         This should account for situations where columns are ODD, but allocated deployItems are EVEN
         Though actually this should never be the case.
         */
         while(!di->active())
         {
            c--;
            if(c < 0)
               break;

            di = wantDeployItem(c,r);
            if(!di)
               break;
         }

         if(di)
         {
            ASSERT(di->active());
            if(di->active())
               return di->d_sp->hex();
         }
      }
   }

   return hex();
#else
   if(sp() == NoBattleSP)
      return hex();

   const int r = 0;
   int c =  maximum(0, (d_deployMap.d_wantColumns - 1));

   DeployItem& di = deployItem(c, r);

   /*
   This should account for situations where columns are ODD, but allocated deployItems are EVEN
   */
   while(!di.active()) {
      c--;
      di = deployItem(c,r);
   }


   ASSERT(di.active());
   return di.d_sp->hex();
#endif
}

HexCord BattleCP::leftHex() const
{
#if 1
   if(sp() != NoBattleSP)
   {
      const int r = 0;
      const int c = 0;

      DeployItem* di = wantDeployItem(c, r);
      ASSERT(di->active());
      if(di->active())
         return di->d_sp->hex();
   }

   return hex();
#else
   if(sp() == NoBattleSP)
      return hex();

   const int r = 0;
   const int c = 0;

   DeployItem& di = deployItem(c, r);
   ASSERT(di.active());
   return di.d_sp->hex();
#endif
}

void BattleCP::lockMap()
{
   for(std::vector<DeployItem>::iterator di = d_deployMap.d_map.begin();
                di != d_deployMap.d_map.end();
                di++)
  {
         di->sync();
  }
}

void BattleCP::syncMapToSP()
{
   for(std::vector<DeployItem>::iterator di = d_deployMap.d_map.begin();
                di != d_deployMap.d_map.end();
                di++)
  {
         if(di->active())
                di->syncHexToSP();
  }
}

int BattleCP::spCount(bool all) const
{
  int count = 0;
  if(all)
  {
             CRefBattleSP sp = d_sp;
             while(sp != NoBattleSP)
             {
                        count++;
                        sp = sp->sister();
             }
   }
   else
   {
      for(std::vector<DeployItem>::const_iterator di = d_deployMap.d_map.begin();
                   di != d_deployMap.d_map.end();
                   di++)
             {
                if(di->active())
                           count++;
             }
   }

   return count;
}

/*---------------------------------------------------
 *  File-Interface for BattleCP
 */

inline bool operator >> (FileReader& f, BattleCP::Status& s)
{
        UBYTE b;
        f >> b;

        s = static_cast<BattleCP::Status>(b);
        return true;
}

inline bool operator << (FileWriter& f, const BattleCP::Status& s)
{
         return f.putUByte(s);
}

inline bool operator >> (FileReader& f, BattleCP::MoveMode& m)
{
        UBYTE b;
        f >> b;

        m = static_cast<BattleCP::MoveMode>(b);
        return true;
}

inline bool operator << (FileWriter& f, const BattleCP::MoveMode& m)
{
        return f.putUByte(m);
}

inline bool operator >> (FileReader& f, BattleOrderInfo::OrderMode& m)
{
        UBYTE b;
        f >> b;

        m = static_cast<BattleOrderInfo::OrderMode>(b);
        return true;
}

inline bool operator << (FileWriter& f, const BattleOrderInfo::OrderMode& m)
{
            return f.putUByte(m);
}

inline bool operator >> (FileReader& f, BattleCP::NearEnemyMode& nem)
{
        UBYTE b;
        f >> b;

        nem = static_cast<BattleCP::NearEnemyMode>(b);
        return true;
}

inline bool operator << (FileWriter& f, const BattleCP::NearEnemyMode& nem)
{
        return f.putUByte(nem);
}

inline bool operator >> (FileReader& f, BattleCP::HexOffset& ho)
{
        UBYTE b;
        f >> b;

        ho = static_cast<BattleCP::HexOffset>(b);
        return true;
}

inline bool operator << (FileWriter& f, const BattleCP::HexOffset& ho)
{
            return f.putUByte(ho);
}

inline bool operator >> (FileReader& f, CPFormation& cpf)
{
        UBYTE b;
        f >> b;

        cpf = static_cast<CPFormation>(b);
        return true;
}

inline bool operator << (FileWriter& f, const CPFormation& cpf)
{
        return f.putUByte(cpf);
}

inline bool operator >> (FileReader& f, BattleOrderInfo::Aggression& ag)
{
        UBYTE b;
        f >> b;

        ag = static_cast<BattleOrderInfo::Aggression>(b);
        return true;
}

inline bool operator << (FileWriter& f, const BattleOrderInfo::Aggression& ag)
{
            return f.putUByte(ag);
}

inline bool operator >> (FileReader& f, BattleOrderInfo::Posture& p)
{
        UBYTE b;
        f >> b;

        p = static_cast<BattleOrderInfo::Posture>(b);
        return true;
}

inline bool operator << (FileWriter& f, const BattleOrderInfo::Posture& p)
{
        return f.putUByte(p);
}

inline bool operator >> (FileReader& f, BattleCP::HQ_AttachStatus& a)
{
        UBYTE b;
        f >> b;

        a = static_cast<BattleCP::HQ_AttachStatus>(b);
        return true;
}

inline bool operator << (FileWriter& f, const BattleCP::HQ_AttachStatus& a)
{
           return f.putUByte(a);
}

inline bool operator >> (FileReader& f, ReinforcementEntryPoint& a)
{
        UBYTE b;
        f >> b;

        a = static_cast<ReinforcementEntryPoint>(b);
        return true;
}

inline bool operator << (FileWriter& f, const ReinforcementEntryPoint& a)
{
           return f.putUByte(a);
}

const UWORD BattleCP::s_fileVersion = 0x0010;

Boolean BattleCP::readData(FileReader& f, OrderBattle* ob)
{
   UWORD version;
   f >> version;
   ASSERT(version <= s_fileVersion);

   d_used = true;
   ASSERT(d_generic != 0);

   CPIndex cpi;
   f >> cpi;
   d_parent = (cpi != NoCPIndex) ? battleCP(ob->command(cpi)) : NoBattleCP;
   f >> cpi;
   d_sister = (cpi != NoCPIndex) ? battleCP(ob->command(cpi)) : NoBattleCP;
   f >> cpi;
   d_child = (cpi != NoCPIndex) ? battleCP(ob->command(cpi)) : NoBattleCP;

   SPIndex spi;
   f >> spi;
   ISP sp = ob->sp(spi);
   d_sp = (spi != NoSPIndex) ? battleSP(sp) : NoBattleSP;

   UBYTE v;

   if(version < 0x0006)
   {
       f >> v;
       morale(v);
   }

   if(version >= 0x0002)
   {
        f >> v;
        d_startMorale = v;

        f >> d_startStrength;
        f >> d_lastStrength;
   }

   if(version < 0x0006)
   {
        f >> v;
        fatigue(v);
   }

   f >> v;
   d_ammoSupply = v;

   f >> v;
   d_disorder = v;

    if(version < 0x0004)
   {
      UBYTE b;
      f >> b;
      d_status = b;
   }
    else
    {
        f >> d_status;
    }

    if(version < 0x0005)
        d_status |= Active;

   f >> d_nearEnemyMode;
   if(version >= 0x0007)
     f >> d_lockMode;

   f >> d_formation;
   f >> d_nextFormation;
   f >> d_aggression;
   f >> d_posture;
   f >> d_orderMode;

   if(version >= 0x0003)
   {
      CPIndex cpi;
      f >> cpi;
      d_targetCP = (cpi != NoCPIndex) ? battleCP(ob->command(cpi)) : NoBattleCP;
   }
   if(version >= 0x0010)
   {
      SPIndex spi;
      f >> spi;
      ISP sp = ob->sp(spi);
      d_companionSP = (spi != NoSPIndex) ? battleSP(sp) : NoBattleSP;
   }

   if(!d_currentOrder.readData(f, ob))
         return False;

   if(!d_actingOrder.readData(f, ob))
         return False;

   if(!d_orderList.readData(f, ob))
         return False;

   f >> d_moveMode;
   f >> d_nextFire;
   if(version >= 0x0002)
    f >> d_nextShockCheck;

   f >> d_lockUntil;

   if(!d_targets.readData(f, ob))
         return False;

   if(!d_deployMap.readData(f, *ob))
         return False;

   f >> d_hexOffset;
   f >> d_attachStatus;

   if(version >= 0x0001)
         d_moveDest.readData(f);

   if(version >= 0x0008)
      f >> d_entryPoint;

   if(!BattleUnit::readData(f, *ob))
         return False;

   if(version >= 0x0009)
      f >> d_nextOrderCheck;

   return f.isOK();
}

Boolean BattleCP::writeData(FileWriter& f, OrderBattle* ob) const
{
   f << s_fileVersion;

   CPIndex cpi = (d_parent != NoBattleCP) ? ob->getIndex(d_parent->generic()) : NoCPIndex; //ob.getIndex(d_destSP);
   f << cpi;

   cpi = (d_sister != NoBattleCP) ? ob->getIndex(d_sister->generic()) : NoCPIndex; //ob.getIndex(d_destSP);
   f << cpi;

   cpi = (d_child != NoBattleCP) ? ob->getIndex(d_child->generic()) : NoCPIndex; //ob.getIndex(d_destSP);
   f << cpi;

   SPIndex spi = (d_sp != NoBattleSP) ? ob->getIndex(d_sp->generic()) : NoSPIndex; //ob.getIndex(d_destSP);
   f << spi;

   // f << d_morale.value();
   f << d_startMorale.value();
   f << d_startStrength;
   f << d_lastStrength;
   // f << d_fatigue.value();
   f << d_ammoSupply.value();
   f << d_disorder.value();
   f << d_status;
   f << d_nearEnemyMode;
   f << d_lockMode;
   f << d_formation;
   f << d_nextFormation;
   f << d_aggression;
   f << d_posture;
   f << d_orderMode;

   cpi = (d_targetCP != NoBattleCP) ? ob->getIndex(d_targetCP->generic()) : NoCPIndex; //ob.getIndex(d_destSP);
   f << cpi;

   spi = (d_companionSP != NoBattleSP) ? ob->getIndex(d_companionSP->generic()) : NoSPIndex; //ob.getIndex(d_destSP);
   f << spi;

   if(!d_currentOrder.writeData(f, ob))
         return False;

   if(!d_actingOrder.writeData(f, ob))
         return False;

   if(!d_orderList.writeData(f, ob))
         return False;

  f << d_moveMode;
  f << d_nextFire;
  f << d_nextShockCheck;
  f << d_lockUntil;

  if(!d_targets.writeData(f, ob))
         return False;

  if(!d_deployMap.writeData(f, *ob))
         return False;

  f << d_hexOffset;
  f << d_attachStatus;

  d_moveDest.writeData(f);

  f << d_entryPoint;

  if(!BattleUnit::writeData(f, *ob))
         return False;

  f << d_nextOrderCheck;
  return f.isOK();
}

/*----------------------------------------------------------
 * Battle Command Position List
 */

BattleCPList::BattleCPList()
{
#ifdef DEBUG
        bobLog.printf("BattleCPList Constructor");
#endif
}

BattleCPList::~BattleCPList()
{
#ifdef DEBUG
        bobLog.printf("BattleCPList Destructor");
#endif
}


ReturnRefBattleCP BattleCPList::createUnit(const RefGenericCP& cp)
{
        ASSERT(cp != NoGenericCP);
        CPIndex cpi = cp->getSelf();

#ifdef DEBUG
        bobLog.printf("BattleCPList::createUnit(%d)", static_cast<int>(cpi));
#endif

        ASSERT(d_items.find(cpi) == d_items.end());             // Not already in list?

        RefBattleCP ncp(new BattleCP());
        iterator battleCP = d_items.insert(d_items.begin(), value_type(cpi, ncp));

        // RefBattleCP ncp = &(*battleCP).second;

        ncp->generic(cp);
        cp->battleInfo(ncp);

        return ncp;
}

/*
 * Remove unit from CPList
 * Note that with refcounting this shouldnt be neccessary
 * but at the moment RefBattleCP is a raw pointer
 */
void BattleCPList::removeUnit(RefBattleCP cp)
{
   CPIndex cpi = cp->generic()->getSelf();

   cp->generic()->battleInfo(0);
   cp->generic(NoGenericCP);  // If Refcounting worked then this would be enough

   iterator it(d_items.find(cpi));
   ASSERT(it != d_items.end());
   d_items.erase(it);
   delete cp;

   //generic one removes itself via refcounting
}

/*
 * Delete all items from list
 */

void BattleCPList::reset()
{
        d_items.erase(d_items.begin(), d_items.end());
}

const UWORD s_listFileVersion = 0x0001;


Boolean BattleCPList::readData(FileReader& f, OrderBattle* ob)
{
  UWORD version;
  f >> version;
  ASSERT(version <= s_listFileVersion);

  int nItems;
  f >> nItems;
  ASSERT(nItems >= 0);

        if(version >= 0x0001)
        {
                /*
                 * Version 1 reader
                 */

                /*
                 * Read Generic ID and create the units
                 */

                int count = nItems;
                while(count--)
                {
                        CPIndex cpi;
                        f >> cpi;

                        RefGenericCP gCP = ob->command(cpi);
                        RefBattleCP cp = createUnit(gCP);
                }

                /*
                 * Read the data
                 */

                count = nItems;
                while(count--)
                {
                        CPIndex cpi;
                        f >> cpi;

                        RefGenericCP gCP = ob->command(cpi);
                        RefBattleCP cp = battleCP(gCP);
                        ASSERT(cp != NoBattleCP);

                        cp->readData(f, ob);
                }
        }
        else
        {
                /*
                 * Complicated version 0 reader
                 */

                /*
                 * Cheat because we know it must have been written by editor
                 */

                for(int count = 0; count < nItems; ++count)
                {
                        CPIndex cpi = count + 3;                // 1st unit is at position 3.

                        RefGenericCP gCP = ob->command(cpi);
                        RefBattleCP cp = createUnit(gCP);
                }

                for(count = 0; count < nItems; ++count)
                {
                        CPIndex cpi;
                        f >> cpi;

                        ASSERT(cpi == (count + 3));     // check if assumption in 1st pass is correct

                        RefGenericCP gCP = ob->command(cpi);
                        RefBattleCP cp = battleCP(gCP);
                        ASSERT(cp != NoBattleCP);

                        cp->readData(f, ob);
                }
        }


  return f.isOK();
}

Boolean BattleCPList::writeData(FileWriter& f, OrderBattle* ob) const
{
        f << s_listFileVersion;

        int nItems = entries();
        f << nItems;
        ASSERT(nItems >= 0);

        /*
         * version 1 writer:
         */

        /*
         * write Generic IDs
         */

        {
                for(const_iterator it = d_items.begin();
                        it != d_items.end();
                        ++it)
                {
                        CPIndex cpi = (*it).first;
                        f << cpi;
                }
        }

        /*
         * Write the data
         */

        {
                for(const_iterator it = d_items.begin();
                        it != d_items.end();
                        ++it)
                {
                        CPIndex cpi = (*it).first;
                        const BattleCP* cp = (*it).second;
                        f << cpi;
                        cp->writeData(f, ob);
                }
        }


  return f.isOK();
}


BattleCP& BattleCPList::operator[](CPIndex cpi)
{
   ASSERT(cpi != NoCPIndex);

   iterator it = d_items.find(cpi);

   ASSERT(it != d_items.end());

   if(it != d_items.end())
   {
           BattleCP* cp = (*it).second;
           ASSERT(cp->isUsed());
           return *cp;
   }
   else
           throw Error();

   // ASSERT(d_items.find(cpi) != end());
   // ASSERT(cpi < d_items.size());
   // return d_items[cpi]; 
}

const BattleCP& BattleCPList::operator [](CPIndex cpi) const
{
   ASSERT(cpi != NoCPIndex);

   const_iterator it = d_items.find(cpi);

   ASSERT(it != d_items.end());

   if(it != d_items.end())
   {
           const BattleCP* cp = (*it).second;
           ASSERT(cp->isUsed());
           return *cp;
   }
   else
           throw Error();
}

/*================================================================
 * $Log$
 * Revision 1.5  2001/11/26 10:34:41  greenius
 * Current Directory detection improved
 *
 * Battlegame sound system initialised properly
 *
 * BattleAI Message queue problems fixed
 *
 * Order of Battles loading
 *
 * HexMap uses reference counted units
 *
 * BattleEditor open-file dialogs use wrapped classes
 *
 * PathFind compiler warnings removed
 *
 * Orphan detection in BattleOB detected and removed
 *
 * Revision 1.4  2001/07/02 13:18:24  greenius
 * Battle, Campaign, Combined game and Campaign Editor now compile and run... mostly
 *
 * Revision 1.3  2001/06/26 19:06:20  greenius
 * Resources compile... and get someway into battle game before asserting
 *
 * Revision 1.2  2001/06/18 08:25:43  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 * Revision 1.1  2001/06/13 08:52:36  greenius
 * Initial Import
 *
 *================================================================
 */



