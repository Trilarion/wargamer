/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 * Battle Player Messages
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "batmsg.hpp"
#include "wmisc.hpp"
#include "batres_s.h"
#include "bob_cp.hpp"
#include "scenario.hpp"
#include "options.hpp"
#include "resstr.hpp"

namespace   // private namespace
{

/*
 * Table of info about Messages:
 * Text may contain keywords encoded as:
 *   {Unit} = CP sending the message
 *   {Target} = Target CP
 *   {Leader} = General's name sending the message
 *   {s1} = String 1
 *   {s2} = String 2
 *   {n1} = Number 1
 *   {n2} = Number 2
 *
 * eg. "{Unit} has been attacked by {Target}.  {Leader} has lost {n1} casualties"
 */

struct BMSG_Info
{
   BattleMessageID::MessageGroup d_group;
   int         d_resID;          // Resource ID
   int         d_descriptionID;  // Short description for options
   const char* d_text;           // For messages without an ID
   const char* d_descriptionText;
};

const int tem = IDS_BATTLEWINDOW_TITLE;
static BMSG_Info s_info[BattleMessageID::BMSG_HowMany] = {
   { BattleMessageID::Group0, -1, -1, "A Test Battle Message from {Leader} commanding {Unit} for you to read", "Test Message" },
   { BattleMessageID::Group0, IDS_BMSG_ENGAGINGWITHARTILLERY, IDS_BMSG_ENGAGINGWITHARTILLERY + IDS_BMSG_DESCRIPTION, 0, 0 },
   { BattleMessageID::Group0, IDS_BMSG_ENGAGINGWITHMUSKETS,   IDS_BMSG_ENGAGINGWITHMUSKETS   + IDS_BMSG_DESCRIPTION, 0, 0 },
   { BattleMessageID::Group0, IDS_BMSG_TAKINGARTILLERYFIRE,   IDS_BMSG_TAKINGARTILLERYFIRE   + IDS_BMSG_DESCRIPTION, 0, 0 },
   { BattleMessageID::Group0, IDS_BMSG_TAKINGMUSKETFIRE,      IDS_BMSG_TAKINGMUSKETFIRE      + IDS_BMSG_DESCRIPTION, 0, 0 },
   { BattleMessageID::Group0, IDS_BMSG_INTOCLOSECOMBAT,       IDS_BMSG_INTOCLOSECOMBAT       + IDS_BMSG_DESCRIPTION, 0, 0 },
   { BattleMessageID::Group0, IDS_BMSG_TAKINGLOSSES,          IDS_BMSG_TAKINGLOSSES          + IDS_BMSG_DESCRIPTION, 0, 0 },
   { BattleMessageID::Group0, IDS_BMSG_INFLICTINGLOSSES,      IDS_BMSG_INFLICTINGLOSSES      + IDS_BMSG_DESCRIPTION, 0, 0 },
   { BattleMessageID::Group0, IDS_BMSG_CAPTUREDTROPHY,        IDS_BMSG_CAPTUREDTROPHY        + IDS_BMSG_DESCRIPTION, 0, 0 },
   { BattleMessageID::Group0, IDS_BMSG_ENEMYCAPTUREDTROPHY,   IDS_BMSG_ENEMYCAPTUREDTROPHY   + IDS_BMSG_DESCRIPTION, 0, 0 },
   { BattleMessageID::Group0, IDS_BMSG_ROUTING,               IDS_BMSG_ROUTING               + IDS_BMSG_DESCRIPTION, 0, 0 },
   { BattleMessageID::Group0, IDS_BMSG_ENEMYROUTING,          IDS_BMSG_ENEMYROUTING          + IDS_BMSG_DESCRIPTION, 0, 0 },
   { BattleMessageID::Group0, IDS_BMSG_ENEMYSIGHTED,          IDS_BMSG_ENEMYSIGHTED          + IDS_BMSG_DESCRIPTION, 0, 0 },
   { BattleMessageID::Group0, IDS_BMSG_CARRIEDOUTORDER,       IDS_BMSG_CARRIEDOUTORDER       + IDS_BMSG_DESCRIPTION, 0, 0 },
   { BattleMessageID::Group0, IDS_BMSG_AVOIDINGENEMY,         IDS_BMSG_AVOIDINGENEMY         + IDS_BMSG_DESCRIPTION, 0, 0 },
   { BattleMessageID::Group0, IDS_BMSG_ENEMYAVOIDING,         IDS_BMSG_ENEMYAVOIDING         + IDS_BMSG_DESCRIPTION, 0, 0 },
   { BattleMessageID::Group0, IDS_BMSG_RETREATINGFROMENEMY,   IDS_BMSG_RETREATINGFROMENEMY   + IDS_BMSG_DESCRIPTION, 0, 0 },
   { BattleMessageID::Group0, IDS_BMSG_ENEMYRETREATING,       IDS_BMSG_ENEMYRETREATING       + IDS_BMSG_DESCRIPTION, 0, 0 },
   { BattleMessageID::Group0, IDS_BMSG_FLEEINGTHEFIELD,       IDS_BMSG_FLEEINGTHEFIELD       + IDS_BMSG_DESCRIPTION, 0, 0 },
   { BattleMessageID::Group0, IDS_BMSG_ENEMYFLEEING,          IDS_BMSG_ENEMYFLEEING          + IDS_BMSG_DESCRIPTION, 0, 0 },
   { BattleMessageID::Group0, IDS_BMSG_CRITICALLOSSREACHED,   IDS_BMSG_CRITICALLOSSREACHED   + IDS_BMSG_DESCRIPTION, 0, 0 },
   { BattleMessageID::Group0, IDS_BMSG_WAVERING,              IDS_BMSG_WAVERING              + IDS_BMSG_DESCRIPTION, 0, 0 },
   { BattleMessageID::Group0, IDS_BMSG_ENEMYWAVERING,         IDS_BMSG_ENEMYWAVERING         + IDS_BMSG_DESCRIPTION, 0, 0 },

   { BattleMessageID::Group1, IDS_BMSG_LEADERHIT,             IDS_BMSG_LEADERHIT             + IDS_BMSG_DESCRIPTION, 0, 0 },
   { BattleMessageID::Group1, IDS_BMSG_HITENEMYLEADER,        IDS_BMSG_HITENEMYLEADER        + IDS_BMSG_DESCRIPTION, 0, 0 },
   { BattleMessageID::Group1, IDS_BMSG_TAKENCOMMAND,          IDS_BMSG_TAKENCOMMAND          + IDS_BMSG_DESCRIPTION, 0, 0 },

   { BattleMessageID::Group1, IDS_BMSG_OUTOFAMMO,             IDS_BMSG_OUTOFAMMO             + IDS_BMSG_DESCRIPTION, 0, 0 },
   { BattleMessageID::Group1, IDS_BMSG_AMMOREMAINING,         IDS_BMSG_AMMOREMAINING         + IDS_BMSG_DESCRIPTION, 0, 0 },

   { BattleMessageID::Group1, IDS_BMSG_UNABLETODEPLOY,        IDS_BMSG_UNABLETODEPLOY        + IDS_BMSG_DESCRIPTION, 0, 0 },
   { BattleMessageID::Group1, IDS_BMSG_UNABLETOFINDROUTE,     IDS_BMSG_UNABLETOFINDROUTE     + IDS_BMSG_DESCRIPTION, 0, 0 },
   { BattleMessageID::Group1, IDS_BMSG_REFUSINGFLANK,         IDS_BMSG_REFUSINGFLANK         + IDS_BMSG_DESCRIPTION, 0, 0 },
   { BattleMessageID::Group1, IDS_BMSG_CHANGINGSPFORMATION,   IDS_BMSG_CHANGINGSPFORMATION   + IDS_BMSG_DESCRIPTION, 0, 0 },
   { BattleMessageID::Group1, IDS_BMSG_CHANGINGXXDEPLOYMENT,  IDS_BMSG_CHANGINGXXDEPLOYMENT  + IDS_BMSG_DESCRIPTION, 0, 0 },
   // Unchecked
   { BattleMessageID::Group1, -1,  -1, "Enemy took VP location", "Enemy took VP" },
   // End
};

static UINT groupNames[BattleMessageID::Group_HowMany] = {
   IDS_MSG_GROUP0,
   IDS_MSG_GROUP1,
   IDS_MSG_GROUP2,
   IDS_MSG_GROUP3
};

}; // private namespace


int BattleMessageID::getGroupID(MessageGroup group)
{
   ASSERT(group < Group_HowMany);
   return groupNames[group];
}


int BattleMessageID::getDescriptionID()
{
   ASSERT(d_id < BMSG_HowMany);
   return s_info[d_id].d_descriptionID;
}

BattleMessageID::MessageGroup BattleMessageID::getGroup()
{
   ASSERT(d_id < BMSG_HowMany);
   return s_info[d_id].d_group;
}



/*=========================================================
 * BattleMessageID Functions
 */


String BattleMessageID::text() const
{
   if(d_id == NoMessage)
      return InGameText::get(IDS_NoMessage); // "No Message";

   ASSERT(d_id < BMSG_HowMany);

   BMSG_Info* info = &s_info[d_id];

   if(info->d_resID != -1)
   {
      ResString res(info->d_resID);
      return res.str();
   }
   else
      return info->d_text;
};

String BattleMessageID::getDescription()
{
   if(d_id == NoMessage)
      return InGameText::get(IDS_NoMessage); // "No Message";

   ASSERT(d_id < BMSG_HowMany);

   BMSG_Info* info = &s_info[d_id];

   if(info->d_descriptionID != -1)
   {
      ResString res(info->d_descriptionID);
      return res.str();
   }
   else
      return info->d_descriptionText;
}

/*=========================================================
 * BattleMessageInfo Functions
 */


BattleMessageInfo::BattleMessageInfo() :
   d_id(BattleMessageID::NoMessage),
   d_side(SIDE_Neutral),
   d_cp(NoBattleCP),
   d_leader(NoGLeader),
   d_cpTarget(NoBattleCP),
   d_s1(),
   d_s2(),
   d_n1(0)
{
};


BattleMessageInfo::BattleMessageInfo(Side s, BattleMessageID id, CRefBattleCP cp) :
   d_id(id),
   d_side(s),
   d_cp(cp),
   d_leader(cp->leader()),
   d_cpTarget(NoBattleCP)
{
}

void BattleMessageInfo::copyMessage(const BattleMessageInfo& msg2)
{
   id(msg2.id());
   side(msg2.side());
   cp(msg2.cp());
   leader(msg2.leader());
   target(msg2.target());
   s1(msg2.s1());
   s2(msg2.s2());
   n1(msg2.n1());
   hex(msg2.hex());
}

/*
 * Message Format functions
 */

String BattleMessageFormat::messageToText(const BattleMessageInfo& msg, const BattleData* batData)
{
   // return msg.id().text();

   // ASSERT(msg.id() < BMSG_HowMany);

   String text = msg.id().text();

   // str.allocSize(128);        // Let it allocate in larger sized chunks

   const char StartKey = '{';
   const char FinishKey = '}';


   String str;    // this is where the result goes

   const char* src = text.c_str();
   while(*src)
   {
      char c = *src++;

      if(c == StartKey)
      {
         char c1 = *src++;
         ASSERT(c1 != 0);
         char c2 = *src++;
         ASSERT(c2 != 0);

         c1 = (char) toupper(c1);
         c2 = (char) toupper(c2);

         /*
          * Brute force comparison!
          */

         if( (c1 == 'U') && (c2 == 'N') )
         {
            // Unit

            CRefBattleCP cpi = msg.cp();
            ASSERT(cpi != NoBattleCP);
            str += cpi->getName();
         }
         else if(c1 == 'T' && (c2 == 'A'))
         {
            // Target

            CRefBattleCP cpi = msg.target();
            ASSERT(cpi != NoBattleCP);
            str += cpi->getName();
         }
         else if(c1 == 'L' && (c2 == 'E'))
         {
            // Leader

            ConstRefGLeader leader = msg.leader();
            ASSERT(leader != NoGLeader);
            str += leader->getName();
         }
         else if(c1 == 'S' && (c2 == '1'))
         {
            // String1

            const char* s = msg.s1();
            ASSERT(s != 0);
            str += s;
         }
         else if(c1 == 'S' && (c2 == '2'))
         {
            // String2

            const char* s = msg.s2();
            ASSERT(s != 0);
            str += s;
         }
         else if(c1 == 'N' && (c2 == '1'))
         {
            // Number 1

            int n = msg.n1();
            char buffer[sizeof(int) * 3 + 2];      // This should be big enough
            _itoa(n, buffer, 10);
            str += buffer;
         }
         else
         {
            FORCEASSERT("Illegal formatting code in string");
            throw GeneralError("Illegal formatting code in Battle string %s", (const char*)text.c_str());
         }

         /*
          * Skip to end of '}'
          */

         while(c2 != FinishKey)
         {
            c2 = *src++;
            ASSERT(c2 != 0);
            if(c2 == 0)
               throw GeneralError("Missing '}' in Battle string %s", (const char*)text.c_str());
         }

      }
      else
      {
         ASSERT ((c != '%') || (*src != 's'));     // String probably contains C formatting

         str += c;
      }
   }


   return str;
}



/*----------------------------------------------------------
 *  Message Options
 */

class BattleMessageOptionsImp : public OptionBase {
  public:
    BattleMessageOptionsImp();
    ~BattleMessageOptionsImp() {}

    void setDefault();
};

#define MsgOptionSize ((BattleMessageID::BMSG_HowMany + 31) / 32)
static const char regMessages[] = "BMsgShow";

BattleMessageOptionsImp::BattleMessageOptionsImp() :
  OptionBase(MsgOptionSize, regMessages)
{
  if(!readRegistry())
  {
    setDefault();
  }
}


void BattleMessageOptionsImp::setDefault()
{
  reset();
  for(BattleMessageID::BMSG_ID id = BattleMessageID::BMSG_First;
      id < BattleMessageID::BMSG_HowMany;
      INCREMENT(id))
  {
    set(id);
  }
}

static BattleMessageOptionsImp s_msgOptions;

/*
 * static access functions
 */

void BattleMessageOptions::set(BattleMessageID what, Boolean how)
{
   ASSERT(what.id() < BattleMessageID::BMSG_HowMany);

   if(how)
      s_msgOptions.set(what.id());
   else
      s_msgOptions.clear(what.id());
}

void BattleMessageOptions::toggle(BattleMessageID what)
{
   ASSERT(what.id() < BattleMessageID::BMSG_HowMany);
   s_msgOptions.flip(what.id());
}

Boolean BattleMessageOptions::get(BattleMessageID what)
{
   ASSERT(what.id() < BattleMessageID::BMSG_HowMany);
   return s_msgOptions.get(what.id());
}

void BattleMessageOptions::setDefault()
{
  s_msgOptions.setDefault();
}

/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.2  2001/06/18 08:25:43  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
