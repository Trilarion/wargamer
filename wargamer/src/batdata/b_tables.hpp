/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef B_TABLES_HPP
#define B_TABLES_HPP

#include "bd_dll.h"
#include "tables.hpp"

class BattleTables {
  public:

	 /*-----------------------------------------------------
	  * SP Formation change tables
	  */

	 BATDATA_DLL static const Table3D<SWORD>& spfInfChangeTime();
	 BATDATA_DLL static const Table3D<SWORD>& spfCavChangeTime();
	 BATDATA_DLL static const Table3D<SWORD>& spfHvyArtChangeTime();
	 BATDATA_DLL static const Table3D<SWORD>& spfFootArtChangeTime();
	 BATDATA_DLL static const Table3D<SWORD>& spfHorseArtChangeTime();
	 BATDATA_DLL static const Table1D<UWORD>& spfModifiers();

	 /*-----------------------------------------------------
	  * Movement tables
	  */

	 BATDATA_DLL static const Table2D<UBYTE>& basicMovementRate();
	 BATDATA_DLL static const Table2D<UBYTE>& roadMovementRate();
	 BATDATA_DLL static const Table3D<UWORD>& terrainModifiers();
	 BATDATA_DLL static const Table3D<UWORD>& pathModifiers();
	 BATDATA_DLL static const Table3D<UBYTE>& terrainDisorder();
	 BATDATA_DLL static const Table3D<UBYTE>& prohibitedTerrain();

	 /*
	  * Fire-Combat tables
	  */

	 BATDATA_DLL static const Table1D<UWORD>& fireCombatRange();
	 BATDATA_DLL static const Table2D<UWORD>& fireCombatPoints();
	 BATDATA_DLL static const Table2D<UWORD>& fireCombatCasualty();
    BATDATA_DLL static const Table1D<SWORD>& fireCombatDieRollModifiers();
    BATDATA_DLL static const Table1D<SWORD>& fireCombatFireValueMultipliers();
    BATDATA_DLL static const Table1D<SWORD>& moraleTestDieRollModifiers();
    BATDATA_DLL static const Table1D<SWORD>& corpDeterminationDieRollModifiers();

    /*
     * Shock-Combat tables
     */

    BATDATA_DLL static const Table1D<SWORD>& cavChargeTestDieRollModifiers();
    BATDATA_DLL static const Table2D<UBYTE>& infVsInfShockLoss();
    BATDATA_DLL static const Table2D<UBYTE>& infVsCavShockLoss();
    BATDATA_DLL static const Table2D<UBYTE>& infVsCavChanceToInflictShockLoss();
    BATDATA_DLL static const Table1D<SWORD>& infVsInfDieRollModifiers();
    BATDATA_DLL static const Table1D<SWORD>& infVsInfMoraleDieRollModifiers();
    BATDATA_DLL static const Table1D<SWORD>& infVsCavCavDieRollModifiers();
    BATDATA_DLL static const Table1D<SWORD>& infVsCavInfDieRollModifiers();
    BATDATA_DLL static const Table1D<SWORD>& cavVsCavDieRollModifiers();

    BATDATA_DLL static const Table1D<UBYTE>& noEffectCutoff();
    BATDATA_DLL static const Table1D<SWORD>& moraleLossMultiplier();
    BATDATA_DLL static const Table1D<UBYTE>& criticalLossCutoff();
    BATDATA_DLL static const Table1D<UBYTE>& recoveryTime();
    BATDATA_DLL static const Table1D<SWORD>& recovery();
    BATDATA_DLL static const Table1D<SWORD>& chargeTestDieRollModifiers();
    BATDATA_DLL static const Table1D<SWORD>& moraleBonus();
};

#endif
