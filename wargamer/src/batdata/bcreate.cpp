/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *      Random Battlefield creation
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "bcreate.hpp"
#include "batdata.hpp"
#include "b_ob_ran.hpp"
#include "maketer.hpp"
#include "batarmy.hpp"
#include "hexmap.hpp"
// just to get a decent random seed
#include "wg_rand.hpp"
#include "mmtimer.hpp"


inline int randEx(int from, int endRange)
{
   return CRandom::get(from, endRange-1);
}

/*

  TERRAIN_TYPE_GOVERNS_POPULATION_LEVEL

  NOTE : this define makes the battlefield generator work ONLY on the BattleInfo.terrain() parameter
         if the isn't defined, the Campaign Game must pass population density info into the Battle Game
         to sensibly match the region in which the battle is taking place
         if defined, the code will fill in the BattleInfo.population() data according to the terrain type
*/

#define TERRAIN_TYPE_GOVERNS_POPULATION_LEVEL


/*
 * Global Functions
 */


void BattleCreate::createBattleField(BattleData* batData, const BattleInfo& info)
{
        TerrainGenerationParameters params;
        params.MapWidth = info.deploymentWidth();
        params.MapHeight = info.deploymentHeight();

      BattleInfo & binfo = const_cast<BattleInfo &>(info);

      /*
      Set up terrain generator paramaters
      */



      switch(binfo.terrain()) {

         // open
         case BattleInfo::Open : {
//          if(CRandom::get(0,100) >= 50) {
            if(CRandom::get(100) >= 50) {
               binfo.heightType(HEIGHT_ROLLING);
               params.HeightType = HEIGHT_ROLLING;
            }
            else {
               binfo.heightType(HEIGHT_FLAT);
               params.HeightType = HEIGHT_FLAT;
            }

            params.FirstTerrainType = LT_Clear;

            params.SecondTerrainType = LT_Orchard;
            params.PercentageSecondTerrain = randEx(0,2);

            params.ThirdTerrainType = LT_Vineyard;
            params.PercentageThirdTerrain = randEx(0,2);

            params.FourthTerrainType = LT_Rough;
            params.PercentageFourthTerrain = randEx(0,2);

            params.FifthTerrainType = LT_Marsh;
            params.PercentageFifthTerrain = randEx(0,2);

            params.NumberOfLightWoods = randEx(4,8);
            params.NumberOfDenseWoods = 0;

            params.MinimumFarmSize = 1;
            params.MaximumFarmSize = 8;
            params.NumberOfRandomSettlements = randEx(12,20);
            params.ChanceFarmsCreateFields = 75;
            params.AverageFarmFieldsSize = 9;

            if(randEx(0,100) < 1) params.NumberOfLakes = 1;

            params.NumberOfRivers = 0;
            params.NumberOfStreams = randEx(2,4);
            params.NumberOfRoads = randEx(3,6);
            params.NumberOfTracks = randEx(3,6);

            params.ChanceRiversSunken = 5;
            params.ChanceStreamsSunken = 25;
            params.ChanceRoadsSunken = 10;
            params.ChanceRiversSunken = 5;

#ifdef TERRAIN_TYPE_GOVERNS_POPULATION_LEVEL
            if(randEx(0,100) > 30) binfo.population(BattleInfo::High);
            else binfo.population(BattleInfo::Dense);
#endif
            break;
         }


         // rough
         case BattleInfo::Rough : {
            if(randEx(0,100) >= 30) {
               binfo.heightType(HEIGHT_ROLLING);
               params.HeightType = HEIGHT_ROLLING;
            }
            else {
               binfo.heightType(HEIGHT_FLAT);
               params.HeightType = HEIGHT_FLAT;
            }

            params.FirstTerrainType = LT_Clear;

            params.SecondTerrainType = LT_Orchard;
            params.PercentageSecondTerrain = randEx(0,4);

            params.ThirdTerrainType = LT_Vineyard;
            params.PercentageThirdTerrain = randEx(0,1);

            params.FourthTerrainType = LT_Rough;
            params.PercentageFourthTerrain = randEx(20,40);

            params.FifthTerrainType = LT_Marsh;
            params.PercentageFifthTerrain = randEx(0,2);

            params.NumberOfLightWoods = randEx(4,6);
            params.NumberOfDenseWoods = randEx(2,3);

            params.MinimumFarmSize = 1;
            params.MaximumFarmSize = 3;
            params.NumberOfRandomSettlements = randEx(12,20);
            params.ChanceFarmsCreateFields = 50;
            params.AverageFarmFieldsSize = 7;

            if(randEx(0,100) < 1) params.NumberOfLakes = 1;

            params.NumberOfRivers = 0;
            params.NumberOfStreams = randEx(2,4);
            params.NumberOfRoads = randEx(3,6);
            params.NumberOfTracks = randEx(3,6);

            params.ChanceRiversSunken = 5;
            params.ChanceStreamsSunken = 25;
            params.ChanceRoadsSunken = 10;
            params.ChanceRiversSunken = 5;

#ifdef TERRAIN_TYPE_GOVERNS_POPULATION_LEVEL
            binfo.population(BattleInfo::Moderate);
#endif
            break;
         }


         // very hilly
         case BattleInfo::MountainPass : {
            binfo.heightType(HEIGHT_STEEP);
            params.HeightType = HEIGHT_STEEP;

            params.FirstTerrainType = LT_Clear;

            params.SecondTerrainType = LT_Rough;
            params.PercentageSecondTerrain = randEx(50,75);

            params.ThirdTerrainType = LT_Vineyard;
            params.PercentageThirdTerrain = 0;

            params.FourthTerrainType = LT_Orchard;
            params.PercentageFourthTerrain = 0;

            params.FifthTerrainType = LT_Marsh;
            params.PercentageFifthTerrain = randEx(5,20);

            params.NumberOfLightWoods = randEx(4,8);
            params.NumberOfDenseWoods = randEx(2,4);

            params.MinimumFarmSize = 1;
            params.MaximumFarmSize = 3;
            params.NumberOfRandomSettlements = randEx(1,5);
            params.ChanceFarmsCreateFields = 25;
            params.AverageFarmFieldsSize = 5;

            params.NumberOfLakes = 0;

            params.NumberOfRivers = 0;
            params.NumberOfStreams = randEx(2,5);
            params.NumberOfRoads = 0;
            params.NumberOfTracks = randEx(1,3);

            params.ChanceRiversSunken = 5;
            params.ChanceStreamsSunken = 50;
            params.ChanceRoadsSunken = 10;
            params.ChanceRiversSunken = 5;

#ifdef TERRAIN_TYPE_GOVERNS_POPULATION_LEVEL
            binfo.population(BattleInfo::Sparse);
#endif
            break;
         }


         // normally hilly
         case BattleInfo::Hilly : {
            binfo.heightType(HEIGHT_HILLY);
            params.HeightType = HEIGHT_HILLY;

            params.FirstTerrainType = LT_Clear;

            params.SecondTerrainType = LT_Orchard;
            params.PercentageSecondTerrain = randEx(2,6);

            params.ThirdTerrainType = LT_Vineyard;
            if(randEx(0,100) < 5) params.PercentageThirdTerrain = randEx(2,6);
            else params.PercentageThirdTerrain = 0;

            params.FourthTerrainType = LT_Rough;
            params.PercentageFourthTerrain = randEx(10,20);

            params.FifthTerrainType = LT_Marsh;
            params.PercentageFifthTerrain = randEx(0,5);

            params.NumberOfLightWoods = randEx(1,4);
            params.NumberOfDenseWoods = randEx(0,2);

            params.MinimumFarmSize = 1;
            params.MaximumFarmSize = 6;
            params.NumberOfRandomSettlements = randEx(12,20);
            params.ChanceFarmsCreateFields = 60;
            params.AverageFarmFieldsSize = 8;

            if(randEx(0,100) < 1) params.NumberOfLakes = 1;

            params.NumberOfRivers = 0;
            params.NumberOfStreams = randEx(3,6);
            params.NumberOfRoads = randEx(3,6);
            params.NumberOfTracks = randEx(3,6);

            params.ChanceRiversSunken = 5;
            params.ChanceStreamsSunken = 10;
            params.ChanceRoadsSunken = 10;
            params.ChanceRiversSunken = 5;

#ifdef TERRAIN_TYPE_GOVERNS_POPULATION_LEVEL
            if(randEx(0,100) > 30) binfo.population(BattleInfo::Moderate);
            else binfo.population(BattleInfo::High);
#endif
            break;
         }

         // wooded
         case BattleInfo::Wooded : {
            if(randEx(0,100) >= 50) {
               binfo.heightType(HEIGHT_ROLLING);
               params.HeightType = HEIGHT_ROLLING;
            }
            else {
               binfo.heightType(HEIGHT_FLAT);
               params.HeightType = HEIGHT_FLAT;
            }

            params.FirstTerrainType = LT_Clear;

            params.SecondTerrainType = LT_Orchard;
            params.PercentageSecondTerrain = randEx(2,8);

            params.ThirdTerrainType = LT_Vineyard;
            if(randEx(0,100) < 5) params.PercentageThirdTerrain = randEx(2,8);
            else params.PercentageThirdTerrain = 0;

            params.FourthTerrainType = LT_Rough;
            params.PercentageFourthTerrain = randEx(2,4);

            params.FifthTerrainType = LT_Marsh;
            params.PercentageFifthTerrain = 0;

            params.NumberOfLightWoods = randEx(8,12);
            params.NumberOfDenseWoods = randEx(8,12);

            params.MinimumFarmSize = 2;
            params.MaximumFarmSize = 6;
            params.NumberOfRandomSettlements = randEx(12,20);
            params.ChanceFarmsCreateFields = 30;
            params.AverageFarmFieldsSize = 6;

            if(randEx(0,100) < 1) params.NumberOfLakes = 1;

            params.NumberOfRivers = 0;
            params.NumberOfStreams = randEx(2,4);
            params.NumberOfRoads = randEx(2,5);
            params.NumberOfTracks = randEx(4,7);

            params.ChanceRiversSunken = 5;
            params.ChanceStreamsSunken = 10;
            params.ChanceRoadsSunken = 10;
            params.ChanceRiversSunken = 5;

#ifdef TERRAIN_TYPE_GOVERNS_POPULATION_LEVEL
            if(randEx(0,100) > 30) binfo.population(BattleInfo::Sparse);
            else binfo.population(BattleInfo::Moderate);
#endif
            break;
         }


         // wooded hilly
         case BattleInfo::WoodedHilly : {
            binfo.heightType(HEIGHT_HILLY);
            params.HeightType = HEIGHT_HILLY;

            params.FirstTerrainType = LT_Clear;

            params.SecondTerrainType = LT_Orchard;
            params.PercentageSecondTerrain = randEx(2,7);

            params.ThirdTerrainType = LT_Vineyard;
            if(randEx(0,100) < 5) params.PercentageThirdTerrain = randEx(2,4);
            else params.PercentageThirdTerrain = 0;

            params.FourthTerrainType = LT_Rough;
            params.PercentageFourthTerrain = randEx(2,4);

            params.FifthTerrainType = LT_Marsh;
            params.PercentageFifthTerrain = 0;

            params.NumberOfLightWoods = randEx(8,12);
            params.NumberOfDenseWoods = randEx(8,12);

            params.MinimumFarmSize = 2;
            params.MaximumFarmSize = 6;
            params.NumberOfRandomSettlements = randEx(12,20);
            params.ChanceFarmsCreateFields = 30;
            params.AverageFarmFieldsSize = 6;

            if(randEx(0,100) < 1) params.NumberOfLakes = 1;

            params.NumberOfRivers = 0;
            params.NumberOfStreams = randEx(2,4);
            params.NumberOfRoads = randEx(2,5);
            params.NumberOfTracks = randEx(4,7);

            params.ChanceRiversSunken = 5;
            params.ChanceStreamsSunken = 10;
            params.ChanceRoadsSunken = 10;
            params.ChanceRiversSunken = 5;

#ifdef TERRAIN_TYPE_GOVERNS_POPULATION_LEVEL
            if(randEx(0,100) > 30) binfo.population(BattleInfo::Sparse);
            else binfo.population(BattleInfo::Moderate);
#endif
            break;
         }

         // marshy
         case BattleInfo::Marsh : {
            binfo.heightType(HEIGHT_FLAT);
            params.HeightType = HEIGHT_FLAT;

            if(randEx(0,100) >= 50) {
               binfo.heightType(HEIGHT_ROLLING);
               params.HeightType = HEIGHT_ROLLING;
            }
            else {
               binfo.heightType(HEIGHT_FLAT);
               params.HeightType = HEIGHT_FLAT;
            }

            params.FirstTerrainType = LT_Clear;

            params.SecondTerrainType = LT_Orchard;
            params.PercentageSecondTerrain = randEx(0,2);

            params.ThirdTerrainType = LT_Vineyard;
            params.PercentageThirdTerrain = 0;

            params.FourthTerrainType = LT_Rough;
            params.PercentageFourthTerrain = randEx(5,15);

            params.FifthTerrainType = LT_Marsh;
            params.PercentageFifthTerrain = randEx(35,50);

            params.NumberOfLightWoods = randEx(1,2);
            params.NumberOfDenseWoods = 0;

            params.MinimumFarmSize = 1;
            params.MaximumFarmSize = 4;
            params.NumberOfRandomSettlements = randEx(2,6);
            params.ChanceFarmsCreateFields = 10;
            params.AverageFarmFieldsSize = 6;

            params.NumberOfLakes = randEx(4,10);

            params.NumberOfRivers = 0;
            params.NumberOfStreams = randEx(5,10);
            params.NumberOfRoads = randEx(0,4);
            params.NumberOfTracks = randEx(4,8);

            params.ChanceRiversSunken = 5;
            params.ChanceStreamsSunken = 10;
            params.ChanceRoadsSunken = 10;
            params.ChanceRiversSunken = 5;

#ifdef TERRAIN_TYPE_GOVERNS_POPULATION_LEVEL
            binfo.population(BattleInfo::Sparse);
#endif
            break;
         }


         case BattleInfo::OpenWithMajorRiver : {
            break;
         }

         case BattleInfo::OpenWithMinorRiver : {
            break;
         }

         case BattleInfo::WoodedWithMajorRiver : {
            break;
         }

         case BattleInfo::WoodedWithMinorRiver : {
            break;
         }

         case BattleInfo::RoughWithMajorRiver : {
            break;
         }

         case BattleInfo::RoughWithMinorRiver : {
            break;
         }

         // randomly flat, or rolling
         default : {
            FORCEASSERT("Error - invalid value in terrain enum for BCREATE");
            break;
         }
      }










      switch(binfo.population()) {
         case BattleInfo::Sparse : {
            params.ChanceSettlementAtCrossroads = 12;
            params.ChanceSettlementAtJunction = 12;
            params.ChanceSettlementAtBridge = 12;
            params.ChanceSettlementAtFord = 12;

            params.MinimumSettlementSize = 2;
            params.MaximumSettlementSize = 5;
            params.MaximumVillageSize = 4;
            params.MaximumTownSize = 5;
            params.VillageDensity = 4;
            params.TownDensity = 4;
            params.CombineSettlementsDistance = 8;
            break;
         }
         case BattleInfo::Moderate : {
            params.ChanceSettlementAtCrossroads = 25;
            params.ChanceSettlementAtJunction = 25;
            params.ChanceSettlementAtBridge = 25;
            params.ChanceSettlementAtFord = 25;

            params.MinimumSettlementSize = 3;
            params.MaximumSettlementSize = 8;
            params.MaximumVillageSize = 6;
            params.MaximumTownSize = 8;
            params.VillageDensity = 4;
            params.TownDensity = 4;
            params.CombineSettlementsDistance = 8;
            break;
         }
         case BattleInfo::High : {
            params.ChanceSettlementAtCrossroads = 50;
            params.ChanceSettlementAtJunction = 50;
            params.ChanceSettlementAtBridge = 50;
            params.ChanceSettlementAtFord = 50;

            params.MinimumSettlementSize = 4;
            params.MaximumSettlementSize = 10;
            params.MaximumVillageSize = 6;
            params.MaximumTownSize = 10;
            params.VillageDensity = 5;
            params.TownDensity = 6;
            params.CombineSettlementsDistance = 8;
            break;
         }
         case BattleInfo::Dense : {
            params.ChanceSettlementAtCrossroads = 75;
            params.ChanceSettlementAtJunction = 75;
            params.ChanceSettlementAtBridge = 75;
            params.ChanceSettlementAtFord = 75;

            params.MinimumSettlementSize = 6;
            params.MaximumSettlementSize = 12;
            params.MaximumVillageSize = 5;
            params.MaximumTownSize = 12;
            params.VillageDensity = 8;
            params.TownDensity = 8;
            params.CombineSettlementsDistance = 8;
            break;
         }
         default : {
            FORCEASSERT("Error - invalid population value in Enumeration for BCREATE");
            break;
         }
      }



      /*
      Create Battlefield
      */
        createRandomBattlefield(
            &params,
            batData);

      /*
      Examine paramaters, to see what population density was *ACTUALLY* created
      */
      int total_settlements =
         params.d_numVillagesCreated +
         params.d_numTownsCreated +
         params.d_numFarmsCreated;

      if(total_settlements < 4) {
         binfo.population(BattleInfo::Sparse);
      }
      else if(total_settlements < 8) {
         binfo.population(BattleInfo::Moderate);
      }
      else if(total_settlements < 12) {
         binfo.population(BattleInfo::High);
      }
      else {
         binfo.population(BattleInfo::Dense);
      }

}

void BattleCreate::createRandomOB(BattleData* batData, bool noUnits)
{
        BattleOB* ob = BattleOBUtility::createRandom(noUnits);
        ASSERT(ob != 0);
        batData->ob(ob);
}

void BattleCreate::createRandomBattle(BattleData* batData, const BattleInfo& info)
{
        createBattleField(batData, info);
        createRandomOB(batData);
}


void BattleCreate::deleteRandomBattle(BattleData* batData)
{
   BattleHexMap* hexMap = batData->hexMap();
   hexMap->clear();

        // get OB & set datbata's reference to NULL
        BattleOB* ob = batData->ob();
        batData->ob(0);
        BattleOBUtility::deleteRandom(ob);

}


/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.2  2001/06/18 08:25:43  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
