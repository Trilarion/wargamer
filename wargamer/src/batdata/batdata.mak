# Microsoft Developer Studio Generated NMAKE File, Based on batdata.dsp
!IF "$(CFG)" == ""
CFG=batdata - Win32 Editor Debug
!MESSAGE No configuration specified. Defaulting to batdata - Win32 Editor Debug.
!ENDIF 

!IF "$(CFG)" != "batdata - Win32 Release" && "$(CFG)" != "batdata - Win32 Debug" && "$(CFG)" != "batdata - Win32 Editor Debug" && "$(CFG)" != "batdata - Win32 Editor Release"
!MESSAGE Invalid configuration "$(CFG)" specified.
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "batdata.mak" CFG="batdata - Win32 Editor Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "batdata - Win32 Release" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE "batdata - Win32 Debug" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE "batdata - Win32 Editor Debug" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE "batdata - Win32 Editor Release" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE 
!ERROR An invalid configuration is specified.
!ENDIF 

!IF "$(OS)" == "Windows_NT"
NULL=
!ELSE 
NULL=nul
!ENDIF 

CPP=cl.exe
MTL=midl.exe
RSC=rc.exe

!IF  "$(CFG)" == "batdata - Win32 Release"

OUTDIR=.\..\..\exe
INTDIR=.\o\release
# Begin Custom Macros
OutDir=.\..\..\exe
# End Custom Macros

!IF "$(RECURSE)" == "0" 

ALL : "$(OUTDIR)\batdata.dll"

!ELSE 

ALL : "gamesup - Win32 Release" "ob - Win32 Release" "system - Win32 Release" "$(OUTDIR)\batdata.dll"

!ENDIF 

!IF "$(RECURSE)" == "1" 
CLEAN :"system - Win32 ReleaseCLEAN" "ob - Win32 ReleaseCLEAN" "gamesup - Win32 ReleaseCLEAN" 
!ELSE 
CLEAN :
!ENDIF 
	-@erase "$(INTDIR)\b_los.obj"
	-@erase "$(INTDIR)\b_morale.obj"
	-@erase "$(INTDIR)\b_ob_ran.obj"
	-@erase "$(INTDIR)\b_send.obj"
	-@erase "$(INTDIR)\b_sounds.obj"
	-@erase "$(INTDIR)\b_tables.obj"
	-@erase "$(INTDIR)\b_terr.obj"
	-@erase "$(INTDIR)\batarmy.obj"
	-@erase "$(INTDIR)\batcord.obj"
	-@erase "$(INTDIR)\batdata.obj"
	-@erase "$(INTDIR)\batlist.obj"
	-@erase "$(INTDIR)\batmsg.obj"
	-@erase "$(INTDIR)\batord.obj"
	-@erase "$(INTDIR)\batsound.obj"
	-@erase "$(INTDIR)\battime.obj"
	-@erase "$(INTDIR)\batunit.obj"
	-@erase "$(INTDIR)\bcreate.obj"
	-@erase "$(INTDIR)\bob_cp.obj"
	-@erase "$(INTDIR)\bob_sp.obj"
	-@erase "$(INTDIR)\bob_vis.obj"
	-@erase "$(INTDIR)\bobiter.obj"
	-@erase "$(INTDIR)\boblog.obj"
	-@erase "$(INTDIR)\bobutil.obj"
	-@erase "$(INTDIR)\bordutil.obj"
	-@erase "$(INTDIR)\breinfor.obj"
	-@erase "$(INTDIR)\bshockl.obj"
	-@erase "$(INTDIR)\btimctrl.obj"
	-@erase "$(INTDIR)\building.obj"
	-@erase "$(INTDIR)\corpdepl.obj"
	-@erase "$(INTDIR)\fractmap.obj"
	-@erase "$(INTDIR)\fx.obj"
	-@erase "$(INTDIR)\hexdata.obj"
	-@erase "$(INTDIR)\hexlist.obj"
	-@erase "$(INTDIR)\hexmap.obj"
	-@erase "$(INTDIR)\initunit.obj"
	-@erase "$(INTDIR)\key_terr.obj"
	-@erase "$(INTDIR)\maketer.obj"
	-@erase "$(INTDIR)\pathfind.obj"
	-@erase "$(INTDIR)\pdeploy.obj"
	-@erase "$(INTDIR)\sqrt.obj"
	-@erase "$(INTDIR)\tresult.obj"
	-@erase "$(INTDIR)\vc60.idb"
	-@erase "$(INTDIR)\vc60.pdb"
	-@erase "$(INTDIR)\wavefile.obj"
	-@erase "$(OUTDIR)\batdata.dll"
	-@erase "$(OUTDIR)\batdata.exp"
	-@erase "$(OUTDIR)\batdata.lib"
	-@erase "$(OUTDIR)\batdata.pdb"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

"$(INTDIR)" :
    if not exist "$(INTDIR)/$(NULL)" mkdir "$(INTDIR)"

CPP_PROJ=/nologo /MD /W3 /Gi /GR /GX /Zi /O2 /I "..\h" /I "..\..\res" /I "..\system" /I "..\gamesup" /I "..\ob" /D "NDEBUG" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "EXPORT_BATDATA_DLL" /Fp"$(INTDIR)\batdata.pch" /YX"stdinc.hpp" /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /c 
MTL_PROJ=/nologo /D "NDEBUG" /mktyplib203 /win32 
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\batdata.bsc" 
BSC32_SBRS= \
	
LINK32=link.exe
LINK32_FLAGS=User32.lib winmm.lib /nologo /dll /incremental:no /pdb:"$(OUTDIR)\batdata.pdb" /debug /machine:I386 /out:"$(OUTDIR)\batdata.dll" /implib:"$(OUTDIR)\batdata.lib" 
LINK32_OBJS= \
	"$(INTDIR)\b_los.obj" \
	"$(INTDIR)\b_morale.obj" \
	"$(INTDIR)\b_ob_ran.obj" \
	"$(INTDIR)\b_send.obj" \
	"$(INTDIR)\b_sounds.obj" \
	"$(INTDIR)\b_tables.obj" \
	"$(INTDIR)\b_terr.obj" \
	"$(INTDIR)\batarmy.obj" \
	"$(INTDIR)\batcord.obj" \
	"$(INTDIR)\batdata.obj" \
	"$(INTDIR)\batlist.obj" \
	"$(INTDIR)\batmsg.obj" \
	"$(INTDIR)\batord.obj" \
	"$(INTDIR)\batsound.obj" \
	"$(INTDIR)\battime.obj" \
	"$(INTDIR)\batunit.obj" \
	"$(INTDIR)\bcreate.obj" \
	"$(INTDIR)\bob_cp.obj" \
	"$(INTDIR)\bob_sp.obj" \
	"$(INTDIR)\bob_vis.obj" \
	"$(INTDIR)\bobiter.obj" \
	"$(INTDIR)\boblog.obj" \
	"$(INTDIR)\bobutil.obj" \
	"$(INTDIR)\bordutil.obj" \
	"$(INTDIR)\breinfor.obj" \
	"$(INTDIR)\bshockl.obj" \
	"$(INTDIR)\btimctrl.obj" \
	"$(INTDIR)\building.obj" \
	"$(INTDIR)\corpdepl.obj" \
	"$(INTDIR)\fractmap.obj" \
	"$(INTDIR)\fx.obj" \
	"$(INTDIR)\hexdata.obj" \
	"$(INTDIR)\hexlist.obj" \
	"$(INTDIR)\hexmap.obj" \
	"$(INTDIR)\initunit.obj" \
	"$(INTDIR)\key_terr.obj" \
	"$(INTDIR)\maketer.obj" \
	"$(INTDIR)\pathfind.obj" \
	"$(INTDIR)\pdeploy.obj" \
	"$(INTDIR)\sqrt.obj" \
	"$(INTDIR)\tresult.obj" \
	"$(INTDIR)\wavefile.obj" \
	"$(OUTDIR)\system.lib" \
	"$(OUTDIR)\ob.lib" \
	"$(OUTDIR)\gamesup.lib"

"$(OUTDIR)\batdata.dll" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

!ELSEIF  "$(CFG)" == "batdata - Win32 Debug"

OUTDIR=.\..\..\exe
INTDIR=.\o\debug
# Begin Custom Macros
OutDir=.\..\..\exe
# End Custom Macros

!IF "$(RECURSE)" == "0" 

ALL : "$(OUTDIR)\batdataDB.dll"

!ELSE 

ALL : "gamesup - Win32 Debug" "ob - Win32 Debug" "system - Win32 Debug" "$(OUTDIR)\batdataDB.dll"

!ENDIF 

!IF "$(RECURSE)" == "1" 
CLEAN :"system - Win32 DebugCLEAN" "ob - Win32 DebugCLEAN" "gamesup - Win32 DebugCLEAN" 
!ELSE 
CLEAN :
!ENDIF 
	-@erase "$(INTDIR)\b_los.obj"
	-@erase "$(INTDIR)\b_morale.obj"
	-@erase "$(INTDIR)\b_ob_ran.obj"
	-@erase "$(INTDIR)\b_send.obj"
	-@erase "$(INTDIR)\b_sounds.obj"
	-@erase "$(INTDIR)\b_tables.obj"
	-@erase "$(INTDIR)\b_terr.obj"
	-@erase "$(INTDIR)\batarmy.obj"
	-@erase "$(INTDIR)\batcord.obj"
	-@erase "$(INTDIR)\batdata.obj"
	-@erase "$(INTDIR)\batlist.obj"
	-@erase "$(INTDIR)\batmsg.obj"
	-@erase "$(INTDIR)\batord.obj"
	-@erase "$(INTDIR)\batsound.obj"
	-@erase "$(INTDIR)\battime.obj"
	-@erase "$(INTDIR)\batunit.obj"
	-@erase "$(INTDIR)\bcreate.obj"
	-@erase "$(INTDIR)\bob_cp.obj"
	-@erase "$(INTDIR)\bob_sp.obj"
	-@erase "$(INTDIR)\bob_vis.obj"
	-@erase "$(INTDIR)\bobiter.obj"
	-@erase "$(INTDIR)\boblog.obj"
	-@erase "$(INTDIR)\bobutil.obj"
	-@erase "$(INTDIR)\bordutil.obj"
	-@erase "$(INTDIR)\breinfor.obj"
	-@erase "$(INTDIR)\bshockl.obj"
	-@erase "$(INTDIR)\btimctrl.obj"
	-@erase "$(INTDIR)\building.obj"
	-@erase "$(INTDIR)\corpdepl.obj"
	-@erase "$(INTDIR)\fractmap.obj"
	-@erase "$(INTDIR)\fx.obj"
	-@erase "$(INTDIR)\hexdata.obj"
	-@erase "$(INTDIR)\hexlist.obj"
	-@erase "$(INTDIR)\hexmap.obj"
	-@erase "$(INTDIR)\initunit.obj"
	-@erase "$(INTDIR)\key_terr.obj"
	-@erase "$(INTDIR)\maketer.obj"
	-@erase "$(INTDIR)\pathfind.obj"
	-@erase "$(INTDIR)\pdeploy.obj"
	-@erase "$(INTDIR)\sqrt.obj"
	-@erase "$(INTDIR)\tresult.obj"
	-@erase "$(INTDIR)\vc60.idb"
	-@erase "$(INTDIR)\vc60.pdb"
	-@erase "$(INTDIR)\wavefile.obj"
	-@erase "$(OUTDIR)\batdataDB.dll"
	-@erase "$(OUTDIR)\batdataDB.exp"
	-@erase "$(OUTDIR)\batdataDB.ilk"
	-@erase "$(OUTDIR)\batdataDB.lib"
	-@erase "$(OUTDIR)\batdataDB.pdb"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

"$(INTDIR)" :
    if not exist "$(INTDIR)/$(NULL)" mkdir "$(INTDIR)"

CPP_PROJ=/nologo /MDd /W3 /Gm /Gi /GR /GX /ZI /Od /I "..\h" /I "..\..\res" /I "..\system" /I "..\gamesup" /I "..\ob" /D "_DEBUG" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "EXPORT_BATDATA_DLL" /Fp"$(INTDIR)\batdata.pch" /YX"stdinc.hpp" /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /GZ /c 
MTL_PROJ=/nologo /D "_DEBUG" /mktyplib203 /win32 
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\batdata.bsc" 
BSC32_SBRS= \
	
LINK32=link.exe
LINK32_FLAGS=User32.lib winmm.lib /nologo /dll /incremental:yes /pdb:"$(OUTDIR)\batdataDB.pdb" /debug /machine:I386 /out:"$(OUTDIR)\batdataDB.dll" /implib:"$(OUTDIR)\batdataDB.lib" /pdbtype:sept 
LINK32_OBJS= \
	"$(INTDIR)\b_los.obj" \
	"$(INTDIR)\b_morale.obj" \
	"$(INTDIR)\b_ob_ran.obj" \
	"$(INTDIR)\b_send.obj" \
	"$(INTDIR)\b_sounds.obj" \
	"$(INTDIR)\b_tables.obj" \
	"$(INTDIR)\b_terr.obj" \
	"$(INTDIR)\batarmy.obj" \
	"$(INTDIR)\batcord.obj" \
	"$(INTDIR)\batdata.obj" \
	"$(INTDIR)\batlist.obj" \
	"$(INTDIR)\batmsg.obj" \
	"$(INTDIR)\batord.obj" \
	"$(INTDIR)\batsound.obj" \
	"$(INTDIR)\battime.obj" \
	"$(INTDIR)\batunit.obj" \
	"$(INTDIR)\bcreate.obj" \
	"$(INTDIR)\bob_cp.obj" \
	"$(INTDIR)\bob_sp.obj" \
	"$(INTDIR)\bob_vis.obj" \
	"$(INTDIR)\bobiter.obj" \
	"$(INTDIR)\boblog.obj" \
	"$(INTDIR)\bobutil.obj" \
	"$(INTDIR)\bordutil.obj" \
	"$(INTDIR)\breinfor.obj" \
	"$(INTDIR)\bshockl.obj" \
	"$(INTDIR)\btimctrl.obj" \
	"$(INTDIR)\building.obj" \
	"$(INTDIR)\corpdepl.obj" \
	"$(INTDIR)\fractmap.obj" \
	"$(INTDIR)\fx.obj" \
	"$(INTDIR)\hexdata.obj" \
	"$(INTDIR)\hexlist.obj" \
	"$(INTDIR)\hexmap.obj" \
	"$(INTDIR)\initunit.obj" \
	"$(INTDIR)\key_terr.obj" \
	"$(INTDIR)\maketer.obj" \
	"$(INTDIR)\pathfind.obj" \
	"$(INTDIR)\pdeploy.obj" \
	"$(INTDIR)\sqrt.obj" \
	"$(INTDIR)\tresult.obj" \
	"$(INTDIR)\wavefile.obj" \
	"$(OUTDIR)\systemDB.lib" \
	"$(OUTDIR)\obDB.lib" \
	"$(OUTDIR)\gamesupDB.lib"

"$(OUTDIR)\batdataDB.dll" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

!ELSEIF  "$(CFG)" == "batdata - Win32 Editor Debug"

OUTDIR=.\..\..\exe
INTDIR=.\o\Editor\Debug
# Begin Custom Macros
OutDir=.\..\..\exe
# End Custom Macros

!IF "$(RECURSE)" == "0" 

ALL : "$(OUTDIR)\batdataEDDB.dll"

!ELSE 

ALL : "gamesup - Win32 Editor Debug" "ob - Win32 Editor Debug" "system - Win32 Editor Debug" "$(OUTDIR)\batdataEDDB.dll"

!ENDIF 

!IF "$(RECURSE)" == "1" 
CLEAN :"system - Win32 Editor DebugCLEAN" "ob - Win32 Editor DebugCLEAN" "gamesup - Win32 Editor DebugCLEAN" 
!ELSE 
CLEAN :
!ENDIF 
	-@erase "$(INTDIR)\b_los.obj"
	-@erase "$(INTDIR)\b_morale.obj"
	-@erase "$(INTDIR)\b_ob_ran.obj"
	-@erase "$(INTDIR)\b_send.obj"
	-@erase "$(INTDIR)\b_sounds.obj"
	-@erase "$(INTDIR)\b_tables.obj"
	-@erase "$(INTDIR)\b_terr.obj"
	-@erase "$(INTDIR)\batarmy.obj"
	-@erase "$(INTDIR)\batcord.obj"
	-@erase "$(INTDIR)\batdata.obj"
	-@erase "$(INTDIR)\batlist.obj"
	-@erase "$(INTDIR)\batmsg.obj"
	-@erase "$(INTDIR)\batord.obj"
	-@erase "$(INTDIR)\batsound.obj"
	-@erase "$(INTDIR)\battime.obj"
	-@erase "$(INTDIR)\batunit.obj"
	-@erase "$(INTDIR)\bcreate.obj"
	-@erase "$(INTDIR)\bob_cp.obj"
	-@erase "$(INTDIR)\bob_sp.obj"
	-@erase "$(INTDIR)\bob_vis.obj"
	-@erase "$(INTDIR)\bobiter.obj"
	-@erase "$(INTDIR)\boblog.obj"
	-@erase "$(INTDIR)\bobutil.obj"
	-@erase "$(INTDIR)\bordutil.obj"
	-@erase "$(INTDIR)\breinfor.obj"
	-@erase "$(INTDIR)\bshockl.obj"
	-@erase "$(INTDIR)\btimctrl.obj"
	-@erase "$(INTDIR)\building.obj"
	-@erase "$(INTDIR)\corpdepl.obj"
	-@erase "$(INTDIR)\fractmap.obj"
	-@erase "$(INTDIR)\fx.obj"
	-@erase "$(INTDIR)\hexdata.obj"
	-@erase "$(INTDIR)\hexlist.obj"
	-@erase "$(INTDIR)\hexmap.obj"
	-@erase "$(INTDIR)\initunit.obj"
	-@erase "$(INTDIR)\key_terr.obj"
	-@erase "$(INTDIR)\maketer.obj"
	-@erase "$(INTDIR)\pathfind.obj"
	-@erase "$(INTDIR)\pdeploy.obj"
	-@erase "$(INTDIR)\sqrt.obj"
	-@erase "$(INTDIR)\tresult.obj"
	-@erase "$(INTDIR)\vc60.idb"
	-@erase "$(INTDIR)\vc60.pdb"
	-@erase "$(INTDIR)\wavefile.obj"
	-@erase "$(OUTDIR)\batdataEDDB.dll"
	-@erase "$(OUTDIR)\batdataEDDB.exp"
	-@erase "$(OUTDIR)\batdataEDDB.ilk"
	-@erase "$(OUTDIR)\batdataEDDB.lib"
	-@erase "$(OUTDIR)\batdataEDDB.pdb"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

"$(INTDIR)" :
    if not exist "$(INTDIR)/$(NULL)" mkdir "$(INTDIR)"

CPP_PROJ=/nologo /MDd /W3 /Gm /Gi /GR /GX /ZI /Od /I "..\h" /I "..\..\res" /I "..\system" /I "..\gamesup" /I "..\ob" /D "_USRDLL" /D "EXPORT_BATDATA_DLL" /D "_DEBUG" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /D "EDITOR" /Fp"$(INTDIR)\batdata.pch" /YX"stdinc.hpp" /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /GZ /c 
MTL_PROJ=/nologo /D "_DEBUG" /mktyplib203 /win32 
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\batdata.bsc" 
BSC32_SBRS= \
	
LINK32=link.exe
LINK32_FLAGS=User32.lib winmm.lib /nologo /dll /incremental:yes /pdb:"$(OUTDIR)\batdataEDDB.pdb" /debug /machine:I386 /out:"$(OUTDIR)\batdataEDDB.dll" /implib:"$(OUTDIR)\batdataEDDB.lib" /pdbtype:sept 
LINK32_OBJS= \
	"$(INTDIR)\b_los.obj" \
	"$(INTDIR)\b_morale.obj" \
	"$(INTDIR)\b_ob_ran.obj" \
	"$(INTDIR)\b_send.obj" \
	"$(INTDIR)\b_sounds.obj" \
	"$(INTDIR)\b_tables.obj" \
	"$(INTDIR)\b_terr.obj" \
	"$(INTDIR)\batarmy.obj" \
	"$(INTDIR)\batcord.obj" \
	"$(INTDIR)\batdata.obj" \
	"$(INTDIR)\batlist.obj" \
	"$(INTDIR)\batmsg.obj" \
	"$(INTDIR)\batord.obj" \
	"$(INTDIR)\batsound.obj" \
	"$(INTDIR)\battime.obj" \
	"$(INTDIR)\batunit.obj" \
	"$(INTDIR)\bcreate.obj" \
	"$(INTDIR)\bob_cp.obj" \
	"$(INTDIR)\bob_sp.obj" \
	"$(INTDIR)\bob_vis.obj" \
	"$(INTDIR)\bobiter.obj" \
	"$(INTDIR)\boblog.obj" \
	"$(INTDIR)\bobutil.obj" \
	"$(INTDIR)\bordutil.obj" \
	"$(INTDIR)\breinfor.obj" \
	"$(INTDIR)\bshockl.obj" \
	"$(INTDIR)\btimctrl.obj" \
	"$(INTDIR)\building.obj" \
	"$(INTDIR)\corpdepl.obj" \
	"$(INTDIR)\fractmap.obj" \
	"$(INTDIR)\fx.obj" \
	"$(INTDIR)\hexdata.obj" \
	"$(INTDIR)\hexlist.obj" \
	"$(INTDIR)\hexmap.obj" \
	"$(INTDIR)\initunit.obj" \
	"$(INTDIR)\key_terr.obj" \
	"$(INTDIR)\maketer.obj" \
	"$(INTDIR)\pathfind.obj" \
	"$(INTDIR)\pdeploy.obj" \
	"$(INTDIR)\sqrt.obj" \
	"$(INTDIR)\tresult.obj" \
	"$(INTDIR)\wavefile.obj" \
	"$(OUTDIR)\systemDB.lib" \
	"$(OUTDIR)\obEDDB.lib" \
	"$(OUTDIR)\gamesupDB.lib"

"$(OUTDIR)\batdataEDDB.dll" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

!ELSEIF  "$(CFG)" == "batdata - Win32 Editor Release"

OUTDIR=.\..\..\exe
INTDIR=.\o\Editor\Release
# Begin Custom Macros
OutDir=.\..\..\exe
# End Custom Macros

!IF "$(RECURSE)" == "0" 

ALL : "$(OUTDIR)\batdataED.dll"

!ELSE 

ALL : "gamesup - Win32 Editor Release" "ob - Win32 Editor Release" "system - Win32 Editor Release" "$(OUTDIR)\batdataED.dll"

!ENDIF 

!IF "$(RECURSE)" == "1" 
CLEAN :"system - Win32 Editor ReleaseCLEAN" "ob - Win32 Editor ReleaseCLEAN" "gamesup - Win32 Editor ReleaseCLEAN" 
!ELSE 
CLEAN :
!ENDIF 
	-@erase "$(INTDIR)\b_los.obj"
	-@erase "$(INTDIR)\b_morale.obj"
	-@erase "$(INTDIR)\b_ob_ran.obj"
	-@erase "$(INTDIR)\b_send.obj"
	-@erase "$(INTDIR)\b_sounds.obj"
	-@erase "$(INTDIR)\b_tables.obj"
	-@erase "$(INTDIR)\b_terr.obj"
	-@erase "$(INTDIR)\batarmy.obj"
	-@erase "$(INTDIR)\batcord.obj"
	-@erase "$(INTDIR)\batdata.obj"
	-@erase "$(INTDIR)\batlist.obj"
	-@erase "$(INTDIR)\batmsg.obj"
	-@erase "$(INTDIR)\batord.obj"
	-@erase "$(INTDIR)\batsound.obj"
	-@erase "$(INTDIR)\battime.obj"
	-@erase "$(INTDIR)\batunit.obj"
	-@erase "$(INTDIR)\bcreate.obj"
	-@erase "$(INTDIR)\bob_cp.obj"
	-@erase "$(INTDIR)\bob_sp.obj"
	-@erase "$(INTDIR)\bob_vis.obj"
	-@erase "$(INTDIR)\bobiter.obj"
	-@erase "$(INTDIR)\boblog.obj"
	-@erase "$(INTDIR)\bobutil.obj"
	-@erase "$(INTDIR)\bordutil.obj"
	-@erase "$(INTDIR)\breinfor.obj"
	-@erase "$(INTDIR)\bshockl.obj"
	-@erase "$(INTDIR)\btimctrl.obj"
	-@erase "$(INTDIR)\building.obj"
	-@erase "$(INTDIR)\corpdepl.obj"
	-@erase "$(INTDIR)\fractmap.obj"
	-@erase "$(INTDIR)\fx.obj"
	-@erase "$(INTDIR)\hexdata.obj"
	-@erase "$(INTDIR)\hexlist.obj"
	-@erase "$(INTDIR)\hexmap.obj"
	-@erase "$(INTDIR)\initunit.obj"
	-@erase "$(INTDIR)\key_terr.obj"
	-@erase "$(INTDIR)\maketer.obj"
	-@erase "$(INTDIR)\pathfind.obj"
	-@erase "$(INTDIR)\pdeploy.obj"
	-@erase "$(INTDIR)\sqrt.obj"
	-@erase "$(INTDIR)\tresult.obj"
	-@erase "$(INTDIR)\vc60.idb"
	-@erase "$(INTDIR)\wavefile.obj"
	-@erase "$(OUTDIR)\batdataED.dll"
	-@erase "$(OUTDIR)\batdataED.exp"
	-@erase "$(OUTDIR)\batdataED.lib"
	-@erase "$(OUTDIR)\batdataED.pdb"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

"$(INTDIR)" :
    if not exist "$(INTDIR)/$(NULL)" mkdir "$(INTDIR)"

CPP_PROJ=/nologo /MD /W3 /Gi /GR /GX /Zd /O2 /I "..\h" /I "..\..\res" /I "..\system" /I "..\gamesup" /I "..\ob" /D "NDEBUG" /D "_USRDLL" /D "EXPORT_BATDATA_DLL" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /D "EDITOR" /Fp"$(INTDIR)\batdata.pch" /YX"stdinc.hpp" /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /c 
MTL_PROJ=/nologo /D "NDEBUG" /mktyplib203 /win32 
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\batdata.bsc" 
BSC32_SBRS= \
	
LINK32=link.exe
LINK32_FLAGS=User32.lib winmm.lib /nologo /dll /incremental:no /pdb:"$(OUTDIR)\batdataED.pdb" /debug /machine:I386 /out:"$(OUTDIR)\batdataED.dll" /implib:"$(OUTDIR)\batdataED.lib" 
LINK32_OBJS= \
	"$(INTDIR)\b_los.obj" \
	"$(INTDIR)\b_morale.obj" \
	"$(INTDIR)\b_ob_ran.obj" \
	"$(INTDIR)\b_send.obj" \
	"$(INTDIR)\b_sounds.obj" \
	"$(INTDIR)\b_tables.obj" \
	"$(INTDIR)\b_terr.obj" \
	"$(INTDIR)\batarmy.obj" \
	"$(INTDIR)\batcord.obj" \
	"$(INTDIR)\batdata.obj" \
	"$(INTDIR)\batlist.obj" \
	"$(INTDIR)\batmsg.obj" \
	"$(INTDIR)\batord.obj" \
	"$(INTDIR)\batsound.obj" \
	"$(INTDIR)\battime.obj" \
	"$(INTDIR)\batunit.obj" \
	"$(INTDIR)\bcreate.obj" \
	"$(INTDIR)\bob_cp.obj" \
	"$(INTDIR)\bob_sp.obj" \
	"$(INTDIR)\bob_vis.obj" \
	"$(INTDIR)\bobiter.obj" \
	"$(INTDIR)\boblog.obj" \
	"$(INTDIR)\bobutil.obj" \
	"$(INTDIR)\bordutil.obj" \
	"$(INTDIR)\breinfor.obj" \
	"$(INTDIR)\bshockl.obj" \
	"$(INTDIR)\btimctrl.obj" \
	"$(INTDIR)\building.obj" \
	"$(INTDIR)\corpdepl.obj" \
	"$(INTDIR)\fractmap.obj" \
	"$(INTDIR)\fx.obj" \
	"$(INTDIR)\hexdata.obj" \
	"$(INTDIR)\hexlist.obj" \
	"$(INTDIR)\hexmap.obj" \
	"$(INTDIR)\initunit.obj" \
	"$(INTDIR)\key_terr.obj" \
	"$(INTDIR)\maketer.obj" \
	"$(INTDIR)\pathfind.obj" \
	"$(INTDIR)\pdeploy.obj" \
	"$(INTDIR)\sqrt.obj" \
	"$(INTDIR)\tresult.obj" \
	"$(INTDIR)\wavefile.obj" \
	"$(OUTDIR)\system.lib" \
	"$(OUTDIR)\obED.lib" \
	"$(OUTDIR)\gamesup.lib"

"$(OUTDIR)\batdataED.dll" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

!ENDIF 

.c{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.c{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<


!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("batdata.dep")
!INCLUDE "batdata.dep"
!ELSE 
!MESSAGE Warning: cannot find "batdata.dep"
!ENDIF 
!ENDIF 


!IF "$(CFG)" == "batdata - Win32 Release" || "$(CFG)" == "batdata - Win32 Debug" || "$(CFG)" == "batdata - Win32 Editor Debug" || "$(CFG)" == "batdata - Win32 Editor Release"
SOURCE=.\b_los.cpp

"$(INTDIR)\b_los.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\b_morale.cpp

"$(INTDIR)\b_morale.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\b_ob_ran.cpp

"$(INTDIR)\b_ob_ran.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\b_send.cpp

"$(INTDIR)\b_send.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\b_sounds.cpp

"$(INTDIR)\b_sounds.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\b_tables.cpp

"$(INTDIR)\b_tables.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\b_terr.cpp

"$(INTDIR)\b_terr.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\batarmy.cpp

"$(INTDIR)\batarmy.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\batcord.cpp

"$(INTDIR)\batcord.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\batdata.cpp

"$(INTDIR)\batdata.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\batlist.cpp

"$(INTDIR)\batlist.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\batmsg.cpp

"$(INTDIR)\batmsg.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\batord.cpp

"$(INTDIR)\batord.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\batsound.cpp

"$(INTDIR)\batsound.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\battime.cpp

"$(INTDIR)\battime.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\batunit.cpp

"$(INTDIR)\batunit.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\bcreate.cpp

"$(INTDIR)\bcreate.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\bob_cp.cpp

"$(INTDIR)\bob_cp.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\bob_sp.cpp

"$(INTDIR)\bob_sp.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\bob_vis.cpp

"$(INTDIR)\bob_vis.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\bobiter.cpp

"$(INTDIR)\bobiter.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\boblog.cpp

"$(INTDIR)\boblog.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\bobutil.cpp

"$(INTDIR)\bobutil.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\bordutil.cpp

"$(INTDIR)\bordutil.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\breinfor.cpp

"$(INTDIR)\breinfor.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\bshockl.cpp

"$(INTDIR)\bshockl.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\btimctrl.cpp

"$(INTDIR)\btimctrl.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\building.cpp

"$(INTDIR)\building.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\corpdepl.cpp

"$(INTDIR)\corpdepl.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\fractmap.cpp

"$(INTDIR)\fractmap.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\fx.cpp

"$(INTDIR)\fx.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\hexdata.cpp

"$(INTDIR)\hexdata.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\hexlist.cpp

"$(INTDIR)\hexlist.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\hexmap.cpp

"$(INTDIR)\hexmap.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\initunit.cpp

"$(INTDIR)\initunit.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\key_terr.cpp

"$(INTDIR)\key_terr.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\maketer.cpp

"$(INTDIR)\maketer.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\pathfind.cpp

"$(INTDIR)\pathfind.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\pdeploy.cpp

"$(INTDIR)\pdeploy.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\sqrt.cpp

"$(INTDIR)\sqrt.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\tresult.cpp

"$(INTDIR)\tresult.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\wavefile.cpp

"$(INTDIR)\wavefile.obj" : $(SOURCE) "$(INTDIR)"


!IF  "$(CFG)" == "batdata - Win32 Release"

"system - Win32 Release" : 
   cd "\src\sf\wargamer\SRC\SYSTEM"
   $(MAKE) /$(MAKEFLAGS) /F .\system.mak CFG="system - Win32 Release" 
   cd "..\batdata"

"system - Win32 ReleaseCLEAN" : 
   cd "\src\sf\wargamer\SRC\SYSTEM"
   $(MAKE) /$(MAKEFLAGS) /F .\system.mak CFG="system - Win32 Release" RECURSE=1 CLEAN 
   cd "..\batdata"

!ELSEIF  "$(CFG)" == "batdata - Win32 Debug"

"system - Win32 Debug" : 
   cd "\src\sf\wargamer\SRC\SYSTEM"
   $(MAKE) /$(MAKEFLAGS) /F .\system.mak CFG="system - Win32 Debug" 
   cd "..\batdata"

"system - Win32 DebugCLEAN" : 
   cd "\src\sf\wargamer\SRC\SYSTEM"
   $(MAKE) /$(MAKEFLAGS) /F .\system.mak CFG="system - Win32 Debug" RECURSE=1 CLEAN 
   cd "..\batdata"

!ELSEIF  "$(CFG)" == "batdata - Win32 Editor Debug"

"system - Win32 Editor Debug" : 
   cd "\src\sf\wargamer\SRC\SYSTEM"
   $(MAKE) /$(MAKEFLAGS) /F .\system.mak CFG="system - Win32 Editor Debug" 
   cd "..\batdata"

"system - Win32 Editor DebugCLEAN" : 
   cd "\src\sf\wargamer\SRC\SYSTEM"
   $(MAKE) /$(MAKEFLAGS) /F .\system.mak CFG="system - Win32 Editor Debug" RECURSE=1 CLEAN 
   cd "..\batdata"

!ELSEIF  "$(CFG)" == "batdata - Win32 Editor Release"

"system - Win32 Editor Release" : 
   cd "\src\sf\wargamer\SRC\SYSTEM"
   $(MAKE) /$(MAKEFLAGS) /F .\system.mak CFG="system - Win32 Editor Release" 
   cd "..\batdata"

"system - Win32 Editor ReleaseCLEAN" : 
   cd "\src\sf\wargamer\SRC\SYSTEM"
   $(MAKE) /$(MAKEFLAGS) /F .\system.mak CFG="system - Win32 Editor Release" RECURSE=1 CLEAN 
   cd "..\batdata"

!ENDIF 

!IF  "$(CFG)" == "batdata - Win32 Release"

"ob - Win32 Release" : 
   cd "\src\sf\wargamer\SRC\OB"
   $(MAKE) /$(MAKEFLAGS) /F .\ob.mak CFG="ob - Win32 Release" 
   cd "..\batdata"

"ob - Win32 ReleaseCLEAN" : 
   cd "\src\sf\wargamer\SRC\OB"
   $(MAKE) /$(MAKEFLAGS) /F .\ob.mak CFG="ob - Win32 Release" RECURSE=1 CLEAN 
   cd "..\batdata"

!ELSEIF  "$(CFG)" == "batdata - Win32 Debug"

"ob - Win32 Debug" : 
   cd "\src\sf\wargamer\SRC\OB"
   $(MAKE) /$(MAKEFLAGS) /F .\ob.mak CFG="ob - Win32 Debug" 
   cd "..\batdata"

"ob - Win32 DebugCLEAN" : 
   cd "\src\sf\wargamer\SRC\OB"
   $(MAKE) /$(MAKEFLAGS) /F .\ob.mak CFG="ob - Win32 Debug" RECURSE=1 CLEAN 
   cd "..\batdata"

!ELSEIF  "$(CFG)" == "batdata - Win32 Editor Debug"

"ob - Win32 Editor Debug" : 
   cd "\src\sf\wargamer\SRC\OB"
   $(MAKE) /$(MAKEFLAGS) /F .\ob.mak CFG="ob - Win32 Editor Debug" 
   cd "..\batdata"

"ob - Win32 Editor DebugCLEAN" : 
   cd "\src\sf\wargamer\SRC\OB"
   $(MAKE) /$(MAKEFLAGS) /F .\ob.mak CFG="ob - Win32 Editor Debug" RECURSE=1 CLEAN 
   cd "..\batdata"

!ELSEIF  "$(CFG)" == "batdata - Win32 Editor Release"

"ob - Win32 Editor Release" : 
   cd "\src\sf\wargamer\SRC\OB"
   $(MAKE) /$(MAKEFLAGS) /F .\ob.mak CFG="ob - Win32 Editor Release" 
   cd "..\batdata"

"ob - Win32 Editor ReleaseCLEAN" : 
   cd "\src\sf\wargamer\SRC\OB"
   $(MAKE) /$(MAKEFLAGS) /F .\ob.mak CFG="ob - Win32 Editor Release" RECURSE=1 CLEAN 
   cd "..\batdata"

!ENDIF 

!IF  "$(CFG)" == "batdata - Win32 Release"

"gamesup - Win32 Release" : 
   cd "\src\sf\wargamer\SRC\GAMESUP"
   $(MAKE) /$(MAKEFLAGS) /F .\gamesup.mak CFG="gamesup - Win32 Release" 
   cd "..\batdata"

"gamesup - Win32 ReleaseCLEAN" : 
   cd "\src\sf\wargamer\SRC\GAMESUP"
   $(MAKE) /$(MAKEFLAGS) /F .\gamesup.mak CFG="gamesup - Win32 Release" RECURSE=1 CLEAN 
   cd "..\batdata"

!ELSEIF  "$(CFG)" == "batdata - Win32 Debug"

"gamesup - Win32 Debug" : 
   cd "\src\sf\wargamer\SRC\GAMESUP"
   $(MAKE) /$(MAKEFLAGS) /F .\gamesup.mak CFG="gamesup - Win32 Debug" 
   cd "..\batdata"

"gamesup - Win32 DebugCLEAN" : 
   cd "\src\sf\wargamer\SRC\GAMESUP"
   $(MAKE) /$(MAKEFLAGS) /F .\gamesup.mak CFG="gamesup - Win32 Debug" RECURSE=1 CLEAN 
   cd "..\batdata"

!ELSEIF  "$(CFG)" == "batdata - Win32 Editor Debug"

"gamesup - Win32 Editor Debug" : 
   cd "\src\sf\wargamer\SRC\GAMESUP"
   $(MAKE) /$(MAKEFLAGS) /F .\gamesup.mak CFG="gamesup - Win32 Editor Debug" 
   cd "..\batdata"

"gamesup - Win32 Editor DebugCLEAN" : 
   cd "\src\sf\wargamer\SRC\GAMESUP"
   $(MAKE) /$(MAKEFLAGS) /F .\gamesup.mak CFG="gamesup - Win32 Editor Debug" RECURSE=1 CLEAN 
   cd "..\batdata"

!ELSEIF  "$(CFG)" == "batdata - Win32 Editor Release"

"gamesup - Win32 Editor Release" : 
   cd "\src\sf\wargamer\SRC\GAMESUP"
   $(MAKE) /$(MAKEFLAGS) /F .\gamesup.mak CFG="gamesup - Win32 Editor Release" 
   cd "..\batdata"

"gamesup - Win32 Editor ReleaseCLEAN" : 
   cd "\src\sf\wargamer\SRC\GAMESUP"
   $(MAKE) /$(MAKEFLAGS) /F .\gamesup.mak CFG="gamesup - Win32 Editor Release" RECURSE=1 CLEAN 
   cd "..\batdata"

!ENDIF 


!ENDIF 

