/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef BCREATE_HPP
#define BCREATE_HPP

#ifndef __cplusplus
#error bcreate.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *      Random Battlefield creation utility
 *
 *----------------------------------------------------------------------
 */

#include "bd_dll.h"
#include "batinfo.hpp"

class BattleData;

namespace BattleCreate
{                       
    BATDATA_DLL void createRandomBattle(BattleData* batData, const BattleInfo& info);
    BATDATA_DLL void deleteRandomBattle(BattleData* batData);
            // Create battlefield and OB to test Battle game

    BATDATA_DLL void createBattleField(BattleData* batdata, const BattleInfo& info);
    BATDATA_DLL void createRandomOB(BattleData* batData, bool noUnits = false);
};


#endif /* BCREATE_HPP */

