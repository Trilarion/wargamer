/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef BOB_CP_HPP
#define BOB_CP_HPP

#ifndef __cplusplus
#error bob_cp.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *      Battle Command Positions
 *
 *----------------------------------------------------------------------
 */

#include "bd_dll.h"
#include "batunit.hpp"
#include "batord.hpp"
#include "obdefs.hpp"
#include "cp.hpp"
#include "rank.hpp"
#include "bob_sp.hpp"
#include "range.hpp"
#include "batlist.hpp"
#include <map>
#include <vector>

class OrderBattle;
class FileReader;
class FileWriter;

// using BattleMeasure::BattlePosition;

/*-------------------------------------------------------------------
 * A Deploy Item consists of a Hex, and a sp that occupies the hex
 */

struct DeployItem {
  BattleMeasure::HexCord d_hex;      // what hex does this item pertain to
  BattleMeasure::HexCord d_wantHex;  // next hex this item is to represent
  RefBattleSP d_sp;                  // sp that occupies hex
  RefBattleSP d_wantSP;              // sp that wants to occupy hex

  static const UWORD s_fileVersion;

  DeployItem() :
         d_hex(),
         d_wantHex(),
         d_sp(NoBattleSP),
         d_wantSP(NoBattleSP) {}

  void sync()
  {
         d_hex = d_wantHex;
         d_sp = d_wantSP;
  }

  void syncHexToSP()
  {
         d_hex = d_sp->hex();
         d_wantHex = d_hex;
		 d_wantSP = d_sp;
  }

//private:
  DeployItem(const DeployItem& di) :
			d_hex(di.d_hex),
         d_wantHex(di.d_wantHex),
         d_sp(di.d_sp),
         d_wantSP(di.d_wantSP) {}

  DeployItem& operator = (const DeployItem& di)
  {
         d_hex = di.d_hex;
         d_wantHex = di.d_wantHex;
         d_sp = di.d_sp;
         d_wantSP = di.d_wantSP;

         return *this;
  }

  bool active()     const { return (d_sp != NoBattleSP); }
  bool wantActive() const { return (d_wantSP != NoBattleSP); }

  /*
        * File Interface
        */

  Boolean readData(FileReader& f, OrderBattle& ob);
  Boolean writeData(FileWriter& f, OrderBattle& ob) const;
};

// where does a reinforcement enter at?
enum ReinforcementEntryPoint {
	REP_Rear,           // to our rear
	REP_NearLeft,       // near left flank
	REP_FarLeft,        // far left flank
	REP_NearRight,      // near right flank
	REP_FarRight,       // near left flank
	REP_EnemyRear,      // enemy rear
	REP_None,           // not a reinforcement
	REP_HowMany
};

/*
 * Battle CommandPosition
 */

class BATDATA_DLL BattleCP :
   public BattleUnit
{
   private:
      BattleCP(const BattleCP&);
      const BattleCP& operator = (const BattleCP&);

   public:
      BattleCP();
       virtual ~BattleCP();

      typedef Range<UBYTE, UBYTE_MAX>   Morale;
      typedef Range<UBYTE, UBYTE_MAX>   Fatigue;
      typedef Range<UBYTE, 100>         AmmoSupply;
      typedef Range<UBYTE, 3>           Disorder;

      /*
       * File Interface
       */

      Boolean readData(FileReader& f, OrderBattle* ob);
      Boolean writeData(FileWriter& f, OrderBattle* ob) const;

      /*
      * Required functions for STL to work
      */

       friend bool operator < (const BattleCP& l, const BattleCP& r);
       friend bool operator == (const BattleCP& l, const BattleCP& r);

      // Accessors

      void init(const RefGenericCP& cp);
      void parent(ParamRefBattleCP cp);
      void sister(ParamRefBattleCP cp);
      void child(ParamRefBattleCP cp);
      CPIndex self() const;

      ReturnRefBattleCP parent();
      ReturnRefBattleCP sister();
      ReturnRefBattleCP child();

      ReturnCRefBattleCP parent() const;
      ReturnCRefBattleCP sister() const;
      ReturnCRefBattleCP child() const;

      const RefGenericCP& generic();
      const ConstRefGenericCP& generic() const;

      void generic(const RefGenericCP& cp);

      void morale(Morale::Value val);
      void fatigue(Fatigue::Value val);

      void ammoSupply(AmmoSupply::Value val);

      Morale::Value     morale()     const;
      Fatigue::Value    fatigue()    const;

      AmmoSupply::Value ammoSupply() const;
      Disorder::Value   disorder()   const;

      UBYTE moralePercent()     const;
      UBYTE fatiguePercent()    const;
      UBYTE ammoSupplyPercent() const;
      UBYTE startMorale() const;

      void startMorale(Morale::Value sm);
      void addMorale(Morale::Value m);
      void addFatigue(Fatigue::Value f);
      void addAmmoSupply(AmmoSupply::Value a);
      void removeMorale(Morale::Value m);
      void removeFatigue(Fatigue::Value f);
      void removeAmmoSupply(AmmoSupply::Value a);
      UINT startStrength() const;
      void startStrength(UINT s);

      UINT lastStrength() const;
      void lastStrength(UINT s);

      void disorder(Disorder::Value v);
      void decreaseDisorder();
      void increaseDisorder();

#ifdef _MSC_VER   // operator << can't work on this if private
   public:
#else
   private:
#endif
      // what is the state of our nerves, etc.
      enum Status {
         Shaken                        = 0x0001,
         Routing                       = 0x0002,
         CriticalLossReached           = 0x0004,
         NoEffectCutoffReached         = 0x0008,
         Rallying                      = 0x0010,
         Pursuing                      = 0x0020,
         Wavering                      = 0x0040,
         FleeingTheField               = 0x0080,
         Active                        = 0x0100,
         AggressionLocked              = 0x0200,
         PostureLocked                 = 0x0400,
         Retreating                    = 0x0800,
         MovingOverBridge              = 0x1000,
         NeedsReadjustment	  	         = 0x2000,
         OffMap                        = 0x4000,
         HistoricalBattleReinforcement = 0x8000
      };

   private:
      void setStatusFlag(UWORD mask, bool f);

   public:
       void setDead();
       void setFled();


      void criticalLossReached(bool f);
      void noEffectCutoffReached(bool f);
      void shaken(bool f);
      void routing(bool f);
      void rallying(bool f);
      void pursuing(bool f);
      void wavering(bool f);
      void fleeingTheField(bool f);
      void active(bool f);
      void aggressionLocked(bool f);
      void postureLocked(bool f);
      void retreating(bool f);
      void movingOverBridge(bool f);
      void needsReadjustment(bool f);
      void offMap(bool f);
      void historicalBattleReinforcent(bool f);

      bool criticalLossReached() const;
      bool noEffectCutoffReached() const;
      bool shaken()                const;
      bool routing()               const;
      bool rallying()		        const;
      bool pursuing()              const;
      bool wavering()              const;
      bool fleeingTheField()       const;
      bool active()                const;
      bool aggressionLocked()      const;
      bool postureLocked()         const;
      bool retreating()            const;
      bool movingOverBridge()      const;
      bool needsReadjustment()     const;
      bool offMap()                const;
      bool historicalBattleReinforcent() const;

      //what we are doing whilst near an enemy
      enum  NearEnemyMode {
         NE_NotNear,

         NE_HoldInPlace, // holding where we're at
         NE_Charge, // close with the enemy
         NE_ChargeAt1,
         NE_ChargeAt2,   // charge enemy if he comes with 2 hexes
         NE_ChargeAt3,

         NE_HaltAt1,
         NE_HaltAt2,     // halt at 2 hexes away
         NE_HaltAt3,     // halt at 3 hexes away
         NE_HaltAt4,     // halt at 4 hexes away
         NE_HaltAt5,     // halt at 5 hexes
         NE_HaltAt6,     // halt at 6 hexes

         NE_KeepAway2,   // keep enemy at least 2 hexes away
         NE_KeepAway3,   // keep enemy at least 2 hexes away
         NE_KeepAway4,   // keep enemy at least 2 hexes away
         NE_KeepAway5,
         NE_KeepAway6,

         NE_Retreat2,
         NE_Retreat3,
         NE_Retreat4,
         NE_Retreat5,
         NE_Retreat6,
         NE_HowMany,
         NE_First = NE_NotNear,
         NE_Default = NE_First
      };

      void nearEnemyMode(NearEnemyMode m);
      NearEnemyMode nearEnemyMode() const;

      void lockMode();
      void unlockMode();

       static const char* nearEnemyName(NearEnemyMode m);


       bool isActingOnOrder(void) const;
       bool canMove() const;
      Side getSide() const;
      void setSide(Side side);

      void setNation(Nationality n);
      Nationality getNation() const;

      void setRank(RankEnum rank);
      const Rank& getRank() const;

      const char* getName() const;

      RefGLeader leader() const;

      ReturnRefBattleSP sp();
      ReturnCRefBattleSP sp() const;
       void addSP(ParamRefBattleSP sp);
      // Add sp to end of spList
      void removeStrengthPoints();
      // Remove all StrengthPoints
      //void removeStrengthPoint(int c, int r);
       void removeStrengthPoint(RefBattleSP& thisSP);

      bool isUsed() const;

       BOB_Definitions::SPFormation spFormation() const; // { return formation(); }
      // Return a current SP Formation.

       BOB_Definitions::SPFormation destSPFormation() const; // { return formation(); }
      // Return a next SP Formation.

      void formation(BOB_Definitions::CPFormation f);
      BOB_Definitions::CPFormation formation() const;
      // Return current Divisional formation

      void nextFormation(BOB_Definitions::CPFormation f);
      BOB_Definitions::CPFormation nextFormation() const;
      // Return current Divisional formation
      void increaseAggression();
      void decreaseAggression();
      void aggression(BattleOrderInfo::Aggression a);
      BattleOrder::Aggression aggression() const;
      void posture(BattleOrderInfo::Posture p);
      void setDefensive();
      BattleOrder::Posture posture() const;

      void orderMode(BattleOrder::OrderMode m);
      BattleOrder::OrderMode orderMode() const;

      void targetCP(RefBattleCP thisUnit);
      ReturnRefBattleCP targetCP() const;

      BOB_Definitions::CPDeployHow deployWhichWay() const;

      BattleOrder& getCurrentOrder();
      const BattleOrder& getCurrentOrder() const;
      // Get current Order

      void setCurrentOrder(const BattleOrder& order);
      // Set the cirrent Order

       void lastOrder(BattleOrder* order) const;
      // Get the last order sent to unit

       BattleMeasure::BattleTime::Tick orderTicks() const;
       bool orderTicks(BattleMeasure::BattleTime::Tick ticks);
       bool lastOrderSent(BattleOrder* order) const;
      // Get the last order sent still in the que
       bool getNewOrder(BattleOrder* order);
      // If there are any orders in the order queue then copy to order and return true
       void setNextOrderCheck(BattleMeasure::BattleTime::Tick t) { d_nextOrderCheck = t; }
       bool nextOrderCheck(BattleMeasure::BattleTime::Tick t) { return (t >= d_nextOrderCheck); }

       bool getNewOrder(BattleMeasure::BattleTime::Tick ticks, BattleOrder* order);
      // If there are any orders in the order queue then copy to order and return true

       void setActingOrder(BattleMeasure::BattleTime::Tick when, const BattleOrder& order);
      // Set the current acting order, with a time for when it is ready

       bool getActingOrder(BattleMeasure::BattleTime::Tick when, BattleOrder* order);
      // Get acting order if time is up

      void sendOrder(BattleMeasure::BattleTime::Tick arrival, const BattleOrder& order)
      {
         d_orderList.addOrder(arrival, order);
      }
      // Add a new order to the order list

       const BattleOrderList * getOrderList(void) const { return &d_orderList; }
      // Get a pointer to orders list (Jim: used by display for checking if there are orders on the way)

      /*
       * SP Deployment map functions
       */

      //vector<DeployItem>& deployMap() { return d_deployMap.d_map; }
      //const vector<DeployItem>& deployMap() const { return d_deployMap.d_map; }

       std::vector<DeployItem>::iterator mapBegin() { return d_deployMap.d_map.begin(); }
       std::vector<DeployItem>::const_iterator mapBegin() const { return d_deployMap.d_map.begin(); }
       std::vector<DeployItem>::iterator mapEnd() { return d_deployMap.d_map.end(); }
       std::vector<DeployItem>::const_iterator mapEnd() const { return d_deployMap.d_map.end(); }
       int mapSize() const { return d_deployMap.d_map.size(); }
       void mapPopBack() { d_deployMap.d_map.pop_back(); }
       void mapResize(int size) { d_deployMap.d_map.resize(size); }

      void wantColumns(UBYTE n) { d_deployMap.d_wantColumns = n; }
      UBYTE wantColumns() const { return d_deployMap.d_wantColumns; }

      void wantRows(UBYTE n) { d_deployMap.d_wantRows = n; }
      UBYTE wantRows() const { return d_deployMap.d_wantRows; }

      void columns(UBYTE n) { d_deployMap.d_columns = n; }
      UBYTE columns() const { return d_deployMap.d_columns; }

      void rows(UBYTE n) { d_deployMap.d_rows = n; }
      UBYTE rows() const { return d_deployMap.d_rows; }

      void nArtillery(UBYTE a) { d_deployMap.d_nArtillery = a; }
      UBYTE nArtillery() const { return d_deployMap.d_nArtillery; }

       int spCount(bool all = False) const;

       void addToMap(RefBattleSP sp, const BattleMeasure::HexCord* hex, const int column, const int row);
      void lockMapItem(int i);
       void lockMap();
       void syncMapToSP();

      void destHex(const BattleMeasure::HexCord& hex) { d_deployMap.d_wantCPHex = hex; }
      BattleMeasure::HexCord& destHex() { return d_deployMap.d_wantCPHex; }
      const BattleMeasure::HexCord& destHex() const { return d_deployMap.d_wantCPHex; }

       BattleMeasure::HexCord centerHex() const;
       bool centerHexIfActive(BattleMeasure::HexCord * centerHex) const;
       BattleMeasure::HexCord rightHex() const;
       BattleMeasure::HexCord leftHex() const;
      // DeployItem& deployItem(const int column, const int row);
       DeployItem* currentDeployItem(int c, int r) const;
       DeployItem* wantDeployItem(int c, int r) const;

      enum HexOffset {
         HO_Left,
         HO_Right,

         HO_HowMany,
         HO_First = HO_Left
      };

      void whichWay(HexOffset ho) { d_hexOffset = ho; }
      HexOffset whichWay() const { return d_hexOffset; }

      enum MoveMode {
         Moving,
         OnManuever,
         LiningUp,
         Deploying,
         Holding,

         MoveMode_Last,
         MoveMode_First = OnManuever,
         MoveMode_Default = Holding
      };

      static const char* moveModeName(MoveMode m);

      void moveMode(MoveMode m);
      MoveMode moveMode() const;
      Boolean moving()     const;
      Boolean deploying()  const;
      Boolean liningUp()   const;
      Boolean onManuever() const;
      Boolean holding()    const;

      void nextFire(BattleMeasure::BattleTime::Tick at) { d_nextFire = at; }
      BattleMeasure::BattleTime::Tick nextFire() const { return d_nextFire; }

      void nextShockCheck(BattleMeasure::BattleTime::Tick at) { d_nextShockCheck = at; }
      BattleMeasure::BattleTime::Tick nextShockCheck() const { return d_nextShockCheck; }

      BattleList& targetList()             { return d_targets; }
      const BattleList& targetList() const { return d_targets; }

      enum HQ_AttachStatus {
         HQ_Unattached,
         HQ_Attached,

         HQ_AttachStatus_HowMany,
         HQ_AttachStatus_Default
      };

      void clearCompanion();
      void setCompanion(BattleSP* sp);
      BattleSP* companionSP() const;

      bool wantsAttach()             const;
      bool attached()                const;
      void setAttached();
      void setUnattached();
      void setAttached(HQ_AttachStatus s);

      bool showHQ() const;

      void moveDest(const BattleMeasure::HexCord& h);
      BattleMeasure::HexCord moveDest() const;

      void entryPoint(ReinforcementEntryPoint ep);
      ReinforcementEntryPoint entryPoint() const;

      /*
       * Implement BattleUnit
       */

      virtual void draw(UnitDrawer* drawer) const;

      /*
       * File Interface
       */

      Boolean readData(FileReader& f);
      Boolean writeData(FileWriter& f) const;

   private:
      static const UWORD s_fileVersion;

      RefGenericCP                     d_generic;                     // pointer to generic CommandPosition
      bool                             d_used;                                // set if data is valid

      // link to parent, sisters, children, etc.
      RefBattleCP                    d_parent;
      RefBattleCP                    d_sister;
      RefBattleCP                    d_child;
      RefBattleSP                    d_sp;                                  // chain of strength points

      // unit attributes
      Morale                          d_startMorale;
      UINT                            d_startStrength;
      UINT                            d_lastStrength;
      AmmoSupply                      d_ammoSupply;
      Disorder                        d_disorder;
      UWORD                           d_status;
      NearEnemyMode                   d_nearEnemyMode;
      bool                            d_lockMode;
      // current unit activity related stuff, etc.
      BOB_Definitions::CPFormation    d_formation;         // What formation it is in
      BOB_Definitions::CPFormation    d_nextFormation;   // Formation it is forming into
      BattleOrder::Aggression         d_aggression;      // Current active aggression
      BattleOrder::Posture            d_posture;         // Current active posture
      BattleOrder::OrderMode          d_orderMode;       // Order being carried out

      // unit orders and lists
      BattleOrder                     d_currentOrder;           // Order being carried out
      BattleSentOrder                 d_actingOrder;          // Order currently in in-tray
      BattleOrderList                 d_orderList;               // Last order that a messenger has sent

      MoveMode                        d_moveMode;  // Movement status
      BattleMeasure::BattleTime::Tick d_nextFire;  // When we fire next (once every 2 minutes)
      BattleMeasure::BattleTime::Tick d_nextShockCheck;  // When we next test for shock combat
      BattleMeasure::BattleTime::Tick d_lockUntil; // Aggression / Posture cannot be changed until this
      BattleMeasure::BattleTime::Tick d_nextOrderCheck; // Aggression / Posture cannot be changed until this

      // list of targets
      BattleList                      d_targets;

      // player plotted bombardement target
      RefBattleCP                     d_targetCP;
      RefBattleSP                     d_companionSP;

      BattleMeasure::HexCord          d_moveDest;

      ReinforcementEntryPoint         d_entryPoint;

      // a map of sp layout
      struct DeployMap {
         static const UWORD s_fileVersion;

	      std::vector<DeployItem> d_map;       // deploy SP to these hexes
	      BattleMeasure::HexCord d_wantCPHex;            // CommandPost deploys to this hex
	      UBYTE d_columns;
	      UBYTE d_rows;
	      UBYTE d_wantColumns;
	      UBYTE d_wantRows;
	      UBYTE d_nArtillery;             // how many artillery SP's does unit have

         DeployMap() :
                   d_map(),
                   d_wantCPHex(),
                   d_columns(0),
                   d_rows(0),
                   d_wantColumns(0),
                   d_wantRows(0),
                   d_nArtillery(0) {} //,

         /*
          * File Interface
          */

         Boolean readData(FileReader& f, OrderBattle& ob);
         Boolean writeData(FileWriter& f, OrderBattle& ob) const;
      };

      DeployMap d_deployMap;
      HexOffset d_hexOffset;

      // attach leader to attachTo unit
      // Note: this is not a formal attachment in the OB
      // but rather this CP is temporarily running the show for
      // for the attachTo unit.
      // Applies only to XXX level and above
      HQ_AttachStatus d_attachStatus;

//   friend bool operator >> (FileReader& f, Status& s);
//   friend bool operator << (FileWriter& f, const Status& s);


};



/*
 * CommandPosition List
 */

class BattleCPList
{
       typedef std::map<CPIndex, RefBattleCP, std::less<CPIndex> > Container;
       typedef Container::iterator iterator;
       typedef Container::const_iterator const_iterator;
       typedef Container::value_type value_type;

       class Error { };
   public:
       // typedef UWORD Index;
       // enum { NoIndex = UWORD_MAX };


       BattleCPList();
       ~BattleCPList();

               /*
        * File Interface
        */

       Boolean readData(FileReader& f, OrderBattle* ob);
       Boolean writeData(FileWriter& f, OrderBattle* ob) const;

       BattleCP& operator[](CPIndex cpi);
       const BattleCP& operator [](CPIndex cpi) const;

       BattleCP& operator[](GenericCP* cp);
       const BattleCP& operator[](const GenericCP* cp) const;

       int entries() const { return d_items.size(); }

       // void addCP(CPIndex cpi, RefGenericCP cp);
       // void delCP(CPIndex cpi);

       void cleanup();
               // Garbage collection

       void reset();   
               // delete all items

       ReturnRefBattleCP createUnit(const RefGenericCP& cp);
               // Create a new unit

       void removeUnit(RefBattleCP cp);

   private:
       static const UWORD fileVersion;

       Container d_items;
};



#endif /* BOB_CP_HPP */


