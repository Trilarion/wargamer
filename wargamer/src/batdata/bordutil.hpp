/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef BORDUTIL_HPP
#define BORDUTIL_HPP

#include "bd_dll.h"
// #include "batdata.hpp"
#include "BattleDataFwd.hpp"
#include "batord.hpp"
#include "bobdef.hpp"

class B_OrderUtil {
public:
  BATDATA_DLL static bool canGiveType(RCPBattleData bd, const BattleOrderInfo::OrderMode& type,
       const RefBattleCP& cp, const BattleMeasure::HexCord* hex = 0);
  static bool canDeployThisWay(RCPBattleData bd, BOB_Definitions::CPDeployHow dh, const RefBattleCP& cp);
  BATDATA_DLL static bool canSPDeployThisWay(RCPBattleData bd, BOB_Definitions::SPFormation sf, BasicUnitType::value t);
  BATDATA_DLL static bool isMoveOrder(BattleOrderInfo::OrderMode& type);
  static const WayPoint* nextToLastWayPoint(const BattleOrder& order);
  static bool lastMoveAttribs(const BattleOrder& order, BOB_Definitions::CPFormation& lastFormation, BattleMeasure::HexPosition::Facing& lastFacing);
};
#endif

/*----------------------------------------------------------------------------*
 * $Log$
 *----------------------------------------------------------------------------*
 */
