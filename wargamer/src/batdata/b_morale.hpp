/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef B_MORALE_HPP
#define B_MORALE_HPP

#ifndef __cplusplus
#error g_morale.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *      Generic morale calculation functions
 *
 *----------------------------------------------------------------------
 */

//#include "cpdef.hpp"
#include "bd_dll.h"
#include "batarmy.hpp"


//class Armies;


class BattleMoraleUtility
{
        public:
                BATDATA_DLL static Attribute CalcMorale(BattleOB * ob, BattleCP * cp);
                        // Calculates Morale for unit
                        // averages up any direct attached SPs and child CPs.
                        // The child CPs are not recalculated


#if defined(CUSTOMIZE)
                static void SetDefaultMorales(BattleOB * ob, BattleCP * cp);

                        // Set morales for any units with zero morale
#endif
};


#endif /* G_MORALE_HPP */

