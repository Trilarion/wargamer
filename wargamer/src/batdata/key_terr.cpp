/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
#include "stdinc.hpp"
#include "key_terr.hpp"

/*
        List of key-points in a randomly generated battlefield
*/

#if defined(DEBUG) && !defined(NOLOG)
#include "clog.hpp"
LogFileFlush KeyTerrainLog("KeyTerrain.log");
#endif

using namespace BattleMeasure;

void KeyTerrainList::crop(const HexCord& bottom_left, const HexCord& top_right) {

    // tempoary list for hexes which are within new dimensions
    SList <KeyTerrainPoint> tmp_list;
    tmp_list.reset();
    
    KeyTerrainPoint * p = d_list.first();

    // add all within bounds to tmp list
    while(p) {

        if(
            (p->hex().x() >= bottom_left.x()) &&
            (p->hex().x() < top_right.x()) &&
            (p->hex().y() >= bottom_left.y()) &&
            (p->hex().y() < top_right.y())) {

                KeyTerrainPoint * new_node = new KeyTerrainPoint;
                new_node->hex(p->hex() );
                new_node->type(p->type() );
                tmp_list.append(new_node);
            }

        p = d_list.next();
    }

    // wipe out d_list
    d_list.reset();

    p = tmp_list.first();

    // now copy back into d_list, adjusting the positions by bottom_left hexcord
    while(p) {

        HexCord pos = p->hex();
        int x = pos.x() - bottom_left.x();
        int y = pos.y() - bottom_left.y();

        KeyTerrainPoint * new_node = new KeyTerrainPoint;
        new_node->hex( HexCord(x,y) );
        new_node->type(p->type() );
        d_list.append(new_node);

        p = tmp_list.next();
    }
}






bool
KeyTerrainList::add(KeyTerrainPoint * node) {

    #if defined(DEBUG) && !defined(NOLOG)
    KeyTerrainLog.printf("Adding KeyTerrainPoint : %s at hex(%i, %i)....", node->getDescription(), node->hex().x(), node->hex().y() );
    #endif

    KeyTerrainPoint * p = d_list.first();

    // make sure there's no entries with this hex
    while(p) {

        if(p->hex() == node->hex()) {
            #if defined(DEBUG) && !defined(NOLOG)
            KeyTerrainLog.printf("Failed - point already assigned as %s\n", p->getDescription() );
            #endif
            return false;
        }
        
        p = d_list.next();
    }

    #if defined(DEBUG) && !defined(NOLOG)
    KeyTerrainLog.printf("Ok\n");
    #endif

    d_list.append(node);
    return true;
}




/*

  Go around the edges of the map, adding on points where paths leave the map

*/

void
KeyTerrainList::checkEdges(BattleMap * map) {

   HexCord mapsize = map->getSize();

   int x,y;
   HexCord hex;

   for(x=0; x<mapsize.x(); x++) {

      // top of map
      hex = HexCord(x,0);
      BattleTerrainHex & maphex = map->get(hex);
      if(maphex.d_path1.isRoad() || maphex.d_path2.isRoad() ) {
         KeyTerrainPoint * p = new KeyTerrainPoint;
            p->hex(hex);
            p->type(KeyTerrainPoint::PathExitingMap);
         if(!add(p)) delete p;
      }

      // bottom of map
      hex = HexCord(x,mapsize.y()-1);
      maphex = map->get(hex);
      if(maphex.d_path1.isRoad() || maphex.d_path2.isRoad() ) {
         KeyTerrainPoint * p = new KeyTerrainPoint;
            p->hex(hex);
            p->type(KeyTerrainPoint::PathExitingMap);
         if(!add(p)) delete p;
      }

   }

   for(y=0; y<mapsize.y(); y++) {

      // left of map
      hex = HexCord(0,y);
      BattleTerrainHex & maphex = map->get(hex);
      if(maphex.d_path1.isRoad() || maphex.d_path2.isRoad() ) {
         KeyTerrainPoint * p = new KeyTerrainPoint;
            p->hex(hex);
            p->type(KeyTerrainPoint::PathExitingMap);
         if(!add(p)) delete p;
      }

      // right of map
      hex = HexCord(mapsize.x()-1,y);
      maphex = map->get(hex);
      if(maphex.d_path1.isRoad() || maphex.d_path2.isRoad() ) {
         KeyTerrainPoint * p = new KeyTerrainPoint;
            p->hex(hex);
            p->type(KeyTerrainPoint::PathExitingMap);
         if(!add(p)) delete p;
      }

   }

}


/*

  Shows all the key terrain features as map labels

*/

void
KeyTerrainList::labelKeyTerrain(BattleMap * map) {

    KeyTerrainPoint * p = d_list.first();

    while(p) {

      BattleTerrainHex & maphex = map->get(p->hex() );

      maphex.d_label = strdup(p->getDescription());

      p = d_list.next();
   }
}






void
KeyTerrainList::check(void) {

    #if defined(DEBUG) && !defined(NOLOG)

    KeyTerrainLog.printf("Checking KeyTerrain points...\n");

    KeyTerrainPoint * p = d_list.first();

    // make sure there's no entries with this hex
    while(p) {

        KeyTerrainLog.printf(" - Type %s at Hex (%i, %i)\n", p->getDescription(), p->hex().x(), p->hex().y() );
        
        p = d_list.next();
    }

    KeyTerrainLog.printf("Ok\n");

    #endif
}




/*
 *----------------------------------------------------------------------
 * $Log$
 *----------------------------------------------------------------------
 */
