/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef B_SEND_HPP
#define B_SEND_HPP

#ifndef __cplusplus
#error b_send.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *	Send Battle Order via despatcher
 *
 *----------------------------------------------------------------------
 */


#include "batarmy.hpp"
// #include "batdata.hpp"
#include "BattleDataFwd.hpp"
#include "bob_cp.hpp"
#include "bobutil.hpp"
#include "wg_rand.hpp"
#include "options.hpp"
#include "bobiter.hpp"
#include "control.hpp"
#include "MultiplayerConnection.hpp"
#include "MultiplayerMsg.hpp"
#include "directplay.hpp"

#include "scenario.hpp"



#include "bd_dll.h"
#include "bobdef.hpp"
#include "mytypes.h"
#include "gdespat.hpp"
#include "batord.hpp"
// #include "StringPtr.hpp"

class BattleData;


class BattleDespatchMessage : public GDespatchMessage
{
	public:
		virtual ~BattleDespatchMessage() = 0;
		virtual void proc(BattleData* batData) = 0;

};

inline BattleDespatchMessage::~BattleDespatchMessage() { }




class BATDATA_DLL BattleDespatcher : private GDespatcher
{
      typedef GDespatcher Super;

    private:
        BattleDespatcher();     // Must use instance() to create
        ~BattleDespatcher() { }
	public:

		void process(BattleData* batData);
		virtual void actionMessage(GDespatchMessage* msg);
      static BattleDespatcher* instance();
      static void clear();
		inline BattleData * batData(void) { return d_batData; }
      void sendMessage(GDespatchMessage* msg, ULONG activationTime) { Super::sendMessage(msg, activationTime); }

		static MP_MSG_BATTLEORDER d_MultiplayerBattleOrder;
		static BattleData* d_batData;
		unsigned int maxSyncInterval;

	private:

      static BattleDespatcher* s_instance;

};




/*
	DS_BattleUnitOrder
*/

class DS_BattleUnitOrder : public BattleDespatchMessage
{
	public:
		BATDATA_DLL DS_BattleUnitOrder(CRefBattleCP cp, const BattleOrder* order);
		BATDATA_DLL DS_BattleUnitOrder(void) { }
		BATDATA_DLL ~DS_BattleUnitOrder(void) { }
		BATDATA_DLL void proc(BattleData* batData);

		BATDATA_DLL virtual int pack(void * buffer, void * gamedata);
		BATDATA_DLL virtual void unpack(void * buffer, void * gamedata);

#ifdef DEBUG
		BATDATA_DLL String getName();
#endif

	private:
		CRefBattleCP d_cp;
		BattleOrder d_order;

};



BATDATA_DLL void sendBattleOrder(CRefBattleCP cp, const BattleOrder* order);
BATDATA_DLL void procBattleDespatch(BattleData* batData);
BATDATA_DLL void clearBattleDespatch();
BATDATA_DLL void slaveSendBattleOrder(BattleDespatchMessage * msg, ULONG activationTime);

#endif /* B_SEND_HPP */

