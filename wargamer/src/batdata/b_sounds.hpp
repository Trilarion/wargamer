/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef B_SOUNDS_HPP
#define B_SOUNDS_HPP

#include "bd_dll.h"
#include "random.hpp"

/*

SoundLibrary

Table of structures which are built into the sound-table for the game

*/



typedef struct BattleSoundTableEntry {

    char * filename;
    // random factor by which frequency is altered each time played
    int frequency_variation;
    // how many buffers are defined for this sound
    int num_buffers;
    // buffers for this sound

        BattleSoundTableEntry(char * fname, int freq_var, int num_buff) {
            filename = fname;
            frequency_variation = freq_var;
            num_buffers = num_buff;
        }

} BattleSoundTableEntry;



enum BattleSoundTypeEnum {

    SOUNDTYPE_MUSKET1,
    SOUNDTYPE_MUSKET2,
    SOUNDTYPE_MUSKET3,
    SOUNDTYPE_MUSKET4,

    SOUNDTYPE_MUSKETS1,
    SOUNDTYPE_MUSKETS2,
    SOUNDTYPE_MUSKETS3,
    SOUNDTYPE_MUSKETS4,
    SOUNDTYPE_MUSKETS5,
    SOUNDTYPE_MUSKETS6,
    
    SOUNDTYPE_CANNON1,
    SOUNDTYPE_CANNON2,
    SOUNDTYPE_CANNON3,
    SOUNDTYPE_CANNON4,
    SOUNDTYPE_CANNON5,
    SOUNDTYPE_CANNON6,
    SOUNDTYPE_CANNON7,
    SOUNDTYPE_CANNON8,

    SOUNDTYPE_HORSEGALLOP1,
    SOUNDTYPE_HORSEGALLOP2,

    SOUNDTYPE_CROWD1,
    SOUNDTYPE_CROWD2,
    SOUNDTYPE_CROWD3,
    SOUNDTYPE_CROWD4,

    SOUNDTYPE_WHINNY1,
    SOUNDTYPE_WHINNY2,
    SOUNDTYPE_WHINNY3,

    SOUNDTYPE_SWORDS1,
    SOUNDTYPE_SWORDS2,

    SOUNDTYPE_SMASH1,

    SOUNDTYPE_SQUEAK1,
    SOUNDTYPE_SQUEAK2,

    SOUNDTYPE_DRUMS1,

    SOUNDTYPE_TROOPMOVE1,
    SOUNDTYPE_TROOPMOVE2,

    SOUNDTYPE_EXPLOSION1,
    SOUNDTYPE_EXPLOSION2,

    SOUNDTYPE_DEATH1,
    SOUNDTYPE_DEATH2,
    SOUNDTYPE_DEATH3,
    SOUNDTYPE_DEATH4,

	SOUNDTYPE_FIRE1,
	SOUNDTYPE_FIRE2,

    SOUNDTYPE_MENUPOPUP,
    SOUNDTYPE_MENUSELECT,
    SOUNDTYPE_WINDOWOPEN,
    SOUNDTYPE_WINDOWCLOSE,

	SOUNDTYPE_FACINGCHANGE,

    SOUNDTYPE_FIRSTCROWD = SOUNDTYPE_CROWD1,
    SOUNDTYPE_LASTCROWD = SOUNDTYPE_CROWD4+1,

    SOUNDTYPE_FIRSTHORSEGALLOP = SOUNDTYPE_HORSEGALLOP1,
    SOUNDTYPE_LASTHORSEGALLOP = SOUNDTYPE_HORSEGALLOP2+1,

    SOUNDTYPE_FIRSTWHINNY = SOUNDTYPE_WHINNY1,
    SOUNDTYPE_LASTWHINNY = SOUNDTYPE_WHINNY3+1,
    
    SOUNDTYPE_FIRSTMUSKET = SOUNDTYPE_MUSKET1,
    SOUNDTYPE_LASTMUSKET = SOUNDTYPE_MUSKET4 +1,

    SOUNDTYPE_FIRSTMUSKETS = SOUNDTYPE_MUSKETS1,
    SOUNDTYPE_LASTMUSKETS = SOUNDTYPE_MUSKETS6 +1,

    SOUNDTYPE_FIRSTCANNON = SOUNDTYPE_CANNON1,
    SOUNDTYPE_LASTCANNON = SOUNDTYPE_CANNON8 +1,

    SOUNDTYPE_FIRSTSWORDS = SOUNDTYPE_SWORDS1,
    SOUNDTYPE_LASTSWORDS = SOUNDTYPE_SWORDS2+1,

    SOUNDTYPE_FIRSTSMASH = SOUNDTYPE_SMASH1,
    SOUNDTYPE_LASTSMASH = SOUNDTYPE_SMASH1+1,

    SOUNDTYPE_FIRSTSQUEAK = SOUNDTYPE_SQUEAK1,
    SOUNDTYPE_LASTSQUEAK = SOUNDTYPE_SQUEAK2+1,

    SOUNDTYPE_FIRSTDRUM = SOUNDTYPE_DRUMS1,
    SOUNDTYPE_LASTDRUM = SOUNDTYPE_DRUMS1+1,

    SOUNDTYPE_FIRSTTROOPMOVE = SOUNDTYPE_TROOPMOVE1,
    SOUNDTYPE_LASTTROOPMOVE = SOUNDTYPE_TROOPMOVE2+1,

    SOUNDTYPE_FIRSTEXPLOSION = SOUNDTYPE_EXPLOSION1,
    SOUNDTYPE_LASTEXPLOSION = SOUNDTYPE_EXPLOSION2+1,

    SOUNDTYPE_FIRSTDEATH = SOUNDTYPE_DEATH1,
    SOUNDTYPE_LASTDEATH = SOUNDTYPE_DEATH4+1,

    SOUNDTYPE_FIRSTFIRE = SOUNDTYPE_FIRE1,
    SOUNDTYPE_LASTFIRE = SOUNDTYPE_FIRE2+1    
};


BATDATA_DLL BattleSoundTypeEnum GetMusketSound(void);
BATDATA_DLL BattleSoundTypeEnum GetMusketsSound(void);
BATDATA_DLL BattleSoundTypeEnum GetCannonSound(void);
BATDATA_DLL BattleSoundTypeEnum GetCrowdSound(void);
BATDATA_DLL BattleSoundTypeEnum GetGallopSound(void);
BATDATA_DLL BattleSoundTypeEnum GetWhinnySound(void);
BATDATA_DLL BattleSoundTypeEnum GetSwordsSound(void);
BATDATA_DLL BattleSoundTypeEnum GetSmashSound(void);
BATDATA_DLL BattleSoundTypeEnum GetSqueakSound(void);
BATDATA_DLL BattleSoundTypeEnum GetDrumSound(void);
BATDATA_DLL BattleSoundTypeEnum GetTroopMoveSound(void);
BATDATA_DLL BattleSoundTypeEnum GetExplosionSound(void);
BATDATA_DLL BattleSoundTypeEnum GetDeathSound(void);
BATDATA_DLL BattleSoundTypeEnum GetFireSound(void);

extern const BattleSoundTableEntry * BattleSoundTable[];




#endif
