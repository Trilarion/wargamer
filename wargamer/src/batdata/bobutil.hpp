/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef BOBUTIL_HPP
#define BOBUTIL_HPP

#ifndef __cplusplus
#error bobutil.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *	Battle Order of Battle Utility Functions
 *
 *----------------------------------------------------------------------
 */

// let's try and replace these includes with forward references

#include "bd_dll.h"
#include "bobdef.hpp"
#include "gamedefs.hpp"
#include "batunit.hpp"
#include "obdefs.hpp"
#include "batlist.hpp"
#include "unittype.hpp"
#include "batcord.hpp"
#include "hexlist.hpp"
#include "BattleDataFwd.hpp"


class BattleUnitIter;
class BattleOB;
class OrderBattle;
class BattleList;
struct BattleOrderInfo;

namespace BattleMeasure 
{
   class HexCord; 
   class HexPosition;
}

namespace BobUtility
{

/*
 * Unit counting routines
 */

int countAttachedSP(ParamRefBattleCP cp);		// SPs attached directly to current unit
int countSP(BattleUnitIter* iter);
int countSP(BattleOB* ob, Side side);		// SPs in whole side
BATDATA_DLL int countSP(ParamRefBattleCP cp);	// SPs in and below cp
int countUnits(BattleUnitIter* iter);
BATDATA_DLL int countUnits(ParamRefBattleCP cp);	// CPs below cp
BATDATA_DLL int countChildren(ParamRefBattleCP cp);		// units directly atatched to cp
BATDATA_DLL void countArtillery(BattleOB* ob, ParamRefBattleCP cp);

/**
 * Counts the number of units satisfying the basicType & flags specified
 * if onlyThisCP is set, then only the CP provided will be processed
 */
BATDATA_DLL unsigned int countUnitType(BattleOB * ob, BattleCP * cp, BasicUnitType::value basicType, UnitTypeFlags::value flags, bool onlyThisCP);
/**
 * The recursive part of the above function
 * @see countUnitType
 */
unsigned int countUnitTypeRecursion(BattleOB * ob, BattleCP * cp, BasicUnitType::value basicType, UnitTypeFlags::value flags);

BATDATA_DLL CRefBattleCP getCinc(const BattleOB* ob, Side s);
BATDATA_DLL RefBattleCP getCinc(BattleOB* ob, Side s);

BATDATA_DLL void initEditUnitTypeFlags(BattleOB* ob, const RefBattleCP& cp); //tc
BATDATA_DLL void initEditUnitTypeFlags(BattleOB* ob); //tc
BATDATA_DLL void initEditUnitTypeFlags(RPBattleData bd); //tc


BATDATA_DLL void initUnitTypeFlags(BattleOB* ob, const RefBattleCP& cp);
BATDATA_DLL void initUnitTypeFlags(BattleOB* ob);
BATDATA_DLL void initUnitTypeFlags(RPBattleData bd);

// in manpower
struct UnitCounts {
	int d_nInf;
	int d_nCav;
	int d_nGuns;
	int d_nArtMP;

	UnitCounts() :
	  d_nInf(0),
	  d_nCav(0),
	  d_nGuns(0),
	  d_nArtMP(0) {}
};

BATDATA_DLL void unitsInManpower(const CRefBattleCP& cp, CPBattleData bd, UnitCounts& uc);

/**
 * Command Radius routines
 */

class WhatCRadius {
public:
  enum What {
	 OutOfRadius,  //!< not in command radius of any direct superior
	 InRadius,     //!< in radius of a direct superior (i.e. a XX in radius of its XXXX commander)
	 InRadiusCO,   //!< in radius of an immediate superior (i.e. a XX in radius of its XXX leader)
	 InRadiusCinC, //!< in radius of the battle CinC

	 HowMany,
	 First = OutOfRadius
  };
};

/**
 * Is unit in command radius of a direct superior (i.e. in its chain of command)
 * nHexes returns distance in hexes to leader
 */
BATDATA_DLL WhatCRadius::What whatCommandRadius(BattleOB* ob, const RefBattleCP& cp, int& hHexes);
/**
 * return leader who is within command radius of unit. Returns higest ranking
 */
BATDATA_DLL RefBattleCP leaderInRadius(BattleOB* ob, const RefBattleCP& cp);

//! return a leaders Command Radius in hexes
BATDATA_DLL int leaderRadius(BattleOB* ob, const RefBattleCP& cp);

/*
 * Unit's SP related routines
 */

BATDATA_DLL void getRowsAndColoumns(const BOB_Definitions::CPFormation f, const int spCount, int& columns, int& rows);
bool findDeployHexes(RPBattleData bd, const RefBattleCP& cp,
  BattleOrderInfo& order, const BattleMeasure::HexCord& srcHex, const BOB_Definitions::CPFormation formation, HexList& list);

/*
 * Other unit stuff
 */

BATDATA_DLL ReturnRefBattleCP getAttachedLeader(RPBattleData bd, const RefBattleCP& cp);
BATDATA_DLL ReturnCRefBattleCP getAttachedLeader(RCPBattleData bd, const CRefBattleCP& cp);

enum UnitStatus {
  Friendly,
  Enemy,

  UnitStatus_HowMany
};

//! are units (Friendly or Enemy) in this hex
BATDATA_DLL bool unitsUnderHex(CPBattleData bd, const BattleMeasure::HexCord& hex, const RefBattleCP& cp,
	UnitStatus who, bool XXOnly);

/** same as above but adds units to a list
 * @see unitsUnderHex
 */
BATDATA_DLL bool unitsUnderHex(CPBattleData bd, const BattleMeasure::HexCord& hex, const RefBattleCP& cp,
	BattleList& list, UnitStatus who, bool XXOnly, UWORD range, BattleItem::Position pos = BattleItem::Front);

BATDATA_DLL Attribute unitResponse(BattleOB* ob, const RefBattleCP& cp);
	// get average unit reponse value for a command
BATDATA_DLL Attribute unitControl(BattleOB* ob, const RefBattleCP& cp);
	// get average unit reponse value for a command
BATDATA_DLL Attribute unitImpact(BattleOB* ob, const RefBattleCP& cp);
	// get average unit Impact value for a command
BATDATA_DLL Attribute unitFireValue(BattleOB* ob, const RefBattleCP& cp);
	// get average unit reponse value for a command
BATDATA_DLL Attribute baseMorale(const BattleOB* ob, const CRefBattleCP& cp);
	// get average base morale for a unit (This represents unit quality, not battle morale)
BATDATA_DLL Attribute averageMorale(const CRefBattleCP& cp);
	// get average battle morale for a whole command
BATDATA_DLL bool changingSPFormation(const CRefBattleCP& cp);

BATDATA_DLL ReturnCRefBattleCP getCP(ParamCRefBattleUnit unit);
	// If unit is CP then return self
	// If unit is SP then return SP's parent
	// Otherwise return NoBattleCP
BATDATA_DLL ReturnCRefBattleSP getSP(ParamCRefBattleUnit unit);
	// If unit is SP then return self
	// Otherwise return NoBattleCP
BATDATA_DLL Side getSide(ParamCRefBattleUnit unit);
Visibility::Value getVisibility(ParamCRefBattleUnit unit);
BATDATA_DLL BattleMeasure::HexPosition::Facing avgUnitFacing(const CRefBattleCP& cp);

/*
 * Index conversion stuff for file routines
 */

BATDATA_DLL CPIndex cpToCPIndex(const CRefBattleCP& cpi, OrderBattle& ob);
BATDATA_DLL RefBattleCP cpIndexToCP(CPIndex cpi, OrderBattle& ob);

BATDATA_DLL LeaderIndex leaderToLeaderIndex(const ConstRefGLeader leader, OrderBattle& ob);
BATDATA_DLL RefGLeader leaderIndexToLeader(LeaderIndex iLeader, OrderBattle& ob);

BATDATA_DLL SPIndex spToSPIndex(ConstISP sp, OrderBattle& ob);
BATDATA_DLL ISP spIndexToSP(SPIndex spi, OrderBattle& ob);

BATDATA_DLL UINT unitSPStrength(RPBattleData bd, const RefBattleCP& cp, bool initializing = False);
BATDATA_DLL void setCommandMorale(const RefBattleCP& cp);
BATDATA_DLL bool canAttach(RCPBattleData bd, const CRefBattleCP& thisCP, const CRefBattleCP& thatCP);

/*
* Calculate an approximate victory level for given side
* for use in BSTATWIN victory bar
*/
BATDATA_DLL void approxVictoryLevels(RPBattleData bd, unsigned int * vLevel1, unsigned int * vLevel2);
BATDATA_DLL int distanceFromUnit(const BattleMeasure::HexCord& hex, const CRefBattleCP& cp);
BATDATA_DLL int distanceFromUnit(const CRefBattleCP& cp, const CRefBattleCP& enemyCP);

};


/*----------------------------------------------------------------------------
 * $Log$
*  Changes - MTC 122  add constructors for initEditUnitTypeFlags
 *----------------------------------------------------------------------------
 */

#endif /* BOBUTIL_HPP */

