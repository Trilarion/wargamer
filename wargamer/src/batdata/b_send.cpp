/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 * Send Battle Order via despatcher
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "b_send.hpp"
#include "batdata.hpp"
#include "battime.hpp"

#ifdef DEBUG
#include "clog.hpp"
static LogFile bdLog("BDespatch.log");
#endif


using BobUtility::WhatCRadius;
using namespace BattleMeasure;



BattleDespatcher* BattleDespatcher::s_instance = 0;
MP_MSG_BATTLEORDER BattleDespatcher::d_MultiplayerBattleOrder;
BattleData * BattleDespatcher::d_batData = 0;


BattleDespatcher::BattleDespatcher() 
{
   d_batData = 0;
   maxSyncInterval = 1;
}

void BattleDespatcher::process(BattleData* batData)
{
   d_batData = batData;
   GDespatcher::process(d_batData->getTick());
}

void BattleDespatcher::actionMessage(GDespatchMessage* msg)
{
   BattleDespatchMessage* bmsg = static_cast<BattleDespatchMessage*>(msg);
   bmsg->proc(d_batData);
}

BattleDespatcher* BattleDespatcher::instance() 
{
   if(s_instance == 0)
      s_instance = new BattleDespatcher;
   return s_instance;
}

void BattleDespatcher::clear() 
{
   delete s_instance;
   s_instance = 0;
}






DS_BattleUnitOrder::DS_BattleUnitOrder(CRefBattleCP cp, const BattleOrder* order) :
   d_cp(cp),
   d_order(*order)
{
}



int
DS_BattleUnitOrder::pack(void * buffer, void * gamedata) {

   BattleData * batData = (BattleData *) gamedata;
   int * dataptr = (int *) buffer;
   unsigned int num_bytes = 0;

   // pack d_cp
   *dataptr = d_cp->self();
   dataptr++;
   num_bytes+=4;

   // pack d_order
   int len = d_order.pack(dataptr, batData->ob()->ob());

   // return bytes packed
   return len + num_bytes;
}

void
DS_BattleUnitOrder::unpack(void * buffer, void * gamedata) {

   BattleData * batData = (BattleData *) gamedata;
   int * dataptr = (int *) buffer;

   int index = *dataptr;
   dataptr++;

   d_cp = BobUtility::cpIndexToCP(index, *batData->ob()->ob());
   d_order.unpack(dataptr, batData->ob()->ob());

}




#if 0
RefBattleCP getCinc(BattleOB* ob, Side s)
{
   RefBattleCP bestCP = NoBattleCP;

   for(BattleUnitIter unitIter(ob, s);
      !unitIter.isFinished();
      unitIter.next())
   {
      RefBattleCP cp = unitIter.cp();
      if(bestCP == NoBattleCP || cp->getRank().isHigher(bestCP->getRank().getRankEnum()))
         bestCP = cp;
      else if(cp->getRank().sameRank(bestCP->getRank().getRankEnum()))
      {
         ConstRefGLeader bestLeader = ob->ob()->whoShouldCommand(cp->leader(), bestCP->leader());
         if(bestLeader == cp->leader())
            bestCP = cp;
      }
   }

   ASSERT(bestCP != NoBattleCP);
   return bestCP;

}
#endif

void DS_BattleUnitOrder::proc(BattleData* batData)
{
   RefBattleCP cp = const_cast<RefBattleCP>(d_cp);
   // calculate time to arrival
#ifdef DEBUG
   bdLog.printf("------ Proc Order Sent for %s %s",
      scenario->getSideName(cp->getSide()), cp->getName());
#endif

   int timeTaken = 0;
   // Unchecked
   if(cp->firstOrderSent() &&
   // End
      !CampaignOptions::get(OPT_InstantOrders) &&
      GamePlayerControl::getControl(cp->getSide()) != GamePlayerControl::AI)
   {
     // first, get unit command radius status
     int nHexes = 0;
     WhatCRadius::What whatCR = BobUtility::whatCommandRadius(batData->ob(), cp, nHexes);
     ASSERT(whatCR < WhatCRadius::HowMany);

     // TODO: move table to scenario tables
     // values are in the number of seconds per hex
     // for order to travel
     static const UBYTE s_table[WhatCRadius::HowMany] = {
     // Unchecked
        60, 30, 30, 30
     // End
     };

     timeTaken = s_table[whatCR] * nHexes;

#ifdef DEBUG
     bdLog.printf("Raw time to arrival -- %d hexes at %d seconds per hex",
        nHexes, static_cast<int>(s_table[whatCR]));

     bdLog.printf("Unmodified time taken = %d minutes, %d seconds",
        static_cast<int>(timeTaken / 60),
        static_cast<int>(timeTaken % 60));
#endif
     // TODO: modify for weather
     // TODO: modify for CinC being attached to another unit

     // random modifier
     CRefBattleCP cinc = BobUtility::getCinc(batData->ob(), cp->getSide());//batData->ob()->getTop(cp->getSide());
     Attribute staffAtt = cinc->leader()->getStaff();
     // TODO: modify staff attribute for SHQ in chaos and squabbling leaders

     // convert staff to local enum for indexing puroposes
     enum {
       Staff51,  // staff is less than 52
       Staff102, // staff is less thatn 102, etc..
       Staff153,
       Staff204,
       Staff255,

       Staff_HowMany
     } staff = (staffAtt <= 51)  ? Staff51 :
               (staffAtt <= 102) ? Staff102 :
               (staffAtt <= 153) ? Staff153 :
               (staffAtt <= 204) ? Staff204 : Staff255;

     // TODO: move this table to scenario tables
     // value is a multiplier to order-time
     // time = (time * table-value) / base
     const int c_nValues = 10; // 10 possible random value
     const int c_base = 100;   // using a base of 100
     static const UBYTE s_rTable[Staff_HowMany][c_nValues] = {
        { 150, 150, 150, 150, 150, 150, 150, 133, 133, 120 }, // staff51
        { 150, 150, 150, 133, 133, 133, 133, 120, 120, 100 }, // staff52
        { 150, 133, 133, 120, 120, 120, 120, 100, 100,  80 }, // staff153
        { 133, 120, 120, 100, 100, 100, 100,  80,  80,  80 }, // staff204
        { 120, 100, 100,  80,  80,  80,  80,  80,  80,  80 }  // staff255
     };

     int rValue = CRandom::get(c_nValues);
     ASSERT(rValue < c_nValues);

     // modify random value
     // TODO: modify for CinC on higher terrain

     // modify for leader being specialist and commanding type (+1)
     if(cp->leader()->isCommandingType())
       rValue += 1;

     //  TODO: modify for CinC being attached to unit

     // modify if sending to a XXX (+1)
     if(cp->getRank().sameRank(Rank_Corps))
       rValue += 1;

     // modify if unit is artillery and is not commanded by specialist (-1)
     if(cp->getRank().sameRank(Rank_Division) &&
        cp->holding() &&
        cp->generic()->isArtillery() &&
        !cp->leader()->isCommandingType())
     {
       rValue -= 1;
     }

     // clip final rValue between 0 - 10
     rValue = clipValue(rValue, 0, c_nValues - 1);

     // convert from table value
     ASSERT(rValue < c_nValues);
     ASSERT(staff < Staff_HowMany);
     timeTaken = (timeTaken * s_rTable[staff][rValue]) / c_base;

#ifdef DEBUG
     bdLog.printf("Modified time taken = %d minutes, %d seconds\n",
        static_cast<int>(timeTaken / 60),
        static_cast<int>(timeTaken % 60));
#endif
   }

   cp->sendOrder(seconds(timeTaken) + batData->getTick(), d_order);
   // Unchecked
   if(!cp->firstOrderSent())
      cp->firstOrderSent(True);
   // End
}

#ifdef DEBUG
String DS_BattleUnitOrder::getName()
{
   return "Battle Unit Order";
}
#endif


/*
 * Global Functions
 */

void sendBattleOrder(CRefBattleCP cp, const BattleOrder* order)
{
   ULONG activationTime;

   DS_BattleUnitOrder * msg = new DS_BattleUnitOrder(cp, order);

   if(CMultiPlayerConnection::connectionType() != ConnectionNone) {

      activationTime = BattleDespatcher::instance()->batData()->getTick() + (BattleDespatcher::instance()->maxSyncInterval * 2);

      // pack message
      // (NOTE :including filling out the 'order_type' field)
      int data_length = msg->pack(BattleDespatcher::d_MultiplayerBattleOrder.data, BattleDespatcher::d_batData);

      // send message to remote machine
      if(g_directPlay) {

         BattleDespatcher::d_MultiplayerBattleOrder.msg_type = MP_MSG_BattleOrder;
         BattleDespatcher::d_MultiplayerBattleOrder.activation_time = activationTime;
         BattleDespatcher::d_MultiplayerBattleOrder.data_length = data_length;

         // message size is the base size, plus the size of the actual data
         unsigned int msg_size = 16 + data_length;

         // send to remote
         g_directPlay->sendMessage(
            (LPVOID) &BattleDespatcher::d_MultiplayerBattleOrder,
            msg_size
         );
      }
   }

   else activationTime = 0;

   BattleDespatcher::instance()->sendMessage(msg, activationTime);
}



void slaveSendBattleOrder(BattleDespatchMessage * msg, ULONG activationTime) {

   // send message internally
   BattleDespatcher::instance()->sendMessage(msg, activationTime);
}


void procBattleDespatch(BattleData* batData)
{
   BattleDespatcher::instance()->process(batData);
}

void clearBattleDespatch()
{
    BattleDespatcher::clear();
}

/*
 *----------------------------------------------------------------------
 * $Log$
 *----------------------------------------------------------------------
 */
