/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef B_OB_RAN_HPP
#define B_OB_RAN_HPP

#ifndef __cplusplus
#error b_ob_ran.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *	Create Random Tactical Order of Battle
 *
 *----------------------------------------------------------------------
 */

class BattleOB;

namespace BattleOBUtility
{
	BattleOB* createRandom(bool noUnits = false);
		// Create a Random Order of Battle

	void deleteRandom(BattleOB* ob);
		// Delete a randomly created Order of Battle

};	// namespace BattleOBUtility


#endif /* B_OB_RAN_HPP */

