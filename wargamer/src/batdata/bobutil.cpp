/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *      Battle Order of Battle Utility
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "bobutil.hpp"
#include "batdata.hpp"
#include "bobiter.hpp"
#include "bu_draw.hpp"
#include "batlist.hpp"
#include "hexmap.hpp"
#include "hexdata.hpp"
#include "options.hpp"
#include "control.hpp"

namespace BobUtility
{

using namespace BattleMeasure;
using namespace BOB_Definitions;

/*

  Counts the number of units satisfying the basicType & flags specified
  if onlyThisCP is set, then only the CP provided will be processed

*/

BATDATA_DLL unsigned int
countUnitType(BattleOB * ob, BattleCP * cp, BasicUnitType::value basicType, UnitTypeFlags::value flags, bool onlyThisCP) {

   unsigned int count = 0;

   // count up for initial CP + SPs
   BattleSP * sp = cp->sp();
   while(sp) {

      UnitTypeItem unitType = ob->getUnitType(sp->getUnitType());
      if((unitType.getBasicType() == basicType) && ((unitType.getFlags().get() & flags) == flags)) count++;

      sp = sp->sister();
   }

   // count up for all subsequent CPs + SPs
   if(!onlyThisCP) count += countUnitTypeRecursion(ob, cp->child(), basicType, flags);

   return count;
}


/*

  The recursive part of the above function

*/

unsigned int
countUnitTypeRecursion(BattleOB * ob, BattleCP * cp, BasicUnitType::value basicType, UnitTypeFlags::value flags) {

   if(!cp) return 0;

   unsigned int count = 0;

   // do for this CP + SPs
   BattleSP * sp = cp->sp();
   while(sp) {

      UnitTypeItem unitType = ob->getUnitType(sp->getUnitType());
      if((unitType.getBasicType() == basicType) && ((unitType.getFlags().get() & flags) == flags)) count++;

      sp = sp->sister();
   }

   // do for all child & sister CPs + SPs
   count+=countUnitTypeRecursion(ob, cp->child(), basicType, flags);
   count+=countUnitTypeRecursion(ob, cp->sister(), basicType, flags);

   return count;
}



/*--------------------------------------------------------------
 * Count number of SPs directly attached to a unit
 */

int countAttachedSP(ParamRefBattleCP cp)
{
        int count = 0;
        RefBattleSP sp = cp->sp();
        while(sp != NoBattleSP)
        {
          count++;
          sp = sp->sister();
        }
        return count;
}

/*
 * Count number of SPs in an already initialised iterator
 */

int countSP(BattleUnitIter* iter)
{
            int count = 0;

        while(!iter->isFinished())
        {
                count += countAttachedSP(iter->cp());

                iter->next();
            }
        return count;
}

/*
 * Count number of SPs in a complete side
 */

int countSP(BattleOB* ob, Side side)
{
        BattleUnitIter iter(ob, side);
        return countSP(&iter);
}

/*
 * Count number of SPs within an organization
 */

int countSP(ParamRefBattleCP cp)
{
        BattleUnitIter iter(cp);
        return countSP(&iter);
}

/*
 * Count all units returned by an iterator
 */

int countUnits(BattleUnitIter* iter)
{
        int count = 0;

        while(!iter->isFinished())
        {
                ++count;
                iter->next();
        }
        return count;
}

/*
 * Count all units below an organization
 */

int countUnits(ParamRefBattleCP cp)
{
        BattleUnitIter iter(cp);
        return countUnits(&iter) - 1;           // Don't include self
}

/*
 * count the units directly atatched to an organization
 */

int countChildren(ParamRefBattleCP cp)
{
        int count = 0;
        RefBattleCP child = cp->child();
        while(child != NoBattleCP)
        {
                ++count;
                child = child->sister();
        }
        return count;
}

void countArtillery(BattleOB* ob, ParamRefBattleCP cp)
{
  UBYTE nArty = 0;
  RefBattleSP sp = cp->sp();
  while(sp != NoBattleSP)
//  for(BattleSPIter spIter(ob, cp, BattleSPIter::ArtilleryOnly); !spIter.isFinished(); spIter.next())
  {
         const UnitTypeItem& uti = ob->getUnitType(sp);
         if(uti.getBasicType() == BasicUnitType::Artillery)
           nArty++;

         sp = sp->sister();
   }

   cp->nArtillery(nArty);
}

/*--------------------------------------------------------------------
 * Return a unit's organization
 * If a CP then return self
 * If an SP then return parent
 * otherwise return error
 *
 * 1) We could do this with dynamic_cast<>
 * 2) Or we could add virtual functions to BattleUnit
 * 3) Or we can use the existing virtual draw() function
 *
 *      (1) is easy, but means we must compile with typeid information
 * (2) could get out hand, as we miay think of other important virtual
 *     functions.  Also, why should BattleUnit have to know anything
 *     about BattleCP?
 * (3) Is fiddly, but only involves a virtual function call.
 */

ReturnCRefBattleCP getCP(ParamCRefBattleUnit unit)
{
#define GETCP_METHOD_RTTI

#if defined(GETCP_METHOD_VIRTUAL_DRAW)
            class GetCP : public UnitDrawer
            {
                        public:
                                    GetCP() : d_cp(NoBattleCP) { }
                                    ReturnCRefBattleCP operator()() const { return d_cp; }
                        private:
                                    void draw(const ParamRefBattleSP sp)
                                    {
                                                d_cp = sp->parent();
                                    }

                                    void draw(const ParamRefBattleCP cp)
                                    {
                                                d_cp = cp;
                                    }

                                    void draw(const BattleUnit* unit)
                                    {
                                                throw GeneralError("Unknown Unit type");
                                    }

                        private:
                                    CRefBattleCP d_cp;

            };

            GetCP cp;
            unit->draw(&cp);
            return cp();
#elif defined(GETCP_METHOD_RTTI)

            CRefBattleCP cp = dynamic_cast<const BattleCP*>(unit.value());
            if(cp != 0)
                        return cp;

            CRefBattleSP sp = dynamic_cast<const BattleSP*>(unit.value());
            if(sp != 0)
                        return sp->parent();

            throw GeneralError("Unknown unit type");
#else
            #error "Please define how to implement getCP()"
#endif
}


ReturnCRefBattleSP getSP(ParamCRefBattleUnit unit)
{
   CRefBattleSP sp = dynamic_cast<const BattleSP*>(unit.value());
   return sp;
}

Side getSide(ParamCRefBattleUnit unit)
{
   return getCP(unit)->getSide();
}

Visibility::Value getVisibility(ParamCRefBattleUnit unit)
{
   // if a player unit return full visibility
   if(GamePlayerControl::getControl(getSide(unit)) == GamePlayerControl::Player ||
       !CampaignOptions::get(OPT_FogOfWar))
   {
      return Visibility::Full;
   }

   return unit->visibility();
}

/*----------------------------------------------------------------
 * Command Raduis routines
 */

inline int squared(int v)
{
   return (v * v);
}

inline int getDist(const HexCord& srcHex, const HexCord& destHex)
{
  return static_cast<int>(sqrt(squared(destHex.x() - srcHex.x()) + squared(destHex.y() - srcHex.y())));
}

CRefBattleCP getCinc(const BattleOB* ob, Side s)
{
   CRefBattleCP bestCP = NoBattleCP;

   for(ConstBattleUnitIter unitIter(ob, s);
      !unitIter.isFinished();
      unitIter.next())
   {
      CRefBattleCP cp = unitIter.cp();
      if(bestCP == NoBattleCP || cp->getRank().isHigher(bestCP->getRank().getRankEnum()))
         bestCP = cp;
      else if(cp->getRank().sameRank(bestCP->getRank().getRankEnum()))
      {
         ConstRefGLeader bestLeader = ob->ob()->whoShouldCommand(cp->leader(), bestCP->leader());
         if(bestLeader == cp->leader())
            bestCP = cp;
      }
   }

   ASSERT(bestCP != NoBattleCP);
   return bestCP;
}

RefBattleCP getCinc(BattleOB* ob, Side s)
{
   const BattleOB* constOB = ob;
   return const_cast<BattleCP*>(getCinc(constOB, s));
}


bool inRadius(BattleOB* ob, const RefBattleCP& thisCP, HexCord& fromHex, const int radius)
{
   HexCord destHex = thisCP->hex();
  const int nHexes = getDist(fromHex, destHex);

  if(nHexes <= radius)
  {
         bool canTrace = True;

         // TODO: check for line of sight, enemy, friendly units not of this command etc.
         // we cannot trace CR through any of these

         return canTrace;
  }

  return False;
}

RefBattleCP leaderInRadius(BattleOB* ob, const RefBattleCP& cp)
{
  RefBattleCP cinc = getCinc(ob, cp->getSide());//ob->getTop(cp->getSide());

  /*----------------------------------------------------------
   *
   */

  // special cases
  // 1. We are cinc OR
  if( (cinc == cp) )
  {
      return cinc;
  }

  // get dist from HQ to CinC
  // Unchecked
  //HexCord srcHex = cp->hex();
  //HexCord cincHex = cinc->hex();
  // End
//  nHexes = getDist(srcHex, cincHex);

  // 2. are we in radius of CinC
  int lRadius = leaderRadius(ob, cinc);

  for(std::vector<DeployItem>::iterator di = cp->mapBegin();
      di != cp->mapEnd();
      di++)
   {
      if(di->active())
      {
         HexCord srcHex = di->d_sp->hex();
         if(inRadius(ob, cinc, srcHex, lRadius))
                return cinc;
      }
  }


   // 3. are we in radius of one of our
  RefBattleCP parentCP = cp->parent();
  while(parentCP != NoBattleCP && parentCP != cinc)
  {
         int lRadius = leaderRadius(ob, parentCP);
         for(std::vector<DeployItem>::iterator di = cp->mapBegin();
             di != cp->mapEnd();
             di++)
         {
           if(di->active())
           {
                HexCord srcHex = di->d_sp->hex();
                if(inRadius(ob, parentCP, srcHex, lRadius))
                  return parentCP; // (parentCP == cp->parent()) ? WhatCRadius::InRadiusCO : WhatCRadius::InRadius;
           }
         }

         parentCP = parentCP->parent();
#if 0
         if(parentCP == NoBattleCP)
         {
                FORCEASSERT("Parent not found");
                break;
         }
#endif
  }

   return NoBattleCP; //WhatCRadius::OutOfRadius;
}


// Unchecked
int distanceFromUnit(const HexCord& hex, const CRefBattleCP& cp)
{
   if(cp->getRank().isHigher(Rank_Division))
      return getDist(cp->hex(), hex);

   // get dist to closest SP
   int bestDist = UWORD_MAX;
   for(std::vector<DeployItem>::const_iterator di = cp->mapBegin();
      di != cp->mapEnd();
      di++)
   {
      if(di->active())
      {
         if(di->d_sp->hex() == hex)
         {
            bestDist = 0;
            break;
         }

         int dist = getDist(di->d_sp->hex(), hex);
         if(dist < bestDist)
            bestDist = dist;
      }
   }

   return bestDist;
}

int distanceFromUnit(const CRefBattleCP& cp, const CRefBattleCP& enemyCP)
{
   int closeDist = UWORD_MAX;
   if(enemyCP->getRank().sameRank(Rank_Division))
   {
      for(std::vector<DeployItem>::const_iterator di = enemyCP->mapBegin();
          di != enemyCP->mapEnd();
          ++di)
      {
         if(di->active())
         {
            HexCord hex = di->d_sp->hex();
            int d = distanceFromUnit(hex, cp);
            if(d < closeDist)
               closeDist = d;
         }
      }
   }

   else
      closeDist = distanceFromUnit(enemyCP->hex(), cp);

   return closeDist;
}

// End

WhatCRadius::What whatCommandRadius(BattleOB* ob, const RefBattleCP& cp, int& nHexes)
{
  RefBattleCP cinc = getCinc(ob, cp->getSide());//ob->getTop(cp->getSide());

  /*----------------------------------------------------------
        *
        */

  // special cases
  // 1. We are cinc OR
  if( (cinc == cp) )
  {
         nHexes = 0;
         return WhatCRadius::InRadiusCinC;
  }

  // get dist from HQ to CinC
  // Unchecked
  //HexCord srcHex = cp->leftHex();
  //HexCord cincHex = cinc->hex();
  //nHexes = getDist(srcHex, cincHex);
  nHexes = distanceFromUnit(cp, cinc);
  // End

  // 2. are we in radius of CinC
  int lRadius = leaderRadius(ob, cinc);

  for(std::vector<DeployItem>::iterator di = cp->mapBegin();
      di != cp->mapEnd();
      di++)
  {
      if(di->active())
      {
         HexCord srcHex = di->d_sp->hex();
         if(inRadius(ob, cinc, srcHex, lRadius))
                return WhatCRadius::InRadiusCinC;
      }
  }


  // 3. are we in radius of one of our
  RefBattleCP parentCP = cp->parent();
   while(parentCP != NoBattleCP && parentCP != cinc)
  {
         int lRadius = leaderRadius(ob, parentCP);
         for(std::vector<DeployItem>::iterator di = cp->mapBegin();
             di != cp->mapEnd();
             di++)
         {
           if(di->active())
           {
                HexCord srcHex = di->d_sp->hex();
                if(inRadius(ob, parentCP, srcHex, lRadius))
                  return (parentCP == cp->parent()) ? WhatCRadius::InRadiusCO : WhatCRadius::InRadius;
           }
         }

         parentCP = parentCP->parent();

//          if(parentCP == NoBattleCP)
//          {
//                 FORCEASSERT("Parent not found");
//                 break;
//          }
  }

  return WhatCRadius::OutOfRadius;
}

// get leaders radius in hexes
int leaderRadius(BattleOB* ob, const RefBattleCP& cp)
{
  ASSERT(cp->leader() != 0);
  ASSERT(cp->getRank().getRankEnum() >= Rank_ArmyGroup);
  ASSERT(cp->getRank().getRankEnum() < Rank_HowMany);

  // raw value
  UWORD cr = cp->leader()->getCommandRadiusAttribute();

  // modify according to command rank, nationality
  // TODO: move tables to scenario tables
  const int c_nNations = 14;
  const int c_base = 10;  // using a base of 10
  static const UBYTE s_table[Rank_HowMany][c_nNations] = {
          {  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0 }, // God - Shouldn't be called
          {  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0 }, // Executive - Shouldn't be called
          { 55, 50, 40, 40, 40, 40, 40, 40, 40, 40, 40, 40, 40, 40 }, // Army-Group
          { 45, 45, 30, 30, 30, 30, 30, 30, 30, 30, 30, 30, 30, 30 }, // Army
          { 30, 30, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20 }, // Corps
          { 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10 }  // Division
  };
  static const UBYTE s_shqTable[c_nNations] = {
          75, 55, 40, 40, 55, 40, 40, 40, 40, 40, 40, 40, 40, 40  // SHQ
   };

  const UBYTE tValue = (cp->leader()->isSupremeLeader()) ?
         s_shqTable[cp->leader()->getNation()] :
         s_table[cp->getRank().getRankEnum()][cp->leader()->nation()];

  // TODO: modifiers for Campaign Conditions, i.e. Squabblin Leaders
  // SHQ in chaos etc.

  // value is modified CommandRadius / 100 + 1, rounded out
   return ((((cr * tValue) / c_base) + 50) / 100) + 1;
}

/*----------------------------------------------------------------
 * Unit's SP related routines
 */
#if 0
void allocateDeployMap(BattleOB* ob, ParamRefBattleCP cp)
{
        /*
         * First count number of SP
         */

        int spCount = 0;
        /*
        UNCHECKED CHANGE TO COUNT SP's NOT USING ITER
            */
        RefBattleSP spcounter = cp->sp();
        while(spcounter) {
            spCount++;
            spcounter = spcounter->sister();
        }
/*
        BattleSPIter spIter(ob, cp);
        while(!spIter.isFinished())
        {
          spCount++;
          spIter.next();
        }
*/
        int columns = 0;
        int rows = 0;

        if(spCount > 0)
        {
          // add 1 to count if this is an odd number,
          // we want an even number of deploy items
          if((spCount % 2) != 0)
                 spCount++;


          // count rows and columns
               switch(cp->formation())
          {
                 case CPF_March:
                 {
                        columns = 1;
                        rows = spCount;
                        break;
                 }

                 case CPF_Massed:
                  {
                        columns = 2;
                        rows = spCount / 2;
                        break;
                 }

                 case CPF_Deployed:
                 {
                        columns = spCount / 2;
                        rows = 2;
                        break;
                 }

                 case CPF_Extended:
                 {
                        columns = spCount;
                        rows = 1;
                        break;
                 }

#ifdef DEBUG
                 default:
                        FORCEASSERT("Inproper Divisional formation in allocateDeployMap()");
#endif
         }
  }

  cp->columns(columns);
  cp->rows(rows);
  cp->wantColumns(columns);
  cp->wantRows(rows);

  const int howMuch = rows * columns;
  std::vector<DeployItem>& map = cp->deployMap();

  if(howMuch > 0)
  {
         map.reserve(howMuch);

         for(int i = 0; i < howMuch; i++)
         {
                DeployItem di;
                map.push_back(di);
         }
  }
}
#endif

Attribute unitResponse(BattleOB* ob, const RefBattleCP& cp)
{
  ULONG value = 0;
  int count = 0;
   for(BattleUnitIter uIter(cp); !uIter.isFinished(); uIter.next())
  {
    for(BattleSPIter spIter(ob, uIter.cp(), BattleSPIter::All); !spIter.isFinished(); spIter.next())
    {
        const UnitTypeItem& uti = ob->getUnitType(spIter.sp()->getUnitType());

        value += uti.getResponse();
        count++;
    }
  }

  return (count > 0) ? static_cast<Attribute>(value / count) : 0;
}

Attribute unitControl(BattleOB* ob, const RefBattleCP& cp)
{
   ULONG value = 0;
  int count = 0;
  for(BattleUnitIter uIter(cp); !uIter.isFinished(); uIter.next())
  {
    for(BattleSPIter spIter(ob, uIter.cp(), BattleSPIter::All); !spIter.isFinished(); spIter.next())
    {
        const UnitTypeItem& uti = ob->getUnitType(spIter.sp()->getUnitType());

        value += uti.getControl();
        count++;
    }
  }

  return (count > 0) ? static_cast<Attribute>(value / count) : 0;
}

Attribute unitImpact(BattleOB* ob, const RefBattleCP& cp)
{
    ULONG value = 0;
   int count = 0;
   for(BattleUnitIter uIter(cp); !uIter.isFinished(); uIter.next())
   {
      for(BattleSPIter spIter(ob, uIter.cp(), BattleSPIter::All); !spIter.isFinished(); spIter.next())
      {
            const UnitTypeItem& uti = ob->getUnitType(spIter.sp()->getUnitType());

            value += uti.getImpact();
            count++;
      }
   }

   return (count > 0) ? static_cast<Attribute>(value / count) : 0;
}

Attribute unitFireValue(BattleOB* ob, const RefBattleCP& cp)
{
   ULONG value = 0;
   int count = 0;
   for(BattleUnitIter uIter(cp); !uIter.isFinished(); uIter.next())
   {
             for(BattleSPIter spIter(ob, uIter.cp(), BattleSPIter::All); !spIter.isFinished(); spIter.next())
             {
                        const UnitTypeItem& uti = ob->getUnitType(spIter.sp()->getUnitType());

                        value += uti.getFireValue();
                        count++;
             }
   }

  return (count > 0) ? static_cast<Attribute>(value / count) : 0;
}

Attribute baseMorale(const BattleOB* ob, const CRefBattleCP& cp)
{
   ULONG value = 0;
   int count = 0;
   for(ConstBattleUnitIter uIter(cp); !uIter.isFinished(); uIter.next())
   {
         for(ConstBattleSPIter spIter(ob, uIter.cp(), BattleSPIter::All); !spIter.isFinished(); spIter.next())
         {
                const UnitTypeItem& uti = ob->getUnitType(spIter.sp()->getUnitType());

                value += uti.getBaseMorale();
                count++;
         }
   }

   return (count > 0) ? static_cast<Attribute>(value / count) : 0;
}

Attribute averageMorale(const CRefBattleCP& cp)
   // get average battle morale for a whole command
{
   if(cp->getRank().sameRank(Rank_Division) ||
       cp->child() == NoBattleCP)
   {
      return cp->morale();
   }

   ULONG value = 0;
   int count = 0;
   for(ConstBattleUnitIter uIter(cp); !uIter.isFinished(); uIter.next())
   {
      if(uIter.cp()->getRank().sameRank(Rank_Division))
      {
         int c = uIter.cp()->spCount(True);
         value += (uIter.cp()->morale() * c);
         count += c;
      }
   }

   return (count > 0) ? static_cast<Attribute>(value / count) : 0;
}

bool changingSPFormation(const CRefBattleCP& cp)
{
   {
      for(std::vector<DeployItem>::const_iterator di = cp->mapBegin();
            di != cp->mapEnd();
            ++di)
      {
         if(di->active() && di->d_sp->formation() == SP_ChangingFormation)
            return True;
      }
   }

   return False;
}

/*
 * Index conversion stuff for file routines
 */

CPIndex cpToCPIndex(const CRefBattleCP& cp, OrderBattle& ob)
{
  return (cp != NoBattleCP) ? ob.getIndex(cp->generic()) : NoCPIndex;
}

RefBattleCP cpIndexToCP(CPIndex cpi, OrderBattle& ob)
{
  return (cpi != NoCPIndex) ? battleCP(ob.command(cpi)) : NoBattleCP;
}

LeaderIndex leaderToLeaderIndex(const ConstRefGLeader leader, OrderBattle& ob)
{
  return (leader != NoGLeader) ? ob.getIndex(leader) : NoLeaderIndex;
}

RefGLeader leaderIndexToLeader(LeaderIndex iLeader, OrderBattle& ob)
{
  return (iLeader != NoLeaderIndex) ? ob.leader(iLeader) : NoGLeader;
}

SPIndex spToSPIndex(ConstISP sp, OrderBattle& ob)
{
  return (sp != NoStrengthPoint) ? ob.getIndex(sp) : NoSPIndex;
}

ISP spIndexToSP(SPIndex spi, OrderBattle& ob)
{
  return (spi != NoSPIndex) ? ob.sp(spi) : NoStrengthPoint;
}

// get attached leader if any
// Note: this is not the cp->leader() value but
// rather a higher commander (such as a corp commander or the cinc)
// that has taken temporary command of the unit
ReturnCRefBattleCP getAttachedLeader(RCPBattleData bd, const CRefBattleCP& cp)
{
  // if we have an attached leader, he comes along to
  CRefBattleCP attachedTo = NoBattleCP;

  CRefBattleCP cinc = getCinc(bd->ob(), cp->getSide());//bd->ob()->getTop(cp->getSide());
  if(cinc->attached() && cinc->getCurrentOrder().attachTo() == cp)
         attachedTo = cinc;

  else
  {
         CRefBattleCP parent = cp->parent();
         while(parent != NoBattleCP && parent != cinc)
         {
            if(parent->attached() && parent->getCurrentOrder().attachTo() == cp)
            {
               attachedTo = parent;
               break;
            }

            parent = parent->parent();
         }
   }

   return attachedTo;
}

ReturnRefBattleCP getAttachedLeader(RPBattleData bd, const RefBattleCP& cp)
{
   RCPBattleData constBD = bd;
   return const_cast<BattleCP*>(getAttachedLeader(constBD, cp));
}




// see if a unit is in the hex
bool unitsUnderHex(CPBattleData bd, const HexCord& hex, const RefBattleCP& cp,
         UnitStatus who, bool XXOnly)
{
   const BattleHexMap* bm = bd->hexMap();
   BattleHexMap::const_iterator lower;
   BattleHexMap::const_iterator upper;

   bool has = False;
   bm->find(hex, lower, upper);
   while(lower != upper)
   {
         const BattleUnit* bu = bm->unit(lower);
         RefBattleCP hq = const_cast<RefBattleCP>(getCP(bu));

         // add if enemy and he has sp add to target list.
         // for now, do not add HQ with no SP
         bool shouldAdd = (XXOnly) ? (hq->sp() != NoBattleSP) : True;
         bool unitStatus = (who == Friendly) ?
                  (hq->getSide() == cp->getSide()) : (hq->getSide() != cp->getSide());

         if(unitStatus && shouldAdd)
         {
                has = True;
         }

         ++lower;
   }

   return has;
}

// see if a unit is in the hex
bool unitsUnderHex(CPBattleData bd, const HexCord& hex, const RefBattleCP& cp,
         BattleList& list, UnitStatus who, bool XXOnly, UWORD range, BattleItem::Position pos)
{
   const BattleHexMap* bm = bd->hexMap();
   BattleHexMap::const_iterator lower;
   BattleHexMap::const_iterator upper;

   bool has = False;
   bm->find(hex, lower, upper);
   while(lower != upper)
   {
         const BattleUnit* bu = bm->unit(lower);
         RefBattleCP hq = const_cast<RefBattleCP>(getCP(bu));

         // add if enemy and he has sp add to target list.
         // for now, do not add HQ with no SP
         bool shouldAdd = (XXOnly) ? (hq->sp() != NoBattleSP) : True;
         bool unitStatus = (who == Friendly) ?
                  (hq->getSide() == cp->getSide()) : (hq->getSide() != cp->getSide());

         if(unitStatus && shouldAdd)
         {
           has = True;
           int fv = 0;
           list.addFireValue(hq, range, fv, pos, False);
#if 0
                if(!list.inList(hq))
                {
                  list.newItem(hq, range, pos);
                }
#endif
         }

         ++lower;
   }

   return has;
}

void unitsInManpower(const CRefBattleCP& cp, CPBattleData bd, UnitCounts& uc)
{
   for(ConstBattleUnitIter uIter(cp); !uIter.isFinished(); uIter.next())
   {
         for(ConstBattleSPIter spIter(bd->ob(), uIter.cp(), BattleSPIter::All); !spIter.isFinished(); spIter.next())
         {
                const UnitTypeItem& uti = bd->ob()->getUnitType(spIter.sp());
                if(uti.getBasicType() == BasicUnitType::Infantry)
                {
                  uc.d_nInf += MulDiv(UnitTypeConst::InfantryPerSP, spIter.sp()->strength(100), 100);
                }
                else if(uti.getBasicType() == BasicUnitType::Cavalry)
                {
                  uc.d_nCav += MulDiv(UnitTypeConst::CavalryPerSP, spIter.sp()->strength(100), 100);
                }
                else if(uti.getBasicType() == BasicUnitType::Artillery)
                {
                  uc.d_nGuns += MulDiv(UnitTypeConst::GunsPerSP, spIter.sp()->strength(100), 100);
                  uc.d_nArtMP += MulDiv(UnitTypeConst::ArtilleryPerSP, spIter.sp()->strength(100), 100);
                }
         }
  }
}

void getRowsAndColoumns(const CPFormation f, const int spCount, int& columns, int& rows)
{
   if(spCount > 2)
   {
        if(f == CPF_March)
        {
          columns = 1;
          rows = spCount;//((spCount % 2) == 0) ? spCount : spCount + 1;
        }
        else if(f == CPF_Massed)
        {
          columns = 2;
          rows = (spCount + 1) / 2;
        }
        else if(f == CPF_Deployed)
        {
          columns = (spCount + 1) / 2;
          rows = 2;
        }
        else
        {
          columns = spCount;
          rows = 1;
        }
   }
   else
   {
        if(f == CPF_March)
        {
          columns = 1;
          rows = spCount;//((spCount % 2) == 0) ? spCount : spCount + 1;
        }
        else
        {
          columns = spCount;//((spCount % 2) == 0) ? spCount : spCount + 1;
          rows = 1;
        }
   }
}

bool addToList(RPBattleData bd, const RefBattleCP& cp, const HexCord& startHex,
        int nDir, HexCord::HexDirection hd, HexList& list)
{
  HexCord hex = startHex;
  while(nDir--)
  {
         list.newItem(hex);

         if(nDir)
         {
                HexCord uHex = hex;
                if(bd->moveHex(uHex, hd, hex))
                  list.newItem(hex);
                else
                  return False;
         }
  }

  return True;
}

bool findDeployHexes(RPBattleData bd, const RefBattleCP& cp, BattleOrderInfo& order, const HexCord& srcHex, const CPFormation formation, HexList& list)
{
        const int spCount = cp->spCount(); //BobUtility::countSP(cp);
        ASSERT(spCount > 0);

        /*
         * Calculate new rows and columns
         */

        int columns;
        int rows;
        BobUtility::getRowsAndColoumns(formation, spCount, columns, rows);

        HexCord hex = srcHex;
        if(order.d_deployHow != CPD_DeployCenter || formation <= CPF_Massed)
        {
//        hex = (order.d_deployHow == CPD_DeployLeft) ? cp->rightHex() : cp->leftHex();
        }
        else
        {
//        hex = cp->centerHex();

               // how many do we add to right side?  columns / 2 rounded + 1
          int nDirL = ((columns % 2) == 0) ?
                 maximum(0, (columns / 2) - 1) : columns / 2;

          if(nDirL > 0)
          {
                 while(nDirL--)
                 {
                        HexCord uHex = hex;
                        if(bd->moveHex(uHex, leftFlank(order.d_facing), hex))
                          ;  // Note: need to figure out what to do if we're at an
                                  // edge or impassable terrain
                        else
                          return False;
                 }
          }
            }

        HexCord nextHex = hex;
        switch(order.d_deployHow)
        {
          case CPD_DeployRight:
          case CPD_DeployCenter:
          {
                 if(!addToList(bd, cp, nextHex, columns, rightFlank(order.d_facing), list))
                        return False;

                 break;
          }

          case CPD_DeployLeft:
          {
                 if(!addToList(bd, cp, nextHex, columns, leftFlank(order.d_facing), list))
                        return False;

                 break;
          }
        }

        return True;
}



void initUnitType(BattleOB* ob, const RefBattleCP& cp)
{
    RefGenericCP gcp = cp->generic();

    gcp->clearFlags();
    const int maxSP = 14;
    const int maxType = 12;
    const int maxOtherType = 2;
    int nInf    = 0;
    int nCav    = 0;
    int nArt    = 0;
    int nOther  = 0;
    int spCount = 0;
    bool isHeavyCav = False;
    bool isGuard = True;
    bool isOldGuard = True;
    bool isMounted = True;
    // TODO: Add heavy artillery to unit type table
    bool isHeavyArt = False;

    // count types
    RefBattleSP sp = cp->sp();
    while(sp != NoBattleSP)
    {
      const UnitTypeItem& uti = ob->getUnitType(sp);
      switch(uti.getBasicType())
      {
         case BasicUnitType::Infantry:
            nInf++;
            break;
         case BasicUnitType::Cavalry:
            nCav++;
            break;
         case BasicUnitType::Artillery:
            nArt++;
            break;
         case BasicUnitType::Special:
            nOther++;
            break;
#ifdef DEBUG
         default:
         FORCEASSERT("Improper type in unitypes");
#endif
      }

      // check various flags
      if(!uti.isMounted())
         isMounted = False;

      if(!uti.isGuard())
         isGuard = False;

      if(!uti.isOldGuard())
         isOldGuard = False;

//    if(uti.getBasicType() != BasicUnitType::Cavalry || uti.isLightCavalry())
      if(uti.getBasicType() == BasicUnitType::Cavalry && !uti.isLightCavalry())
         isHeavyCav = True;

      // TODO: check for heavy artillery

      sp = sp->sister();
    }

    spCount = nInf + nArt + nCav + nOther;
    ASSERT(spCount <= maxSP);

    // if we have infantry, then this is an infantry XX
    // no matter what other SP we may have
    if(nInf > 0)
    {
      gcp->makeInfantryType(True);
      ASSERT(nInf <= maxType);
      ASSERT(nCav <= maxOtherType);
      ASSERT(nArt <= maxOtherType);
      ASSERT(!isMounted);
    }

    // if we have cavalry, then this is a cavalry XX
    // cavalry can only contain Cav and Horse Artillery
    else if(nCav > 0)
    {
      gcp->makeCavalryType(True);
      gcp->makeHeavyCavalryType(isHeavyCav);
      ASSERT(isMounted);
      ASSERT(nInf == 0);
      ASSERT(nCav <= maxType);
      ASSERT(nArt <= maxOtherType);
      ASSERT(nOther == 0);
    }

    // if we have artillery, make it an arty XX
    // artillery can only contain artillery SP or Special types
    else if(nArt > 0)
    {
      gcp->makeArtilleryType(True);
      gcp->makeHeavyArtilleryType(isHeavyArt);
      gcp->makeHorseArtilleryType(isMounted);
      ASSERT(nArt <= maxType);
      ASSERT(nInf == 0);
      ASSERT(nCav == 0);
    }

    // if we have only special type
    else
    {
      // probably shoudn't be here
      gcp->makeInfantryType(True);
    }

    // set any flags
    gcp->makeGuard(isGuard);
    gcp->makeOldGuard(isOldGuard);

    bool isCav = cp->generic()->isCavalry();
    bool isArt = cp->generic()->isArtillery();
    bool isInf = cp->generic()->isInfantry();

    RefBattleCP parent = cp->parent();
    while(parent != NoBattleCP && parent->getRank().isLower(Rank_President))
    {
      if(isInf)
         parent->generic()->makeInfantryType(True);

      if(isCav)
         parent->generic()->makeCavalryType(True);

      if(isArt)
         parent->generic()->makeArtilleryType(True);

      parent = parent->parent();
    }

}

//**************************************************************************************
//mtc122  initialize unit type flags called by battle editor
void initEditUnitTypeFlags(BattleOB* ob)
{

   if(!ob->getTop(0))
      return;
   // go through bottom up an set unit type flags
   //  for(Side side = 0; side < ob->sideCount(); ++side)
//mtc122  There is a problem in the original initUnitTypeFlags as initially empty XX CP are 
//mtc122  set to fled and therefore disappear from the unit displays
   {
     for(TBattleUnitIter<BUIV_All> iter(ob, BUIV_All());
        !iter.isFinished();
        iter.next())  ; // added ; for syntax
//mtc122      {
//mtc122         if(iter.cp()->getRank().sameRank(Rank_Division) && iter.cp()->sp() == NoBattleSP)
//mtc122           iter.cp()->setFled();
//mtc122      }

   }

   // first clear out old flags at XXX level and above
   {
      for(BattleUnitIter iter(ob); !iter.isFinished(); iter.next())
      {
         if(iter.cp()->getRank().isHigher(Rank_Division))
            iter.cp()->generic()->clearFlags();
      }
   }


   // now set flags
   {
      for(BattleUnitIter iter(ob); !iter.isFinished(); iter.next())
      {
         if(iter.cp()->getRank().sameRank(Rank_Division))
            initUnitType(ob, iter.cp());
      }
   }
}
//mtc122
void initEditUnitTypeFlags(RPBattleData bd)
{
    initEditUnitTypeFlags(bd->ob());
}
// end of inserted initEditUnitTypeFlags  mtc122/
//*******************************************************************************
// initialize unit type flags
void initUnitTypeFlags(BattleOB* ob, const RefBattleCP& cp)
{
   // go through bottom up an set unit type flags
   //  for(Side side = 0; side < ob->sideCount(); ++side)
   // Set as fled any XX that does not have SP 
   {
     for(TBattleUnitIter<BUIV_All> iter(cp, BUIV_All());
        !iter.isFinished();
        iter.next())
      {
         if(iter.cp()->getRank().sameRank(Rank_Division) && iter.cp()->sp() == NoBattleSP)
         iter.cp()->setFled() ;
      }
   }

   // first clear out old flags at XXX level and above
   {
      for(BattleUnitIter iter(cp); !iter.isFinished(); iter.next())
      {
         if(iter.cp()->getRank().isHigher(Rank_Division))
            iter.cp()->generic()->clearFlags();
      }
   }


   // now set flags
   {
      for(BattleUnitIter iter(cp); !iter.isFinished(); iter.next())
      {
         if(iter.cp()->getRank().sameRank(Rank_Division))
            initUnitType(ob, iter.cp());
      }
   }
}

// void initUnitTypeFlags(RPBattleData bd)
void initUnitTypeFlags(BattleOB* ob)
{

   if(!ob->getTop(0))
      return;
   // go through bottom up an set unit type flags
   //  for(Side side = 0; side < ob->sideCount(); ++side)
   // Set as fled any XX that does not have SP
   {
     for(TBattleUnitIter<BUIV_All> iter(ob, BUIV_All());
        !iter.isFinished();
        iter.next())
      {
         if(iter.cp()->getRank().sameRank(Rank_Division) && iter.cp()->sp() == NoBattleSP)
           iter.cp()->setFled();
      }
   }

   // first clear out old flags at XXX level and above
   {
      for(BattleUnitIter iter(ob); !iter.isFinished(); iter.next())
      {
         if(iter.cp()->getRank().isHigher(Rank_Division))
            iter.cp()->generic()->clearFlags();
      }
   }


   // now set flags
   {
      for(BattleUnitIter iter(ob); !iter.isFinished(); iter.next())
      {
         if(iter.cp()->getRank().sameRank(Rank_Division))
            initUnitType(ob, iter.cp());
      }
   }
}

void initUnitTypeFlags(RPBattleData bd)
{
    initUnitTypeFlags(bd->ob());
}


UINT unitSPStrength(RPBattleData bd, const RefBattleCP& cp, bool initializing)
{
   if(initializing)
   {
      RefBattleSP sp = cp->sp();
      UINT str = 0;
      while(sp != NoBattleSP)
      {
         str += sp->strength();
         sp = sp->sister();
      }
      return str;
   }

   else
   {
      UINT str = 0;
      for(BattleSPIter spIter(bd->ob(), cp, BSPI_ModeDefs::All);
            !spIter.isFinished();
            spIter.next())
      {
      str += spIter.sp()->strength();
      }

    return str;
   }
}

void setCommandMorale(const RefBattleCP& cp)
{
   RefBattleCP parent = cp->parent();
   while(parent != NoBattleCP)
   {
      Attribute morale = averageMorale(parent);
      parent->morale(morale);
      parent = parent->parent();
   }
}

HexPosition::Facing closeFace(HexPosition::Facing f)
{
   return (f < 42 && f >= 0) ? HexPosition::NorthEastPoint :
             (f <= 86)          ? HexPosition::North :
             (f <= 128)         ? HexPosition::NorthWestPoint :
             (f < 170)          ? HexPosition::SouthWestPoint :
             (f <= 213)         ? HexPosition::South : HexPosition::SouthEastPoint;

}

HexPosition::Facing avgUnitFacing(const CRefBattleCP& cp)
{
   ULONG tFace = 0;
   int nUnits = 0;
   for(ConstBattleUnitIter uIter(cp); !uIter.isFinished(); uIter.next())
   {
      if(uIter.cp()->getRank().sameRank(Rank_Division))
      {
         tFace += uIter.cp()->facing();
         nUnits++;
      }
   }

   if(nUnits > 0)
   {
      return closeFace(static_cast<HexPosition::Facing>(tFace / nUnits));
   }
   else
      return cp->facing();
}

bool canAttach(RCPBattleData bd, const CRefBattleCP& thisCP, const CRefBattleCP& thatCP)
{
   // a quick check for equal or higher rank
   if(thisCP->getSide() != thatCP->getSide() ||
      thatCP->getRank().isHigher(Rank_Division) ||
      thisCP->getRank().sameRank(Rank_Division) ||
      thisCP->getRank().sameRank(thatCP->getRank()) ||
      thisCP->getRank().isLower(thatCP->getRank()))
   {
      return False;
   }

   // cinc can attach to anybody

   BattleOB* ob = const_cast<BattleOB*>(bd->ob());

   if(thisCP == getCinc(ob, thisCP->getSide()))
      return True;

   // we can only attach to a subordinate (or a subordinate of a subordinate)
   for(ConstBattleUnitIter iter(thisCP); !iter.isFinished(); iter.next())
   {
      if(iter.cp()->getRank().sameRank(Rank_Division) && thatCP == iter.cp())
         return True;
   }

   return False;
}





void
approxVictoryLevels(RPBattleData bd, unsigned int * vLevel1, unsigned int * vLevel2) {

   int losses1 = 0;
   int morale1 = 0;

   int losses2 = 0;
   int morale2 = 0;

   /*
   * Calculate for side 1
   */
   TBattleUnitIter<BUIV_All> iter_0(bd->ob(), 0, BUIV_All());
   while(!iter_0.isFinished()) {

      if(iter_0.cp()->active()) {
         losses1 += iter_0.cp()->startStrength() - iter_0.cp()->lastStrength();
         morale1 += iter_0.cp()->morale();
      }

      iter_0.next();
   }

   /*
   * Calculate for side 2
   */
   TBattleUnitIter<BUIV_All> iter_1(bd->ob(), 1, BUIV_All());
   while(!iter_1.isFinished()) {

      if(iter_1.cp()->active()) {
         losses2 += iter_1.cp()->startStrength() - iter_1.cp()->lastStrength();
         morale2 += iter_1.cp()->morale();
      }

      iter_1.next();
   }

   *vLevel1 = losses2;// + morale1;
   *vLevel2 = losses1;// + morale2;
}





}      // namespace BobUtility



/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.5  2002/12/15 14:51:35  greenius
 * Updated and cleaned Tim Carne's patches
 *
 * Revision 1.4  2002/11/24 13:17:48  greenius
 * Added Tim Carne's editor changes including InitEditUnitTypeFlags.
 *
 * mtc122 added function initEditUnitTypeFlags to perform initUnitTypeFlags
 * functions but when called from editor is avoids the disappearance of
 * new division CP with zero SP
 *
 * Revision 1.3  2001/07/02 13:18:24  greenius
 * Battle, Campaign, Combined game and Campaign Editor now compile and run... mostly
 *
 * Revision 1.2  2001/06/18 08:25:43  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
