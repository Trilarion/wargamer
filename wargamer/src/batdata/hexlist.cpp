/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
#include "stdinc.hpp"
#include "hexlist.hpp"
#include "fsalloc.hpp"
#include "filebase.hpp"

/*-------------------------------------------------------------------
 * HexList class, used for storing routes, deployment hexes, etc.
 */

const int chunkSize = 50;

#ifdef DEBUG
static FixedSize_Allocator itemAlloc(sizeof(HexItem), chunkSize, "HexItem");
#else
static FixedSize_Allocator itemAlloc(sizeof(HexItem), chunkSize);
#endif

void* HexItem::operator new(size_t size)
{
  ASSERT(size == sizeof(HexItem));
  return itemAlloc.alloc(size);
}

#ifdef _MSC_VER
void HexItem::operator delete(void* deadObject)
{
  itemAlloc.free(deadObject, sizeof(HexItem));
}
#else
void HexItem::operator delete(void* deadObject, size_t size)
{
  ASSERT(size == sizeof(HexItem));
  itemAlloc.free(deadObject, size);
}
#endif

/*
 * File Interface
 */

const UWORD HexItem::s_fileVersion = 0x0000;
Boolean HexItem::readData(FileReader& f, OrderBattle* ob)
{
  UWORD version;
  f >> version;
  ASSERT(version <= s_fileVersion);

  if(!d_hex.readData(f))
    return False;

  f >> d_facing;

  return f.isOK();
}

Boolean HexItem::writeData(FileWriter& f, OrderBattle* ob) const
{
  f << s_fileVersion;

  if(!d_hex.writeData(f))
    return False;

  f << d_facing;

  return f.isOK();
}

//-------------------------------------------------------------------------
// append list to current list
void HexList::addList(const HexList& hl)
{
  // copy
  SListIterR<HexItem> iter(&hl);
  while(++iter)
  {
    HexItem* hi = new HexItem(iter.current()->d_hex, iter.current()->d_facing);
    ASSERT(hi);

    append(hi);
  }
}

/*
 * File Interface
 */

const UWORD HexList::s_fileVersion = 0x0000;
Boolean HexList::readData(FileReader& f, OrderBattle* ob)
{
  UWORD version;
  f >> version;
  ASSERT(version <= s_fileVersion);

  int nItems;
  f >> nItems;
  ASSERT(nItems >= 0);
  while(nItems--)
  {
    HexItem* hi = new HexItem;
    ASSERT(hi);

    append(hi);

    if(!hi->readData(f, ob))
      return False;
  }

  return f.isOK();
}

Boolean HexList::writeData(FileWriter& f, OrderBattle* ob) const
{
  f << s_fileVersion;

  int nItems = entries();
  f << nItems;

  SListIterR<HexItem> iter(this);
  while(++iter)
  {
    if(!iter.current()->writeData(f, ob))
      return False;
  }

  return f.isOK();
}



/*
 *----------------------------------------------------------------------
 * $Log$
 *----------------------------------------------------------------------
 */
