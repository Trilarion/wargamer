/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#if !defined(BattleDataFwd_H)
#define BattleDataFwd_H

/*
 *------------------------------------------------------------------------------
 * $Header$
 *------------------------------------------------------------------------------
 * $Author$
 *----------------------------------------------------------------------
 * Allow BattleData pointers and references to be used without
 * including all of batdata.hpp
 *----------------------------------------------------------------------
 */


#include "refptr.hpp"


class BattleData;
#ifdef DEBUG            // Use Reference counting for debugging
typedef        RefPtr<BattleData>  PBattleData;
typedef       CRefPtr<BattleData>  CPBattleData;
typedef const  RefPtr<BattleData>& RPBattleData;
typedef const CRefPtr<BattleData>& RCPBattleData;
#else
typedef       BattleData* PBattleData;
typedef const BattleData* CPBattleData;
typedef       BattleData* RPBattleData;
typedef const BattleData* RCPBattleData;
#endif


/*
 *------------------------------------------------------------------------------
 * $Log$
 *------------------------------------------------------------------------------
 */

#endif   // defined(BattleDataFwd_H)