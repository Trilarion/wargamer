/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *      Battle Game Data
 *
 *----------------------------------------------------------------------
 */


#include "stdinc.hpp"
#include "batdata.hpp"
#include "hexdata.hpp"
#include "b_terr.hpp"
#include "batcord.hpp"
#include "misc.hpp"
#include "random.hpp"
#include "building.hpp"
#include "fx.hpp"
#include "batarmy.hpp"
#include "hexmap.hpp"
#include "batsound.hpp"
#include "bshockl.hpp"
#include "bobiter.hpp"
#include "initunit.hpp"
#include "bobutil.hpp"
#include "pdeploy.hpp"
#include "key_terr.hpp"


using namespace BattleMeasure;
using namespace BattleBuildings;

// namespace WG_BattleData
// {

struct SideBoundry {
   HexCord leftHex;
   HexCord rightHex;
};

class BattleDataImp
{

      // disable copying

      BattleDataImp(const BattleDataImp&);
      BattleDataImp& operator =(const BattleDataImp&);
   public:
      // BattleDataImp(BattleDataUser* user);
      BattleDataImp();
      ~BattleDataImp();

      const BuildingTable* buildingTable() const
      { return &d_buildingTable;
      }
      BuildingTable* buildingTable()
      { return &d_buildingTable;
      }

      const BuildingList* buildingList() const
      { return &d_buildings;
      }
      BuildingList* buildingList()
      { return &d_buildings;
      }

      const EffectsTable* effectsTable() const
      { return &d_effectsTable;
      }
      EffectsTable* effectsTable()
      { return &d_effectsTable;
      }

      EffectsList* effectsList() const
      { return &d_effectsList;
      }

      BattleSoundSystem * soundSystem() const
      { return d_battleSoundSystem;
      }

      KeyTerrainList * keyTerrainList(void)
      { return &d_keyTerrainList;
      }
      const KeyTerrainList * keyTerrainList(void) const
      { return &d_keyTerrainList;
      }

      const HexCord& getMapHexSize() const
      { return d_map.getSize();
      }

      HexCord getHex(const BattleLocation& l, BattleData::GetHexMode mode) const;
      // Convert Physical location into the nearest hex.

      BattleLocation getLocation(const HexCord& h) const;
      // Get coordinate of hex center

      BattleArea getViewableArea() const;
      // Return the viewable area

      void getPlayingArea(HexCord * bottomleft, HexCord * size) const;
      void setPlayingArea(HexCord bottomleft, HexCord size);
      bool expandPlayingArea(HexCord newsize);
      void maximizePlayingArea(void);

      // uncheckec update Jan 9, 1999
      void addToShockCombatList(const RefBattleCP& cp);
      ShockBattleList& shockBattles()
      { return d_shockCombatList;
      }
      const ShockBattleList& shockBattles() const
      { return d_shockCombatList;
      }
      // end update
      const BattleTerrainHex& getTerrain(const HexCord& h) const
      { return d_map.get(h);
      }
      BattleTerrainHex& getTerrain(const HexCord& h)
      { return d_map.get(h);
      }

      const TerrainTable& terrainTable() const
      { return d_terrain;
      }
      TerrainTable& terrainTable()
      { return d_terrain;
      }

      const BattleMap* map() const
      { return &d_map;
      }
      BattleMap* map()
      { return &d_map;
      }

      BattleOB* ob()
      { return d_ob;
      }
      const BattleOB* ob() const
      { return d_ob;
      }
      void ob(BattleOB* ob)
      {
         d_ob = ob;
      }

      // const TerrainInfo& terrainInfo(TerrainTable::Index t) const { return d_terrain[t]; }

      // bool isWater(const HexCord& h) const;

      void setSideLeftRight(Side s, HexCord leftHex, HexCord rightHex)
      {
         ASSERT(s < 2);
         d_sideBoundries[s].leftHex = leftHex;
         d_sideBoundries[s].rightHex = rightHex;
      }

      void getSideLeftRight(Side s, HexCord& leftHex, HexCord& rightHex)
      {
         ASSERT(s < 2);
         leftHex = d_sideBoundries[s].leftHex;
         rightHex = d_sideBoundries[s].rightHex;
      }


      bool moveHex(const HexCord& srcHex, HexCord::HexDirection dir, HexCord& destHex) const
      {
         return d_map.moveHex(srcHex, dir, destHex);
      }

      const HexCord& hexSize() const
      {
         return d_map.getSize();
      }

      BattleHexMap* hexMap()
      { return &d_hexMap;
      }
      const BattleHexMap* hexMap() const
      { return &d_hexMap;
      }

      void setChanged();
      // Note that data has changed, signals user to redraw itself

      void cropMap(HexCord center, HexCord size);

      void processBuildings(BattleMeasure::BattleTime::Tick ticks);

      /*
      * Time/Date Functions: Note that these must be thread-safe
      */

      void setDateAndTime(const BattleTime& t);
      // Sets the current Date and Time
      BattleTime getDateAndTime() const;
      // Makes a copy of the current date/time
      void addTime(BattleTime::Tick ticks);
      // Adds on BattleTicks to the time
      BattleMeasure::BattleTime::Tick getTick() const;
      void timeRatio(int n)
      {
         d_time.ratio(n);
      }
      int timeRatio() const
      { return d_time.ratio();
      }

      void type(BattleData::Type t)
      {
         d_type = t;
      }

      BattleData::Type type() const
      {
         return d_type;
      }

      void mode(BattleData::Mode mode)
      {
         d_mode = mode;
      }
      BattleData::Mode mode() const
      { return d_mode;
      }
      void endGameIn(BattleMeasure::BattleTime::Tick time)
      {
         d_endGameIn = time;
      }
      BattleMeasure::BattleTime::Tick endGameIn(void)
      { return d_endGameIn;
      }
      void addStartTimeToEndTime()
      {
         if( d_endGameIn != 0 )
            d_endGameIn += getTick();
      }
      bool shouldEnd() const
      { return (d_endGameIn != 0 && d_type == BattleData::Historical && getTick() >= d_endGameIn);
      }

      /**
       * Delete the complete Order of Battle
       */

      void deleteOB();

      /*
      * File {
      */

      Boolean readData(FileReader& f, OrderBattle* gob);
      Boolean writeData(FileWriter& f, bool standAlone) const;
   private:
   #ifdef DEBUG
      void checkHex(const HexCord& h) const
      {
         d_map.checkHex(h);
      }
      // Assert hex cords are valid
   #endif

   private:
      BattleMap              d_map;
      TerrainTable           d_terrain;

      BuildingTable          d_buildingTable;
      BuildingList           d_buildings;

      // this is now obsolete
      EffectsTable           d_effectsTable;

      mutable EffectsList    d_effectsList;

      BattleSoundSystem *    d_battleSoundSystem;

      KeyTerrainList d_keyTerrainList;

      BattleOB*              d_ob;

      BattleHexMap           d_hexMap;                       // Obejcts referenced by hex

      HexCord d_playingAreaBottomLeft;
      HexCord d_playingAreaSize;

      ShockBattleList        d_shockCombatList;
      BattleTime             d_time;                         // Current logical time/date
      RWLock                 d_timeLock;                     // Make time thread-safe

      SideBoundry            d_sideBoundries[2];

      BattleData::Type       d_type;
      BattleData::Mode       d_mode;
      BattleMeasure::BattleTime::Tick d_endGameIn;

      static const char      s_fileID[];
      static const UWORD     s_fileVersion;
};

// BattleDataImp::BattleDataImp(BattleDataUser* user) :
BattleDataImp::BattleDataImp() :
                  // d_user(user),
                  d_map(),
                  d_terrain(),
                  d_ob(0),
                  d_type(BattleData::Random),
                  d_mode(BattleData::Deploying),
                  d_endGameIn(0)
{
   /*
   * Initialise Terrain Tables, using global scenario for information
   * on where to find files and things.
   */

   d_battleSoundSystem = new BattleSoundSystem(&GlobalSoundSystemObj);


   int num = 2;
   GlobalSoundSystemObj.SetPlayList(num, 3+1, 4+1);
   bool already_playing = GlobalSoundSystemObj.IsCDTrackPlaying(3);
   if(!already_playing) GlobalSoundSystemObj.StopCDPlaying();
   GlobalSoundSystemObj.StartPlayList(!already_playing);

   d_terrain.init();

   d_buildingTable.init();

   d_effectsTable.init();

}

BattleDataImp::~BattleDataImp()
{
   ASSERT(d_ob == 0);              // should have been deleted by a higher level
   delete d_battleSoundSystem;
   d_battleSoundSystem = NULL;
}

/*
 * Note that the data has changed
 */

void BattleDataImp::setChanged()
{
   //------ Do nothing as yet
   // if(d_user)
   //              d_user->dataChanged();
}


/*
 * Convert Physical location into the nearest hex.
 * This will be more complicated because of the way they overlap
 * Also need to resolve what is meant by closest hex, when it is
 * in the vertical overlap area.  May need a flag to indicate how to
 * resolve it, e.g round down, round up, accurate.
 */

HexCord BattleDataImp::getHex(const BattleLocation& l, BattleData::GetHexMode mode) const
{
   BattleMeasure::Distance ly = l.y();

   switch(mode)
   {
   case BattleData::Top:
      ly -= QuarterYHex;
      break;

   case BattleData::Mid:
      ly -= QuarterYHex / 2;
      break;

   case BattleData::Low:
      break;

#ifdef DEBUG
   default:
      FORCEASSERT("Unknown BattleData:GetHexMode");
      break;
#endif
   }

   int y = ly / BattleMeasure::HexYOffset;

   BattleMeasure::Distance lx = l.x();
   if(y & 1)
      lx -= BattleMeasure::HalfXHex;

   int x = lx / BattleMeasure::OneXHex;

   // Clip to viewable area

   const HexCord& hexSize = d_map.getSize();

   x = maximum(0, x);
   y = maximum(0, y);
   x = minimum(hexSize.x() - 1, x);
   y = minimum(hexSize.y() - 1, y);

   return HexCord(static_cast<HexCord::Cord>(x), static_cast<HexCord::Cord>(y));
}


/*
 * Get coordinate of hex center
 * Needs to a bit more complex because of overlap
 *
 * 0,0 is at bottom left of hex(0,0)
 */

BattleLocation BattleDataImp::getLocation(const HexCord& h) const
{
#ifdef DEBUG
   checkHex(h);
#endif

   BattleMeasure::Distance x = h.x() * BattleMeasure::OneXHex + BattleMeasure::HalfXHex;
   if(h.y() & 1)
      x += BattleMeasure::HalfXHex;

   BattleMeasure::Distance y = h.y() * BattleMeasure::HexYOffset + BattleMeasure::HalfYHex;

   return BattleLocation(x, y);
}


BattleArea BattleDataImp::getViewableArea() const
{

   const HexCord& hexSize = d_playingAreaSize;

   if((hexSize.x() == 0) && (hexSize.y() == 0))
      return BattleArea(0,0,0,0);

   return BattleArea(// d_topLeft
      (d_playingAreaBottomLeft.x() * BattleMeasure::OneXHex) + BattleMeasure::OneXHex, //- BattleMeasure::HalfXHex,
      (d_playingAreaBottomLeft.y() * BattleMeasure::HexYOffset) + BattleMeasure::QuarterYHex,
      // d_size
      (hexSize.x() * BattleMeasure::OneXHex) - BattleMeasure::HalfXHex,
      (hexSize.y() * BattleMeasure::HexYOffset) - BattleMeasure::QuarterYHex);

   /*
   const HexCord& hexSize = d_map.getSize();

   if( (hexSize.x() == 0) && (hexSize.y() == 0))
   return BattleArea(0,0,0,0);

   return BattleArea(
   BattleMeasure::HalfXHex,
   BattleMeasure::QuarterYHex,
   hexSize.x() * BattleMeasure::OneXHex - BattleMeasure::HalfXHex,
   hexSize.y() * BattleMeasure::HexYOffset - BattleMeasure::QuarterYHex);
   */
}



void
BattleDataImp::getPlayingArea(HexCord * bottomleft, HexCord * size) const {

   *bottomleft = d_playingAreaBottomLeft;
   *size = d_playingAreaSize;
}

void
BattleDataImp::setPlayingArea(HexCord bottomleft, HexCord size) {

   // check new position doesn't overlap with buffer-zone
   ASSERT(bottomleft.x() >= BATTLEMAP_BUFFERHEXES);
   ASSERT(bottomleft.y() >= BATTLEMAP_BUFFERHEXES);
   ASSERT((bottomleft.x() + size.x()) <= (d_map.getSize().x() - BATTLEMAP_BUFFERHEXES));
   ASSERT((bottomleft.y() + size.y()) <= (d_map.getSize().y() - BATTLEMAP_BUFFERHEXES));

   // do some read/write locking ?

   // notify various things that we're changing the playing area ?

   d_playingAreaBottomLeft = bottomleft;
   d_playingAreaSize = size;
}

bool
BattleDataImp::expandPlayingArea(HexCord newsize) {

   // assert we're increasing size
   ASSERT(newsize.x() > d_playingAreaSize.x() || newsize.y() > d_playingAreaSize.y());//d_map.getSize().x());
//   ASSERT(newsize.y() > d_map.getSize().y());
   // assert we're not being ridiculous
   ASSERT(newsize.x() < (BATTLEMAP_MAXWIDTH - (BATTLEMAP_BUFFERHEXES*2)));
   ASSERT(newsize.y() < (BATTLEMAP_MAXHEIGHT - (BATTLEMAP_BUFFERHEXES*2)));

   // delta size
   HexCord size_diff;
   size_diff.x(newsize.x() - d_playingAreaSize.x());//d_map.getSize().x());
   size_diff.y(newsize.y() - d_playingAreaSize.y());//d_map.getSize().y());

   // adjust bottom left accordingly
   HexCord new_bottomleft;
   new_bottomleft.x(d_playingAreaBottomLeft.x() - (size_diff.x() / 2));
   new_bottomleft.y(d_playingAreaBottomLeft.y() - (size_diff.y() / 2));

   // if we've gone off the bottom / left of map, then adjust
   if(new_bottomleft.x() < BATTLEMAP_BUFFERHEXES) new_bottomleft.x(BATTLEMAP_BUFFERHEXES);
   if(new_bottomleft.y() < BATTLEMAP_BUFFERHEXES) new_bottomleft.y(BATTLEMAP_BUFFERHEXES);

   // if we've gone off top / right of map, then adjust
   if((new_bottomleft.x() + newsize.x()) > (BATTLEMAP_MAXWIDTH - BATTLEMAP_BUFFERHEXES))
   {
      int overlap = (new_bottomleft.x() + newsize.x()) - (BATTLEMAP_MAXWIDTH - BATTLEMAP_BUFFERHEXES);
      new_bottomleft.x(new_bottomleft.x() - overlap);
   }
   if((new_bottomleft.y() + newsize.y()) > (BATTLEMAP_MAXHEIGHT - BATTLEMAP_BUFFERHEXES))
   {
      int overlap = (new_bottomleft.y() + newsize.y()) - (BATTLEMAP_MAXHEIGHT - BATTLEMAP_BUFFERHEXES);
      new_bottomleft.y(new_bottomleft.y() - overlap);
   }

   // now set the area
   setPlayingArea(new_bottomleft, newsize);

   return true;

}


void
BattleDataImp::maximizePlayingArea(void) {

   HexCord bottomleft = HexCord(BATTLEMAP_BUFFERHEXES,BATTLEMAP_BUFFERHEXES);

   HexCord size = HexCord(d_map.getSize().x() - (BATTLEMAP_BUFFERHEXES*2),
      d_map.getSize().y() - (BATTLEMAP_BUFFERHEXES*2));

   setPlayingArea(bottomleft, size);
}



inline Side getOtherSide(Side s)
{
   return (s == 0) ? 1 : 0;
}

// uncheckec update Jan 9, 1999
void BattleDataImp::addToShockCombatList(const RefBattleCP& cp)
{
   // make sure we are not in list
   bool inList = False;
#ifdef DEBUG
   {
      SListIterR<ShockBattleItem> iterR(&d_shockCombatList);
      while(++iterR)
      {
         const SideShockBattleList& si = iterR.current()->d_units[cp->getSide()];
         SideShockBattleListIterR sIterR(&si);
         while(++sIterR)
         {
            if(sIterR.current()->d_cp == cp)
            {
               inList = True;
               break;
            }
         }

         if(inList)
            break;
      }

      ASSERT(!inList);
   }
#endif

   cp->inCloseCombat(True);
   bool result = cp->inCloseCombat();


   // if any of our targets are allready in list, join that battle
   bool added = False;
   SListIterR<BattleItem> bIter(&cp->targetList());
   while(++bIter)
   {
      // Unchecked added by Paul June20
      if(bIter.current()->d_range > (1 * BattleMeasure::XYardsPerHex) ||
         bIter.current()->d_position != BattleItem::Front ||
         bIter.current()->d_cp->sp()->hexPosition().distance() != cp->sp()->hexPosition().distance())
      {
         continue;
      }
      bool inList = False;
      ShockBattleListIter iterR(&d_shockCombatList);
      while(++iterR)
      {

         SideShockBattleList& sl = iterR.current()->d_units[bIter.current()->d_cp->getSide()];
         SideShockBattleListIter sIterR(&sl);
         while(++sIterR)
         {
            if(sIterR.current()->d_cp == bIter.current()->d_cp)
            {
               // join our side
               SideShockBattleList& ourSL = iterR.current()->d_units[cp->getSide()];
               ourSL.addItem(cp);
               inList = True;
               break;
            }
         }

         if(inList)
         {
            break;
         }
      }

      if(inList)
      {
         added = True;
         break;
      }
   }

   // create new battle
   if(!added)
   {
      ShockBattleItem* si = new ShockBattleItem;
      ASSERT(si);

      SideShockBattleList& sl = si->d_units[cp->getSide()];
      sl.addItem(cp);

      SideShockBattleList& osl = si->d_units[getOtherSide(cp->getSide())];

      SListIterR<BattleItem> bIter(&cp->targetList());
      while(++bIter)
      {
         // Unchecked added
         if(bIter.current()->d_range > (1 * BattleMeasure::XYardsPerHex) ||
            bIter.current()->d_position != BattleItem::Front ||
            bIter.current()->d_cp->sp()->hexPosition().distance() != cp->sp()->hexPosition().distance())
         {
            continue;
         }
         bIter.current()->d_cp->inCloseCombat(True);
         osl.addItem(bIter.current()->d_cp);
      }

      d_shockCombatList.append(si);
   }
}
// end update











void
BattleDataImp::cropMap(HexCord center, HexCord size) {

   // set selection area in display
   HexCord topleft = HexCord(center.x() - (size.x() / 2), center.y() - (size.y() / 2));
   HexCord bottomright = HexCord(center.x() + (size.x() / 2), center.y() + (size.y() / 2));

   // ensure we're even, otherwise strange things happen
   if(topleft.x() & 1) topleft.x(topleft.x()-1);
   if(topleft.y() & 1) topleft.y(topleft.y()-1);
   if(bottomright.x() & 1) bottomright.x(bottomright.x()-1);
   if(bottomright.y() & 1) bottomright.y(bottomright.y()-1);

   // crop battlemap to area
   map()->crop(topleft, bottomright);
   buildingList()->crop(topleft, bottomright);
   buildingList()->adjust(topleft);

   HexCord bottom_left = HexCord(topleft.x(), topleft.y());
   HexCord top_right = HexCord(bottomright.x(), bottomright.y());

   keyTerrainList()->crop(bottom_left, top_right);
   keyTerrainList()->checkEdges(map());
#ifdef DEBUG
   keyTerrainList()->labelKeyTerrain(map());
#endif
   keyTerrainList()->check();
}


void
BattleDataImp::processBuildings(BattleMeasure::BattleTime::Tick ticks) {

   buildingList()->process(ticks);
}
















/*
 * Time/Date Functions: Note that these must be thread-safe
 */

/*
 * Sets the current Date and Time
 */

void BattleDataImp::setDateAndTime(const BattleTime& t)
{
   d_timeLock.startWrite();
   d_time = t;
   d_timeLock.endWrite();
}

/*
 * Makes a copy of the current date/time
 */

BattleTime BattleDataImp::getDateAndTime() const
{
   d_timeLock.startRead();
   BattleTime theTime = d_time;
   d_timeLock.endRead();
   return theTime;
}

/*
 * Adds on BattleTicks to the time
 */

void BattleDataImp::addTime(BattleTime::Tick ticks)
{
   d_timeLock.startWrite();
   d_time += ticks;
   d_timeLock.endWrite();
}

BattleMeasure::BattleTime::Tick BattleDataImp::getTick() const
{
   return d_time.getTick();
}

const char BattleDataImp::s_fileID[] = "BattleData";
const UWORD BattleDataImp::s_fileVersion = 0x0007;
Boolean BattleDataImp::readData(FileReader& f, OrderBattle* gob)
{
   UWORD version;
   {
      FileChunkReader fr(f, s_fileID);
      f >> version;
      ASSERT(version <= s_fileVersion);
      f >> d_time;

      if(version >= 0x0001)
      {
         for(int s = 0; s < 2; s++)
         {
            d_sideBoundries[s].leftHex.readData(f);
            d_sideBoundries[s].rightHex.readData(f);
         }
      }
      if(version >= 0x0004)
      {
         f >> d_playingAreaBottomLeft;
         f >> d_playingAreaSize;
      }
      else
      {
         d_playingAreaBottomLeft = HexCord(0,0);
         d_playingAreaSize = d_map.getSize();
      }

      if(version >= 0x0004)
      {
         UBYTE b;
         f >> b;
         d_type = static_cast<BattleData::Type>(b);
      }

      if(version >= 0x0006)
      {
         UBYTE b;
         f >> b;
         d_mode = static_cast<BattleData::Mode>(b);
         f >> d_endGameIn;
      }
   }

   d_map.readData(f);
   d_buildings.readData(f, buildingTable());


   if(version >= 0x0005)
   {
      d_effectsList.readData(f);
   }


   ASSERT(d_ob == 0);

   if(gob == 0)
   {
      gob = new OrderBattle;
      gob->readData(f);
   }

   d_ob = new BattleOB(gob);

   d_ob->readData(f);
   if(version >= 0x0007)
   {
      d_shockCombatList.readData(f, ob()->ob());
   }
#if 1
   if(version < 0x0004)
   {
      if(version >= 0x0002)
      {
         f >> d_playingAreaBottomLeft;
         f >> d_playingAreaSize;
      }
      else
      {
         d_playingAreaBottomLeft = HexCord(0,0);
         d_playingAreaSize = d_map.getSize();
      }

      if(version >= 0x0003)
      {
         UBYTE b;
         f >> b;
         d_type = static_cast<BattleData::Type>(b);
      }
   }
#endif

   return f.isOK();
}

Boolean BattleDataImp::writeData(FileWriter& f, bool standAlone) const
{
   {
      FileChunkWriter fr(f, s_fileID);
      f << s_fileVersion;
      f << d_time;
      for(int s = 0; s < 2; s++)
      {
         d_sideBoundries[s].leftHex.writeData(f);
         d_sideBoundries[s].rightHex.writeData(f);
      }

      f << d_playingAreaBottomLeft;
      f << d_playingAreaSize;
      UBYTE b = static_cast<UBYTE>(d_type);
      f << b;
      b = static_cast<UBYTE>(d_mode);
      f << b;
      f << d_endGameIn;
   }

   d_map.writeData(f);
   d_buildings.writeData(f);
   d_effectsList.writeData(f);

   ASSERT(d_ob != 0);

   // depending on mode, save the Generic Order of Battle

   if(standAlone)
   {
      OrderBattle* gob = d_ob->ob();
      gob->writeData(f);
   }

   d_ob->writeData(f);
   d_shockCombatList.writeData(f, const_cast<OrderBattle*>(ob()->ob()));
#if 0
   f << d_playingAreaBottomLeft;
   f << d_playingAreaSize;
   UBYTE b = static_cast<UBYTE>(d_type);
   f << b;
#endif
   return f.isOK();
}

void BattleDataImp::deleteOB()
{
   d_hexMap.clear();
   BattleOB* ob = d_ob;
   ob->deleteAllUnits();
   ob->deleteGenericOB();
   d_ob = 0;
   delete(ob);
}


/*------------------------------------------------------
 * Interface
 */

// BattleData::BattleData(BattleDataUser* user) :
//              d_imp(new BattleDataImp(user))
BattleData::BattleData() :
        d_imp(new BattleDataImp())
{
}

BattleData::~BattleData()
{
   delete d_imp;
}

// Convert Physical location into the nearest hex.

HexCord BattleData::getHex(const BattleLocation& l, GetHexMode mode) const
{
   return d_imp->getHex(l, mode);
}

// Get coordinate of hex center

BattleLocation BattleData::getLocation(const HexCord& h) const
{
   return d_imp->getLocation(h);
}

BattleArea BattleData::getViewableArea() const
{
   return d_imp->getViewableArea();
}


void BattleData::getPlayingArea(HexCord * bottomleft, HexCord * size) const {

   d_imp->getPlayingArea(bottomleft, size);
}

void BattleData::setPlayingArea(HexCord bottomleft, HexCord size) {

   d_imp->setPlayingArea(bottomleft, size);
}

bool BattleData::expandPlayingArea(HexCord newsize) {

   return d_imp->expandPlayingArea(newsize);
}

void BattleData::maximizePlayingArea(void) {

   d_imp->maximizePlayingArea();
}

const BattleTerrainHex& BattleData::getTerrain(const HexCord& h) const
{
   return d_imp->getTerrain(h);
}


bool BattleData::moveHex(const HexCord& srcHex, HexCord::HexDirection dir, HexCord& destHex) const
{
   return d_imp->moveHex(srcHex, dir, destHex);
}

const TerrainTable& BattleData::terrainTable() const
{
   return d_imp->terrainTable();
}

TerrainTable& BattleData::terrainTable()
{
   return d_imp->terrainTable();
}

const BuildingTable* BattleData::buildingTable() const
{
   return d_imp->buildingTable();
}

BuildingTable* BattleData::buildingTable()
{
   return d_imp->buildingTable();
}

const BuildingList* BattleData::buildingList() const
{
   return d_imp->buildingList();
}

BuildingList* BattleData::buildingList()
{
   return d_imp->buildingList();
}

const EffectsTable* BattleData::effectsTable() const
{
   return d_imp->effectsTable();
}

EffectsTable* BattleData::effectsTable()
{
   return d_imp->effectsTable();
}

EffectsList* BattleData::effectsList() const
{
   return d_imp->effectsList();
}

BattleSoundSystem * BattleData::soundSystem() const
{
   return d_imp->soundSystem();
}

KeyTerrainList * BattleData::keyTerrainList(void) {

   return d_imp->keyTerrainList();
}

const KeyTerrainList * BattleData::keyTerrainList(void) const {

   return d_imp->keyTerrainList();
}

const BattleMap* BattleData::map() const
{
   return d_imp->map();
}

BattleMap* BattleData::map()
{
   return d_imp->map();
}

BattleOB* BattleData::ob()
{
   return d_imp->ob();
}

const BattleOB* BattleData::ob() const
{
   return d_imp->ob();
}

void BattleData::ob(BattleOB* ob)
{
   d_imp->ob(ob);
}
const HexCord& BattleData::hexSize() const
{
   return d_imp->hexSize();
}

#ifdef DEBUG
void BattleData::assertHexOnMap(const HexCord& hex) const
{
   d_imp->map()->checkHex(hex);
}
#endif

BattleHexMap* BattleData::hexMap()
{
   return d_imp->hexMap();
}

const BattleHexMap* BattleData::hexMap() const
{
   return d_imp->hexMap();
}

void BattleData::setChanged()
{
   d_imp->setChanged();
}


void BattleData::cropMap(HexCord center, HexCord size) {

   d_imp->cropMap(center, size);
}


void
BattleData::processBuildings(BattleMeasure::BattleTime::Tick ticks) {

   d_imp->processBuildings(ticks);
}

/*
 * Time/Date Functions: Note that these must be thread-safe
 */

/*
 * Sets the current Date and Time
 */

void BattleData::setDateAndTime(const BattleTime& t)
{
   ASSERT(d_imp != 0);
   d_imp->setDateAndTime(t);
}

/*
 * Makes a copy of the current date/time
 */
BattleTime BattleData::getDateAndTime() const
{
   ASSERT(d_imp != 0);
   return d_imp->getDateAndTime();
}

/*
 * Adds on BattleTicks to the time
 */

void BattleData::addTime(BattleTime::Tick ticks)
{
   ASSERT(d_imp != 0);
   d_imp->addTime(ticks);
}

BattleMeasure::BattleTime::Tick BattleData::getTick() const
{
   return d_imp->getTick();
}

void BattleData::timeRatio(int n)
{
   ASSERT(n > 0);
   d_imp->timeRatio(n);
}

int BattleData::timeRatio() const
{
   return d_imp->timeRatio();
}

void BattleData::setSideLeftRight(Side s, HexCord leftHex, HexCord rightHex)
{
   d_imp->setSideLeftRight(s, leftHex, rightHex);
}

void BattleData::type(BattleData::Type t)
{
   d_imp->type(t);
}

BattleData::Type BattleData::type() const
{
   return d_imp->type();
}

void BattleData::getSideLeftRight(Side s, HexCord& leftHex, HexCord& rightHex)
{
   d_imp->getSideLeftRight(s, leftHex, rightHex);
}

Boolean BattleData::readData(FileReader& f, OrderBattle* gob, bool initDeployMap)
{
   d_imp->readData(f, gob);

   setupUnits(initDeployMap);
   return f.isOK();
}

/**
  * Add Units to HexMap after loading
  */

void BattleData::setupUnits(bool initDeployMap)
{
   BobUtility::initUnitTypeFlags(this);
   BatUnitUtils::InitUnits(ob());


   // If historical battle, then remake deploymaps
   // otherwise just add to hexmap because deployment maps are stored with CP

   B_ReinforceItem * item = ob()->reinforceList().first();
   while(item != NULL)
   {
      // bodge because cp->sp()->parent is NULL, set it to cp
      BattleSP* sp = item->d_cp->sp();
      while(sp)
      {
         sp->parent(item->d_cp);
         sp = sp->sister();
      }

      item = ob()->reinforceList().next();
   }

   if(initDeployMap)
   {

      /*
      Set all reinforcement CPs as inactive
      */
      B_ReinforceItem * item = ob()->reinforceList().first();
      while(item != NULL)
      {
         item->d_cp->active(false);
         item = ob()->reinforceList().next();
      }

      char tmp[1024];

      for(BattleUnitIter iter(ob());
         !iter.isFinished();
         iter.next())
      {
         /*
         Set as inactive if Hex == (0,0)
         */
         if(iter.cp()->hex() == HexCord(0,0))
         {
            sprintf(tmp,
               "FixMe!! - active CommandPosition %s (%s) has HexPosition of (0,0).",
               iter.cp()->getName(),
               iter.cp()->leader()->getName());
            FORCEASSERT(tmp);
            iter.cp()->active(false);
         }
         else
         {

            /*
            Player Deploy CP to the map
            */
            bool retval = PlayerDeploy::playerDeploy(this,
               iter.cp(),
               iter.cp()->hex(),
               0,
               PlayerDeploy::INITIALIZE | PlayerDeploy::TERRAINONLY);
            //                PlayerDeploy::Initializing);

            /*
            If deployment failed, Set unit as inactive
            */
            if(!retval)
            {
               sprintf(tmp,
                  "FixMe!! - active CommandPosition %s (%s) couldn't be deployed at HexPosition (%i,%i).",
                  iter.cp()->getName(),
                  iter.cp()->leader()->getName(),
                  iter.cp()->hex().x(), iter.cp()->hex().y());
               FORCEASSERT(tmp);
               iter.cp()->active(false);
            }
         }
      }

      // Bodge for command morale for top-level HQ units
      // and facings for all HQs
      for(int s = 0; s < 2; s++)
      {
         RefBattleCP topCP = ob()->getTop(s);
         while(topCP != NoBattleCP)
         {
            BobUtility::averageMorale(topCP);

            // bodge for HQ facing
            for(BattleUnitIter iter(topCP); !iter.isFinished(); iter.next())
            {
               if(iter.cp()->getRank().isHigher(Rank_Division))
               {
                  iter.cp()->setFacing(BobUtility::avgUnitFacing(iter.cp()));
               }
            }

            topCP = topCP->sister();
         }
      }

      d_imp->type(BattleData::Historical);

    }
   else
   {
      for(BattleUnitIter cpIter(ob());
         !cpIter.isFinished();
         cpIter.next())
      {
         RefBattleCP cp = cpIter.cp();
         if(cp->getRank().isHigher(Rank_Division))
         {
            hexMap()->add(cp);
         }

         for(BattleSPIter spIter(ob(), cp, BattleSPIter::OnDisplay);
            !spIter.isFinished();
            spIter.next())
         {
            RefBattleSP sp = spIter.sp();
            hexMap()->add(sp);
         }
      }
   }








   /*
   Make sure that all units in OB are within the map
   & complain if not so (so somebody will fix them !!)
   */

   HexCord bottomleft;
   HexCord size;
   d_imp->getPlayingArea(&bottomleft, &size);

   BattleUnitIter cp_iter(ob());
   while(!cp_iter.isFinished()) {

      char tmp[1024];
      
      // if independent, check the hex
      if(cp_iter.cp()->getRank().isHigher(Rank_Division)) {
         
         HexCord cp_hex = cp_iter.cp()->hex();
         // check if CP is completely off map
         if(cp_hex.x() < 0 || cp_hex.x() > map()->getSize().x() || cp_hex.y() < 0 || cp_hex.y() > map()->getSize().y()) {
            sprintf(tmp,
               "FixMe!! - Indpendent CP %s (%s) is completely off the map, at HexPosition (%i,%i).",
               cp_iter.cp()->getName(),
               cp_iter.cp()->leader()->getName(),
               cp_iter.cp()->hex().x(), cp_iter.cp()->hex().y());
            FORCEASSERT(tmp);
            cp_iter.cp()->active(false);
         }
#if 0
         // check if CP is outside playing area
         else if(cp_hex.x() < bottomleft.x() || cp_hex.x() > (bottomleft.x() + size.x()) || cp_hex.y() < bottomleft.y() || cp_hex.y() > (bottomleft.y() + size.y()) ) {
            sprintf(tmp,
               "FixMe!! - Indpendent CP %s (%s) is outside playing area (but within map), at HexPosition (%i,%i).",
               cp_iter.cp()->getName(),
               cp_iter.cp()->leader()->getName(),
               cp_iter.cp()->hex().x(), cp_iter.cp()->hex().y());
            FORCEASSERT(tmp);
            cp_iter.cp()->active(false);
         }
#endif
      }

      // else check the SP's hexes
      else {

         BattleSPIter sp_iter(ob(), cp_iter.cp());
         int sp_count = 0;

         while(!sp_iter.isFinished()) {

            HexCord sp_hex = sp_iter.sp()->hex();

            // check if CP is completely off map
            if(sp_hex.x() < 0 || sp_hex.x() > map()->getSize().x() || sp_hex.y() < 0 || sp_hex.y() > map()->getSize().y()) {
               sprintf(tmp,
                  "FixMe!! - SP number %i (%s (%s)) is completely off the map, at HexPosition (%i,%i).",
                  sp_count,
                  cp_iter.cp()->getName(),
                  cp_iter.cp()->leader()->getName(),
                  sp_iter.sp()->hex().x(), sp_iter.sp()->hex().y());
               FORCEASSERT(tmp);
               cp_iter.cp()->active(false);
            }
#if 0
            // check if CP is outside playing area
            else if(sp_hex.x() < bottomleft.x() || sp_hex.x() > (bottomleft.x() + size.x()) || sp_hex.y() < bottomleft.y() || sp_hex.y() > (bottomleft.y() + size.y()) ) {
               sprintf(tmp,
                  "FixMe!! - SP number %i (%s (%s)) is outside playing area (but within map), at HexPosition (%i,%i).",
                  sp_count,
                  cp_iter.cp()->getName(),
                  cp_iter.cp()->leader()->getName(),
                  sp_iter.sp()->hex().x(), sp_iter.sp()->hex().y());
               FORCEASSERT(tmp);
               cp_iter.cp()->active(false);
            }
#endif
            sp_count++;

            sp_iter.next();
         }

      }
      
   cp_iter.next();
   }

}

Boolean BattleData::writeData(FileWriter& f, bool standAlone) const
{
   return d_imp->writeData(f, standAlone);
}

// uncheckec update Jan 9, 1999
void BattleData::addToShockCombatList(const RefBattleCP& cp)
{
   d_imp->addToShockCombatList(cp);
}

ShockBattleList& BattleData::shockBattles()
{
   return d_imp->shockBattles();
}

const ShockBattleList& BattleData::shockBattles() const
{
   return d_imp->shockBattles();
}
// -------- end update
void BattleData::clear()
{
   delete d_imp;
   d_imp = new BattleDataImp;
}

bool BattleData::roadHex(const HexCord& hex) const
{
   const BattleTerrainHex& hi = getTerrain(hex);
   return hi.isRoad();
}

void BattleData::mode(BattleData::Mode mode)
{
   d_imp->mode(mode);
}

BattleData::Mode BattleData::mode() const
{
   return d_imp->mode();
}

void BattleData::endGameIn(BattleMeasure::BattleTime::Tick time)
{
   d_imp->endGameIn(time);
}

BattleMeasure::BattleTime::Tick BattleData::endGameIn(void)
{
   return d_imp->endGameIn();
}

void BattleData::addStartTimeToEndTime()
{
   d_imp->addStartTimeToEndTime();
}

bool BattleData::shouldEnd() const
{
   return d_imp->shouldEnd();
}

void BattleData::deleteOB()
{
   d_imp->deleteOB();
}


// };   // namespace WG_BattleData

/*----------------------------------------------------------------------
 * $Log$
 * Revision 1.3  2001/11/26 10:34:41  greenius
 * Current Directory detection improved
 *
 * Battlegame sound system initialised properly
 *
 * BattleAI Message queue problems fixed
 *
 * Order of Battles loading
 *
 * HexMap uses reference counted units
 *
 * BattleEditor open-file dialogs use wrapped classes
 *
 * PathFind compiler warnings removed
 *
 * Orphan detection in BattleOB detected and removed
 *
 * Revision 1.2  2001/06/18 08:25:43  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 * Revision 1.1  2001/06/13 08:52:36  greenius
 * Initial Import
 *
 *----------------------------------------------------------------------
 */


