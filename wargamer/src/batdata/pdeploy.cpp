/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
#include "stdinc.hpp"
#include "pdeploy.hpp"
#include "b_tables.hpp"
#include "bob_cp.hpp"
#include "bob_sp.hpp"
#include "batcord.hpp"
#include "batdata.hpp"
#include "batarmy.hpp"
#include "unittype.hpp"
#include "hexdata.hpp"
#include "hexmap.hpp"
#include "bobutil.hpp"
#include "bobiter.hpp"

#include "bob_vis.hpp"

using namespace BattleMeasure;
using namespace BOB_Definitions;


namespace PDeploy_Local {

inline bool hasBridge(const BattleTerrainHex& hi)
{
   return (hi.d_path1.d_type == LP_RoadBridge || hi.d_path2.d_type == LP_RoadBridge ||
         hi.d_path1.d_type == LP_TrackBridge || hi.d_path2.d_type == LP_TrackBridge);
}

inline bool shouldBeVisible(BattleOB* ob, const RefBattleSP& sp)
{
      return BOB_Utility::isSPVisible(ob, sp);
}

int visibleCount(RPBattleData bd, const RefBattleCP& cp, bool bypassZeroSP)
{
      int spCount = 0;
      for(ConstBattleSPIter iter(bd->ob(), cp, BattleSPIter::OnDisplay);
            !iter.isFinished();
            iter.next())
      {
         if(!bypassZeroSP || iter.sp()->strength() > 0)
            ++spCount;
      }

      return spCount;
}
};

namespace PlayerDeploy {

bool canHexBeOccupied(
   RCPBattleData bd,
   const RefBattleCP& cp,
   const HexCord& hex,
   bool terrainOnly,
   bool ourUnitsToo,
   bool currentFormation,
   bool allowForBridge)
{
   const Table3D<UWORD>& tTable = BattleTables::terrainModifiers();
   const Table3D<UWORD>& pTable = BattleTables::pathModifiers();

   enum LUnitType {
      Inf,
      LCav,
      HCav,
      FootArt,
      HvyArt,
      HorseArt,

      End,
      Undefined = End
   } unitType = (cp->getRank().isHigher(Rank_Division) || cp->generic()->isInfantry()) ? Inf :
      (cp->generic()->isHeavyCavalry())   ? HCav :
      (cp->generic()->isCavalry())        ? LCav :
      (cp->generic()->isHeavyArtillery()) ? HvyArt :
      (cp->generic()->isHorseArtillery()) ? HorseArt :
      (cp->generic()->isArtillery())      ? FootArt : Undefined;

   // first see if we are in prohibited terrain
   // get terrain
   const BattleTerrainHex& hexInfo = bd->getTerrain(hex);

   bool cannotOccupy = False;

   enum {
      March,
      Column,
      Other
   } formation = (!currentFormation || cp->sp() == NoBattleSP || cp->sp()->fromFormation() == SP_MarchFormation) ? March :
                 (cp->sp()->fromFormation() == SP_ColumnFormation) ? Column : Other;

   // get terrain modifiers
   int v1 = tTable.getValue(unitType, hexInfo.d_terrainType, formation);
   if(v1 != 0)
   {
      if(allowForBridge && PDeploy_Local::hasBridge(hexInfo))
         ;
      else
      {
         // we passed terrain test. Now check for rivers
         // get path modifiers (i.e streams, rivers)
         for(int i = 0; i < 2; i++)
         {
            PathType pt = (i == 0) ? hexInfo.d_path1.d_type : hexInfo.d_path2.d_type;

            if(pt != LP_None)
            {
               int v2 = pTable.getValue(unitType, pt, CPF_March);
               if(v2 == 0)
                  cannotOccupy = True;
            }
         }
      }
   }

   else
      cannotOccupy = True;

   // now see if hex is already occuppied
   if(!cannotOccupy && !terrainOnly)// && cp->getRank().sameRank(Rank_Division))
   {
      const BattleHexMap* hexMap = bd->hexMap();

      BattleHexMap::const_iterator lower;
      BattleHexMap::const_iterator upper;
      if(hexMap->find(hex, lower, upper))
      {
         while(lower != upper)
         {
            const BattleUnit* unit = hexMap->unit(lower);
            CRefBattleCP uCP = BobUtility::getCP(unit);

            if(uCP != cp && uCP->getRank().sameRank(Rank_Division))
            {
               if(ourUnitsToo || uCP->getSide() != cp->getSide())
               {
                  cannotOccupy = True;
                  break;
               }
            }

            ++lower;
         }
      }
   }

   return !cannotOccupy;
}


bool playerDeploy(
   RPBattleData batData,
   const RefBattleCP & cp,
   const HexCord & startHex,
   const BattleOrderInfo* oi,
   HexList* hl,
   unsigned int flags)
{
   BattleOB* ob = batData->ob();

   // if this is a HQ unit
   if(cp->getRank().isHigher(Rank_Division))
   {

      if( ( (flags & PlayerDeploy::NEVERFAIL)==0) && (!canHexBeOccupied(batData, cp, startHex, (flags & PlayerDeploy::TERRAINONLY) != 0, False, False, False)))
         return False;

      else if(flags & PlayerDeploy::INITIALIZE)
         placeUnit(batData, cp, startHex);

      if(flags & PlayerDeploy::FILLHEXLIST)
         hl->newItem(startHex);

      return True;
   }


   if(flags & PlayerDeploy::INITIALIZE)
   {
      UINT str = BobUtility::unitSPStrength(batData, cp, True);
      cp->startMorale(cp->morale());
      cp->startStrength(str);
      cp->lastStrength(str);
   }


   // first, we need to count SP that will be visible,
   // An Inf XX only Inf SP will be visible
   // A Cav XX only Cav SP will be visible
   // An Arty xX only Arty SP will be visible

   int spCount = PDeploy_Local::visibleCount(batData, cp, (flags & PlayerDeploy::NOCOUNTZEROSP) != 0);

   if(spCount > 0)
   {

      HexCord::HexDirection hdR = rightFlank(oi->d_facing);

      // If here, we have visible SP
      // Calculate rows and columns according to current order
      int columns;
      int rows;
      BobUtility::getRowsAndColoumns(oi->d_divFormation, spCount, columns, rows);


      //vector<DeployItem>& deployMap = cp->deployMap();    // Get this here to simplify code

      if(!(flags & PlayerDeploy::FILLHEXLIST))
      {

         if(flags & PlayerDeploy::INITIALIZE || flags & PlayerDeploy::REORGANIZE)
         {
            cp->columns(columns);
            cp->rows(rows);
            cp->wantColumns(columns);
            cp->wantRows(rows);

            // A Bodge because the editor is adding
            // non-type SP (i.e. Arty in a Inf XX)
            // So we need to clear it out completely and start from scratch
            while(cp->mapSize() > 0)
               cp->mapPopBack();
            ASSERT(cp->mapSize() == 0);

            // allocate vector if we havn't already done so
            int howMuch = ((columns * rows) % 2 == 0) ? columns * rows : (columns * rows) + 1;
            //deployMap.resize(howMuch);
            cp->mapResize(howMuch);

            //ASSERT(cp->deployMap().size() == howMuch);
            ASSERT(cp->mapSize() == howMuch);
         }
      }


      // now assign hexes to map
      HexCord lHex = startHex;
      for(int r = 0; r < rows; r++)
      {

         HexCord hex = lHex;
         for(int c = 0; c < columns; c++)
         {
            if(rows > 1 &&
               columns > 1 &&
               r == rows - 1 &&
               c == columns - 1 &&
               (spCount % 2) != 0)
            {
               continue;
            }

            if((!(flags & PlayerDeploy::NEVERFAIL)) &&
               (!canHexBeOccupied(batData, cp, hex,  (flags & PlayerDeploy::TERRAINONLY) != 0, !(flags & (PlayerDeploy::REORGANIZE | PlayerDeploy::NOCOUNTZEROSP | PlayerDeploy::ENEMYONLY)), False, False)))
            {
               return False;
            }

            else
            {
               if((flags & PlayerDeploy::INITIALIZE) || (flags & PlayerDeploy::REORGANIZE))
                  cp->addToMap(NoBattleSP, &hex, c, r);

               if(flags & PlayerDeploy::FILLHEXLIST) 
                   hl->newItem(hex);
            }

            HexCord nextHex;
            if(batData->moveHex(hex, hdR, nextHex))
               hex = nextHex;
            else
               return False;
         }

         if(r + 1 < rows)
         {
            HexCord::HexDirection hdB;
            if((flags & PlayerDeploy::INITIALIZE) || (flags & PlayerDeploy::REORGANIZE))
            {
               hdB = (r % 2 == 0) ? rightRear(oi->d_facing) : leftRear(oi->d_facing);
            }

            else
            {
               if(cp->whichWay() == BattleCP::HO_Left)
               {
                  hdB = (r % 2 == 0) ? rightRear(oi->d_facing) : leftRear(oi->d_facing);
               }
               else
               {
                  hdB = (r % 2 == 0) ? leftRear(oi->d_facing) : rightRear(oi->d_facing);
               }
            }

            HexCord nextHex;
            if(batData->moveHex(lHex, hdB, nextHex)) lHex = nextHex;
               else return False;
         }
      }

      if(flags & PlayerDeploy::FILLHEXLIST)
         return (hl->entries() > 0);

      if(!(flags & PlayerDeploy::INITIALIZE))
         return True;

      // assign SP to map
      r = 0;
      int c = 0;
      RefBattleSP sp = cp->sp();
      while(sp != NoBattleSP)
      {
         if(PDeploy_Local::shouldBeVisible(ob, sp))
         {
            cp->addToMap(sp, 0, c, r);

            DeployItem* di = cp->wantDeployItem(c, r);
            ASSERT(di);
            if(!di)
               return False;

            di->sync();

            ASSERT(di->active());
            if(!di->active())
               return False;

            // intialize facing
            sp->setFacing(oi->d_facing);
            sp->nextFacing(oi->d_facing);
            sp->setRFacing(oi->d_facing);

            placeUnit(batData, sp, di->d_hex);

            if(++c == columns)
            {
               r++;
               c = 0;
            }

            sp->hexPosition().init(oi->d_facing);
         }

         sp = sp->sister();
      }

      cp->syncMapToSP();
      cp->formation(oi->d_divFormation);
      cp->nextFormation(oi->d_divFormation);
      cp->setFacing(oi->d_facing);
      BobUtility::countArtillery(ob, cp);

      // Another bodge because the editor is setting illegal SPFormation values
      if(cp->getRank().sameRank(Rank_Division) && cp->generic()->isArtillery())
      {
         BattleOrderInfo* nc_oi = const_cast<BattleOrderInfo*>(oi);


         nc_oi->d_spFormation = SP_UnlimberedFormation;
         for(std::vector<DeployItem>::iterator di = cp->mapBegin();
             di != cp->mapEnd();
             ++di)
         {
            if(di->active())
               di->d_sp->setFormation(SP_UnlimberedFormation);
         }
      }

   }

   return True;
}











#if 0
bool
playerDeploy(
   RPBattleData batData,
   const RefBattleCP & cp,
   const HexCord & startHex,
   BattleOrderInfo* oi,
   HexList* hl,
   unsigned int flags) {


      BattleOB* ob = batData->ob();

    // if this is a HQ unit
    if(cp->getRank().isHigher(Rank_Division)) {

      if((!(flags & PlayerDeploy::NEVERFAIL)) && (!canHexBeOccupied(batData, cp, startHex, (flags & PlayerDeploy::TERRAINONLY), False, False))) return False;

        else if(flags & PlayerDeploy::INITIALIZE) placeUnit(batData, cp, startHex);

        if(flags & PlayerDeploy::FILLHEXLIST) hl->newItem(startHex);

            return True;
    }


   if(flags & PlayerDeploy::INITIALIZE) {

      UINT str = BobUtility::unitSPStrength(batData, cp, True);
      cp->startMorale(cp->morale());
      cp->startStrength(str);
      cp->lastStrength(str);
   }


    // first, we need to count SP that will be visible,
    // An Inf XX only Inf SP will be visible
    // A Cav XX only Cav SP will be visible
    // An Arty xX only Arty SP will be visible

      int spCount = PDeploy_Local::visibleCount(batData, cp, (flags & PlayerDeploy::NOCOUNTZEROSP));

      if(spCount > 0) {

            HexCord::HexDirection hdR = rightFlank(oi->d_facing);

            // If here, we have visible SP
            // Calculate rows and columns according to current order
            int columns;
            int rows;
            BobUtility::getRowsAndColoumns(oi->d_divFormation, spCount, columns, rows);


            std::vector<DeployItem>& deployMap = cp->deployMap();    // Get this here to simplify code

            if(!(flags & PlayerDeploy::FILLHEXLIST)) {

         if(flags & PlayerDeploy::INITIALIZE || flags & PlayerDeploy::REORGANIZE) {

            cp->columns(columns);
            cp->rows(rows);
            cp->wantColumns(columns);
            cp->wantRows(rows);

            // A Bodge because the editor is adding
            // non-type SP (i.e. Arty in a Inf XX)
            // So we need to clear it out completely and start from scratch
            while(deployMap.size() > 0) deployMap.pop_back();

               ASSERT(deployMap.size() == 0);

               // allocate vector if we havn't already done so
              int howMuch = ((columns * rows) % 2 == 0) ? columns * rows : (columns * rows) + 1;

               if(deployMap.size() == 0 || howMuch != deployMap.size()) {

                /*
                 * I'm not convinced that this will work properly
                 * if hl==0 and NOT initializing then this could
                 * wipe out the deployment map because just after
                 * this section it returns if !initializing
                 * So this assertion checks that the new deployment map
                 * is smaller than the existing one if not intializing
                         *
                 * Note from Paul:
                 *   This will work because the only time the vector may actualy be resized is
                 *   from DeployBase::reorganize(). This routine does the actual assigning of SP to the vector
                 *   so all it needs from this routine is to resize the vector and assign hexes to it.
                 *
             * Note from Jim..
             *  I've scrapped all the hl==0 checks, & it now works on the 'flags' bit-field
             */

//                   ASSERT((flags & PlayerDeploy::INITIALIZE) || (flags & PlayerDeploy::REORGANIZE) || (howMuch <= deployMap.size()));

                 if(howMuch != deployMap.size()) {
                  while(deployMap.size() > 0)
                     deployMap.pop_back();
                   }

                  ASSERT(deployMap.size() == 0);
                  deployMap.resize(howMuch);
               }

               ASSERT(cp->deployMap().size() == howMuch);
            }
      }


        // now assign hexes to map
        HexCord lHex = startHex;
        for(int r = 0; r < rows; r++) {

         HexCord hex = lHex;
            for(int c = 0; c < columns; c++) {

            if((!(flags & PlayerDeploy::NEVERFAIL)) && (!canHexBeOccupied(batData, cp, hex,  (flags & PlayerDeploy::TERRAINONLY), !(flags & (PlayerDeploy::REORGANIZE | PlayerDeploy::NOCOUNTZEROSP)), False))) return False;

            else {

                            if((flags & PlayerDeploy::INITIALIZE) || (flags & PlayerDeploy::REORGANIZE)) cp->addToMap(NoBattleSP, &hex, c, r);

                            if(flags & PlayerDeploy::FILLHEXLIST) {
                   if(spCount > 1 && r == rows - 1 && c == columns - 1 && (spCount % 2) != 0);
                   else  hl->newItem(hex);
                }
                        }

                        HexCord nextHex;
                        if(batData->moveHex(hex, hdR, nextHex)) hex = nextHex;
                        else return False;
         }

                  if(r + 1 < rows) {

            HexCord::HexDirection hdB;

                        if((flags & PlayerDeploy::INITIALIZE) || (flags & PlayerDeploy::REORGANIZE)) {
               hdB = (r % 2 == 0) ? rightRear(oi->d_facing) : leftRear(oi->d_facing);
                        }

                        else {
               if(cp->whichWay() == BattleCP::HO_Left) {
                  hdB = (r % 2 == 0) ? rightRear(oi->d_facing) : leftRear(oi->d_facing);
                              }
                              else {
                  hdB = (r % 2 == 0) ? leftRear(oi->d_facing) : rightRear(oi->d_facing);
                              }
                        }

                        HexCord nextHex;
                        if(batData->moveHex(lHex, hdB, nextHex)) lHex = nextHex;
                        else return False;
         }
      }

            if(flags & PlayerDeploy::FILLHEXLIST) return (hl->entries() > 0);

            if(!(flags & PlayerDeploy::INITIALIZE)) return True;

            // assign SP to map
            {
                  int r = 0;
                  int c = 0;
                  RefBattleSP sp = cp->sp();
                  while(sp != NoBattleSP) {

            if(PDeploy_Local::shouldBeVisible(ob, sp)) {

                              cp->addToMap(sp, 0, c, r);

                              DeployItem& di = cp->deployItem(c, r);
                              di.sync();

                              ASSERT(di.active());

                              // intialize facing
                              sp->setFacing(oi->d_facing);
                    sp->nextFacing(oi->d_facing);
                    sp->setRFacing(oi->d_facing);

                    placeUnit(batData, sp, di.d_hex);

                    if(++c == columns) {
                        r++;
                        c = 0;
                    }

                    sp->hexPosition().init(oi->d_facing);
            }

                sp = sp->sister();
            }
        }

        cp->syncMapToSP();
        cp->formation(oi->d_divFormation);
        cp->nextFormation(oi->d_divFormation);
        cp->setFacing(oi->d_facing);
        BobUtility::countArtillery(ob, cp);

        // Another bodge because the editor is setting illegal SPFormation values
        if(cp->getRank().sameRank(Rank_Division) && cp->generic()->isArtillery())
        {
           oi->d_spFormation = SP_UnlimberedFormation;
           for(std::vector<DeployItem>::iterator di = cp->deployMap().begin();
               di != cp->deployMap().end();
               ++di)
           {
              if(di->active())
              di->d_sp->setFormation(SP_UnlimberedFormation);
           }
        }
            
    }

    return True;
}
#endif















#if 0


/*

  Does a player deploy, returning the hexlist irrespective of wether deployment was possible or not

*/

bool
getPlayerDeployHexes(
   RPBattleData batData,
   const RefBattleCP& cp,
   const HexCord& startHex,
   BattleOrder * order,
   HexList* hl,
   DeployFlags flags) {

    BattleOrderInfo* oi = (order->nextWayPoint()) ?
                &order->wayPoints().getLast()->d_order : &order->order();

    BattleOB* ob = batData->ob();

    // if this is a HQ unit
      if(cp->getRank().isHigher(Rank_Division))
    {

        if(flags == Initializing)
            placeUnit(batData, cp, startHex);

        if(hl)
            hl->newItem(startHex);
             
        return True;
    }

 // Unchecked -- Added by Paul
   if(flags == Initializing)
    {
         // Unchecked by Paul
         UINT str = BobUtility::unitSPStrength(batData, cp, True);
         // end unchecked
      cp->startMorale(cp->morale());
      cp->startStrength(str);
      cp->lastStrength(str);
   }

   
    // first, we need to count SP that will be visible,
    // An Inf XX only Inf SP will be visible
    // A Cav XX only Cav SP will be visible
    // An Arty xX only Arty SP will be visible

      int spCount = PDeploy_Local::visibleCount(batData, cp);

    if(spCount > 0)
    {
        HexCord::HexDirection hdR = rightFlank(oi->d_facing);

        // If here, we have visible SP
        // Calculate rows and columns according to current order
        int columns;
        int rows;
        BobUtility::getRowsAndColoumns(oi->d_divFormation, spCount, columns, rows);


        std::vector<DeployItem>& deployMap = cp->deployMap();    // Get this here to simplify code

        if(!hl)
        {
#if 0
            // initialize deployment map
            cp->columns(columns);
            cp->rows(rows);
            cp->wantColumns(columns);
            cp->wantRows(rows);
#endif
            if(flags == Initializing || flags == Reorganizing)
            {
               cp->columns(columns);
               cp->rows(rows);
               cp->wantColumns(columns);
               cp->wantRows(rows);

               // A Bodge because the editor is adding
               // non-type SP (i.e. Arty in a Inf XX)
               // So we need to clear it out completely and start from scratch
               while(deployMap.size() > 0)
                  deployMap.pop_back();

               ASSERT(deployMap.size() == 0);
            }

            // allocate vector if we havn't already done so
            int howMuch = ((columns * rows) % 2 == 0) ? columns * rows : (columns * rows) + 1;

            if(deployMap.size() == 0 || howMuch != deployMap.size())
            {
                /*
                 * I'm not convinced that this will work properly
                 * if hl==0 and NOT initializing then this could
                 * wipe out the deployment map because just after
                 * this section it returns if !initializing
                 * So this assertion checks that the new deployment map
                 * is smaller than the existing one if not intializing
                 *
                 * Note from Paul:
                 *   This will work because the only time the vector may actualy be resized is
                 *   from DeployBase::reorganize(). This routine does the actual assigning of SP to the vector
                 *   so all it needs from this routine is to resize the vector and assign hexes to it.
                 */

                ASSERT(flags == Initializing || flags == Reorganizing || (howMuch <= deployMap.size()));

                if(howMuch != deployMap.size())
                {
                  while(deployMap.size() > 0)
                     deployMap.pop_back();
                }

                ASSERT(deployMap.size() == 0);
                deployMap.resize(howMuch);
//                deployMap.reserve(howMuch);
//                deployMap.insert(deployMap.begin(), howMuch, DeployItem());
            }

            // The bodge above takes care of this. As stated above the deploymap needs to be
            // started from scratch
#if 0             
            else if(initializing)    // added by Steven to clear out existing deployMap
            {   // Set each element to default DeployItem
                // otherwise loading badly saved historical battles caused problems
                fill(deployMap.begin(), deployMap.end(), DeployItem());
            }
#endif
            ASSERT(cp->deployMap().size() == howMuch);
        }

        // now assign hexes to map
        HexCord lHex = startHex;
        for(int r = 0; r < rows; r++)
        {
            HexCord hex = lHex;
            for(int c = 0; c < columns; c++)
            {

                   if(flags == Initializing || flags == Reorganizing)
                     cp->addToMap(NoBattleSP, &hex, c, r);

                   if(hl)
                   {
                     if(r == rows - 1 && c == columns - 1 && (spCount % 2) != 0)
                        ;
                     else 
                        hl->newItem(hex);
                   }

                else
                    return False;

                HexCord nextHex;
                if(batData->moveHex(hex, hdR, nextHex))
                    hex = nextHex;
                else
                    return False;
            }

            if(r + 1 < rows)
            {
                HexCord::HexDirection hdB;

                if(flags == Initializing || flags == Reorganizing)
                {
                    hdB = (r % 2 == 0) ? rightRear(oi->d_facing) :
                            leftRear(oi->d_facing);
                }
                else
                {
                    if(cp->whichWay() == BattleCP::HO_Left)
                    {
                        hdB = (r % 2 == 0) ? rightRear(oi->d_facing) :
                                leftRear(oi->d_facing);
                    }
                    else
                    {
                        hdB = (r % 2 == 0) ? leftRear(oi->d_facing) :
                                rightRear(oi->d_facing);
                    }
                }

                HexCord nextHex;
                if(batData->moveHex(lHex, hdB, nextHex))
                    lHex = nextHex;
                else
                    return False;
            }
        }

        if(hl)
                return (hl->entries() > 0);

        if(flags != Initializing)
                return True;

        // assign SP to map
        {
            int r = 0;
            int c = 0;
            RefBattleSP sp = cp->sp();
            while(sp != NoBattleSP)
            {
                if(PDeploy_Local::shouldBeVisible(ob, sp))
                {
                    cp->addToMap(sp, 0, c, r);

                    DeployItem& di = cp->deployItem(c, r);
                    di.sync();

                    ASSERT(di.active());

//                    BattleOrder& order = cp->getCurrentOrder();

                    // intialize facing
                    sp->setFacing(oi->d_facing);
                    sp->nextFacing(oi->d_facing);
                    sp->setRFacing(oi->d_facing);

                    placeUnit(batData, sp, di.d_hex);

                    if(++c == columns)
                    {
                        r++;
                        c = 0;
                    }

                    sp->hexPosition().init(oi->d_facing);
                }

                sp = sp->sister();
            }
        }

        cp->syncMapToSP();
        cp->formation(oi->d_divFormation);
        cp->nextFormation(oi->d_divFormation);
        cp->setFacing(oi->d_facing);
        BobUtility::countArtillery(ob, cp);

        // Another bodge because the editor is setting illegal SPFormation values
        if(cp->getRank().sameRank(Rank_Division) && cp->generic()->isArtillery())
        {
           oi->d_spFormation = SP_UnlimberedFormation;
           for(std::vector<DeployItem>::iterator di = cp->deployMap().begin();
               di != cp->deployMap().end();
               ++di)
           {
              if(di->active())
              di->d_sp->setFormation(SP_UnlimberedFormation);
           }
        }
            
    }

    return True;
}





#endif


















void placeUnit(RPBattleData bd, const RefBattleUnit& unit, const HexCord& hex)
{
#ifdef DEBUG
        bd->assertHexOnMap(hex);
#endif


            HexPosition hp(moveRight(unit->facing()), static_cast<HexPosition::HexDistance>(unit->hexPosition().centerPos()));
            unit->position(BattlePosition(hex, hp));

            if(!bd->hexMap()->isUnitPresent(unit))
            {
             bd->hexMap()->add(unit);
            }
            else
            {
             BattleHexMap::iterator from = bd->hexMap()->find(unit);

//       ASSERT(unit->nextHex());
//       bu->setRFacing(bu->nextHex()->d_facing);
//       unit->position(BattlePosition(hex, unit->hexPosition()));
             bd->hexMap()->move(from);
            }
}

};

/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.3  2001/07/02 13:18:24  greenius
 * Battle, Campaign, Combined game and Campaign Editor now compile and run... mostly
 *
 * Revision 1.2  2001/06/18 08:25:43  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
