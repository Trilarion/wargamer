/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *      Battle Game: Order of Battle
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "batarmy.hpp"
#include "scenario.hpp"
#ifdef DEBUG
#include "boblog.hpp"
#endif
#include "bobiter.hpp"

/*
 * BattleSideInfo
 */


BattleOB::BattleSideInfo::BattleSideInfo() :
            d_top(NoBattleCP)
{
#ifdef DEBUG
        bobLog.printf("BattleSideInfo Constructor");
#endif
}

BattleOB::BattleSideInfo::~BattleSideInfo()
{
#ifdef DEBUG
            bobLog.printf("BattleSideInfo Destructor");
#endif
}

/*
 * Order of Battle
 */

/*
 * Constructor
 * Creates empty BattleOB
 */

BattleOB::BattleOB(OrderBattle* ob) :
    d_ob(ob),
    d_cpList(),
    d_reinforceList(), // Unchecked by Paul
    d_nSides(0),
    d_sides(0)
{
        ASSERT(ob != 0);

#ifdef DEBUG
        bobLog.printf("BattleOB Constructor from OrderBattle");
#endif

            // Create sides from ob.

        ASSERT(ob->sideCount() > 0);

        d_nSides = ob->sideCount();
        d_sides = new BattleSideInfo[d_nSides];
}

BattleOB::~BattleOB()
{
#ifdef DEBUG
        bobLog.printf("BattleOB Destructor");
#endif
        delete[] d_sides;
}

void BattleOB::deleteGenericOB()
{
#ifdef DEBUG
        bobLog.printf("deleteGenericOB");
#endif

        ASSERT(this != 0);
        ASSERT(d_ob != 0);

        deleteAllUnits();

        d_ob->reset();
        d_ob->cleanup();
        d_ob->deleteAll();

        delete d_ob;
        d_ob = 0;
}

void BattleOB::unlinkSister(ParamRefBattleCP paramSister, ParamRefBattleCP cp)
{
        ASSERT(paramSister != NoBattleCP);
        ASSERT(cp != NoBattleCP);

#ifdef DEBUG
        bobLog.printf("unlinkSister(%d %s, %d %s)",
                        static_cast<int>(paramSister->self()),
                (const char*) paramSister->getName(),
                static_cast<int>(cp->self()),
                (const char*) cp->getName());
#endif

        RefBattleCP sister = paramSister;

#ifdef DEBUG
        int safetyCounter = 100;
#endif

        while(sister->sister() != cp)
        {
                sister = sister->sister();
                // ASSERT(sister != NoBattleCP);
                if(sister == NoBattleCP)
                        return;
#ifdef DEBUG
                --safetyCounter;
                ASSERT(safetyCounter > 0);
                if(safetyCounter <= 0)
                        return;
#endif
        }
        ASSERT(sister->sister() == cp);
            sister->sister(cp->sister());
}

void BattleOB::unlinkUnit(ParamRefBattleCP cp)
{
        ASSERT(cp != NoBattleCP);

#ifdef DEBUG
        bobLog.printf("unlinkUnit(%d %s)",
                static_cast<int>(cp->self()),
                        (const char*) cp->getName());
#endif


        RefBattleCP parent = cp->parent();
        cp->parent(NoBattleCP);

        if(parent == NoBattleCP)
        {
                RefBattleCP sister = getTop(cp->getSide());
                if(sister == cp)
                {
                        setTop(cp->getSide(), cp->sister());
                }
                else
                {
                                    unlinkSister(sister, cp);
                }
        }
        else
        {
                RefBattleCP sister = parent->child();
                if(sister == cp)
                {
                        parent->child(cp->sister());
                }
                        else
                {
                        unlinkSister(sister, cp);
                }
        }

        cp->sister(NoBattleCP);
}


void BattleOB::unlinkUnitAndChildren(ParamRefBattleCP cp)
{
#ifdef DEBUG
        bobLog.printf("unlinkUnitAndChildren(%d %s)",
                static_cast<int>(cp->self()),
                (const char*) cp->getName());
#endif

        RefBattleCP child = cp->child();
        while(child != NoBattleCP)
        {
                RefBattleCP sister = child->sister();
                unlinkUnitAndChildren(child);
                child = sister;
        }

            cp->removeStrengthPoints();

        RefGLeader leader = cp->leader();
        d_ob->detachLeader(leader);

        unlinkUnit(cp);

                  ASSERT(cp->generic() != NoGenericCP);
                  cp->generic()->battleInfo(0);
        cp->generic(NoGenericCP);
}

void BattleOB::deleteAllUnits()
{
#ifdef DEBUG
        bobLog.printf("BattleOB::deleteAllUnits");
#endif

   // Remove reinforcement list

   d_reinforceList.reset();

   // Remove deployed units

        for(Side i = 0; i < d_nSides; ++i)
        {
#ifdef DEBUG
                bobLog.printf("deleting side %d", static_cast<int>(i));
#endif

                RefBattleCP cp = getTop(i);
                        if(cp != NoBattleCP)
                {
                        // setTop(i, NoBattleCP);
                        unlinkUnitAndChildren(cp);
                }
        }

        d_cpList.reset();
}

/*
 * Create a new unit
 * It is not attached to anything
 */

ReturnRefBattleCP BattleOB::createUnit()
{
        RefGenericCP cp = d_ob->createCommand();
        RefBattleCP bcp = d_cpList.createUnit(cp);

#ifdef DEBUG
        bobLog.printf("createUnit: %d", static_cast<int>(cp->getSelf()));
#endif

        return bcp;
}


void BattleOB::addChild(ParamRefBattleCP parent, ParamRefBattleCP child)
{
        ASSERT(child != NoBattleCP);

#ifdef DEBUG
        bobLog.printf("addChild(%d %s, %d %s)",
                (parent == NoBattleCP) ? -1 : static_cast<int>(parent->self()),
                (parent == NoBattleCP) ? "Top" : (const char*) parent->getName(),
                static_cast<int>(child->self()),
                (const char*) child->getName());
#endif

        ASSERT(child->parent() == NoBattleCP);
        ASSERT(child->sister() == NoBattleCP);
            ASSERT(child->child() == NoBattleCP);

        if(parent == NoBattleCP)
        {
                RefBattleCP sister = getTop(child->getSide());

                if(sister == NoBattleCP)
                        setTop(child->getSide(), child);
                else
                {
                                    while(sister->sister() != NoBattleCP)
                                sister = sister->sister();
                        sister->sister(child);
                }
        }
        else
        {
                child->parent(parent);

                RefBattleCP sister = parent->child();
                if(sister == NoBattleCP)
                        parent->child(child);
                else
                {
                        while(sister->sister() != NoBattleCP)
                                sister = sister->sister();
                                    sister->sister(child);
                }
        }
}



ReturnRefBattleCP BattleOB::getTop(Side side)
{
        ASSERT(side < d_nSides);
            return d_sides[side].get();
}

ReturnCRefBattleCP BattleOB::getTop(Side side) const
{
        ASSERT(side < d_nSides);
        return d_sides[side].get();
}

void BattleOB::setTop(Side side, ParamRefBattleCP cp)
{
        ASSERT(side < d_nSides);
        d_sides[side].set(cp);
}


void BattleOB::createLeader(ParamRefBattleCP cp)
{
        RefGLeader leader = d_ob->createLeader(cp->getSide(), cp->getNation(),
             cp->getRank().getRankEnum(), True);
        d_ob->attachLeader(cp->generic(), leader);

#ifdef DEBUG
        bobLog.printf("Created Leader %s attached to %s",
                (const char*) leader->getName(),
                (const char*) cp->getName());
#endif
}

void BattleOB::makeUnitName(ParamRefBattleCP cp)
{
        d_ob->makeUnitName(cp->generic(), cp->getRank());
}

ReturnRefBattleSP BattleOB::createStrengthPoint()
{
        ISP gSP = d_ob->createStrengthPoint(); 
        ASSERT(gSP != NoStrengthPoint);
// ---------- Unchecked Update -----------------
        RefBattleSP newSP = d_spList.create(gSP);
// ---------------------------------------------

            return newSP;
}

void BattleOB::attachStrengthPoint(ParamRefBattleCP cpi, ParamRefBattleSP sp)
{
        ASSERT(cpi != NoBattleCP);
        ASSERT(sp != NoBattleSP);
        ASSERT(cpi->generic() != NoGenericCP);
        ASSERT(sp->generic() != NoStrengthPoint);

            d_ob->attachStrengthPoint(cpi->generic(), sp->generic());
        cpi->addSP(sp);
}

ReturnRefBattleCP BattleOB::makeCP(RefGenericCP gcp)
{
        return d_cpList.createUnit(gcp);
}

ReturnRefBattleSP BattleOB::makeSP(ISP gSP)
{
        ASSERT(gSP != NoStrengthPoint);

// ------------- Unchecked update ---------------
        RefBattleSP newSP = d_spList.create(gSP);
// ----------------------------------------------
            return newSP;
}

void BattleOB::removeBattleUnits(bool ownsGeneric)
{
#ifdef DEBUG
        bobLog.printf("removeBattleUnits");
#endif


            for(Side i = 0; i < d_nSides; ++i)
        {
#ifdef DEBUG
                bobLog.printf("deleting side %d", static_cast<int>(i));
#endif

                RefBattleCP cp = getTop(i);
                if(cp != NoBattleCP)
                {
                        // setTop(i, NoBattleCP);
                        removeBattleUnits(cp, ownsGeneric);
                }
        }

        d_cpList.reset();
}



void BattleOB::removeBattleUnits(ParamRefBattleCP cp, bool ownsGeneric)
{
#ifdef DEBUG
        bobLog.printf("removeBattleUnits(%d %s)",
                static_cast<int>(cp->self()),
                (const char*) cp->getName());
#endif

        RefBattleCP child = cp->child();
        while(child != NoBattleCP)
        {
                RefBattleCP sister = child->sister();
                removeBattleUnits(child, ownsGeneric);
                child = sister;
        }

        cp->removeStrengthPoints();

        if(ownsGeneric)
        {
            RefGLeader leader = cp->leader();
            if(leader != NoGLeader)
            {
               d_ob->detachLeader(leader);
            }
        }

        unlinkUnit(cp);

        // ASSERT(cp->generic() != NoGenericCP);
        // cp->generic()->battleInfo(0);
        // cp->generic(NoGenericCP);
        d_cpList.removeUnit(cp);
}

/******************************************************
Start of James's change
******************************************************/
void
BattleOB::removeStrengthPoints(RefBattleCP cp) {

    while(cp->sp() != NoBattleSP) {
        removeStrengthPoint(cp->sp() );
    }

}


void
BattleOB::removeStrengthPoint(RefBattleSP sp) {

    BattleSP * spptr = sp;
    SPIndex spi = d_spList.getIndex(spptr);
      sp->parent()->removeStrengthPoint(sp);
      d_spList.removeSP(spi);

}


/******************************************************
End of James's change
******************************************************/

const UWORD BattleOB::s_fileVersion = 0x0003;
const char BattleOB::s_fileID[] = "BattleOB";

bool BattleOB::readData(FileReader& f)
{
    ASSERT(d_ob != 0);

    // if(d_standAlone)
    // {
    //     ASSERT(d_ob == 0);
    //     d_ob = new OrderBattle;
    //     d_ob->readData(f);
    // }


    FileChunkReader fc(f, s_fileID);
    UWORD version;
    f >> version;
    ASSERT(version <= s_fileVersion);

    if(version <= 0x0001)
    {
                // CPs are saved first...
                // This is a problem because the CPs can not access the SPs
                // Solution is simple... delete all the SPs!!!!

                d_cpList.readData(f, d_ob);
                d_spList.readData(f, *d_ob);

                d_spList.reset();

                GenericCPList* cpList = d_ob->cpList();
                for(CPIndex i = 0; i < cpList->entries(); ++i)
                {
                            if(cpList->isUsed(i))
                            {
                                        d_ob->deleteSPchain(&cpList->get(i));
                            }
                }

                d_ob->spList()->reset();
    }
    else
    {
                d_spList.readData(f, *d_ob);
                d_cpList.readData(f, d_ob);
                // Unchecked by Paul
                if(version >= 0x0003)
                   d_reinforceList.readData(f, d_ob);
                // ----------------------------
    }

    /*
       * Bodge for version where we forgot to save sides!
       */

    if(version < 0x0001)
    {
                d_sides->readData(f, d_ob);
                for(Side i = 1; i < d_nSides; ++i)
                {
                            RefGenericCP top = d_ob->getFirstUnit(i);
                            if(top == NoGenericCP)
                            {
                                        // Create a top unit to get us going...
    #if 0
                                        RefBattleCP newCP = createUnit();
                                        newCP->setSide(i);
                                        newCP->setNation(i);
                                        newCP->setRank(Rank_Army);
                                        makeUnitName(newCP);
                                        createLeader(newCP);
                                        addChild(NoBattleCP, newCP);
    #else
                                        // Assume was written by editor

                                        RefGenericCP top = d_ob->command(14);
                                        d_sides[i].set(battleCP(top));

    #endif
                            }
                            else
                                        d_sides[i].set(battleCP(top));
                }
    }
    else            // Do it properly!
    {
                for(Side i = 0; i < d_nSides; ++i)
                {
                            d_sides[i].readData(f, d_ob);
                }
    }

    /*
       * Set SP parents
       */

    for(TBattleUnitIter<BUIV_All> iter(this);
      !iter.isFinished();
      iter.next() )
    {
        RefBattleCP cp = iter.cp();

        ASSERT(cp->generic() != NoGenericCP);
        ASSERT(cp->generic()->battleInfo() == cp);


        for(TBattleSPIter<SPValidateAll> spIter(this, cp);
         !spIter.isFinished();
         spIter.next() )
        {
           RefBattleSP sp = spIter.sp();
           ASSERT(sp->generic() != NoStrengthPoint);
           ASSERT(sp->generic()->battleInfo() == sp);
           sp->parent(cp);
        }
    }

    return f.isOK();
}

bool BattleOB::writeData(FileWriter& f) const
{
    // write generic OB

    // if(d_standAlone)
    //     d_ob->writeData(f);

    // write Battle OB

   FileChunkWriter fc(f, s_fileID);
   f << s_fileVersion;
   d_spList.writeData(f, *d_ob);
   d_cpList.writeData(f, d_ob);
   // unchecked by Paul
   d_reinforceList.writeData(f, d_ob);
   // -----------------------------
   for(Side i = 0; i < d_nSides; ++i)
   {
      d_sides[i].writeData(f);
   }

   return f.isOK();
}

bool BattleOB::BattleSideInfo::readData(FileReader& f, OrderBattle* ob)
{
        CPIndex self;
        f >> self;
        d_top = battleCP(ob->command(self));
        return f.isOK();
}

bool BattleOB::BattleSideInfo::writeData(FileWriter& f) const
{
        f << d_top->self();
        return f.isOK();
}


void BattleOB::removeOrphans()
{
   BattleOB* bob = this;
   OrderBattle* ob = bob->ob();
   
   ob->cleanup();

   GenericCPList* cpList = ob->cpList();

   int howMany = cpList->entries();
   for(int i = 0; i < howMany; ++i)
   {
      if(cpList->isUsed(i))
      {
         RefGenericCP cp = (*cpList)[i];
         BattleCP* bcp = cp->battleInfo();

         if(bcp != 0)
         {
            bool invalid = false;

            if(bcp->parent() == NoBattleCP)
            {
               BattleCP* top = bob->getTop(bcp->getSide());
               while(top != NoBattleCP)
               {
                  if(top == bcp)
                  {
                     break;
                  }
                  top = top->sister();
               }

               if(top == NoBattleCP)
               {
#ifndef NOLOG
                  bobLog.printf("Orphan: %3d: %s", i, ob->getUnitName(cp));
#endif            
               // Remove the orphan:
                  bob->removeBattleUnits(bcp, true);

               }
            }
         }
      }
   }
}




/*
 * $Log$
 * Revision 1.3  2001/06/26 19:06:20  greenius
 * Resources compile... and get someway into battle game before asserting
 *
 * Revision 1.2  2001/06/18 08:25:43  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 * Revision 1.1  2001/06/13 08:52:36  greenius
 * Initial Import
 *
 *
 */