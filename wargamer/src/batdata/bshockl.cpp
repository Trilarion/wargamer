/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
#include "stdinc.hpp"
#include "bshockl.hpp"
#include "fsalloc.hpp"
#include "filebase.hpp"
#include "filecnk.hpp"
#include "ob.hpp"
#include "obdefs.hpp"
#include "bob_cp.hpp"

/*-------------------------------------------------------------------
 * SideShockBattleItem class, used for storing cps in shock combat.
 */

const int chunkSize = 20;

#ifdef DEBUG
static FixedSize_Allocator sideItemAlloc(sizeof(SideShockBattleItem), chunkSize, "SideShockBattleItem");
#else
static FixedSize_Allocator sideItemAlloc(sizeof(SideShockBattleItem), chunkSize);
#endif

void* SideShockBattleItem::operator new(size_t size)
{
  ASSERT(size == sizeof(SideShockBattleItem));
  return sideItemAlloc.alloc(size);
}

#ifdef _MSC_VER
void SideShockBattleItem::operator delete(void* deadObject)
{
  sideItemAlloc.free(deadObject, sizeof(SideShockBattleItem));
}
#else
void SideShockBattleItem::operator delete(void* deadObject, size_t size)
{
  ASSERT(size == sizeof(SideShockBattleItem));
  sideItemAlloc.free(deadObject, size);
}
#endif



/*
 * File Interface
 */

const UWORD SideShockBattleItem::s_fileVersion = 0x0000;
Boolean SideShockBattleItem::readData(FileReader& f, OrderBattle* ob)
{
  UWORD version;
  f >> version;
  ASSERT(version <= s_fileVersion);

  CPIndex cpi;
  f >> cpi;

  d_cp = (cpi != NoCPIndex) ? battleCP(ob->command(cpi)) : NoBattleCP;

  return f.isOK();
}

Boolean SideShockBattleItem::writeData(FileWriter& f, OrderBattle* ob) const
{
  f << s_fileVersion;

  CPIndex cpi = (d_cp != NoBattleCP) ? ob->getIndex(d_cp->generic()) : NoCPIndex;
  f << cpi;

  return f.isOK();
}

//-------------------------------------------------------------------------
SideShockBattleList& SideShockBattleList::operator = (const SideShockBattleList& hl)
{
  // reset list
  reset();

  // copy
  SideShockBattleListIterR iter(&hl);
  while(++iter)
  {
    SideShockBattleItem* hi = new SideShockBattleItem(*iter.current());
    ASSERT(hi);

    append(hi);
  }

  return *this;
}

SideShockBattleItem* SideShockBattleList::addItem(const RefBattleCP& cp)
{
  SideShockBattleItem* hi = new SideShockBattleItem(cp);
  ASSERT(hi);

  append(hi);

  return hi;
}

/*
 * File Interface
 */

const UWORD SideShockBattleList::s_fileVersion = 0x0000;
Boolean SideShockBattleList::readData(FileReader& f, OrderBattle* ob)
{
  UWORD version;
  f >> version;
  ASSERT(version <= s_fileVersion);

  int nItems;
  f >> nItems;
  ASSERT(nItems >= 0);
  while(nItems--)
  {
    SideShockBattleItem* bi = new SideShockBattleItem;
    ASSERT(bi);

    append(bi);

    if(!bi->readData(f, ob))
      return False;
  }

  return f.isOK();
}

Boolean SideShockBattleList::writeData(FileWriter& f, OrderBattle* ob) const
{
  f << s_fileVersion;

  int nItems = entries();
  f << nItems;

  SideShockBattleListIterR iter(this);
  while(++iter)
  {
    if(!iter.current()->writeData(f, ob))
      return False;
  }

  return f.isOK();
}

/*-------------------------------------------------------------------
 * ShockBattleItem class, used for storing shock battles.
 */

//const int chunkSize = 20;

#ifdef DEBUG
static FixedSize_Allocator itemAlloc(sizeof(ShockBattleItem), chunkSize, "ShockBattleItem");
#else
static FixedSize_Allocator itemAlloc(sizeof(ShockBattleItem), chunkSize);
#endif

void* ShockBattleItem::operator new(size_t size)
{
  ASSERT(size == sizeof(ShockBattleItem));
  return itemAlloc.alloc(size);
}

#ifdef _MSC_VER
void ShockBattleItem::operator delete(void* deadObject)
{
  itemAlloc.free(deadObject, sizeof(ShockBattleItem));
}
#else
void ShockBattleItem::operator delete(void* deadObject, size_t size)
{
  ASSERT(size == sizeof(ShockBattleItem));
  itemAlloc.free(deadObject, size);
}
#endif

/*
 * File Interface
 */

const UWORD ShockBattleItem::s_fileVersion = 0x0003;
Boolean ShockBattleItem::readData(FileReader& f, OrderBattle* ob)
{
  UWORD version;
  f >> version;
  ASSERT(version <= s_fileVersion);

  f >> d_nextRound;
  if(version >= 0x0001)
      f >> d_flags;

  if(version >= 0x0003)
  {
     f >> d_round;
     f >> d_winnerLastRound;
  }

  if(version >= 0x0002)
  {
      for(Side s = 0; s < NumberSides; s++)
      {
         d_units[s].readData(f, ob);
      }
  }

  return f.isOK();
}

Boolean ShockBattleItem::writeData(FileWriter& f, OrderBattle* ob) const
{
  f << s_fileVersion;

  f << d_nextRound;
  f << d_flags;
  f << d_round;
  f << d_winnerLastRound;

  for(Side s = 0; s < NumberSides; s++)
  {
      d_units[s].writeData(f, ob);
  }

  return f.isOK();
}


/*
 * File Interface
 */

static const char* s_chunkName = "ShockBattleList";
const UWORD ShockBattleList::s_fileVersion = 0x0000;
Boolean ShockBattleList::readData(FileReader& f, OrderBattle* ob)
{
   FileChunkReader fc(f, s_chunkName);
   UWORD version;
   f >> version;
   ASSERT(version <= s_fileVersion);

   int nItems;
   f >> nItems;
   ASSERT(nItems >= 0);
   while(nItems--)
   {
      ShockBattleItem* bi = new ShockBattleItem;
      ASSERT(bi);

      append(bi);

      if(!bi->readData(f, ob))
         return False;
   }

   return f.isOK();
}

Boolean ShockBattleList::writeData(FileWriter& f, OrderBattle* ob) const
{
   FileChunkWriter fc(f, s_chunkName);
   f << s_fileVersion;

   int nItems = entries();
   f << nItems;
   SListIterR<ShockBattleItem> iter(this);
   while(++iter)
   {
      if(!iter.current()->writeData(f, ob))
         return False;
   }
   return f.isOK();
}

//#endif
/*
 *----------------------------------------------------------------------
 * $Log$
 *----------------------------------------------------------------------
 */
