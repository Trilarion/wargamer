/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 * Battle Order
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "batord.hpp"
#include "fsalloc.hpp"
#include "filebase.hpp"
#include "bobutil.hpp"
#include "batdata.hpp"
#include "obdefs.hpp"
#include "ob.hpp"
#include "resstr.hpp"
#ifdef DEBUG
#include "clog.hpp"
static LogFile ordLog("BatOrd.log");
#endif

using namespace BOB_Definitions;
using namespace BattleMeasure;

/*-------------------------------------------------------------------
 * HexList class, used for storing routes, deployment hexes, etc.
 */

const int chunkSize = 10;

#ifdef DEBUG
static FixedSize_Allocator woItemAlloc(sizeof(WhatOrder), chunkSize, "WhatOrder");
#else
static FixedSize_Allocator woItemAlloc(sizeof(WhatOrder), chunkSize);
#endif

void* WhatOrder::operator new(size_t size)
{
  ASSERT(size == sizeof(WhatOrder));
  return woItemAlloc.alloc(size);
}

#ifdef _MSC_VER
void WhatOrder::operator delete(void* deadObject)
{
//  ASSERT(size == sizeof(WhatOrder));
  woItemAlloc.free(deadObject, sizeof(WhatOrder));
}
#else
void WhatOrder::operator delete(void* deadObject, size_t size)
{
  ASSERT(size == sizeof(WhatOrder));
  woItemAlloc.free(deadObject, size);
}
#endif

/*
 * File Interface
 */

const UWORD WhatOrder::s_fileVersion = 0x0000;
Boolean WhatOrder::readData(FileReader& f)
{
  UWORD version;
  f >> version;
  ASSERT(version <= s_fileVersion);

  UBYTE b;
  f >> b;
  b = d_whatOrder;

  return f.isOK();
}

Boolean WhatOrder::writeData(FileWriter& f) const
{
  f << s_fileVersion;

  UBYTE b = d_whatOrder;
  f << b;

  return f.isOK();
}

//-------------------------------------------------------------------------
WhatOrderList& WhatOrderList::operator = (const WhatOrderList& wl)
{
  // reset list
  reset();

  // copy
  SListIterR<WhatOrder> iter(&wl);
  while(++iter)
  {
    WhatOrder* wo = new WhatOrder(*iter.current());
    ASSERT(wo);

    append(wo);
  }

  return *this;
}


bool WhatOrderList::inList(WhatOrder::What w)
{
  SListIterR<WhatOrder> iter(this);
  while(++iter)
  {
    if(iter.current()->d_whatOrder == w)
      return True;
  }

  return False;
}

void WhatOrderList::removeWhat(WhatOrder::What w)
{
  SListIter<WhatOrder> iter(this);
  while(++iter)
  {
    if(iter.current()->d_whatOrder == w)
    {
      iter.remove();
      break;
    }
  }
}


int
WhatOrderList::pack(void * buffer, OrderBattle * ob) {

   int * dataptr = (int *) buffer;
   int bytes_written = 0;

   // pack num elements
   *dataptr = (int) getNItems();
   dataptr++;
   bytes_written += 4;

   
   // pack each 'whatOrder' item
   SListIterR<WhatOrder> iter(this);
   while(++iter) {

      *dataptr = (int) (iter.current()->d_whatOrder);
      dataptr++;
      bytes_written+=4;
   }

   return bytes_written;
}

int
WhatOrderList::unpack(void * buffer, OrderBattle * ob) {

   int * dataptr = (int *) buffer;
   int bytes_read = 0;

   // reset list
   reset();

   // unpack num elements
   int num = *dataptr;
   dataptr++;
   bytes_read+=4;

   // read in each element
   for(int i=0; i<num; i++) {

      WhatOrder::What w = (WhatOrder::What) *dataptr;
      dataptr++;
      bytes_read+=4;

      // add at end of list
      WhatOrder * wo = new WhatOrder(w);
      append(wo);
   }

   return bytes_read;
}


/*
 * File Interface
 */

const UWORD WhatOrderList::s_fileVersion = 0x0000;
Boolean WhatOrderList::readData(FileReader& f)
{
  UWORD version;
  f >> version;
  ASSERT(version <= s_fileVersion);

  int nItems;
  f >> nItems;
  ASSERT(nItems >= 0);
  while(nItems--)
  {
    WhatOrder* wo = new WhatOrder;
    ASSERT(wo);

    append(wo);

    if(!wo->readData(f))
      return False;
  }

  return f.isOK();
}

Boolean WhatOrderList::writeData(FileWriter& f) const
{
  f << s_fileVersion;

  int nItems = entries();
  f << nItems;

  SListIterR<WhatOrder> iter(this);
  while(++iter)
  {
    if(!iter.current()->writeData(f))
      return False;
  }

  return f.isOK();
}











int
WayPointList::pack(void * buffer, OrderBattle * ob) {

   int * dataptr = (int *) buffer;
   int bytes_written = 0;

   // write num elements
   *dataptr = (int) getNItems();
   dataptr++;
   bytes_written += 4;

   SListIterR<WayPoint> iter(this);
   while(++iter) {
      
      const WayPoint * wp = iter.current();

      *dataptr = (int) wp->d_hex.x();
      dataptr++;
      bytes_written += 4;

      *dataptr = (int) wp->d_hex.y();
      dataptr++;
      bytes_written += 4;

      WayPoint * ncwp = const_cast<WayPoint *>(wp);

      int n = ncwp->d_order.pack((void*)dataptr, ob);
      dataptr += (n/4);
      bytes_written += n;
   }

   return bytes_written;
}

int
WayPointList::unpack(void * buffer, OrderBattle * ob) {

   int * dataptr = (int *) buffer;
   int bytes_read = 0;

   // read num elements
   int num = (int) *dataptr;
   dataptr++;
   bytes_read+=4;

   for(int i=0; i<num; i++) {

      WayPoint * wp = new WayPoint;

      int x = *dataptr;
      dataptr++;
      bytes_read+=4;
      int y = *dataptr;
      dataptr++;
      bytes_read+=4;

      wp->d_hex = HexCord(x,y);

      int n = wp->d_order.unpack((void*)dataptr, ob);
      bytes_read+=n;
      dataptr += (n/4);

      // add at end of list
      append(wp);
   }

   return bytes_read;
}








// BattleOrderInfo static stuff-------------------------------------------

// TODO: get these strings from the resource file
const char* BattleOrderInfo::orderName(OrderMode om)
{
  ASSERT(om < OM_HowMany);

#if 0
  static const char* text[OM_HowMany] = {
    "Hold",
    "Move",
    "Rest \\ Rally",
    "Attach",
    "Detach",
//  "Attach Leader",
//    "Detach Leader",
    "Bombard",
    "Rout"
  };
#endif

   static InGameText::ID s_id[OM_HowMany] =
   {
      IDS_Hold,
      IDS_Move,
      IDS_RestRally,
      IDS_Attach,
      IDS_Detach,
      IDS_Bombard,
      IDS_Rout
   };

  return InGameText::get(s_id[om]);
}

const char* BattleOrderInfo::aggressionName(Aggression a)
{
  ASSERT(a < Aggress_HowMany);

#if 0
  static const char* text[Aggress_HowMany] = {
     "Avoid Contact",
     "Probe \\ Delay",
     "Limited Attack \\ Defend",
     "All-out Attack \\ Defend"
  };

  return text[a];
#endif

   return InGameText::get(a + IDS_BAT_AGRESSION);

}

const char* BattleOrderInfo::postureName(Posture p)
{
  ASSERT(p < Posture_HowMany);

#if 0
  static const char* text[Posture_HowMany] = {
     "Defensive",
     "Offensive"
  };

  return text[p];
#else

   return InGameText::get(p + IDS_POSTURE_FIRST);

#endif
}

const char* BattleOrderInfo::spFormationName(SPFormation f, bool arty)
{
#ifdef DEBUG
  if(!arty)
    ASSERT(f < SP_Formation_HowMany);
  else
    ASSERT(f < SP_Formation_ArtilleryHowMany);
#endif

#if 0
  static const char* text[SP_Formation_HowMany] = {
     "March",
     "Column",
     "Closed-Column",
     "Line",
     "Square"
  };

  static const char* artyText[SP_Formation_ArtilleryHowMany] = {
     "Limbered",
     "Unlimbered",
     "Prolong"
  };

  return (arty) ? artyText[f] : text[f];
#else

   if(arty)
      return InGameText::get(f + IDS_BAT_ART_FORMATION);
   else
      return InGameText::get(f + IDS_BAT_SP_FORMATION);

#endif

}

const char* BattleOrderInfo::divFormationName(CPFormation f)
{
  ASSERT(f < CPF_Last);

#if 0
  static const char* text[CPF_Last] = {
    "March",
    "Massed",
    "Deployed",
    "Extended"
  };

  return text[f];
#else
   return InGameText::get(f + IDS_BAT_DIV_FORMATION);
#endif
}

const char* BattleOrderInfo::deployHowName(CPDeployHow dh)
{
  ASSERT(dh < CPD_Last);
#if 0
  static const char* text[CPD_Last] = {
     "Center",
     "Right",
     "Left"
  };

  return text[dh];
#endif

   return InGameText::get(dh + IDS_BAT_DEPLOY);
}

const char* BattleOrderInfo::lineHowName(CPLineHow lh)
{
  FORCEASSERT("Obsolete function called");
  return 0;
}

const char* BattleOrderInfo::manueverName(CPManuever m)
{
  ASSERT(m < CPM_Last);
#if 0
  static const char* text[CPM_Last] = {
     "Forward",
     "Wheel Right",
     "Wheel Left",
     "Reverse Wheel Right",
     "Reverse Wheel Left",
     "Side-step Right",
     "Side-step Left",
     "About Face",
     "Hold"
  };

  return text[m];
#else
   return InGameText::get(m + IDS_BAT_MANUEVRE);
#endif
}

// file routines
inline bool operator >> (FileReader& f, BattleOrderInfo::OrderMode& om)
{
   UBYTE b;
   f >> b;

   om = static_cast<BattleOrderInfo::OrderMode>(b);

   return true;
}

inline bool operator << (FileWriter& f, const BattleOrderInfo::OrderMode& om)
{
   return f.putUByte(om);
}

inline bool operator >> (FileReader& f, CPFormation& cpf)
{
   UBYTE b;
   f >> b;

   cpf = static_cast<CPFormation>(b);
   return true;
}

inline bool operator << (FileWriter& f, const CPFormation& cpf)
{
   return f.putUByte(cpf);
}

inline bool operator >> (FileReader& f, SPFormation& spf)
{
   UBYTE b;
   f >> b;

   spf = static_cast<SPFormation>(b);
   return true;
}

inline bool operator << (FileWriter& f, const SPFormation& spf)
{
   return f.putUByte(spf);
}

inline bool operator >> (FileReader& f, CPDeployHow& dh)
{
   UBYTE b;
   f >> b;

   dh = static_cast<CPDeployHow>(b);
   return true;
}

inline bool operator << (FileWriter& f, const CPDeployHow& dh)
{
   return f.putUByte(dh);
}

inline bool operator >> (FileReader& f, CPLineHow& dh)
{
   UBYTE b;
   f >> b;

   dh = static_cast<CPLineHow>(b);
   return true;
}

inline bool operator << (FileWriter& f, const CPLineHow& lh)
{
   return f.putUByte(lh);
}

inline bool operator >> (FileReader& f, CPManuever& m)
{
   UBYTE b;
   f >> b;

   m = static_cast<CPManuever>(b);
   return true;
}

inline bool operator << (FileWriter& f, const CPManuever& m)
{
   return f.putUByte(m);
}

inline bool operator >> (FileReader& f, BattleOrderInfo::Aggression& ag)
{
   UBYTE b;
   f >> b;

   ag = static_cast<BattleOrderInfo::Aggression>(b);
   return true;
}

inline bool operator << (FileWriter& f, const BattleOrderInfo::Aggression& ag)
{
   return f.putUByte(ag);
}

inline bool operator >> (FileReader& f, BattleOrderInfo::Posture& p)
{
   UBYTE b;
   f >> b;

   p = static_cast<BattleOrderInfo::Posture>(b);
   return true;
}

inline bool operator << (FileWriter& f, const BattleOrderInfo::Posture& p)
{
   return f.putUByte(p);
}

const UWORD BattleOrderInfo::s_fileVersion = 0x0002;
Boolean BattleOrderInfo::readData(FileReader& f)
{
  UWORD version;
  f >> version;
  ASSERT(version <= s_fileVersion);

  f >> d_mode;
  f >> d_aggression;
  f >> d_posture;
  f >> d_spFormation;
  f >> d_divFormation;
  f >> d_deployHow;
  f >> d_lineHow;
  f >> d_manuever;
  f >> d_facing;

  if(version >= 0x0002)
  {
    UBYTE b;
    f >> b;
    d_faceHow = static_cast<CPFaceHow>(b);
  }
  if(version >= 0x0001)
    d_whatOrder.readData(f);

  return f.isOK();
}

Boolean BattleOrderInfo::writeData(FileWriter& f) const
{
  f << s_fileVersion;

  f << d_mode;
  f << d_aggression;
  f << d_posture;
  f << d_spFormation;
  f << d_divFormation;
  f << d_deployHow;
  f << d_lineHow;
  f << d_manuever;
  f << d_facing;

  UBYTE b = static_cast<UBYTE>(d_faceHow);
  f << b;

  d_whatOrder.writeData(f);

  return f.isOK();
}

void BattleOrderInfo::copy(const BattleOrderInfo& o)
{
  d_mode = o.d_mode;
  d_aggression = o.d_aggression;
  d_posture = o.d_posture;
  d_spFormation = o.d_spFormation;
  d_divFormation = o.d_divFormation;
  d_deployHow = o.d_deployHow;
  d_lineHow = o.d_lineHow;
  d_manuever = o.d_manuever;
  d_facing = o.d_facing;
  d_faceHow = o.d_faceHow;
  d_whatOrder = o.d_whatOrder;

#ifdef DEBUG
  ordLog.printf("DeployHow before = %s, DepoyHow after = %s",
      deployHowName(o.d_deployHow), deployHowName(d_deployHow));
#endif
}





int
BattleOrderInfo::pack(void * buffer, OrderBattle * ob) {

   int bytes_written = 0;
   int * dataptr = (int *) buffer;

   *dataptr = d_mode;
   dataptr++;
   bytes_written+=4;

   *dataptr = d_aggression;
   dataptr++;
   bytes_written+=4;

   *dataptr = d_posture;
   dataptr++;
   bytes_written+=4;

   *dataptr = d_spFormation;
   dataptr++;
   bytes_written+=4;

   *dataptr = d_divFormation;
   dataptr++;
   bytes_written+=4;

   *dataptr = d_deployHow;
   dataptr++;
   bytes_written+=4;

   *dataptr = d_lineHow;
   dataptr++;
   bytes_written+=4;

   *dataptr = d_manuever;
   dataptr++;
   bytes_written+=4;

   *dataptr = d_facing;
   dataptr++;
   bytes_written+=4;

   *dataptr = d_faceHow;
   dataptr++;
   bytes_written+=4;

   // pack 'whatOrderList'
   bytes_written += d_whatOrder.pack((void *)dataptr, ob);

   return bytes_written;
}


int
BattleOrderInfo::unpack(void * buffer, OrderBattle * ob) {

   int * dataptr = (int *) buffer;
   int bytes_read = 0;

   d_mode = (OrderMode) *dataptr;
   dataptr++;
   bytes_read+=4;

   d_aggression = (Aggression) *dataptr;
   dataptr++;
   bytes_read+=4;

   d_posture = (Posture) *dataptr;
   dataptr++;
   bytes_read+=4;

   d_spFormation = (SPFormation) *dataptr;
   dataptr++;
   bytes_read+=4;

   d_divFormation = (CPFormation) *dataptr;
   dataptr++;
   bytes_read+=4;

   d_deployHow = (CPDeployHow) *dataptr;
   dataptr++;
   bytes_read+=4;

   d_lineHow = (CPLineHow) *dataptr;
   dataptr++;
   bytes_read+=4;

   d_manuever = (CPManuever) *dataptr;
   dataptr++;
   bytes_read+=4;

   d_facing = (HexPosition::Facing) *dataptr;
   dataptr++;
   bytes_read+=4;

   d_faceHow = (CPFaceHow) *dataptr;
   dataptr++;
   bytes_read+=4;

   int n = d_whatOrder.unpack((void *)dataptr, ob);
   bytes_read += n;

   return bytes_read;

}







/*-------------------------------------------------------------------
 * Waypoint class, used for storing waypoint nodes.
 */

#ifdef DEBUG
static FixedSize_Allocator itemAlloc(sizeof(WayPoint), chunkSize, "WayPoint");
#else
static FixedSize_Allocator itemAlloc(sizeof(WayPoint), chunkSize);
#endif

void* WayPoint::operator new(size_t size)
{
  ASSERT(size == sizeof(WayPoint));
  return itemAlloc.alloc(size);
}

#ifdef _MSC_VER
void WayPoint::operator delete(void* deadObject)
{
  itemAlloc.free(deadObject, sizeof(WayPoint));
}
#else
void WayPoint::operator delete(void* deadObject, size_t size)
{
  ASSERT(size == sizeof(WayPoint));
  itemAlloc.free(deadObject, size);
}
#endif

// file interface
const UWORD WayPoint::s_fileVersion = 0x0000;
Boolean WayPoint::readData(FileReader& f)
{
  UWORD version;
  f >> version;
  ASSERT(version <= s_fileVersion);

  if(!d_hex.readData(f))
    return False;

  if(!d_order.readData(f))
    return False;

  return f.isOK();
}

Boolean WayPoint::writeData(FileWriter& f) const
{
  f << s_fileVersion;

  if(!d_hex.writeData(f))
    return False;

  if(!d_order.writeData(f))
    return False;

  return f.isOK();
}

//-------------------------------------------------------------------------
WayPointList& WayPointList::operator = (const WayPointList& hl)
{
  // reset list
  reset();

  // copy
  SListIterR<WayPoint> iter(&hl);
  while(++iter)
  {
    WayPoint* wp = new WayPoint(iter.current()->d_hex, iter.current()->d_order);
    ASSERT(wp);

    append(wp);
  }

  return *this;
}

// file interface
const UWORD WayPointList::s_fileVersion = 0x0000;
Boolean WayPointList::readData(FileReader& f)
{
  UWORD version;
  f >> version;
  ASSERT(version <= s_fileVersion);

  int nItems;
  f >> nItems;
  ASSERT(nItems >= 0);

  while(nItems--)
  {
    WayPoint* wp = new WayPoint;
    ASSERT(wp);

    if(!wp->readData(f))
      return False;

   append(wp);    // Added SWG: 20th August 1999
  }

  return f.isOK();
}

Boolean WayPointList::writeData(FileWriter& f) const
{
  f << s_fileVersion;

  int nItems = entries();
  f << nItems;

  SListIterR<WayPoint> list(this);
  while(++list)
  {
    if(!list.current()->writeData(f))
      return False;
  }

  return f.isOK();
}

/*
 * default Constructor
 */


BattleOrder::BattleOrder() :
   d_order(),
   d_wayPoints(),
   d_attachTo(NoBattleCP),
   d_targetCP(NoBattleCP),
   d_flags(0),
   d_startOrderIn(0)
{
}

/*
 * Internal function to do copy an order
 */

void BattleOrder::copy(const BattleOrder& order)
{
   d_order = order.d_order;
   d_wayPoints = order.d_wayPoints;
   d_attachTo = order.d_attachTo;
   d_targetCP = order.d_targetCP;
   d_flags = 0; //order.d_flags;
   d_startOrderIn = order.d_startOrderIn;
}

/*
 * Comparison operator
 */

bool operator == (const BattleOrderInfo& ord1, const BattleOrderInfo& ord2)
{
   return(
      (ord1.d_mode == ord2.d_mode) &&
      (ord1.d_aggression == ord2.d_aggression) &&
      (ord1.d_posture == ord2.d_posture) &&
      (ord1.d_spFormation == ord2.d_spFormation) &&
      (ord1.d_divFormation == ord2.d_divFormation) &&
      (ord1.d_deployHow == ord2.d_deployHow) &&
      (ord1.d_lineHow == ord2.d_lineHow) &&
      (ord1.d_manuever == ord2.d_manuever) &&
      (ord1.d_facing == ord2.d_facing)
   );
}

bool operator == (const BattleOrder& ord1, const BattleOrder& ord2)
{
  return (ord1.d_order == ord2.d_order);
}



/*
 * File Interface
 */

const UWORD BattleOrder::s_fileVersion = 0x0003;

Boolean BattleOrder::readData(FileReader& f, OrderBattle* ob)
{
   UWORD version;
   f >> version;
   ASSERT(version <= s_fileVersion);

   if(!d_order.readData(f))
    return False;

   if(!d_wayPoints.readData(f))
    return False;

   CPIndex cpi;
   f >> cpi;
   d_attachTo = BobUtility::cpIndexToCP(cpi, *ob);

   if(version >= 0x0002)
   {
      CPIndex cpi;
      f >> cpi;
      d_targetCP = BobUtility::cpIndexToCP(cpi, *ob);
   }

   if(version >= 0x0001)
    f >> d_flags;

   if(version >= 0x0003)
      f >> d_startOrderIn;

   return f.isOK();
}

Boolean BattleOrder::writeData(FileWriter& f, OrderBattle* ob) const
{
   f << s_fileVersion;

   if(!d_order.writeData(f))
    return False;

   if(!d_wayPoints.writeData(f))
    return False;

   CPIndex cpi = BobUtility::cpToCPIndex(d_attachTo, *ob);
   f << cpi;

   cpi = BobUtility::cpToCPIndex(d_targetCP, *ob);
   f << cpi;

   f << d_flags;
   f << d_startOrderIn;

   return f.isOK();
}



int
BattleOrder::pack(void * buffer, OrderBattle * ob) {

   int bytes_written = 0;
   int * dataptr = (int *) buffer;

   int n = d_order.pack((void*)dataptr, ob);
   bytes_written += n;
   dataptr += (n/4);

   n = d_wayPoints.pack((void*)dataptr, ob);
   bytes_written += n;
   dataptr += (n/4);

   int index;

   if(d_attachTo) index = BobUtility::cpToCPIndex(d_attachTo, *ob);
   else index = -1;

   *dataptr = (int) index;
   dataptr++;
   bytes_written += 4;

   if(d_targetCP) index = BobUtility::cpToCPIndex(d_targetCP, *ob);
   else index = -1;

   *dataptr = (int) index;
   dataptr++;
   bytes_written += 4;

   *dataptr = (int) d_startOrderIn;
   dataptr++;
   bytes_written += 4;

   *dataptr = (int) d_flags;
   dataptr++;
   bytes_written += 4;

   return bytes_written;
}

void
BattleOrder::unpack(void * buffer, OrderBattle * ob) {

   int * dataptr = (int *) buffer;

   int n = d_order.unpack(dataptr, ob);
   dataptr += (n/4);

   n = d_wayPoints.unpack(dataptr, ob);
   dataptr += (n/4);

   int index;

   index = *dataptr;
   dataptr++;
   if(index != -1) {
   
      d_attachTo = BobUtility::cpIndexToCP(index, *ob);
   }

   index = *dataptr;
   dataptr++;
   if(index != -1) {
   
      d_targetCP = BobUtility::cpIndexToCP(index, *ob);
   }

   d_startOrderIn = (BattleMeasure::BattleTime::Tick) *dataptr;
   dataptr++;

   d_flags = (UBYTE) *dataptr;

}







BattleSentOrder::BattleSentOrder() :
   BattleOrder(),
   d_ticks(Invalid)
{
}

void BattleSentOrder::set(BattleTime::Tick ticks, const BattleOrder& order)
{
   d_ticks = ticks;
   BattleOrder::operator = (order);
}


void BattleSentOrder::copy(const BattleSentOrder& order)
{
   BattleOrder::operator = (order);
   d_ticks = order.d_ticks;
}

// file interface
const UWORD BattleSentOrder::s_fileVersion = 0x0000;
Boolean BattleSentOrder::readData(FileReader& f, OrderBattle* ob)
{
  UWORD version;
  f >> version;
  ASSERT(version <= s_fileVersion);

  f >> d_ticks;

  if(!BattleOrder::readData(f, ob))
    return False;

  return f.isOK();
}

Boolean BattleSentOrder::writeData(FileWriter& f, OrderBattle* ob) const
{
  f << s_fileVersion;

  f << d_ticks;

  if(!BattleOrder::writeData(f, ob))
    return False;

  return f.isOK();
}


BattleOrderList::BattleOrderList()
{
}


// If there are any orders in the queue then return true

bool BattleOrderList::getOrder(void) const
{
   if(!d_items.empty()) return true;
   else return false;
}

// If there are any orders in the queue then return true and copy to order

bool BattleOrderList::getOrder(BattleTime::Tick ticks, BattleOrder* order)
{
   if(!d_items.empty() && d_items.top().isTime(ticks))
   {
      *order = d_items.top();
      while(d_items.size() != 0)
      {
         d_items.pop();
      }
      return true;
   }
   return false;
}

BattleTime::Tick BattleOrderList::ticks() const
{
   if(!d_items.empty())
   {
      return d_items.top().ticks();
   }
   return 0;
}

bool BattleOrderList::ticks(BattleTime::Tick ticks)
{
   if(!d_items.empty())
   {
      const_cast<BattleSentOrder*>((&d_items.top()))->ticks(ticks);
      return True;
   }
   return False;
}

bool BattleOrderList::getOrder(BattleOrder* order)
{
   if(!d_items.empty())
   {
      *order = d_items.top();
      while(d_items.size() != 0)
      {
         d_items.pop();
      }
      return true;
   }
   return false;
}

// Get the last order that was sent

bool BattleOrderList::getLastOrder(BattleOrder* order) const
{
   if(!d_items.empty())
   {
      *order = d_items.top();
      return true;
   }
   return false;
}

void BattleOrderList::addOrder(BattleTime::Tick ticks, const BattleOrder& order)
{
   d_items.push(BattleSentOrder(ticks, order));
}

// file interface
const UWORD BattleOrderList::s_fileVersion = 0x0001;

Boolean BattleOrderList::readData(FileReader& f, OrderBattle* ob)
{
  UWORD version;
  f >> version;
  ASSERT(version <= s_fileVersion);

  int nItems;
  f >> nItems;
  ASSERT(nItems >= 0);

   if(version >= 0x0001)      // First version had this commented out
   {
      while(nItems--)
      {
         BattleSentOrder bs;
         if(!bs.readData(f, ob))
            return False;

         d_items.push(bs);
      }
   }
  return f.isOK();
}

Boolean BattleOrderList::writeData(FileWriter& f, OrderBattle* ob) const
{
  f << s_fileVersion;

  int nItems = d_items.size();
  f << nItems;

  Container newContainer = d_items;    // Copy items

  while(newContainer.size() != 0)
  {
   newContainer.top().writeData(f, ob);
   newContainer.pop();
  }


#if 0
  for(Container::const_iterator di = d_items.begin();
      di != d_items.end();
      di++)
  {
    if(!di->writeData(f, ob))
      return False;
  }
#endif

  return f.isOK();
}

/*
 * $Log$
 * Revision 1.1  2001/06/13 08:52:36  greenius
 * Initial Import
 *
 */



/*
 *----------------------------------------------------------------------
 * $Log$
 *----------------------------------------------------------------------
 */
