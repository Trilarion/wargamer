/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef FRACTMAP_HPP
#define FRACTMAP_HPP

/*

FractMap.hpp

This file creates a quick heightmap on a square grid, where edge size is a power of two
Height maps tesselate perfectly.  If a different sized map is needed, the system generates a large square map & crops to dimensions

*/

#include <stdio.h>




typedef struct FractalMapParameters {

    /* parameters which fractal routine must work within */

    // starting height of pixels
    unsigned char start_height;
    // minimum height allowed in map
    unsigned char minimum_height;
    // maximum height allowed in map
    unsigned char maximum_height;
    // starting ammount by which height may vary during gereration process
    unsigned char height_variation;
    // size of steps by which the height_variation may vary (ie. sharpness of height change)
    unsigned char height_roughness;
    // range within which new heights will fall after resampling (0...n)
    unsigned char resample_range;

        FractalMapParameters(void) { }

        FractalMapParameters(
            unsigned char st_h,
            unsigned char min_h,
            unsigned char max_h,
            unsigned char h_var,
            unsigned char h_rough,
            unsigned char res_range) :
                start_height(st_h),
                minimum_height(min_h),
                maximum_height(max_h),
                height_variation (h_var),
                height_roughness(h_rough),
                resample_range(res_range) { }


} FractalMapParameters;


    


class FractalMap {

/*
Variables & Structures
*/


    // map is initially created here
    char * TempData;
    // map is copied here, applying any cropping necessary
    char * MapData;
    // size of the generated map
    int grid_width;
    int grid_height;
    // size of the final map
    int width;
    int height;


    FractalMapParameters default_parameters;
    FractalMapParameters * parameters;

    // actual min & max heights in the map, used for resampling
    unsigned char actual_min;
    unsigned char actual_max;


/*
Function prototyes / inlines
*/

public:

    FractalMap(unsigned int w, unsigned int h,FractalMapParameters * params);
    ~FractalMap(void);

    int SetMapSize(unsigned int w, unsigned int h);
    void GenerateMap(void);
    void CropMap(void);
    void ResampleHeights(void);
    void ResampleHeightsRelative(void);
    void SmoothHeights(void);
    void FindMinMax(void);
    void SaveAsRaw(char * filename);
    
    /* returns an offset into the TempData array for an (x,y) coordinate, wrapped if outside valid map dimensions */
    inline int
    FractalMap::MapOffset(int x, int y) {
        // wrap coords over edge of map if necessary
        if(x<0) x+=grid_width;
        else if(x>=grid_width) x-=grid_width;
        if(y<0) y+=grid_height;
        else if(y>=grid_height) y-=grid_height;
        // return the index offset of coordinates
        return((y*grid_width)+x); }


    /* Get the height data from map coords (x,y) */
    inline unsigned char
    GetData(unsigned int x, unsigned int y) {
        if(x < width && y < height) return MapData[(y*width)+x];
        else return 0; }

    /* Set the map data at (x,y) to val */
    inline void
    SetData(unsigned int x, unsigned int y, unsigned char val) {
        if(x < width && y < height) MapData[(y*width)+x] = val; }



};


#endif
