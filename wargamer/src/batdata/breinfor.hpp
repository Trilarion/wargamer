/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef BREINFOR_HPP
#define BREINFOR_HPP

#include "bd_dll.h"
#include "sllist.hpp"
#include "batunit.hpp"
#include "batcord.hpp"

class FileReader;
class FileWriter;
class OrderBattle;

struct B_ReinforceItem : public SLink
{
   static const UWORD              s_fileVersion;

   RefBattleCP                     d_cp;
   BattleMeasure::BattleTime::Tick d_time;
   BattleMeasure::HexCord          d_hex;

   B_ReinforceItem(const RefBattleCP& cp, BattleMeasure::BattleTime::Tick tick, BattleMeasure::HexCord hex) :
      d_cp(cp),
      d_time(tick),
      d_hex(hex)
   {
   }

B_ReinforceItem() :
   d_cp(NoBattleCP),
      d_time(0),
      d_hex()
   {
   }

B_ReinforceItem(const B_ReinforceItem& ri) :
   d_cp(ri.d_cp),
      d_time(ri.d_time),
      d_hex(ri.d_hex)
   {
   }

   B_ReinforceItem& operator = (const B_ReinforceItem& ri)
   {
      d_cp = ri.d_cp;
      d_time = ri.d_time;
      d_hex = ri.d_hex;

      return *this;
   }

   BATDATA_DLL void* operator new(size_t size);
#ifdef _MSC_VER      // Visual C++ complains when using delete(void*, size_t)
        BATDATA_DLL void operator delete(void* deadObject);
#else
   BATDATA_DLL void operator delete(void* deadObject, size_t size);
#endif
   /*
   * File Interface
   */

   Boolean readData(FileReader& f, OrderBattle* ob);
   Boolean writeData(FileWriter& f, OrderBattle* ob) const;

};

class B_ReinforceList : public SList<B_ReinforceItem>
{
   static const UWORD              s_fileVersion;
public:
   B_ReinforceItem* addItem(const RefBattleCP& cp, BattleMeasure::BattleTime::Tick tick, BattleMeasure::HexCord hex)
   {
      B_ReinforceItem* ri = new B_ReinforceItem(cp, tick, hex);
      ASSERT(ri);

      append(ri);
      return ri;
   }

   bool removeItem(const RefBattleCP& cp)
   {

      B_ReinforceItem * ri = first();

      while( ri != NULL )
      {

         if( ri->d_cp == cp )
         {
            remove(ri);
            return true;
         }

         ri = next();
      }
      return false;
   }

   /*
   * File Interface
   */

   Boolean readData(FileReader& f, OrderBattle* ob);
   Boolean writeData(FileWriter& f, OrderBattle* ob) const;
};

#endif
