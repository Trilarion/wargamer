/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *      Create Random Tactical Order of Battle
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "b_ob_ran.hpp"
#include "batdata.hpp"
#include "batarmy.hpp"
#include "ob.hpp"
#include "random.hpp"
#include "bobiter.hpp"
#include "b_morale.hpp"
#include "bobutil.hpp"

#ifdef DEBUG
#include "boblog.hpp"
#endif

namespace BattleOBUtility
{

static const int NumberOfSides = 2;
static const ULONG RandomSeed = 40476931;

static RandomNumber obRand(RandomSeed);

#ifdef DEBUG

static void logOB(BattleOB* ob, BattleUnitIter& iter, bool oneLevel = false)
{
#ifdef TEST_BOBITER
        bobLog.printf("-----------------------------------------------");
#endif
        while(!iter.isFinished())
        {
                RefBattleCP cp = iter.cp();
                ASSERT(cp != NoBattleCP);
                RefGLeader leader = cp->leader();
                ASSERT(leader != NoGLeader);

                bobLog.printf("%s, %s", cp->getName(), leader->getName());

                for(BattleSPIter spIter(ob, cp, BattleSPIter::All); !spIter.isFinished(); spIter.next())
                {
                        RefBattleSP sp = spIter.sp();
                        
                        bobLog.printf("SP: %d %d, %s",
                                ob->getIndex(sp),
                                static_cast<int>(sp->getUnitType()),
                                ob->spName(sp) );
                }

                if(oneLevel)
                        iter.sister();
                else
                        iter.next();
        }
#ifdef TEST_BOBITER
        bobLog.printf("-----------------------------------------------");
#endif
}

static void logOB(BattleOB* ob)
{
#ifdef TEST_BOBITER
        {
                bobLog.printf("Using BattleUnitIter to list all units");
                BattleUnitIter iter(ob);                // Iterate all units from top to bottom
                logOB(ob, iter);
        }
        {
                bobLog.printf("Using BattleUnitIter to list all units bottom up");
                BattleUnitIter iter(ob);                // Iterate all units from top to bottom
                iter.setBottomUp();
                logOB(ob, iter);
        }
        {
                bobLog.printf("Using BattleUnitIter to list all side 0 top down");
                BattleUnitIter iter(ob, 0);     // Iterate all units from top to bottom
                logOB(ob, iter);
        }
        {
                bobLog.printf("Using BattleUnitIter to list all side 1 bottom up");
                BattleUnitIter iter(ob, 1);     // Iterate all units from top to bottom
                iter.setBottomUp();
                logOB(ob, iter);
        }
        {
                bobLog.printf("Using BattleUnitIter to list single unit");
                BattleUnitIter iter(ob->getTop(0)->child());    // Iterate all units from top to bottom
                logOB(ob, iter);
        }
        {
                bobLog.printf("Using BattleUnitIter to list single unit bottom up");
                BattleUnitIter iter(ob->getTop(1)->child());    // Iterate all units from top to bottom
                iter.setBottomUp();
                logOB(ob, iter);
        }
        {
                bobLog.printf("Using BattleUnitIter to list army 0's corps top down");
                BattleUnitIter iter(ob->getTop(0));     // Iterate all units from top to bottom
                iter.next();            // Get to 1st Corps
                logOB(ob, iter, true);
        }
#else
        bobLog.printf("-----------------------------------------------");
        for(Side side = 0; side < ob->sideCount(); ++side)
        {
                bobLog.printf("Side %d", static_cast<int>(side));
                BattleUnitIter iter(ob, side);  // Iterate all units from top to bottom
                logOB(ob, iter);
                bobLog.printf("-----------------------------------------------");
        }
#endif  // TEST_BOBITER
        bobLog.close();
}


#endif


static void createStrengthPoints(BattleOB* ob, ParamRefBattleCP parent, int count,
        BasicUnitType::value bt, BasicUnitType::value ot)
{
        ASSERT(ob != 0);
        ASSERT(parent != NoBattleCP);
        ASSERT(count > 0);

            if(bt == BasicUnitType::Infantry)
               parent->generic()->makeInfantryType(True);
            else if(bt == BasicUnitType::Cavalry)
               parent->generic()->makeCavalryType(True);
            else if(bt == BasicUnitType::Artillery)
               parent->generic()->makeArtilleryType(True);

        Nationality n = parent->getNation();

        // find unit type
        UnitType sput;
        for(;;)
        {
          UnitType ut = obRand(0, ob->getMaxUnitType() - 1);
          const UnitTypeItem& uti = ob->getUnitType(ut);

          if( (uti.isNationality(n)) &&
                        (lstrcmp("z", uti.getName()) != 0) &&
                        (uti.getBasicType() == bt) )
          {
                 sput = ut;
                 break;
          }
        }

        // find other type
        UnitType spot;
        for(;;)
        {
          UnitType ut = obRand(0, ob->getMaxUnitType() - 1);
          const UnitTypeItem& uti = ob->getUnitType(ut);

          if( (uti.isNationality(n)) &&
                        (lstrcmp("z", uti.getName()) != 0) &&
                        (uti.getBasicType() == ot) &&
                        (uti.isMounted() || bt != BasicUnitType::Cavalry) )
          {
                 spot = ut;
                 break;
          }
        }

        const int maxM = 10;
        const int rStartV = (bt == BasicUnitType::Artillery) ? 0 : maximum(0, count - maxM);
        int nOtherType = (bt == BasicUnitType::Artillery) ? 0 : obRand(minimum(2, rStartV), 2);
        int nType = count - nOtherType;

        // find our type of our nationality
        while(count--)
        {
                RefBattleSP sp = ob->createStrengthPoint();

                if((nType--) > 0)
                  sp->setUnitType(sput);

                else
                  sp->setUnitType(spot);

                ob->attachStrengthPoint(parent, sp);

#ifdef DEBUG
                bobLog.printf("Created sp %d %d, %s, attached to %s",
                        ob->getIndex(sp),
                        static_cast<int>(sp->getUnitType()),
                        ob->spName(sp),
                        parent->getName());
#endif
        }
}

static void createStrengthPoints(BattleOB* ob, ParamRefBattleCP parent, int count)
{
        ASSERT(ob != 0);
        ASSERT(parent != NoBattleCP);
        ASSERT(count > 0);

            BasicUnitType::value bt;
            int v = obRand(0, 100);
            if(parent->generic()->isInfantry() && !parent->generic()->isCavalry())
            {

               // what type to build? 70% chance  Inf, 30% chance Cav
               bt = (v <= 50) ? BasicUnitType::Infantry :
                        BasicUnitType::Cavalry;
            }
            else if(parent->generic()->isInfantry() && parent->generic()->isCavalry())
               bt = BasicUnitType::Infantry;
            else if(parent->generic()->isCavalry())
               bt = BasicUnitType::Cavalry;
            else
            {
               // what type to build? 70% chance  Inf, 30% chance Cav
               bt = (v <= 70) ? BasicUnitType::Infantry :
                        BasicUnitType::Cavalry;
            }

            BasicUnitType::value ot = BasicUnitType::Artillery;

            createStrengthPoints(ob, parent, count, bt, ot);
}


static void createUnits(BattleOB* ob, Side side, ParamRefBattleCP parent,
  RankEnum rank, int count, BasicUnitType::value what = BasicUnitType::Infantry)
{
        ASSERT(ob != 0);
        ASSERT(count > 0);
        if(count <= 0)
                return;

        while(count--)
        {
                RefBattleCP newCP = ob->createUnit();
                newCP->setSide(side);

                Nationality n;
                if(side == 0)
                  n = 0;
                else
                  n = (obRand(0, 1) == 0) ? 1 : 4;

                newCP->setNation(n);
                newCP->setRank(rank);
                ob->makeUnitName(newCP);
                ob->createLeader(newCP);
                ob->addChild(parent, newCP);

                switch(rank)
                {
                        case Rank_Army: {
                                createUnits(ob, side, newCP, Rank_Corps, obRand(3, 4));
                                                newCP->morale(BobUtility::baseMorale(ob, newCP));
                                                newCP->startMorale(newCP->morale());
//                                newCP->fatigue(255);
//                                newCP->ammoSupply(255);
//                                BattleMoraleUtility::CalcMorale(ob, newCP);
                                break;
                        }

                        case Rank_Corps: {
                                createUnits(ob, side, newCP, Rank_Division, obRand(3,4));
                                createUnits(ob, side, newCP, Rank_Division, 1 /*obRand(1, 2)*/, BasicUnitType::Artillery);
                                                newCP->morale(BobUtility::baseMorale(ob, newCP));
                                                newCP->startMorale(newCP->morale());
//                                newCP->fatigue(255);
//                                newCP->ammoSupply(255);
//                                BattleMoraleUtility::CalcMorale(ob, newCP);
                                                break;
                                    }

                                    case Rank_Division: {
                                                // Create Strength Points
                                                if(what == BasicUnitType::Infantry)
                                                   createStrengthPoints(ob, newCP, obRand(8, 12));
                                                else
                                                   createStrengthPoints(ob, newCP, (obRand(100) < 50) ? 1 : 2, BasicUnitType::Artillery, BasicUnitType::Artillery);

                                                newCP->morale(BobUtility::baseMorale(ob, newCP));
                                                newCP->startMorale(newCP->morale());

                                                //newCP->fatigue(255);
                                                //newCP->ammoSupply(100);
//                                              BattleMoraleUtility::CalcMorale(ob, newCP);

                                                break;
                                    }

                                    case Rank_God: { }
                                    case Rank_President: { }
                                    case Rank_ArmyGroup: { }
                        default:
                                FORCEASSERT("Illegal rank in BattleOBUtility::createUnits()");
                                break;
                }
        }
}

static void createSide(BattleOB* ob, Side side)
{
        ASSERT(ob != 0);

#ifdef DEBUG
        bobLog.printf("Creating side %d", static_cast<int>(side));
#endif

        createUnits(ob, side, NoBattleCP, Rank_Army, 1);
}

/*
 * Create a Random Order of Battle
 */

BattleOB* createRandom(bool noUnits)
{
#ifdef DEBUG
        bobLog.printf("createRandom");
#endif


        OrderBattle* gob = new OrderBattle;
        gob->createEmptyOB(NumberOfSides);

        BattleOB* ob = new BattleOB(gob);

        /*
         * For each side
         */

        if(!noUnits)
        {
            ASSERT(ob->sideCount() > 0);

            for(Side side = 0; side < ob->sideCount(); ++side)
            {
                createSide(ob, side);
            }

#ifdef DEBUG
            bobLog.printf("Finished Creating Random BattleOB");
            bobLog.close();

            logOB(ob);
#endif
      }

        return ob;
}

/*
 * Delete a random order of battle
 */

void deleteRandom(BattleOB* ob)
{
#ifdef DEBUG
        bobLog.printf("deleteRandom");
#endif  

        ASSERT(ob != 0);
        ob->removeBattleUnits(true);    // ownsGeneric is set so that leaders get removed
        ob->deleteGenericOB();
        delete ob;

#ifdef DEBUG
        bobLog.close();
#endif
}

};      // namespace BattleOBUtility


/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.2  2001/06/18 08:25:43  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
