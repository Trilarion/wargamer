/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1995, Steven Morle-Green, All Rights Reserved
 *----------------------------------------------------------------------
 *
 * Is SP visible?
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/06/13 08:52:36  greenius
 * Initial Import
 *
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "bob_vis.hpp"
#include "unittype.hpp"
#include "batarmy.hpp"

bool BOB_Utility::isSPVisible(const BattleOB* ob, ParamCRefBattleSP sp)
{
    const UnitTypeItem& uti = ob->getUnitType(sp);
    ConstRefGenericCP cp = sp->parent()->generic();

    switch(uti.getBasicType())
    {
      case BasicUnitType::Infantry:
         return cp->isInfantry();

      case BasicUnitType::Artillery:
         return cp->isArtillery();

      case BasicUnitType::Cavalry:
         return cp->isCavalry();

       default:
          return false;
    }
}


/*
 *----------------------------------------------------------------------
 * $Log$
 *----------------------------------------------------------------------
 */
