/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *      Battle Order of Battle Iterator Implementation
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "bobiter.hpp"
#include "bob_vis.hpp"
#ifdef DEBUG_LOG
#include "clog.hpp"
#include "scenario.hpp"
LogFile bLog("BobIter.log");
#endif
/*
 * Iterate ALL units
 */

BaseBattleUnitIter::BaseBattleUnitIter(const BaseBattleUnitIter& iter) :
        d_ob(iter.d_ob),
        d_side(iter.d_side),
        d_cp(iter.d_cp),
        d_top(iter.d_top),
        d_finished(iter.d_finished),
        d_topDown(iter.d_topDown),
        d_initialised(iter.d_initialised)
{
}

BaseBattleUnitIter& BaseBattleUnitIter::operator=(const BaseBattleUnitIter& iter)
{
   if(&iter != this)
   {
        d_ob = iter.d_ob;
        d_side = iter.d_side;
        d_cp = iter.d_cp;
        d_top = iter.d_top;
        d_finished = iter.d_finished;
        d_topDown = iter.d_topDown;
        d_initialised = iter.d_initialised;
    }
    return *this;
}




BaseBattleUnitIter::BaseBattleUnitIter(BattleOB* ob) :
        d_ob(ob),
        d_side(0),
        d_cp(NoBattleCP),
        d_top(NoBattleCP),
        d_finished(false),
        d_topDown(true),
        d_initialised(false)
{
        d_cp = ob->getTop(d_side);
#ifdef DEBUG_LOG
        if(d_cp)
            bLog.printf("\n---- Iterating All, top unit = %s", d_cp->getName());
#endif
}

/*
 * Iterate all units in a side
 */

BaseBattleUnitIter::BaseBattleUnitIter(BattleOB* ob, Side side) :
        d_ob(ob),
        d_side(SIDE_Neutral),
        d_cp(NoBattleCP),
        d_top(NoBattleCP),
        d_finished(false),
        d_topDown(true),
        d_initialised(false)
{
        d_cp = ob->getTop(side);
#ifdef DEBUG_LOG
        if(d_cp)
            bLog.printf("\n---- Iterating Side(%s), top unit = %s", scenario->getSideName(side), d_cp->getName());
#endif
}

/*
 * iterate unit and its children
 */

BaseBattleUnitIter::BaseBattleUnitIter(ParamRefBattleCP cp) :
        d_ob(0),
        d_side(SIDE_Neutral),
        d_cp(cp),
        d_top(cp),
        d_finished(false),
        d_topDown(true),
        d_initialised(false)
{
#ifdef DEBUG_LOG
        if(d_cp)
            bLog.printf("\n---- Iterating Command, top unit = %s", d_cp->getName());
#endif
}

/*
 * iterate top down (default)
 */

void BaseBattleUnitIter::setTopDown()
{
        d_topDown = true;
}

/*
 * return low level units first
 */

void BaseBattleUnitIter::setBottomUp()
{
        d_topDown = false;
        gotoBottom();
}

/*
 * Move to lowest child
 */

void BaseBattleUnitIter::gotoBottom()
{
        ASSERT(d_cp != NoBattleCP);

        while(d_cp->child() != NoBattleCP)
                d_cp = d_cp->child();
}

/*
 * Mark as finished
 */

void BaseBattleUnitIter::finish()
{
        d_finished  = true;
        d_cp = NoBattleCP;
}

/*
 * Advance to next side
 */

void BaseBattleUnitIter::dealWithTop()
{
        if(d_side != SIDE_Neutral)
        {
                ASSERT(d_top == NoBattleCP);
                ASSERT(d_ob != 0);

                ++d_side;

                if(d_side < d_ob->sideCount())
                {
                        d_cp = d_ob->getTop(d_side);
                        ASSERT(d_cp != NoBattleCP);
                        if(!d_topDown)
                                gotoBottom();
                }
                else
                {
                        finish();
                }
        }
        else
                finish();
}

/*
 * Move up to parent with a sister
 *
 *      otherwise, if unit has a sister, return sister
 *      otherwise back up to highest parent that has a sister
 *      If got to top, then advance to next side
 */

void BaseBattleUnitIter::moveUp()
{
        RefBattleCP cp = d_cp;

        ASSERT(d_cp != d_top);

        for(;;)
        {
                ASSERT(cp != NoBattleCP);
                ASSERT(!d_finished);

                if(cp->sister() != NoBattleCP)
                {
                        d_cp = cp->sister();
                        break;
                }

                cp = cp->parent();

                if(cp == d_top)
                {
                        // Get to here, if we've got to the top
                        // Advance to next side, or finish
                        dealWithTop();
                        break;
                }
        }
}

/*
 * advance to next unit, ignoring current unit's children
 */

void BaseBattleUnitIter::sister()
{
    ASSERT(d_topDown);              // doesn't make sense if bottom up

    ASSERT(d_cp != NoBattleCP);
    ASSERT(!d_finished);

    do
    {
        gotoSister();
    } while(!d_finished && (isValid(d_cp) != UnitValidator::OK) );
}

void BaseBattleUnitIter::gotoSister()
{
    ASSERT(d_topDown);              // doesn't make sense if bottom up

    ASSERT(d_cp != NoBattleCP);
    ASSERT(!d_finished);

    if(d_cp == d_top)
        dealWithTop();
    else if(d_cp->sister() != NoBattleCP)
        d_cp = d_cp->sister();
    else
        moveUp();
}

/*
 * advance to next unit, going into current unit's children if they exist
 */

void BaseBattleUnitIter::next()
{
    gotoNext();
    getValidNext();
#ifdef DEBUG_LOG
    if(d_cp)
    {
         bLog.printf("\n---- next unit = %s (active=%s)",
            d_cp->getName(), (d_cp->active()) ? "True" : "False");
    }
#endif
}

void BaseBattleUnitIter::getValidNext()
{
    while(!d_finished)
    {
        switch(isValid(d_cp))
        {
            case UnitValidator::OK:
                return;
            case UnitValidator::Invalid:
                gotoNext();
                break;
            case UnitValidator::OrgInvalid:
                gotoSister();
                break;
            default:
#ifdef DEBUG
                FORCEASSERT("Illegal ValidMode");
#else
                gotoSister();
#endif
        }
    }
}

void BaseBattleUnitIter::gotoNext()
{
    ASSERT(d_cp != NoBattleCP);
    ASSERT(!d_finished);

    // Do top down version first

    if(d_topDown)
    {
        // If current unit has children then go to first child

        RefBattleCP child = d_cp->child();
        if(child != NoBattleCP)
        {
            d_cp = child;
        }
        else if(d_cp == d_top)
            finish();
        else
        {
            // otherwise, if unit has a sister, return sister
            // otherwise back up to highest parent that has a sister
            // If got to top, then advance to next side

            moveUp();
        }
    }
    else
    {
        if(d_cp == d_top)
            finish();
        else
        {
            // If has a sister then go to sister

            if(d_cp->sister() != NoBattleCP)
            {
                d_cp = d_cp->sister();
                gotoBottom();
            }
            else
            {
                // else move up to parent

                d_cp = d_cp->parent();

                if(d_cp == NoBattleCP)
                    dealWithTop();
            }
        }
    }

    ASSERT(d_finished || (d_cp != NoBattleCP));
}


/*
 * report whether iterator has finished
 */

bool BaseBattleUnitIter::isFinished() const
{
    if(!d_initialised)
    {
        BaseBattleUnitIter* ncThis = const_cast<BaseBattleUnitIter*>(this);
        ncThis->getValidNext();

        d_initialised = true;
    }

    return d_finished;
}

/*
 * Get current unit
 */

ReturnRefBattleCP BaseBattleUnitIter::cp() const
{
        ASSERT(!d_finished);
        return d_cp;
}

/*
 * Validator for default of skipping dead or inactive units
 */

UnitValidator::ValidMode BUIV_OnlyActive::isValid(CRefBattleCP cp) const
{
    if(!cp->hasQuitBattle() && cp->active())
        return OK;
    else
        return OrgInvalid;
}

/*
 * Simplified BattleSPIter
 *                                           2
 * Paul's version had the following faults:
 *              -       If cp did not have any attached SPs, 
 *                      but mode was ArtilleryOnly and the CP type was Artillery
 *                      then it referenced a NULL sp while calling getNext()
 *              -       It confused cp type with sp type
 *              -       If a SP chain included any artillery then it would return
 *                      all SPs following it.
 */

bool BSPI_Default::isValid(CRefBattleSP sp) const
{
    if(d_mode == All)
          return true;

    if(sp->hasQuitBattle())
        return false;

    if(d_mode == ArtilleryOnly)
    {
       const UnitTypeItem& unitType = d_ob->getUnitType(sp);
       BasicUnitType::value basicType = unitType.getBasicType();
       return basicType == BasicUnitType::Artillery;
    }
    // else if(d_mode == InfCavOnly)
    else if(d_mode == OnDisplay)
    {
          // return basicType != BasicUnitType::Artillery;
          return BOB_Utility::isSPVisible(d_ob, sp);
    }
    else
    {
          FORCEASSERT("Illegal mode in BattleSPIter");
    }

    return false;
};


#if 0

void BattleSPIter::bodgedNext(void)
{

        ASSERT(!isFinished());

        if(d_sp != NoBattleSP)
        d_sp = d_sp->sister();

}


void BattleSPIter::getNext()
{
    BasicUnitType::value basicType;

    BasicUnitType::value dontType = 
                (d_mode == InfCavOnly) ? BasicUnitType::Artillery :
                                                                                    BasicUnitType::Infantry;

    BasicUnitType::value otherType = 
                (dontType == BasicUnitType::Infantry) ? BasicUnitType::Cavalry :
                                                                                                                            dontType;
    do
    {
        ASSERT(d_sp != NoBattleSP);

        d_sp = d_sp->sister();

        if(d_sp != NoBattleSP)
        {
            // Get Basic Type
            const UnitTypeItem& unitType = d_ob->getUnitType(d_sp);
            basicType = unitType.getBasicType();
        }
    } while( (d_sp != NoBattleSP) &&
                            ( (basicType == dontType) || (basicType == otherType) ) );
}

#endif

/*
 *----------------------------------------------------------------------
 * $Log$
 *----------------------------------------------------------------------
 */
