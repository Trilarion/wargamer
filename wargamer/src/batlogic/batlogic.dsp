# Microsoft Developer Studio Project File - Name="batlogic" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Dynamic-Link Library" 0x0102

CFG=batlogic - Win32 Debug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "batlogic.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "batlogic.mak" CFG="batlogic - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "batlogic - Win32 Release" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE "batlogic - Win32 Debug" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""
CPP=cl.exe
MTL=midl.exe
RSC=rc.exe

!IF  "$(CFG)" == "batlogic - Win32 Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "..\..\exe"
# PROP Intermediate_Dir "o\release"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MT /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "BATLOGIC_EXPORTS" /YX /FD /c
# ADD CPP /nologo /MD /W3 /Gi /GR /GX /Zi /O2 /I "..\h" /I "..\..\res" /I "..\system" /I "..\gamesup" /I "..\ob" /I "..\batdata" /D "NDEBUG" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "EXPORT_BATLOGIC_DLL" /YX"stdinc.hpp" /FD /c
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x809 /d "NDEBUG"
# ADD RSC /l 0x809 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /dll /machine:I386
# ADD LINK32 /nologo /dll /debug /machine:I386

!ELSEIF  "$(CFG)" == "batlogic - Win32 Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "..\..\exe"
# PROP Intermediate_Dir "o\debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MTd /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "BATLOGIC_EXPORTS" /YX /FD /GZ /c
# ADD CPP /nologo /MDd /W3 /Gm /Gi /GR /GX /ZI /Od /I "..\h" /I "..\..\res" /I "..\system" /I "..\gamesup" /I "..\ob" /I "..\batdata" /D "_DEBUG" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "EXPORT_BATLOGIC_DLL" /YX"stdinc.hpp" /FD /GZ /c
# ADD BASE MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x809 /d "_DEBUG"
# ADD RSC /l 0x809 /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /dll /debug /machine:I386 /pdbtype:sept
# ADD LINK32 /nologo /dll /debug /machine:I386 /out:"..\..\exe/batlogicDB.dll" /pdbtype:sept

!ENDIF 

# Begin Target

# Name "batlogic - Win32 Release"
# Name "batlogic - Win32 Debug"
# Begin Group "Source Files"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat"
# Begin Source File

SOURCE=.\b_combat.cpp
# End Source File
# Begin Source File

SOURCE=.\b_detect.cpp
# End Source File
# Begin Source File

SOURCE=.\b_end.cpp
# End Source File
# Begin Source File

SOURCE=.\b_fow.cpp
# End Source File
# Begin Source File

SOURCE=.\b_rction.cpp
# End Source File
# Begin Source File

SOURCE=.\b_reinf.cpp
# End Source File
# Begin Source File

SOURCE=.\batlogic.cpp
# End Source File
# Begin Source File

SOURCE=.\bdeploy.cpp
# End Source File
# Begin Source File

SOURCE=.\blog.cpp
# End Source File
# Begin Source File

SOURCE=.\blosses.cpp
# End Source File
# Begin Source File

SOURCE=.\bmanuevr.cpp
# End Source File
# Begin Source File

SOURCE=.\bmove.cpp
# End Source File
# Begin Source File

SOURCE=.\brecover.cpp
# End Source File
# Begin Source File

SOURCE=.\broute.cpp
# End Source File
# Begin Source File

SOURCE=.\bspform.cpp
# End Source File
# Begin Source File

SOURCE=.\bu_move.cpp
# End Source File
# Begin Source File

SOURCE=.\bu_ord.cpp
# End Source File
# Begin Source File

SOURCE=.\bunits.cpp
# End Source File
# Begin Source File

SOURCE=.\cmbtutil.cpp
# End Source File
# Begin Source File

SOURCE=.\moveutil.cpp
# End Source File
# Begin Source File

SOURCE=.\overnight.cpp
# End Source File
# Begin Source File

SOURCE=.\randomkills.cpp
# End Source File
# Begin Source File

SOURCE=.\reorglog.cpp
# End Source File
# End Group
# Begin Group "Header Files"

# PROP Default_Filter "h;hpp;hxx;hm;inl"
# Begin Source File

SOURCE=.\b_combat.hpp
# End Source File
# Begin Source File

SOURCE=.\b_detect.hpp
# End Source File
# Begin Source File

SOURCE=.\b_end.hpp
# End Source File
# Begin Source File

SOURCE=.\b_fow.hpp
# End Source File
# Begin Source File

SOURCE=.\b_rction.hpp
# End Source File
# Begin Source File

SOURCE=.\b_reinf.hpp
# End Source File
# Begin Source File

SOURCE=.\batlogic.hpp
# End Source File
# Begin Source File

SOURCE=.\bdeploy.hpp
# End Source File
# Begin Source File

SOURCE=.\bl_dll.h
# End Source File
# Begin Source File

SOURCE=.\blog.hpp
# End Source File
# Begin Source File

SOURCE=.\blosses.hpp
# End Source File
# Begin Source File

SOURCE=.\bmanuevr.hpp
# End Source File
# Begin Source File

SOURCE=.\bmove.hpp
# End Source File
# Begin Source File

SOURCE=.\brecover.hpp
# End Source File
# Begin Source File

SOURCE=.\broute.hpp
# End Source File
# Begin Source File

SOURCE=.\bspform.hpp
# End Source File
# Begin Source File

SOURCE=.\bu_move.hpp
# End Source File
# Begin Source File

SOURCE=.\bu_ord.hpp
# End Source File
# Begin Source File

SOURCE=.\bunits.hpp
# End Source File
# Begin Source File

SOURCE=.\cmbtutil.hpp
# End Source File
# Begin Source File

SOURCE=.\moveutil.hpp
# End Source File
# Begin Source File

SOURCE=.\overnight.hpp
# End Source File
# Begin Source File

SOURCE=.\randomkills.hpp
# End Source File
# Begin Source File

SOURCE=.\reorglog.hpp
# End Source File
# Begin Source File

SOURCE=.\stdinc.hpp
# End Source File
# End Group
# Begin Group "Resource Files"

# PROP Default_Filter "ico;cur;bmp;dlg;rc2;rct;bin;rgs;gif;jpg;jpeg;jpe"
# End Group
# End Target
# End Project
