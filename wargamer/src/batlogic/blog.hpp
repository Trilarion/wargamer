/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef BLOG_HPP
#define BLOG_HPP

#ifndef __cplusplus
#error blog.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *	Battle Logic Log
 *
 *----------------------------------------------------------------------
 */

#if !defined(NOLOG)
#include "clog.hpp"

namespace B_Logic
{
// extern LogFileFlush logfile;
extern LogFile logfile;
};

#endif	// NOLOG

#endif /* BLOG_HPP */

