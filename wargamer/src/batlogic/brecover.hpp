/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
#ifndef BRECOVER_HPP
#define BRECOVER_HPP

//#include "batunit.hpp"
//#include "batdata.hpp"
class BattleGameInterface;

namespace B_Logic {

class B_Recover {
   public:
      B_Recover(BattleGameInterface* batgame);
      ~B_Recover() {}

      void process();
      void newDay();
};

}; // end namespace
#endif

/*
 *----------------------------------------------------------------------
 * $Log$
 *----------------------------------------------------------------------
 */
