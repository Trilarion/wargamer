/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
#ifndef BU_MOVE_HPP
#define BU_MOVE_HPP

//#include "batdata.hpp"
//#include "batunit.hpp"
//#include "bobdef.hpp"

//namespace BattleMeasure { class HexCord; };
//using namespace BOB_Definitions;

class BattleGameInterface;

namespace B_Logic
{

class BU_MoveImp;

class BU_MoveInt {
      BU_MoveImp* d_bm;
   public:
      BU_MoveInt(BattleGameInterface* batgame);
      ~BU_MoveInt();

      void process();
};

};



#endif

/*
 *----------------------------------------------------------------------
 * $Log$
 *----------------------------------------------------------------------
 */
