/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
#include "stdinc.hpp"
#include "broute.hpp"
#include "batarmy.hpp"
#include "bobutil.hpp"
#include "hexdata.hpp"
#include "b_tables.hpp"
//#include "moveutil.hpp"
#include "pathfind.hpp"
#ifdef DEBUG
#include "scenario.hpp"
#include "clog.hpp"
static LogFile rLog("B_Route.log");
#endif
#include <utility>

#ifdef _MSC_VER
using namespace std::rel_ops;
#endif



using namespace BattleMeasure;

// local namespace
namespace Util
{
enum { RouteNotFound = UWORD_MAX };

enum LocalSPFormation {
                March,
                Column,
                Other
};

enum LocalUnitType {
         Inf,
         LCav,
         HCav,
         FootArt,
         HvyArt,
         HorseArt,

         End,
         Undefined = End
};

HexCord::HexDirection hexDirection(RPBattleData bd, const HexCord& srcHex, const HexCord& destHex)
{
  for(HexCord::HexDirection hd = HexCord::First; hd < HexCord::Count; INCREMENT(hd))
  {
    HexCord hex;
    if(bd->moveHex(srcHex, hd, hex))
    {
      if(hex == destHex)
        return hd;
    }
  }

  FORCEASSERT("Hex not adjacent");
  return HexCord::Stationary;
}

#if 0
inline int abValue(v)
{
   return (v >= 0) ? v : -v;
}
#endif

inline int abValue(int v) { return (v >= 0) ? v : -v; }
inline int squared(int v)
{
  return (v * v);
}

inline double getDist(const HexCord& srcHex, const HexCord& destHex)
{
   return sqrt(squared(destHex.x() - srcHex.x()) + squared(destHex.y() - srcHex.y()));
}

bool adjacentHex(const HexCord& hex1, const HexCord& hex2)
{
//  return ( (abValue(hex1.x() - hex2.x()) <= 1) && (abValue(hex1.y() - hex2.y()) <= 1) );
   if(hex1.y() % 2 == 0)
   {
      if(hex1.y() == hex2.y())
         return (abValue(hex1.x() - hex2.x()) == 1);

      else
      {
         int xDif = hex2.x() - hex1.x();
         return (abValue(hex1.y() - hex2.y()) == 1  && (xDif == -1 || xDif == 0));
      }
   }
   else
   {
      if(hex1.y() == hex2.y())
         return (abValue(hex1.x() - hex2.x()) == 1);

      else
      {
         int xDif = hex2.x() - hex1.x();
         return (abValue(hex1.y() - hex2.y()) == 1  && (xDif == 1 || xDif == 0));
      }
   }
}

void getTypeAndFormation(const RefBattleCP& cp, LocalSPFormation& spf, LocalUnitType& ut)
{
  // convert unit type flags to local enum
  ut = (cp->getRank().isHigher(Rank_Division) || cp->generic()->isInfantry()) ? Inf :
                 (cp->generic()->isHeavyCavalry())   ? HCav :
                 (cp->generic()->isCavalry())        ? LCav :
                 (cp->generic()->isHeavyArtillery()) ? HvyArt :
                 (cp->generic()->isHorseArtillery()) ? HorseArt :
                 (cp->generic()->isArtillery())      ? FootArt : Undefined;

  ASSERT(ut != Undefined);

  // convert spformation to local enum
  spf = March;
//  spf = (cp->getRank().isHigher(Rank_Division) || cp->spFormation() == SP_MarchFormation) ? March :
//                (cp->spFormation() == SP_ColumnFormation) ? Column : Other;
}

bool prohibitedTerrain(RPBattleData bd, const RefBattleCP& cp, const HexCord& hex)
{
  // convert unit type flags to local enum
  // convert spformation to local enum
  LocalUnitType unitType;
  LocalSPFormation formation;
  getTypeAndFormation(cp, formation, unitType);


  // get terrain
  const BattleTerrainHex& hexInfo = bd->getTerrain(hex);
  int v = BattleTables::prohibitedTerrain().getValue(unitType, hexInfo.d_terrainType, March);
  return (v == 0);
}

bool prohibitedPath(RPBattleData bd, const RefBattleCP& cp, const HexCord& hex)
{
     // if terrain is passable, check for river
     const BattleTerrainHex& hexInfo = bd->getTerrain(hex);
     LocalUnitType unitType;
     LocalSPFormation formation;
     getTypeAndFormation(cp, formation, unitType);
     for(int i = 0; i < 2; i++)
     {
        PathType pt = (i == 0) ? hexInfo.d_path1.d_type : hexInfo.d_path2.d_type;

        if(pt != LP_None)
        {
            if(BattleTables::pathModifiers().getValue(unitType, pt, formation) == 0)
               return True;
        }
     }

     return False;
}

inline bool hasBridge(const BattleTerrainHex& hi)
{
   return (hi.d_path1.d_type == LP_RoadBridge || hi.d_path2.d_type == LP_RoadBridge ||
         hi.d_path1.d_type == LP_TrackBridge || hi.d_path2.d_type == LP_TrackBridge);
}

bool prohibited(RPBattleData bd, const RefBattleCP& cp, const HexCord& startHex, int startC, bool pasteing)
{
   // See if move is prohibited, taking all front-line SP into account
   HexCord::HexDirection hd = (startC == 0) ? rightFlank(cp->facing()) : leftFlank(cp->facing());
   HexCord sHex = startHex;

   int nColumns = cp->columns();
   bool withBridge = False;
   bool noPath = False;

   int rows = (pasteing) ? cp->rows() : 1;
   for(int r = 0; r < rows; r++)
   {
      HexCord hex = sHex;
      if(nColumns > 0)
      {
         int loop = nColumns;
         while(loop--)
         {
            if(prohibitedTerrain(bd, cp, hex))
               return True;

            if(prohibitedPath(bd, cp, hex))
               noPath = True;

            const BattleTerrainHex& hexInfo = bd->getTerrain(hex);
            if(hasBridge(hexInfo))
               withBridge = True;

            if(loop)
            {
               HexCord nextHex;
               if(bd->moveHex(hex, hd, nextHex))
                  hex = nextHex;
               else
                  return True;
            }
         }
      }

      if(r + 1 < rows)
      {
         ASSERT(pasteing);
         DeployItem* di1 = cp->currentDeployItem(startC, r);
         ASSERT(di1);
         DeployItem* di2 = cp->currentDeployItem(startC, r + 1);
         ASSERT(di2);

         ASSERT(di1->active());
         if(!di1 || !di2 || !di1->active())
         {
            FORCEASSERT("SP out of alignment in prohibited");
            return True;
         }

         if(!di2->active())
         {
            ASSERT(cp->columns() > 1);
            // somethings out of sycc if here
            if(cp->columns() <= 1)
               break;

            // or here
            if(startC == 0)
            {
               FORCEASSERT("SP out of alignment in prohibited");
               return True;
            }


            else
            {
               di1 = cp->currentDeployItem(startC - 1, r);
               ASSERT(di1);
               di2 = cp->currentDeployItem(startC - 1, r + 1);
               ASSERT(di2);
            }
         }

         if(!di1 || !di2)
         {
            FORCEASSERT("SP out of alignment in prohibited");
            return True;
         }

         ASSERT(di1->active() && di2->active());
         if(!di1->active() || !di2->active())
            return True;

         if(di1->d_sp->hex() != di2->d_sp->hex())
         {
            // adjust if not adjacent
            if(!Util::adjacentHex(di1->d_sp->hex(), di2->d_sp->hex()))
            {
               ASSERT(cp->columns() > 1);
               // somethings out of sycc if here
               if(cp->columns() <= 1)
                  break;

               // or here
               int c = (startC == 0) ? 1 : startC;

               di1 = cp->currentDeployItem(c, r);
               ASSERT(di1);
               ASSERT(di1->active());
               if(!di1 || !di1->active())
                  return True;
            }

            // check again
            if(!Util::adjacentHex(di1->d_sp->hex(), di2->d_sp->hex()))
            {
               FORCEASSERT("SP out of sync");
               break;
            }

            HexCord::HexDirection hd = Util::hexDirection(bd, di1->d_sp->hex(), di2->d_sp->hex());

            if(!bd->moveHex(di1->d_sp->hex(), hd, sHex))
            {
               return True;
            }
         }
         else
            sHex = di1->d_sp->hex();

      }
   }

   return (noPath && !withBridge);
}

UWORD terrainCost(RPBattleData bd, const RefBattleCP& cp, const HexCord& hex)
{
  // convert unit type flags to local enum
  // convert spformation to local enum
  LocalUnitType unitType;
  LocalSPFormation formation;

  getTypeAndFormation(cp, formation, unitType);

  // get terrain
  const BattleTerrainHex& hexInfo = bd->getTerrain(hex);

  return static_cast<UWORD>(
         100 + maximum(0, 100 - BattleTables::terrainModifiers().getValue(unitType, hexInfo.d_terrainType, formation)));
}

bool hexInList(const HexCord& hex, const HexList& list)
{
  SListIterR<HexItem> iter(&list);
  while(++iter)
  {
         if(iter.current()->d_hex == hex)
                return True;
  }

  return False;
}

}; // end Util namespace

namespace B_Logic
{


bool B_Route::plotRoute(const HexCord& srcHex, const HexCord& destHex,
        HexList* excludeHexes, bool resetList, bool addSrc, bool formationChange)
{
  if(resetList)
         d_unit->routeList().reset();

  // first get straight-line route and its cost.
  HexList lx;
  if(excludeHexes)
         lx = *excludeHexes;

  HexList defaultList;

  // for formation changes, use the original pathing routine
  if(formationChange)
   formationChangeRoute(srcHex, destHex, (excludeHexes) ? &lx : 0, defaultList, addSrc);

  // for full movement use the new pathing routine
  else
   defaultRoute(srcHex, destHex, (excludeHexes) ? &lx : 0, defaultList, addSrc);

  d_unit->routeList().addList(defaultList);

  return (d_unit->routeList().entries() > 0);
}



bool B_Route::plotRoute(const HexCord& srcHex, HexList& destHexes, HexList* excludeHexes, bool formationChange)
{
  d_unit->routeList().reset();

#ifdef DEBUG
  rLog.printf("----------- Plotting route for %s %s SP(%ld)",
          scenario->getSideName(d_cp->getSide()),
          d_cp->getName(),
          reinterpret_cast<LONG>(d_unit));
#endif

  // plot route
  HexCord lSrcHex = srcHex;
  SListIterR<HexItem> iter(&destHexes);
  while(++iter)
  {
         if(!plotRoute(lSrcHex, iter.current()->d_hex, excludeHexes, False, (lSrcHex == srcHex), formationChange))
                return False;

         lSrcHex = iter.current()->d_hex;
  }

#ifdef DEBUG
  SListIterR<HexItem> riter(&d_unit->routeList());
  while(++riter)
  {
         rLog.printf("--------------------------- x = %d, y = %d",
                 static_cast<int>(riter.current()->d_hex.x()),
                 static_cast<int>(riter.current()->d_hex.y()));
  }

  rLog.printf("------------ End Route Search ----------------\n");
#endif
  return True;
}


UWORD B_Route::defaultHex(HexCord& hex, const HexCord& destHex,
        HexList* excludeHexes)
{
  HexCord::HexDirection bestDir = HexCord::Stationary;
  float bestDif = Util::RouteNotFound;
  UWORD bestCost = Util::RouteNotFound;

  for(HexCord::HexDirection d = HexCord::First; d < HexCord::End; INCREMENT(d))
  {
         HexCord edgeHex;
         if( (d_batData->moveHex(hex, d, edgeHex)) &&// )//&&
             (!Util::prohibitedTerrain(d_batData, d_cp, edgeHex)) &&
             (!Util::prohibitedPath(d_batData, d_cp, edgeHex)) )
         {
                /*
                 * See if we can add this hex
                 */

                if(!excludeHexes || !Util::hexInList(edgeHex, *excludeHexes))
                {
                  float dif = Util::getDist(edgeHex, destHex);
                  UWORD c = Util::terrainCost(d_batData, d_cp, edgeHex);

                  if( (bestDir == HexCord::Stationary) ||
                                (dif < bestDif) ||
                                (dif == bestDif && c < bestCost) )
                  {
                         bestDir = d;
                         bestDif = dif;
                         bestCost = c;
                  }
                }
         }
  }

  if(bestDir != HexCord::Stationary)
  {
         HexCord nextHex;
         if(d_batData->moveHex(hex, bestDir, nextHex))
         {
                hex = nextHex;
                return bestCost;
         }

         FORCEASSERT("Hex not found");
  }

  return Util::RouteNotFound;
}

UWORD B_Route::defaultHex(HexCord& hex, const HexCord& destHex, int startC, bool pasteing, const HexList& list)
{
  HexCord::HexDirection bestDir = HexCord::Stationary;
  float bestDif = Util::RouteNotFound;
  UWORD bestCost = Util::RouteNotFound;

  for(HexCord::HexDirection d = HexCord::First; d < HexCord::End; INCREMENT(d))
  {
         HexCord edgeHex;
         if(d_batData->moveHex(hex, d, edgeHex))
         {
            if(Util::hexInList(edgeHex, list) ||
               Util::prohibited(d_batData, d_cp, edgeHex, startC, pasteing))
            {
               continue;
            }

            /*
             * See if we can add this hex
             */

            float dif = Util::getDist(edgeHex, destHex);
            UWORD c = Util::terrainCost(d_batData, d_cp, edgeHex);

            if( (bestDir == HexCord::Stationary) ||
                (dif < bestDif) ||
                (dif == bestDif && c < bestCost) )
            {
                         bestDir = d;
                         bestDif = dif;
                         bestCost = c;
            }
         }
  }

  if(bestDir != HexCord::Stationary)
  {
         HexCord nextHex;
         if(d_batData->moveHex(hex, bestDir, nextHex))
         {
                hex = nextHex;
                return bestCost;
         }

         FORCEASSERT("Hex not found");
  }

  return Util::RouteNotFound;
}

bool B_Route::plotRoute(const HexCord& srcHex, const HexCord& destHex, int startC, bool pasteing, bool testOnly)
{
   UWORD bestCost = Util::RouteNotFound;
   if(!Util::prohibitedTerrain(d_batData, d_cp, destHex) &&
      srcHex != destHex)
   {
      HexCord hex = srcHex;
      HexList list;// = d_unit->routeList();

      list.newItem(hex, d_cp->facing());

      const int dist = static_cast<int>(Util::getDist(srcHex, destHex));

      int loops = 0;
      while(hex != destHex)
      {
         UWORD cost = defaultHex(hex, destHex, startC, pasteing, list);//&xHexes);

         if(cost != Util::RouteNotFound)
         {
            list.newItem(hex, d_cp->facing());
            if(bestCost == Util::RouteNotFound)
               bestCost = 0;

            bestCost += cost;
         }
         else
         {
            list.reset();
            return False;
         }
         if(++loops >= dist * 3)
         {
            list.reset();
            return False;
         }
      }

      if(!testOnly)
      {
         d_unit->routeList().reset();
         d_unit->routeList().addList(list);
      }
   }

   return True;
}


/*
bool formationRoute : should be set when plotting a simple formation-change route
*/

UWORD
B_Route::formationChangeRoute(const HexCord& srcHex,  const HexCord& destHex, HexList* excludeHexes, HexList& list, bool addSrc)
{
  UWORD bestCost = Util::RouteNotFound;
  if(!Util::prohibitedTerrain(d_batData, d_cp, destHex))
  {
         HexCord hex = srcHex;
         if(addSrc)
            list.newItem(hex, d_cp->facing());

         HexList xHexes;
         if(excludeHexes)
            xHexes = *excludeHexes;

         const int dist = static_cast<int>(Util::getDist(srcHex, destHex));

         int loops = 0;
         while(hex != destHex)
         {
                UWORD cost = defaultHex(hex, destHex, &xHexes);

                // add hex to exclude hexes so we don't get stuck in an infinite loop
                if(excludeHexes)
                  xHexes.newItem(hex);

                if(cost != Util::RouteNotFound)
                {
                  list.newItem(hex, d_cp->facing());
                  if(bestCost == Util::RouteNotFound)
                         bestCost = 0;

                  bestCost += cost;
                }
                else
                {
                  list.reset();
//                  return Util::RouteNotFound;
                  return 1;
                }
                if(++loops >= dist * 3)
                {
//                FORCEASSERT("Infinite loop in ::defaultRoute()");
                  list.reset();
                  return Util::RouteNotFound;
                }
         }

         if(excludeHexes)
                excludeHexes->addList(xHexes);
  }

  return bestCost;
}


UWORD
B_Route::defaultRoute(const HexCord& srcHex,  const HexCord& destHex, HexList* excludeHexes, HexList& list, bool addSrc) {

    PathFinder pathfinder(d_batData);

    int path_flags = PATHFLAG_AVOIDPROHIBITED | PATHFLAG_USEROADS | PATHFLAG_TERRAINCOST | PATHFLAG_AVOIDUPHILL | PATHFLAG_AVOIDDOWNHILL;

    if(!excludeHexes)
      pathfinder.FindPath(d_cp, srcHex, destHex, path_flags, NULL, &list);
    else
      pathfinder.FindPathExcludingHexes(d_cp, srcHex, destHex, path_flags, excludeHexes, &list);

    return 1;
}




}; // end namespace

/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.2  2001/06/18 08:25:44  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
