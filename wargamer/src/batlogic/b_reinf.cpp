/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
#include "stdinc.hpp"
#include "b_reinf.hpp"
#include "myassert.hpp"
#include "batctrl.hpp"
#include "moveutil.hpp"
#include "pdeploy.hpp"
#include "bobiter.hpp"
#include "initunit.hpp"
#include "bobutil.hpp"
#ifdef DEBUG
#include "clog.hpp"
LogFile rLog("BReinforcement.log");
#endif

namespace B_Logic
{

#ifdef DEBUG
const char* entryPointName(ReinforcementEntryPoint rep)
{
   ASSERT(rep < REP_HowMany);
   static const char* s_text[REP_HowMany] = {
      "Our Rear",
      "Near Left",
      "Far Left",
      "Near Right",
      "Far Right",
      "Enemy Rear",
      "None"
   };

   return s_text[rep];
}
#endif

class B_ReinforceImp {
      BattleGameInterface*            d_batgame;
      BattleData*                     d_batdata;
      BattleMeasure::BattleTime::Tick d_nextTick;
      bool                            d_bodged;
   public:
      B_ReinforceImp(BattleGameInterface* batgame) :
         d_batgame(batgame),
         d_batdata(batgame->battleData()),
         d_nextTick(0),
         d_bodged(False) {}
      ~B_ReinforceImp() {}

      void process();
      void processHistorical();
      void newDay() { d_nextTick = 0; }
   private:
      void processCommand(const RefBattleCP& cp);
      void processCommandHistorical(const RefBattleCP& cp, HexCord& hex);
      void placeUnit(const RefBattleCP& cp, HexCord& leftHex, HexPosition::Facing facing, BattleMeasure::BattleTime::Tick startWhen);
      void calcStartHex(HexPosition::Facing fFace, ReinforcementEntryPoint rep, HexCord& hex);
      void calcNextHex(HexPosition::Facing fFace, ReinforcementEntryPoint rep, HexCord& hex);
      void calcNextHex(const HexCord& startHex, HexCord& hex);
      HexPosition::Facing reinforcementFacing(HexPosition::Facing fFace, ReinforcementEntryPoint rep);
      HexPosition::Facing reinforcementFacing(const HexCord& startHex, const HexPosition::Facing fFace);
      bool findDeployPosition(const RefBattleCP& cp, HexPosition::Facing facing, HexCord& hex);
      void B_ReinforceImp::setMoveOrder(
         BattleOrder* batOrder,
         const HexCord& tHex);
      int toTheEdge(const HexCord& hex, int howManyPast);
};

int B_ReinforceImp::toTheEdge(const HexCord& hex, int howManyPast)
{
   HexCord bLeft;
   HexCord mSize;
   d_batdata->getPlayingArea(&bLeft, &mSize);
   HexCord tRight(bLeft.x() + mSize.x(), bLeft.y() + mSize.y());

   int bestX = 0;
   int bestY = 0;
   if(hex.x() < bLeft.x())// || hex.y() < bLeft.y()
      bestX = bLeft.x() - hex.x();
   else if(hex.x() >= tRight.x())
      bestX = hex.x() - tRight.x();

   if(hex.y() < bLeft.y())// || hex.y() < bLeft.y()
      bestY = bLeft.y() - hex.y();
   else if(hex.y() >= tRight.y())
      bestY = hex.y() - tRight.y();


   return maximum(bestX, bestY) + howManyPast;
}

void B_ReinforceImp::setMoveOrder(BattleOrder* batOrder, const HexCord& tHex)
{
    BattleOrderInfo* order = (batOrder->nextWayPoint()) ?
             &batOrder->wayPoints().getLast()->d_order : &batOrder->order();

    order->d_mode = BattleOrderInfo::Move;
//  order->d_whatOrder.add(WhatOrder::Facing);

    WayPoint* wp = new WayPoint(tHex, *order);
    ASSERT(wp);

//  wp->d_order.d_whatOrder.reset();
//  wp->d_order.d_facing = facing;
//  wp->d_order.d_whatOrder.add(WhatOrder::Facing);

    batOrder->wayPoints().append(wp);
}

const HexPosition::Facing c_ne = HexPosition::NorthEastPoint;
const HexPosition::Facing c_n  = HexPosition::North;
const HexPosition::Facing c_nw = HexPosition::NorthWestPoint;
const HexPosition::Facing c_sw = HexPosition::SouthWestPoint;
const HexPosition::Facing c_s  = HexPosition::South;
const HexPosition::Facing c_se = HexPosition::SouthEastPoint;

bool B_ReinforceImp::findDeployPosition(
         const RefBattleCP& cp,
         HexPosition::Facing facing,
         HexCord& hex)
{
   // TODO: use new map size system
//  HexCord mapSize = d_batdata->hexSize();
    cp->setFacing(facing);
    cp->getCurrentOrder().facing(facing);
    cp->getCurrentOrder().divFormation(CPF_Massed);
    cp->formation(CPF_Massed);

    HexCord bLeft;
    HexCord mSize;
    d_batdata->getPlayingArea(&bLeft, &mSize);
    HexCord tRight(bLeft.x() + mSize.x(), bLeft.y() + mSize.y());

    const int nDirections = 8;
    static const POINT p[nDirections] = {
                     {  1,  0 },  // E
                     {  1,  1 },  // NE
                     {  0,  1 },  // N
                     { -1,  1 },  // NW
                     { -1,  0 },  // W
                     { -1, -1 },  // SW
                     {  0, -1 },  // S
                     {  1, -1 },  // SE
   };

    enum LFacings {
      NE, N, NW, SW, S, SE, LFacings_HowMany
   } lFacing = (facing == HexPosition::NorthEastPoint) ? NE :
               (facing == HexPosition::North)          ? N :
               (facing == HexPosition::NorthWestPoint) ? NW :
               (facing == HexPosition::SouthWestPoint) ? SW :
               (facing == HexPosition::South)          ? S : SE;

   static const UBYTE s_canMoveThisWay[LFacings_HowMany][nDirections] = {
      {  False, False, False, True,  True,  True,  True,  True  }, // NE
      {  True,  False, False, False, True,  True,  True,  True  }, // N
      {  True,  True,  False, False, False, True,  True,  True  }, // NW
      {  True,  True,  True,  False, False, False, True,  True  }, // SW
      {  True,  True,  True,  True,  False, False, False, True  }, // S
      {  True,  True,  True,  True,  True,  False, False, False }, // SE
   };

#if 0
   static const UBYTE s_canMoveThisWay[LFacings_HowMany][nDirections] = {
                        {  True,  True,  True,  True,  False, False, False, True  }, // NE
                        {  True,  True,  True,  True,  True,  False, False, False  }, // N
                        {  False, True,  True,  True,  True,  True,  False, False  }, // NW
                        {  False, False, True,  True,  True,  True,  True,  False  }, // SW
                        {  False, False, False, True,  True,  True,  True,  True  }, // S
                        {  True,  False, False, False, True,  True,  True,  True }, // SE
   };
#endif
   for(int i = 0; i < nDirections; i++)
   {
      bool found = True;
      HexCord tHex = hex;

      int loop = 0;
      while(!PlayerDeploy::playerDeploy(d_batdata, cp, tHex, 0, PlayerDeploy::INITIALIZE | PlayerDeploy::TERRAINONLY))
      {
         if(!s_canMoveThisWay[lFacing][i])
         {
            found = False;
            break;
         }

         if(++loop >= 1000)
         {
            FORCEASSERT("Infinite loop in assignDeployPosition()");
            break;
         }

         int newX = tHex.x() + p[i].x;
         int newY = tHex.y() + p[i].y;

         if(newX < bLeft.x() ||
            newX >= tRight.x() ||
            newY < bLeft.y() ||
            newY >= tRight.y())
         {
            found = False;
            break;
         }

         found = True;
         tHex.x(static_cast<UBYTE>(newX));
         tHex.y(static_cast<UBYTE>(newY));
      }

      if(found)
         return True;
   }

   return False;
}

HexPosition::Facing B_ReinforceImp::reinforcementFacing(HexPosition::Facing fFace, ReinforcementEntryPoint rep)
{
   ASSERT(fFace == HexPosition::North || fFace == HexPosition::South);
   ASSERT(rep < REP_None);
   enum LFace {
      N, S, LFace_HowMany
   } lFace = (fFace == c_n)  ? N  : S;

   static const HexPosition::Facing s_table[LFace_HowMany][REP_None] = {
   // Our Rear, Near Left, Far Left, Near Right, Far Right, Enemy Rear
      { c_n,      c_ne,      c_se,      c_nw,       c_sw,      c_s }, //N
      { c_s,      c_sw,      c_nw,      c_se,       c_ne,      c_n }  //S
   };

   return s_table[lFace][rep];
}

struct AxisOffsetMult {
   int d_x;
   int d_y;
};

void B_ReinforceImp::calcStartHex(HexPosition::Facing fFace, ReinforcementEntryPoint rep, HexCord& hex)
{
   ASSERT(fFace == HexPosition::North || fFace == HexPosition::South);
   ASSERT(rep < REP_None);
   enum LFace {
      N, S, LFace_HowMany
   } lFace = (fFace == c_n)  ? N  : S;

   static const AxisOffsetMult s_table[REP_None][LFace_HowMany] = {
      // our rear
      {
         // N
         { 5, 0 },
         // S
         { 5, -1 }
      },
      // near left
      {
         // N
         { 0, 4 },
         // S
         { -1, 6 }
      },
      // far left
      {
         // N
         { 0, 6 },
         // S
         { -1, 4 }
      },
      // near right
      {
         // N
         { -1, 4 },
         // S
         { 0, 6 }
      },
      // far right
      {
         // N
         { -1, 6 },
         // S
         { 0, 4 }
      },
      // enemy rear
      {
         // N
         { 5, -1 },
         // S
         { 5, 0 }
      }
   };

   const int divisor = 10;
   AxisOffsetMult mult = s_table[rep][lFace];

// HexCord mapSize = d_batdata->hexSize();
   HexCord bLeft;
   HexCord mSize;
   d_batdata->getPlayingArea(&bLeft, &mSize);
   HexCord tRight(bLeft.x() + mSize.x(), bLeft.y() + mSize.y());

   // TODO: use new map size system
   int x = (mult.d_x == 0) ? bLeft.x() - 1 : (mult.d_x == -1) ? tRight.x() + 2 : bLeft.x() + ((mSize.x() * mult.d_x) / divisor);
   int y = (mult.d_y == 0) ? bLeft.y() - 1 : (mult.d_y == -1) ? tRight.y() + 2 : bLeft.y() + ((mSize.y() * mult.d_y) / divisor);

   hex.x(x);
   hex.y(y);
}

void B_ReinforceImp::calcNextHex(HexPosition::Facing fFace, ReinforcementEntryPoint rep, HexCord& hex)
{
   ASSERT(fFace == HexPosition::North || fFace == HexPosition::South);
   ASSERT(rep < REP_None);
   enum LFace {
      N, S, LFace_HowMany
   } lFace = (fFace == c_n)  ? N  : S;

   static const AxisOffsetMult s_table[REP_None][LFace_HowMany] = {
      // our rear
      {
         // N
         { 3, 0 },
         // S
         { -3, 0 }
      },
      // near left
      {
         // N
         { 0, -3 },
         // S
         { 0,  3 }
      },
      // far left
      {
         // N
         { 0, -3 },
         // S
         { 0,  3 }
      },
      // near right
      {
         // N
         { 0, -3 },
         // S
         { 0,  3 }
      },
      // far right
      {
         // N
         { 0, -3 },
         // S
         { 0,  3 }
      },
      // enemy rear
      {
         // N
         { -3, 0 },
         // S
         { 3, 0 }
      }
   };

   AxisOffsetMult mult = s_table[rep][lFace];

   HexCord mapSize = d_batdata->hexSize();

   // TODO: use new map size system
   int x = clipValue(hex.x() + mult.d_x, 0, mapSize.x() - 1);//bLeft.x(), tRight.x() - 1);
   int y = clipValue(hex.y() + mult.d_y, 0, mapSize.y() - 1);//bLeft.y(), tRight.y() - 1);

   hex.x(x);
   hex.y(y);
}

void B_ReinforceImp::calcNextHex(const HexCord& startHex, HexCord& hex)
{
   HexCord mapSize = d_batdata->hexSize();
   ASSERT(startHex.x() == 8 || startHex.y() == 8 || startHex.x() == mapSize.x() - 9 || startHex.y() == mapSize.y() - 9);

   enum Direction {
      E, N, W, S, Dir_HowMany
   } dir = (startHex.x() == 8)               ? W :
           (startHex.x() == mapSize.x() - 9) ? E :
           (startHex.y() == 8)               ? S : N;

   static const AxisOffsetMult s_table[Dir_HowMany] = {
       {  0,  3 }, // E
       { -3,  0 }, // N
       {  0, -3 }, // W
       {  3,  0 }  // S
   };

   AxisOffsetMult mult = s_table[dir];

   // TODO: use new map size system
   int x = clipValue(hex.x() + mult.d_x, 0, mapSize.x() - 1);//bLeft.x(), tRight.x() - 1);
   int y = clipValue(hex.y() + mult.d_y, 0, mapSize.y() - 1);//bLeft.y(), tRight.y() - 1);

   hex.x(x);
   hex.y(y);
}

HexPosition::Facing B_ReinforceImp::reinforcementFacing(const HexCord& startHex, const HexPosition::Facing fFace)
{
   HexCord mapSize = d_batdata->hexSize();
   ASSERT(startHex.x() == 8 || startHex.y() == 8 || startHex.x() == mapSize.x() - 9 || startHex.y() == mapSize.y() - 9);

   enum Direction {
      DE, DN, DW, DS, Dir_HowMany
   } dir = (startHex.x() == 8)               ? DW :
           (startHex.x() == mapSize.x() - 9) ? DE :
           (startHex.y() == 8)               ? DS : DN;

   enum LFace {
      NE, N, NW, SW, S, SE, LFace_HowMany
   } lFace = (fFace == c_ne) ? NE :
             (fFace == c_n)  ? N  :
             (fFace == c_nw) ? NW :
             (fFace == c_sw) ? SW :
             (fFace == c_s)  ? S : SE;

   static const HexPosition::Facing s_table[Dir_HowMany][LFace_HowMany] = {
      { c_nw, c_nw, c_nw, c_sw, c_sw, c_sw },
      { c_s,  c_s,  c_s,  c_s,  c_s,  c_s  },
      { c_ne, c_ne, c_ne, c_se, c_se, c_se },
      { c_n,  c_n,  c_n,  c_n,  c_n,  c_n  }
   };

   return s_table[dir][lFace];
}

void B_ReinforceImp::placeUnit(const RefBattleCP& cp, HexCord& leftHex, HexPosition::Facing facing, BattleMeasure::BattleTime::Tick startWhen)
{
   if(!findDeployPosition(cp, facing, leftHex))
   {
      FORCEASSERT("Unable to find deploy location");
      cp->setFled();
   }
   else
   {
      cp->active(True);
      leftHex = cp->leftHex();
      int nToTheEdge = toTheEdge(leftHex, 2);
      // now issue a move order to come in
      BattleOrder order;
      cp->lastOrder(&order);
      HexCord destHex;
      if(plotMove(d_batdata, cp->facing(), leftHex, destHex, nToTheEdge, MWD_Front))
      {
         setMoveOrder(&order, destHex);
         order.startOrderIn(startWhen);
         cp->sendOrder(d_batdata->getTick(), order);
#ifdef DEBUG
         rLog.printf("%s will move from hex(%d, %d) to hex(%d, %d) in %d minutes",
            cp->getName(),
            static_cast<int>(leftHex.x()),
            static_cast<int>(leftHex.y()),
            static_cast<int>(destHex.x()),
            static_cast<int>(destHex.y()),
            BattleMeasure::minutesPerTick(startWhen));
#endif
      }
      else
      {
         FORCEASSERT("Unable to move onto map");
         cp->setFled();
      }
   }
}

void B_ReinforceImp::processCommand(const RefBattleCP& cp)
{
   // first determine facing of reinforcements
   // get front facing first
#ifdef DEBUG
   rLog.printf("%s has arrived as reinforcement. Will arrive at %s",
      cp->getName(),
      entryPointName(cp->entryPoint()));
#endif

   BobUtility::initUnitTypeFlags(d_batdata->ob(), cp);

   HexCord sideLeft;
   HexCord sideRight;
   d_batdata->getSideLeftRight(cp->getSide(), sideLeft, sideRight);
   HexPosition::Facing fFace = frontFacing(sideLeft, sideRight);

   // convert to reinforcement facing
   HexPosition::Facing face = reinforcementFacing(fFace, cp->entryPoint());
#ifdef DEBUG
   rLog.printf("Unit facing = %d", static_cast<int>(face));
#endif

   // determin corp layout
   // place Divisions first, by Corp
   {
      HexCord startHex;
      calcStartHex(fFace, cp->entryPoint(), startHex);
      int nCorp = 0;
      BattleMeasure::BattleTime::Tick xxxStarted = 0;
      const BattleMeasure::BattleTime::Tick startXX = BattleMeasure::minutes(15);
      const BattleMeasure::BattleTime::Tick startXXX = BattleMeasure::minutes(20);
      for(BattleUnitIter iter(cp); !iter.isFinished(); iter.next())
      {
         if(iter.cp()->getRank().sameRank(Rank_Corps) ||
            (iter.cp()->getRank().sameRank(Rank_Division) && (iter.cp()->parent() == NoBattleCP || iter.cp()->parent()->getRank().isHigher(Rank_Corps))))
         {
            HexCord hex = startHex;
            int nDiv = 0;
            for(BattleUnitIter xxIter(iter.cp()); !xxIter.isFinished(); xxIter.next())
            {
               if(xxIter.cp()->getRank().sameRank(Rank_Division))
               {
                  BattleMeasure::BattleTime::Tick startIn = (startXX * (nDiv / 3)) + xxxStarted;//(startXXX * nCorp);
                  placeUnit(xxIter.cp(), hex, face, startIn);
                  BatUnitUtils::InitStrengthPoints(xxIter.cp(), d_batdata->ob());
                  if((nDiv + 1) % 3 == 0)
                     hex = startHex;
                  else
                     calcNextHex(fFace, cp->entryPoint(), hex);

                  nDiv++;
               }
            }

            if(iter.cp()->getRank().sameRank(Rank_Corps))
            {
               placeUnit(iter.cp(), startHex, face, xxxStarted);
               BatUnitUtils::InitCommandPositions(iter.cp(), d_batdata->ob());
               xxxStarted += (startXX * (nDiv / 3)) + startXXX;
            }

            nCorp++;
         }

         if(iter.cp()->getRank().isHigher(Rank_Corps))
         {
            placeUnit(iter.cp(), startHex, face, 0);
            BatUnitUtils::InitCommandPositions(iter.cp(), d_batdata->ob());
         }

         iter.cp()->offMap(True);
      }
   }
}

void B_ReinforceImp::processCommandHistorical(const RefBattleCP& cp, HexCord& startHex)
{
   // don't activate if parent is not yet active
   if(cp->parent() != NoBattleCP &&
      !cp->parent()->active())
   {
      return;
   }

   bool parentOnMap = (cp->getRank().sameRank(Rank_Division) && cp->parent() != NoBattleCP && cp->parent()->active());

#ifdef DEBUG
   rLog.printf("%s has arrived as Historical reinforcement",   cp->getName());
#endif

   BobUtility::initUnitTypeFlags(d_batdata->ob(), cp);

   HexCord sideLeft;
   HexCord sideRight;
   d_batdata->getSideLeftRight(cp->getSide(), sideLeft, sideRight);
   HexPosition::Facing fFace = frontFacing(sideLeft, sideRight);
   HexPosition::Facing face = reinforcementFacing(startHex, fFace);

   int nCorp = 0;
   BattleMeasure::BattleTime::Tick xxxStarted = 0;
   const BattleMeasure::BattleTime::Tick startXX = BattleMeasure::minutes(15);
   const BattleMeasure::BattleTime::Tick startXXX = BattleMeasure::minutes(20);

//   for(BattleUnitIter iter(cp); !iter.isFinished(); iter.next())
   for(TBattleUnitIter<BUIV_All> iter(cp, BUIV_All());
       !iter.isFinished();
       iter.next())
   {
      if(parentOnMap ||
         iter.cp()->getRank().sameRank(Rank_Corps) ||
         (iter.cp()->getRank().sameRank(Rank_Division) && (iter.cp()->parent() == NoBattleCP || iter.cp()->parent()->getRank().isHigher(Rank_Corps))))
      {
         HexCord hex = startHex;
         int nDiv = 0;
         for(TBattleUnitIter<BUIV_All> xxIter(iter.cp(), BUIV_All()); !xxIter.isFinished(); xxIter.next())
         {
            if(xxIter.cp()->getRank().sameRank(Rank_Division))
            {
               BattleMeasure::BattleTime::Tick startIn = (startXX * (nDiv / 3)) + xxxStarted;//(startXXX * nCorp);
               placeUnit(xxIter.cp(), hex, face, startIn);
               BatUnitUtils::InitStrengthPoints(xxIter.cp(), d_batdata->ob());
               if((nDiv + 1) % 3 == 0)
                  hex = startHex;
               else
                  calcNextHex(startHex, hex);

               nDiv++;
            }
         }

         if(iter.cp()->getRank().sameRank(Rank_Corps))
         {
            placeUnit(iter.cp(), startHex, face, xxxStarted);
            BatUnitUtils::InitCommandPositions(iter.cp(), d_batdata->ob());
            xxxStarted += (startXX * (nDiv / 3)) + startXXX;
         }

         nCorp++;
      }

      if(iter.cp()->getRank().isHigher(Rank_Corps))
      {
         placeUnit(iter.cp(), startHex, face, 0);
         BatUnitUtils::InitCommandPositions(iter.cp(), d_batdata->ob());
      }

      iter.cp()->offMap(True);
   }
}

void B_ReinforceImp::processHistorical()
{
#if 0
   // a bodge for add current game time to reinforcement time
   // ugly, but...
   if(!d_bodged)
   {
      BattleData::WriteLock lock(d_batdata);
      B_ReinforceList& rList = d_batdata->ob()->reinforceList();
      SListIter<B_ReinforceItem> iter(&rList);
      while(++iter)
      {
         iter.current()->d_time += d_batdata->getTick();
      }

      d_bodged = True;
   }
#endif
   if(d_batdata->getTick() >= d_nextTick)
   {
      BattleData::WriteLock lock(d_batdata);
      B_ReinforceList& rList = d_batdata->ob()->reinforceList();
      SListIter<B_ReinforceItem> iter(&rList);
     BattleMeasure::BattleTime::Tick bat_ticks = d_batdata->getTick();


     /*
     Jim's change to test the time
     */
     {
        int days, hours, mins, secs;
        ticksToTime(bat_ticks, &days, &hours, &mins, &secs);
     }

      while(++iter)
      {
       B_ReinforceItem * item = iter.current();

       /*
       Jim's change to test the time
       */
       {
          int days, hours, mins, secs;
          ticksToTime(item->d_time, &days, &hours, &mins, &secs);
       }

         if(!item->d_cp->active() && bat_ticks >= item->d_time)
         {
            processCommandHistorical(item->d_cp, item->d_hex);
         }
      }

      // rewind and remove all active
      iter.rewind();
      while(++iter)
      {
         if(iter.current()->d_cp->active())
         {
            iter.remove();
         }
      }

      // check every 5 minutes
      d_nextTick = d_batdata->getTick() + BattleMeasure::minutes(5);
   }
}

void B_ReinforceImp::process()
{
#ifdef DEBUG
   rLog.printf("\n------ Processing BattleReinforcements");
   HexCord bLeft;
   HexCord mSize;
   d_batdata->getPlayingArea(&bLeft, &mSize);
   HexCord tRight(bLeft.x() + mSize.x(), bLeft.y() + mSize.y());
   rLog.printf("-------- Map:  Bottom Left(%d, %d),  Top Right(%d, %d)",
         static_cast<int>(bLeft.x()),
         static_cast<int>(bLeft.y()),
         static_cast<int>(tRight.x()),
         static_cast<int>(tRight.y()));
#endif

   // find any units that have just arrived
   for(BattleUnitIter iter(d_batdata->ob()); !iter.isFinished(); iter.next())
   {
      if(iter.cp()->entryPoint() != REP_None)
      {
         processCommand(iter.cp());
         iter.cp()->entryPoint(REP_None);
      }
   }
}

// --- Client access for B_Reinforce --------------------

B_ReinforceInt::B_ReinforceInt(BattleGameInterface* batgame) :
   d_imp(new B_ReinforceImp(batgame))
{
   ASSERT(d_imp);
}

B_ReinforceInt::~B_ReinforceInt()
{
   delete d_imp;
}

void B_ReinforceInt::process()
{
   d_imp->process();
}

void B_ReinforceInt::processHistorical()
{
   d_imp->processHistorical();
}

void B_ReinforceInt::newDay()
{
   d_imp->newDay();
}

}; // end namespace

/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.3  2002/11/24 13:17:48  greenius
 * Added Tim Carne's editor changes including InitEditUnitTypeFlags.
 *
 * Revision 1.2  2001/06/18 08:25:44  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
