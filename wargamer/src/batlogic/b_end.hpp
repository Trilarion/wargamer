/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
#ifndef B_END_HPP
#define B_END_HPP

#ifndef __cplusplus
#error b_end.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 * Battle End Detection
 *
 *----------------------------------------------------------------------
 */

#include "b_result.hpp"

class BattleData;

namespace B_Logic
{

bool checkBattleOver(const BattleData* batData, BattleFinish* results);
        // Checks if battle is over and fills in results
        // returns true if finished
}  // namespace B_Logic

#endif /* B_END_HPP */

/*
 *----------------------------------------------------------------------
 * $Log$
 *----------------------------------------------------------------------
 */
