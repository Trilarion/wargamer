# Microsoft Developer Studio Generated NMAKE File, Based on batlogic.dsp
!IF "$(CFG)" == ""
CFG=batlogic - Win32 Debug
!MESSAGE No configuration specified. Defaulting to batlogic - Win32 Debug.
!ENDIF 

!IF "$(CFG)" != "batlogic - Win32 Release" && "$(CFG)" != "batlogic - Win32 Debug"
!MESSAGE Invalid configuration "$(CFG)" specified.
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "batlogic.mak" CFG="batlogic - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "batlogic - Win32 Release" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE "batlogic - Win32 Debug" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE 
!ERROR An invalid configuration is specified.
!ENDIF 

!IF "$(OS)" == "Windows_NT"
NULL=
!ELSE 
NULL=nul
!ENDIF 

CPP=cl.exe
MTL=midl.exe
RSC=rc.exe

!IF  "$(CFG)" == "batlogic - Win32 Release"

OUTDIR=.\..\..\exe
INTDIR=.\o\release
# Begin Custom Macros
OutDir=.\..\..\exe
# End Custom Macros

!IF "$(RECURSE)" == "0" 

ALL : "$(OUTDIR)\batlogic.dll"

!ELSE 

ALL : "gamesup - Win32 Release" "batdata - Win32 Release" "system - Win32 Release" "ob - Win32 Release" "$(OUTDIR)\batlogic.dll"

!ENDIF 

!IF "$(RECURSE)" == "1" 
CLEAN :"ob - Win32 ReleaseCLEAN" "system - Win32 ReleaseCLEAN" "batdata - Win32 ReleaseCLEAN" "gamesup - Win32 ReleaseCLEAN" 
!ELSE 
CLEAN :
!ENDIF 
	-@erase "$(INTDIR)\b_combat.obj"
	-@erase "$(INTDIR)\b_detect.obj"
	-@erase "$(INTDIR)\b_end.obj"
	-@erase "$(INTDIR)\b_fow.obj"
	-@erase "$(INTDIR)\b_rction.obj"
	-@erase "$(INTDIR)\b_reinf.obj"
	-@erase "$(INTDIR)\batlogic.obj"
	-@erase "$(INTDIR)\bdeploy.obj"
	-@erase "$(INTDIR)\blog.obj"
	-@erase "$(INTDIR)\blosses.obj"
	-@erase "$(INTDIR)\bmanuevr.obj"
	-@erase "$(INTDIR)\bmove.obj"
	-@erase "$(INTDIR)\brecover.obj"
	-@erase "$(INTDIR)\broute.obj"
	-@erase "$(INTDIR)\bspform.obj"
	-@erase "$(INTDIR)\bu_move.obj"
	-@erase "$(INTDIR)\bu_ord.obj"
	-@erase "$(INTDIR)\bunits.obj"
	-@erase "$(INTDIR)\cmbtutil.obj"
	-@erase "$(INTDIR)\moveutil.obj"
	-@erase "$(INTDIR)\overnight.obj"
	-@erase "$(INTDIR)\randomkills.obj"
	-@erase "$(INTDIR)\reorglog.obj"
	-@erase "$(INTDIR)\vc60.idb"
	-@erase "$(INTDIR)\vc60.pdb"
	-@erase "$(OUTDIR)\batlogic.dll"
	-@erase "$(OUTDIR)\batlogic.exp"
	-@erase "$(OUTDIR)\batlogic.lib"
	-@erase "$(OUTDIR)\batlogic.pdb"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

"$(INTDIR)" :
    if not exist "$(INTDIR)/$(NULL)" mkdir "$(INTDIR)"

CPP_PROJ=/nologo /MD /W3 /Gi /GR /GX /Zi /O2 /I "..\h" /I "..\..\res" /I "..\system" /I "..\gamesup" /I "..\ob" /I "..\batdata" /D "NDEBUG" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "EXPORT_BATLOGIC_DLL" /Fp"$(INTDIR)\batlogic.pch" /YX"stdinc.hpp" /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /c 
MTL_PROJ=/nologo /D "NDEBUG" /mktyplib203 /win32 
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\batlogic.bsc" 
BSC32_SBRS= \
	
LINK32=link.exe
LINK32_FLAGS=/nologo /dll /incremental:no /pdb:"$(OUTDIR)\batlogic.pdb" /debug /machine:I386 /out:"$(OUTDIR)\batlogic.dll" /implib:"$(OUTDIR)\batlogic.lib" 
LINK32_OBJS= \
	"$(INTDIR)\b_combat.obj" \
	"$(INTDIR)\b_detect.obj" \
	"$(INTDIR)\b_end.obj" \
	"$(INTDIR)\b_fow.obj" \
	"$(INTDIR)\b_rction.obj" \
	"$(INTDIR)\b_reinf.obj" \
	"$(INTDIR)\batlogic.obj" \
	"$(INTDIR)\bdeploy.obj" \
	"$(INTDIR)\blog.obj" \
	"$(INTDIR)\blosses.obj" \
	"$(INTDIR)\bmanuevr.obj" \
	"$(INTDIR)\bmove.obj" \
	"$(INTDIR)\brecover.obj" \
	"$(INTDIR)\broute.obj" \
	"$(INTDIR)\bspform.obj" \
	"$(INTDIR)\bu_move.obj" \
	"$(INTDIR)\bu_ord.obj" \
	"$(INTDIR)\bunits.obj" \
	"$(INTDIR)\cmbtutil.obj" \
	"$(INTDIR)\moveutil.obj" \
	"$(INTDIR)\overnight.obj" \
	"$(INTDIR)\randomkills.obj" \
	"$(INTDIR)\reorglog.obj" \
	"$(OUTDIR)\ob.lib" \
	"$(OUTDIR)\system.lib" \
	"$(OUTDIR)\batdata.lib" \
	"$(OUTDIR)\gamesup.lib"

"$(OUTDIR)\batlogic.dll" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

!ELSEIF  "$(CFG)" == "batlogic - Win32 Debug"

OUTDIR=.\..\..\exe
INTDIR=.\o\debug
# Begin Custom Macros
OutDir=.\..\..\exe
# End Custom Macros

!IF "$(RECURSE)" == "0" 

ALL : "$(OUTDIR)\batlogicDB.dll"

!ELSE 

ALL : "gamesup - Win32 Debug" "batdata - Win32 Debug" "system - Win32 Debug" "ob - Win32 Debug" "$(OUTDIR)\batlogicDB.dll"

!ENDIF 

!IF "$(RECURSE)" == "1" 
CLEAN :"ob - Win32 DebugCLEAN" "system - Win32 DebugCLEAN" "batdata - Win32 DebugCLEAN" "gamesup - Win32 DebugCLEAN" 
!ELSE 
CLEAN :
!ENDIF 
	-@erase "$(INTDIR)\b_combat.obj"
	-@erase "$(INTDIR)\b_detect.obj"
	-@erase "$(INTDIR)\b_end.obj"
	-@erase "$(INTDIR)\b_fow.obj"
	-@erase "$(INTDIR)\b_rction.obj"
	-@erase "$(INTDIR)\b_reinf.obj"
	-@erase "$(INTDIR)\batlogic.obj"
	-@erase "$(INTDIR)\bdeploy.obj"
	-@erase "$(INTDIR)\blog.obj"
	-@erase "$(INTDIR)\blosses.obj"
	-@erase "$(INTDIR)\bmanuevr.obj"
	-@erase "$(INTDIR)\bmove.obj"
	-@erase "$(INTDIR)\brecover.obj"
	-@erase "$(INTDIR)\broute.obj"
	-@erase "$(INTDIR)\bspform.obj"
	-@erase "$(INTDIR)\bu_move.obj"
	-@erase "$(INTDIR)\bu_ord.obj"
	-@erase "$(INTDIR)\bunits.obj"
	-@erase "$(INTDIR)\cmbtutil.obj"
	-@erase "$(INTDIR)\moveutil.obj"
	-@erase "$(INTDIR)\overnight.obj"
	-@erase "$(INTDIR)\randomkills.obj"
	-@erase "$(INTDIR)\reorglog.obj"
	-@erase "$(INTDIR)\vc60.idb"
	-@erase "$(INTDIR)\vc60.pdb"
	-@erase "$(OUTDIR)\batlogicDB.dll"
	-@erase "$(OUTDIR)\batlogicDB.exp"
	-@erase "$(OUTDIR)\batlogicDB.ilk"
	-@erase "$(OUTDIR)\batlogicDB.lib"
	-@erase "$(OUTDIR)\batlogicDB.pdb"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

"$(INTDIR)" :
    if not exist "$(INTDIR)/$(NULL)" mkdir "$(INTDIR)"

CPP_PROJ=/nologo /MDd /W3 /Gm /Gi /GR /GX /ZI /Od /I "..\h" /I "..\..\res" /I "..\system" /I "..\gamesup" /I "..\ob" /I "..\batdata" /D "_DEBUG" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "EXPORT_BATLOGIC_DLL" /Fp"$(INTDIR)\batlogic.pch" /YX"stdinc.hpp" /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /GZ /c 
MTL_PROJ=/nologo /D "_DEBUG" /mktyplib203 /win32 
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\batlogic.bsc" 
BSC32_SBRS= \
	
LINK32=link.exe
LINK32_FLAGS=/nologo /dll /incremental:yes /pdb:"$(OUTDIR)\batlogicDB.pdb" /debug /machine:I386 /out:"$(OUTDIR)\batlogicDB.dll" /implib:"$(OUTDIR)\batlogicDB.lib" /pdbtype:sept 
LINK32_OBJS= \
	"$(INTDIR)\b_combat.obj" \
	"$(INTDIR)\b_detect.obj" \
	"$(INTDIR)\b_end.obj" \
	"$(INTDIR)\b_fow.obj" \
	"$(INTDIR)\b_rction.obj" \
	"$(INTDIR)\b_reinf.obj" \
	"$(INTDIR)\batlogic.obj" \
	"$(INTDIR)\bdeploy.obj" \
	"$(INTDIR)\blog.obj" \
	"$(INTDIR)\blosses.obj" \
	"$(INTDIR)\bmanuevr.obj" \
	"$(INTDIR)\bmove.obj" \
	"$(INTDIR)\brecover.obj" \
	"$(INTDIR)\broute.obj" \
	"$(INTDIR)\bspform.obj" \
	"$(INTDIR)\bu_move.obj" \
	"$(INTDIR)\bu_ord.obj" \
	"$(INTDIR)\bunits.obj" \
	"$(INTDIR)\cmbtutil.obj" \
	"$(INTDIR)\moveutil.obj" \
	"$(INTDIR)\overnight.obj" \
	"$(INTDIR)\randomkills.obj" \
	"$(INTDIR)\reorglog.obj" \
	"$(OUTDIR)\obDB.lib" \
	"$(OUTDIR)\systemDB.lib" \
	"$(OUTDIR)\batdataDB.lib" \
	"$(OUTDIR)\gamesupDB.lib"

"$(OUTDIR)\batlogicDB.dll" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

!ENDIF 

.c{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.c{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<


!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("batlogic.dep")
!INCLUDE "batlogic.dep"
!ELSE 
!MESSAGE Warning: cannot find "batlogic.dep"
!ENDIF 
!ENDIF 


!IF "$(CFG)" == "batlogic - Win32 Release" || "$(CFG)" == "batlogic - Win32 Debug"
SOURCE=.\b_combat.cpp

"$(INTDIR)\b_combat.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\b_detect.cpp

"$(INTDIR)\b_detect.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\b_end.cpp

"$(INTDIR)\b_end.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\b_fow.cpp

"$(INTDIR)\b_fow.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\b_rction.cpp

"$(INTDIR)\b_rction.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\b_reinf.cpp

"$(INTDIR)\b_reinf.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\batlogic.cpp

"$(INTDIR)\batlogic.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\bdeploy.cpp

"$(INTDIR)\bdeploy.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\blog.cpp

"$(INTDIR)\blog.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\blosses.cpp

"$(INTDIR)\blosses.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\bmanuevr.cpp

"$(INTDIR)\bmanuevr.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\bmove.cpp

"$(INTDIR)\bmove.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\brecover.cpp

"$(INTDIR)\brecover.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\broute.cpp

"$(INTDIR)\broute.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\bspform.cpp

"$(INTDIR)\bspform.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\bu_move.cpp

"$(INTDIR)\bu_move.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\bu_ord.cpp

"$(INTDIR)\bu_ord.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\bunits.cpp

"$(INTDIR)\bunits.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\cmbtutil.cpp

"$(INTDIR)\cmbtutil.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\moveutil.cpp

"$(INTDIR)\moveutil.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\overnight.cpp

"$(INTDIR)\overnight.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\randomkills.cpp

"$(INTDIR)\randomkills.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\reorglog.cpp

"$(INTDIR)\reorglog.obj" : $(SOURCE) "$(INTDIR)"


!IF  "$(CFG)" == "batlogic - Win32 Release"

"ob - Win32 Release" : 
   cd "\src\sf\wargamer\SRC\OB"
   $(MAKE) /$(MAKEFLAGS) /F .\ob.mak CFG="ob - Win32 Release" 
   cd "..\batlogic"

"ob - Win32 ReleaseCLEAN" : 
   cd "\src\sf\wargamer\SRC\OB"
   $(MAKE) /$(MAKEFLAGS) /F .\ob.mak CFG="ob - Win32 Release" RECURSE=1 CLEAN 
   cd "..\batlogic"

!ELSEIF  "$(CFG)" == "batlogic - Win32 Debug"

"ob - Win32 Debug" : 
   cd "\src\sf\wargamer\SRC\OB"
   $(MAKE) /$(MAKEFLAGS) /F .\ob.mak CFG="ob - Win32 Debug" 
   cd "..\batlogic"

"ob - Win32 DebugCLEAN" : 
   cd "\src\sf\wargamer\SRC\OB"
   $(MAKE) /$(MAKEFLAGS) /F .\ob.mak CFG="ob - Win32 Debug" RECURSE=1 CLEAN 
   cd "..\batlogic"

!ENDIF 

!IF  "$(CFG)" == "batlogic - Win32 Release"

"system - Win32 Release" : 
   cd "\src\sf\wargamer\SRC\SYSTEM"
   $(MAKE) /$(MAKEFLAGS) /F .\system.mak CFG="system - Win32 Release" 
   cd "..\batlogic"

"system - Win32 ReleaseCLEAN" : 
   cd "\src\sf\wargamer\SRC\SYSTEM"
   $(MAKE) /$(MAKEFLAGS) /F .\system.mak CFG="system - Win32 Release" RECURSE=1 CLEAN 
   cd "..\batlogic"

!ELSEIF  "$(CFG)" == "batlogic - Win32 Debug"

"system - Win32 Debug" : 
   cd "\src\sf\wargamer\SRC\SYSTEM"
   $(MAKE) /$(MAKEFLAGS) /F .\system.mak CFG="system - Win32 Debug" 
   cd "..\batlogic"

"system - Win32 DebugCLEAN" : 
   cd "\src\sf\wargamer\SRC\SYSTEM"
   $(MAKE) /$(MAKEFLAGS) /F .\system.mak CFG="system - Win32 Debug" RECURSE=1 CLEAN 
   cd "..\batlogic"

!ENDIF 

!IF  "$(CFG)" == "batlogic - Win32 Release"

"batdata - Win32 Release" : 
   cd "\src\sf\wargamer\SRC\batdata"
   $(MAKE) /$(MAKEFLAGS) /F .\batdata.mak CFG="batdata - Win32 Release" 
   cd "..\batlogic"

"batdata - Win32 ReleaseCLEAN" : 
   cd "\src\sf\wargamer\SRC\batdata"
   $(MAKE) /$(MAKEFLAGS) /F .\batdata.mak CFG="batdata - Win32 Release" RECURSE=1 CLEAN 
   cd "..\batlogic"

!ELSEIF  "$(CFG)" == "batlogic - Win32 Debug"

"batdata - Win32 Debug" : 
   cd "\src\sf\wargamer\SRC\batdata"
   $(MAKE) /$(MAKEFLAGS) /F .\batdata.mak CFG="batdata - Win32 Debug" 
   cd "..\batlogic"

"batdata - Win32 DebugCLEAN" : 
   cd "\src\sf\wargamer\SRC\batdata"
   $(MAKE) /$(MAKEFLAGS) /F .\batdata.mak CFG="batdata - Win32 Debug" RECURSE=1 CLEAN 
   cd "..\batlogic"

!ENDIF 

!IF  "$(CFG)" == "batlogic - Win32 Release"

"gamesup - Win32 Release" : 
   cd "\src\sf\wargamer\SRC\GAMESUP"
   $(MAKE) /$(MAKEFLAGS) /F .\gamesup.mak CFG="gamesup - Win32 Release" 
   cd "..\batlogic"

"gamesup - Win32 ReleaseCLEAN" : 
   cd "\src\sf\wargamer\SRC\GAMESUP"
   $(MAKE) /$(MAKEFLAGS) /F .\gamesup.mak CFG="gamesup - Win32 Release" RECURSE=1 CLEAN 
   cd "..\batlogic"

!ELSEIF  "$(CFG)" == "batlogic - Win32 Debug"

"gamesup - Win32 Debug" : 
   cd "\src\sf\wargamer\SRC\GAMESUP"
   $(MAKE) /$(MAKEFLAGS) /F .\gamesup.mak CFG="gamesup - Win32 Debug" 
   cd "..\batlogic"

"gamesup - Win32 DebugCLEAN" : 
   cd "\src\sf\wargamer\SRC\GAMESUP"
   $(MAKE) /$(MAKEFLAGS) /F .\gamesup.mak CFG="gamesup - Win32 Debug" RECURSE=1 CLEAN 
   cd "..\batlogic"

!ENDIF 


!ENDIF 

