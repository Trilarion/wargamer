/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef BSPFORM_HPP
#define BSPFORM_HPP

#include "batdata.hpp"
#include "batunit.hpp"
#include "bobdef.hpp"

namespace BattleMeasure { class HexCord; };
using namespace BOB_Definitions;
//class RefBattleCP;

namespace B_Logic
{

class SPFormation_Imp;

class SPFormation_Int {
    SPFormation_Imp* d_formations;
  public:
    SPFormation_Int(RPBattleData batData);
    ~SPFormation_Int();

    bool run(const RefBattleCP& cp);
    bool runFacingChange(const RefBattleSP& sp);
    bool startChange(const RefBattleCP& cp);
    void startFacingChange(const RefBattleSP& sp);
};

};

#endif
