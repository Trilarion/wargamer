/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
#include "stdinc.hpp"
#include "b_detect.hpp"
#include "batdata.hpp"
#include "batarmy.hpp"
#include "b_tables.hpp"
#include "sync.hpp"
#include "batcord.hpp"
#include "hexmap.hpp"
#include "hexdata.hpp"
#include "bobiter.hpp"
#include "batlist.hpp"
#include "bobutil.hpp"
#include "wg_rand.hpp"
#include "scenario.hpp"
#include "cmbtutil.hpp"
#include "batctrl.hpp"
#include "batmsg.hpp"
#include "moveutil.hpp"
#include "b_los.hpp"
#ifdef DEBUG
#include "clog.hpp"
static LogFile dLog("BDetectEnemy.log");
#endif


namespace B_Logic
{

enum LType {
        Inf,
        Cav,
        FootArt,
         HvyArt,
             HorseArt,
         Type_HowMany
};

inline LType getType(const CRefBattleCP& cp)
{
   return (cp->generic()->isInfantry()) ? Inf :
           (cp->generic()->isCavalry()) ? Cav :
           (cp->generic()->isHorseArtillery()) ? HorseArt :
           (cp->generic()->isHeavyArtillery()) ? HvyArt : FootArt;
}

enum {
         Hexes2,
         Hexes3,
             Hexes4,
             Hexes7,
             Hexes8,
         Hexes_HowMany
};

class B_DetectImp {
    BattleGameInterface* d_batgame;
    PBattleData d_batData;
    B_LineOfSight d_los;
   public:
    B_DetectImp(BattleGameInterface* batgame) :
       d_batgame(batgame),
         d_batData(batgame->battleData()),// {}
            d_los(d_batData) {}
      ~B_DetectImp() {}

      void doEnemyNear(const RefBattleCP& cp);
      bool enemyInRange(const RefBattleCP& cp);

   private:
//  int  getRange(const RefBattleCP& cp);
      void plotArtillery(const RefBattleCP& cp);
      bool willDisorderEnemy(const RefBattleCP& cp);
      void setShootingGuns(const RefBattleCP& cp, const RefBattleCP& targetCP);

      enum WhichWay { Left, Right, Front, Rear };
      void searchFront(const RefBattleCP& cp, HexPosition::Facing facing, int cRange, BattleList& list, BattleMeasure::HexCord& hex, WhichWay w);
      void searchFlank(const RefBattleCP& cp, HexPosition::Facing facing, int cRange, BattleList& list, BattleMeasure::HexCord& hex, WhichWay w);
      void searchFrontFlank(const RefBattleCP& cp, HexPosition::Facing facing, int cRange, BattleList& list, BattleMeasure::HexCord& hex, WhichWay wwFl, WhichWay wwFr);
//    void modifyFireValue(const RefBattleCP& cp, int& fireValue);
};
#if 0
void B_DetectImp::modifyFireValue(const RefBattleCP& cp, int& fireValue)
{
//    if(fireValue == 0)
//       return;

      /*
       * modify fire value
       */

      // if in march, closed, square, or column (fire value X .5)
      if(cp->spFormation() != SP_LineFormation)
         fireValue *= .5;

#ifdef DEBUG
      dLog.printf("FireValue after SP not in line modifier = %d", fireValue);
#endif
      // is infantry and is changing divisional deployment
      if(cp->formation() != cp->getCurrentOrder().divFormation())
      {
         fireValue *= .5;
      }
      // TODO: for moving at over half speed
      else if(!cp->holding() || !cp->allSPAtCenter())
      {
         fireValue *= .5;
      }

#ifdef DEBUG
      dLog.printf("FireValue after moving modifier = %d", fireValue);
#endif
      // modify for disordered unit
      // disorder level 0 (X0.5)
      static UBYTE s_dTable[BattleCP::Disorder::MaxValue + 1] = {
          5, 7, 9, 10
      };

      const int c_base = 10;
      fireValue = (fireValue * s_dTable[cp->disorder()]) / c_base;
#ifdef DEBUG
      dLog.printf("FireValue after disorder modifier = %d", fireValue);
#endif

      // TODO: modify artillery firing on unlimbered arty
      // TODO: infantry firing in heavy rain
      // TODO: Artillery firing at 3 hexes or more in heavy mud
      if(cp->nArtillery() > 0 &&
          cp->targetList().closeRange() > (3 * BattleMeasure::XYardsPerHex))
      {
      }

      // X .25 if firing unit is shaken
      if(bi->d_cp->shaken())
         fireValue *= .25;
#ifdef DEBUG
      dLog.printf("FireValue after shaken modifier = %d", fireValue);
#endif
      // TODO: infantry firing FDF vs charging cav

      // Taret is retreating or routing
      if( (bi->d_cp->routing()) || (bi->d_cp->movingBackwards()) )
         fireValue *= .5;

      // Unit currently crossing river or in Marsh
      if(Combat_Util::crossingRiver(d_batData, cp) || Combat_Util::crossingStream(d_batData, cp))
         fireValue *= .5;

      if(Combat_Util::terrainType(d_batData, cp) == GT_Marsh)
         fireValue *= .5;

      // target is in 'March' (Not sure if this is XX or SP)
      if(bi->d_cp->spFormation() == SP_MarchFormation)
         fireValue *= 2;
#ifdef DEBUG
      dLog.printf("FireValue after enemy in SP March modifier = %d", fireValue);
#endif
}
#endif
bool B_DetectImp::enemyInRange(const RefBattleCP& cp)
{
   // Unchecked
   cp->targetList().resetItems();
   bool result = False;
   if(cp->getRank().sameRank(Rank_Division))
   {
      // get range in hexes sp can fire
      int actualRange = Combat_Util::getRange(d_batData, cp);
      int cRange = 10; //maximum(4, actualRange / BattleMeasure::XYardsPerHex);
      if(cRange > 0)
      {
//      enum { Front, Right, Rear, Left };
         BattleList llist;

         // go through each outer sp and see if cp is within nHexes
         //for(int r = 0; r < cp->wantRows(); r++)
         for(int r = 0; r < cp->rows(); r++)
         {
             //int cols = cp->columns();//(cp->formation() <= cp->nextFormation()) ? cp->columns() : cp->wantColumns();
             for(int c = 0; c < cp->columns(); c++)
             {
               //if(r == 0 || r == cp->wantRows() - 1 || c == 0 || c == cols - 1)
               if(r == 0 || r == cp->rows() - 1 || c == 0 || c == cp->columns() - 1)
               {
                  DeployItem* di = cp->currentDeployItem(c, r);
                  ASSERT(di);
                  if(di && di->active())
                  {
                      // search to the front
                      if(r == 0 || r == cp->rows() - 1)
                      {
                        HexCord hex = di->d_sp->hex();
                        searchFront(cp, cp->facing(), cRange, llist, hex, (r == 0) ? Front : Rear);
                      }

                      // search to either flank
                      if(c == 0 || c == cp->columns() - 1)
                      {
                           HexCord hex = di->d_sp->hex();
                           searchFlank(cp, cp->facing(), cRange, llist, hex, (c == 0) ? Left : Right);
                      }

                      // search to left/right front/rear
               if( (r == 0 || r == cp->rows() - 1) &&
                     (c == 0 || c == cp->columns() - 1) )
               {
                  WhichWay wwFr = (r == 0) ? Front : Rear;
                  WhichWay wwFl = (c == 0) ? Left : Right;
                  HexCord hex = di->d_sp->hex();
                  searchFrontFlank(cp, cp->facing(), cRange, llist, hex, wwFl, wwFr);
               }

               bool gotTarget = False;
               SListIterR<BattleItem> biter(&llist);
               while(++biter)
               {
                  if(biter.current()->d_position == BattleItem::Front)//closeRange != UWORD_MAX)// || closeRangeFlank != UWORD_MAX || claseRangeRear != UWORD_MAX)
                  {
                     // add only front line SP to fire list
                     if(r == 0)// && !gotTarget)
                     {
                        UWORD fireValue = 0;
                        int closeRange = biter.current()->d_range;

                        // If artillery and we have a player-desginated bombardment target
                        // add only if enemy cp is that target
                        bool shouldAdd = (!gotTarget && closeRange <= actualRange &&
                            (cp->targetCP() == NoBattleCP || !cp->generic()->isArtillery() || cp->targetCP() == biter.current()->d_cp) );

                        if(shouldAdd &&
                            !inATrafficJam(d_batData, cp, di->d_sp->hex()))
                        {
                           gotTarget = True;

                           // add to list
                           UBYTE nHexes = (closeRange <= 440)  ? Hexes2 :
                                                 (closeRange <= 660)  ? Hexes3 :
                                                 (closeRange <= 880)  ? Hexes4 :
                                                 (closeRange <= 1540) ? Hexes7 : Hexes8;

                           const Table2D<UWORD>& fpTable = BattleTables::fireCombatPoints();

//                         nTargets = maximum(1, nTargets);
                           fireValue = fpTable.getValue(getType(cp), nHexes); // nTargets;
                        }

                        if(fireValue > 0 && di->d_sp->strength(100) < 100)
                           fireValue = static_cast<UWORD>(MulDiv(fireValue, di->d_sp->strength(100), 100));

                        bool shootingGuns = False;//((cp->generic()->isArtillery() && cp->spFormation() == SP_UnlimberedFormation) ||
                                                    //cp->holding()) && fireValue > 0 && cp->ammoSupply() > 0 && cp->nArtillery() > 0;
                        bool shootingMuskets = False;
                        if(cp->generic()->isArtillery())
                        {
                           if(di->d_sp->formation() == SP_UnlimberedFormation &&  fireValue > 0 && cp->ammoSupply() > 0)
                              shootingGuns = True;
                           else
                              fireValue = 0;
                        }
                        else
                           shootingMuskets = (fireValue > 0);

                        if(!cp->targetList().addFireValue(biter.current()->d_cp, closeRange, fireValue, BattleItem::Front, shouldAdd))
                        {
#ifdef DEBUG
                           dLog.printf("Testing %s %s for nearby enemy",
                              scenario->getSideName(cp->getSide()), cp->getName());

                           dLog.printf("Enemy sighted (%s %s) %d yards away to the Front",
                              scenario->getSideName(biter.current()->d_cp->getSide()),
                              biter.current()->d_cp->getName(),
                              static_cast<int>(closeRange));
#endif
                           // Send player message telling him we have spotted a new enemy
                           {
                              // this side
                              BattleMessageInfo msg(cp->getSide(), BattleMessageID::BMSG_EnemySighted, cp);
                              msg.target(biter.current()->d_cp);
                              d_batgame->sendPlayerMessage(msg);
                           }
                        }

                        if(shootingMuskets)
                        {
                           if(!biter.current()->d_cp->takingMusketHits())
                           {
#ifdef DEBUG
                              dLog.printf("%s is taking musket hits", biter.current()->d_cp->getName());
#endif
                              biter.current()->d_cp->takingMusketHits(True);
                           }

                           if(!cp->shootingMuskets())
                           {
#ifdef DEBUG
                              dLog.printf("%s is shooting muskets", cp->getName());
#endif
                              // send player messages
                              {
                                 // this side
                                 BattleMessageInfo msg(cp->getSide(), BattleMessageID::BMSG_EngagingWithMuskets, cp);
                                 msg.target(biter.current()->d_cp);
                                 d_batgame->sendPlayerMessage(msg);
                              }
                              {
                                 // other guys
                                 BattleMessageInfo msg(biter.current()->d_cp->getSide(), BattleMessageID::BMSG_TakingMusketFire, biter.current()->d_cp);
                                 msg.target(cp);
                                 d_batgame->sendPlayerMessage(msg);
                              }

                              cp->shootingMuskets(True);
                           }
                        }
                        if(shootingGuns)
                        {
                           setShootingGuns(cp, biter.current()->d_cp);

                        }

                        if(!di->d_sp->shootingMuskets())
                           di->d_sp->shootingMuskets(shootingMuskets);

                        if(!di->d_sp->shootingGuns())
                           di->d_sp->shootingGuns(shootingGuns);

#ifdef DEBUG
                        if(shouldAdd)
                        {
                           static const char* s_mtext[2] = {
                              "is not shooting muskets",
                              "is shooting muskets"
                           };

                           static const char* s_gtext[2] = {
                              "is not shooting Artillery",
                              "is shooting Artillery"
                           };

                           dLog.printf("SP %ld(%s)(r %d, c %d) fireValue %d -- %s, and %s",
                              reinterpret_cast<LONG>(di->d_sp),
                              d_batData->ob()->spName(di->d_sp),
                              r, c,
                              (int)fireValue,
                              s_mtext[di->d_sp->shootingMuskets()],
                              s_gtext[di->d_sp->shootingGuns()]);

                           dLog.printf("Targeting %s", biter.current()->d_cp->getName());
                        }

#endif
                     }
                  }

                  else
                  {
                     if(!cp->targetList().addFireValue(biter.current()->d_cp, biter.current()->d_range, 0, biter.current()->d_position))
                     {
#ifdef DEBUG
                        dLog.printf("Testing %s %s for nearby enemy",
                           scenario->getSideName(cp->getSide()), cp->getName());

                        const BattleItem* bi = biter.current();
                        dLog.printf("Enemy sighted (%s %s) %d yards away on %s",
                           scenario->getSideName(bi->d_cp->getSide()),
                           bi->d_cp->getName(),
                           static_cast<int>(bi->d_range),
                           BattleItem::positionText(bi->d_position));
#endif
                        // Send player message telling him we have spotted a new enemy
                        {
                           // this side
                           BattleMessageInfo msg(cp->getSide(), BattleMessageID::BMSG_EnemySighted, cp);
                           msg.target(biter.current()->d_cp);
                           d_batgame->sendPlayerMessage(msg);
                        }
                     }
                  }
               }
            }
          }
         }
      }

      // if we have artillery, and we're not moving then plot artillery fire
      if(cp->holding() &&
         !cp->generic()->isArtillery() &&
         cp->nArtillery() > 0 &&
         cp->targetList().entries() > 0)// &&
//       cp->targetList().closeRange() <= getRange(cp))
      {
//         ASSERT(!cp->generic()->isArtillery());
         plotArtillery(cp);
      }

      result = (cp->targetList().entries() > 0 &&
                   (cp->targetList().closeRangeFront()  <= actualRange || cp->targetList().closeRangeFront() <= (2 * BattleMeasure::XYardsPerHex)));
    }
   }
   else
   {
      BattleList llist;
      HexCord hex = cp->hex();

      searchFront(cp, cp->rFacing(), 4, llist, hex, Front);
      searchFront(cp, cp->rFacing(), 4, llist, hex, Rear);
      searchFlank(cp, cp->rFacing(), 4, llist, hex, Left);
      searchFlank(cp, cp->rFacing(), 4, llist, hex, Right);
      searchFrontFlank(cp, cp->rFacing(), 4, llist, hex, Left, Front);
      searchFrontFlank(cp, cp->rFacing(), 4, llist, hex, Right, Front);
      searchFrontFlank(cp, cp->rFacing(), 4, llist, hex, Left, Rear);
      searchFrontFlank(cp, cp->rFacing(), 4, llist, hex, Right, Rear);

      BattleList& bl = cp->targetList();
      SListIterR<BattleItem> iter(&llist);
      while(++iter)
      {
         bl.addFireValue(iter.current()->d_cp, iter.current()->d_range, 0, iter.current()->d_position);
      }

      result = (bl.entries() > 0);
   }
   // End

   cp->targetList().cleanUpList();
   cp->targetList().moveUpCloseUnit();

#ifdef DEBUG
   BattleListIterR bliter(&cp->targetList());
   if(cp->targetList().entries() > 0)
   {
      dLog.printf("\nTarget List for %s %s", scenario->getSideName(cp->getSide()), cp->getName());
      dLog.printf("---------------------------------");
      while(++bliter)
      {
         const BattleItem* bi = bliter.current();
         dLog.printf("%s %s at %d yards, to the %s",
                  scenario->getSideName(bi->d_cp->getSide()),
                  bi->d_cp->getName(),
                  static_cast<int>(bi->d_range),
                  BattleItem::positionText(bi->d_position));
      }
      dLog.printf("End of Target List-----------------------\n");
   }
#endif

   if(cp->targetList().entries() == 0)
       cp->nearEnemyMode(BattleCP::NE_NotNear);

   return result;
}

void B_DetectImp::setShootingGuns(const RefBattleCP& cp, const RefBattleCP& targetCP)
{
   if(!targetCP->takingGunHits())
   {
#ifdef DEBUG
      dLog.printf("%s is taking gun hits", targetCP->getName());
#endif
      targetCP->takingGunHits(True);
   }

   if(!cp->shootingGuns())
   {
#ifdef DEBUG
      dLog.printf("%s is shooting guns", cp->getName());
#endif
      // send player messages
      {
         // this side
         BattleMessageInfo msg(cp->getSide(), BattleMessageID::BMSG_EngagingWithArtillery, cp);
         msg.target(targetCP);
         d_batgame->sendPlayerMessage(msg);
      }
      {
          // other guys
         BattleMessageInfo msg(targetCP->getSide(), BattleMessageID::BMSG_TakingArtilleryFire, targetCP);
         msg.target(cp);
         d_batgame->sendPlayerMessage(msg);
      }

      cp->shootingGuns(True);
   }
}

void B_DetectImp::plotArtillery(const RefBattleCP& cp)
{
    // return if we have no ammo
    if(cp->ammoSupply() == 0)
    {
#ifdef DEBUG
      dLog.printf("%s has no artillery ammo", cp->getName());
#endif
      return;
    }
     
//     if(cp->moving() || cp->deploying() || cp->manuevering() || cp

    BattleList& blist = cp->targetList();

    // add artillery, if any (there can never be more artillery firing
    // than there is front-line hexes)
   // first, count number of artillery
    int nArtyFiring = (cp->columns() < cp->nArtillery()) ? cp->columns() : cp->nArtillery();
    ASSERT(nArtyFiring > 0);

    // go through target list and find out how many SP's have target in sight
    int nSPsFiring = 0;
    SListIter<BattleItem> liter(&blist);
    while(++liter)
         nSPsFiring += liter.current()->d_spsFiring;

    if(nSPsFiring > 0)
    {
#ifdef DEBUG
         dLog.printf("Plotting Artillery fire for %s", cp->getName());
#endif

         int nArtyFired = 0;
         for(BattleSPIter spIter(d_batData->ob(), cp, BSPI_ModeDefs::ArtilleryOnly);
               !spIter.isFinished();
               spIter.next(), nArtyFired++)
         {
            if(nArtyFired >= nArtyFiring)
               break;

            const UnitTypeItem& uti = d_batData->ob()->getUnitType(spIter.sp());
            ASSERT(uti.getBasicType() == BasicUnitType::Artillery);

            liter.rewind();
            while(++liter)
            {
               // skip past if this is not to the front
               if(liter.current()->d_position != BattleItem::Front)
                  continue;

//             int fireThisTime = nArtyFiring;
               int percentToThisTarget = 100;
               if(cp->targetCP() != NoBattleCP)
               {
                  if(liter.current()->d_cp != cp->targetCP())
                      continue;
#ifdef DEBUG
                  dLog.printf("Firing artillery at a player designated target (%s)", cp->targetCP());
#endif
               }
               else
               {
                  ASSERT(nSPsFiring > 0);
                  percentToThisTarget = MulDiv(liter.current()->d_spsFiring, 100, nSPsFiring);
//                fireThisTime = MulDiv(nArtyFiring, per, 100);
               }

               // get fire value for this sp
               UBYTE nHexes = (liter.current()->d_range <= 440)  ? Hexes2 :
                                     (liter.current()->d_range <= 660)  ? Hexes3 :
                                     (liter.current()->d_range <= 880)  ? Hexes4 :
                                     (liter.current()->d_range <= 1540) ? Hexes7 : Hexes8;

               UBYTE type = (uti.isMounted()) ? HorseArt : FootArt;
               const Table2D<UWORD>& fpTable = BattleTables::fireCombatPoints();
               UWORD fireValue = (percentToThisTarget * fpTable.getValue(type, nHexes)) / 100;

               if(fireValue > 0 && spIter.sp()->strength(100) < 100)
                  fireValue = static_cast<UWORD>(MulDiv(fireValue, spIter.sp()->strength(100), 100));

               // if we have enemy is >= Hexes5 and we are in def posture,
               // modify fireValue * .5
               if(cp->posture() == BattleOrderInfo::Defensive && liter.current()->d_range >= 1100)
                     fireValue *= .5;

#ifdef DEBUG
               dLog.printf("Assigning SP %ld (%s)(fireValue = %d) to %s",
                  reinterpret_cast<LONG>(spIter.sp()), uti.getName(),
                  static_cast<int>(fireValue), liter.current()->d_cp->getName());
#endif
               UWORD r = UWORD_MAX;
                    if(fireValue > 0)
                    {
                  if(!blist.addFireValue(liter.current()->d_cp, r, fireValue, liter.current()->d_position))
                     FORCEASSERT("Enemy not in List");
                        
                     setShootingGuns(cp, liter.current()->d_cp);
                    }
            }
         }
    }
}


const int c_nUnitTypes = 3;
const int c_nPosture = BattleOrderInfo::Posture_HowMany;
const int c_nAggress = BattleOrderInfo::Aggress_HowMany;
const int c_nMove = 2;   // moving or not moving
const int c_nDisorder = BattleCP::Disorder::MaxValue + 1;
const int c_nEDisordered = 2; // enemy disorder, or shaken, or not
const int c_nEnemyCav = 2;    // against enemy cavalry or not

const UBYTE c_hold        = static_cast<UBYTE>(BattleCP::NE_HoldInPlace);
const UBYTE c_charge      = static_cast<UBYTE>(BattleCP::NE_Charge);
const UBYTE c_chargeAt1   = static_cast<UBYTE>(BattleCP::NE_ChargeAt1);
const UBYTE c_chargeAt2   = static_cast<UBYTE>(BattleCP::NE_ChargeAt2);
const UBYTE c_chargeAt3   = static_cast<UBYTE>(BattleCP::NE_ChargeAt3);
const UBYTE c_haltAt2     = static_cast<UBYTE>(BattleCP::NE_HaltAt2);
const UBYTE c_haltAt3     = static_cast<UBYTE>(BattleCP::NE_HaltAt3);
const UBYTE c_haltAt4     = static_cast<UBYTE>(BattleCP::NE_HaltAt4);
const UBYTE c_haltAt5     = static_cast<UBYTE>(BattleCP::NE_HaltAt5);
const UBYTE c_haltAt6     = static_cast<UBYTE>(BattleCP::NE_HaltAt6);
const UBYTE c_keepAway2   = static_cast<UBYTE>(BattleCP::NE_KeepAway2);
const UBYTE c_keepAway3   = static_cast<UBYTE>(BattleCP::NE_KeepAway3);
const UBYTE c_keepAway4   = static_cast<UBYTE>(BattleCP::NE_KeepAway4);
const UBYTE c_keepAway5   = static_cast<UBYTE>(BattleCP::NE_KeepAway5);
const UBYTE c_keepAway6   = static_cast<UBYTE>(BattleCP::NE_KeepAway6);

static const UBYTE s_table[c_nEnemyCav][c_nEDisordered][c_nMove][c_nPosture][c_nUnitTypes][c_nAggress][c_nDisorder] = {
{   // enemy not cav
  {    // enemy not disordered or shaken
    {    // not moving
      {    // defensive posture
        {     // infantry
          {      // aggress level 0
             c_keepAway4, c_keepAway4, c_keepAway4, c_keepAway4
          },
          {      // aggress level 1
             c_hold, c_hold, c_hold, c_hold
          },
          {      // aggress level 2
             c_hold, c_hold, c_hold, c_hold
          },
          {      // aggress level 3
             c_hold, c_hold, c_hold, c_hold
          }
        },
        {     // cavalry
          {      // aggress level 0
             c_keepAway4, c_keepAway4, c_keepAway4, c_keepAway4
          },
          {      // aggress level 1
             c_keepAway4, c_keepAway4, c_keepAway4, c_keepAway4
          },
          {      // aggress level 2
             c_keepAway4, c_keepAway4, c_haltAt4, c_haltAt4
          },
          {      // aggress level 3
             c_keepAway3, c_keepAway3, c_keepAway3, c_keepAway3
          }
        },
        {     // artillery
          {      // aggress level 0
            c_keepAway6, c_keepAway6, c_keepAway6, c_keepAway6
          },
          {      // aggress level 1
            c_keepAway5, c_keepAway5, c_keepAway5, c_keepAway5
          },
          {      // aggress level 2
            c_haltAt4, c_haltAt4, c_haltAt4, c_haltAt4
          },
          {      // aggress level 3
            c_haltAt3, c_haltAt3, c_haltAt3, c_haltAt3
          }
        }
      },
      {    // offensive posture
         {     // infantry
          {      // aggress level 0
             c_keepAway4, c_keepAway4, c_keepAway4, c_keepAway4
          },
          {      // aggress level 1
            c_hold, c_hold, c_hold, c_hold
          },
          {      // aggress level 2
            c_hold, c_hold, c_chargeAt2, c_chargeAt2
          },
          {      // aggress level 3
            c_chargeAt2, c_chargeAt2, c_chargeAt2, c_chargeAt2
          }
        },
        {     // cavalry
          {      // aggress level 0
             c_keepAway4, c_keepAway4, c_keepAway4, c_keepAway4
          },
          {      // aggress level 1
             c_keepAway4, c_keepAway4, c_keepAway4, c_keepAway4
          },
          {      // aggress level 2
             c_keepAway3, c_keepAway3, c_chargeAt2, c_chargeAt2
          },
          {      // aggress level 3
             c_chargeAt3, c_chargeAt3, c_chargeAt3, c_chargeAt3
          }
        },
        {     // artillery
          {      // aggress level 0
            c_keepAway5, c_keepAway5, c_keepAway5, c_keepAway5
          },
          {      // aggress level 1
            c_keepAway5, c_keepAway5, c_keepAway5, c_keepAway5
          },
          {      // aggress level 2
            c_haltAt3, c_haltAt3, c_haltAt3, c_haltAt3
          },
          {      // aggress level 3
            c_haltAt2, c_haltAt2, c_haltAt2, c_haltAt2
          }
        }
      }
    },
    {    // moving
      {    // defensive posture
        {     // infantry
          {      // aggress level 0
            c_keepAway4, c_keepAway4, c_keepAway4, c_keepAway4
          },
          {      // aggress level 1
            c_haltAt4, c_haltAt4, c_haltAt4, c_haltAt3
          },
          {      // aggress level 2
            c_haltAt3, c_haltAt3, c_haltAt3, c_haltAt2
          },
          {      // aggress level 3
            c_haltAt2, c_haltAt2, c_haltAt2, c_haltAt2
          }
        },
        {     // cavalry
          {      // aggress level 0
             c_keepAway4, c_keepAway4, c_keepAway4, c_keepAway4
          },
          {      // aggress level 1
             c_keepAway4, c_keepAway4, c_keepAway4, c_keepAway4
          },
          {      // aggress level 2
             c_keepAway4, c_keepAway4, c_chargeAt2, c_chargeAt2
          },
          {      // aggress level 3
             c_haltAt3, c_haltAt3, c_haltAt3, c_haltAt3
          }
        },
        {     // artillery
          {      // aggress level 0
            c_keepAway6, c_keepAway6, c_keepAway6, c_keepAway6
          },
          {      // aggress level 1
            c_keepAway5, c_keepAway5, c_keepAway5, c_keepAway5
          },
          {      // aggress level 2
            c_haltAt4, c_haltAt4, c_haltAt4, c_haltAt4
          },
          {      // aggress level 3
            c_haltAt3, c_haltAt3, c_haltAt3, c_haltAt3
          }
        }
      },
      {    // offensive posture
        {     // infantry
          {      // aggress level 0
            c_keepAway4, c_keepAway4, c_keepAway4, c_keepAway3
          },
          {      // aggress level 1
            c_haltAt3, c_haltAt3, c_haltAt3, c_haltAt2
          },
          {      // aggress level 2
            c_haltAt2, c_haltAt2, c_charge, c_charge
          },
          {      // aggress level 3
            c_charge, c_charge, c_charge, c_charge
          }
        },
         {     // cavalry
          {      // aggress level 0
             c_keepAway4, c_keepAway4, c_keepAway4, c_keepAway4
          },
          {      // aggress level 1
             c_keepAway4, c_keepAway4, c_keepAway4, c_keepAway4
          },
          {      // aggress level 2
             c_keepAway4, c_keepAway4, c_chargeAt2, c_chargeAt2
          },
          {      // aggress level 3
             c_chargeAt3, c_chargeAt3, c_chargeAt3, c_chargeAt3
          }
        },
        {     // artillery
          {      // aggress level 0
            c_keepAway5, c_keepAway5, c_keepAway5, c_keepAway5
          },
          {      // aggress level 1
            c_keepAway5, c_keepAway5, c_keepAway5, c_keepAway5
          },
          {      // aggress level 2
            c_haltAt3, c_haltAt3, c_haltAt3, c_haltAt3
          },
          {      // aggress level 3
            c_haltAt2, c_haltAt2, c_haltAt2, c_haltAt2
          }
        }
      }
    }
  },
   {    // enemy disordered or shaken
    {    // not moving
      {    // defensive posture
        {     // infantry
          {      // aggress level 0
             c_keepAway4, c_keepAway4, c_keepAway4, c_keepAway4
          },
          {      // aggress level 1
             c_hold, c_hold, c_hold, c_hold
          },
          {      // aggress level 2
             c_hold, c_hold, c_hold, c_hold
          },
          {      // aggress level 3
             c_hold, c_hold, c_hold, c_hold
          }
        },
        {     // cavalry
          {      // aggress level 0
             c_keepAway4, c_keepAway4, c_keepAway4, c_keepAway4
          },
          {      // aggress level 1
             c_keepAway4, c_keepAway4, c_chargeAt2, c_chargeAt2
          },
          {      // aggress level 2
             c_keepAway4, c_keepAway4, c_chargeAt2, c_chargeAt2
          },
          {      // aggress level 3
             c_haltAt3, c_haltAt3, c_chargeAt3, c_chargeAt3
          }
        },
        {     // artillery
          {      // aggress level 0
            c_keepAway6, c_keepAway6, c_keepAway6, c_keepAway6
          },
          {      // aggress level 1
            c_keepAway5, c_keepAway5, c_keepAway5, c_keepAway5
          },
          {      // aggress level 2
            c_haltAt4, c_haltAt4, c_haltAt4, c_haltAt4
          },
          {      // aggress level 3
            c_haltAt3, c_haltAt3, c_haltAt3, c_haltAt3
          }
        }
      },
      {    // offensive posture
         {     // infantry
          {      // aggress level 0
             c_keepAway4, c_keepAway4, c_keepAway4, c_keepAway4
          },
          {      // aggress level 1
            c_hold, c_hold, c_chargeAt2, c_chargeAt2
          },
          {      // aggress level 2
            c_hold, c_hold, c_chargeAt2, c_chargeAt2
          },
          {      // aggress level 3
            c_chargeAt2, c_chargeAt2, c_chargeAt2, c_chargeAt2
          }
        },
        {     // cavalry
          {      // aggress level 0
             c_keepAway4, c_keepAway4, c_keepAway4, c_keepAway4
          },
          {      // aggress level 1
             c_keepAway4, c_keepAway4, c_chargeAt2, c_chargeAt2
          },
          {      // aggress level 2
             c_keepAway3, c_keepAway3, c_chargeAt2, c_chargeAt2
          },
          {      // aggress level 3
             c_chargeAt3, c_chargeAt3, c_chargeAt3, c_chargeAt3
          }
        },
        {     // artillery
          {      // aggress level 0
            c_keepAway5, c_keepAway5, c_keepAway5, c_keepAway5
          },
          {      // aggress level 1
            c_keepAway5, c_keepAway5, c_keepAway5, c_keepAway5
          },
          {      // aggress level 2
            c_haltAt3, c_haltAt3, c_haltAt3, c_haltAt3
          },
          {      // aggress level 3
            c_haltAt2, c_haltAt2, c_haltAt2, c_haltAt2
          }
        }
      }
    },
    {    // moving
      {    // defensive posture
        {     // infantry
          {      // aggress level 0
            c_keepAway4, c_keepAway4, c_keepAway4, c_keepAway4
          },
          {      // aggress level 1
            c_haltAt4, c_haltAt4, c_haltAt4, c_haltAt2
          },
          {      // aggress level 2
            c_haltAt3, c_haltAt3, c_haltAt3, c_haltAt2
          },
          {      // aggress level 3
            c_haltAt2, c_haltAt2, c_haltAt2, c_haltAt2
          }
        },
        {     // cavalry
          {      // aggress level 0
             c_keepAway4, c_keepAway4, c_keepAway4, c_keepAway4
          },
          {      // aggress level 1
             c_keepAway4, c_keepAway4, c_chargeAt2, c_chargeAt2
          },
          {      // aggress level 2
             c_keepAway4, c_keepAway4, c_chargeAt2, c_chargeAt2
          },
          {      // aggress level 3
             c_chargeAt3, c_chargeAt3, c_chargeAt3, c_chargeAt3
          }
        },
        {     // artillery
          {      // aggress level 0
            c_keepAway6, c_keepAway6, c_keepAway6, c_keepAway6
          },
          {      // aggress level 1
            c_keepAway5, c_keepAway5, c_keepAway5, c_keepAway5
          },
          {      // aggress level 2
            c_haltAt4, c_haltAt4, c_haltAt4, c_haltAt4
          },
          {      // aggress level 3
            c_haltAt3, c_haltAt3, c_haltAt3, c_haltAt3
          }
        }
      },
      {    // offensive posture
        {     // infantry
          {      // aggress level 0
            c_keepAway4, c_keepAway4, c_keepAway4, c_keepAway3
          },
          {      // aggress level 1
            c_haltAt3, c_haltAt3, c_haltAt3, c_charge
          },
          {      // aggress level 2
            c_haltAt2, c_haltAt2, c_charge, c_charge
          },
          {      // aggress level 3
            c_charge, c_charge, c_charge, c_charge
          }
        },
         {     // cavalry
          {      // aggress level 0
             c_keepAway4, c_keepAway4, c_keepAway4, c_keepAway4
          },
          {      // aggress level 1
             c_keepAway4, c_keepAway4, c_chargeAt3, c_chargeAt3
          },
          {      // aggress level 2
             c_keepAway4, c_keepAway4, c_chargeAt2, c_chargeAt2
          },
          {      // aggress level 3
             c_chargeAt3, c_chargeAt3, c_chargeAt3, c_chargeAt3
          }
        },
        {     // artillery
          {      // aggress level 0
            c_keepAway5, c_keepAway5, c_keepAway5, c_keepAway5
          },
          {      // aggress level 1
            c_keepAway5, c_keepAway5, c_keepAway5, c_keepAway5
          },
          {      // aggress level 2
            c_haltAt3, c_haltAt3, c_haltAt3, c_haltAt3
          },
          {      // aggress level 3
            c_haltAt2, c_haltAt2, c_haltAt2, c_haltAt2
          }
        }
      }
    }
  }
},

{   // enemy cav
  {    // enemy not disordered or shaken
    {    // not moving
      {    // defensive posture
        {     // infantry
          {      // aggress level 0
             c_keepAway4, c_keepAway4, c_keepAway4, c_keepAway4
          },
          {      // aggress level 1
             c_hold, c_hold, c_hold, c_hold
          },
          {      // aggress level 2
             c_hold, c_hold, c_hold, c_hold
          },
          {      // aggress level 3
             c_hold, c_hold, c_hold, c_hold
          }
        },
        {     // cavalry
          {      // aggress level 0
             c_keepAway4, c_keepAway4, c_keepAway4, c_keepAway4
          },
          {      // aggress level 1
             c_keepAway4, c_keepAway4, c_keepAway4, c_keepAway4
          },
          {      // aggress level 2
             c_keepAway4, c_keepAway4, c_haltAt4, c_haltAt4
          },
          {      // aggress level 3
             c_keepAway3, c_keepAway3, c_keepAway3, c_keepAway3
          }
        },
        {     // artillery
          {      // aggress level 0
            c_keepAway6, c_keepAway6, c_keepAway6, c_keepAway6
          },
          {      // aggress level 1
            c_keepAway5, c_keepAway5, c_keepAway5, c_keepAway5
          },
          {      // aggress level 2
            c_haltAt4, c_haltAt4, c_haltAt4, c_haltAt4
          },
          {      // aggress level 3
            c_haltAt3, c_haltAt3, c_haltAt3, c_haltAt3
          }
         }
      },
      {    // offensive posture
        {     // infantry
          {      // aggress level 0
             c_keepAway4, c_keepAway4, c_keepAway4, c_keepAway4
          },
          {      // aggress level 1
            c_hold, c_hold, c_hold, c_hold
          },
          {      // aggress level 2
            c_hold, c_hold, c_hold, c_hold
          },
          {      // aggress level 3
            c_hold, c_hold, c_hold, c_hold
          }
        },
        {     // cavalry
          {      // aggress level 0
             c_keepAway4, c_keepAway4, c_keepAway4, c_keepAway4
          },
          {      // aggress level 1
             c_keepAway4, c_keepAway4, c_keepAway4, c_keepAway4
          },
          {      // aggress level 2
             c_keepAway3, c_keepAway3, c_haltAt2, c_haltAt2
          },
          {      // aggress level 3
             c_chargeAt3, c_chargeAt3, c_chargeAt3, c_chargeAt3
          }
        },
        {     // artillery
          {      // aggress level 0
            c_keepAway5, c_keepAway5, c_keepAway5, c_keepAway5
          },
          {      // aggress level 1
            c_keepAway5, c_keepAway5, c_keepAway5, c_keepAway5
          },
          {      // aggress level 2
            c_haltAt3, c_haltAt3, c_haltAt3, c_haltAt3
          },
          {      // aggress level 3
            c_haltAt2, c_haltAt2, c_haltAt2, c_haltAt2
          }
        }
      }
    },
    {    // moving
      {    // defensive posture
        {     // infantry
          {      // aggress level 0
            c_keepAway4, c_keepAway4, c_keepAway4, c_keepAway4
          },
          {      // aggress level 1
            c_haltAt4, c_haltAt4, c_haltAt4, c_haltAt4
          },
          {      // aggress level 2
            c_haltAt4, c_haltAt4, c_haltAt4, c_haltAt3
          },
          {      // aggress level 3
            c_haltAt3, c_haltAt3, c_haltAt3, c_haltAt2
          }
        },
        {     // cavalry
          {      // aggress level 0
             c_keepAway4, c_keepAway4, c_keepAway4, c_keepAway4
          },
          {      // aggress level 1
             c_keepAway4, c_keepAway4, c_keepAway4, c_keepAway4
          },
          {      // aggress level 2
             c_keepAway4, c_keepAway4, c_chargeAt2, c_chargeAt2
          },
          {      // aggress level 3
             c_haltAt3, c_haltAt3, c_haltAt3, c_haltAt3
          }
        },
        {     // artillery
          {      // aggress level 0
            c_keepAway6, c_keepAway6, c_keepAway6, c_keepAway6
          },
          {      // aggress level 1
            c_keepAway5, c_keepAway5, c_keepAway5, c_keepAway5
          },
          {      // aggress level 2
            c_haltAt4, c_haltAt4, c_haltAt4, c_haltAt4
          },
          {      // aggress level 3
            c_haltAt3, c_haltAt3, c_haltAt3, c_haltAt3
          }
        }
      },
      {    // offensive posture
        {     // infantry
          {      // aggress level 0
            c_keepAway4, c_keepAway4, c_keepAway4, c_keepAway3
          },
          {      // aggress level 1
            c_haltAt4, c_haltAt4, c_haltAt4, c_haltAt3
          },
          {      // aggress level 2
            c_haltAt3, c_haltAt3, c_haltAt3, c_haltAt2
          },
          {      // aggress level 3
            c_haltAt2, c_haltAt2, c_haltAt2, c_haltAt2
          }
        },
        {     // cavalry
          {      // aggress level 0
             c_keepAway4, c_keepAway4, c_keepAway4, c_keepAway4
          },
          {      // aggress level 1
             c_keepAway4, c_keepAway4, c_keepAway4, c_keepAway4
          },
          {      // aggress level 2
             c_keepAway4, c_keepAway4, c_haltAt2, c_haltAt2
          },
          {      // aggress level 3
             c_chargeAt3, c_chargeAt3, c_chargeAt3, c_chargeAt3
          }
        },
         {     // artillery
          {      // aggress level 0
            c_keepAway5, c_keepAway5, c_keepAway5, c_keepAway5
          },
          {      // aggress level 1
            c_keepAway5, c_keepAway5, c_keepAway5, c_keepAway5
          },
          {      // aggress level 2
            c_haltAt3, c_haltAt3, c_haltAt3, c_haltAt3
          },
          {      // aggress level 3
            c_haltAt2, c_haltAt2, c_haltAt2, c_haltAt2
          }
        }
      }
    }
  },
  {    // enemy disordered or shaken
    {    // not moving
      {    // defensive posture
        {     // infantry
          {      // aggress level 0
             c_keepAway4, c_keepAway4, c_keepAway4, c_keepAway4
          },
          {      // aggress level 1
             c_hold, c_hold, c_hold, c_hold
          },
          {      // aggress level 2
             c_hold, c_hold, c_hold, c_hold
          },
          {      // aggress level 3
             c_hold, c_hold, c_hold, c_hold
          }
        },
        {     // cavalry
          {      // aggress level 0
             c_keepAway4, c_keepAway4, c_keepAway4, c_keepAway4
          },
          {      // aggress level 1
             c_keepAway4, c_keepAway4, c_chargeAt2, c_chargeAt2
          },
          {      // aggress level 2
             c_keepAway4, c_keepAway4, c_chargeAt2, c_chargeAt2
          },
          {      // aggress level 3
             c_haltAt3, c_haltAt3, c_chargeAt3, c_chargeAt3
          }
        },
        {     // artillery
          {      // aggress level 0
            c_keepAway6, c_keepAway6, c_keepAway6, c_keepAway6
          },
          {      // aggress level 1
            c_keepAway5, c_keepAway5, c_keepAway5, c_keepAway5
          },
          {      // aggress level 2
            c_haltAt4, c_haltAt4, c_haltAt4, c_haltAt4
          },
          {      // aggress level 3
            c_haltAt3, c_haltAt3, c_haltAt3, c_haltAt3
          }
         }
      },
      {    // offensive posture
        {     // infantry
          {      // aggress level 0
             c_keepAway4, c_keepAway4, c_keepAway4, c_keepAway4
          },
          {      // aggress level 1
            c_hold, c_hold, c_hold, c_hold
          },
          {      // aggress level 2
            c_hold, c_hold, c_hold, c_hold
          },
          {      // aggress level 3
            c_hold, c_hold, c_hold, c_hold
          }
        },
         {     // cavalry
          {      // aggress level 0
             c_keepAway4, c_keepAway4, c_keepAway4, c_keepAway4
          },
          {      // aggress level 1
             c_keepAway4, c_keepAway4, c_keepAway4, c_keepAway4
          },
          {      // aggress level 2
             c_keepAway3, c_keepAway3, c_haltAt2, c_haltAt2
          },
          {      // aggress level 3
             c_chargeAt3, c_chargeAt3, c_chargeAt3, c_chargeAt3
          }
        },
        {     // artillery
          {      // aggress level 0
            c_keepAway5, c_keepAway5, c_keepAway5, c_keepAway5
          },
          {      // aggress level 1
            c_keepAway5, c_keepAway5, c_keepAway5, c_keepAway5
          },
          {      // aggress level 2
            c_haltAt3, c_haltAt3, c_haltAt3, c_haltAt3
          },
          {      // aggress level 3
            c_haltAt2, c_haltAt2, c_haltAt2, c_haltAt2
          }
        }
      }
    },
    {    // moving
      {    // defensive posture
        {     // infantry
          {      // aggress level 0
            c_keepAway4, c_keepAway4, c_keepAway4, c_keepAway4
          },
          {      // aggress level 1
            c_haltAt4, c_haltAt4, c_haltAt4, c_haltAt4
          },
          {      // aggress level 2
            c_haltAt4, c_haltAt4, c_haltAt4, c_haltAt3
          },
          {      // aggress level 3
            c_haltAt3, c_haltAt3, c_haltAt3, c_haltAt2
          }
        },
        {     // cavalry
          {      // aggress level 0
             c_keepAway4, c_keepAway4, c_keepAway4, c_keepAway4
          },
          {      // aggress level 1
             c_keepAway4, c_keepAway4, c_chargeAt2, c_chargeAt2
          },
          {      // aggress level 2
             c_keepAway4, c_keepAway4, c_chargeAt2, c_chargeAt2
          },
          {      // aggress level 3
             c_chargeAt3, c_chargeAt3, c_chargeAt3, c_chargeAt3
          }
        },
        {     // artillery
          {      // aggress level 0
            c_keepAway6, c_keepAway6, c_keepAway6, c_keepAway6
          },
          {      // aggress level 1
            c_keepAway5, c_keepAway5, c_keepAway5, c_keepAway5
          },
          {      // aggress level 2
            c_haltAt4, c_haltAt4, c_haltAt4, c_haltAt4
          },
          {      // aggress level 3
            c_haltAt3, c_haltAt3, c_haltAt3, c_haltAt3
          }
        }
      },
      {    // offensive posture
        {     // infantry
          {      // aggress level 0
            c_keepAway4, c_keepAway4, c_keepAway4, c_keepAway3
          },
          {      // aggress level 1
            c_haltAt4, c_haltAt4, c_haltAt4, c_haltAt3
          },
          {      // aggress level 2
            c_haltAt3, c_haltAt3, c_haltAt3, c_haltAt2
          },
          {      // aggress level 3
            c_haltAt2, c_haltAt2, c_haltAt2, c_haltAt2
          }
        },
        {     // cavalry
          {      // aggress level 0
             c_keepAway4, c_keepAway4, c_keepAway4, c_keepAway4
          },
          {      // aggress level 1
             c_keepAway4, c_keepAway4, c_keepAway4, c_keepAway4
          },
          {      // aggress level 2
             c_keepAway4, c_keepAway4, c_chargeAt3, c_chargeAt3
          },
          {      // aggress level 3
             c_chargeAt3, c_chargeAt3, c_chargeAt3, c_chargeAt3
          }
        },
         {     // artillery
          {      // aggress level 0
            c_keepAway5, c_keepAway5, c_keepAway5, c_keepAway5
          },
          {      // aggress level 1
            c_keepAway5, c_keepAway5, c_keepAway5, c_keepAway5
          },
          {      // aggress level 2
            c_haltAt3, c_haltAt3, c_haltAt3, c_haltAt3
          },
          {      // aggress level 3
            c_haltAt2, c_haltAt2, c_haltAt2, c_haltAt2
          }
         }
      }
    }
   }
}

};

void B_DetectImp::doEnemyNear(const RefBattleCP& cp)
{
#ifdef DEBUG
   dLog.printf("\n------- Near Enemy Routine for %s", cp->getName());
#endif

   ASSERT(cp->targetList().entries() > 0);
   
   // always keep away if a HQ
   if(cp->getRank().isHigher(Rank_Division))
   {
       if(!cp->attached())
       {
            cp->nearEnemyMode(BattleCP::NE_KeepAway4);
#ifdef DEBUG       
               dLog.printf("Near enemy mode = (%s)", BattleCP::nearEnemyName(BattleCP::NE_KeepAway4));
            dLog.printf("-----------------------------------\n");
#endif
      }
        return;
   }
   
   if(cp->fleeingTheField())
   {
         cp->nearEnemyMode(BattleCP::NE_Retreat6);
#ifdef DEBUG       
            dLog.printf("Near enemy mode = (%s)", BattleCP::nearEnemyName(BattleCP::NE_Retreat6));
         dLog.printf("-----------------------------------\n");
#endif
         return;
   }
   
   CRefBattleCP enemyCP = cp->targetList().first()->d_cp;

   // get unit type by converting to local enum
   enum LocalUnitType {
    L_Inf,
    L_Cav,
    L_Art,
    NTypes_HowMany,
    L_Undefined = NTypes_HowMany
   } unitType = (cp->generic()->isInfantry())  ? L_Inf :
               (cp->generic()->isCavalry())   ? L_Cav :
               (cp->generic()->isArtillery()) ? L_Art : L_Undefined;

   ASSERT(unitType != L_Undefined);

   bool enemyDisordered = (enemyCP->disorder() <= 1 || enemyCP->routing() || enemyCP->shaken());
   bool moving = (!cp->holding() || cp->getCurrentOrder().wayPoints().entries() > 0);
   bool againstCav = ( (enemyCP->generic()->isCavalry()) &&
          (cp->spFormation() == SP_MarchFormation || cp->spFormation() == SP_ColumnFormation || cp->spFormation() == SP_LineFormation) &&
          (!willDisorderEnemy(cp)) );

   ASSERT(unitType < c_nUnitTypes);
   ASSERT(cp->posture() < c_nPosture);
   ASSERT(cp->aggression() < c_nAggress);
   // ASSERT(moving < c_nMove);   // moving or not moving
   ASSERT(cp->disorder() < c_nDisorder);
   // ASSERT(enemyDisordered < c_nEDisordered); // enemy disorder, or shaken, or not
   // ASSERT(againstCav < c_nEnemyCav);    // against enemy cavalry or not

   BattleCP::NearEnemyMode nem = static_cast<BattleCP::NearEnemyMode>(s_table[againstCav][enemyDisordered][moving][cp->posture()][unitType][cp->aggression()][cp->disorder()]);

   // special cases
   if(cp->shaken() && (nem >= BattleCP::NE_Charge && nem < BattleCP::NE_HaltAt2) )
   {
    nem = static_cast<BattleCP::NearEnemyMode>(c_hold);
   }

   if(nem != cp->nearEnemyMode())
   {
      // send player message
      if(nem >= BattleCP::NE_KeepAway2 && nem <= BattleCP::NE_KeepAway6)
      {
         // our side
         {
            BattleMessageInfo msg(cp->getSide(), BattleMessageID::BMSG_AvoidingEnemy, cp);
            d_batgame->sendPlayerMessage(msg);
         }

         // there side
         {
            BattleMessageInfo msg(enemyCP->getSide(), BattleMessageID::BMSG_AvoidingEnemy, enemyCP);
        msg.target(cp);
            d_batgame->sendPlayerMessage(msg);
         }
      }

      cp->nearEnemyMode(nem);
      // set for attached leader, if any
      RefBattleCP hq = BobUtility::getAttachedLeader(d_batData, cp);
      if(hq != NoBattleCP)
          hq->nearEnemyMode(nem);

   }

#ifdef DEBUG
   dLog.printf("e. cav = %d, e. dis = %d, moving = %d, post = %d, unittype = %d, aggr = %d, dis = %d",
      static_cast<int>(againstCav), static_cast<int>(enemyDisordered), static_cast<int>(moving),
      static_cast<int>(cp->posture()), static_cast<int>(unitType), static_cast<int>(cp->aggression()),
      static_cast<int>(cp->disorder()));

   dLog.printf("Near enemy mode = (%s)", BattleCP::nearEnemyName(nem));
   dLog.printf("-----------------------------------\n");
#endif
}

/*
 * Are we occupying terrain that will disorder the enemy
 */

bool B_DetectImp::willDisorderEnemy(const RefBattleCP& cp)
{
  ASSERT(cp->targetList().entries() > 0);
  CRefBattleCP enemyCP = cp->targetList().first()->d_cp;

   bool willDisorder = False;

  // convert enemy unit type  to local enum
  enum {
    Inf,
    LCav,
    HCav,
    FootArt,
    HvyArt,
    HorseArt,

    End,
    Undefined = End
  } unitType = (enemyCP->generic()->isInfantry())       ? Inf :
               (enemyCP->generic()->isCavalry())        ? LCav :
               (enemyCP->generic()->isHeavyCavalry())   ? HCav :
               (enemyCP->generic()->isArtillery())      ? FootArt :
               (enemyCP->generic()->isHeavyArtillery()) ? HvyArt :
               (enemyCP->generic()->isHorseArtillery()) ? HorseArt : Undefined;

  // convert enemyCP spFormation to local enum
  enum {
    March,
    Column,
    Other
  } formation = (enemyCP->spFormation() == SP_MarchFormation) ? March :
                (enemyCP->spFormation() == SP_ColumnFormation) ? Column : Other;

  // Unchecked
  //vector<DeployItem>& map = cp->deployMap();
  for(int c = 0; c < cp->columns(); c++)
  {
    //int index = maximum(0, (cp->columns() * 0) + c);
    DeployItem* di = cp->currentDeployItem(c, 0);//map[index];
    ASSERT(di);
    ASSERT(di->active());
    if(di && di->active())
    {
      const BattleTerrainHex& hexInfo = d_batData->getTerrain(di->d_sp->hex());

      const Table3D<UBYTE>& tdTable = BattleTables::terrainDisorder();
      if(tdTable.getValue(unitType, hexInfo.d_terrainType, formation))
      {
         willDisorder = True;
         break;
      }
    }
  }
  // End

  return willDisorder;
}

void B_DetectImp::searchFront(const RefBattleCP& cp, HexPosition::Facing facing, int cRange, BattleList& llist, HexCord& sHex, B_DetectImp::WhichWay ww)
{
                ASSERT(ww == Front || ww == Rear);
                HexCord::HexDirection hd = (ww == Front) ? leftFront(facing) : leftRear(facing);
                HexCord::HexDirection rHD = rightFlank(facing);
                HexCord lHex = sHex;
                int inYards = 0;
                int loop = cRange;
                int goRightHowMany = 1;
                while(loop)
                {
                  inYards += BattleMeasure::XYardsPerHex;
                  HexCord hex;
                  if(d_batData->moveHex(lHex, hd, hex))
                     lHex = hex;
                  else
                     break;

                  goRightHowMany++;
                  HexCord rHex = lHex;
                  bool breakOuterLoop = False;
                  for(int i = 0; i < goRightHowMany; i++)
                  {
                     bool hasFriendly = False;
                     if(BobUtility::unitsUnderHex(d_batData, rHex, cp, BobUtility::Enemy, True) &&
                        d_los.FastLOS(d_batData->map(), sHex, rHex, cp->getSide()) != Visibility::NotSeen)
//                        Combat_Util::lineOfSight(d_batData, sHex, rHex, 0, cp, &hasFriendly))
                     {
                        if(BobUtility::unitsUnderHex(d_batData, rHex, cp, llist, BobUtility::Enemy, True, inYards, (ww == Front) ? BattleItem::Front : BattleItem::Rear))
                        {
                           // deliberately empty?
                        }
                     }

                     if(d_batData->moveHex(rHex, rHD, hex))
                        rHex = hex;
                     else
                     {
                        breakOuterLoop = True;
                        break;
                     }
                  }

                  if(breakOuterLoop)
                     break;

                  loop = maximum(0, loop - 1);
                }

}

void B_DetectImp::searchFlank(const RefBattleCP& cp, HexPosition::Facing facing, int cRange, BattleList& llist, BattleMeasure::HexCord& sHex, WhichWay w)
{
                  HexCord::HexDirection hd = (w == Left) ? leftFlank(facing) : rightFlank(facing);
                  HexCord lHex = sHex;
                  int inYards = 0;
                  int loop = cRange;
                  while(loop)
                  {
                     inYards += BattleMeasure::XYardsPerHex;
                     HexCord hex;
                     if(d_batData->moveHex(lHex, hd, hex))
                        lHex = hex;
                     else
                        break;

                     if(BobUtility::unitsUnderHex(d_batData, lHex, cp, BobUtility::Enemy, True) &&
                        d_los.FastLOS(d_batData->map(), sHex, lHex) != Visibility::NotSeen)
//                         Combat_Util::lineOfSight(d_batData, sHex, lHex, 0))
                     {
                        if(BobUtility::unitsUnderHex(d_batData, lHex, cp, llist, BobUtility::Enemy, True, inYards, (w == Left) ? BattleItem::LeftFlank : BattleItem::RightFlank))
                        {
                           // deliberately empty?
                        }
                     }

                     loop = maximum(0, loop - 1);
                  }
}

void B_DetectImp::searchFrontFlank(const RefBattleCP& cp, HexPosition::Facing facing, int cRange, BattleList& llist, BattleMeasure::HexCord& sHex, WhichWay wwFl, WhichWay wwFr)
{
                  ASSERT(wwFl == Left || wwFl == Right);
                  ASSERT(wwFr == Front || wwFr == Rear);

                  HexPosition::Facing f = HexPosition::Facing_Undefined;
                  BattleItem::Position pos = BattleItem::LeftFront;
                  if(wwFr == Front && wwFl == Left)
                  {
                     f = leftTurn(facing);
                     pos = BattleItem::LeftFront;
                  }
                  else if(wwFr == Front && wwFl == Right)
                  {
                     f = rightTurn(facing);
                     pos = BattleItem::RightFront;
                  }
                  else if(wwFr == Rear && wwFl == Left)
                  {
                     f = leftTurn(facing);
                     f = leftTurn(f);
                     pos = BattleItem::LeftRear;
                  }
                  else if(wwFr == Rear && wwFl == Right)
                  {
                     f = rightTurn(facing);
                     f = rightTurn(f);
                     pos = BattleItem::RightRear;
                  }

                  ASSERT(f != HexPosition::Facing_Undefined);

                  HexCord::HexDirection hd = leftFront(f);
                  HexCord::HexDirection rHD = rightFlank(f);

                  HexCord lHex = sHex;
                  int inYards = 0;
                  int loop = cRange;
                  int goRightHowMany = 1;
                  while(loop)
                  {
                     inYards += BattleMeasure::XYardsPerHex;
                     HexCord hex;
                     if(d_batData->moveHex(lHex, hd, hex))
                        lHex = hex;
                     else
                        break;

                     goRightHowMany++;
                     HexCord rHex = lHex;
                     bool breakOuterLoop = False;
                     for(int i = 0; i < goRightHowMany; i++)
                     {
                        // do not check extreme left and right hexes
                        // they have already been covered
                        if(i > 0 && i < goRightHowMany - 1)
                        {
//                           bool friendlyInTheWay = False;
                           if( (BobUtility::unitsUnderHex(d_batData, rHex, cp, BobUtility::Enemy, True)) &&
                                 d_los.FastLOS(d_batData->map(), sHex, rHex) != Visibility::NotSeen)
//                                 (Combat_Util::lineOfSight(d_batData, sHex, rHex, 0)))// &&
//                                 (!friendlyInTheWay) )
                           {
                              if(BobUtility::unitsUnderHex(d_batData, rHex, cp, llist, BobUtility::Enemy, True, inYards, pos))
                              {
                                 // deliberately empty?
                              }
                           }
                        }

                        if(d_batData->moveHex(rHex, rHD, hex))
                           rHex = hex;
                        else
                        {
                           breakOuterLoop = True;
                           break;
                        }
                     }

                     if(breakOuterLoop)
                        break;

                     loop = maximum(0, loop - 1);
                  }
}

/*-------------------------------------------------------------
 * Client-access
 */

B_DetectInt::B_DetectInt(BattleGameInterface* batgame) :
   d_di(new B_DetectImp(batgame))
{
  ASSERT(d_di);
}

B_DetectInt::~B_DetectInt()
{
  if(d_di)
    delete d_di;
}

bool B_DetectInt::enemyInRange(const RefBattleCP& cp)
{
  if(d_di)
    return d_di->enemyInRange(cp);
  else
    return False;
}

void B_DetectInt::doEnemyNear(const RefBattleCP& cp)
{
  if(d_di)
    d_di->doEnemyNear(cp);
}

}; // end namespace

/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.2  2001/06/18 08:25:43  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
