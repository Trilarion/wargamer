/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef BROUTE_HPP
#define BROUTE_HPP

#include "batdata.hpp"
#include "batunit.hpp"
#include "bobdef.hpp"

namespace B_Logic
{

class B_Route {
			RPBattleData d_batData;
			const RefBattleCP& d_cp;
			BattleUnit*        d_unit;
  public:
			B_Route(RPBattleData bd, const RefBattleCP& cp, BattleUnit* unit) :
					 d_batData(bd),
					 d_cp(cp),
					 d_unit(unit) {}

			~B_Route() {}

			bool plotRoute(const BattleMeasure::HexCord& srcHex, const BattleMeasure::HexCord& destHex,
						HexList* excludeHexes, bool resetList, bool addSrc, bool formationChange);
			bool plotRoute(const BattleMeasure::HexCord& srcHex, const BattleMeasure::HexCord& destHex, int startC, bool pasteing, bool testOnly);

			bool plotRoute(const BattleMeasure::HexCord& srcHex, const BattleMeasure::HexCord& destHex,
						HexList* excludeHexes, bool formationChange)
			{
					 return plotRoute(srcHex, destHex, excludeHexes, True, True, formationChange);
			}
			bool plotRoute(const BattleMeasure::HexCord& srcHex, HexList& destHexes, HexList* excludeHexes, bool formationChange);

		  //------------------------------------------------
		  // Added by Steven: 11Mar99, because bdeploy is using this

			bool plotRoute(const BattleMeasure::HexCord& srcHex, HexList& destHexes, HexList* excludeHexes)
			{
				return plotRoute(srcHex, destHexes, excludeHexes, false);
			}

			bool B_Route::plotRoute(const BattleMeasure::HexCord& srcHex, const BattleMeasure::HexCord& destHex,
						HexList* excludeHexes)
			{
					 return plotRoute(srcHex, destHex, excludeHexes, True, True, false);
			}

		  // End of Steven's addition
		  //------------------------------------------------

  private:
			//bool searchNodes(const BattleMeasure::HexCord& srcHex, const BattleMeasure::HexCord& destHex,
			//        HexList* excludeHexes, HexList& list,
			//        const UWORD bestCost, UWORD& cost);
			UWORD defaultRoute(const BattleMeasure::HexCord& srcHex,  const BattleMeasure::HexCord& destHex,
					  HexList* excludeHexes, HexList& list, bool addSrc = True);
			UWORD formationChangeRoute(const BattleMeasure::HexCord& srcHex,  const BattleMeasure::HexCord& destHex,
					  HexList* excludeHexes, HexList& list, bool addSrc = True);
			UWORD defaultHex(BattleMeasure::HexCord& hex, const BattleMeasure::HexCord& destHex,
					  HexList* excludeHexes);
			UWORD defaultHex(BattleMeasure::HexCord& hex, const BattleMeasure::HexCord& destHex, int startC, bool pasteing, const HexList& list);

};


}; // end namespace


#endif
