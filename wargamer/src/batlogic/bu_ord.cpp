/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
#include "stdinc.hpp"
#include "bu_ord.hpp"
#include "batarmy.hpp"
#include "b_tables.hpp"
#include "sync.hpp"
#include "batcord.hpp"
#include "hexmap.hpp"
#include "bobiter.hpp"
#include "bobutil.hpp"
#include "wg_rand.hpp"
#include "options.hpp"
#include "moveutil.hpp"
#include "b_send.hpp"
#include "control.hpp"
#ifdef DEBUG
#include "scenario.hpp"
#include "clog.hpp"
static LogFile bopLog("BOrderProc.log");
#endif
#include <utility>

#ifdef _MSC_VER
using namespace std::rel_ops;
#endif



namespace B_Logic
{

/*------------------------------------------------------------------------
 * Implementation of OrderMode virtual classes
 */

class BOrderBase {
   public:
    BOrderBase() {}
    ~BOrderBase() {}

   virtual bool start(RPBattleData bd, const RefBattleCP& cp) = 0;
  virtual bool update(RPBattleData bd, const RefBattleCP& cp) = 0;
};

// hold
class BOrder_Hold : public BOrderBase {
    public:
      BOrder_Hold() {}
      ~BOrder_Hold() {}

      bool start(RPBattleData bd, const RefBattleCP& cp);
      bool update(RPBattleData bd, const RefBattleCP& cp) { return True; }
};

bool BOrder_Hold::start(RPBattleData bd, const RefBattleCP& cp)
{
   cp->clearMove(True);
   cp->getCurrentOrder().startOrder();
   cp->orderMode(BattleOrderInfo::Hold);

   //if(cp->attached())
     // cp->setUnattached();

   return True;
}

// move
class BOrder_Move : public BOrderBase {
    public:
      BOrder_Move() {}
      ~BOrder_Move() {}

      bool start(RPBattleData bd, const RefBattleCP& cp);
      bool update(RPBattleData bd, const RefBattleCP& cp);
};

bool BOrder_Move::start(RPBattleData bd, const RefBattleCP& cp)
{
    cp->clearMove(True);
    cp->getCurrentOrder().startOrder();
    cp->orderMode(BattleOrderInfo::Move);
    //if(cp->attached())
      //cp->setUnattached();

    return True; //False;
}

bool BOrder_Move::update(RPBattleData bd, const RefBattleCP& cp)
{
    // see if we are still moving
    if(!cp->isMoving() && !cp->nextHex() && !cp->getCurrentOrder().nextWayPoint())
    {
         // go through and halt all units
         for(std::vector<DeployItem>::iterator di = cp->mapBegin();
                  di != cp->mapEnd();
                  di++)
         {
             if( (di->active()) &&
                 (di->d_sp->isMoving() || di->d_sp->nextHex()) )
             {
                  return False;
             }
         }

         return True;
    }

    return False;
}

// rest/rally
class BOrder_RestRally : public BOrderBase {
    public:
      BOrder_RestRally() {}
      ~BOrder_RestRally() {}

      bool start(RPBattleData bd, const RefBattleCP& cp)
      {
         cp->clearMove(True);
         cp->getCurrentOrder().startOrder();
         cp->orderMode(BattleOrderInfo::RestRally);
         //if(cp->attached())
           // cp->setUnattached();

         return True;
      }
      bool update(RPBattleData bd, const RefBattleCP& cp);
};

bool BOrder_RestRally::update(RPBattleData bd, const RefBattleCP& cp)
{
    return True;
}

// attach unit
class BOrder_Attach : public BOrderBase {
    public:
      BOrder_Attach() {}
      ~BOrder_Attach() {}

      bool start(RPBattleData bd, const RefBattleCP& cp)
      {
         cp->clearMove(True);
         cp->getCurrentOrder().startOrder();
         cp->orderMode(BattleOrderInfo::Attach);
         //if(cp->attached())
           // cp->setUnattached();

         return True;
//       return update(bd, cp);
      }
      bool update(RPBattleData bd, const RefBattleCP& cp);
};

bool BOrder_Attach::update(RPBattleData bd, const RefBattleCP& cp)
{
   if(cp->getRank().isHigher(Rank_Division))
   {

      BattleOrder& order = cp->getCurrentOrder();
      ASSERT(order.attachTo() != NoBattleCP);
      if(order.attachTo() == NoBattleCP)
      {
         return True;
      }

      if(!BobUtility::canAttach(bd, cp, order.attachTo()))
      {
#ifdef DEBUG
         bopLog.printf("%s cannot attach to %s", cp->getName(), order.attachTo()->getName());
#endif
         return True;
      }

      /*
       * if we are at our dest units location, attach leader
       */

      // first, figure out where we want to be now
      // we want to be centered in the last row of dest unit
      // if this is a XX with SP, if this is a XXX or other
      // non-sp unit then occupy same hex as XXX
      HexCord destHex;
      BattleSP* destSP = NoBattleSP;
      if(order.attachTo()->sp() != NoBattleSP)
      {
         int r = (order.attachTo()->nextFormation() == CPF_Extended) ? 0 : minimum(1, order.attachTo()->wantRows() - 1);
         const int c = maximum(0, (order.attachTo()->wantColumns() / 2) - 1);

         DeployItem* di = order.attachTo()->wantDeployItem(c, r);
         ASSERT(di);
         if(di)
         {
            ASSERT(di->active());
            if(di->active())
            {
               destHex = di->d_wantHex;
               destSP = di->d_sp;
            }
         }

      }
      else
         return True;

      // we we are already there, finish up order
      if(cp->hex() == destHex &&
         cp->allSPAtCenter() &&
         (!order.attachTo()->moving() && !order.attachTo()->deploying() && !order.attachTo()->onManuever()))
      {
#ifdef DEBUG
         bopLog.printf("%s has arrived at %s and will now attach", cp->getName(), order.attachTo()->getName());
#endif
         // make sure 2 HQs are not attached at the same time
         bool shouldAttach = True;
         RefBattleCP hq = BobUtility::getAttachedLeader(bd, const_cast<BattleCP*>(order.attachTo()));
         if(hq != NoBattleCP)
         {
            if(hq->getRank().isHigher(cp->getRank().getRankEnum()))
               shouldAttach = False;
            else
            {
#ifdef DEBUG
               bopLog.printf("Unattaching %s", hq->getName());
#endif
               hq->setUnattached();
            }
         }

         if(shouldAttach)
         {
            cp->setAttached();
            cp->setCompanion(destSP);
         }
#ifdef DEBUG
         else if(hq != NoBattleCP)
         {
            bopLog.printf("Cannot attach. %s is already attached!", hq->getName());
         }
#endif
         return True;
      }

      // otherwise see if we already have dest hex as our last waypoint.
      // If no, then add new waypoint
      bool needWayPoint = True;
      if(order.wayPoints().entries() > 0)
      {
         needWayPoint = (order.wayPoints().getLast()->d_hex != destHex);
      }

      // need a waypoint
      if(needWayPoint)
      {
         cp->clearMove(True);

            //reset waypoints if any
         if(order.wayPoints().entries() > 0)
            order.wayPoints().reset();

         // get new way point
         WayPoint* wp = new WayPoint(destHex, cp->getCurrentOrder().order());
         ASSERT(wp);

         order.wayPoints().append(wp);
      }
    }


    return False;
}

// detach unit
class BOrder_Detach : public BOrderBase {
    public:
      BOrder_Detach() {}
      ~BOrder_Detach() {}

      bool start(RPBattleData bd, const RefBattleCP& cp)
      {
         cp->getCurrentOrder().startOrder();
         cp->orderMode(BattleOrderInfo::Detach);
         return True;
//      return update(bd, cp);
      }
      bool update(RPBattleData bd, const RefBattleCP& cp);
};

bool BOrder_Detach::update(RPBattleData bd, const RefBattleCP& cp)
{
   bool done = False;
   if(cp->getRank().isHigher(Rank_Division))
   {
      if(cp->attached())
      {
         CRefBattleCP attachedTo = cp->getCurrentOrder().attachTo();
         if(attachedTo == NoBattleCP ||
            attachedTo->allSPAtCenter())
         {
            cp->setUnattached();
            cp->clearMove(True);
            done = True;
         }
      }
      else
         done = True;
   }

   else
   {
      RefBattleCP hq = BobUtility::getAttachedLeader(bd, cp);
      if(hq != NoBattleCP)
      {
         if(cp->allSPAtCenter())
         {
            hq->setUnattached();
            hq->clearMove(True);
            done = True;
         }
      }
      else
         done = True;
   }

   return done;
}


// bombard
class BOrder_Bombard : public BOrderBase {
    public:
      BOrder_Bombard() {}
      ~BOrder_Bombard() {}

      bool start(RPBattleData bd, const RefBattleCP& cp);
      bool update(RPBattleData bd, const RefBattleCP& cp);
};

bool BOrder_Bombard::start(RPBattleData bd, const RefBattleCP& cp)
{
    cp->getCurrentOrder().startOrder();
    cp->orderMode(BattleOrderInfo::Bombard);
    cp->targetCP(const_cast<RefBattleCP>(cp->getCurrentOrder().targetCP()));
    return True;//update(bd, cp);
}

bool BOrder_Bombard::update(RPBattleData bd, const RefBattleCP& cp)
{
    if( (cp->generic()->isArtillery() || cp->nArtillery() > 0) &&
          (cp->targetCP() != NoBattleCP) )
    {

       // if target is no longer in target list, finish order
       SListIter<BattleItem> iter(&cp->targetList());
       while(++iter)
       {
          if(iter.current()->d_cp == cp->targetCP())
             return True;
       }
    }

    cp->targetCP(NoBattleCP);
    cp->getCurrentOrder().targetCP(NoBattleCP);
    return True;
}

// rout
class BOrder_Rout : public BOrderBase {
    public:
      BOrder_Rout() {}
      ~BOrder_Rout() {}

      bool start(RPBattleData bd, const RefBattleCP& cp)
      {
         cp->getCurrentOrder().startOrder();
         cp->orderMode(BattleOrderInfo::Rout);
         cp->clearMove(True);
         if(cp->attached())
            cp->setUnattached();
         return True;//update(bd, cp);
      }
      bool update(RPBattleData bd, const RefBattleCP& cp);
};

bool BOrder_Rout::update(RPBattleData bd, const RefBattleCP& cp)
{
   cp->fleeingTheField(True);
   return True;
}

// static instances of the above classes
static BOrder_Hold         s_hold;
static BOrder_Move         s_move;
static BOrder_RestRally    s_restRally;
static BOrder_Attach       s_attach;
static BOrder_Detach       s_detach;
//static BOrder_AttachLeader s_attachLeader;
//static BOrder_DetachLeader s_detachLeader;
static BOrder_Bombard      s_bombard;
static BOrder_Rout         s_rout;

static BOrderBase* s_orders[BattleOrderInfo::OM_HowMany] = {
   &s_hold,
   &s_move,
   &s_restRally,
   &s_attach,
   &s_detach,
//  &s_attachLeader,
//  &s_detachLeader,
   &s_bombard,
   &s_rout
};

/*-------------------------------------------------------------------
 *  Process orders imp
 */

class B_OrderProcImp {
    PBattleData d_batData;
    BattleTime::Tick d_nextTick;

  public:
    B_OrderProcImp(RPBattleData bd) :
      d_batData(bd),
      d_nextTick(0) {}

    ~B_OrderProcImp() {}

    void process();
      // process all units
   private:
    bool procOrders(const RefBattleCP& cp);
      // process in-coming orders

    void updateOrders(const RefBattleCP& cp);
      // update current order

    bool thoughOfOrder(const RefBattleCP& cp, BattleOrder* order);
    bool testLeader(const RefBattleCP& cp);

    BattleTime::Tick calcActionTime(const RefBattleCP& cp);
};

void B_OrderProcImp::process()
{
//  if(d_batData->getTick() >= d_nextTick)
  {
    BattleData::WriteLock lock(d_batData);

#ifdef DEBUG
//    bopLog.printf("------------ Processing Orders --------------");
#endif

    // Iterate all units from top to bottom
    for(BattleUnitIter iter(d_batData->ob()); !iter.isFinished(); iter.next())
    {
      if(!iter.cp()->fleeingTheField() && !procOrders(iter.cp()))
         updateOrders(iter.cp());
    }

//  d_nextTick = d_batData->getTick() + (10 * BattleMeasure::BattleTime::TicksPerSecond);

#ifdef DEBUG
//    bopLog.printf("----------- End Processing Orders ------------\n");
#endif
   }
}

bool B_OrderProcImp::testLeader(const RefBattleCP& cp)
{
   // Raw chance is leaders initiative as a percent
   int chance = (cp->leader()->getInitiative() * 100) / Attribute_Range;
#ifdef DEBUG
   bopLog.printf("\n---Testing %s to see if he thinks of order himself", cp->getName());
   bopLog.printf("Raw chance = %d", chance);
#endif

   // modify chance
   // X 1.5 if morale is over 160
   if(cp->morale() >= 160)
      chance *= 1.5;

   // Cav specialist commanded cav X 1.5
   if(cp->generic()->isCavalry() && cp->leader()->isSpecialistType(Specialist::Cavalry))
      chance *= 1.5;

   // Frence artillery specialist commanding artillery
   if(cp->getSide() == 0 && cp->generic()->isArtillery() && cp->leader()->isSpecialistType(Specialist::Artillery))
      chance *= 1.5;

   int nHexes = 0;
   BobUtility::WhatCRadius::What cr = BobUtility::whatCommandRadius(d_batData->ob(), cp, nHexes);
   if(cr != BobUtility::WhatCRadius::OutOfRadius)
      chance *= 1.5;

   if(cr == BobUtility::WhatCRadius::OutOfRadius)
      chance *= .5;

   if(cp->morale() < 100)
      chance *= .5;

   if(cp->leader()->isSpecialist() && !cp->leader()->isCommandingType())
      chance *= .5;

   if(cp->getSide() == 1 && cp->getRank().sameRank(Rank_Division) && cp->generic()->isArtillery())
      chance *= .5;

#ifdef DEBUG
   bopLog.printf("Modified chance = %d", chance);
#endif

   return (CRandom::get(100) <= chance);
}

// See if leader 'thinks of' order himself
bool B_OrderProcImp::thoughOfOrder(const RefBattleCP& cp, BattleOrder* order)
{
   if(cp->targetList().closeRange() <= (8 * BattleMeasure::XYardsPerHex))
   {
      BattleOrder lastOrder;
      if(cp->lastOrderSent(&lastOrder))
      {
         if(cp->nextOrderCheck(d_batData->getTick()))
         {
            if(testLeader(cp))
            {
               // leader has though of order himself
               if(cp->getNewOrder(order))
               {
#ifdef DEBUG
                  bopLog.printf("Changing order on own initiative!");
#endif
                  return True;
               }
#ifdef DEBUG
               else
                  FORCEASSERT("No Order to get!");
#endif
            }
            else
            {
               // Test superiors
               RefBattleCP parent = cp->parent();
               CRefBattleCP cinc = BobUtility::getCinc(d_batData->ob(), cp->getSide());
               while(parent != NoBattleCP)
               {
                  // don;t test cinc
                  if(parent == cinc)
                     break;

                  if(testLeader(parent))
                  {
                     // leader has though of order himself
                     // if time taken is less than order time
                     int leaderR = BobUtility::leaderRadius(d_batData->ob(), parent);
                     int distFromParent = distanceFromUnit(cp, parent);

                     int mult = (leaderR <= distFromParent) ? 30 : 60;
                     int time = d_batData->getTick() + BattleMeasure::seconds(distFromParent * mult);

                     if(time < cp->orderTicks())
                     {
#ifdef DEBUG
                        bopLog.printf("%s has though of order for %s, will start early (at%d, was%d)",
                           parent->getName(), cp->getName(), time, static_cast<int>(cp->orderTicks()));
#endif
                        cp->orderTicks(time);
                        cp->setNextOrderCheck(BattleMeasure::seconds(CRandom::get(360, 600)) + d_batData->getTick());
                        return False;  // return false because order doesn't get set right away
                     }
                  }

                  parent = parent->parent();
               }

               cp->setNextOrderCheck(BattleMeasure::seconds(CRandom::get(360, 600)) + d_batData->getTick());
            }
         }
      }
   }

   return False;
}

bool B_OrderProcImp::procOrders(const RefBattleCP& cp)
{
   BattleOrder order;

   if(cp->getNewOrder(d_batData->getTick(), &order) ||
      thoughOfOrder(cp, &order))
   {
#ifdef DEBUG
      bopLog.printf("New Order arrives for %s %s from SHQ at tick %ld",
         scenario->getSideName(cp->getSide()),
         cp->getName(),
         d_batData->getTick());
#endif

      BattleTime::Tick tick = calcActionTime(cp);
      tick += order.startOrderIn();
      cp->setActingOrder(tick, order);

#ifdef DEBUG
      bopLog.printf("Will action order at tick %ld", tick);
#endif
   }

   if(cp->getActingOrder(d_batData->getTick(), &order))
   {
#ifdef DEBUG
      bopLog.printf("Order for %s %s is being actioned at tick %ld",
            scenario->getSideName(cp->getSide()), cp->getName(), d_batData->getTick());
      bopLog.printf("--------------------------------------------------");
      bopLog.printf("Order --------");
      bopLog.printf("Waypoints=%d", order.wayPoints().entries());
      if(order.mode() != cp->getCurrentOrder().mode())
           bopLog.printf("     Type          = %s", BattleOrderInfo::orderName(order.mode()));
      if(order.aggression() != cp->aggression())
           bopLog.printf("     Aggression    = %s", BattleOrderInfo::aggressionName(order.aggression()));
      if(order.posture() != cp->posture())
           bopLog.printf("     Posture       = %s", BattleOrderInfo::postureName(order.posture()));
      if(order.spFormation() != cp->getCurrentOrder().spFormation())
         bopLog.printf("     SPFormation   = %s", BattleOrderInfo::spFormationName(order.spFormation()));
      if(order.divFormation() != cp->getCurrentOrder().divFormation())
         bopLog.printf("     Div Formation = %s", BattleOrderInfo::divFormationName(order.divFormation()));
      if(order.manuever() != CPM_Hold)
         bopLog.printf("     Maneuver      = %s", BattleOrderInfo::manueverName(order.manuever()));
#endif
      // if this is a XXX or higher, set aggression, posture for whole command
      if( (GamePlayerControl::getControl(cp->getSide()) != GamePlayerControl::AI) &&
          (cp->getRank().isHigher(Rank_Division)) &&
          (cp->aggression() != order.aggression() || cp->posture() != order.posture() || order.mode() == BattleOrderInfo::Rout) )
      {
         for(RefBattleCP cCP = cp->child(); cCP != NoBattleCP; cCP = cCP->sister())
         {
            if(!cCP->hasQuitBattle())
            {
               BattleOrder cOrder;
               cCP->lastOrder(&cOrder);

               bool shouldSend = False;
               if(cCP->aggression() != order.aggression())
               {
                  cOrder.aggression(order.aggression());
                  shouldSend = True;
               }

               if(cCP->posture() != order.posture())
               {
                  cOrder.posture(order.posture());
                  shouldSend = True;
               }

               if(order.mode() == BattleOrderInfo::Rout)
               {
                  cOrder.mode(BattleOrderInfo::Rout);
                  shouldSend = True;
               }

               if(shouldSend)
               {
#ifdef DEBUG
                  bopLog.printf("%s is passing order on to subordinate %s", cp->getName(), cCP->getName());
#endif
                  sendBattleOrder(cCP, &cOrder);
               }
            }
         }
      }

      // Copy new aggression or posture (if any)
      // Copy to current order
      cp->setCurrentOrder(order);
      cp->aggression(order.aggression());
      cp->posture(order.posture());
   }

   // implement order if new order mode
   if(!cp->getCurrentOrder().started())
   {
      ASSERT(cp->getCurrentOrder().mode() < BattleOrderInfo::OM_HowMany);
#ifdef DEBUG
      bopLog.printf("Changing order-type from %s to %s",
         BattleOrderInfo::orderName(cp->orderMode()),
         BattleOrderInfo::orderName(cp->getCurrentOrder().mode()));
#endif
      return !s_orders[cp->getCurrentOrder().mode()]->start(d_batData, cp);
   }

   return False;
}

void B_OrderProcImp::updateOrders(const RefBattleCP& cp)
{
   if(!cp->getCurrentOrder().finished() &&
      s_orders[cp->orderMode()]->update(d_batData, cp))
   {
      cp->orderMode(BattleOrderInfo::Hold);
      cp->getCurrentOrder().mode(BattleOrderInfo::Hold);
      cp->getCurrentOrder().finishOrder();
   }
}


BattleTime::Tick B_OrderProcImp::calcActionTime(const RefBattleCP& cp)
{
  ASSERT(cp->leader() != 0);
  RefBattleCP parent;
  if(cp->parent() == NoBattleCP)
  {
   parent = BobUtility::getCinc(d_batData->ob(), cp->getSide());
  }
  else
   parent = cp->parent();

  // if the Instant Orders options is set, return current time
//  if(CampaignOptions::get(OPT_InstantOrders))// || cp == parent)
//    return d_batData->getTick();

  // get raw time from deployment table
  // value is in minutes to action
  // TODO: get this from scenario tables
  const int nValues = 4;
  static const UBYTE s_dTable[nValues] = {
     1,        // March
     2,        // Massed
     4,        // Deployed
     8         // Extended
  };

  ASSERT(cp->formation() < CPF_Last);
  int index = (cp->getRank().isHigher(Rank_Division)) ? 0 :
              (cp->columns() <= 2) ? maximum(0, cp->columns() - 1) :
              (cp->columns() <= 5) ? 2 : 3;

  BattleTime::Tick when = minutes(s_dTable[index]);

  // Let AI HQ cheat a little
  if(GamePlayerControl::getControl(cp->getSide()) == GamePlayerControl::AI &&
     cp->getRank().isHigher(Rank_Division))
  {
#ifdef DEBUG
      bopLog.printf("Raw action time = AI HQ - cheating");
#endif
     return d_batData->getTick();
  }
#ifdef DEBUG
  bopLog.printf("Raw action time = %d minutes", s_dTable[cp->formation()]);
#endif

  //  modifiers
  //  cavalry unit commanded by cavalry specialist ( X .5)
  //  or XXX is specialist
  //  ASSERT(cp->parent() != NoBattleCP);
  ASSERT(parent->leader() != 0);
  if( (cp->generic()->isCavalry() && !cp->generic()->isCombinedArms()) &&
      ( (cp->leader()->isCommandingType() || parent->leader()->isSpecialistType(Specialist::Cavalry)) ) )
  {
    when *= .5;
  }

  // unlimbered arty not commanded by specialist (X 2)
  // limbered arty commanded by specialist (X .5)
  if( (cp->generic()->isArtillery()) &&
      (!cp->generic()->isCombinedArms()) )
   {
    if(!cp->leader()->isCommandingType())
    {
      when *= 2;
    }

    else 
    {
      when *= .5;
    }
  }

  // Random modifier
  // combine unit response with leader staff to get index
  UWORD rsValue = BobUtility::unitResponse(d_batData->ob(), cp) + cp->leader()->getStaff();

  // TODO: if a senior leader in Chain of Command is attached to unit,
  // add his tactical abilit X .2  to rsValue

  // convert combined value to local enum for indexing
  enum {
    RS102,
    RS204,
    RS306,
    RS408,
    RS510,

    RS_HowMany
  } value = (rsValue < 102) ? RS102 :
            (rsValue < 204) ? RS204 :
            (rsValue < 306) ? RS306 :
            (rsValue < 408) ? RS408 : RS510;

  // TODO: move this table to scenario files
  const int c_nDieValues = 10;  // number of possible die roll values
  const int c_dBase = 100;
  static const UBYTE s_rTable[RS_HowMany][c_nDieValues] = {
     { 133, 133, 133, 133, 133, 133, 133, 117, 117, 100 }, // RS102
     { 133, 133, 133, 117, 117, 117, 117, 100, 100,  83 }, // RS204 etc...
     { 133, 117, 117, 100, 100, 100, 100,  83,  83,  67 },
     { 117, 100, 100,  83,  83,  83,  83,  67,  67,  67 },
     { 100,  83,  83,  67,  67,  67,  67,  67,  67,  67 }
  };

  // get random value
  int dValue = CRandom::get(10);

  // multiply time by table-value  / base
  ASSERT(dValue < c_nDieValues);
  ASSERT(value < RS_HowMany);
  when = (when * s_rTable[value][dValue]) / c_dBase;

  // A cheating bonus for the AI
  if(GamePlayerControl::getControl(cp->getSide()) == GamePlayerControl::AI)
      when /= 3;

#ifdef DEBUG
  bopLog.printf("Modified action time = %d minutes", minutesPerTick(when));
#endif
   // add order time
   // add current time and return
   when += d_batData->getTick();
   return when;
}

/*-------------------------------------------------------------
 * Client-access
 */

B_OrderProcInt::B_OrderProcInt(RPBattleData batData) :
  d_op(new B_OrderProcImp(batData))
{
  ASSERT(d_op);
}

B_OrderProcInt::~B_OrderProcInt()
{
  if(d_op)
    delete d_op;
}

void B_OrderProcInt::process()
{
  if(d_op)
    d_op->process();
}


}; // end namespace

/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.3  2001/07/02 13:18:24  greenius
 * Battle, Campaign, Combined game and Campaign Editor now compile and run... mostly
 *
 * Revision 1.2  2001/06/18 08:25:44  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
