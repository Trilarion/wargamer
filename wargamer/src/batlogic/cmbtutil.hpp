/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
#ifndef CMBTUTIL_HPP
#define CMBTUTIL_HPP

#include "bl_dll.h"
#include "batdata.hpp"
#include "batunit.hpp"
#include "bobdef.hpp"
#include "hexdata.hpp"

class HexList;
class BattleGameInterface;

namespace Combat_Util {
// terrain cover,
enum CoverType {
   None,
   Light,
   Medium,
   Heavy,
   SuperHeavy,

   Cover_HowMany
};


BATLOGIC_DLL const char* coverTypeName(CoverType ct);
enum Situation {
   Sit_FireCombat,
   Sit_ShockCombat,
   Sit_HowMany
};
BATLOGIC_DLL CoverType hexCoverType(const BattleMeasure::HexCord& hex, RCPBattleData bd, bool checkPath, Situation sit = Sit_FireCombat, BattleMeasure::HexPosition::Facing facing = BattleMeasure::HexPosition::Facing_Undefined);
GraphicalTerrainEnum terrainType(RPBattleData bd, const RefBattleCP& cp);
   // return type of terrain unit is in
bool crossingRiver(RPBattleData bd, const RefBattleCP& cp);
bool crossingStream(RPBattleData bd, const RefBattleCP& cp);
bool crossingSunkenStream(RPBattleData bd, const RefBattleCP& cp);
bool behindWall(RPBattleData bd, const RefBattleCP& cp);
CoverType coverType(const RefBattleCP& cp, RCPBattleData bd, bool checkPath = True);
   // return average covertype a unit is in
BattleTerrainHex::Height averageHeight(const RefBattleCP& cp, RPBattleData bd);
   // return average height for a unit
RefBattleCP bestShockTarget(const RefBattleCP& cp);
   // return best target in shocl list
bool nonRoutingFriendsNear(RPBattleData bd, const RefBattleCP& cp, const int nHexes, bool noSiblings = False);
   // return true if non-routing friendly units of cp are within nHexes
bool enemyUnderHex(RPBattleData bd, const BattleMeasure::HexCord& hex, const RefBattleCP& cp);
//BATLOGIC_DLL bool lineOfSight(RPBattleData bd, const BattleMeasure::HexCord& srcHex, const BattleMeasure::HexCord& destHex, HexList* hl, RefBattleCP cp = NoBattleCP, bool* hasFriendly = 0);
BATLOGIC_DLL int getRange(RCPBattleData bd, const CRefBattleCP& cp);
BATLOGIC_DLL bool canBombard(RCPBattleData bd, const CRefBattleCP& cp, const CRefBattleCP& targetCP);
// Unchecked
bool artillerySupported(RPBattleData bd, const RefBattleCP& cp);
// End
};  // end namespace

#endif

/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.2  2001/06/18 08:25:44  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
