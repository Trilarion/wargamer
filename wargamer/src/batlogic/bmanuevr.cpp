/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
#include "stdinc.hpp"
#include "bmanuevr.hpp"
#include "batdata.hpp"
#include "batarmy.hpp"
#include "bobutil.hpp"
#include "hexdata.hpp"
#include "bobiter.hpp"
#include "hexmap.hpp"
#include "moveutil.hpp"
#include "broute.hpp"
//#include "fsalloc.hpp"
#ifdef DEBUG
#include "random.hpp"
#include "clog.hpp"
LogFile mLog("BManueverLog.log");
#endif
#include <utility>

#ifdef _MSC_VER
using namespace std::rel_ops;
#endif

namespace B_Logic {

/*------------------------------------------------------------------
 * Abstract Manuever class
 *
 * Note: Manuevers are 'Wheel Right', 'Wheel Left', etc.
 */

class ManueverBase {
      PBattleData d_batData;
      BattleOB* d_ob;

  public:

    ManueverBase(RPBattleData bd) :
      d_batData(bd),
      d_ob(d_batData->ob()) {}
    virtual ~ManueverBase() {}

    void startManuever(const RefBattleCP& cp);
    virtual void manueverSP(const RefBattleCP& cp, HexArea& area) = 0;

    static ManueverBase* allocate(RPBattleData bd, CPManuever m);

  protected:

    PBattleData batData() const { return d_batData; }
    BattleOB* ob() const { return d_ob; }

    bool startList(const RefBattleCP& cp, BattleCP::HexOffset ho,
       const int spCount, const HexPosition::Facing f);
};

bool ManueverBase::startList(const RefBattleCP& cp, BattleCP::HexOffset ho,
  const int spCount, const HexPosition::Facing f)
{
  bool shouldShift = ( /*(ho == BattleCP::HO_Left) && */
                       (cp->columns() > 1) &&
                       (cp->rows() > 1) &&
                       (cp->whichWay() ==  ho) );

  for(int r = 0; r < cp->rows(); r++)
  {
    for(int c = 0; c < cp->columns(); c++)
    {
      DeployItem* di = cp->currentDeployItem(c, r);
      ASSERT(di);
      if(!di || !di->active())
        continue;

      di->d_sp->setMoving(True);
      di->d_sp->routeList().reset();

      // if second row and we are offset left, shift right
      Boolean shift = shouldShift; //( (shouldShift) && (r == 1) );
#if 1
      // Don't shift if we have an odd number of SP and...
      if( (shift) &&
          (cp->formation() == CPF_Deployed) &&
          ((spCount % 2) != 0) )
      {
        ASSERT(cp->rows() > 1);
        DeployItem* di1 = cp->currentDeployItem(0, cp->rows() - 2);
        ASSERT(di1);
        DeployItem* di2 = cp->currentDeployItem(0, cp->rows() - 1);
        ASSERT(di2);
        if(!di1 || !di2)
         continue;

        bool adj = adjacentToSameHex(d_batData, di1->d_hex, di2->d_hex,
          rightRear(cp->facing()), leftFlank(cp->facing()));

        shift = (ho == BattleCP::HO_Left) ? adj : !adj;
#ifdef DEBUG
        if(!shift)
        {
          bool result = checkAlignment(d_batData, di1->d_hex, di2->d_hex, cp->facing());
          ASSERT(result);
        }
#endif
      }
#endif
      if(shift && cp->formation() <= CPF_Massed && r > 1)
      {
          DeployItem* di2 = cp->currentDeployItem(c, r - 1);
          ASSERT(di2);
          if(!di2)
            continue;
          ASSERT(di2->active());
          if(di2->active())
          {
            HexCord ourHex = di->d_sp->hex();
            HexCord lastHex = (di2->d_sp->nextHex()) ? di2->d_sp->routeList().getLast()->d_hex : di2->d_sp->hex();
            shift = !adjacentHex(ourHex, lastHex);
          }
      }

      if(shift && r >= 1)
      {
        HexCord::HexDirection hd = (ho ==  BattleCP::HO_Left) ?
          leftFlank(cp->facing()) : rightFlank(cp->facing());

        HexCord nextHex;
        if(batData()->moveHex(di->d_sp->hex(), hd, nextHex))
        {
          HexItem* hi = new HexItem(nextHex, f);
          ASSERT(hi);

          di->d_sp->routeList().append(hi);
        }
        else
          FORCEASSERT("Hex not found");
      }

      // other wise stay at the same place but change facing
      else
      {
        HexPosition::Facing newFace;
        if(cp->formation() == CPF_March)
        {
          newFace = (r == 0) ? f : di->d_sp->facing();
        }
        else if(cp->formation() == CPF_Massed)
        {
          newFace = (r <= 1) ? f : di->d_sp->facing();
        }
        else
          newFace = f;

//      if(shouldShift)
        if(shift)
          di->d_sp->routeList().newItem(di->d_sp->hex(), newFace);

        di->d_sp->routeList().newItem(di->d_sp->hex(), newFace);
      }
    }
  }

  cp->setMoving(True);
  return True;
}

/*---------------------------------------------------------------------
 * Derived manuever classes
 */

// 'Forward' ---------------------------------------------------------
class Forward : public ManueverBase {
  public:
    Forward(RPBattleData bd) : ManueverBase(bd) {}
    ~Forward() {}

    void manueverSP(const RefBattleCP& cp, HexArea& area);
};

void Forward::manueverSP(const RefBattleCP& cp, HexArea& area)
{
#if 0
   // useful constants
   const HexPosition::Facing facing = cp->facing();

   const int spCount = cp->spCount();
   ASSERT(spCount > 0);

   /*
    * which direction are we going to go
    */

   HexCord::HexDirection d = (cp->whichWay() == BattleCP::HO_Left) ?
       rightFront(facing) : leftFront(facing);

   /*
    * go through each front row sp and and assign nex hex
    * all trailing sp's follow the sp in front of them
    */

   for(int c = 0; c < cp->columns(); c++)
   {
     // get dest hex for first unit
     int r = 0;

     DeployItem& di = cp->deployItem(c, r);

     if(!di.active())
       continue;

     ASSERT(di.d_sp != NoBattleSP);
     di.d_sp->routeList().reset();

     HexCord hex;
     if(batData()->moveHex(di.d_sp->hex(), d, hex))
     {
       HexItem* hi = new HexItem(hex, facing);
       ASSERT(hi);
       di.d_sp->routeList().append(hi);
       area.update(hex);

       // now all following rows just trail along
       hex = di.d_sp->hex();
       while(++r < cp->rows())
       {
         DeployItem& di = cp->deployItem(c, r);

         if(!di.active())
           continue;

         ASSERT(di.d_sp != NoBattleSP);
         di.d_sp->routeList().reset();

         // add current hex to list
         HexItem* hi = new HexItem(hex);
         ASSERT(hi);
         di.d_sp->routeList().append(hi);

         hex = di.d_sp->hex();
       }
     }
   }

   cp->whichWay((cp->whichWay() == BattleCP::HO_Left) ? BattleCP::HO_Right : BattleCP::HO_Left);
#endif
}

// 'Wheel Right' ---------------------------------------------------------
class WheelRight : public ManueverBase {
  public:
    WheelRight(RPBattleData bd) : ManueverBase(bd) {}
    ~WheelRight() {}

    void manueverSP(const RefBattleCP& cp, HexArea& area);
};

void WheelRight::manueverSP(const RefBattleCP& cp, HexArea& area)
{
   const int spCount = cp->spCount();
   ASSERT(spCount > 0);

   int moveForward = cp->columns() - 1;
   HexCord::HexDirection hd = rightFront(cp->facing());

   // reset unit facing
   HexPosition::Facing f = rightTurn(cp->facing()); //clockWiseFacing(facing);
   startList(cp, BattleCP::HO_Left, spCount, f);

   // if we are offset to the left we must shift the first row to the
   // right 1 hex
   if(cp->formation() == CPF_March || cp->columns() == 1)
   {
   }
   else if(cp->formation() == CPF_Massed)
   {
      DeployItem* di = cp->currentDeployItem(0, 0);
      ASSERT(di);
      ASSERT(di->active());
      if(di && di->active())
      {
         HexCord hex = (di->d_sp->nextHex()) ? di->d_sp->nextHex()->d_hex : di->d_sp->hex();
         HexCord nextHex;

         if(batData()->moveHex(hex, hd, nextHex))
         {
            di->d_sp->routeList().newItem(nextHex, f);

            for(int r = 1; r < cp->rows(); r++)
            {
               DeployItem* di1 = cp->currentDeployItem(0, r);
               ASSERT(di1);
               if(!di1 || !di1->active())
                  continue;

               HexPosition::Facing newFace = (r == 1) ? f : cp->facing();
               di1->d_sp->routeList().newItem(hex, newFace);
               hex = (di1->d_sp->nextHex()) ? di1->d_sp->nextHex()->d_hex : di1->d_sp->hex();
            }
         }
      }
   }
   else
   {
      ASSERT(cp->formation() >= CPF_Deployed);
      ASSERT(cp->columns() >= 2);

      // now plot route for all SP. All other rows are
      // will follow
      for(int r = 0; r < cp->rows(); r++)
      {
         for(int c = 0; c < cp->columns(); c++)
         {
            DeployItem* di = cp->currentDeployItem(c, r);
            ASSERT(di);
            if(!di || !di->active())
               continue;

            HexCord hex = (di->d_sp->nextHex()) ? di->d_sp->nextHex()->d_hex : di->d_sp->hex();

            int howMany = moveForward--;
            if( (r > 0) &&
                (r == cp->rows() - 1) &&
                ((spCount % 2) != 0) &&
                (cp->whichWay() != BattleCP::HO_Right) )
            {
               howMany--;
            }
            ASSERT(howMany >= 0);

            while(howMany--)
            {
               HexCord nextHex;
               if(batData()->moveHex(hex, hd, nextHex))
               {
                  di->d_sp->routeList().newItem(nextHex, f);

                  if(howMany == 0)
                     area.update(nextHex);
                  else
                     hex = nextHex;
               }

            }
         }

         moveForward = cp->columns() - 1;
      }
   }

   cp->setFacing(f);
#if 0
   RefBattleCP attachedTo = BobUtility::getAttachedLeader(batData(), cp);
   if(attachedTo != NoBattleCP)
   {
     plotHQRoute(batData(), attachedTo);
     attachedTo->setMoving(True);
   }
#endif
}

// 'Wheel left' ----------------------------------------------------
class WheelLeft : public ManueverBase {
  public:
    WheelLeft(RPBattleData bd) : ManueverBase(bd) {}
    ~WheelLeft() {}

    void manueverSP(const RefBattleCP& cp, HexArea& area);
};


void WheelLeft::manueverSP(const RefBattleCP& cp, HexArea& area)
{
   const int spCount = cp->spCount();
   ASSERT(spCount > 0);

   int moveNow = 0;
   HexCord::HexDirection hd = leftFront(cp->facing());

   // reset unit facing
   HexPosition::Facing f = leftTurn(cp->facing()); //clockWiseFacing(facing);

   startList(cp, BattleCP::HO_Right, spCount, f);

   if(cp->formation() == CPF_March || cp->columns() == 1)
   {
   }
   else if(cp->formation() == CPF_Massed)
   {
      DeployItem* di = cp->currentDeployItem(1, 0);
      ASSERT(di);
      ASSERT(di->active());
      if(di && di->active())
      {
         HexCord hex = (di->d_sp->nextHex()) ? di->d_sp->nextHex()->d_hex : di->d_sp->hex();
         HexCord nextHex;

         if(batData()->moveHex(hex, hd, nextHex))
         {
            di->d_sp->routeList().newItem(nextHex, f);

            for(int r = 1; r < cp->rows(); r++)
            {
               DeployItem* di = cp->currentDeployItem(1, r);
               if(!di || !di->active())
                  continue;

               HexPosition::Facing newFace = (r == 1) ? f : cp->facing();

               di->d_sp->routeList().newItem(hex, newFace);
               hex = (di->d_sp->nextHex()) ? di->d_sp->nextHex()->d_hex : di->d_sp->hex();
            }
         }
      }
   }
   else
   {
      ASSERT(cp->formation() >= CPF_Deployed);
      ASSERT(cp->columns() >= 2);

      // if we are offset to the right we must shift the first row to the
      // left 1 hex
      // now plot route for all SP. All other rows
      // will follow
      for(int r = 0; r < cp->rows(); r++)
      {
         for(int c = 0; c < cp->columns(); c++)
         {
            DeployItem* di = cp->currentDeployItem(c, r);
            ASSERT(di);
            if(!di || !di->active())
               continue;

            HexCord hex = (di->d_sp->nextHex()) ? di->d_sp->nextHex()->d_hex : di->d_sp->hex();

            int howMany = moveNow++;
            if( (cp->rows() > 1) &&
                (r > 0) &&
                (r == cp->rows() - 1) &&
                ((spCount % 2) != 0) )
            {
               ASSERT(cp->rows() > 1);
               DeployItem* di1 = cp->currentDeployItem(0, cp->rows() - 2);
               ASSERT(di1);
               DeployItem* di2 = cp->currentDeployItem(0, cp->rows() - 1);
               ASSERT(di2);
               if(!di1 || !di2)
                  continue;

               if(adjacentToSameHex(batData(), di1->d_hex, di2->d_hex,
                   rightRear(cp->facing()), leftFlank(cp->facing())))
               {
                  howMany++;
               }
            }

            ASSERT(howMany >= 0);
            while(howMany--)
            {
               HexCord nextHex;
               if(batData()->moveHex(hex, hd, nextHex))
               {
                  di->d_sp->routeList().newItem(nextHex, f);

                  if(howMany == 0)
                     area.update(nextHex);
                  else
                     hex = nextHex;
               }
            }
         }

         moveNow = 0;
      }
   }

   cp->setFacing(f);
#if 0
   RefBattleCP attachedTo = BobUtility::getAttachedLeader(batData(), cp);
   if(attachedTo != NoBattleCP)
   {
     plotHQRoute(batData(), attachedTo);
     attachedTo->setMoving(True);
   }
#endif
}

// 'Reverse Wheel Right' ---------------------------------------------------------
class ReverseWheelRight : public ManueverBase {
  public:
    ReverseWheelRight(RPBattleData bd) : ManueverBase(bd) {}
    ~ReverseWheelRight() {}

    void manueverSP(const RefBattleCP& cp, HexArea& area);
};

void ReverseWheelRight::manueverSP(const RefBattleCP& cp, HexArea& area)
{
   const int spCount = cp->spCount();
   ASSERT(spCount > 0);
   int moveBack = cp->columns() - 1;
   HexCord::HexDirection hd = leftRear(cp->facing());

   // reset unit facing
   HexPosition::Facing f = rightTurn(cp->facing()); //clockWiseFacing(facing);
   startList(cp, BattleCP::HO_Left, spCount, f);

   if(cp->formation() == CPF_March || cp->columns() == 1)
     ;
   else if(cp->formation() == CPF_Massed)
     ;
   else
   {
     ASSERT(cp->formation() >= CPF_Deployed);
     ASSERT(cp->columns() >= 2);

     // now plot route for all SP. All other rows are
     // will follow
     for(int r = cp->rows() - 1; r >= 0; r--)
     {
       for(int c = cp->columns() - 1; c >= 0; c--)
       {
         DeployItem* di = cp->currentDeployItem(c, r);
         ASSERT(di);
         if(!di || !di->active())
           continue;

         HexCord hex = (di->d_sp->nextHex()) ? di->d_sp->nextHex()->d_hex : di->d_sp->hex();

         int howMany = moveBack--;

         if( (r > 0) &&
             (r == cp->rows() - 1) &&
             ((spCount % 2) != 0) &&
             (cp->whichWay() == BattleCP::HO_Right) )
         {
           howMany--;
         }

         ASSERT(howMany >= 0);

         while(howMany--)
         {
           HexCord nextHex;
           if(batData()->moveHex(hex, hd, nextHex))
           {
             di->d_sp->routeList().newItem(nextHex, f);

             if(howMany == 0)
               area.update(nextHex);
             else
               hex = nextHex;
           }

         }
       }

       moveBack = cp->columns() - 1;
     }
   }

   cp->setFacing(f);
#if 0
   RefBattleCP attachedTo = BobUtility::getAttachedLeader(batData(), cp);
   if(attachedTo != NoBattleCP)
   {
     plotHQRoute(batData(), attachedTo);
     attachedTo->setMoving(True);
   }
#endif
}

// 'Reverse Wheel Left' ---------------------------------------------------------
class ReverseWheelLeft : public ManueverBase {
  public:
    ReverseWheelLeft(RPBattleData bd) : ManueverBase(bd) {}
    ~ReverseWheelLeft() {}

    void manueverSP(const RefBattleCP& cp, HexArea& area);
};

void ReverseWheelLeft::manueverSP(const RefBattleCP& cp, HexArea& area)
{
   const int spCount = cp->spCount();
   ASSERT(spCount > 0);
   int moveNow = 0;
   HexCord::HexDirection hd = rightRear(cp->facing());

   // reset unit facing
   HexPosition::Facing f = leftTurn(cp->facing()); //clockWiseFacing(facing);

   startList(cp, BattleCP::HO_Right, spCount, f);

   if(cp->formation() == CPF_March || cp->columns() == 1)
   {
   }
   else if(cp->formation() == CPF_Massed)
   {
   }
   else
   {
     ASSERT(cp->formation() >= CPF_Deployed);
     ASSERT(cp->columns() >= 2);

     // if we are offset to the right we must shift the first row to the
     // left 1 hex
     // now plot route for all SP. All other rows
     // will follow
     for(int r = cp->rows() - 1; r >= 0; r--)
     {
       for(int c = cp->columns() - 1; c >= 0; c--)
       {
         DeployItem* di = cp->currentDeployItem(c, r);
         ASSERT(di);
         int howMany = moveNow++;
         if(!di || !di->active())
           continue;

         HexCord hex = (di->d_sp->nextHex()) ? di->d_sp->nextHex()->d_hex : di->d_sp->hex();

         if( (cp->rows() > 1) &&
             (r > 0) &&
             (r == cp->rows() - 1) &&
             ((spCount % 2) != 0) )
         {
           ASSERT(cp->rows() > 1);
           DeployItem* di1 = cp->currentDeployItem(0, cp->rows() - 2);
           ASSERT(di1);
           DeployItem* di2 = cp->currentDeployItem(0, cp->rows() - 1);
           ASSERT(di1);
           if(!di1 || !di2)
             continue;

           if(adjacentToSameHex(batData(), di1->d_hex, di2->d_hex,
                   rightRear(cp->facing()), leftFlank(cp->facing())))
           {
             howMany--;
           }
         }

         ASSERT(howMany >= 0);
         while(howMany--)
         {
           HexCord nextHex;
           if(batData()->moveHex(hex, hd, nextHex))
           {
             di->d_sp->routeList().newItem(nextHex, f);

             if(howMany == 0)
               area.update(nextHex);
             else
               hex = nextHex;
           }
         }
       }

       moveNow = 0;
     }
   }

   cp->setFacing(f);
#if 0
   RefBattleCP attachedTo = BobUtility::getAttachedLeader(batData(), cp);
   if(attachedTo != NoBattleCP)
   {
     plotHQRoute(batData(), attachedTo);
     attachedTo->setMoving(True);
   }
#endif
}

// 'Sidestep Right' ----------------------------------------------------
class SideStepRight : public ManueverBase {
  public:
    SideStepRight(RPBattleData bd) : ManueverBase(bd) {}
    ~SideStepRight() {}

    void manueverSP(const RefBattleCP& cp, HexArea& area);
};

void SideStepRight::manueverSP(const RefBattleCP& cp, HexArea& area)
{
// Unchecked
#if 0
   // useful constants
   const HexPosition::Facing facing = cp->facing();

   const int spCount = cp->spCount();
   ASSERT(spCount > 0);

   /*
    * which direction are we going to go
    */

   HexCord::HexDirection d = rightFlank(facing);

   /*
    * each sp moves 1 space to the right
    */

   for(int r = 0; r < cp->rows(); r++)
   {
     // get dest hex for first unit
     int c = cp->columns() - 1;

     DeployItem* di = &cp->deployItem(c, r);
     while(!di->active() && c > 0)
     {
       di = &cp->deployItem(--c, r);
     }

     ASSERT(di->d_sp != NoBattleSP);
     di->d_sp->routeList().reset();

     HexCord hex;
     if(batData()->moveHex(di->d_sp->hex(), d, hex))
     {
       HexItem* hi = new HexItem(hex);
       ASSERT(hi);
       di->d_sp->routeList().append(hi);
       area.update(hex);

       // now all following rows just trail along
       hex = di->d_sp->hex();
       while(c--)
       {
         DeployItem& di = cp->deployItem(c, r);
         if(!di.active())
           continue;

         ASSERT(di.d_sp != NoBattleSP);
         di.d_sp->routeList().reset();

         // add current hex to list
         HexItem* hi = new HexItem(hex);
         ASSERT(hi);
         di.d_sp->routeList().append(hi);

         hex = di.d_sp->hex();
       }
     }
   }
#endif
// End
}

// 'Sidestep Left' ----------------------------------------------------
class SideStepLeft : public ManueverBase {
  public:
    SideStepLeft(RPBattleData bd) : ManueverBase(bd) {}
    ~SideStepLeft() {}

    void manueverSP(const RefBattleCP& cp, HexArea& area);
};

void SideStepLeft::manueverSP(const RefBattleCP& cp, HexArea& area)
{
// Unchecked
#if 0
   // useful constants
   const HexPosition::Facing facing = cp->facing();

   const int spCount = cp->spCount();
   ASSERT(spCount > 0);

   /*
    * which direction are we going to go
    */

   HexCord::HexDirection d = leftFlank(facing);

   /*
    * each sp moves 1 space to the right
    */

   for(int r = 0; r < cp->rows(); r++)
   {
     // get dest hex for first unit
     int c = 0;

     DeployItem& di = cp->deployItem(c, r);
     if(!di.active())
       continue;

     ASSERT(di.d_sp != NoBattleSP);
     di.d_sp->routeList().reset();

     HexCord hex;
     if(batData()->moveHex(di.d_sp->hex(), d, hex))
     {
       HexItem* hi = new HexItem(hex, facing);
       ASSERT(hi);
       di.d_sp->routeList().append(hi);
       area.update(hex);

       // now all following rows just trail along
       hex = di.d_sp->hex();
       while(++c < cp->columns())
       {
         DeployItem& di = cp->deployItem(c, r);
         if(!di.active())
           continue;

         ASSERT(di.d_sp != NoBattleSP);
         di.d_sp->routeList().reset();

         // add next hex to list
         hi = new HexItem(hex);
         ASSERT(hi);
         di.d_sp->routeList().append(hi);
         area.update(hex);

         hex = di.d_sp->hex();
       }
     }
   }
#endif
// End
}

// 'About face' ----------------------------------------------------
class AboutFace : public ManueverBase {
  public:
    AboutFace(RPBattleData bd) : ManueverBase(bd) {}
    ~AboutFace() {}

    void manueverSP(const RefBattleCP& cp, HexArea& area);
};

void AboutFace::manueverSP(const RefBattleCP& cp, HexArea& area)
{
  // Basically everybody just does a 180 facing turn.
  // The deployment map will have to be adjusted accordingly
  //
  // If we have an odd number of SP, then we will have to move the odd row up
  //   i.e. asuming a North Facing:  1  2  3  4
  //                                 5  6  7
  //
  //        goes to South Facing:    1  2  3
  //                                 5  6  7  4
  //

  // get new facing
  HexPosition::Facing newFacing = oppositeFace(cp->facing());

  // adjust deployment map, flip floping deployment map position
  //int newC = (cp->formation() == CPF_Extended && cp->columns() % 2 != 0) ? cp->columns() : cp->columns() - 1;
  int newC = (cp->formation() == CPF_Extended && cp->columns() % 2 != 0 && cp->mapSize() > 1) ? cp->columns() : cp->columns() - 1;
  int newR = cp->rows() - 1;
  const int spCount = cp->spCount();
  bool adj = False;
  if(spCount % 2 != 0 && cp->columns() > 1 && cp->rows() > 1)
  {
    DeployItem* di1 = cp->currentDeployItem(0, cp->rows() - 2);
    ASSERT(di1);
    DeployItem* di2 = cp->currentDeployItem(0, cp->rows() - 1);
    ASSERT(di2);
    if(!di1 || !di2)
      return;

    adj = adjacentToSameHex(batData(), di1->d_hex, di2->d_hex,
       rightRear(cp->facing()), leftFlank(cp->facing()));
  }

  //vector<DeployItem>& map = cp->deployMap();
  for(int r = 0; r < cp->rows(); r++, newR--)
  {
    for(int c = 0; c < cp->columns(); c++, newC--)
    {
      //int index = (cp->columns() * r) + c;
      //ASSERT(index < map.size());

      DeployItem* di = cp->currentDeployItem(c, r);//map[index];
      ASSERT(di);
//    DeployItem& di = cp->deployItem(c, r);
      if(di && di->active())
      {
        HexCord hex = di->d_sp->hex();

        int row = newR;
        int col = newC;
        if(spCount % 2 != 0 &&
           cp->columns() > 1)
        {
          if(!adj)
          {
            if(row - 1 >= 0 && c == cp->columns() - 1)
            {
              row--;

              ASSERT(r + 1 < cp->rows());
              DeployItem* nDI2 = cp->currentDeployItem(cp->columns() - 2, r + 1);//cp->rows() - 1);
              ASSERT(nDI2);
              ASSERT(nDI2->active());

              if(!nDI2 || !nDI2->active())
                continue;

              HexCord::HexDirection hd = rightFlank(cp->facing());

              if(batData()->moveHex(nDI2->d_hex, hd, hex))
              {
//              cp->addToMap(di.d_wantSP, &hex, newC, newR);
              }
              else
              {
                cp->syncMapToSP();
                return;
              }

            }

            if(col - 1 >= 0 && r == 0)
            {
              col--;
              if(cp->formation() == CPF_Massed)
              {
                 HexCord::HexDirection hd = rightFlank(cp->facing());
                 HexCord thisHex = hex;
                 if(batData()->moveHex(thisHex, hd, hex))
                 {
                 }
                 else
                 {
                   cp->syncMapToSP();
                   return;
                 }

              }
            }
          }

          else
          {
            if(row - 1 >= 0 && c == 0)
            {
              row--;

              DeployItem* nDI2 = cp->currentDeployItem(0, cp->rows() - 1);
              ASSERT(nDI2);
              ASSERT(nDI2->active());
              if(!nDI2 || !nDI2->active())
                continue;

              HexCord::HexDirection hd = leftFlank(cp->facing());

              if(batData()->moveHex(nDI2->d_hex, hd, hex))
              {
//              cp->addToMap(di.d_wantSP, &hex, newC, newR);
              }
              else
              {
                cp->syncMapToSP();
                return;
              }

            }

            if(col - 1 >= 0 && r == cp->rows() - 1)
              col--;
          }

        }

        cp->addToMap(di->d_sp, &hex, col, row);
//      spCount++;
      }
    }

    newC = cp->columns() - 1;
  }

  // rewind and go through each Sp again, plotting route if needed
  for(r = 0; r < cp->rows(); r++)
  {
    for(int c = 0; c < cp->columns(); c++)
    {
      DeployItem* di = cp->currentDeployItem(c, r);
      ASSERT(di);
      if(!di)
         continue;

      if(di->d_wantSP != NoBattleSP)
      {
        di->d_wantSP->routeList().reset();
        di->d_wantSP->routeList().newItem(di->d_wantSP->hex(), newFacing);

        if(di->d_wantSP->hex() != di->d_wantHex)
        {
          di->d_wantSP->routeList().newItem(di->d_wantHex, newFacing);
        }

//      di.d_sp->changeFacing(newFacing);
        di->d_wantSP->setMoving(True);
      }
    }
  }

  cp->lockMap();

//#if 0
  // clean up map if odd number of visible SP
  if(spCount % 2 != 0)
  {
     int c = (cp->formation() == CPF_Extended && cp->columns() % 2 != 0) ? cp->columns() : cp->columns() - 1;
    cp->addToMap(NoBattleSP, 0, c, cp->rows() - 1);
    cp->lockMap();
  }
//#endif
  cp->syncMapToSP();
  cp->setFacing(newFacing);

}

// 'Hold' ----------------------------------------------------
class Hold : public ManueverBase {
  public:
    Hold(RPBattleData bd) : ManueverBase(bd) {}
    ~Hold() {}

    void manueverSP(const RefBattleCP& cp, HexArea& area);
};

void Hold::manueverSP(const RefBattleCP& cp, HexArea& area)
{
}

void ManueverBase::startManuever(const RefBattleCP& cp)
{
   if(cp->sp() != NoBattleSP)
   {
      HexPosition::Facing oldFace = cp->facing();
      HexArea area;
      manueverSP(cp, area);

      // go through each route an insure we can manuever
      bool clearAll = False;
      for(std::vector<DeployItem>::iterator di = cp->mapBegin();
          di != cp->mapEnd();
          ++di)
      {
         if(di->active())
         {
            SListIter<HexItem> iter(&di->d_sp->routeList());
            while(++iter)
            {
               if(!canHexBeOccupied(d_batData, cp, iter.current()->d_hex, True, False) ||
#ifdef DEBUG
                  !hexNotOccupied(d_batData, iter.current()->d_hex, 0, cp, mLog))
#else
                  !hexNotOccupied(d_batData, iter.current()->d_hex, 0, cp))
#endif
               {
                  clearAll = True;
                  break;
               }
            }

            if(clearAll)
               break;
         }
      }

      if(clearAll)
      {
         for(std::vector<DeployItem>::iterator di = cp->mapBegin();
             di != cp->mapEnd();
             ++di)
         {
            if(di->active())
            {
               di->d_sp->routeList().reset();
               di->d_sp->setMoving(False);
            }
         }

         cp->moveMode(BattleCP::Holding);
         cp->setFacing(oldFace);
      }
   }
}

/*-----------------------------------------------------------
 * Allocator for derived classes
 */

ManueverBase* ManueverBase::allocate(RPBattleData bd, CPManuever m)
{
  if(m == CPM_Forward)
    return new Forward(bd);

  else if(m == CPM_WheelR)
    return new WheelRight(bd);

  else if(m == CPM_WheelL)
    return new WheelLeft(bd);

  else if(m == CPM_RWheelR)
    return new ReverseWheelRight(bd);

  else if(m == CPM_RWheelL)
    return new ReverseWheelLeft(bd);

  else if(m == CPM_SideStepR)
    return new SideStepRight(bd);

  else if(m == CPM_SideStepL)
    return new SideStepLeft(bd);

  else if(m == CPM_AboutFace)
    return new AboutFace(bd);

  else if(m == CPM_Hold)
    return new Hold(bd);

  else
    return 0;
}

/*----------------------------------------------------------------
 * Client access
 */

Manuever_Int::Manuever_Int(RPBattleData batData) :
  d_manuevers(new ManueverBase*[CPM_Last])
{
  ASSERT(d_manuevers);
  for(CPManuever cm = CPM_First; cm < CPM_Last; INCREMENT(cm))
  {
    d_manuevers[cm] = ManueverBase::allocate(batData, cm);
    ASSERT(d_manuevers[cm]);
  }
}

Manuever_Int::~Manuever_Int()
{
  for(CPManuever cf = CPM_First; cf < CPM_Last; INCREMENT(cf))
  {
    delete d_manuevers[cf];
  }

  delete[] d_manuevers;
}

void Manuever_Int::startManuever(const RefBattleCP& cp)
{
   ASSERT(cp->getCurrentOrder().manuever() < CPM_Last);
  d_manuevers[cp->getCurrentOrder().manuever()]->startManuever(cp);
}

};

/*
 *----------------------------------------------------------------------
 * $Log$
 *----------------------------------------------------------------------
 */
