/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
#ifndef BU_ORD_HPP
#define BU_ORD_HPP

/*--------------------------------------------------------
 * Process orders
 */

#include "batdata.hpp"

namespace B_Logic
{

class B_OrderProcImp;

class B_OrderProcInt {
      B_OrderProcImp* d_op;
   public:
      B_OrderProcInt(RPBattleData batData);
      ~B_OrderProcInt();

      void process();

};

}; // namespace
#endif

/*
 *----------------------------------------------------------------------
 * $Log$
 *----------------------------------------------------------------------
 */
