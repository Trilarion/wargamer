/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
#include "stdinc.hpp"
#include "bmove.hpp"
#include "batord.hpp"
#include "batarmy.hpp"
#include "bobiter.hpp"
#include "blog.hpp"
#include "b_tables.hpp"
#include "scenario.hpp"
#include "pathfind.hpp"
#include "moveutil.hpp"
#include "bobutil.hpp"
#include "broute.hpp"
#include "bordutil.hpp"

#ifdef DEBUG
#include "options.hpp"
#endif
#if !defined(NOLOG)
using B_Logic::logfile;
#endif

namespace Local
{
enum { RouteNotFound = UWORD_MAX };
bool canMoveOnRoad(const RefBattleCP& cp, B_Logic::MoveOrientation mo, int dist)
{
   if(cp->noRoadMove())
      return False;
   if(cp->getRank().isHigher(Rank_Division))
      return True;

   if(cp->movingBackwards() ||
      cp->formation() != CPF_March ||
      cp->spFormation() != SP_MarchFormation ||
      (mo != B_Logic::MO_Front && dist <= cp->spCount() + 1) ||
      cp->targetList().closeRange() <= (4 * BattleMeasure::XYardsPerHex))
   {
      return False;
   }

   return True;
}

}; // end namespace

namespace B_Logic
{

class Movement_Imp {
    PBattleData d_batData;
    enum MoveWhichWay {
      Forward,
      LeftRear,
      RightRear,
      Left,
      Right,

      MWW_HowMany,
      MWW_Undefined

    };

   public:
    Movement_Imp(RPBattleData batData);
    ~Movement_Imp() {}

    bool startMovement(const RefBattleCP& cp);

   private:

    bool move(const RefBattleCP& cp, MoveOrientation mo, int dist);
    void moveForward(const RefBattleCP& cp, HexCord leftHex, HexArea& area,
    HexPosition::Facing f, bool onRoad);
    void moveLeft(const RefBattleCP& cp, HexCord leftHex, HexArea& area);
    void moveRight(const RefBattleCP& cp, HexCord leftHex, HexArea& area);
    MoveWhichWay moveWhichWay(const HexPosition::Facing f, const HexCord& hex1, const HexCord& hex2);
    void clearMove(const RefBattleCP& cp);
    bool plotSPRoutes(const RefBattleCP& cp, HexArea& area);
    bool endMove(const RefBattleCP& cp, bool all = False);
    bool shouldPaste(const RefBattleCP& cp, MoveOrientation mo, int dist)
    {
      // If Pasteing, a XX's 2nd row and greater SP
      // are maintaining its current relative position,
      // rather than following the front row SP
      return ((cp->needsReadjustment() || mo != MO_Front) && !Local::canMoveOnRoad(cp, mo, dist));
    }
};

inline int squared(int v)
{
  return (v * v);
}

inline int getDist(const HexCord& srcHex, const HexCord& destHex)
{
  return static_cast<int>(sqrt(squared(destHex.x() - srcHex.x()) + squared(destHex.y() - srcHex.y())));
}

Movement_Imp::Movement_Imp(RPBattleData batData) :
  d_batData(batData)
{
}

bool Movement_Imp::endMove(const RefBattleCP& cp, bool all)
{
   BattleOrder& order = cp->getCurrentOrder();
   if(!order.nextWayPoint())
      return False;

   if(!all)
   {
      BattleOrderInfo& oi = order.order();
      oi = order.nextWayPoint()->d_order;
      order.removeWayPoint();
      return order.nextWayPoint() != 0;
   }
   else
   {
      while(endMove(cp, False));

      return False;
   }
}

bool Movement_Imp::startMovement(const RefBattleCP& cp)
{
   BattleOrder& order = cp->getCurrentOrder();

#ifdef DEBUG
   if(lstrcmpi("XIV Russian Corps", cp->getName()) == 0)
   {
      int i = 0;
   }

   if(Options::get(OPT_InstantMove))
   {
      return False;
#if 0
      if(order.nextWayPoint())
     {
       HexCord hex = cp->leftHex();
       HexCord destHex = order.nextWayPoint()->d_hex;

       B_Route br(d_batData, cp, cp->deployItem(0, 0).d_sp);
       if(!br.plotRoute(hex, destHex, 0))
       {
         FORCEASSERT("Route not found");
         return False;
       }

       order.removeWayPoint();
       return move(cp, MO_Front, getDist(hex, destHex));
     }
     else
       return False;
#endif
   }
#endif

   ASSERT(order.nextWayPoint());
   if(!order.nextWayPoint())
     return False;

//   bool onRoad = False;
   MoveOrientation mo = MO_Front;
#if !defined(NOLOG)
   logfile << "\n" << cp->getName() << " is moving" << endl;
#endif
// BattleCP::HexOffset ho = BattleCP::HO_Left;
   int dist = 0;
   if(cp->getRank().sameRank(Rank_Division) && cp->sp() != NoBattleSP)
   {

     // plot from top-left hex to dest
      HexCord srcHex = cp->leftHex();
//      if(!onRoad)
      {
       HexCord destHexL = order.nextWayPoint()->d_hex;

       int cols = cp->columns();// - 1;
       HexPosition::Facing f = cp->facing();//order.nextWayPoint()->d_order.d_facing;
//     HexCord destHexR;
//     bool atEdge = !rightDestHex(d_batData, f, destHexL, cols, destHexR);

       mo = moveOrientation(f, srcHex, destHexL, MOA_NormalFrontWideBack);
       cp->movingBackwards(mo == MO_Rear && cp->targetList().entries() > 0 && cp->targetList().closeRange() <= (6 * BattleMeasure::XYardsPerHex));
       cp->movingLeft(mo == MO_Left);
       cp->movingRight(mo == MO_Right);

       if(!cp->canMove())
       {
         cp->movingBackwards(False);
         cp->movingLeft(False);
         cp->movingRight(False);
         return False;
       }
#ifdef DEBUG
       const int maxMO = 4;
       static const char* s_text[maxMO] = {
            "Front",
            "Left",
            "Right",
            "Rear"
       };
       ASSERT(mo < maxMO);
       logfile << "\n" << cp->getName() << " Move orientation is -- " << s_text[mo] << endl;
#endif

       cp->moveDest(destHexL);
       if(destHexL == srcHex)
       {
         // where we want to be
         return endMove(cp);
       }

       // otherwise, plot route for top-left SP
       else
       {
//       HexCord hex;
//       HexCord destHex;

         int c = 0;
         DeployItem* di = cp->currentDeployItem(c, 0);
         ASSERT(di && di->active());
         if(!di || !di->active())
            return False;

         RefBattleSP& sp = di->d_sp;

         dist = getDist(srcHex, destHexL);
         B_Route br(d_batData, cp, sp);
         if(Local::canMoveOnRoad(cp, mo, dist))
         {
            HexList destHexes;

            // if not moving towards the front, find an
            // intermediary point a few hexes to the rear
            // so unit doesnt need to do extensive readjusting
            // at end of move
            if(mo != MO_Front)
            {
               int backHowMany = maximum(3, (cp->spCount() / 2) + 1);
               HexCord newLeft = destHexL;
               MoveWhatDirection md = (mo == MO_Rear) ? MWD_Rear :
                                      (mo == MO_Right) ? MWD_LeftRear : MWD_RightRear;

               if(plotMove(d_batData, cp->facing(), destHexL, newLeft, backHowMany, md, cp))
               {
                  destHexes.newItem(newLeft);
               }
            }

            destHexes.newItem(destHexL);
#if 0
            // add exclude hexes to insure we don't turn back on ourselves
            HexList excludeHexes;
            for(int r = 1; r < cp->rows(); r++)
            {
               DeployItem& di = cp->deployItem(0, r);
               if(di.active())
               {
                  excludeHexes.newItem(di.d_sp->hex());
               }
            }
#endif
            if(!br.plotRoute(srcHex, destHexes, 0, False))
            {
               endMove(cp, True);
               return False;
            }
         }
         else if(!br.plotRoute(srcHex, destHexL, c, shouldPaste(cp, mo, dist), False))
         {
            endMove(cp, True);
            return False;
         }

         // Bodge for now, add facing to each route node
         SListIter<HexItem> iter(&sp->routeList());
#ifdef DEBUG
         logfile.printf("Uncleansed Route for %s", cp->getName());
         logfile.printf("-----------------------");
#endif
         while(++iter)
         {
            iter.current()->d_facing = cp->facing();
#ifdef DEBUG
            logfile.printf("hex(%d, %d)", static_cast<int>(iter.current()->d_hex.x()), static_cast<int>(iter.current()->d_hex.y()));
#endif
         }
         cleanUpRoute(d_batData, sp->routeList());
#ifdef DEBUG
         {
            logfile.printf("Cleansed Route for %s", cp->getName());
            logfile.printf("-----------------------");
            SListIter<HexItem> iter(&sp->routeList());
            while(++iter)
            {
               logfile.printf("hex(%d, %d)", static_cast<int>(iter.current()->d_hex.x()), static_cast<int>(iter.current()->d_hex.y()));
            }
         }
#endif
       }
      }
   }

   if(!move(cp, mo, dist))
   {
      endMove(cp, True);
      return False;
   }
   else
      return True;
}

/*
 * Move each SP in a XX
 */


bool Movement_Imp::move(const RefBattleCP& cp, MoveOrientation mo, int dist)
{
#if !defined(NOLOG)
   logfile << "\n" << cp->getName() << " is moving" << endl;
#endif

   if(shouldPaste(cp, mo, dist))
   {
      // move SP's
      if(cp->getRank().sameRank(Rank_Division) && cp->sp() != NoBattleSP)
      {
         cp->moveMode(BattleCP::Moving);

         DeployItem* cpDI = cp->currentDeployItem(0, 0);
         ASSERT(cpDI);
         if(!cpDI || !cpDI->active())
            return False;

         cpDI->d_sp->setMoving(True);

         // reset list of SP's (except top-left, which is already plotted
         for(std::vector<DeployItem>::iterator di = cp->mapBegin();
             di != cp->mapEnd();
             di++)
         {
            if(di->active() && (di != cpDI))
            {
               di->d_sp->routeList().reset();
            }
         }

         // now go through route-list of top-left SP and a
         // assign route to all hexes in the front rank
         if(cpDI->d_sp->routeList().entries() > 1)
         {
            const HexItem* lastItem = cpDI->d_sp->routeList().first();

            SListIterR<HexItem> iter(&cpDI->d_sp->routeList());
            while(++iter)
            {
               if(iter.current() != lastItem)
               {
                  HexCord::HexDirection hd = hexDirection(d_batData, lastItem->d_hex, iter.current()->d_hex);

                  // paste Unit Layout
                  for(int r = 0; r < cp->rows(); r++)
                  {
                     for(int c = 0; c < cp->columns(); c++)
                     {
                        DeployItem* di = cp->currentDeployItem(c, r);
                        if(di && di->active() && di != cpDI)
                        {
                           // if route not started, add current hex
                           if(di->d_sp->routeList().entries() == 0)
                           {
                              di->d_sp->routeList().newItem(di->d_sp->hex(), cp->facing());
                              di->d_sp->setMoving(True);
                           }

                           HexCord lastHex = di->d_sp->routeList().getLast()->d_hex;
                           HexCord hex;
                           if(d_batData->moveHex(lastHex, hd, hex) )
                           {
                              di->d_sp->routeList().newItem(hex, iter.current()->d_facing);
                           }
                           else
                           {
                              clearMove(cp);
                              return False;
                           }
                        }
                     }
                  }
               }
               lastItem = iter.current();
            }
         }
      }
#if 0
      RefBattleCP attachedTo = BobUtility::getAttachedLeader(d_batData, cp);
      if(attachedTo != NoBattleCP)
      {
         plotHQRoute(d_batData, attachedTo);
      }
#endif
      return True;//f;
   }


   // move SP's
   if(cp->getRank().sameRank(Rank_Division) && cp->sp() != NoBattleSP)
   {
      cp->moveMode(BattleCP::Moving);

      int col = 0;//(ho == BattleCP::HO_Left) ? 0 : cp->columns() - 1;
      DeployItem* cpDI = cp->currentDeployItem(col, 0);
      ASSERT(cpDI);
      if(!cpDI || !cpDI->active())
         return False;

      cpDI->d_sp->setMoving(True);

      // reset list of SP's (except top-left, which is already plotted
      for(std::vector<DeployItem>::iterator di = cp->mapBegin();
            di != cp->mapEnd();
            di++)
      {
            if(di->active() && (di != cpDI))
            {
               di->d_sp->routeList().reset();
            }
      }

      // now go through route-list of top-left SP and a
      // assign route to all hexes in the front rank
      HexArea area;
      if(cpDI->d_sp->routeList().entries() > 0)
      {
            HexCord::HexDirection hd = rightFlank(cp->facing());

            bool noRoute = False;
            SListIterR<HexItem> iter(&cpDI->d_sp->routeList());
            while(++iter)
            {
               HexCord srcHex = iter.current()->d_hex;
               //int start = 1;//(ho == BattleCP::HO_Left) ? 1 : cp->columns() - 2;
               //int end = cp->columns();//(ho == BattleCP::HO_Left) ? cp->columns() : -1;
               //int inc = 1;//(ho == BattleCP::HO_Left) ? 1 : -1;
               for(int c = 1; c < cp->columns(); c++)
               {
                  HexCord hex;
                  if(d_batData->moveHex(srcHex, hd, hex) )
//                  !Local::prohibitedTerrain(d_batData, cp, hex))
                  {
                     DeployItem* di = cp->currentDeployItem(c, 0);
                     if(!di || !di->active())
                           continue;

                     di->d_sp->setMoving(True);
                     di->d_sp->routeList().newItem(hex, iter.current()->d_facing);

                     srcHex = hex;
                  }
                  else
                  {
                     noRoute = True;
                     break;
                  }
               }

               if(noRoute)
                  break;
            }

            // now all other rows follow along
            if(noRoute)
            {
               clearMove(cp);
               return False;
            }

            if(cp->formation() != CPF_Extended)
            {
               if(!plotSPRoutes(cp, area))
               {
                  clearMove(cp);
                  return False;
            }
         }
      }
#if 0
      RefBattleCP attachedTo = BobUtility::getAttachedLeader(d_batData, cp);
      if(attachedTo != NoBattleCP)
      {
         plotHQRoute(d_batData, attachedTo);
      }
#endif
      return True; //f;
   }
   else
   {
      BattleOrder& order = cp->getCurrentOrder();

      if(order.nextWayPoint() &&
         order.nextWayPoint()->d_hex == cp->hex())
      {
         endMove(cp);
      }
      else
      {
         plotHQRoute(d_batData, cp);
         cp->moveMode(BattleCP::Moving);
         cp->setMoving(True);
#if 0
         RefBattleCP attachedTo = BobUtility::getAttachedLeader(d_batData, cp);
         if(attachedTo != NoBattleCP)
         {
            plotHQRoute(d_batData, attachedTo);
         }
#endif
      }

      return cp->moving();
   }
}

Movement_Imp::MoveWhichWay Movement_Imp::moveWhichWay(const HexPosition::Facing facing,
    const HexCord& hex1, const HexCord& hex2)
{
   HexCord::HexDirection hd = hexDirection(d_batData, hex1, hex2);
   ASSERT(hd < HexCord::Count);

   // convert facing to a local facing enum for use as index
   enum LFace {
      NE,
      North,
      NW,
      SW,
      South,
      SE,

      LFace_HowMany,
      LFace_Undefined
   } lface = (facing == HexPosition::NorthEastPoint) ? NE :
             (facing == HexPosition::North)          ? North :
             (facing == HexPosition::NorthWestPoint) ? NW :
             (facing == HexPosition::SouthWestPoint) ? SW :
             (facing == HexPosition::South)          ? South :
             (facing == HexPosition::SouthEastPoint) ? SE : LFace_Undefined;

   ASSERT(lface != LFace_Undefined);
   ASSERT(lface < LFace_HowMany);

   static const MoveWhichWay s_whichWay[LFace_HowMany][HexCord::Count] = {
     { Forward,   Forward,   Left,      LeftRear,  RightRear, Right     },
     { Right,     Forward,   Forward,   Left,      LeftRear,  RightRear },
     { RightRear, Right,     Forward,   Forward,   Left,      LeftRear  },
     { LeftRear,  RightRear, Right,     Forward,   Forward,   Left      },
     { Left,      LeftRear,  RightRear, Right,     Forward,   Forward   },
     { Forward,   Left,      LeftRear,  RightRear, Right,     Forward   }
   };

   return s_whichWay[lface][hd];
}

/*
 * Plot SP routes. This is for 2 rank and beyond.
 */

bool Movement_Imp::plotSPRoutes(const RefBattleCP& cp, HexArea& area)
{
   for(int c = 0; c < cp->columns(); c++)
   {
      DeployItem* fDI = cp->currentDeployItem(c, 0);
      ASSERT(fDI);
      if(!fDI || !fDI->active())
         return False;

#ifdef DEBUG
      BattleCP::HexOffset lastWay = cp->whichWay();
#endif

      for(int r = 1; r < cp->rows(); r++)
      {
         if( (cp->formation() == CPF_Deployed) &&
             (r == 1) &&
             (cp->spCount() % 2 != 0) &&
             (c + 1 < cp->columns()) )
         {
            DeployItem* rDI = cp->currentDeployItem(0, 1);
            ASSERT(rDI);
            if(!rDI || !rDI->active())
               return False;

            if(adjacentToSameHex(d_batData, cp->leftHex(), rDI->d_sp->hex(),
               rightRear(cp->facing()), leftFlank(cp->facing())))
            {
               fDI = cp->currentDeployItem(c + 1, 0);
               ASSERT(fDI);
               if(!fDI || !fDI->active())
                  return False;
            }
         }

         DeployItem* di = cp->currentDeployItem(c, r);
         ASSERT(di);
         if(!di)
            return False;
         if(!di->active())
            continue;

         di->d_sp->setMoving(True);

#ifdef DEBUG
#if 0
         if(Options::get(OPT_InstantMove))
         {
            HexCord::HexDirection hd = (lastWay == BattleCP::HO_Left) ?
               rightRear(cp->facing()) : leftRear(cp->facing());

            HexCord h;
            if(d_batData->moveHex(fDI->d_sp->routeList().getLast()->d_hex, hd, h))
               di.d_sp->routeList().newItem(h, cp->facing());

            lastWay = (lastWay == BattleCP::HO_Left) ? BattleCP::HO_Right :
               BattleCP::HO_Left;

            continue;
         }
#endif
#endif

         // add current hex and facing. needed to keep all in sync
         di->d_sp->routeList().newItem(di->d_sp->hex(), di->d_sp->facing());

         // now copy from list of sp in front
         HexItem* item = fDI->d_sp->routeList().first();

         while( (item) &&
                (item != fDI->d_sp->routeList().getLast()) )
         {
            HexCord lastHex = (di->d_sp->nextHex()) ?
               di->d_sp->routeList().getLast()->d_hex : di->d_sp->hex();
            HexCord hex = item->d_hex;
            HexPosition::Facing facing = item->d_facing;

            item = fDI->d_sp->routeList().next();
            ASSERT(item);

            if(item)
            {
               if(item->d_hex == hex)
                  ;
               else
               {
                  MoveWhichWay ww = (cp->formation() == CPF_March) ?
                     Forward : moveWhichWay(cp->facing(), hex, item->d_hex);

                  HexItem* hi = 0;
                  switch(ww)
                  {
                     case Forward:
                     {
                        hi = new HexItem(hex, facing);
                        break;
                     }

                     case Right:
                     case Left:
                     {
                        HexCord::HexDirection hd2 = (ww == Left) ? leftFlank(cp->facing()) : rightFlank(cp->facing());
                        HexCord h;
                        if(d_batData->moveHex(lastHex, hd2, h))
                        {
                           hi = new HexItem(h, facing);
                        }

                        break;
                     }

                     case RightRear:
                     case LeftRear:
                     {
                        HexCord::HexDirection hd2 = (ww == LeftRear) ? leftRear(cp->facing()) : rightRear(cp->facing());
                        HexCord h;
                        if(d_batData->moveHex(lastHex, hd2, h))
                        {
                           hi = new HexItem(h, facing);
                        }
                        break;
                     }
#ifdef DEBUG
                     default:
                        FORCEASSERT("MoveWichWay not found");
#endif
                  }

                  if(hi)
                     di->d_sp->routeList().append(hi);
                  else
                     return False;
               }
            }
         }

         fDI = di;
      }
   }

   return True;
}

void Movement_Imp::clearMove(const RefBattleCP& cp)
{
   cp->moveMode(BattleCP::Holding);

   // reset list of SP's (except top-left, which is already plotted
   if(cp->getRank().sameRank(Rank_Division) && cp->sp() != NoBattleSP)
   {
      for(std::vector<DeployItem>::iterator di = cp->mapBegin();
          di != cp->mapEnd();
          di++)
    {
      if(di->active())
      {
             di->d_sp->routeList().reset();
         di->d_sp->setMoving(False);
      }
    }
   }
   else
   {
    cp->routeList().reset();
    cp->setMoving(False);
   }
}


/*----------------------------------------------------------------
 * Client access
 */

Movement_Int::Movement_Int(RPBattleData batData) :
  d_move(new Movement_Imp(batData))
{
  ASSERT(d_move);
}

Movement_Int::~Movement_Int()
{
  delete d_move;
}

bool Movement_Int::startMovement(const RefBattleCP& cp)
{
  return d_move->startMovement(cp);
}

}; // namespace


/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.2  2001/06/18 08:25:44  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
