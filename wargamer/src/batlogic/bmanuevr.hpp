/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
#ifndef BMANUEVR_HPP
#define BMANUEVR_HPP

#include "batdata.hpp"
#include "batunit.hpp"
#include "bobdef.hpp"

//namespace BattleMeasure { class HexCord; };
//using namespace BOB_Definitions;

namespace B_Logic
{

class ManueverBase;

class Manuever_Int {
    ManueverBase** d_manuevers;
  public:
    Manuever_Int(RPBattleData batData);
    ~Manuever_Int();

    void startManuever(const RefBattleCP& cp);
};

};
#endif

/*
 *----------------------------------------------------------------------
 * $Log$
 *----------------------------------------------------------------------
 */
