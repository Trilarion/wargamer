/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef B_DETECT
#define B_DETECT

//#include "batdata.hpp"
#include "batunit.hpp"
#include "batcord.hpp"
//#include "bobdef.hpp"

/*
 * Enemy detection routines
 */

class BattleGameInterface;
//class HexCord;
class BattleList;

namespace B_Logic
{

class B_DetectImp;

class B_DetectInt {
      B_DetectImp* d_di;
   public:
      B_DetectInt(BattleGameInterface* batgame);
      ~B_DetectInt();

      void doEnemyNear(const RefBattleCP& cp);
      bool enemyInRange(const RefBattleCP& cp);
};

};


#endif
