/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
#include "stdinc.hpp"
#include "b_fow.hpp"
#include "bob_cp.hpp"
#include "bob_sp.hpp"
#include "batarmy.hpp"
#include "scenario.hpp"
#include "moveutil.hpp"
#include "bobiter.hpp"
#include "bobutil.hpp"
#include "cmbtutil.hpp"
#include "batdata.hpp"
#include "batctrl.hpp"
#include "b_los.hpp"
#ifdef DEBUG
#include "clog.hpp"
static LogFile fowLog("B_fow.log");
#endif

namespace B_Logic {


// Fow implementation class
class B_FowImp {
      BattleGameInterface* d_batgame;
      PBattleData d_batData;
      BattleMeasure::BattleTime::Tick d_nextTick;
      B_LineOfSight d_los;
   public:
      B_FowImp(BattleGameInterface* batgame) :
         d_batgame(batgame),
         d_batData(batgame->battleData()),
         d_nextTick(0),
         d_los(d_batData) {}

      void process();
      void newDay() { d_nextTick = 0; }

   private:

      void procUnit(const RefBattleCP& cp);
      void search(const RefBattleCP& cp, HexCord hex, int& closeDist);
      int squared(int v) { return (v * v); }
};

void B_FowImp::process()
{
    BattleData::WriteLock lock(d_batData);

   if(d_batData->getTick() >= d_nextTick)
   {
#ifdef DEBUG
      SYSTEMTIME st;
      GetSystemTime(&st);
      fowLog.printf("Processing Fog of War at %d:%d.%d.%d------------",
          static_cast<int>(st.wHour), static_cast<int>(st.wMinute),
          static_cast<int>(st.wSecond), static_cast<int>(st.wMilliseconds));
#endif
      for(Side s = 0; s < scenario->getNumSides(); s++)
      {
//         RefBattleCP topCP = d_batData->ob()->getTop(s);
         for(BattleUnitIter iter(d_batData->ob(), s); !iter.isFinished(); iter.next())
            procUnit(iter.cp());
      }
#ifdef DEBUG
      GetSystemTime(&st);
      fowLog.printf("Ending Fog of War at %d:%d.%d.%d------------\n\n",
          static_cast<int>(st.wHour), static_cast<int>(st.wMinute),
          static_cast<int>(st.wSecond), static_cast<int>(st.wMilliseconds));
#endif
      d_nextTick = d_batData->getTick() + (120 * BattleMeasure::BattleTime::TicksPerSecond);
   }

}


// Visibility is determined by range and LOS
void B_FowImp::procUnit(const RefBattleCP& cp)
{
#ifdef DEBUG
   fowLog.printf("--------------- Processing %s", cp->getName());

   // Unchecked
   static const char* s_vText[Visibility::HowMany + 1] = {
      "Not Seen",
      "Poor",
      "Fair",
      "Good",
      "Full",
      "Undefined"
   };
   // End
   fowLog.printf("Current Highest Visibility = %s", s_vText[cp->visibility()]);
#endif

   Visibility::Value v = Visibility::NotSeen;

   // if we have targets in target list, we are seen fully (within 8 hexes)
   if(cp->targetList().closeRange() <= (4 * BattleMeasure::XYardsPerHex))
   {
      v = Visibility::Full;
      for(std::vector<DeployItem>::iterator ourDI = cp->mapBegin();
            ourDI != cp->mapEnd();
            ourDI++)
      {
         int closeSPDist = UWORD_MAX;

         if(ourDI->active())
         {
            ourDI->d_sp->visibility(v);
            ourDI->d_sp->visibilityPercent(100);
         }
      }

      cp->visibility(v);
      cp->visibilityPercent(100);
   }
   else
   {
      // go through each enemy and get closest range
      // setting each SP individually
      int closeDist = UWORD_MAX;

      for(std::vector<DeployItem>::iterator ourDI = cp->mapBegin();
            ourDI != cp->mapEnd();
            ourDI++)
      {
         int closeSPDist = UWORD_MAX;

         if(ourDI->active())
         {
            search(cp, ourDI->d_sp->hex(), closeSPDist);
            
            if(closeSPDist <= 36)
               ourDI->d_sp->visibility(Visibility::Poor);
            else if(closeSPDist <= 18)
               ourDI->d_sp->visibility(Visibility::Fair);
            else if(closeSPDist <= 12)
               ourDI->d_sp->visibility(Visibility::Good);
            else if(closeSPDist <= 8)
               ourDI->d_sp->visibility(Visibility::Full);
            else
               ourDI->d_sp->visibility(Visibility::NotSeen);

            // Now set as a percent of 36
            int perOf36 = minimum(100, (closeSPDist * 100) / 36);
            ourDI->d_sp->visibilityPercent(100 - perOf36); 
         }

         if(closeSPDist < closeDist)
            closeDist = closeSPDist;
      }

      // TODO: modify for weather

      if(cp->getRank().isHigher(Rank_Division))
      {
         search(cp, cp->hex(), closeDist);
      }
      
      if(closeDist <= 36)
         v = Visibility::Poor;
      else if(closeDist <= 18)
         v = Visibility::Fair;
      else if(closeDist <= 12)
         v = Visibility::Good;
      else if(closeDist <= 8)
         v = Visibility::Full;

      cp->visibility(v);
      // Now set as a percent of 36
      int perOf36 = minimum(100, (closeDist * 100) / 36);
      cp->visibilityPercent(100 - perOf36);

   }

   // set HQ
//   cp->visibility(v);
#ifdef DEBUG
   fowLog.printf("New Highest Visibility = %s\n", s_vText[v]);
#endif
}

void B_FowImp::search(const RefBattleCP& cp, HexCord hex, int& closeSPDist)
{
            Side otherSide = (cp->getSide() == 0) ? 1 : 0;
//            RefBattleCP topEnemyCP = d_batData->ob()->getTop(otherSide);

            for(BattleUnitIter uIter(d_batData->ob(), otherSide); !uIter.isFinished(); uIter.next())
            {
               if(uIter.cp()->getRank().sameRank(Rank_Division))
               {
                  int cdist = static_cast<int>(sqrt(squared(hex.x() - uIter.cp()->sp()->hex().x()) + squared(hex.y() - uIter.cp()->sp()->hex().x())));
                  if(cdist > 36 + uIter.cp()->spCount())
                     continue;

                  for(std::vector<DeployItem>::iterator di = uIter.cp()->mapBegin();
                     di != uIter.cp()->mapEnd();
                     di++)
                  {
                     if(di->active())
                     {
                        int dist = static_cast<int>(sqrt(squared(hex.x() - di->d_hex.x()) + squared(hex.y() - di->d_hex.y())));

                        if(dist < closeSPDist &&
                            dist <= 36 &&
                            d_los.FastLOS(d_batData->map(), hex, di->d_hex) != Visibility::NotSeen)
//                            Combat_Util::lineOfSight(d_batData, hex, di->d_hex, 0))
                        {
                           closeSPDist = dist;
                        }
                     }
                  }
               }
               else
               {
                  int dist = static_cast<int>(sqrt(squared(hex.x() - uIter.cp()->hex().x()) + squared(hex.y() - uIter.cp()->hex().y())));
                  if(dist < closeSPDist &&
                      dist <= 36 &&
                      d_los.FastLOS(d_batData->map(), hex, uIter.cp()->hex()) != Visibility::NotSeen)
//                      Combat_Util::lineOfSight(d_batData, hex, uIter.cp()->hex(), 0))
                  {
                     closeSPDist = dist;
                  }
               }
            }
}

//--------------------------------------------------------
// Client access


B_FogOfWar::B_FogOfWar(BattleGameInterface* batgame) :
   d_fi(new B_FowImp(batgame))
{
   ASSERT(d_fi);
}

B_FogOfWar::~B_FogOfWar()
{
   if(d_fi)
      delete d_fi;
}

void B_FogOfWar::process()
{
   if(d_fi)
      d_fi->process();
}

void B_FogOfWar::newDay()
{
   if(d_fi)
      d_fi->newDay();
}

}; // end namespace

/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.2  2001/06/18 08:25:44  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
