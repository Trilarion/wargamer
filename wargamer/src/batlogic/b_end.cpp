/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 * Battle End Detection
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "b_end.hpp"
#include "b_result.hpp"
#include "batdata.hpp"
#include "batarmy.hpp"
#include "bobiter.hpp"
#include "bobutil.hpp"
#ifdef DEBUG
#include "clog.hpp"
#include "battime.hpp"
LogFile eLog("EndOfDay.log");
#endif
using BattleMeasure::BattleTime;

namespace
{

class CCheckBattleOver
{
    public:
        CCheckBattleOver(const BattleData* batData, BattleFinish* result);
        ~CCheckBattleOver() { }

        bool process();

    private:
        bool checkTime();
        bool checkSide(Side s);

    private:
        const BattleData*   d_batData;
        const BattleOB*     d_ob;
        BattleFinish*      d_results;
};

/*
 * Construct Utility Class
 */

CCheckBattleOver::CCheckBattleOver(const BattleData* batData, BattleFinish* result) :
    d_batData(batData),
    d_results(result)
{
    ASSERT(batData != 0);

    d_ob = d_batData->ob();
}

/*
 * Check for Battle Over
 */

bool CCheckBattleOver::process()
{
    if(checkTime())
        return true;

    BattleSideIter sideIter(d_ob);
    while(++sideIter)
    {
        Side s = sideIter.current();
        if(checkSide(s))
            return true;
    }

    return false;
}


/*
 * Check if it is the end of the day
 */

bool CCheckBattleOver::checkTime()
{
    BattleMeasure::BattleTime::Tick tick = d_batData->getTick();
    if( (tick >= BattleTime::EndOfDay()) ||
        (d_batData->type() == BattleData::Historical && d_batData->shouldEnd()) )
    {
        if(tick >= BattleTime::EndOfDay() &&
           d_batData->type() == BattleData::Historical)
        {
            BattleMeasure::BattleTime::Tick endGameIn = const_cast<BattleData*>(d_batData)->endGameIn();
#ifdef DEBUG
            eLog.printf("Game ends (before) in %d hours",
               BattleMeasure::hoursPerTick(endGameIn - d_batData->getTick()));
#endif
            endGameIn = maximum<int>(0, (endGameIn - BattleMeasure::hours(13)));
#ifdef DEBUG
            eLog.printf("Game ends (after) in %d hours",
               BattleMeasure::hoursPerTick(endGameIn - d_batData->getTick()));
#endif
            const_cast<BattleData*>(d_batData)->endGameIn(endGameIn);
        }

        d_results->endDay();
        return true;
    }

    return false;
}

/*
 * Check if a side has lost
 */

bool CCheckBattleOver::checkSide(Side s)
{
    // Check for all units run Away
    // Check for all units destroyed

    int deadCount = 0;
    int fledCount = 0;
    int reinforceCount = 0;
    int aliveCount = 0;

    TConstBattleUnitIter<BUIV_All> iter(d_ob, s);
//    ConstBattleUnitIter iter(d_ob, s);
    while (!iter.isFinished())
    {
      CRefBattleCP cp = iter.cp();
      if(cp->isDead())
         ++deadCount;
      // else if(cp->hasFled())
      else if(cp->fleeingTheField() || cp->hasFled())
         ++fledCount;
      else if(!cp->active())
         ++reinforceCount;
      else
         ++aliveCount;
      iter.next();
    }

    if(!aliveCount)
    {
      BattleResults::HowDefeated how;
      if(fledCount == 0)
         how = BattleResults::Destroyed;
      else
         how = BattleResults::RanAway;
      d_results->defeated(otherSide(s), how);
      return true;
    }

    // Check morale <= 40

    CRefBattleCP topCP = d_ob->getTop(s);
    Attribute morale = BobUtility::averageMorale(topCP);
    if (morale <= 40)
    {
      d_results->defeated(otherSide(s), BattleResults::Morale);
      return true;
    }

    // Check Commander Determination

    // TODO:

    return false;
}


};  // private namespace


namespace B_Logic
{

/*
 * Function Called by Game Logic
 */


bool checkBattleOver(const BattleData* batData, BattleFinish* results)
{
    ASSERT(batData != 0);
    ASSERT(results != 0);

    CCheckBattleOver utility(batData, results);
    return utility.process();
}


};  // namespace B_Logic


/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.2  2001/06/18 08:25:43  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
