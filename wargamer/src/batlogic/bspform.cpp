/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
#include "stdinc.hpp"
#include "bspform.hpp"
#include "batord.hpp"
#include "batarmy.hpp"
#include "bobiter.hpp"
#include "blog.hpp"
#include "b_tables.hpp"
#include "scenario.hpp"
#include "hexdata.hpp"
#ifdef DEBUG
#include "moveutil.hpp"
#endif
#if !defined(NOLOG)
using B_Logic::logfile;
#endif

namespace B_Logic
{

using namespace BattleMeasure;

class SPFormation_Imp {
    PBattleData d_batData;
  public:
    SPFormation_Imp(RPBattleData batData);
    ~SPFormation_Imp() {}

    enum ArtilleryType { Foot, Horse, Heavy, None };

    bool run(const RefBattleCP& cp);
    bool runFacingChange(const RefBattleSP& sp);
    bool startChange(const RefBattleCP& cp);
    void startFacingChange(const RefBattleSP& sp);
  private:
    SWORD SPFormation_Imp::getRawChangeTime(BasicUnitType::value bt, Nationality n,
       ArtilleryType t, const SPFormation lf, const SPFormation df);
    SWORD getRawChangeTime(const RefBattleCP& cp, const SPFormation lf, const SPFormation df);
    bool canDo(const RefBattleCP& cp, SPFormation lf);
};

SPFormation_Imp::SPFormation_Imp(RPBattleData batData) :
   d_batData(batData)
{
}

bool SPFormation_Imp::canDo(const RefBattleCP& cp, SPFormation lf)
{
   enum {
    Inf,
    LCav,
    HCav,
    FootArt,
    HvyArt,
    HorseArt,

    End,
    Undefined = End
  } unitType = (cp->generic()->isInfantry())       ? Inf :
               (cp->generic()->isHeavyCavalry())   ? HCav :
               (cp->generic()->isCavalry())        ? LCav :
               (cp->generic()->isHeavyArtillery()) ? HvyArt :
               (cp->generic()->isHorseArtillery()) ? HorseArt :
               (cp->generic()->isArtillery())      ? FootArt : Undefined;

  if(unitType >= FootArt && unitType <= HorseArt)
     return(lf == SP_LimberedFormation || lf == SP_UnlimberedFormation);

  bool canGoOn = False;

  enum {
      March,
      Column,
      Other
  } formation = (lf == SP_MarchFormation) ? March :
                (lf == SP_ColumnFormation) ? Column : Other;

//  vector<DeployItem>& map = cp->deployMap();
  const Table3D<UBYTE>& tTable = BattleTables::prohibitedTerrain();

  for(int r = 0; r < cp->rows(); r++)
  {
      for(int c = 0; c < cp->columns(); c++)
      {
         //int index = maximum(0, (cp->columns() * r) + c);
         //DeployItem& di = map[index];
         DeployItem* di = cp->currentDeployItem(c, r);
         ASSERT(di);
         if(!di || !di->active())
            continue;

         const BattleTerrainHex& hexInfo = d_batData->getTerrain(di->d_sp->hex());
         // canGoOn = static_cast<bool>(tTable.getValue(unitType, hexInfo.d_terrainType, formation));
         canGoOn = tTable.getValue(unitType, hexInfo.d_terrainType, formation) != 0;

         // if terrain type is not prohibited, check path type (i.e. Rivers)
         if(canGoOn)
         {
            const Table3D<UWORD>& pTable = BattleTables::pathModifiers();
            // get path modifiers (i.e streams, rivers)

            // If we have a water obstacle
            if( (pTable.getValue(unitType, hexInfo.d_path1.d_type, formation) == 0) ||
                (pTable.getValue(unitType, hexInfo.d_path2.d_type, formation) == 0) )
            {
               canGoOn = False;
               r = cp->rows();
               break;
            }
         }
         else
         {
           r = cp->rows();
           break;
         }
      }
   }

   return canGoOn;
}

bool SPFormation_Imp::startChange(const RefBattleCP& cp)
{
   if(cp->sp() == NoBattleSP)
      return False;

#if !defined(NOLOG)
   logfile << "\n" << cp->getName() << " is changing SP-formation" << endl;
#endif

   const BattleOrder& cpOrder = cp->getCurrentOrder();

   /*
    * Set each SP to new formation
    * and add up unittype responce times
    */

   UWORD totalResponse = 0;
   UWORD spCount = 0;

   SPFormation sf = cp->sp()->fromFormation();
   SPFormation df = cpOrder.spFormation();

   if(!canDo(cp, df))
      return False;

   // test to see if we can legally do this

   // go through and halt all units
   for(std::vector<DeployItem>::iterator di = cp->mapBegin();
       di != cp->mapEnd();
       di++)
   {
      if(di->active())
      {
       di->d_sp->changeFormation(df);

       const UnitTypeItem& uti = d_batData->ob()->getUnitType(di->d_sp->generic()->getUnitType());
       totalResponse += uti.getResponse();

       spCount++;
      }
   }

   ASSERT(spCount > 0);
   Attribute aResponse = minimum<int>(Attribute_Range, totalResponse / spCount);

   SWORD sec = getRawChangeTime(cp, sf, df);
   ASSERT(sec >= 0);

#if !defined(NOLOG)
   logfile << "Will take " << sec << "seconds(unmodified)" << endl;
#endif

   /*
    * Modify time for tactical abilit and unit response
    * combine leader's tactical ability and units response time
    */

   UWORD ar = cp->leader()->getTactical() + aResponse;

#if !defined(NOLOG)
   logfile << "Leader Ability + Unit Response time = " << ar << endl;
#endif
   enum {
      ABRLess102,
     ABRLess204,
     ABRLess306,
     ABRLess408,
     ABROver408,
     ABRHowMany
   } abres = (ar < 102) ? ABRLess102 :
             (ar < 204) ? ABRLess204 :
             (ar < 306) ? ABRLess306 :
             (ar < 408) ? ABRLess408 : ABROver408;


   const Table1D<UWORD>& table = BattleTables::spfModifiers();

   sec = MulDiv(sec, table.getValue(abres), table.getResolution());

#if !defined(NOLOG)
   logfile << "Will take " << sec << "seconds(modified)" << endl;
#endif

   cp->startSPChange(d_batData->getTick());
   cp->endSPChange(d_batData->getTick() + seconds(sec));

    // clear shooting guns flags
    if(cp->shootingGuns() || cp->shootingMuskets())
    {
       cp->shootingGuns(False);
       cp->shootingMuskets(False);
       for(std::vector<DeployItem>::iterator di = cp->mapBegin();
             di != cp->mapEnd();
             ++di)
       {
          if(di->active())
          {
             di->d_sp->shootingGuns(False);
             di->d_sp->shootingMuskets(False);
          }
       }
    }
    return True;
}

void SPFormation_Imp::startFacingChange(const RefBattleSP& sp)
{
// ASSERT(sp->nextHex() && sp->facing() != sp->nextHex()->d_facing);
   ASSERT(sp->nextFacing() != sp->facing());

   /*
    * Set each SP to new formation
    * and add up unittype responce times
    */

   UWORD totalResponse = 0;
   UWORD spCount = 0;

   SPFormation sf = sp->fromFormation();
   SPFormation df = sf;

   const UnitTypeItem& uti = d_batData->ob()->getUnitType(sp->generic()->getUnitType());

   const RefBattleCP& cp = sp->parent();
#ifdef DEBUG
   logfile.printf("Changing SP Facing for sp of %s", cp->getName());
#endif
   Nationality n = (scenario->getNationType(cp->getNation()) == MajorNation) ?
      cp->getNation() : scenario->getDefaultNation(cp->getSide());

   ArtilleryType at;
   if(uti.getBasicType() == BasicUnitType::Artillery)
      at = (uti.isMounted()) ? Horse : Foot;
   else
      at = None;

   SWORD sec = getRawChangeTime(uti.getBasicType(), n, at, sf, df);
   ASSERT(sec >= 0);

   /*
    * Modify time for tactical abilit and unit response
    * combine leader's tactical ability and units response time
    */

   UWORD ar = cp->leader()->getTactical() + uti.getResponse();
   enum {
      ABRLess102,
      ABRLess204,
      ABRLess306,
      ABRLess408,
      ABROver408,
      ABRHowMany
   } abres = (ar < 102) ? ABRLess102 :
             (ar < 204) ? ABRLess204 :
             (ar < 306) ? ABRLess306 :
             (ar < 408) ? ABRLess408 : ABROver408;


   const Table1D<UWORD>& table = BattleTables::spfModifiers();

   sec = MulDiv(sec, table.getValue(abres), table.getResolution());

   sp->startSPChange(d_batData->getTick());
   sp->endSPChange(d_batData->getTick() + seconds(sec));
   HexPosition::Facing newFace = nextFacing(sp->facing(), sp->nextFacing());
#ifdef DEBUG
   logfile << "Changing SP-Facing for " << reinterpret_cast<LONG>(sp) << "(" << d_batData->ob()->spName(sp) << ") " << "from " << faceName(sp->facing()) << "' to '" << faceName(newFace) << "' to '" << faceName(sp->nextFacing()) << endl;
#endif
   sp->changeFacing(newFace);
   sp->addTween(1);
}

SWORD SPFormation_Imp::getRawChangeTime(BasicUnitType::value bt, Nationality n,
   ArtilleryType t, const SPFormation lf, const SPFormation df)
{
   if(bt == BasicUnitType::Infantry || bt == BasicUnitType::Cavalry)
   {
#ifdef DEBUG
      if(bt == BasicUnitType::Cavalry)
      {
         ASSERT(lf == SP_MarchFormation || lf == SP_ColumnFormation || lf == SP_LineFormation);
         ASSERT(df == SP_MarchFormation || df == SP_ColumnFormation || df == SP_LineFormation);
      }
      else
      {
         ASSERT(lf < SP_Formation_HowMany);
         ASSERT(df < SP_Formation_HowMany);
      }
#endif
      const Table3D<SWORD>& table = (bt == BasicUnitType::Infantry) ?
          BattleTables::spfInfChangeTime() : BattleTables::spfCavChangeTime();

      return table.getValue(n, lf, df);
   }

   else if(bt == BasicUnitType::Artillery)
   {
      // See if we are foot, horse, or heavy artillery
      ASSERT(lf == SP_LimberedFormation || lf == SP_UnlimberedFormation);
      ASSERT(df == SP_LimberedFormation || df == SP_UnlimberedFormation);

      const Table3D<SWORD>& table = (t == Foot) ?
       BattleTables::spfFootArtChangeTime() : BattleTables::spfHorseArtChangeTime();

      return table.getValue(n, lf, df);
   }

   FORCEASSERT("Improper unittype in getRawChangeTime()");
   return -1;
}

SWORD SPFormation_Imp::getRawChangeTime(const RefBattleCP& cp, const SPFormation lf, const SPFormation df)
{
   /*
    * Calculate time to change
    */

   Nationality n = (scenario->getNationType(cp->getNation()) == MajorNation) ?
      cp->getNation() : scenario->getDefaultNation(cp->getSide());

   BasicUnitType::value bt;
   ArtilleryType type;
   if(cp->generic()->isInfantry())
      bt = BasicUnitType::Infantry;
   else if(cp->generic()->isCavalry())
      bt = BasicUnitType::Cavalry;

   else if(cp->generic()->isArtillery())
   {
      bt = BasicUnitType::Artillery;

      // See if we are foot, horse, or heavy artillery
      ASSERT(lf == SP_LimberedFormation || lf == SP_UnlimberedFormation);
      ASSERT(df == SP_LimberedFormation || df == SP_UnlimberedFormation);

      for(std::vector<DeployItem>::iterator di = cp->mapBegin();
         di != cp->mapEnd();
         di++)
      {
       if(di->active())
       {
         const UnitTypeItem& uti = d_batData->ob()->getUnitType(di->d_sp->generic()->getUnitType());

         // TODO: check for heavy artillery
         if(!uti.isMounted())
         {
            type = Foot;
            break;
         }
         else
            type = Horse;
       }
      }
   }

   return getRawChangeTime(bt, n, type, lf, df);
}

bool SPFormation_Imp::run(const RefBattleCP& cp)
{
   if(cp->sp() == NoBattleSP)
      return True;

#if !defined(NOLOG)
   BattleTime btime = d_batData->getDateAndTime();
   Greenius_System::Time time = btime.time();
   Greenius_System::Date date = btime.date();

   logfile << "\n-------------------------------";
   logfile << "Running SP-Formation change for " << cp->getName() << endl;
   logfile << "Time=" << (int) time.hours() << ":" << (int) time.minutes() << ":" << (int) time.seconds() << endl;
   logfile << "Date=" << date.day() << "/" << 1+date.month() << "/" << date.year() << endl;
#endif

   const BattleOrder& cpOrder = cp->getCurrentOrder();

   const int tTime = maximum<int>(0, cp->endSPChange() - cp->startSPChange());
   const int tSinceStart = maximum<int>(0, d_batData->getTick() - cp->startSPChange());

   BattleSP::FormationTween::Value tween;

   if(tSinceStart >= tTime)
     tween = 0;
   else
   {
     const int tPercent = (tTime != 0) ? minimum(100, MulDiv(tSinceStart, 100, tTime)) : 0;
#if !defined(NOLOG)
     const int tweenPercent = cp->sp()->formationTween().value(100);
     ASSERT(tPercent >= tweenPercent);
     logfile.printf("Time elapsed = %d%%", tPercent);
#endif
     tween = MulDiv(tPercent, BattleSP::FormationTween::MaxValue, 100);
   }

   Boolean stillChanging = False;

   for(std::vector<DeployItem>::iterator di = cp->mapBegin();
       di != cp->mapEnd();
       di++)
   {
     if(di->active())
     {

       /*
        * Update the tween
        */

       // Convert ticks into a Tween value
       // Look up destFormation and fromFormation in table to see how long to change

       SPFormation destFormation = di->d_sp->destFormation();
       SPFormation srcFormation = di->d_sp->fromFormation();

       if(srcFormation != destFormation)
       {
         di->d_sp->setTween(tween);
         if(tween == 0)
           di->d_sp->setFormation(di->d_sp->destFormation());
         else
           stillChanging = True;
       }
     }
   }

#if !defined(NOLOG)
   if(cp->sp() != NoBattleSP)
   {
     logfile << "Tween = " << static_cast<int>(cp->sp()->formationTween().value()) << " Tween %% = " << static_cast<int>(cp->sp()->formationTween().value(100)) << endl;
   }
#endif
   return !stillChanging;
}

bool SPFormation_Imp::runFacingChange(const RefBattleSP& sp)
{
   ASSERT(sp->wantFacing() != sp->facing());

   const int tTime = maximum<int>(0, sp->endSPChange() - sp->startSPChange());
   const int tSinceStart = maximum<int>(0, d_batData->getTick() - sp->startSPChange());

   BattleSP::FormationTween::Value tween;

   if(tSinceStart >= tTime)
     tween = 0;
   else
   {
     const int tPercent = (tTime != 0) ? minimum(100, MulDiv(tSinceStart, 100, tTime)) : 0;
#if !defined(NOLOG)
     const int tweenPercent = sp->formationTween().value(100);
     ASSERT(tPercent >= tweenPercent);
     logfile.printf("Time elapsed = %d%%, current tween = %d%%", tPercent, tweenPercent);
#endif
     tween = MulDiv(tPercent, BattleSP::FormationTween::MaxValue, 100);
   }

#if !defined(NOLOG)
   BattleTime btime = d_batData->getDateAndTime();
   Greenius_System::Time time = btime.time();
   Greenius_System::Date date = btime.date();

   logfile << "\n-------------------------------";
   logfile << "Running SP-Facing change for " << d_batData->ob()->spName(sp) << endl;
   logfile << "Time=" << (int) time.hours() << ":" << (int) time.minutes() << ":" << (int) time.seconds() << endl;
   logfile << "Date=" << date.day() << "/" << 1+date.month() << "/" << date.year() << endl;
   logfile << "Tween value = " << static_cast<int>(sp->formationTween().value()) << endl;
#endif
   /*
    * Update the tween
    */

   sp->setTween(tween);
   if(sp->formationTween().value() == 0)
   {
     sp->setFacing(sp->wantFacing());
     return True;
   }
   else
     return False;
}

/*----------------------------------------------------------------
 * Client access
 */

SPFormation_Int::SPFormation_Int(RPBattleData batData) :
  d_formations(new SPFormation_Imp(batData))
{
  ASSERT(d_formations);
}

SPFormation_Int::~SPFormation_Int()
{
  delete d_formations;
}

bool SPFormation_Int::startChange(const RefBattleCP& cp)
{
  return d_formations->startChange(cp);
}

void SPFormation_Int::startFacingChange(const RefBattleSP& sp)
{
  d_formations->startFacingChange(sp);
}

bool SPFormation_Int::run(const RefBattleCP& cp)
{
  return d_formations->run(cp);
}

bool SPFormation_Int::runFacingChange(const RefBattleSP& sp)
{
  return d_formations->runFacingChange(sp);
}

}; // namespace

/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.2  2001/06/18 08:25:44  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
