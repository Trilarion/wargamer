/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 * Random Kills for debugging
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "randomKills.hpp"
#include "batdata.hpp"
#include "batarmy.hpp"
#include "bobiter.hpp"
#include "blosses.hpp"
#ifdef DEBUG
#include "blog.hpp"
#endif

void B_Logic::randomKills(RPBattleData batData)
{
    BattleOB* bob = batData->ob();

    for(BattleUnitIter iter(bob);
        !iter.isFinished();
        iter.next())
    {
        RefBattleCP cp = iter.cp();

        int spCount = cp->spCount(false);
        if(spCount)
        {
            int losses = random(0, spCount * 50);
            if(losses)
            {
#ifdef DEBUG
                B_LossUtil::applyLoss(batData, cp, losses, logfile);
#else
                B_LossUtil::applyLoss(batData, cp, losses);
#endif
            }
        }
    }
}

/*
 *----------------------------------------------------------------------
 * $Log$
 *----------------------------------------------------------------------
 */
