/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef BDEPLOY_HPP
#define BDEPLOY_HPP

#ifndef __cplusplus
#error bdeploy.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *      Deploy Units on Battlefield
 *
 *----------------------------------------------------------------------
 */

#include "bl_dll.h"
#include "batdata.hpp"
#include "batunit.hpp"
#include "bobdef.hpp"

namespace BattleMeasure { class HexCord; };

namespace B_Logic
{
class DeployBase;

class Deploy_Int {
         DeployBase** d_formations;
  public:
         Deploy_Int(RPBattleData batData);
         ~Deploy_Int();

         bool deploy(const RefBattleCP& cp);
			bool adjustDeployment(const RefBattleCP& cp);
			bool reorganize(const RefBattleCP& cp);
};

BATLOGIC_DLL bool playerDeploy(RPBattleData batData, const RefBattleCP& cp, const BattleMeasure::HexCord& startHex, HexList* hl = 0, bool initializing = True);
  // put a unit on the map.
  // startHex -- is the top-left hex occupied by the unit

BATLOGIC_DLL void deployUnits(BattleData* batData);
  // do AI deployment

void deployUnits(BattleData* batData, Side side);
  // do AI deployment

// do deployment od units from a save game (deploy whole OB) using playerDeploy function
BATLOGIC_DLL void deployUnitsFromSave(BattleData * batdata);
void deployChildrenFromSave(BattleData * batdata, BattleCP * cp);

}


#endif /* BDEPLOY_HPP */

/*----------------------------------------------------------------------------
 * $Log$
 *----------------------------------------------------------------------------
 */
