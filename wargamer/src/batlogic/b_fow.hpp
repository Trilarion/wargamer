/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
#ifndef B_FOW_HPP
#define B_FOW_HPP

//#include "batdata.hpp"

class BattleGameInterface;

namespace B_Logic {

class B_FowImp;
class B_FogOfWar{
      B_FowImp* d_fi;
   public:
      B_FogOfWar(BattleGameInterface* batgame);
      ~B_FogOfWar();

      void process();
      void newDay();
};

}; // end namespace

#endif

/*
 *----------------------------------------------------------------------
 * $Log$
 *----------------------------------------------------------------------
 */
