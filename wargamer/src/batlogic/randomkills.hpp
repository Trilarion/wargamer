/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef RandomKills_HPP
#define RandomKills_HPP

#ifndef __cplusplus
#error RandomKills.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 * Random Kills for debugging
 *
 *----------------------------------------------------------------------
 */

#include "bl_dll.h"

#include "batdata.hpp"

namespace B_Logic
{

BATLOGIC_DLL void randomKills(RPBattleData batdata);

};


#endif /* RandomKills_HPP */

