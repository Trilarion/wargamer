/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef BMOVE_HPP
#define BMOVE_HPP

#include "batdata.hpp"
#include "batunit.hpp"
#include "bobdef.hpp"

//namespace BattleMeasure { class HexCord; };
//using namespace BOB_Definitions;

namespace B_Logic
{

class Movement_Imp;

class Movement_Int {
	 Movement_Imp* d_move;
  public:
	 Movement_Int(RPBattleData batData);
	 ~Movement_Int();

	 bool startMovement(const RefBattleCP& cp);
};

};
#endif
