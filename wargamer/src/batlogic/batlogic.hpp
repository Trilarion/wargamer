/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef BATLOGIC_HPP
#define BATLOGIC_HPP

#ifndef __cplusplus
#error batlogic.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *	Battle Game Logic Thread
 *
 * Note that the imlpementation is hidden
 *
 *----------------------------------------------------------------------
 */

#include "bl_dll.h"
#include "refptr.hpp"
#include "battime.hpp"
//#include "batdata.hpp"
//#include "batctrl.hpp"





class BattleLogicImp;
class BattleGameInterface;
// class BattleData;
// class ThreadManager;

class BattleLogic : public RefBaseDel
{
	public:
		BATLOGIC_DLL BattleLogic(BattleGameInterface* batgame);
		BATLOGIC_DLL ~BattleLogic();

		BATLOGIC_DLL void start();
		BATLOGIC_DLL void reinforced();
      BATLOGIC_DLL void newDay();
		BATLOGIC_DLL void updateTime(BattleMeasure::BattleTime::Tick tick);

	private:
		BattleLogicImp* d_imp;
};

#endif /* BATLOGIC_HPP */

