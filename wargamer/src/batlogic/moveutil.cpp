/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
#include "stdinc.hpp"
#include "moveutil.hpp"
#include "batdata.hpp"
#include "batarmy.hpp"
#include "bobutil.hpp"
#include "hexdata.hpp"
#include "broute.hpp"
#include "hexmap.hpp"
#include "b_tables.hpp"
#include "pdeploy.hpp"
#include "bobiter.hpp"
#include <math.h>
#include <utility>

#ifdef _MSC_VER
using namespace std::rel_ops;
#endif


namespace Local {

inline int squared(int v)
{
   return (v * v);
}

inline double getDist(const HexCord& srcHex, const HexCord& destHex)
{
   return sqrt(squared(destHex.x() - srcHex.x()) + squared(destHex.y() - srcHex.y()));
}

HexPosition::Facing hexToFacing(const HexCord& srcHex, const HexCord& destHex)
{
   if(getDist(srcHex, destHex) <= 4)
    return HexPosition::Facing_Undefined;

   SWORD x = destHex.x() - srcHex.x();
   SWORD y = destHex.y() - srcHex.y();

   // get byte angle
   Bangle bFace = wangleToBangle(direction(x, y));

  // convert to HexPosition enum
   const int FacingResolution = 256;

  if(bFace < HexPosition::NorthEastFace)
    return HexPosition::NorthEastPoint;
  else if(bFace <= HexPosition::NorthWestFace)
    return HexPosition::North;
  else if(bFace <= HexPosition::West)
    return HexPosition::NorthWestPoint;
  else if(bFace < HexPosition::SouthWestFace)
    return HexPosition::SouthWestPoint;
  else if(bFace <= HexPosition::SouthEastFace)
    return HexPosition::South;
  else
    return HexPosition::SouthEastPoint;
}

}; // end namespace Local

namespace B_Logic
{


// TODO: move this somewhere else
// as we must call broute.hpp, which calls this source file
void plotHQRoute(RPBattleData bd, const RefBattleCP& cp)
{
  cp->routeList().reset();
  if(cp->attached())
  {
#if 0
    RefBattleCP attachedTo = cp->getCurrentOrder().attachTo();
    ASSERT(attachedTo != NoBattleCP);

    // if attachedTo unit is not deploying,
    // copy route from sp we share hex with
    BattleUnit* bu = 0;
    HexCord hex;
    if(attachedTo->getRank().sameRank(Rank_Division))
    {
      int r = (attachedTo->nextFormation() == CPF_Extended) ? 0 : minimum(1, attachedTo->wantRows() - 1);
//    attachedTo->spCount() - 1 : attachedTo->wantRows() - 1;
      int c = maximum(0, (attachedTo->wantColumns() / 2) - 1);

      DeployItem* di = attachedTo->wantDeployItem(c, r);
      ASSERT(di);
      ASSERT(di->active());
      if(di && di->active())
      {
         bu = di->d_sp;
         hex = di->d_wantHex;
      }
      else
      {
         cp->setUnattached();
         cp->getCurrentOrder().attachTo(NoBattleCP);
         return;
      }
    }
    else
      bu = attachedTo;

//     cp->hexPosition() = di->d_sp->hexPosition();

    if(!attachedTo->deploying())
    {
      ASSERT(bu->hex() == cp->hex());

      SListIterR<HexItem> iter(&bu->routeList());
      while(++iter)
         cp->routeList().newItem(iter.current()->d_hex);

      if(cp->routeList().entries() > 0)
         cp->destHex(cp->routeList().getLast()->d_hex);
    }

    // deploying
    else
    {
      ASSERT(attachedTo->getRank().sameRank(Rank_Division));
      cp->destHex(hex);

      // use route finder to plot route
      B_Route br(bd, cp, cp);
      br.plotRoute(cp->hex(), cp->destHex(), 0);
    }
#endif
   }
   else
   {
      ASSERT(cp->getCurrentOrder().nextWayPoint());
      cp->destHex(cp->getCurrentOrder().wayPoints().getLast()->d_hex);

      // use route finder to plot route
      B_Route br(bd, cp, cp);
      br.plotRoute(cp->hex(), cp->destHex(), 0);
      SListIter<HexItem> iter(&cp->routeList());
      while(++iter)
      {
         iter.current()->d_facing = cp->facing();
      }
   }
}

bool adjacentHexFrontal(RPBattleData bd, BattleUnit* bu, HexCord& currentHex, const HexCord& destHex)
{
   enum { RightFront, LeftFront, FacingSides };
   for(int i = 0; i < FacingSides; i++)
   {
    HexCord::HexDirection dir = (i == RightFront) ? rightFront(bu->facing()) : leftFront(bu->facing());

    HexCord edgeHex;
    if(bd->moveHex(currentHex, dir, edgeHex))
    {
      if(edgeHex == destHex)
      {
         currentHex = destHex;
         return True;
      }
    }
   }

   return False;
}


bool adjacentHex(const HexCord& hex1, const HexCord& hex2)
{
//  return ( (abValue(hex1.x() - hex2.x()) <= 1) && (abValue(hex1.y() - hex2.y()) <= 1) );
   if(hex1.y() % 2 == 0)
   {
      if(hex1.y() == hex2.y())
         return (abValue(hex1.x() - hex2.x()) == 1);

      else
      {
         int xDif = hex2.x() - hex1.x();
         return (abValue(hex1.y() - hex2.y()) == 1  && (xDif == -1 || xDif == 0));
      }
   }
   else
   {
      if(hex1.y() == hex2.y())
         return (abValue(hex1.x() - hex2.x()) == 1);

      else
      {
         int xDif = hex2.x() - hex1.x();
         return (abValue(hex1.y() - hex2.y()) == 1  && (xDif == 1 || xDif == 0));
      }
   }
}

bool defaultHex(RPBattleData bd, BattleUnit* bu,
   HexCord& currentHex, const HexCord& destHex, const HexCord* excludeHex)
{
   /*
   * Default routine
   */

   HexCord::HexDirection bestDir = HexCord::Stationary;
   double bestDif = ULONG_MAX;
   for(HexCord::HexDirection d = HexCord::First; d < HexCord::End; INCREMENT(d))
   {
    HexCord edgeHex;
    if(bd->moveHex(currentHex, d, edgeHex))
    {
      if( (excludeHex) &&
          (edgeHex == *excludeHex) )
      {
         continue;
      }

      double dif = Local::getDist(edgeHex, destHex);

      if( (bestDir == HexCord::Stationary) ||
          (dif < bestDif) )
      {
         bestDir = d;
        bestDif = dif;
      }
    }
   }

  ASSERT(bestDir != HexCord::Stationary);
  HexCord lh = currentHex;

  bool result = bd->moveHex(lh, bestDir, currentHex);
  ASSERT(result);

  if(!result)
    FORCEASSERT("Next Hex not found!");
  return result;
}

bool plotCPRoute(RPBattleData bd, const RefBattleCP& cp)
{
  cp->routeList().reset();
  HexCord hex = cp->hex();

  while(cp->destHex() != hex)
  {
    // first, see if dest is right in front of us
    HexCord h = hex;
    HexPosition::Facing newFace = cp->facing();

    if(adjacentHexFrontal(bd, cp, h, cp->destHex()))
    {
      HexItem* hi = new HexItem(h, cp->facing());
      ASSERT(hi);
      cp->routeList().append(hi);
      hex = h;
    }

    else
    {
      bool found = defaultHex(bd, cp, h, cp->destHex());
      ASSERT(found);

      if(found)
      {
        HexItem* hi = new HexItem(h, cp->facing());
        ASSERT(hi);
        cp->routeList().append(hi);
        hex = h;
      }
    }
  }

  return True;
}

bool adjacentToSameHex(RPBattleData bd, const HexCord& sHex1, const HexCord& sHex2,
   const HexCord::HexDirection hd1, const HexCord::HexDirection hd2)
{
  HexCord hex1;
   if(bd->moveHex(sHex1, hd1, hex1))
  {
    HexCord hex2;
    if(bd->moveHex(sHex2, hd2, hex2))
      return (hex1 == hex2);
  }

  return False;
}

bool updateWhichWay(RPBattleData bd, const RefBattleCP& cp)
{
  if(cp->rows() > 1)
  {
    DeployItem* di1 = cp->currentDeployItem(0, 0);
    ASSERT(di1);
    DeployItem* di2 = cp->currentDeployItem(0, 1);
    ASSERT(di2);

    if(!di1 || !di1->active() || !di2 || !di2->active())
    {
      cp->whichWay(BattleCP::HO_Left);
      return True;
    }

    enum { Left, Right, HowMany };
    enum { Rear, Flank, Front, RF_HowMany };
    for(int i = Left; i < HowMany; i++)
    {
      for(int h = 0; h < RF_HowMany; h++)
      {
        HexCord::HexDirection hd;
        if(h == Rear)
          hd = (i == Left) ? leftRear(cp->facing()) : rightRear(cp->facing());
        else if(h == Flank)
          hd = (i == Left) ? leftFlank(cp->facing()) : rightFlank(cp->facing());
        else
          hd = (i == Left) ? leftFront(cp->facing()) : rightFront(cp->facing());

        HexCord nextHex;
        if(bd->moveHex(di1->d_hex, hd, nextHex))
        {
          if(nextHex == di2->d_hex)
          {
            cp->whichWay((i == Left) ? BattleCP::HO_Right : BattleCP::HO_Left);
            return True;
          }
      }
      }
    }

#ifdef DEBUG
    ASSERT(cp->columns() > 1);
    HexCord::HexDirection hd1 = rightRear(cp->facing());
    HexCord::HexDirection hd2 = leftFlank(cp->facing());

    if(!adjacentToSameHex(bd, di1->d_hex, di2->d_hex, hd1, hd2))// && !cp->needsReadjustment())
      FORCEASSERT("SP out of alignment in syncMapToSP()");
#endif

    cp->whichWay(BattleCP::HO_Left);
    return True;
  }
  else
    return True;
}

#if 0
bool toTheRear(CPBattleData bd, const HexPosition::Facing facing,
  HexCord& rearHex, const HexCord& destHex, UWORD nHexes)
{
   enum { Left, Right, HowMany } whichWay = Left;
  HexCord hex = destHex;
  while(nHexes--)
  {
    HexCord::HexDirection hd = (whichWay == Left) ? leftRear(facing) : rightRear(facing);
    HexCord nextHex;
    if(bd->moveHex(hex, hd, nextHex))
      hex = nextHex;

    whichWay = (whichWay == Left) ? Right : Left;
  }

  rearHex = hex;
  return True;
}
#endif
#if 0
enum MoveWhatDirection {
   MWD_Front,
    MWD_LeftFront,
    MWD_RightFront,
    MWD_Left,
    MWD_Right,
    MWD_LeftRear,
    MWD_RightRear,
    MWD_Rear,
    MWD_HowMany
};
#endif
#ifdef DEBUG
const char* moveWhatDirectionName(MoveWhatDirection wd)
{
   ASSERT(wd < MWD_HowMany);
    static const char* s_table[MWD_HowMany] = {
      "Front",
        "Left-Front",
        "Right-Front",
        "Left",
        "Right",
        "Left-Rear",
        "Right-Rear",
        "Rear"
    };
    
    return s_table[wd];
}
#endif

bool plotMove(
   CPBattleData bd, 
    const HexPosition::Facing facing, 
    const HexCord& startHex, 
    HexCord& destHex, 
    UWORD nHexes,
    MoveWhatDirection wd,
    RefBattleCP cp)
{
   ASSERT(wd < MWD_HowMany);
   HexCord hex = startHex;
   enum { Left, Right } whichWay = Left;
    while(nHexes--)
    {
      HexCord::HexDirection hd;
         switch(wd)
         {
         case MWD_Front:
               hd = (whichWay == Left) ? leftFront(facing) : rightFront(facing);
                break;

            case MWD_LeftFront:
               hd = leftFront(facing);
                break;

            case MWD_RightFront:
               hd = rightFront(facing);
                break;

            case MWD_Left:
               hd = leftFlank(facing);
                break;

            case MWD_Right:
               hd = rightFlank(facing);
                break;

            case MWD_LeftRear:
               hd = leftRear(facing);
                break;

            case MWD_RightRear:
               hd = rightRear(facing);
                break;

            case MWD_Rear:
               hd = (whichWay == Left) ? leftRear(facing) : rightRear(facing);
                break;

#ifdef DEBUG
         default:
               FORCEASSERT("Improper type in plotMove");
#endif
      }

      HexCord nextHex;
      if(bd->moveHex(hex, hd, nextHex))
      {
         if(cp != NoBattleCP)
         {
            // if(canHexBeOccupied(const_cast<BattleData*>(reinterpret_cast<const BattleData*>(bd)), cp, nextHex, False, False))
            if(canHexBeOccupied(bd, cp, nextHex, False, False))
               hex = nextHex;
            else
               break;
         }
         else
            hex = nextHex;
      }
      else
         break;

      whichWay = (whichWay == Left) ? Right : Left;

    }

    destHex = hex;
    return (destHex != startHex);
}

void syncMapToSP(RPBattleData bd, const RefBattleCP& cp)
{
  const int spCount = BobUtility::countSP(cp);
  ASSERT(spCount > 0);

  cp->syncMapToSP();
}

#ifdef DEBUG
bool checkAlignment(RPBattleData bd, HexCord& lHex, HexCord& bHex, HexPosition::Facing f)
{
  Boolean outOfAlign = True;
  enum { Left, Right, HowMany };
   for(int i = 0; i < HowMany; i++)
  {
    HexCord::HexDirection hd = (i == Left) ? leftRear(f) : rightRear(f);
    HexCord hex;

    if(bd->moveHex(lHex, hd, hex))
    {
      if(hex == bHex)
      {
        outOfAlign = False;
        break;
      }
    }
  }

  ASSERT(!outOfAlign);
  return !outOfAlign;
}
#endif

HexCord::HexDirection hexDirection(RPBattleData bd, const HexCord& srcHex, const HexCord& destHex)
{
  for(HexCord::HexDirection hd = HexCord::First; hd < HexCord::Count; INCREMENT(hd))
  {
    HexCord hex;
    if(bd->moveHex(srcHex, hd, hex))
    {
      if(hex == destHex)
        return hd;
    }
  }

  FORCEASSERT("Hex not adjacent");
  return HexCord::Stationary;
}

#ifdef DEBUG
const char* faceName(HexPosition::Facing f)
{
  if(f == HexPosition::East)
    return "East";
  else if(f == HexPosition::NorthEastPoint)
    return "NorthEastPoint";
  else if(f == HexPosition::NorthEastFace)
    return "NorthEastFace";
  else if(f == HexPosition::North)
    return "North";
  else if(f == HexPosition::NorthWestFace)
    return "NorthWestFace";
  else if(f == HexPosition::NorthWestPoint)
    return "NorthWestPoint";
  else if(f == HexPosition::West)
    return "West";
   else if(f == HexPosition::SouthWestPoint)
    return "SouthWestPoint";
   else if(f == HexPosition::SouthWestFace)
    return "SouthWestFace";
   else if(f == HexPosition::South)
    return "South";
   else if(f == HexPosition::SouthEastFace)
    return "SouthEastFace";
   else if(f == HexPosition::SouthEastPoint)
    return "SouthEastPoint";
   else if(f == HexPosition::Facing_Undefined)
    return "Undefined";
   else
    return "Not Found";
}
#endif

// is this CP moving (Does not count SP that are deploying-redeploying)
// Or if it is currently changing sp formation
bool isMoving(const CRefBattleCP& cp)
{
   if(cp->deploying() || cp->liningUp() || cp->markingTime())
      return False;

// if(cp->getCurrentOrder().wayPoints().entries() > 0)
//    return True;

   if(cp->getRank().sameRank(Rank_Division))
   {
      for(std::vector<DeployItem>::const_iterator di = cp->mapBegin();
            di != cp->mapEnd();
            ++di)
      {
         if(di->active() && (di->d_sp->isMoving()) )// || di->d_sp->routeList().entries() > 0) )
            return True;
      }
   }
   else
   {
      return (cp->isMoving() || cp->routeList().entries() > 0);
   }

   return False;
}

/*
 * Can cp1 pass through cp2?
 */

bool canPassThrough(const CRefBattleCP& cp1, BattleUnit* bu, const CRefBattleCP& cp2)
{
   // Can never pass through if different sides
   if(cp1->getSide() != cp2->getSide())
      return False;

   return True;
}

// see if hex is occupied by another unit
#ifdef DEBUG
bool hexNotOccupied(RPBattleData bd, HexCord& nextHex, BattleUnit* bu, CRefBattleCP excludeCP, LogFile& lf)
#else 
bool hexNotOccupied(RPBattleData bd, HexCord& nextHex, BattleUnit* bu, CRefBattleCP excludeCP)
#endif
{
#ifdef DEBUG
   lf.printf("Seeing if %s can move into hex(%d, %d)",   excludeCP->getName(),
      static_cast<int>(nextHex.x()), static_cast<int>(nextHex.y()));
#endif         
   /*
   * 1. SP cannot move if dest hex is occupied by another unit that is not
   * moving.
   */

   BattleHexMap* hexMap = bd->hexMap();

   BattleHexMap::iterator lower;
   BattleHexMap::iterator upper;
   if(hexMap->find(nextHex, lower, upper))
   {
      while(lower != upper)
      {
         const BattleUnit* bu2 = (*lower).second;
         CRefBattleSP sp2 = (bu) ? BobUtility::getSP(bu) : NoBattleSP;
         if(sp2 != NoBattleSP && sp2->strength() == 0)
            ;
         else if(excludeCP != NoBattleCP)
         {
            CRefBattleCP cp = BobUtility::getCP(bu2);
            if(cp->getSide() != excludeCP->getSide())
                {
#ifdef DEBUG
               lf.printf("Unable to continue as %s is occupying dest hex", cp->getName());
#endif
               return False;
                }
//            if(!canPassThrough(excludeCP, bu, cp))
//               return False;

         }

         ++lower;
      }
   }


   /*
    * 2. Now check all adjacent hexes. If a unit is moving into our next hex
    *    we cannot continue
    */

   HexCord hex;
   for(HexCord::HexDirection hd = HexCord::First; hd < HexCord::End; INCREMENT(hd))
   {
      if(bd->moveHex(nextHex, hd, hex) &&
          (!bu || hex != bu->nextHex()->d_hex) )
      {
         if(hexMap->find(hex, lower, upper))
         {
            while(lower != upper)
            {
               const BattleUnit* bu2 = (*lower).second;
               CRefBattleCP cp = BobUtility::getCP(bu2);
               if(cp->getSide() != excludeCP->getSide() &&
                  bu2->isMoving() &&
                  !bu2->markingTime() &&
                  bu2->nextHex() &&
                  bu2->nextHex()->d_hex == nextHex)
               {
#ifdef DEBUG
                  lf.printf("Unable to continue as %s is moving into dest hex",
                           cp->getName());
#endif
                  return False;
               }
//             if(!canPassThrough(excludeCP, bu, cp))
//                return False;

               ++lower;
            }
         }
      }
   }

    /*
     * 3. See if any nearby enemy are manuevering or deploying,
     *    if so, we cannot move into one of their route hexes
     *    A bit of a bodge, but keeps things from getting out of wack
     *    do to misaligned SP
     */
     
    {
      for(BattleUnitIter cpIter(bd->ob(), (excludeCP->getSide() == 0) ? 1 : 0); 
            !cpIter.isFinished();
            cpIter.next())
        {
         if(cpIter.cp()->getRank().sameRank(Rank_Division) &&
               (cpIter.cp()->onManuever() || cpIter.cp()->deploying()) )
            {
               for(std::vector<DeployItem>::iterator di = cpIter.cp()->mapBegin();
                  di != cpIter.cp()->mapEnd();
                    ++di)
                {
                  if(di->active())
                    {
                     SListIter<HexItem> iter(&di->d_sp->routeList());
                        while(++iter)
                        {
                           if(iter.current()->d_hex == nextHex)
                            {
#ifdef DEBUG
                        lf.printf("Unable to continue as %s is manuevering/deploying into our desthex",
                                 cpIter.cp()->getName());
#endif
                              return False;
                            }
                        }
                    }
                }
            }
        }
    }
   return True;
}

/*
 * See if this sp's next hex is occupied by another unit
 */

#ifdef DEBUG
bool hexNotOccupied(RPBattleData bd, BattleUnit* bu, CRefBattleCP excludeCP, LogFile& lf)
#else 
bool hexNotOccupied(RPBattleData bd, BattleUnit* bu, CRefBattleCP excludeCP)
#endif
{
   ASSERT(bu != NoBattleUnit);
   ASSERT(bu->nextHex());
   ASSERT(bu->hexPosition().atCenter());

   if(bu->hex() == bu->nextHex()->d_hex && bu->routeList().entries() < 2)
      return True;

   if(excludeCP == NoBattleCP)
      return True;

   bu->routeList().first();
   HexCord nextHex = (bu->hex() == bu->nextHex()->d_hex) ?
       bu->routeList().next()->d_hex : bu->nextHex()->d_hex;

#ifdef DEBUG
   return hexNotOccupied(bd, nextHex, bu, excludeCP, lf);
#else         
   return hexNotOccupied(bd, nextHex, bu, excludeCP);
#endif 
   
}

bool rightDestHex(RPBattleData bd, HexPosition::Facing f, const HexCord& destHexL, int nColumns, HexCord& hex)
{
    ASSERT(nColumns >= 1);
    HexCord::HexDirection hd = rightFlank(f);
    hex = destHexL;
    while(--nColumns)
    {
      HexCord lHex;
      if(bd->moveHex(hex, hd, lHex))
         hex = lHex;
      else
         return False;
         //FORCEASSERT("Hex not found in rightDestHex()");
   }

   return True;
}


CPManuever moveHow(RPBattleData bd, const RefBattleCP& cp)
{
   ASSERT(cp->getRank().sameRank(Rank_Division));
   ASSERT(cp->sp() != NoBattleSP);

   CPManuever m = CPM_Forward;
//    cp->movingBackwards(False);

   BattleOrder& order = cp->getCurrentOrder();

   // Do a wheel or about face if needed if we are at destination
   if(!order.nextWayPoint())
   {
      if(order.facing() != cp->facing())
      {
         enum LFace {
            NE,
            N,
            NW,
            SW,
            S,
            SE,

            LFace_HowMany,
            LFace_Undefined = LFace_HowMany
         };

         LFace curFace =  (cp->facing() == HexPosition::NorthEastPoint) ? NE :
                          (cp->facing() == HexPosition::North) ? N :
                          (cp->facing() == HexPosition::NorthWestPoint) ? NW :
                          (cp->facing() == HexPosition::SouthWestPoint) ? SW :
                          (cp->facing() == HexPosition::South) ? S :
                          (cp->facing() == HexPosition::SouthEastPoint) ? SE : LFace_Undefined;
         ASSERT(!(curFace == LFace_Undefined));

         LFace wantFace = (order.facing() == HexPosition::NorthEastPoint) ? NE :
                          (order.facing() == HexPosition::North) ? N :
                          (order.facing() == HexPosition::NorthWestPoint) ? NW :
                          (order.facing() == HexPosition::SouthWestPoint) ? SW :
                          (order.facing() == HexPosition::South) ? S :
                          (order.facing() == HexPosition::SouthEastPoint) ? SE : LFace_Undefined;
         ASSERT(!(wantFace == LFace_Undefined));

         static const CPManuever s_moveHow[CPFaceHow_HowMany][LFace_HowMany][LFace_HowMany] = {
            // CPH_Front
            {
               // NE              N              NW             SW             S             SE
               { CPM_Forward,   CPM_WheelL,    CPM_WheelL,    CPM_AboutFace, CPM_WheelR,    CPM_WheelR}, // NE
               { CPM_WheelR,    CPM_Forward,   CPM_WheelL,    CPM_WheelL,    CPM_AboutFace, CPM_WheelR}, // N
               { CPM_WheelR,    CPM_WheelR,    CPM_Forward,   CPM_WheelL,    CPM_WheelL,    CPM_AboutFace}, // NW
               { CPM_AboutFace, CPM_WheelR,    CPM_WheelR,    CPM_Forward,   CPM_WheelL,    CPM_WheelL}, // SW
               { CPM_WheelL,    CPM_AboutFace, CPM_WheelR,    CPM_WheelR,    CPM_Forward,   CPM_WheelL}, // S
               { CPM_WheelL,    CPM_WheelL,    CPM_AboutFace, CPM_WheelR,    CPM_WheelR,    CPM_Forward}  // SE
            },

            // CPH_Back
            {
               // NE               N           NW               SW              S           SE
               { CPM_Forward,   CPM_RWheelL,   CPM_RWheelL,   CPM_AboutFace, CPM_RWheelR,   CPM_RWheelR}, // NE
               { CPM_RWheelR,   CPM_Forward,   CPM_RWheelL,   CPM_RWheelL,   CPM_AboutFace, CPM_RWheelR}, // N
               { CPM_RWheelR,   CPM_RWheelR,   CPM_Forward,   CPM_RWheelL,   CPM_RWheelL,   CPM_AboutFace}, // NW
               { CPM_AboutFace, CPM_RWheelR,   CPM_RWheelR,   CPM_Forward,   CPM_RWheelL,   CPM_RWheelL}, // SW
               { CPM_RWheelL,   CPM_AboutFace, CPM_RWheelR,   CPM_RWheelR,   CPM_Forward,   CPM_RWheelL}, // S
               { CPM_RWheelL,   CPM_RWheelL,   CPM_AboutFace, CPM_RWheelR,   CPM_RWheelR,   CPM_Forward}  // SE
            }
         };

         ASSERT(order.order().d_faceHow < CPFaceHow_HowMany);
         m = s_moveHow[order.order().d_faceHow][curFace][wantFace];
      }
   }

   // otherwise, keep facing forward,
   else
   {
      m = CPM_Forward;
   }

   return m;
}

// clear out any nodes that may turn back on themselves
void cleanUpRoute(RPBattleData bd, HexList& rList)
{
   if(rList.entries() > 1)
   {
    HexItem* first = rList.first();
    ASSERT(first);

    HexItem* next = rList.next();
    int loop = 0;
    while(next)
    {
      if(++loop > 500)
      {
         FORCEASSERT("Infinite loop in cleanUpRoute()!");
         break;
      }

      HexItem* n2 = rList.next();

      bool removed = False;

      if(n2)
      {
        if( (first->d_hex == n2->d_hex) )
        {
          rList.remove(next);
          rList.remove(n2);
          removed = True;
        }

        else if( (adjacentHex(first->d_hex, next->d_hex)) &&
                 (adjacentHex(first->d_hex, n2->d_hex)) )
        {
          // remove
          rList.remove(next);
          removed = True;
        }

        if(removed)
        {
          first = rList.first();
          next = rList.next();
         }
      }

      if(!removed)
      {
        first = next;
        next = n2;
      }
    }
  }
}

void cleanUpRoute(RPBattleData bd, const RefBattleCP& cp)
{
   for(std::vector<DeployItem>::iterator di = cp->mapBegin();
      di != cp->mapEnd();
      di++)
   {
    if(di->active())
    {
      cleanUpRoute(bd, di->d_sp->routeList());
    }
   }
}

bool canHexBeOccupied(RCPBattleData bd, const RefBattleCP& cp, const HexCord& hex, bool currentFormation, bool allowForBridge)
{
   return PlayerDeploy::canHexBeOccupied(bd, cp, hex, False, False, currentFormation, allowForBridge);
}

// is dest hex to the rear ?
// that is at an angle of between 135 and 225 degrees relative to facing
bool toTheRear(const HexPosition::Facing facing, const HexCord& srcHex, const HexCord& destHex)
{
   if(srcHex == destHex)
    return False;

   // get distances
   // if within 4 hexes, do no facing changes
//  if(getDist(srcHex, destHex) <= 4)
//       return f;

   SWORD x = destHex.x() - srcHex.x();
   SWORD y = destHex.y() - srcHex.y();

   // get byte angle
   Bangle bFace = wangleToBangle(direction(x, y));

   int abv = abValue(facing - bFace);

   return ( (abv >= 64) && (abv <= 192) );

}

// in what general orientation are we moving?
// i.e. either Front, Left, Right, or Rear
MoveOrientation moveOrientation(const HexPosition::Facing facing, const HexCord& srcHex, const HexCord& destHex,  MO_Angle angle)
{
   if(srcHex == destHex)
      return MO_Front;

   SWORD x = destHex.x() - srcHex.x();
   SWORD y = destHex.y() - srcHex.y();

   // get byte angle
   Bangle bFace = wangleToBangle(direction(x, y));

   const HexPosition::Facing base = 64;
   int dif = base - facing;

   if(bFace + dif >= HexPosition::FacingResolution || bFace + dif < 0)
   {
      bFace = (bFace + dif >= HexPosition::FacingResolution) ?
          (bFace + dif) - HexPosition::FacingResolution :
          HexPosition::FacingResolution + (bFace + dif);
   }
   else
      bFace += dif;

   switch(angle)
   {
         case MOA_Normal:
         {
         if( (bFace < 32 || bFace > 224) )
            return MO_Right;
         else if(bFace <= 96)
            return MO_Front;
         else if(bFace < 160)
            return MO_Left;
         else
            return MO_Rear;
        }

        case MOA_Narrow:
        {
         if( (bFace < 49 || bFace > 207) )
            return MO_Right;
         else if(bFace <= 79)
            return MO_Front;
         else if(bFace < 177)
            return MO_Left;
         else
            return MO_Rear;
        }

        case MOA_Wide:
        {
         if( (bFace < 15 || bFace > 241) )
            return MO_Right;
         else if(bFace <= 79)
            return MO_Front;
         else if(bFace < 143)
            return MO_Left;
         else
            return MO_Rear;
        }
        case MOA_NormalFrontWideBack:
        {
         if( (bFace < 32 || bFace > 241) )
            return MO_Right;
         else if(bFace <= 96)
            return MO_Front;
         else if(bFace < 143)
            return MO_Left;
         else
            return MO_Rear;
        }
      default:
        {
#ifdef DEBUG
         FORCEASSERT("Improper angle in moveOrientation()");
#endif
            return MO_Front;
        }
   }

#if 0
    if(srcHex == destHex)
         return MO_Front;

    SWORD x = destHex.x() - srcHex.x();
   SWORD y = destHex.y() - srcHex.y();

   // get byte angle
   Bangle bFace = wangleToBangle(direction(x, y));

   int abv = abValue(facing - bFace);
   if( (abv >= 42 && abv < 85) )
         return MO_Left;
    else if( (abv >= 85 && abv <= 170) )
         return MO_Rear;
    else if( (abv > 170 && abv <= 213) )
         return MO_Right;
    else
         return MO_Front;
#endif
}

// get distance from hex to unit
// if unit is a XX, use dist to closest SP
bool withinNHexes(const HexCord& srcHex, const HexCord& destHex, const int nHexes)
{
   // get distances
   // if within 4 hexes, do no facing changes
    return (Local::getDist(srcHex, destHex) <= nHexes);
}

int distanceFromUnit(const CRefBattleCP& cp, const CRefBattleCP& enemyCP)
{
   return BobUtility::distanceFromUnit(cp, enemyCP);
}

#ifdef DEBUG
bool plotMovementToTarget(RPBattleData bd, const RefBattleCP& cp, LogFile& lf)
#else
bool plotMovementToTarget(RPBattleData bd, const RefBattleCP& cp)
#endif
{
   if(cp->getCurrentOrder().wayPoints().entries() == 0 &&
       cp->targetList().closeRangeFront() != UWORD_MAX)
   {
      cp->clearMove(True);

      RefBattleCP targetCP = cp->targetList().first()->d_cp;

#ifdef DEBUG
      lf.printf("%s is plotting movement to enemy %s", cp->getName(), targetCP->getName());
#endif
      //HexCord targetHexR = targetCP->leftHex();
      HexCord targetHexL = targetCP->rightHex();
      HexCord ourLeft = cp->leftHex();
      MoveOrientation mo = moveOrientation(cp->facing(), ourLeft, targetHexL, MOA_Narrow);
        ASSERT(mo == MO_Left || mo == MO_Right || mo == MO_Front);
      //MoveOrientation moL = moveOrientation(cp->facing(), ourLeft, targetHexR, MOA_Narrow);
      int m = maximum(1, distanceFromUnit(cp, targetCP) - 1);
        
        
      MoveWhatDirection wd = (mo == MO_Front) ? MWD_Front :
                               (mo == MO_Left)  ? MWD_LeftFront : MWD_RightFront;
                
      HexCord hex;
      if(plotMove(bd, cp->facing(), ourLeft, hex, m, wd))
        {
#ifdef DEBUG
         lf.printf("Moving to the %s", moveWhatDirectionName(wd));
            lf.printf("%s is moving from hex(%d, %d) to hex(%d, %d)",
               cp->getName(),
                static_cast<int>(ourLeft.x()), static_cast<int>(ourLeft.y()),
                static_cast<int>(hex.x()), static_cast<int>(hex.y()));
#endif
         // clear out all waypoints
         cp->getCurrentOrder().wayPoints().reset();
         cp->getCurrentOrder().order().d_whatOrder.reset();
         cp->getCurrentOrder().order().d_divFormation = cp->nextFormation();
         if(cp->sp() != NoBattleSP)
            cp->getCurrentOrder().order().d_spFormation = cp->sp()->destFormation();

         WayPoint* wp = new WayPoint(hex, cp->getCurrentOrder().order());
         ASSERT(wp);

//       if(cp->getRank().sameRank(Rank_Division))
//          wp->d_order.d_whatOrder.add(WhatOrder::Facing);

         cp->getCurrentOrder().wayPoints().append(wp);
         cp->getCurrentOrder().mode(BattleOrderInfo::Move);
         cp->getCurrentOrder().resetOrder();
            
            return True;
        }
#ifdef DEBUG
      else
         lf.printf("Unable to plot route to target");
#endif        
   }

   return False;
}

HexPosition::Facing frontFacing(const HexCord& leftHex, const HexCord& rightHex)
   // Which direction does the front face? (as defined by the line leftHex - rightHex)
{
   SWORD x = rightHex.x() - leftHex.x();
   SWORD y = rightHex.y() - leftHex.y();

   // get byte angle
   Bangle bFace = wangleToBangle(direction(x, y));

   return (bFace <= 32 || bFace >= 233) ? HexPosition::North :
             (bFace <= 64 ) ? HexPosition::NorthWestPoint :
             (bFace <= 96 ) ? HexPosition::SouthWestPoint :
                   (bFace <= 160) ? HexPosition::South :
             (bFace <= 192) ? HexPosition::SouthEastPoint : HexPosition::NorthEastPoint;

}

inline bool canMoveOnRoad(const RefBattleCP& cp)
{
   return (cp->getRank().isHigher(Rank_Division) || (cp->formation() == CPF_March && cp->spFormation() == SP_MarchFormation));
}

#ifdef DEBUG
bool plotRetreatHex(RPBattleData bd, const RefBattleCP& cp, const int nHexes, LogFile& lf)
#else
bool plotRetreatHex(RPBattleData bd, const RefBattleCP& cp, const int nHexes)
#endif
{
#ifdef DEBUG
   lf.printf("Plotting retreat rout for %s (%d hexes)", cp->getName(), nHexes);
#endif
   HexCord curHex = (cp->getRank().sameRank(Rank_Division)) ? cp->leftHex() : cp->hex();

   // clear out all waypoints
   cp->getCurrentOrder().wayPoints().reset();
   cp->getCurrentOrder().order().d_whatOrder.reset();
   cp->getCurrentOrder().order().d_divFormation = cp->nextFormation();
   if(cp->sp() != NoBattleSP)
         cp->getCurrentOrder().order().d_spFormation = cp->sp()->destFormation();


    // if we are fleeing the field and the enemy is not near, do an about turn if we
    // are not facing the right direction
    HexPosition::Facing facing = cp->facing();
    if(cp->fleeingTheField() && cp->getRank().sameRank(Rank_Division) && cp->targetList().closeRange() >= 880)
    {
        HexCord lHex;
        HexCord rHex;
        bd->getSideLeftRight(cp->getSide(), lHex, rHex);
        HexPosition::Facing frontFace = frontFacing(lHex, rHex);
        if(!unitIsFacing(frontFace, cp))
        {
           cp->getCurrentOrder().manuever(CPM_AboutFace);
           cp->getCurrentOrder().whatOrder().insert(WhatOrder::Manuever);
#ifdef DEBUG
           lf.printf("%s is fleeing and doing an about-turn", cp->getName());
#endif
           return False;
        }
        else
           facing = oppositeFace(facing);

#ifdef DEBUG
      lf.printf("%s is fleeing", cp->getName());
#endif
    }

    // get what direction we need to go, avoiding nearby enemy
    const dist = (2 * BattleMeasure::XYardsPerHex);
    bool enemyToLeft       = (cp->targetList().closeRangeLeftFlank() <= dist || cp->targetList().closeRangeLeftRear() <= dist);
    bool enemyToRight      = (cp->targetList().closeRangeRightFlank() <= dist || cp->targetList().closeRangeRightRear() <= dist);
    bool enemyToRear       = (cp->targetList().closeRangeRear() <= dist);

    static const MoveWhatDirection s_table[2][2][2] = {
      // not to the rear
      {
         // not to the right
            //   not left,   left
         {
               MWD_Rear,   MWD_RightRear
            },

            // to the right
            {
               MWD_LeftRear, MWD_Rear
            }
        },

        // to the rear
        {
         // not to the right
         {
               MWD_Left,  MWD_Right
            },

            // to the right
            {
               MWD_Left, MWD_Rear
            }
        }
    };

   MoveWhatDirection wd = s_table[enemyToRear][enemyToRight][enemyToLeft];

   HexCord hex;
   if(plotMove(bd, facing, curHex, hex, nHexes, wd))
   {
      // Now we need to adjust in case we have prohibited terrain in the way
      // especially rivers
       const int maxAdj = 15;
       int nLeft = (enemyToLeft) ? 15 : 0;
       int nRight = (enemyToRight) ? 15 : 0;
       B_Route route(bd, cp, cp);
       for(;;)
       {
         if(route.plotRoute(curHex, hex, 0, !canMoveOnRoad(cp), True))
            break;

         HexCord wHex;
         if(nLeft < maxAdj)
         {
            HexCord::HexDirection hd = leftFlank(facing);
            if(bd->moveHex(hex, hd, wHex))
               hex = wHex;
            else
               nLeft = maxAdj;

            nLeft++;
         }
         else if(nRight < maxAdj)
         {
            HexCord::HexDirection hd = rightFlank(facing);
            if(bd->moveHex(hex, hd, wHex))
               hex = wHex;
            else
               nRight = maxAdj;

            nRight++;
         }
         else
         {
#ifdef DEBUG
            lf.printf("Unable to find retreat route due to prohibited terrain");
#endif
            return False;
         }
       }

       WayPoint* wp = new WayPoint(hex, cp->getCurrentOrder().order());
       ASSERT(wp);

#ifdef DEBUG
       lf.printf("Trying to move to the %s", moveWhatDirectionName(wd));
       lf.printf("Will attempt to retreat from hex(%d, %d) to hex(%d, %d)",
            static_cast<int>(curHex.x()), static_cast<int>(curHex.y()),
            static_cast<int>(hex.x()), static_cast<int>(hex.y()));
#endif
//     if(cp->getRank().sameRank(Rank_Division))
//           wp->d_order.d_whatOrder.add(WhatOrder::Facing);

       cp->getCurrentOrder().wayPoints().append(wp);
       cp->getCurrentOrder().mode(BattleOrderInfo::Move);
       cp->getCurrentOrder().resetOrder();
       cp->retreating(True);
   }

   else
   {
#ifdef DEBUG
      lf.printf("Unable to find retreat route");
#endif
      return False;
   }
   return True;
}

// get the distance from unit to hex
// returns 0 if hex is occupied by cp (or one of its SPs)
int distanceFromUnit(const HexCord& hex, const CRefBattleCP& cp)
{
   return BobUtility::distanceFromUnit(hex, cp);
}

// See if a unit should walk backwards whilst still facing forwards
bool movingBackward(const RefBattleCP& cp, BattleUnit* bu)
{
   return ( (cp->movingBackwards()) || // && cp->targetList().entries() > 0) ||
                
             (cp->deploying() && cp->formation() > cp->nextFormation()) ||
          (cp->onManuever() && (cp->getCurrentOrder().manuever() == CPM_RWheelR || cp->getCurrentOrder().manuever() == CPM_RWheelL)) );
}


bool inATrafficJam(RPBattleData bd, const RefBattleCP& cp, HexCord hex)
{
   bool inAJam = False;
   BattleHexMap* hexMap = bd->hexMap();

   ASSERT(cp->sp() != NoBattleSP);

   BattleHexMap::iterator lower;
   BattleHexMap::iterator upper;
   if(hexMap->find(hex, lower, upper))
   {
      while(lower != upper)
      {
         CRefBattleCP cp2 = BobUtility::getCP((*lower).second);
         CRefBattleSP sp2 = BobUtility::getSP((*lower).second);
#ifdef DEBUG
         if(cp2->getSide() != cp->getSide())
         {
            ASSERT(sp2 != NoBattleSP);
            ASSERT(sp2->strength() == 0);
         }
#endif

         if(cp2 != cp &&
             cp2->getRank().sameRank(Rank_Division) &&
             (sp2 == NoBattleSP || sp2->strength() > 0) )
         {
            inAJam = True;
            break;
         }
         ++lower;
      }
   }

   return inAJam;
}

bool inATrafficJam(RPBattleData bd, const RefBattleCP& cp)
{
   for(std::vector<DeployItem>::const_iterator di = cp->mapBegin();
         di != cp->mapEnd();
         ++di)
   {
      if(di->active())
      {
         if(inATrafficJam(bd, cp, di->d_sp->hex()))
            return True;
      }
   }

   return False;
}

void getNewLeft(const RefBattleCP& cp, HexCord& leftHex)
{
                        int x = -1;
                        int y = -1;
                        int bestToTheRight = -1;
                        for(int r = 0; r < cp->rows(); r++)
                        {
                                    for(int c = 0; c < cp->columns(); c++)
                                    {
                                                DeployItem* di = cp->currentDeployItem(c, r);
                                                ASSERT(di);
                                                if(di && di->active() && di->d_sp->strength() > 0)
                                                {
                                                            if(y == -1)
                                                                        y = di->d_hex.y();
                                                            if(bestToTheRight == -1 || c < bestToTheRight)
                                                            {
                                                                        x = di->d_hex.x();
                                                                        bestToTheRight = c;
                                                            }
                                                }
                                    }
                        }

                        ASSERT(x != -1 || y != -1);
                        leftHex.x(x);
                        leftHex.y(y);
}

bool unitIsFacing(HexPosition::Facing facing, const CRefBattleCP& cp2)
// Is unit facing 'facing'?
{
   HexPosition::Facing bFace = cp2->facing();

// if(facing == bFace)
//    return False;
   const HexPosition::Facing base = 64;
   int dif = base - facing;

   if(bFace + dif >= HexPosition::FacingResolution || bFace + dif < 0)
   {
      bFace = (bFace + dif >= HexPosition::FacingResolution) ?
                  (bFace + dif) - HexPosition::FacingResolution :
                  HexPosition::FacingResolution + (bFace + dif);
   }
   else
      bFace += dif;

   return (bFace >= 128);
}
}; // namespace

/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.3  2001/07/02 13:18:24  greenius
 * Battle, Campaign, Combined game and Campaign Editor now compile and run... mostly
 *
 * Revision 1.2  2001/06/18 08:25:44  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
