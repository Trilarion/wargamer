/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
#ifndef B_REINF_HPP
#define B_REINF_HPP

class BattleGameInterface;

namespace B_Logic
{

class B_ReinforceImp;

class B_ReinforceInt {
      B_ReinforceImp* d_imp;
   public:
      B_ReinforceInt(BattleGameInterface* batgame);
      ~B_ReinforceInt();

      void process();
      void processHistorical();
      void newDay();
};

};


#endif

/*
 *----------------------------------------------------------------------
 * $Log$
 *----------------------------------------------------------------------
 */
