/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef BUNITS_HPP
#define BUNITS_HPP

#ifndef __cplusplus
#error bunits.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *	Process/move all units on battlefield
 *
 *----------------------------------------------------------------------
 */

#include "refptr.hpp"
#include "batdata.hpp"
#include "battime.hpp"

namespace B_Logic
{

class DeployUtility;

//void moveUnits(RPBattleData batData, DeployUtility* depUtil, BattleMeasure::BattleTime::Tick ticks);
void processOrders(RPBattleData batData);

};	// namespace B_Logic

#endif /* BUNITS_HPP */

