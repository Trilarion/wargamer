/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*
 * $Id$
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "b_rction.hpp"
#include "bob_cp.hpp"
#include "bob_sp.hpp"
#include "bobutil.hpp"
#include "sync.hpp"
#include "wg_rand.hpp"
#include "bobiter.hpp"
#include "batdata.hpp"
#include "batctrl.hpp"
#include "batmsg.hpp"
#include "moveutil.hpp"
#include "resstr.hpp"
#ifdef DEBUG
#include "clog.hpp"
static LogFile rcLog("BReaction.log");
#endif


namespace B_Logic {

//-----------------------------------------------------
// ReactionBase class
class ReactionBase {
    BattleGameInterface* d_batgame;
    BattleData*          d_batData;
   public:
    ReactionBase() :
       d_batgame(0),
       d_batData(0) {}

    void init(BattleGameInterface* batgame)
    {
       d_batgame = batgame;
       d_batData = d_batgame->battleData();
    }

    BattleGameInterface* batgame() const { return d_batgame; }
    BattleData* batData() const { return d_batData; }

    virtual bool run(const RefBattleCP& cp) = 0;

   private:
//     void retreat(const RefBattleCP& cp);
};

//-----------------------------------------------------
// Derived Reaction classes
class LeaderInDanger : public ReactionBase {
   public:
      LeaderInDanger() {}

      bool run(const RefBattleCP& cp);
};

bool LeaderInDanger::run(const RefBattleCP& cp)
{
#ifdef DEBUG
   rcLog.printf("Running LeaderInDanger for %s", cp->getName());
#endif

   // react if only within 2 hexes and we are not attached
   if(cp->attached() || cp->targetList().closeRange() > (2 * BattleMeasure::XYardsPerHex))
         return False;

   // for now do a quick retreat
// plotRetreatHex(batData(), cp, 4);
   cp->nearEnemyMode(BattleCP::NE_KeepAway6);
    cp->lockMode();
   return True;
}

// ThreatToFlank
class ThreatToFlank : public ReactionBase {
   public:
    ThreatToFlank() {}

    bool run(const RefBattleCP& cp);
   private:
      enum DoWhat {
            Refuse,
            Retreat,
            Envelope,
            Nothing,
            DoWhat_HowMany
      };
      DoWhat shouldDoWhat(
         const RefBattleCP& cp,
         const RefBattleCP& enemyCP,
         BattleItem::Position pos);
};

ThreatToFlank::DoWhat ThreatToFlank::shouldDoWhat(
      const RefBattleCP& cp,
      const RefBattleCP& enemyCP,
      BattleItem::Position pos)
{
#ifdef DEBUG
   rcLog.printf("Processing DoWhat for %s", cp->getName());
#endif

   const int c_nSupported = 2;
   const int c_maxDieRoll = 10;
   const DoWhat s_doWhat[c_nSupported][c_maxDieRoll][BattleOrderInfo::Posture_HowMany][BattleOrderInfo::Aggress_HowMany] = {
    {
      {
         {  Retreat, Retreat, Retreat, Retreat  },
         {  Retreat, Retreat, Retreat, Retreat  }
        },
      {
         {  Retreat, Retreat, Retreat, Retreat  },
         {  Retreat, Retreat, Retreat, Refuse   }
        },
      {
         {  Retreat, Retreat, Retreat, Refuse   },
         {  Retreat, Retreat, Retreat, Refuse   }
        },
      {
         {  Retreat, Retreat, Retreat, Refuse   },
         {  Retreat, Retreat, Refuse,  Refuse   }
        },
      {
         {  Retreat, Retreat, Refuse,  Refuse   },
         {  Retreat, Refuse,  Refuse,  Refuse   }
        },
      {
         {  Retreat, Refuse,  Refuse,  Refuse   },
         {  Retreat, Refuse,  Refuse,  Refuse   }
        },
      {
         {  Retreat, Refuse,  Refuse,  Nothing   },
         {  Retreat,  Refuse,  Refuse,  Nothing   }
        },
      {
         {  Retreat,  Refuse,  Refuse,  Nothing   },
         {  Retreat,  Refuse,  Refuse,  Envelope   }
        },
      {
         {  Retreat,  Refuse,  Nothing,  Envelope   },
         {  Retreat,  Refuse,  Nothing,  Envelope   }
        },
      {
         {  Retreat, Nothing, Envelope, Envelope  },
         {  Retreat, Nothing, Envelope, Envelope }
        }
    },
    {
      {
         {  Retreat, Retreat, Retreat, Retreat  },
         {  Retreat, Retreat, Retreat, Retreat  }
        },
      {
         {  Retreat, Retreat, Retreat, Nothing  },
         {  Retreat, Retreat, Retreat, Nothing  }
        },
      {
         {  Retreat, Retreat, Nothing, Nothing  },
         {  Retreat, Retreat, Nothing, Nothing  }
        },
      {
         {  Retreat, Nothing, Nothing, Nothing  },
         {  Retreat, Nothing, Nothing, Nothing  }
        },
      {
         {  Retreat, Nothing, Nothing, Nothing  },
         {  Retreat, Nothing, Nothing, Envelope  }
        },
      {
         {  Retreat, Nothing, Nothing, Nothing  },
         {  Retreat, Nothing, Nothing, Envelope  }
        },
      {
         {  Retreat, Nothing, Nothing, Nothing  },
         {  Retreat, Nothing, Nothing, Envelope  }
        },
      {
         {  Retreat, Nothing, Nothing, Nothing  },
         {  Retreat, Nothing, Nothing, Envelope  }
        },
      {
         {  Nothing, Nothing, Nothing, Envelope  },
         {  Nothing, Nothing, Nothing, Envelope  }
        },
      {
         {  Nothing, Nothing, Envelope, Envelope  },
         {  Nothing, Nothing, Envelope, Envelope  }
        }
    },
   };

   bool supported = ((enemyCP->movingBackwards() || enemyCP->routing() || enemyCP->fleeingTheField()) ||
         enemyCP->targetList().closeRangeFront() <= (CRandom::get(5) * BattleMeasure::XYardsPerHex));

   int dieRoll = CRandom::get(3, 8);
#ifdef DEBUG
   rcLog.printf("Friendly Support or Enemy Retreating = %d, raw dieRoll = %d", static_cast<int>(supported), dieRoll);
#endif

    // modify die
   // for leader aggression
    RefBattleCP hq = BobUtility::getAttachedLeader(batData(), cp);
    RefGLeader leader = (hq != NoBattleCP) ? hq->leader() : cp->leader();

    if(leader->getAggression() >= 200)
      dieRoll += 3;
    else if(leader->getAggression() >= 170)
      dieRoll += 2;
    else if(leader->getAggression() >= 140)
      dieRoll += 1;
    else if(leader->getAggression() <= 80)
      dieRoll -= 2;
    else if(leader->getAggression() <= 120)
      dieRoll -= 1;

#ifdef DEBUG
   rcLog.printf("Die roll after leader aggression modifier = %d", dieRoll);
#endif

    // for unit morale
    if(cp->morale() >= 200)
      dieRoll += 2;
    else if(cp->morale() >= 170)
      dieRoll += 1;
    else if(cp->morale() <= 80)
      dieRoll -= 2;
    else if(cp->morale() <= 120)
      dieRoll -= 1;

#ifdef DEBUG
   rcLog.printf("Die roll after morale modifier = %d", dieRoll);
#endif

    dieRoll = clipValue(dieRoll, 0, c_maxDieRoll - 1);
    return s_doWhat[supported][dieRoll][cp->posture()][cp->aggression()];
}

bool ThreatToFlank::run(const RefBattleCP& cp)
{
   // Special cases...
   //-----------------------------
   // if we are already manuerving
   if(cp->liningUp() ||
       cp->deploying() ||
       cp->onManuever() ||
       cp->movingBackwards())
   {
      return False;
   }

#ifdef DEBUG
   rcLog.printf("Running ThreatToFlank for %s", cp->getName());
#endif

   RefBattleCP attachedLeader = BobUtility::getAttachedLeader(batData(), cp);
   RefGLeader leader = (attachedLeader != NoBattleCP) ?
         attachedLeader->leader() : cp->leader();

   // if we do NOT have an enemy on our flank
   RefBattleCP enemyCP = NoBattleCP;
   RefBattleCP enemyCPFront = NoBattleCP;
   UWORD closeRange = UWORD_MAX;
   UWORD closeRangeFront = UWORD_MAX;
   BattleItem::Position pos = BattleItem::Position_HowMany;
   bool enemyToTheFront = False;

   SListIterR<BattleItem> bliter(&cp->targetList());
   while(++bliter)
   {
#ifdef DEBUG
      rcLog.printf("%s is to the %s at %d yards",
         bliter.current()->d_cp->getName(),
            BattleItem::positionText(bliter.current()->d_position),
            static_cast<int>(bliter.current()->d_range));
#endif
      if(bliter.current()->d_position == BattleItem::Front)
      {
         enemyToTheFront = True;
         if(bliter.current()->d_range < closeRangeFront)
         {
            enemyCPFront = bliter.current()->d_cp;
            closeRangeFront = bliter.current()->d_range;
         }
         continue;
      }

      else if(bliter.current()->d_range <= (6 * BattleMeasure::XYardsPerHex) &&
                  bliter.current()->d_range < closeRange)
      {
         closeRange = bliter.current()->d_range;
         enemyCP = bliter.current()->d_cp;
         pos = bliter.current()->d_position;
      }
   }

   if(enemyCP != NoBattleCP)
   {
      if( (pos == BattleItem::RightFlank || pos == BattleItem::LeftFlank) ||
            (pos == BattleItem::RightRear || pos == BattleItem::LeftRear) )
      {
         if(cp->movingBackwards() && cp->nearEnemyMode() >= BattleCP::NE_KeepAway2)
            return False;

#ifdef DEBUG
         rcLog.printf("%s is on %s flank",
            enemyCP->getName(), (pos == BattleItem::LeftFlank) ? "Left" :
                                          (pos == BattleItem::RightFlank) ? "Right" :
                                          (pos == BattleItem::RightRear) ? "RightRear" : "LeftRear");
#endif


         // a random chance of this not happening
            // See if we refuse or envelope
         DoWhat doWhat = shouldDoWhat(cp, enemyCP, pos);
#ifdef DEBUG
         static const char* s_howText[DoWhat_HowMany] = {
                  "Refusing",
                  "Retreating",
                  "Enveloping",
                  "Doing Nothing"
         };
         rcLog.printf("%s is reacting to %s by %s flank",
                  cp->getName(), enemyCP->getName(), s_howText[doWhat]);
#endif

         if(doWhat != Nothing)
         {

            if( (doWhat == Retreat || enemyToTheFront || (cp->formation() < CPF_Deployed && doWhat == Refuse)) )
            {
//             if(CRandom::get(100) > CRandom::get(2, 6))
               {
                  int retreatHowMany = CRandom::get(2, 4);
                        BattleCP::NearEnemyMode nem;
                        if(retreatHowMany == 2)
                           nem = BattleCP::NE_Retreat2;
                        else if(retreatHowMany == 3)
                           nem = BattleCP::NE_Retreat3;
                        else
                           nem = BattleCP::NE_Retreat4;

                        cp->nearEnemyMode(nem);
                        cp->lockMode();
//                plotRetreatHex(batData(), cp, retreatHowMany);
#ifdef DEBUG
                  rcLog.printf("%s is reacting to %s by retreating %d hexes",
                     cp->getName(), enemyCP->getName(), retreatHowMany);
#endif
               }
            }
            // if massed or extended refuse flank
            else if(cp->formation() >= CPF_Deployed)
            {
               cp->clearMove(True);

               // if enemy is 880 yards to the front, refuse flank
               // Note: for now, we will use reverse wheels to refuse
               //    cp->getCurrentOrder().manuever((pos == BattleItem::RightFlank) ? CPM_RWheelR : CPM_RWheelL);
               BattleOrder& order = cp->getCurrentOrder();
                    if(doWhat == Envelope)
                    {
                     order.order().d_manuever = (pos == BattleItem::RightFlank || pos == BattleItem::RightRear) ?
                           CPM_WheelR : CPM_WheelL;
                    }
                    else
                    {
                     order.order().d_manuever = (pos == BattleItem::RightFlank || pos == BattleItem::RightRear) ?
                           CPM_RWheelR : CPM_RWheelL;
                    }
               order.order().d_whatOrder.insert(WhatOrder::Manuever);
#if 0
               order.order().d_faceHow = (doWhat == Envelope) ? CPH_Front : CPH_Back;

               // set facing
               order.order().d_facing = (pos == BattleItem::RightFlank || pos == BattleItem::RightRear) ?
                   rightTurn(cp->facing()) : leftTurn(cp->facing());

               order.order().d_whatOrder.insert(WhatOrder::Facing);
#endif
               // send player message
               {
                  BattleMessageInfo msg(cp->getSide(), BattleMessageID::BMSG_RefusingFlank, cp);
                  msg.target(enemyCP);
                  if((pos == BattleItem::RightFlank) || (pos == BattleItem::RightRear))
                     msg.s1(InGameText::get(IDS_Right));
                  else
                     msg.s1(InGameText::get(IDS_Left));
                  msg.s2(BattleItem::positionText(pos));
                  batgame()->sendPlayerMessage(msg);
               }

               return True;
            }

            // if march or massed
//          else
//          {
//             plotRetreatHex(batData(), cp, CRandom::get(2, 4));
//          }
         }
      }
      else if(pos == BattleItem::Rear)// to the rear
      {
         // do an about face
       //   ASSERT(pos == BattleItem::Rear);
         int chance = 100;
         if(closeRangeFront <= 660)
         {
            int strFront = enemyCPFront->lastStrength();
            int strRear = enemyCP->lastStrength();
            if(strFront >= strRear * 2)
               chance -= 90;
            else if(strFront >= strRear * 1.5)
               chance -= 70;
            else if(strRear >= strFront * 2)
               chance -= 10;
            else if(strRear >= strFront * 1.5)
               chance -= 30;
            else
               chance -= 50;

         }


         if(CRandom::get(100) < chance)
         {
            cp->clearMove(True);
            cp->getCurrentOrder().order().d_facing = oppositeFace(cp->facing());
            cp->getCurrentOrder().order().d_whatOrder.insert(WhatOrder::Facing);
            return True;
         }
      }
   }

   return False;
}

bool comingToUs(const RefBattleCP& cp, const RefBattleCP& eCP)
{
   if(eCP->sp() != NoBattleSP)
   {
      for(std::vector<DeployItem>::iterator di = eCP->mapBegin();
            di != eCP->mapEnd();
            ++di)
      {
         if(di->active())
         {
            SListIterR<HexItem> iter(&di->d_sp->routeList());
            while(++iter)
            {
               if(distanceFromUnit(iter.current()->d_hex, cp) <= 1)
            return True;
            }
         }
      }
   }

   return False;
}

//----------------------------------------------------
// CavNearNotSquared
// Enemy cav is nearby and we are not squared or closed
class CavNearNotSquared : public ReactionBase {
   public:
    CavNearNotSquared() {}

    bool run(const RefBattleCP& cp);
};

bool CavNearNotSquared::run(const RefBattleCP& cp)
{
   // only infantry can test
   // inf already in squared / column formation do not test
   if( (cp->movingBackwards() && cp->nearEnemyMode() >= BattleCP::NE_KeepAway2) ||
       (cp->sp() == NoBattleSP) ||
       (!cp->generic()->isInfantry()) ||
       (cp->sp()->formation() == SP_ChangingFormation) )// ||
       //cp->destSPFormation() == SP_ClosedColumnFormation ||
       //cp->destSPFormation() == SP_SquareFormation)
   {
      return False;
   }

#ifdef DEBUG
   rcLog.printf("%s is testing for Cav Near Not Squared", cp->getName());
#endif
   int nInBuiltUp = 0;
   for(int c = 0; c < cp->columns(); c++)
   {
      DeployItem* di = cp->currentDeployItem(c, 0);
      ASSERT(di);
      if(di && di->active())
      {
         const BattleTerrainHex& hexInfo = batData()->getTerrain(di->d_sp->hex());
         if(hexInfo.d_terrainType == GT_Town ||
            hexInfo.d_terrainType == GT_Village ||
            hexInfo.d_terrainType == GT_Farm)
         {
            nInBuiltUp++;
         }
      }
   }

   // if we have 1/2 or more of front-line SP in buitlup terrain
   // We will almost never form square
   if(nInBuiltUp * 2 >= cp->columns())
   {
      if(CRandom::get(100) < CRandom::get(98, 100))
         return False;
   }

   // see if cav that is not routed, shaken, etc.
   RefBattleCP enemyCP = NoBattleCP;
// int closeRange = Distance_MAX;
   bool cavThreat = False;
   SListIter<BattleItem> bliter(&cp->targetList());
   while(++bliter)
   {
      RefBattleCP& eCP = bliter.current()->d_cp;

      if(eCP->generic()->isCavalry() &&
          eCP->moving() &&
          !eCP->routing() &&
          !eCP->shaken() &&
          !eCP->movingBackwards() &&
          bliter.current()->d_range <= (4 * BattleMeasure::XYardsPerHex))
      {
         enemyCP = eCP;
         if(comingToUs(cp, eCP))
         {
            cavThreat = True;
            break;
         }
      }
   }

   // See if we should change back
   if(cp->destSPFormation() == SP_ClosedColumnFormation ||
      cp->destSPFormation() == SP_SquareFormation)
   {
      int chance = (enemyCP == NoBattleCP) ? 100 :
                   (!cavThreat) ? 97 : 0;

      if(CRandom::get(100) < chance)
      {
#ifdef DEBUG
         rcLog.printf("%s is changing back from squared. Enemy no longer a threat", cp->getName());
#endif
         cp->getCurrentOrder().order().d_spFormation = (cp->posture() == BattleOrderInfo::Offensive || cp->moving()) ?
            SP_ColumnFormation : SP_ColumnFormation; //mtc122 : SP_LineFormation;
         cp->getCurrentOrder().order().d_whatOrder.insert(WhatOrder::SPFormation);
         return True;
      }

      return False;
   }

   if(enemyCP != NoBattleCP)
   {
    // a random chance of this not happening
    // Unchecked
    int chance = (cavThreat) ? CRandom::get(5, 10) : CRandom::get(98, 100);
    // End

    // if cav is not threatening us
    if(CRandom::get(100) > chance)
    {
#ifdef DEBUG
      rcLog.printf("%s is reacting to enemy cav", cp->getName());
#endif
      cp->clearMove(True);

      // Determine formation
      // If we have enemy inf nearyby
      bool hasNearbyInf = False;
      SListIter<BattleItem> bliter(&cp->targetList());
      while(++bliter)
      {
         CRefBattleCP tcp = bliter.current()->d_cp;
         if( (tcp->generic()->isInfantry()) &&
            !(tcp->movingBackwards() || tcp->routing() || tcp->fleeingTheField()) )
         {
            hasNearbyInf = (  (bliter.current()->d_range <= (3 * BattleMeasure::XYardsPerHex)) ||
                              (tcp->getCurrentOrder().wayPoints().entries() > 0 && bliter.current()->d_range <= (6 * BattleMeasure::XYardsPerHex)) );
            break;
         }
      }

      // for now I'll pick random
      SPFormation spf;
      if(hasNearbyInf)
      {
         //Unchecked
         spf = (CRandom::get(100) < 60) ? SP_ClosedColumnFormation : SP_ColumnFormation;
         // end
      }
      else
      {
         spf = (CRandom::get(100) < 50) ? SP_ClosedColumnFormation : SP_SquareFormation;
      }

      cp->getCurrentOrder().order().d_spFormation = spf;
      cp->getCurrentOrder().order().d_whatOrder.insert(WhatOrder::SPFormation);

      // send player message
      {
         BattleMessageInfo msg(cp->getSide(), BattleMessageID::BMSG_ChangingSPFormation, cp);
         msg.target(enemyCP);
         msg.s1(BattleOrderInfo::spFormationName(spf));
         batgame()->sendPlayerMessage(msg);
      }

      return True;
    }
   }

   return False;
}

//----------------------------------------------------
// Changing formation
class ChangingFormation : public ReactionBase {
   public:
      ChangingFormation() {}

      bool run(const RefBattleCP& cp);

    private:
         bool canChange(const RefBattleCP& cp);
};

bool ChangingFormation::run(const RefBattleCP& cp)
{
    // do not react if this is artillery, or if we are already changing formation
   if(cp->sp() == NoBattleSP ||
      cp->generic()->isArtillery() ||
      cp->sp()->formation() == SP_ChangingFormation ||
      (cp->movingBackwards() && cp->nearEnemyMode() >= BattleCP::NE_KeepAway2) )
   {
      return False;
   }

   // enemy must be within 880 yards to react
   SPFormation cf = cp->destSPFormation();
   if( (cp->targetList().closeRange() > (4 * BattleMeasure::XYardsPerHex)) ||
      (cf == SP_LineFormation) )
   {
    return False;
   }

#ifdef DEBUG
   rcLog.printf("%s is testing for Changing Formation", cp->getName());
#endif

   // see if enemy cav is near
   bool cavNear = False;
   SListIterR<BattleItem> iter(&cp->targetList());
   while(++iter)
   {
      if(iter.current()->d_range <= (6 * BattleMeasure::XYardsPerHex) &&
         iter.current()->d_cp->generic()->isCavalry())
      {
         cavNear = True;
         break;
      }
   }

   // if we are in march formation, automatically change
   SPFormation sf = cf;
   if(cf == SP_MarchFormation)
      sf = (cp->posture() == BattleOrderInfo::Offensive) ? SP_ColumnFormation : SP_LineFormation;

   // if squared or closed and if no cav around
   else if(cf == SP_SquareFormation || cf == SP_ClosedColumnFormation)
   {
      ASSERT(cp->generic()->isInfantry());
      if(!cavNear)
      {
         sf = (cp->posture() == BattleOrderInfo::Offensive) ?
            SP_ColumnFormation : SP_LineFormation;
      }
   }

   //
   else if(cf == SP_ColumnFormation)
   {
      if(cp->inCombat() && !cp->inCloseCombat())
      {
         if(!cavNear)
         {
            if(cp->posture() == BattleOrderInfo::Defensive)
               sf = SP_LineFormation;
            else
            {
               static const int chance[BattleOrderInfo::Aggress_HowMany] = {
                  100, 67, 33, 0
               };

               if(CRandom::get(100) < chance[cp->aggression()])
               {
                  sf = SP_LineFormation;
               }
            }
         }
      }
   }

   if(sf != cf)
   {
      // a random chance of this not happening
      int chance = CRandom::get(10);
      if(CRandom::get(100) > chance)
      {
#ifdef DEBUG
         ASSERT(sf < SP_Formation_HowMany);
         rcLog.printf("%s is changing SP formation to %s",
             cp->getName(), BattleOrderInfo::spFormationName(sf));
#endif

         cp->clearMove(True);
         cp->getCurrentOrder().order().d_spFormation = sf;
         cp->getCurrentOrder().order().d_whatOrder.insert(WhatOrder::SPFormation);

         // send player message
         BattleMessageInfo msg(cp->getSide(), BattleMessageID::BMSG_ChangingSPFormation, cp);
         msg.target(cp->targetList().first()->d_cp);
         msg.s1(BattleOrderInfo::spFormationName(sf));
         batgame()->sendPlayerMessage(msg);
      }
   }

   return (sf != cf);
}

//----------------------------------------------------
// Changing formation
class ChangingDeployment : public ReactionBase {
   public:
    ChangingDeployment() {}

    bool run(const RefBattleCP& cp);
};

bool ChangingDeployment::run(const RefBattleCP& cp)
{
   if( (cp->movingBackwards() && cp->nearEnemyMode() >= BattleCP::NE_KeepAway2) ||
       (cp->targetList().closeRange() > (6 * BattleMeasure::XYardsPerHex)) )
   {
      return False;
   }
#ifdef DEBUG
   rcLog.printf("%s is testing for Changing Deployment", cp->getName());
#endif

   CPFormation f = cp->nextFormation();

   // pretty much automatic if in march
   if(cp->nextFormation() == CPF_March)
    f = CPF_Deployed;

   // if massed
   else if(cp->nextFormation() == CPF_Massed)
   {
    if(cp->generic()->isInfantry())
    {
      if(cp->posture() == BattleOrderInfo::Defensive)
         f = CPF_Deployed;
      else
      {
         static const int chance[BattleOrderInfo::Aggress_HowMany] = {
          100, 50, 0, 0
         };

         if(CRandom::get(100) < chance[cp->aggression()])
          f = CPF_Deployed;
      }
    }

    else if(cp->generic()->isCavalry())
    {
      if(cp->posture() == BattleOrderInfo::Defensive)
      {
         // if under artillery fire, automatic
         if(cp->takingGunHits())
          f = CPF_Deployed;
         else
         {
          // go to deployed if aggression is 0 or 1
          if(cp->aggression() <= 1)
            f = CPF_Deployed;
         }
      }
      else
      {
         // test to go to deployed if under artillery fire
         if(cp->takingGunHits())
         {
          static const int chance[BattleOrderInfo::Aggress_HowMany] = {
            100, 50, 0, 0
          };

          if(CRandom::get(100) < chance[cp->aggression()])
            f = CPF_Deployed;
         }
      }
    }

    else if(cp->generic()->isArtillery())
      f = CPF_Deployed;
   }

   else if(cp->nextFormation() == CPF_Deployed)
   {
    if(cp->generic()->isArtillery())
      f = CPF_Extended;
   }

   if(f != cp->nextFormation())
   {
    // a random chance of this not happening
    int chance = CRandom::get(10);
    if(CRandom::get(100) > chance)
    {
#ifdef DEBUG
      rcLog.printf("%s is changing formation", cp->getName());
#endif
      cp->clearMove(True);

      cp->getCurrentOrder().order().d_divFormation = f;
      cp->getCurrentOrder().order().d_whatOrder.insert(WhatOrder::CPFormation);
    }
   }

   return False;
}

//----------------------------------------------------
// enveloping formation
class EnvelopingEnemy : public ReactionBase {
   public:
    EnvelopingEnemy() {}

    bool run(const RefBattleCP& cp);
};

bool EnvelopingEnemy::run(const RefBattleCP& cp)
{
   return True;
}

// -----------------------------------------------------
// Static instances
const int nTypes = 5;
const int nGroups = 5;

static LeaderInDanger     s_leaderInDanger;
static ThreatToFlank      s_threatToFlank;
static CavNearNotSquared  s_cavNear;
static ChangingFormation  s_formation;
static ChangingDeployment s_deployment;
static EnvelopingEnemy    s_envelopEnemy;

static ReactionBase* s_reactions[nGroups][nTypes] = {
   {
    &s_threatToFlank,
    &s_cavNear,
    &s_formation,
    &s_deployment,
    &s_envelopEnemy
  },
  {
    &s_cavNear,
    &s_threatToFlank,
    &s_formation,
    &s_deployment,
    &s_envelopEnemy
  },
  {
    &s_formation,
    &s_deployment,
    &s_threatToFlank,
    &s_cavNear,
    &s_envelopEnemy
  },
   {
    &s_cavNear,
      &s_formation,
    &s_deployment,
    &s_threatToFlank,
    &s_envelopEnemy
  },
   {
    &s_envelopEnemy,
    &s_deployment,
    &s_formation,
    &s_cavNear,
    &s_threatToFlank
   }

};

//------------------------------------------------------
// Reaction Manager
class ReactionManager {
    BattleGameInterface* d_batgame;
    PBattleData d_batData;
   public:
    ReactionManager(BattleGameInterface* batgame) :
       d_batgame(batgame),
       d_batData(batgame->battleData())
    {
       // init static reaction classes
       for(int t = 0; t < nTypes; t++)
       {
          s_reactions[0][t]->init(batgame);
          }

       s_leaderInDanger.init(batgame);
    }

    void process();
   private:
    void processUnit(const RefBattleCP& cp);
};

void ReactionManager::process()
{
    BattleData::WriteLock lock(d_batData);

   // process
   for(Side side = 0; side < d_batData->ob()->sideCount(); ++side)
   {
//      RefBattleCP cp = d_batData->ob()->getTop(side);
      for(BattleUnitIter iter(d_batData->ob(), side); !iter.isFinished(); iter.next())
      {
#if 0 //def DEBUG
             rcLog.printf("Testing %s for reaction", iter.cp()->getName());
             rcLog.printf("routing = %d, fleeing = %d, changed = %d, range = %d",
                static_cast<int>(iter.cp()->routing()),
                static_cast<int>(iter.cp()->fleeingTheField()),
                static_cast<int>(iter.cp()->targetList().changed()),
                static_cast<int>(iter.cp()->targetList().closeRange()));
#endif
             if( //(iter.cp()->getRank().sameRank(Rank_Division)) &&
//                    (iter.cp()->allSPAtCenter()) &&
                  (!iter.cp()->routing()) &&
                  (!iter.cp()->fleeingTheField()) &&
                  (iter.cp()->targetList().changed()) &&
                  (iter.cp()->targetList().closeRange() <= (4 * BattleMeasure::XYardsPerHex)) )
         {
            processUnit(iter.cp());
         }
      }
   }

   // process shock combat
}

void ReactionManager::processUnit(const RefBattleCP& cp)
{
    cp->targetList().clearChanged();

    // if retreating from enemy, don't react
    //if(cp->movingBackwards() && cp->nearEnemyMode() >= BattleCP::NE_KeepAway2)
    //    return;

#ifdef DEBUG
    rcLog.printf("--------- Procession Reaction for %s", cp->getName());
    BattleTime btime = d_batData->getDateAndTime();
    Greenius_System::Time time = btime.time();
    Greenius_System::Date date = btime.date();

    rcLog << ", time=" << (int) time.hours() << ":" << (int) time.minutes() << ":" << (int) time.seconds();
    rcLog << ", Date=" << date.day() << "/" << date.month() << "/" << date.year() << endl;
    rcLog.printf("Enemy is %d yards away", cp->targetList().closeRange());
#endif

    // A HQ will always react to enemy
    if(cp->getRank().isHigher(Rank_Division))
    {
         s_leaderInDanger.run(cp);
         return;
    }

    RefBattleCP attachedLeader = BobUtility::getAttachedLeader(d_batData, cp);
    RefGLeader leader = (attachedLeader != NoBattleCP) ?
             attachedLeader->leader() : cp->leader();

    // raw chance for a reaction is leaders tact ability as % of 255
    int chance = ((leader->getTactical() * 100) / Attribute_Range) + CRandom::get(15, 30);
#ifdef DEBUG
    rcLog.printf("Raw chance for reaction = %d%%", chance);
#endif

    // modify ---------------------------
    // X 1.2 unit response if 180+
    if(cp->targetList().closeRangeRear() <= 440 ||
       cp->targetList().closeRangeLeftRear() <= 440 ||
       cp->targetList().closeRangeRightRear() <= 440)
    {
      chance *= 3;
    }
    else if(cp->targetList().closeRangeLeftFlank() <= 440 ||
            cp->targetList().closeRangeRightFlank() <= 440)
    {
      chance *= 2;
    }
#ifdef DEBUG
    rcLog.printf("Chance after enemy on flank modifier = %d%%", chance);
#endif

    Attribute response = BobUtility::unitResponse(d_batData->ob(), cp);
    if(response >= 150)
      chance *= 1.3;
    else if(response >= 125)
      chance *= 1.2;
#ifdef DEBUG
    rcLog.printf("Chance after unit response modifier = %d%%", chance);
#endif

    // X 1.2 Arty with Arty specialist
    if( (cp->generic()->isArtillery()) &&
         (cp->leader()->isSpecialistType(Specialist::Artillery) || leader->isSpecialistType(Specialist::Artillery)) )
    {
      chance *= 1.2;
#ifdef DEBUG
      rcLog.printf("Chance after arty specialist modifier = %d%%", chance);
#endif
    }

    // X 1.2 Arty with Arty specialist
    if( (cp->generic()->isCavalry()) &&
         (cp->leader()->isSpecialistType(Specialist::Cavalry) || leader->isSpecialistType(Specialist::Cavalry)) )
    {
      chance *= 1.2;
#ifdef DEBUG
      rcLog.printf("Chance after Cav specialist modifier = %d%%", chance);
#endif
    }

    //within CR of a senior leader
    RefBattleCP nearByHQ = BobUtility::leaderInRadius(d_batData->ob(), cp);
    if(nearByHQ != NoBattleCP)
    {
      // X 1.2 if tact ability of 180+
      if(nearByHQ->leader()->getTactical() >= 180)
         chance *= 1.2;

      // X 1.1 if tact ability of 145+
      else if(nearByHQ->leader()->getTactical() >=145)
         chance *= 1.1;
#ifdef DEBUG
      rcLog.printf("Chance after nearby HQ modifier = %d%%", chance);
#endif
    }

    // X .8 if unit has 3 aggression
    if(cp->aggression() == 3)
      chance *= .8;
#ifdef DEBUG
    rcLog.printf("Chance after aggression modifier = %d%%", chance);
#endif

    // X .75 if disorder 1
    if(cp->disorder() == 1)
      chance *= .75;
    else if(cp->disorder() == 0)
      chance *= 5;
#ifdef DEBUG
    rcLog.printf("Chance after disorder modifier = %d%%", chance);
#endif

    // X .5 if unit is shaken
    if(cp->shaken())
      chance *= .5;
#ifdef DEBUG
    rcLog.printf("Chance after shaken modifier = %d%%", chance);
#endif


#ifdef DEBUG
    rcLog.printf("Final chance = %d%%", chance);
#endif

    if(CRandom::get(100) <= chance)
    {
      int group = (CRandom::get(100) <= 90) ? 0 : CRandom::get(nGroups);
      ASSERT(group < nGroups);

      for(int i = 0; i < nTypes; i++)
      {
         if(s_reactions[group][i]->run(cp))
             break;
      }
    }
}

//------------------------------------------------------
// Client access

//static ReactionManager s_rm;

B_Reaction::B_Reaction(BattleGameInterface* batgame) :
    d_rm(new ReactionManager(batgame))
{
    ASSERT(d_rm);
}

B_Reaction::~B_Reaction()
{
   if(d_rm)
      delete d_rm;
}

void B_Reaction::process()
{
   if(d_rm)
      d_rm->process();
}

}; // end namespace

/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.2.2 Square now reforms into column rather than line  mtc122
 *
 * Revision 1.2  2001/06/18 08:25:44  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
