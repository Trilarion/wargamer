/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 * Move/Process all battlefield units
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "bunits.hpp"
#include "blog.hpp"
#include "bobiter.hpp"
#include "wg_rand.hpp"
#include "b_send.hpp"
//#include "bdeploy.hpp"

using namespace BOB_Definitions;

#if !defined(NOLOG)
using B_Logic::logfile;
#endif

using namespace BattleMeasure;

namespace {    // Static internal functions

/*
 * Process a Units orders
 *
 * Move orders from order queue into acting and into current
 */

void procOrders(RPBattleData batData, RefBattleCP cp)
{
   BattleOrder order;

   if(cp->getNewOrder(batData->getTick(), &order))
   {
      // Calculate how long to act...
      // depends on realism, leader attributes, etc...

      // BattleTime::Tick when = batData->getTick() + minutes(5);
      BattleTime::Tick when = batData->getTick() + seconds(10);

      cp->setActingOrder(when, order);
   }

   if(cp->getActingOrder(batData->getTick(), &order))
   {
      // Copy to current order
      // Do any nw processing

      cp->setCurrentOrder(order);
      // if this is a XXX or higher, set aggression, posture for whole command
      if( (cp->getRank().isHigher(Rank_Division)) &&
            (cp->aggression() != order.aggression() || cp->posture() != order.posture()) )
      {
         for(BattleUnitIter iter(cp); !iter.isFinished(); iter.next())
         {
            if(iter.cp() != cp)
            {
               BattleOrder cOrder = cp->getCurrentOrder();
               cOrder.posture(order.posture());
               cOrder.aggression(order.aggression());

               sendBattleOrder(iter.cp(), &cOrder);
            }
         }
      }

      cp->aggression(order.aggression());
      cp->posture(order.posture());
   }
}

/*
 * Process a CP's movement
 */

void moveUnit(RPBattleData batData, RefBattleCP cp, BattleTime::Tick ticks)
{
}

/*
 * Process a SP's movement
 *
 * All we do so far is get the desired formation
 */

void moveSP(RPBattleData batData, RefBattleCP cp, RefBattleSP sp, BattleTime::Tick ticks)
{
   const BattleOrder& cpOrder = cp->getCurrentOrder();

   /*
    * Make sure destFormation is correct
    */

   if(cpOrder.spFormation() != sp->destFormation())
      sp->changeFormation(cpOrder.spFormation());

   /*
    * Update the tween
    */

   // Convert ticks into a Tween value

   // Look up destFormation and fromFormation in table to see how long to change

   SPFormation destFormation = sp->destFormation();
   SPFormation srcFormation = sp->fromFormation();

   if(srcFormation != destFormation)
   {
      BattleTime::Tick changeTicks = seconds(5);   // this should come from a table

      BattleSP::FormationTween::Value tween = static_cast<BattleSP::FormationTween::Value>((ticks * BattleSP::FormationTween::MaxValue + changeTicks - 1) / changeTicks);

      sp->addTween(tween);
   }

   /*
    * Move SP to desired location
    *
    * Get speed based on unit type and terrain
    * Get Location to go to... or moving across same hex.. keep in div formation.
    */



}




}; // un-named namespace

/*------------------------------------------------------
 * Global Move All Units
 */


void B_Logic::processOrders(RPBattleData batData)
{
#if !defined(NOLOG)
   logfile << "Process Orders: " << endl;
   logfile.close();
#endif
   // Iterate all units from top to bottom
   for(BattleUnitIter iter(batData->ob()); !iter.isFinished(); iter.next())
   {
      procOrders(batData, iter.cp());
   }
}

#if 0
void B_Logic::moveUnits(RPBattleData batData, DeployUtility* depUtil, BattleTime::Tick ticks)
{
#if !defined(NOLOG)
   logfile << "move Units: " << ticks << endl;
   logfile.close();
#endif

   B_Logic::procDeploy(batData, depUtil);

#if 0
   // Iterate all units from top to bottom
   for(BattleUnitIter iter(batData->ob()); !iter.isFinished(); iter.next())
   {
      processOrders(batData, iter.cp());

      moveUnit(batData, iter.cp(), ticks);

      for(BattleSPIter spIter = iter.cp(); !spIter.isFinished(); spIter.next())
      {
         moveSP(batData, iter.cp(), spIter.sp(), ticks);

         RefBattleSP sp = spIter.sp();
         sp->setFacing(HexPosition::Facing(sp->facing() + ticks));

         if(CRandom::get(0,100) == 0)
         {
            sp->setFormation(SPFormation(CRandom::get(0, SP_Formation_HowMany)));
         }
      }
   }
#endif

}
#endif

/*
 *----------------------------------------------------------------------
 * $Log$
 *----------------------------------------------------------------------
 */
