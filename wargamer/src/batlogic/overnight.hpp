/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef OVERNIGHT_HPP
#define OVERNIGHT_HPP

#ifndef __cplusplus
#error overnight.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 * Overnight Recovery
 *
 *----------------------------------------------------------------------
 */

#include "bl_dll.h"

class BattleData;

namespace B_Logic
{

BATLOGIC_DLL void overnightRecovery(BattleData* batData);


};

#endif /* OVERNIGHT_HPP */

