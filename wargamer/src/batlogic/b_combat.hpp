/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
#ifndef B_COMBAT_HPP
#define B_COMBAT_HPP

//#include "batdata.hpp"

class BattleGameInterface;

namespace B_Logic
{

class B_CombatImp;

class B_CombatInt {
      B_CombatImp* d_ci;
   public:
      B_CombatInt(BattleGameInterface* batgame);
      ~B_CombatInt();

      void process();
      void newDay();
};

};

#endif

/*
 *----------------------------------------------------------------------
 * $Log$
 *----------------------------------------------------------------------
 */
