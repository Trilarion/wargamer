/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 * Leaders (Generals)
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "leader.hpp"
#include "todolog.hpp"
#include "scenario.hpp"
#include "wg_rand.hpp"
#include "random.hpp"
#include "wldfile.hpp"
#include "filebase.hpp"    // FileReader and FileWriter
#include "resstr.hpp"
#include "resdef.h"
#include "savegame.hpp"

#ifdef DEBUG
#include "cplog.hpp"
#endif

#ifdef DEBUG
int GLeader::s_nextID = 0;
#endif

GLeader::GLeader() :
   d_nameID(StringFileTable::Null),
   d_side(SIDE_Neutral),
   d_nation(NATION_Neutral),
   d_rankLevel(Rank_Division),
   d_command(NoGenericCP),
   d_statusFlags(0),
   d_ranksAboveCeiling(0),
   d_helpID(NoHelpID),
   d_portrait(),
   d_specialist(Specialist::NonSpecialist)
// d_commandingWhatType(Specialist::NonSpecialist)
{
   for(AttributeTypes type = AttributeTypes_First; type < AttributeTypes_HowMany; INCREMENT(type))
   {
      d_attributes[type] = 0;
   }

   // NOTE: Temporary to test lucky leaders
   isLucky((CRandom::get(100) <= 5));

#ifdef DEBUG
   d_id = s_nextID++;
#endif

}

// this function is obsolete
int GLeader::getRating()
{
  return 0;
}

/*
 * File interface
 */

inline bool operator >> (FileReader& f, Specialist::Type& t)
{
   UBYTE b;
   f >> b;

   // bodge
   if(b >= Specialist::HowMany)
     b = Specialist::HowMany - 1;

   t = static_cast<Specialist::Type>(b);

   return true;
}

inline bool operator << (FileWriter& f, const Specialist::Type& t)
{
   return f.putUByte(t);
}

const UWORD GLeader::fileVersion = 0x0005;

Boolean GLeader::readData(FileReader& f)
{
   if(f.isAscii())
   {
#ifdef DEBUG
      todoLog.printf("TODO: GLeader::readData().ascii unwritten\n");
#endif
      return False;
   }
   else
   {
      UWORD version;
      f >> version;
      ASSERT(version <= fileVersion);

      if (version < 0x0005)
      {
         NamedItem name;
         f >> name;
         d_nameID = OB_Names::setLeaderName(name.getName());
         // d_name.readData(f);
      }
      else
      {
         UBYTE isNew;
         f >> isNew;
         if (isNew)
         {
            CString name(f.getString());
            d_nameID = OB_Names::setLeaderName(name);
         }
         else
            f >> d_nameID;
      }

      ASSERT(OB_Names::isValidLeaderName(d_nameID));

      f >> d_side;
      f >> d_nation;

      for(AttributeTypes type = AttributeTypes_First; type < AttributeTypes_HowMany; INCREMENT(type))
      {
        f >> d_attributes[type];
      }

      d_rankLevel.readData(f);

      f >> d_helpID;
      if(version == 0x0000)
      {
#if 0
        UBYTE b;
        f >> b;
        setFlag(SupremeLeader, static_cast<Boolean>(b));
#else
        bool b;
        f >> b;
        setFlag(SupremeLeader, (b));
#endif
      }
      f >> d_specialist;

      if(version <= 0x0002)
      {
        Specialist::Type t;
        f >> t;
      }

      if(version <= 0x0001)
      {
        if(f.getMode() == SaveGame::SaveGame)
        {
          if(version == 0x0000)
          {
            UBYTE b;
            f >> b;
            d_statusFlags = 0;
          }
          else
            f >> d_statusFlags;
        }
      }
      else
      {
        f >> d_statusFlags;
      }

      if(version >= 0x0003)
        f >> d_ranksAboveCeiling;

      if(version >= 0x0004)
         f >> d_portrait;
   }

   return f.isOK();
}

Boolean GLeader::writeData(FileWriter& f) const
{
   if(f.isAscii())
   {
      f.printf(startItemString);
      // NamedItem::writeData(f);
      // d_name.writeData(f);
      f << OB_Names::getLeaderName(d_nameID);
      d_rankLevel.writeData(f);
      // f.printf(fmt_sdn, commandToken, static_cast<int>(d_command));
      f.printf(endItemString);
   }
   else
   {
      f.putUWord(fileVersion);
      // d_name.writeData(f);
      //f << d_nameID;
      if (OB_Names::isNewLeaderName(d_nameID))
      {
         UBYTE isNew = true;
         f << isNew;
         f.putString(OB_Names::getLeaderName(d_nameID));
      }
      else
      {
         UBYTE isNew = false;
         f << isNew;
         f << d_nameID;
      }

      f << d_side;
      f << d_nation;

      for(AttributeTypes type = AttributeTypes_First; type < AttributeTypes_HowMany; INCREMENT(type))
      {
        f << d_attributes[type];
      }

      d_rankLevel.writeData(f);

      f << d_helpID;
      f << d_specialist;
//    f << d_commandingWhatType;
      f << d_statusFlags;
      f << d_ranksAboveCeiling;
      f << d_portrait;
   }

   return f.isOK();
}

/*
 * Clear out temporary status flags
 * (these are flags that are used by the battle routines only)
 */

void GLeader::clearBattleStatusFlags()
{
  isLightlyWounded(False);
  isOutOfIt(False);
  isHorseKilled(False);
  isHitDuringBattle(False);
}

/*
 * Adjust given attribute by + and/or - 30%
 */

static Attribute adjustAttribute(Attribute value, Boolean minus, Boolean plus)
{
  int percent = MulDiv(value, 3, 10);

  int lowValue = minus ? value-percent : value;
   int highValue = plus ? value+percent : value;

   return static_cast<Attribute>(CRandom::get(lowValue, highValue));
}

/*
 * Set random default Leader attributes
 */

void GLeader::setDefault(int dieRollModifier, int nCreated)
{
   ASSERT(getNation() != NATION_Neutral);
   ASSERT(getNation() < scenario->getNumNations());
   ASSERT(getSide() != SIDE_Neutral);
   ASSERT(getSide() < scenario->getNumSides());

   /*
   * Get base attribute value
   */

   const Table2D<UBYTE>& table = scenario->getLeaderBaseValueTable();

   UBYTE nationValue = (scenario->getNationType(getNation()) == MajorNation) ? getNation() : 5;

   const int nValues = 10;
   UBYTE dieRoll = CRandom::get(nValues);

   /*
   * apply modifier
   */

   dieRoll = clipValue(dieRoll-dieRollModifier, 0, nValues);
   ASSERT(dieRoll < nValues);

   Attribute baseValue = table.getValue(dieRoll, nationValue);

   /*
   * Each attribute will be within +/- 30% of base value.
   * This is done with each attribute
   */

   // TODO: make this a table or something
   enum {     // for exceptions, until a table is set up
    Prussian = 1,
    Austrian = 2,
    Russian = 4
   };

   d_attributes[Initiative] = adjustAttribute(baseValue, True, True);
   d_attributes[Staff] = adjustAttribute(baseValue, True, (getNation() != Russian));
   d_attributes[Aggression] = adjustAttribute(baseValue, (getNation() != Prussian), (getNation() != Austrian));
   d_attributes[Charisma] = adjustAttribute(baseValue, True, (getNation() != Austrian));
   d_attributes[TacticalAbility] = adjustAttribute(baseValue, True, True);
   d_attributes[CommandRadius] = adjustAttribute(baseValue, True, True);
   d_attributes[Health] = 130; // always 130
   d_attributes[Subordination] = 25 + (5 * (nCreated / 10)); // 20. Incs 5 every 10 created
   d_rankLevel.setRank(Rank_Division);   // always Division
   d_specialist = Specialist::NonSpecialist;
//  d_commandingWhatType = Specialist::NonSpecialist;

   // has a 5% chance of being lucky
   isLucky((CRandom::get(100) <= 5));
}

static const int s_stringIDs[Specialist::HowMany] = {
   IDS_SPECIALIST_CAVALRY,
   IDS_SPECIALIST_ARTILLERY,
   IDS_SPECIALIST_NOTSPECIAL
};

static ResourceStrings s_strings(s_stringIDs, Specialist::HowMany);

const char* Specialist::typeName(Specialist::Type type)
{
   ASSERT(type < Specialist::HowMany);

   return s_strings.get(type);
}

/*
 * Modify attributes
 */

Attribute GLeader::modifyAttribute(AttributeTypes type, bool actual) const
{
   ASSERT(type != Health);
   ASSERT(type != Subordination);

   Attribute value = d_attributes[type];

   if(!actual)
   {
      /*
       * Modify for temporarily out of battle, death, or capture ( return 0 )
       */

      if(isDead() ||
         isCaptured() ||
         isOutOfIt())
      {
        return 0;
      }

      /*
       * modify for specialist
       * Note: For the attributes Initiative, Staff, Charisma, and Tactical Ability
       *       the following applies:
       *
       * 1. If leader is a specialist and is commanding his specialist type then
       *    these attributes are increased by 30%
       * 2. If leader is a specialist and is not commanding his specialist type then
       *    these attributes are halved
       * 3. If leader is not a specialist but is commanding specialist then
       *    these attributes are halved
       *
       */

      if( (type == Initiative) ||
          (type == Staff) ||
          (type == Charisma) ||
          (type == TacticalAbility) )
      {
         if(isCommandingType())
         {
            if(isSpecialist())
            {
               // Visual C++ can't work out templates, so cast them.
               value = clipValue(
                  static_cast<Attribute>(value * 1.3), 
                  static_cast<Attribute>(0), 
                  Attribute_Range);

               // value = static_cast<Attribute>(clipValue(value * 1.3, 0, Attribute_Range));
            }
         }
         else
         {
           value /= 2;
         }
      }

      /*
       * Modify for rank
       * If commanding a unit above our ability
       * these values are modified by X .8 for each level above
       */

      if( (type == Initiative) ||
          (type == Staff) ||
          (type == TacticalAbility) )
      {
        int d = d_ranksAboveCeiling;

        while(d--)
        {
          value *= .8;
        }
      }

      /*
       * Modify for illness, wounds ( value X 50% )
       */

      // Note: we can be both sick and wounded at the same time, so check seperately
      if(isSick())
        value /= 2;
      if(isSeriouslyWounded())
        value /= 2;
   }

   return value;
}


#ifdef LOG_LEADER_REF
int GLeader::addRef() const
{
   int value = RefBaseCount::addRef();
   cpLog.printf("Add Leader %3d = %d, %s", d_id, value, (const char*)getName());
   return value;
}

int GLeader::delRef() const
{
   int value = RefBaseCount::delRef();
   cpLog.printf("Del Leader %3d = %d, %s", d_id, value, (const char*)getName());
   return value;
}
#endif

const char* GLeader::getName() const
{
   return OB_Names::getLeaderName(d_nameID);
}

void GLeader::setName(char* s)
{
   d_nameID = OB_Names::updateLeaderName(d_nameID, s);
}

/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.2  2001/06/18 08:25:45  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
