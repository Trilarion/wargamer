/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *	Strength Point List
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "splist.hpp"
#include "cplog.hpp"
#include "filecnk.hpp"
#include "wldfile.hpp"
#include "cp.hpp"
#include "leader.hpp"

#ifdef DEBUG
#include "todolog.hpp"
#endif	//DEBUG

const char* StrengthPointList::getChunkName() const
{
	return spChunkName;
}

#ifdef DEBUG
void StrengthPointList::logDelRef(StrengthPointList::Index i)
{
	ConstISP sp = operator[](i);
	if(sp->refCount() == 1)
	{
		cpLog.printf("SP %d autodeleted",
			static_cast<int>(i));
	}
}
#endif



static const UWORD StrengthPointListVersion	= 0x0000;

Boolean StrengthPointList::readData(FileReader& f, BaseOrderBattle& ob)
{
	FileChunkReader fr(f, getChunkName());
	if(!fr.isOK())
		return False;

	if(f.isAscii())
	{
#ifdef DEBUG
		todoLog.printf("TODO: PoolArrayFileObject<T>::readItems.ascii unwritten\n");
#endif
		return False;
	}
	else
	{
		UWORD version;
		f.getUWord(version);
		ASSERT(version <= StrengthPointListVersion);
		UWORD count;
		f.getUWord(count);

		if(count != 0)
		{
			reserve(count, true);		// Reserve and mark as used
		}
			// init(count);
	}

	for(PoolIndex i = 0; i < entries(); i++)
	{
		UBYTE flag;
		f.getUByte(flag);
		if(flag)
		{
			get(i).readData(f, ob);
		}
		else
			remove(i);
	}

	return f.isOK();	// result;
}



Boolean StrengthPointList::writeData(FileWriter& f, const BaseOrderBattle& ob) const
{
	FileChunkWriter fc(f, getChunkName());

	if(f.isAscii())
	{
		f.printf(fmt_sdn, entriesToken, (int) entries());
	}
	else
	{
		f.putUWord(StrengthPointListVersion);
		f.putUWord(entries());
	}

	for(PoolIndex i = 0; i < entries(); i++)
	{
		UBYTE flag = isUsed(i);

		if(f.isAscii())
		{
			if(!flag)
				f.printf(fmt_sn, unusedToken);
		}
		else
			f.putUByte(flag);

		if(flag)
			get(i).writeData(f, ob);
	}

	return f.isOK();
}



/*
 * Garbage Colletion
 */

void StrengthPointList::cleanup()
{
#ifdef DEBUG
	cpLog.printf("SP Garbage Collect");
	int count = 0;
#endif

	for(PoolIndex i = 0; i < entries(); i++)
	{
		if(isUsed(i))
		{
			if(get(i).refCount() == 0)
			{
#ifdef DEBUG
				++count;
				cpLog.printf("Deleted %3d", (int)i);
#endif
				remove(i);
			}
		}
	}
#ifdef DEBUG
	cpLog.printf("Removed %d SPs, %d of %d remaining", count, (int)used(), (int)entries());
#endif
}

