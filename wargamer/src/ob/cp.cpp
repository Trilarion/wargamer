/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 * Generic CP Class
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "cp.hpp"
#include "ob_base.hpp"
#include "wldfile.hpp"
#include "scenario.hpp"
#include "savegame.hpp"
#ifdef DEBUG
#include "todolog.hpp"
#include "cplog.hpp"
#endif
#include "filebase.hpp"       // From system\ for FileReader and writer
#include "obiter.hpp"

/*======================================================================
 * Functions for GenericCP
 */

GenericCP::GenericCP() :
   d_side(SIDE_Neutral),
   d_nationality(NATION_Neutral),
   d_rank(),
   d_leader(NoGLeader),
   // d_name(),
   d_nameID(StringFileTable::Null),
   d_link(),
   d_spEntry(NoStrengthPoint),
   d_isDead(false),
   d_campaignInfo(0),
   d_battleInfo(0),
   d_morale(0),
   d_fatigue(Attribute_Range),
   d_supply(0),
   d_flags(0)
{
}

GenericCP::~GenericCP()
{
   // The Armies class must delete the campaign/battle structures
   // otherwise OB.DLL will be dependant on them.

   ASSERT(d_campaignInfo == 0);
   ASSERT(d_battleInfo == 0);
}

const UWORD GenericCP::s_fileVersion = 0x0008;

Boolean GenericCP::readData(FileReader& f, BaseOrderBattle& ob)
{
   if(f.isAscii())
   {
#ifdef DEBUG
      todoLog.printf("TODO: GenericCP::readData().ascii unfinished\n");
#endif
   }
   else
   {
      UWORD version;
      f.getUWord(version);
      ASSERT(version <= s_fileVersion);


      if(version <= 0x0004)
      {
         NamedItem name;
         f >> name;
         d_nameID = OB_Names::setCPName(name.getName());
         ASSERT(OB_Names::isValidCPName(d_nameID));

         // Skip over CampaignPosition
         UWORD posVersion;
         UWORD w;
         ULONG l;
         UBYTE b;

         f >> posVersion;
         f >> l;     // headAlong
         if(posVersion >= 0x0001)
            f >> l;     // tailAlong
         f >> b;        // Direction
         f >> w;        // town or connection

         // f >> d_link;
         d_link.readData(f, ob);

         // skip olf StrengthPointList version
         UWORD v;
         f >> v;

         // f >> d_spEntry;
         SPIndex sp;
         f >> sp;
         d_spEntry = ob.sp(sp);

         // skip old SideObject version

         f >> v;

         f >> d_side;

         f >> d_rank;      // readData(f);
         if(version >= 0x0001)
         {
            f >> w;
            f >> d_nationality;
         }
         // else

         if(d_nationality == NATION_Neutral)
            setNation(scenario->getDefaultNation(d_side));

         LeaderIndex ileader;
         f >> ileader;
         d_leader = ob.leader(ileader);

         f >> b;     // f.getUByte(morale);
         f >> b;     // f.getUByte(fatigue);
         f >> b;     // f.getUByte(supply);

         if(f.getMode() == SaveGame::Campaign)
         {
            if(version >= 0x0003)
            {
               f.getUByte(b); // startOrder
               f.getUByte(b); // startAgression
            }
         }

         if(version >= 0x0004)
         {
            f.getUByte(b);    // d_active
         }

         d_isDead = false;
      }
      else  // new version
      {
         f >> d_side;
         f >> d_nationality;
         f >> d_rank;

         LeaderIndex ileader;
         f >> ileader;
         d_leader = ob.leader(ileader);
         // f >> d_leader;

         if (version < 0x0008)
         {
            NamedItem name;
            f >> name;
            d_nameID = OB_Names::setCPName(name.getName());
         }
         else
         {
            UBYTE isNew;
            f >> isNew;
            if (isNew)
            {
               CString name(f.getString());
               d_nameID = OB_Names::setCPName(name);
            }
            else
               f >> d_nameID;
         }

         ASSERT(OB_Names::isValidCPName(d_nameID));


         // f >> d_link;
         d_link.readData(f, ob);

         SPIndex sp;
         f >> sp;
         d_spEntry = ob.sp(sp);
         // f >> d_spEntry;

         f >> d_isDead;

         if(version >= 0x0006)
           f >> d_flags;
         else
           d_flags = 0;

//       if(version >= 0x0006)
//         d_campaignInfo->readData(f, reinterpret_cast<OrderBattle*>(&ob));

            if(version >= 0x0007)
            {
               f >> d_morale;
               f >> d_fatigue;
               f >> d_supply;
            }
      }
   }
   return f.isOK();
}

Boolean GenericCP::writeData(FileWriter& f, const BaseOrderBattle& ob) const
{
   if(f.isAscii())
   {
      f.printf(startItemString);
         f.printf(fmt_sdn, sideToken, (int) d_side);
         f.printf(fmt_sdn, nationToken, (int) d_nationality);
         f << d_rank;
         f.printf(fmt_sdn, leaderToken, (int) ob.getIndex(d_leader));
         // f << d_name;
         f << OB_Names::getCPName(d_nameID);
         // f << d_link;
         d_link.writeData(f, ob);
         f.printf(fmt_sdn, strengthPointToken, (int)ob.getIndex(d_spEntry));
         // isDead
      f.printf(endItemString);
   }
   else
   {
      f << s_fileVersion;
      f << d_side;
      f << d_nationality;
      f << d_rank;
      f << ob.getIndex(d_leader);
      // f << d_name;
//      f << d_nameID;

      if (OB_Names::isNewCPName(d_nameID))
      {
         UBYTE isNew = true;
         f << isNew;
         f.putString(OB_Names::getCPName(d_nameID));
      }
      else
      {
         UBYTE isNew = false;
         f << isNew;
         f << d_nameID;
      }

      d_link.writeData(f, ob);
      // f << d_link;
      f << ob.getIndex(d_spEntry);
      f << d_isDead;
      f << d_flags;
//    d_campaignInfo->writeData(f, const_cast<OrderBattle*>(reinterpret_cast<const OrderBattle*>(&ob)));

      f << d_morale;
      f << d_fatigue;
      f << d_supply;


   }
   return f.isOK();
}

SPCount GenericCP::spCount(bool all) const
{
   SPCount spCount = 0;

   if (all)
   {
      G_ConstUnitIter iter(this);

      while(iter.next())
      {
	      ConstRefGenericCP iUnit = iter.current();
	      spCount += iUnit->spCount(false);
      }
   }
   else
   {
	   ConstISP spi = getSPEntry();

	   while(spi != NoStrengthPoint)
	   {
		   ++spCount;
		   spi = spi->getNext();
	   }
   }

   return spCount;
}

bool GenericCP::isSPattached(const ConstISP& isp) const
{
   ConstISP spi = getSPEntry();
   while (spi != NoStrengthPoint)
   {
      if(spi == isp)
         return true;
      spi = spi->getNext();
   }
   return false;
}

/*======================================================================
 * Functions for CommandLinks
 */

CommandLink::CommandLink()
{
   d_self = NoCPIndex;
   d_parent = NoGenericCP;
   d_sister = NoGenericCP;
   d_child = NoGenericCP;
}

const UWORD CommandLink::s_fileVersion = 0x0000;

static RefGenericCP readCPI(FileReader& f, BaseOrderBattle& ob)
{
   CPIndex i;
   f >> i;
   return ob.command(i);
}


Boolean CommandLink::readData(FileReader& f, BaseOrderBattle& ob)
{
   if(f.isAscii())
   {
#ifdef DEBUG
      todoLog.printf("TODO: CommandLink::readData().ascii unwritten\n");
#endif
      return False;
   }
   else
   {
      UWORD version;
      f >> version;
      ASSERT(version <= s_fileVersion);
      // f >> d_parent;
      // f >> d_sister;
      // f >> d_child;
      d_parent = readCPI(f, ob);
      d_sister = readCPI(f, ob);
      d_child = readCPI(f, ob);
   }
   return f.isOK();
}


Boolean CommandLink::writeData(FileWriter& f, const BaseOrderBattle& ob) const
{
   if(f.isAscii())
   {
      f.printf(fmt_sdddn, linkToken,
         (int) ob.getIndex(d_parent),
         (int) ob.getIndex(d_sister),
         (int) ob.getIndex(d_child));
   }
   else
   {
      f << s_fileVersion;
      f << ob.getIndex(d_parent);
      f << ob.getIndex(d_sister);
      f << ob.getIndex(d_child);
   }

   return f.isOK();
}



#ifdef LOG_CP_REF
int GenericCP::addRef() const
{
   int value = RefBaseCount::addRef();
   cpLog.printf("Add CP %3d = %d, %s", (int)getSelf(), value, (const char*)getName());
   return value;
}

int GenericCP::delRef() const
{
   int value = RefBaseCount::delRef();
   cpLog.printf("Del CP %3d = %d, %s", (int)getSelf(), value, (const char*)getName());
   return value;
}
#endif

const char* GenericCP::getName() const
{
   return OB_Names::getCPName(d_nameID);
}

void GenericCP::setName(char* s)
{
   d_nameID = OB_Names::updateCPName(d_nameID, s);
}
