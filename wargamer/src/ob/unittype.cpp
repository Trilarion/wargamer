/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1995, Steven Morle-Green, All Rights Reserved
 * Originally started by Paul
 *----------------------------------------------------------------------
 *
 *      Unit Type Table
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/06/13 08:52:38  greenius
 * Initial Import
 *
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include <stdio.h>

#include "unittype.hpp"
#include "filecnk.hpp"
#include "idstr.hpp"
#include "misc.hpp"
#include "scenario.hpp"
#include "crossref.hpp"
#include "..\batdata\g_troops.hpp"  // naughty!  scemario specific and in higher library!

#ifdef DEBUG
#include "clog.hpp"

LogFile utLog("unittype.log");
#endif

const int UnitTypeConst::s_menPerType[] =
{
    InfantryPerSP,
    CavalryPerSP,
    ArtilleryPerSP,
    SpecialPerSP
};


class TroopCrossRef
{
   public:
   TroopCrossRef(const char* fName);
   ~TroopCrossRef();
   unsigned int get(const char* name, BasicUnitType::value basicType, int graphic_number, bool mounted) const;
   private:
   CrossReferencer* d_ref;
};

TroopCrossRef::TroopCrossRef(const char* fName)
{
   d_ref = new CrossReferencer(fName);
   d_ref->DefineSeperator(" = ");
}

TroopCrossRef::~TroopCrossRef()
{
   delete d_ref;
}

/*
 * Given a graphic name, this routine looks it up in the 'g_troop.dat' file
 * and returns its associated integer index value
 *
 * This is very scenario specific because it refers directly to
 * troop types in the header file.
 */

unsigned int TroopCrossRef::get(const char* name, BasicUnitType::value basicType, int graphic_number, bool mounted) const
{
   if(stricmp(name, "NONE") == 0)
      return 0;

   // find entry corresponding to name
   int sprite_index;
   char* ref_found = d_ref->GetIntegerEntry(name, &sprite_index, 0);
   // if found, returun value

   ASSERT(ref_found);

   if(ref_found)
      return sprite_index;

   // otherwise return defaults for unit type
   else {
#ifdef DEBUG
      // FORCEASSERT("Oops - defaulting to a basic unit type graphic");

      switch(basicType) {

      case BasicUnitType::Infantry : {
            // use French Light Infantry as a default
            return LineInfantryFrance;
         }

      case BasicUnitType::Cavalry : {
            // use French Light Cavalry as a default
            return HeavyCavaFrance;
         }

      case BasicUnitType::Artillery : {
            if(mounted) {
               // defaults for horse artillery
               switch(graphic_number) {
               case 0 : return HorseArtiCannonFrance;
               case 1 : return OfficerCavaAllied;
               case 2 : return HorseArtiManFranceHorse;
               case 3 : return HorseArtiManFranceHorse;
               default : return HorseArtiCannonFrance;
               }
            }
            else {
               // defaults for foot artillery
               switch(graphic_number) {
               case 0 : return FootArtiCannonFrance;
               case 1 : return OfficerCavaAllied;
               case 2 : return FootArtiManFrance;
               case 3 : return FootArtiManFrance;
               default : return FootArtiCannonFrance;
               }
            }
         }

      case BasicUnitType::Special : {
            // use flags as a default
            // FORCEASSERT("Oops - trying to find the default sprite for a special type.");
            return 0;
         }

      default : {
            FORCEASSERT("Oops - unit's basic type not recognised - ensure 'unittype.dat' includes the field 'BasicType' for each unit");
            return 0;
         }
      }
#else
      return 0;
#endif  // DEBUG
   }
}

/*
 * Constructor for an item
 * sets all values to something reasonable.
 */


UnitTypeItem::UnitTypeItem()
{
   nations.clear();
   d_ID = NoUnitType;
   basicType = BasicUnitType::Infantry;
   specialType = SpecialUnitType::NotSpecial;
   totalManpowerCost = 0;
   totalResourceCost = 0;
   weeklyManpowerCost = 0;
   weeklyResourceCost = 0;
   allowedFormations.clear();
   weapon = WeaponType::WT_None;
   response = 0;
   control = 0;
   fireValue = 0;
   impact = 0;
   effectiveValue = 10;
   bombardmentValue = 10;
   baseMorale = 0;
   d_flags.include(UnitTypeFlags::NoValue);
   strategicSpeedModifier = 100;
   skirmishRating = 0;
   d_promoteTo = NoUnitType;
   d_combatVictoryValue = 1.0;

   d_SpriteIndex[0] = 0;
   d_SpriteIndex[1] = 0;
   d_SpriteIndex[2] = 0;
   d_SpriteIndex[3] = 0;
}

UnitTypeItem:: ~UnitTypeItem()
{
}

//-------------------------------------------------------------------

/*
 * Construcor for table
 */

UnitTypeTable::UnitTypeTable()
{

}

UnitTypeTable::~UnitTypeTable()
{
}

/*
 * Read Table from disk
 */

#define DEBUG_UNITTYPE

static const char unitTypeChunkName[] = "UnitTypeTable";

enum {
   UTT_Nations,
   UTT_Description,
   UTT_ID,
   UTT_BasicType,
   UTT_BuildCost,
   UTT_Formations,
   UTT_Weapon,
   UTT_Response,
   UTT_Control,
   UTT_FireValue,
   UTT_Impact,
   UTT_EffectiveValue,
   UTT_BombardmentValue,
   UTT_BaseMorale,
   UTT_AttritionClass,
   UTT_Flags,
   UTT_StrategicSpeedModifier,
   UTT_SkirmishRating,
   UTT_PromoteTo,
   UTT_CombatVictoryValue,

   UTT_GraphicName1,
   UTT_GraphicName2,
   UTT_GraphicName3,
   UTT_GraphicName4,

   UTT_HowMany
};

MessagePair unitTypeTokens[] = {
      { UTT_Nations,               "Nations"                },
      { UTT_Description,           "Description"            },
      { UTT_ID,                    "ID"                     },
      { UTT_BasicType,             "BasicType"              },
      { UTT_BuildCost,             "BuildCost"              },
      { UTT_Formations,            "Formations"             },
      { UTT_Weapon,                "Weapon"                 },
      { UTT_Response,              "Response"               },
      { UTT_Control,               "Control"                },
      { UTT_FireValue,             "FireValue"              },
    { UTT_Impact,                "Impact"                 },
      { UTT_EffectiveValue,        "EffectiveValue"         },
      { UTT_BombardmentValue,      "BombardmentValue"       },
      { UTT_BaseMorale,            "BaseMorale"             },
      { UTT_AttritionClass,        "AttritionClass"         },
      { UTT_Flags,                 "Flags"                  },
      { UTT_StrategicSpeedModifier,"StrategicSpeedModifier" },
      { UTT_SkirmishRating,        "SkirmishRating"         },
      { UTT_PromoteTo,             "PromoteTo"              },
      { UTT_CombatVictoryValue,    "CombatVictory"          },
      { UTT_GraphicName1,          "GraphicName1"           },
      { UTT_GraphicName2,          "GraphicName2"           },
      { UTT_GraphicName3,          "GraphicName3"           },
    { UTT_GraphicName4,          "GraphicName4"           },

    MP_End
};

MessagePair basicTypeNames[] = {
        { BasicUnitType::Infantry,                      "Infantry"                      },
        { BasicUnitType::Cavalry,                       "Cavalry"                       },
        { BasicUnitType::Artillery,             "Artillery"                     },
        MP_End
};

MessagePair specialTypeNames[] = {
        { SpecialUnitType::Engineer,    "Engineer"                      },
        { SpecialUnitType::BridgeTrain, "BridgeTrain"                           },
        { SpecialUnitType::SiegeTrain,   "SiegeTrain"      },
        MP_End
};


Boolean UnitTypeTable::init(const char* fileName)
{
#ifdef DEBUG_UNITTYPE
   debugLog("+Starting to Read UnitType Table %s\n", fileName);
#endif

   try
   {
      /*
      * load unittype.dat file
      */

      WinFileAsciiChunkReader f(fileName);
      readData(f);
   }
   catch(const FileError& f)
   {
      throw GeneralError("Can not read UnitTypeTable %s", fileName);
      // return False;
   }

#ifdef DEBUG_UNITTYPE
   debugLog("-Finished Reading UnitType Table %s\n", fileName);
#endif

   return True;
}

Boolean UnitTypeTable::readData(WinFileAsciiChunkReader& f)
{
   ASSERT(f.isAscii());

   FileChunkReader fr(f, unitTypeChunkName);
   if(!fr.isOK())
      throw FileError();

#ifdef DEBUG_UNITTYPE
   debugLog("Found %s chunk\n", unitTypeChunkName);
#endif

   CString crossName = scenario->makeScenarioFileName("g_troops.dat");
   TroopCrossRef crossRef(crossName);

   int count = fr.countEntries();
   if(!fr.isOK())
      throw FileError();

#ifdef DEBUG_UNITTYPE
   debugLog("%d entries\n", count);
#endif

   if(count > 0)
   {
      ASSERT(count < ArrayIndex_MAX);

      items.init((ArrayIndex) count);

      for(ArrayIndex i = 0; i < count; i++)
      {
         items[i].readData(fr, crossRef);
         ASSERT(items[i].getID() == i);
      }
   }

   return fr.isOK();
}


Boolean UnitTypeItem::readData(FileChunkReader& f, const TroopCrossRef& ref)
{
#ifdef DEBUG_UNITTYPE
   debugLog("+UnitTypeItem::readData()\n");
#endif

   if(f.startItem())
   {
      int token;
      StringParse parameters;

      bool ignore = false;

      while(f.getItem(token, parameters, unitTypeTokens))
      {
#ifdef DEBUG
         const char* parameterString = (const char*) parameters;
         const char* tokenName = "Unknown";
         mpGetName(unitTypeTokens, tokenName, token);

         utLog.printf("token=%d (%s), parameters=%s",
            token,
            tokenName,
            (const char*) parameterString);
#endif
         // if(!ignore)
         // {
         switch(token)
         {
         case UTT_Nations:
            {
               unsigned int n;
               while(parameters.getInt(n))
                  nations.include(Nationality(n));
            }
            break;

         case UTT_Description:
            {
               char* s = parameters.getToken();
               ASSERT(s);
               if(s)
               {
                  if(strcmp(s, "z") == 0)
                  {
                     ignore = true;
                  }

                  description = s;    // description is a StringPtr
               }
               else
                  goto badFormat;
            }
            break;

         case UTT_BasicType:
            {
               const char* typeName = parameters.getToken();
               if(typeName)
               {
                  int num;

                  if(mpGetID(basicTypeNames, typeName, num))
                  {
                     basicType = BasicUnitType::value(num);
                     specialType = SpecialUnitType::NotSpecial;
                  }
                  else
                  {
                     basicType = BasicUnitType::Special;

                     if(mpGetID(specialTypeNames, typeName, num))
                        specialType = SpecialUnitType::value(num);
                     else
                        goto badFormat;
                  }
               }
               else
                  goto badFormat;

            }
            break;

         case UTT_BuildCost:
            {
               unsigned int n;
               if(parameters.getInt(n))
                  totalManpowerCost = AttributePoints(n);
               else
                  goto badFormat;

               if(parameters.getInt(n))
                  totalResourceCost = AttributePoints(n);
               else
                  goto badFormat;

               if(parameters.getInt(n))
                  weeklyManpowerCost = AttributePoints(n);
               else
                  goto badFormat;

               if(parameters.getInt(n))
                  weeklyResourceCost = AttributePoints(n);
               else
                  goto badFormat;
            }
            break;

         case UTT_Formations:
            {
               unsigned int n;
               while(parameters.getInt(n))
                  allowedFormations.include(Formations::value(n));
            }
            break;

         case UTT_Weapon:
            {
               unsigned int n;
               if(parameters.getInt(n))
                  weapon = WeaponType::value(n);
               else
                  goto badFormat;
            }
            break;

         case UTT_Response:
            {
               unsigned int n;
               if(parameters.getInt(n))
                  response = Attribute(n);
               else
                  goto badFormat;
            }
            break;

         case UTT_Control:
            {
               unsigned int n;
               if(parameters.getInt(n))
                  control = Attribute(n);
               else
                  goto badFormat;
            }
            break;

         case UTT_FireValue:
            {
               unsigned int n;
               if(parameters.getInt(n))
                  fireValue = Attribute(n);
               else
                  goto badFormat;
            }
            break;

         case UTT_Impact:
            {
               unsigned int n;
               if(parameters.getInt(n))
                  impact = Attribute(n);
               else
                  goto badFormat;
            }
            break;

         case UTT_EffectiveValue:
            {

               unsigned int n;
               if(parameters.getFixed(n, 10))
                  effectiveValue = SPValue(n);
               else
                  goto badFormat;
            }
            break;

         case UTT_BombardmentValue:
            {

               unsigned int n;
               if(parameters.getFixed(n, 10))
                  bombardmentValue = BombValue(n);
               else
                  goto badFormat;
            }
            break;

         case UTT_BaseMorale:
            {
               unsigned int n;
               if(parameters.getInt(n))
                  baseMorale = Attribute(n);
               else
                  goto badFormat;
            }
            break;


         case UTT_AttritionClass:
            {
               unsigned int n;
               if(parameters.getInt(n))
                  attritionClass = AttritionClass::value(n);
               else
                  goto badFormat;
            }
            break;

         case UTT_Flags:
            {
               unsigned int n;
               while(parameters.getInt(n))
                  d_flags.include(static_cast<UnitTypeFlags::value>(n));
            }
            break;

#if 0
            case UTT_Mounted:
            {
               unsigned int n;
               if(parameters.getInt(n))
                  mounted = static_cast<Boolean>(n);
               else
                  goto badFormat;
            }
            break;

         case UTT_LightCavalry:
            {
               unsigned int n;
               if(parameters.getInt(n))
                  lightCavalry = static_cast<Boolean>(n);
               else
                  goto badFormat;
            }
            break;
#endif

         case UTT_StrategicSpeedModifier:
            {
               unsigned int n;
               if(parameters.getFixed(n, 100))
                  strategicSpeedModifier = (UWORD) n;
               else
                  goto badFormat;
            }
            break;

         case UTT_SkirmishRating:
            {
               unsigned int n;
               if(parameters.getInt(n))
                  skirmishRating = Attribute(n);
               else
                  goto badFormat;
            }
            break;

         case UTT_ID:
            {
               unsigned int n;
               if(parameters.getInt(n))
                  d_ID = static_cast<UnitType>(n);
               else
                  goto badFormat;
            }
            break;

         case UTT_PromoteTo:
            {
               unsigned int n;
               if(parameters.getInt(n))
                  d_promoteTo = static_cast<UnitType>(n);
               else
                  goto badFormat;
            }
            break;

         case UTT_CombatVictoryValue:
            {
               unsigned int n;
               const int precision = 100;
               if(parameters.getFixed(n, precision))
               {
                  d_combatVictoryValue = (float) n / (float) precision;
               }
               else
                  goto badFormat;
            }
            break;

         case UTT_GraphicName1 :
            {
               char* s = parameters.getToken();
               ASSERT(s);
               if(s) {
				  /*
					NOTE : It is important that d_SpriteIndex[0..4] are all set here
					This is because in UNITTYPE.DAT some units (ie. Infantry & Cavalry)
					may only specify ONE graphic type, wheras they actuall use all FOUR -
					assuming that they are initialized the same as the first...
				  */
                  d_SpriteIndex[0] = ref.get(s, basicType, 0, isMounted());                        // match string to sprite index value from "g_troops.dat"
                  d_SpriteIndex[1] = ref.get(s, basicType, 0, isMounted());
                  d_SpriteIndex[2] = ref.get(s, basicType, 0, isMounted());
                  d_SpriteIndex[3] = ref.get(s, basicType, 0, isMounted());
               }
               else goto badFormat;
            }
            break;

         case UTT_GraphicName2 :
            {
               char* s = parameters.getToken();
               ASSERT(s);
               if(s) d_SpriteIndex[1] = ref.get(s, basicType, 1, isMounted());                        // match string to sprite index value from "g_troops.dat"
               else goto badFormat;
            }
            break;

         case UTT_GraphicName3 :
            {
               char* s = parameters.getToken();
               ASSERT(s);
               if(s) d_SpriteIndex[2] = ref.get(s, basicType, 2, isMounted());                        // match string to sprite index value from "g_troops.dat"
               else goto badFormat;
            }
            break;

         case UTT_GraphicName4 :
            {
               char* s = parameters.getToken();
               ASSERT(s);
               if(s) d_SpriteIndex[3] = ref.get(s, basicType, 3, isMounted());                        // match string to sprite index value from "g_troops.dat"
               else goto badFormat;
            }
            break;

         badFormat:
            {
#ifdef DEBUG
               const char* tokenName = "Unknown";
               mpGetName(unitTypeTokens, tokenName, token);

               utLog.printf("Bad format in UnitType Table");
               utLog.printf("token=%d (%s)",
                  (int) token,
                  (const char*) tokenName);
               utLog.printf("parameters=%s",
                  (const char*) parameterString);
#else
               throw GeneralError("Error in Unit Type Table");
#endif
            }
            break;
#ifdef DEBUG                    // If not in debug mode, then just ignore token
         default:
            throw GeneralError("Illegal Token (%d) in UnitTypeItem::readData()", token);
#endif
         }
      }

      /*
      * If it was an invalid item, clear out info so it doesn't accidently get used
      */

      if(ignore)
      {
         nations.clear();  // nobody is allowed to build it
         totalManpowerCost = 0;
         totalResourceCost = 0;
         weeklyManpowerCost = 0;
         weeklyResourceCost = 0;
         allowedFormations.clear();
         weapon = WeaponType::WT_None;
         response = 0;
         control = 0;
         fireValue = 0;
         impact = 0;
         effectiveValue = 10;
         bombardmentValue = 10;
         baseMorale = 0;
         d_flags.clear();
         skirmishRating = 0;
         d_promoteTo = NoUnitType;
         d_combatVictoryValue = 0;

         d_SpriteIndex[0] = 0;
         d_SpriteIndex[1] = 0;
         d_SpriteIndex[2] = 0;
         d_SpriteIndex[3] = 0;
      }
   }
   else
      throw GeneralError("Missing '{' in UnitType Data");

#ifdef DEBUG_UNITTYPE
    debugLog("-UnitTypeItem::readData()\n");
#endif

#ifdef DEBUG
    utLog.close();
#endif

    return f.isOK();
}



void UnitTypeItem::getTotalResource(AttributePoints& man, AttributePoints& res) const
{
   man = totalManpowerCost;
   res = totalResourceCost;
}

void UnitTypeItem::getWeeklyResource(AttributePoints& man, AttributePoints& res) const
{
   man = weeklyManpowerCost;
   res = weeklyResourceCost;
}

