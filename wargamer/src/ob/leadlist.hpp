/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef LEADLIST_HPP
#define LEADLIST_HPP

#ifndef __cplusplus
#error leadlist.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *	Leader List
 *
 *----------------------------------------------------------------------
 */

#include "gamedefs.hpp"
#include "obdefs.hpp"
#include "poolfile.hpp"
#include "leader.hpp"

class LeaderList :
	public PoolArrayFileObject<GLeader>
{
	typedef PoolArrayFileObject<GLeader> Super;
	typedef LeaderIndex Index;
public:
	LeaderList() : Super() { }		// LeaderIndex::setList(this); }
	virtual ~LeaderList() { }		// LeaderIndex::setList(0); }

	const char* getChunkName() const;

	RefGLeader operator [] (LeaderIndex spi)
	{
		if(spi == NoLeaderIndex)
			return NoGLeader;
		else
			return &Super::operator[](spi);
	}

	ConstRefGLeader operator [] (LeaderIndex spi) const
	{
		if(spi == NoLeaderIndex)
			return NoGLeader;
		else
			return &Super::operator[](spi);
	}

	void cleanup();
		// Garbage collection

private:
	void remove(PoolIndex i) { Super::remove(i); }		// Demote to private
};

#endif /* LEADLIST_HPP */

