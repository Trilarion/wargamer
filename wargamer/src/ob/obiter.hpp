/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef OBIter_HPP
#define OBIter_HPP

#include "ob_dll.h"
#include "gamedefs.hpp"
#include "sync.hpp"
#include "obdefs.hpp"

/*
 * Order of Battle Iterator
 */

class OrderBattle;

/*
 * Iterator for the sides (previously nations) in an army
 */

class OB_DLL OBSideIter
{
		OBSideIter(const OBSideIter&);
		const OBSideIter& operator =(const OBSideIter&);
	public:
		OBSideIter(const OrderBattle* a);
		~OBSideIter() { }

		bool operator ++();
		Side current() const;
		void reset();

	private:
		const OrderBattle* d_ob;
		Side d_next;
		Reader d_lock;
};

/*=====================================================================
 * Iterates through units in an army
 */

class OB_DLL G_UnitIter {
	public:
		G_UnitIter(OrderBattle* a);
		G_UnitIter(OrderBattle* a, const RefGenericCP& startUnit);
      G_UnitIter(const RefGenericCP& startUnit);
		~G_UnitIter() { }

		void reset(const RefGenericCP& startUnit);
		void resetSide(Side s);
		void reset();

		RefGenericCP current() const;
		// GenericCP* currentCommand() const;
		bool sister();
		// Also need:
		//  child() : goes to current's child (stops if no children)
		bool next();			// Goes through all units (from top to bottom)
	private:
		OrderBattle*	d_container;
		RefGenericCP 	d_cpi;
		RefGenericCP 	d_top;
		bool 			d_started;
		Reader					d_lock;
		// Writer			d_lock;
};

class OB_DLL G_ConstUnitIter {
	public:
		G_ConstUnitIter(const OrderBattle* a);
		G_ConstUnitIter(const OrderBattle* a, const ConstRefGenericCP& startUnit);
		G_ConstUnitIter(const ConstRefGenericCP& startUnit);
		~G_ConstUnitIter() { }

		void reset(const ConstRefGenericCP& startUnit);
		void resetSide(Side s);
		void reset();

		ConstRefGenericCP current() const;
		bool sister();
		bool next(bool intoChildren = true);	// Goes through all units (from top to bottom)
	private:
		const OrderBattle*	d_container;
		ConstRefGenericCP 		d_cpi;
		ConstRefGenericCP 		d_top;
		bool 					d_started;
		Reader					d_lock;
};



/*=====================================================================
 * Combination of Side and Unit Iterators
 * Goes through every unit in every side.
 */

class OB_DLL G_CPIter
{
	public:
		G_CPIter(OrderBattle* ob);
		RefGenericCP current() const { return ui.current(); }
		bool sister();
		bool next();
		void reset();
	private:
		G_UnitIter ui;
		OBSideIter ni;
};

class OB_DLL G_ConstCPIter
{
	public:
		G_ConstCPIter(const OrderBattle* ob);
		ConstRefGenericCP current() const { return ui.current(); }
		bool sister();
		bool next();
		void reset();
	private:
		G_ConstUnitIter ui;
		OBSideIter ni;
};

/*=====================================================================
 * goes through strength points directly attached to unit
 */

class OB_DLL G_StrengthPointIter {
	public:
		G_StrengthPointIter(OrderBattle* a);
		G_StrengthPointIter(OrderBattle* a, const ISP& isp);
		G_StrengthPointIter(OrderBattle* a, const RefGenericCP& cp);
		~G_StrengthPointIter() { }

		bool operator ++();
		ISP current() const;
		void setSP(const ISP& isp);
		void setCP(const RefGenericCP& cp);

	private:
		// StrengthPointList* d_list;
		ISP d_isp;
		ISP d_next;
		bool d_valid;
		Reader					d_lock;
		// Writer d_lock;
};

class OB_DLL G_ConstStrengthPointIter {
	public:
		G_ConstStrengthPointIter(const OrderBattle* a);
		G_ConstStrengthPointIter(const OrderBattle* a, const ConstISP& isp);
		bool operator ++();
		ConstISP current() const;
		void setSP(const ConstISP& isp);

	private:
		// StrengthPointList* d_list;
		ConstISP d_isp;
		ConstISP d_next;
		bool d_valid;
		Reader d_lock;
};



/*=====================================================================
 * Goes through all StrengthPoints in an organization
 */

class OB_DLL G_SPIter
{
	public:
		G_SPIter(OrderBattle* ob, const RefGenericCP& startUnit);
		bool operator ++();
		ISP currentISP() const { return si.current(); }
		RefGenericCP currentICP() const { return ui.current(); }
		ISP current() const { return si.current(); }

	private:
		bool newUnit();

	private:
		OrderBattle* d_ob;
		G_UnitIter ui;
		G_StrengthPointIter si;
};

class OB_DLL G_ConstSPIter
{
	public:
		G_ConstSPIter(const OrderBattle* ob, const ConstRefGenericCP& startUnit);
		bool operator ++();
		ConstISP currentISP() const { return si.current(); }
		ConstRefGenericCP currentICP() const { return ui.current(); }
		ConstISP current() const { return si.current(); }

	private:
		bool newUnit();

	private:
		const OrderBattle* d_ob;
		G_ConstUnitIter ui;
		G_ConstStrengthPointIter si;
};


#endif   // OBIter_HPP
