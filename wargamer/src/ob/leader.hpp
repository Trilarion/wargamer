/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
#ifndef LEADER_HPP
#define LEADER_HPP

#ifndef __cplusplus
#error leader.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 * Leaders (Generals)
 *
 *----------------------------------------------------------------------
 */

#include "ob_dll.h"
#include "gamedefs.hpp"
#include "obdefs.hpp"
#include "rank.hpp"
#include "name.hpp"
#include "obNames.hpp"
#include "refptr.hpp"
// #include "measure.hpp"
#include "cp.hpp"
#include "sp.hpp"
// #include "StringPtr.hpp"

// #ifdef DEBUG
// #define LOG_LEADER_REF
// #endif

/*=====================================================================
 * A General is a person who commmands an organisation
 * His attributes are:
 *    Initiative:
 *       Reaction to new orders
 *       Response to enemy presence
 *       Capability of moving his troops at fast rate
 *    Staff:
 *       Control of multiple units (total of subordinates subordinate rating)
 *       Speed of issuing orders
 *    Subordination:
 *       How controllable they are
 *       NOTE: Low value is good.
 *    Aggression:
 *       Inclination to seek out enemy
 *       Attacking without fear of losses
 *       Willingness to keep fighting despite losses
 *    Charisma:
 *       Ability to inspire troops under his command (increases morale)
 *    Tactical Ability:
 *       Battlefield response to opportunity/threat
 *    Command Radius:
 *       How close to units before effects subordinates
 *    Rank:
 *       Nobility level... can not put low rank officer in charge of higher one
 *    Command Level:
 *       Highest rank at which they perform well
 *    Health:
 *       How alive they are!
 *
 */

class OB_DLL Specialist {  // specialist types
  public:
    enum Type {
      Cavalry,
      Artillery,
      NonSpecialist,

      HowMany,
      First = Cavalry
    };

    static const char* typeName(Type type);
};

class OB_DLL GLeader :
   public RefBaseCount
{
   static const UWORD fileVersion;

   // NamedItem   d_name;
   StringFileTable::ID d_nameID;

   Side        d_side;
   Nationality d_nation;

   enum AttributeTypes {
      Initiative,
      Staff,
      Subordination,
      Aggression,
      Charisma,
      TacticalAbility,
      CommandRadius,             // Convert with look up table
      Health,                    // determines how well leader recovers from illness or wounds

      AttributeTypes_HowMany,

      AttributeTypes_First = Initiative
   };

   Attribute d_attributes[AttributeTypes_HowMany];

   Rank d_rankLevel;             // leaders effective command ceiling
// Rank d_commandRank;           // what
   RefGenericCP d_command;       // What is it commanding?

   // various flags relating to the leader
   enum StatusFlags {
     HorseKilled       = 0x0001, // this is pure chrome. has no real effect
     LightlyWounded    = 0x0002,
      Sick              = 0x0004,
     SeriouslyWounded  = 0x0008,
     Captured          = 0x0010,
     Dead              = 0x0020,
     OutOfIt           = 0x0040, // Cannot participate in next round of battle
     HitDuringBattle   = 0x0080, // was hit during this battle, cleared at the end of a battle
     SupremeLeader     = 0x0100, // is this the supreme leader
     Marshall          = 0x0200, // is he a marshall?
     FearedByEnemy     = 0x0400, // is he feared by opposing forces?
     Lucky             = 0x0800, // is he lucky?
     CommandingType    = 0x1000  // if a specialist, is he commanding his type?
   };

   void setFlag(UWORD mask, Boolean f)
   {
     if(f)
       d_statusFlags |= mask;
     else
       d_statusFlags &= ~mask;
   }

   UWORD d_statusFlags;
   UBYTE d_ranksAboveCeiling;   // how many rank levels above our ceiling are we commanding

   Specialist::Type d_specialist;            // Is he a specialist?
// Specialist::Type d_commandingWhatType;    // Is he commanding all specialist troops?

   HelpID d_helpID;                    // Help-topic ID. Not all leaders will have this
   NamedItem d_portrait;               // Portrait for UnitInfo (0 length for default)

#ifdef DEBUG
   static int s_nextID;
   int d_id;
#endif

public:
   GLeader();
   ~GLeader() { }

#ifdef LOG_LEADER_REF
   int addRef() const;
   int delRef() const;
#else
   int addRef() const { return RefBaseCount::addRef(); }
   int delRef() const { return RefBaseCount::delRef(); }
#endif
   int refCount() const { return RefBaseCount::refCount(); }

   /*
    * Names, Nations, Sides
    */

// const char* getName() const { return d_name.getName(); }
// const char* getNameNotNull(const char* def = 0) const  { return d_name.getNameNotNull(def); }
// void setName(char* s) { d_name.setName(s); }

   const char* getName() const;
   const char* getNameNotNull(const char* def = 0) const  { return getName(); }
   void setName(char* s);

   void nation(Nationality n) { d_nation = n; }
   void setNation(Nationality n) { nation(n); }
   Nationality nation() const { return d_nation; }
   Nationality getNation() const { return nation(); }

   Side side() const { return d_side; }
   void side(Side side) { d_side = side; }
   Side getSide() const { return side(); }
   void setSide(Side s) { side(s); }

   /*
    * Attributes
    */

   void setInitiative(Attribute n)    { d_attributes[Initiative] = n; };
   void setStaff(Attribute n)         { d_attributes[Staff] = n; };
   void setSubordination(Attribute n) { d_attributes[Subordination] = n; };
   void setAggression(Attribute n)    { d_attributes[Aggression] = n; };
   void setCharisma(Attribute n)      { d_attributes[Charisma] = n; };
   void setTactical(Attribute n)      { d_attributes[TacticalAbility] = n; };
   void setCommandRadius(Attribute n) { d_attributes[CommandRadius] = n; }
   void setHealth(Attribute n)        { d_attributes[Health] = n; };

   Attribute getInitiative(Boolean actual = False) const { return modifyAttribute(Initiative, actual); }
   Attribute getStaff(Boolean actual = False) const { return modifyAttribute(Staff, actual); }
   Attribute getSubordination() const { return d_attributes[Subordination]; }
   Attribute getAggression(Boolean actual = False) const { return modifyAttribute(Aggression, actual); }
   Attribute getCharisma(Boolean actual = False) const { return modifyAttribute(Charisma, actual); }
   Attribute getTactical(Boolean actual = False) const { return modifyAttribute(TacticalAbility, actual); }
   Attribute getCommandRadiusAttribute(Boolean actual = False) const { return modifyAttribute(CommandRadius, actual); }
   Attribute getHealth() const { return d_attributes[Health]; }

   void ranksAboveCeiling(UBYTE n) { d_ranksAboveCeiling = n; }
   UBYTE ranksAboveCeiling() const { return d_ranksAboveCeiling; }

   //------- for backwards compatibility
   // void setRankRating(Attribute n) {}
   // Attribute getRankRating() const { return 0; }

   /*
    * Rank level functions
    */

   void setRankLevel(Rank n) { d_rankLevel = n; };
   Rank getRankLevel() const { return d_rankLevel; }

   /*
    * Command access
    */

   void command(const RefGenericCP& n) { d_command = n; }
// RefGenericCP& r_command() { return d_command; }
      // Reference
   const RefGenericCP& command() const { return d_command; }         // What is it commanding?
   const RefGenericCP& getCommand() const { return command(); }

   /*
    * Misc.
    */

   void setHelpID(HelpID id) { d_helpID = id; }
   HelpID getHelpID() const { return d_helpID; }

   const char* portrait() const { return d_portrait.getName(); }
   void portrait(char* name) { d_portrait.setName(name); }

   void setDefault(int dieRollModifier, int nCreated);
   void setDefault(int dieRollModifier = 0)
   {
      setDefault(dieRollModifier, 0);
   }

   /*
    * Status functions
    */

   void clearBattleStatusFlags();
   void isSupremeLeader(Boolean f)    { setFlag(SupremeLeader, f); }
   void isSick(Boolean f)             { setFlag(Sick, f); }
   void isLightlyWounded(Boolean f)   { setFlag(LightlyWounded, f); }
   void isOutOfIt(Boolean f)          { setFlag(OutOfIt, f); }
   void isSeriouslyWounded(Boolean f) { setFlag(SeriouslyWounded, f); }
   void isDead(Boolean f)             { setFlag(Dead, f); }
   void isCaptured(Boolean f)         { setFlag(Captured, f); }
   void isHorseKilled(Boolean f)      { setFlag(HorseKilled, f); }
   void isHitDuringBattle(Boolean f)  { setFlag(HitDuringBattle, f); }
   void isMarshall(Boolean f)         { setFlag(Marshall, f); }
   void isFeared(Boolean f)           { setFlag(FearedByEnemy, f); }
   void isLucky(Boolean f)            { setFlag(Lucky, f); }
   void isCommandingType(Boolean f)   { setFlag(CommandingType, f); }

   Boolean isSupremeLeader()    const { return (d_statusFlags & SupremeLeader) != 0; }
   Boolean isSick()             const { return (d_statusFlags & Sick) != 0; }
   Boolean isLightlyWounded()   const { return (d_statusFlags & LightlyWounded) != 0; }
   Boolean isOutOfIt()          const { return (d_statusFlags & OutOfIt) != 0; }
   Boolean isSeriouslyWounded() const { return (d_statusFlags & SeriouslyWounded) != 0; }
   Boolean isDead()             const { return (d_statusFlags & Dead) != 0; }
   Boolean isHorseKilled()      const { return (d_statusFlags & HorseKilled) != 0; }
   Boolean isCaptured()         const { return (d_statusFlags & Captured) != 0; }
   Boolean isHitDuringBattle()  const { return (d_statusFlags & HitDuringBattle) != 0; }
   Boolean isMarshall()         const { return (d_statusFlags & Marshall) != 0; }
   Boolean isFeared()           const { return (d_statusFlags & FearedByEnemy) != 0; }
   Boolean isLucky()            const { return (d_statusFlags & Lucky) != 0; }
   Boolean isCommandingType()   const { return (d_statusFlags & CommandingType) != 0; }
   /*
    * Leader Commanding Type functions
    */

#if 0
   Boolean isCommandingSpecialist() const { return d_commandingWhatType < Specialist::NonSpecialist; }
     // Is leader commanding specialist troops(i.e. all cavalry, artillery, etc.)
   void setCommandingWhatType(Specialist::Type type) { d_commandingWhatType = type; }
     // Leader is commanding this type
#endif

   void setAsSpecialist(Specialist::Type type) { d_specialist = type; }
     // Set leader as specialist
   Specialist::Type getSpecialistType() const { return d_specialist; }
     // What type of specialist is this leader(or non-specialist)
   Boolean isSpecialist() const { return d_specialist < Specialist::NonSpecialist; }
     // Is this leader a specialist
   Boolean isSpecialistType(Specialist::Type type) const { return d_specialist == type; }
     // Is this leader this type of specialist

   /*
    * File Interface
    */

   Boolean readData(FileReader& f);
   Boolean writeData(FileWriter& f) const;

   int getRating();        // Overall rating

#ifdef DEBUG
   int getID() const { return d_id; }
#endif

private:

   Attribute modifyAttribute(AttributeTypes type, bool actual) const;
};


typedef RefPtr<GLeader> RefRealGLeader;
typedef CRefPtr<GLeader> CRefRealGLeader;

static GLeader* const NoRealGLeader = 0;

#endif /* LEADER_HPP */

/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.2  2001/06/18 08:25:45  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
