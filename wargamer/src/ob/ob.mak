# Microsoft Developer Studio Generated NMAKE File, Based on ob.dsp
!IF "$(CFG)" == ""
CFG=ob - Win32 Editor Debug
!MESSAGE No configuration specified. Defaulting to ob - Win32 Editor Debug.
!ENDIF 

!IF "$(CFG)" != "ob - Win32 Release" && "$(CFG)" != "ob - Win32 Debug" && "$(CFG)" != "ob - Win32 Editor Debug" && "$(CFG)" != "ob - Win32 Editor Release"
!MESSAGE Invalid configuration "$(CFG)" specified.
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "ob.mak" CFG="ob - Win32 Editor Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "ob - Win32 Release" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE "ob - Win32 Debug" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE "ob - Win32 Editor Debug" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE "ob - Win32 Editor Release" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE 
!ERROR An invalid configuration is specified.
!ENDIF 

!IF "$(OS)" == "Windows_NT"
NULL=
!ELSE 
NULL=nul
!ENDIF 

CPP=cl.exe
MTL=midl.exe
RSC=rc.exe

!IF  "$(CFG)" == "ob - Win32 Release"

OUTDIR=.\..\..\exe
INTDIR=.\o\release
# Begin Custom Macros
OutDir=.\..\..\exe
# End Custom Macros

!IF "$(RECURSE)" == "0" 

ALL : "$(OUTDIR)\ob.dll"

!ELSE 

ALL : "system - Win32 Release" "gamesup - Win32 Release" "$(OUTDIR)\ob.dll"

!ENDIF 

!IF "$(RECURSE)" == "1" 
CLEAN :"gamesup - Win32 ReleaseCLEAN" "system - Win32 ReleaseCLEAN" 
!ELSE 
CLEAN :
!ENDIF 
	-@erase "$(INTDIR)\b_result.obj"
	-@erase "$(INTDIR)\cp.obj"
	-@erase "$(INTDIR)\cplist.obj"
	-@erase "$(INTDIR)\cplog.obj"
	-@erase "$(INTDIR)\leader.obj"
	-@erase "$(INTDIR)\leadlist.obj"
	-@erase "$(INTDIR)\nations.obj"
	-@erase "$(INTDIR)\ob.obj"
	-@erase "$(INTDIR)\obinit.obj"
	-@erase "$(INTDIR)\obiter.obj"
	-@erase "$(INTDIR)\obnames.obj"
	-@erase "$(INTDIR)\poolfile.obj"
	-@erase "$(INTDIR)\rank.obj"
	-@erase "$(INTDIR)\sideinfo.obj"
	-@erase "$(INTDIR)\sp.obj"
	-@erase "$(INTDIR)\splist.obj"
	-@erase "$(INTDIR)\unittype.obj"
	-@erase "$(INTDIR)\vc60.idb"
	-@erase "$(INTDIR)\vc60.pdb"
	-@erase "$(OUTDIR)\ob.dll"
	-@erase "$(OUTDIR)\ob.exp"
	-@erase "$(OUTDIR)\ob.lib"
	-@erase "$(OUTDIR)\ob.pdb"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

"$(INTDIR)" :
    if not exist "$(INTDIR)/$(NULL)" mkdir "$(INTDIR)"

CPP_PROJ=/nologo /MD /W3 /Gi /GR /GX /Zi /O2 /I "..\h" /I "..\..\res" /I "..\system" /I "..\gamesup" /D "NDEBUG" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "EXPORT_OB_DLL" /Fp"$(INTDIR)\ob.pch" /YX"stdinc.hpp" /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /c 
MTL_PROJ=/nologo /D "NDEBUG" /mktyplib203 /win32 
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\ob.bsc" 
BSC32_SBRS= \
	
LINK32=link.exe
LINK32_FLAGS=user32.lib /nologo /dll /incremental:no /pdb:"$(OUTDIR)\ob.pdb" /debug /machine:I386 /out:"$(OUTDIR)\ob.dll" /implib:"$(OUTDIR)\ob.lib" 
LINK32_OBJS= \
	"$(INTDIR)\b_result.obj" \
	"$(INTDIR)\cp.obj" \
	"$(INTDIR)\cplist.obj" \
	"$(INTDIR)\cplog.obj" \
	"$(INTDIR)\leader.obj" \
	"$(INTDIR)\leadlist.obj" \
	"$(INTDIR)\nations.obj" \
	"$(INTDIR)\ob.obj" \
	"$(INTDIR)\obinit.obj" \
	"$(INTDIR)\obiter.obj" \
	"$(INTDIR)\obnames.obj" \
	"$(INTDIR)\poolfile.obj" \
	"$(INTDIR)\rank.obj" \
	"$(INTDIR)\sideinfo.obj" \
	"$(INTDIR)\sp.obj" \
	"$(INTDIR)\splist.obj" \
	"$(INTDIR)\unittype.obj" \
	"$(OUTDIR)\gamesup.lib" \
	"$(OUTDIR)\system.lib"

"$(OUTDIR)\ob.dll" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

!ELSEIF  "$(CFG)" == "ob - Win32 Debug"

OUTDIR=.\..\..\exe
INTDIR=.\o\debug
# Begin Custom Macros
OutDir=.\..\..\exe
# End Custom Macros

!IF "$(RECURSE)" == "0" 

ALL : "$(OUTDIR)\obDB.dll"

!ELSE 

ALL : "system - Win32 Debug" "gamesup - Win32 Debug" "$(OUTDIR)\obDB.dll"

!ENDIF 

!IF "$(RECURSE)" == "1" 
CLEAN :"gamesup - Win32 DebugCLEAN" "system - Win32 DebugCLEAN" 
!ELSE 
CLEAN :
!ENDIF 
	-@erase "$(INTDIR)\b_result.obj"
	-@erase "$(INTDIR)\cp.obj"
	-@erase "$(INTDIR)\cplist.obj"
	-@erase "$(INTDIR)\cplog.obj"
	-@erase "$(INTDIR)\leader.obj"
	-@erase "$(INTDIR)\leadlist.obj"
	-@erase "$(INTDIR)\nations.obj"
	-@erase "$(INTDIR)\ob.obj"
	-@erase "$(INTDIR)\obinit.obj"
	-@erase "$(INTDIR)\obiter.obj"
	-@erase "$(INTDIR)\obnames.obj"
	-@erase "$(INTDIR)\poolfile.obj"
	-@erase "$(INTDIR)\rank.obj"
	-@erase "$(INTDIR)\sideinfo.obj"
	-@erase "$(INTDIR)\sp.obj"
	-@erase "$(INTDIR)\splist.obj"
	-@erase "$(INTDIR)\unittype.obj"
	-@erase "$(INTDIR)\vc60.idb"
	-@erase "$(INTDIR)\vc60.pdb"
	-@erase "$(OUTDIR)\obDB.dll"
	-@erase "$(OUTDIR)\obDB.exp"
	-@erase "$(OUTDIR)\obDB.ilk"
	-@erase "$(OUTDIR)\obDB.lib"
	-@erase "$(OUTDIR)\obDB.pdb"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

"$(INTDIR)" :
    if not exist "$(INTDIR)/$(NULL)" mkdir "$(INTDIR)"

CPP_PROJ=/nologo /MDd /W3 /Gm /Gi /GR /GX /ZI /Od /I "..\h" /I "..\..\res" /I "..\system" /I "..\gamesup" /D "_DEBUG" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "EXPORT_OB_DLL" /Fp"$(INTDIR)\ob.pch" /YX"stdinc.hpp" /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /GZ /c 
MTL_PROJ=/nologo /D "_DEBUG" /mktyplib203 /win32 
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\ob.bsc" 
BSC32_SBRS= \
	
LINK32=link.exe
LINK32_FLAGS=user32.lib /nologo /dll /incremental:yes /pdb:"$(OUTDIR)\obDB.pdb" /debug /machine:I386 /out:"$(OUTDIR)\obDB.dll" /implib:"$(OUTDIR)\obDB.lib" /pdbtype:sept 
LINK32_OBJS= \
	"$(INTDIR)\b_result.obj" \
	"$(INTDIR)\cp.obj" \
	"$(INTDIR)\cplist.obj" \
	"$(INTDIR)\cplog.obj" \
	"$(INTDIR)\leader.obj" \
	"$(INTDIR)\leadlist.obj" \
	"$(INTDIR)\nations.obj" \
	"$(INTDIR)\ob.obj" \
	"$(INTDIR)\obinit.obj" \
	"$(INTDIR)\obiter.obj" \
	"$(INTDIR)\obnames.obj" \
	"$(INTDIR)\poolfile.obj" \
	"$(INTDIR)\rank.obj" \
	"$(INTDIR)\sideinfo.obj" \
	"$(INTDIR)\sp.obj" \
	"$(INTDIR)\splist.obj" \
	"$(INTDIR)\unittype.obj" \
	"$(OUTDIR)\gamesupDB.lib" \
	"$(OUTDIR)\systemDB.lib"

"$(OUTDIR)\obDB.dll" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

!ELSEIF  "$(CFG)" == "ob - Win32 Editor Debug"

OUTDIR=.\..\..\exe
INTDIR=.\o\Editor\Debug
# Begin Custom Macros
OutDir=.\..\..\exe
# End Custom Macros

!IF "$(RECURSE)" == "0" 

ALL : "$(OUTDIR)\obEDDB.dll"

!ELSE 

ALL : "system - Win32 Editor Debug" "gamesup - Win32 Editor Debug" "$(OUTDIR)\obEDDB.dll"

!ENDIF 

!IF "$(RECURSE)" == "1" 
CLEAN :"gamesup - Win32 Editor DebugCLEAN" "system - Win32 Editor DebugCLEAN" 
!ELSE 
CLEAN :
!ENDIF 
	-@erase "$(INTDIR)\b_result.obj"
	-@erase "$(INTDIR)\cp.obj"
	-@erase "$(INTDIR)\cplist.obj"
	-@erase "$(INTDIR)\cplog.obj"
	-@erase "$(INTDIR)\leader.obj"
	-@erase "$(INTDIR)\leadlist.obj"
	-@erase "$(INTDIR)\nations.obj"
	-@erase "$(INTDIR)\ob.obj"
	-@erase "$(INTDIR)\obinit.obj"
	-@erase "$(INTDIR)\obiter.obj"
	-@erase "$(INTDIR)\obnames.obj"
	-@erase "$(INTDIR)\poolfile.obj"
	-@erase "$(INTDIR)\rank.obj"
	-@erase "$(INTDIR)\sideinfo.obj"
	-@erase "$(INTDIR)\sp.obj"
	-@erase "$(INTDIR)\splist.obj"
	-@erase "$(INTDIR)\unittype.obj"
	-@erase "$(INTDIR)\vc60.idb"
	-@erase "$(INTDIR)\vc60.pdb"
	-@erase "$(OUTDIR)\obEDDB.dll"
	-@erase "$(OUTDIR)\obEDDB.exp"
	-@erase "$(OUTDIR)\obEDDB.ilk"
	-@erase "$(OUTDIR)\obEDDB.lib"
	-@erase "$(OUTDIR)\obEDDB.pdb"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

"$(INTDIR)" :
    if not exist "$(INTDIR)/$(NULL)" mkdir "$(INTDIR)"

CPP_PROJ=/nologo /MDd /W3 /Gm /Gi /GR /GX /ZI /Od /I "..\h" /I "..\..\res" /I "..\system" /I "..\gamesup" /D "EXPORT_OB_DLL" /D "_USRDLL" /D "EDITOR" /D "_DEBUG" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /Fp"$(INTDIR)\ob.pch" /YX"stdinc.hpp" /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /GZ /c 
MTL_PROJ=/nologo /D "_DEBUG" /mktyplib203 /win32 
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\ob.bsc" 
BSC32_SBRS= \
	
LINK32=link.exe
LINK32_FLAGS=user32.lib /nologo /dll /incremental:yes /pdb:"$(OUTDIR)\obEDDB.pdb" /debug /machine:I386 /out:"$(OUTDIR)\obEDDB.dll" /implib:"$(OUTDIR)\obEDDB.lib" /pdbtype:sept 
LINK32_OBJS= \
	"$(INTDIR)\b_result.obj" \
	"$(INTDIR)\cp.obj" \
	"$(INTDIR)\cplist.obj" \
	"$(INTDIR)\cplog.obj" \
	"$(INTDIR)\leader.obj" \
	"$(INTDIR)\leadlist.obj" \
	"$(INTDIR)\nations.obj" \
	"$(INTDIR)\ob.obj" \
	"$(INTDIR)\obinit.obj" \
	"$(INTDIR)\obiter.obj" \
	"$(INTDIR)\obnames.obj" \
	"$(INTDIR)\poolfile.obj" \
	"$(INTDIR)\rank.obj" \
	"$(INTDIR)\sideinfo.obj" \
	"$(INTDIR)\sp.obj" \
	"$(INTDIR)\splist.obj" \
	"$(INTDIR)\unittype.obj" \
	"$(OUTDIR)\gamesupDB.lib" \
	"$(OUTDIR)\systemDB.lib"

"$(OUTDIR)\obEDDB.dll" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

!ELSEIF  "$(CFG)" == "ob - Win32 Editor Release"

OUTDIR=.\..\..\exe
INTDIR=.\o\Editor\Release
# Begin Custom Macros
OutDir=.\..\..\exe
# End Custom Macros

!IF "$(RECURSE)" == "0" 

ALL : "$(OUTDIR)\obED.dll"

!ELSE 

ALL : "system - Win32 Editor Release" "gamesup - Win32 Editor Release" "$(OUTDIR)\obED.dll"

!ENDIF 

!IF "$(RECURSE)" == "1" 
CLEAN :"gamesup - Win32 Editor ReleaseCLEAN" "system - Win32 Editor ReleaseCLEAN" 
!ELSE 
CLEAN :
!ENDIF 
	-@erase "$(INTDIR)\b_result.obj"
	-@erase "$(INTDIR)\cp.obj"
	-@erase "$(INTDIR)\cplist.obj"
	-@erase "$(INTDIR)\cplog.obj"
	-@erase "$(INTDIR)\leader.obj"
	-@erase "$(INTDIR)\leadlist.obj"
	-@erase "$(INTDIR)\nations.obj"
	-@erase "$(INTDIR)\ob.obj"
	-@erase "$(INTDIR)\obinit.obj"
	-@erase "$(INTDIR)\obiter.obj"
	-@erase "$(INTDIR)\obnames.obj"
	-@erase "$(INTDIR)\poolfile.obj"
	-@erase "$(INTDIR)\rank.obj"
	-@erase "$(INTDIR)\sideinfo.obj"
	-@erase "$(INTDIR)\sp.obj"
	-@erase "$(INTDIR)\splist.obj"
	-@erase "$(INTDIR)\unittype.obj"
	-@erase "$(INTDIR)\vc60.idb"
	-@erase "$(OUTDIR)\obED.dll"
	-@erase "$(OUTDIR)\obED.exp"
	-@erase "$(OUTDIR)\obED.lib"
	-@erase "$(OUTDIR)\obED.pdb"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

"$(INTDIR)" :
    if not exist "$(INTDIR)/$(NULL)" mkdir "$(INTDIR)"

CPP_PROJ=/nologo /MD /W3 /Gi /GR /GX /Zd /O2 /I "..\h" /I "..\..\res" /I "..\system" /I "..\gamesup" /D "EXPORT_OB_DLL" /D "NDEBUG" /D "_USRDLL" /D "EDITOR" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /Fp"$(INTDIR)\ob.pch" /YX"stdinc.hpp" /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /c 
MTL_PROJ=/nologo /D "NDEBUG" /mktyplib203 /win32 
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\ob.bsc" 
BSC32_SBRS= \
	
LINK32=link.exe
LINK32_FLAGS=user32.lib /nologo /dll /incremental:no /pdb:"$(OUTDIR)\obED.pdb" /debug /machine:I386 /out:"$(OUTDIR)\obED.dll" /implib:"$(OUTDIR)\obED.lib" 
LINK32_OBJS= \
	"$(INTDIR)\b_result.obj" \
	"$(INTDIR)\cp.obj" \
	"$(INTDIR)\cplist.obj" \
	"$(INTDIR)\cplog.obj" \
	"$(INTDIR)\leader.obj" \
	"$(INTDIR)\leadlist.obj" \
	"$(INTDIR)\nations.obj" \
	"$(INTDIR)\ob.obj" \
	"$(INTDIR)\obinit.obj" \
	"$(INTDIR)\obiter.obj" \
	"$(INTDIR)\obnames.obj" \
	"$(INTDIR)\poolfile.obj" \
	"$(INTDIR)\rank.obj" \
	"$(INTDIR)\sideinfo.obj" \
	"$(INTDIR)\sp.obj" \
	"$(INTDIR)\splist.obj" \
	"$(INTDIR)\unittype.obj" \
	"$(OUTDIR)\gamesup.lib" \
	"$(OUTDIR)\system.lib"

"$(OUTDIR)\obED.dll" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

!ENDIF 

.c{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.c{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<


!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("ob.dep")
!INCLUDE "ob.dep"
!ELSE 
!MESSAGE Warning: cannot find "ob.dep"
!ENDIF 
!ENDIF 


!IF "$(CFG)" == "ob - Win32 Release" || "$(CFG)" == "ob - Win32 Debug" || "$(CFG)" == "ob - Win32 Editor Debug" || "$(CFG)" == "ob - Win32 Editor Release"
SOURCE=.\b_result.cpp

"$(INTDIR)\b_result.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\cp.cpp

"$(INTDIR)\cp.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\cplist.cpp

"$(INTDIR)\cplist.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\cplog.cpp

"$(INTDIR)\cplog.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\leader.cpp

"$(INTDIR)\leader.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\leadlist.cpp

"$(INTDIR)\leadlist.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\nations.cpp

"$(INTDIR)\nations.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\ob.cpp

"$(INTDIR)\ob.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\obinit.cpp

"$(INTDIR)\obinit.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\obiter.cpp

"$(INTDIR)\obiter.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\obnames.cpp

"$(INTDIR)\obnames.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\poolfile.cpp

"$(INTDIR)\poolfile.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\rank.cpp

"$(INTDIR)\rank.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\sideinfo.cpp

"$(INTDIR)\sideinfo.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\sp.cpp

"$(INTDIR)\sp.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\splist.cpp

"$(INTDIR)\splist.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\unittype.cpp

"$(INTDIR)\unittype.obj" : $(SOURCE) "$(INTDIR)"


!IF  "$(CFG)" == "ob - Win32 Release"

"gamesup - Win32 Release" : 
   cd "\src\sf\wargamer\SRC\GAMESUP"
   $(MAKE) /$(MAKEFLAGS) /F .\gamesup.mak CFG="gamesup - Win32 Release" 
   cd "..\OB"

"gamesup - Win32 ReleaseCLEAN" : 
   cd "\src\sf\wargamer\SRC\GAMESUP"
   $(MAKE) /$(MAKEFLAGS) /F .\gamesup.mak CFG="gamesup - Win32 Release" RECURSE=1 CLEAN 
   cd "..\OB"

!ELSEIF  "$(CFG)" == "ob - Win32 Debug"

"gamesup - Win32 Debug" : 
   cd "\src\sf\wargamer\SRC\GAMESUP"
   $(MAKE) /$(MAKEFLAGS) /F .\gamesup.mak CFG="gamesup - Win32 Debug" 
   cd "..\OB"

"gamesup - Win32 DebugCLEAN" : 
   cd "\src\sf\wargamer\SRC\GAMESUP"
   $(MAKE) /$(MAKEFLAGS) /F .\gamesup.mak CFG="gamesup - Win32 Debug" RECURSE=1 CLEAN 
   cd "..\OB"

!ELSEIF  "$(CFG)" == "ob - Win32 Editor Debug"

"gamesup - Win32 Editor Debug" : 
   cd "\src\sf\wargamer\SRC\GAMESUP"
   $(MAKE) /$(MAKEFLAGS) /F .\gamesup.mak CFG="gamesup - Win32 Editor Debug" 
   cd "..\OB"

"gamesup - Win32 Editor DebugCLEAN" : 
   cd "\src\sf\wargamer\SRC\GAMESUP"
   $(MAKE) /$(MAKEFLAGS) /F .\gamesup.mak CFG="gamesup - Win32 Editor Debug" RECURSE=1 CLEAN 
   cd "..\OB"

!ELSEIF  "$(CFG)" == "ob - Win32 Editor Release"

"gamesup - Win32 Editor Release" : 
   cd "\src\sf\wargamer\SRC\GAMESUP"
   $(MAKE) /$(MAKEFLAGS) /F .\gamesup.mak CFG="gamesup - Win32 Editor Release" 
   cd "..\OB"

"gamesup - Win32 Editor ReleaseCLEAN" : 
   cd "\src\sf\wargamer\SRC\GAMESUP"
   $(MAKE) /$(MAKEFLAGS) /F .\gamesup.mak CFG="gamesup - Win32 Editor Release" RECURSE=1 CLEAN 
   cd "..\OB"

!ENDIF 

!IF  "$(CFG)" == "ob - Win32 Release"

"system - Win32 Release" : 
   cd "\src\sf\wargamer\SRC\SYSTEM"
   $(MAKE) /$(MAKEFLAGS) /F .\system.mak CFG="system - Win32 Release" 
   cd "..\OB"

"system - Win32 ReleaseCLEAN" : 
   cd "\src\sf\wargamer\SRC\SYSTEM"
   $(MAKE) /$(MAKEFLAGS) /F .\system.mak CFG="system - Win32 Release" RECURSE=1 CLEAN 
   cd "..\OB"

!ELSEIF  "$(CFG)" == "ob - Win32 Debug"

"system - Win32 Debug" : 
   cd "\src\sf\wargamer\SRC\SYSTEM"
   $(MAKE) /$(MAKEFLAGS) /F .\system.mak CFG="system - Win32 Debug" 
   cd "..\OB"

"system - Win32 DebugCLEAN" : 
   cd "\src\sf\wargamer\SRC\SYSTEM"
   $(MAKE) /$(MAKEFLAGS) /F .\system.mak CFG="system - Win32 Debug" RECURSE=1 CLEAN 
   cd "..\OB"

!ELSEIF  "$(CFG)" == "ob - Win32 Editor Debug"

"system - Win32 Editor Debug" : 
   cd "\src\sf\wargamer\SRC\SYSTEM"
   $(MAKE) /$(MAKEFLAGS) /F .\system.mak CFG="system - Win32 Editor Debug" 
   cd "..\OB"

"system - Win32 Editor DebugCLEAN" : 
   cd "\src\sf\wargamer\SRC\SYSTEM"
   $(MAKE) /$(MAKEFLAGS) /F .\system.mak CFG="system - Win32 Editor Debug" RECURSE=1 CLEAN 
   cd "..\OB"

!ELSEIF  "$(CFG)" == "ob - Win32 Editor Release"

"system - Win32 Editor Release" : 
   cd "\src\sf\wargamer\SRC\SYSTEM"
   $(MAKE) /$(MAKEFLAGS) /F .\system.mak CFG="system - Win32 Editor Release" 
   cd "..\OB"

"system - Win32 Editor ReleaseCLEAN" : 
   cd "\src\sf\wargamer\SRC\SYSTEM"
   $(MAKE) /$(MAKEFLAGS) /F .\system.mak CFG="system - Win32 Editor Release" RECURSE=1 CLEAN 
   cd "..\OB"

!ENDIF 


!ENDIF 

