/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *  Generic Order of Battle Interface
 *
 * Campaign and Battle build on this for Armies and BattleArmy
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "ob.hpp"
#include "scenario.hpp"
#include "wg_rand.hpp"
#include "control.hpp"
#include "options.hpp"
#include "obnames.hpp"
#include "resstr.hpp"
#include <algorithm>

BaseOrderBattle::~BaseOrderBattle() {}

/*===============================================================
 * Order of Battle Functions
 */

OrderBattle::OrderBattle() :
   d_nSides(0),
   d_commands(),
   d_leaders(),
   d_sps(),
   d_topCommand(NoGenericCP),
   d_nations(0),
   d_nationsList(),
   d_obChanged(0),
   d_unitTypes(),
   d_leadersCreated(0)
{
   OB_Names::create();

   Boolean result = d_unitTypes.init(scenario->getUnitTypeFileName());
   ASSERT(result);
}

OrderBattle::~OrderBattle()
{
   delete[] d_leadersCreated;
   delete[] d_nations;

   OB_Names::destroy();
}


/*
 * garbage collection
 */

void OrderBattle::cleanup()
{
    Writer lock(this);
    d_commands.cleanup();
    d_leaders.cleanup();
    d_sps.cleanup();
}

/*
 * Clear out the presidents, SHQ, etc
 */

void OrderBattle::reset()
{
    Writer lock(this);
    d_topCommand = NoGenericCP;
    for(Side i = 0; i < d_nSides; ++i)
    {
        d_nations[i].setSupremeLeader(NoGLeader);
        d_nations[i].setPresident(NoGenericCP);
        d_leadersCreated[i] = 0;
    }
}

// #ifdef CUSTOMIZE
void OrderBattle::deleteAll()
{
    Writer lock(this);
    d_sps.reset();
    d_commands.reset();
    d_leaders.reset();
}

// #endif

void OrderBattle::createEmptyOB(int nSides)
{
    Writer lock(this);
    reset();
    deleteAll();

    ASSERT(nSides > 0);
    d_nSides = nSides;
    d_nations = new OBSideInfo[d_nSides];
    ASSERT(d_nations != 0);

    d_leadersCreated = new UWORD[d_nSides];
    ASSERT(d_leadersCreated);

    RefGenericCP top = createCommand();
    d_topCommand = top;
    RefGenericCP prev = NoGenericCP;
    for(int i = 0; i < d_nSides; ++i)
    {
        RefGenericCP president = createCommand();
        d_nations[i].setPresident(president);

        if(prev == NoGenericCP)
            top->setChild(president);
        else
            prev->setSister(president);

        prev = president;

        d_leadersCreated[i] = 0;
    }
}

/*-------------------------------------------------------------
 * File Interface
 */

const UWORD OrderBattle::fileVersion = 0x0002;

Boolean OrderBattle::readData(FileReader& f)
{
    Writer lock(this);
    d_nationsList.read(f);
    d_leaders.readData(f);
    d_sps.readData(f, *this);
    d_commands.readData(f, *this);

    /*
     * Bodge for Editor mishap
     */

    {
#if 0   // Changed for RefCP stuff
      GenericCP* cp = getCommand(0);
      if(cp->getChild() == 2)
      {
         setChild(0,1);
         setSister(1,2);
      }
#else
      RefGenericCP cp = d_commands[0];
      if(getIndex(cp->getChild()) == 2)
      {
         d_commands[0]->setChild(d_commands[1]);
         d_commands[1]->setSister(d_commands[2]);
      }
#endif
    }

    {
        FileChunkReader fr(f, armyChunkName);
        if(!fr.isOK())
            return False;

        if(f.isAscii())
        {
            return False;
        }
        else
        {
            UWORD version;
            f.getUWord(version);
            ASSERT(version <= fileVersion);

            f.getUByte(d_nSides);
            ASSERT(d_nSides > 0);
            ASSERT(!d_nations);
            d_nations = new OBSideInfo[d_nSides];
            ASSERT(d_nations != 0);

            ASSERT(!d_leadersCreated);
            d_leadersCreated = new UWORD[d_nSides];
            ASSERT(d_leadersCreated);

            if(version >= 0x0001)
            {
                CPIndex top;
                f >> top;
                d_topCommand = d_commands[top]; // makeCPRef(top);
            }
            else
                d_topCommand = d_commands[0];

            // addCPReference(d_topCommand);
        }

        for(int i = 0; i < d_nSides; i++)
        {
          d_nations[i].readData(f, *this);
          d_leadersCreated[i] = 0;
        }
    }

    /*
     * Fix up data after loading
     *
     * 1. Assign Leaders
     */

    {
        for(CPIndex i = 0; i != d_commands.entries(); ++i)
        {
            if(d_commands.isUsed(i))
            {
                RefGenericCP cp = d_commands[i];
                RefGLeader iLeader = cp->leader();
                if(iLeader != NoGLeader)
                {
                    ASSERT(iLeader->command() == NoGenericCP);

                    iLeader->command(cp);
                }

            }
        }
    }

#if 0  // leaders may not have a cp
#ifdef DEBUG
    // Check that all leaders have a CP
    {
        for(LeaderIndex i = 0; i < d_leaders.entries(); ++i)
        {
            if(d_leaders.isUsed(i))
            {
                ConstRefGLeader l = d_leaders[i];
                ASSERT(l->command() != NoGenericCP);
            }
        }
    }
#endif
#endif
    // initStartingMorale();
    return f.isOK();
}

Boolean OrderBattle::writeData(FileWriter& f) const
{
   Reader lock(this);
#ifdef EDITOR
   OB_Names::writeFile();
#endif
   d_nationsList.write(f);
   d_leaders.writeData(f);
   d_sps.writeData(f, *this);
   d_commands.writeData(f, *this);
   {
      FileChunkWriter fc(f, armyChunkName);

      if( f.isAscii() )
      {
         f.printf(fmt_sddn, entriesToken, (int) d_nSides, static_cast<int>(d_topCommand->getSelf()));
      }
      else
      {
         f.putUWord(fileVersion);
         f.putUByte(d_nSides);
         f.putUWord(d_topCommand->getSelf());
      }

      for( int i = 0; i < d_nSides; i++ )
         d_nations[i].writeData(f, *this);
   }

   return f.isOK();
}


RefGenericCP OrderBattle::createCommand()
{
    Writer lock(this);
#if 0   // Pre-Reference counting version
    CPIndex cpi = d_commands.add();
    addCPReference(cpi);
    noteOBChanged();
    // return cpi;
    return RefGenericCP(&d_commands[cpi]);
#else
    CPIndex cpi = d_commands.add();
    noteOBChanged();
    return d_commands[cpi];
    // cp->addRef();
#endif
}

void OrderBattle::deleteCommand(const RefGenericCP& cpi)
{
    Writer lock(this);
#ifdef DEBUG
    // const GenericCP* cp = &d_commands[cpi];
    ConstRefGenericCP cp = cpi;
    ASSERT(cp->getChild() == NoGenericCP);
    ASSERT(cp->getSister() == NoGenericCP);
    ASSERT(cp->getParent() == NoGenericCP);
    ASSERT(cp->leader() == NoGLeader);
    ASSERT(cp->getSPEntry() == NoStrengthPoint);
    ASSERT(!cp->isDead());
#endif

    noteOBChanged();
    // d_commands[cpi].isDead(true);
    cpi->isDead(true);
    // return delCPReference(cpi);
    // return cpi->refCount() == 0;
}


/*---------------------------------------------------------------
 * Order of Battle : Nationality related functions
 */

Boolean OrderBattle::canNationControlNation(Nationality nControl, Nationality nSubordinate)
{
    if(!CampaignOptions::get(OPT_NationRules))
        return True;

    /*
     * Eliminate Neutral cases first
     */

    if(nSubordinate == NATION_Neutral)
        return True;

    if(nControl == NATION_Neutral)
        return False;

    /*
     * Nations can always control same nation
     */

    if(nControl == nSubordinate)
        return True;

    /*
     * Major can control same, Minor or Generic
     * Minor can control same or Generic
     */

    NationType tControl = scenario->getNationType(nControl);
    NationType tSubord = scenario->getNationType(nSubordinate);

    return (tControl < tSubord);
}


/*
 * Can a unit be of a given nationality
 *
 * This works as:
 *      If unit has no children, then it can be any nationality
 *      otherwise, it depends on the children
 */

Boolean OrderBattle::canNationControlThisUnit(const ConstRefGenericCP& cpi, Nationality nLeader) const
{
    Reader lock(this);
    // const GenericCP* cp = getCommand(cpi);
    ConstRefGenericCP cp = cpi;

    RefGenericCP iChild = cp->getChild();
    // CPIndex iChild = cp->getChild();

    if(iChild == NoGenericCP)
        return True;
    else
    {
        do
        {
            // ConstRefGenericCP cpChild = getCommand(iChild);

            /*
             * Must be able to control CP and Leader of child
             */

            if(canNationControlNation(nLeader, iChild->getNation()))
            {
                RefGLeader iLeader = iChild->leader();
                if(iLeader != NoGLeader)
                {
                    // ConstRefGLeader subLeader = d_leaders[iLeader];
                    if(canNationControlNation(nLeader, iLeader->getNation()))
                        return True;
                }
                else
                    return True;
            }
            iChild = iChild->getSister();
        } while(iChild != NoGenericCP);
    }

    return False;
}


/*
 * Set sides supreme leader
 */

void OrderBattle::setSupremeLeader(Side side, const RefGLeader& iLeader)
{
    Writer lock(this);

  ASSERT(iLeader != NoGLeader);
  ASSERT(side != SIDE_Neutral);

  noteOBChanged();

  RefGLeader oldSHQ = getNation(side)->getSupremeLeader();
  if(oldSHQ != NoGLeader)
  {
     oldSHQ->isSupremeLeader(False);
  }

  getNation(side)->setSupremeLeader(iLeader);
//  setSupremeLeader(side, iLeader);

  iLeader->isSupremeLeader(True);

#ifdef DEBUG

  /*
    *  Check to make sure there is only one supreme leader
    */

  G_UnitIter iter(this, getPresident(side));

  int howMany = 0;
  while(iter.next())
  {
     ConstRefGenericCP cp = iter.current(); // command(iter.current());

     if(cp->isLower(Rank_President))       // skip past president
     {
        // ConstRefGLeader leader = getUnitLeader(iter.current());
        ConstRefGLeader leader = iter.current()->leader();

        if((leader != NoGLeader) && leader->isSupremeLeader())
          howMany++;
     }
  }

  ASSERT(howMany == 1);

#endif
}

/*
 * Pick a new Supreme Commander
 */

void OrderBattle::chooseNewSHQ(Side side)
{
    RefGLeader currentSHQ = getSupremeLeader(side);

    // Scan Top Level of OB for best commander

    RefGLeader bestLeader = NoGLeader;
    RankEnum bestRank = Rank_Division;

    G_UnitIter iter(this, getPresident(side));
    while (iter.next())
    {
        RefGenericCP cp = iter.current();
        if (cp->isLower(Rank_President))
        {
            RefGLeader leader = cp->leader();

            // Skip if there is no leader or it is the current SHQ

            if( (leader == NoGLeader) || (leader == currentSHQ))
                continue;

            bool isBetter = false;

            if(bestLeader == NoGLeader)
                isBetter = true;
            else if(cp->isHigher(bestRank))
                isBetter = true;
            else if(cp->sameRank(bestRank))
            {
                // Check attributes
                isBetter = (leader->getStaff() > bestLeader->getStaff());
            }

            if (isBetter)
            {
                bestLeader = leader;
                bestRank = cp->getRankEnum();
            }

        }
    }

    ASSERT(bestLeader != NoGLeader);

    setSupremeLeader(side, bestLeader);
}


/*---------------------------------------------------------------
 * Order of Battle: CommandPosition related functions
 */


// get units top most parent excluding president

ConstRefGenericCP OrderBattle::getTopParent(const ConstRefGenericCP& cp)
{
  ASSERT(cp != NoGenericCP);
  ConstRefGenericCP cpi = cp;

  while(cpi->getParent()->getRankEnum() != Rank_President)
  {
        cpi = cpi->getParent();
        ASSERT(cpi != NoGenericCP);
  }
  return cpi;
}

RefGenericCP OrderBattle::getTopParent(const RefGenericCP& cp)
{
  ASSERT(cp != NoGenericCP);
  RefGenericCP cpi = cp;
  while(cpi->getParent()->getRankEnum() != Rank_President)
  {
        cpi = cpi->getParent();
        ASSERT(cpi != NoGenericCP);
  }
  return cpi;
}



ConstRefGenericCP OrderBattle::getFirstUnit(Side s) const
{
    Reader lock(this);
    ConstRefGenericCP cpi = getPresident(s);
    if(cpi != NoGenericCP)
        cpi = cpi->getChild();
    return cpi;
}

RefGenericCP OrderBattle::getFirstUnit(Side s)
{
    Reader lock(this);
    RefGenericCP cpi = getPresident(s);
    if(cpi != NoGenericCP)
        cpi = cpi->getChild();
    return cpi;
}


/*
 * Put a unit's name into a buffer
 *
 * Naughty code, assumes buffer is long enough!
 *
 * For now, it will just be "1st Army", "2nd Corps", etc...
 */

const char* OrderBattle::getUnitName(const ConstRefGenericCP& cp, char* buffer)
{
    if(buffer != 0)
    {
        strcpy(buffer, cp->getName());
        return buffer;
    }
    else
        return cp->getName();
}



void OrderBattle::makeUnitName(const RefGenericCP& cp, const Rank& r)
{
    noteOBChanged();

    // ASSERT(r < Rank_HowMany);

    // GenericCP* cp = getCommand(i);

    char nameBuffer[80];

    if(r.getRankEnum() == Rank_God)
    {
        strcpy(nameBuffer, "God");
    }
    else if(r.getRankEnum() == Rank_President)
    {
        wsprintf(nameBuffer, InGameText::get(IDS_SideArmy),    // "%s Army"
         scenario->getSideName(cp->side()));
    }
    else
    {

#if 0
        Boolean attached;
        CPIndex pi = cp->getParent();
        if(pi != NoCPIndex)
        {
            attached = d_commands[pi]->isLower(Rank_President);
        }
        else
            attached = False;
#endif

        RefGenericCP pi = cp->getParent();
        Boolean attached =
            (pi != NoGenericCP) && pi->isLower(Rank_President);

        // Boolean attached = parent->getRank.isLower(Rank_President);
        // Boolean attached = (cp->getParent() != NoCPIndex);

        // Nation* nation = getNation(cp->getSide());
        OBSideInfo* nation = getNation(cp->side());

        int unitNumber = nation->nextRankCount(r);
        wsprintf(nameBuffer, "%d%s %s",
            unitNumber,
            getNths(unitNumber),
            getRankName(r, attached));
    }

    cp->setName(copyString(nameBuffer));
}

#if !defined(EDITOR)

Boolean OrderBattle::isPlayerUnit(const ConstRefGenericCP& cp) const
{
  ASSERT(cp != NoGenericCP);

  return GamePlayerControl::canControl(cp->side());
}

#endif

/*
 *  Get combined subordination value of Commands direct subordinates
 */

UWORD OrderBattle::getCommandSubordination(const ConstRefGenericCP& cpi) const
{
    ASSERT(cpi != NoGenericCP);

    UWORD value = 0;

    ConstRefGenericCP iChild = cpi->getChild();

    if(iChild != NoGenericCP)
    {
        G_ConstUnitIter iter(this, iChild);

        while(iter.sister())
        {
            ConstRefGLeader general = getUnitLeader(iter.current());
            value += general->getSubordination();
        }
    }


    return value;
}

UWORD OrderBattle::getCommandSubordination(const ConstRefGLeader& leader) const
{
    ConstRefGenericCP cp = leader->getCommand();

    if(cp == NoGenericCP)
        return 0;
    else
        return getCommandSubordination(cp);
}

// combines the above two functions
Boolean OrderBattle::findSpecialType(const RefGenericCP& cpi, RefGenericCP& typeCP, ISP& typeSP, SpecialUnitType::value type)
{
    G_SPIter iter(this, cpi);
    while(++iter)
    {
        ConstISP sp = iter.current();
        // if(!sp->isStraggling())
        {
            const UnitTypeItem& uti = getUnitType(sp->getUnitType());

            if(uti.getSpecialType() == type)
            {
                typeSP = iter.currentISP();
                typeCP = iter.currentICP();
                return True;
            }
        }
    }

    typeCP = NoGenericCP;
    typeSP = NoStrengthPoint;

    return False;
}

// useful hasType functions
Boolean OrderBattle::hasBasicType(const ConstRefGenericCP& cpi, BasicUnitType::value t) const
{
    ASSERT(t < BasicUnitType::HowMany);

    G_ConstSPIter iter(this, cpi);
    while(++iter)
    {
        const UnitTypeItem& uti = getUnitType(iter.current()->getUnitType());

        if(uti.getBasicType() == t)
        {
          return True;
        }
    }

    return False;
}

Boolean OrderBattle::hasSpecialType(const ConstRefGenericCP& cpi, SpecialUnitType::value t) const
{
    // ASSERT(t < BasicUnitType::HowMany);
    ASSERT(t < SpecialUnitType::HowMany);

    G_ConstSPIter iter(this, cpi);
    while(++iter)
    {
        const UnitTypeItem& uti = getUnitType(iter.current()->getUnitType());

        if(uti.getSpecialType() == t)
        {
          return True;
        }
    }

    return False;
}

/*
 *  if except is False returns sp count of given type
 *  if except is True  returns sp count of all except given type
 */

SPCount OrderBattle::getNType(const ConstRefGenericCP& cpi, BasicUnitType::value type, Boolean except) const
{
    SPCount nType = 0;

    G_ConstSPIter iter(this, cpi);
    while(++iter)
    {
        ConstISP sp = iter.current();
        // if(!sp->isStraggling())
        {
            const UnitTypeItem& uti = getUnitType(sp->getUnitType());

            if(except)
            {
              if(uti.getBasicType() != type)
                 nType++;
            }
            else
            {
              if(uti.getBasicType() == type)
                 nType++;
            }
        }
    }

    return nType;
}

/*
 * Return Average base morale of a unit
 */

Attribute OrderBattle::getBaseMorale(const ConstRefGenericCP& cp) const
{
  ASSERT(cp != NoGenericCP);

  // ConstRefGenericCP cp = getCommand(cpi);

  ULONG baseMoraleTotal = 0;
  SPCount nSP = 0;

  G_ConstSPIter iter (this, cp);

  while(++iter)
  {
     ConstISP sp = iter.current();

     const UnitTypeItem& uti = getUnitType(sp->getUnitType());
     baseMoraleTotal += uti.getBaseMorale();
     nSP++;
  }

  if(nSP > 0)
     return (Attribute)(baseMoraleTotal/nSP);
  else
     return 0;

}

/*
 *  returns bombardment value. each unittype has a bombardment value.
 *  actual unittype value is 1/10th of this value.
 *
 */


BombValue OrderBattle::getBombardmentValue(const ConstRefGenericCP& hunit) const
{

  ASSERT(hunit != NoGenericCP);

  BombValue value = 0;

  G_ConstSPIter iter(this, hunit);
  while(++iter)
  {
     ConstISP sp = iter.current();

     const UnitTypeItem& uti = getUnitType(sp->getUnitType());
     value += uti.getBombardmentValue();
  }

  // round out to nearest int
  return static_cast<BombValue>((value + 4)/10);

}

/*---------------------------------------------------------------
 * Order Of Battle: Leader related functions
 *
 * create leader
 * Note: the iPoolLeader member indicates whether the new leader is created
 *       because a new Unit was built (a 'Pool Leader'), or because of promotion of
 *       somebody in the order of battle.
 */

RefGLeader OrderBattle::createLeader(Side side, Nationality n, RankEnum defRank, Boolean noModify)
{
    Writer lock(this);
    ASSERT(n < scenario->getNumNations());

    noteOBChanged();
    ASSERT(side != SIDE_Neutral);
    ASSERT(side < d_nSides);

    LeaderIndex iGen = d_leaders.add();
    RefGLeader gen = d_leaders[iGen];

    const StringTable& leaderNames = scenario->getSideLeaderNameTable(side);
    char* name = copyString(leaderNames.getValue(CRandom::get(leaderNames.entries())));
    ASSERT(name);
    gen->setName(name);
    gen->side(side);

    gen->setNation(n);



    int modifier = 0;

    if(!noModify)
    {
      d_nationsList.incLeadersCreated(n);
      // modifier equals #leaders created / table-value
      // Note: Value cannot be greater than 10
      const Table1D<UBYTE>& table = scenario->getLeaderDecreaseAbilityTable();
      const int mult = table.getValue((scenario->getNationType(n) == MajorNation) ? n : 5);
      if(mult > 0)
      {
        modifier = minimum<int>(10, d_nationsList.nLeadersCreated(n) / mult);
      }
      else
        modifier = minimum<int>(10, d_nationsList.nLeadersCreated(n));
    }

    gen->setDefault(modifier, d_nationsList.nLeadersCreated(n));
    return gen;
}

void OrderBattle::deleteLeader(const RefGLeader& iLeader)
{
    ASSERT(iLeader->command() == NoGenericCP);
    // Reference counting takes care of deletion

   if (iLeader->isSupremeLeader())
   {
      chooseNewSHQ(iLeader->side());
   }

}

/*
 * Set General to go with unit
 */

void OrderBattle::attachLeader(const RefGenericCP& cp, const RefGLeader& gen)
{
    Writer lock(this);
    ASSERT(cp != NoGenericCP);
    ASSERT(gen != NoGLeader);
//  ASSERT(cpi != NoCPIndex);
//  ASSERT(iLeader != NoLeaderIndex);

//  GenericCP* cp = command(cpi);
//  Leader* gen = leader(iLeader);


    ASSERT(cp->leader() == NoGLeader);
    ASSERT(gen->command() == NoGenericCP);

#ifdef DEBUG
    Boolean canAssign = canLeaderTakeControl(gen, cp);
    ASSERT(canAssign);
#endif

    // cp->leader(iLeader);

    // assignLeader(cp->r_leader(), iLeader);
    cp->leader(gen);

    if(cp->getSPEntry() == NoStrengthPoint)
        cp->nation(gen->getNation());
    gen->command(cp);
    // assignCP(gen->r_command(), cpi);
    noteOBChanged();
}

/*
 * Detach Leader from CommandPosition
 * Leaves him in Limbo
 */

void OrderBattle::detachLeader(const RefGenericCP& cp)
{
    Writer lock(this);
    noteOBChanged();
    ASSERT(cp != NoGenericCP);
    // GenericCP* cp = command(cpi);

    ASSERT(cp->leader() != NoGLeader);
    RefGLeader general = cp->leader();  // d_leaders[cp->leader()];
    general->command(NoGenericCP);

    cp->leader(NoGLeader);
    // assignLeader(cp, NoLeaderIndex);
}

void OrderBattle::detachLeader(const RefGLeader& general)
{
    Writer lock(this);
    noteOBChanged();

    // ASSERT(iLeader != NoLeaderIndex);
    // Leader* general = leader(iLeader);
    ASSERT(general != NoGLeader);
    RefGenericCP cpi = general->command();
    ASSERT(cpi != NoGenericCP);
    RefGenericCP cp = cpi;

    ASSERT(cp->leader() == general);

    // general->setCommand(NoCPIndex);
    // assignCP(general->r_command(), NoCPIndex);

    general->command(NoGenericCP);
    cp->leader(NoGLeader);
    // May need to adjust references...

    // cp->leader(NoLeaderIndex);
    // assignLeader(cp->r_leader(), NoLeaderIndex);
}


void OrderBattle::killAndReplaceLeader(const RefGLeader& iLeader)
{
    Writer lock(this);
    ASSERT(iLeader != NoGLeader);
    RefGenericCP cpi = iLeader->getCommand();

    if(cpi != NoGenericCP)
    {
        detachLeader(iLeader);

        autoPromote(cpi);

      if(iLeader->isSupremeLeader())
         setSupremeLeader(cpi->side(), cpi->leader());
    }
}


void OrderBattle::autoPromote(const RefGenericCP& cp)
{
    Writer lock(this);

// TODO: take nationality rules into effect
    ASSERT(cp != NoGenericCP);
    if(cp != NoGenericCP)
    {
      RefGLeader iLeader = cp->leader();
      ASSERT(iLeader == NoGLeader);

      RefGenericCP iChild = cp->getChild();

      Nationality n = cp->nation();

      if(iChild != NoGenericCP)
      {
          ASSERT(iChild->leader() != NoGLeader);
          RefGLeader childLeader = iChild->leader();
          RefGenericCP iSister = iChild->getSister();
          while(iSister != NoGenericCP)
          {
             ASSERT(iSister->leader() != NoGLeader);
             RefGLeader sisterLeader = iSister->leader();

             if(whoShouldCommand(childLeader, sisterLeader) == childLeader)
//           if(childLeader->getRankRating() > sisterLeader->getRankRating())
                iSister = iSister->getSister();
             else
             {
                childLeader = sisterLeader;
                iChild = iSister;
                iSister = iSister->getSister();
             }
          }

          detachLeader(iChild);
          attachLeader(cp, childLeader);
          autoPromote(iChild);
      }
      else
      {
         RefGLeader iLeader = createLeader(cp->side(), scenario->getDefaultNation(cp->side()),
             Rank_Division, False);

         setLeaderNation(iLeader, n);
         attachLeader(cp, iLeader);
      }
    }
}

void OrderBattle::setLeaderNation(const RefGLeader& l, Nationality n)
{
    noteOBChanged();
  ASSERT(l != NoGLeader);
  l->nation(n);
}

Nationality OrderBattle::getLeaderNation(const ConstRefGLeader& l) const
{
  ASSERT(l != NoGLeader);
  return l->nation();
}

/*
 * returns which leader of the 2 should take command
 *
 */

ConstRefGLeader OrderBattle::whoShouldCommand(const ConstRefGLeader& leader1, const ConstRefGLeader& leader2) const
{
  /*
    * if leader1 is supreme leader or of greater rank return leader1
    * Note: less than is actually greater than
    */

  if(leader1->isSupremeLeader() ||
      leader1->getRankLevel().getRankEnum() < leader2->getRankLevel().getRankEnum())
  {
     return leader1;
  }

  /*
    * if leader2 is supreme leader or of greater rank return leader2
    * Note: less than is actually greater than
    */

  if(leader2->isSupremeLeader() ||
      leader2->getRankLevel().getRankEnum() < leader1->getRankLevel().getRankEnum())
  {
     return leader2;
  }

  /*
    * If one or the other is a marshall
    */

  if(leader1->isMarshall() && !leader2->isMarshall())
     return leader1;

  if(leader2->isMarshall() && !leader1->isMarshall())
     return leader2;


  /*
    * If we still haven;t decided then
    * The leader with the higest initiative + subordination is it
    * If that is tied then go with the first one
    */

  Attribute init1 = leader1->getInitiative();
  Attribute init2 = leader2->getInitiative();
  Attribute sub1 = leader1->getSubordination();
  Attribute sub2 = leader2->getSubordination();

  return ( (sub1 + init1) >= (sub2 + init2) )  ? leader1 : leader2;

}





RefGLeader OrderBattle::getSupremeLeader(Side side) const
{
    ASSERT(side != SIDE_Neutral);

    RefGLeader iLeader = getNation(side)->getSupremeLeader();
    // ASSERT(iLeader != NoGLeader);

    return iLeader;
}

/*
 * Can leader take control?
 */

Boolean OrderBattle::canLeaderTakeControl(const ConstRefGLeader& general, const ConstRefGenericCP& cp) const
{
    Reader lock(this);
    ASSERT(general != NoGLeader);
  ASSERT(cp != NoGenericCP);

  /*
    * If Nationality-Rules option is off return True
    */

  if(!CampaignOptions::get(OPT_NationRules))
     return True;

  // ConstRefGLeader general = leader(iLeader);
  Nationality nLeader = general->getNation();
  ASSERT(nLeader != NATION_Neutral);

  // return canNationControlThisUnit(cpi, nLeader);

  // ConstRefGenericCP cp = command(cpi);
  Nationality nCPI = cp->nation();

  /*
    * If unit has SPs then must be able to control them
    * Otherwise, just check the next level down
    */

    if( (cp->getSPEntry() != NoStrengthPoint) || (cp->getChild() == NoGenericCP))
        return canNationControlNation(nLeader, nCPI);
    else
    {
        ConstRefGenericCP iChild = cp->getChild();
        do
        {
            // ConstRefGenericCP cp = makeCPRef(iChild);

            /*
             * Must be able to control CP's nationality AND leader's nationality
             * e.g. might have a minor division commanded by major leader.
             */

            if(canNationControlNation(nLeader, iChild->nation()))
            {
                ConstRefGLeader subLeader = iChild->leader();
                if(subLeader != NoGLeader)
                {
                    // ConstRefGLeader subLeader = d_leaders[iLeader];
                    if(canNationControlNation(nLeader, subLeader->getNation()))
                        return True;
                }
                else
                    return True;
            }

            iChild = iChild->getSister();
        } while(iChild != NoGenericCP);

        return False;
    }
}

// Unchecked update --------------------------------------------
// Set unit specialist flags. Includes:
// 1. Unit-Type flags
// 2. Specialist leader flags
void OrderBattle::setUnitSpecialistFlags(const RefGenericCP& cpi)
{
    Reader lock(this);
  ASSERT(cpi != NoGenericCP);
  cpi->clearFlags();

  int nInf = 0;
  int nCav = 0;
  int nArt = 0;
  Boolean allGuard = True;
//  Boolean allOldGuard = True;

  /*
    * If this is a XX, then check SP to get Type
    * otherwise use immediate subordinates
    */

  RefGenericCP cpChild = cpi->getChild();
  if(cpChild == NoGenericCP)
  {
     G_StrengthPointIter iter(this, cpi);
     while(++iter)
     {
        const StrengthPoint* sp = iter.current();
        const UnitTypeItem& uti = getUnitType(sp->getUnitType());

        // count types
        if(uti.getBasicType() == BasicUnitType::Artillery)
          nArt++;
        else if(uti.getBasicType() == BasicUnitType::Cavalry)
          nCav++;
        else
          nInf++;

        if(allGuard && !uti.isGuard() && !uti.isOldGuard())
          allGuard = False;
     }

     /*
      * It is Infantry if XX has any inf SP's than Art Sp's
      * It is Cavalry if XX has more cav SP's than Art Sp's
      */

//   ASSERT(nInf == 0 || nCav == 0);
     if(nInf != 0)
     {
        if(nInf >= nArt)
          cpi->makeInfantryType(True);
        else
          cpi->makeArtilleryType(True);
     }
     else
     {
        if(nCav >= nArt)
          cpi->makeCavalryType(True);
        else
          cpi->makeArtilleryType(True);
     }

  }

  else
  {
     G_UnitIter uiter(this, cpChild);
     while(uiter.sister())
     {
        RefGenericCP cpi = uiter.current();

        if(cpi->isInfantry())
          nInf++;
        if(cpi->isArtillery())
          nArt++;
        if(cpi->isCavalry())
          nCav++;

        if(!cpi->isGuard())
          allGuard = False;
     }

     cpi->makeInfantryType((nInf > 0));
     cpi->makeCavalryType((nCav > 0));
     cpi->makeArtilleryType((nArt > 0));
  }

  cpi->makeGuard(allGuard);
//  cpi->makeOldGuard(allOldGuard);

  /*
    * set leader specialist flags
    * Set as Commanding Type if a specialist is
    * commanding its type or if a non-specialist is commanding inf or combined arms
    */

  Boolean commandingType = False;

  RefGLeader leader = cpi->leader();

  if(leader->isSpecialist())
  {
     commandingType = ( ( (leader->getSpecialistType() == Specialist::Cavalry) &&
                                 (cpi->isCavalry()) )  ||
                              ( (leader->getSpecialistType() == Specialist::Artillery) &&
                                 (cpi->isArtillery()) ) );
  }
  else
  {
     commandingType = (cpi->isInfantry() || cpi->isCombinedArms());
  }

  leader->isCommandingType(commandingType);

  // set leaders command rank above ceiling
  leader->ranksAboveCeiling(maximum(0,                           
      leader->getRankLevel().getRankEnum() - cpi->getRankEnum()));
//      static_cast<UBYTE>(leader->getRankLevel().getRankEnum() - cpi->getRankEnum())));
}

/*
 * OrderBattle: Strength Point related Functions
 */



void OrderBattle::setSPEntry(const RefGenericCP& cp, const ISP& spi)
{
    Writer lock(this);
    cp->setSPEntry(spi);
}

void OrderBattle::setSPNext(const ISP& sp, const ISP& spi)
{
    Writer lock(this);
    sp->setNext(spi);
}

ISP OrderBattle::createStrengthPoint()
{
    Writer lock(this);
    noteOBChanged();
    SPIndex spi = d_sps.add();

    ASSERT(spi != NoSPIndex);

    return d_sps[spi];
}

void OrderBattle::deleteStrengthPoint(const ISP& isp)
{
    ASSERT(isp->getNext() == NoStrengthPoint);
}

void OrderBattle::deleteSPchain(const RefGenericCP& cp)
{
    Writer lock(this);
    ASSERT(cp != NoGenericCP);
    ISP isp = cp->getSPEntry();
    setSPEntry(cp, NoStrengthPoint);

    while(isp != NoStrengthPoint)
    {
        // ISP sp = d_sps[isp]; // getStrengthPoint(isp);
        ISP next = isp->getNext();
        // sp->setNext(NoSPIndex);
        setSPNext(isp, NoStrengthPoint);
        deleteStrengthPoint(isp);
        isp = next;
    }
}

void OrderBattle::attachStrengthPoint(const RefGenericCP& cp, const ISP& sp)
{
    Writer lock(this);
    noteOBChanged();

    ASSERT(cp != NoGenericCP);
    ASSERT(sp != NoStrengthPoint);

    ASSERT(sp->getNext() == NoStrengthPoint);   // Already attached!
    if(sp->getNext() != NoStrengthPoint)  // Defensive Coding
      return;

    ISP spiEntry = cp->getSPEntry();

    if(spiEntry == NoStrengthPoint)
    {
        // cp->setSPEntry(spi);
        setSPEntry(cp, sp);
    }
    else
    {
        ISP spList;

        do
        {
            ASSERT(spiEntry != sp);    // SP is trying to be attached twice!
            if(spiEntry == sp)      // Defensive coding:
               return;

            spList = spiEntry;  // getStrengthPoint(spiEntry);
            spiEntry = spList->getNext();
        } while(spiEntry != NoStrengthPoint);

        // spList->setNext(spi);
        setSPNext(spList, sp);
    }
}

/*
 * Return True if succesful
 */

Boolean OrderBattle::detachStrengthPoint(const RefGenericCP& cp, const ISP& sp)
{
    Writer lock(this);
    noteOBChanged();

    ASSERT(cp != NoGenericCP);
    ASSERT(sp != NoStrengthPoint);

    ISP spiEntry = cp->getSPEntry();
    ISP spLast = NoStrengthPoint;

    while(spiEntry != sp)
    {
        ASSERT(spiEntry != NoStrengthPoint);
        if(spiEntry == NoStrengthPoint)
        {
            debugMessage("detachStrengthPoint(), SP %d is not attached to %s!",
                (int) d_sps.getIndex(sp),
                cp->getName());
            return False;
        }
        spLast = spiEntry;
        spiEntry = spLast->getNext();
    }

    if(spLast == NoStrengthPoint)
    {
        cp->setSPEntry(sp->getNext());
    }
    else
    {
        setSPNext(spLast, sp->getNext());
    }

    setSPNext(sp, NoStrengthPoint);

    return True;
}


/*
 * Count the number of strength points in a chain
 */

SPCount OrderBattle::getSPListCount(const ConstRefGenericCP& cpi) const
{
    Reader lock(this);
#if 0
#ifdef DEBUG
    // checkStragglers(cpi);
#endif
    // const StrengthPointList* spList = getCommand(cpi);
    // SPIndex spi = spList->getSPEntry();

    // SPIndex spi = command(cpi)->getSPEntry();
    ConstISP spi = cpi->getSPEntry();

    SPCount result = 0;

    while(spi != NoStrengthPoint)
    {
        result++;
        // ConstISP sp = d_sps[spi];    // getStrengthPoint(spi);
        spi = spi->getNext();
    }

    return result;
#else
   return cpi->spCount(false);
#endif
}

/*
 * Count the number of strength points in an organization
 * IF all is True, then ALL strength points in organization
 * IF all is False, then only the given unit's SPs
 */

// SPCount OrderBattle::getUnitSPCount(const RefGenericCP& cpi, Boolean all, Boolean excludeStragglers) const
SPCount OrderBattle::getUnitSPCount(const ConstRefGenericCP& cpi, Boolean all) const
{
    Reader lock(this);
    ASSERT(cpi != NoGenericCP);
#if 0
    if(all)
    {
        SPCount spCount = 0;

        G_ConstUnitIter iter (this, cpi);

        while(iter.next())
        {
            ConstRefGenericCP iUnit = iter.current();
            // spCount += getSPListCount(iUnit, excludeStragglers);
            spCount += getSPListCount(iUnit);
        }

        return spCount;
    }
    else
    {
        // return getSPListCount(cpi, excludeStragglers);
        return getSPListCount(cpi);
    }
#else
   return cpi->spCount(all);
#endif
}

/*
 *  get effective SP count taking effective values into account
 *  actual value is 1/10th of UnitTypeItem.effectiveValue
 */

// SPCount OrderBattle::getUnitSPValue(const RefGenericCP& cpi, Boolean excludeStragglers) const
SPCount OrderBattle::getUnitSPValue(const ConstRefGenericCP& cpi) const
{
    Reader lock(this);
  ASSERT(cpi != NoGenericCP);

  SPValue spValue = 0;

  G_ConstSPIter iter (this, cpi);

    while(++iter)
    {
        ConstISP sp = iter.current();
        const UnitTypeItem& uti = getUnitType(sp->getUnitType());
        spValue += uti.getEffectiveValue();
    }

  // round out to nearest int
  return static_cast<SPCount>((spValue + 4)/10);
}

/*
 *  get effective type SP count taking effective values into account
 *  actual value is 1/10th of UnitTypeItem.effectiveValue
 */

SPCount OrderBattle::getUnitSPValue(const ConstRefGenericCP& cpi, BasicUnitType::value type) const
{
    Reader lock(this);
  ASSERT(cpi != NoGenericCP);

  SPValue spValue = 0;

  G_ConstSPIter iter (this, cpi);

    while(++iter)
    {
        ConstISP sp = iter.current();
        const UnitTypeItem& uti = getUnitType(sp->getUnitType());

        if(uti.getBasicType() == type)
          spValue += uti.getEffectiveValue();
    }

  // round out to nearest int
  return static_cast<SPCount>((spValue + 4)/10);
}

const char* OrderBattle::spName(const ConstISP& isp) const
{
    ASSERT(isp != NoStrengthPoint);
    const UnitTypeItem& spData = getUnitType(isp->getUnitType());
    return spData.getName();
}

/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.3  2001/07/02 15:23:17  greenius
 * Battle, Campaign, Combined game and Campaign Editor now compile and run... mostly
 *
 *----------------------------------------------------------------------
 */
