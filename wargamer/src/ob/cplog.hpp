/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef CPLOG_HPP
#define CPLOG_HPP

#ifndef __cplusplus
#error cplog.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *	CommandPosition Log
 *
 *----------------------------------------------------------------------
 */

#ifdef DEBUG

#include "ob_dll.h"
#include "clog.hpp"

extern OB_DLL LogFileFlush cpLog;

#endif

#endif /* CPLOG_HPP */

