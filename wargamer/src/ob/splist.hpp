/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef SPLIST_HPP
#define SPLIST_HPP

#ifndef __cplusplus
#error splist.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *	Strength Point List
 *
 *----------------------------------------------------------------------
 */

#include "gamedefs.hpp"
#include "obdefs.hpp"
// #include "poolfile.hpp"
#include "sp.hpp"

class StrengthPointList : 
	public PoolArray<StrengthPoint>
{
	typedef PoolArray<StrengthPoint> Super;
	typedef SPIndex Index;
public:
	StrengthPointList() : Super() { }	// ISP::setList(this); }
	virtual ~StrengthPointList() { }		// ISP::setList(0); }

private:
	const char* getChunkName() const;

public:
#if 0
	int addRef(Index i) { return Super::addRef(i); }
	int delRef(Index i)
	{
#ifdef DEBUG
		logDelRef(i);
#endif
		return Super::delRef(i); 
	}
#endif

	Boolean readData(FileReader& f, BaseOrderBattle& ob);
	Boolean writeData(FileWriter& f, const BaseOrderBattle& ob) const;

	ISP operator [] (SPIndex spi) 
	{
		if(spi == NoSPIndex)
			return NoStrengthPoint;
		else
			return &Super::operator[](spi); 
	}
	ConstISP operator [] (SPIndex spi) const
	{
		if(spi == NoSPIndex)
			return NoStrengthPoint;
		else
			return &Super::operator[](spi); 
	}

	void cleanup();
		// Garbage collection

private:
	void remove(PoolIndex i) { Super::remove(i); }		// Demote to private
private:
#ifdef DEBUG
	void logDelRef(Index i);
#endif
};

#endif /* SPLIST_HPP */

