/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef OBNAMES_HPP
#define OBNAMES_HPP


/*
 * Name Tables Used in Order of Battle
 */

#include "ob_dll.h"
#include "nameTable.hpp"

class OB_Names
{
   public:
      static const char* getLeaderName(StringFileTable::ID id);
      static const char* getCPName(StringFileTable::ID id);

      static StringFileTable::ID setLeaderName(const char* s);
      static StringFileTable::ID setCPName(const char* s);

      static StringFileTable::ID updateLeaderName(StringFileTable::ID id, const char* s);
      static StringFileTable::ID updateCPName(StringFileTable::ID id, const char* s);

      static bool isValidCPName(StringFileTable::ID id);
      static bool isValidLeaderName(StringFileTable::ID id);

      static bool isNewCPName(StringFileTable::ID id);
      static bool isNewLeaderName(StringFileTable::ID id);

      static OB_DLL bool writeFile();

      static void create();      // setup tables for current scenario
      static void destroy();     // destroy tables
};


#endif   // OBNAMES_HPP
