/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
#ifndef CP_HPP
#define CP_HPP

#ifndef __cplusplus
#error cp.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 * Generic CommandPosition class
 *
 *----------------------------------------------------------------------
 */

#include "ob_dll.h"
#include "mytypes.h"
#include "poolarry.hpp"
#include "gamedefs.hpp"
#include "obdefs.hpp"
#include "rank.hpp"
// #include "name.hpp"
#include "obNames.hpp"
#include "refptr.hpp"


#include "sp.hpp"
#include "leader.hpp"

#ifdef DEBUG
//#define LOG_CP_REF
#endif

class FileReader;
class FileWriter;

class BaseOrderBattle;
//class OrderBattle;

class CommandPosition;
class BattleCP;

/*=====================================================================
 * How units are connected
 */

class CommandLink {
   static const UWORD s_fileVersion;

   CPIndex d_self;         // Stop us referencing ourself
   RefGenericCP d_parent;
   RefGenericCP d_sister;
   RefGenericCP d_child;
   public:
   CommandLink();

   CPIndex self() const
   { return d_self;
   }

   // RefGenericCP parent() const { return d_parent; }
   // RefGenericCP sister() const { return d_sister; }
   // RefGenericCP child() const { return d_child; }
   // GenericCP* parent() const { return d_parent; }
   // GenericCP* sister() const { return d_sister; }
   // GenericCP* child() const { return d_child; }
   const RefGenericCP& parent() const
   { return d_parent;
   }
   const RefGenericCP& sister() const
   { return d_sister;
   }
   const RefGenericCP& child() const
   { return d_child;
   }

   void self(CPIndex p)
   { d_self   = p;
   }
   void parent(const RefGenericCP& p)
   { d_parent = p;
   }
   void sister(const RefGenericCP& p)
   { d_sister = p;
   }
   void child(const RefGenericCP& p)
   { d_child  = p;
   }

   /*
   * File Interface
   */

   Boolean readData(FileReader& f, BaseOrderBattle& cpList);
   Boolean writeData(FileWriter& f, const BaseOrderBattle& cpList) const;
};

#if 0
/*
 * Abstract class for CommanPosition
 */

class CampaignCP_Base {
   public:
   CampaignCP_Base()
   {
   }
   virtual ~CampaignCP_Base()
   {
   }

   virtual Boolean readData(FileReader& f, OrderBattle* ob) = 0;
   virtual Boolean writeData(FileWriter& f, OrderBattle* ob) const = 0;
};
#endif


class OB_DLL GenericCP :
   public RefBaseCount
{
   public:

   /*
   * CP Flags include what kind of unit this is
   * i.e. Inf, Cav, Art, Guard, etc.
   *
   * Note: various flags may or may not be allowed or combined depending on unit rank or type
   *    1. A XXX or higher may be Inf, Cav, and Arty, whilst a XX can only be one or the other
   *    2. Guard may be combined with any basic type (Inf, Cav, Arty)
   *    4. A CoO cannot be combined with any other flag
   *    5. A CoO cannot have subordinates
   *    5. Unit Rank is irrelevant if this is a LOC unit
   *    6. LOC's do not have a leader
   */

   enum
   {
      CPF_Infantry  = 0x01,
         CPF_Cavalry   = 0x02,
         CPF_Artillery = 0x04,
         CPF_Guard     = 0x08,
         CPF_OldGuard  = 0x10,

         CPF_HeavyCavalry   = 0x20,
         CPF_HeavyArtillery = 0x40,
         CPF_HorseArtillery = 0x80
   };

   enum
   { CPF_CoO = 0
   };   // for back referenceing


   GenericCP();
   ~GenericCP();

   /*
   * CP Type functions
   */

   void clearFlags()
   { d_flags = 0;
   }

   void makeInfantryType(Boolean f)
   {
      setFlag(CPF_Infantry, f);
      ASSERTFLAGS();
   }

   void makeCavalryType(Boolean f)
   {
      setFlag(CPF_Cavalry, f);
      ASSERTFLAGS();
   }

   void makeHeavyCavalryType(Boolean f)
   {
      ASSERT(!f || isCavalry());
      setFlag(CPF_HeavyCavalry, f);
   }

   void makeArtilleryType(Boolean f)
   {
      setFlag(CPF_Artillery, f);
      ASSERTFLAGS();
   }

   void makeHeavyArtilleryType(Boolean f)
   {
      ASSERT(!f || isArtillery());
      setFlag(CPF_HeavyArtillery, f);
   }

   void makeHorseArtilleryType(Boolean f)
   {
      ASSERT(!f || isArtillery());
      setFlag(CPF_HorseArtillery, f);
   }

   void makeGuard(Boolean f)
   {
      setFlag(CPF_Guard, f);
   }

   void makeOldGuard(Boolean f)
   {
      setFlag(CPF_OldGuard, f);
   }

   void makeCoO(Boolean f)
   {
   }

   void ASSERTFLAGS() const
   {
#ifdef DEBUG
      if(d_rank.sameRank(Rank_Division))
      {
         UBYTE flag = d_flags & (CPF_Infantry | CPF_Artillery | CPF_Cavalry);
         ASSERT((flag == 0) ||
            (flag == CPF_Infantry) ||
            (flag == CPF_Artillery) ||
            (flag == CPF_Cavalry));
      }
#endif
   }

   Boolean isInfantry()       const
   {
      ASSERTFLAGS();
      return (d_flags & CPF_Infantry);
   }

   Boolean isCavalry()        const
   {
      ASSERTFLAGS();
      return (d_flags & CPF_Cavalry) != 0;
   }

   Boolean isHeavyCavalry() const
   { return (d_flags & CPF_HeavyCavalry) != 0;
   }

   Boolean isArtillery()      const
   {
      ASSERTFLAGS();
      return (d_flags & CPF_Artillery) != 0;
   }

   Boolean isHeavyArtillery() const
   { return (d_flags & CPF_HeavyArtillery) != 0;
   }
   Boolean isHorseArtillery() const
   { return (d_flags & CPF_HorseArtillery) != 0;
   }
#if 0  // simplify because inf/cav/art must be set for any of the advanced types
      Boolean isCombinedArms()   const
   { return ((isInfantry() && (isCavalry() || isHeavyCavalry())) ||
         (isInfantry() && (isArtillery() || isHeavyArtillery() || isHorseArtillery())) ||
         ((isCavalry() || isHeavyCavalry()) && (isArtillery() || isHeavyArtillery() || isHorseArtillery())));
   }
#else
   Boolean isCombinedArms() const
   {
      return (isInfantry() && (isArtillery() || isCavalry())) ||
         (isCavalry() && isArtillery() );
   }
#endif
   Boolean isGuard()        const
   { return (d_flags & CPF_Guard) != 0;
   }
   Boolean isOldGuard()     const
   { return (d_flags & CPF_OldGuard) != 0;
   }
   Boolean isCoO()          const
   { return False;
   }

   // New functions added by Steven
   //
   // bool containsArtillery() const
   // {
   //  return d_flags & (CPF_Artillery | CPF_HeavyArtillery | CPF_HorseArtillery);
   // }
   //
   // bool isOnlyArtillery() const
   // {
   //  return !(d_flags & ~(CPF_Artillery | CPF_HeavyArtillery | CPF_HorseArtillery));
   // }


#ifdef LOG_CP_REF
   int addRef() const;
   int delRef() const;
#else
   int addRef() const
   { return RefBaseCount::addRef();
   }
   int delRef() const
   { return RefBaseCount::delRef();
   }
#endif
   int refCount() const
   { return RefBaseCount::refCount();
   }

   // GenericCP* operator -> () { return this; }
   // const GenericCP* operator -> () const { return this; }

   /*
   * File Interface
   */

   Boolean readData(FileReader& f, BaseOrderBattle& ob);
   Boolean writeData(FileWriter& f, const BaseOrderBattle& ob) const;

   CommandPosition*   campaignInfo()
   { return d_campaignInfo;
   }
   const CommandPosition* campaignInfo() const
   { return d_campaignInfo;
   }

   void campaignInfo(CommandPosition* info)
   {
      ASSERT((d_campaignInfo == 0) || (info == 0));
      d_campaignInfo = info;
   }

   BattleCP*    battleInfo()
   { return d_battleInfo;
   }
   const BattleCP*    battleInfo() const
   { return d_battleInfo;
   }
   void battleInfo(BattleCP* info)
   {
      ASSERT((d_battleInfo == 0) || (info == 0));
      d_battleInfo = info;
   }

   /*
   * Value retrieval and setting
   */

   RefGLeader leader() const
   { return d_leader;
   }
   void leader(const RefGLeader& l)
   { d_leader = l;
   }

   void nation(Nationality n)
   { d_nationality = n;
   }
   Nationality nation() const
   { return d_nationality;
   }
   void setNation(Nationality n)
   { nation(n);
   }
   Nationality getNation() const
   { return nation();
   }

   void side(Side s)
   { d_side = s;
   }
   Side side() const
   { return d_side;
   }

   bool isDead() const
   { return d_isDead;
   }
   void isDead(bool f)
   { d_isDead = f;
   }

   /*
   * Attributes
   */

   Attribute morale() const
   { return d_morale;
   }
   Attribute fatigue() const
   { return d_fatigue;
   }
   Attribute supply() const
   { return d_supply;
   }
   void morale(Attribute m)
   { d_morale = m;
   }
   void fatigue(Attribute f)
   { d_fatigue = f;
   }
   void supply(Attribute s)
   { d_supply = s;
   }


   /*
   * Name
   */

      const char* getName() const;
      const char* getNameNotNull(const char* def = 0) const { return getName(); }
      void setName(char* s);

//    const char* getName() const
//    { return d_name.getName();
//    }
//    const char* getNameNotNull(const char* def = 0) const
//    { return d_name.getNameNotNull(def);
//    }
//    void setName(char* s)
//    {  d_name.setName(s);
//    }

   /*
   * Ranks
   */

   const Rank& getRank() const
   { return d_rank;
   }
   void setRank(const Rank& r)
   { d_rank = r;
   }
   RankEnum getRankEnum() const
   { return d_rank.getRankEnum();
   }
   Boolean isHigher(RankEnum r) const
   { return d_rank.isHigher(r);
   }
   Boolean isLower(RankEnum r) const
   { return d_rank.isLower(r);
   }
   Boolean sameRank(RankEnum r) const
   { return d_rank.sameRank(r);
   }

   /*
   * CommandLink
   */

   void setSelf(CPIndex p)
   { d_link.self(p);
   }
   void setParent(const RefGenericCP& p)
   { d_link.parent(p);
   }
   void setSister(const RefGenericCP& p)
   { d_link.sister(p);
   }
   void setChild(const RefGenericCP& p)
   { d_link.child(p);
   }

   CPIndex getSelf() const
   { return d_link.self();
   }

   // RefGenericCP getParent() const { return d_link.parent(); }
   // RefGenericCP getSister() const { return d_link.sister(); }
   // RefGenericCP getChild()  const { return d_link.child(); }
   // GenericCP* getParent() const { return d_link.parent(); }
   // GenericCP* getSister() const { return d_link.sister(); }
   // GenericCP* getChild()  const { return d_link.child(); }
   const RefGenericCP& getParent() const
   { return d_link.parent();
   }
   const RefGenericCP& getSister() const
   { return d_link.sister();
   }
   const RefGenericCP& getChild()  const
   { return d_link.child();
   }

   bool hasChild()  const
   { return d_link.child()  != NoGenericCP;
   }
   bool hasSister() const
   { return d_link.sister() != NoGenericCP;
   }
   bool hasParent() const
   { return d_link.parent() != NoGenericCP;
   }

   // Return non-constant references

   // CPIndex& r_self()   { return d_link.self(); }
   // RefGenericCP& r_parent()  { return d_link.parent(); }
   // RefGenericCP& r_sister()  { return d_link.sister(); }
   // RefGenericCP& r_child()   { return d_link.child(); }

   ISP getSPEntry() const
   { return d_spEntry;
   }
   // Return Value
   // RefSP& r_spEntry() { return d_spEntry; }
   // Return reference
   void setSPEntry(const ISP& sp)
   { d_spEntry = sp;
   }
   // Set value
   SPCount spCount(bool all) const;

   bool isSPattached(const ConstISP& isp) const;


   private:
   void setFlag(UBYTE mask, Boolean f)
   {
      if(f)
         d_flags |= mask;
      else
         d_flags &= ~mask;
   }

   private:
   static const UWORD s_fileVersion;

   // CommandPosition*  d_campaign;    // Note that Campaign is reponsible for memory management
   // CampaignCP* d_campaign;
   // BattleCP*   d_battle;

   Side        d_side;
   Nationality d_nationality;

   Rank        d_rank;     // What rank is it.
   RefGLeader  d_leader;   // Who is commanding this unit?
//   NamedItem   d_name;
   StringFileTable::ID d_nameID;
   CommandLink d_link;     // Who it is linked to
   ISP         d_spEntry;  // Entry point to Strength Point List
   Boolean     d_isDead;

   CommandPosition*  d_campaignInfo;
   BattleCP*      d_battleInfo;

   Attribute         d_morale;      // 0=routing, 255=good
   Attribute         d_fatigue;     // 0=tired, 255=fresh
   Attribute         d_supply;      // 0=unsupplied, 255=fully supplied

   UBYTE d_flags;


};

#if 1
inline const CommandPosition* campCP(const ConstRefGenericCP& cp)
{
   if(cp == NoGenericCP)
      return 0;
   else
      return cp->campaignInfo();
}


inline CommandPosition* campCP(const RefGenericCP& cp)
{
   if(cp == NoGenericCP)
      return 0;
   else
      return cp->campaignInfo();
}
#else

// inline const CommandPosition* campCP(const GenericCP* cp)
// {
//    if(cp == NoGenericCP)
//       return 0;
//    else
//       return cp->campaignInfo();
// }


inline CommandPosition* campCP(GenericCP* cp)
{
   if(cp == NoGenericCP)
      return 0;
   else
      return cp->campaignInfo();
}

#endif

// ----------------- Unchecked update ----------------------
#if 1

inline const BattleCP* battleCP(const ConstRefGenericCP& cp)
{
   return (cp != NoGenericCP) ? cp->battleInfo() : 0;
}

inline BattleCP* battleCP(const RefGenericCP& cp)
{
   return (cp != NoGenericCP) ? cp->battleInfo() : 0;
}

#else

inline BattleCP* battleCP(GenericCP* cp)
{
   return (cp != NoGenericCP) ? cp->battleInfo() : 0;
}

#endif

//-------------------------------------------------------------
#endif /* CP_HPP */

/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.2  2001/06/18 08:25:45  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
