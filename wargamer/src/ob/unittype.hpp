/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef UNITTYPE_HPP
#define UNITTYPE_HPP

#ifndef __cplusplus
#error unittype.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1995, Steven Morle-Green, All Rights Reserved
 * Originally started by Paul
 *----------------------------------------------------------------------
 *
 *      Unit Type Table
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/06/13 08:52:38  greenius
 * Initial Import
 *
 *
 *----------------------------------------------------------------------
 */

#include "ob_dll.h"
#include "gamedefs.hpp"
#include "array.hpp"
#include "StringPtr.hpp"

#if defined(__WATCOM_CPLUSPLUS__) && (__WATCOM_CPLUSPLUS__ <= 1100)
#define NAMESPACE(n) class n { public:
#else
#define NAMESPACE(n) namespace n {
#endif

class WinFileAsciiChunkReader;
class FileChunkReader;

/*-------------------------------------------------------------------
 * classes for unittype data tables
 *------------------------------------------------------------------*/

/*
 * Set up a namespace for the enums
 * This better practice than prefixing with strange abreviations
 */

/*
 * Some basic Types
 */

class BasicUnitType {
public:
        enum value {
                Infantry, 
                Cavalry,
                Artillery,
                Special,                        // Any non-combat unit
                HowMany
        };
};

class SpecialUnitType {
public:
        enum value {
                Engineer,
//              Supply,
      BridgeTrain,
                SiegeTrain,
                NotSpecial,                     // Engineer, Ship, Supply, etc.

                HowMany,
                First = Engineer,
                FirstHeavy = BridgeTrain,
      LastHeavy = SiegeTrain
        };
};

class WeaponType {
public:
        enum value {
                WT_None,
                WT_S_MUSKET,
                WT_R_MUSKET,
                WT_REPEATER,
                WT_S_CANNON,
                WT_R_CANNON,

                WT_HowMany
        };
};
typedef Set8 WeaponMask;

class Formations {
public:
        enum value {
                FM_LINE,
                FM_COLUMN,
                FM_D_COLUMN,
                FM_M_COLUMN,
                FM_SQUARE,
                FM_LIMBERED,
                FM_UNLIMBERED,
                FM_SKIRMISH,

                FM_HowMany
        };
};
typedef Set16 FormationMask;

class AttritionClass {
public:
        enum value {
                Poor,
                Fair,
                Good,
                Excellent,

                HowMany
        };
};

class UnitTypeFlags {
public:
  enum value {
                NoValue,
                Mounted,
                LightCavalry,
                Guard,
                OldGuard,

                HowMany
  };
};
typedef Set8 FlagsMask;

class UnitTypeConst {
    public:
        enum {
                // raw numbers
                InfantryPerSP = 1000,
                CavalryPerSP = 500,
                ArtilleryPerSP = 250,
                SpecialPerSP = 250,
                GunsPerSP = 12,

                // allowed per division
                InfAllowedPerXX   = 10,
                CavAllowedPerXX   = 10,
                ArtAllowedPerXX   = 12,
                OtherAllowedPerXX = 12,
            MaxAllowedPerXX   = 12
        };

        static int menPerType(BasicUnitType::value t)
        {
            ASSERT(t < BasicUnitType::HowMany);
            return s_menPerType[t]; 
        }
    private:

        OB_DLL static const int s_menPerType[];
};


class TroopCrossRef;

/*
 * An entry in Unit Type Table
 * This could possibly be hidden inside unittype.cpp, which would
 * force access functions from the container class to be used.
 */

class OB_DLL UnitTypeItem {
        friend class UnitTypeTable;

        /*
         * Values used by everything
         */

        NationMask nations;                                             // Who can build these?
        StringPtr description;                                  // What is it called? e.g. "Old Guard Infantry"
        UnitType d_ID;                      // UnitType ID
        BasicUnitType::value basicType;         // Infantry, Cavalry, Artillery, Special
        SpecialUnitType::value specialType;     // If it's not a combat then this says exactly what it is
        FlagsMask d_flags;                  // various flags. see UnitTypeFlags enum

        // Attribute strength;
        Attribute response;
        Attribute control;
				Attribute fireValue;
				Attribute impact;


				/*
				 * Values used by campaign
				 */

				AttributePoints totalManpowerCost;
				AttributePoints totalResourceCost;
				AttributePoints weeklyManpowerCost;
				AttributePoints weeklyResourceCost;

				Attribute baseMorale;                 // Value between 0-255, used for relative quality

				SPValue effectiveValue;               // Effective value of SP for combat purposes
				AttritionClass::value attritionClass; // Attrition-effects value. Between 0 - 3
//      Boolean mounted;                      // Is this a mounted unit?
				UWORD strategicSpeedModifier;         // Modifier to strategic speed.
																																																					// Actual modifier is strategicSpeedModifier/100
				// Speed strategicSpeed;
				BombValue bombardmentValue;           // Effective Bombardment value(non-artillery is 0)

//      Boolean lightCavalry;                 // is this a light cavalry outfit

				UnitType d_promoteTo;                   // Can be promoted to this type

				/*
				 * Values used by Battle
				 */

				// Formations::FormationMask allowedFormations;
				FormationMask allowedFormations;
				WeaponType::value weapon;
				Attribute skirmishRating;
				float d_combatVictoryValue;

				unsigned int d_SpriteIndex[4];

				// More values to be added here including:
				//      Date available (or tech level needed)
				//              Graphics Indeces
				//              Enumeration, DLL or function name (for special type)

				// Prevent Copy Constructor... can't because Array<UnitTypeItem> doesn't work
				// instead, I've changed CString to StringPtr, which can be copied!
				// UnitTypeItem(const UnitTypeItem&);
				// UnitTypeItem& operator = (const UnitTypeItem&);
public:
				 UnitTypeItem();
				 ~UnitTypeItem();

				/*
				 * Access Functions
				 */

				const char* getName() const { return description; }
				BasicUnitType::value getBasicType() const { return basicType; }
				SpecialUnitType::value getSpecialType() const { return specialType; }
				AttritionClass::value getAttritionClass() const { return attritionClass; }

				void getTotalResource(AttributePoints& man, AttributePoints& res) const;
				void getWeeklyResource(AttributePoints& man, AttributePoints& res) const;

				Boolean isNationality(Nationality n) const
				{
								if(n == NATION_Neutral)
												return False;
								else
												return nations.isMember(n);
				}

				// Attribute getQuality() const { return strength; }    // To work out what to get rid of first

				// Formations::FormationMask getAllowedFormations() const { return allowedFormations; }
				FormationMask getAllowedFormations() const { return allowedFormations; }
				WeaponType::value getWeapon() const { return weapon; }
				// Attribute getStrength() const { return strength; }
				Attribute getResponse() const { return response; }
				Attribute getControl() const { return control; }
				Attribute getFireValue() const { return fireValue; }
        Attribute getImpact() const { return impact; }
				// Speed getStrategicSpeed() const { return strategicSpeed; }
				SPValue getEffectiveValue() const { return effectiveValue; }
				BombValue getBombardmentValue() const { return bombardmentValue; }
				Attribute getBaseMorale() const { return baseMorale; }
				Boolean isMounted() const { return d_flags.isMember(UnitTypeFlags::Mounted); }
				Boolean isLightCavalry() const { return d_flags.isMember(UnitTypeFlags::LightCavalry); }
				Boolean isGuard() const { return d_flags.isMember(UnitTypeFlags::Guard); }
				Boolean isOldGuard() const { return d_flags.isMember(UnitTypeFlags::OldGuard); }
				UWORD getStrategicSpeedModifier() const { return strategicSpeedModifier; }
				FlagsMask getFlags(void) { return d_flags; }


				float combatVictoryValue() const { return d_combatVictoryValue; }

				UnitType promoteTo() const { return d_promoteTo; }
				Boolean canPromote() const { return (d_promoteTo != NoUnitType); }
				UnitType getID() const { return d_ID; }

				unsigned int getSpriteIndex(int graphic_index) const { return d_SpriteIndex[graphic_index]; }
				unsigned int getSpriteIndex(int graphic_index) { return d_SpriteIndex[graphic_index]; }

				/*
				 * File Interface
				 */

        Boolean readData(FileChunkReader& f, const TroopCrossRef& ref);

};

/*
 * Unit Type Container Class
 *
 * Ideally all access should go through here.
 */

class OB_DLL UnitTypeTable
{
        Array<UnitTypeItem> items;
public:
        UnitTypeTable();
        ~UnitTypeTable();

        Boolean init(const char* fileName);

        UnitType entries() const
        {
                return (UnitType) items.entries();
        }

        const UnitTypeItem& getData(UnitType n)
        {
                return items[n];
        }

        const UnitTypeItem& operator[](UnitType n) const
        {
                return items[n];
        }

        /*
         * File Interface
         */

        Boolean readData(WinFileAsciiChunkReader& f);

};






#endif /* UNITTYPE_HPP */

