# Microsoft Developer Studio Project File - Name="ob" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Dynamic-Link Library" 0x0102

CFG=ob - Win32 Editor Debug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "ob.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "ob.mak" CFG="ob - Win32 Editor Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "ob - Win32 Release" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE "ob - Win32 Debug" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE "ob - Win32 Editor Debug" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE "ob - Win32 Editor Release" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""
CPP=cl.exe
MTL=midl.exe
RSC=rc.exe

!IF  "$(CFG)" == "ob - Win32 Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "..\..\exe"
# PROP Intermediate_Dir "o\release"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MT /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "OB_EXPORTS" /YX /FD /c
# ADD CPP /nologo /MD /W3 /Gi /GR /GX /Zi /O2 /I "..\h" /I "..\..\res" /I "..\system" /I "..\gamesup" /D "NDEBUG" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "EXPORT_OB_DLL" /YX"stdinc.hpp" /FD /c
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x809 /d "NDEBUG"
# ADD RSC /l 0x809 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /dll /machine:I386
# ADD LINK32 user32.lib /nologo /dll /debug /machine:I386

!ELSEIF  "$(CFG)" == "ob - Win32 Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "..\..\exe"
# PROP Intermediate_Dir "o\debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MTd /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "OB_EXPORTS" /YX /FD /GZ /c
# ADD CPP /nologo /MDd /W3 /Gm /Gi /GR /GX /ZI /Od /I "..\h" /I "..\..\res" /I "..\system" /I "..\gamesup" /D "_DEBUG" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "EXPORT_OB_DLL" /YX"stdinc.hpp" /FD /GZ /c
# ADD BASE MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x809 /d "_DEBUG"
# ADD RSC /l 0x809 /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /dll /debug /machine:I386 /pdbtype:sept
# ADD LINK32 user32.lib /nologo /dll /debug /machine:I386 /out:"..\..\exe/obDB.dll" /pdbtype:sept

!ELSEIF  "$(CFG)" == "ob - Win32 Editor Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "ob___Win32_Editor_Debug"
# PROP BASE Intermediate_Dir "ob___Win32_Editor_Debug"
# PROP BASE Ignore_Export_Lib 0
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "..\..\exe"
# PROP Intermediate_Dir "o\Editor\Debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MDd /W3 /Gm /Gi /GR /GX /ZI /Od /I "..\h" /I "..\..\res" /I "..\system" /I "..\gamesup" /D "_DEBUG" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "EXPORT_OB_DLL" /YX"stdinc.hpp" /FD /GZ /c
# ADD CPP /nologo /MDd /W3 /Gm /Gi /GR /GX /ZI /Od /I "..\h" /I "..\..\res" /I "..\system" /I "..\gamesup" /D "EXPORT_OB_DLL" /D "_USRDLL" /D "EDITOR" /D "_DEBUG" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /YX"stdinc.hpp" /FD /GZ /c
# ADD BASE MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x809 /d "_DEBUG"
# ADD RSC /l 0x809 /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 user32.lib /nologo /dll /debug /machine:I386 /out:"..\..\exe/obDB.dll" /pdbtype:sept
# ADD LINK32 user32.lib /nologo /dll /debug /machine:I386 /out:"..\..\exe/obEDDB.dll" /pdbtype:sept

!ELSEIF  "$(CFG)" == "ob - Win32 Editor Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "ob___Win32_Editor_Release"
# PROP BASE Intermediate_Dir "ob___Win32_Editor_Release"
# PROP BASE Ignore_Export_Lib 0
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "..\..\exe"
# PROP Intermediate_Dir "o\Editor\Release"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MD /W3 /Gi /GR /GX /Zd /O2 /I "..\h" /I "..\..\res" /I "..\system" /I "..\gamesup" /D "NDEBUG" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "EXPORT_OB_DLL" /YX"stdinc.hpp" /FD /c
# ADD CPP /nologo /MD /W3 /Gi /GR /GX /Zd /O2 /I "..\h" /I "..\..\res" /I "..\system" /I "..\gamesup" /D "EXPORT_OB_DLL" /D "NDEBUG" /D "_USRDLL" /D "EDITOR" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /YX"stdinc.hpp" /FD /c
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x809 /d "NDEBUG"
# ADD RSC /l 0x809 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 user32.lib /nologo /dll /debug /machine:I386
# ADD LINK32 user32.lib /nologo /dll /debug /machine:I386 /out:"..\..\exe/obED.dll"

!ENDIF 

# Begin Target

# Name "ob - Win32 Release"
# Name "ob - Win32 Debug"
# Name "ob - Win32 Editor Debug"
# Name "ob - Win32 Editor Release"
# Begin Group "Source Files"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat"
# Begin Source File

SOURCE=.\b_result.cpp
# End Source File
# Begin Source File

SOURCE=.\cp.cpp
# End Source File
# Begin Source File

SOURCE=.\cplist.cpp
# End Source File
# Begin Source File

SOURCE=.\cplog.cpp
# End Source File
# Begin Source File

SOURCE=.\leader.cpp
# End Source File
# Begin Source File

SOURCE=.\leadlist.cpp
# End Source File
# Begin Source File

SOURCE=.\nations.cpp
# End Source File
# Begin Source File

SOURCE=.\ob.cpp
# End Source File
# Begin Source File

SOURCE=.\obinit.cpp
# End Source File
# Begin Source File

SOURCE=.\obiter.cpp
# End Source File
# Begin Source File

SOURCE=.\obnames.cpp
# End Source File
# Begin Source File

SOURCE=.\poolfile.cpp
# End Source File
# Begin Source File

SOURCE=.\rank.cpp
# End Source File
# Begin Source File

SOURCE=.\sideinfo.cpp
# End Source File
# Begin Source File

SOURCE=.\sp.cpp
# End Source File
# Begin Source File

SOURCE=.\splist.cpp
# End Source File
# Begin Source File

SOURCE=.\unittype.cpp
# End Source File
# End Group
# Begin Group "Header Files"

# PROP Default_Filter "h;hpp;hxx;hm;inl"
# Begin Source File

SOURCE=.\b_result.hpp
# End Source File
# Begin Source File

SOURCE=.\cp.hpp
# End Source File
# Begin Source File

SOURCE=.\cplist.hpp
# End Source File
# Begin Source File

SOURCE=.\cplog.hpp
# End Source File
# Begin Source File

SOURCE=.\leader.hpp
# End Source File
# Begin Source File

SOURCE=.\leadlist.hpp
# End Source File
# Begin Source File

SOURCE=.\nations.hpp
# End Source File
# Begin Source File

SOURCE=.\ob.hpp
# End Source File
# Begin Source File

SOURCE=.\ob_base.hpp
# End Source File
# Begin Source File

SOURCE=.\ob_dll.h
# End Source File
# Begin Source File

SOURCE=.\obdefs.hpp
# End Source File
# Begin Source File

SOURCE=.\obinit.hpp
# End Source File
# Begin Source File

SOURCE=.\obiter.hpp
# End Source File
# Begin Source File

SOURCE=.\obnames.hpp
# End Source File
# Begin Source File

SOURCE=.\poolfile.hpp
# End Source File
# Begin Source File

SOURCE=.\rank.hpp
# End Source File
# Begin Source File

SOURCE=.\sideinfo.hpp
# End Source File
# Begin Source File

SOURCE=.\sp.hpp
# End Source File
# Begin Source File

SOURCE=.\splist.hpp
# End Source File
# Begin Source File

SOURCE=.\stdinc.hpp
# End Source File
# Begin Source File

SOURCE=.\unittype.hpp
# End Source File
# End Group
# Begin Group "Resource Files"

# PROP Default_Filter "ico;cur;bmp;dlg;rc2;rct;bin;rgs;gif;jpg;jpeg;jpe"
# End Group
# End Target
# End Project
