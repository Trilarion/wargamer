/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *	Information in OB about each side
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "sideinfo.hpp"
#include "wldfile.hpp"
#include "filebase.hpp"		// For FileReader and FileWriter
#include "todolog.hpp"
#include "ob_base.hpp"

OBSideInfo::OBSideInfo()
{
	d_supremeLeader = NoGLeader;
	clearRankCounts();
	d_president = NoGenericCP;
}

void OBSideInfo::clearRankCounts()
{
	for(int r = 0; r < Rank_HowMany; r++)
		d_rankCount[r] = 1;			// start with 1st of everything
}

UWORD OBSideInfo::nextRankCount(Rank rank)
{
	RankEnum r = rank.getRankEnum();

	ASSERT(r < Rank_HowMany);
	ASSERT(d_rankCount[r] != UWORD_MAX);

	return d_rankCount[r]++;
}



const UWORD OBSideInfo::fileVersion = 0x0003;

Boolean OBSideInfo::readData(FileReader& f, BaseOrderBattle& ob)
{

	if(f.isAscii())
	{
#ifdef DEBUG
		todoLog.printf("TODO: OBSideInfo::readData(),ascii unwritten\n");
#endif
		return False;
	}
	else
	{
		UWORD version;
		f.getUWord(version);
		ASSERT(version <= fileVersion);

		// f.getUWord(president);
		CPIndex iPresident;
		f >> iPresident;
		d_president = ob.command(iPresident);
		// f >> d_president;


		if(version < 0x0002)
		{
			UBYTE b;
			f.getUByte(b);
		}

		if(version >= 0x0003)
		{
		  for(int i = 0; i < Rank_HowMany; i++)
			 f.getUWord(d_rankCount[i]);
		}
		else
		{
		  for(int i = 0; i < (Rank_HowMany + 1); i++)
		  {
			 UWORD w;
			 if(i == Rank_Corps || i >= Rank_HowMany)
				f.getUWord(w);

			 if(i < Rank_HowMany)
				f.getUWord(d_rankCount[i]);
		  }
		}

		if(version >= 0x0001)
		{
		  // f.getUWord(supremeLeader);
		  LeaderIndex iLeader;
		  f >> iLeader;
		  d_supremeLeader = ob.leader(iLeader);
		  // f >> d_supremeLeader;
		}
		else
		  d_supremeLeader = NoGLeader;
	}

	return f.isOK();
}

Boolean OBSideInfo::writeData(FileWriter& f, const BaseOrderBattle& ob) const
{
	if(f.isAscii())
	{
		f.printf(startItemString);
		f.printf(fmt_sdn, presidentToken, static_cast<int>(ob.getIndex(d_president)));
#if 0
		f.printf(fmt_sdn, controlToken, (int) control);
#endif
		f.printf(rankCountToken);
		for(int i = 0; i < Rank_HowMany; i++)
			f.printf(" %d", d_rankCount[i]);
		f.printf(eolString);
		f.printf(endItemString);
	}
	else
	{
		f.putUWord(fileVersion);
		CHECKUWORD(ob.getIndex(d_president));
		// f.putUWord(president);
		f << ob.getIndex(d_president);

#if 0
		if(f.getMode() == SaveGame::SaveGame)
		{
			CHECKUBYTE(control);
			f.putUByte(control);
		}
#endif

		for(int i = 0; i < Rank_HowMany; i++)
		{
			CHECKUWORD(d_rankCount[i]);
			f.putUWord(d_rankCount[i]);
		}
		CHECKUWORD(ob.getIndex(d_supremeLeader));
		// f.putUWord(supremeLeader);
		f << ob.getIndex(d_supremeLeader);
	}

	return f.isOK();
}


