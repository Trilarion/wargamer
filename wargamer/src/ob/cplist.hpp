/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef CPLIST_HPP
#define CPLIST_HPP

#ifndef __cplusplus
#error cplist.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *	CommandPosition List
 *
 *----------------------------------------------------------------------
 */

#include "gamedefs.hpp"
#include "obdefs.hpp"
#include "cp.hpp"
#include "poolarry.hpp"

class FileReader;
class FileWriter;
class BaseOrderBattle;

class GenericCPList :
	public PoolArray<GenericCP>
{
	typedef PoolArray<GenericCP> Super;
	typedef CPIndex Index;

public:
	GenericCPList() : Super() { }
	virtual ~GenericCPList();	// { CPIndex::setList(0); }

private:
	const char* getChunkName() const;

public:
	CPIndex add();
	Boolean readData(FileReader& f, BaseOrderBattle& ob);
	Boolean writeData(FileWriter& f, const BaseOrderBattle& ob) const;

	RefGenericCP operator [] (CPIndex spi) 
	{
		if(spi == NoCPIndex)
			return NoGenericCP;
		else
			return &Super::operator[](spi); 
	}

	ConstRefGenericCP operator [] (CPIndex spi) const
	{
		if(spi == NoCPIndex)
			return NoGenericCP;
		else
			return &Super::operator[](spi); 
	}

	CPIndex getIndex(const ConstRefGenericCP& cpi) const
	{
		if(cpi == NoGenericCP)
			return NoCPIndex;
		else
			return cpi->getSelf(); 
	}

	void cleanup();
		// Garbage collection
};

#endif /* CPLIST_HPP */

