/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
#include "stdinc.hpp"
#include "nations.hpp"
#include "scenario.hpp"
#include "wldfile.hpp"
#include "filebase.hpp"
#include "filecnk.hpp"
#include "savegame.hpp"

UWORD Nations::fileVersion = 0x0006;

Boolean Nations::read(FileReader& f)
{
   if(f.isAscii())
   {
      // Todo
   }
   else
   {
      UWORD version;
      f.getUWord(version);
      ASSERT(version <= fileVersion);
#if 0
      UBYTE b;
      f.getUByte(b);
      d_neverEnter = static_cast<Boolean>(b);
#else
      bool b;
      f >> b;
      d_neverEnter = b;
#endif
      f.getUByte(d_side);

      if(version < 0x0003)
      {
        Attribute rubbish;
        f.getUByte(rubbish);
      }

      if(version >= 0x0002)
        f.getUByte(d_friendlySide);
      else
        d_friendlySide = d_side;

      if(version >= 0x0005)
      {
        f.getUByte(d_nDepotsAllowed);
        f.getUByte(d_nDepotsBuilt);
        f.getUByte(d_nDepotsUC);
      }

      if(f.getMode() == SaveGame::SaveGame)
      {
        if(version >= 0x0003)
        {
          f.getUByte(d_morale);
          f.getUWord(d_nLeadersCreated);
        }

        if(version >= 0x0004)
        {
          f.getULong(d_tick);
        }

        if(version >= 0x0006)
        {
#if 0
          f.getUByte(b);
          d_wantsToDefect = static_cast<Boolean>(b);
#else
          f >> b;
          d_wantsToDefect = b;
#endif
        }
      }
   }

   return f.isOK();
}

Boolean Nations::write(FileWriter& f) const
{
   if(f.isAscii())
   {
      // ToDo
   }
   else
   {
     f.putUWord(fileVersion);
     f.putUByte(d_neverEnter);
     f.putUByte(d_side);
     f.putUByte(d_friendlySide);
     f.putUByte(d_nDepotsAllowed);
     f.putUByte(d_nDepotsBuilt);
     f.putUByte(d_nDepotsUC);

     if(f.getMode() == SaveGame::SaveGame)
     {
       f.putUByte(d_morale);
       f.putUWord(d_nLeadersCreated);
       f.putULong(d_tick);
       f.putUByte(d_wantsToDefect);
     }
   }

  return f.isOK();
}

UWORD NationsList::fileVersion = 0x0000;

Side NationsList::getAllegiance(Nationality n) const
{
  ASSERT(n < d_nations.entries());
  return d_nations[n].getSide();
}

void NationsList::setAllegiance(Nationality n, Side s)
{
  ASSERT(n < d_nations.entries());
  d_nations[n].setSide(s);
  d_nations[n].neverEnter(False);

  // set alliance in scenario data
  scenario->setAlliance(n, s);
}

void NationsList::setNeverEnter(Nationality n)
{
  ASSERT(n < d_nations.entries());
  d_nations[n].neverEnter(True);
}

void NationsList::wantsToDefect(Nationality n, Boolean f)
{
  ASSERT(n < d_nations.entries());
  d_nations[n].wantsToDefect(f);
}

Boolean NationsList::wantsToDefect(Nationality n) const
{
  ASSERT(n < d_nations.entries());
  return d_nations[n].wantsToDefect();
}

void NationsList::setMorale(Nationality n, Attribute morale)
{
  ASSERT(n < d_nations.entries());
  d_nations[n].setMorale(morale);
}

void NationsList::reactivateIn(Nationality n, ULONG t)
{
  ASSERT(n < d_nations.entries());
  d_nations[n].reactivateIn(t);
}

void NationsList::setNDepotsAllowed(Nationality n, UBYTE num)
{
  ASSERT(n < d_nations.entries());
  d_nations[n].setNDepotsAllowed(num);
}

void NationsList::setNDepotsBuilt(Nationality n, UBYTE num)
{
  ASSERT(n < d_nations.entries());
  d_nations[n].setNDepotsBuilt(num);
}

void NationsList::setNDepotsUC(Nationality n, UBYTE num)
{
  ASSERT(n < d_nations.entries());
  d_nations[n].setNDepotsUC(num);
}

void NationsList::incNDepotsBuilt(Nationality n)
{
  ASSERT(n < d_nations.entries());
  d_nations[n].incNDepotsBuilt();
}

void NationsList::decNDepotsBuilt(Nationality n)
{
  ASSERT(n < d_nations.entries());
  d_nations[n].decNDepotsBuilt();
}

void NationsList::incNDepotsUC(Nationality n)
{
  ASSERT(n < d_nations.entries());
  d_nations[n].incNDepotsUC();
}

void NationsList::decNDepotsUC(Nationality n)
{
  ASSERT(n < d_nations.entries());
  d_nations[n].decNDepotsUC();
}

Boolean NationsList::isActive(Nationality n) const
{
  return (n < d_nations.entries()) ? d_nations[n].active() : True;
}

Boolean NationsList::nationShouldNotEnter(Nationality n) const
{
  return (n < d_nations.entries()) ? d_nations[n].neverEnter() : True;
}

Boolean NationsList::shouldReactivate(Nationality n, ULONG t)
{
  ASSERT(n < d_nations.entries());
  return d_nations[n].shouldReactivate(t);
}

Attribute NationsList::getMorale(Nationality n) const
{
  ASSERT(n < d_nations.entries());
  return d_nations[n].getMorale();
}

Side NationsList::getFriendlyTo(Nationality n) const
{
  ASSERT(n < d_nations.entries());
  return d_nations[n].getFriendlySide();
}

UBYTE NationsList::getNDepotsAllowed(Nationality n) const
{
  ASSERT(n < d_nations.entries());
  return d_nations[n].nDepotsAllowed();
}

UBYTE NationsList::getNDepotsBuilt(Nationality n) const
{
  ASSERT(n < d_nations.entries());
  return d_nations[n].nDepotsBuilt();
}

UBYTE NationsList::getNDepotsUC(Nationality n) const
{
  ASSERT(n < d_nations.entries());
  return d_nations[n].nDepotsUC();
}

//----------- Unchecked update -------------
UWORD NationsList::nLeadersCreated(Nationality n) const
{
   if(n < d_nations.entries())
      return d_nations[n].nLeadersCreated();

   return 0;
}

void NationsList::incLeadersCreated(Nationality n)
{
   if(n < d_nations.entries())
      d_nations[n].incNLeadersCreated();
}
//------------------------------------------


Boolean NationsList::read(FileReader& f)
{
   if(f.isAscii())
   {
      // ToDo
   }
   else
   {
      FileChunkReader fr(f, getChunkName());
      if(!fr.isOK())
         return False;

      UWORD version;
      f.getUWord(version);
      ASSERT(version <= fileVersion);

      UWORD nNations;
      f.getUWord(nNations);

      d_nations.init(nNations);
      for(Nationality n = 0; n < nNations; n++)
      {
         d_nations[n].read(f);
         scenario->setAlliance(n, d_nations[n].getSide());     // Added SWG: 11Oct99
      }
   }

  return f.isOK();

}

Boolean NationsList::write(FileWriter& f) const
{
   if(f.isAscii())
   {
      // ToDo
   }
   else
   {
      FileChunkWriter fc(f, getChunkName());

      f.putUWord(fileVersion);

      UWORD nNations = d_nations.entries();
      f.putUWord(nNations);

      for(Nationality n = 0; n < nNations; n++)
      {
         d_nations[n].write(f);
      }
   }

  return f.isOK();
}

const char* NationsList::getChunkName()
{
  return allegianceChunkName;
}

/*
 *----------------------------------------------------------------------
 * $Log$
 *----------------------------------------------------------------------
 */
