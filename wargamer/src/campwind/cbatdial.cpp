/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 * Campaign Battle: Interactive Mode Dialog
 *
 * To be rewritten to use it;s own class instead of being
 * member functions of CampaignBattle, which goes against the practice
 * of defining member functions in the same source file.
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "cbatdial.hpp"

#include "app.hpp"
#include "gamectrl.hpp"
#include "campdint.hpp"
#include "measure.hpp"
#include "armies.hpp"
#include "cbatmsg.hpp"
#include "dialog.hpp"
#include "resdef.h"
#include "scenario.hpp"
#include "cbutton.hpp"
#include "cbattle.hpp"
#include "dib.hpp"
#include "dib_util.hpp"
#include "fonts.hpp"
#include "scenario.hpp"
#include "palette.hpp"
#include "wmisc.hpp"
#include "imglib.hpp"
#include "scn_res.h"
#include "mapwind.hpp"
#include "cwin_int.hpp"
#include "trackwin.hpp"
#include "scrnbase.hpp"
#include "town.hpp"



/*
 * Ask Player what he wants his units to do...
 *
 * This should be a modeless dialog box... so player can still interact
 * with the map to see information about units and things.
 *
 * It might be better to have Side as a parameter
 * so if the player is controlling both sides, he gets this dialog twice.
 *
 * Don't know if player gets a list of all his units in the battle
 * and can set each's mode seperately, or whether it will be one
 * global setting.
 */

 /*
 * Ask Player how he wants to play the battle
 *
 * Choice of Calculated, Interactive or Tactical.
 *
 * This should be a modeless dialog box... so player can still interact
 * with the map to see information about units and things.
 */


class BattleModeWindow : public InsertWind {
    CampaignWindowsInterface* d_campWind;
    const CampaignData* d_campData;
    MapWindow* d_mapWindow;
     ChooseBattleModeRequest* d_request;

    enum ID {
       ID_Calculated,
       ID_Tactical,

       ID_HowMany,

       ID_First = ID_Calculated
    };

  public:
    BattleModeWindow(CampaignWindowsInterface* campWind, const CampaignData* campData, MapWindow* mapWindow);
    ~BattleModeWindow();

    void run(ChooseBattleModeRequest* request);

    // virtual function from TrackWind
    void drawInsertInfo(const PixelRect& r);
  private:
    LRESULT procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
    void onCommand(HWND hwnd, int id, HWND hwndCtl, UINT codeNotify);

    void finish(BattleMode::Mode mode);
    void hide();
};

BattleModeWindow::BattleModeWindow(CampaignWindowsInterface* campWind, const CampaignData* campData, MapWindow* mapWindow) :
  d_campWind(campWind),
  d_campData(campData),
  d_mapWindow(mapWindow),
  d_request(0)
{
  // TODO: get strings from resource
  // TODO: strings from resource
  static IW_ButtonData s_buttonData[] = {
    { IDS_Calculated, 0,  ID_Calculated,   7, 20, 37, 10 },
    { IDS_Tactical, 0,    ID_Tactical,    95, 20, 37, 10 },
    { InGameText::Null, 0, 0, 0, 0, 0, 0 }
  };

  IW_Data iwd;
  iwd.d_hParent = APP::getMainHWND();
  iwd.d_cx = (ScreenBase::dbX() * 141) / ScreenBase::baseX();
  iwd.d_cy = (ScreenBase::dbY() * 37) / ScreenBase::baseY();
  iwd.d_flags = 0;   // IW_Data::DelayShow;
  iwd.d_bBorderColors = scenario->getBorderColors();
  iwd.d_bFillDib = scenario->getScenarioBkDIB(Scenario::BackPatterns::ButtonBackground);
  iwd.d_buttonData = s_buttonData;
  // set transparent color
  COLORREF c;
  scenario->getColour("MapInsert", c);
  iwd.d_insertColor = c;

  create(iwd);

}

BattleModeWindow::~BattleModeWindow()
{
    selfDestruct();
}

LRESULT BattleModeWindow::procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
//  LRESULT result;

  switch(msg)
  {
    HANDLE_MSG(hWnd, WM_CREATE, onCreate);
    HANDLE_MSG(hWnd, WM_PAINT, onPaint);
    HANDLE_MSG(hWnd, WM_ERASEBKGND, onEraseBk);
    HANDLE_MSG(hWnd, WM_TIMER, onTimer);
    HANDLE_MSG(hWnd, WM_COMMAND, onCommand);
    default:
      return defProc(hWnd, msg, wParam, lParam);
  }
}

void BattleModeWindow::finish(BattleMode::Mode mode)
{

    d_request->mode(mode);

    hide();
    d_request->signal();
    d_request = 0;

}

void BattleModeWindow::onCommand(HWND hwnd, int id, HWND hwndCtl, UINT codeNotify)
{
  switch(id)
  {
    case ID_Calculated:
        finish(BattleMode::Calculated);
      break;

    case ID_Tactical:
        finish(BattleMode::Tactical);
      break;
  }
}

void BattleModeWindow::hide()
{
  show(false);
}

void BattleModeWindow::drawInsertInfo(const PixelRect& r)
{
  // set font
  int fontHeight = (ScreenBase::dbY() * 9) / ScreenBase::baseY();

  LogFont lf;
  lf.height(fontHeight);
  lf.weight(FW_MEDIUM);
  lf.face(scenario->fontName(Font_Bold));

  Font font;
  font.set(lf);

  ASSERT(dib());
  HFONT oldFont = dib()->setFont(font);
  dib()->setBkMode(TRANSPARENT);

  // TODO: get text from resource
  const char* text = InGameText::get(IDS_ChooseBattleMethod);

  SIZE s;
  GetTextExtentPoint32(dib()->getDC(), text, lstrlen(text), &s);

  const int x = (width() - s.cx) / 2;
  const int y = (5 * ScreenBase::dbY()) / ScreenBase::baseY();

  wTextOut(dib()->getDC(), x, y, text);
  dib()->setFont(oldFont);
}

void BattleModeWindow::run(ChooseBattleModeRequest* request)
{
    ASSERT(d_request == 0);
    ASSERT(request);
    d_request = request;

    /*
     * run window
     */

    Location l = d_request->location();
    d_campWind->centerOnMap(l, d_mapWindow->getZoomEnum());

    PixelPoint p;
    d_mapWindow->locationToPixel(l, p);

    /*
     * Adjust position to top window's coordinates
     */

    POINT p2 = p;

    ClientToScreen(d_mapWindow->getHWND(), &p2);
    ScreenToClient(APP::getMainHWND(), &p2);

    position(p2);
}

/*
 *-----------------------------------------------------------
 *
 * End Of Battle Day Dialog
 */

class BattleDayDial : public InsertWind { //public ModelessDialog {
    CampaignWindowsInterface* d_campWind;
    const CampaignData* d_campData;
    MapWindow* d_mapWindow;

     BattleDayRequest* d_request;

    enum ID {
      ID_Withdraw,
      ID_Continue,

      ID_HowMany
    };

  public:

    BattleDayDial(CampaignWindowsInterface* campWind, const CampaignData* campData, MapWindow* mapWindow);
    ~BattleDayDial() { }

    void run(BattleDayRequest* request);

private:
    // virtual function from TrackWind
    void drawInsertInfo(const PixelRect& r);
  private:
    LRESULT procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
    void onCommand(HWND hwnd, int id, HWND hwndCtl, UINT codeNotify);

};


BattleDayDial::BattleDayDial(CampaignWindowsInterface* campWind, const CampaignData* campData, MapWindow* mapWindow) :
  d_campWind(campWind),
  d_campData(campData),
  d_mapWindow(mapWindow),
  d_request(0)
{
  ASSERT(d_campWind);
  ASSERT(d_campData);
  ASSERT(d_mapWindow);
  // TODO: get strings from resource
  // TODO: strings from resource
  static IW_ButtonData s_buttonData[] = {
    { IDS_Withdraw, 0, ID_Withdraw,   22, 92, 37, 10 },
    { IDS_Continue, 0, ID_Continue,   81, 92, 37, 10 },
    { InGameText::Null, 0, 0, 0, 0, 0, 0 }
  };

  IW_Data iwd;
  iwd.d_hParent = APP::getMainHWND();
  iwd.d_cx = (ScreenBase::dbX() * 141) / ScreenBase::baseX();
  iwd.d_cy = (ScreenBase::dbY() * 107) / ScreenBase::baseY();
  iwd.d_flags = 0;   // IW_Data::DelayShow;
  iwd.d_bBorderColors = scenario->getBorderColors();
  iwd.d_bFillDib = scenario->getScenarioBkDIB(Scenario::BackPatterns::ButtonBackground);
  iwd.d_buttonData = s_buttonData;
  // set transparent color
  COLORREF c;
  scenario->getColour("MapInsert", c);
  iwd.d_insertColor = c;

  create(iwd);

}

LRESULT BattleDayDial::procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
//  LRESULT result;

  switch(msg)
  {
    HANDLE_MSG(hWnd, WM_CREATE, onCreate);
    HANDLE_MSG(hWnd, WM_PAINT, onPaint);
    HANDLE_MSG(hWnd, WM_ERASEBKGND, onEraseBk);
    HANDLE_MSG(hWnd, WM_TIMER, onTimer);
    HANDLE_MSG(hWnd, WM_COMMAND, onCommand);
    default:
      return defProc(hWnd, msg, wParam, lParam);
  }
}

void BattleDayDial::onCommand(HWND hwnd, int id, HWND hwndCtl, UINT codeNotify)
{
  switch(id)
  {
    case ID_Withdraw:
    case ID_Continue:
    {
        ASSERT(d_request->currentSide() != SIDE_Neutral);

      d_request->setResult( (id == ID_Withdraw) ?
                EndOfDayMode::Withdraw : EndOfDayMode::Continue);

        d_request->signal();
        show(false);
        d_request = 0;
      break;
    }
  }
}

Side getOtherSide(Side s)
{
  ASSERT(s != SIDE_Neutral);
  return (s == 0) ? 1 : 0;
}

void BattleDayDial::drawInsertInfo(const PixelRect& r)
{
  ASSERT(d_request);
  ASSERT(d_request->town() != NoTown);

  const CampaignBattleUnitList& bul = d_request->unitList(d_request->currentSide());
  const CampaignBattleUnitList& otherBul = d_request->unitList(getOtherSide(d_request->currentSide()));

  ASSERT(dib());
  dib()->setBkMode(TRANSPARENT);

  /*
   * Spme useful constants
   */

  const Town& t = d_campData->getTown(d_request->town());

  const LONG dbX = ScreenBase::dbX();
  const LONG dbY = ScreenBase::dbY();
  const LONG baseX = ScreenBase::baseX();
  const LONG baseY = ScreenBase::baseY();

  const int startX = (3 * dbX) / baseX;

//   static const char vs[] = "vs:";     // move this to string resource
//   static const char manpower[] = "Manpower:";     // move this to string resource
//   static const char guns[] = "Guns:"; // move this to string resource
//   static const char situation[] = "Situation";  // move this to string resource
//   static const char morale[] = "Morale";  // move this to string resource

  /*
   * Get fonts
   */

  int fontHeight = (dbY*9)/baseY;

  LogFont lf;
  lf.height(fontHeight);
  lf.weight(FW_MEDIUM);
  lf.italic(true);
  lf.underline(true);
  lf.face(scenario->fontName(Font_Bold));

  Font font;
  font.set(lf);

  HFONT oldFont = dib()->setFont(font);
  COLORREF oldColor = dib()->setTextColor(scenario->getSideColour(bul.cinc()->getSide()));


  // TODO: get string from resource
  SimpleString text = InGameText::get(IDS_BattleDayEnds);
//  const char* text = "Battle Day Ends";
  SIZE s;
  GetTextExtentPoint32(dib()->getDC(), text.toStr(), lstrlen(text.toStr()), &s);

  int x = (width() - s.cx) / 2;
  int y = (3 * dbY) / baseY;

  wTextOut(dib()->getDC(), x, y, width() - (startX * 2), text.toStr());

  y += ((10 * dbY) / baseY);

  /*
   * Put our leaders name and nation flag
   */

  fontHeight = (dbY * 8)/baseY;

  lf.height(fontHeight);
  lf.italic(false);
  lf.underline(false);
  font.set(lf);

  dib()->setFont(font);
  dib()->setTextColor(oldColor);

  // get text size for leaders name
  text = bul.cinc()->getNameNotNull();
  GetTextExtentPoint32(dib()->getDC(), text.toStr(), lstrlen(text.toStr()), &s);

  int w = (FI_Army_CX + startX) + s.cx;
  x = (width() - w) / 2;

  const ImageLibrary* il = scenario->getNationFlag(bul.cinc()->getNation(), True);
  il->blit(dib(), FI_Army, x, y, True);

  x += (FI_Army_CX + startX);
  wTextOut(dib()->getDC(), x, y, width() - (x + startX), text.toStr());

  y += fontHeight;

  /*
   *     Put 'vs'
   */

//  text = vs;
//  ASSERT(text);
  text = InGameText::get(IDS_VersusAbrev);
  text += ':';

  GetTextExtentPoint32(dib()->getDC(), text.toStr(), lstrlen(text.toStr()), &s);

  x = (width() - s.cx) / 2;
  wTextOut(dib()->getDC(), x, y, text.toStr());

  y += fontHeight;

  /*
   *  Put other sides leader
   */

  text = otherBul.cinc()->getNameNotNull();
  GetTextExtentPoint32(dib()->getDC(), text.toStr(), lstrlen(text.toStr()), &s);

  w = (FI_Army_CX + startX) + s.cx;
  x = (width() - w) / 2;

  il = scenario->getNationFlag(otherBul.cinc()->getNation(), True);
  il->blit(dib(), FI_Army, x, y, True);

  x += (FI_Army_CX + startX);
  wTextOut(dib()->getDC(), x, y, width() - (x + startX), text.toStr());

  y += (2 * fontHeight);


  /*
   * Put place of battle
   */

  x = startX;

  lf.underline(true);
  lf.italic(true);
  font.set(lf);

  dib()->setFont(font);

  // TODO: get from string resource
  text = InGameText::get(IDS_BattleOfTown);
  wTextPrintf(dib()->getDC(), x, y, (width() - (2 * x)), text.toStr(), t.getNameNotNull());

  y += fontHeight;

  lf.underline(false);
  lf.italic(false);
  font.set(lf);

  dib()->setFont(font);

  /*
   * Put strength info.
   */

   text = InGameText::get(IDS_Manpower);
   text += ':';
  const int infoOffset = ((40 * dbX) / baseX);
  wTextOut(dib()->getDC(), x, y, text.toStr());

  LONG mp = (bul.nInfantry() * UnitTypeConst::InfantryPerSP) +
            (bul.nCavalry() * UnitTypeConst::CavalryPerSP) +
            (bul.nArtillery() * UnitTypeConst::ArtilleryPerSP) +
            (bul.nOther() * UnitTypeConst::SpecialPerSP);

  LONG startMP = (bul.nStartInfantry() * UnitTypeConst::InfantryPerSP) +
                 (bul.nStartCavalry() * UnitTypeConst::CavalryPerSP) +
                 (bul.nStartArtillery() * UnitTypeConst::ArtilleryPerSP) +
                 (bul.nStartOther() * UnitTypeConst::SpecialPerSP);

  wTextPrintf(dib()->getDC(), infoOffset, y, "%ld / %ld", mp, startMP);

  /*
   * Put number of guns
   */

  y += fontHeight;
  text = InGameText::get(IDS_Guns);
  text += ':';
  wTextOut(dib()->getDC(), x, y, text.toStr());
  int nGuns = (bul.nArtillery() * UnitTypeConst::GunsPerSP);
  int nStartGuns = (bul.nStartArtillery() * UnitTypeConst::GunsPerSP);
  wTextPrintf(dib()->getDC(), infoOffset, y, "%d / %d", nGuns, nStartGuns);


  /*
   * Put morale info.
   */

  y += fontHeight;
  text = InGameText::get(IDS_Morale);
  text += ':';
  wTextOut(dib()->getDC(), x, y, text.toStr());

  //put bar chart
  const LONG barCX  =  (40 * dbX) / baseX;
  const LONG barCY = (5 * dbY) / baseY;

  dib()->drawBarChart(infoOffset, y + 1, bul.currentMorale(), bul.startMorale(), barCX, barCY);

  /*
   * Put situation text.
   */

  y += fontHeight;

#if 0
  static const char* s_sitText[] = {
    "Winning Decisively",
    "Winning Marginally",
    "Losing Decisively",
    "Losing Marginally",
    "Undetermined"
  };
#endif

  text = InGameText::get(IDS_Situation);
  wTextOut(dib()->getDC(), x, y, text.toStr());

  int status = 0;

  if(bul.statusCount() < -2)
    status = BattleStatus::LosingBig;
  else if(bul.statusCount() < 0)
    status = BattleStatus::LosingSmall;
  else if(bul.statusCount() > 2)
    status = BattleStatus::WinningBig;
  else if(bul.statusCount() > 0)
    status = BattleStatus::WinningSmall;
  else
    status = BattleStatus::Undecided;

//  wTextOut(dib()->getDC(), infoOffset, y, s_sitText[status]);
  wTextOut(dib()->getDC(), infoOffset, y, InGameText::get(status + IDS_CBAT_SITUATION));

  dib()->setFont(oldFont);
  dib()->setTextColor(oldColor);
}

void BattleDayDial::run(BattleDayRequest* bd)
{
  ASSERT(bd);
  ASSERT(bd->currentSide() != SIDE_Neutral);

  d_request = bd;

  const CampaignBattleUnitList& bul = d_request->unitList(d_request->currentSide());

  ASSERT(bul.cinc() != NoLeader);
  ASSERT(d_campData->getArmies().isPlayerUnit(bul.cinc()->getCommand()));

  ICommandPosition cpi = bul.cinc()->getCommand();

  Location l;
  cpi->getLocation(l);
  d_campWind->centerMapOnUnit(cpi);

  PixelPoint p;
  d_mapWindow->locationToPixel(l, p);

  POINT p2 = p;

  ClientToScreen(d_mapWindow->getHWND(), &p2);
  ScreenToClient(APP::getMainHWND(), &p2);


  position(p2);

}


/*-----------------------------------------------------
 * Class to hold static instance of BattleDayDial
 */

class BattleDayInterface {
    BattleDayDial* d_battleDial;
    BattleModeWindow* d_battleMode;
  public:

    BattleDayInterface() :
      d_battleDial(0),
      d_battleMode(0) {}

    ~BattleDayInterface() {}

    void init(CampaignWindowsInterface* campWind, const CampaignData* campData, MapWindow* mapWindow)
    {
      d_battleDial = new BattleDayDial(campWind, campData, mapWindow);
      ASSERT(d_battleDial);

      d_battleMode = new BattleModeWindow(campWind, campData, mapWindow);
      ASSERT(d_battleMode);
    }

     void run(BattleDayRequest* r) { d_battleDial->run(r); }
     void run(ChooseBattleModeRequest* r) { d_battleMode->run(r); }

    void destroy()
    {
      if(d_battleDial)
      {
          delete d_battleDial;
        d_battleDial = 0;
      }

      if(d_battleMode)
      {
          delete d_battleMode;
        d_battleMode = 0;
      }
    }
    Boolean initiated() const { return ((d_battleDial != 0) && (d_battleMode != 0)); }
};

static BattleDayInterface s_battleDial;

/*-----------------------------------------------------------
 * Access class
 */

void CampaignBattleDial::create(CampaignWindowsInterface* campWind, const CampaignData* campData, MapWindow* mapWindow)
{
  ASSERT(!s_battleDial.initiated());
  s_battleDial.init(campWind, campData, mapWindow);
}


void CampaignBattleDial::destroy()
{
  ASSERT(s_battleDial.initiated());
  s_battleDial.destroy();
}

/*
 * Implementation of cbatmsg
 */

void ChooseBattleModeRequest::run()
{
    ASSERT(s_battleDial.initiated());
    s_battleDial.run(this);
}

void BattleDayRequest::run()
{
    ASSERT(s_battleDial.initiated());
    s_battleDial.run(this);
}

Location ChooseBattleModeRequest::location() const
{
    Location l;
    d_battle->getLocation(l);
    return l;
}

const CampaignBattleUnitList& BattleDayRequest::unitList(Side s) const
{
    return d_data.d_units[s];
}

ITown BattleDayRequest::town() const
{
    return d_data.d_town;
}

Side BattleDayRequest::currentSide() const
{
    return d_data.d_whichSide;
}

Side BattleDayRequest::victor() const
{
    return d_data.d_victor;
}


/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.2  2001/06/18 08:25:44  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
