/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef FIND_HPP
#define FIND_HPP

class FindWind;
class CampaignWindowsInterface;
class CampaignData;

class FindItem {
	 FindWind* d_findWind;
  public:
	 enum Type {
		 FindTown,
		 FindLeader,

		 HowMany
	 };

	 FindItem(CampaignWindowsInterface* campWind, const CampaignData* campData);
	 ~FindItem();

	 // void init();
	 void run(Type type);
     void hide();
	 // void destroy();
	 Type getType() const;
	 HWND getHWND() const;
};

#endif
