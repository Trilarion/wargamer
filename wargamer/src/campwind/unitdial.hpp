/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef UNITDIAL
#define UNITDIAL

//#include "grtypes.hpp"
#include "cdialog.hpp"
#include "dialog.hpp"
// #include "icompos.hpp"
#include "gamedefs.hpp"
#include "cpdef.hpp"

class CampaignUserInterface;
class CampaignData;
class CampaignOrder;
class StackedUnitList;
class CustomDialog;
class CustomDrawnCombo;
class CampaignWindowsInterface;
class PixelPoint;
class CampaignComboInterface;

/*
 * User Interface Unit Dialogs. Includes:
 *   1. Main Dialog

 *   2. Move-Order Dialog
 */



/*
 * Base class for unit dialogs.
 */

class UnitDialog : public ModelessDialog {
	 CampaignUserInterface* d_owner;
	 const CampaignData* d_campData;
	 CampaignOrder& d_order;
	 CampaignComboInterface& d_combos;
	 CustomDialog d_customDialog;   // dialog subclass routines

	 static DrawDIBDC* s_dib;       // a dib for anything that needs drawing

	 RECT d_tdRect;                 // rect for tabbled dialog
	 Boolean d_unitListExpanded;
  public:
	 class UnitDialogType {
		public:
		  enum Type {
			 First = 0,

			 MoveOrderDialog = First,
			 MainOrderDialog,

			 HowMany
		  };
	 };

	 UnitDialog(CampaignUserInterface* owner, const CampaignData* campData, CampaignOrder& order, CampaignComboInterface& combos) :
		d_owner(owner),
		d_campData(campData),
		d_order(order),
		d_combos(combos),
		d_unitListExpanded(False)
	 {
		SetRect(&d_tdRect, 0, 0, 0, 0);
	 }

	 virtual ~UnitDialog() {}

	 void initCombo(StackedUnitList& list, Boolean all, Boolean showManpower);
	 void hide();
	 void destroy();
	 ICommandPosition currentUnit() const;

	 virtual void showOrderPage(const PixelPoint& p) {}
	 virtual void show(const PixelPoint& p);
	 virtual void initControls() = 0;
	 virtual void redrawCombo() = 0;

	 virtual BOOL procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam) = 0;

	 RECT& tabbedDialogRect() { return d_tdRect; }

	 CampaignWindowsInterface* campWindows() const;
	 CampaignOrder& order() { return d_order; }
	 const StackedUnitList& getUnitList();
	 CampaignComboInterface& combos() { return d_combos; }
	 DrawDIBDC* dib() const { return s_dib; }
	 void allocateStaticDib(int cx, int cy);

	 static UnitDialog* create(
		 UnitDialogType::Type type,
		 CampaignUserInterface* owner,
		 const CampaignData* campData,
		 CampaignOrder& d_order,
		 CampaignComboInterface& combos);

  protected:
	 void drawCombo(const DrawDIB* fillDib, const DRAWITEMSTRUCT* lpDrawItem, Boolean showManpower = False);
	 void updateCaption();
	 void showComboList(HWND hwndCtl);
	 void deleteStaticDib();

	 CampaignUserInterface* owner() const { return d_owner; }
	 const CampaignData* campData() const { return d_campData; }
	 CustomDialog& customDialog() { return d_customDialog; }
	 Boolean unitListExpanded() const { return d_unitListExpanded; }
};

#endif
