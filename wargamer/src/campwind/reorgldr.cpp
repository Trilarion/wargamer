/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
#include "stdinc.hpp"
#include "reorgldr.hpp"
#include "wind.hpp"
#include "winctrl.hpp"
#include "app.hpp"
#include "cbutton.hpp"
#include "scenario.hpp"
#include "fillwind.hpp"
#include "dib.hpp"
#include "dib_util.hpp"
#include "campdint.hpp"
#include "palette.hpp"
#include "scn_res.h"
#include "fonts.hpp"
#include "wmisc.hpp"
#include "itemwind.hpp"
#include "dragwin.hpp"
#include "armies.hpp"
#include "imglib.hpp"
#include "control.hpp"
#include "route.hpp"
#include "gamectrl.hpp"
#include "scrnbase.hpp"
#include "cwin_int.hpp"
#include "scn_img.hpp"
#include "resstr.hpp"

#ifdef DEBUG
#include "logwin.hpp"
#endif
//#include "grtypes.hpp"

/*
 * Internal items, containers
 */

// for leader item list
struct ILeaderItem : public SLink {
   const IndependentLeader* d_leader;

   enum { ChunkSize = 10 };

   ILeaderItem(const IndependentLeader* il) :
     d_leader(il) {}
   ~ILeaderItem() {}

   /*
    * Allocators
    */

   void* operator new(size_t size);
#ifdef _MSC_VER
   void operator delete(void* deadObject);
#else
   void operator delete(void* deadObject, size_t size);
#endif
};

#ifdef DEBUG
static FixedSizeAlloc<ILeaderItem> s_iLeaderAlloc("ILeaderItem");
#else
static FixedSizeAlloc<ILeaderItem> s_iLeaderAlloc;
#endif  // DEBUG

void* ILeaderItem::operator new(size_t size)
{
  return s_iLeaderAlloc.alloc(size);
}

#ifdef _MSC_VER
void ILeaderItem::operator delete(void* deadObject)
{
  s_iLeaderAlloc.release(deadObject);
}
#else
void ILeaderItem::operator delete(void* deadObject, size_t size)
{
  s_iLeaderAlloc.release(deadObject, size);
}
#endif

class ILeaderItemList : public SList<ILeaderItem> {
};

// for unit item list---------------------------
struct ILUnitItem : public SLink {
   ConstICommandPosition d_cpi;
   const IndependentLeader* d_newLeader;

   enum { ChunkSize = 25 };

   ILUnitItem() :
     d_cpi(NoCommandPosition),
     d_newLeader(0) {}
   ~ILUnitItem() {}

   /*
    * Allocators
    */

   void* operator new(size_t size);
#ifdef _MSC_VER
   void operator delete(void* deadObject);
#else
   void operator delete(void* deadObject, size_t size);
#endif
};

#ifdef DEBUG
static FixedSizeAlloc<ILUnitItem> s_iUnitAlloc("ILUnitItem");
#else
static FixedSizeAlloc<ILUnitItem> s_iUnitAlloc;
#endif  // DEBUG

void* ILUnitItem::operator new(size_t size)
{
  return s_iUnitAlloc.alloc(size);
}

#ifdef _MSC_VER
void ILUnitItem::operator delete(void* deadObject)
{
  s_iUnitAlloc.release(deadObject);
}
#else
void ILUnitItem::operator delete(void* deadObject, size_t size)
{
  s_iUnitAlloc.release(deadObject, size);
}
#endif


class ILUnitItemList : public SList<ILUnitItem> {
};

/*-----------------------------------------------------------------
 * Utilities
 */

/*
 * Some useful values
 */

#if 0
const LONG dbUnits = GetDialogBaseUnits();
const LONG dbX = LOWORD(dbUnits);
const LONG dbY = HIWORD(dbUnits);
#endif

class RLUtil {
public:
  // const int dbX;
  // const int dbY;
  // const int baseX;
  // const int baseY;
  const int shadowWidth;
  const int shadowHeight;
  const int scrollCX;
  const int headerFontCY;
  const int headerY;
  const int infoX;
  const int infoY;
  const int infoCX;
  const int infoCY;

  RLUtil::RLUtil() :
    // dbX(ScreenBase::dbX()),
    // dbY(ScreenBase::dbY()),
    // baseX(ScreenBase::baseX()),
    // baseY(ScreenBase::baseY()),
    shadowWidth(ScreenBase::x(3)),      // * dbX) / baseX),
    shadowHeight(ScreenBase::x(3)),     // shadowWidth),
    scrollCX(ScreenBase::x(5)),         // * dbX) / baseX),
    headerFontCY(ScreenBase::y(11)),    // * dbY) / baseY),
    headerY(ScreenBase::y(3)),          // * dbY) / baseY),
    infoX(ScreenBase::x(3)),            // * dbX) / baseX),
    infoY(ScreenBase::y(64)),           // * dbY) / baseY),
//    infoY(ScreenBase::y(60)),           // * dbY) / baseY),
    infoCX(ScreenBase::x(108)),         // * dbX) / baseX),
//    infoCY(ScreenBase::y(72))           // * dbY) / baseY)
    infoCY(ScreenBase::y(68))           // * dbY) / baseY)
  {
  }

  static HWND createButton(HWND hParent, const char* text, int id, int x, int y, int cx, int cy);
  static void drawInfo(const CampaignData* campData, HDC hdc, RLUtil& ru);
  static void drawAttributeGraph(HDC hdc, DrawDIB* dib, const char* text, int x, int y, Attribute value, RLUtil& ru);
};

void RLUtil::drawInfo(const CampaignData* campData, HDC hdc, RLUtil& ru)
{
  LONG x = ScreenBase::x(2);    // * ru.dbX) / ru.baseX;
  LONG y = ScreenBase::y(2);    // * ru.dbY) / ru.baseY;

  COLORREF color;
  if(!scenario->getColour("MapInsert", color))
    color = RGB(255,255,255);

  COLORREF oldColor = SetTextColor(hdc, color);

  /*
   * draw header text
   */

  LogFont lf;
  lf.height(ru.headerFontCY);
  lf.weight(FW_MEDIUM);
  lf.face(scenario->fontName(Font_Bold));
  lf.italic(false);     // true
  lf.underline(true);

  Font font;
  font.set(lf);

  SetBkMode(hdc, TRANSPARENT);
  HFONT oldFont = (HFONT)SelectObject(hdc, font);

  const char* text = InGameText::get(IDS_IndependentLeaders);   // TODO: needs to go in string resource
  wTextPrintf(hdc, (x + FI_Army_CX) + ScreenBase::x(3), // ((3 * ru.dbX) / ru.baseX),
     ru.headerY, "%s", text);

  SelectObject(hdc, oldFont);
  SetTextColor(hdc, oldColor);
}


HWND RLUtil::createButton(HWND hParent, const char* text, int id, int x, int y, int cx, int cy)
{
  HWND hButton = CreateWindow(CUSTOMBUTTONCLASS, text,
            WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON,
            x, y, cx, cy,
            hParent, (HMENU)id, APP::instance(), NULL);

  ASSERT(hButton != 0);

  CustomButton cb(hButton);
  // cb.setFillDib(scenario->getScenarioBkDIB(Scenario::BackPatterns::WoodBackground));
  cb.setFillDib(scenario->getScenarioBkDIB(Scenario::BackPatterns::ButtonBackground));
  cb.setBorderColours(scenario->getBorderColors());

  return hButton;
}

void RLUtil::drawAttributeGraph(HDC hdc, DrawDIB* dib, const char* text, int x, int y, Attribute value, RLUtil& ru)
{
  ASSERT(hdc);
  ASSERT(dib);

  const int barOffsetX = x + ScreenBase::x(50);  // * ru.dbX) / ru.baseX);
  const int barCX = ScreenBase::x(50);      //  * ru.dbX) / ru.baseX);
  const int barCY = ScreenBase::y(5);       // * ru.dbY) / ru.baseY);

  wTextOut(hdc, x, y, text);
  dib->drawBarChart(barOffsetX, y + 1, value, Attribute_Range, barCX, barCY);
}

/*----------------------------------------------------------------
 * Our Drag List windows
 */

struct LeaderItemData {
  const ILeaderItemList* d_iLeaders;
  const ILUnitItemList* d_units;

  LeaderItemData() :
    d_iLeaders(0),
    d_units(0) {}
};

class LeaderItemListBox : public ItemWindow {
        const CampaignData* d_campData;
        const UINT d_listCX;
        const UINT d_listCY;
        enum Type {
            LeaderList,
            UnitList
        } d_type;

    public:
        LeaderItemListBox(const ItemWindowData& data, const CampaignData* campData, const SIZE& s) :
            ItemWindow(data, s),
            d_campData(campData),
            d_listCX(s.cx),
            d_listCY(s.cx),
            d_type(LeaderList) {}
        ~LeaderItemListBox() {}

        const CampaignData* campData() const { return d_campData; }

        void init(const void* data);

    private:
        void drawListItem(const DRAWITEMSTRUCT* lpDrawItem);
        void drawCombo(DrawDIBDC* dib, const void* data) {}
        void drawItem(DrawDIBDC* dib, const PixelRect& r, ListBox& lb, int id, bool dragMode);

        virtual void drawDragItem(DrawDIBDC* dib, ListBox& lb, int id);
};

inline LPARAM ilToLPARAM(const ILeaderItem* leader)
{
   return reinterpret_cast<LPARAM>(const_cast<ILeaderItem*>(leader));
}

inline LPARAM uiToLPARAM(const ILUnitItem* unit)
{
   return reinterpret_cast<LPARAM>(const_cast<ILUnitItem*>(unit));
}

void LeaderItemListBox::init(const void* dataPtr)
{
  ASSERT(d_campData);
  ASSERT(dataPtr);

  const LeaderItemData* ld = reinterpret_cast<const LeaderItemData*>(dataPtr);
  ASSERT(ld->d_iLeaders || ld->d_units);

  d_type = (ld->d_iLeaders) ? LeaderList : UnitList;

  ListBox lb(getHWND(), ItemListBox);
  lb.reset();

  if(d_type == LeaderList)
  {
    ASSERT(ld->d_iLeaders);
    ASSERT(!ld->d_units);

    /*
     * Fill List box
     */

    SListIterR<ILeaderItem> iter(ld->d_iLeaders);
    while(++iter)
    {
      ILeader leader = iter.current()->d_leader->leader();
      lb.add(leader->getNameNotNull(), ilToLPARAM(iter.current()));
    }
  }
  else
  {
    ASSERT(ld->d_units);

    /*
     * Fill List box
     */

    SListIterR<ILUnitItem> iter(ld->d_units);
    while(++iter)
    {
      lb.add("", uiToLPARAM(iter.current()));
    }
  }

  if(lb.getNItems() > 0)
  {
    lb.set(0);
    setCurrentValue();
  }

#if 0   // make full size
  /*
   * Set position of list box
   */

  int itemsShowing = minimum(data().d_maxItemsShowing, lb.getNItems());
  const LONG itemHeight = data().d_itemCY*itemsShowing;
  SetWindowPos(lb.getHWND(), HWND_TOP, 0, 0, d_listCX, itemHeight, SWP_NOMOVE);
#endif
  SetWindowPos(lb.getHWND(), HWND_TOP, 0, 0, d_listCX, d_listCY, SWP_NOMOVE);
}

void LeaderItemListBox::drawDragItem(DrawDIBDC* dib, ListBox& lb, int id)
{
    PixelRect r;
    ListBox_GetItemRect(lb.getHWND(), id, &r);

    ColourIndex mask = *fillDib()->getBits();
    dib->fill(mask);
    dib->setMaskColour(mask);

    drawItem(dib,
        PixelRect(0, 0, r.width(), r.height()),
        lb, id, true);
}

void LeaderItemListBox::drawListItem(const DRAWITEMSTRUCT* lpDrawItem)
{
    ListBox lb(lpDrawItem->hwndItem);

    const int itemCX = lpDrawItem->rcItem.right-lpDrawItem->rcItem.left;
    const int itemCY = lpDrawItem->rcItem.bottom-lpDrawItem->rcItem.top;


    /*
     * Allocate dib if needed
     */

    DrawDIBDC* dib = allocateItemDib(itemCX, itemCY);

    ASSERT(dib);
    ASSERT(fillDib());

    dib->rect(0, 0, itemCX, itemCY, fillDib());

    if( !(lpDrawItem->itemState & ODS_SELECTED) )
        dib->darkenRectangle(0, 0, itemCX, itemCY, 20);


    drawItem(dib, PixelRect(0,0,itemCX,itemCY), lb, lpDrawItem->itemID, false);

    BitBlt(lpDrawItem->hDC, lpDrawItem->rcItem.left, lpDrawItem->rcItem.top, itemCX, itemCY,
         dib->getDC(), 0, 0, SRCCOPY);

}

void LeaderItemListBox::drawItem(DrawDIBDC* dib, const PixelRect& r, ListBox& lb, int id, bool dragMode)
{
    if(lb.getNItems() > 0)
    {
        char text[200];

        /*
         * Get screen base and other useful constants
         */

        const int dbX = ScreenBase::dbX();
        const int dbY = ScreenBase::dbY();
        const int baseX = ScreenBase::baseX();
        const int baseY = ScreenBase::baseY();
        TEXTMETRIC tm;
        GetTextMetrics(dib->getDC(), &tm);
        const int xMargin = (2 * dbX) / baseX;
        const int xOffset = (8 * dbX) / baseX;

        const ImageLibrary* flags = 0;
        UWORD imageID = 0;

        int flagX = xMargin;

        if(d_type == LeaderList)
        {
            const ILeaderItem* il = reinterpret_cast<const ILeaderItem*>(lb.getValue(id));
            ILeader leader = il->d_leader->leader();
            lstrcpy(text, leader->getNameNotNull());
            flags = scenario->getNationFlag(leader->getNation(), True);
            imageID = FI_Division;
        }
        else
        {
            static int imageRankOffset[Rank_HowMany] = {
                    FI_ArmyGroup,    // God
                    FI_ArmyGroup,    // President
                    FI_ArmyGroup,    // Group
                    FI_Army,         // Army
                    FI_Corps,        // Corps
                    FI_Division      // Division
            };

            ILUnitItem* ui = reinterpret_cast<ILUnitItem*>(lb.getValue(id));

            imageID = imageRankOffset[ui->d_cpi->getRank().getRankEnum()];
            flags = scenario->getNationFlag(ui->d_cpi->getNation(), True);

            /*
             * Draw lines
             *
             *
             * Set x offset
             */

            flagX = xMargin;
            int nOffsets = 0;

            // first, count the nunber of offsets
            ICommandPosition parentCPI = ui->d_cpi->getParent();
            while(parentCPI->isLower(Rank_President))
            {
                flagX += xOffset;
                nOffsets++;
                parentCPI = parentCPI->getParent();
            }

            if(!dragMode)
            {
                // more useful constants
                const int xLineMargin = xMargin + (FI_Army_CX / 2);
                const int vw = ScreenBase::x(5);
                int vh = r.height();
                const ColourIndex ci = dib->getColour(PALETTERGB(0, 0, 0));

                // now draw appropriate lines
                parentCPI = ui->d_cpi->getParent();
                ICommandPosition thisCPI = parentCPI;
                while(nOffsets--)
                {
                    ASSERT(parentCPI != NoCommandPosition);

                    const int x = xLineMargin + (xOffset * nOffsets);
                    const int y = 0;

                    // if current objects parent
                    if(parentCPI == ui->d_cpi->getParent())
                    {
                        if(ui->d_cpi->getSister() == NoCommandPosition)
                        {
                            vh = r.height() / 2;
                        }

                        dib->hLine(x, r.height() / 2, vw, ci);
                        dib->vLine(x, y, vh, ci);
                    }
                    else
                    {
                        ASSERT(thisCPI != NoCommandPosition);

                        ICommandPosition sisCPI = thisCPI->getSister();
                        if(sisCPI != NoCommandPosition)
                        {
                            dib->vLine(x, y, r.height(), ci);
                        }
                    }

                    thisCPI = parentCPI;
                    parentCPI = parentCPI->getParent();
                }
            }

            const char* leaderName = (ui->d_newLeader == 0) ?
                ui->d_cpi->getLeader()->getNameNotNull() :
                ui->d_newLeader->leader()->getNameNotNull();

            wsprintf(text, "%s (%s)",
                ui->d_cpi->getNameNotNull(), leaderName);
        }

        int w;
        int h;
        flags->getImageSize(imageID, w, h);

        int y = (r.height() - h) / 2;
        flags->blit(dib, imageID, flagX, y, True);

        y = (r.height() - tm.tmHeight) / 2;
        wTextOut(dib->getDC(), flagX + FI_Army_CX + xMargin, y, text);

    }
}


/*----------------------------------------------------------
 * Actual Reorganize leader window
 */

class Reorg_LeaderWindow : public DragWindow    // WindowBaseND
{
    typedef DragWindow Super;

    HWND d_hParent;
    const CampaignData* d_campData;
    CampaignWindowsInterface* d_campWind;

//  SeeThroughWindow d_seeThroughWindow;
    FillWindow d_fillWind;

    LeaderItemListBox* d_iLeaderLB;
    LeaderItemListBox* d_unitsLB;

    ILeaderItemList d_iLeaders;
    ILUnitItemList d_units;

    PixelPoint d_p1;
    PixelPoint d_p2;

//    UINT d_dragMsgID;

//    DrawDIB* d_dragItemDib;
    HFONT d_dragFont;

    DrawDIB* d_infoDib;
    HDC d_dibDC;

//    int d_itemID;

//    POINT d_lastP;

    Boolean d_orderGiven;
    enum ID {
       List1,
       List2,
       Button_First,
       Cancel = Button_First,
       // Apply,
       Send,
       Button_Last = Send,
       ID_HowMany,
       Button_HowMany = (Button_Last - Button_First) + 1
    };

    RLUtil d_ru;

  public:
    Reorg_LeaderWindow(HWND hParent, CampaignWindowsInterface* cw, const CampaignData* campData);
    ~Reorg_LeaderWindow()
    {
       delete d_iLeaderLB;
       d_iLeaderLB = 0;
       delete d_unitsLB;
       d_unitsLB = 0;

        selfDestruct();

//      if(d_dragItemDib)
//        delete d_dragItemDib;

      if(d_infoDib)
        delete d_infoDib;

      if(d_dibDC)
        DeleteDC(d_dibDC);
    }

    void run();

  private:

        /*
         * Virtual functions to implement DragWindow
         */

        virtual void dropObject(const DragWindowObject& from, const DragWindowObject& dest);
        virtual bool overObject(const PixelPoint& p, const DragWindowObject& from, DragWindowObject* object);
        virtual bool canDrag(const DragWindowObject& ob);

    /*
     * Window Messages
     */

    LRESULT procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
    BOOL onCreate(HWND hwnd, CREATESTRUCT* lpCreateStruct);
    void onPaint(HWND hwnd);
    BOOL onEraseBk(HWND hwnd, HDC hdc);
    UINT onNCHitTest(HWND hwnd, int x, int y);
    void onCommand(HWND hwnd, int id, HWND hwndCtl, UINT codeNotify);
    // LRESULT onDragOp(int id, DRAGLISTINFO* dli);

    /*
     * useful functions
     */

    // void doDragging(int id, DRAGLISTINFO* dli);
    // BOOL doBeginDrag(int id, DRAGLISTINFO* dli);
    // void doDropped(int id, DRAGLISTINFO* dli);
    void fillILeaderList();
    void fillUnitList();
    void onSend();
    void onCancel();
    void drawInfo(HDC hdc);
    void enableControls();
};

Reorg_LeaderWindow::Reorg_LeaderWindow(HWND hParent, CampaignWindowsInterface* cw, const CampaignData* campData) :
  d_hParent(hParent),
  d_campData(campData),
  d_campWind(cw),
  d_iLeaderLB(0),
  d_unitsLB(0),
//  d_dragMsgID(RegisterWindowMessage(DRAGLISTMSGSTRING)),
//  d_dragItemDib(0),
  d_dragFont(0),
  d_infoDib(0),
  d_dibDC(CreateCompatibleDC(NULL)),
//  d_itemID(-1),
  d_orderGiven(False)
{
  ASSERT(d_hParent);
  ASSERT(d_campData);
//  ASSERT(d_dragMsgID != 0);
  ASSERT(d_dibDC);

//  d_lastP.x = -1;
//  d_lastP.y = -1;

  const int cx = ScreenBase::x(240);    // * d_ru.dbX) / d_ru.baseX;
  const int cy = ScreenBase::y(150);    // * d_ru.dbY) / d_ru.baseY;

  HWND hWnd = createWindow(
      0,
      // GenericNoBackClass::className(),
      NULL,
      WS_POPUP,
      0, 0, cx, cy,
      d_hParent, NULL   // , APP::instance()
  );
  ASSERT(hWnd != NULL);

//  d_seeThroughWindow.parent(d_hParent);
}

LRESULT Reorg_LeaderWindow::procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{

#if 0   // Done in DragWindow
  // check for drag message.
  // note: dragMsgID is not constant so it can't be part of the switch statement
  if(msg == d_dragMsgID)
  {
    return onDragOp(static_cast<int>(wParam), reinterpret_cast<DRAGLISTINFO*>(lParam));
  }
#endif

  switch(msg)
  {
    HANDLE_MSG(hWnd, WM_CREATE, onCreate);
    HANDLE_MSG(hWnd, WM_PAINT, onPaint);
    HANDLE_MSG(hWnd, WM_ERASEBKGND, onEraseBk);
    HANDLE_MSG(hWnd, WM_NCHITTEST, onNCHitTest);
    HANDLE_MSG(hWnd, WM_COMMAND, onCommand);
    default:
      // return defProc(hWnd, msg, wParam, lParam);
      return Super::procMessage(hWnd, msg, wParam, lParam);
  }
}

BOOL Reorg_LeaderWindow::onCreate(HWND hwnd, CREATESTRUCT* lpCreateStruct)
{
  const int cx = lpCreateStruct->cx;
  const int cy = lpCreateStruct->cy;

//  d_seeThroughWindow.allocateDib(cx, cy);

  /*
   * init background dib
   */

  d_fillWind.setShadowWidth(d_ru.shadowWidth);
  d_fillWind.allocateDib(cx, cy);
  d_fillWind.setFillDib(scenario->getScenarioBkDIB(Scenario::BackPatterns::CherryWoodBackground));
  d_fillWind.setBorderColors(scenario->getBorderColors());


  // allocate info dib
  d_infoDib = new DrawDIB(d_ru.infoCX, d_ru.infoCY);
  ASSERT(d_infoDib);

  /*
   * Set font
   */

  LogFont lf;
  lf.height(ScreenBase::y(7));       // *d_ru.dbY)/d_ru.baseY);
  lf.weight(FW_MEDIUM);
  lf.face(scenario->fontName(Font_Bold));
  lf.italic(false);
  lf.underline(false);

  Font font;
  font.set(lf);

  d_dragFont = font;

  const int offset = ScreenBase::x(2);  // * d_ru.dbX) / d_ru.baseX;

  ItemWindowData ld;
  ld.d_hParent = hwnd;
  ld.d_style = WS_CHILD | WS_VISIBLE;
  ld.d_id = List1;
  ld.d_hFont = d_dragFont;
  ld.d_scrollCX = d_ru.scrollCX;
  ld.d_itemCY = ScreenBase::y(9);   // static_cast<UWORD>((9 * d_ru.dbY) / d_ru.baseY);
  ld.d_maxItemsShowing = 5;
  ld.d_flags =  ItemWindowData::HasScroll |
                ItemWindowData::NoAutoHide |
                ItemWindowData::NoBorder |
                ItemWindowData::DragList |
                ItemWindowData::InclusiveSize |
                ItemWindowData::OwnerDraw;
  // ld.d_windowFillDib = scenario->getSideBkDIB(SIDE_Neutral);
  ld.d_windowFillDib = scenario->getScenarioBkDIB(Scenario::BackPatterns::PaperBackground);
  ld.d_fillDib = scenario->getScenarioBkDIB(Scenario::BackPatterns::PaperBackground);
  ld.d_scrollBtns = ScenarioImageLibrary::get(ScenarioImageLibrary::VScrollButtons);
  ld.d_scrollThumbDib = scenario->getScenarioBkDIB(Scenario::BackPatterns::VScrollThumbBackground);
  ld.d_scrollFillDib = scenario->getScenarioBkDIB(Scenario::BackPatterns::VScrollBackground);
  ld.d_bColors = scenario->getBorderColors();

  ASSERT(ld.d_flags & ItemWindowData::HasScroll);

  const int borderCX = 0;
  SIZE s;
  s.cx = (((cx - (4 * offset)) - d_ru.shadowWidth) / 2) - ((2 * borderCX) + ld.d_scrollCX);
  s.cy = ld.d_itemCY * ld.d_maxItemsShowing;
  s.cy += 2 * ld.borderHeight();
  d_iLeaderLB = new LeaderItemListBox(ld, d_campData, s);
  ASSERT(d_iLeaderLB);

  d_p1.setX(offset);
  d_p1.setY(d_ru.headerY + d_ru.headerFontCY);

  ld.d_id = List2;
  ld.d_maxItemsShowing = 13;
  ld.d_flags =  ItemWindowData::HasScroll |
                ItemWindowData::NoAutoHide |
                ItemWindowData::NoBorder |
                ItemWindowData::InclusiveSize |
                ItemWindowData::OwnerDraw;
  s.cy = ld.d_itemCY * ld.d_maxItemsShowing;
  s.cy += 2 * ld.borderHeight();

  d_unitsLB = new LeaderItemListBox(ld, d_campData, s);
  ASSERT(d_unitsLB);

  d_p2.setX(d_p1.getX() + ((4 * offset) + s.cx));
  d_p2.setY(d_p1.getY());

//  const int bcx = ScreenBase::x(30);    // * d_ru.dbX) / d_ru.baseX;
  const int bcx = ScreenBase::x(48);
  const int bcy = ScreenBase::y(10);

  int i = 0;
  int spacing = (cx - (bcx * Button_HowMany)) / (Button_HowMany + 1);
  for(int id = Button_First; id < Button_Last + 1; id++, i++)
  {
    // TODO: move to string resource
    // const char* text = (id == Cancel) ? "Cancel" : (id == Apply) ? "Apply" : "Send";

    static const char* str_Cancel = InGameText::get(IDS_Cancel);
    // static const char str_Apply[] = "Apply";
    static const char* str_Send = InGameText::get(IDS_UOP_Send);

    const char* text;
    if(id == Cancel)
        text = str_Cancel;
    // else if(id == Apply)
    //    text = str_Apply;
    else
#if defined(DEBUG)
    if(id == Send)
#endif
        text = str_Send;
#if defined(DEBUG)
    else
        FORCEASSERT("id is invalid");
#endif

    int x = (spacing * (i + 1)) + (i * bcx);
    RLUtil::createButton(hwnd, text, id,
      x, (cy - bcy) - ScreenBase::y(5), bcx, bcy);
    // RLUtil::createButton(hwnd, text, id,
    //   x, (cy - bcy) - ((5 * d_ru.dbY) / d_ru.baseY), bcx, bcy);
  }

  enableControls();
  return TRUE;
}

void Reorg_LeaderWindow::onPaint(HWND hwnd)
{
  PAINTSTRUCT ps;
  HDC hdc = BeginPaint(hwnd, &ps);

  HPALETTE oldPal = SelectPalette(hdc, Palette::get(), False);
  RealizePalette(hdc);

//  d_seeThroughWindow.paint(hdc);

  // draw header text
  RLUtil::drawInfo(d_campData, hdc, d_ru);

  // draw leader info
  drawInfo(hdc);

  SelectPalette(hdc, oldPal, False);

  EndPaint(hwnd, &ps);
}

BOOL Reorg_LeaderWindow::onEraseBk(HWND hwnd, HDC hdc)
{
  d_fillWind.paint(hdc);
//  RLUtil::drawInfo(d_campData, d_fillWind.dib(), d_ru, cx, cy);
  return True;
}

UINT Reorg_LeaderWindow::onNCHitTest(HWND hwnd, int x, int y)
{
  return HTCAPTION;
}

void Reorg_LeaderWindow::onCommand(HWND hwnd, int id, HWND hwndCtl, UINT codeNotify)
{
  switch(id)
  {
    case Cancel:
    {
      onCancel();
      break;
    }

    case Send:
    {
      onSend();
      onCancel();
      break;
    }

#if 0
    case Apply:
    {
      onSend();
      fillILeaderList();
      fillUnitList();

      LeaderItemData ld;
      ld.d_iLeaders = &d_iLeaders;
      d_iLeaderLB->init(&ld);

      ld.d_iLeaders = 0;
      ld.d_units = &d_units;
      d_unitsLB->init(&ld);

      d_iLeaderLB->show(d_p1);
      d_unitsLB->show(d_p2);

      enableControls();
      // note: don't use break statement. let it flow to next case
    }
#endif

    case List1:
    {
      RECT r;
      SetRect(&r, d_ru.infoX, d_ru.infoY,
        d_ru.infoX + d_ru.infoCX,
        d_ru.infoY + d_ru.infoCY);

      InvalidateRect(hwnd, &r, FALSE);
      UpdateWindow(hwnd);
    }
  }
}

void Reorg_LeaderWindow::run()
{
  ASSERT(d_hParent);


  /*
   * First transfer bits from mainwind hdc to our local dib
   */

  // calculate top corner position
  RECT r;
  GetClientRect(getHWND(), &r);
  const int cx = r.right - r.left;
  const int cy = r.bottom - r.top;

  RECT pr;
  GetClientRect(d_hParent, &pr);
  const int x = ((pr.right - pr.left) - cx) / 2;
  const int y = ((pr.bottom - pr.top) - cy) / 2;
  PixelPoint p(x, y);

//  d_seeThroughWindow.transferBits(p);
//  RLUtil::drawInfo(d_campData, d_fillWind.dib(), d_ru, cx, cy);

  /*
   * initialize lists
   */

  fillILeaderList();
  fillUnitList();

  LeaderItemData ld;
  ld.d_iLeaders = &d_iLeaders;
  d_iLeaderLB->init(&ld);

  ld.d_iLeaders = 0;
  ld.d_units = &d_units;
  d_unitsLB->init(&ld);

  d_iLeaderLB->show(d_p1);
  d_unitsLB->show(d_p2);

  /*
   * Convert to screen and show
   */

  POINT p2;
  p2.x = p.getX();
  p2.y = p.getY();

  ClientToScreen(d_hParent, &p2);
  SetWindowPos(getHWND(), HWND_TOPMOST, p2.x, p2.y, 0, 0, SWP_NOSIZE | SWP_SHOWWINDOW);
}

void Reorg_LeaderWindow::fillILeaderList()
{
  ASSERT(d_campData);

  /*
   * Get independent leader list
   */

  const Armies& armies = d_campData->getArmies();
  const IndependentLeaderList& list = armies.independentLeaders();

  /*
   * reset our internal list
   */

  d_iLeaders.reset();

  /*
   * Fill our internal list
   */

  for(Side s = 0; s < scenario->getNumSides(); s++)
  {
    SListIterR<IndependentLeader> iter(&list);
    while(++iter)
    {
      Side lSide = iter.current()->leader()->getSide();
      if(iter.current()->active() &&
         s == lSide &&
         GamePlayerControl::canControl(lSide))// &&
//       iter.current()->travelTime() <= d_campData->getTick())
      {

        ILeaderItem* il = new ILeaderItem(iter.current());
        ASSERT(il);

        d_iLeaders.append(il);
      }
    }
  }
}

void Reorg_LeaderWindow::fillUnitList()
{
  ASSERT(d_campData);

  /*
   * Get independent leader list
   */

  const Armies& armies = d_campData->getArmies();

  /*
   * reset our internal list
   */

  d_units.reset();

  /*
   * Fill our internal list
   */

  for(Side s = 0; s < scenario->getNumSides(); s++)
  {
    if(GamePlayerControl::canControl(s))
    {
      ConstUnitIter iter(&armies, armies.getPresident(s));
      while(iter.next())
      {
        if(iter.current()->getRank().getRankEnum() != Rank_President)
        {
          ILUnitItem* ui = new ILUnitItem();
          ASSERT(ui);

          ui->d_cpi = iter.current();
          d_units.append(ui);
        }
      }
    }
  }
}

#if 0
/*
 * process drag message
 */

LRESULT Reorg_LeaderWindow::onDragOp(int id, DRAGLISTINFO* dli)
{
  switch(dli->uNotification)
  {
    case DL_BEGINDRAG:
    {
      return doBeginDrag(id, dli);
    }

    case DL_DRAGGING:
    {
      doDragging(id, dli);
      return DL_COPYCURSOR;
    }

    case DL_DROPPED:
    {
      doDropped(id, dli);
#ifdef DEBUG
      logWindow->printf("--- DL_DROPPED x = %ld, y = %ld", dli->ptCursor.x, dli->ptCursor.y);
#endif
      return True;
    }
  }

  return 1;
}

BOOL Reorg_LeaderWindow::doBeginDrag(int id, DRAGLISTINFO* dli)
{
#ifdef DEBUG
  logWindow->printf("--- DL_BEGINDRAG x = %ld, y = %ld", dli->ptCursor.x, dli->ptCursor.y);
#endif

  d_itemID = LBItemFromPt(dli->hWnd, dli->ptCursor, FALSE);

  ASSERT(d_itemID >= 0);
  if(d_itemID < 0)
    return FALSE;

  const ILeaderItem* il = reinterpret_cast<const ILeaderItem*>(d_iLeaderLB->currentValue());
  if(il->d_leader->travelTime() > d_campData->getTick())
    return FALSE;

#ifdef DEBUG
  logWindow->printf("---------- itemID = %d", d_itemID);
#endif

  return TRUE;
}

void Reorg_LeaderWindow::doDragging(int id, DRAGLISTINFO* dli)
{
  ASSERT(d_itemID >= 0);
  if(d_itemID < 0)
    return;

  /*
   * Convert to client cordinates
   */

  POINT p = dli->ptCursor;
  ScreenToClient(getHWND(), &p);

  const int oldOffsetX = (d_dragItemDib) ? d_dragItemDib->getWidth() : 0;
  const int oldOffsetY = (d_dragItemDib) ? d_dragItemDib->getHeight() : 0;

  RECT r;
  GetWindowRect(getHWND(), &r);
  if(!PtInRect(&r, dli->ptCursor) ||
     ((p.x - oldOffsetX) == d_lastP.x && (p.y - oldOffsetY) == d_lastP.y))
  {
    return;
  }

  HDC hdc = GetDC(getHWND());
  ASSERT(hdc);

  HPALETTE oldPal = SelectPalette(hdc, Palette::get(), False);
  RealizePalette(hdc);

  /*
   * Put back last background
   */

  if(d_lastP.x >= 0 && d_lastP.y >= 0)
  {
    ASSERT(d_dragItemDib);
    ASSERT(d_dibDC);

    SelectObject(d_dibDC, d_dragItemDib->getHandle());
    BitBlt(hdc, d_lastP.x, d_lastP.y, d_dragItemDib->getWidth(), d_dragItemDib->getWidth(),
       d_dibDC, 0, 0, SRCCOPY);
  }

  /*
   * See if we are over the units listbox
   */

  if(d_unitsLB->overItem(dli->ptCursor))
  {
#ifdef DEBUG
    logWindow->printf("---------- Over Unit List (index = %d)", d_unitsLB->currentIndex());
#endif
  }

  /*
   * Set font
   */

  ASSERT(d_dragFont);
  HFONT oldFont = (HFONT)SelectObject(hdc, d_dragFont);
  ASSERT(oldFont);

  SetBkMode(hdc, TRANSPARENT);

  const ILeaderItem* il = reinterpret_cast<const ILeaderItem*>(d_iLeaderLB->currentValue());
  ILeader leader = il->d_leader->leader();

  char text[200];
  lstrcpy(text, leader->getNameNotNull());

  SIZE s;
  GetTextExtentPoint32(hdc, text, lstrlen(text), &s);

  if(!d_dragItemDib ||
     s.cx > d_dragItemDib->getWidth() ||
     s.cy > d_dragItemDib->getHeight())
  {
    SIZE newS;

    if(!d_dragItemDib)
      newS = s;
    else
    {
      newS.cx = d_dragItemDib->getWidth();
      newS.cy = d_dragItemDib->getHeight();

      if(s.cx > d_dragItemDib->getWidth())
        newS.cx = s.cx;

      if(s.cy > d_dragItemDib->getHeight())
        newS.cy = s.cy;
    }

    if(d_dragItemDib)
    {
      delete d_dragItemDib;
      d_dragItemDib = 0;
    }

    ASSERT(newS.cx > 0);
    ASSERT(newS.cy > 0);
    d_dragItemDib = new DrawDIB(newS.cx, newS.cy);
  }

  ASSERT(d_dragItemDib);
  SelectObject(d_dibDC, d_dragItemDib->getHandle());

  p.x -= d_dragItemDib->getWidth();
  p.y -= d_dragItemDib->getHeight();

  BitBlt(d_dibDC, 0, 0, d_dragItemDib->getWidth(), d_dragItemDib->getHeight(),
     hdc, p.x, p.y, SRCCOPY);

  wTextOut(hdc, p.x, p.y, text);

  d_lastP = p;

  SelectObject(hdc, oldFont);
  SelectPalette(hdc, oldPal, FALSE);
  ReleaseDC(getHWND(), hdc);

#ifdef DEBUG
  logWindow->printf("--- DL_DRAGGING x = %ld, y = %ld", dli->ptCursor.x, dli->ptCursor.y);
#endif
}

void Reorg_LeaderWindow::doDropped(int id, DRAGLISTINFO* dli)
{
  /*
   * Put back last background
   */

  if(d_lastP.x >= 0 && d_lastP.y >= 0)
  {
    HDC hdc = GetDC(getHWND());
    ASSERT(hdc);

    HPALETTE oldPal = SelectPalette(hdc, Palette::get(), False);
    RealizePalette(hdc);

    ASSERT(d_dragItemDib);

    SelectObject(d_dibDC, d_dragItemDib->getHandle());
    BitBlt(hdc, d_lastP.x, d_lastP.y, d_dragItemDib->getWidth(), d_dragItemDib->getWidth(),
       d_dibDC, 0, 0, SRCCOPY);

    SelectPalette(hdc, oldPal, FALSE);
    ReleaseDC(getHWND(), hdc);
  }

  d_lastP.x = -1;
  d_lastP.y = -1;
  d_itemID = -1;

  /*
   * See if we are over the units listbox
   */

  if(d_unitsLB->overItem(dli->ptCursor))
  {
#ifdef DEBUG
    logWindow->printf("--- Dropped over item# = %d", d_unitsLB->currentIndex());
#endif

    ILUnitItem* ui = reinterpret_cast<ILUnitItem*>(d_unitsLB->currentValue());
    ILeaderItem* li = reinterpret_cast<ILeaderItem*>(d_iLeaderLB->currentValue());

    /*
     * Assign leader.
     * Note: Leader who is traveling cannot be assigned
     */

    if(li->d_leader->travelTime() <= d_campData->getTick())
    {
      ui->d_newLeader = const_cast<IndependentLeader*>(li->d_leader);
      d_iLeaders.remove(li);

      // reset leader list box
      LeaderItemData ld;
      ld.d_iLeaders = &d_iLeaders;
      d_iLeaderLB->init(&ld);
      d_iLeaderLB->show(d_p1);

      // redraw unit box
      d_unitsLB->setCurrentIndex(d_unitsLB->currentIndex());

      d_orderGiven = True;
    }
  }

  enableControls();
}
#endif

void Reorg_LeaderWindow::dropObject(const DragWindowObject& from, const DragWindowObject& dest)
{
    ListBox lb(dest.hwnd());
    ListBox fromLB(from.hwnd());
    ILUnitItem* ui = reinterpret_cast<ILUnitItem*>(lb.getValue(dest.id()));
    ILeaderItem* li = reinterpret_cast<ILeaderItem*>(fromLB.getValue(from.id()));

    ui->d_newLeader = li->d_leader;
    d_iLeaders.remove(li);

    // reset leader list box
    LeaderItemData ld;
    ld.d_iLeaders = &d_iLeaders;
    d_iLeaderLB->init(&ld);
    d_iLeaderLB->show(d_p1);

    // redraw unit box
    d_unitsLB->setCurrentIndex(d_unitsLB->currentIndex());

    d_orderGiven = True;

    enableControls();
}


bool Reorg_LeaderWindow::canDrag(const DragWindowObject& ob)
{
  const ILeaderItem* il = reinterpret_cast<const ILeaderItem*>(d_iLeaderLB->currentValue());
  return (il->d_leader->travelTime() <= d_campData->getTick());
}

bool Reorg_LeaderWindow::overObject(const PixelPoint& p, const DragWindowObject& from, DragWindowObject* object)
{
    DragWindowObject dropOb;
    if(d_unitsLB->overItem(p, &dropOb))
    {
        ListBox lb(dropOb.hwnd());
        ILeaderItem* li = reinterpret_cast<ILeaderItem*>(lb.getValue(dropOb.id()));
        if(li->d_leader->travelTime() <= d_campData->getTick())
        {
            if (lb.getIndex() != dropOb.id())
            {
               startDraw();
               lb.set(dropOb.id());
               endDraw();
            }

            *object = dropOb;
            return true;
        }
    }

    return false;
}


/*
 * Send order to leader
 *
 * NOTE: This happens immediately... it should instead be sent via the
 * order despatch queue.
 */

void Reorg_LeaderWindow::onSend()
{
  // RWLock lock;
  // lock.startWrite();

  /*
   * go through unit list and find any units with new leader
   */

  SListIter<ILUnitItem> iter(&d_units);
  while(++iter)
  {
    ILUnitItem* ui = iter.current();


    if(ui->d_newLeader)
    {
      IndependentLeader* iLeader = const_cast<IndependentLeader*>(ui->d_newLeader);
      CommandPosition* cp = const_cast<CommandPosition*>(ui->d_cpi.value());

      iLeader->attachTo(cp);
      ILeader leader = iLeader->leader();

      /*
       * Figure out travel time to leader pool
       *
       * Note: Temporary code
       * this needs more proper design.
       * For now, French leaders will go from SHQ, Allies from closest capital
       *
       */

      CampaignPosition toLoc;

      // temporary. we'll need to get this from a data file
      if(iLeader->atSHQ())
      {
        const CommandPosition* shqCP = d_campData->getArmies().getSHQ(leader->getSide());
        toLoc.setPosition(shqCP->getPosition());
      }
      else
      {
        ASSERT(iLeader->town() != NoTown);
        toLoc.setPosition(iLeader->town());
      }

      TimeTick travelTime = 0;
      Distance routeDistance = 0;
      static CampaignRoute route(d_campData);

      if(route.getRouteDistance(&toLoc, ui->d_cpi->location(), Distance_MAX, routeDistance, True))
      {
        int miles = DistanceToMile(routeDistance);

        /*
         *  Travel time is 12 mph for 8 hours per day(96 mpd).
         *  timeToTravel Function caculates raw time,
         *  so set MPH macro to MPH(4) to account for courier rest time
         */

        travelTime = timeToTravel(routeDistance, MPH(4));
      }

      iLeader->travelTime(travelTime + d_campData->getTick());

#ifdef DEBUG
      g_logWindow.printf("%s taking over %s, travel time = %ld hours",
        leader->getNameNotNull(), ui->d_cpi->getNameNotNull(), (travelTime / TicksPerHour));
#endif
    }
  }

  d_orderGiven = False;
  // lock.endWrite();
}

void Reorg_LeaderWindow::onCancel()
{
  /*
   * reset lists and hide
   */

  d_iLeaders.reset();
  d_units.reset();

  ShowWindow(getHWND(), SW_HIDE);

  d_campWind->reorgLeaderUpdated();
}

void Reorg_LeaderWindow::drawInfo(HDC hdc)
{
  if(d_iLeaderLB->entries())
  {
    const ILeaderItem* ili = reinterpret_cast<const ILeaderItem*>(d_iLeaderLB->currentValue());
    ASSERT(ili);

    const IndependentLeader* il = ili->d_leader;
    const ILeader leader = il->leader();

    HPALETTE oldPal = SelectPalette(hdc, Palette::get(), False);
    RealizePalette(hdc);

    /*
     * fill info background
     */

    d_infoDib->fill(scenario->getScenarioBkDIB(Scenario::BackPatterns::PaperBackground));

    // draw a black border
    ColourIndex ci = d_infoDib->getColour(PALETTERGB(0, 0, 0));
    d_infoDib->frame(0, 0, d_infoDib->getWidth(), d_infoDib->getHeight(), ci);

    SelectObject(d_dibDC, d_infoDib->getHandle());

    /*
     * Now draw info
     */

    LogFont lf;
    lf.height(ScreenBase::y(10));       // * d_ru.dbY) / d_ru.baseY);
    lf.weight(FW_MEDIUM);
    lf.face(scenario->fontName(Font_Bold));
    // lf.italic(true);
    lf.italic(false);
    lf.underline(true);

    Font font;
    font.set(lf);

    HFONT oldFont = reinterpret_cast<HFONT>(SelectObject(d_dibDC, font));
    SetBkMode(d_dibDC, TRANSPARENT);

    const COLORREF ourColor = scenario->getSideColour(leader->getSide());
    const COLORREF black = PALETTERGB(0, 0, 0);
    const int headerX = ScreenBase::x(1);       // (d_ru.dbX) / d_ru.baseX;
    const int headerY = ScreenBase::y(1);       // (d_ru.dbY) / d_ru.baseY;
    const int statusY = headerY + ScreenBase::y(10);    // * d_ru.dbY) / d_ru.baseY);
    const int spaceY = ScreenBase::y(8);        // * d_ru.dbY) / d_ru.baseY);
    const int specialistY = statusY + spaceY;
    const int attributeY = specialistY + spaceY;   // (1.5 * float(spaceY));

    COLORREF oldColor = SetTextColor(d_dibDC, ourColor);

    /*
     * Put nation flag
     */

    const ImageLibrary* flagIcons = scenario->getNationFlag(leader->getNation(), True);
    flagIcons->blit(d_infoDib, FI_Army, headerX, headerY, True);

    /*
     * Put leaders name
     */

    wTextOut(d_dibDC, headerX + FI_Army_CX + ScreenBase::x(5), headerY, leader->getNameNotNull());

    /*
     * Put current activity
     */

    lf.height(ScreenBase::y(7));    // * d_ru.dbY) / d_ru.baseY);
    lf.italic(false);
    lf.underline(false);

    font.set(lf);
    SelectObject(d_dibDC, font);
    SetTextColor(d_dibDC, black);

    enum { Available, Traveling } status;
//    static const char* s_statusStrings[] = {
//      "Available",
//      "Traveling"
//    };

    status = (il->travelTime() <= d_campData->getTick()) ? Available : Traveling;

    if(status == Available)
    {
      const char* text = (il->atSHQ()) ? InGameText::get(IDS_SHQ) :
                         (il->town() != NoTown) ? d_campData->getTownName(il->town()) :
                         "???";

      wTextPrintf(d_dibDC, headerX, statusY, "%s %s",
            InGameText::get(IDS_AvailableAt), text);
         // "%s (at %s)",
         // s_statusStrings[status], text);
    }
    else
    {
      const TimeTick timeRemaining = il->travelTime() - d_campData->getTick();

      const char* text = (il->attachTo() != NoCommandPosition) ? il->attachTo()->getNameNotNull() :
                         (il->atSHQ()) ? InGameText::get(IDS_SHQ) :
                         (il->town() != NoTown) ? d_campData->getTownName(il->town()) :
                         "???";

      wTextPrintf(d_dibDC, headerX, statusY, "%s %s (%ld %s)",
         // s_statusStrings[status],
         InGameText::get(IDS_TravellingTo),
         text,
         timeRemaining / TicksPerHour,
         InGameText::get(IDS_Hours));
    }

    /*
     * Put specialist status
     */

    if(leader->isSpecialist())
    {
      wTextOut(d_dibDC, headerX, specialistY, Specialist::typeName(leader->getSpecialistType()));
    }

    /*
     * Put Attributes
     */

    int y = attributeY;
    //TODO: get this from string resource
    const char* text = InGameText::get(IDS_UI_INITIATIVE);
    RLUtil::drawAttributeGraph(d_dibDC, d_infoDib, text, headerX, y, leader->getInitiative(True), d_ru);

    y += spaceY;
    //TODO: get this from string resource
    text = InGameText::get(IDS_UI_CHARISMA);
    RLUtil::drawAttributeGraph(d_dibDC, d_infoDib, text, headerX, y, leader->getCharisma(True), d_ru);

    y += spaceY;
    //TODO: get this from string resource
    text = InGameText::get(IDS_TacticalAbility);
    RLUtil::drawAttributeGraph(d_dibDC, d_infoDib, text, headerX, y, leader->getTactical(True), d_ru);

    y += spaceY;
    //TODO: get this from string resource
    text = InGameText::get(IDS_UI_AGGRESSION);
    RLUtil::drawAttributeGraph(d_dibDC, d_infoDib, text, headerX, y, leader->getAggression(True), d_ru);

    /*
     * Draw command ablitiy
     */

    y += spaceY;
    //TODO: get this from string resource
    text = InGameText::get(IDS_CommandAbility);
    wTextPrintf(d_dibDC, headerX, y, "%s %s", text,
       leader->getRankLevel().getRankName());


    BitBlt(hdc, d_ru.infoX, d_ru.infoY,
      d_infoDib->getWidth(), d_infoDib->getHeight(),
      d_dibDC, 0, 0, SRCCOPY);

    /*
     * Clean up
     */

    SetTextColor(d_dibDC, oldColor);
    SelectObject(d_dibDC, oldFont);
    SelectPalette(hdc, oldPal, FALSE);
  }
}

void Reorg_LeaderWindow::enableControls()
{
#if 0
  {
    CustomButton b(getHWND(), Apply);
    b.enable(d_orderGiven);
  }
#endif

  {
    CustomButton b(getHWND(), Send);
    b.enable(d_orderGiven);
  }
}

/*-----------------------------------------------------------
 * Container class for Reorganize leader window
 */

ReorgLeader_Int::ReorgLeader_Int(HWND hParent, CampaignWindowsInterface* cw, const CampaignData* campData)
{
  d_cbrWindow = new Reorg_LeaderWindow(hParent, cw, campData);
  ASSERT(d_cbrWindow);
}

ReorgLeader_Int::~ReorgLeader_Int()
{
  // destroy();
  delete d_cbrWindow;
}

void ReorgLeader_Int::run()
{
  d_cbrWindow->run();
}

void ReorgLeader_Int::hide()
{
  ShowWindow(d_cbrWindow->getHWND(), SW_HIDE);
}

void ReorgLeader_Int::destroy()
{
  if(d_cbrWindow)
  {
    delete d_cbrWindow;
    d_cbrWindow = 0;
  }
}

Boolean ReorgLeader_Int::initiated() const
{
  return (d_cbrWindow != 0);
}

HWND ReorgLeader_Int::getHWND() const
{
  return (d_cbrWindow) ? d_cbrWindow->getHWND() : 0;
}




/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.3  2001/07/02 15:23:17  greenius
 * Battle, Campaign, Combined game and Campaign Editor now compile and run... mostly
 *
 * Revision 1.2  2001/06/18 08:25:45  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
