/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef ORG_DIAL_HPP
#define ORG_DIAL_HPP

#ifndef __cplusplus
#error org_dial.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *	Reorganize Units dialogue
 *
 *----------------------------------------------------------------------
 */

#include <windef.h>
#include "except.hpp"

class StackedUnitList;
// class Armies;
class CampaignData;
class OrgDialogImp;

class ReorganizeDialog {
	OrgDialogImp* d_orgDial;

	// Prevent copying

	ReorganizeDialog(const ReorganizeDialog&);
	ReorganizeDialog& operator = (const ReorganizeDialog&);

 public:
//	 ReorganizeDialog(HWND hParent, const CampaignData* campData, const StackedUnitList& ul, const RECT& r);
	 ReorganizeDialog(HWND hParent, const CampaignData* campData, const RECT& r); //const CampaignData* campData, const StackedUnitList& ul);
	 ~ReorganizeDialog();

	 void initUnits(const StackedUnitList& ul);
	 HWND getHWND() const;

	 class OrgDialError : public GeneralError {
	 	public:
	  		OrgDialError() : GeneralError("Error in ReorganizeDialog") { }
	 };
};

#endif /* ORG_DIAL_HPP */

