/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
#include "stdinc.hpp"
#include "towndial.hpp"
#include "dib.hpp"
#include "town.hpp"
#include "scenario.hpp"
#include "resdef.h"
#include "userint.hpp"
#include "palette.hpp"
#include "app.hpp"
#include "ctab.hpp"
#include "fonts.hpp"
#include "campdint.hpp"
#include "towninfo.hpp"
#include "bld_dial.hpp"
#include "button.hpp"
#include "ds_town.hpp"
#include "scn_img.hpp"
#include "imglib.hpp"
#include "townprop.hpp"
#include "cbutton.hpp"
#include "resstr.hpp"

/*------------------ String Resource management ---------------
 *
 */

class TownDialogStrings {
public:
  enum {
    Summary,
    Build,
    Status,
    Town,
    Province,

    HowMany
  };
};

static const int ids[TownDialogStrings::HowMany] = {
   IDS_UI_SUMMARY,
   IDS_TI_BUILD,
   IDS_TI_STATUS,
   IDS_TI_TOWN,
   IDS_TI_PROVINCE
};

static ResourceStrings s_strings(ids, TownDialogStrings::HowMany);

/*
 *----------------------------------------------------------------------
 *  TownDialog Base class functions
 */

void TownDialog::updateCaption()
{
  if(d_town != NoTown)
  {
    const Town& t = d_campData->getTown(d_town);
    const Province& p = d_campData->getProvince(t.getProvince());

    char buf[200];
    wsprintf(buf, "%s, %s", t.getNameNotNull(), p.getNameNotNull());
    d_customDialog.setCaption(buf);
  }
}

void TownDialog::destroy()
{
  DestroyWindow(getHWND());
}

void TownDialog::allocateStaticDib(int cx, int cy)
{
  if(!d_dib ||
     d_dib->getWidth() < cx || d_dib->getHeight() < cy)
  {
    int oldCX = 0;
    int oldCY = 0;

    if(d_dib)
    {
      oldCX = d_dib->getWidth();
      oldCY = d_dib->getHeight();

      delete d_dib;
      d_dib = 0;
    }
    d_dib = new DrawDIBDC(maximum(oldCX, cx), maximum(oldCY, cy));

    d_dib->setBkMode(TRANSPARENT);
  }

  ASSERT(d_dib);
}

void TownDialog::deleteStaticDib()
{
  if(d_dib)
    delete d_dib;

  d_dib = 0;
}


/*-----------------------------------------------------------------
 * Dialog Page Base class
 */

class TownDialogPage : public ModelessDialog {
   const CampaignData* d_campData;
   DLGTEMPLATE* d_dialog;
   TownDialog* d_owner;
   CustomDialog d_customDialog;
public:
   enum Page {
      First = 0,
      SummaryPage = First,
      TownPage,
      ProvincePage,

      BuildAllowedOnly_First,
      BuildPage = BuildAllowedOnly_First,
      BuildAllowedOnly_Last,

      SeenOnly_First = BuildAllowedOnly_Last,
      StatusPage = SeenOnly_First,
      SeenOnly_Last,

      HowMany = SeenOnly_Last
   };

   TownDialogPage(const CampaignData* campData, TownDialog* p, const char* dlgName);
   ~TownDialogPage();

   DLGTEMPLATE* getDialog() const { return d_dialog; }
   virtual const char* getTitle() const = 0;

   HWND create();
   void enable();
   void disable();
   virtual void updateValues() = 0;
   virtual void initControls() = 0;
   virtual void enableControls() = 0;

   static TownDialogPage* createPage(Page page, const CampaignData* campData, TownDialog* p);

protected:

   void setPosition();

   TownDialog* owner() const { return d_owner; }
   const CampaignData* campData() const { return d_campData; }
   CustomDialog& customDialog() { return d_customDialog; }
};

TownDialogPage::TownDialogPage(const CampaignData* campData, TownDialog* p, const char* dlgName) :
  d_campData(campData),
  d_owner(p)
{
   HRSRC rhTep = FindResource(NULL, dlgName, RT_DIALOG);
   ASSERT(rhTep != NULL);
   HGLOBAL lphTep = (HGLOBAL) LoadResource(NULL, rhTep);
   ASSERT(lphTep != NULL);
   d_dialog = (LPDLGTEMPLATE) LockResource(lphTep);
   ASSERT(d_dialog != NULL);

   /*
    * Initialize Custom Dialog settings
    */

// d_customDialog.setBkDib(scenario->getSideBkDIB(SIDE_Neutral));
}

TownDialogPage::~TownDialogPage()
{
}

HWND TownDialogPage::create()
{
   ASSERT(hWnd == NULL);      // Already created?

   HWND hDialog = createDialogIndirect(d_dialog, d_owner->getHWND());

   ASSERT(hDialog != NULL);

   return hDialog;
}

void TownDialogPage::enable()
{
   ASSERT(hWnd != NULL);
   ShowWindow(hWnd, SW_SHOW);
}

void TownDialogPage::disable()
{
   ASSERT(hWnd != NULL);
   ShowWindow(hWnd, SW_HIDE);
}


void TownDialogPage::setPosition()
{
   const RECT& r = d_owner->tabbedDialogRect();

   SetWindowPos(getHWND(), HWND_TOP, r.left, r.top, r.right-r.left, r.bottom-r.top, 0);
}

/*----------------------------------------------------------------------
 * Summary Page
 */

class TownSummaryPage : public TownDialogPage {
   const DrawDIB* d_textFillDib;
public:
   TownSummaryPage(const CampaignData* campData, TownDialog* p) :
     TownDialogPage(campData, p, townSummaryPage),
     d_textFillDib(scenario->getScenarioBkDIB(Scenario::BackPatterns::PaperBackground)) {}

   ~TownSummaryPage();

   const char* getTitle() const { return s_strings.get(TownDialogStrings::Summary); }
private:
   void initControls() {}// drawSummaryDib(); }
   void updateValues() {}
   void enableControls() {}

   void drawSummaryDib(int cx, int cy);

   BOOL procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
   BOOL onInitDialog(HWND hwnd, HWND hwndFocus, LPARAM lParam);
   void onDrawItem(HWND hwnd, const DRAWITEMSTRUCT* lpDrawItem);
};

TownSummaryPage::~TownSummaryPage()
{
}

BOOL TownSummaryPage::procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
   switch(msg)
   {
      case WM_INITDIALOG:
         HANDLE_WM_INITDIALOG(hWnd, wParam, lParam, onInitDialog);
         break;
      case WM_DRAWITEM:
         HANDLE_WM_DRAWITEM(hWnd, wParam, lParam, onDrawItem);
         break;
      default:
         return FALSE;
   }

   return TRUE;
}

BOOL TownSummaryPage::onInitDialog(HWND hwnd, HWND hwndFocus, LPARAM lParam)
{
  customDialog().init(hwnd);

  setPosition();
  return TRUE;
}

void TownSummaryPage::onDrawItem(HWND hwnd, const DRAWITEMSTRUCT* lpDrawItem)
{
  HPALETTE oldPal = SelectPalette(lpDrawItem->hDC, Palette::get(), FALSE);
  RealizePalette(lpDrawItem->hDC);

  const int cx = lpDrawItem->rcItem.right - lpDrawItem->rcItem.left;
  const int cy = lpDrawItem->rcItem.bottom - lpDrawItem->rcItem.top;

  switch(lpDrawItem->CtlID)
  {
    case TSP_SUMMARYTEXT:
      drawSummaryDib(cx, cy);
      CampaignUserInterface::blitDibToControl(owner()->dib(), lpDrawItem);
      break;
  }

  SelectPalette(lpDrawItem->hDC, oldPal, FALSE);
}

void TownSummaryPage::drawSummaryDib(int cx, int cy)
{
  /*
   * Some useful values
   * Note: All positioning is done with dialog units
   */

  static LONG dbUnits = GetDialogBaseUnits();

  const LONG dbX = LOWORD(dbUnits);
  const LONG dbY = HIWORD(dbUnits);

  HWND h = GetDlgItem(getHWND(), TSP_SUMMARYTEXT);
  ASSERT(h != NULL);

  owner()->allocateStaticDib(cx, cy);
  ASSERT(owner()->dib());
  ASSERT(d_textFillDib);

  /*
   * Fill in background
   */

  owner()->dib()->rect(0, 0, cx, cy, d_textFillDib);

  /*
   * Draw Info
   */

  int fontHeight = (dbY*7)/8;

  LogFont lf;
  lf.height(fontHeight);
  lf.weight(FW_MEDIUM);
  lf.face(scenario->fontName(Font_Bold));

  Font font;
  font.set(lf);

  owner()->dib()->setFont(font);

  if(owner()->currentTown() != NoTown)
  {
    TownInfoDib::drawSummaryDib(campData(), owner()->getAttributes(), owner()->dib(), dbX, dbY);
  }

}

/*----------------------------------------------------------------------
 * Build Page
 */

class TownBuildPage : public TownDialogPage {
   const DrawDIB* d_fillDib;
   const DrawDIB* d_checkButtonFillDib;

   BasicUnitType::value d_basicType;

   enum { UnitTypeWindowID = 300 };
public:
   TownBuildPage(const CampaignData* campData, TownDialog* p) :
     TownDialogPage(campData, p, townBuildPage),
     d_fillDib(scenario->getScenarioBkDIB(Scenario::BackPatterns::PaperBackground)),
     d_checkButtonFillDib(scenario->getSideBkDIB(SIDE_Neutral)),
     d_basicType(BasicUnitType::HowMany)
     {
     }

   ~TownBuildPage();

   const char* getTitle() const { return s_strings.get(TownDialogStrings::Build); }
private:
   void initControls();
   void updateValues();
   void enableControls();

   void redrawItems();

   void drawTextDib(int cx, int cy);
   void drawBuildItemDib(BasicUnitType::value basicType, int cx, int cy);

   BOOL procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
   BOOL onInitDialog(HWND hwnd, HWND hwndFocus, LPARAM lParam);
   void onCommand(HWND hwnd, int id, HWND hwndCtl, UINT codeNotify);
   void onDrawItem(HWND hwnd, const DRAWITEMSTRUCT* lpDrawItem);
};

TownBuildPage::~TownBuildPage()
{
}

BOOL TownBuildPage::procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
   switch(msg)
   {

      case WM_INITDIALOG:
         HANDLE_WM_INITDIALOG(hWnd, wParam, lParam, onInitDialog);
         break;
      case WM_COMMAND:
         HANDLE_WM_COMMAND(hWnd, wParam, lParam, onCommand);
         break;
      case WM_DRAWITEM:
         HANDLE_WM_DRAWITEM(hWnd, wParam, lParam, onDrawItem);
         break;
      default:
         return FALSE;
   }

   return TRUE;
}

BOOL TownBuildPage::onInitDialog(HWND hwnd, HWND hwndFocus, LPARAM lParam)
{
  customDialog().init(hwnd);

  static LONG dbUnits = GetDialogBaseUnits();

  const LONG dbX = LOWORD(dbUnits);
  const LONG dbY = HIWORD(dbUnits);

  SIZE s;
  s.cx = (170*dbX)/4;
  s.cy = (75*dbY)/8;

  BuildItemInterface::create(hwnd, UnitTypeWindowID, campData(), s);

  for(int id = TBP_FIRSTPUSHBUTTON; id < TBP_LASTPUSHBUTTON; id++)
  {
    CustomButton cb(hwnd, id);
    cb.setFillDib(scenario->getScenarioBkDIB(Scenario::BackPatterns::WoodBackground));
    cb.setBorderColours(scenario->getBorderColors());
  }

  for(id = TBP_FIRSTCHECKBUTTON; id < TBP_LASTCHECKBUTTON; id++)
  {
    CustomButton cb(hwnd, id);
    cb.setFillDib(scenario->getScenarioBkDIB(Scenario::BackPatterns::WoodBackground));
    cb.setBorderColours(scenario->getBorderColors());
    cb.setCheckBoxImages(ScenarioImageLibrary::get(ScenarioImageLibrary::CheckButtons));
  }

  setPosition();
  return TRUE;
}

void TownBuildPage::updateValues()
{
  BuildItemInterface::hide();
}

BasicUnitType::value idToBasicType(int id)
{
  BasicUnitType::value basicType = (id == TBP_INFANTRY) ? BasicUnitType::Infantry :
                                   (id == TBP_CAVALRY) ? BasicUnitType::Cavalry :
                                   (id == TBP_ARTILLERY) ? BasicUnitType::Artillery :
                                   BasicUnitType::Special;

  return basicType;
}

BasicUnitType::value moreThanIDToBasicType(int id)
{
  BasicUnitType::value basicType = (id == TBP_MOREINFANTRY) ? BasicUnitType::Infantry :
                                   (id == TBP_MORECAVALRY) ? BasicUnitType::Cavalry :
                                   (id == TBP_MOREARTILLERY) ? BasicUnitType::Artillery :
                                   BasicUnitType::Special;

  return basicType;
}

BasicUnitType::value lessThanIDToBasicType(int id)
{
  BasicUnitType::value basicType = (id == TBP_LESSINFANTRY) ? BasicUnitType::Infantry :
                                   (id == TBP_LESSCAVALRY) ? BasicUnitType::Cavalry :
                                   (id == TBP_LESSARTILLERY) ? BasicUnitType::Artillery :
                                   BasicUnitType::Special;

  return basicType;
}

BasicUnitType::value autoIDToBasicType(int id)
{
  BasicUnitType::value basicType = (id == TBP_AUTOINFANTRY) ? BasicUnitType::Infantry :
                                   (id == TBP_AUTOCAVALRY) ? BasicUnitType::Cavalry :
                                   (id == TBP_AUTOARTILLERY) ? BasicUnitType::Artillery :
                                   BasicUnitType::Special;

  return basicType;
}


int basicTypeToID(BasicUnitType::value value)
{
  ASSERT(value < BasicUnitType::HowMany);
  int id = (value == BasicUnitType::Infantry) ? TBP_INFANTRY :
           (value == BasicUnitType::Cavalry) ? TBP_CAVALRY :
           (value == BasicUnitType::Artillery) ? TBP_ARTILLERY :
           TBP_OTHER;

  return id;
}

int basicTypeToLessThanID(BasicUnitType::value value)
{
  ASSERT(value < BasicUnitType::HowMany);
  int id = (value == BasicUnitType::Infantry) ? TBP_LESSINFANTRY :
           (value == BasicUnitType::Cavalry) ? TBP_LESSCAVALRY :
           (value == BasicUnitType::Artillery) ? TBP_LESSARTILLERY :
           TBP_LESSOTHER;

  return id;
}

int basicTypeToMoreThanID(BasicUnitType::value value)
{
  ASSERT(value < BasicUnitType::HowMany);
  int id = (value == BasicUnitType::Infantry) ? TBP_MOREINFANTRY :
           (value == BasicUnitType::Cavalry) ? TBP_MORECAVALRY :
           (value == BasicUnitType::Artillery) ? TBP_MOREARTILLERY :
           TBP_MOREOTHER;

  return id;
}

int basicTypeToAutoID(BasicUnitType::value value)
{
  ASSERT(value < BasicUnitType::HowMany);
  int id = (value == BasicUnitType::Infantry) ? TBP_AUTOINFANTRY :
           (value == BasicUnitType::Cavalry) ? TBP_AUTOCAVALRY :
           (value == BasicUnitType::Artillery) ? TBP_AUTOARTILLERY :
           TBP_AUTOOTHER;

  return id;
}

void TownBuildPage::onCommand(HWND hwnd, int id, HWND hwndCtl, UINT codeNotify)
{
  ITown town = owner()->currentTown();
  switch(id)
  {
    case TBP_INFANTRY:
    case TBP_CAVALRY:
    case TBP_ARTILLERY:
    case TBP_OTHER:
    {
      if(town != NoTown)
      {
        const Town& t = campData()->getTown(town);
        const Province& prov = campData()->getProvince(t.getProvince());

        RECT r;
        GetWindowRect(hwndCtl, &r);

        d_basicType = idToBasicType(id);

        PixelPoint p(r.left, r.top);

        BuildItemInterface::Data data;
        data.d_nation = prov.getNationality();
        data.d_what = d_basicType;
        data.d_currentType = owner()->getBuild(d_basicType).getWhat();

        BuildItemInterface::init(data);
        BuildItemInterface::show(p);
      }
      break;
    }

    case UnitTypeWindowID:
    {
      ASSERT(d_basicType < BasicUnitType::HowMany);

      UnitType t = BuildItemInterface::currentType();
      BuildItem& item = owner()->getBuild(d_basicType);

      Boolean redraw = True;
      if(t != NoUnitType)
      {
        if(t != item.getWhat())
        {
          ASSERT(t < campData()->getMaxUnitType());
          const UnitTypeItem& uit = campData()->getUnitType(t);

          AttributePoints tRes = 0;
          AttributePoints tMan = 0;

          uit.getTotalResource(tMan, tRes);
          item.setResources(tMan, tRes);

          item.setQuantity(1);
          item.setWhat(t);
          item.setChanged();
        }
        else
          redraw = False;
      }
      else
        item.reset();

      enableControls();

      if(redraw)
        redrawItems();
      break;
    }

    case TBP_MOREINFANTRY:
    case TBP_MORECAVALRY:
    case TBP_MOREARTILLERY:
    case TBP_MOREOTHER:
    {
      if(town != NoTown)
      {

        BasicUnitType::value basicType = moreThanIDToBasicType(id);
        BuildItem& b = owner()->getBuild(basicType);
        ASSERT(b.getQuantity() > 0);

        ASSERT(b.getWhat() < campData()->getMaxUnitType());
        const UnitTypeItem& uit = campData()->getUnitType(b.getWhat());

        AttributePoints tRes = 0;
        AttributePoints tMan = 0;

        uit.getTotalResource(tMan, tRes);
        b.setResources(b.getManpower()+tMan, b.getResource()+tRes);

        b.setQuantity(b.getQuantity()+1);

        enableControls();
        redrawItems();
      }
      break;
    }

    case TBP_LESSINFANTRY:
    case TBP_LESSCAVALRY:
    case TBP_LESSARTILLERY:
    case TBP_LESSOTHER:
    {
      if(town != NoTown)
      {
        BasicUnitType::value basicType = lessThanIDToBasicType(id);
        BuildItem& b = owner()->getBuild(basicType);

        ASSERT(b.getQuantity() > 1);
        ASSERT(b.getWhat() < campData()->getMaxUnitType());

        const UnitTypeItem& uit = campData()->getUnitType(b.getWhat());

        AttributePoints tRes = 0;
        AttributePoints tMan = 0;

        uit.getTotalResource(tMan, tRes);
        b.reduceNeeded(tMan, tRes);

        b.setQuantity(maximum(1, b.getQuantity()-1));

        enableControls();
        redrawItems();
      }
      break;
    }

    case TBP_AUTOINFANTRY:
    case TBP_AUTOCAVALRY:
    case TBP_AUTOARTILLERY:
    case TBP_AUTOOTHER:
    {
      if(codeNotify == BN_CLICKED)
      {
        if(town != NoTown)
        {
          BasicUnitType::value basicType = autoIDToBasicType(id);
          BuildItem& b = owner()->getBuild(basicType);

          b.toggleAutoBuild();

          CustomButton tb(hwndCtl);
          tb.setCheck(b.getAuto());

          enableControls();
        }
      }
    }
  }
}

void TownBuildPage::redrawItems()
{
  for(BasicUnitType::value v = BasicUnitType::Infantry; v < BasicUnitType::HowMany; INCREMENT(v))
  {
    HWND h = GetDlgItem(getHWND(), basicTypeToID(v));
    ASSERT(h);

    InvalidateRect(h, NULL, FALSE);
  }
}

void TownBuildPage::initControls()
{
  enableControls();
}

void TownBuildPage::drawTextDib(int cx, int cy)
{
  /*
   * Some useful values
   * Note: All positioning is done with dialog units
   */

  static LONG dbUnits = GetDialogBaseUnits();

  const LONG dbX = LOWORD(dbUnits);
  const LONG dbY = HIWORD(dbUnits);

  owner()->allocateStaticDib(cx, cy);
  ASSERT(owner()->dib());

  ASSERT(d_fillDib);

  /*
   * Fill in background
   */

  owner()->dib()->rect(0, 0, cx, cy, d_fillDib);

  int fontHeight = (dbY*7)/8;

  LogFont lf;
  lf.height(fontHeight);
  lf.weight(FW_MEDIUM);
  lf.face(scenario->fontName(Font_Bold));

  Font font;
  font.set(lf);

  owner()->dib()->setFont(font);

  ITown iTown = owner()->currentTown();
  if(iTown != NoTown)
  {
    TownInfoDib::drawBuildInfoDib(campData(), owner()->getAttributes(), owner()->dib(), dbX, dbY);
  }
}

void TownBuildPage::drawBuildItemDib(BasicUnitType::value basicType, int cx, int cy)
{
  ASSERT(basicType < BasicUnitType::HowMany);

  /*
   * Some useful values
   * Note: All positioning is done with dialog units
   */

  static LONG dbUnits = GetDialogBaseUnits();

  const LONG dbX = LOWORD(dbUnits);
  const LONG dbY = HIWORD(dbUnits);

  owner()->allocateStaticDib(cx, cy);
  ASSERT(owner()->dib());
  ASSERT(d_fillDib);

  /*
   * Fill in background
   */

  owner()->dib()->rect(0, 0, cx, cy, d_fillDib);

  int fontHeight = (dbY*7)/8;

  LogFont lf;
  lf.height(fontHeight);
  lf.weight(FW_MEDIUM);
  lf.face(scenario->fontName(Font_Bold));

  Font font;
  font.set(lf);

  owner()->dib()->setFont(font);

  ITown iTown = owner()->currentTown();
  if(iTown != NoTown)
  {
    TownInfoDib::drawBuildTypeInfoDib(campData(), owner()->getAttributes(), basicType, owner()->getBuild(basicType), owner()->getTotalBuilds(), owner()->dib(), dbX, dbY);
  }
}

void TownBuildPage::onDrawItem(HWND hwnd, const DRAWITEMSTRUCT* lpDrawItem)
{
  HPALETTE oldPal = SelectPalette(lpDrawItem->hDC, Palette::get(), FALSE);
  RealizePalette(lpDrawItem->hDC);

  const int cx = lpDrawItem->rcItem.right - lpDrawItem->rcItem.left;
  const int cy = lpDrawItem->rcItem.bottom - lpDrawItem->rcItem.top;

  switch(lpDrawItem->CtlID)
  {
    case TBP_INFOTEXT:
      drawTextDib(cx, cy);
      CampaignUserInterface::blitDibToControl(owner()->dib(), lpDrawItem);
      break;

    case TBP_INFANTRY:
    case TBP_CAVALRY:
    case TBP_ARTILLERY:
    case TBP_OTHER:
      drawBuildItemDib(idToBasicType(lpDrawItem->CtlID), cx, cy);
      CampaignUserInterface::blitDibToControl(owner()->dib(), lpDrawItem);
      break;
  }

  SelectPalette(lpDrawItem->hDC, oldPal, FALSE);
}


void TownBuildPage::enableControls()
{
  for(BasicUnitType::value t = BasicUnitType::Infantry; t < BasicUnitType::HowMany; INCREMENT(t))
  {
    const BuildItem& item = owner()->getBuild(t);

    {
      CustomButton b(getHWND(), basicTypeToLessThanID(t));

      b.enable( (item.getQuantity() > 1) );
    }

    {
      CustomButton b(getHWND(), basicTypeToMoreThanID(t));

      b.enable( (item.isEnabled()) );
    }

    {
      CustomButton b(getHWND(), basicTypeToAutoID(t));

      b.enable( (item.isEnabled()) );
    }
  }
}

/*----------------------------------------------------------------------
 * Status Page
 */

class TownStatusPage : public TownDialogPage {
   const DrawDIB* d_fillDib;
public:
   TownStatusPage(const CampaignData* campData, TownDialog* p) :
     TownDialogPage(campData, p, townStatusPage),
     d_fillDib(scenario->getScenarioBkDIB(Scenario::BackPatterns::PaperBackground)) {}

   ~TownStatusPage();

   const char* getTitle() const { return s_strings.get(TownDialogStrings::Status); }
private:
   void initControls() {} // drawStatusDib(); }
   void updateValues() {}
   void enableControls() {}

   void drawStatusDib(int cx, int cy);

   BOOL procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
   BOOL onInitDialog(HWND hwnd, HWND hwndFocus, LPARAM lParam);
   void onDrawItem(HWND hwnd, const DRAWITEMSTRUCT* lpDrawItem);
};

TownStatusPage::~TownStatusPage()
{
}

BOOL TownStatusPage::procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
   switch(msg)
   {
      case WM_INITDIALOG:
         HANDLE_WM_INITDIALOG(hWnd, wParam, lParam, onInitDialog);
         break;
      case WM_DRAWITEM:
         HANDLE_WM_DRAWITEM(hWnd, wParam, lParam, onDrawItem);
         break;
      default:
         return FALSE;
   }

   return TRUE;
}

BOOL TownStatusPage::onInitDialog(HWND hwnd, HWND hwndFocus, LPARAM lParam)
{
  customDialog().init(hwnd);

  setPosition();
  return TRUE;
}

void TownStatusPage::onDrawItem(HWND hwnd, const DRAWITEMSTRUCT* lpDrawItem)
{
  HPALETTE oldPal = SelectPalette(lpDrawItem->hDC, Palette::get(), FALSE);
  RealizePalette(lpDrawItem->hDC);

  const int cx = lpDrawItem->rcItem.right - lpDrawItem->rcItem.left;
  const int cy = lpDrawItem->rcItem.bottom - lpDrawItem->rcItem.top;

  switch(lpDrawItem->CtlID)
  {
    case TSTP_SUMMARYTEXT:
      drawStatusDib(cx, cy);
      CampaignUserInterface::blitDibToControl(owner()->dib(), lpDrawItem);
      break;
  }

  SelectPalette(lpDrawItem->hDC, oldPal, FALSE);
}

void TownStatusPage::drawStatusDib(int cx, int cy)
{
  /*
   * Some useful values
   * Note: All positioning is done with dialog units
   */

  static LONG dbUnits = GetDialogBaseUnits();

  const LONG dbX = LOWORD(dbUnits);
  const LONG dbY = HIWORD(dbUnits);

  owner()->allocateStaticDib(cx, cy);
  ASSERT(owner()->dib());

  ASSERT(d_fillDib);

  /*
   * Fill in background
   */

  owner()->dib()->rect(0, 0, cx, cy, d_fillDib);

  /*
   * Draw Info
   */

  int fontHeight = (dbY*7)/8;

  LogFont lf;
  lf.height(fontHeight);
  lf.weight(FW_MEDIUM);
  lf.face(scenario->fontName(Font_Bold));

  Font font;
  font.set(lf);

  owner()->dib()->setFont(font);
  if(owner()->currentTown() != NoTown)
  {
    TownInfoDib::drawStatusDib(campData(), owner()->getAttributes(), owner()->dib(), dbX, dbY);
  }
}


/*----------------------------------------------------------------------
 * Town Page
 */

class TownTownPage : public TownDialogPage {
   const DrawDIB* d_fillDib;
public:
   TownTownPage(const CampaignData* campData, TownDialog* p) :
     TownDialogPage(campData, p, townTownPage),
     d_fillDib(scenario->getScenarioBkDIB(Scenario::BackPatterns::PaperBackground)) {}

   ~TownTownPage();

   const char* getTitle() const { return s_strings.get(TownDialogStrings::Town); }
private:
   void initControls() {} // drawTownDib(); }
   void updateValues() {}
   void enableControls() {}

   void drawTownDib(int cx, int cy);

   BOOL procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
   BOOL onInitDialog(HWND hwnd, HWND hwndFocus, LPARAM lParam);
   void onDrawItem(HWND hwnd, const DRAWITEMSTRUCT* lpDrawItem);
};

TownTownPage::~TownTownPage()
{
}

BOOL TownTownPage::procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
   switch(msg)
   {
      case WM_INITDIALOG:
         HANDLE_WM_INITDIALOG(hWnd, wParam, lParam, onInitDialog);
         break;
      case WM_DRAWITEM:
         HANDLE_WM_DRAWITEM(hWnd, wParam, lParam, onDrawItem);
         break;
      default:
         return FALSE;
   }

   return TRUE;
}

BOOL TownTownPage::onInitDialog(HWND hwnd, HWND hwndFocus, LPARAM lParam)
{
  customDialog().init(hwnd);

  setPosition();
  return TRUE;
}

void TownTownPage::onDrawItem(HWND hwnd, const DRAWITEMSTRUCT* lpDrawItem)
{
  HPALETTE oldPal = SelectPalette(lpDrawItem->hDC, Palette::get(), FALSE);
  RealizePalette(lpDrawItem->hDC);

  const int cx = lpDrawItem->rcItem.right - lpDrawItem->rcItem.left;
  const int cy = lpDrawItem->rcItem.bottom - lpDrawItem->rcItem.top;

  switch(lpDrawItem->CtlID)
  {
    case TTP_SUMMARYTEXT:
      drawTownDib(cx, cy);
      CampaignUserInterface::blitDibToControl(owner()->dib(), lpDrawItem);
      break;
  }

  SelectPalette(lpDrawItem->hDC, oldPal, FALSE);
}

void TownTownPage::drawTownDib(int cx, int cy)
{
  /*
   * Some useful values
   * Note: All positioning is done with dialog units
   */

  static LONG dbUnits = GetDialogBaseUnits();

  const LONG dbX = LOWORD(dbUnits);
  const LONG dbY = HIWORD(dbUnits);

  owner()->allocateStaticDib(cx, cy);
  ASSERT(owner()->dib());

  ASSERT(d_fillDib);

  /*
   * Fill in background
   */

  owner()->dib()->rect(0, 0, cx, cy, d_fillDib);

  /*
   * Draw Info
   */

  int fontHeight = (dbY*7)/8;

  LogFont lf;
  lf.height(fontHeight);
  lf.weight(FW_MEDIUM);
  lf.face(scenario->fontName(Font_Bold));

  Font font;
  font.set(lf);

  owner()->dib()->setFont(font);
  if(owner()->currentTown() != NoTown)
  {
    TownInfoDib::drawTownDib(campData(), owner()->getAttributes(), owner()->dib(), dbX, dbY);
  }
}

/*----------------------------------------------------------------------
 * Province Page
 */

class TownProvincePage : public TownDialogPage {
   const DrawDIB* d_fillDib;
public:
   TownProvincePage(const CampaignData* campData, TownDialog* p) :
     TownDialogPage(campData, p, townProvincePage),
     d_fillDib(scenario->getScenarioBkDIB(Scenario::BackPatterns::PaperBackground)) {}

   ~TownProvincePage();

   const char* getTitle() const { return s_strings.get(TownDialogStrings::Province); }
private:
   void initControls() {} // drawProvinceDib(); }
   void updateValues() {}
   void enableControls() {}

   void drawProvinceDib(int cx, int cy);

   BOOL procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
   BOOL onInitDialog(HWND hwnd, HWND hwndFocus, LPARAM lParam);
   void onDrawItem(HWND hwnd, const DRAWITEMSTRUCT* lpDrawItem);
};

TownProvincePage::~TownProvincePage()
{
}

BOOL TownProvincePage::procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
   switch(msg)
   {
      case WM_INITDIALOG:
         HANDLE_WM_INITDIALOG(hWnd, wParam, lParam, onInitDialog);
         break;
      case WM_DRAWITEM:
         HANDLE_WM_DRAWITEM(hWnd, wParam, lParam, onDrawItem);
         break;
      default:
         return FALSE;
   }

   return TRUE;
}

BOOL TownProvincePage::onInitDialog(HWND hwnd, HWND hwndFocus, LPARAM lParam)
{
  customDialog().init(hwnd);

  setPosition();
  return TRUE;
}

void TownProvincePage::onDrawItem(HWND hwnd, const DRAWITEMSTRUCT* lpDrawItem)
{
  HPALETTE oldPal = SelectPalette(lpDrawItem->hDC, Palette::get(), FALSE);
  RealizePalette(lpDrawItem->hDC);

  int cx = lpDrawItem->rcItem.right - lpDrawItem->rcItem.left;
  int cy = lpDrawItem->rcItem.bottom - lpDrawItem->rcItem.top;

  switch(lpDrawItem->CtlID)
  {
    case TPP_SUMMARYTEXT:
      drawProvinceDib(cx, cy);
      CampaignUserInterface::blitDibToControl(owner()->dib(), lpDrawItem);
      break;
  }

  SelectPalette(lpDrawItem->hDC, oldPal, FALSE);
}

void TownProvincePage::drawProvinceDib(int cx, int cy)
{
  /*
   * Some useful values
   * Note: All positioning is done with dialog units
   */

  static LONG dbUnits = GetDialogBaseUnits();

  const LONG dbX = LOWORD(dbUnits);
  const LONG dbY = HIWORD(dbUnits);

  owner()->allocateStaticDib(cx, cy);
  ASSERT(owner()->dib());

  ASSERT(d_fillDib);

  /*
   * Fill in background
   */

  owner()->dib()->rect(0, 0, cx, cy, d_fillDib);

  /*
   * Draw Info
   */

  int fontHeight = (dbY*7)/8;

  LogFont lf;
  lf.height(fontHeight);
  lf.weight(FW_MEDIUM);
  lf.face(scenario->fontName(Font_Bold));

  Font font;
  font.set(lf);

  owner()->dib()->setFont(font);
  if(owner()->currentTown() != NoTown)
  {
    TownInfoDib::drawProvinceDib(campData(), owner()->getAttributes(), owner()->dib(), dbX, dbY);
  }
}


/*
 * Unit Dialog allocation ----------------
 */

TownDialogPage* TownDialogPage::createPage(TownDialogPage::Page page, const CampaignData* campData, TownDialog* p)
{
  ASSERT(page < HowMany);

  if(page == SummaryPage)
    return new TownSummaryPage(campData, p);
  else if(page == BuildPage)
    return new TownBuildPage(campData, p);
  else if(page == StatusPage)
    return new TownStatusPage(campData, p);
  else if(page == TownPage)
    return new TownTownPage(campData, p);
  else
    return new TownProvincePage(campData, p);
}


/*-------------------------------------------------------------------
 *
 */

class TownOrderDial : public TownDialog  {
    CustomDrawnTab d_customTab;
    TownDialogPage* d_pages[TownDialogPage::HowMany];
    TownDialogPage::Page d_currentPage;
    Boolean d_hasBuildPage;
    Boolean d_hasStatusPage;
    BuildItem d_builds[BasicUnitType::HowMany];

    TownAttributes d_townAttributes;

    const DrawDIB* d_fillDib;

    enum { TBD_TABBED = 200 };
  public:
    TownOrderDial(CampaignUserInterface* owner, const CampaignData* campData);
    ~TownOrderDial();

    void onCommand(HWND hWnd, int id, HWND hwndCtl, UINT codeNotify);
    void initControls();
    void hide();
    void enableControls();

    BuildItem& getBuild(int i)
    {
      ASSERT(i < BasicUnitType::HowMany);
      return d_builds[i];
    }

    int getTotalBuilds() const;

    void show(const PixelPoint& p);

    TownAttributes& getAttributes() { return d_townAttributes; }

  private:
    BOOL procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
    BOOL onInitDialog(HWND hwnd, HWND hwndFocus, LPARAM lParam);
    void onDestroy(HWND hwnd) {}
    void onMeasureItem(HWND hwnd, MEASUREITEMSTRUCT* lpMeasureItem);
    void onDrawItem(HWND hwnd, const DRAWITEMSTRUCT* lpDrawItem);
    LRESULT onNotify(HWND hWnd, int id, NMHDR* lpNMHDR);

    void setTabs();
    void onSelChanged();
    void sendOrder();
    void drawHeaderDib(int cx, int cy);
};


TownOrderDial::TownOrderDial(CampaignUserInterface* owner, const CampaignData* campData) :
  TownDialog(owner, campData),
  d_currentPage(TownDialogPage::SummaryPage),
  d_hasBuildPage(True),
  d_hasStatusPage(True),
  d_fillDib(scenario->getSideBkDIB(SIDE_Neutral))
{
  for(int i = 0; i < TownDialogPage::HowMany; i++)
  {
    d_pages[i] = 0;
  }

  HWND dhwnd = createDialog(townBuildDialog, APP::getMainHWND(), False);
  ASSERT(dhwnd != NULL);
}

TownOrderDial::~TownOrderDial()
{
  deleteStaticDib();
}

BOOL TownOrderDial::procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
   switch(msg)
   {
      case WM_INITDIALOG:
         HANDLE_WM_INITDIALOG(hWnd, wParam, lParam, onInitDialog);
         break;
      case WM_DESTROY:
         HANDLE_WM_DESTROY(hWnd, wParam, lParam, onDestroy);
         break;
      case WM_COMMAND:
         HANDLE_WM_COMMAND(hWnd, wParam, lParam, onCommand);
         break;
      case WM_DRAWITEM:
         HANDLE_WM_DRAWITEM(hWnd, wParam, lParam, onDrawItem);
         break;
      case WM_NOTIFY:
         HANDLE_WM_NOTIFY(hWnd, wParam, lParam, onNotify);
         break;
      default:
         return FALSE;
   }

   return TRUE;
}

BOOL TownOrderDial::onInitDialog(HWND hwnd, HWND hwndFocus, LPARAM lParam)
{

  customDialog().setBkDib(scenario->getSideBkDIB(SIDE_Neutral));
  customDialog().setCaptionBkDib(scenario->getScenarioBkDIB(Scenario::BackPatterns::WoodBackground));
  customDialog().init(hwnd);


  /*
   * Create tabbed pages
   */

  for(int i = 0; i < TownDialogPage::HowMany; i++)
  {
    d_pages[i] = TownDialogPage::createPage(static_cast<TownDialogPage::Page>(i), campData(), this);
    ASSERT(d_pages[i] != 0);
  }

  for(int id = TBD_FIRSTBUTTON; id < TBD_LASTBUTTON; id++)
  {
    CustomButton cb(hwnd, id);
    cb.setFillDib(scenario->getScenarioBkDIB(Scenario::BackPatterns::WoodBackground));
    cb.setBorderColours(scenario->getBorderColors());
  }

  /*
   * Set Position of page
   */

  // top left corner of page (in dialog units)
  const LONG baseX = 3;
  const LONG baseY = 32;

  // get size of dialog(use first page)
  // this is in dialog units
  DLGTEMPLATE* dialog = d_pages[TownDialogPage::First]->getDialog();

  RECT& r = tabbedDialogRect();
  SetRect(&r, baseX, baseY, baseX+dialog->cx, baseY+dialog->cy);

  // convert dialog units to pixel coordinates
  MapDialogRect(hwnd, &r);

  /*
   * Create a tabbed window
   */

  HWND hwndTab = CreateWindow(
      WC_TABCONTROL, "",
      WS_CHILD | WS_CLIPSIBLINGS | WS_VISIBLE |
         TCS_TOOLTIPS | TCS_FIXEDWIDTH,
      0, 0, r.right-r.left, 100,
      hwnd,
      (HMENU) TBD_TABBED,
      APP::instance(),
      NULL);

   ASSERT(hwndTab != NULL);

   /*
    * Set up the tabbed titles
    *
    * This must be done before using AdjustRect
    */

   TC_ITEM tie;
   tie.mask = TCIF_TEXT | TCIF_PARAM;

   for(i = 0; i < TownDialogPage::HowMany; i++)
   {
      tie.pszText = (char*) d_pages[i]->getTitle();

      tie.lParam = i;
      TabCtrl_InsertItem(hwndTab, i, &tie);
   }

   /*
    * Set size of tabs (in Dialog Units)
    */

   const LONG tabWidth = 40;
   const LONG tabHeight = 13;

   RECT tRect;
   SetRect(&tRect, 0, 0, tabWidth, tabHeight);

   MapDialogRect(hwnd, &tRect);

   TabCtrl_SetItemSize(hwndTab, tRect.right, tRect.bottom);


   /*
    * Convert coordinates to pixel coordinates
    * and Move the window
    */

  // Work out complete size with tabs, and shift to desired location
  TabCtrl_AdjustRect(hwndTab, TRUE, &r);

  // Move tabbed Window
  SetWindowPos(hwndTab, NULL, r.left, r.top, r.right - r.left, r.bottom - r.top, SWP_NOZORDER);

  // Get display area
  TabCtrl_AdjustRect(hwndTab, FALSE, &r);    // Get display area

  /*
   * Initialize custom drawn tab class
   */

  d_customTab.tabHWND(hwndTab);
  d_customTab.setBkFillDib(scenario->getSideBkDIB(SIDE_Neutral));
//  d_customTab.setTabFillDib(scenario->getScenarioBkDIB(Scenario::BackPatterns::PaperBackground));

  // get base font size (in dialog units)
  static LONG dbUnits = GetDialogBaseUnits();
  const LONG dbY = HIWORD(dbUnits);
  const int fontSize = (6*dbY)/8;

  LogFont lf;
  lf.height(fontSize);
  lf.weight(FW_MEDIUM);
  lf.face(scenario->fontName(Font_Bold));

  Font font;
  font.set(lf);

  d_customTab.setFont(font);

  /*
   * Initialize tabbed pages
   */

  for(i = 0; i < TownDialogPage::HowMany; i++)
  {
    d_pages[i]->create();
  }

  onSelChanged();

  return True;
}

void TownOrderDial::initControls()
{
  /*
   * Get province builds
   */

  if(currentTown() != NoTown)
  {
    const Town& t = campData()->getTown(currentTown());
    Province& p = campData()->getProvince(t.getProvince());

    for(int i = 0; i < BasicUnitType::HowMany; i++)
    {
      BuildItem& item = p.getBuild(i);

      if(item.isEnabled())
      {
        d_builds[i] = item;
      }
      else
        d_builds[i].reset();
    }

    d_townAttributes.reset();
    d_townAttributes.makeAttributes(currentTown(), campData());

//  drawHeaderDib();
  }

  setTabs();

  d_pages[d_currentPage]->initControls();

  enableControls();
}

void TownOrderDial::drawHeaderDib(int cx, int cy)
{
  /*
   * Some useful values
   * Note: All positioning is done with dialog units
   */

  static LONG dbUnits = GetDialogBaseUnits();

  const LONG dbX = LOWORD(dbUnits);
  const LONG dbY = HIWORD(dbUnits);

  allocateStaticDib(cx, cy);
  ASSERT(dib());

  ASSERT(d_fillDib);

  /*
   * Fill in background
   */

  dib()->rect(0, 0, cx, cy, d_fillDib);

  /*
   * Draw Info
   */

  int fontHeight = (dbY*8)/8;

  LogFont lf;
  lf.height(fontHeight);
  lf.weight(FW_MEDIUM);
  lf.face(scenario->fontName(Font_Bold));
  lf.italic(true);

  Font font;
  font.set(lf);

  dib()->setFont(font);
  if(currentTown() != NoTown)
  {
    TownInfoDib::drawHeaderDib(campData(), d_townAttributes, dib(), cx, cy, dbX, dbY);
  }
}

void TownOrderDial::setTabs()
{
  /*
   * Add or remove 'Build' tab depending on whether town resources are available
   */

  ITown iTown = currentTown();
  if(iTown != NoTown)
  {
    if( (campData()->isTownOrderable(iTown)) &&
        (d_townAttributes.availableResource() > 0) &&
        (d_townAttributes.availableManpower() > 0) )
    {
      if(!d_hasBuildPage)
      {
        TC_ITEM tie;
        tie.mask = TCIF_TEXT | TCIF_PARAM;

        for(int i = TownDialogPage::BuildAllowedOnly_First; i < TownDialogPage::BuildAllowedOnly_Last; i++)
        {
          tie.lParam = i;

          tie.pszText = (char*) d_pages[i]->getTitle();
          TabCtrl_InsertItem(d_customTab.tabHWND(), i, &tie);
        }

        d_hasBuildPage = True;
      }
    }
    else
    {
      if(d_hasBuildPage)
      {
        for(int i = TownDialogPage::BuildAllowedOnly_First; i < TownDialogPage::BuildAllowedOnly_Last; i++)
        {
          TabCtrl_DeleteItem(d_customTab.tabHWND(), TownDialogPage::BuildAllowedOnly_First);
        }

        d_hasBuildPage = False;
      }
    }
  }

  /*
   * Add or remove 'Status' tab depending on whether town is seen
   */

  if(iTown != NoTown)
  {
    int start = (d_hasBuildPage) ? TownDialogPage::SeenOnly_First :
                                   TownDialogPage::SeenOnly_First-1;

    int stop = (d_hasBuildPage) ? TownDialogPage::SeenOnly_Last :
                                  TownDialogPage::SeenOnly_Last-1;

    if(d_townAttributes.isSeen())
    {
      if(!d_hasStatusPage)
      {
        TC_ITEM tie;
        tie.mask = TCIF_TEXT | TCIF_PARAM;

        for(int i = start; i < stop; i++)
        {
          tie.lParam = i;

          tie.pszText = (char*) d_pages[i]->getTitle();
          TabCtrl_InsertItem(d_customTab.tabHWND(), i, &tie);
        }

        d_hasStatusPage = True;
      }
    }
    else
    {
      if(d_hasStatusPage)
      {
        for(int i = start; i < stop; i++)
        {
          TabCtrl_DeleteItem(d_customTab.tabHWND(), start);
        }

        d_hasStatusPage = False;
      }
    }
  }
}

void TownOrderDial::sendOrder()
{
  if(currentTown() != NoTown)
  {
    for(BasicUnitType::value i = BasicUnitType::Infantry; i < BasicUnitType::HowMany; INCREMENT(i))
    {
      if(d_builds[i].changed())
      {
        d_builds[i].changed(False);
        TownOrder::send(currentTown(), i, d_builds[i]);
      }
    }
  }
}

void TownOrderDial::onCommand(HWND hwnd, int id, HWND hwndCtl, UINT codeNotify)
{
  switch(id)
  {
    case TBD_APPLY:
    {
      sendOrder();
      break;
    }

    case TBD_CANCEL:
    {
      for(int i = 0; i < BasicUnitType::HowMany; i++)
        d_builds[i].reset();

      owner()->cancelOrder();
      break;
    }

    case TBD_OK:
    {
      Boolean changed = False;
      for(int i = 0; i < BasicUnitType::HowMany; i++)
      {
        if(d_builds[i].changed())
        {
          changed = True;
          break;
        }
      }

      if(changed)
        sendOrder();

      owner()->cancelOrder();
      break;
    }
  }
}

void TownOrderDial::onDrawItem(HWND hwnd, const DRAWITEMSTRUCT* lpDrawItem)
{
  HPALETTE oldPal = SelectPalette(lpDrawItem->hDC, Palette::get(), FALSE);
  RealizePalette(lpDrawItem->hDC);

  const int cx = lpDrawItem->rcItem.right - lpDrawItem->rcItem.left;
  const int cy = lpDrawItem->rcItem.bottom - lpDrawItem->rcItem.top;

  switch(lpDrawItem->CtlID)
  {
    case TBD_HEADERTEXT:
      drawHeaderDib(cx, cy);
      CampaignUserInterface::blitDibToControl(dib(), lpDrawItem);
      break;
  }

  SelectPalette(lpDrawItem->hDC, oldPal, FALSE);
}

LRESULT TownOrderDial::onNotify(HWND hWnd, int id, NMHDR* lpNMHDR)
{

   // Assume its from the tabbed dialog

   if(lpNMHDR->idFrom == TBD_TABBED)
   {
      switch(lpNMHDR->code)
      {
      case TCN_SELCHANGING:
         return FALSE;

      case TCN_SELCHANGE:
         onSelChanged();
         break;
      }
   }

   return TRUE;
}

void TownOrderDial::onSelChanged()
{
  ITown iTown  = currentTown();

  if(iTown != NoTown)
  {
    int iSel = TabCtrl_GetCurSel(d_customTab.tabHWND());
    TC_ITEM tci;

    tci.mask = TCIF_PARAM;

    TabCtrl_GetItem(d_customTab.tabHWND(), iSel, &tci);

    TownDialogPage::Page newPage = static_cast<TownDialogPage::Page>(tci.lParam);

    if(d_currentPage != newPage)
    {
      d_pages[d_currentPage]->updateValues();
      d_pages[d_currentPage]->disable();
    }

    d_pages[newPage]->enable();

    d_currentPage = newPage;
    initControls();
  }
}


void TownOrderDial::enableControls()
{
  {
    CustomButton b(getHWND(), TBD_APPLY);
    b.enable(d_currentPage == TownDialogPage::BuildPage); // && (changed)) );
  }
}

void TownOrderDial::show(const PixelPoint& p) //const MapWindowData& mapData, const Location& l)
{
  TabCtrl_SetCurSel(d_customTab.tabHWND(), TownDialogPage::First);

  onSelChanged();
  SetWindowPos(getHWND(), HWND_TOP, p.getX(), p.getY(), 0, 0, SWP_SHOWWINDOW | SWP_NOSIZE);
}

int TownOrderDial::getTotalBuilds() const
{
  int total = 0;

  for(int i = 0; i < BasicUnitType::HowMany; i++)
    total += d_builds[i].getQuantity();

  return total;
}

void TownOrderDial::hide()
{
  for(int i = 0; i < TownDialogPage::HowMany; i++)
  {
    d_pages[i]->updateValues();
  }
  ShowWindow(getHWND(), SW_HIDE);
}

/* ------------------------------ Dialog Creation function ---------------
 *
 */

TownDialog* TownDialog::create(CampaignUserInterface* owner, const CampaignData* campData)
{
  return new TownOrderDial(owner, campData);
}


/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.3  2001/07/02 15:23:17  greenius
 * Battle, Campaign, Combined game and Campaign Editor now compile and run... mostly
 *
 * Revision 1.2  2001/06/18 08:25:45  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
