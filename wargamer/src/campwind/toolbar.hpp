/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef TOOLBAR_H
#define TOOLBAR_H

#ifndef __cplusplus
#error toolbar.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1995, Steven Morle-Green, All Rights Reserved
 *----------------------------------------------------------------------
 *
 *	Wargamer Tool Bar
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.3  1995/12/05 09:20:07  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.2  1995/11/13 11:11:05  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.1  1995/10/16 11:33:16  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#include "mytypes.h"
#include "wind.hpp"

class CampToolBar_Imp;
class PixelPoint;
class CampaignWindowsInterface;

class ToolBar : public Window
{
        CampToolBar_Imp* d_toolBar;
    public:
        ToolBar(HWND parent, CampaignWindowsInterface* cw);
        ~ToolBar();

        void position(const PixelPoint& p);
        // void show();
        // void destroy();
        void setCheck(int menuID, Boolean f);
        void enable(int menuID, Boolean f);
        int height() const;

	    virtual HWND getHWND() const;
        virtual bool isVisible() const;
        virtual bool isEnabled() const;
        virtual void show(bool visible);
        virtual void enable(bool enable);

        // Boolean shouldShow() const;
        // void suspend(bool visible);
};


#endif /* TOOLBAR_H */

