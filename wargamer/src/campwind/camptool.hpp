/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef CAMPTOOL_H
#define CAMPTOOL_H

#ifndef __cplusplus
#error camptool.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1995, Steven Morle-Green, All Rights Reserved
 *----------------------------------------------------------------------
 *
 *	Campaign Floating Toolbar class
 *
 *----------------------------------------------------------------------
 */


#include "gamedefs.hpp"
#include "cpdef.hpp"
#include "wind.hpp"

class MapWindow;					// Window that toolbar affects
class CompleteTinyMapWindow;
class CampaignWindowsInterface;

class TinyMap : public Window
{
	 CompleteTinyMapWindow* d_tinyMap;
  public:
	 TinyMap() :
		d_tinyMap(0) {}

	 TinyMap(HWND parent, CampaignWindowsInterface* cw, MapWindow* map) :
		d_tinyMap(0) { create(parent, cw, map); }
	 ~TinyMap();

	 void create(HWND parent, CampaignWindowsInterface* cw, MapWindow* map);
	 void areaChanged();
	 void draw(ConstICommandPosition cpi);
	 void magnificationUpdated();
	 // void destroy();
	 // void toggle();
	 // void show();
     // void hide();
	 // Boolean isShowing();
     // 
	 // void suspend(bool visible);
	    virtual HWND getHWND() const;
        virtual bool isVisible() const;
        virtual bool isEnabled() const;
        virtual void show(bool visible);
        virtual void enable(bool enable);
};

#endif /* CAMPTOOL_H */

