/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 * Base class for orders window
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "ordertb.hpp"
#include "dib.hpp"

/*
 * OrderTB Functions: 
 * These ought to be in mapwind, seeing that is where the definition is.
 */


OrderTB::OrderTB(CampaignWindowsInterface* campWind, const CampaignData* campData, HWND h, MapWindow* map) :
   d_campWind(campWind),
   d_campData(campData)
{
  parentHWND = h;
  mapWindow = map;
  dib = 0;
}


void OrderTB::createDIB(HWND hwnd)
{
   RECT cRect;
   GetClientRect(hwnd, &cRect);

   if(dib)
   {
     delete dib;
     dib = 0;
   }

   if(dib == 0)
   {
      // Height must be -ve to go from top down

      dib = new DrawDIBDC(cRect.right - cRect.left, (cRect.bottom - cRect.top));
   }

   ASSERT(dib != 0);

}

void OrderTB::drawInfo(HDC hdc)
{

   ASSERT(dib != 0);

   drawDIB();  // virtual

   BitBlt(hdc, 0,0, dib->getWidth(), dib->getHeight(),
            dib->getDC(), 0,0,
            SRCCOPY);
}



/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.2  2001/06/18 08:25:45  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
