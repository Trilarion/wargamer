/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef WEATHWIN_HPP
#define WEATHWIN_HPP

#include "mytypes.h"
#include "wind.hpp"

class CampaignData;
class CampaignWindowsInterface;
class WeatherWindow;

class CampaignWeatherWindow : public Window
{
	 WeatherWindow* d_weatherWindow;
  public:
	 CampaignWeatherWindow() : d_weatherWindow(0) {}
	 CampaignWeatherWindow(CampaignWindowsInterface* campWind, const CampaignData* campData, HWND parent);
	 ~CampaignWeatherWindow();

	 void create(CampaignWindowsInterface* campWind, const CampaignData* campData, HWND parent);

	 void update();
	 // void toggle();
	 // void destroy();
	 // void show();
     // void hide();
	 // Boolean isShowing();
	 // void suspend(bool visible);

	virtual HWND getHWND() const;
    virtual bool isVisible() const;
    virtual bool isEnabled() const;
    virtual void show(bool visible);
    virtual void enable(bool enable);

};

#endif
