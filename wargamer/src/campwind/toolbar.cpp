/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1995, Steven Morle-Green, All Rights Reserved
 *----------------------------------------------------------------------
 *
 * Tool bar for Wargamer
 *
 * I give up trying to get status line hints to work.
 *
 *----------------------------------------------------------------------
 */


#include "stdinc.hpp"
#include "toolbar.hpp"
#include "resdef.h"
#include "scn_res.h"
#include "scenario.hpp"
#include "scn_img.hpp"
#include "cwin_int.hpp"
#include "tooltip.hpp"
#include "res_str.h"
#include "gtoolbar.hpp"
#include "fonts.hpp"

#define DEBUG_MTOOLBAR
#ifdef DEBUG_MTOOLBAR
#include "logwin.hpp"
#endif


// #define DISABLE_OB_WINDOW

/*------------------------------------------------------------
 *  The mother window
 */

class CampToolBar_Imp : public GToolBar {
    CampaignWindowsInterface* d_campWind;

    /*
     * Each section has its own MToolBar
     */

    enum Sections {
       File,
       Zoom,
       MapDisplay,
//     MapMode,
       Info,
//     Help,

       Sections_HowMany
    };

    /*
     * Each Section has its own Button enum
     */

    enum FileButtons
    {
      Open,
      Save,
      File_HowMany
    };

    enum ZoomButtons
    {
      ZoomIn,
      ZoomOut,
      Zoom_HowMany
    };

    enum MapDisplayButtons
    {
      TinyMap,
      WeatherWindow,
      MessageWindow,
      ShowOrders,
      ShowSupply,
      ShowSupplyLines,
      ShowChokePoints,
      MapDisplay_HowMany
    };


    enum InfoButtons
    {
#if !defined(DISABLE_OB_WINDOW)
      OBWindow,
#endif
      IUnitWindow,
      ILeaderWindow,
      FindTown,
      FindLeader,
      Info_HowMany
    };

    enum HelpButtons
    {
      OHR,
      HelpButton,
      Help_HowMany
    };

    static TipData s_FileTipData[];
    static TipData s_ZoomTipData[];
    static TipData s_MapDisplayTipData[];
    static TipData s_MapModeTipData[];
    static TipData s_InfoTipData[];
    static TipData s_HelpTipData[];

    static TipData* s_td[Sections_HowMany];

    static ToolButtonData s_FileBD[File_HowMany];
    static ToolButtonData s_ZoomBD[Zoom_HowMany];
    static ToolButtonData s_MapDisplayBD[MapDisplay_HowMany];
//  static ToolButtonData s_MapModeBD[MapMode_HowMany];
    static ToolButtonData s_InfoBD[Info_HowMany];
//  static ToolButtonData s_HelpBD[Help_HowMany];

    static ToolButtonData* s_bd[Sections_HowMany];

    static int s_howMany[Sections_HowMany];
    static const InGameText::ID s_capNames[Sections_HowMany];
    static const char* s_regNames[Sections_HowMany];

  public:
    CampToolBar_Imp(HWND hParent, CampaignWindowsInterface* cw);
    ~CampToolBar_Imp() {}

    void positionWindows() { d_campWind->positionWindows(); }
    void updateAll()       { d_campWind->updateAll(); }
};

/*------------------------------------------------------------------------
 * New Data
 */

/*
 * Buttons
 */

static const int ButtonMode_Normal = WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON;
static const int ButtonMode_OnOff = WS_CHILD | WS_VISIBLE | BS_AUTOCHECKBOX | BS_PUSHLIKE;

ToolButtonData CampToolBar_Imp::s_FileBD[File_HowMany] = {
   { IDM_LOADGAME, ButtonMode_Normal, TBI_Open },
   { IDM_SAVEGAME, ButtonMode_Normal, TBI_Save }
};

ToolButtonData CampToolBar_Imp::s_ZoomBD[Zoom_HowMany] = {
   { IDM_ZOOMIN,  ButtonMode_Normal, TBI_ZoomIn },
   { IDM_ZOOMOUT, ButtonMode_Normal, TBI_ZoomOut }
};

ToolButtonData CampToolBar_Imp::s_MapDisplayBD[MapDisplay_HowMany] = {
   { IDM_TINYMAPWINDOW, ButtonMode_OnOff, TBI_TinyMap },
   { IDM_WEATHERWINDOW, ButtonMode_OnOff, TBI_Weather },
   { IDM_MESSAGEWINDOW, ButtonMode_OnOff, TBI_MessageWindow },
   { IDM_SHOWALLORDERS, ButtonMode_OnOff, TBI_Movement },
   { IDM_SHOWSUPPLY, ButtonMode_OnOff, TBI_Supply },
   { IDM_SHOWSUPPLYLINES, ButtonMode_OnOff, TBI_SupplyLines },
   { IDM_SHOWCHOKEPOINTS, ButtonMode_OnOff, TBI_ChokePoints }
};

// ToolButtonData CampToolBar_Imp::s_MapModeBD[MapMode_HowMany] = {
//    { IDM_RESOURCEMODE,  ButtonMode_OnOff, TBI_Resource },
//    { IDM_UNITMODE,      ButtonMode_OnOff, TBI_Unit }
// };

ToolButtonData CampToolBar_Imp::s_InfoBD[Info_HowMany] = {
#if !defined(DISABLE_OB_WINDOW)
   { IDM_OB,         ButtonMode_OnOff, TBI_OB         },
#endif
   { IDM_REPOPOOL,   ButtonMode_OnOff, TBI_IUnit      },
   { IDM_ILEADERS,   ButtonMode_OnOff, TBI_ILeader    },
   { IDM_FINDTOWN,   ButtonMode_OnOff, TBI_FindTown   },
   { IDM_FINDLEADER, ButtonMode_OnOff, TBI_FindLeader }
};

//ToolButtonData  CampToolBar_Imp::s_HelpBD[Help_HowMany] = {
// { IDM_HELP_HISTORY,  ButtonMode_Normal, TBI_OHR },
// { IDM_HELP_CONTENTS, ButtonMode_Normal, TBI_Help }
//};



/*
 * Tool Tips
 */

TipData CampToolBar_Imp::s_FileTipData[] = {
   { Open, TTS_CTB_Open, IDM_LOADGAME },
   { Save, TTS_CTB_Save, IDM_SAVEGAME },
   EndTipData
};

TipData CampToolBar_Imp::s_ZoomTipData[] = {
   { ZoomIn,   TTS_CTB_ZoomIn,   IDM_ZOOMIN  },
   { ZoomOut,  TTS_CTB_ZoomOut,  IDM_ZOOMOUT },
   EndTipData
};

TipData CampToolBar_Imp::s_MapDisplayTipData[] = {
   { TinyMap,        TTS_CTB_TinyMap,        IDM_TINYMAPWINDOW },
   { WeatherWindow,  TTS_CTB_WeatherWindow,  IDM_WEATHERWINDOW },
   { MessageWindow,  TTS_CTB_MessageWindow,  IDM_MESSAGEWINDOW },
   { ShowOrders,     TTS_CTB_ShowAllOrders,  IDM_SHOWALLORDERS },
   { ShowSupply,     TTS_CTB_ShowSupply,     IDM_SHOWSUPPLY },
   { ShowSupplyLines,   TTS_CTB_ShowSupplyLines,   IDM_SHOWSUPPLYLINES },
   { ShowChokePoints,   TTS_CTB_ShowChokePoints,   IDM_SHOWCHOKEPOINTS },
   EndTipData
};

// TipData CampToolBar_Imp::s_MapModeTipData[] = {
//    { ResourceMode,   TTS_CTB_ResourceMode,   IDM_RESOURCEMODE  },
//    { UnitMode,       TTS_CTB_UnitMode,       IDM_UNITMODE      },
//    EndTipData
// };

TipData CampToolBar_Imp::s_InfoTipData[] = {
#if !defined(DISABLE_OB_WINDOW)
   { OBWindow,       TTS_CTB_OrderBattle, IDM_OB         },
#endif
   { IUnitWindow,    TTS_CTB_IUnit,       IDM_REPOPOOL   },
   { ILeaderWindow,  TTS_CTB_ILeaders,    IDM_ILEADERS   },
   { FindTown,       TTS_CTB_FindTown,    IDM_FINDTOWN   },
   { FindLeader,     TTS_CTB_FindLeader,  IDM_FINDLEADER },
   EndTipData
};

//TipData CampToolBar_Imp::s_HelpTipData[] = {
// { OHR,         TTS_CTB_HistoricalRef,  IDM_HELP_HISTORY  },
// { HelpButton,  TTS_CTB_Help,           IDM_HELP_CONTENTS },
// EndTipData
//};


ToolButtonData* CampToolBar_Imp::s_bd[Sections_HowMany] = {
   s_FileBD,
   s_ZoomBD,
   s_MapDisplayBD,
// s_MapModeBD,
   s_InfoBD,
// s_HelpBD
};

TipData* CampToolBar_Imp::s_td[Sections_HowMany] = {
   s_FileTipData,
   s_ZoomTipData,
   s_MapDisplayTipData,
// s_MapModeTipData,
   s_InfoTipData,
// s_HelpTipData
};

int CampToolBar_Imp::s_howMany[Sections_HowMany] = {
   File_HowMany,
   Zoom_HowMany,
   MapDisplay_HowMany,
// MapMode_HowMany,
   Info_HowMany,
// Help_HowMany
};

/*------------------------------------------------------------------------
 * Implementation
 */

  // caption names for each type
  // TODO: get strings from resource
#if 0
static const char* CampToolBar_Imp::s_capNames[Sections_HowMany] = {
    "File",
    "Zoom",
    "Map-Display",
//  "Map-Mode",
    "Info",
//  "Help"
};
#endif

const InGameText::ID CampToolBar_Imp::s_capNames[Sections_HowMany] = {
    IDS_File,
    IDS_Zoom,
    IDS_MapDisplay,
    IDS_Info
};


// registry names. Note: these can remain local
const char* CampToolBar_Imp::s_regNames[Sections_HowMany] = {
    "FileToolBar",
    "ZoomToolBar",
    "Map-DisplayToolBar",
//  "Map-ModeToolBar",
    "InfoToolBar",
//  "HelpToolBar"
};

CampToolBar_Imp::CampToolBar_Imp(HWND parent, CampaignWindowsInterface* cw) :
  d_campWind(cw)
{
  static ToolBarInitData td;
  td.d_hParent = parent;
  td.d_nSections = Sections_HowMany;
  td.d_tipData = s_td;
  td.d_buttonData = s_bd;
  td.d_howMany = s_howMany;
  td.d_fillDib = scenario->getScenarioBkDIB(Scenario::BackPatterns::CherryWoodBackground);
  td.d_buttonFillDib = scenario->getScenarioBkDIB(Scenario::BackPatterns::ButtonBackground);
  td.d_buttonImages = ScenarioImageLibrary::get(ScenarioImageLibrary::CToolBarImages);
  td.d_capDib = scenario->getScenarioBkDIB(Scenario::BackPatterns::MenuBarBackground);
  int capCY = GetSystemMetrics(SM_CYCAPTION);
  const int fontHeight = capCY - 2;
  ASSERT(fontHeight >= 3);    // check for silly size?
  LogFont lf;
  lf.face(scenario->fontName(Font_Bold));
  lf.height(fontHeight);
  lf.weight(FW_MEDIUM);
  lf.charset(Greenius_System::ANSI);      // otherwise we may get symbols!
  td.d_capFont = lf;
  td.d_borderColors = scenario->getBorderColors();
  td.d_captionNames = s_capNames;
  td.d_registryNames = s_regNames;
  init(&td);
}

/*------------------------------------------------------------
 * Client access
 */

ToolBar::ToolBar(HWND parent, CampaignWindowsInterface* cw) :
  d_toolBar(new CampToolBar_Imp(parent, cw))
{
  ASSERT(d_toolBar);
}

ToolBar::~ToolBar()
{
  // destroy();
  delete d_toolBar;
}

void ToolBar::position(const PixelPoint& p)
{
  ASSERT(d_toolBar);
  d_toolBar->position(p);
}

// void ToolBar::show()
// {
//   ASSERT(d_toolBar);
//   ShowWindow(d_toolBar->getHWND(), SW_SHOW);
// }
//
// void ToolBar::destroy()
// {
//   if(d_toolBar)
//   {
//     DestroyWindow(d_toolBar->getHWND());
//     d_toolBar = 0;
//   }
// }

void ToolBar::setCheck(int menuID, Boolean f)
{
  ASSERT(d_toolBar);
  d_toolBar->setCheck(menuID, f);
}

void ToolBar::enable(int menuID, Boolean f)
{
  ASSERT(d_toolBar);
  d_toolBar->enable(menuID, f);
}

int ToolBar::height() const
{
  return d_toolBar->height();
}


// Boolean ToolBar::shouldShow() const
// {
//   return d_toolBar->shouldShow();
// }
//
// void ToolBar::suspend(bool visible)
// {
//    d_toolBar->suspend(visible);
// }


HWND ToolBar::getHWND() const
{
    return d_toolBar->getHWND();
}

bool ToolBar::isVisible() const
{
  return d_toolBar->isVisible();
}

bool ToolBar::isEnabled() const
{
  return d_toolBar->isEnabled();
}

void ToolBar::show(bool visible)
{
  d_toolBar->show(visible);
}

void ToolBar::enable(bool enable)
{
  d_toolBar->enable(enable);
}

/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.2  2001/06/18 08:25:45  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
