/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
#include "stdinc.hpp"
#include "ccombo.hpp"
#include "scenario.hpp"
#include "scn_res.h"
#include "bmp.hpp"
#include "palette.hpp"
#include "dib.hpp"
#include "winctrl.hpp"
#include "unitlist.hpp"
#include "wmisc.hpp"
#include "town.hpp"
#include "campdint.hpp"
#include "compos.hpp"
#include "armies.hpp"
#include "realord.hpp"
#include "options.hpp"
#include "imglib.hpp"
#include "itemwind.hpp"
#include "dib_blt.hpp"
#include "armyutil.hpp"
#include "scrnbase.hpp"
#include "scn_img.hpp"
#include "fonts.hpp"
#include "resstr.hpp"
#include "control.hpp"
#include "random.hpp"

/*----------------------------------------------------------------
 * MoveHow combo
 */

class MoveHowItem : public ItemWindow {
  const CampaignData* d_campData;
  const UINT d_listCX;
  bool d_filled;
public:
   MoveHowItem(const ItemWindowData& data, const CampaignData* campData, const SIZE& s) :
      ItemWindow(data, s),
      d_campData(campData),
      d_listCX(s.cx),
      d_filled(false)
   {
   }

   ~MoveHowItem() { selfDestruct(); }

   const CampaignData* campData() const { return d_campData; }

   void init(const void* data); //ICommandPosition cpi, CampaignOrder& order);
   void drawListItem(const DRAWITEMSTRUCT* lpDrawItem);
   void drawCombo(DrawDIBDC* dib, const void* data); //ICommandPosition cpi, CampaignOrder& order);
};

void MoveHowItem::init(const void* dataPtr) //(ICommandPosition cpi, CampaignOrder& order)
{
  ASSERT(dataPtr);
  const CampaignComboData& cData = *(static_cast<const CampaignComboData*>(dataPtr));

  ASSERT(cData.d_order);
  const CampaignOrder& order = *cData.d_order;

  Orders::AdvancedOrders::MoveHow currentHow = order.getMoveHow();
  ASSERT(currentHow < Orders::AdvancedOrders::MoveHow_HowMany);

  /*
   * Some useful values
   */

  const LONG offsetX = ScreenBase::x(3);    // *dbX())/4;
  const LONG offsetY = ScreenBase::y(3);    // *dbY())/8;

  //---- being static doesn't work if game is restarted
  // static Boolean listFilled = False;
  ListBox lb(getHWND(), ItemListBox);

  if(!d_filled)
  {
    /*
     * Fill List box
     */

    lb.reset();

    /*
     * Add move how items to listbox
     */

    for(Orders::AdvancedOrders::MoveHow i = Orders::AdvancedOrders::MoveHow_First;
         i < Orders::AdvancedOrders::MoveHow_HowMany; INCREMENT(i))
    {
      lb.add(Orders::AdvancedOrders::getMoveHowName(i), i);
    }

    d_filled = True;
  }

  lb.setValue(currentHow);
  setCurrentValue();

  /*
   * Set position of list box
   */

//  RECT r;
//  GetWindowRect(getHWND(), &r);
  const LONG itemHeight = data().d_itemCY*lb.getNItems();

  SetWindowPos(lb.getHWND(), HWND_TOP, 0, 0, d_listCX, itemHeight, SWP_NOMOVE);

//  MoveWindow(lb.getHWND(), offsetX, offsetY, (r.right-r.left)-(2*offsetX), itemHeight, TRUE);

}

void MoveHowItem::drawListItem(const DRAWITEMSTRUCT* lpDrawItem)
{
  ListBox lb(lpDrawItem->hwndItem);
  Orders::AdvancedOrders::MoveHow mh = static_cast<Orders::AdvancedOrders::MoveHow>(lb.getValue(lpDrawItem->itemID));

  ASSERT(mh < Orders::AdvancedOrders::MoveHow_HowMany);
  blitText(lpDrawItem, Orders::AdvancedOrders::getMoveHowName(mh));
}

void MoveHowItem::drawCombo(DrawDIBDC* dib, const void* dataPtr) //ICommandPosition cpi, CompleteOrder& order)
{
  ASSERT(dataPtr);
  const CampaignComboData& cData = *(reinterpret_cast<const CampaignComboData*>(dataPtr));

  // put text
  TEXTMETRIC tm;
  GetTextMetrics(dib->getDC(), &tm);
  const int y = (cData.d_itemCY - tm.tmHeight) / 2;
  const int x = 2*CustomBorderWindow::ThinBorder;
  const int cx = cData.d_itemCX - (2 + x);

  ASSERT(cData.d_order);
  wTextOut(dib->getDC(), x, y, cx, Orders::AdvancedOrders::getMoveHowName(cData.d_order->getMoveHow()));
}

/*----------------------------------------------------------------
 * OnArrival combo
 */

class OnArrivalItem : public ItemWindow {
  const CampaignData* d_campData;
  const UINT d_listCX;
public:
  OnArrivalItem(const ItemWindowData& data, const CampaignData* campData, const SIZE& s) :
    ItemWindow(data, s),
    d_campData(campData),
    d_listCX(s.cx) {}
  ~OnArrivalItem() { selfDestruct(); }

  const CampaignData* campData() const { return d_campData; }

  void init(const void* data); //ICommandPosition cpi, CampaignOrder& order);
  void drawListItem(const DRAWITEMSTRUCT* lpDrawItem);
  void drawCombo(DrawDIBDC* dib, const void* data); //ICommandPosition cpi, CompleteOrder& order);

private:
//  Boolean canGiveOnArrival(ICommandPosition cpi, const CampaignOrder& order, Orders::Type::Value o);
};


void OnArrivalItem::init(const void* dataPtr) //ICommandPosition cpi, CampaignOrder& order)
{
  ASSERT(dataPtr);
  const CampaignComboData& cData = *(reinterpret_cast<const CampaignComboData*>(dataPtr));

  ASSERT(cData.d_order);
  ASSERT(cData.d_currentUnit != NoCommandPosition);

  CampaignOrder& order = *cData.d_order;
  ConstICommandPosition cpi = cData.d_currentUnit;

  Orders::Type::Value coa = order.getOrderOnArrival();
  ASSERT(coa < Orders::Type::HowMany);

  /*
   * Some useful values
   */

  const LONG offsetX = ScreenBase::x(3);    // *dbX())/4;
  const LONG offsetY = ScreenBase::y(3);    // *dbY())/8;

    /*
     * Fill List box
     */

  ListBox lb(getHWND(), ItemListBox);
  lb.reset();

  /*
   * Add move how items to listbox
   */

  Boolean has = False;
  for(Orders::Type::Value t = Orders::Type::First;
      t < Orders::Type::HowMany; INCREMENT(t))
  {
    if(CampaignOrderUtil::canGiveOnArrival(d_campData, cpi, order, t))
    {
      if(coa == t)
        has = True;

      lb.add(Orders::AdvancedOrders::getOrderOnArrivalName(t), t);
    }
  }

  if(!has)
    order.setOrderOnArrival(Orders::Type::Hold);

  ASSERT(lb.getNItems() > 0);
  lb.setValue(order.getOrderOnArrival());
  setCurrentValue();

  /*
   * Set position of list box
   */

//  RECT r;
//  GetWindowRect(getHWND(), &r);
  const LONG itemHeight = data().d_itemCY*lb.getNItems();
  SetWindowPos(lb.getHWND(), HWND_TOP, 0, 0, d_listCX, itemHeight, SWP_NOMOVE);

//  MoveWindow(lb.getHWND(), offsetX, offsetY, (r.right-r.left)-(2*offsetX), itemHeight, TRUE);

}

void OnArrivalItem::drawListItem(const DRAWITEMSTRUCT* lpDrawItem)
{
  ListBox lb(lpDrawItem->hwndItem);
  Orders::Type::Value oa = static_cast<Orders::Type::Value>(lb.getValue(lpDrawItem->itemID));

  ASSERT(oa < Orders::Type::HowMany);
  blitText(lpDrawItem, Orders::AdvancedOrders::getOrderOnArrivalName(oa));

}

void OnArrivalItem::drawCombo(DrawDIBDC* dib, const void* dataPtr) //ICommandPosition cpi, CompleteOrder& order)
{
  ASSERT(dataPtr);
  const CampaignComboData& cData = *(reinterpret_cast<const CampaignComboData*>(dataPtr));

  ASSERT(cData.d_order);

  // put text
  TEXTMETRIC tm;
  GetTextMetrics(dib->getDC(), &tm);
  const int y = (cData.d_itemCY - tm.tmHeight) / 2;
  const int x = 2*CustomBorderWindow::ThinBorder;
  const int cx = cData.d_itemCX - (2 + x);

  wTextOut(dib->getDC(), x, y, cx, Orders::AdvancedOrders::getOrderOnArrivalName(cData.d_order->getOrderOnArrival()));
}


/*----------------------------------------------------------------
 * Order Type combo
 */

class OrderTypeItem : public ItemWindow {
  const CampaignData* d_campData;
  const UINT d_listCX;
public:
  OrderTypeItem(const ItemWindowData& data, const CampaignData* campData, const SIZE& s) :
    ItemWindow(data, s),
    d_campData(campData),
    d_listCX(s.cx) {}
  ~OrderTypeItem() { selfDestruct(); }

  const CampaignData* campData() const { return d_campData; }

  void init(const void* data); //ICommandPosition cpi, CampaignOrder& order);
  void drawListItem(const DRAWITEMSTRUCT* lpDrawItem);
  void drawCombo(DrawDIBDC* dib, const void* data); //ICommandPosition cpi, CampaignOrder& order);
};


void OrderTypeItem::init(const void* dataPtr) //ICommandPosition cpi, CampaignOrder& order)
{
  ASSERT(dataPtr);
  const CampaignComboData& cData = *(reinterpret_cast<const CampaignComboData*>(dataPtr));

  ASSERT(cData.d_order);
  ASSERT(cData.d_currentUnit != NoCommandPosition);

  CampaignOrder& order = *cData.d_order;
  ConstICommandPosition cpi = cData.d_currentUnit;

  /*
   * Some useful values
   */

  const LONG offsetX = ScreenBase::x(3);    // *dbX())/4;
  const LONG offsetY = ScreenBase::y(3);    // *dbY())/8;

    /*
     * Fill List box
     */

  ListBox lb(getHWND(), ItemListBox);
  lb.reset();

  const Boolean hasMoveOrder = (order.getType() == Orders::Type::MoveTo);

  for(Orders::Type::Value t = Orders::Type::First; t < Orders::Type::HowMany; INCREMENT(t))
  {
    if(Orders::Type::canGiveToUnit(t))
    {
      Boolean add = False;

      if(hasMoveOrder)
      {
        add = CampaignOrderUtil::allowed(t, cpi, campData());
      }
      else
      {
        if(t != Orders::Type::MoveTo)
        {
          add = CampaignOrderUtil::allowed(t, cpi, campData());
        }
      }

      if(add)
        lb.add(Orders::Type::text(t), t);
    }
  }

  int nItems = lb.getNItems();
  ASSERT(nItems > 0);

  /*
   * See if current type is allowed, if not set to first item in combo
   */

  // if(!Orders::Type::allowed(owner()->order().getType(), owner()->currentUnit(), campData()))
  if(!CampaignOrderUtil::allowed(order.getType(), cpi, campData()))
    order.setType(static_cast<Orders::Type::Value>(lb.getValue(0)));

  lb.setValue(order.getType());
  setCurrentValue();

  /*
   * Set position of list box
   */

//  RECT r;
//  GetWindowRect(getHWND(), &r);
  const LONG itemHeight = data().d_itemCY*lb.getNItems();
  SetWindowPos(lb.getHWND(), HWND_TOP, 0, 0, d_listCX, itemHeight, SWP_NOMOVE);

//  MoveWindow(lb.getHWND(), offsetX, offsetY, (r.right-r.left)-(2*offsetX), itemHeight, TRUE);

}

void OrderTypeItem::drawListItem(const DRAWITEMSTRUCT* lpDrawItem)
{
  ListBox lb(lpDrawItem->hwndItem);
  Orders::Type::Value t = static_cast<Orders::Type::Value>(lb.getValue(lpDrawItem->itemID));

  ASSERT(t < Orders::Type::HowMany);
  blitText(lpDrawItem, Orders::Type::text(t));
}

void OrderTypeItem::drawCombo(DrawDIBDC* dib, const void* dataPtr) //ICommandPosition cpi, CompleteOrder& order)
{
  ASSERT(dataPtr);
  const CampaignComboData& data = *(reinterpret_cast<const CampaignComboData*>(dataPtr));

  ASSERT(data.d_order);

  // put text
  TEXTMETRIC tm;
  GetTextMetrics(dib->getDC(), &tm);
  const int y = (data.d_itemCY - tm.tmHeight) / 2;
  const int x = 2*CustomBorderWindow::ThinBorder;
  const int cx = data.d_itemCX - (2 + x);

  wTextOut(dib->getDC(), x, y, cx, Orders::Type::text(data.d_order->getType()));
}

/*----------------------------------------------------------------
 * Aggression combo
 */

class AggressionTypeItem : public ItemWindow {
  const CampaignData* d_campData;
  const UINT d_listCX;
  bool d_filled;
public:
  AggressionTypeItem(const ItemWindowData& data, const CampaignData* campData, const SIZE& s) :
    ItemWindow(data, s),
    d_campData(campData),
    d_listCX(s.cx),
    d_filled(false)
    {}

  ~AggressionTypeItem() { selfDestruct(); }

  const CampaignData* campData() const { return d_campData; }

  void init(const void* data); //ICommandPosition cpi, CampaignOrder& order);
  void drawListItem(const DRAWITEMSTRUCT* lpDrawItem);
  void drawCombo(DrawDIBDC* dib, const void* data); //ICommandPosition cpi, CampaignOrder& order);
};


void AggressionTypeItem::init(const void* dataPtr) //ICommandPosition cpi, CampaignOrder& order)
{
  ASSERT(dataPtr);
  const CampaignComboData& cData = *(reinterpret_cast<const CampaignComboData*>(dataPtr));

  ASSERT(cData.d_order);
  const CampaignOrder& order = *cData.d_order;

  Orders::Aggression::Value ag = order.getAggressLevel();
  ASSERT(ag < Orders::Aggression::HowMany);

    /*
     * Fill List box
     */

  ListBox lb(getHWND(), ItemListBox);

  // static Boolean filled = False;

  if(!d_filled)
  {
    lb.reset();

    for(Orders::Aggression::Value v = Orders::Aggression::First;
        v < Orders::Aggression::HowMany; INCREMENT(v))
    {
      lb.add(Orders::Aggression::getName(v), v);
    }
    d_filled = true;
  }

  lb.setValue(ag);
  setCurrentValue();

  /*
   * Set position of list box
   */

  const LONG itemHeight = data().d_itemCY*lb.getNItems();

  SetWindowPos(lb.getHWND(), HWND_TOP, 0, 0, d_listCX, itemHeight, SWP_NOMOVE);
//  MoveWindow(lb.getHWND(), offsetX, offsetY, (r.right-r.left)-(2*offsetX), itemHeight, TRUE);
}

void AggressionTypeItem::drawListItem(const DRAWITEMSTRUCT* lpDrawItem)
{
  ListBox lb(lpDrawItem->hwndItem);
  Orders::Aggression::Value ag = static_cast<Orders::Aggression::Value>(lb.getValue(lpDrawItem->itemID));

  ASSERT(ag < Orders::Aggression::HowMany);
  blitText(lpDrawItem, Orders::Aggression::getName(ag));
}

void AggressionTypeItem::drawCombo(DrawDIBDC* dib, const void* dataPtr) //ICommandPosition cpi, CompleteOrder& order)
{
  ASSERT(dataPtr);
  const CampaignComboData& data = *(reinterpret_cast<const CampaignComboData*>(dataPtr));

  ASSERT(data.d_order);

  // put text
  TEXTMETRIC tm;
  GetTextMetrics(dib->getDC(), &tm);
  const int y = (data.d_itemCY - tm.tmHeight) / 2;
  const int x = 2*CustomBorderWindow::ThinBorder;
  const int cx = data.d_itemCX - (2 + x);

  wTextOut(dib->getDC(), x, y, cx, Orders::Aggression::getName(data.d_order->getAggressLevel()));
}

/*------------------------------------------------------------------------
 * Unit Combo
 */

class UnitItem : public ItemWindow
{
  const CampaignData* d_campData;
  const UINT d_listCX;
public:
  UnitItem(const ItemWindowData& data, const CampaignData* campData, const SIZE& s) :
    ItemWindow(data, s),
    d_campData(campData),
    d_listCX(s.cx) { }
  ~UnitItem() { selfDestruct(); }

  const CampaignData* campData() const { return d_campData; }

  void init(const void* data); //ICommandPosition cpi, CampaignOrder& order);
  void drawListItem(const DRAWITEMSTRUCT* lpDrawItem);
  void drawCombo(DrawDIBDC* dib, const void* data); //ICommandPosition cpi, CampaignOrder& order);
};


void UnitItem::init(const void* dataPtr) //ICommandPosition cpi, CampaignOrder& order)
{
  ASSERT(dataPtr);
  const CampaignComboData& cData = *(reinterpret_cast<const CampaignComboData*>(dataPtr));

  ASSERT(cData.d_currentUnit != NoCommandPosition);
  ASSERT(cData.d_unitList != 0);

  const StackedUnitList& list = *cData.d_unitList;

  /*
   * Some useful values
   */

//  const LONG offsetX = (4*dbX())/4;
//  const LONG offsetY = offsetX;

    /*
     * Fill List box
     */

  ListBox lb(getHWND(), ItemListBox);
  lb.reset();

  char buf[200];
  for(int i = 0; i < list.unitCount(); i++)
  {
    const CommandPosition* cp = list.getUnit(i);

    /*
     * Get unit strength in manpower
     * This should be a function in Armies class
     */

//    if(GamePlayerControl::getControl(cp->getSide()) == GamePlayerControl::Player)
    if(CampaignArmy_Util::isUnitOrderable(cp))
    {

       Boolean limitedInfo = (CampaignOptions::get(OPT_FogOfWar) && !CampaignArmy_Util::isUnitOrderable(list.getUnit(i)));

       if( (!limitedInfo || cp->getInfoQuality() >= CommandPosition::LimitedInfo) )
       {
         ConstUnitIter iter(&campData()->getArmies(), list.getUnit(i));
         while(iter.next())
         {
           ConstICommandPosition cp = iter.current();
           ConstILeader leader = cp->getLeader();

           const char* leaderName;
           if(leader == NoLeader)
               leaderName = InGameText::get(IDS_UI_LEADERUNKNOWN);
            else
               leaderName = leader->getNameNotNull();

           wsprintf(buf, "%s (%s)", cp->getNameNotNull(), leaderName);

           //--- Bugfix: SWG: 12Jul99... was adding child units instead of top level unit.
           //--- Bugfix: SWG: 28Jul99... it should have added the child units!
           // lb.add(buf, cpToLPARAM(list.getUnit(i)));
           lb.add(buf, cpToLPARAM(cp));
         }
       }
       else
       {
         ConstICommandPosition cp = list.getUnit(i);
         ConstILeader leader = cp->getLeader();

           const char* leaderName;
           if(leader == NoLeader)
               leaderName = InGameText::get(IDS_UI_LEADERUNKNOWN);
           else
               leaderName = leader->getNameNotNull();

         if(limitedInfo && (cp->getInfoQuality() == CommandPosition::NoInfo))
           lstrcpy(buf, InGameText::get(IDS_UI_UNIDUNIT));
         else
           wsprintf(buf, "%s (%s)", cp->getNameNotNull(), leaderName);

         lb.add(buf, cpToLPARAM(cp));
         // lb.add(buf, cpToLPARAM(list.getUnit(i)));
       }
    }
  }

  lb.setValue(cpToLPARAM(cData.d_currentUnit));
  setCurrentValue();

  /*
   * Set position of list box
   */

//  RECT r;
//  GetWindowRect(getHWND(), &r);

  int itemsShowing = minimum(data().d_maxItemsShowing, lb.getNItems());
  const LONG itemHeight = data().d_itemCY*itemsShowing;

  SetWindowPos(lb.getHWND(), HWND_TOP, 0, 0, d_listCX, itemHeight, SWP_NOMOVE);

//  MoveWindow(lb.getHWND(), offsetX, offsetY, ((r.right-r.left)-(2*offsetX))-data().d_scrollCX, itemHeight, TRUE);

  if(lb.getNItems() > data().d_maxItemsShowing)
    showScroll();
}

static int imageRankOffset[Rank_HowMany] = {
               FI_ArmyGroup,     // God
               FI_ArmyGroup,     // President
               FI_ArmyGroup,     // Group
               FI_Army,          // Army
               FI_Corps,         // Corps
               FI_Division    // Division
};

void UnitItem::drawListItem(const DRAWITEMSTRUCT* lpDrawItem)
{
  ListBox lb(lpDrawItem->hwndItem);

  const int itemCX = lpDrawItem->rcItem.right-lpDrawItem->rcItem.left;
  const int itemCY = lpDrawItem->rcItem.bottom-lpDrawItem->rcItem.top;

  /*
   * Allocate dib if needed
   */

  allocateItemDib(itemCX, itemCY);

  ASSERT(itemDib());
  ASSERT(fillDib());

  itemDib()->rect(0, 0, itemCX, itemCY, fillDib());

  if( !(lpDrawItem->itemState & ODS_SELECTED) )
    itemDib()->darkenRectangle(0, 0, itemCX, itemCY, 20);

  if(lb.getNItems() > 0)
  {
    /*
     * Get screen base and other useful constants
     */

    // const int dbX = ScreenBase::dbX();
    // const int dbY = ScreenBase::dbY();
    // const int baseX = ScreenBase::baseX();
    // const int baseY = ScreenBase::baseY();
    TEXTMETRIC tm;
    GetTextMetrics(itemDib()->getDC(), &tm);
    const int xMargin = ScreenBase::x(2);  // * dbX) / baseX;
    const int xOffset = ScreenBase::x(8);  // * dbX) / baseX;

    char text[200];
    SendMessage(lpDrawItem->hwndItem, LB_GETTEXT, lpDrawItem->itemID, (LPARAM)(LPCSTR)text);

    ICommandPosition cpi = lparamToCP(lb.getValue(lpDrawItem->itemID));

    // Lock the OB for reading because all sorts of horrid things
    // could happen if a unit gets transferred or dies

    ArmyReadLocker lock(d_campData->getArmies());

    Boolean limitedInfo = (CampaignOptions::get(OPT_FogOfWar) && !CampaignArmy_Util::isUnitOrderable(cpi));

    const ImageLibrary* flags = scenario->getNationFlag(cpi->getNation(), (limitedInfo && cpi->getInfoQuality() < CommandPosition::VeryLimitedInfo) ? False : True);

    /*
     * Draw lines
     *
     *
     * Set x offset
     */

    int flagX = xMargin;
    int nOffsets = 0;

    // first, count the nunber of offsets
    ICommandPosition parentCPI = cpi->getParent();
    while((parentCPI != NoCommandPosition) && parentCPI->isLower(Rank_President))
    {
      flagX += xOffset;
      nOffsets++;
      parentCPI = parentCPI->getParent();
    }

    // more useful constants
    const int xLineMargin = xMargin + (FI_Army_CX / 2);
    const int vw = ScreenBase::x(5);   // * dbX) / baseX;
    int vh = itemCY;
    const ColourIndex ci = itemDib()->getColour(PALETTERGB(0, 0, 0));

    // now draw appropriate lines
    parentCPI = cpi->getParent();
    ICommandPosition thisCPI = parentCPI;
    while(nOffsets--)
    {
      // Shouldn't get in here if it is NoCommandPosition because nOffsets
      // will have been set appropriately in the loop above

      ASSERT(parentCPI != NoCommandPosition);

      const int x = xLineMargin + (xOffset * nOffsets);
      const int y = 0;

      // if current objects parent
      if(parentCPI == cpi->getParent())
      {
        if(cpi->getSister() == NoCommandPosition)
        {
          vh = itemCY / 2;
        }

        itemDib()->hLine(x, itemCY / 2, vw, ci);
        itemDib()->vLine(x, y, vh, ci);
      }
      else
      {
        ASSERT(thisCPI != NoCommandPosition);

        ICommandPosition sisCPI = thisCPI->getSister();
        if(sisCPI != NoCommandPosition)
        {
          itemDib()->vLine(x, y, itemCY, ci);
        }
      }

      thisCPI = parentCPI;
      parentCPI = parentCPI->getParent();
    }

    int w;
    int h;
    flags->getImageSize(imageRankOffset[cpi->getRankEnum()], w, h);

    int y = (itemCY - h) / 2;
    flags->blit(itemDib(), imageRankOffset[cpi->getRankEnum()], flagX, y, True);

    y = (itemCY - tm.tmHeight) / 2;

    int x = flagX + FI_Army_CX + xMargin;
    int cx = itemCX - (2 + x);
    wTextOut(itemDib()->getDC(), x, y, cx, text);

    BitBlt(lpDrawItem->hDC, lpDrawItem->rcItem.left, lpDrawItem->rcItem.top, itemCX, itemCY,
         itemDib()->getDC(), 0, 0, SRCCOPY);

  }

}

void UnitItem::drawCombo(DrawDIBDC* dib, const void* dataPtr) //ICommandPosition cpi, CompleteOrder& order)
{
  ASSERT(dataPtr);
  const CampaignComboData& data = *(reinterpret_cast<const CampaignComboData*>(dataPtr));

  ArmyReadLocker lock(d_campData->getArmies());

  ASSERT(data.d_currentUnit != NoCommandPosition);
  ConstICommandPosition cp = data.d_currentUnit;

//  const CommandPosition* cp = cpi;
//  const Leader* leader = cp->getLeader();
   ILeader leader = cp->getLeader();

  Boolean limitedInfo = (CampaignOptions::get(OPT_FogOfWar) && !CampaignArmy_Util::isUnitOrderable(cp));

  const char* leaderName;
  if(leader == NoLeader)
   leaderName = InGameText::get(IDS_UI_LEADERUNKNOWN);
  else
   leaderName = leader->getNameNotNull();

  char text[200];
  if (limitedInfo && cp->getInfoQuality() == CommandPosition::NoInfo)
  {
    wsprintf(text, "%s %s",
        InGameText::get(IDS_UI_UNIDUNIT),
        InGameText::get(IDS_UI_STRENGTHUNKNOWN)
    );
  }
  else if(data.d_showManpower)
  {
    LONG strength = CampaignArmy_Util::totalManpower(campData(), cp);

    wsprintf(text, "%s (%s)",
      cp->getNameNotNull(),
      leaderName);

    char* textPtr = text + strlen(text);

    if(limitedInfo && cp->getInfoQuality() == CommandPosition::VeryLimitedInfo)
    {
      // randomize strength value +- 20%
      int percent = MulDiv(strength, 20, 100);
      strength = random(strength-percent, strength+percent);
      wsprintf(textPtr, "  %s %ld ???",
        InGameText::get(IDS_UI_STR),
        strength);
    }
    else
    {
      wsprintf(textPtr, "  %s %ld",
        InGameText::get(IDS_UI_STR),
        strength);
    }
  }

  const ImageLibrary* flags = scenario->getNationFlag(cp->getNation(), (limitedInfo && cp->getInfoQuality() < CommandPosition::VeryLimitedInfo) ? False : True);

  const int xOffset = 4;

  int w;
  int h;
  flags->getImageSize(imageRankOffset[cp->getRankEnum()], w, h);

  int y = (data.d_itemCY - h) / 2;
  flags->blit(dib, imageRankOffset[cp->getRankEnum()], xOffset, y, True);

  TEXTMETRIC tm;
  GetTextMetrics(dib->getDC(), &tm);
  y = (data.d_itemCY - tm.tmHeight) / 2;
  int x = xOffset+FI_Army_CX+3;
  int cx = data.d_itemCX - (2 + x);
  wTextOut(dib->getDC(), x, y, cx, text);
}


CampaignComboInterface::CampaignComboInterface() :
  d_items(0)
{
  d_items = new ItemWindow*[HowMany];
  ASSERT(d_items);

  for(int i = 0; i < HowMany; i++)
    d_items[i] = 0;
}

CampaignComboInterface::~CampaignComboInterface()
{
    for(int i = 0; i < HowMany; i++)
    {
      if(d_items[i])
      {
         delete d_items[i];
         d_items[i] = 0;
      }
    }

   delete[] d_items;
    d_items = 0;
}

void CampaignComboInterface::create(HWND hParent, CampaignComboInterface::Type type, UWORD id, const CampaignData* campData, const SIZE& s, UBYTE flags)
{
  ItemWindow* iw = 0;
  ASSERT(type < HowMany);

  const LONG dbUnits = GetDialogBaseUnits();

  ItemWindowData ld;
  ld.d_hParent = hParent;
  ld.d_id = id;
  // ld.d_dbX = LOWORD(dbUnits);
  // ld.d_dbY = HIWORD(dbUnits);
  ld.d_scrollCX = (flags & ItemWindowData::HasScroll) ? 10 : 0;
  ld.d_itemCY = ScreenBase::y(10);  // static_cast<UWORD>((10*ld.d_dbY)/8);
  ld.d_maxItemsShowing = 10;
  ld.d_flags = flags;
  ld.d_windowFillDib = scenario->getSideBkDIB(SIDE_Neutral);
  ld.d_fillDib = scenario->getScenarioBkDIB(Scenario::BackPatterns::PaperBackground);
  ld.d_bColors = scenario->getBorderColors();
  LogFont lf;
  lf.height(ScreenBase::y(8));  // * ScreenBase::dbY()) / ScreenBase::baseY());
  lf.weight(FW_MEDIUM);
  lf.face(scenario->fontName(Font_Bold));

  Font font;
  font.set(lf);
  ld.d_hFont = font;

  if(flags & ItemWindowData::HasScroll)
  {
    ld.d_scrollBtns = ScenarioImageLibrary::get(ScenarioImageLibrary::VScrollButtons);
    ld.d_scrollThumbDib = scenario->getScenarioBkDIB(Scenario::BackPatterns::VScrollThumbBackground);
    ld.d_scrollFillDib = scenario->getScenarioBkDIB(Scenario::BackPatterns::VScrollBackground);
  }

  switch(type)
  {
    case MoveHow:
      iw = new MoveHowItem(ld, campData, s);
      break;

    case OnArrival:
      iw = new OnArrivalItem(ld, campData, s);
      break;

    case OrderType:
      iw = new OrderTypeItem(ld, campData, s);
      break;

    case Aggression:
      iw = new AggressionTypeItem(ld, campData, s);
      break;

    case UnitList:
      iw = new UnitItem(ld, campData, s);
      break;

#ifdef DEBUG
    default:
      FORCEASSERT("Combo type not found");
      break;
#endif
  }

  ASSERT(iw);
  ASSERT(d_items[type] == 0);

  d_items[type] = iw;
}

void CampaignComboInterface::init(CampaignComboInterface::Type type, const CampaignComboData& data) //ICommandPosition cpi, CampaignOrder& order)
{
  ASSERT(type < HowMany);
  ASSERT(d_items[type]);

  if(d_items[type])
  {
    d_items[type]->init(&data);
  }
}

void CampaignComboInterface::hide(CampaignComboInterface::Type type)
{
  ASSERT(type < HowMany);
  ASSERT(d_items[type]);

  if(d_items[type])
  {
    d_items[type]->hide();
  }
}

void CampaignComboInterface::show(CampaignComboInterface::Type type, PixelPoint& p)
{
  ASSERT(type < HowMany);
  ASSERT(d_items[type]);

  if(d_items[type])
  {
    d_items[type]->show(p);
  }
}

Boolean CampaignComboInterface::showing(CampaignComboInterface::Type type)
{
  ASSERT(type < HowMany);
  ASSERT(d_items[type]);

  if(d_items[type])
  {
    return d_items[type]->showing();
  }

  return False;
}

ULONG CampaignComboInterface::getValue(CampaignComboInterface::Type type)
{
  ASSERT(type < HowMany);
  ASSERT(d_items[type]);

  if(d_items[type])
  {
    return d_items[type]->currentValue();
  }

  return 0;
}

Boolean CampaignComboInterface::created(CampaignComboInterface::Type type)
{
  return (d_items[type] != 0);
}

void CampaignComboInterface::destroy()
{
  for(int i = 0; i < HowMany; i++)
  {
    if(d_items[i])
    {
      delete d_items[i];
      d_items[i] = 0;
    }
  }
}

class ComboButton {
    DrawDIB* d_dib;
  public:
    ComboButton() :
      d_dib(0) {}

    ~ComboButton()
    {
      if(d_dib)
        delete d_dib;
    }

    void init()
    {
      d_dib = BMP::newDrawDIB(scenario->getScenarioResDLL(), MAKEINTRESOURCE(BM_COMBOBUTTON), BMP::RBMP_Normal);
      ASSERT(d_dib);
    }

    Boolean isInitiated() const { return (d_dib != 0); }
    DrawDIB* buttonDib() const { return d_dib; }
};

static ComboButton s_comboButton;

void CampaignComboInterface::drawCombo(CampaignComboInterface::Type type, DrawDIBDC* dib, const DrawDIB* fillDib, CampaignComboData& data)
{
  ASSERT(dib);
  ASSERT(fillDib);
  ASSERT(type < HowMany);
  ASSERT(d_items[type]);

  if(d_items[type])
  {
    CustomBorderWindow bw(scenario->getBorderColors());
    dib->rect(0, 0, data.d_itemCX, data.d_itemCY, fillDib);

    RECT r;
    SetRect(&r, 0, 0, data.d_itemCX, data.d_itemCY);

    // draw border
    bw.drawThinBorder(dib->getDC(), r);

    // put button
    if(!s_comboButton.isInitiated())
      s_comboButton.init();

    const int buttonX = (r.right-(s_comboButton.buttonDib()->getWidth()+CustomBorderWindow::ThinBorder))+1;

    DIB_Utility::stretchDIB(dib, buttonX, CustomBorderWindow::ThinBorder,
               s_comboButton.buttonDib()->getWidth(),
               r.bottom-(CustomBorderWindow::ThinBorder*2),
               s_comboButton.buttonDib(), 0, 0,
               s_comboButton.buttonDib()->getWidth(), s_comboButton.buttonDib()->getHeight());

    data.d_itemCX = buttonX;
    d_items[type]->drawCombo(dib, &data);
  }
}



/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.2  2001/06/18 08:25:44  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
