/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef CTIMCTRL_HPP
#define CTIMCTRL_HPP

#ifndef __cplusplus
#error ctimctrl.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 * Campaign Time Control
 *
 * Factored out from CampaignImp to avoid having so many virtual
 * function calls.
 *
 *----------------------------------------------------------------------
 */

#include "cwdll.h"
#include "mytypes.h"
#include "camptime.hpp"

class CampaignTimeData;

class CampaignTimeControl {
      CampaignTimeControl(const CampaignTimeControl&);
      CampaignTimeControl& operator = (const CampaignTimeControl&);
    public:
      CAMPWIND_DLL CampaignTimeControl();
      CAMPWIND_DLL ~CampaignTimeControl();

      CAMPWIND_DLL void freezeTime(Boolean state);
         // Pause or Unpause the game

      Boolean isTimeFrozen() const { return d_frozen; }
         // Return whetehr game is frozen or not

      int setTimeRate(int n);
         // Set rate using lookup table

      int getTimeRate() const { return d_rate; }

      CAMPWIND_DLL int getTimeRange() const;

      int getSecondsPerDay() const { return d_secondsPerDay; }
      int getSecondsPerDay(int rate) const;

   // CAMPWIND_DLL GameTick addTicks(CampaignTimeData& campTime, SysTick::Value ticks);
      CAMPWIND_DLL CampaignTime addTicks(const CampaignTime& lastWantTime, const CampaignTime& realTime, SysTick::Value ticks);
   // CAMPWIND_DLL CampaignTime addTicks(const CampaignTime& campTime, SysTick::Value ticks);
         // Add system ticks to cTime, return new time in GameTicks

      // void setTimeJump(int days);
         // Advance some days

      void setTimeJump(GameDay day) { d_timeJump = day; }
         // Advance to given time

      bool isTimeJumping() const { return (d_timeJump != 0); }

   private:
      void initSettings();

      int               d_secondsPerDay;     // campaign time rate (0=paused)
      int               d_rate;              // Rate as index into rateTable
      Boolean           d_frozen;
      GameDay           d_timeJump;          // Day to jump to if bigger than today

      static bool s_initialised;
      static int s_range;
      static int* s_rateTable;
      static int s_defaultRate;

};

#endif /* CTIMCTRL_HPP */

