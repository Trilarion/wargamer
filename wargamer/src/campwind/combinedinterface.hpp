/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef H_CombinedInterface_HPP
#define H_CombinedInterface_HPP
/*
 * Combined Campaign User Interface
 *
 * A combination of the old dual mode system
 */

#include "userint.hpp"
#include "routelst.hpp"

class UnitInfo_Int;
class UnitOrder_Int;
class TrackingWindow;
class UnitMenu_Int;
class ReorgOB_Int;
class TownOrder_Int;
class TownInfo_Int;


class CombinedCInterface : public CampaignUserInterface
{
      // Disable copying
      CombinedCInterface(const CombinedCInterface&);
      CombinedCInterface& operator=(const CombinedCInterface&);
   public:
      CombinedCInterface(CampaignWindowsInterface* cwi, const CampaignData* campData, MapWindow* mapWindow);

      /*------------------------------------------------------
       * Implementation of CampaignUserInterface
       */


       ~CombinedCInterface();

       /*
        * virtual functions
        */

   	 TrackMode getTrack() { return d_trackMode; }		// Return how to do tracking

       /*
        * Mouse Tracking functions
        * These shouldn't all be pure virtual since only unitint needs most of these
        */

       void onLClick(MapSelect& info);
       void onRClick(MapSelect& info);
       void onButtonDown(MapSelect& info);
       bool onStartDrag(MapSelect& info);
       void onDrag(MapSelect& info);
       void onEndDrag(MapSelect& info);

       void overObject(MapSelect& info);
       void notOverObject();

       /*
        * Map Drawing
        */

       void onDraw(DrawDIBDC* dib, MapSelect& info);

       void update();					// Info may have changed

       // void orderTown(ITown itown);
       // void orderUnit(ConstParamCP cp);
       void updateZoom();        // update map arrows when zooming


       /*
        * Sending Orders
        */

      //	 void setOrderDate(Date& date);
       void sendOrder(ConstParamCP cp);
       void cancelOrder();

       /*
        * Misc.
        */

       CampaignWindowsInterface* campWindows() const { return d_campWind; }
//       const void* getData() const;   // derived-class defined data

       /*
        * Functions Dialogs use
        */

//       void toggle();
//       void initUnits(Boolean all);
//       void syncCombos(int i);

//         void runOrders(const PixelPoint& p);
//       void runInfo(const PixelPoint& p);
//       void runOB(const PixelPoint& p);
//       void runMenu(const PixelPoint& p);
         void orderUnit(ConstParamCP cpi);
         void orderTown(ITown itown);
         void infoUnit(ConstParamCP cpi);
         void infoTown(ITown itown);
         void reorgOB(ConstParamCP cpi);

       /*
        * Clean up
        */

//       void destroy();



   private:
//      void initOrderWindow();
      void calcMoveOrderDialPosition(const Location& endLocation, PixelPoint& endP);
      void setUnit(ConstParamCP cp);

      void clearTrackingFlags()
      {
         d_trackingUnit = false;
         d_trackingTown = false;
      }


      enum Mode
      {
         Mode_Waiting,
         Mode_SettingUpMove,
         Mode_SettingUpOrder,
         Mode_SettingUpBuild
      };

      // Info from other parts of game

      CampaignWindowsInterface* d_campWind;
      const CampaignData* d_campData;
      MapWindow* d_mapWindow;

      // MapWindow Tracking Mode

      TrackMode d_trackMode;         // are we tracking units or towns?
      Mode d_mode;                   // What interface is doing
      Location d_startMouseLocation; // mouse location at the start of a drag

      // Dialogs and windows for Units

      UnitInfo_Int* d_unitInfoWindow;          // detailed
      UnitOrder_Int* d_unitOrderWindow;        // orders
      TrackingWindow* d_trackingWindow;    // pop-up info
      UnitMenu_Int* d_menu;                // orders menu
      ReorgOB_Int* d_obWindow;

      // Dialogs for Towns

      // TrackingWindow* d_trackingDial;
      TownOrder_Int* d_townOrderWind;
      TownInfo_Int* d_townInfoWind;

      // Info related to Units

      ITown d_showTown;             // Town in Info Window
      // ITown d_orderedTown;          // Town being ordered
      // ITown d_townUnderCursor;      // town under cursor

      ConstICommandPosition d_showUnit;
      // ConstICommandPosition d_unitUnderCursor;
      // ConstICommandPosition d_orderedUnit;      // same as d_stackedUnits(d_unitID)

      StackedUnitList d_stackedUnits;
      UnitListID d_unitID;             // ID of stacked unit (NoUnitListID for town)

      CompleteOrder d_orderValues;
      RouteList d_routeList;

      bool d_trackingUnit;
      bool d_trackingTown;
      bool d_canOrderUnit;
};


#endif // H_CombinedInterface_HPP
