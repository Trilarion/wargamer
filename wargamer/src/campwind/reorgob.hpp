/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
#ifndef REORGOB_HPP
#define REORGOB_HPP

class ReorgOBWin;
class CampaignUserInterface;
class CampaignWindowsInterface;
class CampaignData;
class StackedUnitList;
class PixelPoint;

class ReorgOB_Int {
    ReorgOBWin* d_obWind;

  public:
    ReorgOB_Int(CampaignUserInterface* owner, HWND hParent,
        CampaignWindowsInterface* campWind, const CampaignData* campData);
    ~ReorgOB_Int();

    void init(const StackedUnitList& units);
    void run(const PixelPoint& p);
    void destroy();
};


#endif

/*
 *----------------------------------------------------------------------
 * $Log$
 *----------------------------------------------------------------------
 */
