/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef ORG_ORDR_HPP
#define ORG_ORDR_HPP

#ifndef __cplusplus
#error org_ordr.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *	Organisation Dialog: Sending of orders
 *
 *----------------------------------------------------------------------
 */

#include "gamedefs.hpp"
#include "cpdef.hpp"

class OrgList;

namespace OrgOrders
{
	void sendOrders(const OrgList& orgList, ConstParamCP seniorCP);
};

#endif /* ORG_ORDR_HPP */

