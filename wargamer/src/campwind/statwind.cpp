/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1995, Steven Morle-Green, All Rights Reserved
 * Started by Paul, 22nd July 1996
 *----------------------------------------------------------------------
 *
 * Status Window (at bottom of main window)
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/06/13 08:52:38  greenius
 * Initial Import
 *
 *
 * Steven 23rd July 1996:
 *  Attempt to stop program locking up during MsgWinSection::update()
 *   +-----------------------------------------------------------
 *   | SendMessage replaced with PostMessage to prevent lock up when
 *   | update functions invoked from another thread.
 *   |
 *   | Set up critical section... removed it again!
 *   |
 *   | Used Button class to alter MsgWinSection Image
 *   |
 *   | Changed message to be a CString (see misc.hpp) so that it
 *   | is auto-deleted.
 *   +-----------------------------------------------------------
 *----------------------------------------------------------------------
 */


#include "stdinc.hpp"
#include "statwind.hpp"
#include "app.hpp"
#include "imglib.hpp"
#include "wmisc.hpp"
//#include "msgwin.hpp"
#include "cwin_int.hpp"
#include "winctrl.hpp"
#include "scn_res.h"
#include "scenario.hpp"
#include "palette.hpp"
#include "scn_res.h"
#include "fonts.hpp"

//======================= StatusSection =============

class StatusSection : public CustomBkWindow {
//  static const char className[];
//  static ATOM classAtom;
  protected:
    Boolean created;
    int startX;        // starting point for message
    CString message;
    HDC hdc;
    StatusWindow* owner;
    FontManager font;
  public:
    StatusSection(StatusWindow* parent);
    ~StatusSection();
//  static ATOM registerClass();
    virtual void create(HWND hWnd) = 0;
//  virtual void onSize(HWND hWnd, UINT state, int cx, int cy) = 0;
    virtual void update() = 0;
    void putMessage(const char* text);
  private:
    LRESULT procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
    void onPaint(HWND hwnd);
    void onDestroy(HWND hWnd);
    virtual void onCommand(HWND hWnd, int id, HWND hwndCtl, UINT codeNotify);
    virtual void onDrawItem(HWND hwnd, const DRAWITEMSTRUCT * lpDrawItem) {}

#ifdef DEBUG
   void checkPalette() { }
#endif
};

//======================= derived classes of StatusSection =============
#if !defined(EDITOR)

#define IDMSGWIN_BUTTON 100

class MsgWinSection : public StatusSection {
//  MessageWindow* d_msgWin;
    int numMessages;
    int messagesRead;

    ImagePos* buttonPos;
    ImageLibrary* buttons;

    enum {
       NoMessageOff,
       MessageOff,
       NoReadOff,
       NoMessageOn,
       MessageOn,
       NoReadOn,
       HowMany
    };
//  HBITMAP hRead;
//  HBITMAP hNotRead;
//  HBITMAP hNoMsg;
  public:
    MsgWinSection(StatusWindow* parent);
    ~MsgWinSection();
  private:
    //StatusSection virtual functions
    void create(HWND hWnd);
    void onSize(HWND hWnd, UINT state, int cx, int cy);
    void update();
    void onCommand(HWND hWnd, int id, HWND hwndCtl, UINT codeNotify);
    void onDrawItem(HWND hwnd, const DRAWITEMSTRUCT * lpDrawItem);

};

#endif   // !EDITOR

class EmptySection : public StatusSection {
  public:
    EmptySection(StatusWindow* parent) : StatusSection(parent) {}

  private:
    //StatusSection virtual functions
    void create(HWND hWnd);
    // void onSize(HWND hWnd, UINT state, int cx, int cy);
    void update();

};

class HelperTextSection : public StatusSection {
    Boolean active;
  public:
    HelperTextSection(StatusWindow* parent);
  private:
    // makeOnlyWindow();

    //StatusSection virtual functions

    // void onPaint(HWND hWnd);
    void create(HWND hWnd);
    void onSize(HWND hWnd, UINT state, int cx, int cy);
    void update();

};

//=================== StatusWindow class ===============================

static const char StatusWindow::s_className[] = "StatusWindow";


static ATOM StatusWindow::classAtom = 0;
//static const char msgWindowRegName[] = "msgWindow";


static ATOM StatusWindow::registerClass()
{
   if(!classAtom)
   {
      WNDCLASS wc;

      wc.style = CS_OWNDC | CS_DBLCLKS;
      wc.lpfnWndProc = baseWindProc;
      wc.cbClsExtra = 0;
      wc.cbWndExtra = sizeof(StatusWindow*);
      wc.hInstance = APP::instance();
      wc.hIcon = NULL;
      wc.hCursor = LoadCursor(NULL, IDC_ARROW);
      wc.hbrBackground = (HBRUSH) (1 + COLOR_3DFACE);
      wc.lpszMenuName = NULL;
      wc.lpszClassName = s_className;
      classAtom = RegisterClass(&wc);
   }

   ASSERT(classAtom != 0);

   return classAtom;
}

const int nSections = 2;

// TODO: remove numSections parameter when Steven is done with Campwind

StatusWindow::StatusWindow(HWND parent, CampaignWindowsInterface* campWind) :
  d_campWindows(campWind)
{
#ifdef DEBUG_MESSAGE_WINDOW
   debugLog("StatusWindow::create\n");
#endif
   ASSERT(d_campWindows);

   // hwndParent = parent;
   // onlyWindowActive = False;

// nSections = numSections;
   sections = new StatusSection*[nSections];
   ASSERT(sections != 0);

   for(int i = 0; i < nSections; i++)
     sections[i] = 0;

    registerClass();

   HWND hWnd = createWindow(
      WS_EX_LEFT,
      // className,
         NULL,
         WS_CHILD |
         WS_CLIPSIBLINGS,     /* Style */
      0,                   /* init. x pos */
      0,                   /* init. y pos */
      0,                   /* init. x size */
      0,                   /* init. y size */
      parent,                 /* parent window */
      NULL
      // wndInstance(parent)
   );

   ASSERT(hWnd != NULL);

   g_toolTip.setHintDisplay(this);
}

StatusWindow::~StatusWindow()
{
    selfDestruct();
   g_toolTip.setHintDisplay(0);
}

LRESULT StatusWindow::procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
#ifdef DEBUG_MESSAGE_WINDOW
   debugLog("StatusWindow::procMessage(%s)\n",
      getWMdescription(hWnd, msg, wParam, lParam));
#endif

   // LockData lock = this;

   // LRESULT l;
   // if(handlePalette(hWnd, msg, wParam, lParam, l))
   //    return l;

   switch(msg)
   {
      HANDLE_MSG(hWnd, WM_CREATE,   onCreate);
      HANDLE_MSG(hWnd, WM_SIZE, onSize);
//    HANDLE_MSG(hWnd, WM_DESTROY,  onDestroy);
//    HANDLE_MSG(hWnd, WM_PAINT, onPaint);
      HANDLE_MSG(hWnd, WM_COMMAND, onCommand);
//    HANDLE_MSG(hWnd, WM_SHOWWINDOW, onShow);
//    HANDLE_MSG(hWnd, WM_MOVE, onMove);

   default:
      return defProc(hWnd, msg, wParam, lParam);
   }
}

void StatusWindow::onCommand(HWND hWnd, int id, HWND hwndCtl, UINT codeNotify)
{
#if 0
#if !defined(EDITOR)
  switch(id)
  {
    case IDMSGWIN_BUTTON:
      {
         d_msgWin->show();
#if 0
         MessageWindow* mw = campWind->getMessageWindow();
         if(mw)
            mw->show();
#endif
      }
      break;
  }
#endif
#endif
}


BOOL StatusWindow :: onCreate(HWND hWnd, CREATESTRUCT* lpCreateStruct)
{

#if defined(EDITOR)
   sections[0] = new EmptySection(this);
#else
  sections[0] = new MsgWinSection(this);
#endif
  ASSERT(sections[0] != 0);
  sections[0]->create(sections[0]->getHWND());

  sections[1] = new HelperTextSection(this);
  ASSERT(sections[1] != 0);
  sections[1]->create(sections[1]->getHWND());

  return TRUE;
}

void StatusWindow::onSize(HWND hWnd, UINT state, int cx, int cy)
{
#if 0
  RECT r;
  GetClientRect(hwndParent, &r);
  SetWindowPos(hWnd, HWND_TOP, r.left, r.bottom - 50, r.right-348, 50, SWP_SHOWWINDOW);

  for(int i = 0; i < 2; i++)
  {
    int cx = (r.right-349) - r.left;
    int cy = 23;
    sections[i]->onSize(sections[i]->getHWND(), -1, cx, cy);
  }
#else
  for(int i = 0; i < 2; i++)
  {
   SetWindowPos(sections[i]->getHWND(), HWND_TOP,
      0, (cy * i) / 2,
      cx, cy / 2,
      0);
  }
#endif

}


#if 0
void StatusWindow::printf(int section, const char* fmt, ...)
{
   // LockData lock = this;

   ASSERT(fmt != 0);
   ASSERT(section < nSections);
   char buffer[500];
   va_list vaList;
   va_start(vaList, fmt);
   _vbprintf(buffer, 500, fmt, vaList);
   va_end(vaList);
   sections[section]->putMessage(buffer);
   sections[section]->update();
}
#else

void StatusWindow::showHint(const char* text)
{
   sections[1]->putMessage(text);
   sections[1]->update();
}

void StatusWindow::clearHint()
{
   sections[1]->putMessage("");
   sections[1]->update();
}

void StatusWindow::updateMessageWindow()
{
   sections[0]->update();
}

#endif

void StatusWindow::update()
{
   for(int i = 0; i < nSections; i++)
     sections[i]->update();
}

//======================= Implementation of StatusSection class ==========


StatusSection::StatusSection(StatusWindow* parent) :
   CustomBkWindow(scenario->getBorderColors())
{
   owner = parent;
   // message = 0;
   created = False;
   hdc = 0;

// bkDib = BMP::newDrawDIB(scenario->getScenarioResDLL(), MAKEINTRESOURCE(BM_BK_SIDE1), BMP::RBMP_Normal);
   bkDib = scenario->getSideBkDIB(SIDE_Neutral);
   ASSERT(bkDib != 0);

   HWND hwnd = createWindow(
      0,
      // className(),
         NULL,
         WS_CHILD |
//       WS_BORDER |
         WS_VISIBLE |
         WS_CLIPSIBLINGS,     /* Style */
      0,                   /* init. x pos */
      0,                   /* init. y pos */
      0,                   /* init. x size */
      0,                   /* init. y size */
      owner->getHWND(),                /* parent window */
      NULL
      // wndInstance(owner->getHWND())
   );

   ASSERT(hwnd != NULL);
// UpdateWindow(hWnd);
}

StatusSection::~StatusSection()
{
//  font.deleteFont();
  // if(message != 0)
  //   delete[] message;
}



LRESULT StatusSection::procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
#ifdef DEBUG_MESSAGE_WINDOW
   debugLog("StatusSection::procMessage(%s)\n",
      getWMdescription(hWnd, msg, wParam, lParam));
#endif

   LRESULT l;
   if(handlePalette(hWnd, msg, wParam, lParam, l))
      return l;

   switch(msg)
   {
      HANDLE_MSG(hWnd, WM_PAINT, onPaint);
      HANDLE_MSG(hWnd, WM_COMMAND, onCommand);
      HANDLE_MSG(hWnd, WM_ERASEBKGND, onEraseBk);
      HANDLE_MSG(hWnd, WM_DRAWITEM, onDrawItem);
   default:
      return defProc(hWnd, msg, wParam, lParam);
   }

}

void StatusSection::onCommand(HWND hWnd, int id, HWND hwndCtl, UINT codeNotify)
{
   owner->onCommand(hWnd, id, hwndCtl, codeNotify);
}

void StatusSection :: onDestroy(HWND hWnd)
{
//  saveWindowState(msgWindowRegName, hWnd);

  if(hdc != 0)
    ReleaseDC(hWnd, hdc);
}



void StatusSection::onPaint(HWND hwnd)
{
  PAINTSTRUCT ps;
  HDC hdc = BeginPaint(hwnd, &ps);
  ASSERT(hdc != 0);
#if 0
  if(message != 0)
    wTextOut(hdc, startX, 3, message);
#else
  RECT r;
  GetClientRect(hwnd, &r);

  drawThinBorder(hdc, r);

  const char* msg = message;
  if(msg != 0)
    wTextOut(hdc, startX, 3, msg);
#endif
  EndPaint(hwnd, &ps);
}

void StatusSection::putMessage(const char* text)
{
  // if(message != 0)
  //   delete[] message;
  // const char* msg = message;

  int result = 1;

   // check to see if string has changed to prevent excessive screen flicker

   if((message == 0) || (lstrcmp(text, message) != 0))
   {
      message = copyString(text);
      InvalidateRect(getHWND(), NULL, TRUE);
   }
}

//===================== Implementation of EmptySection class ================


void EmptySection::create(HWND hWnd)
{
  hdc = GetDC(hWnd);
  ASSERT(hdc != 0);
  SetBkMode(hdc, TRANSPARENT);
  created = True;
  startX = 3;
  font.setFont(hdc, "Copperplate Gothic Bold", 15);

}


void EmptySection::update()
{
   // InvalidateRect(getHWND(), NULL, TRUE);
}

#if !defined(EDITOR)

MsgWinSection::MsgWinSection(StatusWindow* parent) :
   StatusSection(parent)
{
  numMessages = 0;
  messagesRead = 0;
  buttonPos = 0;
  buttons = 0;
}

MsgWinSection::~MsgWinSection()
{
  if(buttons)
    delete buttons;
}

//===================== Implementation of MsgWinSection class ==============


void MsgWinSection::create(HWND hWnd)
{

  hdc = GetDC(hWnd);
  ASSERT(hdc != 0);
  SetBkMode(hdc, TRANSPARENT);

  // get position data for Icons
  buttonPos = (ImagePos*)loadResource(scenario->getScenarioResDLL(), MAKEINTRESOURCE(PR_MESSAGEBUTTONS), RT_IMAGEPOS);
  ASSERT(buttonPos != NULL);

  /*
   *  Now allocate and create Image libraries
   */

  // for order icons
  buttons = scenario->getImageLibrary(MAKEINTRESOURCE(BM_MESSAGEICONS), HowMany, buttonPos, True);
  ASSERT(buttons != 0);

  HWND hButton = addOwnerDrawButton(hWnd, IDMSGWIN_BUTTON, 3, 1, 19, 19);
  EnableWindow(hButton, numMessages != 0);

  InvalidateRect(hButton, NULL, TRUE);

  font.setFont(hdc, "Copperplate Gothic Bold", 15);

  created = True;
  startX = 32;

}


void MsgWinSection::onDrawItem(HWND hwnd, const DRAWITEMSTRUCT * lpDrawItem)
{
   HPALETTE oldPal = SelectPalette(lpDrawItem->hDC, Palette::get(), FALSE);
   RealizePalette(lpDrawItem->hDC);

   if(lpDrawItem->CtlID == IDMSGWIN_BUTTON)
   {

     UWORD index = 0;
     Boolean selected = (Boolean)(lpDrawItem->itemState & ODS_SELECTED);

     if(numMessages == 0)
       index = (UWORD) ((selected == True) ? NoMessageOn : NoMessageOff);
     else if(messagesRead == 0 || messagesRead < numMessages)
       index = (UWORD) ((selected == True) ? MessageOn : MessageOff);
     else
       index = (UWORD) ((selected == True) ? NoReadOn : NoReadOff);

     buttons->blit(lpDrawItem->hDC, index, 0, 0);

   }
   SelectPalette(lpDrawItem->hDC, oldPal, FALSE);
}


void MsgWinSection::update()
{
   // MessageWindow* mw = owner->campWind->getMessageWindow();

#if !defined(EDITOR)
   numMessages = owner->campWindow()->getNumCampaignMessages();
   messagesRead = owner->campWindow()->getCampaignMessagesRead();
#else
   numMessages = 0;
   messagesRead = 0;
#endif

   Button button(getHWND(), IDMSGWIN_BUTTON);

   InvalidateRect(button.getHWND(), NULL, TRUE);
   button.enable(numMessages != 0);

   char buffer[80];
   wsprintf(buffer, "Messages: %d Unread: %d",
      numMessages,
      numMessages - messagesRead);
   putMessage(buffer);

   // InvalidateRect(getHWND(), NULL, TRUE);
}


void MsgWinSection::onCommand(HWND hWnd, int id, HWND hwndCtl, UINT codeNotify)
{
  switch(id)
  {
    case IDMSGWIN_BUTTON:
      owner->campWindow()->toggleMessageWindow();
      break;
  }
}


#endif   // !EDITOR

//============== Implementation of HelperTextSection class ==================

HelperTextSection::HelperTextSection(StatusWindow* parent) : StatusSection(parent)
{
  active = False;
}

void HelperTextSection::update()
{
  active = !active;
  
  // SendMessage(owner->getHWND(), WM_SIZE, 0, 0);
  // InvalidateRect(owner->getHWND(), NULL, TRUE);
  // InvalidateRect(getHWND(), NULL, TRUE);
}



void HelperTextSection::create(HWND hWnd)
{
  hdc = GetDC(hWnd);
  ASSERT(hdc != 0);
  SetBkMode(hdc, TRANSPARENT);
  created = True;
  startX = 3;

//  HFONT oldFont = (HFONT)SelectObject(hdc, hFont);

  font.setFont(hdc, "Copperplate Gothic Bold", 19);

}


/*
 *----------------------------------------------------------------------
 * $Log$
 *----------------------------------------------------------------------
 */
