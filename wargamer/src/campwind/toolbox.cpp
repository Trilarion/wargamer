/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
#include "stdinc.hpp"
#include "toolbox.hpp"
#include "app.hpp"
#include "scenario.hpp"
#include "campint.hpp"
#include "imglib.hpp"
#include "scn_res.h"
#include "button.hpp"
#include "wmisc.hpp"
#include "palette.hpp"
#include "registry.hpp"
#include "dib.hpp"
#include "palwind.hpp"
#include "scn_img.hpp"
#include "weathwin.hpp"
#include "camptool.hpp"
#include "msgwin.hpp"
#include "cdialog.hpp"
// #include "generic.hpp"
#include "tooltip.hpp"
#include "res_str.h"
#include "cwin_int.hpp"

#ifdef DEBUG
#include "logwin.hpp"
#endif

static const char registryName[] = "\\ToolBox";

/*
 * IDs for toolbox buttons
 */

class ToolBoxButtonID {
public:
  enum Value {
    First = 0,
    ResourceModeButton = First,
    UnitModeButton,
    WeatherWindowButton,
    MessageWindowButton,
    TinyMapButton,

    HowMany
  };
};

/*
 * Indexes for toolbox button images
 */

class ToolBoxButtonImage {
public:
  enum Value {
    ResourceModeOff,
    ResourceModeOn,

    UnitModeOff,
    UnitModeOn,

    WeatherWindowOff,
    WeatherWindowOn,

    MessageWindowOff,
    MessageWindowOn,
    MessageWindowDisabled,

    TinyMapOff,
    TinyMapOn,

    HowMany
  };
};

class ToolBoxWindow : public WindowBaseND, public PaletteWindow { //public CustomBkWindow {
    CampaignInterface* d_campGame;
    CampaignWindowsInterface* d_campWind;
    UINT d_nColumns;
    UINT d_nRows;

    Boolean d_weatherWinShowing;
    Boolean d_tinyMapShowing;
    Boolean d_messageWinShowing;

    CustomDialog d_customDial;
//  UINT d_roundOffValue;
  public:
    ToolBoxWindow(HWND parent, CampaignInterface* campGame, CampaignWindowsInterface* campWind);
    ~ToolBoxWindow();

  private:
    LRESULT procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);

    BOOL onCreate(HWND hWnd, CREATESTRUCT* lpCreateStruct);
    void onDestroy(HWND hWnd);
    void onCommand(HWND hWnd, int id, HWND hwndCtl, UINT codeNotify);
    void onDrawItem(HWND hwnd, const DRAWITEMSTRUCT * lpDrawItem);
//  void onShowWindow(HWND hwnd, BOOL fShow, UINT status);
//  void onPaint(HWND hWnd);
//  void onContextMenu(HWND hwnd, HWND hwndCtl, int x, int y);
//  LRESULT onNotify(HWND hWnd, int id, NMHDR* lpNMHDR);
//  void onMove(HWND hWnd, int x, int y);
    void onSize(HWND hWnd, UINT state, int cx, int cy);
//  BOOL onSetCursor(HWND hwnd, HWND hwndCursor, UINT codeHitTest, UINT msg);
//  BOOL onSizing(HWND hWnd, UINT fwSide, RECT* lpRect);
//  UINT onNCHitTest(HWND hwnd, int x, int y);

//  void adjustSize(int& cx, int& cy);
    void setupButtonStates();
    void setFlags();

#ifdef DEBUG
    void checkPalette() { }
#endif
};

const int ButtonWidth = 20;
const int ButtonHeight = 20;
const UINT offset = 3;  // offset from edges, in pixels,

ToolBoxWindow::ToolBoxWindow(HWND parent, CampaignInterface* campGame, CampaignWindowsInterface* campWind) :
   d_campGame(campGame),
   d_campWind(campWind),
   d_nColumns(2),
   d_nRows(3),
   d_weatherWinShowing(False),
   d_tinyMapShowing(False),
   d_messageWinShowing(False)
{
   ASSERT(d_campGame != 0);

   const int borderWidth = GetSystemMetrics(SM_CXSIZEFRAME);
   const int captionCY = GetSystemMetrics(SM_CYCAPTION);
   int cx = ((d_nColumns*ButtonWidth)+(2*offset))+(2*borderWidth);
   int cy = ((d_nRows*ButtonHeight)+(2*offset))+(2*borderWidth)+captionCY;

   RECT r;
   // set up default position
   SetRect(&r, 400, 200, cx, cy);

   POINT p;
   if(getRegistry("position", &p, sizeof(POINT), registryName))
     SetRect(&r, p.x, p.y, cx, cy);

   if(getRegistry("screen", &p, sizeof(POINT), registryName))
     checkResolution(r, p);

   HWND hWnd = createWindow(
      0, //WS_EX_TOOLWINDOW, // | WS_EX_CONTEXTHELP,
      GenericClass::className(),       // Class Name
      NULL,    // Window Name
      WS_POPUP |
      WS_CAPTION,// |
//    WS_SIZEBOX,
//    WS_BORDER |
//    WS_VISIBLE, // |
//    WS_CLIPSIBLINGS,        /* Style */
      r.left, r.top, cx, cy,     // Position will get set from campwind
      parent,                 /* parent window */
      NULL,
      APP::instance()
   );

   ASSERT(hWnd != NULL);

   setupButtonStates();

}

ToolBoxWindow::~ToolBoxWindow()
{
    selfDestruct();
}

/* BOOL onSizing(HWND hwnd, UINT fwSide, RECT* lpRect) */
#define HANDLE_WM_SIZING(hwnd, wParam, lParam, fn) \
    ((LRESULT)(DWORD)(BOOL)(fn)((hwnd), (UINT)(wParam), (RECT*)(lParam)), 0L)

LRESULT ToolBoxWindow::procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
   LRESULT l;

   if(handlePalette(hWnd, msg, wParam, lParam, l))
      return l;

   switch(msg)
   {
      HANDLE_MSG(hWnd, WM_CREATE,   onCreate);
      HANDLE_MSG(hWnd, WM_DESTROY,  onDestroy);
      HANDLE_MSG(hWnd, WM_DRAWITEM, onDrawItem);
      HANDLE_MSG(hWnd, WM_SIZE, onSize);
      HANDLE_MSG(hWnd, WM_COMMAND, onCommand);
//    HANDLE_MSG(hWnd, WM_SHOWWINDOW, onShowWindow);
//    HANDLE_MSG(hWnd, WM_ERASEBKGND, onEraseBk);
//    HANDLE_MSG(hWnd, WM_SIZING, onSizing);
//    HANDLE_MSG(hWnd, WM_NCHITTEST, onNCHitTest);

   default:
      return defProc(hWnd, msg, wParam, lParam);
   }
}


BOOL ToolBoxWindow::onCreate(HWND hwnd, CREATESTRUCT* lpCreateStruct)
{

   /*
    * Add some buttons
    */


   initToolButton(APP::instance());

   /*
    * Zoom buttons
    */

   for(int i = ToolBoxButtonID::First; i < ToolBoxButtonID::HowMany; i++)
   {
     HWND hButton = addToolButton(hwnd, i, 0, 0, ButtonWidth, ButtonHeight);
     ASSERT(hButton);
   }

   d_customDial.setBkDib(scenario->getSideBkDIB(SIDE_Neutral));
   d_customDial.setCaptionBkDib(scenario->getScenarioBkDIB(Scenario::BackPatterns::WoodBackground));
   d_customDial.init(hwnd);

   /*
    * Setup the initial states
    */

   setFlags();
// CampaignWindowsInterface* cw = d_campGame->getCampaignWindows();

// d_weatherWinShowing = cw->weatherWindowShowing();
// d_messageWinShowing = MessageWindow::isShowing();
// d_tinyMapShowing = TinyMap::isShowing();

// setupButtonStates();
// adjustSize(lpCreateStruct->cx, lpCreateStruct->cy);

  /*
   * Add tooltips
   */

  static TipData tipData[] = {
    { ToolBoxButtonID::ResourceModeButton,   TTS_CTB_RESOURCE,      SWS_CTB_RESOURCE     },
    { ToolBoxButtonID::UnitModeButton,       TTS_CTB_UNIT,          SWS_CTB_UNIT         },
    { ToolBoxButtonID::MessageWindowButton,  TTS_CTB_MESSAGEWINDOW, SWS_CTB_MESSAGEWINDOW  },
    { ToolBoxButtonID::WeatherWindowButton,  TTS_CTB_WEATHERWINDOW, SWS_CTB_WEATHERWINDOW  },
    { ToolBoxButtonID::TinyMapButton,        TTS_CTB_TINYMAP,       SWS_CTB_TINYMAP  },
    EndTipData
  };

  g_toolTip.addTips(hWnd, tipData);
   return TRUE;
}

void ToolBoxWindow::setFlags()
{
  ASSERT(d_campWind);

  d_weatherWinShowing = d_campWind->weatherWindowShowing();
  d_messageWinShowing = d_campWind->messageWindowShowing();
  d_tinyMapShowing = d_campWind->tinyMapShowing();

  setupButtonStates();
}

#if 0
BOOL ToolBoxWindow::onSizing(HWND hWnd, UINT fwSide, RECT* lpRect)
{
  int cx = lpRect->right-lpRect->left;
  int cy = lpRect->bottom-lpRect->top;

#ifdef DEBUG
  logWindow->printf("pre-adjusted size -- cx = %d, cy = %d", cx, cy);
#endif

  adjustSize(cx, cy);

#ifdef DEBUG
  logWindow->printf("adjusted size -- cx = %d, cy = %d", cx, cy);
#endif

  SetRect(lpRect, lpRect->left, lpRect->top, lpRect->left+cx, lpRect->top+cy);
  return TRUE;
}
#endif

#if 0
void ToolBoxWindow::adjustSize(int& cx, int& cy)
{
  /*
   *  Adjust width and height according to number of buttons
   */

//  const int borderWidth = GetSystemMetrics(SM_CXSIZEFRAME);
//  const int captionCY = GetSystemMetrics(SM_CYCAPTION);

  int larger = maximum(cx, cy);
  int smaller = minimum(cx, cy);

  int r = maximum(1, (smaller/2)-1);  // round off value

  int ratio = (larger+r)/smaller;

  enum {
    ColumnsHaveMore,
    RowsHaveMore
  } whoHasMore;

  whoHasMore = (cy > cx) ? RowsHaveMore : ColumnsHaveMore;

  if(whoHasMore == ColumnsHaveMore)
  {
    const int divisor = minimum(ToolBoxButtonID::HowMany, (ratio+1));
//  int r = maximum(1, (divisor/2)-1);  // round off value
    d_nRows = ToolBoxButtonID::HowMany/divisor;
    d_nColumns = (d_nRows == 1) ? ToolBoxButtonID::HowMany :
      (ToolBoxButtonID::HowMany/d_nRows)+(ToolBoxButtonID::HowMany%d_nRows);

  }
  else
  {
    const int divisor = minimum(ToolBoxButtonID::HowMany, (ratio+1));
//  int r = maximum(1, (divisor/2)-1);  // round off value
    d_nColumns = ToolBoxButtonID::HowMany/divisor;
    d_nRows = (d_nRows == 1) ? ToolBoxButtonID::HowMany :
      (ToolBoxButtonID::HowMany/d_nColumns)+(ToolBoxButtonID::HowMany%d_nColumns);
  }

  // now readjust size
  cx = ((d_nColumns*ButtonWidth)+(2*offset)); //+(2*borderWidth);
  cy = ((d_nRows*ButtonHeight)+(2*offset)); //+(2*borderWidth)+captionCY;


}
#endif

#if 0
UINT ToolBoxWindow::onNCHitTest(HWND hwnd, int x, int y)
{
//  POINT p;
//  p.x = x;
//  p.y = y;

  const int borderWidth = GetSystemMetrics(SM_CXSIZEFRAME);

  RECT r;
  GetWindowRect(hwnd, &r);

//  if(PtInRect(&captionRect, p))
//  return HTCAPTION;
  if( (x > r.left && x <= r.left+borderWidth) &&
      (y > r.top && y <= r.bottom) )
  {
    return HTLEFT;
  }
  else if( (x > r.right-borderWidth && x <= r.right) &&
           (y > r.top && y <= r.bottom) )
  {
    return HTRIGHT;
  }
  else if( (y > r.top && y <= r.top+borderWidth) &&
           (x > r.left && x <= r.right) )
  {
    return HTTOP;
  }
  else if( (y > r.bottom-borderWidth && y <= r.bottom) &&
           (x > r.left && x <= r.right) )
  {
    return HTBOTTOM;
  }
  else
    return defProc(hwnd, WM_NCHITTEST, (WPARAM) 0, MAKELPARAM(x, y));

}
#endif

void ToolBoxWindow::onSize(HWND hWnd, UINT state, int cx, int cy)
{
#if 0
  static Boolean resized = False;

//  if(!resized)
//  {
  const int borderWidth = GetSystemMetrics(SM_CXSIZEFRAME);
  const int captionCY = GetSystemMetrics(SM_CYCAPTION);

#ifdef DEBUG
  logWindow->printf("onSize before -- cx = %d, cy = %d", cx, cy);
  logWindow->printf("nColumns = %d, nRows = %d", d_nColumns, d_nRows);
#endif

  if(!resized)
  {
    adjustSize(cx, cy);

#ifdef DEBUG
    logWindow->printf("onSize after -- cx = %d, cy = %d", cx, cy);
    logWindow->printf("nColumns = %d, nRows = %d", d_nColumns, d_nRows);
#endif
    resized = True;
  }
  else
    resized = False;
#endif

  int y = offset;
  ToolBoxButtonID::Value id = ToolBoxButtonID::First;

  for(int r = 0; r < d_nRows; r++)
  {
    int x = offset;
    for(int c = 0; c < d_nColumns; c++)
    {
      if(id < ToolBoxButtonID::HowMany)
      {

        HWND h = GetDlgItem(hWnd, id);
        ASSERT(h);

        MoveWindow(h, x, y, ButtonWidth, ButtonHeight, TRUE);
        x += ButtonWidth;
      }
      INCREMENT(id);
    }
    y += ButtonHeight;
  }

//  SetWindowPos(hWnd, HWND_TOP, 0, 0, cx+(2*borderWidth), cy+(2*borderWidth)+captionCY, SWP_NOMOVE);
//  resized = True;
//  }
//  else
//  resized = False;
}

void ToolBoxWindow::onDestroy(HWND hWnd)
{
// g_toolTip.delTips(hWnd);
  /*
   * Set registry
   */

  RECT r;
  GetWindowRect(hWnd, &r);

  // set position
  POINT p;
  p.x = r.left;
  p.y = r.top;
  setRegistry("position", &p, sizeof(POINT), registryName);

  // set resolution
  p.x = GetSystemMetrics(SM_CXSCREEN);
  p.y = GetSystemMetrics(SM_CYSCREEN);
  setRegistry("screen", &p, sizeof(POINT), registryName);

}


enum ButtonState {
  ButtonOff,
  ButtonOn,
  ButtonDisabled,

  ButtonState_HowMany
};

UWORD getImageIndex(ToolBoxButtonID::Value id, ButtonState state)
{
  ASSERT(id < ToolBoxButtonID::HowMany);
  ASSERT(state < ButtonState_HowMany);

  if(id == ToolBoxButtonID::ResourceModeButton)
  {
    return (state == ButtonOff) ? static_cast<UWORD>(ToolBoxButtonImage::ResourceModeOff) :
                                  static_cast<UWORD>(ToolBoxButtonImage::ResourceModeOn);
  }

  else if(id == ToolBoxButtonID::UnitModeButton)
  {
    return (state == ButtonOff) ? static_cast<UWORD>(ToolBoxButtonImage::UnitModeOff) :
                                  static_cast<UWORD>(ToolBoxButtonImage::UnitModeOn);
  }

  else if(id == ToolBoxButtonID::WeatherWindowButton)
  {
    return (state == ButtonOff) ? static_cast<UWORD>(ToolBoxButtonImage::WeatherWindowOff) :
                                  static_cast<UWORD>(ToolBoxButtonImage::WeatherWindowOn);
  }

  else if(id == ToolBoxButtonID::MessageWindowButton)
  {
    return (state == ButtonOff) ? static_cast<UWORD>(ToolBoxButtonImage::MessageWindowOff) :
                                  static_cast<UWORD>(ToolBoxButtonImage::MessageWindowOn);
  }
  else
  {
    ASSERT(id == ToolBoxButtonID::TinyMapButton);
    return (state == ButtonOff) ? static_cast<UWORD>(ToolBoxButtonImage::TinyMapOff) :
                                  static_cast<UWORD>(ToolBoxButtonImage::TinyMapOn);
  }
}

void ToolBoxWindow::onDrawItem(HWND hwnd, const DRAWITEMSTRUCT * lpDrawItem)
{
   HPALETTE oldPalette = SelectPalette(lpDrawItem->hDC, Palette::get(), FALSE);
   RealizePalette(lpDrawItem->hDC);

   const ImageLibrary* buttonImages = ScenarioImageLibrary::get(ScenarioImageLibrary::ToolBoxButtons);

   ButtonState state = (lpDrawItem->itemState & (ODS_SELECTED | ODS_CHECKED)) ? ButtonOn :
                       (lpDrawItem->itemState & (ODS_DISABLED)) ? ButtonDisabled :
                       ButtonOff;


   UWORD index = getImageIndex(static_cast<ToolBoxButtonID::Value>(lpDrawItem->CtlID), state);
   buttonImages->blit(lpDrawItem->hDC, index, 0, 0);

   SelectPalette(lpDrawItem->hDC, oldPalette, TRUE);
   RealizePalette(lpDrawItem->hDC);
}

void ToolBoxWindow::onCommand(HWND hWnd, int id, HWND hwndCtl, UINT codeNotify)
{
   if(codeNotify == BN_CLICKED)
   {
      switch(id)
      {
        case ToolBoxButtonID::ResourceModeButton:
          if(codeNotify == BN_CLICKED)
          {
            d_campGame->setMode(CampaignInterface::CM_RESOURCE);
            setupButtonStates();
          }
          break;

        case ToolBoxButtonID::UnitModeButton:
          if(codeNotify == BN_CLICKED)
          {
            d_campGame->setMode(CampaignInterface::CM_UNITS);
            setupButtonStates();
          }
          break;

        case ToolBoxButtonID::WeatherWindowButton:
          if(codeNotify == BN_CLICKED)
          {
            d_campWind->toggleWeatherWindow();
            setFlags();
          }
          break;

        case ToolBoxButtonID::MessageWindowButton:
          if(codeNotify == BN_CLICKED)
          {
            d_campWind->toggleMessageWindow();
            setFlags();
          }
          break;

        case ToolBoxButtonID::TinyMapButton:
          if(codeNotify == BN_CLICKED)
          {
            d_campWind->toggleTinyMap();
            setFlags();
          }
          break;
      }
   }

}

void ToolBoxWindow::setupButtonStates()
{
   {
     ToolButton b(getHWND(), ToolBoxButtonID::ResourceModeButton);
     b.setCheck(d_campGame->getMode() == CampaignInterface::CM_RESOURCE);
   }

   {
     ToolButton b(getHWND(), ToolBoxButtonID::UnitModeButton);
     b.setCheck(d_campGame->getMode() == CampaignInterface::CM_UNITS);
   }

   {
     ToolButton b(getHWND(), ToolBoxButtonID::WeatherWindowButton);
     b.setCheck(d_weatherWinShowing);
   }

   {
     ToolButton b(getHWND(), ToolBoxButtonID::TinyMapButton);
     b.setCheck(d_tinyMapShowing);
   }

   {
     ToolButton b(getHWND(), ToolBoxButtonID::MessageWindowButton);
//   b.enable(MessageWindow::getNumMessages() > 0);
     b.setCheck(d_messageWinShowing);
   }
}

/*-------------------------------------------------------
 * Interface to the outside world
 */

//static ToolBoxWindow* s_toolBox = 0;

void CampaignToolBox::create(HWND parent, CampaignInterface* campGame, CampaignWindowsInterface* campWind)
{
  ASSERT(d_toolBox == 0);
  d_toolBox = new ToolBoxWindow(parent, campGame, campWind);
  ASSERT(d_toolBox);
}

void CampaignToolBox::show()
{
  ASSERT(d_toolBox);
  if(d_toolBox)
  {
    ShowWindow(d_toolBox->getHWND(), SW_SHOW);
  }
}

void CampaignToolBox::destroy()
{
  if(d_toolBox)
  {
    DestroyWindow(d_toolBox->getHWND());
    d_toolBox = 0;
  }
}


/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.2  2001/06/18 08:25:45  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
