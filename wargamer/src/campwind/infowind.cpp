/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1995, Steven Morle-Green, All Rights Reserved
 *----------------------------------------------------------------------
 *
 * Tracking Information Window
 *
 * This should be updated, so that townInfo and unitInfo are in
 * their own seperate windows which get resized to use the full
 * space available.  The calls to drawTownInfo and drawUnitInfo must
 * be aware of how much space is available, and what mode they are in.
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "infowind.hpp"
#include "unitinfo.hpp"
#include "towninfo.hpp"
#include "armyutil.hpp"
#include "dib_util.hpp"
#include "dib.hpp"
#include "scrnbase.hpp"
#include "scenario.hpp"
#include "trackwin.hpp"

#if defined(DEBUG) && !defined(EDITOR)
#include "logwin.hpp"
#endif

class RealTrackingWindow : public InsertWind {
//  SeeThroughWindow d_seeThroughWindow;
    const CampaignData* d_campData;
    ConstICommandPosition d_cpi;
    ITown d_town;

  public:
    RealTrackingWindow(HWND hParent, const CampaignData* d_campData);
    ~RealTrackingWindow();

     void show(bool visible) { WindowBaseND::show(visible); }

    void show(PixelPoint& p, ConstParamCP cpi);
    void update(ConstParamCP cpi);
    void show(PixelPoint& p, ITown iTown);
    void update(ITown itown);

    // virtual function from TrackWind
    void drawInsertInfo(const PixelRect& rect);
  private:
    LRESULT procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
};

RealTrackingWindow::RealTrackingWindow(HWND hParent, const CampaignData* campData) :
  d_campData(campData),
  d_cpi(NoCommandPosition),
  d_town(NoTown)
{
  IW_Data iwd;
  iwd.d_hParent = hParent;
//  iwd.d_cx = (ScreenBase::dbX() * 80) / ScreenBase::baseX();
  iwd.d_cx = (ScreenBase::dbX() * 96) / ScreenBase::baseX();
  iwd.d_cy = (ScreenBase::dbY() * 62) / ScreenBase::baseY();
  iwd.d_flags = IW_Data::DelayShow;
//  iwd.d_bColors = scenario->getBorderColors();
//  iwd.d_bFillDib = scenario->getScenarioBkDIB(Scenario::BackPatterns::WoodBackground);
  // set transparent color
  COLORREF c;
  scenario->getColour("MapInsert", c);
//  setInsertColor(c);
  iwd.d_insertColor = c;

  create(iwd);

}

RealTrackingWindow::~RealTrackingWindow()
{
    selfDestruct();
}

LRESULT RealTrackingWindow::procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{

  switch(msg)
  {
    HANDLE_MSG(hWnd, WM_CREATE, onCreate);
    HANDLE_MSG(hWnd, WM_PAINT, onPaint);
    HANDLE_MSG(hWnd, WM_ERASEBKGND, onEraseBk);
    HANDLE_MSG(hWnd, WM_MOUSEMOVE, onMouseMove);
    HANDLE_MSG(hWnd, WM_TIMER, onTimer);
    default:
      return defProc(hWnd, msg, wParam, lParam);
  }
}


void RealTrackingWindow::show(PixelPoint& p, ConstParamCP cpi)
{
  d_cpi = cpi;
  d_town = NoTown;
  InsertWind::show(p);
}

void RealTrackingWindow::update(ConstParamCP cpi)
{
  d_cpi = cpi;
  d_town = NoTown;
  InsertWind::update();
}

void RealTrackingWindow::update(ITown itown)
{
  d_cpi = NoCommandPosition;
  d_town = itown;
  InsertWind::update();
}

void RealTrackingWindow::drawInsertInfo(const PixelRect& r)
{
   ASSERT(d_cpi != NoCommandPosition || d_town != NoTown);

   if(d_cpi != NoCommandPosition)
   {
      UnitInfoData ud(d_cpi, d_campData, dib(), r);
      ud.drawTrackingDib();
   }
   else if(d_town != NoTown)
   {
      TownInfoData td(d_town, d_campData, dib(), r);
      td.drawTrackingDIB();

      // TownInfoDib::drawTrackingDib(d_campData, d_town, dib(), width(), height());
      // TownInfoDib::drawTrackingDib(d_campData, d_town, dib(), r.width(), r.height());
   }
}

void RealTrackingWindow::show(PixelPoint& p, ITown iTown)
{
  d_town = iTown;
  d_cpi = NoCommandPosition;
  InsertWind::show(p);
}

/*------------------------------------------------------
 * Access
 */

TrackingWindow::TrackingWindow(HWND hParent, const CampaignData* campData) :
  d_trackWindow(new RealTrackingWindow(hParent, campData))
{
  ASSERT(d_trackWindow);
}

TrackingWindow::~TrackingWindow()
{
  if(d_trackWindow)
  {
    // DestroyWindow(d_trackWindow->getHWND());
     delete d_trackWindow;
    d_trackWindow = 0;
  }
}

void TrackingWindow::show(PixelPoint& p, ConstParamCP cpi)
{
  if(d_trackWindow)
    d_trackWindow->show(p, cpi);
}

void TrackingWindow::show(PixelPoint& p, ITown iTown)
{
  if(d_trackWindow)
    d_trackWindow->show(p, iTown);
}

void TrackingWindow::update(ConstParamCP cpi)
{
  if(d_trackWindow)
  {
    d_trackWindow->update(cpi);
  }
}

void TrackingWindow::update(ITown itown)
{
  if(d_trackWindow)
  {
    d_trackWindow->update(itown);
  }
}

void TrackingWindow::hide()
{
  if(d_trackWindow)
    d_trackWindow->hide();
}

int TrackingWindow::width()
{
  return (d_trackWindow) ? d_trackWindow->width() : 0;
}

int TrackingWindow::height()
{
  return (d_trackWindow) ? d_trackWindow->height() : 0;
}


/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.2  2001/06/18 08:25:45  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
