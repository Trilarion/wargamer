/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#error "Obsolete"

#ifndef TOWNINT_HPP
#define TOWNINT_HPP

#include "userint.hpp"
#include "gamedefs.hpp"

/*
 * Town Iterface class. Replaces TownOrderTB
 */

class CampaignWindowsInterface;
class CampaignData;
class MapWindow;
class TownDialog;
class TrackingWindow;
class TownMenu_Int;
class TownInfo_Int;
class TownOrder_Int;


class CampaignTownInterface : public CampaignUserInterface {
  public:
	 class TownInterfaceMode {
	 public:
		enum Mode {
		  Waiting,        // waiting for player to do something
		  SettingUpBuild, // player has clicked on town

		  HowMany
		};
	 };

  private:

	 /*
	  * Interface to other parts of game
	  */

	 CampaignWindowsInterface* d_campWind;
	 const CampaignData* d_campData;
	 MapWindow* d_mapWindow;

	 /*
	  * Town Dialog
	  */

//	 TownDialog* d_townDial;

	 TrackingWindow* d_trackingDial;
	 TownOrder_Int* d_orderWind;
    TownInfo_Int* d_townInfoWind;
	 TownMenu_Int* d_menu;

	 TownInterfaceMode::Mode d_mode;

	 /*
	  * Map tracking
	  */

	 TrackMode d_trackMode;       // are we tracking units or towns?
	 ITown d_overThisTown;        // we are tracking this town
    ITown d_town;                // curent town
  public:

	 CampaignTownInterface(CampaignWindowsInterface* cwi, const CampaignData* campData, MapWindow* mapWindow);
	 ~CampaignTownInterface();

	 /*
	  * virtual functions from CampaignUserInterface
	  */

	 TrackMode getTrack() { return d_trackMode; }		// Return how to do tracking


	 /*
	  * Mouse Tracking functions
	  */

	 void onLClick(MapSelect& info);
	 void onButtonDown(MapSelect& info) { onLClick(info); }
	 void onRClick(MapSelect& info);

	 void overObject(MapSelect& info);
	 void notOverObject();

	 /*
	  * Map Drawing
	  */

	 void onDraw(DrawDIBDC* dib, MapSelect& info) {}

	 void update();// {}					// Info may have changed
	 void addObject(ICommandPosition cpi) {}
	 void addObject(ITown town);
	 void updateZoom() {}       // update map arrows when zooming

	 /*
	  * Sending Orders
	  */

	 void setOrderDate(Date& date) {}
	 void sendOrder(ICommandPosition t) {}
	 void cancelOrder();

	 /*
	  * Misc.
	  */

	 void runInfo(const PixelPoint& p);
	 void runOrders(const PixelPoint& p);
	 void runMenu(const PixelPoint& p);

	 CampaignWindowsInterface* campWindows() const { return d_campWind; }
	 const void* getData() const { return 0; }   // derived-class defined data

	 /*
	  * Clean up
	  */

	 void destroy() {}

	 /*
	  * Dialogs
	  */

	 void toggle() {}
	 void initUnits(Boolean all) {}
	 void syncCombos(int i) {}
};

#endif





