/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
#include "stdinc.hpp"
#include "s_result.hpp"
#include "wind.hpp"
#include "app.hpp"
#include "scenario.hpp"
#include "dib.hpp"
#include "dib_util.hpp"
#include "siegeint.hpp"
#include "fonts.hpp"
#include "campdint.hpp"
#include "palette.hpp"
#include "wmisc.hpp"
#include "res_str.h"
#include "resstr.hpp"
#include "scn_img.hpp"
#include "imglib.hpp"
#include "scn_res.h"
#include "town.hpp"
#include "scrnbase.hpp"
#include "trackwin.hpp"
#include "compos.hpp"
#include "random.hpp"


using namespace WG_CampaignSiege;

class SiegeResultUtil {
public:
  static void run(const SiegeInfoData& bd, Event& event);

  static void drawInfo(const SiegeInfoData& bd, const CampaignData* campData,
     DrawDIBDC* dib, const int d_cx, const int d_cy);

  static Side getOtherSide(Side s)
  {
    ASSERT(s != SIDE_Neutral);
    return (s == 0) ? 1 : 0;
  }
};

// draw battle info onto dib
void SiegeResultUtil::drawInfo(const SiegeInfoData& sd, const CampaignData* campData,
   DrawDIBDC* dib, const int cx, const int cy)
{
  ASSERT(sd.d_town != NoTown);
  const Town& t = campData->getTown(sd.d_town);

  /*
   * Some useful values
   */

  const int dbX = ScreenBase::dbX();
  const int dbY = ScreenBase::dbY();
  const int baseX = ScreenBase::baseX();
  const int baseY = ScreenBase::baseY();
  const int shadowWidth = (3 * dbX) / baseX;
  const int shadowHeight = shadowWidth;
  const int mapX = 0;
  const int mapY = 0;
  const int headerY = mapY + (dbY / baseY);

  const COLORREF ourColor = scenario->getSideColour(sd.d_ourList.side());
  const COLORREF theirColor = scenario->getSideColour(sd.d_theirList.side());
  const COLORREF black = PALETTERGB(0, 0, 0);

  const int ourUnitsY = headerY + ((25 * dbY) / baseY);
  const int theirUnitsY = ourUnitsY + ((18 * dbY) / baseY);
  const int ourLossY =  theirUnitsY + ((18 * dbY) / baseY);
  const int theirLossY = ourLossY + ((34 * dbY) / baseY);
  const int leaderNameX = ((40 * dbX) / baseX);
  const int rOffsetX = ((2 * dbX) / baseX);

  LONG x = mapX + (dbX / baseX);
  LONG y = mapY + (dbY / baseY);

  /*
   * draw header text
   */

  LogFont lf;
  lf.height((11 * dbY) / baseY);
  lf.weight(FW_MEDIUM);
  lf.face(scenario->fontName(Font_Bold));
  lf.italic(true);
  lf.underline(true);

  Font font;
  font.set(lf);
  dib->setBkMode(TRANSPARENT);
  HFONT oldFont = dib->setFont(font);

  const ImageLibrary* flagIcons = scenario->getNationFlag(sd.d_ourList.side(), True);
  flagIcons->blit(dib, FI_Army, x, headerY, True);

  COLORREF oldColor = dib->setTextColor(ourColor);

  const char* text = InGameText::get(IDS_SiegeOf);

  x = (x+FI_Army_CX)+((3 * dbX) / baseX);
  wTextPrintf(dib->getDC(), x, headerY, cx - (x + rOffsetX),
     "%s %s", text, t.getNameNotNull());

  /*
   * Draw report headers
   */

  lf.height((8 * dbY) / baseY);
  font.set(lf);
  dib->setFont(font);

//  y += (11 * dbY) / baseY;

  x = mapX + (dbX / baseX);

  // Our side Units
  wTextOut(dib->getDC(), x, ourUnitsY, scenario->getSideName(sd.d_ourList.side()));

  // their side Units
  dib->setTextColor(theirColor);
  wTextOut(dib->getDC(), x, theirUnitsY, scenario->getSideName(sd.d_theirList.side()));

  // our losses
  dib->setTextColor(ourColor);
  text = InGameText::get(IDS_Losses);
  wTextPrintf(dib->getDC(), x, ourLossY, "%s %s",
     scenario->getSideName(sd.d_ourList.side()),
     text);

  // their losses
  dib->setTextColor(theirColor);
  wTextPrintf(dib->getDC(), x, theirLossY, "%s %s",
     scenario->getSideName(sd.d_theirList.side()),
     text);

  /*
   * Now put our actual info
   */

  y = mapY + (dbY / baseY);

  lf.height((7*dbY) / baseY);
  lf.weight(FW_MEDIUM);
  lf.italic(false);
  lf.face(scenario->fontName(Font_Bold));

  font.set(lf);

  dib->setTextColor(black);
  dib->setFont(font);

  // situation, result
  y += ((11 * dbY) / baseY);

  if(!sd.d_ourList.isAttacker())
  {
    // TODO: move to string resouce
    text = InGameText::get(IDS_UnderSiege);
  }
  else
  {
#if 0
    // TODO: move to string resouce
    static const char* s_situationText[ActionType_HowMany] = {
        "Sortied",
        "Minor Skirmishing",
        "No Action",
        "Breached and Stormed",
        "Devasting Bombardment - Breached and Stormed"
    };

    text = (sd.d_ourList.siegeActive()) ?
      s_situationText[sd.d_ourList.actionType()] : "No Action - Inactive Siege";
#endif

     if(sd.d_ourList.siegeActive())
         text = InGameText::get(sd.d_ourList.actionType() + IDS_SIEGE_SITUATION);
      else
         text = InGameText::get(IDS_NoActionInactiveSiege);
  }

  wTextOut(dib->getDC(), x, y, cx - (x + rOffsetX), text);

  // TODO: move these strings to string resource
  enum {
      SiegeContinues,
      GarrisonSurrenders,
      GarrisonDestroyed,
      BesiegerRetreats,
      BesiegerRaids } status;

#if 0
  static const char* s_outcomeText[] = {
     "Siege Continues",
     "Garrison Surrenders",
     "Garrison Destroyed",
     "Besieger Retreats",
     "Siege Ends. Besieger now Raiding"
  };
#endif

  if(sd.d_ourList.wonSiege() ||
    sd.d_theirList.wonSiege())
//  sd.d_ourList.hasSurrendered())
  {
   if(sd.d_ourList.wonSiege())
      status = (sd.d_theirList.hasSurrendered()) ? GarrisonSurrenders : GarrisonDestroyed;
    else
      status = (sd.d_ourList.hasSurrendered()) ? GarrisonSurrenders : GarrisonDestroyed;
  }
  else if(sd.d_ourList.retreating() || sd.d_theirList.retreating())
    status = BesiegerRetreats;
  else if(sd.d_ourList.raiding() || sd.d_theirList.raiding())
    status = BesiegerRaids;
  else
    status = SiegeContinues;

  y += ((6 * dbY) / baseY);
//  wTextOut(dib->getDC(), x, y, s_outcomeText[status]);
  wTextOut(dib->getDC(), x, y, InGameText::get(status + IDS_SIEGE_OUTCOME));

  /*
   * Show participants
   */

  // our units
  y = ourUnitsY + ((8 * dbY) / baseY);

  SListIterR<CPListItem> ourIter(&sd.d_ourList);
  int unitX = x;
  const int spaceX = ((5 * dbX) / baseX);
  while(++ourIter)
  {
    const ICommandPosition& cpi = ourIter.current()->d_cpi;

    // make sure we don't run out of room
    SIZE s;
    GetTextExtentPoint32(dib->getDC(), cpi->getNameNotNull(),
       lstrlen(cpi->getNameNotNull()), &s);

    if( (unitX + s.cx + spaceX) < (cx - x) )
    {
      wTextOut(dib->getDC(), unitX, y, cpi->getNameNotNull());
      unitX += (s.cx + spaceX);
    }
    else
      break;
  }

  // their units
  y = theirUnitsY + ((8 * dbY) / baseY);
  SListIterR<CPListItem> theirIter(&sd.d_theirList);
  unitX = x;
  while(++theirIter)
  {
    const ICommandPosition& cpi = theirIter.current()->d_cpi;

    // make sure we don;t run out of room
    SIZE s;
    GetTextExtentPoint32(dib->getDC(), cpi->getNameNotNull(),
       lstrlen(cpi->getNameNotNull()), &s);

    if( (unitX + s.cx + spaceX) < (cx - x) )
    {
      wTextOut(dib->getDC(), unitX, y, cpi->getNameNotNull());
      unitX += (s.cx + spaceX);
    }
    else
      break;
  }

  /*
   * show losses
   */

  // manpower
  y = ourLossY + ((8*dbY) / baseY);

  text = InGameText::get(IDS_StartStrength);
  wTextPrintf(dib->getDC(), x, y, "%s = %ld", text, sd.d_ourList.startManpower());

  text = InGameText::get(IDS_CurrentStrength);     // TODO: move this to string resource
  wTextPrintf(dib->getDC(), x + (cx / 2), y, "%s = %ld", text, sd.d_ourList.endManpower());

  // losses
  y += ((6 * dbY) / baseY);
  text = InGameText::get(IDS_TotalLosses);     // TODO: move this to string resource
  ULONG tLosses = sd.d_ourList.startManpower() - sd.d_ourList.endManpower();

  wTextPrintf(dib->getDC(), x, y, "%s = %ld",
     text, tLosses);

  text = InGameText::get(IDS_Total);
  int lossPercent = (sd.d_ourList.startManpower() > 0) ?
    MulDiv(tLosses, 100, sd.d_ourList.startManpower()) : 0;

  y += ((6 * dbY) / baseY);
  wTextPrintf(dib->getDC(), x, y, "%s = %d%%", text, lossPercent);

  y = theirLossY + ((8 * dbY) / baseY);

  text = InGameText::get(IDS_EstimatedLosses);     // TODO: move this to string resource
  int value = random(-10, 30);
  ULONG theirLoss = maximum(0, (sd.d_theirList.startManpower() - sd.d_theirList.endManpower()));

  theirLoss = maximum(0, theirLoss + MulDiv(value, theirLoss, 100));

  UBYTE theirLossPercent = (sd.d_theirList.startManpower() > 0) ?
      MulDiv(theirLoss, 100, sd.d_theirList.startManpower()) : 0;

  wTextPrintf(dib->getDC(), x, y, "%s = %ld %s / %d%%",
      text,
      theirLoss,
      InGameText::get(IDS_Men),
      static_cast<int>(theirLossPercent));

  // clean up
  dib->setTextColor(oldColor);
  dib->setFont(oldFont);
}

#if 0
void SiegeResultUtil::calcLosses(const CampaignBattleUnitList& units, LossData& ld)
{
  ASSERT(units.entries() > 0);

  /*
   * Calculate total manpower
   */

  ld.d_startMP = ( (units.nStartInfantry() * UnitTypeConst::InfantryPerSP) +
                   (units.nStartCavalry() * UnitTypeConst::CavalryPerSP) +
                   (units.nStartArtillery() * UnitTypeConst::ArtilleryPerSP) +
                   (units.nStartOther() * UnitTypeConst::SpecialPerSP) );

  ld.d_endMP = ( (units.nInfantry() * UnitTypeConst::InfantryPerSP) +
                 (units.nCavalry() * UnitTypeConst::CavalryPerSP) +
                 (units.nArtillery() * UnitTypeConst::ArtilleryPerSP) +
                 (units.nOther() * UnitTypeConst::SpecialPerSP) );


  /*
   * Calculate total losses
   */

  UWORD startGuns = (units.nStartArtillery() * UnitTypeConst::GunsPerSP);
  UWORD endGuns = (units.nArtillery() * UnitTypeConst::GunsPerSP);
  ld.d_totalMPLoss = maximum(0, ld.d_startMP - ld.d_endMP);
  ld.d_totalGunLoss = maximum(0, startGuns - endGuns);

  /*
   * Calc loss percentages
   */

  SPCount loss = static_cast<SPCount>(maximum(0, (units.nStartInfantry() - units.nInfantry())));
  ld.d_infLoss = (units.nStartInfantry() > 0) ?
       MulDiv(loss, 100, units.nStartInfantry()) : 0;

  loss = static_cast<SPCount>(maximum(0, (units.nStartCavalry() - units.nCavalry())));
  ld.d_cavLoss = (units.nStartCavalry() > 0) ?
       MulDiv(loss, 100, units.nStartCavalry()) : 0;

  loss = static_cast<SPCount>(maximum(0, (units.nStartArtillery() - units.nArtillery())));
  ld.d_artyLoss = (units.nStartArtillery() > 0) ?
       MulDiv(loss, 100, units.nStartArtillery()) : 0;

  loss = static_cast<SPCount>(maximum(0, (units.nStartOther() - units.nOther())));
  ld.d_otherLoss = (units.nStartOther() > 0) ?
       MulDiv(loss, 100, units.nStartOther()) : 0;

  ld.d_totalLoss = (ld.d_startMP > 0) ?
       MulDiv(ld.d_totalMPLoss, 100, ld.d_startMP) : 0;
}
#endif

/*--------------------------------------------------------
 * Message from logic thread
 */

void SiegeResultMsg::run()
{
   SiegeResultUtil::run(d_data, d_event);
}

/*----------------------------------------------------------
 * Actual Campaign Battle Result window
 */

class SiegeResultWindow : public InsertWind {
    HWND d_hParent;
    const CampaignData* d_campData;
    CampaignWindowsInterface* d_campWind;
    Event* d_event;
    const SiegeInfoData* d_bd;

    enum ID {
       Close,
       ID_HowMany
    };

  public:
    SiegeResultWindow(HWND hParent, CampaignWindowsInterface* cw, const CampaignData* campData);
    ~SiegeResultWindow() { selfDestruct(); }

    void run(const SiegeInfoData& bd, Event* event);
    void drawInsertInfo(const PixelRect& r);
  private:
    LRESULT procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
    void onCommand(HWND hwnd, int id, HWND hwndCtl, UINT codeNotify);
    void onKey(HWND hwnd, UINT vk, BOOL fDown, int cRepeat, UINT flags);
};

SiegeResultWindow::SiegeResultWindow(HWND hParent, CampaignWindowsInterface* cw, const CampaignData* campData) :
  d_hParent(hParent),
  d_campData(campData),
  d_campWind(cw),
  d_event(0),
  d_bd(0)
{
  ASSERT(d_hParent);
  ASSERT(d_campData);

  // TODO: strings from resource
  static IW_ButtonData s_buttonData[] = {
    { IDS_OK, 0,  Close, 65, 114, 30, 10 },
    { InGameText::Null, 0, 0, 0, 0, 0, 0 }
  };

  IW_Data iwd;
  iwd.d_hParent = hParent;
  iwd.d_cx = (160 * ScreenBase::dbX()) / ScreenBase::baseX();
  iwd.d_cy = (130 * ScreenBase::dbY()) / ScreenBase::baseY();
  iwd.d_bBorderColors = scenario->getBorderColors();
  iwd.d_bFillDib = scenario->getScenarioBkDIB(Scenario::BackPatterns::WoodBackground);
  iwd.d_buttonData = s_buttonData;
  // set transparent color
  COLORREF c;
  scenario->getColour("MapInsert", c);
  iwd.d_insertColor = c;

  create(iwd);
}

LRESULT SiegeResultWindow::procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
  switch(msg)
  {
    HANDLE_MSG(hWnd, WM_CREATE, onCreate);
    HANDLE_MSG(hWnd, WM_PAINT, onPaint);
    HANDLE_MSG(hWnd, WM_ERASEBKGND, onEraseBk);
    HANDLE_MSG(hWnd, WM_TIMER, onTimer);
    HANDLE_MSG(hWnd, WM_COMMAND, onCommand);
    HANDLE_MSG(hWnd, WM_KEYDOWN, onKey);
    default:
      return defProc(hWnd, msg, wParam, lParam);
  }
}


void SiegeResultWindow::onCommand(HWND hwnd, int id, HWND hwndCtl, UINT codeNotify)
{
  switch(id)
  {
    case Close:
    {
      ShowWindow(hwnd, SW_HIDE);
      ASSERT(d_event);
      d_event->set();
      d_event = 0;
      d_bd = 0;
    }
    break;
  }
}


void SiegeResultWindow::onKey(HWND hwnd, UINT vk, BOOL fDown, int cRepeat, UINT flags) {

   if(fDown) {

      /*
      * Player's hit RETURN or ESCAPE to cancel the window
      */
      if(vk == VK_RETURN || vk == VK_ESCAPE) {

         PostMessage(
            hwnd,
            WM_COMMAND,
            MAKEWPARAM(Close, 0),
            (LPARAM) GetDlgItem(hwnd, Close)
         );
      }
   }
}


void SiegeResultWindow::drawInsertInfo(const PixelRect& r)
{
   SiegeResultUtil::drawInfo(*d_bd, d_campData, dib(), r.width(), r.height());
}


void SiegeResultWindow::run(const SiegeInfoData& bd, Event* event)
{
  ASSERT(d_hParent);
//  ASSERT(d_event == 0);

  // set event pointer
  d_event = event;
  d_bd = &bd;

  /*
   * First transfer bits from mainwind hdc to our local dib
   */

  // calculate top corner position
  RECT r;
  GetClientRect(getHWND(), &r);

  RECT pr;
  GetClientRect(d_hParent, &pr);

  const int cx = r.right - r.left;
  const int cy = r.bottom - r.top;
  const int x = ((pr.right - pr.left) - cx) / 2;
  const int y = ((pr.bottom - pr.top) - cy) / 2;
  PixelPoint p(x, y);

  InsertWind::show(p);
}

/*-----------------------------------------------------------
 * Container class for Campaign Battle Result window
 */

class  SR_Window {
    SiegeResultWindow* d_cbrWindow;
  public:
    SR_Window() :
      d_cbrWindow(0) {}
    ~SR_Window()
    {
      destroy();
    }

    void init(HWND hParent, CampaignWindowsInterface* cw, const CampaignData* campData)
    {
      d_cbrWindow = new SiegeResultWindow(hParent, cw, campData);
      ASSERT(d_cbrWindow);
    }

    void run(const SiegeInfoData& bd, Event& event)
    {
      d_cbrWindow->run(bd, &event);
    }

    void destroy()
    {
      if(d_cbrWindow)
      {
        // DestroyWindow(d_cbrWindow->getHWND());
          delete d_cbrWindow;
        d_cbrWindow = 0;
      }
    }

    Boolean initiated() const { return (d_cbrWindow != 0); }
};


static SR_Window s_srWindow;

/*----------------------------------------------------------
 * Internal access routines
 */

void SiegeResultUtil::run(const SiegeInfoData& bd, Event& event)
{
  ASSERT(s_srWindow.initiated());
  s_srWindow.run(bd, event);
}

/*----------------------------------------------------------
 *  Outside access routines
 */

void SiegeResult_Int::create(HWND hParent, CampaignWindowsInterface* cw, const CampaignData* campData)
{
  ASSERT(!s_srWindow.initiated());
  s_srWindow.init(hParent, cw, campData);
}

void SiegeResult_Int::destroy()
{
  ASSERT(s_srWindow.initiated());
  s_srWindow.destroy();
}


/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.3  2001/07/02 15:23:17  greenius
 * Battle, Campaign, Combined game and Campaign Editor now compile and run... mostly
 *
 * Revision 1.2  2001/06/18 08:25:45  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
