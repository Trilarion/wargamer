/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 * Campaign Battle Results Display
 *
 *----------------------------------------------------------------------
 */



#include "stdinc.hpp"
#include "cbr_wind.hpp"
#include "wind.hpp"
#include "app.hpp"
#include "scenario.hpp"
#include "dib.hpp"
#include "dib_util.hpp"
#include "cbatmsg.hpp"
#include "fonts.hpp"
#include "campdint.hpp"
#include "palette.hpp"
#include "wmisc.hpp"
#include "res_str.h"
#include "resstr.hpp"
#include "scn_img.hpp"
#include "imglib.hpp"
#include "scn_res.h"
#include "town.hpp"
#include "scrnbase.hpp"
#include "trackwin.hpp"
#ifdef DEBUG
#include "print.hpp"
#endif
#include "cleader.hpp"
#include "compos.hpp"
#include "random.hpp"

/*---------------------------------------------------------
 * Resource strings
 */

// class to hold array indeces for unit info resource strings

class BattleInfoString {
public:
  enum ID {
    Unit,

    HowMany
  };
};

// resource ids
static const int s_resourceIDs[BattleInfoString::HowMany] = {
   IDS_UI_UNIT
};

static ResourceStrings s_strings(s_resourceIDs, BattleInfoString::HowMany);


/*--------------------------------------------------------
 *   internal utilities
 */

struct LossData {
  ULONG d_startMP;           // pre-battle manpower
  ULONG d_endMP;             // after-battle manpower
  ULONG d_totalMPLoss;       // total manpower loss
  UWORD d_totalGunLoss;      // total guns lost
  UBYTE d_infLoss;           // %infantry losses
  UBYTE d_cavLoss;           // %cavalry losses
  UBYTE d_artyLoss;          // %artillery losses
  UBYTE d_otherLoss;         // %other losses
  UBYTE d_totalLoss;         // %total losses

  LossData() :
    d_startMP(0),
    d_endMP(0),
    d_totalMPLoss(0),
    d_infLoss(0),
    d_cavLoss(0),
    d_artyLoss(0),
    d_otherLoss(0),
    d_totalLoss(0) {}
};

class CBattleResultUtil {
public:
  static void run(const BattleInfoData& bd, Event& event);
  static void drawInfo(const BattleInfoData& bd, const CampaignData* campData, DrawDIBDC* dib, const int d_cx, const int d_cy);
  static void drawLeaderCasualties(const BattleInfoData& bd, const CampaignData* campData, DrawDIBDC* dib, const int d_cx, const int d_cy);
#ifdef DEBUG
  static void drawPrinterInfo(HDC hdc, const BattleInfoData& bd, const CampaignData* campData);
#endif
  static Side getOtherSide(Side s)
  {
    ASSERT(s != SIDE_Neutral);
    return (s == 0) ? 1 : 0;
  }
private:
  static void calcLosses(const CampaignBattleUnitList& units, LossData& ld);
};

// draw battle info onto dib
void CBattleResultUtil::drawInfo(const BattleInfoData& bd, const CampaignData* campData, DrawDIBDC* dib, const int cx, const int cy)
{
  ASSERT(bd.d_whichSide != SIDE_Neutral);
  ASSERT(bd.d_units);
  ASSERT(bd.d_town != NoTown);

  /*
   * Some useful values
   */

  const int dbX = ScreenBase::dbX();
  const int dbY = ScreenBase::dbY();
  const int baseX = ScreenBase::baseX();
  const int baseY = ScreenBase::baseY();
  const int shadowWidth = (3 * dbX) / baseX;
  const int shadowHeight = shadowWidth;
  const int mapX = 0;
  const int mapY = 0;
  const int headerY = mapY + (dbY / baseY);
  const int ourUnitsY = headerY + ((25 * dbY) / baseY);
  const int theirUnitsY = ourUnitsY + ((18 * dbY) / baseY);
  const int ourLossY =  theirUnitsY + ((18 * dbY) / baseY);
  const int theirLossY = ourLossY + ((34 * dbY) / baseY);
  const int leaderNameX = ((40 * dbX) / baseX);
  const CampaignBattleUnitList& ourUnits = bd.d_units[bd.d_whichSide];
  const CampaignBattleUnitList& theirUnits = bd.d_units[CBattleResultUtil::getOtherSide(bd.d_whichSide)];
  ASSERT(ourUnits.entries() > 0);
  ASSERT(theirUnits.entries() > 0);
  ASSERT(ourUnits.cinc() != NoLeader);
  ASSERT(theirUnits.cinc() != NoLeader);
  const COLORREF ourColor = scenario->getSideColour(bd.d_whichSide);
  const COLORREF theirColor = scenario->getSideColour(CBattleResultUtil::getOtherSide(bd.d_whichSide));
  const COLORREF black = PALETTERGB(0, 0, 0);
  LONG x = mapX + (dbX / baseX);
  LONG y = mapY + (dbY / baseY);

  /*
   * draw header text
   */

  LogFont lf;
  lf.height((11 * dbY) / baseY);
  lf.weight(FW_MEDIUM);
  lf.face(scenario->fontName(Font_Bold));
  lf.italic(true);
  lf.underline(true);

  Font font;
  font.set(lf);

  dib->setBkMode(TRANSPARENT);
  COLORREF oldColor = dib->setTextColor(ourColor);
  HFONT oldFont = dib->setFont(font);

  const ImageLibrary* flagIcons = scenario->getNationFlag(ourUnits.cinc()->getNation(), True);
  flagIcons->blit(dib, FI_Army, x, headerY, True);

  const char* text = InGameText::get(IDS_BattleResults);
  wTextPrintf(dib->getDC(), (x+FI_Army_CX)+((3 * dbX) / baseX), headerY, "%s %s",
      scenario->getSideName(bd.d_whichSide), text);

  /*
   * Draw report headers
   */

  lf.height((8 * dbY) / baseY);
  font.set(lf);
  dib->setFont(font);

  // Our side Units
  wTextOut(dib->getDC(), x, ourUnitsY, scenario->getSideName(ourUnits.cinc()->getSide()));

  // their side Units
  dib->setTextColor(theirColor);
  wTextOut(dib->getDC(), x, theirUnitsY, scenario->getSideName(theirUnits.cinc()->getSide()));

  // our losses
  dib->setTextColor(ourColor);
  text = InGameText::get(IDS_Losses);
  wTextPrintf(dib->getDC(), x, ourLossY, "%s %s",
     scenario->getSideName(ourUnits.cinc()->getSide()),
     text);

  // their losses
  dib->setTextColor(theirColor);
  wTextPrintf(dib->getDC(), x, theirLossY, "%s %s",
     scenario->getSideName(theirUnits.cinc()->getSide()),
     text);

  /*
   * Put Cinc names
   */

  dib->setTextColor(black);

  lf.height((7 * dbY) / baseY);
  lf.weight(FW_MEDIUM);
  lf.face(scenario->fontName(Font_Decorative));
  lf.italic(false);
  lf.underline(false);

  font.set(lf);
  dib->setFont(font);

  wTextOut(dib->getDC(), leaderNameX, ourUnitsY, ourUnits.cinc()->getNameNotNull());
  wTextOut(dib->getDC(), leaderNameX, theirUnitsY, theirUnits.cinc()->getNameNotNull());

  /*
   * Now put our actual info
   */

  y = mapY + (dbY / baseY);

  lf.height((7*dbY) / baseY);
  lf.weight(FW_MEDIUM);
  lf.face(scenario->fontName(Font_Bold));

  font.set(lf);
  dib->setFont(font);

  // situation, result
  y += ((11 * dbY) / baseY);

#if 0
  // TODO: move to string resouce
  static const char* s_situationText[PreBattlePosture::HowMany] = {
    "Defending at",
    "Defending near",
    "Attacking at",
    "Attacking near",
    "Battle at"
  };
#endif

  ASSERT(ourUnits.preBattlePosture() < PreBattlePosture::HowMany);
  const Town& t = campData->getTown(bd.d_town);
  wTextPrintf(dib->getDC(), x, y, "%s %s",
     InGameText::get(IDS_CBR_SITUATION + ourUnits.preBattlePosture()),
     t.getNameNotNull());

#if 0
  // TODO: move these strings to string resource
  static const char* s_outcomeText[BattleStatus::HowMany] = {
    "Decisive Victory",
    "Marginal Victory",
    "Decisive Loss",
    "Marginal Loss",
    "Undetermined"
  };
#endif

  BattleStatus::Status status = BattleStatus::Undecided;

  // TODO:
  //   Clip statuscount values such that:
  //   IF currentSide==victor
  //      statuscount >= 0
  //   ELSE
  //      statuscount < 0

  if(bd.d_victor == bd.d_whichSide)
  {
    if(ourUnits.statusCount() > 2)
       status = BattleStatus::WinningBig;
    else
       status = BattleStatus::WinningSmall;
  }
  else
  {
    if(ourUnits.statusCount() < -2)
       status = BattleStatus::LosingBig;
    else if(ourUnits.statusCount() < 0)
       status = BattleStatus::LosingSmall;
  }


  y += ((6 * dbY) / baseY);
//  wTextOut(dib->getDC(), x, y, s_outcomeText[status]);
  wTextOut(dib->getDC(), x, y,
   InGameText::get(IDS_CBR_OUTCOME + status));

  /*
   * Show participants
   */

  // our units
  y = ourUnitsY + ((8 * dbY) / baseY);

  CampaignBattleUnitListIterR ourIter(&ourUnits, False);
  int unitX = x;
  const int spaceX = ((5 * dbX) / baseX);
  while(++ourIter)
  {
    const CampaignBattleUnit* bu = ourIter.current();

    // make sure we don;t run out of room
    SIZE s;
    GetTextExtentPoint32(dib->getDC(), bu->getUnit()->getNameNotNull(),
       lstrlen(bu->getUnit()->getNameNotNull()), &s);

    if( (unitX + s.cx + spaceX) < (cx - x) )
    {
      wTextOut(dib->getDC(), unitX, y, bu->getUnit()->getNameNotNull());
      unitX += (s.cx + spaceX);
    }
    else
      break;
  }

  // their units
  y = theirUnitsY + ((8 * dbY) / baseY);
  CampaignBattleUnitListIterR theirIter(&theirUnits, False);
  unitX = x;
  while(++theirIter)
  {
    const CampaignBattleUnit* bu = theirIter.current();

    // make sure we don;t run out of room
    SIZE s;
    GetTextExtentPoint32(dib->getDC(), bu->getUnit()->getNameNotNull(),
       lstrlen(bu->getUnit()->getNameNotNull()), &s);

    if( (unitX + s.cx + spaceX) < (cx - x) )
    {
      wTextOut(dib->getDC(), unitX, y, bu->getUnit()->getNameNotNull());
      unitX += (s.cx + spaceX);
    }
    else
      break;
  }

  /*
   * show losses
   */

  LossData ol;
  calcLosses(ourUnits, ol);

  // manpower
  y = ourLossY + ((8*dbY) / baseY);

  text = InGameText::get(IDS_StartStrength);     // TODO: move this to string resource
  wTextPrintf(dib->getDC(), x, y, "%s = %ld", text, ol.d_startMP);

  text = InGameText::get(IDS_CurrentStrength);     // TODO: move this to string resource
  wTextPrintf(dib->getDC(), x + (cx / 2), y, "%s = %ld", text, ol.d_endMP);

  // losses
  y += ((6 * dbY) / baseY);
  text = InGameText::get(IDS_TotalLosses);     // TODO: move this to string resource
  wTextPrintf(dib->getDC(), x, y, "%s = %ld", text, ol.d_totalMPLoss);

  text = InGameText::get(IDS_GunLosses);     // TODO: move this to string resource
  wTextPrintf(dib->getDC(), x + (cx / 2), y, "%s = %ld", text, ol.d_totalGunLoss);

  // loss percentages
  y += ((6 * dbY) / baseY);
  text = InGameText::get(IDS_Inf);     // TODO: move this to string resource
  int lossX = x;
  wTextPrintf(dib->getDC(), lossX, y, "%s = %d%%", text, static_cast<int>(ol.d_infLoss));

  text = InGameText::get(IDS_Cav);
  lossX += ((35 * dbX) / baseX);
  wTextPrintf(dib->getDC(), lossX, y, "%s = %d%%", text, static_cast<int>(ol.d_cavLoss));

  text = InGameText::get(IDS_Art);
  lossX += ((35 * dbX) / baseX);
  wTextPrintf(dib->getDC(), lossX, y, "%s = %d%%", text, static_cast<int>(ol.d_artyLoss));

  text = InGameText::get(IDS_UI_OTHER);
  lossX += ((35 * dbX) / baseX);
  wTextPrintf(dib->getDC(), lossX, y, "%s = %d%%", text, static_cast<int>(ol.d_otherLoss));

  text = InGameText::get(IDS_Total);
  y += ((6 * dbY) / baseY);
  wTextPrintf(dib->getDC(), x, y, "%s = %d%%", text, static_cast<int>(ol.d_totalLoss));

  // their losses  estimated.  we need a proper test for this
  LossData tl;
  calcLosses(theirUnits, tl);

  y = theirLossY + ((8 * dbY) / baseY);

  text = InGameText::get(IDS_EstimatedLosses);

  int percentChange = random(-10, 30);
  int theirLosses = tl.d_startMP - tl.d_endMP;
  int estLoss = theirLosses + MulDiv(theirLosses, percentChange, 100);
  estLoss = maximum(estLoss, 0);
  estLoss = minimum(estLoss, tl.d_startMP);
  int theirLossPercent = MulDiv(estLoss, 100, tl.d_startMP);

  wTextPrintf(dib->getDC(), x, y, "%s = %ld %s / %d%%",
      text,
      estLoss,
      InGameText::get(IDS_Men),
      static_cast<int>(theirLossPercent));

  // clean up
  dib->setTextColor(oldColor);
  dib->setFont(oldFont);
}

// draw battle info onto dib
void CBattleResultUtil::drawLeaderCasualties(const BattleInfoData& bd, const CampaignData* campData, DrawDIBDC* dib, const int cx, const int cy)
{
  ASSERT(bd.d_whichSide != SIDE_Neutral);
  ASSERT(bd.d_units);
  ASSERT(bd.d_town != NoTown);

  /*
   * Some useful values
   */

  const int dbX = ScreenBase::dbX();
  const int dbY = ScreenBase::dbY();
  const int baseX = ScreenBase::baseX();
  const int baseY = ScreenBase::baseY();
  const int shadowWidth = (3 * dbX) / baseX;
  const int shadowHeight = shadowWidth;
  const int mapX = 0;
  const int mapY = 0;
  const int headerY = mapY + (dbY / baseY);
  const int ourLeadersY = headerY + ((15 * dbY) / baseY);
  const int theirLeadersY = ourLeadersY + ((60 * dbY) / baseY);

  const CampaignBattleUnitList& ourUnits = bd.d_units[bd.d_whichSide];
  const CampaignBattleUnitList& theirUnits = bd.d_units[CBattleResultUtil::getOtherSide(bd.d_whichSide)];
  ASSERT(ourUnits.entries() > 0);
  ASSERT(theirUnits.entries() > 0);
  ASSERT(ourUnits.cinc() != NoLeader);
  ASSERT(theirUnits.cinc() != NoLeader);
  const COLORREF ourColor = scenario->getSideColour(bd.d_whichSide);
  const COLORREF theirColor = scenario->getSideColour(CBattleResultUtil::getOtherSide(bd.d_whichSide));
  const COLORREF black = PALETTERGB(0, 0, 0);
  LONG x = mapX + (dbX / baseX);
  LONG y = mapY + (dbY / baseY);

  /*
   * draw header text
   */

  LogFont lf;
  lf.height((11*dbY)/baseY);
  lf.weight(FW_MEDIUM);
  lf.face(scenario->fontName(Font_Bold));
  lf.italic(true);
  lf.underline(true);

  Font font;
  font.set(lf);

  dib->setBkMode(TRANSPARENT);
  COLORREF oldColor = dib->setTextColor(ourColor);
  HFONT oldFont = dib->setFont(font);

  const ImageLibrary* flagIcons = scenario->getNationFlag(ourUnits.cinc()->getNation(), True);
  flagIcons->blit(dib, FI_Army, x, headerY, True);

  const char* text = InGameText::get(IDS_LeaderCasualtyReport);   // TODO: needs to go in string resource
  wTextPrintf(dib->getDC(), (x+FI_Army_CX)+((3*dbX)/baseX), headerY, "%s %s",
      scenario->getSideName(bd.d_whichSide), text);

  /*
   * Draw report headers
   */

  lf.height((8 * dbY) / baseY);
  font.set(lf);
  dib->setFont(font);

  // their side
  dib->setTextColor(theirColor);
  text = InGameText::get(IDS_CapturedLeaders);
  wTextPrintf(dib->getDC(), x, theirLeadersY, "%s(%s)",
      text, scenario->getSideName(theirUnits.cinc()->getSide()));

  /*
   * Put leader names
   */

  dib->setTextColor(black);

  lf.height((7 * dbY) / baseY);
  lf.weight(FW_MEDIUM);
  lf.face(scenario->fontName(Font_Bold));
  lf.italic(false);
  lf.underline(false);

  font.set(lf);
  dib->setFont(font);

  // our leaders
  y = ourLeadersY;
  const int spaceY = ((8 * dbY) / baseY);
  const int midX = ((x + cx) / 2) + ((20 * dbX) / baseX);
  const int textW = midX - (x + 4);

#if 0
  // todo: move this to string resource
  static const char* s_whatHappened[HitLeader::HitHow_HowMany] = {
     "Near miss",
     "Horse killed",
     "Light Wound",
     "Serious Wound",
     "Captured",
     "Killed"
  };
#endif

  const HitLeaderList& hl = ourUnits.hitLeaders();
  SListIterR<HitLeader> ourIter(&hl);
  while(++ourIter)
  {
    ICommandPosition cpi = ourIter.current()->command();
    ILeader leader = ourIter.current()->leader();
    HitLeader::HitHow hitHow = ourIter.current()->hitHow();

    wTextPrintf(dib->getDC(), x, y, textW, "%s (%s)",
       leader->getNameNotNull(), cpi->getNameNotNull());

    wTextPrintf(dib->getDC(), midX, y, textW, "- %s -",
         InGameText::get(hitHow + IDS_CBR_LEADERHIT));
//      s_whatHappened[hitHow]);

    y += spaceY;

    if(y >= theirLeadersY)
      break;
  }

  // their leaders
  y = theirLeadersY + ((10 * dbY) / baseY);

  SListIterR<HitLeader> theirIter(&theirUnits.hitLeaders());
  while(++theirIter)
  {
    ICommandPosition cpi = theirIter.current()->command();
    ILeader leader = theirIter.current()->leader();
    HitLeader::HitHow hitHow = theirIter.current()->hitHow();

    if(hitHow == HitLeader::Captured)
    {
      wTextPrintf(dib->getDC(), x, y, "%s (%s)",
        leader->getNameNotNull(), cpi->getNameNotNull());

      y += spaceY;
    }

    if(y >= cx - spaceY)
      break;
  }
}

#ifdef DEBUG
void CBattleResultUtil::drawPrinterInfo(HDC hdc, const BattleInfoData& bd, const CampaignData* campData)
{
  ASSERT(hdc);
  ASSERT(campData);
  ASSERT(bd.d_victor != SIDE_Neutral);

  /*
   * Some useful values
   */

  // printer diminsions
  const int cWidthPels = GetDeviceCaps(hdc, HORZRES);
  const int cHeightPels = GetDeviceCaps(hdc, VERTRES);

  /*
   * Retrieve the number of pixels-per-logical-inch
   * in the horizontal and vertical directions
   * for the display upon which the bitmap
   * was created.
   */

  HDC mainDC = GetDC(APP::getMainHWND());
  ASSERT(mainDC);
  float fLogPelsX1 = (float) GetDeviceCaps(mainDC, LOGPIXELSX);
  float fLogPelsY1 = (float) GetDeviceCaps(mainDC, LOGPIXELSY);
  ReleaseDC(APP::getMainHWND(), mainDC);

  /*
   * Retrieve the number of pixels-per-logical-inch
   * in the horizontal and vertical directions
   * for the printer upon which the bitmap
   * will be printed.
   */

  float fLogPelsX2 = (float) GetDeviceCaps(hdc, LOGPIXELSX);
  float fLogPelsY2 = (float) GetDeviceCaps(hdc, LOGPIXELSY);

  /*
   * Determine the scaling factors required to
   * print the bitmap and retain its original
   * proportions.
   */

  float fScaleX = (fLogPelsX1 > fLogPelsX2) ?
         fLogPelsX1 / fLogPelsX2 : fLogPelsX2 / fLogPelsX1;

  float fScaleY = (fLogPelsY1 > fLogPelsY2) ?
         fLogPelsY1 / fLogPelsY2 : fLogPelsY2 / fLogPelsY1;

  const int spacingY = 18 * fScaleY;
  const int ourUnitsY = 120 * fScaleY;
  const int theirUnitsY = 200 * fScaleY;
  const int ourLossesY = 280 * fScaleY;
  const int theirLossesY = 520 * fScaleY;
  const CampaignBattleUnitList& ourUnits = bd.d_units[bd.d_whichSide];
  const CampaignBattleUnitList& theirUnits = bd.d_units[CBattleResultUtil::getOtherSide(bd.d_whichSide)];
  ASSERT(ourUnits.entries() > 0);
  ASSERT(theirUnits.entries() > 0);
  ASSERT(ourUnits.cinc() != NoLeader);
  ASSERT(theirUnits.cinc() != NoLeader);
  const COLORREF ourColor = scenario->getSideColour(bd.d_whichSide);
  const COLORREF theirColor = scenario->getSideColour(CBattleResultUtil::getOtherSide(bd.d_whichSide));
  const COLORREF black = PALETTERGB(0, 0, 0);


  /*
   * Header
   */

  LogFont lf;
  lf.height(32 * fScaleY);
  lf.weight(FW_MEDIUM);
  lf.face(scenario->fontName(Font_Bold));
  lf.italic(true);
  lf.underline(true);

  Font font;
  font.set(lf);

  COLORREF oldColor = SetTextColor(hdc, black);
  HFONT oldFont = reinterpret_cast<HFONT>(SelectObject(hdc, font));

  const char* text = InGameText::get(IDS_BattleResults);
  SIZE s;
  GetTextExtentPoint32(hdc, text, lstrlen(text), &s);

  int x = 1 * fScaleX;
  int y = 1 * fScaleY;

  wTextOut(hdc, x, y, text);

  /*
   * Draw report headers
   */

  SetTextColor(hdc, ourColor);

  lf.height(24 * fScaleY);
  font.set(lf);
  SelectObject(hdc, font);

  // Our side Units
  wTextOut(hdc, x, ourUnitsY, scenario->getSideName(ourUnits.cinc()->getSide()));

  // their side Units
  SetTextColor(hdc, theirColor);
  wTextOut(hdc, x, theirUnitsY, scenario->getSideName(theirUnits.cinc()->getSide()));

  // our losses
  SetTextColor(hdc, ourColor);
  text = InGameText::get(IDS_Losses);
  wTextPrintf(hdc, x, ourLossesY, "%s %s",
     scenario->getSideName(ourUnits.cinc()->getSide()),
     text);

  // their losses
  SetTextColor(hdc, theirColor);
  wTextPrintf(hdc, x, theirLossesY, "%s %s",
     scenario->getSideName(theirUnits.cinc()->getSide()),
     text);

  /*
   * Put Cinc names
   */

  SetTextColor(hdc, black);

  lf.height(20 * fScaleY);
  lf.weight(FW_MEDIUM);
  lf.face(scenario->fontName(Font_Decorative));
  lf.italic(false);
  lf.underline(false);

  font.set(lf);
  SelectObject(hdc, font);

  const int leaderNameX = cWidthPels / 2;
  wTextOut(hdc, leaderNameX, ourUnitsY, ourUnits.cinc()->getNameNotNull());
  wTextOut(hdc, leaderNameX, theirUnitsY, theirUnits.cinc()->getNameNotNull());

  /*
   * Now put our actual info
   */

  x += (40 * fScaleX);
  lf.height(16 * fScaleY);
  lf.weight(FW_MEDIUM);
  lf.face(scenario->fontName(Font_Bold));

  font.set(lf);
  SelectObject(hdc, font);

#if 0
  // situation, result
  static const char* s_situationText[PreBattlePosture::HowMany] = {
    "Defending at",
    "Defending near",
    "Attacking at",
    "Attacking near",
    "Battle at"
  };
#endif

  y = 38 * fScaleY;

  for(Side side = 0; side < scenario->getNumSides(); side++)
  {
    ASSERT(bd.d_units[side].preBattlePosture() < PreBattlePosture::HowMany);
    const Town& t = campData->getTown(bd.d_town);
    wTextPrintf(hdc, x, y, "%s %s %s",
       scenario->getSideName(side),
       // s_situationText[bd.d_units[side].preBattlePosture()],
       InGameText::get(bd.d_units[side].preBattlePosture() + IDS_CBR_SITUATION),
       t.getNameNotNull());

    y += spacingY;
  }
#if 0
  static const char* s_outcomeText[BattleStatus::HowMany] = {
    "Decisive Victory",
    "Marginal Victory",
    "Decisive Loss",
    "Marginal Loss",
    "Undetermined"
  };
#endif

  for(side = 0; side < scenario->getNumSides(); side++)
  {
    BattleStatus::Status status = BattleStatus::Undecided;

    if(bd.d_units[side].statusCount() < -2)
      status = BattleStatus::LosingBig;
    else if(bd.d_units[side].statusCount() < 0)
      status = BattleStatus::LosingSmall;
    else if(bd.d_units[side].statusCount() > 2)
      status = BattleStatus::WinningBig;
    else
      status = BattleStatus::WinningSmall;

    wTextPrintf(hdc, x, y, "%s %s",
//        scenario->getSideName(side), s_outcomeText[status]);
       scenario->getSideName(side),
       InGameText::get(status + IDS_CBR_OUTCOME));

    y += spacingY;
  }

  // our units
  y = ourUnitsY + (24 * fScaleY);

  CampaignBattleUnitListIterR ourIter(&ourUnits, False);
  int unitX = x;
  const int spaceX = (40 * fScaleX);
  while(++ourIter)
  {
    const CampaignBattleUnit* bu = ourIter.current();

    // make sure we don;t run out of room
    SIZE s;
    GetTextExtentPoint32(hdc, bu->getUnit()->getNameNotNull(),
       lstrlen(bu->getUnit()->getNameNotNull()), &s);

    if( (unitX + s.cx + spaceX) < (cWidthPels - x) )
    {
      wTextOut(hdc, unitX, y, bu->getUnit()->getNameNotNull());
      unitX += (s.cx + spaceX);
    }
    else
      break;
  }

  // their units
  y = theirUnitsY + (24 * fScaleY);
  CampaignBattleUnitListIterR theirIter(&theirUnits, False);
  unitX = x;
  while(++theirIter)
  {
    const CampaignBattleUnit* bu = theirIter.current();

    // make sure we don;t run out of room
    SIZE s;
    GetTextExtentPoint32(hdc, bu->getUnit()->getNameNotNull(),
       lstrlen(bu->getUnit()->getNameNotNull()), &s);

    if( (unitX + s.cx + spaceX) < (cWidthPels - x) )
    {
      wTextOut(hdc, unitX, y, bu->getUnit()->getNameNotNull());
      unitX += (s.cx + spaceX);
    }
    else
      break;
  }

  /*
   * show losses
   */

  for(side = 0; side < scenario->getNumSides(); side++)
  {

    const int spaceX = (100 * fScaleX);
    LossData ol;
    calcLosses(bd.d_units[side], ol);

    // manpower
    y = (side == bd.d_whichSide) ? ourLossesY + (24 * fScaleY) :
       theirLossesY + (24 * fScaleY);

    text = InGameText::get(IDS_StartStrength);     // TODO: move this to string resource
    wTextPrintf(hdc, x, y, "%s = %ld", text, ol.d_startMP);

    text = InGameText::get(IDS_CurrentStrength);     // TODO: move this to string resource
    wTextPrintf(hdc, (cWidthPels / 2), y, "%s = %ld", text, ol.d_endMP);

    // losses
    y += spacingY;
    text = InGameText::get(IDS_TotalLosses);     // TODO: move this to string resource
    wTextPrintf(hdc, x, y, "%s = %ld", text, ol.d_totalMPLoss);

    text = InGameText::get(IDS_GunLosses);     // TODO: move this to string resource
    wTextPrintf(hdc, (cWidthPels / 2), y, "%s = %ld", text, ol.d_totalGunLoss);

    // morale
    y += spacingY;
    text = "Starting Morale";
    wTextPrintf(hdc, x, y, "%s = %d", text,
        static_cast<int>(bd.d_units[side].startMorale()));

    text = "Current Morale";
    wTextPrintf(hdc, (cWidthPels / 2), y, "%s = %d", text,
        static_cast<int>(bd.d_units[side].currentMorale()));

    y += spacingY;
    text = "Starting SP Count =";
    SPCount startCount = ( (bd.d_units[side].nStartInfantry()) +
                           (bd.d_units[side].nStartCavalry()) +
                           (bd.d_units[side].nStartArtillery()) +
                           (bd.d_units[side].nStartOther()) );

    wTextPrintf(hdc, x, y, "%s %d", text,
        static_cast<int>(startCount));

    text = "Current SP Count =";
    wTextPrintf(hdc, (cWidthPels / 2), y, "%s %d", text,
        static_cast<int>(bd.d_units[side].spCount()));


    /*
     * SP loss by type
     */

    int lossX = x;
    y += (2 * spacingY);

    text = "Start Counts --";     // TODO: move this to string resource
    lossX = x;
    wTextOut(hdc, lossX, y, text);

    text = "Inf =";     // TODO: move this to string resource
    lossX += (spaceX * 2);
    wTextPrintf(hdc, lossX, y, "%s %d%", text, static_cast<int>(bd.d_units[side].nStartInfantry()));

    text = "Cav =";
    lossX += spaceX;
    wTextPrintf(hdc, lossX, y, "%s %d%", text, static_cast<int>(bd.d_units[side].nStartCavalry()));

    text = "Art =";
    lossX += spaceX;
    wTextPrintf(hdc, lossX, y, "%s %d%", text, static_cast<int>(bd.d_units[side].nStartArtillery()));

    text = "Other =";
    lossX += spaceX;
    wTextPrintf(hdc, lossX, y, "%s %d%", text, static_cast<int>(bd.d_units[side].nStartOther()));

    lossX = x;
    y += spacingY;

    // current counts
    text = "Current Counts --";     // TODO: move this to string resource
    lossX = x;
    wTextOut(hdc, lossX, y, text);

    text = "Inf =";     // TODO: move this to string resource
    lossX += (2 * spaceX);
    wTextPrintf(hdc, lossX, y, "%s %d%", text, static_cast<int>(bd.d_units[side].nInfantry()));

    text = "Cav =";
    lossX += spaceX;
    wTextPrintf(hdc, lossX, y, "%s %d%", text, static_cast<int>(bd.d_units[side].nCavalry()));

    text = "Art =";
    lossX += spaceX;
    wTextPrintf(hdc, lossX, y, "%s %d%", text, static_cast<int>(bd.d_units[side].nArtillery()));

    text = "Other =";
    lossX += spaceX;
    wTextPrintf(hdc, lossX, y, "%s %d%", text, static_cast<int>(bd.d_units[side].nOther()));

    /*
     * loss percentages
     */

    lossX = x;
    y += (2 * spacingY);

    text = "Loss Rates --";     // TODO: move this to string resource
    lossX = x;
    wTextOut(hdc, lossX, y, text);

    text = "Inf =";     // TODO: move this to string resource
    lossX += (2 * spaceX);
    wTextPrintf(hdc, lossX, y, "%s %d%%", text, static_cast<int>(ol.d_infLoss));

    text = "Cav =";
    lossX += spaceX;
    wTextPrintf(hdc, lossX, y, "%s %d%%", text, static_cast<int>(ol.d_cavLoss));

    text = "Art =";
    lossX += spaceX;
    wTextPrintf(hdc, lossX, y, "%s %d%%", text, static_cast<int>(ol.d_artyLoss));

    text = "Other =";
    lossX += spaceX;
    wTextPrintf(hdc, lossX, y, "%s %d%%", text, static_cast<int>(ol.d_otherLoss));

    /*
     * loss percentages of total
     */

    lossX = x;
    y += (spacingY);

    text = "Loss Rates (of Total) --";     // TODO: move this to string resource
    lossX = x;
    wTextOut(hdc, lossX, y, text);

    text = "Inf =";     // TODO: move this to string resource
    lossX += (2 * spaceX);
    int nTotalLosses = (startCount - bd.d_units[side].spCount());
    int nLosses = (bd.d_units[side].nStartInfantry() - bd.d_units[side].nInfantry());
    int value = (nTotalLosses > 0) ? MulDiv(nLosses, 100, nTotalLosses) : 0;
    wTextPrintf(hdc, lossX, y, "%s %d%%", text, value);

    text = "Cav =";
    lossX += spaceX;
    nLosses = (bd.d_units[side].nStartCavalry() - bd.d_units[side].nCavalry());
    value = (nTotalLosses > 0) ? MulDiv(nLosses, 100, nTotalLosses) : 0;
    wTextPrintf(hdc, lossX, y, "%s %d%%", text, value);

    text = "Art =";
    lossX += spaceX;
    nLosses = (bd.d_units[side].nStartArtillery() - bd.d_units[side].nArtillery());
    value = (nTotalLosses > 0) ? MulDiv(nLosses, 100, nTotalLosses) : 0;
    wTextPrintf(hdc, lossX, y, "%s %d%%", text, value);

    text = "Other =";
    lossX += spaceX;
    nLosses = (bd.d_units[side].nStartOther() - bd.d_units[side].nOther());
    value = (nTotalLosses > 0) ? MulDiv(nLosses, 100, nTotalLosses) : 0;
    wTextPrintf(hdc, lossX, y, "%s %d%%", text, value);

    text = "Total Loss Rate =";
    y += (spacingY);
    wTextPrintf(hdc, x, y, "%s %d%%", text, static_cast<int>(ol.d_totalLoss));
  }

  /*
   * Clean up
   */

  SetTextColor(hdc, oldColor);
  SelectObject(hdc, oldFont);

}
#endif

void CBattleResultUtil::calcLosses(const CampaignBattleUnitList& units, LossData& ld)
{
  ASSERT(units.entries() > 0);

  /*
   * Calculate total manpower
   */

  ld.d_startMP = ( (units.nStartInfantry() * UnitTypeConst::InfantryPerSP) +
                   (units.nStartCavalry() * UnitTypeConst::CavalryPerSP) +
                   (units.nStartArtillery() * UnitTypeConst::ArtilleryPerSP) +
                   (units.nStartOther() * UnitTypeConst::SpecialPerSP) );

  ld.d_endMP = ( (units.nInfantry() * UnitTypeConst::InfantryPerSP) +
                 (units.nCavalry() * UnitTypeConst::CavalryPerSP) +
                 (units.nArtillery() * UnitTypeConst::ArtilleryPerSP) +
                 (units.nOther() * UnitTypeConst::SpecialPerSP) );


  /*
   * Calculate total losses
   */

  UWORD startGuns = (units.nStartArtillery() * UnitTypeConst::GunsPerSP);
  UWORD endGuns = (units.nArtillery() * UnitTypeConst::GunsPerSP);
  ld.d_totalMPLoss = maximum(0, ld.d_startMP - ld.d_endMP);
  ld.d_totalGunLoss = maximum(0, startGuns - endGuns);

  /*
   * Calc loss percentages
   */

  SPCount loss = static_cast<SPCount>(max(0, (units.nStartInfantry() - units.nInfantry())));
  ld.d_infLoss = (units.nStartInfantry() > 0) ?
       MulDiv(loss, 100, units.nStartInfantry()) : 0;

  loss = static_cast<SPCount>(max(0, (units.nStartCavalry() - units.nCavalry())));
  ld.d_cavLoss = (units.nStartCavalry() > 0) ?
       MulDiv(loss, 100, units.nStartCavalry()) : 0;

  loss = static_cast<SPCount>(max(0, (units.nStartArtillery() - units.nArtillery())));
  ld.d_artyLoss = (units.nStartArtillery() > 0) ?
       MulDiv(loss, 100, units.nStartArtillery()) : 0;

  loss = static_cast<SPCount>(max(0, (units.nStartOther() - units.nOther())));
  ld.d_otherLoss = (units.nStartOther() > 0) ?
       MulDiv(loss, 100, units.nStartOther()) : 0;

  ld.d_totalLoss = (ld.d_startMP > 0) ?
       MulDiv(ld.d_totalMPLoss, 100, ld.d_startMP) : 0;
}

/*--------------------------------------------------------
 * Message from logic thread
 */

void BattleResultMsg::run()
{
   CBattleResultUtil::run(d_data, d_event);
}


/*----------------------------------------------------------
 * Actual Campaign Battle Result window
 */

class CBattleResultWindow : public InsertWind {
    HWND d_hParent;
    const CampaignData* d_campData;
    Event* d_event;
    const BattleInfoData* d_bd;

    enum Page {
      Page_First = 0,
      BattleReport = Page_First,
      LeaderCasualtyReport,

      Page_Max
    } d_page;

    enum ID {
       Close,
#ifdef DEBUG
       Print,
#endif
       ID_HowMany
    };

  public:
    CBattleResultWindow(HWND hParent, const CampaignData* campData);
    ~CBattleResultWindow()
    {
        selfDestruct();
    }

    void run(const BattleInfoData& bd, Event* event);
    void drawInsertInfo(const PixelRect& r);
  private:
    LRESULT procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
    void onCommand(HWND hwnd, int id, HWND hwndCtl, UINT codeNotify);
    void onKey(HWND hwnd, UINT vk, BOOL fDown, int cRepeat, UINT flags);

    void print();
};

CBattleResultWindow::CBattleResultWindow(HWND hParent, const CampaignData* campData) :
  d_hParent(hParent),
  d_campData(campData),
  d_event(0),
  d_bd(0),
  d_page(Page_First)
{
  ASSERT(d_hParent);
  ASSERT(d_campData);

  // TODO: strings from resource
  static IW_ButtonData s_buttonData[] = {
    { IDS_OK,       0, Close, 65, 114, 30, 10 },
#ifdef DEBUG
    { InGameText::Null, "Print", Print, 100, 114, 30, 10 },
#endif
    { InGameText::Null, 0, 0, 0, 0, 0, 0 }
  };

  IW_Data iwd;
  iwd.d_hParent = hParent;
  iwd.d_cx = (160 * ScreenBase::dbX()) / ScreenBase::baseX();
  iwd.d_cy = (130 * ScreenBase::dbY()) / ScreenBase::baseY();
  iwd.d_bBorderColors = scenario->getBorderColors();
  iwd.d_bFillDib = scenario->getScenarioBkDIB(Scenario::BackPatterns::ButtonBackground);
  iwd.d_buttonData = s_buttonData;
  // set transparent color
  COLORREF c;
  scenario->getColour("MapInsert", c);
  iwd.d_insertColor = c;

  create(iwd);
}

LRESULT CBattleResultWindow::procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
//  LRESULT result;

  switch(msg)
  {
    HANDLE_MSG(hWnd, WM_CREATE, onCreate);
    HANDLE_MSG(hWnd, WM_PAINT, onPaint);
    HANDLE_MSG(hWnd, WM_ERASEBKGND, onEraseBk);
    HANDLE_MSG(hWnd, WM_TIMER, onTimer);
    HANDLE_MSG(hWnd, WM_COMMAND, onCommand);
    HANDLE_MSG(hWnd, WM_KEYDOWN, onKey);
    default:
      return defProc(hWnd, msg, wParam, lParam);
  }
}


void CBattleResultWindow::onCommand(HWND hwnd, int id, HWND hwndCtl, UINT codeNotify)
{
  switch(id)
  {
    case Close:
    {
      INCREMENT(d_page);
      // don't show leader hit page, if no leaders are hit
      const CampaignBattleUnitList& ourUnits = d_bd->d_units[d_bd->d_whichSide];
      const CampaignBattleUnitList& theirUnits = d_bd->d_units[CBattleResultUtil::getOtherSide(d_bd->d_whichSide)];
      if(ourUnits.hitLeaders().entries() == 0 && theirUnits.hitLeaders().entries() == 0)
         d_page = Page_Max;

      if(d_page >= Page_Max)
      {
        d_page = Page_First;
        ShowWindow(hwnd, SW_HIDE);
        ASSERT(d_event);
        d_event->set();
        d_event = 0;
        d_bd = 0;
      }
      else
      {
        update();
      }
      break;
    }

#ifdef DEBUG
    case Print:
      print();
      break;
#endif
  }
}


void CBattleResultWindow::onKey(HWND hwnd, UINT vk, BOOL fDown, int cRepeat, UINT flags) {

   if(fDown) {

      /*
      * Player's hit RETURN or ESCAPE to cancel the window
      */
      if(vk == VK_RETURN || vk == VK_ESCAPE) {

         PostMessage(
            hwnd,
            WM_COMMAND,
            MAKEWPARAM(Close, 0),
            (LPARAM) GetDlgItem(hwnd, Close)
         );
      }
   }
}


void CBattleResultWindow::drawInsertInfo(const PixelRect& r)
{
  if(d_page == BattleReport)
    CBattleResultUtil::drawInfo(*d_bd, d_campData, dib(), r.width(), r.height());
  else if(d_page == LeaderCasualtyReport)
    CBattleResultUtil::drawLeaderCasualties(*d_bd, d_campData, dib(), r.width(), r.height());
}

#ifdef DEBUG
void CBattleResultWindow::print()
{
  ASSERT(d_bd);
  ASSERT(d_bd->d_town != NoTown);

  HDC printDC = 0;
  if(Print::print(getHWND(), printDC, 1))
  {
    ASSERT(printDC);

    /*
     * Start Print Job
     */

    const Town& t = d_campData->getTown(d_bd->d_town);
    DOCINFO di;
    memset(&di, 0, sizeof(DOCINFO));
    di.cbSize = sizeof(DOCINFO);
    static char buf[200];
    wsprintf(buf, "Battle at %s", t.getNameNotNull());
    di.lpszDocName = buf;

#if 1
    ASSERT(printDC);
    int result = StartDoc(printDC, &di);
    ASSERT(result);

    // start page
    result = StartPage(printDC);
    ASSERT(result > 0);
#endif

    CBattleResultUtil::drawPrinterInfo(printDC, *d_bd, d_campData);

#if 1
    // end page
    result = EndPage(printDC);
    ASSERT(result > 0);

    EndDoc(printDC);
#endif

    DeleteDC(printDC);

  }
}
#endif

void CBattleResultWindow::run(const BattleInfoData& bd, Event* event)
{
  ASSERT(d_hParent);
  ASSERT(d_event == 0);

  // set event pointer
  d_event = event;
  d_bd = &bd;

  /*
   * First transfer bits from mainwind hdc to our local dib
   */

  // calculate top corner position
  RECT r;
  GetClientRect(getHWND(), &r);

  RECT pr;
  GetClientRect(d_hParent, &pr);

  const int cx = r.right - r.left;
  const int cy = r.bottom - r.top;
  const int x = ((pr.right - pr.left) - cx) / 2;
  const int y = ((pr.bottom - pr.top) - cy) / 2;
  PixelPoint p(x, y);

  InsertWind::show(p);
}

/*-----------------------------------------------------------
 * Container class for Campaign Battle Result window
 */

class  CBR_Window {
    CBattleResultWindow* d_cbrWindow;
  public:
    CBR_Window() : d_cbrWindow(0) {}

    ~CBR_Window()
    {
      destroy();
    }

    void init(HWND hParent, const CampaignData* campData)
    {
      d_cbrWindow = new CBattleResultWindow(hParent, campData);
      ASSERT(d_cbrWindow);
    }

    void run(const BattleInfoData& bd, Event& event)
    {
      d_cbrWindow->run(bd, &event);
    }

    void destroy()
    {
      if(d_cbrWindow)
      {
          delete d_cbrWindow;
        d_cbrWindow = 0;
      }
    }

    Boolean initiated() const { return (d_cbrWindow != 0); }
};


static CBR_Window s_cbrWindow;

/*----------------------------------------------------------
 * Internal access routines
 */

void CBattleResultUtil::run(const BattleInfoData& bd, Event& event)
{
  ASSERT(s_cbrWindow.initiated());
  s_cbrWindow.run(bd, event);
}

/*----------------------------------------------------------
 *  Outside access routines
 */

void CBattleResultInterface::create(HWND hParent, const CampaignData* campData)
{
  ASSERT(!s_cbrWindow.initiated());
  s_cbrWindow.init(hParent, campData);
}

void CBattleResultInterface::destroy()
{
  ASSERT(s_cbrWindow.initiated());
  s_cbrWindow.destroy();
}


/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.3  2001/07/02 15:23:17  greenius
 * Battle, Campaign, Combined game and Campaign Editor now compile and run... mostly
 *
 * Revision 1.2  2001/06/18 08:25:44  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
