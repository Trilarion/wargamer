/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef OPTWIZ_HPP
#define OPTWIZ_HPP

#ifndef __cplusplus
#error optwiz.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *	Options Wizard for Front End
 *
 *----------------------------------------------------------------------
 */

class OptionManager;

namespace OptionsWizard {
	 void runWizard(HWND parent);//, OptionManager* optManager);
};	// namespace OptionsWizard


#endif /* OPTWIZ_HPP */

