/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *  Player Options\Settings Wizard for FrontEnd
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "optwiz.hpp"
#include "dialog.hpp"
#include "plyrdlg.hpp"
#include "app.hpp"
#include "resdef.h"
// #include "options.hpp"
#include "optdial.hpp"
#include "misc.hpp"

namespace OptionsWizard {

   enum WizardPages {
      SettingsPage,
      OptionsFirstPage,
      OptionsLastPage = OptionsFirstPage + OptionsDialog::LastPage,

      WizardPages_HowMany,

      FirstPage = 0,
      DefaultPage = 0
    };

void OptionsWizard::runWizard(HWND hwnd)
{
    PlayerSettingsDial* playerSettingsPage = new PlayerSettingsDial;
    ASSERT(playerSettingsPage != 0);

    PROPSHEETPAGE psp[WizardPages_HowMany]; //[WizardPages_HowMany];
    PROPSHEETHEADER psh;

    psp[SettingsPage].dwSize = sizeof(PROPSHEETPAGE);
    psp[SettingsPage].dwFlags = PSP_HASHELP;
    psp[SettingsPage].hInstance = APP::instance();
    psp[SettingsPage].pszTemplate = optionsScreenSettingsDlg;
    psp[SettingsPage].pfnDlgProc = ModelessDialog::basePropsheetProc;
    psp[SettingsPage].lParam = (LPARAM)playerSettingsPage;
    psp[SettingsPage].pfnCallback = NULL;

    PlayerOptionsDial* playerOptions = new PlayerOptionsDial();
    ASSERT(playerOptions != 0);

    for(WizardPages i = OptionsFirstPage; i <= OptionsLastPage; INCREMENT(i))
    {
      OptionPage p = static_cast<OptionPage>(i - OptionsFirstPage);

      psp[i].dwSize = sizeof(PROPSHEETPAGE);
      psp[i].dwFlags = PSP_HASHELP | PSP_DLGINDIRECT;
      psp[i].hInstance = APP::instance();
      // psp[i].pszTemplate = playerOptions->getPageResName(p);
      psp[i].pResource = playerOptions->getPageTemplate(p);
      psp[i].pfnDlgProc = ModelessDialog::basePropsheetProc;
      psp[i].lParam = (LPARAM)playerOptions->getPage(p);
      psp[i].pfnCallback = NULL;
    }

    psh.dwSize = sizeof(PROPSHEETHEADER);
    psh.dwFlags = PSH_WIZARD | PSH_PROPSHEETPAGE; // | PSH_USECALLBACK;
    psh.hwndParent = hwnd;
    psh.hInstance = APP::instance();
    psh.pszCaption = (LPSTR) "Game Settings";
    psh.nPages = WizardPages_HowMany; //sizeof(psp) / sizeof(PROPSHEETPAGE);
    psh.nStartPage = 0;
    psh.ppsp = (LPCPROPSHEETPAGE) psp;
    psh.pfnCallback = NULL; //(PFNPROPSHEETCALLBACK)propSheetProc;

    PropertySheet(&psh);

}

}; // namespace optionsSizard

/*
 *----------------------------------------------------------------------
 * $Log$
 *----------------------------------------------------------------------
 */
