/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef TO_WIN_HPP
#define TO_WIN_HPP

#include "camptime.hpp"
#include "wind.hpp"

class CampaignWindowsInterface;
class PixelPoint;
class CalenderWindow;
class CampaignOrder;

/*
 * Timed-Order Window
 */

class TimedOrderWindow : public Window
{
  CalenderWindow* d_calender;
public:
  TimedOrderWindow() : d_calender(0) {}
  ~TimedOrderWindow();

  void create(HWND hParent, int id, CampaignOrder& order, const Date& date);
  void init(const Date& startDate);
  void show(const PixelPoint& p);
  // void hide();
  // void destroy();
  // Boolean isShowing();
  // HWND hwnd();

	    virtual HWND getHWND() const;
        virtual bool isVisible() const;
        virtual bool isEnabled() const;
        virtual void show(bool visible);
        virtual void enable(bool enable);


};


#endif
