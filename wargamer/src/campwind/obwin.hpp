/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef OBWIN_H
#define OBWIN_H

#ifndef __cplusplus
#error obwin.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1995, Steven Morle-Green, All Rights Reserved
 *----------------------------------------------------------------------
 *
 *	Order of Battle Window
 *
 *----------------------------------------------------------------------
 *
 * Would be good idea to insulate this from other components
 *
 *----------------------------------------------------------------------
 */

#include "wind.hpp"
#include "unitlist.hpp"
#include "gamedefs.hpp"
#include "palwind.hpp"
#include "mw_user.hpp"			// For MapSelect
#include "cwdll.h"

class CampaignWindowsInterface;

class OrderTB;


#define OBW_MENU   0
#define OBW_DIALOG 1

class DrawDIB;

class OBTreeView {
		ICommandPosition cpi;
		ITown iTown;
		Side side;
		HIMAGELIST imgList;
		HTREEITEM hTreeItem;
		Boolean independentLeaders;
	public:
		OBTreeView();
		OBTreeView(ICommandPosition from);
		virtual ~OBTreeView() { }

		LRESULT onNotify(HWND hWnd, int id, NMHDR* lpNMHDR);
		Side getSide() {return side;}
		void setCPI(ICommandPosition from) {cpi = from;}
		ICommandPosition getCPI() {return cpi;}

		HWND createTV(HWND hwnd, int id);
		void createImageList(HWND hwnd);
		HTREEITEM getHTreeItem() { return hTreeItem; }

		HIMAGELIST getImgList() {return imgList; }
		void setIndependentLeaders(Boolean flag) {independentLeaders = flag;}
		Boolean getIndependentLeaders() {return independentLeaders; }

		void addItems(HWND hwndTV);
		void addDetachItems(HWND hwndTV);
		void addUnit(HWND hwndTV, TV_INSERTSTRUCT& tvi, ICommandPosition cpi, HTREEITEM hti);

		void setITown(ITown set) {iTown = set;}
		ITown getITown() {return iTown;}
#if defined(CUSTOMIZE)
		void addTownItems(HWND hwndTV);
		void addTown(HWND hwndTV, TV_INSERTSTRUCT& tvi, HTREEITEM hti);
#endif

		virtual void orderUnit(unsigned int id, unsigned int code) = 0;
};


class OBWindow :
	public WindowBaseND,
	public PaletteWindow,
	public OBTreeView
{
	static const char s_className[];
	static ATOM classAtom;

	CampaignWindowsInterface* d_campWind;

	HDC hdc;
	BOOL createdFlag; // flag for onSize function

	StackedUnitList unitList;
	MapSelect info;

	// used for detach window created from order dialog
	int obWinType;

//	OrderTB* unitDial;
   HWND hwndParent;

public:
	OBWindow(CampaignWindowsInterface* cwind, HWND parent);
	OBWindow(CampaignWindowsInterface* cwind, HWND parent, OrderTB* get, ICommandPosition from);
	~OBWindow() { selfDestruct(); }

    virtual const char* className() const { return s_className; }

private:
	static ATOM registerClass();
	LRESULT procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
	// static LPCSTR getClassName() { return className; }

	void onDestroy(HWND hWnd);
	// void onPaint(HWND hWnd);
	BOOL onCreate(HWND hWnd, CREATESTRUCT* lpCreateStruct);
	void onCommand(HWND hWnd, int id, HWND hwndCtl, UINT codeNotify);
	void onSize(HWND hwnd, UINT state, int cx, int cy);
	void detachUnit(HWND hWnd);
	void orderUnit(unsigned int id, unsigned int code);

#ifdef DEBUG
	void checkPalette() { }
#endif
};


#endif /* OBWIN_H */

