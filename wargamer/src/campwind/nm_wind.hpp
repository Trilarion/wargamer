/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef NM_WIND_HPP
#define NM_WIND_HPP

#include "wind.hpp"

/*---------------------------------------------------------------
 *  Nation Status window. Includes:
 *    1. morale-level for each major nationality,
 *    2. each sides victory point level
 */

class NS_Wind;
class CampaignWindowsInterface;
class CampaignData;
class PixelPoint;

class NationStatus_Int : public Window 
{
	 NS_Wind* d_nsWind;
  public:
	 NationStatus_Int(HWND hParent, CampaignWindowsInterface* cw, const CampaignData* campData);
	 ~NationStatus_Int();

	 void position(const PixelPoint& p);
	 // void show();
	 // void hide();
	 // void destroy();
	 void update();
	 int height() const;

	 // void suspend(bool visible);

	virtual HWND getHWND() const;
    virtual bool isVisible() const;
    virtual bool isEnabled() const;
    virtual void show(bool visible);
    virtual void enable(bool enable);

};

/*------------------------------------------------------------
 *  Game Status window. (what used to be StatusWindow in statwind.hpp/cpp) Includes:
 *     1. Message Section
 *     2. Hint-Line Section
 *     3. Game Clock
 */

class GS_Wind;
class CampaignTimeControl;
class CampaignTimeData;

class GameStatus_Int : public Window
{
	 GS_Wind* d_gsWind;
  public:
	 GameStatus_Int(HWND hParent, CampaignWindowsInterface* cw, const CampaignData* campData,
		 CampaignTimeControl* control, const CampaignTimeData* date);
	 ~GameStatus_Int();

	 void position(const PixelPoint& p);
	 // void show();
	 // void hide();
	 // void destroy();
	 void updateMessageWindow();
	 void updateClock();
	 int height() const;
	 // void suspend(bool visible);

	virtual HWND getHWND() const;
    virtual bool isVisible() const;
    virtual bool isEnabled() const;
    virtual void show(bool visible);
    virtual void enable(bool enable);

};

#endif
