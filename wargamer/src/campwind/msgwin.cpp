/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1995, Steven Morle-Green, All Rights Reserved
 *----------------------------------------------------------------------
 *
 *      Message Window
 *
 * Adjusted so that threads that append messages work by posting
 * messages.  This means we don't need to fiddle about with critical
 * sections and things.
 *
 *----------------------------------------------------------------------
 */



#include "stdinc.hpp"
#include "msgwin.hpp"
#include "gmsgwin.hpp"
#include "cmsgutil.hpp"
#include "camptime.hpp"
#include "critical.hpp"         // for SharedData
#include "app.hpp"
#include "cwin_int.hpp"
#include "palette.hpp"
#include "wmisc.hpp"
#include "sounds.hpp"
#include "armies.hpp"
#include "town.hpp"
#include "scenario.hpp"
#include "optdial.hpp"
#include "dib.hpp"
#include "campdint.hpp"                 // campaignData
#include "camp_snd.hpp"
#include "fonts.hpp"
#include "simpstr.hpp"
#include "fsalloc.hpp"

#ifdef DEBUG
// #define DEBUG_MESSAGE_WINDOW
#include "clog.hpp"
#include "logwin.hpp"
#include "msgenum.hpp"
LogFileFlush msgLog("msgwind.log");
#endif


/*------------------------------------------------------------------
 * Message's stored in window
 */


struct CampaignMessageItem : public MessageItem
{

        public:
                static CampaignMessageItem* make(const CampaignMessageInfo& info, const Date& date)
                {
                        return new CampaignMessageItem(info, date);
                }


                static void destroy(CampaignMessageItem* msg) { delete msg; }



                CampaignMessages::CMSG_ID messageID() const { return d_info.id(); }
                ICommandPosition cp() const { return d_info.cp(); }
                ILeader leader() const { return d_info.leader(); }
                ITown town() const { return d_info.where(); }
                const Date& date() const { return d_date; }
                CampaignSoundTypeEnum sound() const { return d_info.sound(); }
                SimpleString text(const CampaignData* campData) const
                {
                        SimpleString text;
                        CampaignMessageFormat::messageToText(text, d_info, campData);
                        return text;
                }

                void* operator new(size_t size);
#ifdef _MSC_VER
                void operator delete(void* deadObject);
#else
                void operator delete(void* deadObject, size_t size);
#endif
                enum { ChunkSize = 25 };

        private:
                CampaignMessageItem(const CampaignMessageInfo& info, const Date& date) :
                        d_info(info),
                        d_date(date)
                {
                        if(d_info.cp() != NoCommandPosition)
                        {
                                if(d_info.leader() == NoLeader)
                                        d_info.leader(d_info.cp()->getLeader());
                        }
                }

                ~CampaignMessageItem() { }

                CampaignMessageInfo d_info;
                Date d_date;
};

#ifdef DEBUG
static FixedSizeAlloc<CampaignMessageItem> campaignMessageItemAlloc("CampaignMessageItem");
#else
static FixedSizeAlloc<CampaignMessageItem> campaignMessageItemAlloc;
#endif

void* CampaignMessageItem::operator new(size_t size)
{
  return campaignMessageItemAlloc.alloc(size);
}

#ifdef _MSC_VER
void CampaignMessageItem::operator delete(void* deadObject)
{
  campaignMessageItemAlloc.release(deadObject);
}
#else
void CampaignMessageItem::operator delete(void* deadObject, size_t size)
{
  campaignMessageItemAlloc.release(deadObject, size);
}
#endif


/*---------------------------------------------------------------
 * Message Window
 */



class RealMessageWindow :
        public TMessageWindow<CampaignMessageItem>
        // public GenericMessageWindow
{
                // typedef MsgWinList<CampaignMessageItem> CampaignMessageList;

        private:
                static const char s_registryName[];

                CampaignWindowsInterface* d_campWind;
                const CampaignData* d_campData;
                // CampaignMessageList d_msgList;
                // CampaignMessageItem* d_currentMessage;
                //
                // enum {
                //      WhoFrom,
                //      DateSent,
                //      Description,
                //
                //      TextItems_HowMany,
                //
                //      SpaceCX = 15
                // };
                //
                // UWORD d_textCX[TextItems_HowMany];


  public:
         RealMessageWindow(CampaignWindowsInterface* campWind, const CampaignData* campData);
         ~RealMessageWindow();

         /*
          * Virtual Function Implementations
          */

         void addMessage(const CampaignMessageInfo& msg);

         /*
          * Other Public Functions
          */


 private:
                // void onAddMessage(LPARAM lparam);
                                // Private message to add message

//              #ifdef DEBUG
//                              void assertMsgIndex() const
//                              {
//                                      ASSERT(msgIndex() <= messageCount());
//                                      if(d_currentMessage == 0)
//                                      {
//                                              ASSERT(msgIndex() == 0);
//                                      }
//                                      else
//                                      {
//                                              ASSERT(msgIndex() > 0);
//                                              ASSERT(messageCount() != 0);
//                                      }
//                              }
//                      #else
//                              void assertMsgIndex() const { }
//                      #endif

                /*
                 * Implement GenericMessageWindow virtual functions
                 */

                const char* registryName() const { return s_registryName; }

                // bool nextMessage();
                // bool prevMessage();

                // void deleteMessage();
                void orderDialog();
                void drawMessage(HDC hdc, const PixelRect& rect, const PixelRect& fromRect, const PixelRect& dateRect);

                void onDontShow();
                // void onDeleteAll();

                void doSettings() { d_campWind->editOptions(ShowMessagePage); }
                void windowUpdated() { d_campWind->messageWindowUpdated(); }
                // void windowDestroyed(HWND hWnd) { d_campWind->windowDestroyed(hWnd); }
                // int messageCount() const { return d_msgList.size(); }
                void updateStatus();

                int fillListBox(ListBox* lb, HDC hdc);
                void drawListItem(DrawDIBDC* dib, const RECT* r, LPARAM value);
                // void setFromListBox(const ListBox& lb);

                void playSound() const;
};



const char RealMessageWindow::s_registryName[] = "msgWindow";


RealMessageWindow::RealMessageWindow(CampaignWindowsInterface* campWind, const CampaignData* campData) :
        // GenericMessageWindow(),
        d_campWind(campWind),
        d_campData(campData)
        // d_currentMessage(0)
{
#ifdef DEBUG
        msgLog.printf("RealMessageWindow::RealMessageWindow()--- constructor");
#endif

        // for(int i = 0; i < TextItems_HowMany; i++)
        //      d_textCX[i] = 0;
}

RealMessageWindow::~RealMessageWindow()
{
#ifdef DEBUG
        msgLog.printf("RealMessageWindow::~RealMessageWindow()--- destructor");
#endif

    selfDestruct();
}


void RealMessageWindow::onDontShow()
{
  ASSERT(d_currentMessage != 0);
  MessageOptions::set(d_currentMessage->messageID(), False);
}


// void RealMessageWindow::onDeleteAll()
// {
//      while(d_currentMessage)
//              deleteMessage();
// }



// void RealMessageWindow::deleteMessage()
// {
//      ASSERT(d_currentMessage);
//
//      CampaignMessageItem* next = d_msgList.next(d_currentMessage);
//      CampaignMessageItem* prev = d_msgList.prev(d_currentMessage);
//
//      if(d_currentMessage->isRead())
//        decReadCount();
//
//      ASSERT(readCount() <= messageCount());
//      ASSERT(readCount() >= 0);
//
//      d_msgList.erase(d_currentMessage);
//      d_currentMessage = 0;
//
//      if(next)
//      {
//        d_currentMessage = next;
//      }
//      else
//      {
//        if(prev)
//               d_currentMessage = prev;
//
//        decMsgIndex();
//      }
//
//      assertMsgIndex();
//
//      postChanged();
//
// }


/*
 * Bring up unit or town dialog
 *
 * This nees improving to check if such a dialogue is already up
 */

void RealMessageWindow::orderDialog()
{
        ASSERT(d_msgList.size() > 0);
        ASSERT(d_currentMessage != 0);

        if(d_currentMessage->cp() != NoCommandPosition)
        {
                ICommandPosition cp = d_currentMessage->cp();
                ILeader iLeader = cp->getLeader();
                ASSERT(d_currentMessage->leader() != NoLeader);

                if(iLeader == d_currentMessage->leader())
                {
                        d_campWind->orderUnit(cp);
                }

                // Location l;

                // cp->getLocation(l);
                d_campWind->centerMapOnUnit(cp);

        }
        else if(d_currentMessage->town() != NoTown)
        {
                d_campWind->orderTown(d_currentMessage->town());
                // Town& town = d_campData->getTown(d_currentMessage->town());
                // const Location& l = town.getLocation();
                d_campWind->centerMapOnTown(d_currentMessage->town());
        }
}


void RealMessageWindow::drawMessage(HDC hdc, const PixelRect& textRect, const PixelRect& fromRect, const PixelRect& dateRect)
{
#ifdef DEBUG_MESSAGE_WINDOW
        msgLog.printf(">drawMessage()");
#endif

        if(d_currentMessage != 0)
        {
                int fontHeight = minimum(fromRect.height() / 2, dateRect.height());
                fontHeight = minimum(fontHeight, 16);

                Fonts fonts;
                // fonts.setFace(hdc, scenario->fontName(Font_BoldItalic), fontHeight, 0, FW_DONTCARE);
                fonts.setFace(hdc, scenario->fontName(Font_Bold), fontHeight, 0, FW_DONTCARE);

                UINT oldAlign = SetTextAlign(hdc, TA_TOP | TA_LEFT);


                if(d_currentMessage->cp() != NoCommandPosition)
                {
                        ICommandPosition cp = d_currentMessage->cp();
                        ASSERT(cp != NoCommandPosition);
                        ASSERT(d_currentMessage->leader() != NoLeader);
                        ILeader general = d_currentMessage->leader();
                        ASSERT(general != NoLeader);

                        wTextOut(hdc, fromRect.left(), fromRect.top(), general->getName());
                        wTextOut(hdc, fromRect.left(), fromRect.top() + fontHeight, cp->getName());
                }
                else if(d_currentMessage->town() != NoTown)
                {
                        const Town& town = d_campData->getTown(d_currentMessage->town());
                        const Province& prov = d_campData->getProvince(town.getProvince());

                        wTextOut(hdc, fromRect.left(), fromRect.top(),    town.getNameNotNull());
                        wTextOut(hdc, fromRect.left(), fromRect.top() + fontHeight, prov.getNameNotNull());
                }


                SetTextAlign(hdc, TA_TOP | TA_RIGHT);

                // wTextPrintf(hdc, dateRect.left(), dateRect.top(), "%s  %d%s",
                wTextPrintf(hdc, dateRect.right(), dateRect.top(), "%s  %d%s",
                                                (const char*)getMonthName(d_currentMessage->date().month, FALSE),
                                                (int)d_currentMessage->date().day,
                                                (const char*)getNths(d_currentMessage->date().day)
                                                );

                fontHeight = minimum(fontHeight, 14);

//                fonts.setFace(hdc, scenario->fontName(Font_Italic), fontHeight, 0, FW_DONTCARE);
                fonts.setFace(hdc, scenario->fontName(Font_Normal), fontHeight, 0, FW_DONTCARE);

                SimpleString text = d_currentMessage->text(d_campData);

                SetTextAlign(hdc, TA_TOP | TA_LEFT);

                wTextOut(hdc, textRect, text.toStr());
                if(!d_currentMessage->isRead())
                {
                        d_currentMessage->setRead();    // = True;
                        incReadCount();
                }

                SetTextAlign(hdc, oldAlign);

        }

#ifdef DEBUG_MESSAGE_WINDOW
        msgLog.printf("<drawMessage()");
#endif
}


/*
 * Tell Campaign Windows to update the status window
 * This can be called from another thread
 */

void RealMessageWindow::updateStatus()
{
        d_campWind->msgWinUpdated();
}


/*
 * Add items to list box
 * and return the desired width
 */

int RealMessageWindow::fillListBox(ListBox* lb, HDC hdc)
{
        LONG dbUnits = GetDialogBaseUnits();
        WORD dbX = LOWORD(dbUnits);
        WORD dbY = HIWORD(dbUnits);
        const LONG offsetX = (3*dbX)/4;
        const LONG offsetY = (3*dbY)/8;

        /*
         * Iterate through all messages, adding to box
         */

        for(MessageList::const_iterator iter = d_msgList.begin();
                iter != d_msgList.end();
                ++iter)
        {
                const CampaignMessageItem* msg = iter;
                lb->add("", d_msgList.itemToLParam(msg));

                // work out lengths

                /*
                 * Set text length for each item
                 * First set name
                 */

                const char* name = 0;

                if(iter->cp() != NoCommandPosition)
                {
                        ICommandPosition cp = iter->cp();
                        if(cp->isRealUnit())
                           name = cp->getNameNotNull();
                        else
                        {
                           // ASSERT(cp->getLeader() != NoLeader);
                           ILeader leader = iter->leader();

                           if(leader != NoLeader)
                              name = leader->getName();
                           else
                              name = "?";
                        }
                }
                else if(iter->town() != NoTown)
                {
                        name = d_campData->getTownName(iter->town());
                }
#ifdef DEBUG
                else
                        FORCEASSERT("Message has no unit or town");
#endif

                ASSERT(name);

                LONG length = textWidth(hdc, name);
                d_textCX[WhoFrom] = maximum(d_textCX[WhoFrom], length);

                /*
                 * Set date length
                 */

                char date[100];

                wsprintf(date, "%s %d%s",
                        (const char*) getMonthName(iter->date().month, FALSE),
                        (int) iter->date().day,
                        (const char*) getNths(iter->date().day));

                length = textWidth(hdc, date);
                d_textCX[DateSent] = maximum(d_textCX[DateSent], length);

                if(iter->messageID() != CampaignMessages::NoMessage)
                {
                        int id = CampaignMessages::getDescriptionID(iter->messageID());
                        if(id != -1)
                        {
                                ResString description(id);

                                length = textWidth(hdc, description.c_str());
                                d_textCX[Description] = maximum(d_textCX[Description], length);
                        }
                }
        }

        return (2*offsetX)+(2* SpaceCX)+d_textCX[WhoFrom]+d_textCX[DateSent]+d_textCX[Description];

}

/*
 * Draw a list box item
 */

void RealMessageWindow::drawListItem(DrawDIBDC* dib, const RECT* rect, LPARAM value)
{
        // const CampaignMessageItem* msg = lparamToCampaignMessageItem(value);
        const CampaignMessageItem* msg = d_msgList.lParamToItem(value);
        int itemCX = rect->right - rect->left;
        int itemCY = rect->bottom - rect->top;
        LONG dbUnits = GetDialogBaseUnits();
        WORD dbX = LOWORD(dbUnits);
        WORD dbY = HIWORD(dbUnits);

        ASSERT(msg);

        const char* name = 0;

        if(msg->cp() != NoCommandPosition)
        {
                ICommandPosition cp = msg->cp();
                if(cp->isRealUnit())
                  name = cp->getNameNotNull();
                else if(msg->leader() != NoLeader)
                  name = msg->leader()->getNameNotNull();
               else
                  name = "?";
        }
        else if(msg->town() != NoTown)
        {
                name = d_campData->getTownName(msg->town());
        }
#ifdef DEBUG
        else
                FORCEASSERT("Message has no unit or town");
#endif

        ASSERT(name);
        char date[100];

        wsprintf(date, "%s %d%s",
        (const char*) getMonthName(msg->date().month, FALSE),
        (int) msg->date().day,
        (const char*) getNths(msg->date().day));

        const int offsetX = (3*dbX)/4;

        TEXTMETRIC tm;
        GetTextMetrics(dib->getDC(), &tm);
        const int y = ((itemCY - tm.tmHeight) / 2);

        int x = offsetX;
        wTextOut(dib->getDC(), x, y, name);

        x += d_textCX[WhoFrom]+SpaceCX;
        wTextOut(dib->getDC(), x, y, date);

        if(msg->messageID() != CampaignMessages::NoMessage)
        {
                int id = CampaignMessages::getDescriptionID(msg->messageID());
                if(id != -1)
                {
                        ResString description(id);

                        x += d_textCX[DateSent]+SpaceCX;
                        wTextOut(dib->getDC(), x, y, description.str());
                }
        }
}

// void RealMessageWindow::setFromListBox(const ListBox& lb)
// {
//      CampaignMessageItem* msg = d_msgList.lParamToItem(lb.getValue());
//      ASSERT(msg);
//
//      if(msg)
//      {
//              d_currentMessage = msg;
//              msgIndex(lb.getIndex()+1);
//      postChanged();
//      }
// }
//
// /*
//  * Advance to next message
//  */
//
// bool RealMessageWindow::nextMessage()
// {
//      CampaignMessageItem* next = d_msgList.next(d_currentMessage);
//      if(next)
//      {
//              d_currentMessage = next;
//              assertMsgIndex();
//              incMsgIndex();
//              assertMsgIndex();
//              postChanged();
//              return true;
//      }
//      return false;
// }
//
// bool RealMessageWindow::prevMessage()
// {
//      CampaignMessageItem* prev = d_msgList.prev(d_currentMessage);
//      if(prev)
//      {
//              d_currentMessage = prev;
//              assertMsgIndex();
//              decMsgIndex();
//              assertMsgIndex();
//              postChanged();
//              return true;
//      }
//      return false;
// }
//
// void RealMessageWindow::onAddMessage(LPARAM lparam)
// {
//      // CampaignMessageItem* newMsg = reinterpret_cast<CampaignMessageItem*>(lparam);
//      CampaignMessageItem* newMsg = d_msgList.lParamToItem(lparam);
//
// #ifdef DEBUG_MESSAGE_WINDOW
//      msgLog.printf(">onAddMessage");
// #endif
//
//      if(msgIndex() == d_msgList.size())
//      {
//        d_currentMessage = newMsg;
//        incMsgIndex();
//      }
//
//      d_msgList.push_back(newMsg);
//
// #if !defined(NO_SOUND)
//      try {
//              Sound::playSound("sounds\\newmsg.wav");
//      }
//      catch(Sound::SoundError e)
//      {
//              // Ignore Sound Error
//              FORCEASSERT("Caught SoundError");
//      }
// #endif
//
//      postChanged();
//
// #ifdef DEBUG_MESSAGE_WINDOW
//      msgLog.printf("<onAddMessage");
// #endif
// }

void RealMessageWindow::playSound() const
{
        d_campData->soundSystem()->TriggerSound(d_currentMessage->sound(), CAMPAIGNSOUNDPRIORITY_MEDIUM);
#if 0
        try {
                Sound::playSound("sounds\\newmsg.wav");
        }
        catch(Sound::SoundError e)
        {
                // Ignore Sound Error
                FORCEASSERT("Caught SoundError");
        }
#endif
}

/*=================================================================
 * Adding Messages:
 * This may be called from another Thread via addMessage
 */

/*=================================================================
 * This is the only function that can be called from other threads
 */

void RealMessageWindow::addMessage(const CampaignMessageInfo& msg)
{
#ifdef DEBUG_MESSAGE_WINDOW
        msgLog.printf(">appendMessage(%d, %d, %s, %d)",
                (int) d_campData->getArmies().getIndex(from),
                (int) town,
                (const char*) (text ? text : "NULL"),
                (int) msgID);
#endif

      if(MessageOptions::get(msg.id()))
      {
           ASSERT((msg.where() != NoTown) || (msg.cp() != NoCommandPosition));

           CampaignMessageInfo newMsg = msg;
           if( (newMsg.leader() == NoLeader) && (newMsg.cp() != NoCommandPosition) )
                   newMsg.leader(newMsg.cp()->getLeader());

           CampaignMessageItem* item = CampaignMessageItem::make(msg, d_campData->getDate());

   #ifdef DEBUG_MESSAGE_WINDOW
           msgLog.printf("created CampaignMessageItem");
   #endif


   #ifdef DEBUG_MESSAGE_WINDOW
           msgLog.printf("About to log unit name and town");
           msgLog.printf("Unit:%s, town:%s, '%s'",
                           (from == NoCommandPosition) ? "NULL" : from->getName(),
                           (town == NoTown) ? "NULL" : d_campData->getTown(town).getNameNotNull(),
                           text);
   #endif

           /*
            * A Message is sent to the Message Window to attach the new
            * message.
            * This means that all user interface and GDI functions are
            * handled by the Message Window creators thread and solves
            * a lot of problems with threads getting locked up
            */

           // postMessage(reinterpret_cast<LPARAM>(newMsg));
           postMessage(item);
      }

#ifdef DEBUG_MESSAGE_WINDOW
        msgLog.printf("<appendMessage()");
#endif
}


/*-----------------------------------------------------------------
 * Access to the outside world
 */

MessageWindow::MessageWindow(CampaignWindowsInterface* campWind, const CampaignData* campData, HWND hwnd)
{
        ASSERT(campWind != 0);
        ASSERT(campData != 0);
        ASSERT(hwnd != NULL);

        d_msgWindow = new RealMessageWindow(campWind, campData);
        d_msgWindow->init(hwnd);
        ASSERT(d_msgWindow);
}

MessageWindow::~MessageWindow()
{
    delete d_msgWindow;
}


MessageWindow* MessageWindow::make(CampaignWindowsInterface* campWind, const CampaignData* campData, HWND hwnd)
{
        ASSERT(campWind != 0);
        ASSERT(campData != 0);
        ASSERT(hwnd != NULL);

        return new MessageWindow(campWind, campData, hwnd);
}

void MessageWindow::addMessage(const CampaignMessageInfo& msg)
{
  ASSERT(d_msgWindow);
  if(d_msgWindow)
  {
         d_msgWindow->addMessage(msg);
  }
}

// void MessageWindow::show()
// {
//   ASSERT(d_msgWindow);
//   if(d_msgWindow)
//   {
//          d_msgWindow->show();
//   }
// }
//
// void MessageWindow::hide()
// {
//   ASSERT(d_msgWindow);
//   if(d_msgWindow)
//   {
//          d_msgWindow->hide();
//   }
// }

void MessageWindow::clear()
{
  ASSERT(d_msgWindow);
  if(d_msgWindow)
  {
         d_msgWindow->clear();
  }
}

// void MessageWindow::destroy()
// {
//   ASSERT(d_msgWindow);
//   if(d_msgWindow)
//   {
//         d_msgWindow->destroy();
//          // DestroyWindow(d_msgWindow->getHWND());
//          d_msgWindow = 0;
//   }
// }
//
// void MessageWindow::toggle()
// {
//   ASSERT(d_msgWindow);
//   if(d_msgWindow)
//   {
//          d_msgWindow->toggle();
//   }
// }
//
// Boolean MessageWindow::isShowing() const
// {
//   if(d_msgWindow)
//   {
//          return d_msgWindow->isShowing();
//   }
//
//   return False;
// }

int MessageWindow::getNumMessages() const
{
  if(d_msgWindow)
  {
         return d_msgWindow->getNumMessages();
  }
  else
         return 0;
}

int MessageWindow::getMessagesRead() const
{
  if(d_msgWindow)
  {
         return d_msgWindow->getMessagesRead();
  }
  else
         return 0;
}

// void MessageWindow::suspend(bool visible)
// {
//         d_msgWindow->suspend(visible);
// }

HWND MessageWindow::getHWND() const
{
    return d_msgWindow->getHWND();
}

bool MessageWindow::isVisible() const
{
  return d_msgWindow->isVisible();
}

bool MessageWindow::isEnabled() const
{
  return d_msgWindow->isEnabled();
}

void MessageWindow::show(bool visible)
{
  d_msgWindow->show(visible);
}

void MessageWindow::enable(bool enable)
{
  d_msgWindow->enable(enable);
}


/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.4  2002/11/16 18:03:32  greenius
 * Various changes so that it will compile with Visual Studio .net
 *
 * Revision 1.3  2001/07/02 15:23:17  greenius
 * Battle, Campaign, Combined game and Campaign Editor now compile and run... mostly
 *
 * Revision 1.2  2001/06/18 08:25:45  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
