/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef CCOMBO_HPP
#define CCOMBO_HPP

#include "palwind.hpp"
#include "campord.hpp"
#include "gamedefs.hpp"

/*
 * Class for custom drawing a combo box
 */

class DrawDIBDC;
//class DrawDIB;


class CampaignData;
class PixelPoint;
class CampaignOrder;
class StackedUnitList;
class ItemWindow;

struct CampaignComboData  {
  const StackedUnitList* d_unitList;
  ConstICommandPosition d_currentUnit;
  Boolean d_showAll;
  Boolean d_showManpower;
  CampaignOrder* d_order;
  UINT d_itemCX;
  UINT d_itemCY;

  CampaignComboData() :
	 d_unitList(0),
	 d_currentUnit(NoCommandPosition),
	 d_showAll(False),
	 d_showManpower(False),
	 d_order(0),
	 d_itemCX(0),
	 d_itemCY(0) {}
};

/*
 * access class for the combo-like windows used by unitdial
 */

class CampaignComboInterface {
public:
  enum Type {
	 OrderPageFirst = 0,
	 MoveHow = OrderPageFirst,
	 OnArrival,
	 OrderType,
	 Aggression,
	 OrderPageLast,

	 UnitList = OrderPageLast,

	 HowMany
  };
private:
  ItemWindow** d_items;
public:
  CampaignComboInterface();
  ~CampaignComboInterface();

  void create(HWND hParent, Type type, UWORD id, const CampaignData* campData, const SIZE& s, UBYTE flags = 0);
  void init(Type type, const CampaignComboData& data); //ICommandPosition cpi, CampaignOrder& order);
  void show(Type type, PixelPoint& p);
  void hide(Type type);
  ULONG getValue(Type type);
  void drawCombo(Type type, DrawDIBDC* dib, const DrawDIB* fillDib, CampaignComboData& data);
  Boolean created(Type type);
  Boolean showing(Type type);
  void destroy();
};

#if 0
class CampaignComboInterface {
public:
  enum Type {
	 OrderPageFirst = 0,
	 MoveHow = OrderPageFirst,
	 OnArrival,
	 OrderType,
	 Aggression,
	 OrderPageLast,

	 UnitList = OrderPageLast,

	 HowMany
  };

  static void create(HWND hParent, Type type, UWORD id, const CampaignData* campData, const SIZE& s, Boolean hasScroll = False);
  static void init(Type type, const CampaignComboData& data); //ICommandPosition cpi, CampaignOrder& order);
  static void show(Type type, PixelPoint& p);
  static void hide(Type type);
  static ULONG getValue(Type type);
  static void drawCombo(Type type, DrawDIBDC* dib, const DrawDIB* fillDib, const CampaignComboData& data);
  static Boolean created(Type type);
  static Boolean showing(Type type);
};


class CustomDrawnCombo : public SubClassWindowBase, public CustomBorderWindow {
	 DrawDIBDC* d_buttonDib;
	 DrawDIBDC* d_displayDib;
//	 DrawDIBDC* d_itemDib;

//	 const DrawDIB* d_displayFillDib;
	 HFONT d_font;
//	 ComboEdit d_edit;
  public:
	 CustomDrawnCombo();
	 ~CustomDrawnCombo();

	 void setFont(HFONT hFont) { d_font = hFont; }

//	 void initCombo(HWND hwnd);
	 HFONT font() const { return d_font; }

	 // default draw function for user drawn combo
	 void drawCombo(const DRAWITEMSTRUCT* lpDrawItem);

  private:
	 LRESULT procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
	 void onPaint(HWND hwnd);
//	 BOOL onEraseBk(HWND hwnd, HDC hdc) { return FALSE; }

//    void onSetFocus(HWND hwnd, HWND hwndOldFocus);
//    void onActivate(HWND hwnd, UINT state, HWND hwndActDeact, BOOL fMinimized);
//    BOOL onNCActivate(HWND hwnd, BOOL fActive, HWND hwndActDeact, BOOL fMinimized);

};
#endif

#endif
