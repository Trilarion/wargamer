/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef STATWIND_HPP
#define STATWIND_HPP

#ifndef __cplusplus
#error statwind.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1995, Steven Morle-Green, All Rights Reserved
 *----------------------------------------------------------------------
 *
 *	Status Window
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/06/13 08:52:38  greenius
 * Initial Import
 *
 *
 *----------------------------------------------------------------------
 */

#include "palwind.hpp"
#include "tooltip.hpp"

// status window class to replace windows status class

class StatusSection;
class CampaignWindowsInterface;

class StatusWindow :
	public WindowBaseND,
	public HintLineBase,
	public SuspendableWindow
{
	friend class StatusSection;

	 static const char s_className[];
	 static ATOM classAtom;

	 CampaignWindowsInterface* d_campWindows;

	 StatusSection** sections;

	 // Disable Copy Constructors

	 StatusWindow(const StatusWindow& statWind);
	 const StatusWindow& operator = (const StatusWindow& statWind);

  public:
	 StatusWindow(HWND hParent, CampaignWindowsInterface* campWind);
	 ~StatusWindow();

	 void showHint(const char* text);
	 void clearHint();
	 void updateMessageWindow();

    CampaignWindowsInterface* campWindow() const { return d_campWindows; }

    virtual const char* className() const { return s_className; }

  private:
	 static ATOM registerClass();
	 void onCommand(HWND hWnd, int id, HWND hwndCtl, UINT codeNotify);
	 LRESULT procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
	 BOOL onCreate(HWND hWnd, CREATESTRUCT* lpCreateStruct);
	 void onSize(HWND hWnd, UINT state, int cx, int cy);
	 void update();

};

#endif /* STATWIND_HPP */

