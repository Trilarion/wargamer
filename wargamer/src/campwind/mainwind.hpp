/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
#ifndef MAINWIND_H
#define MAINWIND_H

#ifndef __cplusplus
#error mainwind.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1995, Steven Morle-Green, All Rights Reserved
 *----------------------------------------------------------------------
 *
 * Main Window for Wargamer
 *
 *----------------------------------------------------------------------
 */

class MainWindow_Imp;
class CampaignWindowsInterface;
class CustomMenu;

class MainCampaignWindow {
    MainWindow_Imp* d_mainWind;
  public:
    MainCampaignWindow(HWND hMain, CampaignWindowsInterface* campWind, int nCmdShow);
    ~MainCampaignWindow();

    HWND getHWND() const;

    void checkMenu(int id, bool state);
    void enableMenu(int id, bool flag);
    void toggleMenu();
    void optionsUpdated();

    int menuHeight() const;

    void enable();      // setup subclass
    void disable();     // remove subclass

      CustomMenu* getMenu();

};
#endif /* MAINWIND_H */

/*
 *----------------------------------------------------------------------
 * $Log$
 *----------------------------------------------------------------------
 */
