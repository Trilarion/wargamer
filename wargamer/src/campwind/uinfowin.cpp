/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 * Unit Information dialog
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "uinfowin.hpp"
#include "scenario.hpp"
#include "tabwin.hpp"
#include "unitinfo.hpp"
#include "scn_img.hpp"
#include "fonts.hpp"
#include "scrnbase.hpp"
#include "res_str.h"
#include "resstr.hpp"

/*----------------------------------------------------------------------
 * Unit info window
 */

class UnitInfoWindow : public TabbedInfoWindow {

   CampaignUserInterface* d_owner;

    enum Tabs {
       SummaryTab,
       LeaderTab,
       ActivityTab,
       OrdersTab,
       OBTab,

       Tab_HowMany
    };

    // static const char* s_tabText[Tab_HowMany];
    static const int s_tabID[Tab_HowMany];
    static ResourceStrings s_tabText;

    ConstICommandPosition d_cpi;
    const CampaignData* d_campData;

  public:
    UnitInfoWindow(CampaignUserInterface* owner, HWND hParent, const CampaignData* campData,
       const DrawDIB* infoFillDib, const DrawDIB* bkDib,
       const DrawDIB* bFillDib, const ImageLibrary* il, HFONT font,
       CustomBorderInfo bc) :
       TabbedInfoWindow(hParent, Tab_HowMany, s_tabText, infoFillDib, bkDib, bFillDib, il, font, bc),
       d_owner(owner),
       d_cpi(NoCommandPosition),
       d_campData(campData) {}
    ~UnitInfoWindow() {}

    void run(const ConstICommandPosition& cpi, const PixelPoint& p);
    void onSelChange();
    void onRefresh();
    int getRefreshRate() const { return 1000; }
 private:
   bool checkDead();
};

#if 0
// TODO: get this from string resource
const char* UnitInfoWindow::s_tabText[Tab_HowMany] = {
  "Summary",
  "Leader",
  "Activity",
  "Orders",
  "OB"
};
#endif

const int UnitInfoWindow::s_tabID[Tab_HowMany] =
{
   IDS_UI_SUMMARY,
   IDS_UI_LEADER,
   IDS_UI_ACTIVITY,
   IDS_UI_ORDERS,
   IDS_UI_OB
};

ResourceStrings UnitInfoWindow::s_tabText(s_tabID, Tab_HowMany);

/*
 * Check if unit is dead and close down if it is
 * return true if it was closed
 */

bool UnitInfoWindow::checkDead()
{
   if ( (d_cpi == NoCommandPosition) || d_cpi->isDead())
   {
      FORWARD_WM_CLOSE(getHWND(), PostMessage);

      return true;
   }
   return false;
}

void UnitInfoWindow::onSelChange()
{
   if(checkDead())
      return;


  Tabs whichTab = static_cast<Tabs>(currentTab());
  ASSERT(whichTab < Tab_HowMany);

  fillStatic();

  if(d_cpi != NoCommandPosition)
  {
    UnitInfoData ud(d_cpi, d_campData, dib(), dRect());

    switch(whichTab)
    {
      case SummaryTab:
        ud.drawSummaryDib();
        break;

      case LeaderTab:
        ud.drawLeaderDib();
        break;

      case OBTab:
        ud.drawOBDib();
        break;

      case ActivityTab:
        ud.drawActivityDib();
         break;
      case OrdersTab:
        ud.drawOrdersDib();
         break;

      default:
         FORCEASSERT("Illegal Tab");
    }
  }
}

void UnitInfoWindow::run(const ConstICommandPosition& cpi, const PixelPoint& p)
{
  d_cpi = cpi;
  onSelChange();
  TabbedInfoWindow::run(p);
}

/*
 * Called every second
 * If current Unit is dead, then window is closed
 * otherwise it updates the display
 */

void UnitInfoWindow::onRefresh()
{
   if(!checkDead())
      onSelChange();    // This redraws the display
}

/*---------------------------------------------------------------------
 * Outside access
 */

UnitInfo_Int::UnitInfo_Int(CampaignUserInterface* owner, HWND hParent, const CampaignData* campData) :
  d_infoWind(0) // new UnitInfoWindow(owner, hParent, campData))
{
  LogFont lf;
  lf.height((6 * ScreenBase::dbY()) / ScreenBase::baseY());
  lf.weight(FW_MEDIUM);
  lf.face(scenario->fontName(Font_Decorative));

  Font font;
  font.set(lf);

  d_infoWind = new UnitInfoWindow(owner, hParent, campData,
    scenario->getScenarioBkDIB(Scenario::BackPatterns::PaperBackground),
    scenario->getScenarioBkDIB(Scenario::BackPatterns::CherryWoodBackground),
    scenario->getScenarioBkDIB(Scenario::BackPatterns::WoodBackground),
    ScenarioImageLibrary::get(ScenarioImageLibrary::CTabImages), font,
    scenario->getBorderColors());
  ASSERT(d_infoWind);
}

UnitInfo_Int::~UnitInfo_Int()
{
  destroy();
}

void UnitInfo_Int::run(const ConstICommandPosition& cpi, const PixelPoint& p)
{
  if(d_infoWind)
  {
    d_infoWind->run(cpi, p);
  }
}

void UnitInfo_Int::destroy()
{
  if(d_infoWind)
  {
    delete d_infoWind;
    d_infoWind = 0;
  }
}

/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.2  2001/06/18 08:25:45  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
