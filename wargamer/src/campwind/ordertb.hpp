/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef ORDERTB_HPP
#define ORDERTB_HPP

#ifndef __cplusplus
#error ordertb.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *	Orders Window
 *
 *----------------------------------------------------------------------
 */

#include "wind.hpp"
#include "mw_user.hpp"

class DrawDIBDC;
class CampaignWindowsInterface;
class MapWindow;
class CampaignData;
struct Date;

/*
 *  virtual class for orders TB, in case we do town orders with TB also
 *  Basically copied from MapDialog
 *
 * Some of this does not belong here... e.g. CustomBkWindow should
 * be dcelared when the window that uses this class is defined
 * This class should only be used as a way for the mapwindow to
 * communicate with the dialog.
 *
 * Some of these functions are implemented in TownTB.cpp
 * And others are in mapwind.cpp
 * They should be defined in mapwind, because that is where it is
 * defined.
 *
 * Some of these functions (createDIB, setPosition, drawInfo) really
 * belong with the CustomBkWindow class!
 */

class OrderTB //	: public CampaignWindow {
{
protected:
	CampaignWindowsInterface* d_campWind;
	const CampaignData* d_campData;

	MapWindow* mapWindow;
	HWND parentHWND;
	DrawDIBDC* dib;
public:
	OrderTB(CampaignWindowsInterface* campWind, const CampaignData* campData, HWND h, MapWindow* p);
	virtual ~OrderTB() { }

	void createDIB(HWND hwnd);
	void drawInfo(HDC hdc);

	static ATOM registerClass();

	virtual TrackMode getTrack() = 0;		// Return how to do tracking
	virtual Boolean onLClick(MapSelect& info) = 0;
	virtual void onRClick(MapSelect& info) = 0;
	// virtual void onTrack(MapSelect& info) = 0;
	virtual void onDraw(DrawDIBDC* dib, MapSelect& info) = 0;

	virtual void update() = 0;					// Info may have changed
	virtual void addObject(ConstICommandPosition cpi) = 0;
	virtual void onButtonDown(MapSelect& info) = 0;
	virtual void onStartDrag() = 0;
	virtual void onDrag(MapSelect& info) = 0;
	virtual void onEndDrag(MapSelect& info) = 0;
	virtual void updateZoom() = 0;        // update map arrows when zooming

	virtual void setDetaching(Boolean flag) = 0;
	virtual void setRightClicked(Boolean flag) = 0;
	virtual void setOrderDate(Date& date) = 0;
//	virtual void timedOrders() = 0;
	virtual void drawDIB() = 0;           // info dib
	virtual void reset() = 0;            // clear out current values

	virtual HWND getHWND() const = 0;
};

#endif /* ORDERTB_HPP */

