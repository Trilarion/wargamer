/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef INFOWIND_H
#define INFOWIND_H

#ifndef __cplusplus
#error infowind.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1995, Steven Morle-Green, All Rights Reserved
 *----------------------------------------------------------------------
 *
 *	Tracking Information Window
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.2  1996/06/22 19:06:39  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.1  1996/02/16 17:34:14  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#include "compos.hpp"

/*
 * Forward references
 */

class PixelPoint;
//class DrawDIBDC;
class RealTrackingWindow;
class CampaignData;

/*
 * new town/unit tracking dialogs. replaces old info windows
 */

class TrackingWindow {
	 RealTrackingWindow* d_trackWindow;
  public:
	 TrackingWindow(HWND hParent, const CampaignData* campData);
	 ~TrackingWindow();

	 void show(PixelPoint& p, ConstParamCP cpi);
	 void show(PixelPoint& p, ITown iTown);
	 void update(ConstParamCP cpi);
	 void update(ITown town);
	 void hide();

	 int height();
    int width();
//    void redraw();
};

#endif /* INFOWIND_H */

