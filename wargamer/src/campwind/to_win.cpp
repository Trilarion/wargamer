/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
#include "stdinc.hpp"
#include "to_win.hpp"
#include "wmisc.hpp"
#include "scenario.hpp"
#include "palette.hpp"
#include "app.hpp"
#include "calctrl.hpp"
#include "fonts.hpp"
#include "winctrl.hpp"
#include "palwind.hpp"
#include "fillwind.hpp"
#include "cbutton.hpp"
#include "scrnbase.hpp"
#include "campord.hpp"
#include "compos.hpp"
#include "resstr.hpp"


struct CInfo {
  UWORD d_x;
  UWORD d_y;
  UWORD d_cx;
  UWORD d_cy;
};

class Util {
public:
  const int d_shadowWidth;
  const int d_cwWidth;
  const int d_cwHeight;
  const int d_monthX;
  const int d_monthY;
  const int d_dayX;
  const int d_dayY;

  Util::Util() :
    d_shadowWidth((ScreenBase::dbX() * 3) / ScreenBase::baseX()),
    d_cwWidth(((ScreenBase::dbX() * 90) / ScreenBase::baseX()) + d_shadowWidth),
    d_cwHeight(((ScreenBase::dbY() * 85) / ScreenBase::baseY()) + d_shadowWidth),
    d_monthX((ScreenBase::dbY() * 2) / ScreenBase::baseY()),
    d_monthY((ScreenBase::dbY() * 2) / ScreenBase::baseY()),
    d_dayX((ScreenBase::dbY() *  3) / ScreenBase::baseY()),
    d_dayY((ScreenBase::dbY() * 13) / ScreenBase::baseY()) {}

  static HWND createButton(HWND hParent, const char* text, int id, const CInfo& ci);
};

HWND Util::createButton(HWND hParent, const char* text, int id, const CInfo& ci)
{
  HWND hButton = CreateWindow(CUSTOMBUTTONCLASS, text,
            BS_PUSHBUTTON | WS_CHILD | WS_VISIBLE,
            (ScreenBase::dbX() * ci.d_x) / ScreenBase::baseX(), (ScreenBase::dbY() * ci.d_y) / ScreenBase::baseY(),
            (ScreenBase::dbX() * ci.d_cx) / ScreenBase::baseX(), (ScreenBase::dbY() * ci.d_cy) / ScreenBase::baseY(),
            hParent, (HMENU)id, APP::instance(), NULL);

  ASSERT(hButton != 0);

  CustomButton cb(hButton);
  // cb.setFillDib(scenario->getScenarioBkDIB(Scenario::BackPatterns::WoodBackground));
  cb.setFillDib(scenario->getScenarioBkDIB(Scenario::BackPatterns::ButtonBackground));
  cb.setBorderColours(scenario->getBorderColors());

  /*
   * Set up font
   */

  const int fontCY = (8 * ScreenBase::dbY()) / ScreenBase::baseY();

  LogFont lf;
  lf.height(fontCY);
  lf.weight(FW_MEDIUM);
  lf.face(scenario->fontName(Font_Normal));

  Font font;
  font.set(lf);
  cb.setFont(font);

  return hButton;
}

class CalenderWindow : public WindowBaseND, private DateDefinitions {
    HWND d_hParent;
    int d_id;
    HWND d_hCalender;
    Date d_startDate;
    Date d_currentDate;

    CampaignOrder& d_order;
    FillWindow d_fillWind;

    // Boolean d_showing;
    Util d_util;

    enum ID {
       First = 100,
       FirstButton = First,
       LastMonth = FirstButton,
       NextMonth,
       OK,
       Close,
       LastButton = Close,

       Calender,

       Last
    };

//  static const char* s_strings[Last - First];
    static const CInfo s_cInfo[Last - First];

  public:
    CalenderWindow(HWND hParent, int id, CampaignOrder& order, const Date& date);
    ~CalenderWindow();

    void init(const Date& startDate);
     void setPosition(const PixelPoint& p);

    // void hide();
    // void destroy();
    // Boolean isShowing() { return d_showing; }

  private:

    /*
     *   Message Functions
     */

    LRESULT procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
    BOOL onCreate(HWND hWnd, CREATESTRUCT* lpCreateStruct);
    void onCommand(HWND hWnd, int id, HWND hwndCtl, UINT codeNotify);
    void onPaint(HWND hWnd);
    void onDestroy(HWND hWnd);
    void onShow(HWND hWnd, BOOL fShow, UINT status);
    void onClose(HWND hWnd);
    void onSize(HWND hwnd, UINT state, int cx, int cy);
    void onMouseMove(HWND hwnd, int x, int y, UINT keyFlags);
    void onLButtonDown(HWND hwnd, BOOL fDoubleClick, int x, int y, UINT keyFlags);
    BOOL onEraseBk(HWND hwnd, HDC hdc);

    void onLastMonth();
    void onNextMonth();
    void onOK();
    void enableControls();
    void redrawDate();
#ifdef DEBUG
    void checkPalette() { }
#endif

    const CInfo& controlInfo(ID id) const
    {
      ASSERT(id < Last);
      return s_cInfo[id - First];
    }

    const char* idToString(ID id);

};

const CInfo CalenderWindow::s_cInfo[Last - First] = {
  {  65,  3,  10,  7 },      // last month
  {  77,  3,  10,  7 },      // next month
//  {  16, 72,  20,  8 },      // ok
//  {  52, 72,  20,  8 },      // close
  {   9, 72,  32,  8 },      // ok
  {  48, 72,  32,  8 },      // close
  {   2, 20,  86, 48 }      // calender

};

// // TODO: get this from string resource
// const char* CalenderWindow::s_strings[Last - First] = {
//    "<-",
//    "->",
//    "OK",
//    "Close",
//    0,
// };

const char* CalenderWindow::idToString(ID id)
{
  ASSERT(id < Last);
//  ASSERT(s_strings[id - First]);
//  return s_strings[id - First];

  struct
  {
      InGameText::ID d_id;
      const char* d_text;
  } s_strings[Last - First] = {
      { InGameText::Null, "<-"   },
      { InGameText::Null, "->"   },
      { IDS_OK,           0      },
      { TTS_UID_CLOSE,    0      }
  };

  int i = id - First;
  if(s_strings[i].d_text != 0)
   return s_strings[i].d_text;
  else
   return InGameText::get(s_strings[i].d_id);
}

//=================== CalenderWindow Implementation =============================


CalenderWindow::CalenderWindow(HWND hParent, int id, CampaignOrder& order, const Date& date) :
  d_hParent(hParent),
  d_id(id),
  d_order(order),
  d_hCalender(0)
  // d_showing(False)
{
  d_startDate = date;
  d_currentDate = date;

//  bkDib = scenario->getSideBkDIB(SIDE_Neutral);

  HWND hWnd = createWindow(
      0,
      // GenericNoBackClass::className(),
        NULL,
      WS_POPUP | WS_CLIPSIBLINGS,
      0, 0, d_util.d_cwWidth, d_util.d_cwHeight,
      hParent, NULL   // , APP::instance()
  );
  ASSERT(hWnd != NULL);

}

CalenderWindow::~CalenderWindow()
{
    selfDestruct();
}

LRESULT CalenderWindow::procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{

//  if(handlePalette(hWnd, msg, wParam, lParam, result))
//  return result;

  switch(msg)
  {
    HANDLE_MSG(hWnd, WM_PAINT,   onPaint);
    HANDLE_MSG(hWnd, WM_CREATE,  onCreate);
    HANDLE_MSG(hWnd, WM_DESTROY, onDestroy);
//  HANDLE_MSG(hWnd, WM_MOUSEMOVE, onMouseMove);
    HANDLE_MSG(hWnd, WM_COMMAND, onCommand);
    HANDLE_MSG(hWnd, WM_ERASEBKGND, onEraseBk);
//  HANDLE_MSG(hWnd, WM_SIZE, onSize);

    default:
      return defProc(hWnd, msg, wParam, lParam);
  }
}

BOOL CalenderWindow::onCreate(HWND hWnd, CREATESTRUCT* lpCreateStruct)
{
  /*
   * create calender control
   */

  const CInfo& ci = controlInfo(Calender);
  d_hCalender = CalenderControl::make(APP::instance(), hWnd, Calender, d_startDate,
     (ScreenBase::dbX() * ci.d_x) / ScreenBase::baseX(), (ScreenBase::dbY() * ci.d_y) / ScreenBase::baseY(),
     (ScreenBase::dbX() * ci.d_cx) / ScreenBase::baseX(), (ScreenBase::dbY() * ci.d_cy) / ScreenBase::baseY());

  ASSERT(d_hCalender != 0);
  CalenderControl::setBk(d_hCalender, scenario->getScenarioBkDIB(Scenario::BackPatterns::PaperBackground));

  /*
   * create buttons
   */

  for(ID id = FirstButton; id < LastButton + 1; INCREMENT(id))
  {
    const CInfo& ci = controlInfo(id);
    Util::createButton(hWnd, idToString(id), id, ci);
  }


  /*
   * init background dib
   */

  d_fillWind.setShadowWidth(d_util.d_shadowWidth);
  d_fillWind.allocateDib(d_util.d_cwWidth, d_util.d_cwHeight);
  d_fillWind.setFillDib(scenario->getScenarioBkDIB(Scenario::BackPatterns::CherryWoodBackground));
  d_fillWind.setBorderColors(scenario->getBorderColors());

  enableControls();
  return TRUE;
}

void CalenderWindow::onDestroy(HWND hWnd)
{
  if(d_hCalender)
    DestroyWindow(d_hCalender);
}

BOOL CalenderWindow::onEraseBk(HWND hwnd, HDC hdc)
{
  d_fillWind.paint(hdc);
  return True;
}

void CalenderWindow::onCommand(HWND hWnd, int id, HWND hwndCtl, UINT codeNotify)
{
  switch(id)
  {
    case LastMonth:
      onLastMonth();
      break;

    case NextMonth:
      onNextMonth();
      break;

    case Calender:
      CalenderControl::getCurrentDate(hwndCtl, d_currentDate);
      onOK();
      break;

    case OK:
      onOK();
      break;

    case Close:
      // ShowWindow(hWnd, SW_HIDE);
        show(false);
      break;
  }
}


// static const char* dayAbrev[] = {
//   "M",
//   "T",
//   "W",
//   "T",
//   "F",
//   "S",
//   "S"
// };

void CalenderWindow::onPaint(HWND hwnd)
{
  PAINTSTRUCT ps;
  HDC hdc = BeginPaint(hwnd, &ps);

  HPALETTE oldPal = SelectPalette(hdc, Palette::get(), FALSE);
  RealizePalette(hdc);

  const int dbY = ScreenBase::dbY();
  const int baseY = ScreenBase::baseY();

  // set text color
  COLORREF color;
  scenario->getColour("MapInsert", color);
  COLORREF oldColor = SetTextColor(hdc, color);

  /*
   * Set menu fonts (one normal, the other underlined
   */

  LogFont lf;
  lf.height((8 * dbY) / baseY);
  lf.weight(FW_MEDIUM);
  lf.face(scenario->fontName(Font_Bold));

  Font font;
  font.set(lf);
  HFONT oldFont = reinterpret_cast<HFONT>(SelectObject(hdc, font));
  ASSERT(oldFont);

  SetBkMode(hdc, TRANSPARENT);

  /*
   * Put border around calender
   */

  /*
   * Put month, and year text
   */

  wTextPrintf(hdc, d_util.d_monthX, d_util.d_monthY, "%s, %d",
     getMonthName(d_currentDate.month, False), static_cast<int>(d_currentDate.year));

  /*
   * Put day text
   */

  lf.height((6 * dbY) / baseY);
//  lf.face(scenario->fontName(Font_Bold));
  font.set(lf);
  SelectObject(hdc, font);

  int x = d_util.d_dayX;
  int y = d_util.d_dayY;

  const int days = 7;

  RECT r;
  GetClientRect(d_hCalender, &r);

  const int calenderWidth = r.right - r.left;
  const int width = calenderWidth/days;

  for(int i = 0; i < days; i++)
  {
//  wTextOut(hdc, x, y, dayAbrev[i]);
    wTextOut(hdc, x, y, InGameText::get(i + IDS_DAY_INITIAL));
    x += width;
  }

  /*
   * clean up
   */

  SelectObject(hdc, oldFont);
  SetTextColor(hdc, oldColor);
  SelectPalette(hdc, oldPal, FALSE);
  EndPaint(hwnd, &ps);
}

void CalenderWindow::enableControls()
{
  /*
   * enable/disable last-month control
   */

  {
    CustomButton b(getHWND(), LastMonth);
    b.enable(!d_currentDate.sameMonth(d_startDate));
  }


}

void CalenderWindow::onLastMonth()
{
  ASSERT(!d_currentDate.sameMonth(d_startDate));

  Date lastMonth;

  if(d_currentDate.month == January)
  {
    lastMonth.set(0, December, d_currentDate.year-1);
  }
  else
  {
    lastMonth.set(0, d_currentDate.month-1, d_currentDate.year);
  }

  d_currentDate = (lastMonth.sameMonth(d_startDate)) ? d_startDate : lastMonth;

  CalenderControl::setMonth(d_hCalender, d_currentDate);
  enableControls();

  redrawDate();
}

void CalenderWindow::init(const Date& date)
{
  ASSERT(d_hCalender);

  d_currentDate = date;
  d_startDate = date;

  CalenderControl::setMonth(d_hCalender, d_currentDate);

  enableControls();
}

void CalenderWindow::setPosition(const PixelPoint& p)
{
    SetWindowPos(getHWND(), HWND_TOP, p.getX(), p.getY(), 0, 0, SWP_NOSIZE);
}

//
// void CalenderWindow::hide()
// {
//   ShowWindow(getHWND(), SW_HIDE);
//   d_showing = False;
// }

void CalenderWindow::onNextMonth()
{
  if(d_currentDate.month == December)
  {
    d_currentDate.set(0, January, d_currentDate.year+1);
  }
  else
  {
    d_currentDate.set(0, d_currentDate.month+1, d_currentDate.year);
  }

  CalenderControl::setMonth(d_hCalender, d_currentDate);
  enableControls();

  redrawDate();
}


void CalenderWindow::onOK()
{
  d_order.setDate(d_currentDate);
  show(false);  // hide();

  SendMessage(d_hParent, WM_COMMAND, MAKEWPARAM(d_id, 0), reinterpret_cast<LPARAM>(getHWND()));
}

void CalenderWindow::redrawDate()
{
  RECT r;
  SetRect(&r, 0, 0, 126, 30);
  InvalidateRect(getHWND(), &r, TRUE);
}

/*------------------------------------------------------------------------
 * TimedOrderWindow
 */


//static CalenderWindow* s_calender = 0;

void TimedOrderWindow::create(HWND hParent, int id, CampaignOrder& order, const Date& date)
{
  ASSERT(d_calender == 0);

  d_calender = new CalenderWindow(hParent, id, order, date);
  ASSERT(d_calender);
}

TimedOrderWindow::~TimedOrderWindow()
{
    delete d_calender;
}

void TimedOrderWindow::init(const Date& startDate)
{
  ASSERT(d_calender);
  if(d_calender)
    d_calender->init(startDate);
}

void TimedOrderWindow::show(const PixelPoint& p)
{
  ASSERT(d_calender);
  if(d_calender)
  {
    d_calender->setPosition(p);
     d_calender->show(true);
  }
}

// void TimedOrderWindow::hide()
// {
//   ASSERT(d_calender);
//   if(d_calender)
//     d_calender->hide();
// }
//
// void TimedOrderWindow::destroy()
// {
//   ASSERT(d_calender);
//   if(d_calender)
//   {
//     DestroyWindow(d_calender->getHWND());
//     d_calender = 0;
//   }
// }
//
// Boolean TimedOrderWindow::isShowing()
// {
//   ASSERT(d_calender);
//   if(d_calender)
//     return d_calender->isShowing();
//
//   return True;
// }
//
// HWND TimedOrderWindow::hwnd()
// {
//   ASSERT(d_calender);
//   if(d_calender)
//     return d_calender->getHWND();
//
//   return 0;
// }


HWND TimedOrderWindow::getHWND() const
{
    return d_calender->getHWND();
}

bool TimedOrderWindow::isVisible() const
{
  return d_calender->isVisible();
}

bool TimedOrderWindow::isEnabled() const
{
  return d_calender->isEnabled();
}

void TimedOrderWindow::show(bool visible)
{
  d_calender->show(visible);
}

void TimedOrderWindow::enable(bool enable)
{
  d_calender->enable(enable);
}


/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.2  2001/06/18 08:25:45  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
