/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
#include "stdinc.hpp"
#include "tordrwin.hpp"
#include "tbldimg.hpp"
#include "campdint.hpp"
#include "town.hpp"
#include "wind.hpp"
#include "fonts.hpp"
#include "app.hpp"
#include "cbutton.hpp"
#include "scenario.hpp"
#include "dib.hpp"
#include "fillwind.hpp"
#include "itemwind.hpp"
#include "userint.hpp"
#include "armies.hpp"
#include "dib_util.hpp"
#include "palette.hpp"
#include "scn_img.hpp"
#include "scrnbase.hpp"
#include "to_win.hpp"
#include "wmisc.hpp"
#include "towninfo.hpp"
#include "ds_town.hpp"
#include "bld_dial.hpp"
#include "colours.hpp"
#include "control.hpp"
#include "resstr.hpp"

/*---------------------------------------------------------------
 * Our actual order window
 */

class TownOrderWindow : public WindowBaseND {
    CampaignUserInterface* d_owner;
    HWND d_hParent;
    CampaignWindowsInterface* d_campWind;
    const CampaignData* d_campData;

    ITown d_town;
    BuildItem d_builds[BasicUnitType::HowMany];
    BasicUnitType::value d_basicType;

    DrawDIBDC* d_dib;
    const DrawDIB* d_comboFillDib;

    Boolean d_changed;

     TownBuildImages d_imgLib;

    enum B_ID
    {
        // Buttons for a BuildItem

        IDB_Item,
        IDB_Info,
        IDB_Less,
        IDB_More,
        IDB_Repo,
        IDB_Auto,
        IDB_Number,

        IDB_HowMany,

    };

    enum ID {
        ID_First = 100,
        ID_Info = ID_First,
        ID_MoreInfo,
        ID_Send,
        ID_Cancel,
        ID_Inf,
        ID_Cav = ID_Inf + IDB_HowMany,
        ID_Art = ID_Cav + IDB_HowMany,
        ID_Other = ID_Art + IDB_HowMany,
        ID_UnitTypeListBox = ID_Other + IDB_HowMany,
        ID_Last,
    };

    enum {
      TimerID = 500,
    };

    PixelPoint d_point;

    FillWindow d_fillWind;
    BuildItemInterface* d_buildWind;

    int d_cx;
    int d_cy;

  public:

    TownOrderWindow(CampaignUserInterface* owner, HWND hParent,
        CampaignWindowsInterface* campWind, const CampaignData* campData);
    ~TownOrderWindow()
    {
         delete d_buildWind; d_buildWind = 0;
        selfDestruct();
      if(d_dib)
        delete d_dib;
    }

    void init(ITown town);
    void run(const PixelPoint& p);
    void hide();

  private:
    LRESULT procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
    void onCommand(HWND hwnd, int id, HWND hwndCtl, UINT codeNotify);
    BOOL onCreate(HWND hWnd, CREATESTRUCT* lpCreateStruct);
    BOOL onEraseBk(HWND hwnd, HDC hdc);

    void onDestroy(HWND hWnd)
     {
     }

    UINT onNCHitTest(HWND hwnd, int x, int y);
    void onDrawItem(HWND hwnd, const DRAWITEMSTRUCT* di);
    void onTimer(HWND hwnd, UINT id);

    void enableControls();

    void redrawControl(int id);
    int getTotalBuilds() const;
    void redrawItems();
    void sendBuild();

    bool townIsValid() const;

     bool idToItemID(ID p_id, B_ID& bid, BasicUnitType::value& basicType) const
     {
        int id = p_id;

        id -= ID_Inf;
        if(id < 0)
            return false;

        for(BasicUnitType::value t = BasicUnitType::Infantry; t < BasicUnitType::HowMany; INCREMENT(t))
        {
            if(id < IDB_HowMany)
            {
                basicType = t;
                bid = static_cast<B_ID>(id);
                return true;
            }

            id -= IDB_HowMany;
        }
        return false;
     }

     ID getID(BasicUnitType::value basicType) const
     {
        return static_cast<ID>(ID_Inf + basicType * IDB_HowMany);
     }

     ID getID(BasicUnitType::value basicType, B_ID bid) const
     {
        return static_cast<ID>(getID(basicType) + bid);
     }


     HWND createButton(
            HWND hParent,
            int id,
          DWORD style,
            const DIB* fillDib,
            PixelDimension x, PixelDimension y, PixelDimension w, PixelDimension h,
            const char* text,
            TownBuildImages::Value image);

};


TownOrderWindow::TownOrderWindow(CampaignUserInterface* owner, HWND hParent,
     CampaignWindowsInterface* campWind, const CampaignData* campData) :
  d_owner(owner),
  d_hParent(hParent),
  d_campWind(campWind),
  d_campData(campData),
  d_town(NoTown),
  d_basicType(BasicUnitType::Infantry),
  d_dib(0),
  d_comboFillDib(scenario->getScenarioBkDIB(Scenario::BackPatterns::PaperBackground)),
  d_changed(False),
  d_buildWind(0),
  d_cx(0),
  d_cy(0)
{
    ASSERT(d_hParent);
    ASSERT(d_campData);
    ASSERT(d_comboFillDib);

   /*
    * Calculate sizes and Positions
    */

   const int yBorder = 8;
   const int outerXBorder = 6 + 2;
   const int innerXBorder = 4;  // 6;

   // int itemX = xBorder;
   int itemHeight = ScreenBase::y(20);

   int itemButtonX = outerXBorder;
   int itemButtonY = 0;     // 2;
   int itemButtonHeight = itemHeight;      // - 4;
   int itemButtonWidth = itemButtonHeight;

   int itemTextX = itemButtonX + itemButtonWidth;
   int itemTextY = 0;
   int itemTextWidth = ScreenBase::x(120);     // ScreenBase::x(90);
   int itemTextHeight = itemHeight;

   int itemCtrlHeight = itemHeight - 4 - 8;
   int itemCtrlWidth = itemCtrlHeight;
   int itemCtrlX = itemTextX + itemTextWidth + innerXBorder;
   int itemCtrlY = (itemHeight - itemCtrlHeight) / 2;

   // int itemWidth = itemButtonWidth + itemTextWidth + 3 * itemCtrlWidth + 4;

   int autoX = itemCtrlX + 3 * itemCtrlWidth;   // repoX + repoWidth + innerXBorder;
   int autoHeight = itemCtrlHeight;
   int autoWidth = autoHeight;
   int autoY = (itemHeight - autoHeight) / 2;

   // int repoX = xBorder + itemWidth + xBorder;
   int repoX = autoX + autoWidth + innerXBorder; // itemCtrlX + 3 * itemCtrlWidth + xBorder;
   int repoHeight = itemButtonHeight;
   int repoWidth = repoHeight;
   int repoY = (itemHeight - repoHeight) / 2;


   int totalWidth = repoX + repoWidth + outerXBorder;    // autoX + autoWidth + xBorder;

//   int buttonWidth = ScreenBase::x(25);
   int buttonWidth = ScreenBase::x(48);      // widened to fit "Abbrechen" (German for "Cancel")
   int buttonHeight = ScreenBase::y(10);

   int titleX = outerXBorder;
   int titleY = yBorder;
   int titleWidth = totalWidth - titleX - outerXBorder;
   int titleHeight = ScreenBase::y(50);

   int itemY = titleY + titleHeight + yBorder;
   int buttonY = itemY + (itemHeight + yBorder) * BasicUnitType::HowMany + yBorder;

   int totalHeight = buttonY + buttonHeight + yBorder;

   d_cx = totalWidth;
   d_cy = totalHeight;


    HWND hWnd = createWindow(
      0,
        NULL,
      WS_POPUP | WS_CLIPSIBLINGS,
      0, 0, totalWidth, totalHeight,
      d_hParent, NULL
  );

  const DIB* fill = scenario->getScenarioBkDIB(Scenario::BackPatterns::ButtonBackground);
  const DIB* buttonFill = scenario->getScenarioBkDIB(Scenario::BackPatterns::ButtonBackground);

  /*
   * Create Child Windows
   */

  // Title

  HWND hTitle = addOwnerDrawStatic(hWnd, ID_Info, titleX, titleY, titleWidth, titleHeight);

  // Items

  for(BasicUnitType::value t = BasicUnitType::Infantry; t < BasicUnitType::HowMany; INCREMENT(t))
  {
    // TownItemWindows* itemWindow = &d_itemWindows[t];

    // create item

    ID baseID = getID(t);
    int y = itemY + t * (itemHeight + yBorder);

    HWND hItem = addOwnerDrawStatic(
        hWnd,
        baseID + IDB_Info,
        itemTextX, y + itemTextY, itemTextWidth, itemHeight);
    // itemWindow->d_itemWindow = new SubClassStatic(hItem);

    // itemWindow->d_hIcon =
    HWND hButton = createButton(
        hWnd,
        static_cast<ID>(baseID + IDB_Item),
        BS_PUSHBUTTON | WS_CHILD | WS_VISIBLE,
        fill,
        itemButtonX, y + itemButtonY, itemButtonWidth, itemButtonHeight,
        0,
        d_imgLib.getUnitTypeIndex(t));
    SetWindowPos(hButton, hItem, 0,0,0,0, SWP_NOSIZE | SWP_NOMOVE);

    // itemWindow->d_hLess =
    hButton = createButton(
        hWnd,
        static_cast<ID>(baseID + IDB_Less),
        BS_PUSHBUTTON | WS_CHILD | WS_VISIBLE,
        buttonFill,
        itemCtrlX, y + itemCtrlY, itemCtrlWidth, itemCtrlHeight,
        0,
        TownBuildImages::UnpressedMinus);
    SetWindowPos(hButton, hItem, 0,0,0,0, SWP_NOSIZE | SWP_NOMOVE);

    // itemWindow->d_hMore =
    hButton = createButton(
        hWnd,
        static_cast<ID>(baseID + IDB_More),
        BS_PUSHBUTTON | WS_CHILD | WS_VISIBLE,
        buttonFill,
        itemCtrlX + itemCtrlWidth*2, y + itemCtrlY, itemCtrlWidth, itemCtrlHeight,
        0,
        TownBuildImages::UnpressedPlus);
    SetWindowPos(hButton, hItem, 0,0,0,0, SWP_NOSIZE | SWP_NOMOVE);

    // itemWindow->d_hQuantity =
    hButton = addOwnerDrawStatic(
        hWnd,
        static_cast<ID>(baseID + IDB_Number),
        itemCtrlX + itemCtrlWidth, y + itemCtrlY, itemCtrlWidth, itemCtrlHeight);
    SetWindowPos(hButton, hItem, 0,0,0,0, SWP_NOSIZE | SWP_NOMOVE);

    createButton(
        hWnd,
        static_cast<ID>(baseID + IDB_Repo),
        BS_PUSHBUTTON | WS_CHILD | WS_VISIBLE | BS_AUTOCHECKBOX,
        buttonFill,
        repoX, y + repoY, repoWidth, repoHeight,
        0,
        TownBuildImages::UnpressedPool);

    createButton(
        hWnd,
        static_cast<ID>(baseID + IDB_Auto),
        BS_PUSHBUTTON | WS_CHILD | WS_VISIBLE | BS_AUTOCHECKBOX,
        buttonFill,
        autoX, y + autoY, autoWidth, autoHeight,
        0,
        TownBuildImages::UnpressedAuto);
  }

  // Buttons

  const char* str_Info = InGameText::get(IDS_UOP_Info);
  const char* str_Send = InGameText::get(IDS_UOP_Send);
  const char* str_Cancel = InGameText::get(IDS_UOP_Cancel);

  createButton(
        hWnd,
        ID_MoreInfo,
        BS_PUSHBUTTON | WS_CHILD | WS_VISIBLE,
        buttonFill,
        outerXBorder, buttonY, buttonWidth, buttonHeight,
        str_Info,
        TownBuildImages::NoImage);

  int x1 = totalWidth - outerXBorder;
  int x = x1 - buttonWidth;

  createButton(
        hWnd,
        ID_Send,
        BS_PUSHBUTTON | WS_CHILD | WS_VISIBLE,
        buttonFill,
        x, buttonY, buttonWidth, buttonHeight,
        str_Send,
        TownBuildImages::NoImage);

  x1 = x - innerXBorder;
  x = x1 - buttonWidth;

  createButton(
        hWnd,
        ID_Cancel,
        BS_PUSHBUTTON | WS_CHILD | WS_VISIBLE,
        buttonFill,
        x, buttonY, buttonWidth, buttonHeight,
        str_Cancel,
        TownBuildImages::NoImage);

    SIZE s;
    s.cx = itemTextWidth;   // itemWidth;
    s.cy = itemHeight;
    ASSERT(!d_buildWind);
    d_buildWind = new BuildItemInterface(hWnd, ID_UnitTypeListBox, d_campData, s);

}

LRESULT TownOrderWindow::procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
  switch(msg)
  {
    HANDLE_MSG(hWnd, WM_CREATE,  onCreate);
    HANDLE_MSG(hWnd, WM_DESTROY, onDestroy);
    HANDLE_MSG(hWnd, WM_COMMAND, onCommand);
    HANDLE_MSG(hWnd, WM_NCHITTEST, onNCHitTest);
    HANDLE_MSG(hWnd, WM_ERASEBKGND, onEraseBk);
    HANDLE_MSG(hWnd, WM_DRAWITEM, onDrawItem);
    HANDLE_MSG(hWnd, WM_TIMER, onTimer);

    default:
      return defProc(hWnd, msg, wParam, lParam);
  }
}


BOOL TownOrderWindow::onCreate(HWND hWnd, CREATESTRUCT* lpCreateStruct)
{
  int cx = lpCreateStruct->cx;
  int cy = lpCreateStruct->cy;

  /*
   * init background dib
   */

  d_fillWind.allocateDib(cx, cy);
  d_fillWind.setFillDib(scenario->getScenarioBkDIB(Scenario::BackPatterns::CherryWoodBackground));
  d_fillWind.setBorderColors(scenario->getBorderColors());

  return TRUE;
}

void TownOrderWindow::onTimer(HWND hwnd, UINT id)
{
  ASSERT(id == TimerID);

//   SetWindowPos(getHWND(), HWND_TOPMOST, d_point.getX(), d_point.getY(), 0, 0, SWP_NOSIZE | SWP_SHOWWINDOW);
//
//   BOOL result = KillTimer(hwnd, id);
//   ASSERT(result);

   if(!townIsValid())
      hide();
}


void TownOrderWindow::onCommand(HWND hwnd, int id, HWND hwndCtl, UINT codeNotify)
{
   BasicUnitType::value basicType;
    B_ID bid;

    if(idToItemID(static_cast<ID>(id), bid, basicType))
    {
        switch(bid)
        {
            case IDB_Item:
            case IDB_Info:
              if(d_town != NoTown)
              {
                  const Town& t = d_campData->getTown(d_town);
                  const Province& prov = d_campData->getProvince(t.getProvince());

                    d_basicType = basicType;

                  RECT r;
                  GetWindowRect(hwndCtl, &r);
                  PixelPoint p(r.left, r.top);

                  BuildItemInterface::Data data;
                  data.d_nation = prov.getNationality();
                  data.d_what = d_basicType;
                  data.d_currentType = d_builds[d_basicType].getWhat();

                   ASSERT(d_buildWind);
                  d_buildWind->init(data);
                  d_buildWind->show(p);
              }
              break;

            case IDB_Less:
              if(d_town != NoTown)
              {
                  BuildItem& b = d_builds[basicType];

                  // ASSERT(b.getQuantity() > 1);
                  ASSERT(b.getQuantity() > 0);
                  ASSERT(b.getWhat() < d_campData->getMaxUnitType());

                  b.setQuantity(b.getQuantity()-1);

                    if(b.getQuantity() == 0)
                    {
                        b.setResources(0,0);
                    }
                    else
                    {
                      const UnitTypeItem& uit = d_campData->getUnitType(b.getWhat());

                      AttributePoints tRes = 0;
                      AttributePoints tMan = 0;

                      uit.getTotalResource(tMan, tRes);
                      b.reduceNeeded(tMan, tRes);
                    }


                  d_changed = True;

                  enableControls();
                  redrawItems();
                  redrawControl(ID_Info);
              }
              break;

            case IDB_More:
              if(d_town != NoTown)
              {
                  BuildItem& b = d_builds[basicType];

                  ASSERT(b.getQuantity() > 0);
                  ASSERT(b.getWhat() < d_campData->getMaxUnitType());

                    const UnitTypeItem& uit = d_campData->getUnitType(b.getWhat());

                  AttributePoints tRes = 0;
                  AttributePoints tMan = 0;

                  uit.getTotalResource(tMan, tRes);
                  b.setResources(b.getManpower()+tMan, b.getResource()+tRes);

                  b.setQuantity(b.getQuantity()+1);

                  d_changed = True;

                  enableControls();
                  redrawItems();
                  redrawControl(ID_Info);
              }
              break;

            case IDB_Repo:
              if(codeNotify == BN_CLICKED)
              {
                  if(d_town != NoTown)
                  {
                     BuildItem& b = d_builds[basicType];

                     b.repoPool(!b.repoPool());

                     CustomButton tb(hwndCtl);
                     tb.setCheck(b.repoPool());

                     d_changed = True;
                     enableControls();

                  }
              }
              break;

            case IDB_Auto:
              if(codeNotify == BN_CLICKED)
              {
                  if(d_town != NoTown)
                  {
                     BuildItem& b = d_builds[basicType];

                     b.toggleAutoBuild();

                     CustomButton tb(hwndCtl);
                     tb.setCheck(b.getAuto());

                     d_changed = True;
                     enableControls();

                  }
              }
              break;

            case IDB_Number:
                break;
        }
    }
    else
    switch(id)
    {
    case ID_UnitTypeListBox:
    {
      ASSERT(d_basicType < BasicUnitType::HowMany);

      ASSERT(d_buildWind);
      UnitType t = d_buildWind->currentType();
      BuildItem& item = d_builds[d_basicType];

      Boolean redraw = True;
      if(t != NoUnitType)
      {
        if(t != item.getWhat())
        {
          ASSERT(t < d_campData->getMaxUnitType());
          const UnitTypeItem& uit = d_campData->getUnitType(t);

          AttributePoints tRes = 0;
          AttributePoints tMan = 0;

          uit.getTotalResource(tMan, tRes);
          item.setResources(tMan, tRes);

          item.setQuantity(1);
          item.setWhat(t);
          item.setChanged();
        }
        else
          redraw = False;
      }
      else
        item.reset();

      if(redraw)
      {
        redrawItems();
        redrawControl(ID_Info);
        d_changed = True;
      }

      enableControls();

      break;
    }

    case ID_Cancel:
      hide();
      break;

    case ID_Send:
      sendBuild();
      hide();
      break;

//  case ID_Apply:
//       sendBuild();
//       d_changed = False;
//       break;

    case ID_MoreInfo:
    {
      POINT pt;
      pt.x = 0;
      pt.y = 0;

      ClientToScreen(hwndCtl, &pt);

//    PixelPoint p(pt.x, pt.y);
//    d_owner->runInfo(p);
      d_owner->infoTown(d_town);

      break;
    }
  }
}

void TownOrderWindow::onDrawItem(HWND hwnd, const DRAWITEMSTRUCT* di)
{
  ASSERT(d_comboFillDib);

  const int cx = di->rcItem.right - di->rcItem.left;
  const int cy = di->rcItem.bottom - di->rcItem.top;

  DIB_Utility::allocateDib(&d_dib, cx, cy);
  ASSERT(d_dib);

  d_dib->setBkMode(TRANSPARENT);

  // fill with bk
  d_dib->rect(0, 0, cx, cy, d_comboFillDib);

  // draw a border
  const ColourIndex ci = d_dib->getColour(PALETTERGB(0, 0, 0));
  d_dib->frame(0, 0, cx, cy, ci);

  HPALETTE oldPal = SelectPalette(di->hDC, Palette::get(), FALSE);
  RealizePalette(di->hDC);


  if(d_town != NoTown)
  {
      if(di->CtlID == ID_Info)
        {
            TownInfoData td(d_town, d_campData, d_dib, PixelRect(1,1,cx-1,cy-1));
          td.drawBuildInfoDib(d_builds);
        }
        else if( (di->CtlID >= ID_Inf) && (di->CtlID < (ID_Other + IDB_HowMany) ))
        {
         BasicUnitType::value basicType;
            B_ID id;

            bool result = idToItemID(static_cast<ID>(di->CtlID), id, basicType);
            ASSERT(result);

            switch(id)
            {
                case IDB_Info:
                {
                    // TownInfoData td(d_town, d_campData, d_dib,
                    //     PixelRect(cy, 1, cx - 1 - 4*cy, cy-1));

                    TownInfoData td(d_town, d_campData, d_dib, PixelRect(1,1,cx-1,cy-1));
               td.drawBuildTypeInfoDib(d_builds, basicType, false);
                    break;
                }

                case IDB_Number:
                {
                    if(d_builds[basicType].isEnabled())
                    {
                        COLORREF color = PaletteColours::White;
                        d_dib->rect(1, 1, cx-2, cy-2, d_dib->getColour(color));
                    }

                    char buf[10];
                    wsprintf(buf, "%d", static_cast<int>(d_builds[basicType].getQuantity()));

                    LogFont lf;
                    lf.height(minimum(cy - 4, ScreenBase::y(12)));
                    lf.weight(FW_MEDIUM);
                    lf.face(scenario->fontName(Font_Bold));
                    lf.italic(false);
                    lf.underline(false);

                    Fonts font;
                    font.set(d_dib->getDC(), lf);

                    d_dib->setTextAlign(TA_CENTER | TA_TOP);
                    d_dib->setTextColor(PaletteColours::Black);

                    SIZE s;
                    BOOL result = GetTextExtentPoint32(d_dib->getDC(), buf, lstrlen(buf), &s);
                    ASSERT(result);

                    int x = cx / 2;
                    int y = (cy - s.cy) / 2;

                    wTextOut(d_dib->getDC(), x, y, buf);

                    break;
                }
            }
      }
  }

  CampaignUserInterface::blitDibToControl(d_dib, di);

  SelectPalette(di->hDC, oldPal, FALSE);
}

BOOL TownOrderWindow::onEraseBk(HWND hwnd, HDC hdc)
{
  d_fillWind.paint(hdc);
  return True;
}

UINT TownOrderWindow::onNCHitTest(HWND hwnd, int x, int y)
{
  return HTCAPTION;
}

int TownOrderWindow::getTotalBuilds() const
{
  int total = 0;

  for(int i = 0; i < BasicUnitType::HowMany; i++)
    total += d_builds[i].getQuantity();

  return total;
}

void TownOrderWindow::redrawItems()
{
  for(BasicUnitType::value v = BasicUnitType::Infantry; v < BasicUnitType::HowMany; INCREMENT(v))
  {
    HWND h = GetDlgItem(getHWND(), getID(v, IDB_Info));
    ASSERT(h);
    InvalidateRect(h, NULL, FALSE);

     h = GetDlgItem(getHWND(), getID(v, IDB_Number));
    ASSERT(h);
    InvalidateRect(h, NULL, FALSE);
  }
}

void TownOrderWindow::sendBuild()
{
  if(d_town != NoTown)
  {
    if(townIsValid())
    {
       for(BasicUnitType::value i = BasicUnitType::Infantry; i < BasicUnitType::HowMany; INCREMENT(i))
       {
         if(d_builds[i].changed())
         {
           d_builds[i].changed(False);
           TownOrder::send(d_town, i, d_builds[i]);
         }
       }
    }
  }
}

void TownOrderWindow::redrawControl(int id)
{
  ASSERT(id < ID_Last);

  HWND h = GetDlgItem(hWnd, id);
  ASSERT(h);

  InvalidateRect(h, NULL, FALSE);
  UpdateWindow(h);
}

void TownOrderWindow::enableControls()
{
  /*
   * Enable more, less, and auto buttons for each type
   */

  for(BasicUnitType::value t = BasicUnitType::Infantry; t < BasicUnitType::HowMany; INCREMENT(t))
  {
    const BuildItem& item = d_builds[t];

    {
      // CustomButton b(d_itemWindows[t].d_hLess);
      CustomButton b(getHWND(), getID(t, IDB_Less));
      // b.enable( (item.getQuantity() > 1) );
      b.enable(item.isEnabled() && (item.getQuantity() != 0) );
    }

    {
      // CustomButton b(d_itemWindows[t].d_hMore);
      CustomButton b(getHWND(), getID(t, IDB_More));
      // b.enable( (item.isEnabled()) );
      b.enable(item.isEnabled() && (item.getQuantity() < 10));
    }

    {
      CustomButton b(getHWND(), getID(t, IDB_Auto));
      b.enable( (item.isEnabled()) );
    }

    {
      CustomButton b(getHWND(), getID(t, IDB_Repo));
      b.enable( (item.isEnabled()) );
    }
  }

  /*
   * enable send, apply buttons
   */

  {
    CustomButton b(getHWND(), ID_Send);
    b.enable(d_changed);
  }

//  {
//     CustomButton b(getHWND(), ID_Apply);
//     b.enable(d_changed);
//   }
}

void TownOrderWindow::init(ITown town)
{
  ASSERT(town != NoTown);
  d_changed = False;
  d_town = town;

  /*
   * Get province builds
   */

  if(d_town != NoTown)
  {
    const Town& t = d_campData->getTown(d_town);
    const Province& p = d_campData->getProvince(t.getProvince());

//  for(int i = 0; i < BasicUnitType::HowMany; i++)
   for(BasicUnitType::value i = BasicUnitType::Infantry; i < BasicUnitType::HowMany; INCREMENT(i))
    {
      const BuildItem& item = p.getBuild(i);

      if(item.isEnabled())
      {
        d_builds[i] = item;
      }
      else
        d_builds[i].reset();

      CustomButton bAuto(getHWND(), getID(i, IDB_Auto));
      bAuto.setCheck(d_builds[i].getAuto());
      CustomButton bRepo(getHWND(), getID(i, IDB_Repo));
      bRepo.setCheck(d_builds[i].repoPool());

    }
  }
}

void TownOrderWindow::run(const PixelPoint& p)
{
  const int offset = (ScreenBase::dbY() * 10) / ScreenBase::baseY();

  /*
   * make sure entire window is within mainwindows rect
   */

  PixelRect r;
  GetWindowRect(APP::getMainHWND(), &r);

  int x = p.getX();
  int y = p.getY();

  x = minimum(p.getX(), maximum(0, r.right() - d_cx));
  y = minimum(p.getY(), maximum(0, r.bottom() - d_cy));

  d_point.set(x, y);

  enableControls();

  int result = SetTimer(getHWND(), TimerID, 150, NULL);
  ASSERT(result);

  SetWindowPos(getHWND(), HWND_TOPMOST, d_point.getX(), d_point.getY(), 0, 0, SWP_NOSIZE | SWP_SHOWWINDOW);
}

void TownOrderWindow::hide()
{
   BOOL result = KillTimer(getHWND(), TimerID);
   ASSERT(result);

    show(false);
    d_owner->cancelOrder();
}

HWND TownOrderWindow::createButton(
            HWND hParent,
            int id,
          DWORD style,
            const DIB* fillDib,
            PixelDimension x, PixelDimension y, PixelDimension w, PixelDimension h,
            const char* text,
            TownBuildImages::Value image)
{
    HWND hButton = CreateWindow(CUSTOMBUTTONCLASS, text,
             style,
                x, y, w, h,
             hParent,
                (HMENU)id,
                APP::instance(),
                NULL);

    ASSERT(hButton != 0);

    CustomButton cb(hButton);
    cb.setFillDib(fillDib);
    cb.setBorderColours(scenario->getBorderColors());

    if(style & BS_AUTOCHECKBOX)
    {
        if(image == TownBuildImages::NoImage)
           cb.setCheckBoxImages(ScenarioImageLibrary::get(ScenarioImageLibrary::CheckButtons));
        else
        {
            cb.setCheckBoxImages(d_imgLib.lib(), image, image + 1);
        }
    }
    else if(image != TownBuildImages::NoImage)
    {
        cb.setButtonImage(d_imgLib.lib(), image);
    }

    /*
     * Set up font
     */

    if(text)
    {
       const int fontCY = ScreenBase::y( (style & BS_AUTOCHECKBOX) ? 7 : 8);

       LogFont lf;
       lf.height(fontCY);
       lf.weight(FW_MEDIUM);
       lf.face(scenario->fontName(Font_Normal));

       Font font;
       font.set(lf);
       cb.setFont(font);
    }

    return hButton;
}


/*
 * Is the current location still valid
 */

bool TownOrderWindow::townIsValid() const
{
    const Town& t = d_campData->getTown(d_town);
    const Province& p = d_campData->getProvince(t.getProvince());
    return (GamePlayerControl::canControl(p.getSide()));
}

/*--------------------------------------------------------
 *  Client access
 */

TownOrder_Int::TownOrder_Int(CampaignUserInterface* owner, HWND hParent,
    CampaignWindowsInterface* campWind, const CampaignData* campData) :
  d_orderWind(new TownOrderWindow(owner, hParent, campWind, campData))
{
  ASSERT(d_orderWind);
}

TownOrder_Int::~TownOrder_Int()
{
  // destroy();
  delete d_orderWind;
}

void TownOrder_Int::init(ITown town)
{
  if(d_orderWind)
    d_orderWind->init(town);
}

void TownOrder_Int::run(const PixelPoint& p)
{
  if(d_orderWind)
    d_orderWind->run(p);
}

void TownOrder_Int::hide()
{
  if(d_orderWind)
    d_orderWind->hide();
}


HWND TownOrder_Int::getHWND() const
{
  return d_orderWind->getHWND();
}


/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.2  2001/06/18 08:25:45  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
