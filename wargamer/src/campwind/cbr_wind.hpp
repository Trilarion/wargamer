/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef CBR_WIND_HPP
#define CBR_WIND_HPP

#include "cwdll.h"
#include "wg_msg.hpp"

/*
 * CampaignBattle result window.
 * Displays final results of a strategic battle
 */


class CampaignData;
struct BattleInfoData;


class BattleResultMsg : public WargameMessage::WaitableMessage 
{
		const BattleInfoData& d_data;	// info for battle in progress
	public:
		BattleResultMsg(const BattleInfoData& data) : d_data(data) { }
		CAMPWIND_DLL void run();
};

class CBattleResultInterface {
  public:
	 static void create(HWND hParent, const CampaignData* campData);
	 static void destroy();
};


#endif
