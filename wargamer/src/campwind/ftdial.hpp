/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef FTDIAL_HPP
#define FTDIAL_HPP

#ifndef __cplusplus
#error ftdial.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *
 * Started by Paul Sample
 *
 *----------------------------------------------------------------------
 *
 *	Find Town and Leader Dialog
 *
 * This should be rewritten to hide the implementation details from
 * other modules
 *
 *----------------------------------------------------------------------
 */

#include "dialog.hpp"
#include "gamedefs.hpp"
#include "cpdef.hpp"
#include "winctrl.hpp"			// Needed for ImageList

class CampaignWindowsInterface;
class MapWindow;
class PixelPoint;

/*
 * Utility Class for common code shared by FindLeader and FindTown
 */

class FindDial {
  protected:
	CampaignWindowsInterface* d_campWind;
	MapWindow* d_mapWind;
	HWND d_hwndParent;
	ImageList images;
	int currentItem;
	int lastItem;
  public:
	FindDial(CampaignWindowsInterface* owner, MapWindow* mapWind, HWND parent);
	virtual ~FindDial() {}

	VOID APIENTRY findDialPopupMenu(HWND hwnd, PixelPoint& pt, int id);
	void findItem(HWND hwnd, const char* text);
};


class FindTownDial : public ModelessDialog, public FindDial
	//, public CampaignWindow 
{
    static const DWORD ids[];
	 ITown iTown;
  public:
	 FindTownDial(CampaignWindowsInterface* owner, MapWindow* mapWind, HWND parent);
	 ~FindTownDial();
  private:
	 BOOL procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
	 void onCommand(HWND hWnd, int id, HWND hwndCtl, UINT codeNotify);
	 BOOL onInitDialog(HWND hwnd, HWND hwndFocus, LPARAM lParam);
	 void onDestroy(HWND hwnd);
	 void onClose(HWND hwnd);
    BOOL onHelp(HWND hwnd, LPHELPINFO lparam);
	 void onContextMenu(HWND hwnd, HWND hwndCtl, int x, int y);
	 LRESULT onNotify(HWND hWnd, int id, NMHDR* lpNMHDR);

	 void addItems(HWND hwnd);
	 void onOK(HWND hwnd);
	 void onFindTown();
};

class FindLeaderDial : public ModelessDialog, public FindDial
	// , public CampaignWindow 
{
    static const DWORD ids[];
	 ILeader iLeader;
  public:
	 FindLeaderDial(CampaignWindowsInterface* owner, MapWindow* mapWind, HWND parent);
	 ~FindLeaderDial();
  private:
	 BOOL procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
	 void onCommand(HWND hWnd, int id, HWND hwndCtl, UINT codeNotify);
	 BOOL onInitDialog(HWND hwnd, HWND hwndFocus, LPARAM lParam);
	 void onDestroy(HWND hwnd);
	 void onClose(HWND hwnd);
    BOOL onHelp(HWND hwnd, LPHELPINFO lparam);
	 void onContextMenu(HWND hwnd, HWND hwndCtl, int x, int y);
	 LRESULT onNotify(HWND hWnd, int id, NMHDR* lpNMHDR);

	 void addItems(HWND hwnd);
	 void onOK(HWND hwnd);
	 void onFindLeader();
};


#endif /* FTDIAL_HPP */

