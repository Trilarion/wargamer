/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
#include "stdinc.hpp"
#include "nm_wind.hpp"
#include "gsecwind.hpp"
#include "wind.hpp"
#include "dib.hpp"
#include "dib_util.hpp"
#include "cwin_int.hpp"
#include "campdint.hpp"
#include "app.hpp"
#include "palette.hpp"
#include "wmisc.hpp"
#include "scrnbase.hpp"
// #include "generic.hpp"
#include "palwind.hpp"
#include "fonts.hpp"
#include "scenario.hpp"
#include "scn_res.h"
#include "imglib.hpp"
#include "cbutton.hpp"
#include "cclock.hpp"
#include "scn_img.hpp"
#include "resdef.h"
#include "tooltip.hpp"
#include "simpstr.hpp"
#include "bargraph.hpp"
#include "colours.hpp"
#include "resstr.hpp"

#ifdef DEBUG
#include "logwin.hpp"
#endif

/*-----------------------------------------------
 * Local utils
 */

class NSUtil {
public:
  static HWND createButton(HWND hParent, const char* text, int id, Boolean autoCheck = False, int imgID = -1);
};

HWND NSUtil::createButton(HWND hParent, const char* text, int id, Boolean autoCheck, int imgID)
{
  DWORD style = (autoCheck) ? WS_CHILD | BS_AUTOCHECKBOX | BS_PUSHLIKE :
       WS_CHILD | BS_PUSHBUTTON;

  HWND hButton = CreateWindow(CUSTOMBUTTONCLASS, text,
            style,
            0, 0, 0, 0,
            hParent, (HMENU)id, APP::instance(), NULL);

  ASSERT(hButton != 0);

  CustomButton cb(hButton);
  // cb.setFillDib(scenario->getScenarioBkDIB(Scenario::BackPatterns::WoodBackground));
  cb.setFillDib(scenario->getScenarioBkDIB(Scenario::BackPatterns::ButtonBackground));
  cb.setBorderColours(scenario->getBorderColors());

  if(imgID != -1)
    cb.setButtonImage(ScenarioImageLibrary::get(ScenarioImageLibrary::CToolBarImages), imgID);

  return hButton;
}

/*---------------------------------------------------------------------
 *  National Morale section. 1 per side. Includes:
 *  1. Morale barchart for each nationality (of that side)
 *  2. Horz. scroll buttons for scroll nations (if need be, i.e.
 *     not enough screen space)
 */

class NM_Section : public GSecWind {
    const CampaignData* d_campData;
    Side d_side;

    int d_scrollPos;
    int d_nNations;
    int d_nNationsShown;

    struct SideButtons {
      HWND d_hLeft;
      HWND d_hRight;

      SideButtons() :
        d_hLeft(0),
        d_hRight(0) {}
    } d_buttons;

  public:
    enum ID {
       ID_First = 0,
       ID_LeftScroll = ID_First,
       ID_RightScroll,

       ID_HowMany
    };

  private:
    enum Flags {
      ShowButtons  = 0x01,
      ButtonsShown = 0x02
    };

    UBYTE d_flags;

  public:
    NM_Section(HWND hParent, const CampaignData* campData, Side side, const DrawDIB* fillDib);
    ~NM_Section() {}

    // virtual functions from GSecWind
    void drawDib();
    void enableControls();

  private:
    LRESULT procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
    void onCommand(HWND hwnd, int id, HWND hwndCtl, UINT codeNotify);
    BOOL onCreate(HWND hWnd, CREATESTRUCT* lpCreateStruct);
    void onDestroy(HWND hWnd);

    // other stuff
    void showButtons(const int buttonX, const int buttonCX, const int buttonCY, const int xOffset);
    int nationsPerSide(Side s);
};

NM_Section::NM_Section(HWND hParent, const CampaignData* campData, Side side, const DrawDIB* fillDib) :
  GSecWind(fillDib),
  d_campData(campData),
  d_side(side),
  d_scrollPos(0),
  d_nNations(0),
  d_nNationsShown(0),
  d_flags(0)
{
  HWND h = createWindow(
      0,
      // GenericNoBackClass::className(),
        NULL,
      WS_CHILD | WS_CLIPSIBLINGS,
      0, 0, 0, 0,
      hParent, NULL   // , APP::instance()
  );

  ASSERT(h != NULL);
}

static const char* s_leftScrollTxt = "<-";
static const char* s_rightScrollTxt = "->";

static const char* s_buttonTxt[NM_Section::ID_HowMany] = {
  s_leftScrollTxt,
  s_rightScrollTxt
};

LRESULT NM_Section::procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{

  switch(msg)
  {
    HANDLE_MSG(hWnd, WM_CREATE, onCreate);
    HANDLE_MSG(hWnd, WM_DESTROY, onDestroy);
    HANDLE_MSG(hWnd, WM_PAINT, onPaint);
    HANDLE_MSG(hWnd, WM_COMMAND, onCommand);

    default:
      return defProc(hWnd, msg, wParam, lParam);
  }
}

BOOL NM_Section::onCreate(HWND hwnd, CREATESTRUCT* cs)
{
  /*
   * Create scroll buttons
   */

  ASSERT(d_side != SIDE_Neutral);

  for(int id = 0; id < ID_HowMany; id++)
  {
    HWND h = NSUtil::createButton(hwnd, s_buttonTxt[id], id);
    ASSERT(h);
  }

  return TRUE;
}

void NM_Section::onCommand(HWND hwnd, int id, HWND hwndCtl, UINT codeNotify)
{
  switch(id)
  {
    case ID_LeftScroll:
      d_scrollPos = maximum(0, d_scrollPos - 1);
#ifdef DEBUG
      g_logWindow.printf("scrollPos1 = %d", d_scrollPos);
#endif
      redraw();
      enableControls();
      break;

    case ID_RightScroll:
    {
      d_scrollPos = minimum((d_nNations - d_nNationsShown) + 1, d_scrollPos + 1);
#ifdef DEBUG
      g_logWindow.printf("scrollPos1 = %d", d_scrollPos);
#endif
      redraw();
      enableControls();
      break;
    }
  }
}

void NM_Section::onDestroy(HWND hwnd)
{
}

int NM_Section::nationsPerSide(Side s)
{
  ASSERT(s != SIDE_Neutral);

  int nNations = 0;
  for(Nationality n = 0; n < scenario->getNumNations(); n++)
  {
    if( (d_campData->getNationAllegiance(n) == s) )
    {
      nNations++;
    }
  }

  return nNations;
}

void NM_Section::drawDib()
{
  /*
   * Get current size of client area
   */

  const int dbX = ScreenBase::dbX();
  const int dbY = ScreenBase::dbY();
  const int baseX = ScreenBase::baseX();
  const int baseY = ScreenBase::baseY();
  const int startX = ((2 * dbX) / baseX) + (2 * CustomBorderWindow::ThinBorder);
  const int startY = 0; //((1 * dbY) / baseY) + (2 * CustomBorderWindow::ThinBorder);
  const int sideNameCX = (75 * dbX) / baseX;
  const int xOffset = ((2 * dbX) / baseX);
  const int buttonCX = (20 * dbX) / baseX;
  const int buttonCY = cy() - ((3 * dbX) / baseX);
  const int rButtonOffsetX = (2 * buttonCX) + (2 * xOffset);
  const int buttonX = cx() - (rButtonOffsetX + (2 * xOffset));
  const int nBargraphCX = (52 * dbX) / baseX;
  const int nBargraphCY = (7 * dbY) / baseY;

  /*
   * Allocate dib
   */

  ASSERT(dib());
  dib()->setBkMode(TRANSPARENT);

  /*
   * Get fonts
   */

  // large font
  LogFont lf;
  lf.height((9 * dbY) / baseY);
  lf.weight(FW_MEDIUM);
  lf.face(scenario->fontName(Font_Decorative));

  Font lFont;
  lFont.set(lf);

  // small font
  lf.height((7 * dbY) / baseY);
  lf.italic(false);

  Font sFont;
  sFont.set(lf);

  int x = startX;
  int y = startY;

  dib()->rect(0, 0, cx(), cy(), fillDib());

  /*
   * put 'Morale - Text'
   */

  HFONT oldFont = dib()->setFont(lFont);
  // COLORREF oldColor = dib()->setTextColor(scenario->getSideColour(d_side));
  COLORREF oldColor = dib()->setTextColor(scenario->getColour("MenuText"));
  UINT oldAlign = dib()->setTextAlign(TA_RIGHT | TA_TOP);
  ASSERT(oldAlign != GDI_ERROR);

  static Boolean gotMetrics = False;
  static TEXTMETRIC tm;
  if(!gotMetrics)
  {
    GetTextMetrics(dib()->getDC(), &tm);
    gotMetrics = True;
  }

  // TODO: get from resource
  // const char* text = "Morale";
  ResString moraleText(IDS_MORALE);
  const char* text = moraleText.c_str();

  int workingY = y + ((cy() - tm.tmHeight) / 2);
  // wTextPrintf(dib()->getDC(), x, workingY, sideNameCX, "%s %s",
  //      scenario->getSideName(d_side), text);
  wTextPrintf(dib()->getDC(), x + sideNameCX, workingY, sideNameCX, "%s %s",
       scenario->getSideName(d_side), text);


  UINT align = dib()->setTextAlign(oldAlign);
  ASSERT(align != GDI_ERROR);

  /*
   * put major nation morale
   */

  dib()->setFont(sFont);
  x += (sideNameCX + xOffset);

  /*
   * First calculate size needed to display flags, and bargraphs
   * If not enough size, diplay scroll buttons
   */

  if(sizeChanged())
  {
    d_nNations = nationsPerSide(d_side);
    int countX = x;

    d_nNationsShown = 0;
    int nNations = d_nNations;
    while(nNations--)
    {
      if((countX + FI_Corps_CX + nBargraphCX + (2 *xOffset)) < buttonX)
      {
        d_nNationsShown++;
        countX += FI_Corps_CX + nBargraphCX + (2 *xOffset);
      }
      else
      {
        d_flags |= ShowButtons;
        d_flags &= ~ButtonsShown;
        break;
      }
    }

    sizeChanged(False);
  }

  if(d_nNationsShown >= d_nNations)
    d_flags &= ~ShowButtons;

  int nSide = 0;
  for(Nationality n = 0; n < scenario->getNumNations(); n++)
  {
    if(d_campData->getNationAllegiance(n) == d_side)
    {
      if( (nSide >= d_scrollPos) || (d_nNationsShown >= d_nNations) )
      {
        /*
         * Calculate size needed to display flag, and bargraph
         */

        const ImageLibrary* il = scenario->getNationFlag(n, True);
        ASSERT(il);

        /*
         * Don't add if not enough width
         */

        if((x + FI_Corps_CX + nBargraphCX + (2 *xOffset)) < buttonX)
        {
          // put flag
          workingY = y + ((cy() - FI_Corps_CY) / 2);
          il->blit(dib(), FI_Corps, x, workingY, True);


          // put bargraph
          x += (FI_Corps_CX + xOffset);
          workingY = y + ((cy() - nBargraphCY) / 2);

          COLORREF burstColors[3] =
          {
            PALETTERGB(255,0,0),       // Red at left
            PALETTERGB(222,214,0),     // Yellow in middle
            PALETTERGB(0,255,0)        // Green on right
            // PALETTERGB(8,132,0)        // Green on right
          };

          BarChartInfo barInfo;
          barInfo.setLocation(x, workingY, nBargraphCX, nBargraphCY);
          barInfo.setValue(d_campData->getNationMorale(n), Attribute_Range);
          barInfo.setColors(3, burstColors);
          barInfo.setBorder(PaletteColours::Black, PaletteColours::LightGrey);
          barInfo.setBackground(PaletteColours::DarkGrey);
          barInfo.draw(dib());

          // dib()->drawBarChart(x, workingY, d_campData->getNationMorale(n), Attribute_Range,
          //     nBargraphCX, nBargraphCY);

          // put nation name
          text = scenario->getNationName(n);
          SIZE s;
          GetTextExtentPoint32(dib()->getDC(), text, lstrlen(text), &s);

          workingY = y + ((cy() - s.cy) / 2);
          int barX = x + ((nBargraphCX - s.cx) / 2);
          barX = maximum(barX + 2, x);

          dib()->drawBargraphText(text, sFont, barX, workingY, nBargraphCX - 2);

          x += (nBargraphCX + (4 * xOffset));
        }
      }

      nSide++;

    }

  }

  /*
   * Clean up
   */

  dib()->setTextColor(oldColor);
  dib()->setFont(oldFont);

  // draw border
  drawBorder();

  // show buttons
  showButtons(buttonX, buttonCX, buttonCY, xOffset);
}

void NM_Section::showButtons(const int buttonX, const int buttonCX, const int buttonCY, const int xOffset)
{

  int y = (cy() - buttonCY) / 2;

  if(d_flags & ShowButtons)
  {
    if(!(d_flags & ButtonsShown))
    {
#ifdef DEBUG
      g_logWindow.printf("Showing Buttons");
#endif

      d_flags |= ButtonsShown;

      int x = buttonX;
      for(int id = 0; id < ID_HowMany; id++)
      {
        HWND h = GetDlgItem(getHWND(), id);
        ASSERT(h);

        SetWindowPos(h, HWND_TOP, x, y, buttonCX, buttonCY, SWP_SHOWWINDOW);
        x += (buttonCX + xOffset);

        // force button to be drawn
        InvalidateRect(h, NULL, FALSE);
      }
    }
  }

  else
  {
#ifdef DEBUG
    g_logWindow.printf("Hiding Buttons");
#endif
    if(d_flags & ButtonsShown)
    {
      for(int id = 0; id < ID_HowMany; id++)
      {
        HWND h = GetDlgItem(getHWND(), id);
        ASSERT(h);

        ShowWindow(h, SW_HIDE);
      }

      d_flags &= ~ButtonsShown;
    }
  }
}

void NM_Section::enableControls()
{
  {
    CustomButton b(getHWND(), ID_LeftScroll);
    b.enable(d_scrollPos > 0);
  }
  {
    CustomButton b(getHWND(), ID_RightScroll);
    b.enable(d_scrollPos < ((d_nNations - d_nNationsShown) + 1));
  }
}

/*---------------------------------------------------------------------
 * Victory section.
 * Displays a victory barchart
 */

class V_Section : public GSecWind {
    const CampaignData* d_campData;

  public:
    V_Section(HWND hParent, const CampaignData* campData, const DrawDIB* fillDib);
    ~V_Section() {}

    // virtual functions from GSecWind
    void drawDib();
    void enableControls() {}

  private:
    LRESULT procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
    BOOL onCreate(HWND hWnd, CREATESTRUCT* lpCreateStruct);
    void onDestroy(HWND hWnd) {}
};

V_Section::V_Section(HWND hParent, const CampaignData* campData, const DrawDIB* fillDib) :
  GSecWind(fillDib),
  d_campData(campData)
{
  HWND h = createWindow(
      0,
      // GenericNoBackClass::className(),
        NULL,
      WS_CHILD | WS_CLIPSIBLINGS,
      0, 0, 0, 0,
      hParent, NULL   // , APP::instance()
  );

  ASSERT(h != NULL);
}

LRESULT V_Section::procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{

  switch(msg)
  {
    HANDLE_MSG(hWnd, WM_CREATE, onCreate);
    HANDLE_MSG(hWnd, WM_DESTROY, onDestroy);
    HANDLE_MSG(hWnd, WM_PAINT, onPaint);

    default:
      return defProc(hWnd, msg, wParam, lParam);
  }
}

BOOL V_Section::onCreate(HWND hwnd, CREATESTRUCT* cs)
{
  return TRUE;
}

void V_Section::drawDib()
{
  /*
   * Get current size of client area
   */

  const int dbX = ScreenBase::dbX();
  const int dbY = ScreenBase::dbY();
  const int baseX = ScreenBase::baseX();
  const int baseY = ScreenBase::baseY();
  const int startX = ((2 * dbX) / baseX) + (2 * CustomBorderWindow::ThinBorder);
  const int sideNameCX = (75 * dbX) / baseX;
  const int xOffset = ((2 * dbX) / baseX);
  const int vBargraphCY = (7 * dbY) / baseY;

  /*
   * Allocate dib
   */

  dib()->setBkMode(TRANSPARENT);

  /*
   * Get fonts
   */

  // large font
  LogFont lf;
  lf.height((9 * dbY) / baseY);
  lf.weight(FW_MEDIUM);
  lf.face(scenario->fontName(Font_Decorative));

  Font lFont;
  lFont.set(lf);

  static TEXTMETRIC tm;
  static Boolean gotMetrics = False;
  if(!gotMetrics)
  {
    GetTextMetrics(dib()->getDC(), &tm);
    gotMetrics = True;
  }

  /*
   * Draw victory level bar
   */

  int y = 0;

  // fill in dib
  dib()->rect(0, 0, cx(), cy(), fillDib());

  // Bugfix: SWG: 28Jul99:
  //   Changed ULONG to LONG because they  begin initialised to -1
  //   and are compared to 0 later on.

  LONG side1Victory = d_campData->getVictoryLevel(0);
  LONG side2Victory = d_campData->getVictoryLevel(1);
  LONG totalPoints = side1Victory + side2Victory;

  int x = startX;

  // TODO: get from resource
  // const char* text = "Victory-Level";
  ResString victoryText(IDS_VICTORYLEVEL);
  const char* text = victoryText.c_str();

  dib()->setFont(lFont);
   dib()->setTextColor(scenario->getColour("MenuText"));

  UINT oldAlign = dib()->setTextAlign(TA_RIGHT | TA_TOP);
  ASSERT(oldAlign != GDI_ERROR);

  int workingY = ((cy() - tm.tmHeight) / 2);
  // wTextOut(dib()->getDC(), x, workingY, text);
  wTextOut(dib()->getDC(), x + sideNameCX, workingY, sideNameCX, text);

  UINT align = dib()->setTextAlign(oldAlign);
  ASSERT(align != GDI_ERROR);

  /*
   * The 2 side bars will be combined into one long bar
   */

  const ColourIndex frame1CI = dib()->getColour(PALETTERGB(0, 0, 0));
  const ColourIndex frame2CI = dib()->getColour(PALETTERGB(255, 255, 255));

  const ColourIndex side1CI = dib()->getColour(scenario->getSideColour(0));
  const ColourIndex side2CI = dib()->getColour(scenario->getSideColour(1));

  const int frameCX = 1;

  x += sideNameCX;

  // draw side 1 flag
  const ImageLibrary* il = scenario->getNationFlag(scenario->getDefaultNation(0), True);
  workingY = ((cy() - FI_Army_CY) / 2);
  il->blit(dib(), FI_Army, x, workingY, True);

  x += (FI_Army_CX + xOffset);
  workingY = ((cy() - vBargraphCY) / 2);
  const int barCX = cx() - (x + (4 * xOffset) + (2 * FI_Army_CX));

  // draw frame
  dib()->frame(x, workingY, barCX, vBargraphCY, frame1CI, frame2CI);

  // draw first sides bar
//  const int side1W = (totalPoints > 0) ?
//    (barCX * ((side1Victory * 100) / totalPoints)) / 100 : barCX / 2;
   int side1W;
   if(totalPoints > 0)
      side1W = MulDiv(barCX, side1Victory, totalPoints);
   else
      side1W = barCX / 2;

  dib()->rect(x + frameCX, workingY + frameCX,
      side1W, vBargraphCY - (2 * frameCX), side1CI);

  // draw second sides bar
  dib()->rect(x + frameCX + side1W, workingY + frameCX,
      (barCX - side1W) - (2 * frameCX), vBargraphCY - (2 * frameCX), side2CI);

  // draw side2 flag
  x += (barCX + xOffset);
  il = scenario->getNationFlag(scenario->getDefaultNation(1), True);
  workingY = y + ((cy() - FI_Army_CY) / 2);
  il->blit(dib(), FI_Army, x, workingY, True);

  /*
   * If Armistice in effect then display text
   */

   if (d_campData->armisticeInEffect())
   {
      const char* strArmistice = InGameText::get(IDS_Armistice);

      int fontHeight = ScreenBase::y(8);

      LogFont lf;
      lf.height(fontHeight);
      lf.weight(FW_MEDIUM);
      lf.face(scenario->fontName(Font_Decorative));

      Font lFont;
      lFont.set(lf);

      dib()->setFont(lFont);
      dib()->setTextColor(scenario->getColour("MenuText"));

      UINT oldAlign = dib()->setTextAlign(TA_CENTER | TA_TOP);
      ASSERT(oldAlign != GDI_ERROR);

      int x = cx() / 2;
      int y = ((cy() - fontHeight) / 2);
      wTextOut(dib()->getDC(), x, y, strArmistice);

      UINT align = dib()->setTextAlign(oldAlign);
      ASSERT(align != GDI_ERROR);

   }

   drawBorder();
}

/*----------------------------------------------------------------------
 *  Nation Status window. Includes:
 *    1. morale-level for each major nationality,
 *    2. each sides victory point level
 */

class NS_Wind :
    public WindowBaseND
    // public SuspendableWindow
{
    HWND d_hParent;
    CampaignWindowsInterface* d_campWind;
    const CampaignData* d_campData;

    int d_cx;
    int d_cy;

    Boolean d_shouldRedraw;

    enum Sections {
      Side1Morale,
      Side2Morale,
      VictoryLevel,

      Sections_HowMany
    };

    GSecWind* d_sections[Sections_HowMany];

  public:
    NS_Wind(HWND hParent, CampaignWindowsInterface* cw, const CampaignData* campData);
    ~NS_Wind();

    void position(const PixelPoint& p);
    void update();

    int height() const { return d_cy; }

  private:

    LRESULT procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
    BOOL onCreate(HWND hWnd, CREATESTRUCT* lpCreateStruct);
    void onDestroy(HWND hWnd);
    void onPaint(HWND hwnd);
    void redraw();
};


NS_Wind::NS_Wind(HWND hParent, CampaignWindowsInterface* cw, const CampaignData* campData) :
  d_hParent(hParent),
  d_campWind(cw),
  d_campData(campData),
  d_cx(0),
  d_cy((2 * CustomBorderWindow::ThinBorder) + (33 * ScreenBase::dbY()) / ScreenBase::baseY()),
  d_shouldRedraw(True)
{
  for(int i = 0; i < Sections_HowMany; i++)
    d_sections[i] = 0;

  HWND h = createWindow(
      0,
      // GenericNoBackClass::className(),
        NULL,
      WS_CHILD | WS_CLIPSIBLINGS,
      0, 0, 0, d_cy,
      d_hParent, NULL // , APP::instance()
  );

  ASSERT(h != NULL);
}

NS_Wind::~NS_Wind()
{
    selfDestruct();
}

LRESULT NS_Wind::procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{

  switch(msg)
  {
    HANDLE_MSG(hWnd, WM_CREATE, onCreate);
    HANDLE_MSG(hWnd, WM_DESTROY, onDestroy);
    HANDLE_MSG(hWnd, WM_PAINT, onPaint);

    default:
      return defProc(hWnd, msg, wParam, lParam);
  }
}

BOOL NS_Wind::onCreate(HWND hwnd, CREATESTRUCT* cs)
{
  /*
   * Create sections
   */

  d_sections[Side1Morale] = new NM_Section(hwnd, d_campData, 0, scenario->getSideBkDIB(0));
  ASSERT(d_sections[Side1Morale]);

  d_sections[Side2Morale] = new NM_Section(hwnd, d_campData, 1, scenario->getSideBkDIB(1));
  ASSERT(d_sections[Side2Morale]);

  d_sections[VictoryLevel] = new V_Section(hwnd, d_campData, scenario->getSideBkDIB(SIDE_Neutral));
  ASSERT(d_sections[VictoryLevel]);

  return TRUE;
}

void NS_Wind::onDestroy(HWND hwnd)
{
  for(int i = 0; i < Sections_HowMany; i++)
  {
    if(d_sections[i])
    {
      delete d_sections[i];
      d_sections[i] = 0;
    }
  }
}

void NS_Wind::onPaint(HWND hwnd)
{
  PAINTSTRUCT ps;
  HDC hdc = BeginPaint(hwnd, &ps);

  HPALETTE oldPal = SelectPalette(hdc, Palette::get(), FALSE);

  RECT r;
  GetClientRect(hwnd, &r);

  static CustomBorderWindow bw(scenario->getBorderColors());
  bw.drawThinBorder(hdc, r);

  // clean up
  SelectPalette(hdc, oldPal, FALSE);
  EndPaint(hwnd, &ps);
}

void NS_Wind::redraw()
{
  for(int i = 0; i < Sections_HowMany; i++)
  {
    if(d_sections[i])
      d_sections[i]->redraw();
  }
}

/*
 * Thread safe function
 */

void NS_Wind::update()
{
  for(int i = 0; i < Sections_HowMany; i++)
  {
    if(d_sections[i])
      d_sections[i]->update();
  }
}


void NS_Wind::position(const PixelPoint& p)
{
  d_shouldRedraw = True;

  RECT r;
  GetClientRect(d_hParent, &r);

  const int cx = r.right - r.left;
  const int secCX = cx - (2 * CustomBorderWindow::ThinBorder);
  const int secCY = (d_cy - (2 * CustomBorderWindow::ThinBorder)) / Sections_HowMany;

  SetWindowPos(getHWND(), HWND_TOP, p.getX(), p.getY(), cx, d_cy, 0);

  // set section positions
  int x = CustomBorderWindow::ThinBorder;
  int y = CustomBorderWindow::ThinBorder;
  for(int i = 0; i < Sections_HowMany; i++)
  {
    ASSERT(d_sections[i]);
    SetWindowPos(d_sections[i]->getHWND(), HWND_TOP, x, y, secCX, secCY, SWP_SHOWWINDOW);
    y += secCY;
  }
}

/*---------------------------------------------------------------
 * Client Access for Nation Status
 */

NationStatus_Int::NationStatus_Int(HWND hParent, CampaignWindowsInterface* cw, const CampaignData* campData) :
  d_nsWind(new NS_Wind(hParent, cw, campData))
{
  ASSERT(d_nsWind);
}

NationStatus_Int::~NationStatus_Int()
{
  // destroy();
  delete d_nsWind;
}

void NationStatus_Int::position(const PixelPoint& p)
{
  ASSERT(d_nsWind);
  d_nsWind->position(p);
}

void NationStatus_Int::update()
{
  ASSERT(d_nsWind);
  d_nsWind->update();
}

// void NationStatus_Int::show()
// {
//   ASSERT(d_nsWind);
//   ShowWindow(d_nsWind->getHWND(), SW_SHOW);
// }
//
// void NationStatus_Int::hide()
// {
//   ASSERT(d_nsWind);
//   ShowWindow(d_nsWind->getHWND(), SW_HIDE);
// }
//
// void NationStatus_Int::destroy()
// {
//   if(d_nsWind)
//   {
//     DestroyWindow(d_nsWind->getHWND());
//     d_nsWind = 0;
//   }
// }

int NationStatus_Int::height() const
{
  ASSERT(d_nsWind);
  return d_nsWind->height();
}

// void NationStatus_Int::suspend(bool visible)
// {
//    d_nsWind->suspend(visible);
// }

HWND NationStatus_Int::getHWND() const
{
    return d_nsWind->getHWND();
}

bool NationStatus_Int::isVisible() const
{
  return d_nsWind->isVisible();
}

bool NationStatus_Int::isEnabled() const
{
  return d_nsWind->isEnabled();
}

void NationStatus_Int::show(bool visible)
{
  d_nsWind->show(visible);
}

void NationStatus_Int::enable(bool enable)
{
  d_nsWind->enable(enable);
}


/*---------------------------------------------------------------------
 * Status Window
 */
/*---------------------------------------------------------------------
 * Victory section.
 * Displays a victory barchart
 */

class Msg_Section : public GSecWind {
    CampaignWindowsInterface* d_campWind;
//    SimpleString d_text;

    enum { ID_Button = 100 };
  public:
    Msg_Section(HWND hParent, CampaignWindowsInterface* cw, const DrawDIB* fillDib);
    ~Msg_Section() {}

    // virtual functions from GSecWind
    void drawDib();
    void enableControls() {}

  private:
    LRESULT procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
    BOOL onCreate(HWND hWnd, CREATESTRUCT* lpCreateStruct);
    void onCommand(HWND hWnd, int id, HWND hwndCtl, UINT codeNotify);
    void onDestroy(HWND hWnd) {}
};

Msg_Section::Msg_Section(HWND hParent, CampaignWindowsInterface* cw, const DrawDIB* fillDib) :
  GSecWind(fillDib),
  d_campWind(cw)
{
  HWND h = createWindow(
      0,
      // GenericNoBackClass::className(),
        NULL,
      WS_CHILD | WS_CLIPSIBLINGS,
      0, 0, 0, 0,
      hParent, NULL       //, APP::instance()
  );

  ASSERT(h != NULL);
}

LRESULT Msg_Section::procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{

  switch(msg)
  {
    HANDLE_MSG(hWnd, WM_CREATE, onCreate);
    HANDLE_MSG(hWnd, WM_DESTROY, onDestroy);
    HANDLE_MSG(hWnd, WM_COMMAND, onCommand);
    HANDLE_MSG(hWnd, WM_PAINT, onPaint);

    default:
      return defProc(hWnd, msg, wParam, lParam);
  }
}

BOOL Msg_Section::onCreate(HWND hwnd, CREATESTRUCT* cs)
{
  const int imgID = TBI_MessageWindow;

  HWND h = NSUtil::createButton(hwnd, NULL, ID_Button, True, imgID);
  InvalidateRect(h, NULL, FALSE);

  return TRUE;
}

void Msg_Section::onCommand(HWND hWnd, int id, HWND hwndCtl, UINT codeNotify)
{
  if(id == ID_Button)
  {
    SendMessage(APP::getMainHWND(), WM_COMMAND,
        MAKEWPARAM(IDM_MESSAGEWINDOW, codeNotify), reinterpret_cast<LPARAM>(hwndCtl));
  }
}

void Msg_Section::drawDib()
{
  const int dbX = ScreenBase::dbX();
  const int dbY = ScreenBase::dbY();
  const int baseX = ScreenBase::baseX();
  const int baseY = ScreenBase::baseY();
  const int buttonX = ((2 * dbX) / baseX) + (2 * CustomBorderWindow::ThinBorder);
  const int buttonCX = cy() - ((2 * CustomBorderWindow::ThinBorder) + 2);
  const int startX = ((3 * dbX) / baseX) + buttonX + buttonCX;

  /*
   * if size has changed, position button
   */

  if(sizeChanged())
  {

    HWND h = GetDlgItem(getHWND(), ID_Button);
    ASSERT(h);

    int y = (cy() - buttonCX) / 2;
    SetWindowPos(h, HWND_TOP, buttonX, y, buttonCX, buttonCX, SWP_SHOWWINDOW);
    InvalidateRect(h, NULL, FALSE);
    sizeChanged(False);
  }

  dib()->setBkMode(TRANSPARENT);

  /*
   * Get fonts
   */

  // large font
  LogFont lf;
  lf.height((9 * dbY) / baseY);
  lf.weight(FW_MEDIUM);
  lf.face(scenario->fontName(Font_Decorative));

  Font lFont;
  lFont.set(lf);

  static TEXTMETRIC tm;
  static Boolean gotMetrics = False;
  if(!gotMetrics)
  {
    GetTextMetrics(dib()->getDC(), &tm);
    gotMetrics = True;
  }

  /*
   * Draw Number of messages
   */

  int y = 0;

  // fill in dib
  dib()->rect(0, 0, cx(), cy(), fillDib());

#if !defined(EDITOR)
  int numMessages = d_campWind->getNumCampaignMessages();
//  int  messagesRead = d_campWind->getCampaignMessagesRead();
#else
  int numMessages = 0;
//  int  messagesRead = 0;
#endif

  dib()->setFont(lFont);
  dib()->setTextColor(scenario->getColour("MenuText"));

  int workingY = ((cy() - tm.tmHeight) / 2);

  const char* strMessage = InGameText::get(IDS_Messages);

  // TODO: get from resource
  wTextPrintf(dib()->getDC(),
      startX, workingY,
      "%s: %d",      // Unread: %d",
      strMessage,
      numMessages);    //, messagesRead);

  drawBorder();
}

/*----------------------------------------------------------------
 *  Hint-Line Section.
 */

class Hint_Section : public GSecWind, public HintLineBase {
    CampaignWindowsInterface* d_campWind;
    String d_text;

    enum { ID_Button = 100 };
  public:
    Hint_Section(HWND hParent, CampaignWindowsInterface* cw, const DrawDIB* fillDib);
    ~Hint_Section()
    {
      g_toolTip.setHintDisplay(0);
    }

    // virtual functions from GSecWind
    void drawDib();
    void enableControls() {}

    // virtual functions from HintLineBase
    void showHint(const String& text);
    void clearHint();

  private:
    LRESULT procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
    BOOL onCreate(HWND hWnd, CREATESTRUCT* lpCreateStruct);
    void onDestroy(HWND hWnd) {}
};

Hint_Section::Hint_Section(HWND hParent, CampaignWindowsInterface* cw, const DrawDIB* fillDib) :
  GSecWind(fillDib),
  d_campWind(cw)
{
  HWND h = createWindow(
      0,
      // GenericNoBackClass::className(),
        NULL,
      WS_CHILD | WS_CLIPSIBLINGS,
      0, 0, 0, 0,
      hParent, NULL   // , APP::instance()
  );

  ASSERT(h != NULL);
  g_toolTip.setHintDisplay(this);
}

LRESULT Hint_Section::procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{

  switch(msg)
  {
    HANDLE_MSG(hWnd, WM_CREATE, onCreate);
    HANDLE_MSG(hWnd, WM_DESTROY, onDestroy);
    HANDLE_MSG(hWnd, WM_PAINT, onPaint);

    default:
      return defProc(hWnd, msg, wParam, lParam);
  }
}

BOOL Hint_Section::onCreate(HWND hwnd, CREATESTRUCT* cs)
{
  return TRUE;
}

void Hint_Section::drawDib()
{
  const int dbX = ScreenBase::dbX();
  const int dbY = ScreenBase::dbY();
  const int baseX = ScreenBase::baseX();
  const int baseY = ScreenBase::baseY();
  const int startX = ((2 * dbX) / baseX) + (2 * CustomBorderWindow::ThinBorder);

  dib()->setBkMode(TRANSPARENT);

  // fill in dib
  dib()->rect(0, 0, cx(), cy(), fillDib());

  if(!d_text.empty())
  {
    /*
     * Get fonts
     */

    // large font
    LogFont lf;
    lf.height((9 * dbY) / baseY);
    lf.weight(FW_MEDIUM);
    lf.face(scenario->fontName(Font_Decorative));

    Font lFont;
    lFont.set(lf);

    static TEXTMETRIC tm;
    static Boolean gotMetrics = False;
    if(!gotMetrics)
    {
      GetTextMetrics(dib()->getDC(), &tm);
      gotMetrics = True;
    }

    /*
     * Draw Hint
     */

    int workingY = (cy() - tm.tmHeight) / 2;

    dib()->setTextColor(scenario->getColour("MenuText"));


    wTextOut(dib()->getDC(), startX, workingY,
        cx() - (startX + (2 * CustomBorderWindow::ThinBorder)), d_text.c_str());
  }

  drawBorder();
}

void Hint_Section::showHint(const String& text)
{
  d_text = text;
  update();
}

void Hint_Section::clearHint()
{
  d_text.erase(); // d_text.clear();
  update();
}

/*----------------------------------------------------------------
 *  Game Status window. includes:
 *    1. Nessage Section
 *    2. Hint-Line Section
 *    3. Clock Window
 */

class GS_Wind : public WindowBaseND //, public SuspendableWindow
{
    HWND d_hParent;
    CampaignWindowsInterface* d_campWind;
    const CampaignData* d_campData;
    CampaignTimeControl* d_control;
    const CampaignTimeData* d_date;

    ClockWindow* d_cClockWind;

    int d_cx;
    int d_cy;

    enum Sections {
      MsgSection,
      HintSection,

      Sections_HowMany
    };

    GSecWind* d_sections[Sections_HowMany];

  public:
    GS_Wind(HWND hParent, CampaignWindowsInterface* cw, const CampaignData* campData,
       CampaignTimeControl* control, const CampaignTimeData* date);
    ~GS_Wind();

    void position(const PixelPoint& p);
    void updateMessageWindow();
    void updateClock();

    int height() const { return d_cy; }
  private:

    LRESULT procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
    BOOL onCreate(HWND hWnd, CREATESTRUCT* lpCreateStruct);
    void onDestroy(HWND hWnd);
    void onPaint(HWND hwnd);
};


GS_Wind::GS_Wind(HWND hParent, CampaignWindowsInterface* cw, const CampaignData* campData,
       CampaignTimeControl* control, const CampaignTimeData* date) :
  d_hParent(hParent),
  d_campWind(cw),
  d_campData(campData),
  d_control(control),
  d_date(date),
  d_cClockWind(0),
  d_cx(0),
  d_cy((2 * CustomBorderWindow::ThinBorder) + (22 * ScreenBase::dbY()) / ScreenBase::baseY())
{
  for(int i = 0; i < Sections_HowMany; i++)
    d_sections[i] = 0;

  HWND h = createWindow(
      0,
      // GenericNoBackClass::className(),
        NULL,
      WS_CHILD | WS_CLIPSIBLINGS,
      0, 0, 0, d_cy,
      d_hParent, NULL // , APP::instance()
  );

  ASSERT(h != NULL);
}

GS_Wind::~GS_Wind()
{
    selfDestruct();
  if(d_cClockWind)
    delete d_cClockWind;
}

LRESULT GS_Wind::procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{

  switch(msg)
  {
    HANDLE_MSG(hWnd, WM_CREATE, onCreate);
    HANDLE_MSG(hWnd, WM_DESTROY, onDestroy);
    HANDLE_MSG(hWnd, WM_PAINT, onPaint);

    default:
      return defProc(hWnd, msg, wParam, lParam);
  }
}

BOOL GS_Wind::onCreate(HWND hwnd, CREATESTRUCT* cs)
{
  /*
   * Create sections
   */

  d_sections[MsgSection] = new Msg_Section(hwnd, d_campWind, scenario->getSideBkDIB(SIDE_Neutral));
  ASSERT(d_sections[MsgSection]);

  d_sections[HintSection] = new Hint_Section(hwnd, d_campWind, scenario->getSideBkDIB(SIDE_Neutral));
  ASSERT(d_sections[HintSection]);

   /*
    * Status and clock windows
    */

#if !defined(EDITOR)
  d_cClockWind = new ClockWindow(hwnd, d_control, d_date);
#else
  d_cClockWind = new ClockWindow(hwnd, 0, d_date);
#endif
  ASSERT(d_cClockWind != 0);

  return TRUE;
}

void GS_Wind::onDestroy(HWND hwnd)
{
  for(int i = 0; i < Sections_HowMany; i++)
  {
    if(d_sections[i])
    {
      delete d_sections[i];
      d_sections[i] = 0;
    }

    delete d_cClockWind;
     d_cClockWind = 0;
  }
}

void GS_Wind::onPaint(HWND hwnd)
{
  PAINTSTRUCT ps;
  HDC hdc = BeginPaint(hwnd, &ps);

  HPALETTE oldPal = SelectPalette(hdc, Palette::get(), FALSE);

  RECT r;
  GetClientRect(hwnd, &r);

  static CustomBorderWindow bw(scenario->getBorderColors());
  bw.drawThinBorder(hdc, r);

  // clean up
  SelectPalette(hdc, oldPal, FALSE);
  EndPaint(hwnd, &ps);
}

void GS_Wind::updateMessageWindow()
{
  if(d_sections[MsgSection])
    d_sections[MsgSection]->update();
}

void GS_Wind::updateClock()
{
  if(d_cClockWind)
    d_cClockWind->update();
}

void GS_Wind::position(const PixelPoint& p)
{
  // get parent rect
  RECT r;
  GetClientRect(d_hParent, &r);

  // set clock width
  // RECT rClock;
  // GetWindowRect(d_cClockWind->getHWND(), &rClock);

  // const int clockCX = rClock.right - rClock.left;
  // SetWindowPos(d_cClockWind->getHWND(), HWND_TOP,
  //    r.right - (CustomBorderWindow::ThinBorder + clockCX), CustomBorderWindow::ThinBorder,
  //    clockCX, d_cy - (2 * CustomBorderWindow::ThinBorder), SWP_SHOWWINDOW);

  int clockCX = d_cClockWind->getWidth();
  d_cClockWind->setPosition(
     r.right - (CustomBorderWindow::ThinBorder + clockCX),
      CustomBorderWindow::ThinBorder,
     clockCX,
      d_cy - (2 * CustomBorderWindow::ThinBorder) );

  const int cx = r.right - r.left;
  const int secCX = cx - ((2 * CustomBorderWindow::ThinBorder) + clockCX);
  const int secCY = (d_cy - (2 * CustomBorderWindow::ThinBorder)) / Sections_HowMany;

  SetWindowPos(getHWND(), HWND_TOP, p.getX(), p.getY(), cx, d_cy, 0);

  // set section positions
  int x = CustomBorderWindow::ThinBorder;
  int y = CustomBorderWindow::ThinBorder;
  for(int i = 0; i < Sections_HowMany; i++)
  {
    ASSERT(d_sections[i]);

    SetWindowPos(d_sections[i]->getHWND(), HWND_TOP, x, y, secCX, secCY, SWP_SHOWWINDOW);

    y += secCY;
  }
}

/*----------------------------------------------------------------
 * Client Access for status window
 */

GameStatus_Int::GameStatus_Int(HWND hParent, CampaignWindowsInterface* cw, const CampaignData* campData,
       CampaignTimeControl* control, const CampaignTimeData* date) :
  d_gsWind(new GS_Wind(hParent, cw, campData, control, date))
{
  ASSERT(d_gsWind);
}

GameStatus_Int::~GameStatus_Int()
{
  // destroy();
  delete d_gsWind;
}

void GameStatus_Int::position(const PixelPoint& p)
{
  ASSERT(d_gsWind);
  d_gsWind->position(p);
}

// void GameStatus_Int::show()
// {
//   ASSERT(d_gsWind);
//   ShowWindow(d_gsWind->getHWND(), SW_SHOW);
// }
//
// void GameStatus_Int::hide()
// {
//   ASSERT(d_gsWind);
//   ShowWindow(d_gsWind->getHWND(), SW_HIDE);
// }
//
// void GameStatus_Int::destroy()
// {
//   if(d_gsWind)
//   {
//     DestroyWindow(d_gsWind->getHWND());
//     d_gsWind = 0;
//   }
// }

void GameStatus_Int::updateMessageWindow()
{
  ASSERT(d_gsWind);
  d_gsWind->updateMessageWindow();
}

void GameStatus_Int::updateClock()
{
  ASSERT(d_gsWind);
  d_gsWind->updateClock();
}

int GameStatus_Int::height() const
{
  ASSERT(d_gsWind);
  return d_gsWind->height();
}


// void GameStatus_Int::suspend(bool visible)
// {
//    d_gsWind->suspend(visible);
// }


HWND GameStatus_Int::getHWND() const
{
    return d_gsWind->getHWND();
}

bool GameStatus_Int::isVisible() const
{
  return d_gsWind->isVisible();
}

bool GameStatus_Int::isEnabled() const
{
  return d_gsWind->isEnabled();
}

void GameStatus_Int::show(bool visible)
{
  d_gsWind->show(visible);
}

void GameStatus_Int::enable(bool enable)
{
  d_gsWind->enable(enable);
}


/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.5  2003/02/08 19:38:54  greenius
 * Replaced use of basic_string::clear() with erase so that it will
 * compile with MSVC version 6.
 *
 * Fixed bad use of stringstream buffer in Battle AI logStream().
 *
 * Revision 1.4  2002/11/16 18:03:32  greenius
 * Various changes so that it will compile with Visual Studio .net
 *
 * Revision 1.3  2001/07/02 15:23:17  greenius
 * Battle, Campaign, Combined game and Campaign Editor now compile and run... mostly
 *
 * Revision 1.2  2001/06/18 08:25:45  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */

