/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 * Base class for Unit/Town Information display
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "infobase.hpp"
#include "colours.hpp"
#include "dib.hpp"
#include "scenario.hpp"
#include "scn_res.h"
#include "imglib.hpp"
#include "fonts.hpp"
#include "wmisc.hpp"
#include "dc_hold.hpp"

const int InfoDrawBase::s_borderThickness = 2;

void InfoDrawBase::drawThickBorder(const PixelRect& r)
{
   ColourIndex col = d_dib->getColour(Colours::Black);
   int width = s_borderThickness;

   LONG x = r.left();
   LONG y = r.top();
   LONG w = r.width();
   LONG h = r.height();

   while(width--)
   {
      ASSERT(w > 0);
      ASSERT(h > 0);

      d_dib->frame(x, y, w, h, col);
      ++x;
      ++y;
      w -= 2;
      h -= 2;
   }
}

void InfoDrawBase::shrinkRect(PixelRect& r, int border)
{
   r.left(r.left() + border);
   r.top(r.top() + border);
   r.right(r.right() - border);
   r.bottom(r.bottom() - border);
}



void InfoDrawBase::drawNames(const PixelRect& rect, const char* name1, const char* name2, const char* title1, const char* title2, Nationality nation, bool compact)
{
   // const ImageLibrary* flagIcons = scenario->getNationFlag(nation, false);
   const ImageLibrary* flagIcons = scenario->getNationFlag(nation, true);

   UWORD flagIndex = FI_Army;
   int flagW;
   int flagH;
   flagIcons->getImageSize(flagIndex, flagW, flagH);

   LONG x = rect.left();   // + 1;  // ((1 * ScreenBase::dbX()) / ScreenBase::baseX());
   LONG y = rect.top();    // + 1;     // ((1 * ScreenBase::dbY()) / ScreenBase::baseY());
   LONG h = rect.height(); // - 2;
   LONG w = MulDiv(flagW, h, flagH);

   flagIcons->stretchBlit(d_dib, flagIndex, x,  y, w, h);


   /*
    * Draw border around flag
    */

   // ColourIndex borderColor = d_dib->getColour(Colours::Black);
   // d_dib->frame(rect.left(), rect.top(), w+2, rect.height(), borderColor);

   /*
    * Draw border around name
    */

   const int xMargin = 1;  // ScreenBase::x(1)

   PixelRect textRect = rect;
   textRect.left(x + w + xMargin);

   // drawThickBorder(d_dib, textRect, s_borderThickness, borderColor);
   drawThickBorder(textRect);
   // d_dib->frame(textRect.left(), textRect.top(), textRect.width(), textRect.height(), borderColor);
   // d_dib->frame(textRect.left()+1, textRect.top()+1, textRect.width()-2, textRect.height()-2, borderColor);

   /*
    * create font
    */

   LogFont lf;
   lf.height( (textRect.height() - s_borderThickness*2) / 2);
   lf.weight(FW_BOLD);  // MEDIUM);
   lf.face(scenario->fontName(Font_Bold));
   lf.italic(false);
   lf.underline(false);

   Fonts font;
   font.set(d_dib->getDC(), lf);

   // HFONT oldFont = d_dib->setFont(font);
   // COLORREF oldColor = d_dib->setTextColor(scenario->getSideColour(d_cpi->getSide()));

   TextColorHolder textCol(d_dib->getDC());

   // COLORREF oldColor = d_dib->setTextColor(Colours::Black);
   textCol.set(Colours::Black);

   /*
    * Display "Unit" and "Leader"
    */

   int unitY = textRect.top() + s_borderThickness;
   int leaderY = textRect.top() + textRect.height() / 2;

   int textX = textRect.left() + s_borderThickness+1;    // 2 for border and 1 for a space

   if(!compact)
   {
      if(title1)
         wTextOut(d_dib->getDC(), textX, unitY, title1);
      if(title2)
         wTextOut(d_dib->getDC(), textX, leaderY, title2);

      // Find width of text...

      int titleWidth = 0;
      if(title1)
         titleWidth = textWidth(d_dib->getDC(), title1);
      if(title2)
         titleWidth = maximum(titleWidth, textWidth(d_dib->getDC(), title2));

      textX += titleWidth;

      if(titleWidth)
      {
         static const char strColon[] = " : ";

         wTextOut(d_dib->getDC(), textX, unitY, strColon);
         wTextOut(d_dib->getDC(), textX, leaderY, strColon);

         textX += textWidth(d_dib->getDC(), strColon);
      }
   }

   int boxWidth = textRect.right() - textX - s_borderThickness - 1;

   wTextOut(d_dib->getDC(), textX, unitY,   boxWidth, name1);
   wTextOut(d_dib->getDC(), textX, leaderY, boxWidth, name2);

   // d_dib->setTextColor(oldColor);
}


/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.3  2001/07/02 15:23:17  greenius
 * Battle, Campaign, Combined game and Campaign Editor now compile and run... mostly
 *
 * Revision 1.2  2001/06/18 08:25:45  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
