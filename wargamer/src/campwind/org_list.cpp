/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 * Temporary Order of Battle for doing reorganizations
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "org_list.hpp"
#include "campdint.hpp"
#include "armies.hpp"
#include "armyutil.hpp"
#include "compos.hpp"
#include "scn_res.h"
#include "resstr.hpp"

#ifdef DEBUG
#define DEBUG_ORGLIST

#ifdef DEBUG_ORGLIST
#include "clog.hpp"
extern LogFileFlush orgLog;
#endif
#endif


/*=====================================================================
 * Org Object Functions
 */


OrgObject::OrgObject() :
   d_what(OOW_Nothing),
   d_leader(NoLeader),
   d_sp(NoStrengthPoint),
   d_cp(NoCommandPosition),
   d_originalParent(NoCommandPosition),
   d_parent(),
   d_sister(),
   d_child(),
   d_nation(NATION_Neutral),
   d_rank(Rank_Division),
   d_treeIndex(NULL),
   d_contentIndex(NULL)
{
}

/*=====================================================================
 * OrgList functions
 */

OrgList::OrgList(const CampaignData* campData, ConstParamCP topCPI) :
   d_campData(campData)
{
   initUnits(topCPI);
}

OrgList::OrgList(const CampaignData* campData) :
   d_campData(campData)
{
}

void OrgList::initUnits(ConstParamCP cpi)
{
   d_objects.reset();

   // Add Dummy entry

   OrgCPI iOrg = d_objects.add();
   ASSERT(iOrg == OrgCPI::None);

   ASSERT(cpi != NoCommandPosition);
   const CommandPosition* cp = d_campData->getCommand(cpi);

   OrgObject& ob = d_objects[iOrg];

   ob.d_what = OrgObject::OOW_CommandPosition;
   ob.d_cp = cp->getParent();
   ob.d_originalParent = NoCommandPosition;
   ob.d_nation = cp->getNation();
   // ob.d_rank = cp->getRank().getRankEnum();
   ob.d_rank = ob.d_cp->getRankEnum();
}

ConstICommandPosition OrgList::getCP(OrgCPI iOrg) const
{
   const OrgObject& ob = d_objects[iOrg];

   ASSERT(ob.d_what == OrgObject::OOW_CommandPosition);

   if(ob.d_what == OrgObject::OOW_CommandPosition)
      return ob.d_cp;
   else
      return NoCommandPosition;
}

void OrgList::addChild(OrgCPI iParent, OrgCPI iChild)
{
  OrgObject* parent = &d_objects[iParent];
  OrgObject* child = &d_objects[iChild];

  // if a leader, make first child
  if( (child->what() == OrgObject::OOW_Leader) &&
      (iParent != OrgCPI::None) )
  {
    OrgCPI iSister = parent->d_child;
    parent->d_child = iChild;
    child->d_sister = iSister;
  }
  else
  {
    OrgCPI iSister = parent->d_child;

    if(iSister == OrgCPI::None)
    {
      parent->d_child = iChild;
    }
    else
    {
      do
      {
        parent = &d_objects[iSister];
        iSister = parent->d_sister;
      } while(iSister != OrgCPI::None);

      parent->d_sister = iChild;
    }
  }

  child->d_parent = iParent;
}

/*
 * Add a CommandPosition
 *
 * This should be simplified by combining common parts from addLeader
 */

OrgCPI OrgList::addCP(ConstParamCP cpi, OrgCPI iParent)
{
#ifdef DEBUG_ORGLIST
   orgLog.printf(">add(%s, %d)",
      (const char*) d_campData->getArmies().getUnitName(cpi),
      (int) iParent);
#endif

   ASSERT(d_campData != 0);
   ASSERT(cpi != NoCommandPosition);
   ASSERT( (iParent == OrgCPI::None) || (isValid(iParent)));

   OrgCPI iOrg = d_objects.add();
   OrgObject& ob = d_objects[iOrg];

   const CommandPosition* cp = d_campData->getCommand(cpi);

   ob.d_what = OrgObject::OOW_CommandPosition;
   ob.d_cp = cpi;
   ob.d_originalParent = cp->getParent();
   ob.d_parent = OrgCPI::None;   // iParent;
   ob.d_sister = OrgCPI::None;
   ob.d_child = OrgCPI::None;
   ob.d_nation = cp->getNation();
   ob.d_rank = cp->getRank().getRankEnum();

   addChild(iParent, iOrg);

#ifdef DEBUG_ORGLIST
   orgLog.printf("<add: Returns %d", (int) iOrg);
#endif
   return iOrg;
}


OrgCPI OrgList::addLeader(ConstILeader il, OrgCPI iParent)
{
   ASSERT(d_campData != 0);
   ASSERT(il != NoLeader);
   ASSERT( (iParent == OrgCPI::None) || isValid(iParent));

   const Leader* leader = il; // d_campData->getLeader(il);

#ifdef DEBUG_ORGLIST
   orgLog.printf(">addLeader(%s, %d)",
      (const char*) leader->getName(),
      (int) iParent);
#endif

   OrgCPI iOrg = d_objects.add();
   OrgObject& ob = d_objects[iOrg];

   ob.d_what = OrgObject::OOW_Leader;
   ob.d_leader = il;
   ob.d_originalParent = leader->getCommand();
   ob.d_parent = OrgCPI::None;   // iParent;
   ob.d_sister = OrgCPI::None;
   ob.d_child = OrgCPI::None;
   ob.d_nation = leader->getNation();
   ob.d_rank = leader->getRankLevel().getRankEnum();

   addChild(iParent, iOrg);

#ifdef DEBUG_ORGLIST
   orgLog.printf("<addLeader: Returns %d", (int) iOrg);
#endif
   return iOrg;
}

OrgCPI OrgList::addSP(ConstISP isp, OrgCPI iParent)
{
   ASSERT(d_campData != 0);
   ASSERT(isp != NoStrengthPoint);
   ASSERT( (iParent == OrgCPI::None) || isValid(iParent));

   const StrengthPointItem* sp = isp;  // d_campData->getArmies().getStrengthPoint(isp);
   const UnitTypeItem& ut = d_campData->getUnitType(sp->getUnitType());

#ifdef DEBUG_ORGLIST
   orgLog.printf(">addSP(%d, %s, %d)",
      (int) sp->getUnitType(),
      (const char*) ut.getName(),
      (int) iParent);
#endif

   OrgCPI iOrg = d_objects.add();
   OrgObject& ob = d_objects[iOrg];

   ob.d_what = OrgObject::OOW_StrengthPoint;
   ob.d_sp = isp;
   ob.d_originalParent = getCP(iParent);
   ob.d_parent = OrgCPI::None;   // iParent;
   ob.d_sister = OrgCPI::None;
   ob.d_child = OrgCPI::None;

   addChild(iParent, iOrg);

#ifdef DEBUG_ORGLIST
   orgLog.printf("<addSP: Returns %d", (int) iOrg);
#endif

   return iOrg;
}



Boolean OrgList::hasCPChild(OrgCPI i) const
{
   const OrgObject* ob = &d_objects[i];

   OrgCPI iChild = ob->d_child;
   while(iChild != OrgCPI::None)
   {
      ob = &d_objects[iChild];

      if(ob->d_what == OrgObject::OOW_CommandPosition)
         return True;

      iChild = ob->d_sister;
   }

   return False;
}

void OrgList::detach(OrgCPI iOrg)
{
   OrgObject* ob = &d_objects[iOrg];

   OrgObject* parent = &d_objects[ob->d_parent];
   OrgCPI iChild = parent->d_child;

   if(iChild == iOrg)
   {
      parent->d_child = ob->d_sister;
   }
   else
   {
      do
      {
         parent = &d_objects[iChild];
         iChild = parent->d_sister;

         ASSERT(iChild != OrgCPI::None);

      } while(iChild != iOrg);

      parent->d_sister = ob->d_sister;
   }

   ob->d_parent = OrgCPI::None;
   ob->d_sister = OrgCPI::None;
}

/*
 * if detaching object is a leader, replace leader with new guy
 */

void OrgList::addNewLeader(const OrgObject& oldLeader)
{
  ASSERT(oldLeader.what() == OrgObject::OOW_Leader);

  OrgCPI iOrg = d_objects.add();
  OrgObject& ob = d_objects[iOrg];

  ob.d_what = OrgObject::OOW_Leader;
  ob.d_originalParent = NoCommandPosition;
  ob.d_parent = OrgCPI::None; // iParent;
  ob.d_sister = OrgCPI::None;
  ob.d_child = OrgCPI::None;
  ob.d_nation = oldLeader.nation();
  ob.d_rank = oldLeader.rank();

  addChild(oldLeader.parent(), iOrg);
}

OrgCPI OrgList::addNewUnit(OrgCPI parent)
{
  OrgCPI iOrg = d_objects.add();
  OrgObject& ob = d_objects[iOrg];

  ob.d_what = OrgObject::OOW_CommandPosition;
  ob.d_originalParent = NoCommandPosition;
  ob.d_parent = OrgCPI::None; // iParent;
  ob.d_sister = OrgCPI::None;
  ob.d_child = OrgCPI::None;

  addChild(parent, iOrg);

  return iOrg;
}

void OrgList::transfer(OrgCPI from, OrgCPI dest, Boolean replaceLeader)
{
  /*
   * if this is a fictional leader, and he is not attached, then remove
   */

  ASSERT(isValid(from));
  if( (d_objects[from].what() == OrgObject::OOW_Leader) &&
      (d_objects[from].isFictional()) &&
      (dest == OrgCPI::None) )
  {
    detach(from);
    remove(from);
  }

  else
  {

    /*
     * If a leader and dest already has a leader, transfer him to independent
     */

    if(dest != OrgCPI::None)
    {
      ASSERT(isValid(dest));

      if( (d_objects[from].what() == OrgObject::OOW_Leader) &&
         (d_objects[dest].child() != OrgCPI::None) )
      {
        const OrgObject& ob = d_objects[d_objects[dest].child()];
        ASSERT(ob.what() == OrgObject::OOW_Leader);

        if(ob.what() == OrgObject::OOW_Leader)
        {
          transfer(d_objects[dest].child(), OrgCPI::None, False);  // recursive!!
        }
      }
    }

    if( (replaceLeader) &&
        (d_objects[from].what() == OrgObject::OOW_Leader) &&
        (d_objects[from].parent() != OrgCPI::None) )
    {
      addNewLeader(d_objects[from]);
    }

     /*
      * Detach from existing location
      */

    detach(from);

    /*
     * Attach to new location
     */

    addChild(dest, from);
  }
}


/*
 * Is it allowable to drop from onto to?
 */

Boolean OrgList::canTransfer(OrgCPI from, OrgCPI to)
{
   /*
    * destination must be a CommandPosition
    */

   const OrgObject* obTo = &d_objects[to];

   if(obTo->what() != OrgObject::OOW_CommandPosition)
      return False;

   const OrgObject* obFrom = &d_objects[from];

   /*
    * Eliminate case where either object is NULL or they are the same
    */

   if( (from == OrgCPI::None) || (from == to) )
      return False;

   /*
    * Disable if to is already from's parent
    */

   if(obFrom->parent() == to)
      return False;

   switch(obFrom->what())
   {
      case OrgObject::OOW_CommandPosition:
      {
         /*
          * CP onto CP:
          *    Destination must have a higher rank
          *    Except for Army unless the destination is already attached
          *    to a wing.
          */

         return Rank(obTo->rank()).isHigher(obFrom->rank());
//       return (obTo->cp()->getRank().isHigher(obFrom->cp()->getRank()));
//       return True;
      }

      case OrgObject::OOW_Leader:
         /*
          * Leader onto CP:
          *    Check for Nationality rules
          *    Nobility Rules
          */

         return canTransferLeader(from, to);

      case OrgObject::OOW_StrengthPoint:
         /*
          * SP onto CP:
          *    Check for Nationality rules
          *    Check for maximum size
          */

         return canTransferSP(from, to);

#ifdef DEBUG
      default:
         FORCEASSERT("Unknown What");
         break;
#endif
   }

   return False;

}

Boolean OrgList::canTransferSP(OrgCPI from, OrgCPI to) const
{
   /*
    * Strength Point cannot be independent
    */

   if(to == OrgCPI::None)
     return False;

  ASSERT(from != OrgCPI::None);
  ASSERT(to != OrgCPI::None);
  ASSERT(isValid(from));
  ASSERT(isValid(to));

  const OrgObject& spObj = d_objects[from];
  const OrgObject& toObj = d_objects[to];

  ASSERT(spObj.what() == OrgObject::OOW_StrengthPoint);
  ASSERT(toObj.what() == OrgObject::OOW_CommandPosition);
  ASSERT(spObj.parent() != OrgCPI::None);
  ASSERT(isValid(spObj.parent()));

  /*
   * Get nationalities of the strength point, and attachto unit
   */

  const OrgObject& parentCP = d_objects[spObj.parent()];
  ASSERT(parentCP.what() == OrgObject::OOW_CommandPosition);

//  Nationality spN = parentCP.cp()->getNation();
//  Nationality atN = toObj.cp()->getNation();
  Nationality spN = parentCP.nation();
  Nationality atN = toObj.nation();

  return d_campData->getArmies().canNationControlNation(atN, spN);
}

Boolean OrgList::canTransferLeader(OrgCPI from, OrgCPI to) const
{
    ASSERT(from != OrgCPI::None);
    ASSERT(isValid(from));
    ASSERT(isValid(to));

    const OrgObject& leaderObj = d_objects[from];
    const OrgObject& toObj = d_objects[to];
    ASSERT(leaderObj.what() == OrgObject::OOW_Leader);
    ASSERT(toObj.what() == OrgObject::OOW_CommandPosition);

    // New Leader can not be transferred at all

    if(leaderObj.leader() == NoLeader)
    {
        // allow new leader to go anywhere

        return false;
    }
    else
    {

      /*
       * Get nationalities of the leader, and attachto unit
       */

        Nationality leaderN = leaderObj.leader()->getNation();
        Nationality atN = toObj.nation();    // toObj.cp()->getNation();

        // can leader control this nationality
        if(!d_campData->getArmies().canNationControlNation(leaderN, atN))
           return False;

        // if not making independent (by attaching to 'Executive')
        if(to != OrgCPI::None)
        {
//         ILeader atLeader = toObj.cp()->getLeader();
//         return (leaderObj.leader()->getRankLevel().getRankEnum() <= atLeader->getRankLevel().getRankEnum());
           Rank leaderRank = leaderObj.leader()->getRankLevel();
           Rank toRank = toObj.rank();
           return !leaderRank.isLower(toRank);

        }
        else
           return True;
    }
}

const char* OrgList::getName(OrgCPI iOrg) const
{
   const OrgObject* ob = &d_objects[iOrg];
   // const char* text = 0;
   static SimpleString text;

   if(iOrg == OrgCPI::None)
      return InGameText::get(IDS_Root);

   if(ob->d_what == OrgObject::OOW_CommandPosition)
   {
      if(ob->d_cp == NoCommandPosition)
         text = InGameText::get(IDS_NewCommand);
      else
         text = d_campData->getArmies().getUnitName(ob->d_cp);
   }
   else if(ob->d_what == OrgObject::OOW_Leader)
   {
      if(ob->d_leader == NoLeader)
         text = InGameText::get(IDS_NewLeader);
      else
      {
         const Leader* leader = ob->d_leader;   // d_campData->getLeader(ob->d_leader);
         text = leader->getName();
      }
   }
   else if(ob->d_what == OrgObject::OOW_StrengthPoint)
   {
      if(ob->d_sp != NoStrengthPoint)
      {
         const StrengthPoint* sp = ob->d_sp; // d_campData->getArmies().getStrengthPoint(ob->d_sp);
         const UnitTypeItem& unitType = d_campData->getUnitType(sp->getUnitType());
         return unitType.getName();
      }
      else
         text = InGameText::get(IDS_NewStrengthPoint);
   }
   else
      text = InGameText::get(IDS_UI_UNKNOWN);

   return text.toStr();
}

static struct {
   int icon;
   int selIcon;
} cpIcons[Rank_HowMany] =
{
   { OBI_Side,       OBI_SideSelected  },    // Rank_God
   { OBI_Side,       OBI_SideSelected  },    // Rank_President
// { OBI_Army,       OBI_ArmySelected  },    // Rank_Executive
   { OBI_Army,       OBI_ArmySelected  },    // Rank_ArmyGroup
   { OBI_Army,       OBI_ArmySelected  },    // Rank_Army
   { OBI_Corps,      OBI_CorpsSelected },    // Rank_Corps
   { OBI_Division,   OBI_DivisionSelected }  // Rank_Division
// { OBI_Brigade,    OBI_BrigadeSelected }   // Rank_Brigade
};


int OrgList::getImage(OrgCPI iOrg) const
{
   ASSERT(iOrg != OrgCPI::None);

   const OrgObject* ob = &d_objects[iOrg];

   if(ob->d_what == OrgObject::OOW_CommandPosition)
   {
      if(ob->d_cp == NoCommandPosition)
         return 0;
      else
      {
         const CommandPosition* cp = d_campData->getCommand(ob->d_cp);
         return cp->getSide() * OBI_IconsPerSide + cpIcons[cp->getRankEnum()].icon;
      }
   }
   else if(ob->d_what == OrgObject::OOW_Leader)
   {
      if(ob->d_leader == NoLeader)
         return 0;
      else
      {
         const Leader* leader = ob->d_leader;   // d_campData->getLeader(ob->d_leader);
         return leader->getSide() * OBI_IconsPerSide + cpIcons[leader->getRankLevel().getRankEnum()].icon;
      }
   }
   else if(ob->d_what == OrgObject::OOW_StrengthPoint)
   {
      return 0;
   }
   else
      return 0;
}

int OrgList::getSelectedImage(OrgCPI iOrg) const
{
   ASSERT(iOrg != OrgCPI::None);

   const OrgObject* ob = &d_objects[iOrg];

   if(ob->d_what == OrgObject::OOW_CommandPosition)
   {
      if(ob->d_cp == NoCommandPosition)
         return 1;
      else
      {
         const CommandPosition* cp = d_campData->getCommand(ob->d_cp);
         return cp->getSide() * OBI_IconsPerSide + cpIcons[cp->getRankEnum()].selIcon;
      }
   }
   else if(ob->d_what == OrgObject::OOW_Leader)
   {
      if(ob->d_leader == NoLeader)
         return 1;
      else
      {
         const Leader* leader = ob->d_leader;   // d_campData->getLeader(ob->d_leader);
         return leader->getSide() * OBI_IconsPerSide + cpIcons[leader->getRankLevel().getRankEnum()].selIcon;
      }
   }
   else if(ob->d_what == OrgObject::OOW_StrengthPoint)
   {
      return 1;
   }
   else
      return 1;
}

Side OrgList::getSide(OrgCPI iOrg) const
{
   ASSERT(iOrg != OrgCPI::None);

   const OrgObject* ob = &d_objects[iOrg];

   if(ob->d_what == OrgObject::OOW_CommandPosition)
   {
      if(ob->d_cp == NoCommandPosition)
         return SIDE_Neutral;
      else
      {
         const CommandPosition* cp = d_campData->getCommand(ob->d_cp);
         return cp->getSide();
      }
   }
   else if(ob->d_what == OrgObject::OOW_Leader)
   {
      if(ob->d_leader == NoLeader)
         return SIDE_Neutral;
      else
      {
         const Leader* leader = ob->d_leader;   // d_campData->getLeader(ob->d_leader);
         return leader->getSide();
      }
   }
   else if(ob->d_what == OrgObject::OOW_StrengthPoint)
   {
      return SIDE_Neutral;
   }
   else
      return SIDE_Neutral;
}

/*
 * Return True if Org is allowed to detach
 *
 * If unit is already detached then return False
 */

Boolean OrgList::canDetach(OrgCPI iOrg)
{
   if(iOrg != OrgCPI::None)
   {
      if(d_objects[iOrg].d_parent != OrgCPI::None)
         return True;
   }
   return False;
}

/*
 * Return True if 2 orgs may potentially merge with each other
 *
 * to merge they must both be of the same rank, and independent,
 * or attached directly to a unit 2 ranks higher
 */

Boolean OrgList::canMerge(OrgCPI from, OrgCPI to)
{
  /*
   * special cases
   */

  // can't merge with our root
  // can't merge with ourself
  if( (from == OrgCPI::None) ||
      (to == OrgCPI::None) ||
      (from == to) )
  {
    return False;
  }

  ASSERT(isValid(from));
  ASSERT(isValid(to));

  const OrgObject& ob1 = d_objects[from];
  const OrgObject& ob2 = d_objects[to];

  // can't merge if both are not cp's
  // can't merge if not the same rank
  // can't merge if an army
  if( (ob1.what() != OrgObject::OOW_CommandPosition) ||
      (ob2.what() != OrgObject::OOW_CommandPosition) ||
      (ob1.rank() != ob2.rank()) ||
      (ob1.rank() <= Rank_Army) )
  {
    return False;
  }

  // can't merge if 2nd units parent is not an army group
  if(ob2.parent() != OrgCPI::None)
  {
    RankEnum rank1 = ob2.rank();
    RankEnum pRank = d_objects[ob2.parent()].rank();

    ASSERT(rank1 == Rank_Division || rank1 == Rank_Corps || rank1 == Rank_Army);

    if(rank1 == Rank_Division)
      return (pRank <= Rank_ArmyGroup);
    else
      return (pRank <= Rank_President);
  }

  return True;
}

void OrgList::merge(OrgCPI from, OrgCPI to)
{
#ifdef DEBUG
  Boolean result = canMerge(from, to);
  ASSERT(result);
#endif

  const OrgObject& ob1 = d_objects[from];
  const OrgObject& ob2 = d_objects[to];

  /*
   * decide which leader takes over
   */

  OrgCPI leader1 = OrgCPI::None;
  OrgCPI leader2 = OrgCPI::None;

  // find first leader
  OrgCPI iChild = ob1.child();
  while(iChild != OrgCPI::None)
  {
    const OrgObject& ob = d_objects[iChild];

    if(ob.what() == OrgObject::OOW_Leader)
    {
      leader1 = iChild;
      break;
    }

    iChild = ob.sister();
  }

  ASSERT(leader1 != OrgCPI::None);

  // find 2nd leader
  iChild = ob2.child();
  while(iChild != OrgCPI::None)
  {
    const OrgObject& ob = d_objects[iChild];

    if(ob.what() == OrgObject::OOW_Leader)
    {
      leader2 = iChild;
      break;
    }

    iChild = ob.sister();
  }

  ASSERT(leader2 != OrgCPI::None);

  OrgCPI leader = leader1;

  ConstILeader l1 = d_objects[leader1].leader();
  ConstILeader l2 = d_objects[leader2].leader();
  if(l2 != NoLeader)
  {
      if(l1 == NoLeader)
         leader = leader2;
      else
      {
         ConstILeader l = d_campData->getArmies().whoShouldCommand(l1, l2);

         if(l == l2)
            leader = leader2;
      }
  }

  // set rank
  RankEnum rank = ob1.rank();
  DECREMENT(rank);

  /*
   *  Create new command and attach units
   */

  OrgCPI newCPI = addNewUnit(ob2.parent());
  OrgObject& newObj = d_objects[newCPI];
  newObj.d_nation = d_objects[leader].nation();
  newObj.d_rank = rank;

  transfer(leader, newCPI);
  transfer(from, newCPI);
  transfer(to, newCPI);
}

/*
 * Return True if Org may potentially merge with something else
 *
 */

Boolean OrgList::canMerge(OrgCPI iOrg)
{
   if(iOrg != OrgCPI::None)
   {
      const OrgObject* ob = &d_objects[iOrg];
      if(ob->d_what == OrgObject::OOW_CommandPosition)
      {
         // Scan children
         // Add up SP count
         // If any CP's below then abort

         int spCount = 0;
         int cpCount = 0;

         OrgCPI iChild = ob->d_child;
         while(iChild != OrgCPI::None)
         {
            const OrgObject* obChild = &d_objects[iChild];

            if(obChild->d_what == OrgObject::OOW_CommandPosition)
            {
               cpCount++;
               break;
            }
            else if(obChild->d_what ==    OrgObject::OOW_StrengthPoint)
            {
               spCount++;
            }

            iChild = obChild->d_sister;
         }

         if((cpCount == 0) && (spCount > 1) && (spCount < OrderBattle::MaxSPperDivision))
            return True;
      }
   }
   return False;
}

Boolean OrgListIter::next()
{
  if(d_currentCPI == OrgCPI::None)
  {
    d_currentCPI = d_startCPI;
  }
  else
  {
    ASSERT(d_currentCPI != OrgCPI::None);
    ASSERT(d_list.isValid(d_currentCPI));

    OrgCPI oldcpi = d_currentCPI;

    d_currentCPI = d_list[d_currentCPI].child();

    while( (d_currentCPI == OrgCPI::None) &&
           (oldcpi != d_startCPI) &&
           (oldcpi != OrgCPI::None) )
    {
      d_currentCPI = d_list[oldcpi].sister();
      if(d_currentCPI == OrgCPI::None)
      {
        oldcpi = d_list[oldcpi].parent();
//          if(oldcpi == d_startCPI && d_list[oldcpi].sister() != OrgCPI::None)
//              d_currentCPI = d_list[oldcpi].sister();
        }
    }

  }

  ASSERT(d_list.isValid(d_currentCPI));
  return (d_currentCPI != OrgCPI::None);
}

Boolean OrgListIter::sister()
{
  if(d_currentCPI == OrgCPI::None)
  {
    d_currentCPI = d_startCPI;
  }
  else
  {
    ASSERT(d_currentCPI != OrgCPI::None);
    ASSERT(d_list.isValid(d_currentCPI));
    d_currentCPI = d_list[d_currentCPI].sister();
  }

  ASSERT(d_list.isValid(d_currentCPI));
  return (d_currentCPI != OrgCPI::None);
}


/*
 *----------------------------------------------------------------------
 * $Log$
 *----------------------------------------------------------------------
 */
