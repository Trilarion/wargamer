/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef CBATDIAL_HPP
#define CBATDIAL_HPP

#include "cwdll.h"
#include "wg_msg.hpp"
#include "gamedefs.hpp"
#include "cbatmode.hpp"
#include "measure.hpp"

class CampaignWindowsInterface;
class CampaignData;
class MapWindow;
class CampaignBattle;
struct BattleInfoData;
class CampaignBattleUnitList;

class ChooseBattleModeRequest : public WargameMessage::WaitableMessage
{
		const CampaignBattle* d_battle;
        BattleMode::Mode d_mode;        // Filled in by run()

	public:
		ChooseBattleModeRequest(const CampaignBattle* battle) :
            d_battle(battle),
            d_mode(BattleMode::Setup)
        {
        }

		CAMPWIND_DLL virtual void run();

        void mode(BattleMode::Mode mode) { d_mode = mode; }
        BattleMode::Mode mode() const { return d_mode; }
        Location location() const;
};


class BattleDayRequest : public WargameMessage::WaitableMessage 
{
		const BattleInfoData& d_data;	// info for battle in progress
        EndOfDayMode::Mode d_result;

	public:
		BattleDayRequest(const BattleInfoData& data) : d_data(data), d_result() { }
		CAMPWIND_DLL void run();
        
        EndOfDayMode::Mode result() const { return d_result; }

        // Functions called by run()

        const CampaignBattleUnitList& unitList(Side s) const;
        ITown town() const;
        Side currentSide() const;
        Side victor() const;

        void setResult(EndOfDayMode::Mode result) { d_result = result; }


};


class CampaignBattleDial {
  public:
	 static void create(CampaignWindowsInterface* campwind, const CampaignData* campData, MapWindow* mapWindow);
	 static void destroy();

	 void run(ChooseBattleModeRequest* request);
     void run(BattleDayRequest* request);
};


#endif
