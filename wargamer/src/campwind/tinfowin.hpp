/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef TINFOWIN_HPP
#define TINFOWIN_HPP

#include "gamedefs.hpp"

class TownInfoWindow;
class CampaignData;
class CampaignUserInterface;
class PixelPoint;

/*
 * Interface to unit-information window
 */

class TownInfo_Int {
	 TownInfoWindow* d_infoWind;
  public:
	 TownInfo_Int(CampaignUserInterface* owner, HWND hParent, const CampaignData* campData);
	 ~TownInfo_Int();

	 void run(ITown town, const PixelPoint& p);
	 void destroy();
};

#endif
