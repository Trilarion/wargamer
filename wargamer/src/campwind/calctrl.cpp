/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
#include "stdinc.hpp"
#include "calctrl.hpp"
#include "camptime.hpp"
#include "date.hpp"
#include "fonts.hpp"
#include "winctrl.hpp"
#include "dib.hpp"
//#include "bmp.hpp"
#include "palette.hpp"
#include "palwind.hpp"
#include "wmisc.hpp"

namespace CalenderControl
{

// void onSetBk(HWND hwnd, const DrawDIB* dib)
#define HANDLE_CCM_SETBK(hwnd, wParam, lParam, fn) \
   ((fn)(hwnd, reinterpret_cast<const DrawDIB*>(lParam)), 0L)

// void onSetMonth(HWND hwnd, const Date* date)
#define HANDLE_CCM_SETMONTH(hwnd, wParam, lParam, fn) \
   ((fn)(hwnd, reinterpret_cast<Date*>(lParam)), 0L)

// void onSetMonth(HWND hwnd, Date* date)
#define HANDLE_CCM_GETCURSEL(hwnd, wParam, lParam, fn)   \
   ((fn)(hwnd, reinterpret_cast<Date*>(lParam)), 0L)

enum MessageID {
  CCM_SETMONTH = WM_USER,
  CCM_SETBK,
  CCM_GETCURSEL
};



struct CalenderControlImp{
  HWND d_hControl;
  HWND d_hParent;
  int d_id;
  Date d_date;
  Day d_currentDay;
  const int d_blockWidth;
  const int d_blockHeight;

  const DrawDIB* d_bkDib;
  DrawDIBDC* d_calenderDib;

  enum {
    DayBlocks = 7,
    WeekBlocks = 6,
    TotalBlocks = 42
  };

  struct CalenderBlock {
    Day d_day;
    CalenderBlock() : d_day(0) {}
  } d_blocks[TotalBlocks];

  CalenderControlImp(int cx, int cy) :
    d_hControl(0),
    d_hParent(0),
    d_currentDay(0),
    d_blockWidth(cx/DayBlocks),
    d_blockHeight(cy/WeekBlocks),
    d_bkDib(0),
    d_calenderDib(new DrawDIBDC(cx, cy))
    {
      ASSERT(d_calenderDib != 0);
    }

  ~CalenderControlImp()
  {
    if(d_calenderDib)
      delete d_calenderDib;
  }

};

inline CalenderControlImp* cc_getData(HWND hwnd)
{
  ASSERT_CLASS(hwnd, CALENDERCONTROLCLASS);
  CalenderControlImp* cci = reinterpret_cast<CalenderControlImp*>(GetWindowLong(hwnd, GWL_USERDATA));
  ASSERT(cci != 0);

  return cci;
}

BOOL cc_onCreate(HWND hwnd, LPCREATESTRUCT lpCreateStruct)
{
  ASSERT_CLASS(hwnd, CALENDERCONTROLCLASS);
  CalenderControlImp* cci = new CalenderControlImp(lpCreateStruct->cx, lpCreateStruct->cy);
  ASSERT(cci != 0);
  if(cci == 0)
    return FALSE;

  SetWindowLong(hwnd, GWL_USERDATA, reinterpret_cast<LONG>(cci));

  cci->d_hParent = lpCreateStruct->hwndParent; //reinterpret_cast<HWND>(GetWindowLong(hwnd, GWL_HWNDPARENT));
  cci->d_hControl = hwnd;
  cci->d_id = static_cast<int>(GetWindowLong(hwnd, GWL_ID));

  return TRUE;
}

void cc_onDestroy(HWND hwnd)
{
  ASSERT_CLASS(hwnd, CALENDERCONTROLCLASS);

  CalenderControlImp* cci = cc_getData(hwnd);

  delete cci;
}

void cc_fillBlockData(CalenderControlImp* cci)
{
  /*
   * First, clear out old values
   */

  for(int i = 0; i < CalenderControlImp::TotalBlocks; i++)
  {
    cci->d_blocks[i].d_day = 0;
  }

  /*
   * Find out what block we start filling days in on
   */

  zDate date(static_cast<zDate::month>(cci->d_date.month+1), 1, cci->d_date.year);

  Day day = 1;
  for(i = date.DayOfWeek()-1; i < date.DaysInMonth()+(date.DayOfWeek()-1); i++)
  {
    cci->d_blocks[i].d_day = day++;
  }
}

BOOL cc_onEraseBk(HWND hwnd, HDC hdc)
{
  ASSERT_CLASS(hwnd, CALENDERCONTROLCLASS);
  return TRUE;
}


void cc_onSetBk(HWND hwnd, const DrawDIB* fill)
{
  ASSERT_CLASS(hwnd, CALENDERCONTROLCLASS);

  CalenderControlImp* cci = cc_getData(hwnd);

  cci->d_bkDib = fill;
  ASSERT(cci->d_bkDib != 0);
}

void cc_onGetCurSel(HWND hwnd, Date* date)
{
  ASSERT_CLASS(hwnd, CALENDERCONTROLCLASS);
  ASSERT(date != 0);

  CalenderControlImp* cci = cc_getData(hwnd);

  date->set(cci->d_currentDay, cci->d_date.month, cci->d_date.year);
}

/*
 * Highlight a calender block
 */

void cc_highlightBlock(CalenderControlImp* cci, int iBlock, Boolean currentBlock)
{
  ASSERT(cci->d_calenderDib != 0);
  ASSERT(iBlock < CalenderControlImp::TotalBlocks);


  /*
   * First, get our highlight rectangle
   */

  int xBlock = iBlock%CalenderControlImp::DayBlocks;
  int yBlock = iBlock/CalenderControlImp::DayBlocks;

  int x = xBlock*cci->d_blockWidth;
  int y = yBlock*cci->d_blockHeight;

  static ColourIndex iColorCurrent = cci->d_calenderDib->getColour(PALETTERGB(255, 10, 10));
  static ColourIndex iColorHighlight = cci->d_calenderDib->getColour(PALETTERGB(255, 255, 20));

  cci->d_calenderDib->frame(x, y, cci->d_blockWidth, cci->d_blockHeight, (currentBlock) ? iColorCurrent : iColorHighlight);

}

void cc_drawCalender(CalenderControlImp* cci)
{
  ASSERT(cci->d_calenderDib != 0);

  /*
   * fill in calender dib with background pattern
   */

  if(cci->d_bkDib != 0)
  {
    cci->d_calenderDib->fill(cci->d_bkDib);
  }

  /*
   * If we don't have a background pattern, fill in with white
   */

  else
  {
    static ColourIndex fillColour = cci->d_calenderDib->getColour(PALETTERGB(255, 255, 255));

    cci->d_calenderDib->fill(fillColour);
  }

  /*
   * Set our fonts
   */

  static HFONT hFont = 0;

  if(!hFont)
  {
    hFont = FontManager::getFont(12);
    ASSERT(hFont != 0);
  }

  HFONT oldFont = cci->d_calenderDib->setFont(hFont);
  cci->d_calenderDib->setBkMode(TRANSPARENT);

  /*
   * First, we want to draw our Calender Blocks
   * There are 42 blocks layed out 7*6
   *
   * Start by drawing our vertical lines
   */


  // temp
  static ColourIndex cLine = cci->d_calenderDib->getColour(PALETTERGB(0, 0, 0));;

  // draw border
  const int dibCX = cci->d_calenderDib->getWidth();
  const int dibCY = cci->d_calenderDib->getHeight();

  cci->d_calenderDib->frame(0, 0, dibCX, dibCY, cLine);

//  RECT r;
  int x = cci->d_blockWidth;

  int i = CalenderControlImp::DayBlocks;
  while(--i)    // we want 6 vertical lines
  {
    cci->d_calenderDib->vLine(x, 0, cci->d_calenderDib->getHeight(), cLine);
    x += cci->d_blockWidth;
  }

  /*
   * Now our horizontal lines
   */

  int y = cci->d_blockHeight;

  i = CalenderControlImp::WeekBlocks;
  while(--i)    // we want 5 horizontal lines
  {
    cci->d_calenderDib->hLine(0, y, cci->d_calenderDib->getWidth(), cLine);
    y += cci->d_blockHeight;
  }

  /*
   * Now we need to fill in the Days
   *
   * First, we need to get offset for text.
   * Get one each for a single and double digit number
   */

  static SIZE s;

  GetTextExtentPoint32(cci->d_calenderDib->getDC(), "8", 1, &s);
  POINT sOffset;
  sOffset.x = (cci->d_blockWidth-s.cx)/2;
  sOffset.y = (cci->d_blockHeight-s.cy)/2;

  GetTextExtentPoint32(cci->d_calenderDib->getDC(), "88", 2, &s);
  POINT dOffset;
  dOffset.x = (cci->d_blockWidth-s.cx)/2;
  dOffset.y = (cci->d_blockHeight-s.cy)/2;

  int iBlock = 0;
  for(int w = 0; w < CalenderControlImp::WeekBlocks; w++)
  {
    for(int d = 0; d < CalenderControlImp::DayBlocks; d++)
    {
      if(cci->d_blocks[iBlock].d_day > 0)
      {
        /*
         * If here then fill in this day
         */

        int tX = (cci->d_blocks[iBlock].d_day < 10) ? (d*cci->d_blockWidth) + sOffset.x : (d*cci->d_blockWidth) + dOffset.x;
        int tY = (cci->d_blocks[iBlock].d_day < 10) ? (w*cci->d_blockHeight) + sOffset.y : (w*cci->d_blockHeight) + dOffset.y;


        if(cci->d_blocks[iBlock].d_day == cci->d_date.day) // actual current day
        {
          static COLORREF textColor = PALETTERGB(255, 0, 0);
          COLORREF oldColor = cci->d_calenderDib->setTextColor(textColor);

          wTextPrintf(cci->d_calenderDib->getDC(), tX, tY, "%d", static_cast<int>(cci->d_blocks[iBlock].d_day));

          cci->d_calenderDib->setTextColor(oldColor);
        }
        else
          wTextPrintf(cci->d_calenderDib->getDC(), tX, tY, "%d", static_cast<int>(cci->d_blocks[iBlock].d_day));

        /*
         * If day is before current day, put an X
         */

        if(cci->d_blocks[iBlock].d_day < cci->d_date.day)
        {
          static ColourIndex xColor = cci->d_calenderDib->getColour(PALETTERGB(255, 0, 0));

          cci->d_calenderDib->line(d*cci->d_blockWidth, w*cci->d_blockHeight,
             (d*cci->d_blockWidth)+cci->d_blockWidth, (w*cci->d_blockHeight)+cci->d_blockHeight, xColor);

          cci->d_calenderDib->line((d*cci->d_blockWidth), (w*cci->d_blockHeight)+cci->d_blockHeight,
             (d*cci->d_blockWidth)+cci->d_blockWidth, w*cci->d_blockHeight, xColor);

        }

        /*
         * If the current day then highlight
         */

        else if(cci->d_blocks[iBlock].d_day == cci->d_currentDay)
        {
          cc_highlightBlock(cci, iBlock, True);
        }
      }
      iBlock++;
    }
  }
}

void cc_onSetMonth(HWND hwnd, const Date* date)
{
  ASSERT_CLASS(hwnd, CALENDERCONTROLCLASS);

  CalenderControlImp* cci = cc_getData(hwnd);
  ASSERT(cci != 0);

  cci->d_date = *date;    // structure copy

  cci->d_currentDay = cci->d_date.day;

  cc_fillBlockData(cci);

  cc_drawCalender(cci);

  InvalidateRect(hwnd, NULL, FALSE);
}


void cc_onPaint(HWND hwnd)
{
  ASSERT_CLASS(hwnd, CALENDERCONTROLCLASS);

  CalenderControlImp* cci = cc_getData(hwnd);
  ASSERT(cci != 0);


  PAINTSTRUCT ps;
  HDC hdc = BeginPaint(hwnd, &ps);

  HPALETTE oldPal = SelectPalette(hdc, Palette::get(), FALSE);
  RealizePalette(hdc);

  /*
   * Put Calender dib
   */

  ASSERT(cci->d_calenderDib != 0);

  BitBlt(hdc, 0, 0, cci->d_calenderDib->getWidth(), cci->d_calenderDib->getHeight(),
         cci->d_calenderDib->getDC(), 0, 0, SRCCOPY);

  SelectPalette(hdc, oldPal, FALSE);
  EndPaint(hwnd, &ps);
}

int cc_getBlock(CalenderControlImp* cci, int x, int y)
{
  int xBlock = x/cci->d_blockWidth;
  int yBlock = y/cci->d_blockHeight;

  int iBlock = (yBlock*CalenderControlImp::DayBlocks)+xBlock;

  if((iBlock < 0) || (iBlock >= CalenderControlImp::TotalBlocks))
   return -1;

  return iBlock;
}


/*
 * We want to hightlight block that mouse is over
 */

void cc_onMouseMove(HWND hwnd, int x, int y, UINT keyFlags)
{
  ASSERT_CLASS(hwnd, CALENDERCONTROLCLASS);

  CalenderControlImp* cci = cc_getData(hwnd);
  ASSERT(cci != 0);

  static int iLastBlock = -1;

  int iBlock = cc_getBlock(cci, x, y);

  if( (iLastBlock == -1) || (iLastBlock != iBlock) )
  {
    /*
     * Highlight only if block is active
     */

    if( (cci->d_blocks[iBlock].d_day > 0) && (cci->d_blocks[iBlock].d_day >= cci->d_date.day) )
    {
      cc_drawCalender(cci);
      cc_highlightBlock(cci, iBlock, False);
      iLastBlock = iBlock;
      InvalidateRect(hwnd, NULL, FALSE);
    }
  }

}

void cc_onLButtonDown(HWND hwnd, BOOL fDoubleClick, int x, int y, UINT keyFlags)
{
  ASSERT_CLASS(hwnd, CALENDERCONTROLCLASS);

  CalenderControlImp* cci = cc_getData(hwnd);
  ASSERT(cci != 0);

  int iBlock = cc_getBlock(cci, x, y);
  if (iBlock != -1)
  {
     ASSERT(iBlock < CalenderControlImp::TotalBlocks);

     /*
      * If clicking on a day that has already passed, do nothing
      */

     if(cci->d_blocks[iBlock].d_day >= cci->d_date.day)
     {
       cci->d_currentDay = cci->d_blocks[iBlock].d_day;
       cc_drawCalender(cci);

       SendMessage(cci->d_hParent, WM_COMMAND, MAKEWPARAM(cci->d_id, 0), reinterpret_cast<LPARAM>(hwnd));

       InvalidateRect(hwnd, NULL, FALSE);
     }
  }
}


LRESULT CALLBACK calenderControlProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{

   LRESULT l;
   if(PaletteWindow::handlePalette(hWnd, msg, wParam, lParam, l))
      return l;

   switch(msg)
   {
      HANDLE_MSG(hWnd, WM_CREATE,   cc_onCreate);
      HANDLE_MSG(hWnd, WM_DESTROY,  cc_onDestroy);
      HANDLE_MSG(hWnd, WM_PAINT,    cc_onPaint);
      HANDLE_MSG(hWnd, WM_MOUSEMOVE, cc_onMouseMove);
      HANDLE_MSG(hWnd, WM_LBUTTONDOWN, cc_onLButtonDown);
//    HANDLE_MSG(hWnd, WM_SIZE, cc_onSize);
//    HANDLE_MSG(hWnd, WM_DRAWITEM, cc_onDrawItem);
      HANDLE_MSG(hWnd, WM_ERASEBKGND, cc_onEraseBk);

      HANDLE_MSG(hWnd, CCM_SETMONTH, cc_onSetMonth);
      HANDLE_MSG(hWnd, CCM_SETBK,    cc_onSetBk);
      HANDLE_MSG(hWnd, CCM_GETCURSEL,    cc_onGetCurSel);

   default:
      return DefWindowProc(hWnd, msg, wParam, lParam);
   }
}




//============================ Interface ===========================
const char CALENDERCONTROLCLASS[] = "CALENDERCONTROL";
static Boolean ccInitialised = False;

void
setMonth(HWND hwnd, const Date& date)
{
  ASSERT_CLASS(hwnd, CALENDERCONTROLCLASS);
  SendMessage(hwnd, CCM_SETMONTH, 0, reinterpret_cast<LPARAM>(&date));
}

void setBk(HWND hwnd, const DrawDIB* fill)
{
  ASSERT_CLASS(hwnd, CALENDERCONTROLCLASS);
  SendMessage(hwnd, CCM_SETBK, 0, reinterpret_cast<LPARAM>(fill));
}

void
getCurrentDate(HWND hwnd, Date& date)
{
  ASSERT_CLASS(hwnd, CALENDERCONTROLCLASS);
  SendMessage(hwnd, CCM_GETCURSEL, 0, reinterpret_cast<LPARAM>(&date));
}

void initCalenderControl(HINSTANCE instance)
{
  if(!ccInitialised)
  {
    ccInitialised = True;

    WNDCLASSEX wc;

    wc.cbSize        = sizeof(WNDCLASSEX);
    wc.style         = CS_DBLCLKS | CS_PARENTDC;
    wc.lpfnWndProc   = calenderControlProc;
    wc.cbClsExtra    = 0;
    wc.cbWndExtra    = 0;
    wc.hInstance     = instance;
    wc.hIcon         = NULL;
    wc.hCursor       = LoadCursor(NULL, IDC_ARROW);
    wc.hbrBackground = NULL; //(HBRUSH) (1 + COLOR_3DFACE);
    wc.lpszMenuName  = NULL;
    wc.lpszClassName = CALENDERCONTROLCLASS;
    wc.hIconSm       = NULL;

    RegisterClassEx(&wc);
  }
}

HWND
make(HINSTANCE instance, HWND hParent, int id, const Date& date, int x, int y, int cx, int cy)
{
  ASSERT(hParent != 0);

  if(!ccInitialised)
    initCalenderControl(instance);

  /*
   * TODO: We need to have some sort of minimum size limit
   */

  HWND hWnd = CreateWindowEx(
      0,
      CALENDERCONTROLCLASS, NULL,
      WS_CHILD | WS_CLIPSIBLINGS | WS_VISIBLE,
      x, y, cx, cy,
      hParent, (HMENU)id, instance, NULL
  );
  ASSERT(hWnd != NULL);

  setMonth(hWnd, date);
  return hWnd;
}





}; // end namespace





/*
 *----------------------------------------------------------------------
 * $Log$
 *----------------------------------------------------------------------
 */
