/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/


/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *
 * Started by Paul Sample
 *----------------------------------------------------------------------
 *
 * Dialog for Adjusting Game Options
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "optdial.hpp"
//#include "options.hpp"
#include "winctrl.hpp"
#include "campint.hpp"
#include "app.hpp"
#include "resdef.h"
#include "res_str.h"
#include "campmsg.hpp"
// #include "help.h"
#include "misc.hpp"
#include "wmisc.hpp"
#include "scenario.hpp"
// #include "generic.hpp"
#include "dib.hpp"
#include "cbutton.hpp"
#include "dib_util.hpp"
#include "scn_img.hpp"
#include "simpstr.hpp"
#include "palette.hpp"
#include "palwind.hpp"

#ifdef DEBUG
#include "todolog.hpp"
#endif
#include "grtypes.hpp"

using namespace OptionsDialog;

/*
 * Helper Functions for creating Dialog templates
 */

namespace DTemplate
{
   inline int getResStringW(int resID, WCHAR* dest)
   {
   #if 1
      ResString title(resID);
      int nChars=  MultiByteToWideChar(CP_ACP, 0, title.c_str(), -1, dest, 100);
      ASSERT(nChars != 0);
   #else
      int nChars = LoadStringW(APP::instance(), pageTitles[d_page], lpwstr, 100);
      ASSERT(nChars != 0);
      nChars++;         // Count the 0.
   #endif

      return nChars;
   }

   inline void* align4(void* ptr)
   {
      return reinterpret_cast<void*>((reinterpret_cast<ULONG>(ptr) + 3) & ~3);
   }

   UWORD* makeHeader(DLGTEMPLATE* pTemplate, UINT titleID, int w, int h)
   {
      WCHAR* lpwstr;
      UWORD* lpw;

      pTemplate->style = WS_CHILD | DS_3DLOOK | DS_CONTROL | WS_DLGFRAME | DS_SETFONT;
      pTemplate->dwExtendedStyle = 0;
      pTemplate->cdit = 0;       // Initially there are no items
      pTemplate->x = 0;
      pTemplate->y = 0;
      pTemplate->cx = w;
      pTemplate->cy = h;

      lpw = reinterpret_cast<UWORD*>(pTemplate + 1);

      *lpw++ = 0; // No menu
      *lpw++ = 0; // Default to dialog

      lpwstr = reinterpret_cast<WCHAR*>(lpw);

      int nChars = getResStringW(titleID, lpwstr);
      lpwstr += nChars;
      lpw = reinterpret_cast<UWORD*>(lpwstr);

      /*
       * Set up Font... note that DS_FONT must be in the style for this to work
       */

      *lpw++ = 8;    // Height of font
      lpwstr = reinterpret_cast<WCHAR*>(lpw);
      nChars=  MultiByteToWideChar(CP_ACP, 0, "Helv", -1, lpwstr, 100);
      lpwstr += nChars;
      lpw = reinterpret_cast<UWORD*>(lpwstr);

      return lpw;
   }

   UWORD* addGroupBox(UWORD* lpw, UINT titleID, int x, int y, int w, int h)
   {
      DLGITEMTEMPLATE* item = reinterpret_cast<DLGITEMTEMPLATE*>(align4(lpw));

      item->style = BS_GROUPBOX | BS_LEFT | WS_CHILD | WS_VISIBLE;
      item->dwExtendedStyle = 0;
      item->x = x;
      item->y = y;
      item->cx = w;
      item->cy = h;
      item->id = static_cast<WORD>(-1);

      lpw = reinterpret_cast<UWORD*>(item + 1);
      *lpw++ = 0xffff;
      *lpw++ = 0x0080;        // Button

      WCHAR* lpwstr = reinterpret_cast<WCHAR*>(lpw);
      int nChars = getResStringW(titleID, lpwstr);
      lpwstr += nChars;

      lpw = reinterpret_cast<UWORD*>(lpwstr);
      *lpw++ = 0;          // No creation data

      return lpw;
   }


   UWORD* addButton(UWORD* lpw, int x, int y, int w, int h, int buttonID, UINT nameID, DWORD style)
   {
      DLGITEMTEMPLATE* item = reinterpret_cast<DLGITEMTEMPLATE*>(align4(lpw));

      item->style = style;
      item->dwExtendedStyle = 0;
      item->x = x;
      item->y = y;
      item->cx = w;  // - 2;
      item->cy = h;  // - 2;
      item->id = buttonID;

      lpw = reinterpret_cast<UWORD*>(item + 1);
      *lpw++ = 0xffff;
      *lpw++ = 0x0080;        // Button

      WCHAR* lpwstr = reinterpret_cast<WCHAR*>(lpw);
      int nChars = getResStringW(nameID, lpwstr);
      lpwstr += nChars;

      lpw = reinterpret_cast<UWORD*>(lpwstr);
      *lpw++ = 0;          // No creation data

      return lpw;
   }

   UWORD* addButton(UWORD* lpw, int x, int y, int w, int h, int buttonID, UINT nameID)
   {
     return addButton(lpw, x, y, w, h, buttonID, nameID, BS_AUTOCHECKBOX | WS_CHILD | WS_VISIBLE | WS_TABSTOP);
   }
};

namespace OptionsDialog
{

/*
 * Positions and Sizes
 */

const int ItemWidth = 96;  // 74;
const int ItemHeight = 12; // 13
const int ItemXMargin = 4;
const int ItemYMargin = 10;

const int TabWidth = ItemWidth * 2 + ItemXMargin * 2;
const int TabHeight = ItemHeight * 11 + ItemYMargin;
const int MinimumTabWidth = TabWidth;
const int MinimumTabHeight = TabHeight;

/*
 * Table of data concerning Options
 */
/*
struct OptionInfo {
   int            s_option;   // What option does this refer to
   OptionPage     s_page;     // Which Options Page does it belong to
   int            s_resID;    // ID in resource file for text to display in dialog
   int            s_helpID;   // ID in help file
   Boolean        s_beginner; // Default starting value for beginner level
   Boolean        s_normal;   // Default for normal play
   Boolean        s_advanced; // Default for advanced play
};


#define IDH_POD_ShowAI -1

const int OptionInfo_HowMany = OPT_GameOption_HowMany + OPT_CampaignOption_HowMany;

static OptionInfo optionInfo[] = {
#ifdef DEBUG
   { OPT_InstantMove,         GameOptionsPage,  IDS_OPT_InstantMove,    IDH_POD_InstantMove,       False, False, False },
   { OPT_OrderEnemy,       GameOptionsPage,  IDS_OPT_OrderEnemy,        IDH_POD_OrderEnemy,     False, False, False },
   { OPT_ShowAI,           GameOptionsPage,  IDS_OPT_ShowAI,            IDH_POD_ShowAI,         False, False, False },
   { OPT_QuickMove,        GameOptionsPage,  IDS_OPT_QuickMove,         IDH_POD_QuickMove,         False, False, False },
#endif

//   { OPT_FullRoute,        GameOptionsPage,  IDS_OPT_FullRoute,         IDH_POD_FullRoute,         True,  True,  True  },
   { OPT_ColorCursor,         GameOptionsPage,  IDS_OPT_ColorCursor,    IDH_POD_ColorCursor,       False, False, False },
   { OPT_ToolTips,            GameOptionsPage,  IDS_OPT_ToolTips,       IDH_POD_ToolTips,          True,  True,  True  },
   { OPT_AlertBox,            GameOptionsPage,  IDS_OPT_AlertBox,       IDH_POD_AlertBox,          True,  True,  True  },
   { OPT_TownNames,        GameOptionsPage,  IDS_OPT_TownNames,         IDH_POD_TownNames,         True,  True,  True  },
   { OPT_NationDisplay,    GameOptionsPage,  IDS_OPT_NationDisplay,     IDH_POD_NationDisplay,     False, True,  True  },
   { OPT_TownIcons,        GameOptionsPage,  IDS_OPT_TownIcons,         IDH_POD_TownIcons,         True,  True,  True  },
   { OPT_TownHilightIcons,    GameOptionsPage,  IDS_OPT_TownHilightIcons,  IDH_POD_TownHilightIcons,  True,  True,  True  },
   { OPT_DrawMapScale,         GameOptionsPage,    IDS_OPT_DrawMapScale,       IDH_POD_DrawMapScale,      True,  True,  True  },

#ifdef DEBUG
   { OPT_LimitedUnitInfo,     RealismPage,       IDS_OPT_LimitedInfoUnits, IDH_POD_LimitedUnitInfo,   False, True,  True  },
   { OPT_LimitedTownInfo,     RealismPage,       IDS_OPT_LimitedInfoTowns, IDH_POD_LimitedTownInfo,   False, False,  True },
   { OPT_InstantOrders,    RealismPage,       IDS_OPT_InstantOrders,       IDH_POD_InstantOrders,     True,  True,  False },
   { OPT_AggressionChange,    RealismPage,       IDS_OPT_AgressionChange,  IDH_POD_AggressChange,     False, False,  True },

   { OPT_Supply,           RealismPage,       IDS_OPT_Supply,           IDH_POD_SupplyRules,    False, True,   True },
   { OPT_Attrition,        RealismPage,       IDS_OPT_Attrition,        IDH_POD_Attrition,         False, True,   True },
   { OPT_NationRules,         RealismPage,        IDS_OPT_NationRules,     IDH_POD_NationRules,        False, False,  True },
#endif
   { OptionInfo_HowMany }     // Mark end of table
};
*/

/*
 * Arbitary IDs for buttons in options dialog
 * Important to make sure that POD_OK and POD_CANCEL (defined in resdef.h)
 * do not overlap these ranges.
 */

const int ButtonID = 1000;
const int RealismButtonID = 2000;
const int MsgID = 3000;

inline int optionToButton(int opt)
{
   return opt + ButtonID;
}

inline int campaignOptionToButton(int opt)
{
   return opt + RealismButtonID;
}

inline GameOptionEnum buttonToEnum(int b)
{
   b -= ButtonID;
   ASSERT(b >= 0);
   ASSERT(b < OPT_GameOption_HowMany);
   return static_cast<GameOptionEnum>(b);
}

inline CampaignOptionEnum buttonToCampaignEnum(int b)
{
   b -= ButtonID;
   ASSERT(b >= 0);
   ASSERT(b < OPT_CampaignOption_HowMany);
   return static_cast<CampaignOptionEnum>(b);
}

bool isOptionButton(int b)
{
   return (b >= ButtonID) && (b < (ButtonID + OPT_GameOption_HowMany+OPT_CampaignOption_HowMany));
}

bool isMessagePage(OptionPageEnum page)
{
   return (page >= ShowMessagePage) && (page <= LastMessagePage);
}

inline int msgToButton(CampaignMessages::CMSG_ID msg)
{
   return msg + MsgID;
}

inline CampaignMessages::CMSG_ID buttonToMsg(int b)
{
   b -= MsgID;
   ASSERT(b >= 0);
   ASSERT(b < CampaignMessages::CMSG_HowMany);
   return static_cast<CampaignMessages::CMSG_ID>(b);
}

CampaignMessages::MessageGroup pageToMsgGroup(OptionPageEnum page)
{
   ASSERT(isMessagePage(page));

   return static_cast<CampaignMessages::MessageGroup>(static_cast<int>(page) - static_cast<int>(ShowMessagePage));
}

OptionPageEnum msgGroupToPage(CampaignMessages::MessageGroup group)
{
   ASSERT(group < CampaignMessages::Group_HowMany);

   OptionPageEnum page =  static_cast<OptionPageEnum>(static_cast<int>(group) + static_cast<int>(ShowMessagePage));

   ASSERT(isMessagePage(page));

   return page;
}

/*
 * Titles of pages
 */

static int pageTitles[OptionPage_HowMany] =
{
   IDS_OPT_RealismPage,    // RealismPage
   IDS_OPT_MiscPage,       // GameOptionsPage
   IDS_MSG_GROUP0,         // ShowMessagePage
   IDS_MSG_GROUP1,
   IDS_MSG_GROUP2,
   IDS_MSG_GROUP3
};

/*
 * Define Sub-Pages
 */

/*
 * Base class
 */

class PlayerOptionPage : public ModelessDialog {
   DLGTEMPLATE*   d_dialog;         // Dialog Template
   PlayerOptionsDial*   d_owner;
   OptionPageEnum d_page;
public:
   PlayerOptionPage(PlayerOptionsDial* owner, OptionPageEnum page) { d_owner = owner; d_dialog = 0; d_page = page; }
   virtual ~PlayerOptionPage() { d_dialog = 0; d_owner = 0; }

   // void enable();
   //    // Enable window
    //
   // void disable();
   //    // Disable window

   void setPosition();
      // Set window position

   const DLGTEMPLATE* dlgTemplate() const { return d_dialog; }

   DLGTEMPLATE* dlgTemplate(DLGTEMPLATE* dlgTemplate)
   {
      DLGTEMPLATE* old = d_dialog;
      d_dialog = dlgTemplate;
      return old;
   }  // Get and set Dialog Template

   PlayerOptionsDial* owner() const { return d_owner; }

   OptionPageEnum page() const { return d_page; }

   HWND create() { return create(d_owner->getHWND()); }
   HWND create(HWND hParent);
      // Create a window


   virtual void makeTemplate() = 0;
   virtual UINT getTitleID() = 0;

   virtual void saveSettings() = 0;
   virtual void initSettings() = 0;
private:
   virtual BOOL procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
   virtual BOOL onInitDialog(HWND hwnd, HWND hwndFocus, LPARAM lParam);
   // virtual void onDestroy(HWND hwnd) { }
   virtual void onNotify(HWND hwnd, int id, NMHDR* lpNMHDR);

};

/*
 * Class for options pages created dynamically
 */

class AutoOptionPage : public PlayerOptionPage {
   public:
      AutoOptionPage(PlayerOptionsDial* parent, OptionPageEnum page);
      ~AutoOptionPage();

      void makeTemplate();

      UINT getTitleID() { return pageTitles[page()]; }

      void saveSettings();
      void initSettings();
   private:
      // BOOL procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
      // BOOL onInitDialog(HWND hwnd, HWND hwndFocus, LPARAM lParam);
      // void onDestroy(HWND hwnd) { }
      // void onNotify(HWND hwnd, int id, NMHDR* lpNMHDR);

      const OptionInfo* d_options;
      int d_maxOption;
};

class MsgShowPage : public PlayerOptionPage {
      CampaignMessages::MessageGroup d_group;

   public:
      MsgShowPage(PlayerOptionsDial* parent, OptionPageEnum page);
      ~MsgShowPage();

      void makeTemplate();

      UINT getTitleID() { return CampaignMessages::getGroupID(d_group); }

      void saveSettings();
      void initSettings();
   private:
      // BOOL procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
      // BOOL onInitDialog(HWND hwnd, HWND hwndFocus, LPARAM lParam);
      // void onDestroy(HWND hwnd) { }
      // void onNotify(HWND hwnd, int id, NMHDR* lpNMHDR);
};



/*--------------------------------------------------------------
 * PlayerOptionPage Implemention
 */

HWND PlayerOptionPage::create(HWND hParent)
{
    ASSERT(d_dialog != 0);
    HWND hDialog = createDialogIndirect(d_dialog, hParent);
    ASSERT(hDialog != NULL);
    return hDialog;
}


// void PlayerOptionPage::enable()
// {
//    ASSERT(hWnd != NULL);
//    ShowWindow(hWnd, SW_SHOW);
// }
//
// void PlayerOptionPage::disable()
// {
//    ASSERT(hWnd != NULL);
//    ShowWindow(hWnd, SW_HIDE);
// }


void PlayerOptionPage::setPosition()
{
   const RECT& r = d_owner->getDialogRect();

#ifdef DEBUG
   debugLog("setPosition(%ld,%ld,%ld,%ld)\n",
      r.left, r.top, r.right, r.bottom);
#endif

   SetWindowPos(getHWND(), HWND_TOP, r.left, r.top, r.right-r.left, r.bottom-r.top, 0);
}

#ifdef __WATCOM_CPLUSPLUS__
#pragma warning 387 9   // Disable Side effect warning
#endif

//lint -emacro(522,HANDLE_WM_INITDIALOG)
//lint -emacro(522,HANDLE_WM_COMMAND)

BOOL PlayerOptionPage::procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
   switch(msg)
   {
      case WM_INITDIALOG:
         HANDLE_WM_INITDIALOG(hWnd, wParam, lParam, onInitDialog);
         break;
#if 0
      case WM_DESTROY:
         HANDLE_WM_DESTROY(hWnd, wParam, lParam, onDestroy);
         break;
#endif
      case WM_COMMAND:
         HANDLE_WM_COMMAND(hWnd, wParam, lParam, d_owner->onCommand);
         break;

      case WM_NOTIFY:
         HANDLE_WM_NOTIFY(hWnd, wParam, lParam, onNotify);
         break;

      default:
         return FALSE;
   }

   return TRUE;
}

#ifdef __WATCOM_CPLUSPLUS__
#pragma warning 387 3   // Re-enable Side effect warning
#endif

BOOL PlayerOptionPage::onInitDialog(HWND hwnd, HWND hwndFocus, LPARAM lParam)
{
   setPosition();
   initSettings();
   return TRUE;
}


void PlayerOptionPage::onNotify(HWND hwnd, int id, NMHDR* lpNMHDR)
{
  switch(lpNMHDR->code)
  {

    case PSN_SETACTIVE:
    {
      DWORD flags;
      if(d_page == LastPage)
         flags = PSWIZB_BACK | PSWIZB_FINISH;
      else
         flags = PSWIZB_BACK | PSWIZB_NEXT;

      HWND hPropSheet = GetParent(hwnd);
      PostMessage(hPropSheet, PSM_SETWIZBUTTONS, 0, (LPARAM)flags);
      break;
    }

    case PSN_WIZFINISH:
      owner()->onOK();
      break;

    default:
      break;
  }
}


//====================== Auto-Created implementation =================

AutoOptionPage::AutoOptionPage(PlayerOptionsDial* parent, OptionPageEnum page) :
   PlayerOptionPage(parent, page),
   d_options(0),
   d_maxOption(0)
{
   //lint -save -e788 ... enum not used in switch
   switch(page)
   {
      case RealismPage:
         d_options = OptionSettingsInfo::campaignOptionsSettings;
         d_maxOption = OPT_CampaignOption_HowMany;
         break;
      case GameOptionsPage:
         d_options = OptionSettingsInfo::gameOptionsSettings;
         d_maxOption = OPT_GameOption_HowMany;
         break;
      default:
         FORCEASSERT("Illegal Page for AutoOptionPage");
   }
   //lint -restore
}

AutoOptionPage::~AutoOptionPage()
{
   ::operator delete(dlgTemplate(0));
   d_options = 0;
}


void AutoOptionPage::makeTemplate()
{
   static UBYTE buffer[2048];

   DLGTEMPLATE* pTemplate = reinterpret_cast<DLGTEMPLATE*>(buffer);
   UWORD* lpw;

   /*
    * Set up header
    */

   lpw = DTemplate::makeHeader(pTemplate, pageTitles[page()], TabWidth, TabHeight);

   /*
    * Set up outer box
    */

   lpw = DTemplate::addGroupBox(lpw, pageTitles[page()], 0, 0, TabWidth, TabHeight);
   pTemplate->cdit++;      // Increment number of controls

   /*
    * set up each item
    */

   const int maxY = pTemplate->cy - ItemHeight;
   const int maxX = pTemplate->cx - ItemWidth;

   int x = ItemXMargin;
   int y = ItemYMargin;

   // for(const OptionInfo* info = OptionSettingsInfo::gameOptionsSettings;
   //   info->s_option != OPT_GameOption_HowMany;
   //   info++)
   for(const OptionInfo* info = d_options;
      info->s_option != d_maxOption;
      info++)
   {
      if(info->s_page == page())
      {
         // Add button

// #ifdef DEBUG
         int id = (page() == RealismPage) ? campaignOptionToButton(info->s_option) :
           optionToButton(info->s_option);
// #else
//         int id = optionToButton(info->s_option);
// #endif
         lpw = DTemplate::addButton(lpw, x, y, ItemWidth, ItemHeight, id, info->s_resID);
         pTemplate->cdit++;      // Increment number of controls

         // Update coordinates

         y += ItemHeight;
         if(y >= maxY)
         {
            y = ItemYMargin;
            x += ItemWidth;
            ASSERT(x < maxX);
         }

      }
   }

   size_t used = reinterpret_cast<size_t>(lpw) - reinterpret_cast<size_t>(buffer);

   DLGTEMPLATE* templateMemory = reinterpret_cast<DLGTEMPLATE*>(operator new(used));
   ASSERT(templateMemory != 0);
   memcpy(templateMemory, buffer, used);

   dlgTemplate(templateMemory);
}

void AutoOptionPage::initSettings()
{
   HWND hwnd = getHWND();

   /*
    * Set buttons to correct state.
    */

   // for(const OptionInfo* info = OptionSettingsInfo::gameOptionsSettings;
   //   info->s_option != OPT_GameOption_HowMany;
   //   info++)
   for(const OptionInfo* info = d_options;
      info->s_option != d_maxOption;
      info++)
   {
      if(info->s_page == page())
      {
// #ifdef DEBUG
         int buttonID = (page() == RealismPage) ? campaignOptionToButton(info->s_option) :
            optionToButton(info->s_option);

         Boolean set = (page() == RealismPage) ? CampaignOptions::get(static_cast<CampaignOptionEnum>(info->s_option)) :
            Options::get(static_cast<GameOptionEnum>(info->s_option));
// #else
//         int buttonID = optionToButton(info->s_option);
//         Boolean set = Options::get(static_cast<GameOptionEnum>(info->s_option));
// #endif

         setButtonCheck(hwnd, buttonID, set);
      }
   }
}

void AutoOptionPage::saveSettings()
{
   HWND hwnd = getHWND();

   /*
    * Set buttons to correct state.
    */

   // for(const OptionInfo* info = OptionSettingsInfo::gameOptionsSettings;
   //   info->s_option != OPT_GameOption_HowMany;
   //   info++)
   for(const OptionInfo* info = d_options;
      info->s_option != d_maxOption;
      info++)
   {
      if(info->s_page == page())
      {
// #ifdef DEBUG
         if(page() == RealismPage)
         {
           Button b(hwnd, campaignOptionToButton(info->s_option));
           CampaignOptions::set(static_cast<CampaignOptionEnum>(info->s_option), b.getCheck());
         }
         else
// #endif
         {
           Button b(hwnd, optionToButton(info->s_option));
           Options::set(static_cast<GameOptionEnum>(info->s_option), b.getCheck());
         }
      }
   }
}

/*=========================================================================
 * MessageShow Options
 */

MsgShowPage::MsgShowPage(PlayerOptionsDial* parent, OptionPageEnum page) :
   PlayerOptionPage(parent, page),
   d_group(pageToMsgGroup(page))
{
}

MsgShowPage::~MsgShowPage()
{
   ::operator delete(dlgTemplate(0));
}

void MsgShowPage::makeTemplate()
{
   static UBYTE buffer[2048];
   DLGTEMPLATE* pTemplate = reinterpret_cast<DLGTEMPLATE*>(buffer);
   UWORD* lpw;

   lpw = DTemplate::makeHeader(pTemplate, CampaignMessages::getGroupID(d_group), TabWidth, TabHeight);
   lpw = DTemplate::addGroupBox(lpw, CampaignMessages::getGroupID(d_group), 0, 0, TabWidth, TabHeight);
   pTemplate->cdit++;      // Increment number of controls

   /*
    * Add buttons
    */

   /*
    * set up each item
    */

   const int w = ItemWidth;
   const int h = ItemHeight;
   const int maxY = pTemplate->cy - h;
   const int maxX = pTemplate->cx - w;

   int x = ItemXMargin;
   int y = ItemYMargin;

   using namespace CampaignMessages;

   for(CMSG_ID msgID = CMSG_First; msgID < CMSG_HowMany; INCREMENT(msgID))
   {
      if(getGroup(msgID) == d_group)
      {
         // Add button

         lpw = DTemplate::addButton(lpw, x, y, ItemWidth, ItemHeight, msgToButton(msgID), getDescriptionID(msgID));
         pTemplate->cdit++;      // Increment number of controls

         // Update coordinates

         y += ItemHeight;
         if(y >= maxY)
         {
            y = ItemYMargin;
            x += ItemWidth;
            ASSERT(x < maxX);
         }

      }
   }

   size_t used = reinterpret_cast<size_t>(lpw) - reinterpret_cast<size_t>(buffer);

   DLGTEMPLATE* templateMemory = reinterpret_cast<DLGTEMPLATE*>(operator new(used));
   ASSERT(templateMemory != 0);
   memcpy(templateMemory, buffer, used);

   dlgTemplate(templateMemory);
}

void MsgShowPage::initSettings()
{
   HWND hwnd = getHWND();

   using namespace CampaignMessages;

   for(CMSG_ID msgID = CMSG_First; msgID < CMSG_HowMany; INCREMENT(msgID))
   {
      if(getGroup(msgID) == d_group)
      {
         int buttonID = msgToButton(msgID);
         setButtonCheck(hwnd, buttonID, MessageOptions::get(msgID));
      }
   }
}

void MsgShowPage::saveSettings()
{
   HWND hwnd = getHWND();

   using namespace CampaignMessages;

   for(CMSG_ID msgID = CMSG_First; msgID < CMSG_HowMany; INCREMENT(msgID))
   {
      if(getGroup(msgID) == d_group)
      {
         Button b(hwnd, msgToButton(msgID));
         MessageOptions::set(msgID, b.getCheck());
      }
   }
}


};    // Namespace OptionsDialog



/*==========================================================================
 * PlayerOptionsDial Implementation
 */

/*
 * Constructor
 */

PlayerOptionsDial::PlayerOptionsDial(CampaignInterface* campaign, HWND parent, OptionPage page) :
   d_campaign(campaign),
   hwndParent(parent)
{
  setDeleteSelfMode(false);

  ASSERT(page < OptionPage_HowMany);
//  ASSERT(optMan != 0);

  d_mode = HasButtons;

  makePages();

  tabHwnd = NULL;                // Tab Dialog handle
  curPage = page;
  SetRectEmpty(&tdRect);

  HWND dhwnd = createDialog(playerOptionsDlgName, parent);
  ASSERT(dhwnd != NULL);
}


PlayerOptionsDial::PlayerOptionsDial(HWND parent) :
   d_campaign(0),
// d_optManager(optMan),
   d_mode(NoButtons),
   hwndParent(parent),
   d_helpIDs(0) {

   ASSERT(hwndParent != 0);

   setDeleteSelfMode(false);

   makePages();

   /*
    * Initialise pages
    */

   for(int i = 0; i < OptionPage_HowMany; i++)
      d_pages[i]->create(hwndParent);
}

PlayerOptionsDial::PlayerOptionsDial() :
   d_campaign(0),
   d_mode(NoButtons),
   hwndParent(0),
   d_helpIDs(0)
{
   setDeleteSelfMode(false);

   makePages();
}

/*
 * Destructor
 */

PlayerOptionsDial::~PlayerOptionsDial()
{


   DestroyWindow(getHWND());

   if(d_helpIDs) {
      delete[] d_helpIDs;
      d_helpIDs = 0;
   }
   if(d_pages) {
      delete[] d_pages;
      d_pages = 0;
   }

}

/*
 * Create pages
 */

void PlayerOptionsDial::makePages()
{
   d_pages = new PlayerOptionPage*[OptionPage_HowMany];
   ASSERT(d_pages != 0);

   for(int i = 0; i < OptionPage_HowMany; i++)
      d_pages[i] = 0;

   for(OptionPage p = FirstPage; p < OptionPage_HowMany; INCREMENT(p))
   {
      if(isMessagePage(p))
         d_pages[p] = new MsgShowPage(this, p);
      else
         d_pages[p] = new AutoOptionPage(this, p);

      ASSERT(d_pages[p] != 0);

      d_pages[p]->makeTemplate();
   }

   /*
    * Make HelpIDs
    */

#ifdef DEBUG
   // int nItems = OPT_GameOption_HowMany; //sizeof(OptionSettingsInfo::gameOptionsSettings) / sizeof(OptionInfo);
   unsigned int nItems = 0;
   const OptionInfo* pInfo;
   for(pInfo = OptionSettingsInfo::gameOptionsSettings;
     pInfo->s_option != OPT_GameOption_HowMany;
     pInfo++)
   {
         ++nItems;
         ASSERT(nItems < 1000);  // check for corrupt table
   }
   for(pInfo = OptionSettingsInfo::campaignOptionsSettings;
     pInfo->s_option != OPT_CampaignOption_HowMany;
     pInfo++)
   {
         ++nItems;
         ASSERT(nItems < 1000);  // check for corrupt table
   }

   ASSERT(nItems == (static_cast<int>(OPT_GameOption_HowMany) + static_cast<int>(OPT_CampaignOption_HowMany)));    // table is missing some options
#else
   unsigned int nItems = OPT_GameOption_HowMany + OPT_CampaignOption_HowMany;
   const OptionInfo* pInfo;
#endif

   d_helpIDs = new HelpIDList[nItems + 1];      // leave space for empty one at end
   ASSERT(d_helpIDs != 0);
   HelpIDList* pID = d_helpIDs;

   for(pInfo = OptionSettingsInfo::gameOptionsSettings;
      pInfo->s_option != OPT_GameOption_HowMany;
      pInfo++, pID++)
   {
// #ifdef DEBUG
//       pID->s_control = (pInfo->s_page == RealismPage) ? campaignOptionToButton(pInfo->s_option) :
//         optionToButton(pInfo->s_option);
// #else
      pID->s_control = optionToButton(pInfo->s_option);
// #endif
      pID->s_topic = pInfo->s_helpID;
   }
   for(pInfo = OptionSettingsInfo::campaignOptionsSettings;
      pInfo->s_option != OPT_CampaignOption_HowMany;
      pInfo++, pID++)
   {
      pID->s_control = campaignOptionToButton(pInfo->s_option);
// #ifdef DEBUG
//       pID->s_control = (pInfo->s_page == RealismPage) ? campaignOptionToButton(pInfo->s_option) :
//         optionToButton(pInfo->s_option);
// #else
//       pID->s_control = optionToButton(pInfo->s_option);
// #endif
      pID->s_topic = pInfo->s_helpID;
   }

   pID->s_control = 0;
   pID->s_topic = 0;
}

/*
 * Set current page
 */

void PlayerOptionsDial::setPage(OptionPage page)
{
   if(page != curPage)
   {
      TabCtrl_SetCurSel(tabHwnd, page);
      onSelChanged();
   }
}


const DLGTEMPLATE* PlayerOptionsDial::getPageTemplate(OptionPage page) const
{
  ASSERT(page < OptionPage_HowMany);
  return d_pages[page]->dlgTemplate();
}

/*
 * Windows Message Processor
 */

#ifdef __WATCOM_CPLUSPLUS__
#pragma warning 387 9   // Disable Side effect warning
#endif

BOOL PlayerOptionsDial::procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
   switch(msg)
   {
      case WM_INITDIALOG:
         HANDLE_WM_INITDIALOG(hWnd, wParam, lParam, onInitDialog);
         break;

      case WM_DESTROY:
         HANDLE_WM_DESTROY(hWnd, wParam, lParam, onDestroy);
         break;
      case WM_COMMAND:
         HANDLE_WM_COMMAND(hWnd, wParam, lParam, onCommand);
         break;
      case WM_CLOSE:
         HANDLE_WM_CLOSE(hWnd, wParam, lParam, onClose);
         break;
      case WM_CONTEXTMENU:
         HANDLE_WM_CONTEXTMENU(hWnd, wParam, lParam, onContextMenu);
         break;
      case WM_NOTIFY:
         HANDLE_WM_NOTIFY(hWnd, wParam, lParam, onNotify);
         break;
      default:
         return FALSE;
   }

   return TRUE;
}

#ifdef __WATCOM_CPLUSPLUS__
#pragma warning 387 8   // Re-enable Side effect warning
#endif

void PlayerOptionsDial::onContextMenu(HWND hwnd, HWND hwndCtl, int x, int y)
{
   ASSERT(d_helpIDs != 0);
//   WinHelp(hwndCtl, "help\\Wg95Help.hlp", HELP_CONTEXTMENU, reinterpret_cast<DWORD>(d_helpIDs));
}

BOOL PlayerOptionsDial::onInitDialog(HWND hwnd, HWND hwndFocus, LPARAM lParam)
{

   RECT rcTab;
   rcTab.left = 0;
   rcTab.top = 0;
   rcTab.right = MinimumTabWidth;      // Minimum size in dialog units
   rcTab.bottom = MinimumTabHeight;


   int i;
   for(i = 0; i < OptionPage_HowMany; i++)
   {
      // DLGTEMPLATE* dialog = pages[i]->getDialog();
      const DLGTEMPLATE* dialog = d_pages[i]->dlgTemplate();

      ASSERT(dialog != NULL);

      /*
       * Adjust for maximum size
       */

      if(dialog->cx > rcTab.right)
         rcTab.right = dialog->cx;
      if(dialog->cy > rcTab.bottom)
         rcTab.bottom = dialog->cy;
   }

#ifdef DEBUG
   debugLog("Largest Dialog (Dialog Units): %ld,%ld,%ld,%ld\n",
      rcTab.left, rcTab.top, rcTab.right, rcTab.bottom);
#endif

   // Get the position of the box (convert from dialog units)

   LONG dbUnits = GetDialogBaseUnits();
   LONG dbX = LOWORD(dbUnits);
   LONG dbY = HIWORD(dbUnits);
   // LONG xMargin = (3 * dbX) / 4;
   LONG xMargin = (2 * dbX) / 4;
   // LONG yTop = (20 * dbY) / 8;
   LONG yTop = 0; // ( * dbY) / 8;
   LONG yMargin = (4 * dbY) / 8;

   // Convert to pixel coordinates

   MapDialogRect(hwnd, &rcTab);

#ifdef DEBUG
   debugLog("Largest Dialog (Pixels): %ld,%ld,%ld,%ld\n",
      rcTab.left, rcTab.top, rcTab.right, rcTab.bottom);
#endif

   /*
    * Create a tabbed window
    * Don't be concerned with the size, because that will
    * be set up in a little while, but we need a handle to
    * a tabbed control to use AdjustRect
    *
    * Do need to set up width, so that AdjustRect doesn't think
    * it needs to do several rows of buttons
    */

   tabHwnd = CreateWindow(
      WC_TABCONTROL, "",
      WS_CHILD | WS_CLIPSIBLINGS | WS_VISIBLE |
         TCS_TOOLTIPS | TCS_RIGHTJUSTIFY | TCS_MULTILINE,
      0, 0, rcTab.right, 100,
      hwnd,
      (HMENU) POD_TABBED,
      APP::instance(),
      NULL);

   ASSERT(tabHwnd != NULL);

#ifdef DEBUG
   debugLog("TabHwnd = %08lx\n", (ULONG) tabHwnd);
#endif

   /*
    * Set up the tabbed titles
    * If possible try and get the title from the Dialog's CAPTION
    *
    * This must be done before using AdjustRect
    */

   TC_ITEM tie;
   tie.mask = TCIF_TEXT | TCIF_IMAGE;
   tie.iImage = -1;

   for(i = 0; i < OptionPage_HowMany; i++)
   {
      // ResString title(pageTitles[i]);
      ResString title(d_pages[i]->getTitleID());

      tie.pszText = const_cast<LPTSTR>(title.c_str()); // (char*) pages[i]->getTitle();

      TabCtrl_InsertItem(tabHwnd, i, &tie);
   }

   /*
    * Convert coordinates to pixel coordinates
    * and Move the window
    */

   // Work out complete size with tabs, and shift to desired location

   TabCtrl_AdjustRect(tabHwnd, TRUE, &rcTab);
#ifdef DEBUG
   debugLog("Tab Size: %ld,%ld,%ld,%ld\n",
      rcTab.left, rcTab.top, rcTab.right, rcTab.bottom);
#endif
   OffsetRect(&rcTab, xMargin - rcTab.left, yTop - rcTab.top);
#ifdef DEBUG
   debugLog("Offset Tab: %ld,%ld,%ld,%ld\n",
      rcTab.left, rcTab.top, rcTab.right, rcTab.bottom);
#endif

   // Move tabbed Window

   SetWindowPos(tabHwnd, NULL,
      rcTab.left, rcTab.top,
      rcTab.right - rcTab.left, rcTab.bottom - rcTab.top,
      SWP_NOZORDER);

   // Get display area

   CopyRect(&tdRect, &rcTab);
   TabCtrl_AdjustRect(tabHwnd, FALSE, &tdRect);    // Get display area
#ifdef DEBUG
   debugLog("Display Area: %ld,%ld,%ld,%ld\n",
      tdRect.left, tdRect.top, tdRect.right, tdRect.bottom);
#endif

   /*
    * Move the lower buttons (to rcTab.bottom + a bit)
    */

   RECT rcButton;
   SetRect(&rcButton, 0, 0, 0, 0);
   LONG y = rcTab.bottom + yMargin;

   if(d_mode == HasButtons)
   {
     LONG x = rcTab.right;
//   y = rcTab.bottom + yMargin;

     HWND hwndButton = GetDlgItem(hwnd, POD_OK);
     ASSERT(hwndButton != 0);
     GetWindowRect(hwndButton, &rcButton);
     x -= rcButton.right - rcButton.left;
     SetWindowPos(hwndButton, NULL, x, y, 0, 0, SWP_NOSIZE | SWP_NOZORDER);

#ifdef DEBUG
     debugLog("OK: %ld,%ld,%ld,%ld\n",
        x, y, rcButton.right-rcButton.left, rcButton.bottom-rcButton.top);
#endif


     hwndButton = GetDlgItem(hwnd, POD_CANCEL);
     ASSERT(hwndButton != 0);
     GetWindowRect(hwndButton, &rcButton);
     x -= rcButton.right - rcButton.left + xMargin;
     SetWindowPos(hwndButton, NULL, x, y, 0, 0, SWP_NOSIZE | SWP_NOZORDER);
#ifdef DEBUG
     debugLog("Cancel: %ld,%ld,%ld,%ld\n",
        x, y, rcButton.right-rcButton.left, rcButton.bottom-rcButton.top);
#endif
   }

   /*
    * Adjust size of overall dialogue box
    *
    * Would be a good idea to adjust coordinates
    * so that box is on screen.
    *
    * e.g. if off right, adjust coords to be left of given position
    * if off bottom, adjust to be above.
    */

   rcTab.bottom = y + yMargin + rcButton.bottom - rcButton.top;
   rcTab.bottom += GetSystemMetrics(SM_CYCAPTION);
   rcTab.bottom += GetSystemMetrics(SM_CYDLGFRAME) * 2;
   // rcTab.bottom -= rcTab.top;

   rcTab.right += xMargin * 2;
   rcTab.right += GetSystemMetrics(SM_CXDLGFRAME) * 2;
   rcTab.right -= rcTab.left;

#ifdef DEBUG
   debugLog("Overall Window: %ld,%ld,%ld,%ld\n",
      rcTab.left, rcTab.top, rcTab.right, rcTab.bottom);
#endif

   PixelPoint position(100, 100);

   SetWindowPos(hwnd, NULL,
      position.getX(),
      position.getY(),
      rcTab.right,
      rcTab.bottom,
      SWP_NOZORDER);

   /*
    * Initialise pages
    */

   for(i = 0; i < OptionPage_HowMany; i++)
      d_pages[i]->create();

   /*
    * For testing: create an initial dialog
    */

   if(curPage > 0)
     TabCtrl_SetCurSel(tabHwnd, curPage);


   onSelChanged();

   /*
    * End of tabbed dialog setup
    * Now get on with positioning ourselves and setting the controls
    */

   /*===============================================================
    * Set the position
    * Ought to do a bit of checking to see if it will fit on screen
    */

  return TRUE;
}

void PlayerOptionsDial::onDestroy(HWND hwnd)
{
  SendMessage(hwndParent, WM_PARENTNOTIFY, (WPARAM)WM_DESTROY, (LPARAM)hwnd);
}

void PlayerOptionsDial::onCommand(HWND hwnd, int id, HWND hwndCtl, UINT codeNotify)
{
   if(id == POD_OK)
      onOK();
   else if(id == POD_CANCEL)
      onClose(hwnd);
}

LRESULT PlayerOptionsDial::onNotify(HWND hWnd, int id, NMHDR* lpNMHDR)
{
#ifdef DEBUG
   debugLog("PlayerOptionsDial::onNotify(%08lx %d %08lx %d %d)\n",
      (ULONG) hWnd,
      (int) id,
      (ULONG) lpNMHDR->hwndFrom,
      (int) lpNMHDR->idFrom,
      (int) lpNMHDR->code);
#endif

   // Assume its from the tabbed dialog

   if(lpNMHDR->idFrom == POD_TABBED)
   {
      switch(lpNMHDR->code)
      {
      case TCN_SELCHANGING:
         return FALSE;

      case TCN_SELCHANGE:
         onSelChanged();
         break;

      default:
         break;
      }
   }

   return TRUE;
}

void PlayerOptionsDial::onSelChanged()
{
   int iSel = TabCtrl_GetCurSel(tabHwnd);
   if(curPage != iSel)
      d_pages[curPage]->show(false);  // disable();

   d_pages[iSel]->show(true);  // enable();
   curPage = iSel;
}


void PlayerOptionsDial::onClose(HWND hwnd)
{
  if(d_mode == HasButtons)
    DestroyWindow(hwnd);
}

void PlayerOptionsDial::onOK()
{
   for(int page = 0; page <= LastPage; page++)
   {
      d_pages[page]->saveSettings();
   }

   if(d_campaign)
      d_campaign->redrawMap();

   for(AlertBoxEnum i = AB_First; i < AB_HowMany; INCREMENT(i))
      AlertBoxOptions::set(i, Options::get(OPT_AlertBox));



  if(d_mode == HasButtons)
    onClose(getHWND());
}


/*=========================================================================
 * Campaign Options
 *
 * NOTE: This is used by FrontEnd
 */

class RealCampaignOptionDial : public WindowBaseND {
  private:
    // enum {
    //   CustomID = CampaignOptions::SkillLevel_HowMany
    // };

    const DrawDIB* d_fillDib;
    DrawDIBDC* d_windowDib;

    static const int s_width;
    static const int s_height;

  public:
    RealCampaignOptionDial(HWND parent);
    ~RealCampaignOptionDial();

  private:
    LRESULT procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
    void onCommand(HWND hWnd, int id, HWND hwndCtl, UINT codeNotify);
    BOOL onCreate(HWND hwnd, CREATESTRUCT* lpCreateStruct);
    void onPaint(HWND hwnd);
    void onDestroy(HWND hwnd);
    void onContextMenu(HWND hwnd, HWND hwndCtl, int x, int y);

    void allocateDib(int cx, int cy);
    void initSettings();
    void saveSettings();
         // Create the dialog
};

const int RealCampaignOptionDial::s_width = 380;
const int RealCampaignOptionDial::s_height = 380;

RealCampaignOptionDial::RealCampaignOptionDial(HWND hParent) :
  d_windowDib(0),
  d_fillDib(scenario->getSideBkDIB(SIDE_Neutral))
//  d_textMask(0)
{
  ASSERT(d_fillDib);

  HWND hWnd = createWindow(
      0,
      // GenericClass::className(),
        NULL,
      WS_CHILD | WS_CLIPSIBLINGS,
      0, 0, s_width, s_height,
      hParent, NULL // , APP::instance()
  );
  ASSERT(hWnd != NULL);
}

RealCampaignOptionDial::~RealCampaignOptionDial()
{
    selfDestruct();
    if(d_windowDib)
       delete d_windowDib;
   d_fillDib = 0;
}
/*
#ifdef DEBUG
static OptionInfo campOptionInfo[] = {
   { OPT_LimitedUnitInfo,     RealismPage,         IDS_OPT_LimitedInfoUnits,  IDH_POD_LimitedUnitInfo,   False,  True,  True },
   { OPT_LimitedTownInfo,     RealismPage,         IDS_OPT_LimitedInfoTowns,  IDH_POD_LimitedTownInfo,   False, False,  True },
   { OPT_InstantOrders,       RealismPage,         IDS_OPT_InstantOrders,     IDH_POD_InstantOrders,      True,  True, False },
   { OPT_AggressionChange,    RealismPage,         IDS_OPT_AgressionChange,   IDH_POD_AggressChange,     False, False,  True },

   { OPT_Supply,              RealismPage,         IDS_OPT_Supply,            IDH_POD_SupplyRules,       False, True,   True },
   { OPT_Attrition,           RealismPage,         IDS_OPT_Attrition,         IDH_POD_Attrition,         False, True,   True },
   { OPT_NationRules,         RealismPage,         IDS_OPT_NationRules,       IDH_POD_NationRules,       False, False,  True },

   { OPT_CampaignOption_HowMany }      // Mark end of table
};
#else
static OptionInfo campOptionInfo[] = {
   { OPT_LimitedUnitInfo,     DefaultPage,         IDS_OPT_LimitedInfoUnits,  IDH_POD_LimitedUnitInfo,   False,  True,  True },
   { OPT_LimitedTownInfo,     DefaultPage,         IDS_OPT_LimitedInfoTowns,  IDH_POD_LimitedTownInfo,   False, False,  True },
   { OPT_InstantOrders,       DefaultPage,         IDS_OPT_InstantOrders,     IDH_POD_InstantOrders,      True,  True, False },
   { OPT_AggressionChange,    DefaultPage,         IDS_OPT_AgressionChange,   IDH_POD_AggressChange,     False, False,  True },

   { OPT_Supply,              DefaultPage,         IDS_OPT_Supply,            IDH_POD_SupplyRules,       False, True,   True },
   { OPT_Attrition,           DefaultPage,         IDS_OPT_Attrition,         IDH_POD_Attrition,         False, True,   True },
   { OPT_NationRules,         DefaultPage,         IDS_OPT_NationRules,       IDH_POD_NationRules,       False, False,  True },

   { OPT_CampaignOption_HowMany }      // Mark end of table
};
#endif
*/

HWND createCustomButton(HWND hParent, const char* text, int id, int x, int y, int cx, int cy, Boolean checkBox)
{
  DWORD style = (checkBox) ? WS_CHILD | WS_VISIBLE | BS_AUTOCHECKBOX :
         WS_CHILD | WS_VISIBLE | BS_AUTOCHECKBOX | BS_PUSHLIKE;

  HWND hButton = CreateWindow(CUSTOMBUTTONCLASS/*"BUTTON"*/, text,
            style,
            x, y, cx, cy,
            hParent, (HMENU)id, APP::instance(), NULL);

  ASSERT(hButton != 0);

  CustomButton cb(hButton);
  cb.setBorderColours(scenario->getBorderColors());
  if(checkBox)
  {
    cb.setCheckBoxImages(ScenarioImageLibrary::get(ScenarioImageLibrary::CheckButtons));
    cb.setFillDib(scenario->getSideBkDIB(SIDE_Neutral));
  }
  else
    cb.setFillDib(scenario->getScenarioBkDIB(Scenario::BackPatterns::ButtonBackground));
    // cb.setFillDib(scenario->getScenarioBkDIB(Scenario::BackPatterns::WoodBackground));

  return hButton;

}

BOOL RealCampaignOptionDial::onCreate(HWND hwnd, CREATESTRUCT* lpCreateStruct)
{
  // some useful constants
  const int offsetX = 5;
  const int offsetY = 30;
  const int groupCX = lpCreateStruct->cx - (2*offsetX);
  const int nCheckRows = 2;
  const int checkCX = (lpCreateStruct->cx / nCheckRows) - (4 * offsetX);
  const int checkCY = 20;
  const int maxItemsPer = 8;
  const int buttonCX = 70;
  const int buttonCY = 30;
  const int buttonGroupCY = buttonCY + (2 * offsetX);
  const int checkOffsetY = 4;
  const int checkGroupCY = (2*offsetX) + (checkCY * maxItemsPer) + (checkOffsetY * (maxItemsPer - 1));
  const int windowCY = ((2*offsetX) + (2*offsetY) +  checkGroupCY + buttonGroupCY);
  const int maxY = windowCY - (checkCY + offsetX);
  const int maxX = lpCreateStruct->cx - (checkCX + (2 * offsetX));

  /*
   * calculate window height
   */


  allocateDib(lpCreateStruct->cx, windowCY);
  ASSERT(d_windowDib);

  /*
   *  draw group box for "Skill Level"
   */


  RECT r;
  SetRect(&r, offsetX, offsetY, offsetX + groupCX, offsetY + buttonGroupCY);

  SimpleString skillText;
  idsToString(skillText, IDS_OPT_SkillLevel);

  DIB_Utility::drawGroupBox(d_windowDib, 0, r,
       skillText.toStr(), scenario->getBorderColors());

  /*
   * add buttons to group
   */

  const int nSkills = CampaignOptions::SkillLevel_HowMany;
  static int s_skillButtonText[] = {
     IDS_OPT_Novice,
     IDS_OPT_Normal,
     IDS_OPT_Expert,
     IDS_OPT_Custom
  };

  int buttonSpacing = (lpCreateStruct->cx - (nSkills*buttonCX)) / (nSkills + 1);

  int y = ((r.bottom + r.top) - buttonCY) / 2;
  int x = buttonSpacing;

  for(int id = 0; id < nSkills; id++)
  {
    SimpleString buttonText;
    idsToString(buttonText, s_skillButtonText[id]);
    createCustomButton(hwnd, buttonText.toStr(), id, x, y, buttonCX, buttonCY, False);

    x += (buttonCX + buttonSpacing);
  }


  /*
   * Add buttons and group box for check options
   */

  /*
   * set up each item
   */

  buttonSpacing = (lpCreateStruct->cx - (nCheckRows*checkCX)) / (nCheckRows+1);

  y = (r.bottom + offsetY + offsetX);
  x = buttonSpacing;

  for(const OptionInfo* info = OptionSettingsInfo::campaignOptionsSettings;
      info->s_option != OPT_CampaignOption_HowMany;
      info++)
  {
     // Add button
     int id = campaignOptionToButton(info->s_option);

     SimpleString checkText;
     idsToString(checkText, info->s_resID);
     createCustomButton(hwnd, checkText.toStr(), id, x, y, checkCX, checkCY, True);

     // Update coordinates
     y += checkCY + checkOffsetY;
     if(y >= maxY)
     {
       y = (r.bottom + offsetY + offsetX);
       x += (checkCX + buttonSpacing);
       ASSERT(x < maxX);
     }
  }

  SetRect(&r, r.left, r.bottom + offsetY, r.right, (r.bottom + offsetY) + checkGroupCY);

  SimpleString realismText;
  idsToString(realismText, IDS_OPT_RealismPage);

  DIB_Utility::drawGroupBox(d_windowDib, 0, r,
       realismText.toStr(), scenario->getBorderColors());

  /*
   * adjust height of window
   */

  SetWindowPos(hwnd, HWND_TOP, 0, 0, lpCreateStruct->cx, windowCY, SWP_NOMOVE);

  initSettings();
  return True;
}

void RealCampaignOptionDial::allocateDib(int cx, int cy)
{
  if(!d_windowDib ||
     d_windowDib->getWidth() != cx ||
     d_windowDib->getHeight() != cy)
  {
    if(d_windowDib)
    {
      delete d_windowDib;
      d_windowDib = 0;
    }

    d_windowDib = new DrawDIBDC(cx, cy);
  }

  ASSERT(d_windowDib);
  ASSERT(d_fillDib);

  if(d_fillDib)   //lint !e774 ... always true
    d_windowDib->fill(d_fillDib);

  // draw border
  RECT r;
  SetRect(&r, 0, 0, d_windowDib->getWidth(), d_windowDib->getHeight());

  CustomBorderWindow cw(scenario->getBorderColors());
  cw.drawThinBorder(d_windowDib->getDC(), r);
}

void RealCampaignOptionDial::onPaint(HWND hwnd)
{
  PAINTSTRUCT ps;
  HDC hdc = BeginPaint(hwnd, &ps);

  HPALETTE oldPal = SelectPalette(hdc, Palette::get(), FALSE);
  RealizePalette(hdc);

  /*
   *  Put background image
   */

  ASSERT(d_windowDib);

  if(d_windowDib)    //lint !e774 ... always true
  {
    BOOL result = BitBlt(hdc, 0, 0, d_windowDib->getWidth(), d_windowDib->getHeight(),
       d_windowDib->getDC(), 0, 0, SRCCOPY);
   ASSERT(result);
  }

  SelectPalette(hdc, oldPal, FALSE);

  EndPaint(hwnd, &ps);
}


void RealCampaignOptionDial::initSettings()
{
   HWND hwnd = getHWND();

   for(int i = 0; i < CampaignOptions::SkillLevel_HowMany; i++)
   {
     CustomButton b(hwnd, i);
     b.setCheck(CampaignOptions::getSkillLevel() == i);
   }


   for(CampaignOptionEnum id = OPT_CampaignOptionFirst;
       id < OPT_CampaignOption_HowMany;
       INCREMENT(id))
   {
     int buttonID = campaignOptionToButton(id);

     CustomButton b(hwnd, buttonID);
     b.setCheck(CampaignOptions::get(id));

     b.enable( (CampaignOptions::getSkillLevel() == CampaignOptions::Custom) ? TRUE: FALSE);
//   setButtonCheck(hwnd, buttonID, CampaignOptions::get(id));
   }
}

void RealCampaignOptionDial::saveSettings()
{
   HWND hwnd = getHWND();

   for(CampaignOptionEnum id = OPT_CampaignOptionFirst;
       id < OPT_CampaignOption_HowMany;
       INCREMENT(id))
   {
     CustomButton b(hwnd, campaignOptionToButton(id));
     CampaignOptions::set(id, b.getCheck());
   }
}

void RealCampaignOptionDial::onCommand(HWND hWnd, int id, HWND hwndCtl, UINT codeNotify)
{
   switch(id)
   {
     case CampaignOptions::Novice:
     case CampaignOptions::Normal:
     case CampaignOptions::Expert:
     case CampaignOptions::Custom:
       CampaignOptions::setSkillLevel(static_cast<CampaignOptions::SkillLevel>(id));
       initSettings();
       break;
     default:
         break;
   }
}

LRESULT RealCampaignOptionDial::procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
  switch(msg)
  {
    HANDLE_MSG(hWnd, WM_CREATE,  onCreate);
    HANDLE_MSG(hWnd, WM_DESTROY, onDestroy);
    HANDLE_MSG(hWnd, WM_PAINT, onPaint);
    HANDLE_MSG(hWnd, WM_COMMAND, onCommand);

    default:
      return defProc(hWnd, msg, wParam, lParam);
  }
}

void RealCampaignOptionDial::onDestroy(HWND hwnd)
{
  saveSettings();
}

/*-------------------------------------------------------
 * Access to FrontEnd OptionDial
 */

CampaignOptionDial::CampaignOptionDial(HWND hwnd) :
  d_campOptionDial(new RealCampaignOptionDial(hwnd))
{
  ASSERT(d_campOptionDial);
}

CampaignOptionDial::~CampaignOptionDial()
{
  // if(d_campOptionDial)
  //   destroy();
  delete d_campOptionDial;
}

// void CampaignOptionDial::show(int x, int y)
// {
//   if(d_campOptionDial)
//     SetWindowPos(d_campOptionDial->getHWND(), HWND_TOP, x, y, 0, 0, SWP_NOSIZE | SWP_SHOWWINDOW);
// }
//
// void CampaignOptionDial::hide()
// {
//   if(d_campOptionDial)
//     ShowWindow(d_campOptionDial->getHWND(), SW_HIDE);
// }
//
// void CampaignOptionDial::destroy()
// {
//   if(d_campOptionDial)
//   {
//     DestroyWindow(d_campOptionDial->getHWND());
//     d_campOptionDial = 0;
//   }
// }
//
// HWND CampaignOptionDial::getHWND() const
// {
//   return (d_campOptionDial) ? d_campOptionDial->getHWND() : 0;
// }


HWND CampaignOptionDial::getHWND() const
{
    return d_campOptionDial->getHWND();
}

bool CampaignOptionDial::isVisible() const
{
  return d_campOptionDial->isVisible();
}

bool CampaignOptionDial::isEnabled() const
{
  return d_campOptionDial->isEnabled();
}

void CampaignOptionDial::show(bool visible)
{
  d_campOptionDial->show(visible);
}

void CampaignOptionDial::enable(bool enable)
{
  d_campOptionDial->enable(enable);
}


/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.3  2001/07/02 15:23:17  greenius
 * Battle, Campaign, Combined game and Campaign Editor now compile and run... mostly
 *
 * Revision 1.2  2001/06/18 08:25:45  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
