/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#error "Obsolete"

#ifndef TOWNMENU_HPP
#define TOWNMENU_HPP

#include "grtypes.hpp"
#include "gamedefs.hpp"

class TownMenu_Imp;
class CampaignData;
class CampaignWindowsInterface;
class CampaignUserInterface;

class TownMenu_Int {
	 TownMenu_Imp* d_townMenu;

  public:
	 TownMenu_Int(CampaignUserInterface* owner, HWND hParent, CampaignWindowsInterface* campWind,
			const CampaignData* campData);
	 ~TownMenu_Int();

	 void run(ITown town, const PixelPoint& p);
	 // void destroy();
};

#endif
