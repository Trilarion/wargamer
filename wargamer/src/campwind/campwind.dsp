# Microsoft Developer Studio Project File - Name="campwind" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Dynamic-Link Library" 0x0102

CFG=campwind - Win32 Editor Debug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "campwind.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "campwind.mak" CFG="campwind - Win32 Editor Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "campwind - Win32 Release" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE "campwind - Win32 Debug" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE "campwind - Win32 Editor Debug" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE "campwind - Win32 Editor Release" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 1
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""
CPP=cl.exe
MTL=midl.exe
RSC=rc.exe

!IF  "$(CFG)" == "campwind - Win32 Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "..\..\exe"
# PROP Intermediate_Dir "o\release"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MT /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "CAMPWIND_EXPORTS" /YX /FD /c
# ADD CPP /nologo /MD /W3 /Gi /GR /GX /Zi /O2 /I "..\mapwind" /I "..\h" /I "..\..\res" /I "..\system" /I "..\gamesup" /I "..\ob" /I "..\campdata" /I "..\gwind" /I "..\campLogic" /D "NDEBUG" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "EXPORT_CAMPWIND_DLL" /YX"stdinc.hpp" /FD /c
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x809 /d "NDEBUG"
# ADD RSC /l 0x809 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /dll /machine:I386
# ADD LINK32 gdi32.lib user32.lib comctl32.lib /nologo /dll /debug /machine:I386

!ELSEIF  "$(CFG)" == "campwind - Win32 Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "..\..\exe"
# PROP Intermediate_Dir "o\debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MTd /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "CAMPWIND_EXPORTS" /YX /FD /GZ /c
# ADD CPP /nologo /MDd /W3 /Gm /Gi /GR /GX /ZI /Od /I "..\mapwind" /I "..\h" /I "..\..\res" /I "..\system" /I "..\gamesup" /I "..\ob" /I "..\campdata" /I "..\gwind" /I "..\campLogic" /D "_DEBUG" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "EXPORT_CAMPWIND_DLL" /YX"stdinc.hpp" /FD /GZ /c
# ADD BASE MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x809 /d "_DEBUG"
# ADD RSC /l 0x809 /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /dll /debug /machine:I386 /pdbtype:sept
# ADD LINK32 gdi32.lib user32.lib comctl32.lib /nologo /dll /debug /machine:I386 /out:"..\..\exe/campwindDB.dll" /pdbtype:sept

!ELSEIF  "$(CFG)" == "campwind - Win32 Editor Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "campwind___Win32_Editor_Debug"
# PROP BASE Intermediate_Dir "campwind___Win32_Editor_Debug"
# PROP BASE Ignore_Export_Lib 0
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "..\..\exe"
# PROP Intermediate_Dir "o\Editor\Debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MDd /W3 /Gm /Gi /GR /GX /ZI /Od /I "..\mapwind" /I "..\h" /I "..\..\res" /I "..\system" /I "..\gamesup" /I "..\ob" /I "..\campdata" /I "..\gwind" /I "..\campLogic" /D "_DEBUG" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "EXPORT_CAMPWIND_DLL" /YX"stdinc.hpp" /FD /GZ /c
# ADD CPP /nologo /MDd /W3 /Gm /Gi /GR /GX /ZI /Od /I "..\mapwind" /I "..\h" /I "..\..\res" /I "..\system" /I "..\gamesup" /I "..\ob" /I "..\campdata" /I "..\gwind" /I "..\campLogic" /I "..\campedit" /D "_USRDLL" /D "EXPORT_CAMPWIND_DLL" /D "_DEBUG" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /D "EDITOR" /YX"stdinc.hpp" /FD /GZ /c
# ADD BASE MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x809 /d "_DEBUG"
# ADD RSC /l 0x809 /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 gdi32.lib user32.lib comctl32.lib /nologo /dll /debug /machine:I386 /out:"..\..\exe/campwindDB.dll" /pdbtype:sept
# ADD LINK32 gdi32.lib user32.lib comctl32.lib /nologo /dll /debug /machine:I386 /out:"..\..\exe/campwindEDDB.dll" /pdbtype:sept

!ELSEIF  "$(CFG)" == "campwind - Win32 Editor Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "campwind___Win32_Editor_Release"
# PROP BASE Intermediate_Dir "campwind___Win32_Editor_Release"
# PROP BASE Ignore_Export_Lib 0
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "..\..\exe"
# PROP Intermediate_Dir "o\Editor\Release"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MD /W3 /Gi /GR /GX /Zd /O2 /I "..\mapwind" /I "..\h" /I "..\..\res" /I "..\system" /I "..\gamesup" /I "..\ob" /I "..\campdata" /I "..\gwind" /I "..\campLogic" /D "NDEBUG" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "EXPORT_CAMPWIND_DLL" /YX"stdinc.hpp" /FD /c
# ADD CPP /nologo /MD /W3 /Gi /GR /GX /Zd /O2 /I "..\mapwind" /I "..\h" /I "..\..\res" /I "..\system" /I "..\gamesup" /I "..\ob" /I "..\campdata" /I "..\gwind" /I "..\campLogic" /I "..\campedit" /D "NDEBUG" /D "_USRDLL" /D "EXPORT_CAMPWIND_DLL" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /D "EDITOR" /YX"stdinc.hpp" /FD /c
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x809 /d "NDEBUG"
# ADD RSC /l 0x809 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 gdi32.lib user32.lib comctl32.lib /nologo /dll /debug /machine:I386
# ADD LINK32 gdi32.lib user32.lib comctl32.lib /nologo /dll /debug /machine:I386 /out:"..\..\exe/campwindED.dll"

!ENDIF 

# Begin Target

# Name "campwind - Win32 Release"
# Name "campwind - Win32 Debug"
# Name "campwind - Win32 Editor Debug"
# Name "campwind - Win32 Editor Release"
# Begin Group "Source Files"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat"
# Begin Source File

SOURCE=.\bld_dial.cpp
# End Source File
# Begin Source File

SOURCE=.\calctrl.cpp
# End Source File
# Begin Source File

SOURCE=.\camptool.cpp
# End Source File
# Begin Source File

SOURCE=.\campwind.cpp
# End Source File
# Begin Source File

SOURCE=.\cbatdial.cpp

!IF  "$(CFG)" == "campwind - Win32 Release"

!ELSEIF  "$(CFG)" == "campwind - Win32 Debug"

!ELSEIF  "$(CFG)" == "campwind - Win32 Editor Debug"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "campwind - Win32 Editor Release"

# PROP Exclude_From_Build 1

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\cbr_wind.cpp

!IF  "$(CFG)" == "campwind - Win32 Release"

!ELSEIF  "$(CFG)" == "campwind - Win32 Debug"

!ELSEIF  "$(CFG)" == "campwind - Win32 Editor Debug"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "campwind - Win32 Editor Release"

# PROP Exclude_From_Build 1

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\cclock.cpp
# End Source File
# Begin Source File

SOURCE=.\ccombo.cpp

!IF  "$(CFG)" == "campwind - Win32 Release"

!ELSEIF  "$(CFG)" == "campwind - Win32 Debug"

!ELSEIF  "$(CFG)" == "campwind - Win32 Editor Debug"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "campwind - Win32 Editor Release"

# PROP Exclude_From_Build 1

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\chckdata.cpp
# End Source File
# Begin Source File

SOURCE=.\combinedinterface.cpp

!IF  "$(CFG)" == "campwind - Win32 Release"

!ELSEIF  "$(CFG)" == "campwind - Win32 Debug"

!ELSEIF  "$(CFG)" == "campwind - Win32 Editor Debug"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "campwind - Win32 Editor Release"

# PROP Exclude_From_Build 1

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\ctimctrl.cpp
# End Source File
# Begin Source File

SOURCE=.\debug.bat
# End Source File
# Begin Source File

SOURCE=.\find.cpp
# End Source File
# Begin Source File

SOURCE=.\ftdial.cpp

!IF  "$(CFG)" == "campwind - Win32 Release"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "campwind - Win32 Debug"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "campwind - Win32 Editor Debug"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "campwind - Win32 Editor Release"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\infobase.cpp
# End Source File
# Begin Source File

SOURCE=.\infowind.cpp

!IF  "$(CFG)" == "campwind - Win32 Release"

!ELSEIF  "$(CFG)" == "campwind - Win32 Debug"

!ELSEIF  "$(CFG)" == "campwind - Win32 Editor Debug"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "campwind - Win32 Editor Release"

# PROP Exclude_From_Build 1

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\mainwind.cpp
# End Source File
# Begin Source File

SOURCE=.\msgwin.cpp

!IF  "$(CFG)" == "campwind - Win32 Release"

!ELSEIF  "$(CFG)" == "campwind - Win32 Debug"

!ELSEIF  "$(CFG)" == "campwind - Win32 Editor Debug"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "campwind - Win32 Editor Release"

# PROP Exclude_From_Build 1

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\nm_wind.cpp

!IF  "$(CFG)" == "campwind - Win32 Release"

!ELSEIF  "$(CFG)" == "campwind - Win32 Debug"

!ELSEIF  "$(CFG)" == "campwind - Win32 Editor Debug"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "campwind - Win32 Editor Release"

# PROP Exclude_From_Build 1

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\obwin.cpp
# End Source File
# Begin Source File

SOURCE=.\optdial.cpp
# End Source File
# Begin Source File

SOURCE=.\optwiz.cpp

!IF  "$(CFG)" == "campwind - Win32 Release"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "campwind - Win32 Debug"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "campwind - Win32 Editor Debug"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "campwind - Win32 Editor Release"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\ordertb.cpp
# End Source File
# Begin Source File

SOURCE=.\org_dial.cpp

!IF  "$(CFG)" == "campwind - Win32 Release"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "campwind - Win32 Debug"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "campwind - Win32 Editor Debug"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "campwind - Win32 Editor Release"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\org_list.cpp

!IF  "$(CFG)" == "campwind - Win32 Release"

!ELSEIF  "$(CFG)" == "campwind - Win32 Debug"

!ELSEIF  "$(CFG)" == "campwind - Win32 Editor Debug"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "campwind - Win32 Editor Release"

# PROP Exclude_From_Build 1

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\org_ordr.cpp

!IF  "$(CFG)" == "campwind - Win32 Release"

!ELSEIF  "$(CFG)" == "campwind - Win32 Debug"

!ELSEIF  "$(CFG)" == "campwind - Win32 Editor Debug"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "campwind - Win32 Editor Release"

# PROP Exclude_From_Build 1

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\reorgldr.cpp

!IF  "$(CFG)" == "campwind - Win32 Release"

!ELSEIF  "$(CFG)" == "campwind - Win32 Debug"

!ELSEIF  "$(CFG)" == "campwind - Win32 Editor Debug"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "campwind - Win32 Editor Release"

# PROP Exclude_From_Build 1

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\reorgob.cpp

!IF  "$(CFG)" == "campwind - Win32 Release"

!ELSEIF  "$(CFG)" == "campwind - Win32 Debug"

!ELSEIF  "$(CFG)" == "campwind - Win32 Editor Debug"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "campwind - Win32 Editor Release"

# PROP Exclude_From_Build 1

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\s_result.cpp
# End Source File
# Begin Source File

SOURCE=.\spwind.cpp

!IF  "$(CFG)" == "campwind - Win32 Release"

!ELSEIF  "$(CFG)" == "campwind - Win32 Debug"

!ELSEIF  "$(CFG)" == "campwind - Win32 Editor Debug"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "campwind - Win32 Editor Release"

# PROP Exclude_From_Build 1

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\statwind.cpp

!IF  "$(CFG)" == "campwind - Win32 Release"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "campwind - Win32 Debug"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "campwind - Win32 Editor Debug"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "campwind - Win32 Editor Release"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\tinfowin.cpp

!IF  "$(CFG)" == "campwind - Win32 Release"

!ELSEIF  "$(CFG)" == "campwind - Win32 Debug"

!ELSEIF  "$(CFG)" == "campwind - Win32 Editor Debug"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "campwind - Win32 Editor Release"

# PROP Exclude_From_Build 1

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\to_win.cpp
# End Source File
# Begin Source File

SOURCE=.\toolbar.cpp
# End Source File
# Begin Source File

SOURCE=.\toolbox.cpp

!IF  "$(CFG)" == "campwind - Win32 Release"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "campwind - Win32 Debug"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "campwind - Win32 Editor Debug"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "campwind - Win32 Editor Release"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\tordrwin.cpp

!IF  "$(CFG)" == "campwind - Win32 Release"

!ELSEIF  "$(CFG)" == "campwind - Win32 Debug"

!ELSEIF  "$(CFG)" == "campwind - Win32 Editor Debug"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "campwind - Win32 Editor Release"

# PROP Exclude_From_Build 1

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\towndial.cpp

!IF  "$(CFG)" == "campwind - Win32 Release"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "campwind - Win32 Debug"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "campwind - Win32 Editor Debug"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "campwind - Win32 Editor Release"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\towninfo.cpp

!IF  "$(CFG)" == "campwind - Win32 Release"

!ELSEIF  "$(CFG)" == "campwind - Win32 Debug"

!ELSEIF  "$(CFG)" == "campwind - Win32 Editor Debug"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "campwind - Win32 Editor Release"

# PROP Exclude_From_Build 1

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\townint.cpp

!IF  "$(CFG)" == "campwind - Win32 Release"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "campwind - Win32 Debug"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "campwind - Win32 Editor Debug"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "campwind - Win32 Editor Release"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\townmenu.cpp

!IF  "$(CFG)" == "campwind - Win32 Release"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "campwind - Win32 Debug"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "campwind - Win32 Editor Debug"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "campwind - Win32 Editor Release"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\uinfowin.cpp

!IF  "$(CFG)" == "campwind - Win32 Release"

!ELSEIF  "$(CFG)" == "campwind - Win32 Debug"

!ELSEIF  "$(CFG)" == "campwind - Win32 Editor Debug"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "campwind - Win32 Editor Release"

# PROP Exclude_From_Build 1

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\unitdial.cpp

!IF  "$(CFG)" == "campwind - Win32 Release"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "campwind - Win32 Debug"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "campwind - Win32 Editor Debug"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "campwind - Win32 Editor Release"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\unitinfo.cpp

!IF  "$(CFG)" == "campwind - Win32 Release"

!ELSEIF  "$(CFG)" == "campwind - Win32 Debug"

!ELSEIF  "$(CFG)" == "campwind - Win32 Editor Debug"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "campwind - Win32 Editor Release"

# PROP Exclude_From_Build 1

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\unitint.cpp

!IF  "$(CFG)" == "campwind - Win32 Release"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "campwind - Win32 Debug"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "campwind - Win32 Editor Debug"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "campwind - Win32 Editor Release"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\unitmenu.cpp

!IF  "$(CFG)" == "campwind - Win32 Release"

!ELSEIF  "$(CFG)" == "campwind - Win32 Debug"

!ELSEIF  "$(CFG)" == "campwind - Win32 Editor Debug"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "campwind - Win32 Editor Release"

# PROP Exclude_From_Build 1

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\uordrwin.cpp

!IF  "$(CFG)" == "campwind - Win32 Release"

!ELSEIF  "$(CFG)" == "campwind - Win32 Debug"

!ELSEIF  "$(CFG)" == "campwind - Win32 Editor Debug"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "campwind - Win32 Editor Release"

# PROP Exclude_From_Build 1

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\userint.cpp
# End Source File
# Begin Source File

SOURCE=.\victwind.cpp

!IF  "$(CFG)" == "campwind - Win32 Release"

!ELSEIF  "$(CFG)" == "campwind - Win32 Debug"

!ELSEIF  "$(CFG)" == "campwind - Win32 Editor Debug"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "campwind - Win32 Editor Release"

# PROP Exclude_From_Build 1

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\weathwin.cpp

!IF  "$(CFG)" == "campwind - Win32 Release"

!ELSEIF  "$(CFG)" == "campwind - Win32 Debug"

!ELSEIF  "$(CFG)" == "campwind - Win32 Editor Debug"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "campwind - Win32 Editor Release"

# PROP Exclude_From_Build 1

!ENDIF 

# End Source File
# End Group
# Begin Group "Header Files"

# PROP Default_Filter "h;hpp;hxx;hm;inl"
# Begin Source File

SOURCE=.\bld_dial.hpp
# End Source File
# Begin Source File

SOURCE=.\calctrl.hpp
# End Source File
# Begin Source File

SOURCE=.\camptool.hpp
# End Source File
# Begin Source File

SOURCE=.\campwind.hpp
# End Source File
# Begin Source File

SOURCE=.\cbatdial.hpp
# End Source File
# Begin Source File

SOURCE=.\cbr_wind.hpp
# End Source File
# Begin Source File

SOURCE=.\cclock.hpp
# End Source File
# Begin Source File

SOURCE=.\ccombo.hpp
# End Source File
# Begin Source File

SOURCE=.\chckdata.hpp
# End Source File
# Begin Source File

SOURCE=.\combinedinterface.hpp
# End Source File
# Begin Source File

SOURCE=.\ctimctrl.hpp
# End Source File
# Begin Source File

SOURCE=.\cwdll.h
# End Source File
# Begin Source File

SOURCE=.\find.hpp
# End Source File
# Begin Source File

SOURCE=.\ftdial.hpp
# End Source File
# Begin Source File

SOURCE=.\infobase.hpp
# End Source File
# Begin Source File

SOURCE=.\infowind.hpp
# End Source File
# Begin Source File

SOURCE=.\mainwind.hpp
# End Source File
# Begin Source File

SOURCE=.\msgwin.hpp
# End Source File
# Begin Source File

SOURCE=.\nm_wind.hpp
# End Source File
# Begin Source File

SOURCE=.\obwin.hpp
# End Source File
# Begin Source File

SOURCE=.\optdial.hpp
# End Source File
# Begin Source File

SOURCE=.\optwiz.hpp
# End Source File
# Begin Source File

SOURCE=.\ordertb.hpp
# End Source File
# Begin Source File

SOURCE=.\org_dial.hpp
# End Source File
# Begin Source File

SOURCE=.\org_list.hpp
# End Source File
# Begin Source File

SOURCE=.\org_ordr.hpp
# End Source File
# Begin Source File

SOURCE=.\reorgldr.hpp
# End Source File
# Begin Source File

SOURCE=.\reorgob.hpp
# End Source File
# Begin Source File

SOURCE=.\s_result.hpp
# End Source File
# Begin Source File

SOURCE=.\spwind.hpp
# End Source File
# Begin Source File

SOURCE=.\statwind.hpp
# End Source File
# Begin Source File

SOURCE=.\stdinc.hpp
# End Source File
# Begin Source File

SOURCE=.\tinfowin.hpp
# End Source File
# Begin Source File

SOURCE=.\to_win.hpp
# End Source File
# Begin Source File

SOURCE=.\toolbar.hpp
# End Source File
# Begin Source File

SOURCE=.\toolbox.hpp
# End Source File
# Begin Source File

SOURCE=.\tordrwin.hpp
# End Source File
# Begin Source File

SOURCE=.\towndial.hpp
# End Source File
# Begin Source File

SOURCE=.\towninfo.hpp
# End Source File
# Begin Source File

SOURCE=.\townint.hpp
# End Source File
# Begin Source File

SOURCE=.\townmenu.hpp
# End Source File
# Begin Source File

SOURCE=.\uinfowin.hpp
# End Source File
# Begin Source File

SOURCE=.\unitdial.hpp
# End Source File
# Begin Source File

SOURCE=.\unitinfo.hpp
# End Source File
# Begin Source File

SOURCE=.\unitint.hpp
# End Source File
# Begin Source File

SOURCE=.\unitmenu.hpp
# End Source File
# Begin Source File

SOURCE=.\uordrwin.hpp
# End Source File
# Begin Source File

SOURCE=.\userint.hpp
# End Source File
# Begin Source File

SOURCE=.\victwind.hpp
# End Source File
# Begin Source File

SOURCE=.\weathwin.hpp
# End Source File
# End Group
# Begin Group "Resource Files"

# PROP Default_Filter "ico;cur;bmp;dlg;rc2;rct;bin;rgs;gif;jpg;jpeg;jpe"
# End Group
# End Target
# End Project
