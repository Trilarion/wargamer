/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 * Reorganize Units dialogue
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "org_dial.hpp"
#include "org_list.hpp"
#include "org_ordr.hpp"
#include "wind.hpp"
#include "palwind.hpp"
#include "except.hpp"
#include "myassert.hpp"
#include "unitlist.hpp"
#include "wmisc.hpp"
#include "scenario.hpp"
#include "winctrl.hpp"
#include "palette.hpp"
#include "campdint.hpp"
#include "armies.hpp"
#include "scn_res.h"
#include "dib.hpp"
#include "autoptr.hpp"
#include "resstr.hpp"

#ifdef DEBUG
#include "resdef.h"

#define DEBUG_ORGDIAL

#ifdef DEBUG_ORGDIAL
#include "msgenum.hpp"
#include "clog.hpp"
LogFileFlush orgLog("orgDial.log");
#endif
#endif

// #define SUBCLASSTREEVIEW
#if !defined(SUBCLASSTREEVIEW)

typedef TreeView OrgTreeView;
// typedef ListBox OrgListBox;

#endif

#if defined(SUBCLASSTREEVIEW)

/*
 * My subclassed TreeView class
 */

class OrgTreeView : public SubClassWindowBase, public TreeView
{
   public:
      OrgTreeView() { }
      ~OrgTreeView() { }

      void create(int id, HWND hParent, DWORD exStyle, DWORD style, const RECT& r);

   private:
      LRESULT procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);

      // Messages to process

      BOOL onEraseBk(HWND hwnd, HDC hdc);
#if 0
      void onMouseMove(HWND hwnd, int x, int y, UINT keyFlags);
      void onLButtonDown(HWND hwnd, BOOL fDoubleClick, int x, int y, UINT keyFlags);
      void onRButtonDown(HWND hwnd, BOOL fDoubleClick, int x, int y, UINT keyFlags);
      void onLButtonUp(HWND hwnd, int x, int y, UINT keyFlags);
      void onRButtonUp(HWND hwnd, int x, int y, UINT keyFlags);
#endif
};

void OrgTreeView::create(int id, HWND hParent, DWORD exStyle, DWORD style, const RECT& r)
{
   TreeView::create(id, hParent, exStyle, style, r);
   SubClassWindowBase::init(getHWND());
}

LRESULT OrgTreeView::procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
   switch(msg)
   {
      // Insert HANDLE_* macros here

      HANDLE_MSG(hWnd, WM_ERASEBKGND,     onEraseBk);    // Part of CustomBkWindow
      // HANDLE_MSG(hWnd, WM_MOUSEMOVE, onMouseMove);
      // HANDLE_MSG(hWnd, WM_LBUTTONDOWN, onLButtonDown);
      // HANDLE_MSG(hWnd, WM_RBUTTONDOWN, onRButtonDown);
      // HANDLE_MSG(hWnd, WM_LBUTTONUP, onLButtonUp);
      // HANDLE_MSG(hWnd, WM_RBUTTONUP, onRButtonUp);

   default:
      return defProc(hWnd, msg, wParam, lParam);
   }
}


#if 0
void OrgTreeView::onMouseMove(HWND hwnd, int x, int y, UINT keyFlags)
{
   FORWARD_WM_MOUSEMOVE(hwnd, x, y, keyFlags, defProc);
}

void OrgTreeView::onLButtonDown(HWND hwnd, BOOL fDoubleClick, int x, int y, UINT keyFlags)
{
   FORWARD_WM_LBUTTONDOWN(hwnd, fDoubleClick, x, y, keyFlags, defProc);
}

void OrgTreeView::onRButtonDown(HWND hwnd, BOOL fDoubleClick, int x, int y, UINT keyFlags)
{
   FORWARD_WM_RBUTTONDOWN(hwnd, fDoubleClick, x, y, keyFlags, defProc);
}

void OrgTreeView::onLButtonUp(HWND hwnd, int x, int y, UINT keyFlags)
{
   FORWARD_WM_LBUTTONUP(hwnd, x, y, keyFlags, defProc);
}

void OrgTreeView::onRButtonUp(HWND hwnd, int x, int y, UINT keyFlags)
{
   FORWARD_WM_RBUTTONUP(hwnd, x, y, keyFlags, defProc);
}
#endif

BOOL OrgTreeView::onEraseBk(HWND hwnd, HDC hdc)
{
   // SetBkColor(hdc, PALETTERGB(128,255,192));
   // return FORWARD_WM_ERASEBKGND(hwnd, hdc, defProc);
   return TRUE;
}

#endif   // defined(SUBCLASTREEVIEW)

#if 0
/*
 * My Subclassed Listbox class
 */

class OrgListBox : public SubClassWindowBase, public ListBox
{
   public:
      OrgListBox() { }
      ~OrgListBox() { }

      void create(int id, HWND hParent, DWORD exStyle, DWORD style, const RECT& r);

   private:
      LRESULT procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);

      // Messages to process

      void onMouseMove(HWND hwnd, int x, int y, UINT keyFlags);
      void onLButtonDown(HWND hwnd, BOOL fDoubleClick, int x, int y, UINT keyFlags);
      void onRButtonDown(HWND hwnd, BOOL fDoubleClick, int x, int y, UINT keyFlags);
      void onLButtonUp(HWND hwnd, int x, int y, UINT keyFlags);
      void onRButtonUp(HWND hwnd, int x, int y, UINT keyFlags);
};

void OrgListBox::create(int id, HWND hParent, DWORD exStyle, DWORD style, const RECT& r)
{
   ListBox::create(id, hParent, exStyle, style, r);
   SubClassWindowBase::init(getHWND());
}

LRESULT OrgListBox::procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
   switch(msg)
   {
      // Insert HANDLE_* macros here

      HANDLE_MSG(hWnd, WM_MOUSEMOVE, onMouseMove);
      HANDLE_MSG(hWnd, WM_LBUTTONDOWN, onLButtonDown);
      HANDLE_MSG(hWnd, WM_RBUTTONDOWN, onRButtonDown);
      HANDLE_MSG(hWnd, WM_LBUTTONUP, onLButtonUp);
      HANDLE_MSG(hWnd, WM_RBUTTONUP, onRButtonUp);
   default:
      return defProc(hWnd, msg, wParam, lParam);
   }
}

void OrgListBox::onMouseMove(HWND hwnd, int x, int y, UINT keyFlags)
{
   FORWARD_WM_MOUSEMOVE(hwnd, x, y, keyFlags, defProc);
}

void OrgListBox::onLButtonDown(HWND hwnd, BOOL fDoubleClick, int x, int y, UINT keyFlags)
{
   FORWARD_WM_LBUTTONDOWN(hwnd, fDoubleClick, x, y, keyFlags, defProc);
}

void OrgListBox::onRButtonDown(HWND hwnd, BOOL fDoubleClick, int x, int y, UINT keyFlags)
{
   FORWARD_WM_RBUTTONDOWN(hwnd, fDoubleClick, x, y, keyFlags, defProc);
}

void OrgListBox::onLButtonUp(HWND hwnd, int x, int y, UINT keyFlags)
{
   FORWARD_WM_LBUTTONUP(hwnd, x, y, keyFlags, defProc);
}

void OrgListBox::onRButtonUp(HWND hwnd, int x, int y, UINT keyFlags)
{
   FORWARD_WM_RBUTTONUP(hwnd, x, y, keyFlags, defProc);
}

#endif   // End of subclass stuff commented out


/*
 * Implementation class
 */

class OrgDialogImp : public CustomBkWindow {
   HWND                 d_hParent;     // Handle of Parent
   const CampaignData*  d_campData;
   StackedUnitList      d_unitList;    // List of stacked Units

   OrgList              d_orgList;     // List of units under control of dialog

   Button               d_okButton;
   Button               d_cancelButton;
   OrgTreeView          d_treeView;
   // OrgListBox           d_listBox;
   OrgTreeView          d_contentBox;
   StaticWindow         d_infoWindow;
   std::auto_ptr<DrawDIBDC>  d_infoDib;

   ImageList            d_imgList;     // images used in Tree view

   Boolean              d_pickedUp;          // We picked up by right mouse click
   enum {
      DoMerge,
      DoTransfer
   } d_mergeMode;

   // Structure for referencing an item in a tree
   class TreeItem {
         OrgCPI         d_iOrg;
         OrgTreeView*   d_tree;
         HTREEITEM      d_treeItem;

      public:
         TreeItem() :
            d_iOrg(OrgCPI::None),
            d_tree(0),
            d_treeItem(NULL)
         {
         }

         TreeItem(OrgCPI iOrg, OrgTreeView* tree, HTREEITEM item) :
            d_iOrg(iOrg),
            d_tree(tree),
            d_treeItem(item)
         {
         }

         void clear()
         {
            d_iOrg = OrgCPI::None;
            d_tree = 0;
            d_treeItem = NULL;
         }

         void set(OrgCPI iOrg, OrgTreeView* tree, HTREEITEM item)
         {
            d_iOrg = iOrg;
            d_tree = tree;
            d_treeItem = item;
         }

         OrgCPI org() const { return d_iOrg; }
         OrgTreeView* tree() const { return d_tree; }
         HTREEITEM item() const { return d_treeItem; }

         Boolean isEmpty() const { return d_iOrg == OrgCPI::None; }
         int operator ! () const { return isEmpty(); }
         operator int () const { return !isEmpty(); }
   };

   TreeItem             d_dragItem;
   TreeItem             d_dropItem;

   // OrgCPI               d_dragItem;          // Object being dragged
   // HTREEITEM            d_dragTreeItem;      // Handle of dragged object in TreeView
   // HTREEITEM            d_dragContentItem;         // Handle of object being dragged from contents pane

   // HWND                 d_dropWind;          // Window with Drop Highlighted
   // HTREEITEM            d_dropItem;          // Item being dropped

   OrgCPI               d_currentObject;     // Object to display info about
   OrgCPI               d_lastObject;        // Object currently being displayed
   OrgCPI               d_contentObject;     // What is being displayed in contents window?

   enum Controls {
      ID_OK,            // OK Button
      ID_Cancel,        // Cancel Button
      ID_TreeView,      // Tree View
      ID_ListBox,       // List box
      ID_Info,          // Info about selected item

      ID_HowMany
   };

   enum WhichButton {
      Left,
      Right
   };

   Boolean d_resetting;

 public:
    OrgDialogImp(HWND hParent, const CampaignData* campData, const RECT& r); //const StackedUnitList& ul);
//  OrgDialogImp(HWND hParent, const CampaignData* campData, const StackedUnitList& ul, const RECT& r);
    ~OrgDialogImp();

    void initUnits();
    void initUnits(const StackedUnitList& ul)
    {
      d_unitList = ul;
      initUnits();
    } // structure copy

 private:
   virtual LRESULT procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
      // Window Message Procedure

   BOOL onCreate(HWND hWnd, CREATESTRUCT* lpCreateStruct);
      // Window is being created
   void onDestroy(HWND hWnd);
      // Window is being destroyed
   void onGetMinMaxInfo(HWND hWnd, LPMINMAXINFO lpMinMaxInfo);
      // Return Minimum Window size
   void onSize(HWND hwnd, UINT state, int cx, int cy);
      // Window is resized
   LRESULT onNotify(HWND hWnd, int id, NMHDR* lpNMHDR);
      // Message from TreeView or ListBox
   void onCommand(HWND hWnd, int id, HWND hwndCtl, UINT codeNotify);
      // Button has been pressed
   void onNCPaint(HWND hwnd, HRGN hrgn);
      // Draw Border
   // void onMeasureItem(HWND hwnd, MEASUREITEMSTRUCT * lpMeasureItem);
      // Return how big a control's items are
   void onDrawItem(HWND hwnd, const DRAWITEMSTRUCT * lpDrawItem);
      // Draw a control's item
   void onMouseMove(HWND hwnd, int x, int y, UINT keyFlags);
      // Mouse is moved over window (or while captured)
   void onLButtonUp(HWND hwnd, int x, int y, UINT keyFlags);
      // Left Button released over window (or while captured)
   void onRButtonUp(HWND hwnd, int x, int y, UINT keyFlags);
      // Right Button released over window (or while captured)
   void onContextMenu(HWND hwnd, HWND hwndCtl, int x, int y);
      // Right button clicked in a child control


   void onTVSelect(OrgCPI iOrg);
      // Object is selected in TreeView
   void onTVBeginDrag(NM_TREEVIEW* tv, WhichButton button, HWND tvHwnd);
      // TreeView starts dragging
   void onTVEndDrag(HWND hwnd, int x, int y);
      // Stop dragging
      // Do whatever is neccessary
   Boolean doDragOver(int x, int y);
      // Set up dragging variables for mouse coordinates
   HTREEITEM checkMouseOver(OrgTreeView* tree, int x, int y);
      // Find if given coordinates are over an item in treeview
      // return NULL if not over anything
   void pickup(OrgTreeView* tree, HTREEITEM hItem, OrgCPI iOrg, int x, int y);
      // Pickup object

   void drawInfo(HDC hdc);
      // Draw Information Window
   void updateInfo(OrgCPI iOrg);
      // Ask for information to be redrawn

   void doTransfer();
      // Transfer dragItem to dropItem
   void transfer(OrgCPI iFrom, OrgCPI iDest);
      // Tramsfer from to dest


   void createOrgList();
         // Convert StackedUnitList into OrgList
   void addOrganization(ICommandPosition cpi, OrgCPI iParent, TV_INSERTSTRUCT& tvi, HTREEITEM tParent);
         // Add a CommandPosition to the OrgList

   OrgTreeView* hwndToTree(HWND hwndCtl);
      // Get the treeview for a corresponding window handle
      // return 0 if HWND is not a treeview

#ifdef DEBUG
   virtual void checkPalette() { }
#endif
};

/*-------------------------------------------------------------------------
 * Insulated class
 */

ReorganizeDialog::ReorganizeDialog(HWND hParent, const CampaignData* campData, const RECT&r)
{
   d_orgDial = new OrgDialogImp(hParent, campData, r);

   if(!d_orgDial)
      throw OrgDialError();
}

ReorganizeDialog::~ReorganizeDialog()
{
   delete d_orgDial;
}

HWND ReorganizeDialog::getHWND() const
{
   ASSERT(d_orgDial);
   return d_orgDial->getHWND();
}

void ReorganizeDialog::initUnits(const StackedUnitList& ul)
{
  ASSERT(d_orgDial);
  d_orgDial->initUnits(ul);
}
/*========================================================================
 * Utility Functions
 */

BOOL ClientToWindow(HWND hWnd, LPPOINT lpPoint)
{
   BOOL result = ClientToScreen(hWnd, lpPoint);
   ASSERT(result);
   if(result)
   {
      RECT wRect;
      result = GetWindowRect(hWnd, &wRect);
      ASSERT(result);
      if(result)
      {
         lpPoint->x -= wRect.left;
         lpPoint->y -= wRect.top;
      }
   }
   return result;
}

/*========================================================================
 * Implementation functions
 */

OrgDialogImp::OrgDialogImp(HWND hParent, const CampaignData* campData, const RECT& r) :
   CustomBkWindow(scenario->getBorderColors()),
   d_hParent(hParent),
   d_campData(campData),
// d_unitList(ul),         // This is a copy
   d_orgList(campData), //ul.getUnit(0)),
   d_okButton(),
   d_cancelButton(),
   d_treeView(),
   // d_listBox(),
   d_contentBox(),
   d_infoWindow(),
   d_infoDib(0),
   d_imgList(),
   d_pickedUp(False),
   d_mergeMode(DoTransfer),
   d_dragItem(),
   d_dropItem(),
   // d_dragItem(OrgCPI::None),
   // d_dragTreeItem(NULL),
   // d_dragContentItem(NULL),
   // d_dropWind(NULL),
   // d_dropItem(NULL),
   d_currentObject(OrgCPI::None),
   d_lastObject(OrgCPI::None),
   d_contentObject(OrgCPI::None)
{
#if defined(DEBUG_ORGDIAL)
   orgLog.printf(">Constructor");
#endif

   /*
    * Set up background for custom
    */

   bkDib = scenario->getSideBkDIB(SIDE_Neutral);


   registerClass(wndInstance(d_hParent));

   HWND hwnd = createWindow(
      WS_EX_TOOLWINDOW,
      className(),
      NULL,
//    WS_POPUP |
//       WS_BORDER |
//       WS_CAPTION |
//       WS_SIZEBOX |
//       WS_SYSMENU |
         WS_CHILD |
         WS_CLIPSIBLINGS,
//    120,90,400,300,
      r.left, r.top, r.right-r.left, r.bottom-r.top,
      d_hParent,
      NULL,
      wndInstance(d_hParent)
   );

   ASSERT(hwnd != NULL);

   SetWindowText(hwnd, InGameText::get(IDS_ReorganizeOB));

// createOrgList();

   d_okButton.show(TRUE);
   d_cancelButton.show(TRUE);
   d_treeView.show(TRUE);
   // d_listBox.show(TRUE);
   d_contentBox.show(TRUE);
   d_infoWindow.show(TRUE);

   ShowWindow(hwnd, SW_SHOW);

#if defined(DEBUG_ORGDIAL)
   orgLog.printf("<Constructor");
#endif
}

OrgDialogImp::~OrgDialogImp()
{
#if defined(DEBUG_ORGDIAL)
   orgLog.printf(">Destructor");
#endif

   // delete d_infoDIB;    // Automatically deleted because auto_ptr

#if defined(DEBUG_ORGDIAL)
   orgLog.printf("<Destructor");
#endif
}

/*
 * Little Utility Functions
 * Defined here, so compiler may decide to inline them
 */

OrgTreeView* OrgDialogImp::hwndToTree(HWND hwndCtl)
{
   OrgTreeView* tree;

   if(hwndCtl == d_treeView.getHWND())
      tree = &d_treeView;
   else if(hwndCtl == d_contentBox.getHWND())
      tree = &d_contentBox;
   else
      tree = 0;

   return tree;
}

void OrgDialogImp::initUnits()
{
  if(d_unitList.unitCount() > 0)
  {
    d_resetting = True;
    createOrgList();
    d_treeView.show(TRUE);
    d_resetting = False;
  }
}
/*------------------------------------------------------------------------
 * Window Procedure
 */

virtual LRESULT OrgDialogImp::procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
#if defined(DEBUG_ORGDIAL)
   orgLog.printf("procMessage(%s)",
      getWMdescription(hWnd, msg, wParam, lParam));
#endif


   LRESULT result;
   if(handlePalette(hWnd, msg, wParam, lParam, result))
      return result;

   switch(msg)
   {
      HANDLE_MSG(hWnd, WM_ERASEBKGND,     onEraseBk);    // Part of CustomBkWindow
      HANDLE_MSG(hWnd, WM_NCPAINT,        onNCPaint);
      HANDLE_MSG(hWnd, WM_CREATE,         onCreate);
      HANDLE_MSG(hWnd, WM_DESTROY,        onDestroy);
      HANDLE_MSG(hWnd, WM_NOTIFY,         onNotify);
      HANDLE_MSG(hWnd, WM_GETMINMAXINFO,  onGetMinMaxInfo);
      HANDLE_MSG(hWnd, WM_SIZE,           onSize);
      HANDLE_MSG(hWnd, WM_COMMAND,        onCommand);
      // HANDLE_MSG(hWnd, WM_MEASUREITEM,    onMeasureItem);
      HANDLE_MSG(hWnd, WM_DRAWITEM,       onDrawItem);

      HANDLE_MSG(hWnd, WM_MOUSEMOVE, onMouseMove);
      // HANDLE_MSG(hWnd, WM_LBUTTONDOWN, onLButtonDown);
      // HANDLE_MSG(hWnd, WM_RBUTTONDOWN, onRButtonDown);
      HANDLE_MSG(hWnd, WM_LBUTTONUP, onLButtonUp);
      HANDLE_MSG(hWnd, WM_RBUTTONUP, onRButtonUp);
      HANDLE_MSG(hWnd, WM_CONTEXTMENU, onContextMenu);
   default:
      return defProc(hWnd, msg, wParam, lParam);
   }
}


/*------------------------------------------------------------------------
 * Specific Window procedures
 */

BOOL OrgDialogImp::onCreate(HWND hWnd, CREATESTRUCT* lpCreateStruct)
{
#if defined(DEBUG_ORGDIAL)
   orgLog.printf(">onCreate");
#endif


   d_imgList.loadBitmap(
      scenario->getScenarioResDLL(),
      MAKEINTRESOURCE(BM_OBICONS),
      OBI_CX);
   // d_imgList.addBitmap(scenario->getScenarioResDLL(), MAKEINTRESOURCE(BM_BUILD_TYPE));

   /*
    * Create child windows
    */

   RECT r;

   SetRectEmpty(&r);

   d_treeView.create(ID_TreeView,
                     hWnd,
                     WS_EX_CLIENTEDGE,
                     WS_CHILD
                     // | TVS_SHOWSELALWAYS
                     | TVS_HASLINES
                     | TVS_HASBUTTONS
                     | TVS_LINESATROOT,
                     r);
   d_treeView.setImageList(d_imgList);

   d_contentBox.create(ID_ListBox,
                     hWnd,
                     WS_EX_CLIENTEDGE,
                     WS_CHILD ,
                     // | TVS_SHOWSELALWAYS
                     // | TVS_HASLINES
                     // | TVS_HASBUTTONS
                     // | TVS_LINESATROOT,
                     r);
   d_contentBox.setImageList(d_imgList);

   d_infoWindow.create(ID_Info,
                     hWnd,
                     WS_EX_CLIENTEDGE,
                     WS_CHILD
                     | SS_OWNERDRAW,
                     r);

   d_okButton.create(ID_OK,
                     InGameText::get(IDS_OK),
                     hWnd,
                     0,
                     WS_CHILD
                     | BS_PUSHBUTTON,
                     r);

   d_cancelButton.create(ID_Cancel,
                     InGameText::get(IDS_Cancel),
                     hWnd,
                     0,
                     WS_CHILD
                     | BS_PUSHBUTTON,
                     r);

#if defined(DEBUG_ORGDIAL)
   orgLog.printf("<onCreate");
#endif

   return TRUE;
}

void OrgDialogImp::onDestroy(HWND hWnd)
{
#if defined(DEBUG_ORGDIAL)
   orgLog.printf(">onDestroy");
#endif
   // SendMessage(d_hParent, WM_PARENTNOTIFY, (WPARAM)MAKELONG(WM_DESTROY, GetWindowLong(hwnd, GWL_ID)), (LPARAM)hwnd);
   // FORWARD_WM_PARENTNOTIFY(d_hParent, WM_DESTROY, hWnd, GetWindowLong(hWnd, GWL_ID), SendMessage);

#ifdef DEBUG
   FORWARD_WM_PARENTNOTIFY(d_hParent, WM_DESTROY, hWnd, DLG_UNITORDER_ATTACHSP, SendMessage);
#else
   FORWARD_WM_PARENTNOTIFY(d_hParent, WM_DESTROY, hWnd, 0, SendMessage);
#endif

#if defined(DEBUG_ORGDIAL)
   orgLog.printf("<onDestroy");
#endif
}

void OrgDialogImp::onGetMinMaxInfo(HWND hWnd, LPMINMAXINFO lpMinMaxInfo)
{
   lpMinMaxInfo->ptMinTrackSize.x = 200;
   lpMinMaxInfo->ptMinTrackSize.y = 100;
}

void OrgDialogImp::onSize(HWND hwnd, UINT state, int cx, int cy)
{
#if defined(DEBUG_ORGDIAL)
   orgLog.printf(">onSize(%d,%d,%d)", state, cx, cy);
#endif

   /*
    * Reposition elements within window
    */

   if( (state == SIZE_RESTORED) || (state == SIZE_MAXIMIZED) )
   {
      const int wButton = 64;
      const int hButton = 20;
      const int hBorder = 4;
      const int hInfo = 40;      // Height of Info window
      const int tvPercent = 65;  // treeview to use 65% of width, contents 35%

      /*
       * Buttons at bottom
       */

      RECT r;

      r.left = (cx - (wButton * 2)) / 3;
      r.right = r.left + wButton;
      r.top = cy - hButton - hBorder;
      r.bottom = r.top + hButton;
      d_okButton.move(r);

      r.right = cx - r.left;
      r.left = r.right - wButton;
      d_cancelButton.move(r);

      /*
       * Info Window above buttons
       */

      r.left = 0;
      r.right = cx;
      r.bottom = r.top - hBorder;
      r.top = r.bottom - hInfo;
      d_infoWindow.move(r);

      /*
       * TreeViews at top
       */

      r.left = 0;
      r.right = (cx * tvPercent) / 100 - 1;
      r.bottom = r.top; // cy - (hButton + 2 * hBorder) - hInfo;
      r.top = 0;
      d_treeView.move(r);

      r.left = r.right + 1;
      r.right = cx;
      // d_listBox.move(r);
      d_contentBox.move(r);


   }

#if defined(DEBUG_ORGDIAL)
   orgLog.printf("<onSize");
#endif
}

LRESULT OrgDialogImp::onNotify(HWND hWnd, int id, NMHDR* lpNMHDR)
{
#if defined(DEBUG_ORGDIAL)
   orgLog.printf(">onNotify(%d, { %p,%d,%s} )",
         static_cast<int>           (id),
         static_cast<void*>         (lpNMHDR->hwndFrom),
         static_cast<int>           (lpNMHDR->idFrom),
         static_cast<const char*>   (getNotifyName(lpNMHDR->code))
      );
#endif

   switch(lpNMHDR->idFrom)
   {
   case ID_TreeView:
#if defined(DEBUG_ORGDIAL)
      orgLog.printf("From TreeView");
      goto ProcTV;
#endif
   case ID_ListBox:
#if defined(DEBUG_ORGDIAL)
      orgLog.printf("From ListBox");
   ProcTV:
#endif

      switch(lpNMHDR->code)
      {
      case TVN_GETDISPINFO:
         {
            TV_DISPINFO* tvdi = (TV_DISPINFO*) lpNMHDR;

            OrgCPI iOrg (tvdi->item.lParam);
            // const OrgObject& ob = d_orgList[iOrg];

#if defined(DEBUG_ORGDIAL)
            orgLog.printf("GetDispInfo(%d), mask=%d", static_cast<int>(iOrg), static_cast<int>(tvdi->item.mask));
#endif

            if(tvdi->item.mask & TVIF_TEXT)
            {
               tvdi->item.pszText = (char*) d_orgList.getName(iOrg);
#if defined(DEBUG_ORGDIAL)
               orgLog.printf("GetName(%d)=%s", static_cast<int>(iOrg), static_cast<const char*>(tvdi->item.pszText));
#endif
            }

            if(tvdi->item.mask & TVIF_IMAGE)
            {
               tvdi->item.iImage = d_orgList.getImage(iOrg);
#if defined(DEBUG_ORGDIAL)
               orgLog.printf("GetImage(%d)=%d", static_cast<int>(iOrg), static_cast<int>(tvdi->item.iImage));
#endif
            }

            if(tvdi->item.mask & TVIF_SELECTEDIMAGE)
            {
               tvdi->item.iSelectedImage = d_orgList.getSelectedImage(iOrg);
#if defined(DEBUG_ORGDIAL)
               orgLog.printf("GetSelectedImage(%d)=%d", static_cast<int>(iOrg), static_cast<int>(tvdi->item.iSelectedImage));
#endif
            }

            if(tvdi->item.mask & TVIF_CHILDREN)
            {
               tvdi->item.cChildren = d_orgList.hasCPChild(iOrg) ? 1: 0;
               // (ob.child() == OrgCPI::None) ? 0 : 1;
#if defined(DEBUG_ORGDIAL)
               orgLog.printf("GetChildren(%d)=%d", static_cast<int>(iOrg), static_cast<int>(tvdi->item.cChildren));
#endif
            }
         }
         break;

      case TVN_SELCHANGED:
         {
#if defined(DEBUG_ORGDIAL)
            orgLog.printf("TreeView selection Changed");
#endif
            NM_TREEVIEW* tv = (NM_TREEVIEW*) lpNMHDR;
            OrgCPI iOrg = tv->itemNew.lParam;

            if(lpNMHDR->idFrom == ID_TreeView)
            {
               onTVSelect(iOrg);
            }

            updateInfo(iOrg);
         }
         break;

      case NM_DBLCLK:
#if defined(DEBUG_ORGDIAL)
         orgLog.printf("TreeView Double Click");
#endif
         break;

      case TVN_BEGINDRAG:
         onTVBeginDrag((NM_TREEVIEW*) lpNMHDR, Left, lpNMHDR->hwndFrom);
         break;
      case TVN_BEGINRDRAG:
         onTVBeginDrag((NM_TREEVIEW*) lpNMHDR, Right, lpNMHDR->hwndFrom);
         break;

      case TVN_DELETEITEM:
      {
         if(!d_resetting)
         {
           NM_TREEVIEW* tv = (NM_TREEVIEW*) lpNMHDR;
           OrgCPI iOrg = tv->itemOld.lParam;
           ASSERT(iOrg != OrgCPI::None);
           OrgObject* ob = &d_orgList[iOrg];
           if(lpNMHDR->idFrom == ID_TreeView)
             ob->treeIndex(NULL);
           else if(lpNMHDR->idFrom == ID_ListBox)
             ob->contentIndex(NULL);
           else
             FORCEASSERT("ID is not a treeview");
         }
      }

#if defined(DEBUG_ORGDIAL)
      default:
         orgLog.printf("TreeView: Unknown code %d", static_cast<int>(lpNMHDR->code));
#endif
      }
      break;         // End of ID_TREEVIEW


#if defined(DEBUG_ORGDIAL)
   default:
      orgLog.printf("From Unknown control");
#endif
   }

#if defined(DEBUG_ORGDIAL)
   orgLog.printf("<onNotify");
#endif
   return 0;
}

void OrgDialogImp::onCommand(HWND hWnd, int id, HWND hwndCtl, UINT codeNotify)
{
#if defined(DEBUG_ORGDIAL)
   orgLog.printf(">onCommand(%d)", static_cast<int>(id));
#endif

   switch(id)
   {
#if 0
   case ID_ListBox:
      {
#if defined(DEBUG_ORGDIAL)
         orgLog.printf("From ListBox");
#endif
         switch(codeNotify)
         {
         case LBN_DBLCLK:
#if defined(DEBUG_ORGDIAL)
            orgLog.printf("Double Click");
#endif
            break;
         case LBN_SELCHANGE:
#if defined(DEBUG_ORGDIAL)
            orgLog.printf("Selection Changed");
#endif
            break;
         case LBN_KILLFOCUS:
            {
#if defined(DEBUG_ORGDIAL)
               orgLog.printf("Kill Focus");
#endif
            }
            break;
         case LBN_SETFOCUS:
#if defined(DEBUG_ORGDIAL)
            orgLog.printf("Set Focus");
#endif
            break;
         }

      }
      break;
#endif

   case ID_OK:
      if(codeNotify == BN_CLICKED)
      {
         OrgOrders::sendOrders(d_orgList, d_unitList.getUnit(0));
//       goto closeDown;
      }
      break;

   case ID_Cancel:
      if(codeNotify == BN_CLICKED)
      {
        initUnits();
//    closeDown:
//       FORWARD_WM_CLOSE(hWnd, SendMessage);
      }
      break;

#if defined(DEBUG_ORGDIAL)
   default:
         orgLog.printf("From Unknown");
#endif
   }


#if defined(DEBUG_ORGDIAL)
   orgLog.printf("<onCommand");
#endif
}

void OrgDialogImp::onNCPaint(HWND hwnd, HRGN hrgn)
{
#if defined(DEBUG_ORGDIAL)
   orgLog.printf(">onNCPaint()");
#endif

   FORWARD_WM_NCPAINT(hwnd, hrgn, defProc);

   HDC hdc = GetWindowDC(hwnd);
   RECT r;
   GetWindowRect(hwnd, &r);
   OffsetRect(&r, -r.left, -r.top);

   HPALETTE oldPal = SelectPalette(hdc, Palette::get(), FALSE);
   RealizePalette(hdc);

      drawThickBorder(hdc, r);

   SelectPalette(hdc, oldPal, FALSE);

   ReleaseDC(hwnd, hdc);

#if defined(DEBUG_ORGDIAL)
   orgLog.printf("<onNCPaint()");
#endif
}

#if 0
void OrgDialogImp::onMeasureItem(HWND hwnd, MEASUREITEMSTRUCT * lpMeasureItem)
{
#if defined(DEBUG_ORGDIAL)
   orgLog.printf(">onMeasureItem(%d,%d,%d)",
      (int) lpMeasureItem->CtlType,
      (int) lpMeasureItem->CtlID,
      (int) lpMeasureItem->itemID);
#endif

   switch(lpMeasureItem->CtlID)
   {
   case ID_ListBox:
      {
         // Set width.
         // this should probably use a maximum(imageHeight, fontHeight)

         int newHeight = d_imgList.getImgHeight() + 2;
         if(newHeight > lpMeasureItem->itemHeight)
            lpMeasureItem->itemHeight = d_imgList.getImgHeight();
      }
      break;

   default:
#if defined(DEBUG_ORGDIAL)
      orgLog.printf("onMeasureItem: Unknown ID");
#endif
      FORWARD_WM_MEASUREITEM(hwnd, lpMeasureItem, defProc);
      break;

   }

#if defined(DEBUG_ORGDIAL)
   orgLog.printf("<onMeasureItem()");
#endif
}
#endif   // .. was used for ListBox

void OrgDialogImp::onDrawItem(HWND hwnd, const DRAWITEMSTRUCT * lpDrawItem)
{
#if defined(DEBUG_ORGDIAL)
   orgLog.printf(">onDrawItem(%d,%d,%d,%d,%d)",
      (int) lpDrawItem->CtlType,
      (int) lpDrawItem->CtlID,
      (int) lpDrawItem->itemID,
      (int) lpDrawItem->itemAction,
      (int) lpDrawItem->itemState);
#endif

   switch(lpDrawItem->CtlID)
   {
#if 0
   case ID_ListBox:
      {
         // Draw something...

         OrgCPI iOrg = (int) lpDrawItem->itemData;
         const OrgObject& ob = d_orgList[iOrg];

#if 0
         // Draw a filled rectangle so we can see what we've got

         HPEN hPen = CreatePen(PS_SOLID, 0, PALETTERGB(255,128,0));
         HBRUSH hBrush = CreateSolidBrush(PALETTERGB(0,255,255));

         HPEN oldPen = (HPEN) SelectObject(lpDrawItem->hDC, hPen);
         HBRUSH oldBrush = (HBRUSH) SelectObject(lpDrawItem->hDC, hBrush);

         Rectangle(lpDrawItem->hDC,
            lpDrawItem->rcItem.left, lpDrawItem->rcItem.top,
            lpDrawItem->rcItem.right, lpDrawItem->rcItem.bottom);

         SelectObject(lpDrawItem->hDC, oldBrush);
         SelectObject(lpDrawItem->hDC, oldPen);

         DeleteObject(hBrush);
         DeleteObject(hPen);
#endif

         COLORREF oldBkColor = GetBkColor(lpDrawItem->hDC);

         if(lpDrawItem->itemState & ODS_SELECTED)
         {
            SetBkColor(lpDrawItem->hDC, scenario->getSideColour(ob.getSide(d_campData)));
         }

         /*
          * Draw Text in default font and colour
          */

         int textX = lpDrawItem->rcItem.left + d_imgList.getImgWidth() + 2;

         TEXTMETRIC tm;
         GetTextMetrics(lpDrawItem->hDC, &tm);
         int textY = (lpDrawItem->rcItem.top + lpDrawItem->rcItem.bottom - tm.tmHeight) / 2;

         const char* text = ob.getName(d_campData, 0);
         size_t sLen = lstrlen(text);

         RECT textRect;
         CopyRect(&textRect, &lpDrawItem->rcItem);
         textRect.left = textX;

         int oldAlign = SetTextAlign(lpDrawItem->hDC, TA_LEFT | TA_TOP);
         BOOL result = ExtTextOut(lpDrawItem->hDC, textX, textY,
               ETO_CLIPPED | ETO_OPAQUE, &textRect,
               text, sLen, NULL);

         ASSERT(result);


         /*
          * draw Icon
          */

         int imgY = (lpDrawItem->rcItem.top + lpDrawItem->rcItem.bottom - d_imgList.getImgHeight()) / 2;

         int image;
         if(lpDrawItem->itemState & ODS_SELECTED)
            image = ob.getSelectedImage(d_campData);
         else
            image = ob.getImage(d_campData);

         d_imgList.draw(lpDrawItem->hDC, image,
            lpDrawItem->rcItem.left, imgY);

         /*
          * Draw Focus
          */

         if(lpDrawItem->itemState & ODS_FOCUS)
         {
            DrawFocusRect(lpDrawItem->hDC, &lpDrawItem->rcItem);
         }


         SetTextAlign(lpDrawItem->hDC, oldAlign);
         SetBkColor(lpDrawItem->hDC, oldBkColor);


      }
      break;
#endif

   case ID_Info:
      drawInfo(lpDrawItem->hDC);
      break;

   default:
#if defined(DEBUG_ORGDIAL)
      orgLog.printf("onDrawItem: Unknown ID");
#endif
      FORWARD_WM_DRAWITEM(hwnd, lpDrawItem, defProc);
      break;

   }

#if defined(DEBUG_ORGDIAL)
   orgLog.printf("<onDrawItem()");
#endif
}

/*
 * Mouse Moved over Window or while captured
 *
 * If an item is being dragged, then we deal with the ImageList
 * Highlight objects underneath cursor, etc...
 */

void OrgDialogImp::onMouseMove(HWND hwnd, int x, int y, UINT keyFlags)
{
   if(d_dragItem)    // .s_iOrg != OrgCPI::None)
   {
      /*
       * Stupid ImageList_DragMove needs window coordinates
       * but we are given client coordinates
       */

      POINT screenPt;
      screenPt.x = x;
      screenPt.y = y;
      ClientToScreen(hwnd, &screenPt);
      RECT wRect;
      GetWindowRect(getHWND(), &wRect);

      ImageList_DragMove(screenPt.x - wRect.left, screenPt.y - wRect.top);

      // Check for over TreeView and Content box

      doDragOver(screenPt.x, screenPt.y);
   }
   else
      FORWARD_WM_MOUSEMOVE(hwnd, x, y, keyFlags, defProc);
}

/*
 * Left Button Released while over window or while captured
 *
 * If we are dragging, then we stop dragging and do whatever action is required
 */

void OrgDialogImp::onLButtonUp(HWND hwnd, int x, int y, UINT keyFlags)
{
   // if(!d_copyDrag && d_dragItem) // .s_iOrg != OrgCPI::None))
   if(d_dragItem)
   {
      onTVEndDrag(hwnd, x, y);
   }
   else
      FORWARD_WM_LBUTTONUP(hwnd, x, y, keyFlags, defProc);
}

/*
 * Right Button Released while over window or while captured
 *
 * If we are dragging, then we stop dragging and do whatever action is required
 */

void OrgDialogImp::onRButtonUp(HWND hwnd, int x, int y, UINT keyFlags)
{
   // if(!d_copyDrag && d_dragItem) // .s_iOrg != OrgCPI::None))
   if(d_dragItem)
   {
      onTVEndDrag(hwnd, x, y);
   }
   else
      FORWARD_WM_RBUTTONUP(hwnd, x, y, keyFlags, defProc);
}

/*
 * Right button pressed in a control
 */

void OrgDialogImp::onContextMenu(HWND hwnd, HWND hwndCtl, int x, int y)
{
   if( (hwndCtl == d_treeView.getHWND()) ||
       (hwndCtl == d_contentBox.getHWND()) )
   {
#if defined(DEBUG_ORGDIAL)
      orgLog.printf("TreeView/contentBox::onContextMenu(%d,%d)", x, y);
#endif

      /*
       * What object are we over?
       *
       * Coordinates are SCREEN coordinates
       */

      TV_HITTESTINFO info;
      info.pt.x = x;
      info.pt.y = y;
      ScreenToClient(hwndCtl, &info.pt);
      HTREEITEM hit = TreeView_HitTest(hwndCtl, &info);

#if defined(DEBUG_ORGDIAL)
      orgLog.printf("Hit=%p", (void*) hit);
#endif

      if(hit != NULL)
      {
         TV_ITEM tv;
         tv.hItem = hit;
         tv.mask = TVIF_PARAM;
         TreeView_GetItem(hwndCtl, &tv);
         OrgCPI iOrg = tv.lParam;

         ASSERT(iOrg != OrgCPI::None);

#if defined(DEBUG_ORGDIAL)
         orgLog.printf("iOrg=%d", static_cast<int>(iOrg));
#endif

         /*
          * Create a popup menu containing:
          *    new Wing (If clicked on empty space)
          *    new Army (empty or wing)
          *    new Corps (empty or army or wing)
          *    new Division (empty or army or wing or corps)
          *    pickup (if not empty)
          */

         enum {
            IDM_ORG_None,
            IDM_ORG_Transfer,
            IDM_ORG_Merge,
            IDM_ORG_Detach,

            IDM_ORG_HowMany
         };

         HMENU hMenu = CreatePopupMenu();

         // Set up default values for menu items

         MENUITEMINFO mInfo;
         mInfo.cbSize = sizeof(mInfo);
         mInfo.fMask = MIIM_ID | MIIM_STATE | MIIM_TYPE;
         mInfo.fType = MFT_STRING;
         mInfo.fState = MFS_ENABLED;
         mInfo.wID = 0;
         mInfo.hSubMenu = NULL;
         mInfo.hbmpChecked = NULL;
         mInfo.hbmpUnchecked = NULL;
         mInfo.dwItemData = 0;
         mInfo.dwTypeData = InGameText::get(IDS_AnItem);
         mInfo.cch = 0;

         /*
          * Transfer for any object
          */

         mInfo.wID = IDM_ORG_Transfer;
         mInfo.dwTypeData = InGameText::get(IDS_Transfer);
         BOOL result = InsertMenuItem(hMenu, UINT(-1), TRUE, &mInfo);
         ASSERT(result);

         /*
          * Merge if object is a division with strength points
          */

         if(d_orgList.canMerge(iOrg))
         {
            mInfo.wID = IDM_ORG_Merge;
            mInfo.dwTypeData = InGameText::get(IDS_Merge);
            result = InsertMenuItem(hMenu, UINT(-1), TRUE, &mInfo);
            ASSERT(result);
         }

         /*
          * Detach if object is a CP or Leader that is not already detached
          */

         if(d_orgList.canDetach(iOrg))
         {
            mInfo.wID = IDM_ORG_Detach;
            mInfo.dwTypeData = InGameText::get(IDS_Detach);
            result = InsertMenuItem(hMenu, UINT(-1), TRUE, &mInfo);
            ASSERT(result);
         }


         int command = TrackPopupMenuEx(hMenu,
            TPM_LEFTALIGN | TPM_RIGHTBUTTON | TPM_RETURNCMD | TPM_NONOTIFY,
            x, y, hwnd, NULL);


#if defined(DEBUG_ORGDIAL)
         orgLog.printf("TrackPopupMenu returned %d", (int) command);
#endif

         DestroyMenu(hMenu);

         switch(command)
         {
            case IDM_ORG_Transfer:
               // Set mode to transfer and pickup
               d_mergeMode = DoTransfer;
               goto doPickup;

            case IDM_ORG_Merge:
               // Set mode to merge and pickup
               d_mergeMode = DoMerge;
            doPickup:
               {
                  /*
                   * Convert to main dialog window's coordinates
                   */

                  POINT p;
                  p.x = x;
                  p.y = y;
                  ScreenToClient(getHWND(), &p);
                  d_pickedUp = True;
                  pickup(hwndToTree(hwndCtl), hit, iOrg, p.x, p.y);
               }
               break;

            case IDM_ORG_Detach:
               // Immediately detach unit
               transfer(iOrg, OrgCPI::None);
               break;
         }
      }
   }
   else
   {
#if defined(DEBUG_ORGDIAL)
      orgLog.printf("Unknown::onContextMenu");
#endif
   }
}

#if 0
/*
 * A Top Down Unit Iterator... move this to Armies when finished?
 *
 * The format is somewhat different than the other iterators
 * instead of: UnitIter(campData, cpi); while(++iter) { f(iter.current); }
 * we are using:
 * for(Iter i(army, cpi); iter; ++iter) { f(*iter); }
 */

class UnitIterTopDown
{
      const Armies& d_army;
      ICommandPosition d_top;
      ICommandPosition d_current;
   public:
      UnitIterTopDown(const Armies& army, ICommandPosition top) :
         d_army(army),
         d_top(top),
         d_current(top)
      {
      }

      ICommandPosition operator * () const { return d_current; }
      const CommandPosition* operator * () const { return d_army.getCommand(d_current); }

      Boolean operator ++ ();
      Boolean operator ()() { return d_current != NoCommandPosition; }
};

Boolean UnitITerTopDown::operator ++ ()
{
   ASSERT(d_current != NoCommandPosition);

   if(d_current != NoCommandPosition)
   {
      const CommandPosition* cp = d_army.getCommand(d_current);
      ICommandPosition cpi = cp->getChild();
      if(cpi != NoCommandPosition)
      {
         d_current = cpi;
         return True;
      }
      else
      {
         while(d_current != d_top)
         {
            const CommandPosition* cp = d_army.getCommand(d_current);
            cpi = cp->getSister();
            if(cpi != NoCommandPosition)
            {
               d_current = cpi;
               return True;
            }
            else
               d_current = cp->getParent();
         }
      }
   }

   return False;
}

#endif

/*
 * Create the OrgList from stacked Units
 * and add to TreeView
 */

void OrgDialogImp::createOrgList()
{
#if defined(DEBUG_ORGDIAL)
   orgLog.printf(">createOrgList()");
#endif

   d_treeView.reset();
   d_orgList.initUnits(d_unitList.getUnit(0));

   /*
    * Set up default tvi values
    */

   TV_INSERTSTRUCT tvi;
   tvi.hParent = TVI_ROOT;
   tvi.hInsertAfter = TVI_LAST;
   tvi.item.mask = TVIF_CHILDREN | TVIF_IMAGE | TVIF_PARAM | TVIF_SELECTEDIMAGE | TVIF_STATE | TVIF_TEXT;
   tvi.item.hItem = NULL;
   tvi.item.state = TVIS_EXPANDED | TVIS_SELECTED;
   tvi.item.stateMask = TVIS_EXPANDED | TVIS_BOLD | TVIS_SELECTED;
   tvi.item.pszText = LPSTR_TEXTCALLBACK;
   tvi.item.cchTextMax = 0;
   tvi.item.iImage = I_IMAGECALLBACK;
   tvi.item.iSelectedImage = I_IMAGECALLBACK;
   tvi.item.cChildren = I_CHILDRENCALLBACK;
   tvi.item.lParam = OrgCPI::None;

   for(UnitListID i = 0; i < d_unitList.unitCount(); i++)
   {
      ICommandPosition top = d_unitList.getUnit(i);

      addOrganization(top, OrgCPI::None, tvi, TVI_ROOT);
   }

#if defined(DEBUG_ORGDIAL)
   orgLog.printf("<createOrgList()");
#endif
}

/*
 * Recursively Add units
 */

void OrgDialogImp::addOrganization(ICommandPosition cpi, OrgCPI iParent, TV_INSERTSTRUCT& tvi, HTREEITEM tParent)
{
#if defined(DEBUG_ORGDIAL)
   orgLog.printf(">addOrganization(%s)", (const char*)d_campData->getUnitName(cpi).toStr());
#endif

   OrgCPI iOrg = d_orgList.addCP(cpi, iParent);

#if defined(DEBUG_ORGDIAL)
   orgLog.printf("addOrganization: iOrg=%d", static_cast<int>(iOrg));
#endif

   tvi.hParent = tParent;
   tvi.item.lParam = static_cast<LPARAM> (iOrg);
   tParent = d_treeView.addItem(tvi);
   d_orgList[iOrg].treeIndex(tParent);

   if(tvi.item.state & TVIS_SELECTED)
   {
      onTVSelect(iOrg);
   }

   tvi.item.state &= ~(TVIS_EXPANDED | TVIS_SELECTED);      // Only 1st object to be expanded

#if defined(DEBUG_ORGDIAL)
   orgLog.printf("addOrganization: treeItem=%d", (int) tParent);
#endif

   const CommandPosition* cp = d_campData->getCommand(cpi);

   /*
    * Add Leader
    */

   ILeader iLeader = cp->getLeader();
   if(iLeader != NoLeader)
   {
      OrgCPI iOrgLeader = d_orgList.addLeader(iLeader, iOrg);

      if(cp->getChild() != NoCommandPosition)
      {
         tvi.item.lParam = static_cast<LPARAM>(iOrgLeader);
         tvi.hParent = tParent;
         HTREEITEM item = d_treeView.addItem(tvi);
         d_orgList[iOrgLeader].treeIndex(item);
      }
   }

   /*
    * Add children
    */

   ICommandPosition cpiChild = cp->getChild();
   while(cpiChild != NoCommandPosition)
   {
      addOrganization(cpiChild, iOrg, tvi, tParent);

      const CommandPosition* cpChild = d_campData->getCommand(cpiChild);
      cpiChild = cpChild->getSister();
   }

   /*
    * Add Strength Points
    */

   ISP isp = cp->getSPEntry();
   while(isp != NoStrengthPoint)
   {
      OrgCPI orgSP = d_orgList.addSP(isp, iOrg);

      StrengthPointItem* sp = d_campData->getArmies().getStrengthPoint(isp);
      isp = sp->getNext();
   }


#if defined(DEBUG_ORGDIAL)
   orgLog.printf("<addOrganization");
#endif
}


/*
 * Item is selected in TreeView
 * It should clear out ListBox and replace items with childen of iOrg
 */

void OrgDialogImp::onTVSelect(OrgCPI iOrg)
{
   const OrgObject& ob = d_orgList[iOrg];

   updateInfo(iOrg);
   d_contentObject = iOrg;

   /*
    * Stop Window from being redrawn while items are added
    */

   SetWindowRedraw(d_contentBox.getHWND(), FALSE);

   /*
    * Clear ListBox
    */

   // d_listBox.reset();
   d_contentBox.reset();

   TV_INSERTSTRUCT tvi;
   tvi.hParent = TVI_ROOT;
   tvi.hInsertAfter = TVI_LAST;
   tvi.item.mask = TVIF_CHILDREN | TVIF_IMAGE | TVIF_PARAM | TVIF_SELECTEDIMAGE | TVIF_STATE | TVIF_TEXT;
   tvi.item.hItem = NULL;
   tvi.item.state = 0;
   tvi.item.stateMask = TVIS_EXPANDED | TVIS_BOLD | TVIS_SELECTED;
   tvi.item.pszText = LPSTR_TEXTCALLBACK;
   tvi.item.cchTextMax = 0;
   tvi.item.iImage = I_IMAGECALLBACK;
   tvi.item.iSelectedImage = I_IMAGECALLBACK;
   tvi.item.cChildren = 0;
   tvi.item.lParam = OrgCPI::None;

   /*
    * Add Children to listbox
    */

   OrgCPI iChild = ob.child();
   while(iChild != OrgCPI::None)
   {
      OrgObject& obChild = d_orgList[iChild];

      // d_listBox.add(obChild.getName(d_campData, 0), iChild);

      tvi.item.lParam = static_cast<LPARAM>(iChild);
      HTREEITEM item = d_contentBox.addItem(tvi);
      obChild.contentIndex(item);

      iChild = obChild.sister();
   }

   // Get Window redrawn

   SetWindowRedraw(d_contentBox.getHWND(), TRUE);
   InvalidateRect(d_contentBox.getHWND(), NULL, TRUE);
}

/*
 * TreeView starts dragging
 *
 * x,y are client coordinates of OrgDialogImp
 */

void OrgDialogImp::pickup(OrgTreeView* tree, HTREEITEM hItem, OrgCPI iOrg, int x, int y)
{
   ASSERT(!d_dragItem); // .s_iOrg == OrgCPI::None);
   ASSERT(iOrg != OrgCPI::None);
   ASSERT(tree != 0);

   HWND tvHwnd = tree->getHWND();

   // const OrgObject& ob = d_orgList[iOrg];

#if defined(DEBUG_ORGDIAL)
   orgLog.printf("pickup: %s", d_orgList.getName(iOrg));
#endif

   /*
    * Get an Image
    */

   HIMAGELIST himl = TreeView_CreateDragImage(tvHwnd, hItem);
   ASSERT(himl != NULL);

   // Fiddle with coordinates to get a hotspot
   // Not quite sure what the given coordinates really are
   // We might need to offset by the position of the treeview within the window

   RECT rcItem;
   TreeView_GetItemRect(tvHwnd, hItem, &rcItem, TRUE);
   // int indent = TreeView_GetIndent(tvHwnd);

   int hotX = x - rcItem.left + d_imgList.getImgWidth();
   int hotY = y - rcItem.top;

   ImageList_BeginDrag(himl, 0, hotX, hotY);

   // Might want to consider using ImageList_SetDragCursor() to add a
   // pointer.

   // ImageList_SetDragCursorImage(cursImg, 0, 0, 0);

   // ImageList_BeginDrag(d_imgList.getHandle(), ob.getImage(d_campData), tv->ptDrag.x - rcItem.left, tv->ptDrag.y - rcItem.top);
   // ShowCursor(FALSE);
   SetCapture(getHWND());

   // DragEnter uses coordinates relative to Window instead of Client
   // Made more confusing because SDK isn't clear on just what the
   // coordinates are: NM_TREEVIEW says client (of window receiving message or of treeview?)
   // TVN_BEGINDRAG says Screen coordinates!


   POINT screenPt;
   screenPt.x = x;
   screenPt.y = y;
   ClientToWindow(getHWND(), &screenPt);
   // ClientToScreen(getHWND(), &screenPt);
   // RECT wRect;
   // GetWindowRect(getHWND(), &wRect);
   // screenPt.x -= wRect.left;
   // screenPt.y -= wRect.top;

   ImageList_DragEnter(getHWND(), screenPt.x, screenPt.y);

   /*
    * Set up drag variables
    */

   d_dragItem.set(iOrg, tree, hItem);
   // d_dragTreeItem = hItem;
   // ASSERT(d_dragContentItem == NULL);
}

void OrgDialogImp::onTVBeginDrag(NM_TREEVIEW* tv, WhichButton button, HWND tvHwnd)
{
#if defined(DEBUG_ORGDIAL)
   orgLog.printf(">onTVBeginDrag(%d, %s)", tv->itemNew.hItem, (button == Left) ? "Left" : "Right");
#endif

   OrgCPI iOrg = tv->itemNew.lParam;

   OrgTreeView* tree = hwndToTree(tvHwnd);
   ASSERT(tree != 0);

   Boolean canDrag = True;

   if(button == Right)
   {
      canDrag = d_orgList.canMerge(iOrg);
      d_mergeMode = DoMerge;
   }
   else
      d_mergeMode = DoTransfer;

   d_pickedUp = False;

   if(canDrag)
      pickup(tree, tv->itemNew.hItem, iOrg, tv->ptDrag.x, tv->ptDrag.y);


#if defined(DEBUG_ORGDIAL)
   orgLog.printf("<onTVBeginDrag");
#endif
}


void OrgDialogImp::onTVEndDrag(HWND hwnd, int x, int y)
{
#if defined(DEBUG_ORGDIAL)
   orgLog.printf(">onTVEndDrag(%d, %d)", x, y);
#endif

   ASSERT(d_dragItem != OrgCPI::None);

   d_pickedUp = False;

   POINT screenPt;
   screenPt.x = x;
   screenPt.y = y;
   ClientToScreen(hwnd, &screenPt);
   doDragOver(screenPt.x, screenPt.y);

   if(d_dropItem)
   {
      doTransfer();
   }

   ImageList_EndDrag();
   ImageList_DragLeave(getHWND());
   ReleaseCapture();
   // ShowCursor(TRUE);

   d_dragItem.clear();
   // d_dragItem = OrgCPI::None;
   // d_dragTreeItem = NULL;

   if(d_dropItem && d_dropItem.tree())
      TreeView_SelectDropTarget(d_dropItem.tree()->getHWND(), NULL);

   // d_dropWind = NULL;
   // d_dropItem = NULL;
   d_dropItem.clear();

   updateInfo(OrgCPI::None);

#if defined(DEBUG_ORGDIAL)
   orgLog.printf("<onTVEndDrag");
#endif
}

/*
 * x,y are SCREEN coordinates
 */

Boolean OrgDialogImp::doDragOver(int x, int y)
{
   Boolean result = False;

   OrgTreeView* treeOver = &d_treeView;
   HTREEITEM itemOver = checkMouseOver(treeOver, x, y);
   if(itemOver == NULL)
   {
      treeOver = &d_contentBox;
      itemOver = checkMouseOver(treeOver, x, y);
      if(itemOver == NULL)
         treeOver = NULL;
   }

   OrgCPI iOrg = OrgCPI::None;

   /*
    * get Information about new item
    */

   if(itemOver != NULL)
   {
      TV_ITEM tvItem;
      tvItem.hItem = itemOver;
      tvItem.mask = TVIF_PARAM;
      TreeView_GetItem(treeOver->getHWND(), &tvItem);
      iOrg = tvItem.lParam;
   }

   /*
    * Check if it is a valid place to drop
    */

   if(!d_orgList.canTransfer(d_dragItem.org(), iOrg))
   {
      itemOver = NULL;
      treeOver = 0;
      iOrg = OrgCPI::None;
   }

   /*
    * If item has changed
    */

   if( (itemOver != d_dropItem.item()) ||
       (treeOver != d_dropItem.tree()) )
   {
      ImageList_DragShowNolock(FALSE);    // Turn cursor off while updating

      /*
       * If old object is in another tree, then remove drop target
       */

      if(d_dropItem.tree() && (d_dropItem.tree() != treeOver))
      {
         TreeView_SelectDropTarget(d_dropItem.tree()->getHWND(), NULL);
      }

      d_dropItem.set(iOrg, treeOver, itemOver);
      updateInfo(iOrg);

      if(treeOver)
         TreeView_SelectDropTarget(treeOver->getHWND(), itemOver);

      ImageList_DragShowNolock(TRUE);  // re-show cursor

      result = True;
   }

   return result;
}

/*
 * x,y are SCREEN coordinates
 */

HTREEITEM OrgDialogImp::checkMouseOver(OrgTreeView* tree, int x, int y)
{
   /*
    * Convert x,y into client coordinates relative to treeview window
    */

   TV_HITTESTINFO info;
   info.pt.x = x;
   info.pt.y = y;
   ScreenToClient(tree->getHWND(), &info.pt);

   return TreeView_HitTest(tree->getHWND(), &info);
}


/*
 * Draw Information Window
 */

void OrgDialogImp::drawInfo(HDC hdc)
{
#if defined(DEBUG_ORGDIAL)
   orgLog.printf(">drawInfo");
#endif

   RECT wr;
   GetClientRect(d_infoWindow.getHWND(), &wr);

   Boolean redraw;

   if(d_currentObject != d_lastObject)
   {
#if defined(DEBUG_ORGDIAL)
      orgLog.printf("drawInfo: New Object");
#endif

      redraw = True;
      d_lastObject = d_currentObject;
   }
   else
      redraw = False;


   // Make sure we have a suitably sized DIB

   if( !d_infoDib.get() ||
       (d_infoDib->getWidth() != wr.right) ||
       (d_infoDib->getHeight() != wr.bottom) )
   {
#if defined(DEBUG_ORGDIAL)
      orgLog.printf("drawInfo: Creating DIB");
#endif

      // delete d_infoDib;    // Automatically deleted because auto_ptr
      d_infoDib = auto_ptr<DrawDIBDC>(new DrawDIBDC(wr.right, wr.bottom));
      redraw = True;
   }

   // If selected object has changed, or DIB was resized

   if(redraw)
   {
#if defined(DEBUG_ORGDIAL)
      orgLog.printf("drawInfo: Drawing Info");
#endif

      Side side = 0;

      // Fill in background

      const DrawDIB* bkDib = scenario->getSideBkDIB(side);
      d_infoDib->fill(bkDib);

      // Draw relevant Information

      wTextPrintf(d_infoDib->getDC(), 2, 2, "%s: %d",
         InGameText::get(IDS_InfoAbout),
         static_cast<int>(d_currentObject));

      if(d_currentObject != OrgCPI::None)
      {
         // const OrgObject& ob = d_orgList[d_currentObject];

         d_imgList.draw(d_infoDib->getDC(), d_orgList.getImage(d_currentObject), 2, 20);

         wTextPrintf(d_infoDib->getDC(), 2 + d_imgList.getImgWidth() + 2, 20, "%s", d_orgList.getName(d_currentObject));
      }
   }

   /*
    * Copy DIB to screen
    */

   BitBlt(hdc, 0, 0, d_infoDib->getWidth(), d_infoDib->getHeight(), d_infoDib->getDC(), 0, 0, SRCCOPY);

#if defined(DEBUG_ORGDIAL)
   orgLog.printf("<drawInfo");
#endif
}

void OrgDialogImp::updateInfo(OrgCPI iOrg)
{
   d_currentObject = iOrg;    // Update Infowindow

   InvalidateRect(d_infoWindow.getHWND(), NULL, TRUE);
}



/*
 * Transfer Dragged Object to Dropped Object
 *
 * This could get messy, sinc eas well as updating the orgList
 * we must also update the TreeViews.
 * An object can be in more than 1 tree views at a time.
 *
 * Possible methods are:
 *    1) Keep a HTREEITEM for each TreeView within OrgObject
 *    2) Refresh each Tree by stepping through visible objects
 *
 * Method 1 sounds easiest, but requires more memory.
 */


void OrgDialogImp::doTransfer()
{
   ASSERT(d_dropItem);
   ASSERT(d_dragItem);

   transfer(d_dragItem.org(), d_dropItem.org());
}

void OrgDialogImp::transfer(OrgCPI iFrom, OrgCPI iDest)
{
   OrgObject& from = d_orgList[iFrom];
   OrgObject& dest = d_orgList[iDest];

#if defined(DEBUG_ORGDIAL)
   orgLog.printf("Transfer %s to %s",
      d_orgList.getName(iFrom),
      d_orgList.getName(iDest));
#endif

   if(from.parent() == iDest)
   {
#if defined(DEBUG_ORGDIAL)
      orgLog.printf("Destination is already object's parent");
#endif
      return;
   }

   d_orgList.transfer(iFrom, iDest);

   /*
    * Update the TreeViews
    */

   /*
    * Remove from existing location
    */

   HTREEITEM oldItem = from.treeIndex();
   HTREEITEM oldContent = from.contentIndex();

   if(oldItem != NULL)
   {
      d_treeView.deleteItem(oldItem);
      from.treeIndex(NULL);
      InvalidateRect(d_treeView.getHWND(), NULL, TRUE);
   }

   if(oldContent != NULL)
   {
      d_contentBox.deleteItem(oldContent);
      from.contentIndex(NULL);
      InvalidateRect(d_contentBox.getHWND(), NULL, TRUE);
   }

   /*
    * If new parent is the current object being displayed in contents
    * then add it
    * (Note that this means it has been transferred to the same place!)
    * We can probably ignore this case by ignoring drop if parent remains
    * the same!
    */

   if(iDest == d_contentObject)
   {
      TV_INSERTSTRUCT tvi;
      tvi.hParent = TVI_ROOT;
      tvi.hInsertAfter = TVI_LAST;
      tvi.item.mask = TVIF_CHILDREN | TVIF_IMAGE | TVIF_PARAM | TVIF_SELECTEDIMAGE | TVIF_STATE | TVIF_TEXT;
      tvi.item.hItem = NULL;
      tvi.item.state = 0;
      tvi.item.stateMask = TVIS_EXPANDED | TVIS_BOLD | TVIS_SELECTED;
      tvi.item.pszText = LPSTR_TEXTCALLBACK;
      tvi.item.cchTextMax = 0;
      tvi.item.iImage = I_IMAGECALLBACK;
      tvi.item.iSelectedImage = I_IMAGECALLBACK;
      tvi.item.cChildren = I_CHILDRENCALLBACK;
      tvi.item.lParam = static_cast<LPARAM>(iFrom);
      HTREEITEM newItem = d_contentBox.addItem(tvi);
      from.contentIndex(newItem);
      InvalidateRect(d_contentBox.getHWND(), NULL, TRUE);
   }

   /*
    * Add to Main TreeView
    */

   if( (dest.treeIndex() != NULL) || (iDest == OrgCPI::None) )
   {
      TV_INSERTSTRUCT tvi;
      tvi.hParent = dest.treeIndex();
      tvi.hInsertAfter = TVI_LAST;
      tvi.item.mask = TVIF_CHILDREN | TVIF_IMAGE | TVIF_PARAM | TVIF_SELECTEDIMAGE | TVIF_STATE | TVIF_TEXT;
      tvi.item.hItem = NULL;
      tvi.item.state = 0;
      tvi.item.stateMask = TVIS_EXPANDED | TVIS_BOLD | TVIS_SELECTED;
      tvi.item.pszText = LPSTR_TEXTCALLBACK;
      tvi.item.cchTextMax = 0;
      tvi.item.iImage = I_IMAGECALLBACK;
      tvi.item.iSelectedImage = I_IMAGECALLBACK;
      tvi.item.cChildren = I_CHILDRENCALLBACK;
      tvi.item.lParam = static_cast<LPARAM>(iFrom);
      HTREEITEM newItem = d_treeView.addItem(tvi);
      from.treeIndex(newItem);

      /*
       * Make sure it is visible
       */

      d_treeView.showItem(newItem);
   }

}


/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.3  2001/07/02 15:23:17  greenius
 * Battle, Campaign, Combined game and Campaign Editor now compile and run... mostly
 *
 * Revision 1.2  2001/06/18 08:25:45  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
