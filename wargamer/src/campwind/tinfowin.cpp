/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
#include "stdinc.hpp"
#include "tinfowin.hpp"
#include "tabwin.hpp"
#include "towninfo.hpp"
#include "scenario.hpp"
#include "scn_img.hpp"
#include "fonts.hpp"
#include "scrnbase.hpp"
#include "res_str.h"
#include "resstr.hpp"

/*----------------------------------------------------------------------
 * Town info window
 */

class TownInfoWindow : public TabbedInfoWindow {
   CampaignUserInterface* d_owner;

    enum Tabs {
       SummaryTab,
       BuildTab,
       StatusTab,
       TownTab,
       ProvinceTab,

       Tab_HowMany
    };

    static const int s_ids[Tab_HowMany]; // string id's
    static const ResourceStrings s_strings;

    // static const char* s_tabText[Tab_HowMany];

    ITown d_town;
    // TownAttributes d_townAttribs;
    const CampaignData* d_campData;

  public:
    TownInfoWindow(CampaignUserInterface* owner, HWND hParent, const CampaignData* campData,
       const DrawDIB* infoFillDib, const DrawDIB* bkDib,
       const DrawDIB* bFillDib, const ImageLibrary* il, HFONT font,
       CustomBorderInfo bc) :
       TabbedInfoWindow(hParent, Tab_HowMany, s_strings, infoFillDib, bkDib, bFillDib, il, font, bc),
       d_owner(owner),
       d_town(NoTown),
       d_campData(campData) {}
    ~TownInfoWindow() {}

    void run(ITown town, const PixelPoint& p);
    void onSelChange();
    void onRefresh();
    int getRefreshRate() const { return 1000; }
};

#if 0
// temp. need to convert to resource strings
const char* TownInfoWindow::s_tabText[Tab_HowMany] = {
   "Summary",
   "Builds",
   "Status",
   "Town",
   "Province"
};
#endif

const int TownInfoWindow::s_ids[Tab_HowMany] = {
   IDS_Summary,
   IDS_Build,
   IDS_Status,
   IDS_Town,
   IDS_Province
};

const ResourceStrings TownInfoWindow::s_strings(s_ids, Tab_HowMany);

void TownInfoWindow::onSelChange()
{
  Tabs whichTab = static_cast<Tabs>(currentTab());
  ASSERT(whichTab < Tab_HowMany);

  fillStatic();

  if(d_town != NoTown)
  {
#if 0
    TownInfoData td;
    td.d_dib = dib();
    td.d_campData = d_campData;
    td.d_dRect = dRect();
    td.d_ta = &d_townAttribs;

    switch(whichTab)
    {
      case SummaryTab:
        TownInfoDib::drawSummaryDib(td);
        break;

      case BuildTab:
        break;

      case StatusTab:
        TownInfoDib::drawStatusDib(td);
        break;

      case TownTab:
        TownInfoDib::drawTownDib(td);
        break;

      case ProvinceTab:
        TownInfoDib::drawProvinceDib(td);
        break;

    }
#else
      TownInfoData td(d_town, d_campData, dib(), dRect());

      switch(whichTab)
      {
         case SummaryTab:
            td.drawSummaryDib();
            break;

         case BuildTab:
            td.drawBuildDib();
            break;

         case StatusTab:
            td.drawStatusDib();
            break;

         case TownTab:
            td.drawTownDib();
            break;

         case ProvinceTab:
            td.drawProvinceDib();
            break;

      }

#endif
  }
}

void TownInfoWindow::run(ITown town, const PixelPoint& p)
{
  d_town = town;
  // d_townAttribs.makeAttributes(d_town, d_campData);
  onSelChange();
  TabbedInfoWindow::run(p);
}

void TownInfoWindow::onRefresh()
{
   onSelChange();    // This redraws the display
}

/*---------------------------------------------------------------------
 * Outside access
 */

TownInfo_Int::TownInfo_Int(CampaignUserInterface* owner, HWND hParent, const CampaignData* campData) :
  d_infoWind(0) //new TownInfoWindow(owner, hParent, campData))
{
  LogFont lf;
  lf.height((6 * ScreenBase::dbY()) / ScreenBase::baseY());
  lf.weight(FW_MEDIUM);
  lf.face(scenario->fontName(Font_Decorative));

  Font font;
  font.set(lf);

  d_infoWind = new TownInfoWindow(owner, hParent, campData,
    scenario->getScenarioBkDIB(Scenario::BackPatterns::PaperBackground),
    scenario->getScenarioBkDIB(Scenario::BackPatterns::CherryWoodBackground),
    scenario->getScenarioBkDIB(Scenario::BackPatterns::WoodBackground),
    ScenarioImageLibrary::get(ScenarioImageLibrary::CTabImages), font,
    scenario->getBorderColors());

  ASSERT(d_infoWind);
}

TownInfo_Int::~TownInfo_Int()
{
  delete d_infoWind;
}

void TownInfo_Int::run(ITown town, const PixelPoint& p)
{
  if(d_infoWind)
  {
    d_infoWind->run(town, p);
  }
}

void TownInfo_Int::destroy()
{
  if(d_infoWind)
  {
    delete d_infoWind;
    d_infoWind = 0;
  }
}

/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.2  2001/06/18 08:25:45  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
