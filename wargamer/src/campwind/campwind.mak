# Microsoft Developer Studio Generated NMAKE File, Based on campwind.dsp
!IF "$(CFG)" == ""
CFG=campwind - Win32 Editor Debug
!MESSAGE No configuration specified. Defaulting to campwind - Win32 Editor Debug.
!ENDIF 

!IF "$(CFG)" != "campwind - Win32 Release" && "$(CFG)" != "campwind - Win32 Debug" && "$(CFG)" != "campwind - Win32 Editor Debug" && "$(CFG)" != "campwind - Win32 Editor Release"
!MESSAGE Invalid configuration "$(CFG)" specified.
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "campwind.mak" CFG="campwind - Win32 Editor Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "campwind - Win32 Release" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE "campwind - Win32 Debug" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE "campwind - Win32 Editor Debug" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE "campwind - Win32 Editor Release" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE 
!ERROR An invalid configuration is specified.
!ENDIF 

!IF "$(OS)" == "Windows_NT"
NULL=
!ELSE 
NULL=nul
!ENDIF 

CPP=cl.exe
MTL=midl.exe
RSC=rc.exe

!IF  "$(CFG)" == "campwind - Win32 Release"

OUTDIR=.\..\..\exe
INTDIR=.\o\release
# Begin Custom Macros
OutDir=.\..\..\exe
# End Custom Macros

!IF "$(RECURSE)" == "0" 

ALL : "$(OUTDIR)\campwind.dll"

!ELSE 

ALL : "clogic - Win32 Release" "gwind - Win32 Release" "mapwind - Win32 Release" "ob - Win32 Release" "gamesup - Win32 Release" "system - Win32 Release" "campdata - Win32 Release" "$(OUTDIR)\campwind.dll"

!ENDIF 

!IF "$(RECURSE)" == "1" 
CLEAN :"campdata - Win32 ReleaseCLEAN" "system - Win32 ReleaseCLEAN" "gamesup - Win32 ReleaseCLEAN" "ob - Win32 ReleaseCLEAN" "mapwind - Win32 ReleaseCLEAN" "gwind - Win32 ReleaseCLEAN" "clogic - Win32 ReleaseCLEAN" 
!ELSE 
CLEAN :
!ENDIF 
	-@erase "$(INTDIR)\bld_dial.obj"
	-@erase "$(INTDIR)\calctrl.obj"
	-@erase "$(INTDIR)\camptool.obj"
	-@erase "$(INTDIR)\campwind.obj"
	-@erase "$(INTDIR)\cbatdial.obj"
	-@erase "$(INTDIR)\cbr_wind.obj"
	-@erase "$(INTDIR)\cclock.obj"
	-@erase "$(INTDIR)\ccombo.obj"
	-@erase "$(INTDIR)\chckdata.obj"
	-@erase "$(INTDIR)\combinedinterface.obj"
	-@erase "$(INTDIR)\ctimctrl.obj"
	-@erase "$(INTDIR)\find.obj"
	-@erase "$(INTDIR)\infobase.obj"
	-@erase "$(INTDIR)\infowind.obj"
	-@erase "$(INTDIR)\mainwind.obj"
	-@erase "$(INTDIR)\msgwin.obj"
	-@erase "$(INTDIR)\nm_wind.obj"
	-@erase "$(INTDIR)\obwin.obj"
	-@erase "$(INTDIR)\optdial.obj"
	-@erase "$(INTDIR)\ordertb.obj"
	-@erase "$(INTDIR)\org_list.obj"
	-@erase "$(INTDIR)\org_ordr.obj"
	-@erase "$(INTDIR)\reorgldr.obj"
	-@erase "$(INTDIR)\reorgob.obj"
	-@erase "$(INTDIR)\s_result.obj"
	-@erase "$(INTDIR)\spwind.obj"
	-@erase "$(INTDIR)\tinfowin.obj"
	-@erase "$(INTDIR)\to_win.obj"
	-@erase "$(INTDIR)\toolbar.obj"
	-@erase "$(INTDIR)\tordrwin.obj"
	-@erase "$(INTDIR)\towninfo.obj"
	-@erase "$(INTDIR)\uinfowin.obj"
	-@erase "$(INTDIR)\unitinfo.obj"
	-@erase "$(INTDIR)\unitmenu.obj"
	-@erase "$(INTDIR)\uordrwin.obj"
	-@erase "$(INTDIR)\userint.obj"
	-@erase "$(INTDIR)\vc60.idb"
	-@erase "$(INTDIR)\vc60.pdb"
	-@erase "$(INTDIR)\victwind.obj"
	-@erase "$(INTDIR)\weathwin.obj"
	-@erase "$(OUTDIR)\campwind.dll"
	-@erase "$(OUTDIR)\campwind.exp"
	-@erase "$(OUTDIR)\campwind.lib"
	-@erase "$(OUTDIR)\campwind.pdb"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

"$(INTDIR)" :
    if not exist "$(INTDIR)/$(NULL)" mkdir "$(INTDIR)"

CPP_PROJ=/nologo /MD /W3 /Gi /GR /GX /Zi /O2 /I "..\mapwind" /I "..\h" /I "..\..\res" /I "..\system" /I "..\gamesup" /I "..\ob" /I "..\campdata" /I "..\gwind" /I "..\campLogic" /D "NDEBUG" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "EXPORT_CAMPWIND_DLL" /Fp"$(INTDIR)\campwind.pch" /YX"stdinc.hpp" /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /c 
MTL_PROJ=/nologo /D "NDEBUG" /mktyplib203 /win32 
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\campwind.bsc" 
BSC32_SBRS= \
	
LINK32=link.exe
LINK32_FLAGS=gdi32.lib user32.lib comctl32.lib /nologo /dll /incremental:no /pdb:"$(OUTDIR)\campwind.pdb" /debug /machine:I386 /out:"$(OUTDIR)\campwind.dll" /implib:"$(OUTDIR)\campwind.lib" 
LINK32_OBJS= \
	"$(INTDIR)\bld_dial.obj" \
	"$(INTDIR)\calctrl.obj" \
	"$(INTDIR)\camptool.obj" \
	"$(INTDIR)\campwind.obj" \
	"$(INTDIR)\cbatdial.obj" \
	"$(INTDIR)\cbr_wind.obj" \
	"$(INTDIR)\cclock.obj" \
	"$(INTDIR)\ccombo.obj" \
	"$(INTDIR)\chckdata.obj" \
	"$(INTDIR)\combinedinterface.obj" \
	"$(INTDIR)\ctimctrl.obj" \
	"$(INTDIR)\find.obj" \
	"$(INTDIR)\infobase.obj" \
	"$(INTDIR)\infowind.obj" \
	"$(INTDIR)\mainwind.obj" \
	"$(INTDIR)\msgwin.obj" \
	"$(INTDIR)\nm_wind.obj" \
	"$(INTDIR)\obwin.obj" \
	"$(INTDIR)\optdial.obj" \
	"$(INTDIR)\ordertb.obj" \
	"$(INTDIR)\org_list.obj" \
	"$(INTDIR)\org_ordr.obj" \
	"$(INTDIR)\reorgldr.obj" \
	"$(INTDIR)\reorgob.obj" \
	"$(INTDIR)\s_result.obj" \
	"$(INTDIR)\spwind.obj" \
	"$(INTDIR)\tinfowin.obj" \
	"$(INTDIR)\to_win.obj" \
	"$(INTDIR)\toolbar.obj" \
	"$(INTDIR)\tordrwin.obj" \
	"$(INTDIR)\towninfo.obj" \
	"$(INTDIR)\uinfowin.obj" \
	"$(INTDIR)\unitinfo.obj" \
	"$(INTDIR)\unitmenu.obj" \
	"$(INTDIR)\uordrwin.obj" \
	"$(INTDIR)\userint.obj" \
	"$(INTDIR)\victwind.obj" \
	"$(INTDIR)\weathwin.obj" \
	"$(OUTDIR)\campdata.lib" \
	"$(OUTDIR)\system.lib" \
	"$(OUTDIR)\gamesup.lib" \
	"$(OUTDIR)\ob.lib" \
	"$(OUTDIR)\mapwind.lib" \
	"$(OUTDIR)\gwind.lib" \
	"$(OUTDIR)\clogic.lib"

"$(OUTDIR)\campwind.dll" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

!ELSEIF  "$(CFG)" == "campwind - Win32 Debug"

OUTDIR=.\..\..\exe
INTDIR=.\o\debug
# Begin Custom Macros
OutDir=.\..\..\exe
# End Custom Macros

!IF "$(RECURSE)" == "0" 

ALL : "$(OUTDIR)\campwindDB.dll"

!ELSE 

ALL : "clogic - Win32 Debug" "gwind - Win32 Debug" "mapwind - Win32 Debug" "ob - Win32 Debug" "gamesup - Win32 Debug" "system - Win32 Debug" "campdata - Win32 Debug" "$(OUTDIR)\campwindDB.dll"

!ENDIF 

!IF "$(RECURSE)" == "1" 
CLEAN :"campdata - Win32 DebugCLEAN" "system - Win32 DebugCLEAN" "gamesup - Win32 DebugCLEAN" "ob - Win32 DebugCLEAN" "mapwind - Win32 DebugCLEAN" "gwind - Win32 DebugCLEAN" "clogic - Win32 DebugCLEAN" 
!ELSE 
CLEAN :
!ENDIF 
	-@erase "$(INTDIR)\bld_dial.obj"
	-@erase "$(INTDIR)\calctrl.obj"
	-@erase "$(INTDIR)\camptool.obj"
	-@erase "$(INTDIR)\campwind.obj"
	-@erase "$(INTDIR)\cbatdial.obj"
	-@erase "$(INTDIR)\cbr_wind.obj"
	-@erase "$(INTDIR)\cclock.obj"
	-@erase "$(INTDIR)\ccombo.obj"
	-@erase "$(INTDIR)\chckdata.obj"
	-@erase "$(INTDIR)\combinedinterface.obj"
	-@erase "$(INTDIR)\ctimctrl.obj"
	-@erase "$(INTDIR)\find.obj"
	-@erase "$(INTDIR)\infobase.obj"
	-@erase "$(INTDIR)\infowind.obj"
	-@erase "$(INTDIR)\mainwind.obj"
	-@erase "$(INTDIR)\msgwin.obj"
	-@erase "$(INTDIR)\nm_wind.obj"
	-@erase "$(INTDIR)\obwin.obj"
	-@erase "$(INTDIR)\optdial.obj"
	-@erase "$(INTDIR)\ordertb.obj"
	-@erase "$(INTDIR)\org_list.obj"
	-@erase "$(INTDIR)\org_ordr.obj"
	-@erase "$(INTDIR)\reorgldr.obj"
	-@erase "$(INTDIR)\reorgob.obj"
	-@erase "$(INTDIR)\s_result.obj"
	-@erase "$(INTDIR)\spwind.obj"
	-@erase "$(INTDIR)\tinfowin.obj"
	-@erase "$(INTDIR)\to_win.obj"
	-@erase "$(INTDIR)\toolbar.obj"
	-@erase "$(INTDIR)\tordrwin.obj"
	-@erase "$(INTDIR)\towninfo.obj"
	-@erase "$(INTDIR)\uinfowin.obj"
	-@erase "$(INTDIR)\unitinfo.obj"
	-@erase "$(INTDIR)\unitmenu.obj"
	-@erase "$(INTDIR)\uordrwin.obj"
	-@erase "$(INTDIR)\userint.obj"
	-@erase "$(INTDIR)\vc60.idb"
	-@erase "$(INTDIR)\vc60.pdb"
	-@erase "$(INTDIR)\victwind.obj"
	-@erase "$(INTDIR)\weathwin.obj"
	-@erase "$(OUTDIR)\campwindDB.dll"
	-@erase "$(OUTDIR)\campwindDB.exp"
	-@erase "$(OUTDIR)\campwindDB.ilk"
	-@erase "$(OUTDIR)\campwindDB.lib"
	-@erase "$(OUTDIR)\campwindDB.pdb"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

"$(INTDIR)" :
    if not exist "$(INTDIR)/$(NULL)" mkdir "$(INTDIR)"

CPP_PROJ=/nologo /MDd /W3 /Gm /Gi /GR /GX /ZI /Od /I "..\mapwind" /I "..\h" /I "..\..\res" /I "..\system" /I "..\gamesup" /I "..\ob" /I "..\campdata" /I "..\gwind" /I "..\campLogic" /D "_DEBUG" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "EXPORT_CAMPWIND_DLL" /Fp"$(INTDIR)\campwind.pch" /YX"stdinc.hpp" /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /GZ /c 
MTL_PROJ=/nologo /D "_DEBUG" /mktyplib203 /win32 
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\campwind.bsc" 
BSC32_SBRS= \
	
LINK32=link.exe
LINK32_FLAGS=gdi32.lib user32.lib comctl32.lib /nologo /dll /incremental:yes /pdb:"$(OUTDIR)\campwindDB.pdb" /debug /machine:I386 /out:"$(OUTDIR)\campwindDB.dll" /implib:"$(OUTDIR)\campwindDB.lib" /pdbtype:sept 
LINK32_OBJS= \
	"$(INTDIR)\bld_dial.obj" \
	"$(INTDIR)\calctrl.obj" \
	"$(INTDIR)\camptool.obj" \
	"$(INTDIR)\campwind.obj" \
	"$(INTDIR)\cbatdial.obj" \
	"$(INTDIR)\cbr_wind.obj" \
	"$(INTDIR)\cclock.obj" \
	"$(INTDIR)\ccombo.obj" \
	"$(INTDIR)\chckdata.obj" \
	"$(INTDIR)\combinedinterface.obj" \
	"$(INTDIR)\ctimctrl.obj" \
	"$(INTDIR)\find.obj" \
	"$(INTDIR)\infobase.obj" \
	"$(INTDIR)\infowind.obj" \
	"$(INTDIR)\mainwind.obj" \
	"$(INTDIR)\msgwin.obj" \
	"$(INTDIR)\nm_wind.obj" \
	"$(INTDIR)\obwin.obj" \
	"$(INTDIR)\optdial.obj" \
	"$(INTDIR)\ordertb.obj" \
	"$(INTDIR)\org_list.obj" \
	"$(INTDIR)\org_ordr.obj" \
	"$(INTDIR)\reorgldr.obj" \
	"$(INTDIR)\reorgob.obj" \
	"$(INTDIR)\s_result.obj" \
	"$(INTDIR)\spwind.obj" \
	"$(INTDIR)\tinfowin.obj" \
	"$(INTDIR)\to_win.obj" \
	"$(INTDIR)\toolbar.obj" \
	"$(INTDIR)\tordrwin.obj" \
	"$(INTDIR)\towninfo.obj" \
	"$(INTDIR)\uinfowin.obj" \
	"$(INTDIR)\unitinfo.obj" \
	"$(INTDIR)\unitmenu.obj" \
	"$(INTDIR)\uordrwin.obj" \
	"$(INTDIR)\userint.obj" \
	"$(INTDIR)\victwind.obj" \
	"$(INTDIR)\weathwin.obj" \
	"$(OUTDIR)\campdataDB.lib" \
	"$(OUTDIR)\systemDB.lib" \
	"$(OUTDIR)\gamesupDB.lib" \
	"$(OUTDIR)\obDB.lib" \
	"$(OUTDIR)\mapwindDB.lib" \
	"$(OUTDIR)\gwindDB.lib" \
	"$(OUTDIR)\clogicDB.lib"

"$(OUTDIR)\campwindDB.dll" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

!ELSEIF  "$(CFG)" == "campwind - Win32 Editor Debug"

OUTDIR=.\..\..\exe
INTDIR=.\o\Editor\Debug
# Begin Custom Macros
OutDir=.\..\..\exe
# End Custom Macros

!IF "$(RECURSE)" == "0" 

ALL : "$(OUTDIR)\campwindEDDB.dll"

!ELSE 

ALL : "campedit - Win32 Editor Debug" "gwind - Win32 Editor Debug" "mapwind - Win32 Editor Debug" "ob - Win32 Editor Debug" "gamesup - Win32 Editor Debug" "system - Win32 Editor Debug" "campdata - Win32 Editor Debug" "$(OUTDIR)\campwindEDDB.dll"

!ENDIF 

!IF "$(RECURSE)" == "1" 
CLEAN :"campdata - Win32 Editor DebugCLEAN" "system - Win32 Editor DebugCLEAN" "gamesup - Win32 Editor DebugCLEAN" "ob - Win32 Editor DebugCLEAN" "mapwind - Win32 Editor DebugCLEAN" "gwind - Win32 Editor DebugCLEAN" "campedit - Win32 Editor DebugCLEAN" 
!ELSE 
CLEAN :
!ENDIF 
	-@erase "$(INTDIR)\bld_dial.obj"
	-@erase "$(INTDIR)\calctrl.obj"
	-@erase "$(INTDIR)\camptool.obj"
	-@erase "$(INTDIR)\campwind.obj"
	-@erase "$(INTDIR)\cclock.obj"
	-@erase "$(INTDIR)\chckdata.obj"
	-@erase "$(INTDIR)\ctimctrl.obj"
	-@erase "$(INTDIR)\find.obj"
	-@erase "$(INTDIR)\infobase.obj"
	-@erase "$(INTDIR)\mainwind.obj"
	-@erase "$(INTDIR)\obwin.obj"
	-@erase "$(INTDIR)\optdial.obj"
	-@erase "$(INTDIR)\ordertb.obj"
	-@erase "$(INTDIR)\s_result.obj"
	-@erase "$(INTDIR)\to_win.obj"
	-@erase "$(INTDIR)\toolbar.obj"
	-@erase "$(INTDIR)\userint.obj"
	-@erase "$(INTDIR)\vc60.idb"
	-@erase "$(INTDIR)\vc60.pdb"
	-@erase "$(OUTDIR)\campwindEDDB.dll"
	-@erase "$(OUTDIR)\campwindEDDB.exp"
	-@erase "$(OUTDIR)\campwindEDDB.ilk"
	-@erase "$(OUTDIR)\campwindEDDB.lib"
	-@erase "$(OUTDIR)\campwindEDDB.pdb"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

"$(INTDIR)" :
    if not exist "$(INTDIR)/$(NULL)" mkdir "$(INTDIR)"

CPP_PROJ=/nologo /MDd /W3 /Gm /Gi /GR /GX /ZI /Od /I "..\mapwind" /I "..\h" /I "..\..\res" /I "..\system" /I "..\gamesup" /I "..\ob" /I "..\campdata" /I "..\gwind" /I "..\campLogic" /I "..\campedit" /D "_USRDLL" /D "EXPORT_CAMPWIND_DLL" /D "_DEBUG" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /D "EDITOR" /Fp"$(INTDIR)\campwind.pch" /YX"stdinc.hpp" /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /GZ /c 
MTL_PROJ=/nologo /D "_DEBUG" /mktyplib203 /win32 
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\campwind.bsc" 
BSC32_SBRS= \
	
LINK32=link.exe
LINK32_FLAGS=gdi32.lib user32.lib comctl32.lib /nologo /dll /incremental:yes /pdb:"$(OUTDIR)\campwindEDDB.pdb" /debug /machine:I386 /out:"$(OUTDIR)\campwindEDDB.dll" /implib:"$(OUTDIR)\campwindEDDB.lib" /pdbtype:sept 
LINK32_OBJS= \
	"$(INTDIR)\bld_dial.obj" \
	"$(INTDIR)\calctrl.obj" \
	"$(INTDIR)\camptool.obj" \
	"$(INTDIR)\campwind.obj" \
	"$(INTDIR)\cclock.obj" \
	"$(INTDIR)\chckdata.obj" \
	"$(INTDIR)\ctimctrl.obj" \
	"$(INTDIR)\find.obj" \
	"$(INTDIR)\infobase.obj" \
	"$(INTDIR)\mainwind.obj" \
	"$(INTDIR)\obwin.obj" \
	"$(INTDIR)\optdial.obj" \
	"$(INTDIR)\ordertb.obj" \
	"$(INTDIR)\s_result.obj" \
	"$(INTDIR)\to_win.obj" \
	"$(INTDIR)\toolbar.obj" \
	"$(INTDIR)\userint.obj" \
	"$(OUTDIR)\campdataEDDB.lib" \
	"$(OUTDIR)\systemDB.lib" \
	"$(OUTDIR)\gamesupDB.lib" \
	"$(OUTDIR)\obEDDB.lib" \
	"$(OUTDIR)\mapwindEDDB.lib" \
	"$(OUTDIR)\gwindEDDB.lib" \
	"$(OUTDIR)\campeditEDDB.lib"

"$(OUTDIR)\campwindEDDB.dll" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

!ELSEIF  "$(CFG)" == "campwind - Win32 Editor Release"

OUTDIR=.\..\..\exe
INTDIR=.\o\Editor\Release
# Begin Custom Macros
OutDir=.\..\..\exe
# End Custom Macros

!IF "$(RECURSE)" == "0" 

ALL : "$(OUTDIR)\campwindED.dll"

!ELSE 

ALL : "campedit - Win32 Editor Release" "gwind - Win32 Editor Release" "mapwind - Win32 Editor Release" "ob - Win32 Editor Release" "gamesup - Win32 Editor Release" "system - Win32 Editor Release" "campdata - Win32 Editor Release" "$(OUTDIR)\campwindED.dll"

!ENDIF 

!IF "$(RECURSE)" == "1" 
CLEAN :"campdata - Win32 Editor ReleaseCLEAN" "system - Win32 Editor ReleaseCLEAN" "gamesup - Win32 Editor ReleaseCLEAN" "ob - Win32 Editor ReleaseCLEAN" "mapwind - Win32 Editor ReleaseCLEAN" "gwind - Win32 Editor ReleaseCLEAN" "campedit - Win32 Editor ReleaseCLEAN" 
!ELSE 
CLEAN :
!ENDIF 
	-@erase "$(INTDIR)\bld_dial.obj"
	-@erase "$(INTDIR)\calctrl.obj"
	-@erase "$(INTDIR)\camptool.obj"
	-@erase "$(INTDIR)\campwind.obj"
	-@erase "$(INTDIR)\cclock.obj"
	-@erase "$(INTDIR)\chckdata.obj"
	-@erase "$(INTDIR)\ctimctrl.obj"
	-@erase "$(INTDIR)\find.obj"
	-@erase "$(INTDIR)\infobase.obj"
	-@erase "$(INTDIR)\mainwind.obj"
	-@erase "$(INTDIR)\obwin.obj"
	-@erase "$(INTDIR)\optdial.obj"
	-@erase "$(INTDIR)\ordertb.obj"
	-@erase "$(INTDIR)\s_result.obj"
	-@erase "$(INTDIR)\to_win.obj"
	-@erase "$(INTDIR)\toolbar.obj"
	-@erase "$(INTDIR)\userint.obj"
	-@erase "$(INTDIR)\vc60.idb"
	-@erase "$(OUTDIR)\campwindED.dll"
	-@erase "$(OUTDIR)\campwindED.exp"
	-@erase "$(OUTDIR)\campwindED.lib"
	-@erase "$(OUTDIR)\campwindED.pdb"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

"$(INTDIR)" :
    if not exist "$(INTDIR)/$(NULL)" mkdir "$(INTDIR)"

CPP_PROJ=/nologo /MD /W3 /Gi /GR /GX /Zd /O2 /I "..\mapwind" /I "..\h" /I "..\..\res" /I "..\system" /I "..\gamesup" /I "..\ob" /I "..\campdata" /I "..\gwind" /I "..\campLogic" /I "..\campedit" /D "NDEBUG" /D "_USRDLL" /D "EXPORT_CAMPWIND_DLL" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /D "EDITOR" /Fp"$(INTDIR)\campwind.pch" /YX"stdinc.hpp" /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /c 
MTL_PROJ=/nologo /D "NDEBUG" /mktyplib203 /win32 
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\campwind.bsc" 
BSC32_SBRS= \
	
LINK32=link.exe
LINK32_FLAGS=gdi32.lib user32.lib comctl32.lib /nologo /dll /incremental:no /pdb:"$(OUTDIR)\campwindED.pdb" /debug /machine:I386 /out:"$(OUTDIR)\campwindED.dll" /implib:"$(OUTDIR)\campwindED.lib" 
LINK32_OBJS= \
	"$(INTDIR)\bld_dial.obj" \
	"$(INTDIR)\calctrl.obj" \
	"$(INTDIR)\camptool.obj" \
	"$(INTDIR)\campwind.obj" \
	"$(INTDIR)\cclock.obj" \
	"$(INTDIR)\chckdata.obj" \
	"$(INTDIR)\ctimctrl.obj" \
	"$(INTDIR)\find.obj" \
	"$(INTDIR)\infobase.obj" \
	"$(INTDIR)\mainwind.obj" \
	"$(INTDIR)\obwin.obj" \
	"$(INTDIR)\optdial.obj" \
	"$(INTDIR)\ordertb.obj" \
	"$(INTDIR)\s_result.obj" \
	"$(INTDIR)\to_win.obj" \
	"$(INTDIR)\toolbar.obj" \
	"$(INTDIR)\userint.obj" \
	"$(OUTDIR)\campdataED.lib" \
	"$(OUTDIR)\system.lib" \
	"$(OUTDIR)\gamesup.lib" \
	"$(OUTDIR)\obED.lib" \
	"$(OUTDIR)\mapwindED.lib" \
	"$(OUTDIR)\gwindED.lib" \
	"$(OUTDIR)\campeditED.lib"

"$(OUTDIR)\campwindED.dll" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

!ENDIF 

.c{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.c{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<


!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("campwind.dep")
!INCLUDE "campwind.dep"
!ELSE 
!MESSAGE Warning: cannot find "campwind.dep"
!ENDIF 
!ENDIF 


!IF "$(CFG)" == "campwind - Win32 Release" || "$(CFG)" == "campwind - Win32 Debug" || "$(CFG)" == "campwind - Win32 Editor Debug" || "$(CFG)" == "campwind - Win32 Editor Release"
SOURCE=.\bld_dial.cpp

"$(INTDIR)\bld_dial.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\calctrl.cpp

"$(INTDIR)\calctrl.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\camptool.cpp

"$(INTDIR)\camptool.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\campwind.cpp

"$(INTDIR)\campwind.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\cbatdial.cpp

!IF  "$(CFG)" == "campwind - Win32 Release"


"$(INTDIR)\cbatdial.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "campwind - Win32 Debug"


"$(INTDIR)\cbatdial.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "campwind - Win32 Editor Debug"

!ELSEIF  "$(CFG)" == "campwind - Win32 Editor Release"

!ENDIF 

SOURCE=.\cbr_wind.cpp

!IF  "$(CFG)" == "campwind - Win32 Release"


"$(INTDIR)\cbr_wind.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "campwind - Win32 Debug"


"$(INTDIR)\cbr_wind.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "campwind - Win32 Editor Debug"

!ELSEIF  "$(CFG)" == "campwind - Win32 Editor Release"

!ENDIF 

SOURCE=.\cclock.cpp

"$(INTDIR)\cclock.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\ccombo.cpp

!IF  "$(CFG)" == "campwind - Win32 Release"


"$(INTDIR)\ccombo.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "campwind - Win32 Debug"


"$(INTDIR)\ccombo.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "campwind - Win32 Editor Debug"

!ELSEIF  "$(CFG)" == "campwind - Win32 Editor Release"

!ENDIF 

SOURCE=.\chckdata.cpp

"$(INTDIR)\chckdata.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\combinedinterface.cpp

!IF  "$(CFG)" == "campwind - Win32 Release"


"$(INTDIR)\combinedinterface.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "campwind - Win32 Debug"


"$(INTDIR)\combinedinterface.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "campwind - Win32 Editor Debug"

!ELSEIF  "$(CFG)" == "campwind - Win32 Editor Release"

!ENDIF 

SOURCE=.\ctimctrl.cpp

"$(INTDIR)\ctimctrl.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\find.cpp

"$(INTDIR)\find.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\ftdial.cpp
SOURCE=.\infobase.cpp

"$(INTDIR)\infobase.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\infowind.cpp

!IF  "$(CFG)" == "campwind - Win32 Release"


"$(INTDIR)\infowind.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "campwind - Win32 Debug"


"$(INTDIR)\infowind.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "campwind - Win32 Editor Debug"

!ELSEIF  "$(CFG)" == "campwind - Win32 Editor Release"

!ENDIF 

SOURCE=.\mainwind.cpp

"$(INTDIR)\mainwind.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\msgwin.cpp

!IF  "$(CFG)" == "campwind - Win32 Release"


"$(INTDIR)\msgwin.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "campwind - Win32 Debug"


"$(INTDIR)\msgwin.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "campwind - Win32 Editor Debug"

!ELSEIF  "$(CFG)" == "campwind - Win32 Editor Release"

!ENDIF 

SOURCE=.\nm_wind.cpp

!IF  "$(CFG)" == "campwind - Win32 Release"


"$(INTDIR)\nm_wind.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "campwind - Win32 Debug"


"$(INTDIR)\nm_wind.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "campwind - Win32 Editor Debug"

!ELSEIF  "$(CFG)" == "campwind - Win32 Editor Release"

!ENDIF 

SOURCE=.\obwin.cpp

"$(INTDIR)\obwin.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\optdial.cpp

"$(INTDIR)\optdial.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\optwiz.cpp
SOURCE=.\ordertb.cpp

"$(INTDIR)\ordertb.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\org_dial.cpp
SOURCE=.\org_list.cpp

!IF  "$(CFG)" == "campwind - Win32 Release"


"$(INTDIR)\org_list.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "campwind - Win32 Debug"


"$(INTDIR)\org_list.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "campwind - Win32 Editor Debug"

!ELSEIF  "$(CFG)" == "campwind - Win32 Editor Release"

!ENDIF 

SOURCE=.\org_ordr.cpp

!IF  "$(CFG)" == "campwind - Win32 Release"


"$(INTDIR)\org_ordr.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "campwind - Win32 Debug"


"$(INTDIR)\org_ordr.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "campwind - Win32 Editor Debug"

!ELSEIF  "$(CFG)" == "campwind - Win32 Editor Release"

!ENDIF 

SOURCE=.\reorgldr.cpp

!IF  "$(CFG)" == "campwind - Win32 Release"


"$(INTDIR)\reorgldr.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "campwind - Win32 Debug"


"$(INTDIR)\reorgldr.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "campwind - Win32 Editor Debug"

!ELSEIF  "$(CFG)" == "campwind - Win32 Editor Release"

!ENDIF 

SOURCE=.\reorgob.cpp

!IF  "$(CFG)" == "campwind - Win32 Release"


"$(INTDIR)\reorgob.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "campwind - Win32 Debug"


"$(INTDIR)\reorgob.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "campwind - Win32 Editor Debug"

!ELSEIF  "$(CFG)" == "campwind - Win32 Editor Release"

!ENDIF 

SOURCE=.\s_result.cpp

"$(INTDIR)\s_result.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\spwind.cpp

!IF  "$(CFG)" == "campwind - Win32 Release"


"$(INTDIR)\spwind.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "campwind - Win32 Debug"


"$(INTDIR)\spwind.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "campwind - Win32 Editor Debug"

!ELSEIF  "$(CFG)" == "campwind - Win32 Editor Release"

!ENDIF 

SOURCE=.\statwind.cpp
SOURCE=.\tinfowin.cpp

!IF  "$(CFG)" == "campwind - Win32 Release"


"$(INTDIR)\tinfowin.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "campwind - Win32 Debug"


"$(INTDIR)\tinfowin.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "campwind - Win32 Editor Debug"

!ELSEIF  "$(CFG)" == "campwind - Win32 Editor Release"

!ENDIF 

SOURCE=.\to_win.cpp

"$(INTDIR)\to_win.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\toolbar.cpp

"$(INTDIR)\toolbar.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\toolbox.cpp
SOURCE=.\tordrwin.cpp

!IF  "$(CFG)" == "campwind - Win32 Release"


"$(INTDIR)\tordrwin.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "campwind - Win32 Debug"


"$(INTDIR)\tordrwin.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "campwind - Win32 Editor Debug"

!ELSEIF  "$(CFG)" == "campwind - Win32 Editor Release"

!ENDIF 

SOURCE=.\towndial.cpp
SOURCE=.\towninfo.cpp

!IF  "$(CFG)" == "campwind - Win32 Release"


"$(INTDIR)\towninfo.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "campwind - Win32 Debug"


"$(INTDIR)\towninfo.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "campwind - Win32 Editor Debug"

!ELSEIF  "$(CFG)" == "campwind - Win32 Editor Release"

!ENDIF 

SOURCE=.\townint.cpp
SOURCE=.\townmenu.cpp
SOURCE=.\uinfowin.cpp

!IF  "$(CFG)" == "campwind - Win32 Release"


"$(INTDIR)\uinfowin.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "campwind - Win32 Debug"


"$(INTDIR)\uinfowin.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "campwind - Win32 Editor Debug"

!ELSEIF  "$(CFG)" == "campwind - Win32 Editor Release"

!ENDIF 

SOURCE=.\unitdial.cpp
SOURCE=.\unitinfo.cpp

!IF  "$(CFG)" == "campwind - Win32 Release"


"$(INTDIR)\unitinfo.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "campwind - Win32 Debug"


"$(INTDIR)\unitinfo.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "campwind - Win32 Editor Debug"

!ELSEIF  "$(CFG)" == "campwind - Win32 Editor Release"

!ENDIF 

SOURCE=.\unitint.cpp
SOURCE=.\unitmenu.cpp

!IF  "$(CFG)" == "campwind - Win32 Release"


"$(INTDIR)\unitmenu.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "campwind - Win32 Debug"


"$(INTDIR)\unitmenu.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "campwind - Win32 Editor Debug"

!ELSEIF  "$(CFG)" == "campwind - Win32 Editor Release"

!ENDIF 

SOURCE=.\uordrwin.cpp

!IF  "$(CFG)" == "campwind - Win32 Release"


"$(INTDIR)\uordrwin.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "campwind - Win32 Debug"


"$(INTDIR)\uordrwin.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "campwind - Win32 Editor Debug"

!ELSEIF  "$(CFG)" == "campwind - Win32 Editor Release"

!ENDIF 

SOURCE=.\userint.cpp

"$(INTDIR)\userint.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\victwind.cpp

!IF  "$(CFG)" == "campwind - Win32 Release"


"$(INTDIR)\victwind.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "campwind - Win32 Debug"


"$(INTDIR)\victwind.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "campwind - Win32 Editor Debug"

!ELSEIF  "$(CFG)" == "campwind - Win32 Editor Release"

!ENDIF 

SOURCE=.\weathwin.cpp

!IF  "$(CFG)" == "campwind - Win32 Release"


"$(INTDIR)\weathwin.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "campwind - Win32 Debug"


"$(INTDIR)\weathwin.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "campwind - Win32 Editor Debug"

!ELSEIF  "$(CFG)" == "campwind - Win32 Editor Release"

!ENDIF 

!IF  "$(CFG)" == "campwind - Win32 Release"

"campdata - Win32 Release" : 
   cd "\src\sf\wargamer\src\campdata"
   $(MAKE) /$(MAKEFLAGS) /F .\campdata.mak CFG="campdata - Win32 Release" 
   cd "..\campwind"

"campdata - Win32 ReleaseCLEAN" : 
   cd "\src\sf\wargamer\src\campdata"
   $(MAKE) /$(MAKEFLAGS) /F .\campdata.mak CFG="campdata - Win32 Release" RECURSE=1 CLEAN 
   cd "..\campwind"

!ELSEIF  "$(CFG)" == "campwind - Win32 Debug"

"campdata - Win32 Debug" : 
   cd "\src\sf\wargamer\src\campdata"
   $(MAKE) /$(MAKEFLAGS) /F .\campdata.mak CFG="campdata - Win32 Debug" 
   cd "..\campwind"

"campdata - Win32 DebugCLEAN" : 
   cd "\src\sf\wargamer\src\campdata"
   $(MAKE) /$(MAKEFLAGS) /F .\campdata.mak CFG="campdata - Win32 Debug" RECURSE=1 CLEAN 
   cd "..\campwind"

!ELSEIF  "$(CFG)" == "campwind - Win32 Editor Debug"

"campdata - Win32 Editor Debug" : 
   cd "\src\sf\wargamer\src\campdata"
   $(MAKE) /$(MAKEFLAGS) /F .\campdata.mak CFG="campdata - Win32 Editor Debug" 
   cd "..\campwind"

"campdata - Win32 Editor DebugCLEAN" : 
   cd "\src\sf\wargamer\src\campdata"
   $(MAKE) /$(MAKEFLAGS) /F .\campdata.mak CFG="campdata - Win32 Editor Debug" RECURSE=1 CLEAN 
   cd "..\campwind"

!ELSEIF  "$(CFG)" == "campwind - Win32 Editor Release"

"campdata - Win32 Editor Release" : 
   cd "\src\sf\wargamer\src\campdata"
   $(MAKE) /$(MAKEFLAGS) /F .\campdata.mak CFG="campdata - Win32 Editor Release" 
   cd "..\campwind"

"campdata - Win32 Editor ReleaseCLEAN" : 
   cd "\src\sf\wargamer\src\campdata"
   $(MAKE) /$(MAKEFLAGS) /F .\campdata.mak CFG="campdata - Win32 Editor Release" RECURSE=1 CLEAN 
   cd "..\campwind"

!ENDIF 

!IF  "$(CFG)" == "campwind - Win32 Release"

"system - Win32 Release" : 
   cd "\src\sf\wargamer\SRC\SYSTEM"
   $(MAKE) /$(MAKEFLAGS) /F .\system.mak CFG="system - Win32 Release" 
   cd "..\campwind"

"system - Win32 ReleaseCLEAN" : 
   cd "\src\sf\wargamer\SRC\SYSTEM"
   $(MAKE) /$(MAKEFLAGS) /F .\system.mak CFG="system - Win32 Release" RECURSE=1 CLEAN 
   cd "..\campwind"

!ELSEIF  "$(CFG)" == "campwind - Win32 Debug"

"system - Win32 Debug" : 
   cd "\src\sf\wargamer\SRC\SYSTEM"
   $(MAKE) /$(MAKEFLAGS) /F .\system.mak CFG="system - Win32 Debug" 
   cd "..\campwind"

"system - Win32 DebugCLEAN" : 
   cd "\src\sf\wargamer\SRC\SYSTEM"
   $(MAKE) /$(MAKEFLAGS) /F .\system.mak CFG="system - Win32 Debug" RECURSE=1 CLEAN 
   cd "..\campwind"

!ELSEIF  "$(CFG)" == "campwind - Win32 Editor Debug"

"system - Win32 Editor Debug" : 
   cd "\src\sf\wargamer\SRC\SYSTEM"
   $(MAKE) /$(MAKEFLAGS) /F .\system.mak CFG="system - Win32 Editor Debug" 
   cd "..\campwind"

"system - Win32 Editor DebugCLEAN" : 
   cd "\src\sf\wargamer\SRC\SYSTEM"
   $(MAKE) /$(MAKEFLAGS) /F .\system.mak CFG="system - Win32 Editor Debug" RECURSE=1 CLEAN 
   cd "..\campwind"

!ELSEIF  "$(CFG)" == "campwind - Win32 Editor Release"

"system - Win32 Editor Release" : 
   cd "\src\sf\wargamer\SRC\SYSTEM"
   $(MAKE) /$(MAKEFLAGS) /F .\system.mak CFG="system - Win32 Editor Release" 
   cd "..\campwind"

"system - Win32 Editor ReleaseCLEAN" : 
   cd "\src\sf\wargamer\SRC\SYSTEM"
   $(MAKE) /$(MAKEFLAGS) /F .\system.mak CFG="system - Win32 Editor Release" RECURSE=1 CLEAN 
   cd "..\campwind"

!ENDIF 

!IF  "$(CFG)" == "campwind - Win32 Release"

"gamesup - Win32 Release" : 
   cd "\src\sf\wargamer\SRC\GAMESUP"
   $(MAKE) /$(MAKEFLAGS) /F .\gamesup.mak CFG="gamesup - Win32 Release" 
   cd "..\campwind"

"gamesup - Win32 ReleaseCLEAN" : 
   cd "\src\sf\wargamer\SRC\GAMESUP"
   $(MAKE) /$(MAKEFLAGS) /F .\gamesup.mak CFG="gamesup - Win32 Release" RECURSE=1 CLEAN 
   cd "..\campwind"

!ELSEIF  "$(CFG)" == "campwind - Win32 Debug"

"gamesup - Win32 Debug" : 
   cd "\src\sf\wargamer\SRC\GAMESUP"
   $(MAKE) /$(MAKEFLAGS) /F .\gamesup.mak CFG="gamesup - Win32 Debug" 
   cd "..\campwind"

"gamesup - Win32 DebugCLEAN" : 
   cd "\src\sf\wargamer\SRC\GAMESUP"
   $(MAKE) /$(MAKEFLAGS) /F .\gamesup.mak CFG="gamesup - Win32 Debug" RECURSE=1 CLEAN 
   cd "..\campwind"

!ELSEIF  "$(CFG)" == "campwind - Win32 Editor Debug"

"gamesup - Win32 Editor Debug" : 
   cd "\src\sf\wargamer\SRC\GAMESUP"
   $(MAKE) /$(MAKEFLAGS) /F .\gamesup.mak CFG="gamesup - Win32 Editor Debug" 
   cd "..\campwind"

"gamesup - Win32 Editor DebugCLEAN" : 
   cd "\src\sf\wargamer\SRC\GAMESUP"
   $(MAKE) /$(MAKEFLAGS) /F .\gamesup.mak CFG="gamesup - Win32 Editor Debug" RECURSE=1 CLEAN 
   cd "..\campwind"

!ELSEIF  "$(CFG)" == "campwind - Win32 Editor Release"

"gamesup - Win32 Editor Release" : 
   cd "\src\sf\wargamer\SRC\GAMESUP"
   $(MAKE) /$(MAKEFLAGS) /F .\gamesup.mak CFG="gamesup - Win32 Editor Release" 
   cd "..\campwind"

"gamesup - Win32 Editor ReleaseCLEAN" : 
   cd "\src\sf\wargamer\SRC\GAMESUP"
   $(MAKE) /$(MAKEFLAGS) /F .\gamesup.mak CFG="gamesup - Win32 Editor Release" RECURSE=1 CLEAN 
   cd "..\campwind"

!ENDIF 

!IF  "$(CFG)" == "campwind - Win32 Release"

"ob - Win32 Release" : 
   cd "\src\sf\wargamer\SRC\OB"
   $(MAKE) /$(MAKEFLAGS) /F .\ob.mak CFG="ob - Win32 Release" 
   cd "..\campwind"

"ob - Win32 ReleaseCLEAN" : 
   cd "\src\sf\wargamer\SRC\OB"
   $(MAKE) /$(MAKEFLAGS) /F .\ob.mak CFG="ob - Win32 Release" RECURSE=1 CLEAN 
   cd "..\campwind"

!ELSEIF  "$(CFG)" == "campwind - Win32 Debug"

"ob - Win32 Debug" : 
   cd "\src\sf\wargamer\SRC\OB"
   $(MAKE) /$(MAKEFLAGS) /F .\ob.mak CFG="ob - Win32 Debug" 
   cd "..\campwind"

"ob - Win32 DebugCLEAN" : 
   cd "\src\sf\wargamer\SRC\OB"
   $(MAKE) /$(MAKEFLAGS) /F .\ob.mak CFG="ob - Win32 Debug" RECURSE=1 CLEAN 
   cd "..\campwind"

!ELSEIF  "$(CFG)" == "campwind - Win32 Editor Debug"

"ob - Win32 Editor Debug" : 
   cd "\src\sf\wargamer\SRC\OB"
   $(MAKE) /$(MAKEFLAGS) /F .\ob.mak CFG="ob - Win32 Editor Debug" 
   cd "..\campwind"

"ob - Win32 Editor DebugCLEAN" : 
   cd "\src\sf\wargamer\SRC\OB"
   $(MAKE) /$(MAKEFLAGS) /F .\ob.mak CFG="ob - Win32 Editor Debug" RECURSE=1 CLEAN 
   cd "..\campwind"

!ELSEIF  "$(CFG)" == "campwind - Win32 Editor Release"

"ob - Win32 Editor Release" : 
   cd "\src\sf\wargamer\SRC\OB"
   $(MAKE) /$(MAKEFLAGS) /F .\ob.mak CFG="ob - Win32 Editor Release" 
   cd "..\campwind"

"ob - Win32 Editor ReleaseCLEAN" : 
   cd "\src\sf\wargamer\SRC\OB"
   $(MAKE) /$(MAKEFLAGS) /F .\ob.mak CFG="ob - Win32 Editor Release" RECURSE=1 CLEAN 
   cd "..\campwind"

!ENDIF 

!IF  "$(CFG)" == "campwind - Win32 Release"

"mapwind - Win32 Release" : 
   cd "\src\sf\wargamer\src\mapwind"
   $(MAKE) /$(MAKEFLAGS) /F .\mapwind.mak CFG="mapwind - Win32 Release" 
   cd "..\campwind"

"mapwind - Win32 ReleaseCLEAN" : 
   cd "\src\sf\wargamer\src\mapwind"
   $(MAKE) /$(MAKEFLAGS) /F .\mapwind.mak CFG="mapwind - Win32 Release" RECURSE=1 CLEAN 
   cd "..\campwind"

!ELSEIF  "$(CFG)" == "campwind - Win32 Debug"

"mapwind - Win32 Debug" : 
   cd "\src\sf\wargamer\src\mapwind"
   $(MAKE) /$(MAKEFLAGS) /F .\mapwind.mak CFG="mapwind - Win32 Debug" 
   cd "..\campwind"

"mapwind - Win32 DebugCLEAN" : 
   cd "\src\sf\wargamer\src\mapwind"
   $(MAKE) /$(MAKEFLAGS) /F .\mapwind.mak CFG="mapwind - Win32 Debug" RECURSE=1 CLEAN 
   cd "..\campwind"

!ELSEIF  "$(CFG)" == "campwind - Win32 Editor Debug"

"mapwind - Win32 Editor Debug" : 
   cd "\src\sf\wargamer\src\mapwind"
   $(MAKE) /$(MAKEFLAGS) /F .\mapwind.mak CFG="mapwind - Win32 Editor Debug" 
   cd "..\campwind"

"mapwind - Win32 Editor DebugCLEAN" : 
   cd "\src\sf\wargamer\src\mapwind"
   $(MAKE) /$(MAKEFLAGS) /F .\mapwind.mak CFG="mapwind - Win32 Editor Debug" RECURSE=1 CLEAN 
   cd "..\campwind"

!ELSEIF  "$(CFG)" == "campwind - Win32 Editor Release"

"mapwind - Win32 Editor Release" : 
   cd "\src\sf\wargamer\src\mapwind"
   $(MAKE) /$(MAKEFLAGS) /F .\mapwind.mak CFG="mapwind - Win32 Editor Release" 
   cd "..\campwind"

"mapwind - Win32 Editor ReleaseCLEAN" : 
   cd "\src\sf\wargamer\src\mapwind"
   $(MAKE) /$(MAKEFLAGS) /F .\mapwind.mak CFG="mapwind - Win32 Editor Release" RECURSE=1 CLEAN 
   cd "..\campwind"

!ENDIF 

!IF  "$(CFG)" == "campwind - Win32 Release"

"gwind - Win32 Release" : 
   cd "\src\sf\wargamer\src\gwind"
   $(MAKE) /$(MAKEFLAGS) /F .\gwind.mak CFG="gwind - Win32 Release" 
   cd "..\campwind"

"gwind - Win32 ReleaseCLEAN" : 
   cd "\src\sf\wargamer\src\gwind"
   $(MAKE) /$(MAKEFLAGS) /F .\gwind.mak CFG="gwind - Win32 Release" RECURSE=1 CLEAN 
   cd "..\campwind"

!ELSEIF  "$(CFG)" == "campwind - Win32 Debug"

"gwind - Win32 Debug" : 
   cd "\src\sf\wargamer\src\gwind"
   $(MAKE) /$(MAKEFLAGS) /F .\gwind.mak CFG="gwind - Win32 Debug" 
   cd "..\campwind"

"gwind - Win32 DebugCLEAN" : 
   cd "\src\sf\wargamer\src\gwind"
   $(MAKE) /$(MAKEFLAGS) /F .\gwind.mak CFG="gwind - Win32 Debug" RECURSE=1 CLEAN 
   cd "..\campwind"

!ELSEIF  "$(CFG)" == "campwind - Win32 Editor Debug"

"gwind - Win32 Editor Debug" : 
   cd "\src\sf\wargamer\src\gwind"
   $(MAKE) /$(MAKEFLAGS) /F .\gwind.mak CFG="gwind - Win32 Editor Debug" 
   cd "..\campwind"

"gwind - Win32 Editor DebugCLEAN" : 
   cd "\src\sf\wargamer\src\gwind"
   $(MAKE) /$(MAKEFLAGS) /F .\gwind.mak CFG="gwind - Win32 Editor Debug" RECURSE=1 CLEAN 
   cd "..\campwind"

!ELSEIF  "$(CFG)" == "campwind - Win32 Editor Release"

"gwind - Win32 Editor Release" : 
   cd "\src\sf\wargamer\src\gwind"
   $(MAKE) /$(MAKEFLAGS) /F .\gwind.mak CFG="gwind - Win32 Editor Release" 
   cd "..\campwind"

"gwind - Win32 Editor ReleaseCLEAN" : 
   cd "\src\sf\wargamer\src\gwind"
   $(MAKE) /$(MAKEFLAGS) /F .\gwind.mak CFG="gwind - Win32 Editor Release" RECURSE=1 CLEAN 
   cd "..\campwind"

!ENDIF 

!IF  "$(CFG)" == "campwind - Win32 Release"

"clogic - Win32 Release" : 
   cd "\src\sf\wargamer\src\campLogic"
   $(MAKE) /$(MAKEFLAGS) /F .\clogic.mak CFG="clogic - Win32 Release" 
   cd "..\campwind"

"clogic - Win32 ReleaseCLEAN" : 
   cd "\src\sf\wargamer\src\campLogic"
   $(MAKE) /$(MAKEFLAGS) /F .\clogic.mak CFG="clogic - Win32 Release" RECURSE=1 CLEAN 
   cd "..\campwind"

!ELSEIF  "$(CFG)" == "campwind - Win32 Debug"

"clogic - Win32 Debug" : 
   cd "\src\sf\wargamer\src\campLogic"
   $(MAKE) /$(MAKEFLAGS) /F .\clogic.mak CFG="clogic - Win32 Debug" 
   cd "..\campwind"

"clogic - Win32 DebugCLEAN" : 
   cd "\src\sf\wargamer\src\campLogic"
   $(MAKE) /$(MAKEFLAGS) /F .\clogic.mak CFG="clogic - Win32 Debug" RECURSE=1 CLEAN 
   cd "..\campwind"

!ELSEIF  "$(CFG)" == "campwind - Win32 Editor Debug"

!ELSEIF  "$(CFG)" == "campwind - Win32 Editor Release"

!ENDIF 

!IF  "$(CFG)" == "campwind - Win32 Release"

!ELSEIF  "$(CFG)" == "campwind - Win32 Debug"

!ELSEIF  "$(CFG)" == "campwind - Win32 Editor Debug"

"campedit - Win32 Editor Debug" : 
   cd "\src\sf\wargamer\src\campedit"
   $(MAKE) /$(MAKEFLAGS) /F .\campedit.mak CFG="campedit - Win32 Editor Debug" 
   cd "..\campwind"

"campedit - Win32 Editor DebugCLEAN" : 
   cd "\src\sf\wargamer\src\campedit"
   $(MAKE) /$(MAKEFLAGS) /F .\campedit.mak CFG="campedit - Win32 Editor Debug" RECURSE=1 CLEAN 
   cd "..\campwind"

!ELSEIF  "$(CFG)" == "campwind - Win32 Editor Release"

"campedit - Win32 Editor Release" : 
   cd "\src\sf\wargamer\src\campedit"
   $(MAKE) /$(MAKEFLAGS) /F .\campedit.mak CFG="campedit - Win32 Editor Release" 
   cd "..\campwind"

"campedit - Win32 Editor ReleaseCLEAN" : 
   cd "\src\sf\wargamer\src\campedit"
   $(MAKE) /$(MAKEFLAGS) /F .\campedit.mak CFG="campedit - Win32 Editor Release" RECURSE=1 CLEAN 
   cd "..\campwind"

!ENDIF 


!ENDIF 

