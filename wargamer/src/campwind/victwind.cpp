/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
 * Campaign Victory Results Window
 */


#include "stdinc.hpp"
#include "victwind.hpp"
#include "cbutton.hpp"
#include "palwind.hpp"
#include "app.hpp"
#include "wmisc.hpp"
#include "grtypes.hpp"
#include "scrnbase.hpp"
#include "scenario.hpp"
#include "dib.hpp"
#include "bmp.hpp"
#include "control.hpp"
#include "scn_res.h"
#include "imglib.hpp"
#include "colours.hpp"
#include "dib_blt.hpp"
#include "fonts.hpp"
#include "dc_hold.hpp"
#include "campdint.hpp"
#include "campside.hpp"
#include "victory.hpp"
#include "soundsys.hpp"
#include "resstr.hpp"
#include "simpstr.hpp"
#include "random.hpp"

#if 0    // All to redo

#include "campside.hpp"
#include "campdint.hpp"
#include "campinfo.hpp"
#include "myassert.hpp"
#include "app.hpp"
#include "mciwind.hpp"
#include "scenario.hpp"
#include "bmp.hpp"
#include "dib.hpp"
// #include "generic.hpp"
#include "wg_msg.hpp"
#include "palette.hpp"
#include "simpstr.hpp"
#include "winfile.hpp"
#include "fonts.hpp"
#include "wmisc.hpp"
#include "gamectrl.hpp"
#include "victint.hpp"



#include <vfw.h>

// #ifdef DEBUG
// #ifndef NO_CDDRIVE
// #define NO_CDDRIVE
// #endif
// #endif

/*
 * victint Implementation
 */

void VictoryRequest::run()
{
#if 0
   ASSERT(d_campData != 0);

   VictoryWindow::runVictoryWindow(d_campData);
#endif
}

#if 0

/*
 * Constants
 */

const int DBaseWidth = 640;     // base resolution for positioning controls
const int DBaseHeight = 430;

class AnimationWindow : public ScreenWindow {
    VictoryWindow* d_owner;
    CString d_fileName;
    HWND d_hMCI;                         // Handle to MCI Window
  public:
    AnimationWindow(VictoryWindow* owner, const char* fileName);

    void playAnimation();

  private:
    LRESULT procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
    BOOL onCreate(HWND hWnd, CREATESTRUCT* lpCreateStruct);
    void onMCINotifyMode(HWND hwnd, HWND hwndMCI, LONG mode);
    void onMove(HWND hwnd, int x, int y) {}
};

class StatisticWindow : public ScreenWindow {
  public:
    enum ID {
       ID_Start = 0,
       ShowMap = ID_Start,
       Quit,
       ID_HowMany
    };

  private:
    VictoryWindow* d_owner;
  public:
    StatisticWindow(VictoryWindow* owner);
    ~StatisticWindow();

  private:
    LRESULT procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
    BOOL onCreate(HWND hWnd, CREATESTRUCT* lpCreateStruct);
    void onPaint(HWND hwnd);
    void onCommand(HWND hwnd, int id, HWND hwndCtl, UINT codeNotify);
    void onDestroy(HWND hWnd);
    void onClose(HWND hwnd);
    void onSize(HWND hWnd, UINT state, int cx, int cy);
    void onMove(HWND hwnd, int x, int y) {}

    static ControlInfo StatisticWindow::getControlInfo(int i);

};

static const char VictoryWindow::s_className[] = "VictoryWindow";

static ATOM VictoryWindow::classAtom = 0;

ATOM VictoryWindow::registerClass()
{
   if(!classAtom)
   {
      WNDCLASS wc;

      wc.style = CS_HREDRAW | CS_VREDRAW;
      wc.lpfnWndProc = baseWindProc;
      wc.cbClsExtra = 0;
      wc.cbWndExtra = sizeof(VictoryWindow*);
      wc.hInstance = APP::instance();
      wc.hIcon = NULL;
      wc.hCursor = LoadCursor(NULL, IDC_ARROW);
      wc.hbrBackground = (HBRUSH) (COLOR_BACKGROUND + 1);
      wc.lpszMenuName = NULL;
      wc.lpszClassName = s_className;  // szMainClass;   // szAppName;
      classAtom = RegisterClass(&wc);
   }

   ASSERT(classAtom != 0);

   return classAtom;
}

VictoryWindow::VictoryWindow(HWND parent, CampaignData* campData) :
  d_campData(campData),
  d_animationWindow(0),
  d_statisticWindow(0)
{
  ASSERT(campData != 0);

  registerClass();


  int cx = GetSystemMetrics(SM_CXSCREEN);
  int cy = GetSystemMetrics(SM_CYSCREEN);

  HWND hWnd = createWindow(
      0,
      // getClassName(),
         NULL,
         WS_MAXIMIZE |
         WS_POPUP |
         WS_CLIPSIBLINGS,     /* Style */
      0,                      /* init. x pos */
      0,                      /* init. y pos */
      cx,                     /* init. x size */
      cy,                     /* init. y size */
      parent,              /* parent window */
      NULL
      // APP::instance()
   );


   ASSERT(hWnd != NULL);

   ShowWindow(hWnd, SW_SHOW);
   UpdateWindow(hWnd);
}

LRESULT VictoryWindow::procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{

   switch(msg)
   {
      HANDLE_MSG(hWnd, WM_CREATE,   onCreate);
//    HANDLE_MSG(hWnd, WM_DESTROY,  onDestroy);
      HANDLE_MSG(hWnd, WM_CLOSE, onClose);
//    HANDLE_MSG(hWnd, MCIWNDM_NOTIFYMODE, onMCINotifyMode);
//    HANDLE_MSG(hWnd, WM_SIZE, onSize);
//    HANDLE_MSG(hWnd, WM_PARENTNOTIFY, onParentNotify);
      HANDLE_MSG(hWnd, WM_COMMAND, onCommand);
//    HANDLE_MSG(hWnd, WM_MOVE, onMove);

      default:
        return defProc(hWnd, msg, wParam, lParam);
   }
}

BOOL VictoryWindow::onCreate(HWND hwnd, CREATESTRUCT* lpCreateStruct)
{
  /*
   * Create StatisticWindow
   */

  d_statisticWindow = new StatisticWindow(this);
  ASSERT(d_statisticWindow != 0);

  /*
   * Find end game .AVI files
   *
   *  1. Make sure we actually have a victor
   *  2. Get victors .AVI file
   *  3. If .AVI file == NoAVIFile bypass animation
   *  3. Insert CD path, scenario folder, & animation folder
   *  4. See if file exists on CD. If so create animation window, otherwise bypass.
   */

  const GameVictory& victory = d_campData->getGameVictory();
  ASSERT(victory.gameOver());

  const CampaignInfo& info = d_campData->getCampaignInfo();

  const char* aviFileName = info.getAVIFileName(victory.whoWon());
  ASSERT(aviFileName != 0);

  if(lstrcmp(aviFileName, CampaignInfo::NoAVIFileName) != 0)
  {
    SimpleString fullPath;

#if 0 // Modified by Steven to check local drive first, then CD
#ifdef NO_CDDRIVE    // since we don't actually have a cd
    fullPath = scenario->getAnimationFolderName();
    fullPath += "\\";
    fullPath += aviFileName;
#else
    SimpleString animationPath;
    animationPath = scenario->getAnimationFolderName();
    animationPath += "\\";
    animationPath += aviFileName;

    WinFileReader::makeCDFileName(fullPath, animationPath.toStr());
#endif
#else

    fullPath = scenario->getAnimationFolderName();
    fullPath += "\\";
    fullPath += aviFileName;

    if(!FileSystem::fileExists(fullPath.toStr()))
    {
      SimpleString animationPath;
      animationPath = scenario->getAnimationFolderName();
      animationPath += "\\";
      animationPath += aviFileName;

      WinFileReader::makeCDFileName(fullPath, animationPath.toStr());
    }

#endif

    if(FileSystem::fileExists(fullPath.toStr()))
    {
      d_animationWindow = new AnimationWindow(this, fullPath.toStr());
      ASSERT(d_animationWindow != 0);

      d_animationWindow->playAnimation();
    }
  }

  if(!d_animationWindow)
      displayStatistics();

  return TRUE;
}

void VictoryWindow::onClose(HWND hwnd)
{
//  DestroyWindow(hwnd);
}

void VictoryWindow::onCommand(HWND hwnd, int id, HWND hwndCtl, UINT codeNotify)
{
  switch(id)
  {
    case StatisticWindow::ShowMap:
      break;

    case StatisticWindow::Quit:
      onQuit();
      break;
  }
}

void VictoryWindow::killAnimation()
{
  ASSERT(d_animationWindow != 0);
  DestroyWindow(d_animationWindow->getHWND());
  d_animationWindow = 0;

  displayStatistics();
}

void VictoryWindow::displayStatistics()
{
  ASSERT(d_animationWindow == 0);
  ASSERT(d_statisticWindow != 0);

  ShowWindow(d_statisticWindow->getHWND(), SW_SHOW);
}

void VictoryWindow::onQuit()
{
  WargameMessage::postAbandon();
}

AnimationWindow::AnimationWindow(VictoryWindow* owner, const char* fileName) :
  d_fileName(copyString(fileName)),
  d_owner(owner)
{
  ASSERT(d_fileName != 0);
  ASSERT(d_owner != 0);

  RECT r;
  GetClientRect(d_owner->getHWND(), &r);

  HWND hWnd = createWindow(
      WS_EX_LEFT,
      // GenericClass::className(),
         NULL,
         WS_CHILD |
         WS_MAXIMIZE |
         WS_VISIBLE |
         WS_CLIPSIBLINGS,     /* Style */
      0,                      /* init. x pos */
      0,                      /* init. y pos */
      r.right,                   /* init. x size */
      r.bottom,                     /* init. y size */
      owner->getHWND(),             /* parent window */
      NULL
      // APP::instance()
   );

   ASSERT(hWnd != 0);
}

LRESULT AnimationWindow::procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
   switch(msg)
   {
      HANDLE_MSG(hWnd, WM_CREATE,   onCreate);
      HANDLE_MSG(hWnd, MCIWNDM_NOTIFYMODE, onMCINotifyMode);

      default:
        return defProc(hWnd, msg, wParam, lParam);
   }
}

BOOL AnimationWindow::onCreate(HWND hwnd, CREATESTRUCT* lpCreateStruct)
{
  ASSERT(d_fileName != 0);

  d_hMCI = MCIWindow::create(hwnd, d_fileName);

  return TRUE;
}


void AnimationWindow::onMCINotifyMode(HWND hwnd, HWND hwndMCI, LONG mode)
{
  if(mode == MCI_MODE_STOP)  // device stopped
  {
    ASSERT(d_hMCI != NULL);
    ASSERT(hwndMCI == d_hMCI);

    MCIWindow mciWnd(d_hMCI);
    mciWnd.kill();

    d_hMCI = 0;
    d_owner->killAnimation();
  }
}

void AnimationWindow::playAnimation()
{
  ASSERT(d_hMCI != 0);

  MCIWindow mciWnd(d_hMCI);
  mciWnd.play();
}

StatisticWindow::StatisticWindow(VictoryWindow* owner) :
  d_owner(owner)
{
  ASSERT(d_owner != 0);

  /*
   * Get Background dib
   */

  ASSERT(scenario->getStartScreenBMPFileName() != 0);
  d_bkDib = BMP::newDrawDIBDC(scenario->getStartScreenBMPFileName(), BMP::RBMP_Normal);
  ASSERT(d_bkDib != 0);

  RECT r;
  GetClientRect(d_owner->getHWND(), &r);

  HWND hWnd = createWindow(
      WS_EX_LEFT,
      // GenericClass::className(),
         NULL,
         WS_CHILD |
         WS_MAXIMIZE |
         WS_CLIPSIBLINGS,     /* Style */
      0,                      /* init. x pos */
      0,                      /* init. y pos */
      r.right,                   /* init. x size */
      r.bottom,                     /* init. y size */
      owner->getHWND(),             /* parent window */
      NULL
      // APP::instance()
   );

   ASSERT(hWnd != 0);
}

StatisticWindow::~StatisticWindow()
{
    selfDestruct();
  if(d_bkDib)
    delete d_bkDib;
}

LRESULT StatisticWindow::procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{

   switch(msg)
   {
      HANDLE_MSG(hWnd, WM_CREATE,   onCreate);
      HANDLE_MSG(hWnd, WM_DESTROY,  onDestroy);
      HANDLE_MSG(hWnd, WM_CLOSE, onClose);
      HANDLE_MSG(hWnd, WM_PAINT, onPaint);
      HANDLE_MSG(hWnd, WM_COMMAND, d_owner->onCommand);
      HANDLE_MSG(hWnd, WM_SIZE, onSize);

      default:
        return defProc(hWnd, msg, wParam, lParam);
   }
}

BOOL StatisticWindow::onCreate(HWND hwnd, CREATESTRUCT* lpCreateStruct)
{
  {
    HWND hButton = CreateWindow("BUTTON", "Show Map",
            WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON | WS_GROUP | WS_TABSTOP,
            0, 0, 0, 0,
            hwnd, (HMENU)ShowMap, APP::instance(), NULL);

    ASSERT(hButton != 0);
  }

  {
    HWND hButton = CreateWindow("BUTTON", "Quit",
            WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON | WS_GROUP | WS_TABSTOP,
            0, 0, 0, 0,
            hwnd, (HMENU)Quit, APP::instance(), NULL);

    ASSERT(hButton != 0);
  }


  return TRUE;
}

void StatisticWindow::onDestroy(HWND hwnd)
{
}

void StatisticWindow::onClose(HWND hwnd)
{
}

void StatisticWindow::onPaint(HWND hwnd)
{
  PAINTSTRUCT ps;
  HDC hdc = BeginPaint(hwnd, &ps);

  HPALETTE oldPal = SelectPalette(hdc, Palette::get(), FALSE);
  RealizePalette(hdc);

  RECT r;
  GetClientRect(hwnd, &r);

  ASSERT(d_bkDib != 0);
  StretchBlt(hdc, 0, 0, r.right, r.bottom,
        d_bkDib->getDC(), 0, 0, d_bkDib->getWidth(), d_bkDib->getHeight(),
        SRCCOPY);

  /*
   *  Temporarily display some statistics
   */

  const CampaignData* campData = d_owner->getCampaignData();
  ASSERT(campData != 0);

  int x = 200;
  int y = 200;

  FontManager fonts;
  fonts.setFont(hdc, 0, 20);


  for(int i = 0; i < scenario->getNumSides(); i++)
  {
    wTextOut(hdc, x, y, scenario->getSideName(i));
    y += 20;

    wTextPrintf(hdc, x, y, "Victory Points = %ld", campData->getVictoryLevel(i));
    y += 20;

    wTextPrintf(hdc, x, y, "Total Losses = %d", static_cast<int>(campData->getSideData().getSideLosses(i)));
    y += 20;

    wTextPrintf(hdc, x, y, "Infantry Losses = %d", static_cast<int>(campData->getSideData().getTypeLosses(i, BasicUnitType::Infantry)));
    y += 20;

    wTextPrintf(hdc, x, y, "Artillery Losses = %d", static_cast<int>(campData->getSideData().getTypeLosses(i, BasicUnitType::Artillery)));
    y += 20;

    wTextPrintf(hdc, x, y, "Cavalry Losses = %d", static_cast<int>(campData->getSideData().getTypeLosses(i, BasicUnitType::Cavalry)));
    y += 20;

    wTextPrintf(hdc, x, y, "Other Losses = %d", static_cast<int>(campData->getSideData().getTypeLosses(i, BasicUnitType::Special)));

    y = 200;
    x += 200;
  }


  SelectPalette(hdc, oldPal, FALSE);

  EndPaint(hwnd, &ps);
}

const int nStatisticButtons = 2;


static const ScreenWindow::ControlInfo statisticWindowControlInfo[StatisticWindow::ID_HowMany] = {
   { 0, 390, 117, 34 },     // Show Map Button
   { 0, 390, 117, 34 }      // Quit Button
};

static ScreenWindow::ControlInfo StatisticWindow::getControlInfo(int i)
{
  ASSERT(i < ID_HowMany);
  return statisticWindowControlInfo[i];
}

void StatisticWindow::onSize(HWND hwnd, UINT state, int cx, int cy)
{
  Size base(DBaseWidth, DBaseHeight);
  Size current(cx, cy);

  /*
   *  Adjust position of Buttons
   */

  {

    int offset = 0;
    for(int i = ID_Start; i < ID_HowMany; i++)
    {
      HWND hButton = GetDlgItem(hwnd, i);
      ASSERT(hButton != 0);

      ControlInfo info = getControlInfo(i);

      adjustPosition(base, current, info, ScreenWindow::NoAdjustX);

      /*
       * re-adjust x position
       */

      int space = (cx-(info.w*nStatisticButtons))/(nStatisticButtons+1);

      info.x = UWORD(space+((info.w+space)*offset));

      MoveWindow(hButton, info.x, info.y, info.w, info.h, TRUE);

      offset++;
    }
  }

}


static void VictoryWindow::runVictoryWindow(const CampaignData* campData)
{
  ASSERT(campData != 0);

  GameControl::pause(True);
  new VictoryWindow(APP::getMainHWND(), campData);
}
#endif

#endif      // Old animated version

class CampaignVictoryImp :
   public CampaignVictoryWindow,
   public PaletteWindow,
   public WindowBaseND
{
        enum ButtonID
        {
            ID_Continue,
        };

        enum
        {
            IconBorder = 8
        };

   public:
      CampaignVictoryImp(CampaignVictoryUser* user);
      ~CampaignVictoryImp();

      void init();


      HWND getHWND() const     { return WindowBaseND::getHWND(); }
      bool isVisible() const   { return WindowBaseND::isVisible(); }
      bool isEnabled() const   { return WindowBaseND::isEnabled(); }
      void show(bool visible)  { WindowBaseND::show(visible); }
      void enable(bool enable) { WindowBaseND::enable(enable); }

   private:
      LRESULT procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
      BOOL onCreate(HWND hwnd, CREATESTRUCT* lpCreateStruct);
      void onDestroy(HWND hwnd);
      void onSize(HWND hwnd, UINT state, int cx, int cy);
      void onPaint(HWND hwnd);
      BOOL onEraseBk(HWND hwnd, HDC hdc);
      void onCommand(HWND hwnd, int id, HWND hwndCtl, UINT codeNotify);

      CustomButton createButton(const char* text, ButtonID id);

      void makeDIB();

   private:
      CampaignVictoryUser* d_user;
      CustomButton d_continueButton;

      DrawDIBDC* d_dib;
      DIB* d_bkPic;
      ImageLibrary* d_dotImages;
      ImageLibrary* d_flagImages;

};


CampaignVictoryImp::CampaignVictoryImp(CampaignVictoryUser* user) :
   d_user(user),
   d_continueButton(0),
   d_dib(0),
   d_bkPic(0),
   d_dotImages(0),
   d_flagImages(0)

{
    const CampaignData* campData = d_user->getCampaignData();
    const GameVictory& results = campData->getGameVictory();

   d_bkPic = GameVictory::getPicture(results.whoWon());

   Side playerSide = GamePlayerControl::getSide(GamePlayerControl::Player);

   ImagePos* imagePos = reinterpret_cast<ImagePos*>(loadResource(scenario->getScenarioResDLL(), MAKEINTRESOURCE(PR_DOTMODEIMAGES), RT_IMAGEPOS));
   d_dotImages = scenario->getImageLibrary(MAKEINTRESOURCE(BM_DOTMODEIMAGES), DotMode_HowMany, imagePos);
   d_flagImages = scenario->getNationFlag(scenario->getGenericNation(playerSide), true);


   /*
   Play the appropriate CD music
   */

   int track_no = 0;

   /*
   French Won !
   */
   if(results.whoWon() == 0) {
      // Player Victory
      if(results.whoWon() == playerSide) {
         track_no = 9;
      }
      // Player Defeat
      else {
         track_no = 8;
      }
   }

   /*
   Allies Won !
   */
   else if(results.whoWon() == 1) {
      // Player Victory
      if(results.whoWon() == playerSide) {
         track_no = 7;
      }
      // Player Defeat
      else {
         track_no = 6;
      }
   }

   /*
   Nobody one - it was a draw !
   */
   else {
      track_no = 5;
   }

   ASSERT(track_no != 0);
   int num = 1;
   GlobalSoundSystemObj.StopCDPlaying();
   GlobalSoundSystemObj.SetPlayList(num, track_no+1);
   GlobalSoundSystemObj.StartPlayList(true);


}



CampaignVictoryImp::~CampaignVictoryImp()
{
   selfDestruct();
    delete d_dib;
    delete d_bkPic;
   delete d_dotImages;
}

void CampaignVictoryImp::init()
{
   HWND hParent = d_user->getHWND();

   CustomButton::initCustomButton(APP::instance());

   PixelRect r;
   GetClientRect(hParent, &r);

   CommonWindowBase::createWindow(
      0,                               // exStyle
      NULL,                            // WindowNAme
      WS_CHILD | WS_CLIPSIBLINGS,      // Style
      r.left(), r.top(), r.width(), r.height(),
      hParent,                         // parent
      NULL                            // Menu
   );

   makeDIB();

}

LRESULT CampaignVictoryImp::procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
    LRESULT result;

    if(!handlePalette(hWnd, msg, wParam, lParam, result))
    {
        switch(msg)
        {
            HANDLE_MSG(hWnd, WM_CREATE,     onCreate);
            HANDLE_MSG(hWnd, WM_PAINT,      onPaint);
            HANDLE_MSG(hWnd, WM_ERASEBKGND, onEraseBk);
            HANDLE_MSG(hWnd, WM_COMMAND,    onCommand);
            HANDLE_MSG(hWnd, WM_DESTROY,    onDestroy);
            HANDLE_MSG(hWnd, WM_SIZE,     onSize);

            default:
                return defProc(hWnd, msg, wParam, lParam);
        }
    }

    return result;
}

CustomButton CampaignVictoryImp::createButton(const char* text, ButtonID id)
{
    CustomButton cb = CreateWindow(
            CUSTOMBUTTONCLASS,
            text,
            WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON,
            ScreenBase::x(0),
            ScreenBase::y(0),
            ScreenBase::x(64),
            ScreenBase::y(12),
            getHWND(),
            HMENU(id),
            wndInstance(getHWND()),
            NULL);

    cb.setFillDib(scenario->getScenarioBkDIB(Scenario::BackPatterns::ButtonBackground));
    cb.setBorderColours(scenario->getBorderColors());

    return cb;
}

BOOL CampaignVictoryImp::onCreate(HWND hwnd, CREATESTRUCT* lpCreateStruct)
{
   d_continueButton = createButton(InGameText::get(IDS_Continue), ID_Continue);
   return TRUE;
}

void CampaignVictoryImp::onDestroy(HWND hwnd)
{
}

void CampaignVictoryImp::onSize(HWND hwnd, UINT state, int cx, int cy)
{
//    if(d_dib)
//    {
//        if( (cx != d_dib->getWidth()) || (cy != d_dib->getHeight()) )
   if(!d_dib || (cx != d_dib->getWidth()) || (cy != d_dib->getHeight()) )
   {
      makeDIB();
   }
//     }



   int border = (IconBorder * cx) / 1024;
   LONG x = cx - border;
   LONG y = cy - border;

   if (d_continueButton.getHWND())
   {
        PixelRect r;
        GetClientRect(d_continueButton.getHWND(), &r);

        PixelRect r1;
        r1.right(x);
        r1.bottom(y);
        r1.left(r1.right() - r.width());
        r1.top(r1.bottom() - r.height());

        d_continueButton.move(r1);

        // x = r1.left() - border;
   }

}

void CampaignVictoryImp::onPaint(HWND hwnd)
{
  PAINTSTRUCT ps;
  HDC hdc = BeginPaint(hwnd, &ps);

   ASSERT(d_dib);

  // HPALETTE oldPal = SelectPalette(hdc, Palette::get(), False);
  // RealizePalette(hdc);

   if(d_dib)
     BitBlt(hdc, 0, 0, d_dib->getWidth(), d_dib->getHeight(), d_dib->getDC(), 0, 0, SRCCOPY);

  // SelectPalette(hdc, oldPal, False);

  EndPaint(hwnd, &ps);
}

BOOL CampaignVictoryImp::onEraseBk(HWND hwnd, HDC hdc)
{
   return TRUE;
}

void CampaignVictoryImp::onCommand(HWND hwnd, int id, HWND hwndCtl, UINT codeNotify)
{
    switch(id)
    {
        case ID_Continue:
            d_user->endVictoryScreen();
            break;
    }
}

void CampaignVictoryImp::makeDIB()
{
    ASSERT(getHWND());

   const CampaignData* campData = d_user->getCampaignData();
   const CampaignSides& sideData = campData->getSideData();
    const GameVictory& results = campData->getGameVictory();

    PixelRect r;
    HRESULT success = GetClientRect(getHWND(), &r);
    ASSERT(success);

    if((r.width() == 0) || (r.height() == 0))
       return;

    if(d_dib)
        d_dib->resize(r.width(), r.height());
    else
        d_dib = new DrawDIBDC(r.width(), r.height());

    /*
     * Fill Background
     */

    Side playerSide = GamePlayerControl::getSide(GamePlayerControl::Player);
    Side otherSide = ::otherSide(playerSide);

    COLORREF playerColor = scenario->getSideColour(playerSide);
    COLORREF otherColor = scenario->getSideColour(otherSide);
    COLORREF Black = Colours::Black;


    ASSERT(d_bkPic != 0);

    DIB_Utility::stretchFit(d_dib, 0,0,d_dib->getWidth(),d_dib->getHeight(), d_bkPic);

    /*
     * Work out some font sizes and positions
     */

    class CNiceHeight
    {
            enum
            {
                VirtualHeight = 768     // Height that design was drawn at
            };

        public:
            CNiceHeight(int actualHeight) : d_height(actualHeight) { }
            int convert(int y)
            {
                return (y * d_height) / VirtualHeight;
            }

        private:
            int d_height;
    };

    CNiceHeight height(d_dib->getHeight());

    int titleHeight = maximum(8, height.convert(48));
    int headHeight = maximum(8, height.convert(32));
    int itemHeight = maximum(8, height.convert(24));

    LogFont lf;
    lf.height(titleHeight);
    lf.weight(FW_BOLD);
    lf.face(scenario->fontName(Font_Bold));
    lf.italic(false);
    lf.underline(false);

    Font titleFont;
    Font headFont;
    Font itemFont;
    Font totalFont;

    titleFont.set(lf);

    lf.height(headHeight);
    lf.weight(FW_NORMAL);
    lf.face(scenario->fontName(Font_Normal));
    headFont.set(lf);

    lf.height(itemHeight);
    itemFont.set(lf);

    lf.weight(FW_BOLD);
    totalFont.set(lf);

    DCFontHolder font(d_dib->getDC(), headFont);

    int centreX = d_dib->getWidth() / 2;

    /*
     * Draw Results
     */

    d_dib->setBkMode(TRANSPARENT);

    // Title

    int y = height.convert(32);

    char buffer[100];

    const char* str_CampaignResults = InGameText::get(IDS_CampaignResults);
    const char* str_VictoryPoints   = InGameText::get(IDS_VictoryPoints);
    const char* str_TotalLosses     = InGameText::get(IDS_TotalLosses);
    const char* str_InfantryLosses  = InGameText::get(IDS_InfantryLosses);
    const char* str_ArtilleryLosses = InGameText::get(IDS_ArtilleryLosses);
    const char* str_CavalryLosses   = InGameText::get(IDS_CavalryLosses);
    const char* str_OtherLosses     = InGameText::get(IDS_OtherLosses);
    const char* str_Losses          = InGameText::get(IDS_Losses);

    wsprintf(buffer, "%s %s",
        scenario->getSideName(playerSide),
        str_CampaignResults);

    font.set(titleFont);
    d_dib->setTextColor(playerColor);
    d_dib->setTextAlign(TA_TOP | TA_CENTER);
    wTextOut(d_dib->getDC(), centreX, y, buffer);

    // Draw Battle Icons either side
    int titleWidth = textWidth(d_dib->getDC(), buffer);
    int x = (d_dib->getWidth() - titleWidth) / 2;

    int xFlag1 = x/2;
    int xFlag2 = d_dib->getWidth() - x/2;
    int yBattle = y + titleHeight/2;
    d_dotImages->blit(d_dib, DotMode_Battle, xFlag1, yBattle);
    d_dotImages->blit(d_dib, DotMode_Battle, xFlag2, yBattle);

    y = height.convert(80);

    // Draw flags underneath

    int w;
    int h;
    d_flagImages->getImageSize(FI_Army, w, h);

    int xh;
    int yh;
    d_flagImages->getHotSpots(FI_Army, xh, yh);

    x = d_dib->getWidth() / 2 - w/2 + xh;    // centre adjusted for hotspot
    int x1 = x;
    y += yh;        // adjust Y to ignore hotspot

    d_flagImages->blit(d_dib, FI_Army, x, y);
    while(x > xFlag1)
    {
        x -= w + 1;
        x1 += w + 1;
        d_flagImages->blit(d_dib, FI_Army, x, y);
        d_flagImages->blit(d_dib, FI_Army, x1, y);
    }


    // Victory Style

    // We could display this using 2 tables, so result is always
    // relative to player
    //
    // e.g. French Minor Victory <==> Allied Minor Defeat


    y = height.convert(192);
    font.set(headFont);
    d_dib->setTextColor(Black);
    d_dib->setTextAlign(TA_TOP | TA_CENTER);

    // static const char* whyNames[] =
    // {
    //     "Unknown",
    //     "End of Day",
    //     "Surrender",
    //     "Defeated"
    // };

    static const InGameText::ID strVictory[] = {
        IDS_Draw,
        IDS_MinorVictory,
        IDS_MajorVictory,
        IDS_CrushingVictory
    };

    static const InGameText::ID strDefeat[] = {
        IDS_Draw,
        IDS_MinorDefeat,
        IDS_MajorDefeat,
        IDS_CrushingDefeat
    };

    SimpleString whyStr;     // = whyNames[why];

    const InGameText::ID* resultStrings = strVictory;
    if(results.whoWon() != playerSide)
      resultStrings = strDefeat;

    if(results.victoryLevel() != GameVictory::NoVictor)
    {
        // whyStr = scenario->getSideName(results.whoWon());
        whyStr = scenario->getSideName(playerSide);
        whyStr += " ";
    }
    whyStr += InGameText::get(resultStrings[results.victoryLevel()]);

    wTextOut(d_dib->getDC(), centreX, y, whyStr.toStr());

    /*
     * Player's losses
     */

    for(int column = 0; column < 2; ++column)
    {
        Side cSide;
        COLORREF cColor;
        int xLeft;

        const int xMargin = 32;

        if(column == 0)
        {
            cSide = playerSide;
            cColor = playerColor;
            xLeft = (d_dib->getWidth() * xMargin) / 1024;
        }
        else
        {
            cSide = otherSide;
            cColor = otherColor;
            xLeft = (d_dib->getWidth() * (512 + xMargin) ) / 1024;
        }


        y = height.convert(256);

        font.set(itemFont);

        /*
         * Details
         */

         y = height.convert(384);

         // Display <side> "losses"

         font.set(headFont);
         d_dib->setTextAlign(TA_TOP | TA_LEFT);
         d_dib->setTextColor(cColor);
         wTextPrintf(d_dib->getDC(), xLeft, y, "%s %s",
             (const char*) scenario->getSideName(cSide),
             (const char*) str_Losses);


         font.set(itemFont);
         d_dib->setTextColor(Black);

         int y = height.convert(416);
         int rowHeight = itemHeight + 2;


         wTextPrintf(d_dib->getDC(),
               xLeft, y,
               "%s = %ld",
               str_VictoryPoints,
               (long) campData->getVictoryLevel(cSide));
         y += rowHeight;

         wTextPrintf(d_dib->getDC(),
               xLeft, y,
               "%s = %d",
               str_TotalLosses,
               (int) sideData.getSideLosses(cSide));
         y += rowHeight;

         wTextPrintf(d_dib->getDC(),
               xLeft, y,
               "%s = %d",
               str_InfantryLosses,
               (int) sideData.getTypeLosses(cSide, BasicUnitType::Infantry));
         y += rowHeight;

         wTextPrintf(d_dib->getDC(),
               xLeft, y,
               "%s = %d",
               str_ArtilleryLosses,
               (int) sideData.getTypeLosses(cSide, BasicUnitType::Artillery));
         y += rowHeight;
         wTextPrintf(d_dib->getDC(),
               xLeft, y,
               "%s = %d",
               str_CavalryLosses,
               (int) sideData.getTypeLosses(cSide, BasicUnitType::Cavalry));
         y += rowHeight;
         wTextPrintf(d_dib->getDC(),
               xLeft, y,
               "%s = %d",
               str_OtherLosses,
               (int) sideData.getTypeLosses(cSide, BasicUnitType::Special));
         y += rowHeight;



    }
}



CampaignVictoryWindow* CampaignVictoryWindow::create(CampaignVictoryUser* user)
{
   CampaignVictoryImp* victWind = new CampaignVictoryImp(user);
   victWind->init();
   victWind->show(true);
   return victWind;
}


/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.3  2001/07/02 15:23:17  greenius
 * Battle, Campaign, Combined game and Campaign Editor now compile and run... mostly
 *
 * Revision 1.2  2001/06/18 08:25:45  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
