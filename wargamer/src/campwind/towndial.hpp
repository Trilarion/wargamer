/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
#ifndef TOWNDIAL_HPP
#define TOWNDIAL_HPP

//#include "grtypes.hpp"
#include "dialog.hpp"
#include "cdialog.hpp"
#include "gamedefs.hpp"

class CampaignUserInterface;
class CampaignData;
class PixelPoint;
class BuildItem;
class TownAttributes;
class DrawDIBDC;

class TownDialog : public ModelessDialog {
    CampaignUserInterface* d_owner;
    const CampaignData* d_campData;

    DrawDIBDC* d_dib;

    ITown d_town;

    RECT d_tdRect;

    CustomDialog d_customDialog;
  public:
    TownDialog(CampaignUserInterface* owner, const CampaignData* campData) :
      d_owner(owner),
      d_campData(campData),
      d_dib(0),
      d_town(NoTown)
    {
      SetRect(&d_tdRect, 0, 0, 0, 0);
    }

    virtual ~TownDialog() {}
    virtual BOOL procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam) = 0;

    virtual void show(const PixelPoint& p) = 0;
    virtual void hide() = 0;
    void destroy();
    void init(ITown town) { d_town = town; updateCaption(); }

    RECT& tabbedDialogRect() { return d_tdRect; }
    const CampaignData* campData() const { return d_campData; }
    CampaignUserInterface* owner() const { return d_owner; }

    ITown currentTown() const { return d_town; }
    CustomDialog& customDialog() { return d_customDialog; }

    DrawDIBDC* dib() const { return d_dib; }

    void allocateStaticDib(int cx, int cy);
    void deleteStaticDib();

    virtual BuildItem& getBuild(int index) = 0;
    virtual int getTotalBuilds() const = 0;

    virtual TownAttributes& getAttributes() = 0;
//    virtual void getResources(ResourceSet& totalRes, ResourceSet& usedRes) = 0;

    void updateCaption();

                         static TownDialog* create(CampaignUserInterface* owner, const CampaignData* campData);
};


#endif

/*
 *----------------------------------------------------------------------
 * $Log$
 *----------------------------------------------------------------------
 */
