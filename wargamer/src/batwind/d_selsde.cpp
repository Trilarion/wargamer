/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
#include "stdinc.hpp"
#include "d_selsde.hpp"
#include "fonts.hpp"
#include "cbutton.hpp"
#include "dplyuser.hpp"
#include "batinfo.hpp"
#include "scenario.hpp"
// #include "generic.hpp"
#include "app.hpp"
#include "dib.hpp"
#include "dib_blt.hpp"
#include "bmp.hpp"
#include "wmisc.hpp"
#include "bobutil.hpp"
#include "control.hpp"
#include "bargraph.hpp"
#include "resstr.hpp"



#define SELECT_BATTLEFIELD_BUTTON       1

SelectionSideWindow::SelectionSideWindow(SelectionSideUser* user, BattleData * batdata, const BattleInfo& batinfo) :
    CustomBorderWindow(scenario->getBorderColors()),
    d_owner(user),
   d_batData(batdata),
    d_parentHwnd(user->hwnd()),
    d_battlefieldRect(),
    d_dateRect(),
    d_weatherRect(),

   d_fullTerrainDescription(0),

   d_regionDIB(NULL),
   d_terrainDIB(NULL),
   d_elevationDIB(NULL),
   d_populationDIB(NULL),
   d_weatherDIB(NULL),

   d_regionText(0),
   d_terrainText(0),
   d_elevationText(0),
   d_populationText(0),

    d_OKButton(NULL),
    d_lpBattleInfo(batinfo),
    d_screenDIB(0)
{
    d_screenDIB = new DrawDIBDC(64,64);

   /*
   Get region DIB
   */
   if(batinfo.provincePicture()) {
      d_regionDIB = const_cast<DIB *>(batinfo.provincePicture() );
   }
   else d_regionDIB = NULL;

   d_regionText = const_cast<char *>(batinfo.provinceDescription() );
   if(!d_regionText) d_regionText = InGameText::get(IDS_UI_UNKNOWN);

   d_fullTerrainDescription = const_cast<char *>(batinfo.getTerrainTypeText() );

   /*
   Get terrain DIB
   */
   d_terrainText = const_cast<char *>(batinfo.getGroundTypeText() );

   switch(batinfo.terrain() ) {

      case BattleInfo::Open :
      case BattleInfo::Hilly :
      case BattleInfo::OpenWithMajorRiver :
      case BattleInfo::OpenWithMinorRiver : {

         d_terrainDIB = BMP::newDrawDIBDC(scenario->getScenarioResDLL(), "TerrainClear", BMP::RBMP_Normal);
         break;
      }

      case BattleInfo::Rough :
      case BattleInfo::MountainPass :
      case BattleInfo::Marsh :
      case BattleInfo::RoughWithMajorRiver :
      case BattleInfo::RoughWithMinorRiver : {

         d_terrainDIB = BMP::newDrawDIBDC(scenario->getScenarioResDLL(), "TerrainRough", BMP::RBMP_Normal);
         break;
      }

      case BattleInfo::Wooded :
      case BattleInfo::WoodedHilly :
      case BattleInfo::WoodedWithMajorRiver :
      case BattleInfo::WoodedWithMinorRiver : {

         d_terrainDIB = BMP::newDrawDIBDC(scenario->getScenarioResDLL(), "TerrainWoody", BMP::RBMP_Normal);
         break;
      }

      default : {
         FORCEASSERT("ERROR - bad terrain type enum when creating selection windows");
         d_terrainDIB = BMP::newDrawDIBDC(scenario->getScenarioResDLL(), "TerrainClear", BMP::RBMP_Normal);
         d_terrainText = "Clear";
         break;
      }
   }


   /*
   Get elevation DIB
   */
   switch(batinfo.heightType() ) {

      case HEIGHT_FLAT : {
         d_elevationDIB = BMP::newDrawDIBDC(scenario->getScenarioResDLL(), "HillLevel1", BMP::RBMP_Normal);
         d_elevationText = InGameText::get(IDS_Flat);
         break;
      }
      case HEIGHT_ROLLING : {
         d_elevationDIB = BMP::newDrawDIBDC(scenario->getScenarioResDLL(), "HillLevel2", BMP::RBMP_Normal);
         d_elevationText = InGameText::get(IDS_Rolling);
         break;
      }
      case HEIGHT_HILLY : {
         d_elevationDIB = BMP::newDrawDIBDC(scenario->getScenarioResDLL(), "HillLevel3", BMP::RBMP_Normal);
         d_elevationText = InGameText::get(IDS_Hilly);
         break;
      }
      case HEIGHT_STEEP : {
         d_elevationDIB = BMP::newDrawDIBDC(scenario->getScenarioResDLL(), "HillLevel4", BMP::RBMP_Normal);
         d_elevationText = InGameText::get(IDS_Steep);
         break;
      }
      default : {
         FORCEASSERT("ERROR - bad HeightType enum when creating selection windows");
         d_elevationDIB = BMP::newDrawDIBDC(scenario->getScenarioResDLL(), "HillLevel1", BMP::RBMP_Normal);
         d_elevationText = InGameText::get(IDS_Flat);
         break;
      }
   }

   /*
   Get population DIB
   */
   switch(batinfo.population() ) {

      case BattleInfo::Sparse : {
         d_populationDIB = BMP::newDrawDIBDC(scenario->getScenarioResDLL(), "PopLevel1", BMP::RBMP_Normal);
         d_populationText = InGameText::get(IDS_Sparse);
         break;
      }
      case BattleInfo::Moderate : {
         d_populationDIB = BMP::newDrawDIBDC(scenario->getScenarioResDLL(), "PopLevel2", BMP::RBMP_Normal);
         d_populationText = InGameText::get(IDS_Moderate);
         break;
      }
      case BattleInfo::High : {
         d_populationDIB = BMP::newDrawDIBDC(scenario->getScenarioResDLL(), "PopLevel3", BMP::RBMP_Normal);
         d_populationText = InGameText::get(IDS_High);
         break;
      }
      case BattleInfo::Dense : {
         d_populationDIB = BMP::newDrawDIBDC(scenario->getScenarioResDLL(), "PopLevel4", BMP::RBMP_Normal);
         d_populationText = InGameText::get(IDS_Dense);
         break;
      }
      default : {
         FORCEASSERT("ERROR - bad population enum when creating selection windows");
         d_populationDIB = BMP::newDrawDIBDC(scenario->getScenarioResDLL(), "PopLevel1", BMP::RBMP_Normal);
         d_populationText = InGameText::get(IDS_Sparse);
         break;
      }
   }

   /*
   Get date string
   */
    BattleMeasure::BattleTime batTime = d_batData->getDateAndTime();
    Greenius_System::Time theTime = batTime.time();
    Greenius_System::Date date = batTime.date();

    char buf[80];
    wsprintf(buf,
        "%s %d%s %d, %d:%02d",
         date.monthName(true),
         (int) date.day(),
            (const char*) getNths(date.day()),
         (int) date.year(),
            (int) theTime.hours(),
            (int) theTime.minutes()
   );
   d_dateText[0] = 0;
   strcpy(d_dateText, buf);

   /*
   Get weather DIB
   */
   switch(batinfo.weather() ) {

      case BattleInfo::SunnyDry : {
         d_weatherDIB = BMP::newDrawDIBDC(scenario->getScenarioResDLL(), "WeatherSunnyDry", BMP::RBMP_Normal);
         d_weatherText = InGameText::get(IDS_SunnyDry);
         break;
      }
      case BattleInfo::SunnyMuddy : {
         d_weatherDIB = BMP::newDrawDIBDC(scenario->getScenarioResDLL(), "WeatherSunnyMuddy", BMP::RBMP_Normal);
         d_weatherText = InGameText::get(IDS_SunnyMuddy);
         break;
      }
      case BattleInfo::RainyDry : {
         d_weatherDIB = BMP::newDrawDIBDC(scenario->getScenarioResDLL(), "WeatherRainyDry", BMP::RBMP_Normal);
         d_weatherText = InGameText::get(IDS_RainyDry);
         break;
      }
      case BattleInfo::RainyMuddy : {
         d_weatherDIB = BMP::newDrawDIBDC(scenario->getScenarioResDLL(), "WeatherRainyMuddy", BMP::RBMP_Normal);
         d_weatherText = InGameText::get(IDS_RainyMuddy);
         break;
      }
      case BattleInfo::StormyDry : {
         d_weatherDIB = BMP::newDrawDIBDC(scenario->getScenarioResDLL(), "WeatherStormyDry", BMP::RBMP_Normal);
         d_weatherText = InGameText::get(IDS_StormyDry);
         break;
      }
      case BattleInfo::StormyMuddy : {
         d_weatherDIB = BMP::newDrawDIBDC(scenario->getScenarioResDLL(), "WeatherStormyMuddy", BMP::RBMP_Normal);
         d_weatherText = InGameText::get(IDS_StormyMuddy);
         break;
      }
      default : {
         FORCEASSERT("ERROR - bad weather enum when creating slection windows");
         d_weatherDIB = BMP::newDrawDIBDC(scenario->getScenarioResDLL(), "WeatherSunnyDry", BMP::RBMP_Normal);
         d_weatherText = InGameText::get(IDS_SunnyDry);
         break;
      }
   }


    CreateWindows();
}

SelectionSideWindow::~SelectionSideWindow(void)
{
    if(d_OKButton)
    {
        DestroyWindow(d_OKButton); d_OKButton = 0;
    }

    selfDestruct();

    if(d_screenDIB) delete d_screenDIB;
   if(d_terrainDIB) delete d_terrainDIB;
   if(d_elevationDIB) delete d_elevationDIB;
   if(d_populationDIB) delete d_populationDIB;
   if(d_weatherDIB) delete d_weatherDIB;
}


void
SelectionSideWindow::CreateWindows(void) {

    createWindow(
        0,
        // GenericNoBackClass::className(),
        NULL,
        WS_CHILD,
        0,0,64,64,
        d_parentHwnd,
        NULL
        // APP::instance()
    );

    SetWindowText(getHWND(), "SelectionWindowSideBar");

    // init custom buttons class
    CustomButton::initCustomButton(APP::instance() );

    // select buttons filldib
    const DrawDIB* fillDib = scenario->getScenarioBkDIB(Scenario::BackPatterns::HScrollThumbBackground);

    // select buttons font
    LogFont lf;
    lf.height(14);
    lf.weight(FW_MEDIUM);
    lf.face(scenario->fontName(Font_Bold));
    Font font;
    font.set(lf);

    // OK button
    d_OKButton = CreateWindow(
        CUSTOMBUTTONCLASS,
        InGameText::get(IDS_SelectBattlefield),
        BS_PUSHBUTTON | WS_CHILD | WS_VISIBLE,
        0, 0, 64, 64,
        getHWND(),
        (HMENU) SELECT_BATTLEFIELD_BUTTON,
        APP::instance(),
        NULL);

    CustomButton cb(d_OKButton);
    cb.setFillDib(fillDib);
    cb.setBorderColours(scenario->getBorderColors());
    cb.setFont(font);

}



void
SelectionSideWindow::SetSize(RECT rect) {

   /*
   Main window rect
   */
    d_windowRect.left = rect.left;
    d_windowRect.top = rect.top;
    d_windowRect.right = rect.right - rect.left;
    d_windowRect.bottom = rect.bottom - rect.top;

   int hoff = d_windowRect.right / 16;
   int voff = d_windowRect.bottom / 32;

   /*
   Inside rect
   */
   d_insideRect.left = hoff;
   d_insideRect.top = 0;//voff;
   d_insideRect.right = d_windowRect.right - (hoff *2);
   d_insideRect.bottom = d_windowRect.bottom - (voff *2);

   /*
   Battlefield rect
   */

   int height = d_insideRect.bottom;
   int hdiv2 = height/2;
   int hdiv4 = height/4;
   int hdiv8 = height/8;

   d_battlefieldRect.top = d_insideRect.top;
   d_battlefieldRect.left = d_insideRect.left;
   d_battlefieldRect.right = d_insideRect.right;
   d_battlefieldRect.bottom = hdiv2;

   d_dateFrameRect.top = d_battlefieldRect.top + d_battlefieldRect.bottom;
   d_dateFrameRect.bottom = (hdiv8/3)*2;
   d_dateFrameRect.left = d_insideRect.left;
   d_dateFrameRect.right = d_insideRect.right;

   d_weatherFrameRect.top = d_dateFrameRect.top + d_dateFrameRect.bottom;
   d_weatherFrameRect.bottom = hdiv8 + (hdiv8/2);
   d_weatherFrameRect.left = d_insideRect.left;
   d_weatherFrameRect.right = d_insideRect.right;

   d_armiesFrameRect.top = d_weatherFrameRect.top + d_weatherFrameRect.bottom;
   d_armiesFrameRect.bottom = (d_insideRect.top + d_insideRect.bottom) - d_armiesFrameRect.top;
   d_armiesFrameRect.left = d_insideRect.left;
   d_armiesFrameRect.right = d_insideRect.right;


   // offset units
   int hoffset = d_battlefieldRect.right / 32;
   int voffset = d_battlefieldRect.bottom / 24;
   int picleft = d_battlefieldRect.left + hoffset;
   int picwidth = d_battlefieldRect.right - (hoffset*2);
   d_fontMinimumHeight = voffset;

   /*
   Battlefield title rect
   */
   d_battlefieldTitleRect.top = d_battlefieldRect.top;
   d_battlefieldTitleRect.left = d_battlefieldRect.left;
   d_battlefieldTitleRect.right = d_battlefieldRect.right;
   d_battlefieldTitleRect.bottom = voffset*2;

   /*
   Terrain type description rect
   */
   d_terrainDescriptionRect.top = d_battlefieldTitleRect.top + d_battlefieldTitleRect.bottom;
   d_terrainDescriptionRect.left = d_battlefieldRect.left;
   d_terrainDescriptionRect.right = d_battlefieldRect.right;
   d_terrainDescriptionRect.bottom = voffset*2;

   /*
   Region title rect
   */
   d_regionTitleRect.top = d_terrainDescriptionRect.top + d_terrainDescriptionRect.bottom;
   d_regionTitleRect.left = d_battlefieldRect.left;
   d_regionTitleRect.right = d_battlefieldRect.right;
   d_regionTitleRect.bottom = voffset;

   /*
   Region rect
   */
   d_regionRect.top = d_regionTitleRect.top + d_regionTitleRect.bottom;
   d_regionRect.left = picleft;
   d_regionRect.right = picwidth;
   d_regionRect.bottom = voffset*4;


   /*
   Terrain title rect
   */
   d_terrainTitleRect.top = d_regionRect.top + d_regionRect.bottom;
   d_terrainTitleRect.left = d_battlefieldRect.left;
   d_terrainTitleRect.right = d_battlefieldRect.right;
   d_terrainTitleRect.bottom = voffset;

   /*
   Terrain rect
   */
   d_terrainRect.top = d_terrainTitleRect.top + d_terrainTitleRect.bottom;
   d_terrainRect.left = picleft;
   d_terrainRect.right = picwidth;
   d_terrainRect.bottom = voffset*4;

   /*
   Elevation title rect
   */
   d_elevationTitleRect.top = d_terrainRect.top + d_terrainRect.bottom;
   d_elevationTitleRect.left = d_battlefieldRect.left;
   d_elevationTitleRect.right = d_battlefieldRect.right;
   d_elevationTitleRect.bottom = voffset;

   /*
   Elevation rect
   */
   d_elevationRect.top = d_elevationTitleRect.top + d_elevationTitleRect.bottom;
   d_elevationRect.left = picleft;
   d_elevationRect.right = picwidth;
   d_elevationRect.bottom = voffset*4;

   /*
   Population title rect
   */
   d_populationTitleRect.top = d_elevationRect.top + d_elevationRect.bottom;
   d_populationTitleRect.left = d_battlefieldRect.left;
   d_populationTitleRect.right = d_battlefieldRect.right;
   d_populationTitleRect.bottom = voffset;

   /*
   Population rect
   */
   d_populationRect.top = d_populationTitleRect.top + d_populationTitleRect.bottom;
   d_populationRect.left = picleft;
   d_populationRect.right = picwidth;
   d_populationRect.bottom = voffset*4;


   /*
   Date title rect
   */
   d_dateTitleRect.top = d_dateFrameRect.top;
   d_dateTitleRect.bottom = d_dateFrameRect.bottom/2;
   d_dateTitleRect.left = d_dateFrameRect.left;
   d_dateTitleRect.right = d_dateFrameRect.right;

   /*
   Date rect
   */
   d_dateRect.top = d_dateTitleRect.top + d_dateTitleRect.bottom;
   d_dateRect.bottom = d_dateFrameRect.bottom/2;
   d_dateRect.left = d_dateFrameRect.left;
   d_dateRect.right = d_dateFrameRect.right;


   /*
   Weather title rect
   */
   d_weatherTitleRect.top = d_weatherFrameRect.top;
   d_weatherTitleRect.bottom = d_weatherFrameRect.bottom/4;
   d_weatherTitleRect.left = d_weatherFrameRect.left;
   d_weatherTitleRect.right = d_weatherFrameRect.right;

   /*
   Weather text rect
   */
   d_weatherTextRect.top = d_weatherTitleRect.top + d_weatherTitleRect.bottom;
   d_weatherTextRect.bottom = d_weatherFrameRect.bottom/4;
   d_weatherTextRect.left = d_weatherFrameRect.left;
   d_weatherTextRect.right = d_weatherFrameRect.right;

   /*
   Weather rect
   */
   d_weatherRect.top = d_weatherTextRect.top + d_weatherTextRect.bottom;
   d_weatherRect.bottom = d_weatherFrameRect.bottom - (d_weatherTextRect.bottom + d_weatherTitleRect.bottom);
   d_weatherRect.left = picleft;
   d_weatherRect.right = picwidth;

   /*
   Armies title rect
   */
   d_armiesTitleRect.top = d_armiesFrameRect.top;
   d_armiesTitleRect.bottom = d_armiesFrameRect.bottom/3;
   d_armiesTitleRect.left = d_armiesFrameRect.left;
   d_armiesTitleRect.right = d_armiesFrameRect.right;

   /*
   Armies french text rect
   */
   d_armiesSide0TextRect.top = d_armiesTitleRect.top + d_armiesTitleRect.bottom;
   d_armiesSide0TextRect.bottom = d_armiesFrameRect.bottom/6;
   d_armiesSide0TextRect.left = d_armiesFrameRect.left;
   d_armiesSide0TextRect.right = d_armiesFrameRect.right;

   /*
   Armies french bar rect
   */
   d_armiesSide0BarRect.top = d_armiesSide0TextRect.top + d_armiesSide0TextRect.bottom;
   d_armiesSide0BarRect.bottom = d_armiesFrameRect.bottom/6;
   d_armiesSide0BarRect.left = d_armiesFrameRect.left;
   d_armiesSide0BarRect.right = d_armiesFrameRect.right;


   /*
   Armies allied text rect
   */
   d_armiesSide1TextRect.top = d_armiesSide0BarRect.top + d_armiesSide0BarRect.bottom;
   d_armiesSide1TextRect.bottom = d_armiesFrameRect.bottom/6;
   d_armiesSide1TextRect.left = d_armiesFrameRect.left;
   d_armiesSide1TextRect.right = d_armiesFrameRect.right;

   /*
   Armies allied bar rect
   */
   d_armiesSide1BarRect.top = d_armiesSide1TextRect.top + d_armiesSide1TextRect.bottom;
   d_armiesSide1BarRect.bottom = d_armiesFrameRect.bottom/6;
   d_armiesSide1BarRect.left = d_armiesFrameRect.left;
   d_armiesSide1BarRect.right = d_armiesFrameRect.right;




   /*
   OK button rect
   NOTE : this is only temporary - eventually it should replace the 'clock-window' below this side-bar
   */
   d_OKRect.top = d_armiesSide1BarRect.top + d_armiesSide1BarRect.bottom;
   d_OKRect.left = d_insideRect.left;
   d_OKRect.right = d_insideRect.right;
   d_OKRect.bottom = ((d_insideRect.top + d_insideRect.bottom) - d_OKRect.top) * 8;

   d_OKRect.bottom+=16;



    delete(d_screenDIB);
    d_screenDIB = new DrawDIBDC(d_windowRect.right, d_windowRect.bottom);
    DrawDib();

    MoveWindow(getHWND(), d_windowRect.left, d_windowRect.top, d_windowRect.right, d_windowRect.bottom, TRUE);

   MoveWindow(d_OKButton, d_OKRect.left, d_OKRect.top, d_OKRect.right, d_OKRect.bottom, TRUE);
    CustomButton cb(d_OKButton);
   InvalidateRect(d_OKButton, NULL, true);
   UpdateWindow(d_OKButton);
}



void
SelectionSideWindow::DrawDib(void) {

   HDC hdc = d_screenDIB->getDC();

   /*
   Setup colours & text mode
   */
   SetBkMode(hdc, TRANSPARENT);
   COLORREF colour = RGB(0,0,0);
   COLORREF oldColour = SetTextColor(hdc, colour);
   RECT textRect;
   Fonts font;

   /*
   Font for title captions
   */
   int minFontHeight;
   int fontHeight;
   int h;

   minFontHeight = 20;
   fontHeight = 25;
   fontHeight = (fontHeight * (d_fontMinimumHeight*2)) / 1024;
   h = maximum(fontHeight, minFontHeight) + 5;

   LogFont titleLogFont;
   titleLogFont.height(h);
// titleLogFont.weight(FW_HEAVY);
   titleLogFont.face(scenario->fontName(Font_Bold));
   titleLogFont.underline(true);


   /*
   Font for normal text
   */
   minFontHeight = 15;
   fontHeight = 20;
   fontHeight = (fontHeight * (d_fontMinimumHeight)) / 1024;
   h = maximum(fontHeight, minFontHeight) + 5;

   LogFont textLogFont;
   textLogFont.height(h);
// textLogFont.weight(FW_HEAVY);
   textLogFont.face(scenario->fontName(Font_Normal));
   textLogFont.underline(false);

   char text[1024];

    /*
    * fill background
    */
    const DrawDIB* fillDib = scenario->getScenarioBkDIB(Scenario::BackPatterns::CherryWoodBackground);
    ASSERT(fillDib);

    d_screenDIB->rect(0, 0, d_windowRect.right, d_windowRect.bottom, fillDib);
    d_screenDIB->setBkMode(TRANSPARENT);

#if 0
    /*
    Inside Rect
    */
    d_screenDIB->frame(
        d_insideRect.left,
        d_insideRect.top,
        d_insideRect.right,
        d_insideRect.bottom,
        0);

    /*
    Battlefield Rect
    */
    d_screenDIB->frame(
        d_battlefieldRect.left,
        d_battlefieldRect.top,
        d_battlefieldRect.right,
        d_battlefieldRect.bottom,
        0);

   /*
    Date Rect
    */
    d_screenDIB->frame(
        d_dateFrameRect.left,
        d_dateFrameRect.top,
        d_dateFrameRect.right,
        d_dateFrameRect.bottom,
        0);

   /*
    Weather Rect
    */
    d_screenDIB->frame(
        d_weatherFrameRect.left,
        d_weatherFrameRect.top,
        d_weatherFrameRect.right,
        d_weatherFrameRect.bottom,
        0);

   /*
    Weather Rect
    */
    d_screenDIB->frame(
        d_armiesFrameRect.left,
        d_armiesFrameRect.top,
        d_armiesFrameRect.right,
        d_armiesFrameRect.bottom,
        0);

#endif

   /*
   Battlefield title caption
   */
   font.set(hdc, titleLogFont);
   textRect.left = d_battlefieldTitleRect.left;
   textRect.right = d_battlefieldTitleRect.left + d_battlefieldTitleRect.right;
   textRect.top = d_battlefieldTitleRect.top;
   textRect.bottom = d_battlefieldTitleRect.top + d_battlefieldTitleRect.bottom;
   colour = RGB(148,115,33);
   SetTextColor(hdc, colour);
   const char* strBattlefield = InGameText::get(IDS_Battlefield);
   DrawText(hdc, strBattlefield, lstrlen(strBattlefield), &textRect, DT_CENTER | DT_SINGLELINE | DT_LEFT | DT_VCENTER);


   /*
   Terrain type description
   */
   font.set(hdc, textLogFont);
   textRect.left = d_terrainDescriptionRect.left;
   textRect.right = d_terrainDescriptionRect.left + d_terrainDescriptionRect.right;
   textRect.top = d_terrainDescriptionRect.top;
   textRect.bottom = d_terrainDescriptionRect.top + ((d_terrainDescriptionRect.bottom / 4) *3);
   colour = RGB(148,115,33);
   SetTextColor(hdc, colour);
   DrawText(hdc, d_fullTerrainDescription, lstrlen(d_fullTerrainDescription), &textRect, DT_CENTER | DT_SINGLELINE | DT_LEFT | DT_VCENTER);


   /*
   Region title
   */
   font.set(hdc, textLogFont);
   textRect.left = d_regionTitleRect.left;
   textRect.right = d_regionTitleRect.left + d_regionTitleRect.right;
   textRect.top = d_regionTitleRect.top;
   textRect.bottom = d_regionTitleRect.top + d_regionTitleRect.bottom;
   colour = RGB(148,115,33);
   SetTextColor(hdc, colour);
   strcpy(text, InGameText::get(IDS_Region));
   strcat(text, " :");
   if(d_regionText) strcat(text, d_regionText);
   DrawText(hdc, text, lstrlen(text), &textRect, DT_CENTER | DT_SINGLELINE | DT_LEFT | DT_VCENTER);

   /*
   Region graphic
   */
   if(d_regionDIB) {
      DIB_Utility::stretchDIB(
         d_screenDIB,
         d_regionRect.left,
         d_regionRect.top,
         d_regionRect.right,
         d_regionRect.bottom,
         d_regionDIB,
         0,0, d_regionDIB->getWidth(), d_regionDIB->getHeight()
      );
   }

   /*
    Region Frame
    */
    d_screenDIB->frame(d_regionRect.left, d_regionRect.top, d_regionRect.right, d_regionRect.bottom, d_screenDIB->getColour(RGB(148,115,33)));
   d_screenDIB->frame(d_regionRect.left+1, d_regionRect.top+1, d_regionRect.right-2, d_regionRect.bottom-2, d_screenDIB->getColour(RGB(255,255,255)));

   /*
   Terrain title
   */
   font.set(hdc, textLogFont);
   textRect.left = d_terrainTitleRect.left;
   textRect.right = d_terrainTitleRect.left + d_terrainTitleRect.right;
   textRect.top = d_terrainTitleRect.top;
   textRect.bottom = d_terrainTitleRect.top + d_terrainTitleRect.bottom;
   colour = RGB(148,115,33);
   SetTextColor(hdc, colour);
   strcpy(text, InGameText::get(IDS_Terrain));
   strcat(text, " :");
   if(d_terrainText) strcat(text, d_terrainText);
   DrawText(hdc, text, lstrlen(text), &textRect, DT_CENTER | DT_SINGLELINE | DT_LEFT | DT_VCENTER);

   /*
   Terrain graphic
   */
   if(d_terrainDIB) {
      DIB_Utility::stretchDIB(
         d_screenDIB,
         d_terrainRect.left,
         d_terrainRect.top,
         d_terrainRect.right,
         d_terrainRect.bottom,
         d_terrainDIB,
         0,0, d_terrainDIB->getWidth(), d_terrainDIB->getHeight()
      );
   }

   /*
    Terrain Frame
    */
    d_screenDIB->frame(d_terrainRect.left, d_terrainRect.top, d_terrainRect.right, d_terrainRect.bottom, d_screenDIB->getColour(RGB(148,115,33)));
   d_screenDIB->frame(d_terrainRect.left+1, d_terrainRect.top+1, d_terrainRect.right-2, d_terrainRect.bottom-2, d_screenDIB->getColour(RGB(255,255,255)));

   /*
   Elevation title
   */
   font.set(hdc, textLogFont);
   textRect.left = d_elevationTitleRect.left;
   textRect.right = d_elevationTitleRect.left + d_elevationTitleRect.right;
   textRect.top = d_elevationTitleRect.top;
   textRect.bottom = d_elevationTitleRect.top + d_elevationTitleRect.bottom;
   colour = RGB(148,115,33);
   SetTextColor(hdc, colour);
   strcpy(text, InGameText::get(IDS_Elevation));
   strcat(text, " :");
   if(d_elevationText) strcat(text, d_elevationText);
   DrawText(hdc, text, lstrlen(text), &textRect, DT_CENTER | DT_SINGLELINE | DT_LEFT | DT_VCENTER);

   /*
   Terrain graphic
   */
   if(d_elevationDIB) {
      DIB_Utility::stretchDIB(
         d_screenDIB,
         d_elevationRect.left,
         d_elevationRect.top,
         d_elevationRect.right,
         d_elevationRect.bottom,
         d_elevationDIB,
         0,0, d_elevationDIB->getWidth(), d_elevationDIB->getHeight()
      );
   }

   /*
    Elevation Frame
    */
    d_screenDIB->frame(d_elevationRect.left, d_elevationRect.top, d_elevationRect.right, d_elevationRect.bottom, d_screenDIB->getColour(RGB(148,115,33)));
   d_screenDIB->frame(d_elevationRect.left+1, d_elevationRect.top+1, d_elevationRect.right-2, d_elevationRect.bottom-2, d_screenDIB->getColour(RGB(255,255,255)));

   /*
   Population title
   */
   font.set(hdc, textLogFont);
   textRect.left = d_populationTitleRect.left;
   textRect.right = d_populationTitleRect.left + d_populationTitleRect.right;
   textRect.top = d_populationTitleRect.top;
   textRect.bottom = d_populationTitleRect.top + d_populationTitleRect.bottom;
   colour = RGB(148,115,33);
   SetTextColor(hdc, colour);
   strcpy(text, InGameText::get(IDS_Population));
   strcat(text, " :");
   if(d_populationText) strcat(text, d_populationText);
   DrawText(hdc, text, lstrlen(text), &textRect, DT_CENTER | DT_SINGLELINE | DT_LEFT | DT_VCENTER);

   /*
   Population graphic
   */
   if(d_populationDIB) {
      DIB_Utility::stretchDIB(
         d_screenDIB,
         d_populationRect.left,
         d_populationRect.top,
         d_populationRect.right,
         d_populationRect.bottom,
         d_populationDIB,
         0,0, d_populationDIB->getWidth(), d_populationDIB->getHeight()
      );
   }

   /*
    Population Frame
    */
    d_screenDIB->frame(d_populationRect.left, d_populationRect.top, d_populationRect.right, d_populationRect.bottom, d_screenDIB->getColour(RGB(148,115,33)));
   d_screenDIB->frame(d_populationRect.left+1, d_populationRect.top+1, d_populationRect.right-2, d_populationRect.bottom-2, d_screenDIB->getColour(RGB(255,255,255)));

   /*
   Date title caption
   */
   font.set(hdc, titleLogFont);
   textRect.left = d_dateTitleRect.left;
   textRect.right = d_dateTitleRect.left + d_dateTitleRect.right;
   textRect.top = d_dateTitleRect.top;
   textRect.bottom = d_dateTitleRect.top + d_dateTitleRect.bottom;
   colour = RGB(148,115,33);
   SetTextColor(hdc, colour);
   const char* strDate = InGameText::get(IDS_Date);
   DrawText(hdc, strDate, lstrlen(strDate), &textRect, DT_CENTER | DT_SINGLELINE | DT_LEFT | DT_VCENTER);

   /*
   Date text
   */
   font.set(hdc, textLogFont);
   textRect.left = d_dateRect.left;
   textRect.right = d_dateRect.left + d_dateRect.right;
   textRect.top = d_dateRect.top;
   textRect.bottom = d_dateRect.top + d_dateRect.bottom;
   colour = RGB(148,115,33);
   SetTextColor(hdc, colour);
   DrawText(hdc, d_dateText, lstrlen(d_dateText), &textRect, DT_CENTER | DT_SINGLELINE | DT_LEFT | DT_VCENTER);


   /*
   Weather title caption
   */
   font.set(hdc, titleLogFont);
   textRect.left = d_weatherTitleRect.left;
   textRect.right = d_weatherTitleRect.left + d_weatherTitleRect.right;
   textRect.top = d_weatherTitleRect.top;
   textRect.bottom = d_weatherTitleRect.top + d_weatherTitleRect.bottom;
   colour = RGB(148,115,33);
   SetTextColor(hdc, colour);
   SimpleString strWeather = InGameText::get(IDS_Weather);
   strWeather += " :";
   DrawText(hdc, strWeather.toStr(), lstrlen(strWeather.toStr()), &textRect, DT_CENTER | DT_SINGLELINE | DT_LEFT | DT_VCENTER);

   /*
   Weather text
   */
   font.set(hdc, textLogFont);
   textRect.left = d_weatherTextRect.left;
   textRect.right = d_weatherTextRect.left + d_weatherTextRect.right;
   textRect.top = d_weatherTextRect.top;
   textRect.bottom = d_weatherTextRect.top + d_weatherTextRect.bottom;
   colour = RGB(148,115,33);
   SetTextColor(hdc, colour);
   DrawText(hdc, d_weatherText, lstrlen(d_weatherText), &textRect, DT_CENTER | DT_SINGLELINE | DT_LEFT | DT_VCENTER);

   /*
   Weather graphic
   */
   if(d_weatherDIB) {
      DIB_Utility::stretchDIB(
         d_screenDIB,
         d_weatherRect.left,
         d_weatherRect.top,
         d_weatherRect.right,
         d_weatherRect.bottom,
         d_weatherDIB,
         0,0, d_weatherDIB->getWidth(), d_weatherDIB->getHeight()
      );
   }

   /*
   Armies title caption
   */
   font.set(hdc, titleLogFont);
   textRect.left = d_armiesTitleRect.left;
   textRect.right = d_armiesTitleRect.left + d_armiesTitleRect.right;
   textRect.top = d_armiesTitleRect.top;
   textRect.bottom = d_armiesTitleRect.top + d_armiesTitleRect.bottom;
   colour = RGB(148,115,33);
   SetTextColor(hdc, colour);
   SimpleString strArmies = InGameText::get(IDS_Armies);
   strArmies += " :";
   DrawText(hdc, strArmies.toStr(), lstrlen(strArmies.toStr()), &textRect, DT_CENTER | DT_SINGLELINE | DT_LEFT | DT_VCENTER);



   /*
   Armies side0 text
   */
   char * approx_char;
   Side s0 = 0;
   BobUtility::UnitCounts s0_unitCount;
   BobUtility::unitsInManpower(d_batData->ob()->getTop(s0), d_batData, s0_unitCount);
   int s0_total = s0_unitCount.d_nInf + s0_unitCount.d_nCav + s0_unitCount.d_nGuns + s0_unitCount.d_nArtMP;

   // if this isn't player's side then introduce some error
   if(GamePlayerControl::getControl(s0) != GamePlayerControl::Player) {
      approx_char = "~";
//    approx_char = "c.";
      int percent25 = (s0_total/100)*25;
      int half_percent = percent25/2;
      int modifier = random(0,percent25) - half_percent;
      s0_total += modifier;
      if(s0_total <= 0) s0_total += percent25/2;
   }
   else approx_char = "";

   sprintf( text, "%s : %s %i", scenario->getSideName(s0), approx_char, s0_total );

   // draw text
   font.set(hdc, textLogFont);
   textRect.left = d_armiesSide0TextRect.left;
   textRect.right = d_armiesSide0TextRect.left + d_armiesSide0TextRect.right;
   textRect.top = d_armiesSide0TextRect.top;
   textRect.bottom = d_armiesSide0TextRect.top + d_armiesSide0TextRect.bottom;
   colour = RGB(148,115,33);
   SetTextColor(hdc, colour);
   DrawText(hdc, text, lstrlen(text), &textRect, DT_CENTER | DT_SINGLELINE | DT_LEFT | DT_VCENTER);


   /*
   Armies side1 text
   */
   Side s1 = 1;
   BobUtility::UnitCounts s1_unitCount;
   BobUtility::unitsInManpower(d_batData->ob()->getTop(s1), d_batData, s1_unitCount);
   int s1_total = s1_unitCount.d_nInf + s1_unitCount.d_nCav + s1_unitCount.d_nGuns + s1_unitCount.d_nArtMP;

   // if this isn't player's side then introduce some error
   if(GamePlayerControl::getControl(s1) != GamePlayerControl::Player) {
      approx_char = "~";
//    approx_char = "c.";
      int percent25 = (s1_total/100)*25;
      int half_percent = percent25/2;
      int modifier = random(0,percent25) - half_percent;
      s1_total += modifier;
      if(s1_total <= 0) s1_total += percent25/2;
   }
   else approx_char = "";

   sprintf( text, "%s : %s %i", scenario->getSideName(s1), approx_char, s1_total );

   // draw text
   font.set(hdc, textLogFont);
   textRect.left = d_armiesSide1TextRect.left;
   textRect.right = d_armiesSide1TextRect.left + d_armiesSide1TextRect.right;
   textRect.top = d_armiesSide1TextRect.top;
   textRect.bottom = d_armiesSide1TextRect.top + d_armiesSide1TextRect.bottom;
   colour = RGB(148,115,33);
   SetTextColor(hdc, colour);
   DrawText(hdc, text, lstrlen(text), &textRect, DT_CENTER | DT_SINGLELINE | DT_LEFT | DT_VCENTER);


   /*
   Armies side0 bargraph
   */

   BarGraph::putBarGraph(
      hdc,
      d_armiesSide0BarRect.left,
      d_armiesSide0BarRect.top + (d_armiesSide0BarRect.bottom/4),
      s0_total,
      (maximum(s0_total,s1_total)),
      d_armiesSide0BarRect.right,
      d_armiesSide0BarRect.bottom/2,
      scenario->getSideColour(s0),
      false
   );


   /*
   Armies side0 bargraph
   */

   BarGraph::putBarGraph(
      hdc,
      d_armiesSide1BarRect.left,
      d_armiesSide1BarRect.top + (d_armiesSide1BarRect.bottom/4),
      s1_total,
      (maximum(s0_total,s1_total)),
      d_armiesSide1BarRect.right,
      d_armiesSide1BarRect.bottom/2,
      scenario->getSideColour(s1),
      false
   );




}


void
SelectionSideWindow::DrawTextLines(DrawDIBDC * dib, RECT device_rect, char ** text_lines, int num) {

    int lineheight = device_rect.bottom / num;
    RECT temp_rect;

    for(int f=0; f<num; f++) {

        char * text = text_lines[f];
        temp_rect.top = device_rect.top + (lineheight * f);
        temp_rect.bottom = temp_rect.top + lineheight;
        temp_rect.left = device_rect.left;
        temp_rect.right = device_rect.right;

        DPtoLP(dib->getDC(), (LPPOINT) &temp_rect, 2);

        wTextOut(dib->getDC(), temp_rect, text, DT_LEFT | DT_VCENTER | DT_WORDBREAK);
    }

}





/*
Message Processing
*/

LRESULT
SelectionSideWindow::procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam) {

    LRESULT l;
    if(handlePalette(hWnd, msg, wParam, lParam, l))
    return l;

    switch(msg) {
//        HANDLE_MSG(hWnd, WM_CREATE,    onCreate);
        HANDLE_MSG(hWnd, WM_COMMAND, onCommand);
//        HANDLE_MSG(hWnd, WM_NCHITTEST, onNCHitTest);
        HANDLE_MSG(hWnd, WM_ERASEBKGND, onEraseBk);
        HANDLE_MSG(hWnd, WM_PAINT, onPaint);
//        HANDLE_MSG(hWnd, WM_GETMINMAXINFO, onGetMinMaxInfo);
//        HANDLE_MSG(hWnd, WM_NCPAINT, onNCPaint);
//        HANDLE_MSG(hWnd, WM_HSCROLL, onHScroll);

        default : return defProc(hWnd, msg, wParam, lParam);
    }
}




void
SelectionSideWindow::onCommand(HWND hWnd, int id, HWND hwndCtl, UINT codeNotify) {

    switch (id) {

        case SELECT_BATTLEFIELD_BUTTON : {
            // d_lpWindowChangeUser->NextScreen();
            d_owner->onFinish();
            return;
        }

    }

}



void
SelectionSideWindow::onNCPaint(HWND hwnd, HRGN hrgn) {

    FORWARD_WM_NCPAINT(hwnd, hrgn, defProc);

    HDC hdc = GetWindowDC(hwnd);
    RECT r;
    GetWindowRect(hwnd, &r);
    OffsetRect(&r, -r.left, -r.top);

    HPALETTE oldPal = SelectPalette(hdc, Palette::get(), FALSE);
    RealizePalette(hdc);

    drawThickBorder(hdc, r);

    SelectPalette(hdc, oldPal, FALSE);

    ReleaseDC(hwnd, hdc);
}



BOOL
SelectionSideWindow::onEraseBk(HWND hwnd, HDC hdc) {

    return True;

}



void
SelectionSideWindow::onPaint(HWND hwnd) {

  PAINTSTRUCT ps;
  HDC hdc = BeginPaint(hwnd, &ps);

  HPALETTE oldPal = SelectPalette(hdc, Palette::get(), FALSE);
  RealizePalette(hdc);

  BitBlt(hdc, 0, 0, d_windowRect.right, d_windowRect.bottom, d_screenDIB->getDC(), 0, 0, SRCCOPY);

  SelectPalette(hdc, oldPal, FALSE);

  EndPaint(hwnd, &ps);

}







/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.2  2001/06/18 08:25:44  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
