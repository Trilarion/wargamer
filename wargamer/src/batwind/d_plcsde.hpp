/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef D_PLCSDE_HPP
#define D_PLCSDE_HPP

// include "batwind_dll.h"
#include "wind.hpp"
#include "palwind.hpp"
#include "batw_oob.hpp"

class PlacementSideUser : public BattleOBUserInterface
{
    public:
        virtual const BattleData* battleData() const = 0;
        virtual BattleData* battleData() = 0;
        virtual HWND hwnd() const = 0;
        virtual void onFinish() = 0;

};


class BW_ModeOwner;

class PlacementSideWindow :
    public WindowBaseND,
    public PaletteWindow,
    public CustomBorderWindow,
    public BattleOBTreeView 
{

public:

    // PlacementSideWindow(WindowChangeUser * window_user, BattleOBUserInterface * ob_user, HWND hparent, BattleData * batdata, Side side);
    PlacementSideWindow(PlacementSideUser* user, Side side);
    ~PlacementSideWindow();
    void SetSize(RECT rect);
    
private:

    void CreateWindows(void);
    void DestroyWindows(void);
    void DrawDib(void);


    LRESULT procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);

    void onCommand(HWND hWnd, int id, HWND hwndCtl, UINT codeNotify);
    void onNCPaint(HWND hwnd, HRGN hrgn);
    BOOL onEraseBk(HWND hwnd, HDC hdc);
    void onPaint(HWND hwnd);
    LRESULT onNotify(HWND hWnd, int id, NMHDR* lpNMHDR);
    
    private:

       // WindowChangeUser * m_lpWindowChangeUser;
       HWND m_ParentHwnd;

       RECT m_WindowRect;
       RECT m_TitleRect;
       RECT m_OBRect;
       RECT m_OKRect;


       HWND m_TreeViewHwnd;    
       HWND m_OKButton;

       Side m_PlayerSide;

       BattleData * m_lpBattleData;

       DrawDIBDC * m_Dib;

       PlacementSideUser* d_owner;


};






#endif

