/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996-1998, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *      Battle Game Windows
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "batwind.hpp"      // batwind
#include "batside.hpp"      // batwind: Side(Information) window
#include "bw_unit.hpp"      // batwind: Unit User Interface
#include "bw_tiny.hpp"      // batwind: Locator window
#include "bmsgwin.hpp"      // batwind: Message Window
#include "d_select.hpp"     // batwind: DeploySelectionWindows
#include "d_place.hpp"      // batwind: DeployPlacementWindows
#include "bw_mode.hpp"      // batwind: Deployment mode base class
#include "batmsg.hpp"       // batdata: Messages
#include "b_info.hpp"       // batwind: Information Tracking Window
#include "btwin_i.hpp"      // batdisp: Interface for batmap user
#include "batmap.hpp"       // batdisp: Battlefield display window
#include "batdata.hpp"      // batdata: Battle Data
#include "batctrl.hpp"
#include "scenario.hpp"     // gamesup: Scenario information
#include "scn_img.hpp"
#include "scrnbase.hpp"
#include "fonts.hpp"
#include "app.hpp"          // system
#include "palette.hpp"      // system: Palette manager
#include "wmisc.hpp"        // system: Miscellaneous Windows utilities
#include "gamectrl.hpp"     // system
#include "tooltip.hpp"      // system
#include "help.h"           // system
#include "bmp.hpp"          // system: BMP File utilities
#include "framewin.hpp"     // system
#include "cmenu.hpp"
#include "batres.h"         // res: Resources
#include "batres_s.h"       // res: Resource Strings
#include "wg_msg.hpp"
#include "options.hpp"
#include "btimctrl.hpp"
#include "btoolbar.hpp"
#include "b_info.hpp"
#include "batarmy.hpp"
#include "bstatwin.hpp"
#include "plyrdlg.hpp"
#include "control.hpp"
#include "b_result.hpp"
#include "GResultsWindow.hpp"
#include "dib.hpp"
#include "capstest.hpp"
#include "wg_msg.hpp"
#include "registry.hpp"
#include "gOptDial.hpp"
#include "options.hpp"
#include "alertbox.hpp"
#include "resstr.hpp"
#ifdef DEBUG
#include "randomKills.hpp"
#include "random.hpp"
#include "palwin.hpp"                   // system: debug window displaying colors
#include "clog.hpp"                             // system: Logfile
static LogFileFlush bwLog("batWind.log");
#endif
#include <ShellAPI.h>



#define SHOW_DISPLAY_TOGGLES


using namespace BattleWindows_Internal;

namespace { // private namespace

#ifdef DEBUG

enum DebugMenuItems
{
    IDM_DEBUG_END_DAY = IDM_BAT_FIRST + 1000,
    IDM_DEBUG_WIN_0,
    IDM_DEBUG_WIN_1,
    IDM_RANDOM_KILLS,

};


enum BattleMenuSections
{
    IDM_BAT_MENU_FILE,
    IDM_BAT_MENU_VIEW,
    IDM_BAT_MENU_SETTINGS,
    IDM_BAT_MENU_WINDOW,
#if defined(CUSTOMIZE)
    IDM_BAT_MENU_CUSTOMIZE,
#endif
#ifdef DEBUG
    IDM_BAT_MENU_DEBUG,
    IDM_BAT_MENU_HELP
#endif
};

#endif



};  // private namespace

/*
 * Implementation class
 */

class BattleData;

namespace WG_BattleWindows
{

class BWMode_PlayGame :
    public BW_Mode
{
    public:
        BWMode_PlayGame(BW_ModeOwner* owner);
      ~BWMode_PlayGame(void);

        virtual void positionWindows(const PixelRect& r, DeferWindowPosition* def);

        // Forward Interface functions onto ui

      virtual void onLButtonDown(const BattleMapSelect& info) { d_ui.onLButtonDown(info); }
      virtual void onLButtonUp(const BattleMapSelect& info) { d_ui.onLButtonUp(info); }
      virtual void onRButtonDown(const BattleMapSelect& info) { d_ui.onRButtonDown(info); }
      virtual void onRButtonUp(const BattleMapSelect& info) { d_ui.onRButtonUp(info); }
      virtual void onStartDrag(const BattleMapSelect& info) { d_ui.onStartDrag(info); }
      virtual void onEndDrag(const BattleMapSelect& info) { d_ui.onEndDrag(info); }
      virtual void onMove(const BattleMapSelect& info) { d_ui.onMove(info); }
      virtual bool setCursor(void) { return d_ui.setCursor(); }
        virtual bool hasWindow(BW_ID id);
        virtual void show(bool visible) { }

      virtual void mapZoomChanged(void) { d_ui.mapZoomChanged(); }
      virtual void timeChanged(void) {
         // tell UI about time change
         d_ui.timeChanged();
      }
     virtual BattleUnitUI * getUserInferface(void) { return &d_ui; }


    private:
        BattleUnitUI d_ui;
        static bool s_enabledWindows[];
      static const char s_regHexvisibility[];
      static const char s_registryName[];
};

const char BWMode_PlayGame::s_regHexvisibility[] = "HexVisibility";
const char BWMode_PlayGame::s_registryName[] = "BattleMap";

BWMode_PlayGame::BWMode_PlayGame(BW_ModeOwner* owner) :
    BW_Mode(owner),
    d_ui(owner, owner->battleData())
{

   /*
   * Start game centered & at 4 mile zoom distance
   */
   d_owner->battleData()->map()->removeAllHighlights();
   d_owner->mapWindow()->centerLocation();
   d_owner->setZoom(BattleMapInfo::FourMile);

   /*
   * Ensure FogOfWar is correctly setup, displayed & thread is running
   * or not
   */
   if(CampaignOptions::get(OPT_FogOfWar)) {
      d_owner->mapWindow()->startVisibilityChecks();
      d_owner->mapWindow()->getDisplay()->showHexVisibility(true);
   }
   else {
      d_owner->mapWindow()->stopVisibilityChecks();
      d_owner->mapWindow()->getDisplay()->showHexVisibility(false);
   }

   d_owner->mapWindow()->updateScrollBars();
   d_owner->mapWindow()->update(false);
   d_owner->tinyMap()->update(true);

}


BWMode_PlayGame::~BWMode_PlayGame(void) {

}


bool BWMode_PlayGame::s_enabledWindows[] =
{
    true,   // BW_VictoryLevel,
    true,   // BW_StatusWindow,
    true,   // BW_TrackingWindow,
    true,   // BW_MapWindow,
    true,   // BW_Locator,
    true,   // BW_MessageWindow
    true,   // BW_ToolBar
};

bool BWMode_PlayGame::hasWindow(BW_ID id)
{
    return s_enabledWindows[id];
}

void BWMode_PlayGame::positionWindows(const PixelRect& r, DeferWindowPosition* def)
{
    if(def)
        def->setPos(d_owner->mapWindow()->getHWND(), HWND_TOP, r.left(), r.top(), r.width(), r.height(), 0);
    else
        SetWindowPos(d_owner->mapWindow()->getHWND(), HWND_TOP, r.left(), r.top(), r.width(), r.height(), SWP_SHOWWINDOW);
}

/*
 * Wrap around the deployment window
 */

class BWMode_SelectMap : public DeploySelectionWindows
{
    public:
        BWMode_SelectMap(BW_ModeOwner* owner, const BattleInfo& batInfo);

       virtual bool hasWindow(BW_ID id);

       virtual void onFinish();

      virtual void mapZoomChanged(void) { }
      virtual void timeChanged(void) { }
     virtual BattleUnitUI * getUserInferface(void) { return NULL; }

    private:
        static bool s_enabledWindows[];
};


BWMode_SelectMap::BWMode_SelectMap(BW_ModeOwner* owner, const BattleInfo& batInfo) :
    DeploySelectionWindows(owner, batInfo)
{
   /*
   * Make sure scroll bars are correct
   */
   d_owner->mapWindow()->updateScrollBars();
   d_owner->tinyMap()->update(true);

}

bool BWMode_SelectMap::s_enabledWindows[] =
{
    false,  // BW_VictoryLevel,
    true,   // BW_StatusWindow,
    true,   // BW_TrackingWindow,
    true,   // BW_MapWindow,
    false,  // BW_Locator,
    false,  // BW_MessageWindow
    true,   // BW_ToolBar
};

bool BWMode_SelectMap::hasWindow(BW_ID id)
{
    return s_enabledWindows[id];
}


/*
 * Let owner know that screen is done
 */

void BWMode_SelectMap::onFinish()
{
    class MSG_SelectFinished :
        public WargameMessage::MessageBase,
        public BattleResultTypes
    {
        public:
            static void send(BW_ModeOwner* user)
            {
                WargameMessage::postMessage(new MSG_SelectFinished(user));
            }

        private:
            MSG_SelectFinished(BW_ModeOwner* user) :
                d_user(user)
            {
            }

            void run() { d_user->selectMapFinished(); }
            void clear() { delete this; }

        private:
            BW_ModeOwner* d_user;
    };

   SetPlayingArea();

    MSG_SelectFinished::send(d_owner);
}

class BWMode_Deploy : public DeployPlacementWindows
{
    public:
        BWMode_Deploy(BW_ModeOwner* owner, const BattleInfo& batInfo);

        virtual bool hasWindow(BW_ID id);
        virtual void onFinish();

      virtual void timeChanged(void) { }
      virtual void mapZoomChanged(void) { }
     virtual BattleUnitUI * getUserInferface(void) { return NULL; }

    private:
        static bool s_enabledWindows[];
};

BWMode_Deploy::BWMode_Deploy(BW_ModeOwner* owner, const BattleInfo& batInfo) :
    DeployPlacementWindows(owner, batInfo)
{

   /*
   * Make sure scroll bars are correct
   */
   d_owner->mapWindow()->updateScrollBars();
   d_owner->tinyMap()->update(true);

}

bool BWMode_Deploy::s_enabledWindows[] =
{
    false,  // BW_VictoryLevel,
    true,   // BW_StatusWindow,
    true,   // BW_TrackingWindow,
    true,   // BW_MapWindow,
    true,   // BW_Locator,
    false,  // BW_MessageWindow
    true,   // BW_ToolBar
};

bool BWMode_Deploy::hasWindow(BW_ID id)
{
    return s_enabledWindows[id];
}

/*
 * Let owner know that screen is done
 */

void BWMode_Deploy::onFinish()
{
    class MSG_DeployFinished :
        public WargameMessage::MessageBase,
        public BattleResultTypes
    {
        public:
            static void send(BW_ModeOwner* user)
            {
                WargameMessage::postMessage(new MSG_DeployFinished(user));
            }

        private:
            MSG_DeployFinished(BW_ModeOwner* user) :
                d_user(user)
            {
            }

            void run() { d_user->deployFinished(); }
            void clear() { delete this; }

        private:
            BW_ModeOwner* d_user;
    };

    MSG_DeployFinished::send(d_owner);
}

class BWMode_Results :
    public BW_Mode,
    public ResultWindowUser
{
    public:
        BWMode_Results(BW_ModeOwner* owner, BResultWindowUser* user);
        ~BWMode_Results();

        virtual void positionWindows(const PixelRect& r, DeferWindowPosition* def);
        virtual void show(bool visible);

      virtual void onLButtonDown(const BattleMapSelect& info) { }
      virtual void onLButtonUp(const BattleMapSelect& info) { }
      virtual void onRButtonDown(const BattleMapSelect& info) { }
      virtual void onRButtonUp(const BattleMapSelect& info) { }
      virtual void onStartDrag(const BattleMapSelect& info) { }
      virtual void onEndDrag(const BattleMapSelect& info) { }
      virtual void onMove(const BattleMapSelect& info) { }
      virtual bool setCursor(void) { return false; }
        virtual bool hasWindow(BW_ID id);

        virtual void onButton(BattleContinueMode mode);
        virtual const BattleResults* results() const;
        virtual HWND hwnd() const { return d_owner->hwnd(); }
        virtual bool willShowEnemyResults(Side s) const
        {
         if(d_resultUser->isHistorical())
            return true;
         else
            return ResultWindowUser::willShowEnemyResults(s);
        }

      virtual void timeChanged(void) { }
      virtual void mapZoomChanged(void) { }
     virtual BattleUnitUI * getUserInferface(void) { return NULL; }

    private:
        static bool s_enabledWindows[];

        BResultWindowUser* d_resultUser;
        CGenericResultsWindow* d_resultWindow;
};

BWMode_Results::BWMode_Results(BW_ModeOwner* owner, BResultWindowUser* user) :
    BW_Mode(owner),
    d_resultUser(user),
    d_resultWindow(0)      // created during positionWindows()
    // d_resultWindow(TResultWindow::create(this))
{
}

BWMode_Results::~BWMode_Results()
{
    delete d_resultWindow;
    d_resultWindow = 0;
    d_resultUser = 0;
}

class MSG_ResultFinished :
    public WargameMessage::MessageBase,
    public BattleResultTypes
{
    public:
        static void send(BResultWindowUser* user, BattleContinueMode mode)
        {
            WargameMessage::postMessage(new MSG_ResultFinished(user, mode));
        }

    private:
        MSG_ResultFinished(BResultWindowUser* user, BattleContinueMode mode) :
            d_user(user),
            d_mode(mode)
        {
        }

        void run() { d_user->resultsFinished(d_mode); }
        void clear() { delete this; }

    private:
        BResultWindowUser* d_user;
        BattleContinueMode d_mode;
};


void BWMode_Results::onButton(BattleContinueMode mode)
{
    ASSERT(d_resultWindow);
    ASSERT(d_resultUser);

    MSG_ResultFinished::send(d_resultUser, mode);

}

const BattleResults* BWMode_Results::results() const
{
    ASSERT(d_resultUser);
    return d_resultUser->battleResults();
}


bool BWMode_Results::s_enabledWindows[] =
{
    false,   // BW_VictoryLevel,
    false,   // BW_StatusWindow,
    false,   // BW_TrackingWindow,
    false,   // BW_MapWindow,
    false,   // BW_Locator,
    false,   // BW_MessageWindow
    true,   // BW_ToolBar
};

bool BWMode_Results::hasWindow(BW_ID id)
{
    return s_enabledWindows[id];
}

void BWMode_Results::positionWindows(const PixelRect& r, DeferWindowPosition* def)
{
    if(!d_resultWindow)
       d_resultWindow = CGenericResultsWindow::create(this);

    d_resultWindow->setPosition(r.left(), r.top(), r.width(), r.height(), def);
}

void BWMode_Results::show(bool visible)
{
    ASSERT(d_resultWindow);

    if (d_resultWindow)
    {
       if(visible)
          d_resultWindow->show();
       else
          d_resultWindow->hide();
    }
}


/*
 * Main Windows Implementation
 */

class BattleWindowsImp :
    virtual public BattleWindowsInterface,
    public PlayerSettingsContainer,
    public SubClassWindow,
    public PaletteWindow,
    public BW_ModeOwner,
    public OptionsOwner
{

    public:
        //--------------------
        // Don't allow copying

        BattleWindowsImp(const BattleWindowsImp&);
        BattleWindowsImp& operator =(const BattleWindowsImp&);

public:
        BattleWindowsImp(HWND hMain, BattleWindowUser* batGame);    // RCPBattleData battleData, BatTimeControl& tc);
        ~BattleWindowsImp();

        /*
         * BattleWindows Implementation
         */

        void setMode(BW_Mode* mode);

        void timeChanged();
                            // Battle time has changed, so update clock

        void redrawMap(bool all) { d_mapWind->update(all); }

        void addMessage(const BattleMessageInfo& msg);

        /*
         * Implementation of BattleWindowsInterface
         */

        HWND getHWND() const { return SubClassWindow::getHWND(); }
        virtual HWND hwnd() const { return getHWND(); }
                            // Get Window Handle

        virtual bool setMapLocation(const BattleLocation& l);
                            // Set centre of main map, return true if has changed

        virtual void mapAreaChanged(const BattleArea& area);
                            // MapWindow has changed what it is showing
        virtual void mapZoomChanged();
                            // Get mapwindow zoom buttons updated

        virtual void onLButtonDown(const BattleMapSelect& info);
                            // sent by mapwind when Button is pressed
        virtual void onLButtonUp(const BattleMapSelect& info);
                            // sent by mapwind when Button is released while not dragging
        virtual void onRButtonDown(const BattleMapSelect& info);
                            // sent by mapwind when Button is pressed
        virtual void onRButtonUp(const BattleMapSelect& info);
                            // sent by mapwind when Button is released while not dragging
        virtual void onStartDrag(const BattleMapSelect& info);
                            // sent by mapwind when Mouse is moved while button is held
        virtual void onEndDrag(const BattleMapSelect& info);
                            // sent by mapwind when button is released while dragging
        virtual void onMove(const BattleMapSelect& info);
                            // sent by mapwind when Mouse has moved
        virtual void positionWindows();
                            // Set the positions of all the child windows
        virtual void updateAll();
                            // Update all menu / toolbar items
        virtual bool updateTracking(const HexCord& hex);
        virtual bool setCursor(void);

        virtual const DrawDIBDC* mapDib() const
        {
                return (d_mapWind && d_mapWind->getDisplay()) ?
                    d_mapWind->getDisplay()->mainDIB() : 0;
        }

        virtual BattleMapDisplay* mapDisplay() const
        {
            return (d_mapWind) ? d_mapWind->getDisplay() : 0;
        }

        int menuHeight() const { return d_menu.height(); }

        void orderCP(CRefBattleCP cp);
        void messageWindowUpdated();

        /*
         * Mode Owner Functions
         */

        virtual const BattleData* battleData() const { return d_battleData; }
        virtual BattleData* battleData() { return d_battleData; }
        virtual BattleMapWind* mapWindow() { return d_mapWind; }
      virtual BW_Locator * tinyMap() { return d_locator; }

        virtual void selectMapFinished() { d_batGame->selectMapFinished(); }
        virtual void deployFinished() { d_batGame->deployFinished(); }
      virtual void setZoom(BattleMapInfo::Mode mode) { setMapMode(mode); }

    private:
        LRESULT procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);

        void createWindows();
        void destroyWindows();

        void onCommand(HWND hWnd, int id, HWND hwndCtl, UINT codeNotify);
        void onSize(HWND hwnd, UINT state, int cx, int cy);
        void onGetMinMaxInfo(HWND hWnd, LPMINMAXINFO lpMinMaxInfo);
        LRESULT onNotify(HWND hWnd, int id, NMHDR* lpNMHDR);
        bool onUserWindowDestroyed(HWND hwnd, HWND hChild, int idChild);
        void onMenuSelect(HWND hwnd, HMENU hmenu, int item, HMENU hmenuPopup, UINT flags);
        BOOL onHelp(HWND hwnd, LPHELPINFO lParam);

        bool hasWindow(BW_Mode::BW_ID id) const;
        void showWindow(Window* window, BW_Mode::BW_ID id);
        void showWindows();
        void setZoomButtons();
                            // Set up the zoom buttons and menu

        void setMapMode(BattleMapInfo::Mode mode);
        void startMapZoom();

        void checkMenu(int id, bool flag);
        void enableMenu(int id, bool flag);
        void showMenu();


        /*
         * PlayerSettingsUser implementation
         */

        void setController(Side side, GamePlayerControl::Controller control);

        // OptionsOwner
        void optionsChanged();

    private:
        static ATOM s_classAtom;

        PBattleData d_battleData;
        BattleWindowUser* d_batGame;

        // Child Windows

        B_ToolBar*                      d_toolBar;
        BattleVictory_Int*              d_bvWind;
        BGameStatus_Int*                d_gsWind;
        BattleTrackingWindow*           d_trackingWindow;
        BattleMapWind*                  d_mapWind;
        BW_Locator*                     d_locator;
        BattleMessageWindow*            d_msgWin;
        GameOptionsDialog*              d_optWind;
        CustomMenu d_menu;
#ifdef DEBUG
        PalWindow*                      d_palWind;
#endif
        BatTimeControl*                 d_timeControl;

        BW_Mode* d_mode;

      int d_victoryCalcCounter;
};

/*
 * Implementation functions
 */

static const char battleWindowRegName[] = "BattleWindow";
static const char positionRegName[] = "Position";


BattleWindowsImp::BattleWindowsImp(HWND hMain, BattleWindowUser* batGame) :
    SubClassWindow(hMain),
    d_battleData(batGame->battleData()),
    d_batGame(batGame),
    d_toolBar(0),
    d_bvWind(0),
    d_gsWind(0),
    d_trackingWindow(0),
    d_mapWind(0),
    d_locator(0),
    d_msgWin(0),
    d_optWind(0),
   d_victoryCalcCounter(100),
    d_menu(),
#ifdef DEBUG
    d_palWind(0),
#endif
    d_timeControl(batGame->timeControl()),
    d_mode(0)
{
        // Load a test palette

        CString palName = scenario->makeScenarioFileName("BattlePalette.bmp");
        BMP::getPalette(palName);

        /*
         * Alter class
         */

        // for now
        int result = SetMenu(hMain, NULL);
        ASSERT(result);

        /*
         * Set menu data
         */

        CustomMenuData md;
        md.d_type = CustomMenuData::CMT_BarMenu;
        md.d_menuID = MENU_BATTLE;
        md.d_fillDib = scenario->getScenarioBkDIB(Scenario::BackPatterns::CherryWoodBackground);
        md.d_checkImages = ScenarioImageLibrary::get(ScenarioImageLibrary::CustomMenuImages);
        md.d_hCommand = hMain;
        md.d_hParent = hMain;
        md.d_borderColors = scenario->getBorderColors();
        scenario->getColour("MenuText", md.d_color);
        scenario->getColour("MenuHilite", md.d_hColor);

        /*
         * Set menu fonts (one normal, the other underlined
         */

        LogFont lf;
        lf.height((8 * ScreenBase::dbY()) / ScreenBase::baseY());
        lf.weight(FW_MEDIUM);
        lf.face(scenario->fontName(Font_Bold));

        Font font;
        font.set(lf);
        md.d_hFont = font;

        lf.underline(true);
        font.set(lf);
        md.d_hULFont = font;

        /*
         * initialize menu
         */

        d_menu.init(md);


#ifdef DEBUG
        // Create extra menu items for finishing the game
      {
        MenuWindow* hMenu = d_menu.getSubMenu(0,0);
        MenuWindow* debugMenu = d_menu.getSubMenu(hMenu, IDM_BAT_MENU_DEBUG);
        int i = d_menu.getMenuItemCount(debugMenu);

        MenuItemInfo miSep;
        miSep.d_id = -1;
        miSep.d_state = MenuItemInfo::CMS_Seperator;
        miSep.d_subMenu = 0;
        miSep.d_text = "---";
        SetRect(&miSep.d_rect, 0,0,0,0);
        d_menu.insertMenuItem(debugMenu, i++, TRUE, &miSep);

        MenuItemInfo mi;
        mi.d_state = MenuItemInfo::CMS_Enabled;
        mi.d_subMenu = 0;
        SetRect(&mi.d_rect, 0,0,0,0);

        mi.d_id = IDM_DEBUG_END_DAY;
        mi.d_text = "Supper Time";
        d_menu.insertMenuItem(debugMenu, i++, TRUE, &mi);

        mi.d_id = IDM_DEBUG_WIN_0;
        mi.d_text = "French Win";
        d_menu.insertMenuItem(debugMenu, i++, TRUE, &mi);

        mi.d_id = IDM_DEBUG_WIN_1;
        mi.d_text = "Allied Win";
        d_menu.insertMenuItem(debugMenu, i++, TRUE, &mi);

        d_menu.insertMenuItem(debugMenu, i++, TRUE, &miSep);

        mi.d_id = IDM_RANDOM_KILLS;
        mi.d_text = "Earthquake";
        d_menu.insertMenuItem(debugMenu, i++, TRUE, &mi);
      }
#endif


#ifdef SHOW_DISPLAY_TOGGLES
#ifdef DEBUG

      /*
      Add items to view menu for toggling parts of the display on/off
      */
        MenuWindow* hMenu = d_menu.getSubMenu(0,0);
        MenuWindow* debugMenu = d_menu.getSubMenu(hMenu, 1);
        int i = d_menu.getMenuItemCount(debugMenu);

        MenuItemInfo miSep;
        miSep.d_id = -1;
        miSep.d_state = MenuItemInfo::CMS_Seperator;
        miSep.d_subMenu = 0;
        miSep.d_text = "---";
        SetRect(&miSep.d_rect, 0,0,0,0);
        d_menu.insertMenuItem(debugMenu, i++, TRUE, &miSep);

        MenuItemInfo mi;
        mi.d_state = MenuItemInfo::CMS_Enabled;
        mi.d_subMenu = 0;
        SetRect(&mi.d_rect, 0,0,0,0);

        mi.d_id = IDM_DRAW_DISPLAY_STATS;
        mi.d_text = "Display Stats";
        d_menu.insertMenuItem(debugMenu, i++, TRUE, &mi);

        mi.d_id = IDM_DRAW_TERRAIN;
        mi.d_text = "Draw Terrain";
        d_menu.insertMenuItem(debugMenu, i++, TRUE, &mi);

        mi.d_id = IDM_DRAW_OVERLAPS;
        mi.d_text = "Draw Overlaps";
        d_menu.insertMenuItem(debugMenu, i++, TRUE, &mi);

        mi.d_id = IDM_DRAW_PATHWAYS;
        mi.d_text = "Draw Pathways";
        d_menu.insertMenuItem(debugMenu, i++, TRUE, &mi);

        mi.d_id = IDM_DRAW_COASTS;
        mi.d_text = "Draw Coasts";
        d_menu.insertMenuItem(debugMenu, i++, TRUE, &mi);

        mi.d_id = IDM_DRAW_HILLS;
        mi.d_text = "Draw Hills";
        d_menu.insertMenuItem(debugMenu, i++, TRUE, &mi);

        mi.d_id = IDM_DRAW_EDGES;
        mi.d_text = "Draw Edges";
        d_menu.insertMenuItem(debugMenu, i++, TRUE, &mi);

        miSep.d_id = -1;
        miSep.d_state = MenuItemInfo::CMS_Seperator;
        miSep.d_subMenu = 0;
        miSep.d_text = "---";
        SetRect(&miSep.d_rect, 0,0,0,0);
        d_menu.insertMenuItem(debugMenu, i++, TRUE, &miSep);

        mi.d_id = IDM_DRAW_TROOPS;
        mi.d_text = "Draw Troop Sprites";
        d_menu.insertMenuItem(debugMenu, i++, TRUE, &mi);

        mi.d_id = IDM_DRAW_BUILDINGS;
        mi.d_text = "Draw Buildings Sprites";
        d_menu.insertMenuItem(debugMenu, i++, TRUE, &mi);

        mi.d_id = IDM_DRAW_EFFECTS;
        mi.d_text = "Draw Effects Sprites";
        d_menu.insertMenuItem(debugMenu, i++, TRUE, &mi);

#endif
#endif


        createWindows();
        positionWindows();

        ASSERT(hWnd != NULL);
        if(hWnd == NULL) return;


        /*
         * initialize caption/border subclass
         */

        SendMessage(hMain, WM_NCPAINT, 0, 0);

        UpdateWindow(hMain);

}

BattleWindowsImp::~BattleWindowsImp()
{
        destroyWindows();
}

void BattleWindowsImp::addMessage(const BattleMessageInfo& msg)
{
        if(d_msgWin)
        {
                d_msgWin->addMessage(msg);
        }
}



void BattleWindowsImp::setMode(BW_Mode* mode)
{
    delete d_mode;
    d_mode = mode;
    positionWindows();
    showWindows();
}

/*
 * Message Handler
 */

LRESULT BattleWindowsImp::procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
#ifdef DEBUG_MAINWIND_MESSAGES
                  debugLog("MainCampaignWindow::procMessage(%s)\n",
                                         getWMdescription(hWnd, msg, wParam, lParam));
#endif

                  LRESULT l;
                  if(handlePalette(hWnd, msg, wParam, lParam, l))
                                         return l;

                  switch(msg)
                  {
                                HANDLE_MSG(hWnd, WM_COMMAND,                    onCommand);
                                HANDLE_MSG(hWnd, WM_SIZE,                               onSize);
                                HANDLE_MSG(hWnd, WM_GETMINMAXINFO,      onGetMinMaxInfo);
                                HANDLE_MSG(hWnd, WM_NOTIFY,                     onNotify);
                                HANDLE_MSG(hWnd, WM_USER_WINDOWDESTROYED, onUserWindowDestroyed);
                                HANDLE_MSG(hWnd, WM_MENUSELECT,         onMenuSelect);
                                HANDLE_MSG(hWnd, WM_HELP,               onHelp);

                  default:
                                         return defProc(hWnd, msg, wParam, lParam);
                  }
}

void BattleWindowsImp::createWindows()
{
        HWND hWnd = getHWND();

        d_toolBar = new B_ToolBar(hWnd, this);
        ASSERT(d_toolBar);
        d_bvWind = new BattleVictory_Int(hWnd, this, d_battleData);
        ASSERT(d_bvWind);
        d_gsWind = new BGameStatus_Int(hWnd, this, d_battleData, *d_timeControl);
        ASSERT(d_gsWind);
        d_trackingWindow = new BattleTrackingWindow(hWnd, this, d_battleData);
        ASSERT(d_trackingWindow);
        d_locator = new BW_Locator(this, d_battleData);
        d_mapWind = new BattleMapWind(this, d_battleData);
        d_msgWin = BattleMessageWindow::make(this, d_battleData, hWnd);

        d_mapWind->create();

      checkMenu(IDM_BAT_TINYMAPWINDOW, d_locator->isVisible());
      checkMenu(IDM_BAT_TRACKINGWINDOW, d_trackingWindow->isVisible());
      checkMenu(IDM_BAT_MESSAGEWINDOW, d_msgWin->isVisible());


}

void BattleWindowsImp::destroyWindows()
{
        delete d_mode;
        d_mode = 0;

        delete d_toolBar;
        d_toolBar = 0;
        delete d_bvWind;
        d_bvWind = 0;
        delete d_gsWind;
        d_gsWind = 0;
        delete d_trackingWindow;
        d_trackingWindow = 0;

#ifdef DEBUG
        delete d_palWind;
        d_palWind = 0;
#endif

        delete d_mapWind;
        d_mapWind = 0;

        delete d_locator;
        d_locator = 0;

        delete d_msgWin;
        d_msgWin = 0;

        delete d_optWind;
        d_optWind = 0;
}


void BattleWindowsImp::onCommand(HWND hWnd, int id, HWND hwndCtl, UINT codeNotify)
{
#ifdef DEBUG
   bwLog.printf("onCommand(%d,%d)", (int) id, (int) codeNotify);
#endif

   switch(id)
   {
   case IDM_BAT_LOADGAME:
   case IDM_BAT_LOADWORLD:
      d_batGame->requestOpenBattle();
      break;

   case IDM_BAT_SAVEGAME:
   case IDM_BAT_SAVEWORLD:
      d_batGame->requestSaveBattle(true);
      break;

   case IDM_BAT_SURRENDER:
      {

//         Boolean doSurrender = False;
         GameControl::Pauser pause;    // pauses game until destructed

//          if(Options::get(OPT_AlertBox) && AlertBoxOptions::get(AB_ExitGame))
//          {
//             long val = alertBox(APP::getMainHWND(), InGameText::get(IDS_RUSureSurrenderBattle),
//                InGameText::get(IDS_SurrenderBattle), MB_OKCANCEL);
//
//             AlertBoxOptions::set(AB_ExitGame, (Boolean)HIWORD(val));
//             doSurrender = (LOWORD(val) == IDOK);
//          }
//          else doSurrender = True;

            int val = alertBox(APP::getMainHWND(),
               InGameText::get(IDS_RUSureSurrenderBattle),
                InGameText::get(IDS_SurrenderBattle),
                MB_OKCANCEL,
                AB_SurrenderBattle,
                IDOK);
//            bool doSurrender = (val == IDOK);


//         if(doSurrender)
         if(val == IDOK)
         {
#ifdef DEBUG
            debugLog("really surrendering battle\n");
#endif
            BattleFinish results;
            results.surrender(GamePlayerControl::getSide(GamePlayerControl::Player));
            d_batGame->requestBattleOver(results);
         }
         else
         {
#ifdef DEBUG
            debugLog("Not surrendering\n");
#endif
         }

         break;
      }



   case IDM_BAT_ABANDONGAME:
      {

//         Boolean doAbandon = False;
         Boolean oldPause = GameControl::pause(True);

//          if(Options::get(OPT_AlertBox) && AlertBoxOptions::get(AB_ExitGame))
//          {
//             long val = alertBox(APP::getMainHWND(), InGameText::get(IDS_SureAbandonGame),
//                InGameText::get(IDS_AbandonGame), MB_OKCANCEL);
//
//             AlertBoxOptions::set(AB_ExitGame, (Boolean)HIWORD(val));
//             doAbandon = (LOWORD(val) == IDOK);
//          }
//          else doAbandon = True;
//
//          if(doAbandon)

            int val = alertBox(APP::getMainHWND(),
                  InGameText::get(IDS_SureAbandonGame),
                  InGameText::get(IDS_AbandonGame),
                  MB_OKCANCEL,
                  AB_AbandonGame,
                  IDOK);

         if(val == IDOK)
         {


#ifdef DEBUG
            debugLog("really abandoning game\n");
#endif
            WargameMessage::postAbandon();
            // Deliberately leave the game paused
         }
         else
         {
            GameControl::pause(oldPause);
#ifdef DEBUG
            debugLog("Not abandoning\n");
#endif
         }

         break;
      }


   case IDM_BAT_EXIT:
      FORWARD_WM_CLOSE(hWnd, SendMessage);
      break;

   case IDM_BAT_ZOOMIN:
      startMapZoom();
      break;
   case IDM_BAT_ZOOMOVERVIEW:
      setMapMode(BattleMapInfo::Strategic);
      setZoomButtons();
      break;
   case IDM_BAT_ZOOMDETAIL:
      setMapMode(BattleMapInfo::OneMile);
      setZoomButtons();
      break;
   case IDM_BAT_ZOOMTWOMILE:
      setMapMode(BattleMapInfo::TwoMile);
      setZoomButtons();
      break;
   case IDM_BAT_ZOOMFOURMILE:
      setMapMode(BattleMapInfo::FourMile);
      setZoomButtons();
      break;



#ifdef SHOW_DISPLAY_TOGGLES

   case IDM_DRAW_DISPLAY_STATS :
      {
         HexCord size;
         HexCord bottomleft;

         d_battleData->getPlayingArea(&bottomleft, &size);
         HexCord newsize(size.x() + 16, size.y() + 16);
         d_battleData->expandPlayingArea(newsize);

         break;
      }

   case IDM_DRAW_TERRAIN :
      {
         d_mapWind->getDisplay()->drawTerrain(!d_mapWind->getDisplay()->isDrawingTerrain());
         break;
      }

   case IDM_DRAW_OVERLAPS :
      {
         d_mapWind->getDisplay()->drawOverlaps(!d_mapWind->getDisplay()->isDrawingOverlaps());
         break;
      }

   case IDM_DRAW_PATHWAYS :
      {
         d_mapWind->getDisplay()->drawPathways(!d_mapWind->getDisplay()->isDrawingPathways());
         break;
      }

   case IDM_DRAW_COASTS :
      {
         d_mapWind->getDisplay()->drawCoasts(!d_mapWind->getDisplay()->isDrawingCoasts());
         break;
      }

   case IDM_DRAW_HILLS :
      {
         d_mapWind->getDisplay()->drawHills(!d_mapWind->getDisplay()->isDrawingHills());
         break;
      }

   case IDM_DRAW_EDGES :
      {
         d_mapWind->getDisplay()->drawEdges(!d_mapWind->getDisplay()->isDrawingEdges());
         break;
      }

   case IDM_DRAW_TROOPS :
      {
         d_mapWind->getDisplay()->drawTroops(!d_mapWind->getDisplay()->isDrawingTroops());
         break;
      }

   case IDM_DRAW_BUILDINGS :
      {
         d_mapWind->getDisplay()->drawBuildings(!d_mapWind->getDisplay()->isDrawingBuildings());
         break;
      }

   case IDM_DRAW_EFFECTS :
      {

         char win_dir[MAX_PATH];
         GetWindowsDirectory(win_dir, MAX_PATH);

         SHELLEXECUTEINFO info;
         info.cbSize = sizeof(SHELLEXECUTEINFO);
         info.fMask = 0;
         info.hwnd = getHWND();
         info.lpVerb = "Open";
         info.lpFile = "SndVol32.exe";
         info.lpParameters = NULL;
         info.lpDirectory = win_dir;
         info.nShow = SW_SHOW;

         BOOL retval = ShellExecuteEx(&info);

         break;
      }

#endif



#ifdef DEBUG
   case IDM_BAT_INSTANTMOVE:
      Options::toggle(OPT_InstantMove);
      checkMenu(IDM_BAT_INSTANTMOVE, Options::get(OPT_InstantMove));
      break;
#endif
#if !defined(EDITOR)
   case IDM_BAT_INSTANTORDERS:
      CampaignOptions::toggle(OPT_InstantOrders);
      checkMenu(IDM_BAT_INSTANTORDERS, CampaignOptions::get(OPT_InstantOrders));
      break;
#endif
   case IDM_BAT_HEXOUTLINE:
      d_mapWind->toggleHexOutline();
      checkMenu(IDM_BAT_HEXOUTLINE, d_mapWind->hexOutline());
      break;
#ifdef DEBUG
   case IDM_BAT_HEXDEBUG:
      {

         // I'm going to use this menu command to try out these GDI caps functions
         DrawDIBDC * dib = d_mapWind->getDisplay()->staticDIB();
         CapsTest capstester(dib->getDC(), &(dib->getBitmapInfo()->bmiHeader));
         capstester.DebugOut();

         d_mapWind->toggleHexDebug();
         checkMenu(IDM_BAT_HEXDEBUG, d_mapWind->hexDebug());
         break;
      }
#endif

   case IDM_BAT_PLAYERSETTINGS:
      showPlayerSettings();
      break;

   case IDM_BAT_GAMEOPTIONS:
      {
         if(d_optWind)
         {
            delete d_optWind;
            d_optWind = 0;
            checkMenu(IDM_BAT_GAMEOPTIONS, false);
         }
         else
         {
            d_optWind = GameOptionsDialog::create(this, GameOptionsPage);
            checkMenu(IDM_BAT_GAMEOPTIONS, true);
         }
         break;
      }

   case IDM_BAT_TINYMAPWINDOW:
      d_locator->toggle();
      checkMenu(id, d_locator->isVisible());
      break;

   case IDM_BAT_TRACKINGWINDOW:
      d_trackingWindow->toggle();
      checkMenu(id, d_trackingWindow->isVisible());
      break;
#ifdef DEBUG
   case IDM_BAT_PALETTEWINDOW:
      if(d_palWind == 0)
      {
         d_palWind = new PalWindow(getHWND());
         ASSERT(d_palWind != 0);
      }
      else
      {
         delete d_palWind;
         d_palWind = 0;
      }
      checkMenu(IDM_BAT_PALETTEWINDOW, d_palWind != 0);
      break;
#endif
   case IDM_BAT_MESSAGEWINDOW:
      d_msgWin->toggle();
      checkMenu(id, d_msgWin->isVisible());
      break;

   case IDM_BAT_CLOCKWINDOW:

   case IDM_BAT_EDIT_OB:
   case IDM_BAT_EDIT_DATE:
   case IDM_BAT_EDITINFO:
   case IDM_BAT_EDITCONDITIONS:
   case IDM_BAT_NEW_OBJECT:
   case IDM_BAT_EDIT_NOTHING:

   case IDM_BAT_OB:
   case IDM_BAT_FINDLEADER:

   case IDM_BAT_DEBUG:

   case IDM_BAT_HELP_CONTENTS:
   case IDM_BAT_HELP_HISTORY:
   case IDM_BAT_ABOUT:
      break;



#ifdef DEBUG

   case IDM_DEBUG_END_DAY:
      {
         BattleFinish results;
         results.endDay();
         d_batGame->requestBattleOver(results);
         break;
      }

   case IDM_DEBUG_WIN_0:
      {
         BattleFinish results;
         results.defeated(0, BattleFinish::Morale);
         d_batGame->requestBattleOver(results);
         break;
      }

   case IDM_DEBUG_WIN_1:
      {
         BattleFinish results;
         results.defeated(1, BattleFinish::Determination);
         d_batGame->requestBattleOver(results);
         break;
      }

   case IDM_RANDOM_KILLS:
      {
         GameControl::Pauser pause;    // pauses game until destructed

         B_Logic::randomKills(d_batGame->battleData());
         d_mapWind->update(false);

         PixelRect r;
         GetWindowRect(d_mapWind->getHWND(), &r);
         POINT p;
         p.x = r.left();
         p.y = r.top();
         ScreenToClient(hWnd, &p);
         for(int i = 0; i < 100; ++i)
         {
            int x = p.x + random(-10, +10);
            int y = p.y + random(-10, +10);

            MoveWindow(d_mapWind->getHWND(), x, y, r.width(), r.height(), TRUE);

            MSG msg;
            while(PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
            {
               TranslateMessage(&msg);
               DispatchMessage(&msg);
            }

            Sleep(20);
         }

         MoveWindow(d_mapWind->getHWND(), p.x, p.y, r.width(), r.height(), TRUE);

      }

   default:
      bwLog.printf("Unrecognised Command");
#endif
   }
}

void BattleWindowsImp::onSize(HWND hwnd, UINT state, int cx, int cy)
{
   if((cx == 0) || (cy == 0) || (state == SIZE_MINIMIZED))
      return;


        d_menu.run(PixelPoint(0,0));
        positionWindows();

        // Forward on so wg_main can deal with the Caption Bar

        FORWARD_WM_SIZE(hwnd, state, cx, cy, defProc);
}

void BattleWindowsImp::onGetMinMaxInfo(HWND hWnd, LPMINMAXINFO lpMinMaxInfo)
{
        lpMinMaxInfo->ptMinTrackSize.x = 640;
                  lpMinMaxInfo->ptMinTrackSize.y = 480;
}

LRESULT BattleWindowsImp::onNotify(HWND hWnd, int id, NMHDR* lpNMHDR)
{
                  switch(lpNMHDR->code)
                  {
                                         case TTN_NEEDTEXT:                              // Tool Tip needed
                                                                {
                                                                                  TOOLTIPTEXT* ttt = (TOOLTIPTEXT*) lpNMHDR;

                                                                                  // Set up default values if menuInfo fails

                                                                                  ttt->lpszText = MAKEINTRESOURCE(id);
                                                                                  ttt->hinst = APP::instance();

                                                                                  // Get the actual Menu Text

                                                                                  HMENU hMenu = GetMenu(hWnd);
                                                                                  if(hMenu != NULL)
                                                                                  {
                                                                                                         MENUITEMINFO mInfo;
                                                                                                         static TCHAR buffer[256];
                                        mInfo.cbSize = sizeof(MENUITEMINFO);
                                        mInfo.fMask = MIIM_TYPE;
                                        mInfo.dwTypeData = buffer;
                                                                                                         mInfo.cch = 256;
                                                                                                         if(GetMenuItemInfo(hMenu, id, FALSE, &mInfo))
                                                                                                         {
                                                if(mInfo.fType == MFT_STRING)
                                                                                                                                {
                                                        ttt->lpszText = buffer;
                                                        ttt->hinst = NULL;
                                                                                                                                }
                                        }
                                }

                        }
                                                                break;

                                         case TTN_POP:                           // Tool Tip Gone
                        break;
                  }

                  return 0;
}

bool BattleWindowsImp::onUserWindowDestroyed(HWND hwnd, HWND hChild, int idChild)
{
#ifdef DEBUG
    if(d_palWind && (hChild == d_palWind->getHWND()))
    {
        d_palWind = 0;
        checkMenu(IDM_BAT_PALETTEWINDOW, false);
        return true;
    }
#endif

    if(d_msgWin && (hChild == d_msgWin->getHWND()))
    {
        checkMenu(IDM_BAT_MESSAGEWINDOW, false);
        return false;
    }


    if(d_optWind && (hChild == d_optWind->getHWND()))
    {
        d_optWind = 0;
        checkMenu(IDM_BAT_GAMEOPTIONS, false);
        return true;
    }

    return false;
}

bool BattleWindowsImp::updateTracking(const HexCord& hex)
{
   return true;
}

void BattleWindowsImp::onMenuSelect(HWND hwnd, HMENU hmenu, int item, HMENU hmenuPopup, UINT flags)
{
                  if((hmenuPopup == NULL) && ( ((UWORD)flags) == 0xffff))
                                         g_toolTip.clearHint();
                  else
                                         g_toolTip.showHint(APP::instance(), item);
}

BOOL BattleWindowsImp::onHelp(HWND hwnd, LPHELPINFO lparam)
{
  LPHELPINFO lphi = reinterpret_cast<LPHELPINFO>(lparam);

  if (lphi->iContextType == HELPINFO_WINDOW)   // must be for a control
  {
//                        WinHelp ((HWND)lphi->hItemHandle, "help\\Wg95Help.hlp", HELP_CONTEXT, IDH_Wargamer);
  }
  return TRUE;
}


void BattleWindowsImp::positionWindows()
{
    if(getHWND())
    {
        const int nWindows = 10;
        showMenu();

        {
            DeferWindowPosition dwp(nWindows);

            RECT rMain;
            GetClientRect(getHWND(), &rMain);

         // Return if we're Minimized
         if(rMain.right <=0 || rMain.bottom <= 0) return;

            // Adjust for Custom Menu
            rMain.top += d_menu.height();

            // Toolbar
            if(d_toolBar && hasWindow(BW_Mode::BW_ToolBar))
            {
                PixelPoint p(rMain.left, rMain.top);
                d_toolBar->position(p);
                rMain.top += d_toolBar->height();
            }

            // victory window
            if(d_bvWind && hasWindow(BW_Mode::BW_VictoryLevel))
            {
                PixelPoint p(rMain.left, rMain.top);
                d_bvWind->position(p);
                rMain.top += d_bvWind->height();
            }

            // status window
            if(d_gsWind && hasWindow(BW_Mode::BW_StatusWindow))
            {
                PixelPoint p(rMain.left, rMain.bottom - d_gsWind->height());
                d_gsWind->position(p);
                rMain.bottom -= d_gsWind->height();
            }

            if(d_mode)
                d_mode->positionWindows(rMain, &dwp);


        }       // dwp gets destructed and EndDeferPosition is called

        show(true);
    }
}

bool BattleWindowsImp::hasWindow(BW_Mode::BW_ID id) const
{
    return d_mode && d_mode->hasWindow(id);
}

void BattleWindowsImp::showWindow(Window* window, BW_Mode::BW_ID id)
{
    if(window)
        window->show(hasWindow(id));
}

void BattleWindowsImp::showWindows()
{
    if(d_locator)
        d_locator->reset();
    if(d_trackingWindow)
        d_trackingWindow->reset();
    if(d_mapWind)
        d_mapWind->reset();

    showWindow(d_toolBar,           BW_Mode::BW_ToolBar);
    showWindow(d_bvWind,            BW_Mode::BW_VictoryLevel);
    showWindow(d_gsWind,            BW_Mode::BW_StatusWindow);
    showWindow(d_trackingWindow,    BW_Mode::BW_TrackingWindow);
    showWindow(d_mapWind,           BW_Mode::BW_MapWindow);
    showWindow(d_locator,           BW_Mode::BW_Locator);
    showWindow(d_msgWin,            BW_Mode::BW_MessageWindow);
    d_mode->show(true);
}


void BattleWindowsImp::setZoomButtons()
{
    if(d_mapWind)
    {
        BattleMapInfo::Mode mode = d_mapWind->mode();

        if(d_locator)
        {
                            d_locator->setZoomButtons(mode);
        }

        checkMenu(IDM_BAT_ZOOMOVERVIEW, mode == BattleMapInfo::Strategic);
        checkMenu(IDM_BAT_ZOOMFOURMILE, mode == BattleMapInfo::FourMile);
        checkMenu(IDM_BAT_ZOOMTWOMILE, mode == BattleMapInfo::TwoMile);
        checkMenu(IDM_BAT_ZOOMDETAIL, mode == BattleMapInfo::OneMile);
    }
}

void BattleWindowsImp::setMapMode(BattleMapInfo::Mode mode)
{
    if(d_mapWind)
    {
        d_mapWind->mode(mode);
    }
    setZoomButtons();
}

void BattleWindowsImp::updateAll()
{
  setZoomButtons();
#if !defined(EDITOR)
  checkMenu(IDM_BAT_INSTANTORDERS, CampaignOptions::get(OPT_InstantOrders));
#endif
#ifdef DEBUG
  checkMenu(IDM_BAT_PALETTEWINDOW, d_palWind != 0);
  checkMenu(IDM_BAT_HEXDEBUG, d_mapWind->hexDebug());
  checkMenu(IDM_BAT_INSTANTMOVE, Options::get(OPT_InstantMove));
#endif
  checkMenu(IDM_BAT_HEXOUTLINE, d_mapWind->hexOutline());
  checkMenu(IDM_BAT_TINYMAPWINDOW, d_locator->isVisible());
  checkMenu(IDM_BAT_MESSAGEWINDOW, d_msgWin->isVisible());
  checkMenu(IDM_BAT_TRACKINGWINDOW, d_trackingWindow->isVisible());

  checkMenu(IDM_BAT_OB, False);
  checkMenu(IDM_BAT_FINDLEADER, False);
}

void BattleWindowsImp::checkMenu(int id, bool flag)
{
        d_menu.checked(id, flag);

        if(d_toolBar != 0)
                d_toolBar->setCheck(id, flag);
}

void BattleWindowsImp::enableMenu(int id, bool flag)
{
    d_menu.enable(id, flag);

    if(d_toolBar != 0)
        d_toolBar->enable(id, flag);
}

void BattleWindowsImp::startMapZoom()
{
    if(d_mapWind)
    {
        d_mapWind->startZoom();
    }
    setZoomButtons();
}


/*
 * Implementation of BattleWindowsInterface function
 */




bool BattleWindowsImp::setMapLocation(const BattleLocation& l)
{
        bool flag = false;

        if(d_mapWind)
        {
                flag = d_mapWind->setLocation(l);
        }
        return flag;
}

void BattleWindowsImp::mapAreaChanged(const BattleArea& area)
{
        if(d_locator)
                d_locator->setLocation(area);
}

void BattleWindowsImp::mapZoomChanged()
{
   setZoomButtons();

   /*
   * Set cursor back to normal
   */
    if(d_mode)
    {
        d_mode->mapZoomChanged();
   }

}

/*
 * sent by mapwind when Button is pressed
 */

void BattleWindowsImp::onLButtonDown(const BattleMapSelect& info)
{
    if(d_mode)
        d_mode->onLButtonDown(info);
}

/*
 * sent by mapwind when Button is released while not dragging
 */

void BattleWindowsImp::onLButtonUp(const BattleMapSelect& info)
{
    if(d_mode)
        d_mode->onLButtonUp(info);
}

void BattleWindowsImp::onRButtonDown(const BattleMapSelect& info)
{
    if(d_mode)
        d_mode->onRButtonDown(info);
}

/*
 * sent by mapwind when Button is released while not dragging
 */

void BattleWindowsImp::onRButtonUp(const BattleMapSelect& info)
{
    if(d_mode)
        d_mode->onRButtonUp(info);
}

/*
 * sent by mapwind when Mouse is moved while button is held
 */

void BattleWindowsImp::onStartDrag(const BattleMapSelect& info)
{
    if(d_mode)
        d_mode->onStartDrag(info);
}

/*
 * sent by mapwind when button is released while dragging
 */

void BattleWindowsImp::onEndDrag(const BattleMapSelect& info)
{
    if(d_mode)
        d_mode->onEndDrag(info);
}

/*
 * sent by mapwind when Mouse has moved
 */

void BattleWindowsImp::onMove(const BattleMapSelect& info)
{
  ASSERT(d_trackingWindow);
  d_trackingWindow->update(d_battleData->getHex(info.location(), BattleData::Mid));

    if(d_mode)
        d_mode->onMove(info);
}

bool BattleWindowsImp::setCursor(void)
{
    if(d_mode) return d_mode->setCursor();
   else return false;
}

/*
 * Battle Time has changed, so get the map to be redrawn
 */

void BattleWindowsImp::timeChanged()
{
    if(d_mapWind)
        d_mapWind->update(false);
    if(d_locator)
        d_locator->update(false);
    if(d_gsWind)
        d_gsWind->updateClock();
   if(d_mode)
      d_mode->timeChanged();
   if(d_bvWind) {
      d_victoryCalcCounter--;
      if(d_victoryCalcCounter == 0) {
         d_bvWind->update();
         d_victoryCalcCounter = 100;
      }
   }
}

void BattleWindowsImp::showMenu()
{
  PixelPoint p(0, 0);
  d_menu.run(p);
}

void BattleWindowsImp::orderCP(CRefBattleCP cp)
{
    ASSERT(cp != NoBattleCP);

    const BattleLocation& l = d_battleData->getLocation(cp->hex());
   setMapLocation(l);

   if(d_mode && d_mode->getUserInferface() ) {

      d_mode->getUserInferface()->selectUnit(const_cast<BattleCP *>(cp) );
   }
}

void BattleWindowsImp::messageWindowUpdated()
{
        // todo... update status window, update menu tick, update toolbar
}

void BattleWindowsImp::setController(Side side, GamePlayerControl::Controller control)
{
    d_batGame->setController(side, control);
}



// OptionsOwner
void
BattleWindowsImp::optionsChanged() {

   if(CampaignOptions::get(OPT_FogOfWar)) {
      mapWindow()->startVisibilityChecks();
      mapWindow()->getDisplay()->showHexVisibility(true);
   }
   else {
      mapWindow()->stopVisibilityChecks();
      mapWindow()->getDisplay()->showHexVisibility(false);
   }
   mapWindow()->update(false);
}


/*===================================================================
 * Insulated class functions
 */

BattleWindows::BattleWindows(HWND hMain, BattleWindowUser* batGame)
{
                  d_imp = new BattleWindowsImp(hMain, batGame);
}

BattleWindows::~BattleWindows()
{
                  delete d_imp;
}


void BattleWindows::timeChanged()
{
        ASSERT(d_imp != 0);
        d_imp->timeChanged();
}

HWND BattleWindows::getHWND() const
{
         ASSERT(d_imp != 0);
        return d_imp->getHWND();
}

BattleMapWind *
BattleWindows::getBattleMap(void) {

    ASSERT(d_imp != 0);
    return d_imp->mapWindow();

}

BW_Locator *
BattleWindows::getTinyMap(void) {

    ASSERT(d_imp != 0);
    return d_imp->tinyMap();

}

void BattleWindows::sendPlayerMessage(const BattleMessageInfo& msg)
{
        d_imp->addMessage(msg);
}

void BattleWindows::showResults(BResultWindowUser* user)
{
    d_imp->setMode(new BWMode_Results(d_imp, user));
}

void BattleWindows::setGameMode()
{
    d_imp->setMode(new BWMode_PlayGame(d_imp));
}

void BattleWindows::setMapSelectMode(const BattleInfo& info)
{
    d_imp->setMode(new BWMode_SelectMap(d_imp, info));
}

void BattleWindows::setDeployMode(const BattleInfo& info)
{
    d_imp->setMode(new BWMode_Deploy(d_imp, info));
}


};  // namespace WG_BattleWindows


/*
 *----------------------------------------------------------------------
 * $Log$
 *----------------------------------------------------------------------
 */
