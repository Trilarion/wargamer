/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *      Battle Unit Order Dialog
 *
 * The complete Info and Ordering dialog presented to the player
 *
 * Not using a private hidden implementation, as the only place that
 * uses this is bw_unit.cpp.  We may change our mind if this
 * is dependant on too many Header files.
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "bud_ordr.hpp"
#include "bu_dial.hpp"
#include "bud_info.hpp"
#include "cbutton.hpp"
#include "batres.h"
#include "scenario.hpp"
#include "palette.hpp"
#include "batarmy.hpp"
#include "app.hpp"
#include "scrnbase.hpp"
#include "c_combo.hpp"
#include "scn_img.hpp"
#include "fonts.hpp"
#include "dib.hpp"
#include "dib_util.hpp"
#include "DIB_blt.hpp"
#include "dib_poly.hpp"
#include "wind.hpp"
#include "bmp.hpp"
#include "scenario.hpp"
//#include "batdata.hpp"
#include "fillwind.hpp"
// #include "generic.hpp"
#include "batord.hpp"
#include "grtypes.hpp"
#include "scn_img.hpp"
#include "imglib.hpp"
#include "scn_res.h"
#include "wmisc.hpp"
#include "bobutil.hpp"
#include "hexdata.hpp"
//#include "moveutil.hpp"
#include "bordutil.hpp"
#include "bobiter.hpp"
#include "bfacectl.hpp"
#include "hexmap.hpp"
#include "bobiter.hpp"
#include "resstr.hpp"
#ifdef DEBUG
#include "sllist.hpp"
#include "pathfind.hpp"
#endif
//namespace BattleWindows_Internal
//{


using namespace BOB_Definitions;
using namespace BattleMeasure;


struct Poly {
   POINT pt1;
   POINT pt2;
   POINT pt3;
};




         enum ID {
                 ID_First = 100,
                 ID_FirstCombo = ID_First,
                 ID_OrderType = ID_FirstCombo,         // current order-type
                 ID_Aggression,                        // aggression-level
                 ID_DivDeployment,                     // Divisional Deployment
                 ID_SPDeployment,                      // SP Formation
                 ID_LastCombo = ID_SPDeployment,

                 ID_FirstButton,
                 ID_Send = ID_FirstButton,
                 ID_Cancel,
                 ID_Full,
                 ID_Less,
                 ID_Add,
                 ID_Del,
                 ID_LastButton = ID_Del,

                 ID_FirstCheckBox,

                 ID_RefuseLeft = ID_FirstCheckBox,
                 ID_EchelonLeft,
                 ID_RefuseRight,
                 ID_EchelonRight,

                 ID_Defensive, // = ID_FirstCheckBox,
                 ID_Offensive,
                 ID_DeployLeft,
                 ID_DeployCenter,
                 ID_DeployRight,
                 ID_LastCheckBox = ID_DeployRight,

                 ID_Info,

                 ID_Last,

             ID_FirstFacing,
                 ID_Facing_North = ID_FirstFacing,
                 ID_Facing_NorthEast,
                 ID_Facing_SouthEast,
                 ID_Facing_South,
                 ID_Facing_SouthWest,
                 ID_Facing_NorthWest,
/*
                 ID_FirstFacingCheckBox = ID_Facing_North,
                 ID_LastFacingCheckBox = ID_Facing_NorthWest + 1,
*/
                 ID_FirstDeployHow = ID_DeployLeft,
                 ID_EndDeployHow = ID_DeployRight + 1,

                 ID_FirstPosture = ID_Defensive,
                 ID_EndPosture = ID_Offensive + 1,

                 ID_FirstLineHow = ID_RefuseLeft,
                 ID_EndLineHow = ID_EchelonRight +1

         };



class FacingWindow : public WindowBaseND {

public:

   FacingWindow(HWND hparent, RECT * rect) {

      for(int f=0; f<6; f++) d_facingDIB[f] = NULL;

      d_facingRect.left = rect->left;
      d_facingRect.top = rect->top;
      d_facingRect.right = rect->right;
      d_facingRect.bottom = rect->bottom;

      d_screenDIB = NULL;
      d_screenDIB = new DrawDIBDC(rect->right, rect->bottom);
      ASSERT(d_screenDIB);

      d_facingDIB[NORTH] = BMP::newDrawDIBDC(scenario->getScenarioResDLL(), "FacingNorth", BMP::RBMP_Normal);
      ASSERT(d_facingDIB[NORTH]);
      d_facingDIB[NORTH_EAST_POINT] = BMP::newDrawDIBDC(scenario->getScenarioResDLL(), "FacingNorthEastPoint", BMP::RBMP_Normal);
      ASSERT(d_facingDIB[NORTH_EAST_POINT]);
      d_facingDIB[SOUTH_EAST_POINT] = BMP::newDrawDIBDC(scenario->getScenarioResDLL(), "FacingSouthEastPoint", BMP::RBMP_Normal);
      ASSERT(d_facingDIB[SOUTH_EAST_POINT]);
      d_facingDIB[SOUTH] = BMP::newDrawDIBDC(scenario->getScenarioResDLL(), "FacingSouth", BMP::RBMP_Normal);
      ASSERT(d_facingDIB[SOUTH]);
      d_facingDIB[SOUTH_WEST_POINT] = BMP::newDrawDIBDC(scenario->getScenarioResDLL(), "FacingSouthWestPoint", BMP::RBMP_Normal);
      ASSERT(d_facingDIB[SOUTH_WEST_POINT]);
      d_facingDIB[NORTH_WEST_POINT] = BMP::newDrawDIBDC(scenario->getScenarioResDLL(), "FacingNorthWestPoint", BMP::RBMP_Normal);
      ASSERT(d_facingDIB[NORTH_WEST_POINT]);

      d_currentFacing = NORTH;

      calcPolys();

      drawScreenDIB();

      createWindow(
         WS_EX_LEFT,
         // GenericNoBackClass::className(),
         "FacingWindow",
         WS_CHILD | WS_CLIPSIBLINGS | WS_VISIBLE,
         rect->left, rect->top, rect->right, rect->bottom,
         hparent,
         NULL);
         // APP::instance() );

      ASSERT(getHWND());

      SetWindowText(getHWND(),"FacingWindow");
   }

   ~FacingWindow(void) {
      for(int f=0; f<6; f++) delete d_facingDIB[f];
      delete d_screenDIB;
      selfDestruct();
   }

   enum FacingPolyEnum {
      NORTH,
      NORTH_EAST_POINT,
      SOUTH_EAST_POINT,
      SOUTH,
      SOUTH_WEST_POINT,
      NORTH_WEST_POINT
   };

   FacingPolyEnum d_currentFacing;

   void initFacing(HexPosition::Facing facing);

private:

   RECT d_facingRect;
   Poly d_facingPolys[6];
   DrawDIBDC * d_facingDIB[6];
   DrawDIBDC * d_screenDIB;

   POINT rotatePoint(POINT pt, float degrees);
   void calcPolys(void);
   bool PointInPoly(POINT & point, POINT * poly_points, int num);
   void drawScreenDIB(void);

   LRESULT procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);

   void onPaint(HWND hwnd);
   void onLButtonDown(HWND hwnd, BOOL fDoubleClick, int x, int y, UINT keyFlags);
   void onLButtonUp(HWND hwnd, int x, int y, UINT keyFlags);
};

void
FacingWindow::initFacing(HexPosition::Facing facing) {

   if(facing == HexPosition::North) {
      d_currentFacing = NORTH;
      drawScreenDIB();
      InvalidateRect(getHWND(), NULL, FALSE);
      return; }
   if(facing == HexPosition::NorthEastPoint) {
      d_currentFacing = NORTH_EAST_POINT;
      drawScreenDIB();
      InvalidateRect(getHWND(), NULL, FALSE);
      return; }
   if(facing == HexPosition::SouthEastPoint) {
      d_currentFacing = SOUTH_EAST_POINT;
      drawScreenDIB();
      InvalidateRect(getHWND(), NULL, FALSE);
      return; }
   if(facing == HexPosition::South) {
      d_currentFacing = SOUTH;
      drawScreenDIB();
      InvalidateRect(getHWND(), NULL, FALSE);
      return; }
   if(facing == HexPosition::SouthWestPoint) {
      d_currentFacing = SOUTH_WEST_POINT;
      drawScreenDIB();
      InvalidateRect(getHWND(), NULL, FALSE);
      return; }
   if(facing == HexPosition::NorthWestPoint) {
      d_currentFacing = NORTH_WEST_POINT;
      drawScreenDIB();
      InvalidateRect(getHWND(), NULL, FALSE);
      return; }

   FORCEASSERT("ERROR - trying to init facing-control with an invalid facing (must be to hex-points).");
   d_currentFacing = NORTH;
   drawScreenDIB();
   InvalidateRect(getHWND(), NULL, FALSE);
   return;
}


LRESULT
FacingWindow::procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam) {

   switch(msg) {

      HANDLE_MSG(hWnd, WM_PAINT, onPaint);
        HANDLE_MSG(hWnd, WM_LBUTTONDOWN, onLButtonDown);
        HANDLE_MSG(hWnd, WM_LBUTTONUP, onLButtonUp);

      default:
         return defProc(hWnd, msg, wParam, lParam);
   }
}


POINT
FacingWindow::rotatePoint(POINT pt, float degrees) {

   POINT new_pt;

   float radians = (float) ((degrees * 3.14159265359f) / 180.0f);

   new_pt.x = (cos(radians) * pt.x) + (-sin(radians) * pt.y);
   new_pt.y = (sin(radians) * pt.x) + (cos(radians) * pt.y);

   return new_pt;
}


void
FacingWindow::calcPolys(void) {

   // make the radius a little less than the total rect height
   float radius = d_facingRect.bottom - 1;

   // point we'll rotate around
   POINT initial;
   initial.x = 0;
   initial.y = -radius;

   float pt1_degs = -30.0f;
   float pt2_degs = 30.0f;

   for(int f=0; f<6; f++) {

      // pt1 is always the the center
      d_facingPolys[f].pt1.x = 0;
      d_facingPolys[f].pt1.y = 0;

      //d_facingPolys[f].pt2.x = (rotatePoint(initial, pt1_degs).x /6) *5;
      d_facingPolys[f].pt2.x = rotatePoint(initial, pt1_degs).x;
      d_facingPolys[f].pt2.y = rotatePoint(initial, pt1_degs).y;

      //d_facingPolys[f].pt3.x = (rotatePoint(initial, pt2_degs).x /6) *5;
      d_facingPolys[f].pt3.x = rotatePoint(initial, pt2_degs).x;
      d_facingPolys[f].pt3.y = rotatePoint(initial, pt2_degs).y;

      pt1_degs += 60.0f;
      pt2_degs += 60.0f;
   }
}


bool
FacingWindow::PointInPoly(POINT & point, POINT * poly_points, int num) {

   float x=point.x;
   float y=point.y;

   POINT * xp = poly_points;
   POINT * yp = poly_points;

   int i,j;
   bool c = false;

   for(i=0, j=num-1; i<num; j=i++) {
      if(
         (((yp[i].y <=y) && (y<yp[j].y)) ||
         ((yp[j].y <=y) && (y<yp[i].y))) &&
         (x < (xp[j].x - xp[i].x) * (y - yp[i].y) / (yp[j].y - yp[i].y) + xp[i].x)
         ) { c = !c; }
   }

   return c;
}


void
FacingWindow::drawScreenDIB(void) {

   // 1) blit normally to screenDIB
   DIB_Utility::stretchDIB(
      d_screenDIB,
      0,0,
      d_screenDIB->getWidth(), d_screenDIB->getHeight(),
      d_facingDIB[d_currentFacing],
      0,0,
      d_facingDIB[d_currentFacing]->getWidth(), d_facingDIB[d_currentFacing]->getHeight()
   );

   // 2) blit nornally to tmpDIB (at dimensions of max radius or polys)
   DrawDIBDC * tmpDIB = new DrawDIBDC(d_facingRect.bottom*2, d_facingRect.bottom*2);

   // these are the relative coordinates to blit to of the screenDIB within the tmpDIB
   int left = (tmpDIB->getWidth()/2) - (d_screenDIB->getWidth()/2);
   int top = (tmpDIB->getHeight()/2) - (d_screenDIB->getHeight()/2);

   DIB_Utility::stretchDIB(
      tmpDIB,
      left,top,
      d_screenDIB->getWidth(), d_screenDIB->getHeight(),
      d_facingDIB[d_currentFacing],
      0,0,
      d_facingDIB[d_currentFacing]->getWidth(), d_facingDIB[d_currentFacing]->getHeight()
   );

   // 3) darken tmpDIB
   tmpDIB->darken(20);

   // 4) draw MaskCol poly to TmpDIB
   tmpDIB->setMaskColour(0);
   ColourIndex maskCol = 0;

   POINT center;
   center.x = tmpDIB->getWidth()/2;
   center.y = tmpDIB->getHeight()/2;

   PixelPoint polyPoints[3];
   int facing = static_cast<int>(d_currentFacing);

   polyPoints[0].x( d_facingPolys[facing].pt1.x + center.x);
   polyPoints[0].y( d_facingPolys[facing].pt1.y + center.y);

   polyPoints[1].x( d_facingPolys[facing].pt2.x + center.x);
   polyPoints[1].y( d_facingPolys[facing].pt2.y + center.y);

   polyPoints[2].x( d_facingPolys[facing].pt3.x + center.x);
   polyPoints[2].y( d_facingPolys[facing].pt3.y + center.y);

   DIB_Utility::drawPoly(tmpDIB, polyPoints, 3, maskCol);

   // 5) blit graphic region from tmpDIb to screenDIB, using masking
   d_screenDIB->blit(
      0,0,
      d_screenDIB->getWidth(), d_screenDIB->getHeight(),
      tmpDIB,
      left, top
   );

   delete tmpDIB;

}


void
FacingWindow::onPaint(HWND handle) {

   if(!d_screenDIB) return;

    PAINTSTRUCT ps;
    HDC hdc = BeginPaint(handle, &ps);
    HPALETTE oldPal = SelectPalette(hdc, Palette::get(), FALSE);
    RealizePalette(hdc);

    BitBlt(hdc,0,0,d_screenDIB->getWidth(),d_screenDIB->getHeight(),d_screenDIB->getDC(),0,0,SRCCOPY);

    SelectPalette(hdc, oldPal, FALSE);
    EndPaint(handle, &ps);
}


void
FacingWindow::onLButtonDown(HWND hwnd, BOOL fDoubleClick, int x, int y, UINT keyFlags) {

   POINT mouse_pos;
   mouse_pos.x = x;
   mouse_pos.y = y;

   // center to offset by
   POINT center;
   center.x = d_facingRect.right / 2;
   center.y = d_facingRect.bottom / 2;

   for(int f=0; f<6; f++) {

      POINT poly_points[3];
      poly_points[0].x = d_facingPolys[f].pt1.x + center.x;
      poly_points[0].y = d_facingPolys[f].pt1.y + center.y;
      poly_points[1].x = d_facingPolys[f].pt2.x + center.x;
      poly_points[1].y = d_facingPolys[f].pt2.y + center.y;
      poly_points[2].x = d_facingPolys[f].pt3.x + center.x;
      poly_points[2].y = d_facingPolys[f].pt3.y + center.y;


      if(PointInPoly(mouse_pos, poly_points, 3)) {

         d_currentFacing = static_cast<FacingPolyEnum>(f);
         drawScreenDIB();
         InvalidateRect(getHWND(), NULL, FALSE);
         int id = ID_FirstFacing + f;
         SendMessage(
            GetParent(getHWND()),
            WM_COMMAND,
            MAKEWPARAM((UINT)(id),(UINT)(0)),
            (LPARAM)(HWND)(getHWND())
         );
         return;
      }
   }

}


void
FacingWindow::onLButtonUp(HWND hwnd, int x, int y, UINT keyFlags) {

}




/*--------------------------------------------------------------
 * First, some utilities
 */


class Util {
public:
  static HWND createButton(HWND hParent, const char* text, int id,
                 DWORD style, const DrawDIB* fillDib, const CtrlInfo& ci);

  static void getRowsAndColumns(const CPFormation f, const int spCount, int& columns, int& rows);
  static HWND createOwnerDrawStatic(HWND hParent, int id, const CtrlInfo& ci);
  static void drawInfo(CPBattleData bd, HWND hwnd, DrawDIBDC* dib, const DrawDIB* fillDib, CRefBattleCP& cp,
          const int cx, const int cy);
};

// calculate rows and columns required for given formation
void Util::getRowsAndColumns(const CPFormation f, const int spCount, int& columns, int& rows)
{

        if(f == CPF_March)
        {
          columns = 1;
          rows = spCount;
        }
        else if(f == CPF_Massed)
        {
          columns = 2;
          rows = (spCount + 1) / 2;
        }
        else if(f == CPF_Deployed)
        {
          columns = (spCount + 1) / 2;
          rows = 2;
        }
        else
        {
          columns = spCount;
          rows = 1;
        }
}

HWND Util::createButton(HWND hParent, const char* text, int id, DWORD style, const DrawDIB* fillDib, const CtrlInfo& ci)
{
  HWND hButton = CreateWindow(CUSTOMBUTTONCLASS, text,
                                style,
                                (ScreenBase::dbX() * ci.d_x) / ScreenBase::baseX(), (ScreenBase::dbY() * ci.d_y) / ScreenBase::baseY(),
                                (ScreenBase::dbX() * ci.d_cx) / ScreenBase::baseX(), (ScreenBase::dbY() * ci.d_cy) / ScreenBase::baseY(),
                                hParent, (HMENU)id, APP::instance(), NULL);

  ASSERT(hButton != 0);

  CustomButton cb(hButton);
  cb.setFillDib(fillDib);
  cb.setBorderColours(scenario->getBorderColors());
  if(style & BS_AUTOCHECKBOX)
         cb.setCheckBoxImages(ScenarioImageLibrary::get(ScenarioImageLibrary::CheckButtons));

  /*
        * Set up font
        */

  const int fontCY = (style & BS_AUTOCHECKBOX) ?
                (7 * ScreenBase::dbY()) / ScreenBase::baseY() :
                (8 * ScreenBase::dbY()) / ScreenBase::baseY();

  LogFont lf;
  lf.height(fontCY);
  lf.weight(FW_MEDIUM);
  lf.face(scenario->fontName(Font_Bold));

  Font font;
  font.set(lf);
  cb.setFont(font);

  return hButton;
}

HWND Util::createOwnerDrawStatic(HWND hParent, int id, const CtrlInfo& ci)
{
  HWND h = CreateWindow("Static", NULL,
                                SS_OWNERDRAW | SS_NOTIFY | WS_CHILD | WS_VISIBLE,
                                (ScreenBase::dbX() * ci.d_x) / ScreenBase::baseX(), (ScreenBase::dbY() * ci.d_y) / ScreenBase::baseY(),
                                (ScreenBase::dbX() * ci.d_cx) / ScreenBase::baseX(), (ScreenBase::dbY() * ci.d_cy) / ScreenBase::baseY(),
                                hParent, (HMENU)id, APP::instance(), NULL);

  ASSERT(h != 0);


  return h;
}

void Util::drawInfo(CPBattleData bd, HWND hwnd, DrawDIBDC* dib, const DrawDIB* fillDib, CRefBattleCP& cp,
        const int cx, const int cy)
{
  ASSERT(dib);
  ASSERT(fillDib);
  ASSERT(cx <= dib->getWidth());
  ASSERT(cy <= dib->getHeight());

  const int dbX = ScreenBase::dbX();
  const int dbY = ScreenBase::dbY();
  const int baseX = ScreenBase::baseX();
  const int baseY = ScreenBase::baseY();

  /*
        * Fill in background
        */

  const int dispCX = cx;
  const int dispCY = cy;

  dib->rect(0, 0, dispCX, dispCY, fillDib);

  // draw a 1 pixel black border
  ColourIndex ci = dib->getColour(PALETTERGB(0, 0, 0));
  dib->frame(0, 0, dispCX, dispCY, ci);

  /*
        * Fill in our info
        */

  int fontCX = (9 * dbY) / baseY;
  const int cutoff = 10;
  LogFont lf;
  lf.height(fontCX);
  lf.weight(FW_MEDIUM);
  lf.italic(true);
  lf.face(scenario->fontName((fontCX > cutoff) ? Font_Bold : Font_SmallBold));

  Font font;
  font.set(lf);
  HFONT oldFont = dib->setFont(font);
  COLORREF oldColor = dib->setTextColor(scenario->getSideColour(cp->getSide()));

  // starting point
  const int startX =  (2 * dbX) / baseX;
  const int startY =  (2 * dbY) / baseY;
  const int xOffset = (2 * dbX) / baseX;
  const int spacing = (7 * dbY) / baseY;
  const int infPosX  = (80 * dbX) / baseX;
  const int cavPosX  = (100 * dbX) / baseX;
  const int artPosX  = (120 * dbX) / baseX;

  int x = startX;
  int y = startY;

  // put Nation flag
  // draw nation flag
  RefGLeader leader = cp->leader();
  const ImageLibrary* il = scenario->getNationFlag(leader->getNation(), True);
  ASSERT(il);

  il->blit(dib, FI_Army, x, y, True);

  // put Unit Name
  x += (FI_Army_CX + xOffset);
  wTextOut(dib->getDC(), x, y, infPosX - (x + xOffset), cp->getName());

  // put leader name
  dib->setTextColor(oldColor);
  lf.height((8 * dbY) / baseY);
  font.set(lf);
  dib->setFont(font);

  y += spacing;
  wTextOut(dib->getDC(), x, y, infPosX - (x + xOffset), cp->leader()->getName());

  // put strength figures
  lf.height((7 * dbX) / baseX);
  lf.italic(false);
  lf.underline(true);
  lf.face(scenario->fontName(Font_SmallBold));

  font.set(lf);
  dib->setFont(font);

  y = startY;
  x = infPosX;
  wTextOut(dib->getDC(), x, y, InGameText::get(IDS_Inf)); // "Inf");
  x = cavPosX;
  wTextOut(dib->getDC(), x, y, InGameText::get(IDS_Cav));   // "Cav");
  x = artPosX;
  wTextOut(dib->getDC(), x, y, InGameText::get(IDS_GunsAbrev));   // "Guns");

  BobUtility::UnitCounts uc;
  BobUtility::unitsInManpower(cp, bd, uc);

  lf.underline(false);
  lf.face(scenario->fontName(Font_SmallBold));

  font.set(lf);
  dib->setFont(font);

  y += spacing;
  x = infPosX;
  wTextPrintf(dib->getDC(), x, y, "%d", uc.d_nInf);
  x = cavPosX;
  wTextPrintf(dib->getDC(), x, y, "%d", uc.d_nCav);
  x = artPosX;
  wTextPrintf(dib->getDC(), x, y, "%d", uc.d_nGuns);


  // clean up
  dib->setFont(oldFont);
//  d_dib->setTextColor(oldColor);
}

/*----------------------------------------------------------
 * Deployment / Manuever Window
 *
 * Note: Manuever Window is structured as follows
 *
 * A ComboBox listing Divisional Deployment (March, Mass, Deployed, Extended)
 * Auto Check Boxes for alignment (Line, Refused-Right, Extended-Right, etc.)
 * A ComboBox listing Manuevers (Forward, Wheel-Right, About-face, etc.)
 * A ComboBox listing SP Formations (Line, Square, Closed, etc.)
 *
 * Send, and Cancel Buttons
 */

class BM_Window : public WindowBaseND {

         DrawDIBDC* d_dib;
         const DrawDIB* d_fillDib;


         BUI_UnitInterface* d_owner;
         CPBattleData d_batData;
         BattleOrder&     d_baseOrder;
         BattleOrderInfo* d_order;
         CCombo d_orderTypeCombo;
         CCombo d_aggressionCombo;
         CCombo d_divDeployCombo;
//       CCombo d_manueverCombo;
         CCombo d_spDeployCombo;
         UBYTE d_flags;



         enum IDText {
                 IDT_OrderType,
                 IDT_Aggression,
                 IDT_Posture,
                 IDT_DivDeployment,
                 IDT_Manuever,
                 IDT_SPDeployment,
                 IDT_Facing,
                 IDT_DeployHow,

                 IDT_HowMany,
                 IDT_First = IDT_OrderType
         };


private:

         PixelPoint d_point;
         BUD_Info   d_info;
         FillWindow d_fillWind;
         static const CtrlInfo s_cInfo[ID_Last - ID_First];
         static const CtrlInfo s_textCInfo[IDT_HowMany];
//         static const char* s_strings[ID_Last - ID_First];
//         static const char* s_textStrings[IDT_HowMany];


         const int d_shadowCX;
         int d_cx;
         int d_cy;
         const int d_moveCY;
         const int d_deployCY;


         CRefBattleCP d_cp;
         HexCord d_hex;

       FacingWindow * d_facingWindow;

  public:

         BM_Window(BUI_UnitInterface* owner, RCPBattleData batData, BattleOrder& bo);
         ~BM_Window()
         {
                selfDestruct();

                if(d_dib)
                  delete d_dib;

                if(d_facingWindow)
                  delete d_facingWindow;

         }

//       void init(StackedUnitList& units, UnitListID unitID);
         bool run(const PixelPoint& p, const BUD_Info& info, BattleOrderInfo* order);
         bool run(const PixelPoint& p, const BUD_Info& info, BattleOrderInfo* order, UBYTE flags);
         void setMode(UBYTE mode) { SwitchMode(mode); }

        BattleOrder * getOrder(void) { return &d_baseOrder; }

         void hide();
         int width() const { return d_cx; }
         int fullHeight() const
         { return d_cy; }
//                return (d_owner->mode() == BUI_UnitInterface::Order) ? d_cy :
//                               (d_owner->mode() == BUI_UnitInterface::Moving) ? d_moveCY : d_deployCY; }


       //       int moveHeight() const { return d_moveCY; }

  private:
         LRESULT procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
         void onCommand(HWND hwnd, int id, HWND hwndCtl, UINT codeNotify);
         BOOL onCreate(HWND hWnd, CREATESTRUCT* lpCreateStruct);
         void onPaint(HWND hWnd);
         BOOL onEraseBk(HWND hwnd, HDC hdc);
         void onDestroy(HWND hWnd) {}
         UINT onNCHitTest(HWND hwnd, int x, int y);
         void onDrawItem(HWND hwnd, const DRAWITEMSTRUCT* di);

         void drawText(HDC hdc);
         void drawFacings(HDC hdc);

         void updateValues();
//       void updateMoveValues();
         void enableControls();

         const CtrlInfo& controlInfo(ID id) const
         {
                ASSERT(id < ID_Last);
                return s_cInfo[id - ID_First];
         }

         const CtrlInfo& controlTextInfo(IDText id) const
         {
                ASSERT(id < IDT_HowMany);
                return s_textCInfo[id];
         }

         void redrawControl(int id);

         const char* idToString(ID id);
         const char* textIDToString(IDText id);
         HexPosition::Facing idToFacing(ID id);
         CPDeployHow idToDeployHow(ID id);
         BattleOrderInfo::Posture idToPosture(ID id);
         Boolean idToCheck(ID id);
         void sizeWindow();
         void updateWindowStatus();
         void adjustPosition(const PixelPoint& p);

         void SwitchMode(int mode);
         void setMoveableButton(ID id, int y);

//         void runMove();
//         void runOrder();
//         void runDeploy();

         void run();


};

/*
 * Coodinates (in base units) of controls
 */


/*
Static Text Boxes sizes
*/

const CtrlInfo BM_Window::s_textCInfo[IDT_HowMany] = {
  {   56,   78, 94, 10, CtrlInfo::FullMode   },     // Orders
  {   56,  102, 94, 10, CtrlInfo::FullMode   },     // Aggression
  {   5,   132, 32, 10, CtrlInfo::FullMode   },     // Posture
  {   56,   52, 94, 10, CtrlInfo::SmallMode | CtrlInfo::FullMode },     // Division Deployment
  {   56,   28, 94, 10, CtrlInfo::SmallMode | CtrlInfo::FullMode },     // SPDeployment
  {   5,   28, 40, 10, CtrlInfo::SmallMode | CtrlInfo::FullMode   },     // Facing
  {   5,   78, 48, 10, CtrlInfo::FullMode  },     // DeployHow
  {   5,   106, 40, 10, 0 }, //CtrlInfo::FullMode  }     // Manuever
};

/*
Control sizes
*/

const CtrlInfo BM_Window::s_cInfo[ID_Last - ID_First] = {

  /*
  Combo Boxes
  */
  {   56,   90, 94, 10, CtrlInfo::FullMode  },     // OrderType combo
  {   56,   114, 94, 10, CtrlInfo::FullMode  },     // Aggression combo
  {   56,   64, 94, 10, CtrlInfo::SmallMode | CtrlInfo::FullMode },    // Division Deployment combo
  {   56,   40, 94, 10, CtrlInfo::SmallMode | CtrlInfo::FullMode },    // SPDeployment (formation) combo

//  {   88,  79,  32, 10, CtrlInfo::SmallMode | CtrlInfo::FullMode },    // Send button ( as for SmallMode, MOVED WHEN MODE CHANGES)
//  {   123,  79,  32, 10, CtrlInfo::SmallMode | CtrlInfo::FullMode },    // Cancel button ( as for SmallMode, MOVED WHEN MODE CHANGES)
//  {   5,  79,  32, 10, CtrlInfo::SmallMode },  // Full... button
//  {   5,  148,  32, 10, CtrlInfo::FullMode },  // Less... button
  {   57,  79,  48, 10, CtrlInfo::SmallMode | CtrlInfo::FullMode },    // Send button ( as for SmallMode, MOVED WHEN MODE CHANGES)
  {   107,  79,  48, 10, CtrlInfo::SmallMode | CtrlInfo::FullMode },    // Cancel button ( as for SmallMode, MOVED WHEN MODE CHANGES)
  {   5,  79,  48, 10, CtrlInfo::SmallMode },  // Full... button
  {   5,  148,  48, 10, CtrlInfo::FullMode },  // Less... button
  {   63, 79, 18, 10, CtrlInfo::SmallMode | CtrlInfo::FullMode },  // Add waypoint button
  {   43, 79, 18, 10, CtrlInfo::SmallMode | CtrlInfo::FullMode },  // Del waypoint button


/*
  {  21,  43,  8, 8, 0 }, //CtrlInfo::SmallMode | CtrlInfo::FullMode },  // Facing_North checkbox
  {  33,  48,  8, 8, 0 }, //CtrlInfo::SmallMode | CtrlInfo::FullMode },  // Facing_NorthEast checkbox
  {  33,  59,  8, 8, 0 }, //CtrlInfo::SmallMode | CtrlInfo::FullMode },  // Facing_SouthNorth checkbox
  {  21,  63,  8, 8, 0 }, //CtrlInfo::SmallMode | CtrlInfo::FullMode },  // Facing_South checkbox
  {  9,  59,  8, 8, 0 }, //CtrlInfo::SmallMode | CtrlInfo::FullMode },  // Facing_SouthWest checkbox
  {  9,  48,  8, 8, 0 }, //CtrlInfo::SmallMode | CtrlInfo::FullMode },  // Facing_NorthWest checkbox
*/
  { 5, 118,  48, 10, 0 }, //CtrlInfo::FullMode  },      // refuse left checkbox
  { 5, 142,  48, 10, 0 }, //CtrlInfo::FullMode  },      // echelon left checkbox
  { 5, 130,  48, 10, 0 }, //CtrlInfo::FullMode  },      // refuse right checkbox
  { 5, 154,  48, 10, 0 }, //CtrlInfo::FullMode  },      // echelon right checkbox


  {  55, 132,  44,  10, CtrlInfo::FullMode  },      // defensive checkbox
  { 105, 132,  44,  10, CtrlInfo::FullMode  },      // offensive checkbox

  {   8,  90,  40, 10, CtrlInfo::FullMode  },      // deploy left
  {   8, 102,  40, 10, CtrlInfo::FullMode  },      // deploy center
  {   8, 114,  40, 10, CtrlInfo::FullMode  },      // deploy right

  { 5, 4, 150, 20, CtrlInfo::SmallMode | CtrlInfo::FullMode }

};











BM_Window::BM_Window(BUI_UnitInterface* owner, RCPBattleData batData, BattleOrder& bo) :
  d_dib(0),
  d_fillDib(scenario->getScenarioBkDIB(Scenario::BackPatterns::PaperBackground)),
  d_owner(owner),
  d_batData(batData),
  d_baseOrder(bo),
  d_order(0),
  d_flags(CtrlInfo::SmallMode),
  d_shadowCX((ScreenBase::dbX() * 2) / ScreenBase::baseX()),
  d_cx((ScreenBase::dbX() * 160) / ScreenBase::baseX()),
  d_cy((ScreenBase::dbY() * 92) / ScreenBase::baseY()),

  d_moveCY((ScreenBase::dbY() * 92) / ScreenBase::baseY()),
  d_deployCY((ScreenBase::dbY() * 92) / ScreenBase::baseY()),

//  d_moveCY((ScreenBase::dbY() * 40) / ScreenBase::baseY()),
//  d_deployCY((ScreenBase::dbY() * 159) / ScreenBase::baseY()),
  d_facingWindow(NULL),
  d_cp(NoBattleCP)
//  d_facing(HexPosition::Facing_Default)
{
  ASSERT(d_batData);



  HWND hWnd = createWindow(
                0,
                // GenericNoBackClass::className(),
                NULL,
                WS_POPUP | WS_CLIPSIBLINGS,
                0, 0, d_cx, d_cy,
                APP::getMainHWND(), NULL    // , APP::instance()
  );

  SwitchMode(d_flags);
}

LRESULT BM_Window::procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
  switch(msg)
  {
         HANDLE_MSG(hWnd, WM_CREATE,    onCreate);
         HANDLE_MSG(hWnd, WM_COMMAND, onCommand);
         HANDLE_MSG(hWnd, WM_NCHITTEST, onNCHitTest);
         HANDLE_MSG(hWnd, WM_ERASEBKGND, onEraseBk);
         HANDLE_MSG(hWnd, WM_DRAWITEM, onDrawItem);
         HANDLE_MSG(hWnd, WM_PAINT, onPaint);

         default:
                return defProc(hWnd, msg, wParam, lParam);
  }
}
#if 0
// TODO: get this from string resource
const char* BM_Window::s_strings[ID_Last - ID_First] = {
        0,      // ordertype combo
        0,      // aggression combo
        0,      // divdeployment combo
        0,      // spdeployment (formation) combo

        "Send",
        "Cancel",
        "Full...",
        "Less...",
        "Add",
        "Del",

        "Refuse Left",
        "Echelon Left",
        "Refuse Right",
        "Echelon Right",

        "Def",
        "Off",

        "Left",
        "Center",
        "Right",

        0       // ID_Info (static header)
};


const char* BM_Window::idToString(ID id)
{
  ASSERT(id < ID_Last);
  ASSERT(s_strings[id - ID_First]);
  return s_strings[id - ID_First];
}
#endif

const char* BM_Window::idToString(ID id)
{
  ASSERT(id < ID_Last);
//  ASSERT(s_strings[id - ID_First]);

  static InGameText::ID s_textID[ID_Last - ID_First] = {
           InGameText::Null,      // ordertype combo
           InGameText::Null,      // aggression combo
           InGameText::Null,      // divdeployment combo
           InGameText::Null,      // spdeployment (formation) combo

           IDS_UOP_Send,
           IDS_UOP_Cancel,
           IDS_UOP_FullDot,      // "Full...",
           IDS_UOP_LessDot,      // "Less...",
           IDS_UOP_Add,          // "Add",
           IDS_UOP_Del,          // "Del",

           IDS_RefuseLeft,
           IDS_EchelonLeft,
           IDS_RefuseRight,
           IDS_EchelonRight,

           IDS_UOP_Def,
           IDS_UOP_Off,

           IDS_BAT_DEPLOY_3,      // Left,
           IDS_BAT_DEPLOY_1,      // "Center",
           IDS_BAT_DEPLOY_2,     // Right,

           InGameText::Null       // ID_Info (static header)
   };

   InGameText::ID rid = s_textID[id - ID_First];
   if(rid == InGameText::Null)
      return 0;
   else
     return InGameText::get(rid);
}

#if 0
// TODO: get this from string resource
const char* BM_Window::s_textStrings[IDT_HowMany] = {
        "Orders :",
        "Aggression :",
        "Posture :",
        "Deployment :",
        "Formation :",
        "Facing :",
        "Deploy How :",
        "Manuever :"
};

const char* BM_Window::textIDToString(IDText id)
{
  ASSERT(id < IDT_HowMany);
  ASSERT(s_textStrings[id]);
  return s_textStrings[id];
}
#endif

const char* BM_Window::textIDToString(IDText id)
{
  ASSERT(id < IDT_HowMany);
//  ASSERT(s_textStrings[id]);

   const InGameText::ID s_textID[IDT_HowMany] = {
           IDS_OrdersColon,      // "Orders :",
           IDS_AggressionColon,  // "Aggression :",
           IDS_PostureColon,     // "Posture :",
           IDS_DeploymentColon,  // "Deployment :",
           IDS_FormationColon,   // "Formation :",
           IDS_FacingColon,      // "Facing :",
           IDS_DeployHowColon,   // "Deploy How :",
           IDS_ManueverColon     // "Manuever :"
   };


  return InGameText::get(s_textID[id]);
}


Boolean BM_Window::idToCheck(ID id)
{
  ASSERT(id >= ID_FirstCheckBox);
  ASSERT(id <= ID_LastCheckBox);
/*
  if(id == ID_Facing_North)
        return(d_order->d_facing == HexPosition::North);
  if(id == ID_Facing_NorthEast)
        return(d_order->d_facing == HexPosition::NorthEastPoint);
  if(id == ID_Facing_SouthEast)
        return(d_order->d_facing == HexPosition::SouthEastPoint);
  if(id == ID_Facing_South)
        return(d_order->d_facing == HexPosition::South);
  if(id == ID_Facing_SouthWest)
        return(d_order->d_facing == HexPosition::SouthWestPoint);
  if(id == ID_Facing_NorthWest)
        return(d_order->d_facing == HexPosition::NorthWestPoint);
*/
  if(id == ID_RefuseLeft)
         return (d_order->d_lineHow == CPL_RefuseL);
  else if(id == ID_EchelonLeft)
         return (d_order->d_lineHow == CPL_EchelonL);
  if(id == ID_RefuseRight)
         return (d_order->d_lineHow == CPL_RefuseR);
  else if(id == ID_EchelonRight)
         return (d_order->d_lineHow == CPL_EchelonR);

  else if(id == ID_Defensive)
         return (d_order->d_posture == BattleOrderInfo::Defensive);
  else if(id == ID_Offensive)
         return (d_order->d_posture == BattleOrderInfo::Offensive);

  else if(id == ID_DeployLeft)
         return (d_order->d_deployHow == CPD_DeployLeft);
  else if(id == ID_DeployCenter)
         return (d_order->d_deployHow == CPD_DeployCenter);
  else if(id == ID_DeployRight)
         return (d_order->d_deployHow == CPD_DeployRight);
  else
         return False;
}


HexPosition::Facing
BM_Window::idToFacing(ID id) {

    if(id == ID_Facing_North) return HexPosition::North;
    else if(id == ID_Facing_NorthEast) return HexPosition::NorthEastPoint;
    else if(id == ID_Facing_SouthEast) return HexPosition::SouthEastPoint;
    else if(id == ID_Facing_South) return HexPosition::South;
    else if(id == ID_Facing_SouthWest) return HexPosition::SouthWestPoint;
   else if(id == ID_Facing_NorthWest) return HexPosition::NorthWestPoint;
    else {
      FORCEASSERT("ID to Facing given a bad value (defaulting to North)");
      return HexPosition::North;
   }

}

CPDeployHow BM_Window::idToDeployHow(ID id)
{
  ASSERT(id >= ID_FirstDeployHow);
  ASSERT(id < ID_EndDeployHow);

  if(id == ID_DeployLeft)
         return CPD_DeployLeft;
  else if(id == ID_DeployCenter)
         return CPD_DeployCenter;
  else
         return CPD_DeployRight;
}

BattleOrderInfo::Posture BM_Window::idToPosture(ID id)
{
  ASSERT(id >= ID_FirstPosture);
  ASSERT(id < ID_EndPosture);

  if(id == ID_Defensive)
         return BattleOrderInfo::Defensive;
  else
         return BattleOrderInfo::Offensive;
}

BOOL BM_Window::onCreate(HWND hWnd, CREATESTRUCT* lpCreateStruct)
{
  const int dbX = ScreenBase::dbX();
        const int dbY = ScreenBase::dbY();
  const int baseX = ScreenBase::baseX();
  const int baseY = ScreenBase::baseY();

  /*
        * create static info contro;
        */
  const CtrlInfo& ci = controlInfo(ID_Info);
  Util::createOwnerDrawStatic(hWnd, ID_Info, ci);

  DIB_Utility::allocateDib(&d_dib, (ci.d_cx * dbX) / baseX, (ci.d_cy * dbY) / baseY);
  ASSERT(d_dib);
  d_dib->setBkMode(TRANSPARENT);

  CustomButton::initCustomButton(getInstance());

        /*
        * init background dib
        */

  d_fillWind.setFillDib(scenario->getScenarioBkDIB(Scenario::BackPatterns::CherryWoodBackground));
  d_fillWind.setBorderColors(scenario->getBorderColors());
  d_fillWind.allocateDib(d_cx, d_cy);

  /*
        * create combos
        */

  // get combo font
  LogFont lf;
  lf.height((8 * dbY) / baseY);
  lf.weight(FW_MEDIUM);
  lf.face(scenario->fontName(Font_Bold));

  Font font;
  font.set(lf);

  CCombo* combos[(ID_LastCombo - ID_FirstCombo) + 1] = {
         &d_orderTypeCombo,
         &d_aggressionCombo,
         &d_divDeployCombo,
         &d_spDeployCombo
  };

  int i = 0;
  for(ID id = ID_FirstCombo; id < (ID_LastCombo + 1); INCREMENT(id), i++)
  {
         const CtrlInfo& ci = controlInfo(id);

         CComboData cd;
         cd.d_hParent = hWnd;
         cd.d_id = id;
         cd.d_hFont = font;
         cd.d_cx = (ci.d_cx * dbX) / baseX;
         cd.d_cy = (ci.d_cy * dbY) / baseY;
         cd.d_itemCY = (10 * dbY) / baseY;
         cd.d_windowFillDib = scenario->getSideBkDIB(SIDE_Neutral);
         cd.d_fillDib = scenario->getScenarioBkDIB(Scenario::BackPatterns::PaperBackground);
         cd.d_comboBtn = ScenarioImageLibrary::get(ScenarioImageLibrary::CComboButtonImages);
         cd.d_bColors = scenario->getBorderColors();
         scenario->getColour("MapInsert", cd.d_colorize);

         combos[i]->init(cd);
         combos[i]->position((ci.d_x * dbX) / baseX, (ci.d_y * dbY) / baseY);
  }

  // init combos






        /*
        * create check boxes
        */

  for(id = ID_FirstCheckBox; id < (ID_LastCheckBox + 1); INCREMENT(id))
  {
         const CtrlInfo& ci = controlInfo(id);

//       const DrawDIB* fill = scenario->getScenarioBkDIB(Scenario::BackPatterns::WoodBackground);
         const DrawDIB* fill = scenario->getScenarioBkDIB(Scenario::BackPatterns::HScrollThumbBackground);
         Util::createButton(hWnd, idToString(id), id,
                        BS_AUTOCHECKBOX | WS_CHILD | WS_VISIBLE, fill, ci);
  }

  /*
        * create push buttons
        */

  for(id = ID_FirstButton; id < (ID_LastButton + 1); INCREMENT(id))
  {
         const CtrlInfo& ci = controlInfo(id);

         const DrawDIB* fill = scenario->getScenarioBkDIB(Scenario::BackPatterns::HScrollThumbBackground);

         Util::createButton(hWnd, idToString(id), id,
                        BS_PUSHBUTTON | WS_CHILD | WS_VISIBLE, fill, ci);
        }

  /*
        * create facing check boxes
        */

  /*
  create ID_Facing_Static control
  */
  RECT facingRect;
  facingRect.left = (ScreenBase::dbX() * 5) / ScreenBase::baseX();
  facingRect.top = (ScreenBase::dbY() * 40) / ScreenBase::baseY();
  facingRect.right = (ScreenBase::dbX() * 40) / ScreenBase::baseX();
  facingRect.bottom = (ScreenBase::dbY() * 34) / ScreenBase::baseY();

  d_facingWindow = new FacingWindow(hWnd, &facingRect);
  ASSERT(d_facingWindow);


  return TRUE;
}



void BM_Window::setMoveableButton(ID id, int newY)
{
   CustomButton cb(getHWND(), id);
   const CtrlInfo& info = controlInfo(id);

//      MoveWindow(cb.getHWND(), (88 * dbX) / baseX, (148 * dbY) / baseY, (32 * dbX) / baseX, (10 * dbY) / baseY, FALSE);
   MoveWindow(cb.getHWND(),
      ScreenBase::x(info.d_x),    //(88 * dbX) / baseX,
      newY,
      ScreenBase::x(info.d_cx),  // (32 * dbX) / baseX,
      ScreenBase::y(info.d_cy),   // (10 * dbY) / baseY,
      FALSE);
}





void BM_Window::SwitchMode(int mode) {

//  if(d_flags == mode) return;

  const int dbX = ScreenBase::dbX();
  const int dbY = ScreenBase::dbY();
  const int baseX = ScreenBase::baseX();
  const int baseY = ScreenBase::baseY();

  /*
  Resize main window
  */



  if(mode == CtrlInfo::SmallMode) {
      d_cx = ( (ScreenBase::dbX() * 160) / ScreenBase::baseX() );
      d_cy = ( (ScreenBase::dbY() * 92) / ScreenBase::baseY() );
  }
  else if(mode == CtrlInfo::FullMode) {
      d_cx = ( (ScreenBase::dbX() * 160) / ScreenBase::baseX() );
      d_cy = ( (ScreenBase::dbY() * 163) / ScreenBase::baseY() );
  }

  RECT rect;
  GetWindowRect(getHWND(), &rect);

//  MoveWindow(getHWND(), rect.left, rect.top, (d_cx * dbX) / baseX, (d_cy * dbY) / baseY, FALSE);

  MoveWindow(getHWND(), rect.left, rect.top, d_cx, d_cy, FALSE);

  d_fillWind.setFillDib(scenario->getScenarioBkDIB(Scenario::BackPatterns::CherryWoodBackground));
  d_fillWind.setBorderColors(scenario->getBorderColors());
  d_fillWind.allocateDib(d_cx, d_cy);

  /*
  Hide / Show combos
  */

  CCombo* combos[(ID_LastCombo - ID_FirstCombo) + 1] = {
         &d_orderTypeCombo,
         &d_aggressionCombo,
         &d_divDeployCombo,
         &d_spDeployCombo
  };

  int i = 0;
  for(ID id = ID_FirstCombo; id < (ID_LastCombo + 1); INCREMENT(id), i++)
  {
         const CtrlInfo& ci = controlInfo(id);

         HWND wnd = combos[i]->getHWND();

         if(! (ci.d_flags & mode) ) {
             ShowWindow(wnd, SW_HIDE);
         }
         else ShowWindow(wnd, SW_SHOW);
  }

  /*
  Hide / Show buttons
  */

  for(id = ID_FirstButton; id < (ID_LastButton + 1); INCREMENT(id) )
  {
         const CtrlInfo& ci = controlInfo(id);
         CustomButton cb(getHWND(), id);

         if(! (ci.d_flags & mode) ) {
             ShowWindow(cb.getHWND(), SW_HIDE);
         }
         else ShowWindow(cb.getHWND(), SW_SHOW);
  }

  /*
  Hide / Show checkboxes
  */

  for(id = ID_FirstCheckBox; id < (ID_LastCheckBox + 1); INCREMENT(id) )
  {
         const CtrlInfo& ci = controlInfo(id);
         CustomButton cb(getHWND(), id);

         if(! (ci.d_flags & mode) ) {
             ShowWindow(cb.getHWND(), SW_HIDE);
         }
         else ShowWindow(cb.getHWND(), SW_SHOW);
  }


   int newY;
   if(mode == CtrlInfo::FullMode)
      newY = ScreenBase::y(148);
   else
      newY = ScreenBase::y(79);

   setMoveableButton(ID_Send, newY);
   setMoveableButton(ID_Cancel, newY);
   setMoveableButton(ID_Add, newY);
   setMoveableButton(ID_Del, newY);

#if 0
   CustomButton cb(getHWND(), ID_Send);
//      MoveWindow(cb.getHWND(), (88 * dbX) / baseX, (148 * dbY) / baseY, (32 * dbX) / baseX, (10 * dbY) / baseY, FALSE);
   MoveWindow(cb.getHWND(),
      s_cInfo[ID_Send-ID_First].d_x,    //(88 * dbX) / baseX,
      newY,
      s_cInfo[ID_Send-ID_First].d_cx,  // (32 * dbX) / baseX,
      s_cInfo[ID_Send-ID_First].d_cy,   // (10 * dbY) / baseY,
      FALSE);

   // ci = controlInfo(ID_Cancel);
   cb = CustomButton(getHWND(), ID_Cancel);
   MoveWindow(cb.getHWND(),
      s_cInfo[ID_Cancel-ID_First].d_x,   // (123 * dbX) / baseX,
      newY,                              // (148 * dbY) / baseY,
      s_cInfo[ID_Cancel-ID_First].d_cx,  // (32 * dbX) / baseX,
      s_cInfo[ID_Cancel-ID_First].d_cy,   // (10 * dbY) / baseY,
      FALSE);

   // ci = controlInfo(ID_Add);
   cb = CustomButton(getHWND(), ID_Add);
   MoveWindow(cb.getHWND(),
      s_cInfo[ID_Add-ID_First].d_x,   // (43 * dbX) / baseX,
      newY,                           // (148 * dbY) / baseY,
      s_cInfo[ID_Add-ID_First].d_cx,  // (18 * dbX) / baseX,
      s_cInfo[ID_Add-ID_First].d_cy,   // (10 * dbY) / baseY,
      FALSE);

   // ci = controlInfo(ID_Del);
   cb = CustomButton(getHWND(), ID_Del);
   MoveWindow(cb.getHWND(),
      s_cInfo[ID_Del-ID_First].d_x,   // (63 * dbX) / baseX,
      newY,                           // (148 * dbY) / baseY,
      s_cInfo[ID_Del-ID_First].d_cx,  // (18 * dbX) / baseX,
      s_cInfo[ID_Del-ID_First].d_cy,   // (10 * dbY) / baseY,
      FALSE);
#endif

  // hide waypoint buttons, until implemented fully
  CustomButton cb(getHWND(), ID_Add);
  ShowWindow(cb.getHWND(), SW_HIDE);
  cb = CustomButton(getHWND(), ID_Del);
  ShowWindow(cb.getHWND(), SW_HIDE);

  // finally set the mode
  d_flags = mode;

  InvalidateRect(getHWND(), NULL, FALSE);
}



void
BM_Window::onCommand(HWND hwnd, int id, HWND hwndCtl, UINT codeNotify) {

   switch(id) {

      case ID_OrderType : {

         BattleOrderInfo::OrderMode om = static_cast<BattleOrderInfo::OrderMode>(d_orderTypeCombo.getValue());
#if 0
         /*
         Sort out discrepancies with context-sensitive value indexes for Divisional CPs
         */
         if(d_cp->getRank().sameRank(Rank_Division)) {

            // if we're already attached, this item really means detach
            if(!d_cp->attached() && om == BattleOrderInfo::Attach) om = BattleOrderInfo::Detach;

            else {
               // take account for attach / detach sharing same slot
               if(om >= BattleOrderInfo::Detach) INCREMENT(om);
               // take account for bombard dometimes not being present
               if((!d_cp->generic()->isArtillery()) && om == BattleOrderInfo::Bombard) om = BattleOrderInfo::Rout;
            }
         }
         /*
         Sort out discrepancies with context-sensitive value indexes for other CPs
         */
         else {

            // take into account rest/rally not being present
            //if(om >= BattleOrderInfo::RestRally) INCREMENT(om);

            if(!d_cp->attached() && om == BattleOrderInfo::Attach) om = BattleOrderInfo::Detach;
         }

#endif

                if(B_OrderUtil::isMoveOrder(om))
                {
                        d_order->d_mode = om;
                        hide();
                        d_owner->mode(BUI_UnitInterface::Normal);
                }
                else if(om == BattleOrderInfo::Bombard)
                {
                        hide();
                        d_owner->mode(BUI_UnitInterface::Targetting);
                }
                else
                {
                        d_order->d_mode = om;
                        enableControls();
                }

                break;
         }

         case ID_Aggression:
         {
                BattleOrderInfo::Aggression ag = static_cast<BattleOrderInfo::Aggression>(d_aggressionCombo.getValue());
                d_order->d_aggression = ag;
                enableControls();
                // sound
                d_batData->soundSystem()->triggerSound(SOUNDTYPE_MENUSELECT);
                break;
         }

         case ID_DivDeployment:
         {

                CPFormation f = static_cast<CPFormation>(d_divDeployCombo.getValue());

                if(f != d_order->d_divFormation)
                {
                    d_owner->highlightUnit(false);
                    d_order->d_divFormation = f;
                    d_order->d_whatOrder.add(WhatOrder::CPFormation);

                    updateValues();

                    enableControls();
                    d_owner->highlightUnit(True);
                    // sound
                    d_batData->soundSystem()->triggerSound(SOUNDTYPE_MENUSELECT);
                }

                break;
         }
         case ID_SPDeployment:
         {
                d_owner->highlightUnit(false);
                d_order->d_spFormation = static_cast<SPFormation>(d_spDeployCombo.getValue());
                d_order->d_whatOrder.add(WhatOrder::SPFormation);
                enableControls();
                d_owner->highlightUnit(True);
                // sound
                d_batData->soundSystem()->triggerSound(SOUNDTYPE_MENUSELECT);
                break;
         }


        case ID_RefuseLeft :
        case ID_RefuseRight :
        case ID_EchelonLeft :
        case ID_EchelonRight : {

            d_owner->highlightUnit(false);

            CPLineHow lh;
            if(id == ID_RefuseLeft) {
                if(d_order->d_lineHow == CPL_RefuseL) lh = CPL_Line;
                else lh = CPL_RefuseL;
            }
            if(id == ID_RefuseRight) {
                if(d_order->d_lineHow == CPL_RefuseR) lh = CPL_Line;
                else lh = CPL_RefuseR;
            }
            if(id == ID_EchelonLeft) {
                if(d_order->d_lineHow == CPL_EchelonL) lh = CPL_Line;
                else lh = CPL_EchelonL;
            }
            if(id == ID_EchelonRight) {
                if(d_order->d_lineHow == CPL_EchelonR) lh = CPL_Line;
                else lh = CPL_EchelonR;
            }

            d_order->d_lineHow = lh;
            d_order->d_whatOrder.add(WhatOrder::Manuever);

            if(lh == CPL_Line) {
                for(ID i = ID_FirstLineHow; i < ID_EndLineHow; INCREMENT(i) ) {
                    CustomButton cb(getHWND(), i);
                    cb.setCheck(false);
                }
            }
            else {
                for(ID i = ID_FirstLineHow; i < ID_EndLineHow; INCREMENT(i) ) {
                    CustomButton cb(getHWND(), i);
                    cb.setCheck(idToCheck(i) );
                }
            }

            d_owner->highlightUnit(True);
            // sound
            d_batData->soundSystem()->triggerSound(SOUNDTYPE_MENUSELECT);
            break;
        }



         case ID_DeployLeft:
         case ID_DeployCenter:
         case ID_DeployRight:
         {
                CPDeployHow dh = idToDeployHow(static_cast<ID>(id));
                if(d_order->d_deployHow != dh)
                {

                  d_owner->highlightUnit(false);

                  d_order->d_deployHow = dh;

                  /*
                  * Set check buttons
                  */

                  for(ID i = ID_FirstDeployHow; i < ID_EndDeployHow; INCREMENT(i))
                  {
                         CustomButton cb(getHWND(), i);
                         cb.setCheck( (d_order->d_deployHow == idToDeployHow(i)) );
                  }

                  d_owner->highlightUnit(True);

                  // sound
                  d_batData->soundSystem()->triggerSound(SOUNDTYPE_MENUSELECT);
                }

                break;
         }

         case ID_Offensive:
         case ID_Defensive:
         {
                d_order->d_posture = idToPosture(static_cast<ID>(id));

                /*
                 * Set check buttons
                 */

                for(ID i = ID_FirstPosture; i < ID_EndPosture; INCREMENT(i))
                {
                  CustomButton cb(getHWND(), i);
                  cb.setCheck( (d_order->d_posture == idToPosture(i)) );
                }

                // sound
                d_batData->soundSystem()->triggerSound(SOUNDTYPE_MENUSELECT);
                break;
         }
/*
         case ID_Move:
                d_owner->mode(BUI_UnitInterface::Moving);
                run();
                adjustPosition(d_point);
                break;
*/

         case ID_Full : {
             SwitchMode(CtrlInfo::FullMode);
             // sound
             d_batData->soundSystem()->triggerSound(SOUNDTYPE_MENUPOPUP);
             break;
         }
         case ID_Less : {
             SwitchMode(CtrlInfo::SmallMode);
             // sound
             d_batData->soundSystem()->triggerSound(SOUNDTYPE_MENUPOPUP);
             d_owner->redrawMap();
             break;
         }



         case ID_Add : {

             break;
         }

         case ID_Del : {

             break;
         }



         case ID_Send:
         {
                d_owner->highlightUnit(false);

                updateValues();
//              if(d_owner->mode() == BUI_UnitInterface::Moving)
//                updateMoveValues();

                hide();
/*
                if(d_owner->mode() == BUI_UnitInterface::Moving || d_owner->mode() == BUI_UnitInterface::Deploy)
                  d_owner->mode(BUI_UnitInterface::Order);
*/

                d_owner->sendOrder();
                break;
         }
         case ID_Cancel:
         {
                d_owner->highlightUnit(false);

                hide();
/*
                if(d_owner->mode() == BUI_UnitInterface::Moving || d_owner->mode() == BUI_UnitInterface::Deploy)
                  d_owner->mode(BUI_UnitInterface::Order);
*/
                d_owner->cancelOrder();
                break;
         }

//       case ID_NextWayPoint:
//              hide();

//              if(d_order->nextWayPoint() || d_owner->mode() == BUI_UnitInterface::Moving)
//                updateMoveValues();

//              d_owner->mode(BUI_UnitInterface::Moving);
//              break;


/*
         case ID_Deploy:
         case ID_Deploy2:
         {
                BUI_UnitInterface::Mode m =  (d_cp && d_cp->getRank().sameRank(Rank_Division)) ?
                        BUI_UnitInterface::Deploy : BUI_UnitInterface::Order;
                d_owner->mode(m);
                run();
                adjustPosition(d_point);
                break;
         }
*/

/*
         case ID_Remove:
                hide();
#if 0
                ASSERT(d_order->wayPoints().entries() > 0);
                if(d_order->wayPoints().entries() > 0)
                  d_order->wayPoints().remove(d_order->wayPoints().getLast());
#endif
                break;

*/


        case ID_Facing_North :
        case ID_Facing_NorthEast :
        case ID_Facing_SouthEast :
        case ID_Facing_South :
        case ID_Facing_SouthWest :
        case ID_Facing_NorthWest : {

            d_owner->highlightUnit(false);

            d_order->d_facing = idToFacing(static_cast<ID>(id) );
            const UBYTE VK_R = 0x52;
            d_order->d_whatOrder.add(WhatOrder::Facing);
            d_order->d_faceHow = (GetKeyState(VK_R) & 0x8000) ? CPH_Back : CPH_Front;
            updateValues();

/*
            for(ID i = ID_FirstFacingCheckBox; i < ID_LastFacingCheckBox; INCREMENT(i))
            {
                CustomButton cb(getHWND(), i);
                cb.setCheck( (d_order->d_facing == idToFacing(i)) );
            }
*/
            d_owner->highlightUnit(true);

            // sound
            d_batData->soundSystem()->triggerSound(SOUNDTYPE_FACINGCHANGE);

            break;
        }


/*
         case ID_Facing:
         {
                HexPosition::Facing f = d_faceCtl->currentFace();
                if(d_order->d_facing != f)
                {
                  d_order->d_facing = f;
                  d_order->d_whatOrder.add(WhatOrder::Facing);

                  // If 'R' key is down player is plotting reverse facing
                  const UBYTE VK_R = 0x52;
                  d_order->d_faceHow = (GetKeyState(VK_R) & 0x8000) ? CPH_Back : CPH_Front;

                  d_owner->highlightUnit(True);
                }
                break;
         }
*/

/*
         case ID_Orders:
                d_owner->mode(BUI_UnitInterface::Order);
                run();
                adjustPosition(d_point);
                break;
*/

/*
         case ID_InfoBtn:
                d_owner->showInfo();
                break;
*/

/*
         case ID_Time:
#ifdef DEBUG
//              MessageBox(hwnd, "Window not yet implemented!", "Warning", MB_ICONWARNING | MB_OK);
#endif
                // use temporarily to show LOS
    d_owner->showLOS();
                break;
  }
*/


        } // end of switch statement




}


void BM_Window::onDrawItem(HWND hwnd, const DRAWITEMSTRUCT* di)
{
  ASSERT(d_dib);
  HPALETTE oldPal = SelectPalette(di->hDC, Palette::get(), FALSE);
  RealizePalette(di->hDC);

  const int cx = di->rcItem.right - di->rcItem.left;
  const int cy = di->rcItem.bottom - di->rcItem.top;

  if(di->CtlID == ID_Info)
         BitBlt(di->hDC, di->rcItem.left, di->rcItem.top, cx, cy,
                 d_dib->getDC(), 0, 0, SRCCOPY);

  SelectPalette(di->hDC, oldPal, FALSE);
}

BOOL BM_Window::onEraseBk(HWND hwnd, HDC hdc)
{
//      static RECT r;
//      if(GetUpdateRect(hwnd, &r, TRUE))
//              d_fillWind.paint(hdc, r);
//  drawText(hdc);
        return True;
}

void BM_Window::onPaint(HWND hwnd)
{
        PAINTSTRUCT ps;
        HDC hdc = BeginPaint(hwnd, &ps);

        d_fillWind.paint(hdc, ps.rcPaint);

        HPALETTE oldPal = SelectPalette(hdc, Palette::get(), FALSE);
        RealizePalette(hdc);

        drawText(hdc);

        SelectPalette(hdc, oldPal, FALSE);
        EndPaint(hwnd, &ps);

}

void BM_Window::drawText(HDC hdc)
{
        const int dbX = ScreenBase::dbX();
        const int dbY = ScreenBase::dbY();
        const int baseX = ScreenBase::baseX();
        const int baseY = ScreenBase::baseY();

        // get  font
        LogFont lf;
        lf.height((7 * dbY) / baseY);
        lf.weight(FW_MEDIUM);
        lf.face(scenario->fontName(Font_Bold));
        lf.underline(true);

        Font font;
        font.set(lf);

        HFONT oldFont = (HFONT)SelectObject(hdc, font);
        ASSERT(oldFont);

        COLORREF tc = scenario->getColour("MenuText");
        COLORREF oldColor = SetTextColor(hdc, tc);
        SetBkMode(hdc, TRANSPARENT);

        for(IDText i = IDT_First; i < IDT_HowMany; INCREMENT(i))
        {
         const CtrlInfo& ci = controlTextInfo(i);

         if(ci.d_flags & d_flags)
                wTextOut(hdc, (ci.d_x * dbX) / baseX, (ci.d_y * dbY) / baseY, textIDToString(i));
        }

        SelectObject(hdc, oldFont);
        SetTextColor(hdc, oldColor);
}



void BM_Window::drawFacings(HDC hdc) {

}





UINT BM_Window::onNCHitTest(HWND hwnd, int x, int y)
{
        return HTCAPTION;
}

void BM_Window::redrawControl(int id)
{
        ASSERT(id < ID_Last);

        HWND h = GetDlgItem(hWnd, id);
        ASSERT(h);

        InvalidateRect(h, NULL, FALSE);
        UpdateWindow(h);
}

void BM_Window::enableControls()
{
//        CustomButton b(getHWND(), ID_Deploy);
//        b.enable( (d_cp != NoBattleCP && d_cp->getRank().sameRank(Rank_Division)) );
}

void BM_Window::updateValues()
{
        if(d_order->d_mode == BattleOrderInfo::Attach)
        {
         const BattleHexMap* hexMap = d_batData->hexMap();

         BattleHexMap::const_iterator lower;
         BattleHexMap::const_iterator upper;
         if(hexMap->find(d_hex, lower, upper))
         {
                const BattleUnit* unit = hexMap->unit(lower);
                RefBattleCP uCP = const_cast<RefBattleCP>(BobUtility::getCP(unit));

                if(uCP->getSide() == d_cp->getSide())
                {
                  if(d_cp->getRank().isHigher(Rank_Division))
                  {
                         // a leader can only attached to a subordinate in his chain of command
                         for(BattleUnitIter cpIter(const_cast<RefBattleCP>(d_cp)); !cpIter.isFinished(); cpIter.next())
                         {
                                if(cpIter.cp() == uCP)
                                {
                                  d_baseOrder.attachTo(uCP);

                                  const HexCord* destHex = 0;
                                  if(uCP->getRank().sameRank(Rank_Division))
                                  {
                                         DeployItem* di = uCP->wantDeployItem(uCP->wantColumns() / 2, minimum(uCP->spCount() - 1, uCP->wantRows() - 1));
                                         ASSERT(di);
                                         if(di)
                                         {
                                           ASSERT(di->active());
                                           if(di->active())
                                             destHex = &di->d_sp->hex();
                                         }
                                  }
                                  else
                                         destHex = &uCP->hex();

                                  ASSERT(d_baseOrder.nextWayPoint());
                                  if(destHex)
                                        d_baseOrder.wayPoints().getLast()->d_hex = *destHex;
                                  break;
                                }
                         }

                         if(d_baseOrder.attachTo() == NoBattleCP)
            d_order->d_mode = BattleOrderInfo::Hold;
                  }
                }
         }
  }
}

void BM_Window::run()
{
        updateWindowStatus();
/*
        if( (d_owner->mode() == BUI_UnitInterface::Order) || (d_owner->mode() == BUI_UnitInterface::PlottingTarget) )
                runOrder();
        else if(d_owner->mode() == BUI_UnitInterface::Moving)
                runMove();
        else
         runDeploy();
*/
        enableControls();

        d_flags = CtrlInfo::SmallMode;
        SwitchMode(d_flags);

//      UpdateWindow(getHWND());
}

void BM_Window::adjustPosition(const PixelPoint& p)
{
         /*
          * make sure entire window is within mainwindows rect
          */

         RECT r;
         GetWindowRect(APP::getMainHWND(), &r);

         int x = p.getX();
         int y = p.getY();
         const int offset = 5;
         // check right side
         if((x + d_cx) > r.right)
         {
                x -= (d_cx + offset);
         }
/*
         const int cy = (d_owner->mode() == BUI_UnitInterface::Order) ? d_cy :
                                                 (d_owner->mode() == BUI_UnitInterface::Moving) ? d_moveCY : d_deployCY;
*/
         const int cy = d_cy;

         // check bottom
         if((y + cy) > r.bottom)
         {
                y -= (cy + offset);
         }

         d_point.set(x, y);

         SetWindowPos(getHWND(), HWND_TOPMOST, d_point.getX(), d_point.getY(), 0, 0, SWP_NOSIZE | SWP_SHOWWINDOW);
}


bool BM_Window::run(const PixelPoint& p, const BUD_Info& info, BattleOrderInfo* order) {

    return run(p, info, order, CtrlInfo::SmallMode);

}


bool BM_Window::run(const PixelPoint& p, const BUD_Info& info, BattleOrderInfo* order, UBYTE flags)
{
        // draw info
        ASSERT(d_dib);

        d_flags = flags;
        SwitchMode(d_flags);

        // Don't update
//      if(d_owner->mode() != BUI_UnitInterface::PlottingTarget)
        {
                d_cp = info.cp();
                d_order = order;
                ASSERT(d_order);
        }

        d_hex = info.hex();
        d_info = info;

        // If 'W' key is down do not show window, player is plotting waypoints
        const UBYTE VK_W = 0x57;
        if( !(GetKeyState(VK_W) & 0x8000) )
        {
         Util::drawInfo(d_batData, getHWND(), d_dib, d_fillDib, d_cp, d_dib->getWidth(), d_dib->getHeight());
         run();

         adjustPosition(p);

         //d_faceCtl->initFace(order->d_facing);
       d_facingWindow->initFacing(order->d_facing);
         enableControls();


/*

        These following initializations are from runOrder()

*/


         // Init Combo's


  static CComboInit s_orderTypeItems[] = {
      { BattleOrderInfo::orderName(BattleOrderInfo::Hold),          BattleOrderInfo::Hold  },
      { BattleOrderInfo::orderName(BattleOrderInfo::Move),          BattleOrderInfo::Move  },
      { BattleOrderInfo::orderName(BattleOrderInfo::RestRally),     BattleOrderInfo::RestRally  },
      { BattleOrderInfo::orderName(BattleOrderInfo::Attach),        BattleOrderInfo::Attach  },
      { BattleOrderInfo::orderName(BattleOrderInfo::Detach),        BattleOrderInfo::Detach  },
//        { BattleOrderInfo::orderName(BattleOrderInfo::AttachLeader),  BattleOrderInfo::AttachLeader  },
//        { BattleOrderInfo::orderName(BattleOrderInfo::DetachLeader),  BattleOrderInfo::DetachLeader  },
      { BattleOrderInfo::orderName(BattleOrderInfo::Bombard),       BattleOrderInfo::Bombard  },
      { BattleOrderInfo::orderName(BattleOrderInfo::Rout),          BattleOrderInfo::Rout  },
      { 0, 0 }
  };

   d_orderTypeCombo.reset();

   /*
   Fill in ordertype combo for Divisional CP
   */
   if(d_cp->getRank().sameRank(Rank_Division)) {

      d_orderTypeCombo.add(s_orderTypeItems[BattleOrderInfo::Hold].d_text, BattleOrderInfo::Hold);
      d_orderTypeCombo.add(s_orderTypeItems[BattleOrderInfo::Move].d_text, BattleOrderInfo::Move);
      d_orderTypeCombo.add(s_orderTypeItems[BattleOrderInfo::RestRally].d_text, BattleOrderInfo::RestRally);
      // context sensitive attatch order
//    if(!d_cp->attached()) d_orderTypeCombo.add(s_orderTypeItems[BattleOrderInfo::Attach].d_text, BattleOrderInfo::Attach);
//    else d_orderTypeCombo.add(s_orderTypeItems[BattleOrderInfo::Detach].d_text, BattleOrderInfo::Detach);

         // RefBattleCP hq = BobUtility::getAttachedLeader(const_cast<BattleData*>(reinterpret_cast<const BattleData*>(d_batData)), const_cast<BattleCP*>(d_cp));
#ifdef DEBUG
      RefBattleCP hq = BobUtility::getAttachedLeader(const_cast<BattleData*>(d_batData()), const_cast<BattleCP*>(d_cp));
#else
      RefBattleCP hq = BobUtility::getAttachedLeader(const_cast<BattleData*>(d_batData), const_cast<BattleCP*>(d_cp));
#endif
        if(hq != NoBattleCP)
         d_orderTypeCombo.add(s_orderTypeItems[BattleOrderInfo::Detach].d_text, BattleOrderInfo::Detach);

      // context sensitive bombard order
      if(d_cp->generic()->isArtillery()) d_orderTypeCombo.add(s_orderTypeItems[BattleOrderInfo::Bombard].d_text, BattleOrderInfo::Bombard);
      d_orderTypeCombo.add(s_orderTypeItems[BattleOrderInfo::Rout].d_text, BattleOrderInfo::Rout);

//    LPARAM val = static_cast<LPARAM>(d_order->d_mode);

//    if(d_order->d_mode > BattleOrderInfo::Attach) val--;
//    if(d_order->d_mode >= BattleOrderInfo::Bombard && !d_cp->generic()->isArtillery() ) val--;

      d_orderTypeCombo.setValue(static_cast<LPARAM>(d_order->d_mode));
   }

   /*
   Fill in ordertype combo for HQ CP
   */
   else {

      d_orderTypeCombo.add(s_orderTypeItems[BattleOrderInfo::Hold].d_text, BattleOrderInfo::Hold);
      d_orderTypeCombo.add(s_orderTypeItems[BattleOrderInfo::Move].d_text, BattleOrderInfo::Move);
      // context sensitive attatch order
      if(!d_cp->attached()) d_orderTypeCombo.add(s_orderTypeItems[BattleOrderInfo::Attach].d_text, BattleOrderInfo::Attach);
      else d_orderTypeCombo.add(s_orderTypeItems[BattleOrderInfo::Detach].d_text, BattleOrderInfo::Detach);

      d_orderTypeCombo.add(s_orderTypeItems[BattleOrderInfo::Rout].d_text, BattleOrderInfo::Rout);
      LPARAM val;
      if(d_order->d_mode >= BattleOrderInfo::RestRally) val = static_cast<LPARAM>(d_order->d_mode) -1;
      else val = val = static_cast<LPARAM>(d_order->d_mode);

      d_orderTypeCombo.setValue(val);
   }






  // Aggression
  // Note: get Strings from resource
  static CComboInit s_aggressionItems[] = {
          { BattleOrderInfo::aggressionName(BattleOrderInfo::AvoidContact),      BattleOrderInfo::AvoidContact    },
          { BattleOrderInfo::aggressionName(BattleOrderInfo::ProbeDelay),        BattleOrderInfo::ProbeDelay    },
          { BattleOrderInfo::aggressionName(BattleOrderInfo::LimitedEngagement), BattleOrderInfo::LimitedEngagement    },
          { BattleOrderInfo::aggressionName(BattleOrderInfo::EngageAtAllCost),   BattleOrderInfo::EngageAtAllCost    },
          { 0, 0 }
  };

  d_aggressionCombo.reset();
  d_aggressionCombo.add(s_aggressionItems);
  d_aggressionCombo.setValue(static_cast<LPARAM>(d_order->d_aggression));


/*

        These following initializations are from runMove
*/




        // SP Formation
        // Note: get Strings from resource
        static CComboResInit s_spDeployItems[SP_Formation_HowMany] = {
                { IDS_BAT_SP_FORMATION_1,         SP_MarchFormation    },
                { IDS_BAT_SP_FORMATION_2,        SP_ColumnFormation     },
                { IDS_BAT_SP_FORMATION_3, SP_ClosedColumnFormation },
                { IDS_BAT_SP_FORMATION_4,          SP_LineFormation },
                { IDS_BAT_SP_FORMATION_5,        SP_SquareFormation }
//        { 0, 0 }
        };

        static CComboResInit s_spArtDeployItems[] = {
                { IDS_BAT_ART_FORMATION_1,         SP_LimberedFormation    },
                { IDS_BAT_ART_FORMATION_2,      SP_UnlimberedFormation  }
        };


      /*
      Fill out SP Formation Combo for Division
      */

      d_spDeployCombo.reset();

      if(d_cp->getRank().sameRank(Rank_Division)) {

         for(SPFormation i = SP_MarchFormation; i < SP_Formation_HowMany; INCREMENT(i)) {

            BasicUnitType::value t = (d_cp->generic()->isInfantry()) ? BasicUnitType::Infantry :
               (d_cp->generic()->isCavalry()) ? BasicUnitType::Cavalry : BasicUnitType::Artillery;

            // only add if can deploy like this
            if(B_OrderUtil::canSPDeployThisWay(d_batData, i, t)) {

               const char* text = InGameText::get(
                  (t != BasicUnitType::Artillery) ?
                     s_spDeployItems[i].d_textID :
                     s_spArtDeployItems[i].d_textID);

               // only add if a Divisional CP
               d_spDeployCombo.add(text, i);
            }
         }

         d_spDeployCombo.setValue(static_cast<LPARAM>(d_order->d_spFormation));
      }

      /*
      Fill out SP Formation Combo for HQ CP
      */

      else {
         d_spDeployCombo.add(InGameText::get(IDS_None), SP_MarchFormation);
         d_spDeployCombo.setValue(0);
      }



      /*
      Fill out SP Deployment Combo for Division
      */

      d_divDeployCombo.reset();

      if(d_cp->getRank().sameRank(Rank_Division)) {

         // Divisional Deployment
         // Note: get Strings from resource
         static CComboResInit s_divDeployItems[] = {
            { IDS_BAT_DIV_FORMATION_1,    CPF_March    },
            { IDS_BAT_DIV_FORMATION_2,   CPF_Massed   },
            { IDS_BAT_DIV_FORMATION_3, CPF_Deployed },
            { IDS_BAT_DIV_FORMATION_4, CPF_Extended },
            { InGameText::Null, 0 }
         };


         d_divDeployCombo.add(s_divDeployItems);
         d_divDeployCombo.setValue(static_cast<LPARAM>(d_order->d_divFormation));
      }

      /*
      Fill out SP Deployment Combo for HQ CP
      */

      else {
         d_divDeployCombo.add(InGameText::get(IDS_None), CPF_March);
         d_divDeployCombo.setValue(0);
      }





        for(ID id = ID_FirstCheckBox; id < (ID_LastCheckBox + 1); INCREMENT(id))
        {
         const CtrlInfo& ci = controlInfo(id);

            CustomButton cb(getHWND(), id);
             cb.setCheck(idToCheck(static_cast<ID>(id)));
        }




         return True;
      }

        /*

        Otherwise 'W' is down, & we're plotting waypoints...

        */

        return False;
}












void BM_Window::updateWindowStatus()
{
        sizeWindow();

        CCombo* s_combos[(ID_LastCombo - ID_FirstCombo) + 1] = {
         &d_orderTypeCombo,
         &d_aggressionCombo,
         &d_divDeployCombo,
         &d_spDeployCombo
  };
}




void BM_Window::sizeWindow()
{
  const int cy = d_cy; //(d_owner->mode() == BUI_UnitInterface::Order) ? d_cy :
                         //                 (d_owner->mode() == BUI_UnitInterface::Moving) ? d_moveCY : d_deployCY;

  const int dbX = ScreenBase::dbX();
  const int dbY = ScreenBase::dbY();
  const int baseX = ScreenBase::baseX();
  const int baseY = ScreenBase::baseY();
  const int bOffset = (3 * dbY) / baseY;

  SetWindowPos(getHWND(), HWND_TOP, 0, 0, d_cx, cy, SWP_NOMOVE);
}

void BM_Window::hide()
{
  ShowWindow(getHWND(), SW_HIDE);
}

/*---------------------------------------------------------
 * Client access
 */

BattleOrder_Int::BattleOrder_Int(BUI_UnitInterface* owner, RCPBattleData batData, BattleOrder& order) :
  d_wind(new BM_Window(owner, batData, order))
{
  ASSERT(d_wind);
}

BattleOrder_Int::~BattleOrder_Int()
{
  // destroy();
  delete d_wind;
}

bool BattleOrder_Int::run(const PixelPoint& p, const BUD_Info& info, BattleOrderInfo* order, unsigned int flags)
{
  ASSERT(d_wind);
  return (d_wind) ? d_wind->run(p, info, order) : False;
}

bool BattleOrder_Int::run(const PixelPoint& p, const BUD_Info& info, BattleOrderInfo* order)
{
  return run(p, info, order, CtrlInfo::SmallMode);
}

void BattleOrder_Int::hide()
{
  ASSERT(d_wind);
  ShowWindow(d_wind->getHWND(), SW_HIDE);
}


BattleOrder *
BattleOrder_Int::getOrder(void) {
    return d_wind->getOrder();
}


void BattleOrder_Int::destroy()
{
  if(d_wind)
  {
         // DestroyWindow(d_wind->getHWND());
         delete d_wind;
         d_wind = 0;
  }
}



/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.3  2001/11/26 10:34:41  greenius
 * Current Directory detection improved
 *
 * Battlegame sound system initialised properly
 *
 * BattleAI Message queue problems fixed
 *
 * Order of Battles loading
 *
 * HexMap uses reference counted units
 *
 * BattleEditor open-file dialogs use wrapped classes
 *
 * PathFind compiler warnings removed
 *
 * Orphan detection in BattleOB detected and removed
 *
 * Revision 1.2  2001/06/18 08:25:44  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
