/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
#include "stdinc.hpp"
#include "bu_menu.hpp"
#include "batres.h"
#include "wind.hpp"
#include "scenario.hpp"
#include "app.hpp"
#include "fonts.hpp"
#include "wmisc.hpp"
#include "cmenu.hpp"
#include "scn_img.hpp"
#include "scrnbase.hpp"
#include "bu_dial.hpp"
#include "bob_cp.hpp"
#include "control.hpp"
#include "batsound.hpp"
#include "resstr.hpp"

using namespace BOB_Definitions;

/*----------------------------------------------------------
 * Unit Menu Implementation
 */

class BUnitMenu_Imp : public WindowBaseND {
    BUI_UnitInterface* d_owner;
    HWND d_hParent;
    BattleWindowsInterface* d_batWind;
    const BattleData * d_batData;
    CustomMenu d_menu;

    HMENU d_topMenu;
    HMENU d_subMenu;
    HMENU d_formationMenu;
    HMENU d_deploymentMenu;
    HMENU d_manueverMenu;

    BattleOrder& d_order;
    CRefBattleCP d_cpi;

    PixelPoint d_point;

  public:
    BUnitMenu_Imp(BUI_UnitInterface* owner, HWND hParent, BattleWindowsInterface* batWind,
       RCPBattleData batData, BattleOrder& order);
    ~BUnitMenu_Imp();

    void run(const CRefBattleCP& cp, const PixelPoint& p);

    void destroy()
    {
      d_menu.destroy();
      DestroyWindow(getHWND());
    }

  private:
    LRESULT procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);

    BOOL onCreate(HWND hWnd, CREATESTRUCT* lpCreateStruct);
    void onDestroy(HWND hWnd) {}
    void onCommand(HWND hWnd, int id, HWND hwndCtl, UINT codeNotify);
    void onDrawItem(HWND hwnd, const DRAWITEMSTRUCT * lpDrawItem);
    void onMeasureItem(HWND hwnd, MEASUREITEMSTRUCT* lpMeasureItem);

    void showInfo();
    void showOrders();
    void showDeployment();
    void updateMenu();
//  Boolean canGiveOnArrival(Orders::AdvancedOrders::OrderOnArrival o);
#if 0
    Boolean idToOption(int id);
    void setOrder(int id);
    void setAggression(int id);
    void setOption(int id);
    void setMoveHow(int id);
    void setPosture(int id);
    void setOnArrival(int id);
#endif
};

BUnitMenu_Imp::BUnitMenu_Imp(BUI_UnitInterface* owner, HWND hParent,
   BattleWindowsInterface* batWind, RCPBattleData batData, BattleOrder& order) :
  d_owner(owner),
  d_hParent(hParent),
  d_batWind(batWind),
  d_batData(batData),
  d_cpi(NoBattleCP),
  d_order(order)
{
  HWND hWnd = createWindow(
      0,
      // GenericClass::className(),
        NULL,
      WS_POPUP,
      0, 0, 0, 0,
      hParent, NULL       // , APP::instance()
  );

  ASSERT(hWnd != NULL);
}

BUnitMenu_Imp::~BUnitMenu_Imp()
{
    selfDestruct();
}

LRESULT BUnitMenu_Imp::procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
   switch(msg)
   {
      HANDLE_MSG(hWnd, WM_CREATE,   onCreate);
      HANDLE_MSG(hWnd, WM_COMMAND,  onCommand);
      HANDLE_MSG(hWnd, WM_DESTROY,  onDestroy);

      default:
        return defProc(hWnd, msg, wParam, lParam);
   }
}

BOOL BUnitMenu_Imp::onCreate(HWND hWnd, CREATESTRUCT* lpCreateStruct)
{
  const LONG dbY = ScreenBase::dbY();
  const LONG baseY = ScreenBase::baseY();

  /*
   * Set menu data
   */

  d_topMenu = 0;
  d_subMenu = 0;
  d_formationMenu = 0;
  d_deploymentMenu = 0;
  d_manueverMenu = 0;

#if 0

  d_windowsMenu = CreatePopupMenu();


  CustomMenuData md;
  md.d_type = CustomMenuData::CMT_Popup;
  md.d_menuID = -1;//MENU_BUNITORDERPOPUP;
  md.d_fillDib = scenario->getScenarioBkDIB(Scenario::BackPatterns::CherryWoodBackground);
  md.d_checkImages = ScenarioImageLibrary::get(ScenarioImageLibrary::CustomMenuImages);
  md.d_hCommand = hWnd;
  md.d_hParent = d_hParent;
  md.d_borderColors = scenario->getBorderColors();
  scenario->getColour("MapInsert", md.d_color);
  scenario->getColour("Orange", md.d_hColor);

  /*
   * Set menu fonts (one normal, the other underlined
   */

  LogFont lf;
  lf.height((8 * dbY) / baseY);
  lf.weight(FW_MEDIUM);
  lf.face(scenario->fontName(Font_Bold));

  Font font;
  font.set(lf);
  md.d_hFont = font;

  lf.underline(true);
  font.set(lf);
  md.d_hULFont = font;

  /*
   * initialize menu
   */

  d_menu.init(md, d_topMenu);

#endif

  return TRUE;
}

void BUnitMenu_Imp::onCommand(HWND hWnd, int id, HWND hwndCtl, UINT codeNotify)
{
  switch(id)
  {
    case IDM_BAT_ORDERS: {
      showOrders();
      d_batData->soundSystem()->triggerSound(SOUNDTYPE_MENUPOPUP);
      break;
   }
    case IDM_BAT_DEPLOY: {
      showDeployment();
      d_batData->soundSystem()->triggerSound(SOUNDTYPE_MENUPOPUP);
      break;
   }

    case IDM_BAT_INFO: {
      showInfo();
      d_batData->soundSystem()->triggerSound(SOUNDTYPE_MENUPOPUP);
      break;
   }

    case IDM_BAT_ORDERHOLD: {
      d_order.mode(BattleOrderInfo::Hold);
      d_owner->sendOrder();
      d_batData->soundSystem()->triggerSound(SOUNDTYPE_MENUSELECT);
      break;
   }

    case IDM_BAT_ORDERMOVE: {
      d_order.mode(BattleOrderInfo::Move);
      d_owner->mode(BUI_UnitInterface::Normal);
      d_owner->sendOrder();
      d_batData->soundSystem()->triggerSound(SOUNDTYPE_MENUSELECT);
      break;
   }

    case IDM_BAT_ORDERRESTRALLY: {
      d_order.mode(BattleOrderInfo::RestRally);
      d_owner->sendOrder();
      d_batData->soundSystem()->triggerSound(SOUNDTYPE_MENUSELECT);
      break;
   }

    case IDM_BAT_ORDERATTACH: {
      d_order.mode(BattleOrderInfo::Attach);
      d_owner->sendOrder();
      d_batData->soundSystem()->triggerSound(SOUNDTYPE_MENUSELECT);
      break;
   }

    case IDM_BAT_ORDERDETACH: {
      d_order.mode(BattleOrderInfo::Detach);
      d_owner->sendOrder();
      d_batData->soundSystem()->triggerSound(SOUNDTYPE_MENUSELECT);
      break;
   }

#if 0
    case IDM_BAT_ORDERATTACHLEADER:
      d_order.mode(BattleOrderInfo::AttachLeader);
      d_owner->sendOrder();
      break;

    case IDM_BAT_ORDERDETACHLEADER:
      d_order.mode(BattleOrderInfo::DetachLeader);
      d_owner->sendOrder();
      break;
#endif

    case IDM_BAT_ORDERBOMBARD: {

      d_order.mode(BattleOrderInfo::Bombard);
//    d_owner->sendOrder();
      d_menu.hide();
      d_owner->mode(BUI_UnitInterface::Targetting);

      d_batData->soundSystem()->triggerSound(SOUNDTYPE_MENUSELECT);
      break;
   }

    case IDM_BAT_ORDERROUT: {
      d_order.mode(BattleOrderInfo::Rout);
      d_owner->sendOrder();
      d_batData->soundSystem()->triggerSound(SOUNDTYPE_MENUSELECT);
      break;
   }

    case IDM_BAT_AVOID: {
      d_order.aggression(BattleOrderInfo::AvoidContact);
      d_owner->sendOrder();
      d_batData->soundSystem()->triggerSound(SOUNDTYPE_MENUSELECT);
      break;
   }

    case IDM_BAT_PROBEDELAY: {
      d_order.aggression(BattleOrderInfo::ProbeDelay);
      d_owner->sendOrder();
      d_batData->soundSystem()->triggerSound(SOUNDTYPE_MENUSELECT);
      break;
   }

    case IDM_BAT_LIMITED: {
      d_order.aggression(BattleOrderInfo::LimitedEngagement);
      d_owner->sendOrder();
      d_batData->soundSystem()->triggerSound(SOUNDTYPE_MENUSELECT);
      break;
   }

    case IDM_BAT_ATALLCOST: {
      d_order.aggression(BattleOrderInfo::EngageAtAllCost);
      d_owner->sendOrder();
      d_batData->soundSystem()->triggerSound(SOUNDTYPE_MENUSELECT);
      break;
   }

    case IDM_BAT_OFFENSIVE: {
      d_order.posture(BattleOrderInfo::Offensive);
      d_owner->sendOrder();
      d_batData->soundSystem()->triggerSound(SOUNDTYPE_MENUSELECT);
      break;
   }

    case IDM_BAT_DEFENSIVE: {
      d_order.posture(BattleOrderInfo::Defensive);
      d_owner->sendOrder();
      d_batData->soundSystem()->triggerSound(SOUNDTYPE_MENUSELECT);
      break;
   }

    case IDM_BAT_DEPLOYMARCH: {
      d_order.divFormation(CPF_March);
      d_order.whatOrder().add(WhatOrder::CPFormation);
      d_owner->sendOrder();
      d_batData->soundSystem()->triggerSound(SOUNDTYPE_MENUSELECT);
      break;
   }

    case IDM_BAT_DEPLOYMASSED: {
      d_order.divFormation(CPF_Massed);
      d_order.whatOrder().add(WhatOrder::CPFormation);
      d_owner->sendOrder();
      d_batData->soundSystem()->triggerSound(SOUNDTYPE_MENUSELECT);
      break;
   }

    case IDM_BAT_DEPLOYDEPLOYED: {
      d_order.divFormation(CPF_Deployed);
      d_order.whatOrder().add(WhatOrder::CPFormation);
      d_owner->sendOrder();
      d_batData->soundSystem()->triggerSound(SOUNDTYPE_MENUSELECT);
      break;
   }

    case IDM_BAT_DEPLOYEXTENDED: {
      d_order.divFormation(CPF_Extended);
      d_order.whatOrder().add(WhatOrder::CPFormation);
      d_owner->sendOrder();
      d_batData->soundSystem()->triggerSound(SOUNDTYPE_MENUSELECT);
      break;
   }

    case IDM_BAT_DEPLOYRIGHT: {
      d_order.deployHow(CPD_DeployRight);
      d_owner->sendOrder();
      d_batData->soundSystem()->triggerSound(SOUNDTYPE_MENUSELECT);
      break;
   }

    case IDM_BAT_DEPLOYLEFT: {
      d_order.deployHow(CPD_DeployLeft);
      d_owner->sendOrder();
      d_batData->soundSystem()->triggerSound(SOUNDTYPE_MENUSELECT);
      break;
   }

    case IDM_BAT_DEPLOYCENTER: {
      d_order.deployHow(CPD_DeployCenter);
      d_owner->sendOrder();
      d_batData->soundSystem()->triggerSound(SOUNDTYPE_MENUSELECT);
      break;
   }

    case IDM_BAT_MARCHFORMATION: {
      d_order.spFormation(SP_MarchFormation);
      d_order.whatOrder().add(WhatOrder::SPFormation);
      d_owner->sendOrder();
      d_batData->soundSystem()->triggerSound(SOUNDTYPE_MENUSELECT);
      break;
   }

    case IDM_BAT_COLUMNFORMATION: {
      d_order.spFormation(SP_ColumnFormation);
      d_order.whatOrder().add(WhatOrder::SPFormation);
      d_owner->sendOrder();
      d_batData->soundSystem()->triggerSound(SOUNDTYPE_MENUSELECT);
      break;
   }

    case IDM_BAT_CLOSEDFORMATION: {
      d_order.spFormation(SP_ClosedColumnFormation);
      d_order.whatOrder().add(WhatOrder::SPFormation);
      d_owner->sendOrder();
      d_batData->soundSystem()->triggerSound(SOUNDTYPE_MENUSELECT);
      break;
   }

    case IDM_BAT_LINEFORMATION: {
      d_order.spFormation(SP_LineFormation);
      d_order.whatOrder().add(WhatOrder::SPFormation);
      d_owner->sendOrder();
      d_batData->soundSystem()->triggerSound(SOUNDTYPE_MENUSELECT);
      break;
   }

    case IDM_BAT_SQUAREFORMATION: {
      d_order.spFormation(SP_SquareFormation);
      d_order.whatOrder().add(WhatOrder::SPFormation);
      d_owner->sendOrder();
      d_batData->soundSystem()->triggerSound(SOUNDTYPE_MENUSELECT);
      break;
   }

    case IDM_BAT_UNLIMBEREDFORMATION: {
      d_order.spFormation(SP_UnlimberedFormation);
      d_order.whatOrder().add(WhatOrder::SPFormation);
      d_owner->sendOrder();
      d_batData->soundSystem()->triggerSound(SOUNDTYPE_MENUSELECT);
      break;
   }

    case IDM_BAT_LIMBEREDFORMATION: {
      d_order.spFormation(SP_LimberedFormation);
      d_order.whatOrder().add(WhatOrder::SPFormation);
      d_owner->sendOrder();
      d_batData->soundSystem()->triggerSound(SOUNDTYPE_MENUSELECT);
      break;
   }

    case IDM_BAT_FORWARD: {
      d_order.manuever(CPM_Forward);
      d_order.whatOrder().add(WhatOrder::Manuever);
      d_owner->sendOrder();
      d_batData->soundSystem()->triggerSound(SOUNDTYPE_MENUSELECT);
      break;
   }

    case IDM_BAT_WHEELRIGHT: {
      d_order.manuever(CPM_WheelR);
      d_order.whatOrder().add(WhatOrder::Manuever);
      d_owner->sendOrder();
      d_batData->soundSystem()->triggerSound(SOUNDTYPE_MENUSELECT);
      break;
   }

    case IDM_BAT_WHEELLEFT: {
      d_order.manuever(CPM_WheelL);
      d_order.whatOrder().add(WhatOrder::Manuever);
      d_owner->sendOrder();
      d_batData->soundSystem()->triggerSound(SOUNDTYPE_MENUSELECT);
      break;
   }

    case IDM_BAT_RWHEELRIGHT: {
      d_order.manuever(CPM_RWheelR);
      d_order.whatOrder().add(WhatOrder::Manuever);
      d_owner->sendOrder();
      d_batData->soundSystem()->triggerSound(SOUNDTYPE_MENUSELECT);
      break;
   }

    case IDM_BAT_RWHEELLEFT: {
      d_order.manuever(CPM_RWheelL);
      d_order.whatOrder().add(WhatOrder::Manuever);
      d_owner->sendOrder();
      d_batData->soundSystem()->triggerSound(SOUNDTYPE_MENUSELECT);
      break;
   }

    case IDM_BAT_SIDESTEPLEFT: {
      d_order.manuever(CPM_SideStepL);
      d_order.whatOrder().add(WhatOrder::Manuever);
      d_owner->sendOrder();
      d_batData->soundSystem()->triggerSound(SOUNDTYPE_MENUSELECT);
      break;
   }

    case IDM_BAT_SIDESTEPRIGHT: {
      d_order.manuever(CPM_SideStepR);
      d_order.whatOrder().add(WhatOrder::Manuever);
      d_owner->sendOrder();
      d_batData->soundSystem()->triggerSound(SOUNDTYPE_MENUSELECT);
      break;
   }

    case IDM_BAT_ABOUTFACE: {
      d_order.manuever(CPM_AboutFace);
      d_order.whatOrder().add(WhatOrder::Manuever);
      d_owner->sendOrder();
      d_batData->soundSystem()->triggerSound(SOUNDTYPE_MENUSELECT);
      break;
   }

  }

  d_menu.hide();
}

void BUnitMenu_Imp::showInfo()
{
  d_owner->showInfo();
}

void BUnitMenu_Imp::showOrders()
{
  d_owner->showOrders();
}

void BUnitMenu_Imp::showDeployment()
{
  d_owner->showDeployment();
}

#if 0
void BUnitMenu_Imp::setOrder(int id)
{
  Orders::Type::Value order;
  if(id == IDM_UTB_HOLD)
    order = Orders::Type::Hold;
  else if(id == IDM_UTB_RESTRALLY)
    order = Orders::Type::RestRally;
  else if(id == IDM_UTB_DETACHLEADER)
    order = Orders::Type::Leader;
  else if(id == IDM_UTB_GARRISON)
    order = Orders::Type::Garrison;
  else if(id == IDM_UTB_INSTALLSUPPLY)
    order = Orders::Type::InstallSupply;
  else
  {
    ASSERT(id == IDM_UTB_UPGRADEFORT);
    order = Orders::Type::UpgradeFort;
  }

  d_order.setType(order);
  d_owner->sendOrder(d_cpi);
}

void BUnitMenu_Imp::setAggression(int id)
{
  Orders::Aggression::Value aggress;
  if(id == IDM_UTB_TIMID)
    aggress = Orders::Aggression::Timid;
  else if(id == IDM_UTB_DEFEND)
    aggress = Orders::Aggression::DefendSmall;
  else if(id == IDM_UTB_ATTACKSMALL)
    aggress = Orders::Aggression::AttackSmall;
  else
  {
    ASSERT(id == IDM_UTB_ATTACK);
    aggress = Orders::Aggression::Attack;
  }

  d_order.setAggressLevel(aggress);
  d_owner->sendOrder(d_cpi);
}

void BUnitMenu_Imp::setOption(int id)
{
  if(id == IDM_UTB_MARCHTO)
    d_order.setSoundGuns(!d_order.getSoundGuns());
  else if(id == IDM_UTB_PURSUE)
    d_order.setPursue(!d_order.getPursue());
  else if(id == IDM_UTB_SIEGEACTIVE)
    d_order.setSiegeActive(!d_order.getSiegeActive());
  else if(id == IDM_UTB_AUTOSTORM)
    ;
  else if(id == IDM_UTB_DROPOFFGARRISON)
    ;

  d_owner->sendOrder(d_cpi);
}

void BUnitMenu_Imp::setMoveHow(int id)
{
  Orders::AdvancedOrders::MoveHow mh;

  if(id == IDM_UTB_EASYMARCH)
    mh = Orders::AdvancedOrders::Easy;
  else if(id == IDM_UTB_NORMALMARCH)
    mh = Orders::AdvancedOrders::Normal;
  else
  {
    ASSERT(id == IDM_UTB_FORCEMARCH);
    mh = Orders::AdvancedOrders::Force;
  }

  d_order.setMoveHow(mh);
  d_owner->sendOrder(d_cpi);
}

void BUnitMenu_Imp::setPosture(int id)
{
  Orders::Posture::Type p = (id == IDM_UTB_OFFENSIVE) ?
     Orders::Posture::Offensive : Orders::Posture::Defensive;

  d_order.setPosture(p);
  d_owner->sendOrder(d_cpi);
}

void BUnitMenu_Imp::setOnArrival(int id)
{
  Orders::Type::Value oa;

  if(id == IDM_UTB_HOLD_OA)
    oa = Orders::Type::Hold;
  else if(id == IDM_UTB_RESTRALLY_OA)
    oa = Orders::Type::RestRally;
  else if(id == IDM_UTB_GARRISON_OA)
    oa = Orders::Type::Garrison;
  else
  {
    ASSERT(id == IDM_UTB_ATTACH_OA);
    oa = Orders::Type::Attach;
  }

  d_order.setOrderOnArrival(oa);
  d_owner->sendOrder(d_cpi);
}

#endif

void BUnitMenu_Imp::run(const CRefBattleCP& cp, const PixelPoint& pt)
{
  d_cpi = cp;
  d_point = pt;

  d_order = cp->getCurrentOrder();

  updateMenu();
  d_menu.run(pt);
}




/*

  Recreate menu items dependent upon selected CP

*/

void
BUnitMenu_Imp::updateMenu(void) {

  ASSERT(d_cpi != NoBattleCP);


  CustomMenuData md;
  md.d_type = CustomMenuData::CMT_Popup;
  md.d_menuID = -1;//MENU_BUNITORDERPOPUP;
  md.d_fillDib = scenario->getScenarioBkDIB(Scenario::BackPatterns::CherryWoodBackground);
  md.d_checkImages = ScenarioImageLibrary::get(ScenarioImageLibrary::CustomMenuImages);
  md.d_hCommand = hWnd;
  md.d_hParent = d_hParent;
  md.d_borderColors = scenario->getBorderColors();
  scenario->getColour("MapInsert", md.d_color);
  scenario->getColour("Orange", md.d_hColor);

   /*
   * Set menu fonts (one normal, the other underlined
   */
  const LONG dbY = ScreenBase::dbY();
  const LONG baseY = ScreenBase::baseY();

  LogFont lf;
  lf.height((8 * dbY) / baseY);
  lf.weight(FW_MEDIUM);
  lf.face(scenario->fontName(Font_Bold));

  Font font;
  font.set(lf);
  md.d_hFont = font;

  lf.underline(true);
  font.set(lf);
  md.d_hULFont = font;

   /*
   * initialize menus
   */

  if(d_topMenu) DestroyMenu(d_topMenu);
  if(d_subMenu) DestroyMenu(d_subMenu);
  if(d_formationMenu) DestroyMenu(d_formationMenu);
  if(d_deploymentMenu) DestroyMenu(d_deploymentMenu);
  if(d_manueverMenu) DestroyMenu(d_manueverMenu);

  // create the top menu
  d_topMenu = CreatePopupMenu();
  d_subMenu = CreatePopupMenu();
  d_formationMenu = CreatePopupMenu();
  d_deploymentMenu = CreatePopupMenu();
  d_manueverMenu = CreatePopupMenu();

  int menupos = 1;
  int submenupos;

  // empty item to link to subMenu
  MENUITEMINFO minfo;
  minfo.cbSize = sizeof(MENUITEMINFO);

  minfo.fMask = MIIM_ID | MIIM_STATE | MIIM_SUBMENU | MIIM_TYPE;
  minfo.fType = MFT_STRING;
  minfo.fState = MFS_ENABLED | MFS_UNCHECKED | MFS_UNHILITE;
  minfo.wID = 0;
  minfo.hSubMenu = d_subMenu;
  minfo.dwTypeData = "Menu Header";
  minfo.cch = strlen(minfo.dwTypeData);

  InsertMenuItem(d_topMenu, 1, TRUE, &minfo);


  // orders item
  if(GamePlayerControl::canControl(d_cpi->getSide()) && (!d_cpi->fleeingTheField()) ) {

     minfo.fMask = MIIM_ID | MIIM_STATE | MIIM_SUBMENU | MIIM_TYPE;
     minfo.fType = MFT_STRING;
     minfo.fState = MFS_ENABLED | MFS_UNCHECKED | MFS_UNHILITE;
     minfo.wID = IDM_BAT_ORDERS;
     minfo.hSubMenu = NULL;
     minfo.dwTypeData = const_cast<char*>(InGameText::get(IDS_UI_ORDERS));
     minfo.cch = strlen(minfo.dwTypeData);

     InsertMenuItem(d_subMenu, menupos, TRUE, &minfo);
     menupos++;
  }

  // unit info
  minfo.fMask = MIIM_ID | MIIM_STATE | MIIM_SUBMENU | MIIM_TYPE;
  minfo.fType = MFT_STRING;
  minfo.fState = MFS_ENABLED | MFS_UNCHECKED | MFS_UNHILITE;
  minfo.wID = IDM_BAT_INFO;
  minfo.hSubMenu = NULL;
  minfo.dwTypeData = const_cast<char*>(InGameText::get(IDS_UnitInfo));
  minfo.cch = strlen(minfo.dwTypeData);

  InsertMenuItem(d_subMenu, menupos, TRUE, &minfo);
  menupos++;

  // hold - if not already on hold
  if(GamePlayerControl::canControl(d_cpi->getSide()) && d_order.mode() != BattleOrderInfo::Hold && (!d_cpi->fleeingTheField())) {

     // hold order
     minfo.fMask = MIIM_ID | MIIM_STATE | MIIM_SUBMENU | MIIM_TYPE;
     minfo.fType = MFT_STRING;
     minfo.fState = MFS_ENABLED | MFS_UNCHECKED | MFS_UNHILITE;
     minfo.wID = IDM_BAT_ORDERHOLD;
     minfo.hSubMenu = NULL;
     minfo.dwTypeData = const_cast<char*>(InGameText::get(IDS_ORDER_HOLD)); // "Hold";
     minfo.cch = strlen(minfo.dwTypeData);

     InsertMenuItem(d_subMenu, menupos, TRUE, &minfo);
     menupos++;
  }


  if(d_cpi->getRank().sameRank(Rank_Division)) {


     // bombard - if artillery
     if(GamePlayerControl::canControl(d_cpi->getSide()) && (d_cpi->generic()->isArtillery() || (d_cpi->nArtillery() > 0)) && (!d_cpi->fleeingTheField())) {

        minfo.fMask = MIIM_ID | MIIM_STATE | MIIM_SUBMENU | MIIM_TYPE;
        minfo.fType = MFT_STRING;
        minfo.fState = MFS_ENABLED | MFS_UNCHECKED | MFS_UNHILITE;
        minfo.wID = IDM_BAT_ORDERBOMBARD;
        minfo.hSubMenu = NULL;
        minfo.dwTypeData = const_cast<char*>(InGameText::get(IDS_Bombard));  // "Bombard";
        minfo.cch = strlen(minfo.dwTypeData);

        InsertMenuItem(d_subMenu, menupos, TRUE, &minfo);
        menupos++;
     }


     // seperator
     if(GamePlayerControl::canControl(d_cpi->getSide()) && (!d_cpi->fleeingTheField())) {

        minfo.fMask = MIIM_ID | MIIM_STATE | MIIM_SUBMENU | MIIM_TYPE;
        minfo.fType = MFT_SEPARATOR;
        minfo.fState = MFS_ENABLED | MFS_UNCHECKED | MFS_UNHILITE;
        minfo.wID = IDM_BAT_INFO;
        minfo.hSubMenu = NULL;
        minfo.dwTypeData = "seperator";
        minfo.cch = strlen(minfo.dwTypeData);

        InsertMenuItem(d_subMenu, menupos, TRUE, &minfo);
        menupos++;

        // formation
        minfo.fMask = MIIM_ID | MIIM_STATE | MIIM_SUBMENU | MIIM_TYPE;
        minfo.fType = MFT_STRING;
        minfo.fState = MFS_ENABLED | MFS_UNCHECKED | MFS_UNHILITE;
        minfo.wID = IDM_BAT_INFO;
        minfo.hSubMenu = d_formationMenu;
        minfo.dwTypeData = const_cast<char*>(InGameText::get(IDS_Formation));
        minfo.cch = strlen(minfo.dwTypeData);

        InsertMenuItem(d_subMenu, menupos, TRUE, &minfo);
        menupos++;

        // deployment
        minfo.fMask = MIIM_ID | MIIM_STATE | MIIM_SUBMENU | MIIM_TYPE;
        minfo.fType = MFT_STRING;
        minfo.fState = MFS_ENABLED | MFS_UNCHECKED | MFS_UNHILITE;
        minfo.wID = IDM_BAT_INFO;
        minfo.hSubMenu = d_deploymentMenu;
        minfo.dwTypeData = const_cast<char*>(InGameText::get(IDS_Deployment)); //"Deployment";
        minfo.cch = strlen(minfo.dwTypeData);

        InsertMenuItem(d_subMenu, menupos, TRUE, &minfo);
        menupos++;

        // manuever
        minfo.fMask = MIIM_ID | MIIM_STATE | MIIM_SUBMENU | MIIM_TYPE;
        minfo.fType = MFT_STRING;
        minfo.fState = MFS_ENABLED | MFS_UNCHECKED | MFS_UNHILITE;
        minfo.wID = IDM_BAT_INFO;
        minfo.hSubMenu = d_manueverMenu;
        minfo.dwTypeData = const_cast<char*>(InGameText::get(IDS_Maneuver));   // "Manuever";
        minfo.cch = strlen(minfo.dwTypeData);

        InsertMenuItem(d_subMenu, menupos, TRUE, &minfo);
        menupos++;



        /*
        Build up formation subMenu dependent upon unit-type
        I'm sure this could be done better - but there's no time to do it nicely now !!
        */

        if(d_cpi->generic()->isInfantry() || d_cpi->generic()->isCavalry() ) {

           submenupos = 1;

           // march
           minfo.fMask = MIIM_ID | MIIM_STATE | MIIM_SUBMENU | MIIM_TYPE;
           minfo.fType = MFT_STRING;
           minfo.fState = MFS_ENABLED | MFS_UNHILITE;
           minfo.wID = IDM_BAT_MARCHFORMATION;
           minfo.hSubMenu = NULL;
           minfo.dwTypeData = const_cast<char*>(InGameText::get(IDS_BAT_SP_FORMATION_1));  // "March";
           minfo.cch = strlen(minfo.dwTypeData);

           InsertMenuItem(d_formationMenu, submenupos, TRUE, &minfo);
           submenupos++;


           // column
           minfo.fMask = MIIM_ID | MIIM_STATE | MIIM_SUBMENU | MIIM_TYPE;
           minfo.fType = MFT_STRING;
           minfo.fState = MFS_ENABLED | MFS_UNHILITE;
           minfo.wID = IDM_BAT_COLUMNFORMATION;
           minfo.hSubMenu = NULL;
           minfo.dwTypeData = const_cast<char*>(InGameText::get(IDS_BAT_SP_FORMATION_2));  // "Column";
           minfo.cch = strlen(minfo.dwTypeData);

           InsertMenuItem(d_formationMenu, submenupos, TRUE, &minfo);
           submenupos++;

           // closed column
           minfo.fMask = MIIM_ID | MIIM_STATE | MIIM_SUBMENU | MIIM_TYPE;
           minfo.fType = MFT_STRING;
           minfo.fState = MFS_ENABLED | MFS_UNCHECKED | MFS_UNHILITE;
           minfo.wID = IDM_BAT_CLOSEDFORMATION;
           minfo.hSubMenu = NULL;
           minfo.dwTypeData = const_cast<char*>(InGameText::get(IDS_BAT_SP_FORMATION_3));  // "Closed Column";
           minfo.cch = strlen(minfo.dwTypeData);

           InsertMenuItem(d_formationMenu, submenupos, TRUE, &minfo);
           submenupos++;

           // line
           minfo.fMask = MIIM_ID | MIIM_STATE | MIIM_SUBMENU | MIIM_TYPE;
           minfo.fType = MFT_STRING;
           minfo.fState = MFS_ENABLED | MFS_UNCHECKED | MFS_UNHILITE;
           minfo.wID = IDM_BAT_LINEFORMATION;
           minfo.hSubMenu = NULL;
           minfo.dwTypeData = const_cast<char*>(InGameText::get(IDS_BAT_SP_FORMATION_4));  // "Line";
           minfo.cch = strlen(minfo.dwTypeData);

           InsertMenuItem(d_formationMenu, submenupos, TRUE, &minfo);
           submenupos++;

           // square formation for infantry
           if(d_cpi->generic()->isInfantry() ) {

              // square
              minfo.fMask = MIIM_ID | MIIM_STATE | MIIM_SUBMENU | MIIM_TYPE;
              minfo.fType = MFT_STRING;
              minfo.fState = MFS_ENABLED | MFS_UNCHECKED | MFS_UNHILITE;
              minfo.wID = IDM_BAT_SQUAREFORMATION;
              minfo.hSubMenu = NULL;
              minfo.dwTypeData = const_cast<char*>(InGameText::get(IDS_BAT_SP_FORMATION_5));     // "Square";
              minfo.cch = strlen(minfo.dwTypeData);

              InsertMenuItem(d_formationMenu, submenupos, TRUE, &minfo);
              submenupos++;
           }

        }


        else if(d_cpi->generic()->isArtillery() ) {

           submenupos = 1;

           // limbered
           minfo.fMask = MIIM_ID | MIIM_STATE | MIIM_SUBMENU | MIIM_TYPE;
           minfo.fType = MFT_STRING;
           minfo.fState = MFS_ENABLED | MFS_UNCHECKED | MFS_UNHILITE;
           minfo.wID = IDM_BAT_LIMBEREDFORMATION;
           minfo.hSubMenu = NULL;
           minfo.dwTypeData = const_cast<char*>(InGameText::get(IDS_BAT_ART_FORMATION_1));    // "Limbered";
           minfo.cch = strlen(minfo.dwTypeData);

           InsertMenuItem(d_formationMenu, submenupos, TRUE, &minfo);
           submenupos++;

           // un-limbered
           minfo.fMask = MIIM_ID | MIIM_STATE | MIIM_SUBMENU | MIIM_TYPE;
           minfo.fType = MFT_STRING;
           minfo.fState = MFS_ENABLED | MFS_UNCHECKED | MFS_UNHILITE;
           minfo.wID = IDM_BAT_UNLIMBEREDFORMATION;
           minfo.hSubMenu = NULL;
           minfo.dwTypeData = const_cast<char*>(InGameText::get(IDS_BAT_ART_FORMATION_2));    // "UnLimbered";
           minfo.cch = strlen(minfo.dwTypeData);

           InsertMenuItem(d_formationMenu, submenupos, TRUE, &minfo);
           submenupos++;
        }

        /*
        Build up deployment submenu
        */

        submenupos = 1;

        // march
        minfo.fMask = MIIM_ID | MIIM_STATE | MIIM_SUBMENU | MIIM_TYPE;
        minfo.fType = MFT_STRING;
        minfo.fState = MFS_ENABLED | MFS_UNCHECKED | MFS_UNHILITE;
        minfo.wID = IDM_BAT_DEPLOYMARCH;
        minfo.hSubMenu = NULL;
        minfo.dwTypeData = const_cast<char*>(InGameText::get(IDS_BAT_DIV_FORMATION_1));    // "March";
        minfo.cch = strlen(minfo.dwTypeData);

        InsertMenuItem(d_deploymentMenu, submenupos, TRUE, &minfo);
        submenupos++;

        // massed
        minfo.fMask = MIIM_ID | MIIM_STATE | MIIM_SUBMENU | MIIM_TYPE;
        minfo.fType = MFT_STRING;
        minfo.fState = MFS_ENABLED | MFS_UNCHECKED | MFS_UNHILITE;
        minfo.wID = IDM_BAT_DEPLOYMASSED;
        minfo.hSubMenu = NULL;
        minfo.dwTypeData = const_cast<char*>(InGameText::get(IDS_BAT_DIV_FORMATION_2)); // "Massed";
        minfo.cch = strlen(minfo.dwTypeData);

        InsertMenuItem(d_deploymentMenu, submenupos, TRUE, &minfo);
        submenupos++;

        // deployed
        minfo.fMask = MIIM_ID | MIIM_STATE | MIIM_SUBMENU | MIIM_TYPE;
        minfo.fType = MFT_STRING;
        minfo.fState = MFS_ENABLED | MFS_UNCHECKED | MFS_UNHILITE;
        minfo.wID = IDM_BAT_DEPLOYDEPLOYED;
        minfo.hSubMenu = NULL;
        minfo.dwTypeData = const_cast<char*>(InGameText::get(IDS_BAT_DIV_FORMATION_3)); // "Deployed";
        minfo.cch = strlen(minfo.dwTypeData);

        InsertMenuItem(d_deploymentMenu, submenupos, TRUE, &minfo);
        submenupos++;

        // extended
        minfo.fMask = MIIM_ID | MIIM_STATE | MIIM_SUBMENU | MIIM_TYPE;
        minfo.fType = MFT_STRING;
        minfo.fState = MFS_ENABLED | MFS_UNCHECKED | MFS_UNHILITE;
        minfo.wID = IDM_BAT_DEPLOYEXTENDED;
        minfo.hSubMenu = NULL;
        minfo.dwTypeData = const_cast<char*>(InGameText::get(IDS_BAT_DIV_FORMATION_4));    // "Extended";
        minfo.cch = strlen(minfo.dwTypeData);

        InsertMenuItem(d_deploymentMenu, submenupos, TRUE, &minfo);
        submenupos++;
      }
      /*
      Build up manuever subMenu
      */

      submenupos = 1;
#if 0 // CPM_Forward is not an actual manuever, rather its a dummy manuever used by the movement logic
      // should not be available to player
      // forward
      minfo.fMask = MIIM_ID | MIIM_STATE | MIIM_SUBMENU | MIIM_TYPE;
      minfo.fType = MFT_STRING;
      minfo.fState = MFS_ENABLED | MFS_UNCHECKED | MFS_UNHILITE;
      minfo.wID = IDM_BAT_FORWARD;
      minfo.hSubMenu = NULL;
      minfo.dwTypeData = const_cast<char*>(InGameText::get(IDS_BAT_MANUEVRE_1));  // "Forward";
      minfo.cch = strlen(minfo.dwTypeData);

      InsertMenuItem(d_manueverMenu, submenupos, TRUE, &minfo);
      submenupos++;
#endif
      // wheel right
      minfo.fMask = MIIM_ID | MIIM_STATE | MIIM_SUBMENU | MIIM_TYPE;
      minfo.fType = MFT_STRING;
      minfo.fState = MFS_ENABLED | MFS_UNCHECKED | MFS_UNHILITE;
      minfo.wID = IDM_BAT_WHEELRIGHT;
      minfo.hSubMenu = NULL;
      minfo.dwTypeData = const_cast<char*>(InGameText::get(IDS_BAT_MANUEVRE_2));  // "Wheel Right";
      minfo.cch = strlen(minfo.dwTypeData);

      InsertMenuItem(d_manueverMenu, submenupos, TRUE, &minfo);
      submenupos++;

      // wheel left
      minfo.fMask = MIIM_ID | MIIM_STATE | MIIM_SUBMENU | MIIM_TYPE;
      minfo.fType = MFT_STRING;
      minfo.fState = MFS_ENABLED | MFS_UNCHECKED | MFS_UNHILITE;
      minfo.wID = IDM_BAT_WHEELLEFT;
      minfo.hSubMenu = NULL;
      minfo.dwTypeData = const_cast<char*>(InGameText::get(IDS_BAT_MANUEVRE_3));  // "Wheel Left";
      minfo.cch = strlen(minfo.dwTypeData);

      InsertMenuItem(d_manueverMenu, submenupos, TRUE, &minfo);
      submenupos++;

      // reverse wheel right
      minfo.fMask = MIIM_ID | MIIM_STATE | MIIM_SUBMENU | MIIM_TYPE;
      minfo.fType = MFT_STRING;
      minfo.fState = MFS_ENABLED | MFS_UNCHECKED | MFS_UNHILITE;
      minfo.wID = IDM_BAT_RWHEELRIGHT;
      minfo.hSubMenu = NULL;
      minfo.dwTypeData = const_cast<char*>(InGameText::get(IDS_BAT_MANUEVRE_4));  // "Reverse Wheel Right";
      minfo.cch = strlen(minfo.dwTypeData);

      InsertMenuItem(d_manueverMenu, submenupos, TRUE, &minfo);
      submenupos++;

      // reverse wheel left
      minfo.fMask = MIIM_ID | MIIM_STATE | MIIM_SUBMENU | MIIM_TYPE;
      minfo.fType = MFT_STRING;
      minfo.fState = MFS_ENABLED | MFS_UNCHECKED | MFS_UNHILITE;
      minfo.wID = IDM_BAT_RWHEELLEFT;
      minfo.hSubMenu = NULL;
      minfo.dwTypeData = const_cast<char*>(InGameText::get(IDS_BAT_MANUEVRE_5));  // "Reverse Wheel Left";
      minfo.cch = strlen(minfo.dwTypeData);

      InsertMenuItem(d_manueverMenu, submenupos, TRUE, &minfo);
      submenupos++;
#if 0   // side-steps are obsolete. Player can get the same effect by dragging unit
      // side-step right
      minfo.fMask = MIIM_ID | MIIM_STATE | MIIM_SUBMENU | MIIM_TYPE;
      minfo.fType = MFT_STRING;
      minfo.fState = MFS_ENABLED | MFS_UNCHECKED | MFS_UNHILITE;
      minfo.wID = IDM_BAT_SIDESTEPRIGHT;
      minfo.hSubMenu = NULL;
      minfo.dwTypeData = const_cast<char*>(InGameText::get(IDS_BAT_MANUEVRE_6));  // "Side-Step Right";
      minfo.cch = strlen(minfo.dwTypeData);

      InsertMenuItem(d_manueverMenu, submenupos, TRUE, &minfo);
      submenupos++;

      // side-step left
      minfo.fMask = MIIM_ID | MIIM_STATE | MIIM_SUBMENU | MIIM_TYPE;
      minfo.fType = MFT_STRING;
      minfo.fState = MFS_ENABLED | MFS_UNCHECKED | MFS_UNHILITE;
      minfo.wID = IDM_BAT_SIDESTEPLEFT;
      minfo.hSubMenu = NULL;
      minfo.dwTypeData = const_cast<char*>(InGameText::get(IDS_BAT_MANUEVRE_7));  // "Side-Step Left";
      minfo.cch = strlen(minfo.dwTypeData);

      InsertMenuItem(d_manueverMenu, submenupos, TRUE, &minfo);
      submenupos++;
#endif
      // about-face
      minfo.fMask = MIIM_ID | MIIM_STATE | MIIM_SUBMENU | MIIM_TYPE;
      minfo.fType = MFT_STRING;
      minfo.fState = MFS_ENABLED | MFS_UNCHECKED | MFS_UNHILITE;
      minfo.wID = IDM_BAT_ABOUTFACE;
      minfo.hSubMenu = NULL;
      minfo.dwTypeData = const_cast<char*>(InGameText::get(IDS_BAT_MANUEVRE_8));  // "About Face";
      minfo.cch = strlen(minfo.dwTypeData);

      InsertMenuItem(d_manueverMenu, submenupos, TRUE, &minfo);
      submenupos++;
#if 0   // hold is obsolete. Rather, the player should use the 'hold' order-type
      // hold
      minfo.fMask = MIIM_ID | MIIM_STATE | MIIM_SUBMENU | MIIM_TYPE;
      minfo.fType = MFT_STRING;
      minfo.fState = MFS_ENABLED | MFS_UNCHECKED | MFS_UNHILITE;
      minfo.wID = IDM_BAT_HOLD;
      minfo.hSubMenu = NULL;
      minfo.dwTypeData = const_cast<char*>(InGameText::get(IDS_BAT_MANUEVRE_9));  // "Hold";
      minfo.cch = strlen(minfo.dwTypeData);

      InsertMenuItem(d_manueverMenu, submenupos, TRUE, &minfo);
      submenupos++;
#endif
   }




  d_menu.init(md, d_topMenu);


   /*
   Go through & check the relevant menu items
   NB : this is dependent upon the fact that between usage, the menu
   is completely destroyed & recreated.  Otherwise, we'll have multiple
   items being checked.
   */
   if(d_cpi->generic()->isInfantry() || d_cpi->generic()->isCavalry() ) {
      d_menu.checked(IDM_BAT_MARCHFORMATION, d_cpi->spFormation() == SP_MarchFormation);
      d_menu.checked(IDM_BAT_COLUMNFORMATION, d_cpi->spFormation() == SP_ColumnFormation);
      d_menu.checked(IDM_BAT_CLOSEDFORMATION, d_cpi->spFormation() == SP_ClosedColumnFormation);
      d_menu.checked(IDM_BAT_LINEFORMATION, d_cpi->spFormation() == SP_LineFormation);
      if(d_cpi->generic()->isInfantry() ) {
         d_menu.checked(IDM_BAT_SQUAREFORMATION, d_cpi->spFormation() == SP_SquareFormation);
      }
   }

   else if(d_cpi->generic()->isArtillery() ) {
      d_menu.checked(IDM_BAT_LIMBEREDFORMATION, d_cpi->spFormation() == SP_LimberedFormation);
      d_menu.checked(IDM_BAT_UNLIMBEREDFORMATION, d_cpi->spFormation() == SP_UnlimberedFormation);
   }

   d_menu.checked(IDM_BAT_DEPLOYMARCH, d_cpi->formation() == CPF_March);
   d_menu.checked(IDM_BAT_DEPLOYMASSED, d_cpi->formation() == CPF_Massed);
   d_menu.checked(IDM_BAT_DEPLOYDEPLOYED, d_cpi->formation() == CPF_Deployed);
   d_menu.checked(IDM_BAT_DEPLOYEXTENDED, d_cpi->formation() == CPF_Extended);

}



#if 0
void BUnitMenu_Imp::updateMenu()
{
  ASSERT(d_cpi != NoBattleCP);
#if 0
  d_menu.enable(IDM_BAT_DEPLOY, (d_cpi->getRank().sameRank(Rank_Division)));
//  d_menu.enable(3, False, True);


  /*
   * if this is not a players unit allow only info to be accessed
   */

  Boolean canOrder = CampaignArmy_Util::isUnitOrderable(d_cpi);

  d_menu.enable(IDM_UTB_ORDERSWIN, canOrder);
  d_menu.enable(IDM_UTB_REORG, canOrder);
  d_menu.enable(4, canOrder, True);

  /*
   * Disable any non-allowed orders
   */

  for(Orders::Type::Value t = Orders::Type::First; t < Orders::Type::HowMany; INCREMENT(t))
  {
    if(Orders::Type::canGiveToUnit(t))
    {
      Boolean add = False;

      if(d_order.getType() == Orders::Type::MoveTo)
      {
        add = CampaignOrderUtil::allowed(t, d_cpi, d_batData);
      }
      else
      {
        if(t != Orders::Type::MoveTo)
        {
          add = CampaignOrderUtil::allowed(t, d_cpi, d_batData);
        }
      }

      int id = orderTypeToID(t);
      if(id != -1)
      {
        d_menu.enable(id, add);
        d_menu.checked(id, (t == d_order.getType()));
      }
    }
  }

  /*
   * Set advanced options
   */

  for(int id = IDM_UTB_MARCHTO; id < IDM_UTB_DROPOFFGARRISON + 1; id++)
  {
    Boolean checked = idToOption(id);
    d_menu.checked(id, checked);
  }

  /*
   * enable move how
   */

  Boolean isMoveOrder = (d_order.getType() == Orders::Type::MoveTo);

  const int moveHowID = 19;
  d_menu.enable(moveHowID, isMoveOrder, True);

  const int onArrivalID = 23;
  d_menu.enable(onArrivalID, isMoveOrder, True);

  if(isMoveOrder)
  {
    /*
     * set checks for movehow
     */

    for(int i = 0; i < Orders::AdvancedOrders::MoveHow_HowMany; i++)
    {
      int id = moveHowToID(static_cast<Orders::AdvancedOrders::MoveHow>(i));
      if(id != -1)
      {
        d_menu.checked(id, (i == d_order.getMoveHow()));
      }
    }

    /*
     * set checks on arrival
     */

    for(Orders::Type::Value o = Orders::Type::First;
        o < Orders::Type::HowMany; INCREMENT(o))
    {
      int id = onArrivalToID(o);

      if(id != -1)
      {
        Boolean canGive = CampaignOrderUtil::canGiveOnArrival(d_batData, d_cpi, d_order, o);

        d_menu.enable(id, canGive);
        d_menu.checked(id, (o == d_order.getOrderOnArrival()));
      }
    }
  }

  /*
   * Aggression
   */

  for(Orders::Aggression::Value v = Orders::Aggression::First;
        v < Orders::Aggression::HowMany; INCREMENT(v))
  {
    int id = aggressToID(v);
    if(id != -1)
    {
      d_menu.checked(id, (v == d_order.getAggressLevel()));
    }
  }

  /*
   * Posture
   */

  for(Orders::Posture::Type p = Orders::Posture::First;
       p < Orders::Posture::HowMany; INCREMENT(p))
  {
    int id = postureToID(p);
    if(id != -1)
    {
      d_menu.checked(id, (p == d_order.posture()));
    }
  }
#endif
}
#endif

/*----------------------------------------------------------
 * Unit Menu Interface
 */

BUnitMenu_Int::BUnitMenu_Int(BUI_UnitInterface* owner, HWND hParent,
     BattleWindowsInterface* batWind, RCPBattleData batData, BattleOrder& order) :
  d_unitMenu(new BUnitMenu_Imp(owner, hParent, batWind, batData, order))
{
  ASSERT(d_unitMenu);
}

BUnitMenu_Int::~BUnitMenu_Int()
{
  // if(d_unitMenu)
  //   d_unitMenu->destroy();
  delete d_unitMenu;
}

void BUnitMenu_Int::runMenu(const CRefBattleCP& cp, const PixelPoint& p)
{
  if(d_unitMenu)
    d_unitMenu->run(cp, p);
}

void BUnitMenu_Int::destroy()
{
  if(d_unitMenu)
  {
    delete d_unitMenu;
    d_unitMenu = 0;
  }
}

/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.3  2001/07/02 13:18:24  greenius
 * Battle, Campaign, Combined game and Campaign Editor now compile and run... mostly
 *
 * Revision 1.2  2001/06/18 08:25:44  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
