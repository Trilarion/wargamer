/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef BMSGWIN_HPP
#define BMSGWIN_HPP

#ifndef __cplusplus
#error bmsgwin.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *	Battle Message Window
 *
 *----------------------------------------------------------------------
 */

#include "gamedefs.hpp"
#include <windef.h>
#include <stdarg.h>
#include "wind.hpp"

class BattleWindowsInterface;
class BattleData;
class BattleMessageInfo;
class BattleMessageWindowImp;

class BattleMessageWindow : public Window
{
	private:
		// Only construct using make()
   	BattleMessageWindow(BattleWindowsInterface* campWind, const BattleData* campData, HWND hwnd);
	public:
		~BattleMessageWindow();

		void addMessage(const BattleMessageInfo& msg);
		void clear();
		int getNumMessages();
		int getMessagesRead();

		// void destroy();
		// void suspend(bool visible);
        void enable(bool enabled);
        void show(bool visible);
		void toggle();
		bool isVisible() const;
        bool isEnabled() const;
        HWND getHWND() const;

		static BattleMessageWindow* make(BattleWindowsInterface* batWind, const BattleData* batData, HWND hwnd);

	private:
		BattleMessageWindowImp* d_imp;
};



#endif /* BMSGWIN_HPP */

