/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 * Battle Message Window
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "bmsgwin.hpp"
#include "gmsgwin.hpp"
#include "batmsg.hpp"
#include "datetime.hpp"
#include "fsalloc.hpp"
#include "batarmy.hpp"
#include "batdata.hpp"
#include "btwin_i.hpp"
#include "wmisc.hpp"
#include "dib.hpp"
#include "scenario.hpp"

// #define DEBUG_BMSGWIN
#undef DEBUG_BMSGWIN

#ifdef DEBUG_BMSGWIN
#include "logw_imp.hpp"
#include "app.hpp"
#endif


using Greenius_System::Time;
using namespace BattleMeasure;


/*---------------------------------------------------------------------
 * Battle Message Information
 */

class BattleMessageItem : public MessageItem
{
   enum { ChunkSize = 16 };

   public:
      static BattleMessageItem* make(const BattleMessageInfo& info, const BattleTime& time)
      {
         return new BattleMessageItem(info, time);
      }
      static void destroy(BattleMessageItem* msg) { delete msg; }

      BattleMessageID            messageID() const { return d_info.id();     }
      CRefBattleCP               cp()        const { return d_info.cp();     }
      ConstRefGLeader            leader()    const { return d_info.leader(); }
      CRefBattleCP               target()    const { return d_info.target(); }

      const BattleTime&          time()      const { return d_time;          }

      const BattleMessageInfo&   info()      const { return d_info;          }

      const char* cpName() const;
      const char* leaderName() const;
      const char* timeText(char* buf) const;
      String getDescription() const { return messageID().getDescription(); }


   private:
      BattleMessageItem(const BattleMessageInfo& info, const BattleTime& date) :
         d_info(info),
         d_time(date)
      {
      }


      ~BattleMessageItem() { }

      void* operator new(size_t size)
      {
         ASSERT(size == sizeof(BattleMessageItem));
         return s_alloc.alloc(size);
      }

#ifdef _MSC_VER
      void operator delete(void* deadObject)
      {
         s_alloc.free(deadObject, sizeof(BattleMessageItem));
      }
#else
      void operator delete(void* deadObject, size_t size)
      {
         ASSERT(size == sizeof(BattleMessageItem));
         s_alloc.free(deadObject, size);
      }
#endif


      BattleMessageInfo d_info;
      BattleTime        d_time;

      static FixedSize_Allocator s_alloc;
};

#ifdef DEBUG
FixedSize_Allocator BattleMessageItem::s_alloc(sizeof(BattleMessageItem), ChunkSize, "BattleMessageItem");
#else
FixedSize_Allocator BattleMessageItem::s_alloc(sizeof(BattleMessageItem), ChunkSize);
#endif

const char* BattleMessageItem::cpName() const
{
   ASSERT(cp() != NoBattleCP);
   const char* name = cp()->getName();
   ASSERT(name);

   return name;
}

const char* BattleMessageItem::leaderName() const
{
   ASSERT(cp() != NoBattleCP);

   ConstRefGLeader leader = cp()->leader();
   const char* name = leader->getName();
   ASSERT(name);

   return name;
}


const char* BattleMessageItem::timeText(char* buf) const
{
   ASSERT(buf);

   const Greenius_System::Time theTime = time().time();

   sprintf(buf, "%d:%02d",
      (int) theTime.hours(),
      (int) theTime.minutes());

   return buf;
}


/*---------------------------------------------------------------------
 * Battle Window Implementation
 */

class BattleMessageWindowImp :
   public TMessageWindow<BattleMessageItem>
{
   public:
      BattleMessageWindowImp(BattleWindowsInterface* batWind, const BattleData* batData);
      ~BattleMessageWindowImp();

      /*
       * Message list manipulation
       */

      void addMessage(const BattleMessageInfo& msg);

   private:
      /*
       * GenericMessageWindow virtual implementations
       */

      const char* registryName() const { return s_registryName; }

      void orderDialog();
      void drawMessage(HDC hdc, const PixelRect& rect, const PixelRect& fromRect, const PixelRect& dateRect);
      void onDontShow();
      void doSettings();
      void windowUpdated();   // notify owner that window is closed
      // void windowDestroyed(HWND hwnd);
      void updateStatus();
      int fillListBox(ListBox* lb, HDC hdc);
      void drawListItem(DrawDIBDC* dib, const RECT* r, LPARAM value);
      void playSound() const;

   private:

      static const char s_registryName[];

      const BattleData* d_batData;
      BattleWindowsInterface* d_batWind;

      #ifdef DEBUG_BMSGWIN
         mutable LogWinPtr d_logWin;
      #endif
};

BattleMessageWindowImp::BattleMessageWindowImp(BattleWindowsInterface* batWind, const BattleData* batData) :
   TMessageWindow<BattleMessageItem>(),
   d_batWind(batWind),
   d_batData(batData)
{
   #ifdef DEBUG_BMSGWIN
      // WindowsLogWindow::make(&d_logWin, APP::getMainHWND(), "Battle Message Window Log", "batmsg.log");
      d_logWin.set(WindowsLogWindow::make(APP::getMainHWND(), "Battle Message Window Log", "batmsg.log"));
      if(d_logWin)
         d_logWin.printf("Battle Message Window constructed");
   #endif

   ASSERT(d_batWind != 0);
   ASSERT(d_batData != 0);

}

BattleMessageWindowImp::~BattleMessageWindowImp()
{
   selfDestruct();
}

void BattleMessageWindowImp::addMessage(const BattleMessageInfo& msg)
{
   #ifdef DEBUG_BMSGWIN
      if(d_logWin)
         d_logWin.printf("addMessage(%s, %s)",
            (const char*) scenario->getSideName(msg.side()),
            (const char*) msg.id().text().c_str());
      if(msg.cp() != NoBattleCP)
         d_logWin.printf("CP = %s",
            (const char*) msg.cp()->getName());
      if(msg.leader() != NoGLeader)
         d_logWin.printf("Leader = %s",
            (const char*) msg.leader()->getName());
      if(msg.target() != NoBattleCP)
         d_logWin.printf("Target = %s",
            (const char*) msg.target()->getName());
   #endif

   if (BattleMessageOptions::get(msg.id()))
   {
      BattleMessageInfo newMsg = msg;
      if( (newMsg.leader() == NoGLeader) && (newMsg.cp() != NoBattleCP) )
         newMsg.leader(newMsg.cp()->leader());

      ASSERT(newMsg.cp() != NoBattleCP);
      ASSERT(newMsg.leader() != NoGLeader);

      BattleMessageItem* item = BattleMessageItem::make(newMsg, d_batData->getDateAndTime());
      postMessage(item);
   }
}

/*----------------------------------------------------------------------
 * Virtual Function Implementations
 */

const char BattleMessageWindowImp::s_registryName[] = "BatMsgWin";

void BattleMessageWindowImp::orderDialog()
{
   ASSERT(messageCount() > 0);
   ASSERT(d_currentMessage);

   CRefBattleCP cp = d_currentMessage->cp();
   if(cp != NoBattleCP)
   {
      d_batWind->orderCP(cp);
   }
}

void BattleMessageWindowImp::drawMessage(HDC hdc, const PixelRect& textRect, const PixelRect& fromRect, const PixelRect& dateRect)
{
   if(d_currentMessage != 0)
   {
      int fontHeight = minimum(fromRect.height() / 2, dateRect.height());
      fontHeight = minimum(fontHeight, 16);

      Fonts fonts;
      fonts.setFace(hdc, scenario->fontName(Font_Bold), fontHeight, 0, FW_DONTCARE);
      // fonts.setFace(hdc, scenario->fontName(Font_BoldItalic), fontHeight, 0, FW_DONTCARE);

      ASSERT(d_currentMessage->cp() != NoBattleCP);

      const char* name = d_currentMessage->leaderName();
      wTextOut(hdc, fromRect.left(), fromRect.top(), name);

      name = d_currentMessage->cpName();
      wTextOut(hdc, fromRect.left(), fromRect.top() + fontHeight, name);

      char dateText[10];
      d_currentMessage->timeText(dateText);

      wTextOut(hdc, dateRect.left(), dateRect.top(), dateText);

      fontHeight = minimum(fontHeight, 14);

      // fonts.setFace(hdc, scenario->fontName(Font_Italic), fontHeight, 0, FW_DONTCARE);
      fonts.setFace(hdc, scenario->fontName(Font_Normal), fontHeight, 0, FW_DONTCARE);

      String text = BattleMessageFormat::messageToText(d_currentMessage->info(), d_batData);
      wTextOut(hdc, textRect, text.c_str());
      if(!d_currentMessage->isRead())
      {
         d_currentMessage->setRead();
         incReadCount();
      }

   }
}

void BattleMessageWindowImp::onDontShow()
{
#ifdef DEBUG_BMSGWIN
   if(d_logWin)
      d_logWin.printf("Unimplemented: onDontShow");
#endif
  ASSERT(d_currentMessage != 0);
  BattleMessageOptions::set(d_currentMessage->messageID(), False);
}


void BattleMessageWindowImp::doSettings()
{
#ifdef DEBUG_BMSGWIN
   if(d_logWin)
      d_logWin.printf("Unimplemented: doSettings");
#endif
   BattleMessageOptions::setDefault();
}

void BattleMessageWindowImp::windowUpdated()
{
   d_batWind->messageWindowUpdated();
}

// void BattleMessageWindowImp::windowDestroyed(HWND hwnd)
// {
//    d_batWind->windowDestroyed(hwnd);
// }

void BattleMessageWindowImp::updateStatus()
{
   d_batWind->messageWindowUpdated();
}

int BattleMessageWindowImp::fillListBox(ListBox* lb, HDC hdc)
{
   LONG dbUnits = GetDialogBaseUnits();
   WORD dbX = LOWORD(dbUnits);
   WORD dbY = HIWORD(dbUnits);
   const LONG offsetX = (3*dbX)/4;
   const LONG offsetY = (3*dbY)/8;

   /*
    * Iterate through all messages, adding to box
    */

   for(MessageList::const_iterator iter = d_msgList.begin();
      iter != d_msgList.end();
      ++iter)
   {
      const BattleMessageItem* msg = iter;
      lb->add("", d_msgList.itemToLParam(msg));

      const char* name = iter->cpName();
      LONG length = textWidth(hdc, name);
      d_textCX[WhoFrom] = maximum(d_textCX[WhoFrom], length);

      /*
       * Set date length
       */

      char date[10];
      msg->timeText(date);

      length = textWidth(hdc, date);
      d_textCX[DateSent] = maximum(d_textCX[DateSent], length);

      // if(iter->messageID() != BattleMessageID::NoMessage)
      // {
         String description = iter->messageID().getDescription();
         length = textWidth(hdc, description.c_str());
         d_textCX[Description] = maximum(d_textCX[Description], length);
      // }
   }

   return (2*offsetX)+(2* SpaceCX)+d_textCX[WhoFrom]+d_textCX[DateSent]+d_textCX[Description];

}

void BattleMessageWindowImp::drawListItem(DrawDIBDC* dib, const RECT* rect, LPARAM value)
{
   const BattleMessageItem* msg = d_msgList.lParamToItem(value);
   int itemCX = rect->right - rect->left;
   int itemCY = rect->bottom - rect->top;
   LONG dbUnits = GetDialogBaseUnits();
   WORD dbX = LOWORD(dbUnits);
   WORD dbY = HIWORD(dbUnits);

   ASSERT(msg);

   const int offsetX = (3*dbX)/4;

   TEXTMETRIC tm;
   GetTextMetrics(dib->getDC(), &tm);
   int y = ((itemCY - tm.tmHeight) / 2);

   const char* name = msg->cpName();
   char date[10];
   msg->timeText(date);

   int x = offsetX;
   wTextOut(dib->getDC(), x, y, name);

   x += d_textCX[WhoFrom]+SpaceCX;
   wTextOut(dib->getDC(), x, y, date);

   String description = msg->getDescription();
   x += d_textCX[DateSent]+SpaceCX;
   wTextOut(dib->getDC(), x, y, description.c_str());

}

void BattleMessageWindowImp::playSound() const
{
#if 0
   try {
      Sound::playSound("sounds\\newmsg.wav");
   }
   catch(Sound::SoundError e)
   {
      // Ignore Sound Error
      FORCEASSERT("Caught SoundError");
   }
#endif
}

/*----------------------------------------------------------------------
 * Visible Class Implementation
 */

BattleMessageWindow::BattleMessageWindow(BattleWindowsInterface* batWind, const BattleData* batData, HWND hwnd)
{
   ASSERT(batWind != 0);
   ASSERT(batData != 0);
   ASSERT(hwnd != NULL);

   d_imp = new BattleMessageWindowImp(batWind, batData);
   d_imp->init(hwnd);
}

BattleMessageWindow::~BattleMessageWindow()
{
   delete d_imp;
}

void BattleMessageWindow::addMessage(const BattleMessageInfo& msg)
{
   d_imp->addMessage(msg);
}

void BattleMessageWindow::clear()
{
   d_imp->clear();
}

int BattleMessageWindow::getNumMessages()
{
   return d_imp->getNumMessages();
}

int BattleMessageWindow::getMessagesRead()
{
   return d_imp->getMessagesRead();
}

void BattleMessageWindow::toggle()
{
   d_imp->toggle();
}

void BattleMessageWindow::show(bool visible)
{
    d_imp->show(visible);
}


void BattleMessageWindow::enable(bool visible)
{
    d_imp->enable(visible);
}

bool BattleMessageWindow::isVisible() const
{
    return d_imp->isVisible();
}

bool BattleMessageWindow::isEnabled() const
{
    return d_imp->isEnabled();
}

HWND BattleMessageWindow::getHWND() const
{
    return d_imp->getHWND();
}

// void BattleMessageWindow::show()
// {
//    d_imp->show();
// }
//
// void BattleMessageWindow::hide()
// {
//    d_imp->hide();
// }
//
// void BattleMessageWindow::destroy()
// {
//    d_imp->destroy();
// }
//
// Boolean BattleMessageWindow::isShowing()
// {
//    return d_imp->isShowing();
// }
//
// void BattleMessageWindow::suspend(bool visible)
// {
//    d_imp->suspend(visible);
// }

BattleMessageWindow* BattleMessageWindow::make(BattleWindowsInterface* batWind, const BattleData* batData, HWND hwnd)
{
   ASSERT(batWind != 0);
   ASSERT(batData != 0);
   ASSERT(hwnd != NULL);

   return new BattleMessageWindow(batWind, batData, hwnd);
}


/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.3  2001/07/30 09:27:23  greenius
 * Various bug fixes
 *
 * Revision 1.2  2001/06/18 08:25:44  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
