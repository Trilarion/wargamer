/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *      Battle Window User interface for units
 * This is used by BattleWindows when something happens over the battlemap
 *
 * Analogous to campwind\unitint.cpp
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "bw_unit.hpp"
#include "bu_dial.hpp"

#include "bud_ordr.hpp"         // batwind: Battle Unit Order Dialog
//#include "bud_move.hpp"               // batwind: Battle Unit Move Dialog
#include "bud_info.hpp"         // batwind: Info used by unit User Interface
#include "btwin_i.hpp"          // batdisp: Interface for batmap user
#include "batcord.hpp"          // batdata/ Coordinate system
#include "hexmap.hpp"           // batdata/ BattleHexMap
#include "hexdata.hpp"
#include "bobutil.hpp"          // batdata: Order of Battle Helper functions
#include "grtypes.hpp"          // system: Graphic Types
#include "b_send.hpp"           // batlogic: Despatcher
#include <memory>
#include "bobiter.hpp"
#include "bu_menu.hpp"
#include "btw_info.hpp"
#include "scenario.hpp"
#include "moveutil.hpp"
#include "fsalloc.hpp"
#include "bordutil.hpp"
#include "cmbtutil.hpp"
#include "b_los.hpp"
#include "control.hpp"

#include "app.hpp"
#include "batres.h"
#include "options.hpp"
#include "pdeploy.hpp"

#if !defined(NOLOG)
#include "clog.hpp"
#endif

#ifdef _MSC_VER
using namespace std::rel_ops;
#endif

class WaypointHexes : public SLink {

  public:

    WaypointHexes(void) { }
    ~WaypointHexes(void) { }

    HexList d_hexlist;
};




/*
  List of waypoints' hex lists
*/

class WaypointDeploymentHexes {

  public:

    WaypointDeploymentHexes(void) { }
    ~WaypointDeploymentHexes(void) { }

    SList <WaypointHexes> d_waypoints;

    void reset(void) {
      WaypointHexes * wp = d_waypoints.first();
      while(wp) {
        wp->d_hexlist.reset();
        wp = d_waypoints.next();
      }
      d_waypoints.reset();
    }

};









//namespace BattleWindows_Internal
//{

#if !defined(NOLOG)
namespace {
LogFile logFile("bui.log");
};
#endif





/*
 * Reduce some typing by declaring some classes from other namespaces
 */

using BattleMeasure::HexCord;





















/*
 * Implementation of the Battle Unit Interface
 */

class BUI_UnitImp : public BUI_UnitInterface {

  public:

    /*
      Constructors
    */

    BUI_UnitImp(RPBattleWindows batWind, RCPBattleData battleData);
    ~BUI_UnitImp();

    /*
     * Implements BattleUI_Interface
     */

    void onLButtonDown(const BattleMapSelect& info);
    // Button is pressed
    void onLButtonUp(const BattleMapSelect& info);
    // Button is released while not dragging
    void onRButtonDown(const BattleMapSelect& info);
    // Button is pressed
    void onRButtonUp(const BattleMapSelect& info);
    // Button is released while not dragging
    void onStartDrag(const BattleMapSelect& info);
    // Mouse is moved while button is held
    void onEndDrag(const BattleMapSelect& info);
    // button is released while dragging

    void onMove(const BattleMapSelect& info);
    // Mouse has moved
    void overNothing(void);
    // Mouse is not over anything (e.g. off map)
    bool setCursor(void);

    void timeChanged(void);

    void mapZoomChanged(void);

    void selectUnit(BattleCP * cp);


    bool showOrderWind(const PixelPoint& p, const BUD_Info& info, BattleOrderInfo* order, unsigned int flags) {
      return d_orderWind->run(p, info, order, flags);
    }


    void destroy();
    void updateDestination();
#if 0
    void findHighlightHexes(
                            BattleOrderInfo& order,
                                HexPosition::Facing lastFacing,
                                CPFormation lastFormation,
                                const int spCount,
                                const HexCord& hex,
                                HighlightList& hl
      );
#endif

    /*
     * virual functions from BUI_UnitInterface
     */

    void sendOrder(void);
    void cancelOrder(void);

    void mode(BUI_UnitInterface::Mode m) {
      // set mode
      d_mode = m;
      // clean up targetting highlights

      if(d_mode == BUI_UnitInterface::Targetting) {
        highlightTarget(false);
        d_targetHighlighted = false;
      }

    }
    BUI_UnitInterface::Mode mode(void) const { return d_mode; }

    void showOrders(void);
    void showDeployment(void);
    void showInfo(void);
    void showLOS(void);
    void redrawMap(void) { d_batWind->redrawMap(False); }

    void highlightUnitWithoutDeploy(bool on);
    void highlightUnit(bool on);

    void highlightUnitAndChildren(bool state);
    void HighlightCP(BattleCP * cp, bool use_darkest_highlight);
    void HighlightSP(BattleSP * sp, bool use_darkest_highlight);

    void highlightTarget(bool state);

    void highlightDestination(bool state);

    bool doPlayerDeploy(RPBattleData batData, const CRefBattleCP& cp, const HexCord& startHex, HexList* hl);

    void SetWaypointDeploymentHexes(CPBattleData batData, const CRefBattleCP& cp);

  private:

    void setUnit(ParamCRefBattleCP cp);

  private:

    CPBattleData d_batData;      // Battle Data
    PBattleWindows d_batWind;      // User Interface Controller

    BUI_UnitInterface::Mode d_mode;

    const BattleCP * d_selectedUnit;
    HexCord d_selectedUnitHex;

    const BattleCP * d_lastSelectedUnit;
    SList<CPListEntry> d_UnitsInHexList;

    bool d_unitHighlighted;
    HexCord d_leftButtonDownHex;
    HexCord d_rightButtonDownHex;
    HexCord d_leftButtonUpHex;
    HexCord d_rightButtonUpHex;
    bool d_leftButtonDragging;
    bool d_rightButtonDragging;
    HexCord d_mouseMoveHex;

    HexCord d_deploymentHex;
    HexList d_deploymentHexList;

    HexList d_selectedUnitHexList;

    HexCord d_deploymentMouseOffset;

    HexPosition::Facing d_originalUnitFacing;
    HexPosition::Facing d_moveUnitFacing;

    WaypointDeploymentHexes d_waypointDeploymentHexes;

    const BattleCP * d_attachToUnit;

    const BattleCP * d_targetUnit;
    HexCord d_targetUnitHex;

    HexList d_targetUnitHexList;
    bool d_targetHighlighted;

    HexList d_destinationHexList;
    bool d_destinationHighlighted;

    BattleCursorEnum d_currentCursorMode;
    BattleCursorEnum d_desiredCursorMode;

    HCURSOR hCursor_Normal;
    HCURSOR hCursor_OverUnit;
    HCURSOR hCursor_DragUnit;
    HCURSOR hCursor_BadHex;
    HCURSOR hCursor_TargetMode;
    HCURSOR hCursor_ValidTarget;
    HCURSOR hCursor_InvalidTarget;
    HCURSOR hCursor_AttatchUnit;


    HexCord d_losSrcHex;
    HexCord d_lastHex;      // Hex that mouse was last over
    bool d_lastHexValid;

    BUD_Info d_info;                  // Common info used by order/move dialogs.
    BattleMapSelect d_mapInfo;          // Copy of Info passed from Map

    CRefBattleCP d_cp;           // current unit (TODO: will need stacked units)
    BattleOrder d_currentOrder; // current Order
    BattleOrder_Int * d_orderWind;
    BUnitMenu_Int * d_menu;
    BUnitInfo_Int * d_infoWind;
    PixelPoint d_point;

    B_LineOfSight d_los;
};



BUI_UnitImp::BUI_UnitImp(RPBattleWindows batWind, RCPBattleData batData) :
  d_batData(batData),
  d_batWind(batWind),
  d_lastHex(),
  d_lastHexValid(false),
//      d_trackWind(0),
  d_orderWind(0),
  d_menu(0),
  d_infoWind(0),

  d_mode(BUI_UnitInterface::Normal),

  d_selectedUnit(NULL),
  d_unitHighlighted(false),

  d_destinationHighlighted(false),
  d_leftButtonDragging(false),
  d_rightButtonDragging(false),

  d_targetUnit(NULL),
  d_targetHighlighted(false),

  d_attachToUnit(NULL),

#ifdef DEBUG
  d_los(const_cast<BattleData*>(d_batData()))
#else
  d_los(const_cast<BattleData*>(d_batData))
#endif
{
//      d_trackWind = new BattleTrackingWindow(batWind->hwnd(), batData);n
  d_orderWind = new BattleOrder_Int(this, batData, d_currentOrder);
  d_menu = new BUnitMenu_Int(this, batWind->hwnd(), d_batWind, d_batData, d_currentOrder);
  d_infoWind = new BUnitInfo_Int(this, batWind->hwnd(), d_batData);



  /*
   * Load colour cursors
   */
  if(Options::get(OPT_ColorCursor))
  {
    hCursor_Normal = LoadCursor(NULL, IDC_ARROW);
    ASSERT(hCursor_Normal);
    hCursor_OverUnit = LoadCursor(APP::instance(), MAKEINTRESOURCE(CURS_BAT_OVERUNIT_COLOR));
    ASSERT(hCursor_OverUnit);
    hCursor_DragUnit = LoadCursor(APP::instance(), MAKEINTRESOURCE(CURS_BAT_DRAGUNIT_COLOR));
    ASSERT(hCursor_DragUnit);
    hCursor_BadHex = LoadCursor(APP::instance(), MAKEINTRESOURCE(CURS_BAT_BADHEX_COLOR));
    ASSERT(hCursor_BadHex);
    hCursor_TargetMode = LoadCursor(APP::instance(), MAKEINTRESOURCE(CURS_BAT_TARGETMODE_COLOR));
    ASSERT(hCursor_TargetMode);
    hCursor_ValidTarget = LoadCursor(APP::instance(), MAKEINTRESOURCE(CURS_BAT_VALIDTARGET_COLOR));
    ASSERT(hCursor_ValidTarget);
    hCursor_InvalidTarget = LoadCursor(APP::instance(), MAKEINTRESOURCE(CURS_BAT_INVALIDTARGET_COLOR));
    ASSERT(hCursor_InvalidTarget);
    hCursor_AttatchUnit = LoadCursor(APP::instance(), MAKEINTRESOURCE(CURS_BAT_ATTATCHUNIT_COLOR));
    ASSERT(hCursor_AttatchUnit);
  }
  /*
   * Load monochrome cursors
   */
  else {
    hCursor_Normal = LoadCursor(NULL, IDC_ARROW);
    ASSERT(hCursor_Normal);
    hCursor_OverUnit = LoadCursor(APP::instance(), MAKEINTRESOURCE(CURS_BAT_OVERUNIT_MONO));
    ASSERT(hCursor_OverUnit);
    hCursor_DragUnit = LoadCursor(APP::instance(), MAKEINTRESOURCE(CURS_BAT_DRAGUNIT_MONO));
    ASSERT(hCursor_DragUnit);
    hCursor_BadHex = LoadCursor(APP::instance(), MAKEINTRESOURCE(CURS_BAT_BADHEX_MONO));
    ASSERT(hCursor_BadHex);
    hCursor_TargetMode = LoadCursor(APP::instance(), MAKEINTRESOURCE(CURS_BAT_TARGETMODE_MONO));
    ASSERT(hCursor_TargetMode);
    hCursor_ValidTarget = LoadCursor(APP::instance(), MAKEINTRESOURCE(CURS_BAT_VALIDTARGET_MONO));
    ASSERT(hCursor_ValidTarget);
    hCursor_InvalidTarget = LoadCursor(APP::instance(), MAKEINTRESOURCE(CURS_BAT_INVALIDTARGET_MONO));
    ASSERT(hCursor_InvalidTarget);
    hCursor_AttatchUnit = LoadCursor(APP::instance(), MAKEINTRESOURCE(CURS_BAT_ATTATCHUNIT_MONO));
    ASSERT(hCursor_AttatchUnit);
  }

  d_currentCursorMode = Cursor_Normal;
  d_desiredCursorMode = Cursor_Normal;


  d_UnitsInHexList.reset();
  d_lastSelectedUnit = NULL;

}












BUI_UnitImp::~BUI_UnitImp()
{
#if !defined(NOLOG)
  logFile << "~BUI_UnitImp()" << endl;
#endif
//  if(d_trackWind)
//       delete d_trackWind;

  if(d_orderWind)
    delete d_orderWind;

  if(d_menu)
    delete d_menu;

  if(d_infoWind)
    delete d_infoWind;
}



/*
  If the time has changed, redo highlights to keep up with moving units
*/

void
BUI_UnitImp::timeChanged(void) {


  if(d_unitHighlighted) {
    if(!d_selectedUnit) highlightUnitAndChildren(false);
    else {
      highlightUnitAndChildren(false);
      highlightUnitAndChildren(true);
    }
  }
/*
  if(d_selectedUnit) {

  // unhighlight target
  if(d_targetUnit && d_targetHighlighted) {
  highlightTarget(false);
  d_targetHighlighted = false;
  }

  // highlight unit's target
  if(d_selectedUnit->targetCP()) {

  #ifdef DEBUG
  logFile.printf("Selected unit has target\n");
  #endif

  d_targetUnit = d_selectedUnit->targetCP();
  highlightTarget(true);
  d_targetHighlighted = true;
  }

  else {
  d_targetUnit = NULL;

  #ifdef DEBUG
  logFile.printf("Selected unit has NO target\n");
  #endif
  }
  }
*/
  if(d_destinationHighlighted) {
    highlightDestination(false);
    highlightDestination(true);
  }
}


void
BUI_UnitImp::mapZoomChanged(void) {

  mode(BUI_UnitInterface::Normal);
  d_currentCursorMode = Cursor_Normal;
  SetCursor(hCursor_Normal);
}






void
BUI_UnitImp::selectUnit(BattleCP * cp) {

  d_lastSelectedUnit = d_selectedUnit;

  // if we have a selected unit, deselect it & remove highlights
  if(d_selectedUnit) {

    // if unit is highlighted, remove highlights
    if(d_unitHighlighted) {
      highlightUnitAndChildren(false);
      d_unitHighlighted = false;
    }

    if(d_destinationHighlighted) {
      highlightDestination(false);
      d_destinationHighlighted = false;
    }

    highlightUnitWithoutDeploy(false);
    d_selectedUnit = NULL;
    d_orderWind->hide();
  }


  // fill d_UnitsInHexList list with units in hex
  d_UnitsInHexList.reset();
  d_selectedUnit = cp;

  SetWaypointDeploymentHexes(d_batData, d_selectedUnit);

  d_selectedUnit->lastOrder(&d_currentOrder);
  d_currentOrder.facing(d_selectedUnit->facing());
  d_currentOrder.wayPoints().reset();
  d_currentOrder.order().d_whatOrder.reset();

  d_originalUnitFacing = d_selectedUnit->facing();
  d_moveUnitFacing = d_selectedUnit->facing();

  // remember the position
  if(d_selectedUnit->sp()) d_selectedUnitHex = d_selectedUnit->sp()->hex();
  else d_selectedUnitHex = d_selectedUnit->hex();

  // set up initial deployment hex highlights list
  d_deploymentHex = d_leftButtonDownHex;
  d_deploymentHexList.reset();

//    const DeployItem * di = d_selectedUnit->mapBegin();
//   if(di) {
  std::vector<DeployItem>::const_iterator di = d_selectedUnit->mapBegin();
  if(di != d_selectedUnit->mapEnd())
  {
    HexCord di_hex = di->d_hex;
    d_deploymentMouseOffset.x( d_deploymentHex.x() - di_hex.x() );
    d_deploymentMouseOffset.y( d_deploymentHex.y() - di_hex.y() );
  }
  else d_deploymentMouseOffset = HexCord(0,0);

  // highlight this unit
  highlightUnitAndChildren(true);
  d_unitHighlighted = true;

  /*
   * highlight unit's destination
   * if a player unit
   */
  d_destinationHexList.reset();
  if(GamePlayerControl::canControl(d_selectedUnit->getSide())) {

    highlightDestination(true);
    d_destinationHighlighted = true;
  }

  redrawMap();
}





const BattleCP* CastBattleUnitToCP(const BattleUnit * unit) 
{
  // ensure we are refenencing a CP
  const BattleCP * cp = dynamic_cast<const BattleCP *>(unit);
  if(! cp) 
  {
    // else reference its parent
    const BattleSP* sp = dynamic_cast<const BattleSP *>(unit);
    ASSERT(sp);
    cp = sp->parent();
  }
  ASSERT(cp);
  return cp;
}



/*
 * Button is pressed
 */

void
BUI_UnitImp::onLButtonDown(const BattleMapSelect& info) {

  d_leftButtonDownHex = d_batData->getHex(info.location(), BattleData::Mid);
  d_leftButtonDragging = true;

  d_lastSelectedUnit = d_selectedUnit;

  switch(d_mode) {

    case BUI_UnitInterface::Normal : {

      BattleHexMap::iterator lower_iter;
      BattleHexMap::iterator upper_iter;

      // if we have a selected unit, deselect it & remove highlights
      if(d_selectedUnit) {
        // if unit is highlighted, remove highlights
        if(d_unitHighlighted) {
          highlightUnitAndChildren(false);
          d_unitHighlighted = false;
        }
/*
  if(d_targetHighlighted) {
  highlightTarget(false);
  d_targetHighlighted = false;
  }
*/
        if(d_destinationHighlighted) {
          highlightDestination(false);
          d_destinationHighlighted = false;
        }

        highlightUnitWithoutDeploy(false);
        // deselect unit
        d_selectedUnit = NULL;
        d_orderWind->hide();
      }


      // if player has not clicked on a unit, return
      const BattleData * bd = d_batData;
      if(! const_cast<BattleData *>(bd)->hexMap()->find(d_leftButtonDownHex, lower_iter, upper_iter)) {
        // return
        return;
      }


      // fill d_UnitsInHexList list with units in hex
      d_UnitsInHexList.reset();

      const BattleUnit * map_unit = d_batData->hexMap()->unit(lower_iter);
      const BattleCP * map_cp = CastBattleUnitToCP(map_unit);

      CPListEntry * entry = new CPListEntry;
      entry->cp(map_cp);
      d_UnitsInHexList.insert(entry);
      ++lower_iter;

      while(lower_iter != upper_iter) {

        map_unit = d_batData->hexMap()->unit(lower_iter);
        map_cp = CastBattleUnitToCP(map_unit);

        entry = new CPListEntry;
        entry->cp(map_cp);
        d_UnitsInHexList.insert(entry);
        ++lower_iter;
      }

      // get unit AFTER d_lastSelectedUnit in list, OR wrap around to the first unit

      bool unit_found = false;
      entry = d_UnitsInHexList.first();
      // if list is empty, then my previous logic was wrong !
      ASSERT(entry);

      while(entry && (!unit_found)) {

        if(entry->cp() == d_lastSelectedUnit) {
          CPListEntry * next_entry = d_UnitsInHexList.next();
          if(next_entry) {
            d_selectedUnit = next_entry->cp();
          }
          else {
            d_selectedUnit = d_UnitsInHexList.first()->cp();
          }
          unit_found = true;
        }

        entry = d_UnitsInHexList.next();
      }
      if(!unit_found) d_selectedUnit = d_UnitsInHexList.first()->cp();



      SetWaypointDeploymentHexes(d_batData, d_selectedUnit);

      d_selectedUnit->lastOrder(&d_currentOrder);
      d_currentOrder.facing(d_selectedUnit->facing());
      d_currentOrder.wayPoints().reset();
      d_currentOrder.order().d_whatOrder.reset();

      d_originalUnitFacing = d_selectedUnit->facing();
      d_moveUnitFacing = d_selectedUnit->facing();

      // remember the position
      if(d_selectedUnit->sp()) d_selectedUnitHex = d_selectedUnit->sp()->hex(); // if a non-visible CP take the first SP's position (1st in deploy-map)
      else d_selectedUnitHex = d_selectedUnit->hex();     // else the CP is visible & will have a d_position hex

      // set up initial deployment hex highlights list
      d_deploymentHex = d_leftButtonDownHex;
      d_deploymentHexList.reset();

//            const DeployItem * di = d_selectedUnit->mapBegin();
//         if(di) {
      std::vector<DeployItem>::const_iterator di = d_selectedUnit->mapBegin();
      if(di != d_selectedUnit->mapEnd())
      {
        HexCord di_hex = di->d_hex;
        d_deploymentMouseOffset.x( d_deploymentHex.x() - di_hex.x() );
        d_deploymentMouseOffset.y( d_deploymentHex.y() - di_hex.y() );
      }
      else d_deploymentMouseOffset = HexCord(0,0);

      // highlight this unit
      highlightUnitAndChildren(true);
      d_unitHighlighted = true;

      /*
       * highlight unit's destination
       * if a player unit
       */
      d_destinationHexList.reset();
      if(GamePlayerControl::canControl(d_selectedUnit->getSide())) {

        highlightDestination(true);
        d_destinationHighlighted = true;
/*
// highlight unit's target
if(d_selectedUnit->targetCP()) {
d_targetUnit = d_selectedUnit->targetCP();
highlightTarget(true);
d_targetHighlighted = true;
}
else if(d_targetUnit) {
highlightTarget(false);
d_targetUnit=NULL;
d_targetHighlighted = false;
}
*/
      }

      redrawMap();

      return;
    }


    case BUI_UnitInterface::Attaching : {

      return;
    }


    case BUI_UnitInterface::Targetting : {

      if(!d_selectedUnit) return;

      BattleHexMap::iterator lower_iter;
      BattleHexMap::iterator upper_iter;

      // if player has not clicked on a unit, return & quit Targetting mode
      const BattleData * bd = d_batData;
      if(! const_cast<BattleData *>(bd)->hexMap()->find(d_leftButtonDownHex, lower_iter, upper_iter)) {
        d_targetUnit = NULL;
        mode(BUI_UnitInterface::Normal);
        d_desiredCursorMode = Cursor_Normal;
        setCursor();
        cancelOrder();
        return;
      }

      // get unit clicked on
      const BattleUnit * unit = d_batData->hexMap()->unit(lower_iter);

      // ensure we are refenencing a CP
      const BattleCP * cp = dynamic_cast<const BattleCP *>(unit);
      const BattleSP * sp;
      if(! cp) {
        // else reference its parent
        sp = dynamic_cast<const BattleSP *>(unit);
        ASSERT(sp);
        cp = sp->parent();
      }
      ASSERT(cp);

      // make this unit the target
      d_targetUnit = cp;
      d_targetUnitHex = d_leftButtonDownHex;

      // reset mode & cursor...
      d_mode = BUI_UnitInterface::Normal;
      onMove(info);

      return;
    }

  } // end of switch statement

}




void
BUI_UnitImp::onRButtonDown(const BattleMapSelect& info) {


  d_rightButtonDownHex = d_batData->getHex(info.location(), BattleData::Mid);
  d_rightButtonDragging = true;

  // get out of targetting mode & set cursor ro normal
  if(d_mode == BUI_UnitInterface::Targetting) {
    d_mode = BUI_UnitInterface::Normal;
    onMove(info);
  }



// switch(d_mode) {

//    case BUI_UnitInterface::Normal : {

  // if we're already dragging a unit, then this will rotate it clockwise
  if(d_selectedUnit && d_leftButtonDragging) {

    return;
  }

  BattleHexMap::iterator lower_iter;
  BattleHexMap::iterator upper_iter;

  // if we have a selected unit, deselect it & remove highlights
  if(d_selectedUnit) {
    // if unit is highlighted, remove highlights
    if(d_unitHighlighted) {
      highlightUnitAndChildren(false);
      d_unitHighlighted = false;
    }
/*
  if(d_targetHighlighted) {
  highlightTarget(false);
  d_targetHighlighted = false;
  }
*/
    if(d_destinationHighlighted) {
      highlightDestination(false);
      d_destinationHighlighted = false;
    }
    // deselect unit
    highlightUnitWithoutDeploy(false);
    d_selectedUnit = NULL;
    d_orderWind->hide();
  }

  // if player has not clicked on a unit, return
  const BattleData * bd = d_batData;
  if(! const_cast<BattleData *>(bd)->hexMap()->find(d_rightButtonDownHex, lower_iter, upper_iter)) {
    // return
    return;
  }

  // get unit clicked on
  const BattleUnit * unit = d_batData->hexMap()->unit(lower_iter);

  // ensure we are refenencing a CP
  const BattleCP * cp = dynamic_cast<const BattleCP *>(unit);
  const BattleSP * sp;
  if(! cp) {
    sp = dynamic_cast<const BattleSP *>(unit);
    ASSERT(sp);
    cp = sp->parent();
  }
  ASSERT(cp);

  // make this unit selected
  d_selectedUnit = cp;
  SetWaypointDeploymentHexes(d_batData, cp);

  d_currentOrder = cp->getCurrentOrder();
  d_currentOrder.facing(d_selectedUnit->facing());
  d_currentOrder.wayPoints().reset();
  d_currentOrder.order().d_whatOrder.reset();

  d_originalUnitFacing = d_selectedUnit->facing();
  d_moveUnitFacing = d_selectedUnit->facing();


  // remember the position
  if(cp->sp()) d_selectedUnitHex = cp->sp()->hex(); // if a non-visible CP take the first SP's position (1st in deploy-map)
  else d_selectedUnitHex = cp->hex();     // else the CP is visible & will have a d_position hex

  // set up initial deployment hex highlights list
  d_deploymentHex = d_rightButtonDownHex;
  d_deploymentHexList.reset();

//            const DeployItem * di = cp->mapBegin();
//         if(di) {
  std::vector<DeployItem>::const_iterator di = cp->mapBegin();
  if(di != cp->mapEnd())
  {
    HexCord di_hex = di->d_hex;
    d_deploymentMouseOffset.x( d_deploymentHex.x() - di_hex.x() );
    d_deploymentMouseOffset.y( d_deploymentHex.y() - di_hex.y() );
  }
  else d_deploymentMouseOffset = HexCord(0,0);


  // highlight this unit
  highlightUnitAndChildren(true);
  d_unitHighlighted = true;

  /*
   * highlight unit's destination
   * if a player unit
   */
  d_destinationHexList.reset();
  if(GamePlayerControl::canControl(d_selectedUnit->getSide())) {

    highlightDestination(true);
    d_destinationHighlighted = true;
/*
// highlight unit's target
if(d_selectedUnit->targetCP()) {
d_targetUnit = d_selectedUnit->targetCP();
highlightTarget(true);
d_targetHighlighted = true;
}
else if(d_targetUnit) {
highlightTarget(false);
d_targetUnit=NULL;
d_targetHighlighted = false;
}
*/
  }

  return;
}





/*

Attach Unit

*/
/*
  case BUI_UnitInterface::Attaching  : {

  return;
  }

  case BUI_UnitInterface::Targetting : {
  return;
  }

  } // end of switch statement
*/

//}



void
BUI_UnitImp::destroy() {

  d_orderWind->destroy();
  d_menu->destroy();
  d_infoWind->destroy();
}









/*
 * Button is released while not dragging
 */

void
BUI_UnitImp::onLButtonUp(const BattleMapSelect& info) {

  d_leftButtonUpHex = d_batData->getHex(info.location(), BattleData::Mid);

  switch(d_mode) {

    case BUI_UnitInterface::Normal : {

      // player has clicked and dragged a unit to a new hex, with the left mouse button
      if(d_selectedUnit && d_leftButtonDragging && (d_leftButtonUpHex != d_leftButtonDownHex) ) {

        // if this is an enemy unit, just return
        // if this unit if fleeing the field, just return
        if(!GamePlayerControl::canControl(d_selectedUnit->getSide()) || d_selectedUnit->fleeingTheField()) {
          d_leftButtonDragging = false;
          return;
        }

        // show the small orders dialog
        POINT p = info.pixelPoint();
        ClientToScreen(d_batWind->hwnd(), &p);
        d_point.set(p.x, p.y);

        d_info.cp(d_selectedUnit);
        HexCord hex_offset(d_leftButtonUpHex.x() - d_deploymentMouseOffset.x(), d_leftButtonUpHex.y() - d_deploymentMouseOffset.y() );
        d_info.hex(hex_offset);

        updateDestination();

        BattleOrderInfo* order = (d_currentOrder.nextWayPoint()) ? &d_currentOrder.wayPoints().getLast()->d_order : &d_currentOrder.order();

        d_deploymentHex = d_leftButtonUpHex;

        highlightUnitAndChildren(true);
        d_unitHighlighted = true;

        /*
          If we have an attach-to unit, then set this order to attach
        */
        if(d_attachToUnit != NULL) {
          d_currentOrder.mode(BattleOrderInfo::Attach);
          d_currentOrder.attachTo(d_attachToUnit);
        }

        d_desiredCursorMode = Cursor_Normal;
        setCursor();
        d_batData->soundSystem()->triggerSound(SOUNDTYPE_WINDOWOPEN);
        showOrderWind(d_point, d_info, order, CtrlInfo::SmallMode);


        // reset drag status
        d_leftButtonDragging = false;
        return;
      }
      // reset drag status
      d_leftButtonDragging = false;
      return;
    }

    case BUI_UnitInterface::Attaching : {

      BattleHexMap::iterator lower_iter;
      BattleHexMap::iterator upper_iter;

      // if player has not clicked on a unit, return
      const BattleData * bd = d_batData;
      if(! const_cast<BattleData *>(bd)->hexMap()->find(d_leftButtonUpHex, lower_iter, upper_iter)) {
        d_attachToUnit = NULL;
        // return
        return;
      }

      // get unit clicked on
      const BattleUnit * unit = d_batData->hexMap()->unit(lower_iter);

      // ensure we are refenencing a CP
      const BattleCP * cp = dynamic_cast<const BattleCP *>(unit);

      if(! cp) {
        const BattleSP * sp;
        sp = dynamic_cast<const BattleSP *>(unit);
        ASSERT(sp);
        cp = sp->parent();
      }
      ASSERT(cp);

      d_attachToUnit = cp;
    }


    case BUI_UnitInterface::Targetting : {

      // if there's no target, then don't bother
      if(!d_targetUnit || !d_selectedUnit) return;

      // if mouse has moved, return
      if(d_targetUnitHex != d_leftButtonUpHex) return;

      // otherwise, set CP target & order target, as this unit
      d_currentOrder.targetCP(d_targetUnit);

      sendOrder();

      // reset
      d_leftButtonDragging = false;
      d_targetUnit = NULL;
      return;
    }

  } // end of switch statement

}

void
BUI_UnitImp::onRButtonUp(const BattleMapSelect& info) {

  d_rightButtonUpHex = d_batData->getHex(info.location(), BattleData::Mid);

  switch(d_mode) {

    case BUI_UnitInterface::Normal : {

      // if we're already dragging a unit, then this will rotate it clockwise ( if it belongs to our side 7 is not fleeing )
      if(d_selectedUnit && d_leftButtonDragging && (GamePlayerControl::canControl(d_selectedUnit->getSide())) && (!d_selectedUnit->fleeingTheField()) ) {

        highlightUnitWithoutDeploy(false);

        d_moveUnitFacing = clockWiseFacing(d_moveUnitFacing);
        // get past clockwiseFacing bug
        if(d_moveUnitFacing == 63 || d_moveUnitFacing == 191) d_moveUnitFacing++;
        d_currentOrder.facing(d_moveUnitFacing);

        d_deploymentHexList.reset();
        HexCord hex_offset(d_rightButtonUpHex.x() - d_deploymentMouseOffset.x(), d_rightButtonUpHex.y() - d_deploymentMouseOffset.y() );

        const BattleData * bd = d_batData;
        //doPlayerDeploy(const_cast<BattleData *>(bd), d_selectedUnit, d_mouseMoveHex, &d_deploymentHexList);
        doPlayerDeploy(const_cast<BattleData *>(bd), d_selectedUnit, hex_offset, &d_deploymentHexList);

        highlightUnitWithoutDeploy(true);

        // sound
        d_batData->soundSystem()->triggerSound(SOUNDTYPE_FACINGCHANGE);

        return;
      }

      // player has right-clicked on a unit, & released button at same hex
      if(d_selectedUnit && (d_rightButtonUpHex == d_rightButtonDownHex) ) {

        // set up the info structure used by menu & dialogs
        // d_info.d_unit = d_selectedUnit;
        // d_info.d_hex = d_rightButtonUpHex;
        d_info.cp(d_selectedUnit);
        d_info.hex(d_rightButtonUpHex);

        // display the pop-up menu
        POINT p = info.pixelPoint();
        ScreenToClient(d_batWind->hwnd(), &p);
        RECT rect;
        GetWindowRect(d_batWind->hwnd(), &rect);
        d_point.set(p.x + rect.left, p.y + rect.top);

        d_desiredCursorMode = Cursor_Normal;
        setCursor();
        d_batData->soundSystem()->triggerSound(SOUNDTYPE_WINDOWOPEN);
        d_menu->runMenu(d_selectedUnit, d_point);

        d_rightButtonDragging = false;

        return;
      }

      // player has right-clicked, & released button in a different hex
      d_rightButtonDragging = false;
      return;
    }


    case BUI_UnitInterface::Targetting : {
      // reset drag status
      d_rightButtonDragging = false;
      return;
    }

  } // end of switch statement

}





/*
 * Mouse is moved while button is held
 */

void
BUI_UnitImp::onStartDrag(const BattleMapSelect& info) {

}

/*
 * button is released while dragging
 */

void
BUI_UnitImp::onEndDrag(const BattleMapSelect& info) {

// d_mapInfo = info;

}






/*
 * Mouse has moved
 */

void
BUI_UnitImp::onMove(const BattleMapSelect& info) {

  d_mouseMoveHex = d_batData->getHex(info.location(), BattleData::Mid);
  d_batWind->updateTracking(d_mouseMoveHex);

  switch(d_mode) {

    case BUI_UnitInterface::Normal : {

      /*
        player has left clicked & dragged a unit to a new hex, so switch to move mode
      */
      if(d_selectedUnit && d_leftButtonDragging && (d_mouseMoveHex != d_leftButtonDownHex) ) {

        /*
          If we're trying to drag an enemy unit - just set to normal cursor, & return
        */
        if(!GamePlayerControl::canControl(d_selectedUnit->getSide()))
        {
          // just the Normal cursor
          d_desiredCursorMode = Cursor_Normal;
          setCursor();
          return;
        }

        /*
          if we're dragging a HQ CP, then check to attatching to a Division
        */
        if(d_selectedUnit->getRank().isHigher(Rank_Division) && (!d_selectedUnit->fleeingTheField()))
        {

          BattleHexMap::iterator lower_iter;
          BattleHexMap::iterator upper_iter;

          const BattleData * bd = d_batData;
          if(const_cast<BattleData *>(bd)->hexMap()->find(d_mouseMoveHex, lower_iter, upper_iter))
          {

            const BattleUnit * unit = d_batData->hexMap()->unit(lower_iter);

            // ensure we are refenencing a CP
            const BattleCP * cp = dynamic_cast<const BattleCP *>(unit);

            if(! cp) {
              const BattleSP * sp;
              sp = dynamic_cast<const BattleSP *>(unit);
              ASSERT(sp);
              cp = sp->parent();
            }
            ASSERT(cp);

            /*
             * NEW : check that we can attach these two units
             */
            if(BobUtility::canAttach(const_cast<BattleData *>(bd), const_cast<BattleCP *>(d_selectedUnit), const_cast<BattleCP *>(cp)))
              //if(BobUtility::canAttach(d_batData, d_selectedUnit, cp))
            {

              // remove existing highlighting
              highlightUnitWithoutDeploy(false);
              // make sure that we haven't erased our initial-position highlights !
              if(d_unitHighlighted) {
                highlightUnitAndChildren(false);
                highlightUnitAndChildren(true);
              }

              d_attachToUnit = cp;
              // Set cursor to indicate Attatching
              d_desiredCursorMode = Cursor_AttatchUnit;
              setCursor();
              return;
            }
          }

        }

        // not attatching
        d_attachToUnit = NULL;

        // if this hex is different from the last deployment one
        if(d_mouseMoveHex != d_deploymentHex && (!d_selectedUnit->fleeingTheField())) {

          highlightUnitWithoutDeploy(false);

          d_deploymentHexList.reset();
          const BattleData * bd = d_batData;

          HexCord hex_offset(d_mouseMoveHex.x() - d_deploymentMouseOffset.x(), d_mouseMoveHex.y() - d_deploymentMouseOffset.y() );
          doPlayerDeploy(const_cast<BattleData *>(bd), d_selectedUnit, hex_offset, &d_deploymentHexList);

          // make sure that we haven't erased our initial-position highlights !
          if(d_unitHighlighted) {
            highlightUnitAndChildren(false);
            highlightUnitAndChildren(true);
          }

          highlightUnitWithoutDeploy(true);

          // Set cursor to indicate Dragging
          d_desiredCursorMode = Cursor_DragUnit;
          setCursor();
          return;
        }
      }

      // if there is a unit here
      BattleHexMap::iterator lower_iter;
      BattleHexMap::iterator upper_iter;

      const BattleData * bd = d_batData;
      if(const_cast<BattleData *>(bd)->hexMap()->find(d_mouseMoveHex, lower_iter, upper_iter)) {

        const BattleUnit * unit = d_batData->hexMap()->unit(lower_iter);

        // ensure we are refenencing a CP
        const BattleCP * cp = dynamic_cast<const BattleCP *>(unit);

        if(! cp) {
          const BattleSP * sp;
          sp = dynamic_cast<const BattleSP *>(unit);
          ASSERT(sp);
          cp = sp->parent();
        }
        ASSERT(cp);

        // if the unit belongs to player's side, then change cursor
        if(GamePlayerControl::canControl(cp->getSide())) {

          // OverUnit cursor
          d_desiredCursorMode = Cursor_OverUnit;
          setCursor();
          return;
        }
      }

      // just the Normal cursor
      d_desiredCursorMode = Cursor_Normal;
      setCursor();
      return;
    }




    case BUI_UnitInterface::Targetting : {

      /*
        Change cursor if we are over a viable target
      */

      d_batWind->updateTracking(d_mouseMoveHex);

      BattleHexMap::iterator lower_iter;
      BattleHexMap::iterator upper_iter;

      // if cursor is over a unit
      const BattleData * bd = d_batData;
      if(const_cast<BattleData *>(bd)->hexMap()->find(d_mouseMoveHex, lower_iter, upper_iter)) {

        // get unit clicked on
        const BattleUnit * unit = d_batData->hexMap()->unit(lower_iter);

        // ensure we are refenencing a CP
        const BattleCP * cp = dynamic_cast<const BattleCP *>(unit);

        if(! cp) {
          const BattleSP * sp;
          sp = dynamic_cast<const BattleSP *>(unit);
          ASSERT(sp);
          cp = sp->parent();
        }
        ASSERT(cp);
        ASSERT(d_selectedUnit);

        /*
          is this a valid target
        */
        bool valid_target = Combat_Util::canBombard(bd, d_selectedUnit, cp);
        // Set cursor, if we have a ValidTarget
        if(valid_target) d_desiredCursorMode = Cursor_ValidTarget;
        else d_desiredCursorMode = Cursor_InvalidTarget;
        setCursor();
        return;
      }

      // if no unit under cursor, then just use targetting cursor
      else {

        // Set cursor to indicate TargettingMode
        d_desiredCursorMode = Cursor_TargetMode;
        setCursor();
      }
      return;
    }

    default : {
      FORCEASSERT("Oops - OnMove is in an undefined BUI_UnitInterface mode.  Bad news..\n");
      return;
    }

  } // end of mode() switch

}




bool
BUI_UnitImp::setCursor(void) {

  if(d_currentCursorMode == d_desiredCursorMode) return true;

  d_currentCursorMode = d_desiredCursorMode;
//   HCURSOR cursor;

  switch(d_currentCursorMode) {

    case Cursor_Normal : {

      SetCursor(hCursor_Normal);
      return true;
    }

    case Cursor_OverUnit : {

      SetCursor(hCursor_OverUnit);
      return true;
    }

    case Cursor_DragUnit : {

      SetCursor(hCursor_DragUnit);
      return true;
    }

    case Cursor_BadHex : {

      SetCursor(hCursor_BadHex);
      return true;
    }

    case Cursor_TargetMode : {

      SetCursor(hCursor_TargetMode);
      return true;
    }

    case Cursor_ValidTarget : {

      SetCursor(hCursor_ValidTarget);
      return true;
    }

    case Cursor_InvalidTarget : {

      SetCursor(hCursor_InvalidTarget);
      return true;
    }

    case Cursor_AttatchUnit : {

      SetCursor(hCursor_AttatchUnit);
      return true;
    }

    default : {
      FORCEASSERT("Oops - UserInterface trying to set cursor for mode which doesn't exist.\n");
      return true;
    }
  }

}



/*
 * Mouse is not over anything (e.g. off map)
 */

void
BUI_UnitImp::overNothing(void) {

}



#if 0
void
BUI_UnitImp::findHighlightHexes(
                                BattleOrderInfo& order,
                                    HexPosition::Facing lastFacing,
                                    CPFormation lastFormation,
                                    const int spCount,
                                    const HexCord& hex,
                                    HighlightList& hl) {

  // hex is the tentative startpoint
  // it may be adjusted depending on deployment, manuever etc.
  HexCord leftHex = hex;

  int nDir = 0;
  if(!B_Logic::findOrderLeft(const_cast<BattleData*>(reinterpret_cast<const BattleData*>(d_batData)),
                             order, spCount, lastFacing, lastFormation, hex, leftHex, nDir))
  {
    FORCEASSERT("Order Left not found");
  }

  hl.newItem(leftHex);

  if(nDir > 0) {

    HexCord::HexDirection hdR = rightFlank(lastFacing);

    while(--nDir) {

      HexCord hex2;
      if(d_batData->moveHex(leftHex, hdR, hex2)) {
        leftHex = hex2;
        hl.newItem(leftHex);
      }
    }
  }

}

#endif


void
BUI_UnitImp::updateDestination(void) {


  BattleOrderInfo* order = (d_currentOrder.nextWayPoint()) ? &d_currentOrder.wayPoints().getLast()->d_order : &d_currentOrder.order();
/*
// if an attach order, make sure we have a target unit under hex
if((d_selectedUnit->getRank().isHigher(Rank_Division)) && (order->d_mode == BattleOrderInfo::Attach)) {

BattleList bl;
if(BobUtility::unitsUnderHex(
d_batData,
d_info.hex(),
const_cast<RefBattleCP>(d_selectedUnit),
bl, BobUtility::Friendly,
False,
0)) {

ASSERT(bl.entries() > 0);
d_currentOrder.attachTo(bl.first()->d_cp);
}

else return;
}
*/
  // otherwise this is a move order

  /*

  NOTE : this line commented out due to causing DeployItem exceptions

  HexCord srcHex = (d_currentOrder.lastWayPoint()) ? d_currentOrder.lastWayPoint()->d_hex : (d_selectedUnit->sp() != NoBattleSP) ? const_cast<RefBattleCP>(d_selectedUnit)->leftHex() : d_selectedUnit->hex();
  */
  order->d_mode = BattleOrderInfo::Move;

  if(d_selectedUnit->getRank().sameRank(Rank_Division)) order->d_whatOrder.add(WhatOrder::Facing);

  WayPoint* wp = new WayPoint(d_info.hex(), *order);
  ASSERT(wp);

  wp->d_order.d_whatOrder.reset();
// if(d_selectedUnit->getRank().sameRank(Rank_Division)) wp->d_order.d_whatOrder.add(WhatOrder::Facing);

  d_currentOrder.wayPoints().append(wp);

}





/*----------------------------------------------------
 * Implement BUI_UnitInterface
 */

/*
 * Send order to current unit
 */


void
BUI_UnitImp::sendOrder(void) {

  sendBattleOrder(d_selectedUnit, &d_currentOrder);
  cancelOrder();

}

/*
 * cancel the order
 */

void
BUI_UnitImp::cancelOrder(void) {


  d_batData->soundSystem()->triggerSound(SOUNDTYPE_WINDOWCLOSE);
  d_orderWind->hide();

  // this highlight is for the deploy-list hexes
  highlightUnitWithoutDeploy(false);

  if(d_selectedUnit) {

    if(d_unitHighlighted) {
      highlightUnitAndChildren(false);
      d_unitHighlighted =false;
    }

    d_selectedUnit = NULL;
  }

  d_mode = BUI_UnitInterface::Normal;
}











void
BUI_UnitImp::highlightUnit(bool on) {

  d_deploymentHexList.reset();

  HexCord hex_offset(d_deploymentHex.x() - d_deploymentMouseOffset.x(), d_deploymentHex.y() - d_deploymentMouseOffset.y() );

  const BattleData * bd = d_batData;
  //doPlayerDeploy(const_cast<BattleData *>(bd), d_selectedUnit, d_deploymentHex, &d_deploymentHexList);
  doPlayerDeploy(const_cast<BattleData *>(bd), d_selectedUnit, hex_offset, &d_deploymentHexList);

  highlightUnitWithoutDeploy(on);


}




void
BUI_UnitImp::highlightUnitWithoutDeploy(bool on) {

  // set hex highlights on deploying hexes
  SListIterR<HexItem> deploy_iter(&d_deploymentHexList);

  deploy_iter = SListIterR<HexItem>(&d_deploymentHexList);
  BattleMap * map = const_cast<BattleMap *>(d_batData->map() );
  while(++deploy_iter) {
    if(on) map->highlightHex(deploy_iter.current()->d_hex, d_selectedUnit->getSide()  | HIGHLIGHT_SET);
    else map->unhighlightHex(deploy_iter.current()->d_hex );
  }

  redrawMap();
}



void
BUI_UnitImp::highlightUnitAndChildren(bool state) {

  // if we're unhighlighting, use the hexlist we already built up
  if(!state) {
    SListIterR<HexItem> hexlist_iter(&d_selectedUnitHexList);
    hexlist_iter = SListIterR<HexItem>(&d_selectedUnitHexList);
    BattleMap * map = const_cast<BattleMap *>(d_batData->map() );
    while(++hexlist_iter) {
      map->unhighlightHex(hexlist_iter.current()->d_hex);
    }
    map->unhighlightHex(d_selectedUnit->hex() );
    return;
  }

  // otherwise, reset list & add hexes to it (highlighting as we go)

  d_selectedUnitHexList.reset();

  const BattleCP * top_cp = d_selectedUnit;

  bool darkest;
  if(top_cp->getRank().isHigher(Rank_Division)) darkest = true;
  else darkest = false;

  unsigned char h_flags = top_cp->getSide() | HIGHLIGHT_SET;
  if(darkest) h_flags |= HIGHLIGHT_DARKEST;
  else h_flags |= HIGHLIGHT_DARKENED;


  ConstBattleUnitIter cp_iter(top_cp);
  while(!cp_iter.isFinished()) {

    ConstBattleSPIter sp_iter(d_batData->ob(), cp_iter.cp());

    while(!sp_iter.isFinished()) {

//       if((GamePlayerControl::getControl(cp_iter.cp()->getSide()) == GamePlayerControl::Player) || (sp_iter.sp()->visibility() != Visibility::NotSeen)) {
      BattleMap * map = const_cast<BattleMap *>(d_batData->map() );
      if(map->get(sp_iter.sp()->hex()).d_visibility > Visibility::NotSeen) {
        map->highlightHex(sp_iter.sp()->hex(), h_flags);
        d_selectedUnitHexList.newItem(sp_iter.sp()->hex());
      }
      sp_iter.next();
    }

    if(cp_iter.cp()->getRank().isHigher(Rank_Division)) {
      BattleMap * map = const_cast<BattleMap *>(d_batData->map() );
      if(map->get(cp_iter.cp()->hex()).d_visibility > Visibility::NotSeen) {
        map->highlightHex(cp_iter.cp()->hex(), h_flags);
        d_selectedUnitHexList.newItem(cp_iter.cp()->hex());
      }
    }

    cp_iter.next();
  }

  redrawMap();
}




void
BUI_UnitImp::highlightTarget(bool state) {
/*
  BattleMap * map = const_cast<BattleMap *>(d_batData->map() );

  // if we're unhighlighting, use the hexlist we already built up
  if(!state) {
  SListIterR<HexItem> hexlist_iter(&d_targetUnitHexList);
  hexlist_iter = SListIterR<HexItem>(&d_targetUnitHexList);
  BattleMap * map = const_cast<BattleMap *>(d_batData->map() );
  while(++hexlist_iter) {
  map->unhighlightHex(hexlist_iter.current()->d_hex);
  }
  return;
  }

  d_targetUnitHexList.reset();

  // highlight CP
  map->highlightHex(d_targetUnit->hex(), d_targetUnit->getSide() | HIGHLIGHT_SET | HIGHLIGHT_DARKEST);
  d_targetUnitHexList.newItem(d_targetUnit->hex());

  BattleSPIter sp_iter(d_batData->ob(), d_targetUnit);
  while(!sp_iter.isFinished()) {

  map->highlightHex(sp_iter.sp()->hex(), d_targetUnit->getSide() | HIGHLIGHT_SET | HIGHLIGHT_DARKEST);
  d_selectedUnitHexList.newItem(sp_iter.sp()->hex());

  sp_iter.next();
  }
*/
}





void
BUI_UnitImp::highlightDestination(bool state) {

  /*
    Make thread-safe (d_selectedUnit was sometimes changing status half-way through routine)
  */
  RWLock readLock;
  readLock.startRead();

  BattleMap * map = const_cast<BattleMap *>(d_batData->map() );

  // if we're unhighlighting, use the hexlist we already built up
  if(!state) {
    SListIterR<HexItem> hexlist_iter(&d_destinationHexList);
    hexlist_iter = SListIterR<HexItem>(&d_destinationHexList);
    BattleMap * map = const_cast<BattleMap *>(d_batData->map() );
    while(++hexlist_iter) {
      map->unhighlightHex(hexlist_iter.current()->d_hex);
    }
    return;
  }

  d_destinationHexList.reset();

  // if no selected unit, or selected unit is enemy, we return
  if(!d_selectedUnit || (!GamePlayerControl::canControl(d_selectedUnit->getSide()))) return;

  // get batdata into a useable format
  const BattleData * bdcp = d_batData;
  BattleData * bd = const_cast<BattleData *>(bdcp);

  // get first waypoint hex form order
  const WayPointList * waypoints = &(d_selectedUnit->getCurrentOrder().wayPoints());
  // const WayPoint * wp = waypoints->first();
  const WayPoint * wp = waypoints->head();

  // if no waypoint, then we have no move order
  if(wp) {

    BattleOrderInfo* oi = (d_currentOrder.nextWayPoint()) ? &(d_currentOrder.wayPoints().getLast()->d_order) : &(d_currentOrder.order() );

    bool retval = PlayerDeploy::playerDeploy(
                                             bd,
                                                 d_selectedUnit,
                                                 wp->d_hex,
                                                 oi,
                                                 &d_destinationHexList,
                                                 PlayerDeploy::FILLHEXLIST | PlayerDeploy::NEVERFAIL
//       PlayerDeploy::None
      );

    // highlight hexes
    SListIterR<HexItem> hexlist_iter(&d_destinationHexList);
    hexlist_iter = SListIterR<HexItem>(&d_destinationHexList);
    while(++hexlist_iter) {
      map->highlightHex(hexlist_iter.current()->d_hex, d_selectedUnit->getSide() | HIGHLIGHT_SET | HIGHLIGHT_DARKENED);
    }
  }

  /*
    End exclusion
  */
  readLock.endRead();
}








/*
  Wrapper to insulate deployment calls
*/
bool
BUI_UnitImp::doPlayerDeploy(RPBattleData batData, const CRefBattleCP& cp, const HexCord& startHex, HexList* hl) {

//    return PlayerDeploy::getPlayerDeployHexes(batData, cp, startHex, &d_currentOrder, hl, PlayerDeploy::None);

  BattleOrderInfo* oi = (d_currentOrder.nextWayPoint()) ? &(d_currentOrder.wayPoints().getLast()->d_order) : &(d_currentOrder.order() );

  return PlayerDeploy::playerDeploy(batData, cp, startHex, oi, hl, PlayerDeploy::FILLHEXLIST | PlayerDeploy::NEVERFAIL);
}





void
BUI_UnitImp::SetWaypointDeploymentHexes(CPBattleData batData, const CRefBattleCP& cp) {

  // clear out waypoint-hexes-list
  d_waypointDeploymentHexes.reset();
  // get the slist of hex-lists
  SList <WaypointHexes> * waypoint_list = &(d_waypointDeploymentHexes.d_waypoints);

  // get the current order's waypoints
  WayPointList * waypoints = &(d_currentOrder.wayPoints());
  // get the first one
  WayPoint * wp = waypoints->first();

  while(wp) {

    HexCord waypoint_hex = wp->d_hex;
    WaypointHexes * hexlist = new WaypointHexes;

    const BattleData * bdp1 = batData;
    BattleData * bdp2 = const_cast<BattleData *>(bdp1);
//        PlayerDeploy::playerDeployFromOrder(bdp2, cp, waypoint_hex, &(wp->d_order), &(hexlist->d_hexlist), PlayerDeploy::None);
    PlayerDeploy::playerDeploy(bdp2, cp, waypoint_hex, &(wp->d_order), &(hexlist->d_hexlist), PlayerDeploy::FILLHEXLIST | PlayerDeploy::NEVERFAIL);

    waypoint_list->append(hexlist);

    wp = waypoints->next();
  }

}











void
BUI_UnitImp::showOrders(void) {
/*
  d_mode = BUI_UnitInterface::Normal;
  setFlag(PlottingMove, True);
  d_point.set(d_point.getX(), d_point.getY());
  showOrderWind();
*/

  // show the small orders dialog
// POINT p = info.pixelPoint();
// ClientToScreen(d_batWind->hwnd(), &p);
// d_point.set(p.x, p.y);

  if(!d_selectedUnit) return;

  d_info.cp(d_selectedUnit);
  d_info.hex(d_selectedUnit->hex() );

  d_point.set(d_point.getX(), d_point.getY());

  // Note from Paul:
  // Since this is called from the right-click pop-up menu,
  // we are operating on the base order not the last way-point.
  BattleOrderInfo* order =  &d_currentOrder.order();//(d_currentOrder.nextWayPoint()) ? &d_currentOrder.wayPoints().getLast()->d_order : &d_currentOrder.order();


// HexCord hex_offset(d_leftButtonUpHex.x() - d_deploymentMouseOffset.x(), d_leftButtonUpHex.y() - d_deploymentMouseOffset.y() );
// d_info.hex(hex_offset);

//    updateDestination();

//    BattleOrderInfo* order = (d_currentOrder.nextWayPoint()) ? &d_currentOrder.wayPoints().getLast()->d_order : &d_currentOrder.order();

//    d_deploymentHex = d_leftButtonUpHex;

  //updateDestination();

  //highlightUnitAndChildren(true);
  //d_unitHighlighted = true;

  d_batData->soundSystem()->triggerSound(SOUNDTYPE_WINDOWOPEN);
  showOrderWind(d_point, d_info, order, CtrlInfo::SmallMode);

}

void
BUI_UnitImp::showDeployment(void) {
/*
  setFlag(PlottingMove, True);
  d_mode = Deploy;
  d_point.set(d_point.getX(), d_point.getY());
  showOrderWind();
*/
}

void
BUI_UnitImp::showInfo(void) {

  if(d_selectedUnit != NULL) {
    d_point.set(d_point.getX(), d_point.getY());
    d_infoWind->run(d_selectedUnit, d_point);
  }

}

// show a XX's Line of fire.
// That is, all hexes that it can fire into
void
BUI_UnitImp::showLOS(void) {
#if 0

  BattleData* bd = const_cast<BattleData*>(reinterpret_cast<const BattleData*>(d_batData));
  BattleCP* cp = const_cast<BattleCP*>(reinterpret_cast<const BattleCP*>(d_cp));

  if(d_cp != NoBattleCP) {

    for(BattleUnitIter iter(cp); !iter.isFinished(); iter.next()) {

      if(iter.cp()->getRank().isHigher(Rank_Division) || iter.cp()->sp() == NoBattleSP) continue;

      // get range in hexes sp can fire
      int cRange = Combat_Util::getRange(bd, iter.cp()) / BattleMeasure::XYardsPerHex;

      if(cRange > 0) {

        enum { Right, Left };

        // go through front row and determine hexes unit can fire into
        int cols = (iter.cp()->formation() <= iter.cp()->nextFormation()) ? iter.cp()->columns() : iter.cp()->wantColumns();

        for(int c = 0; c < cols; c++) {

          DeployItem& di = iter.cp()->deployItem(c, 0);
          if(di.active()) {

            HexCord::HexDirection hd = leftFront(iter.cp()->facing());
            HexCord::HexDirection rHD = rightFlank(iter.cp()->facing());
            HexCord lHex = di.d_sp->hex();
            int inYards = 0;
            int loop = cRange;
            int goRightHowMany = 1;

            while(loop) {
              inYards += BattleMeasure::XYardsPerHex;
              HexCord hex;
              if(d_batData->moveHex(lHex, hd, hex)) lHex = hex;
              else break;

              goRightHowMany++;
              HexCord rHex = lHex;
              bool breakOuterLoop = False;

              for(int i = 0; i < goRightHowMany; i++) {

//                      if(Combat_Util::lineOfSight(bd, di.d_sp->hex(), rHex, 0)) d_highlightedHexes.newItem(rHex);
                if(d_los.FastLOS(bd->map(), di.d_sp->hex(), rHex)) d_highlightedHexes.newItem(rHex);

                if(bd->moveHex(rHex, rHD, hex)) rHex = hex;
                else {

                  breakOuterLoop = True;
                  break;
                }
              }

              if(breakOuterLoop) break;

              loop = maximum(0, loop - 1);
            }
          }
        }

        // set highlight flags
        SListIterR<HighlightItem> iter(&d_highlightedHexes);
        while(++iter) {

          if(iter.current()->d_unit != NoBattleUnit) iter.current()->d_unit->unitSelected(True);
          else bd->map()->highlightHex(iter.current()->d_hex, d_cp->getSide()  | HIGHLIGHT_SET | HIGHLIGHT_DARKENED);
        }

        if(d_highlightedHexes.entries() > 0) redrawMap();
      }
    }
  }
#endif
}




/*========================================================================
 * Public Interface Functions
 */

BattleUnitUI::BattleUnitUI(RPBattleWindows batWind, RCPBattleData batData) : d_imp(new BUI_UnitImp(batWind, batData)) {

}

BattleUnitUI::~BattleUnitUI(void) {
  delete d_imp;
}

void
BattleUnitUI::onLButtonDown(const BattleMapSelect& info) {

  ASSERT(d_imp != 0);
  d_imp->onLButtonDown(info);
}

void
BattleUnitUI::onLButtonUp(const BattleMapSelect& info) {

  ASSERT(d_imp != 0);
  d_imp->onLButtonUp(info);
}

void
BattleUnitUI::onRButtonDown(const BattleMapSelect& info) {

  ASSERT(d_imp != 0);
  d_imp->onRButtonDown(info);
}

void
BattleUnitUI::onRButtonUp(const BattleMapSelect& info) {

  ASSERT(d_imp != 0);
  d_imp->onRButtonUp(info);
}

void
BattleUnitUI::onStartDrag(const BattleMapSelect& info) {

  ASSERT(d_imp != 0);
  d_imp->onStartDrag(info);
}

void
BattleUnitUI::onEndDrag(const BattleMapSelect& info) {

  ASSERT(d_imp != 0);
  d_imp->onEndDrag(info);
}


void
BattleUnitUI::onMove(const BattleMapSelect& info) {

  ASSERT(d_imp != 0);
  d_imp->onMove(info);
}

void
BattleUnitUI::overNothing(void) {

  ASSERT(d_imp != 0);
  d_imp->overNothing();
}

bool
BattleUnitUI::setCursor(void) {

  ASSERT(d_imp != 0);
  return d_imp->setCursor();
}

void
BattleUnitUI::destroy(void) {

  ASSERT(d_imp != 0);
  d_imp->destroy();
}


void
BattleUnitUI::timeChanged(void) {

  ASSERT(d_imp != 0);
  d_imp->timeChanged();
}


void
BattleUnitUI::mapZoomChanged(void) {

  ASSERT(d_imp != 0);
  d_imp->mapZoomChanged();
}


void
BattleUnitUI::selectUnit(BattleCP * cp) {

  ASSERT(d_imp != 0);
  d_imp->selectUnit(cp);
}


/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.6  2002/11/24 13:17:48  greenius
 * Added Tim Carne's editor changes including InitEditUnitTypeFlags.
 *
 * Revision 1.5  2002/11/16 18:03:30  greenius
 * Various changes so that it will compile with Visual Studio .net
 *
 * Revision 1.4  2001/11/26 10:34:41  greenius
 * Current Directory detection improved
 *
 * Battlegame sound system initialised properly
 *
 * BattleAI Message queue problems fixed
 *
 * Order of Battles loading
 *
 * HexMap uses reference counted units
 *
 * BattleEditor open-file dialogs use wrapped classes
 *
 * PathFind compiler warnings removed
 *
 * Orphan detection in BattleOB detected and removed
 *
 * Revision 1.3  2001/07/02 13:18:24  greenius
 * Battle, Campaign, Combined game and Campaign Editor now compile and run... mostly
 *
 * Revision 1.2  2001/06/18 08:25:44  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
