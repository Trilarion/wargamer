/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef D_SELECT_HPP
#define D_SELECT_HPP

/*
 * Choose Area of Battlefield to play on
 */


// #include "batwind_dll.h"
#include "bw_mode.hpp"
// #include "wind.hpp"
// #include "palwind.hpp"
#include "batcord.hpp"
// #include "btwin_i.hpp"
#include "batmap_i.hpp"
#include "d_selsde.hpp"
#include "..\gwind\titlebar.hpp"

using BattleMeasure::HexCord;

// class WindowChangeUser;
class BattleData;
struct BattleInfo;
class BattleMapWind;
class SelectionSideWindow;

class DeploySelectionWindows :
    public BW_Mode,
    public SelectionSideUser
{
    public:

       DeploySelectionWindows(BW_ModeOwner* owner, const BattleInfo& batinfo);
       ~DeploySelectionWindows();

       //void cropMap();
	   //static void cropMap(BattleData * batData, HexCord center, HexCord size);
	   void SetPlayingArea(void);

       // Selection Side User

       virtual HWND hwnd() const { return d_owner->hwnd(); }
       virtual void onFinish() = 0;        // batwind handles this

       /*
        * BW_Mode Implementation
        */

       virtual void show(bool visible);

       virtual void positionWindows(const PixelRect& r, DeferWindowPosition* def);

       virtual void onLButtonDown(const BattleMapSelect& info);
       virtual void onLButtonUp(const BattleMapSelect& info);
       virtual void onRButtonDown(const BattleMapSelect& info);
       virtual void onRButtonUp(const BattleMapSelect& info);
       virtual void onStartDrag(const BattleMapSelect& info);
       virtual void onEndDrag(const BattleMapSelect& info);
       virtual void onMove(const BattleMapSelect& info);
       virtual bool setCursor();

       virtual bool hasWindow(BW_ID id) = 0;  // Implemented higher up

    // BATWIND_DLL void SetSize(RECT rect);
    // BATWIND_DLL void ShowWindows(void);

private:

    void CreateWindows(void);
    // void DestroyWindows(void);


    void SetSelection(void);


    /*
    Message Processing
    */

    // LRESULT procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
    // 
    // void onNCPaint(HWND hwnd, HRGN hrgn);
    // void onPaint(HWND hwnd);
    // void onGetMinMaxInfo(HWND hWnd, LPMINMAXINFO lpMinMaxInfo);


    /*
     * BattleMap Interface Functions
     */
    
    // virtual HWND hwnd() const { return getHWND(); }

    // BattleMapWind * getBattleMap(void) { return m_MapWind; }
    
    // virtual bool setMapLocation(const BattleLocation& l);
            // Set centre of main map, return true if has changed
    // virtual void mapAreaChanged(const BattleArea& area);
            // Sent by mapwind when it has changed its area
    // virtual void mapZoomChanged();
            // Get mapwindow zoom buttons updated
public:
    // virtual void onLButtonDown(const BattleMapSelect& info);
            // sent by mapwind when Button is pressed
    // virtual void onLButtonUp(const BattleMapSelect& info);
            // sent by mapwind when Button is released while not dragging
    // virtual void onRButtonDown(const BattleMapSelect& info);
            // sent by mapwind when Button is pressed
    // virtual void onRButtonUp(const BattleMapSelect& info);
            // sent by mapwind when Button is released while not dragging
    // virtual void onStartDrag(const BattleMapSelect& info);
            // sent by mapwind when Mouse is moved while button is held
    // virtual void onEndDrag(const BattleMapSelect& info);
            // sent by mapwind when button is released while dragging
    // virtual void onMove(const BattleMapSelect& info);
            // sent by mapwind when Mouse has moved
    // virtual bool setCursor();
private:
    virtual void redrawMap(bool all);
    // virtual void orderCP(CRefBattleCP cp);
    // virtual void messageWindowUpdated();
    // virtual void positionWindows();
    // virtual void updateAll();
    // virtual bool updateTracking(const BattleMeasure::HexCord& hex);
    // virtual const DrawDIBDC* mapDib() const;
    // virtual BattleMapDisplay* mapDisplay() const;

        void setMapMode(BattleMapInfo::Mode mode);

    private:

       // WindowChangeUser * m_lpWindowChangeUser;
       // HWND m_ParentHwnd;
       // 
       // RECT m_WindowRect;
       // RECT m_MapRect;
       // RECT m_SideBarRect;
       
       SelectionSideWindow * m_lpSideBar;

       BattleMapWind* m_MapWind;
       BattleData * m_lpBattleData;
       const BattleInfo& m_lpBattleInfo;

       HexCord select_center;
       HexCord select_size;

	   TitleBarClass * m_TitleBar;
	   RECT m_TitleBarRect;
};


#if 0     // Jim's old version

class DeploySelectionWindows :
    public BattleWindowsInterface,
    public WindowBaseND,
    public PaletteWindow,
    public CustomBorderWindow {

public:

    BATWIND_DLL DeploySelectionWindows(WindowChangeUser * window_user, HWND hparent, BattleData * batdata, const BattleInfo * batinfo);
    BATWIND_DLL ~DeploySelectionWindows(void);
    BATWIND_DLL void SetSize(RECT rect);
    BATWIND_DLL void ShowWindows(void);

private:

    void CreateWindows(void);
    void DestroyWindows(void);


    void SetSelection(void);

    WindowChangeUser * m_lpWindowChangeUser;
    HWND m_ParentHwnd;

    RECT m_WindowRect;
    RECT m_MapRect;
    RECT m_SideBarRect;

//    RefPtr<BattleMapWind> m_MapWind;
    BattleMapWind * m_MapWind;
    SelectionSideWindow * m_lpSideBar;
	

    BattleData * m_lpBattleData;
    const BattleInfo* m_lpBattleInfo;

    HexCord select_center;
    HexCord select_size;


    /*
    Message Processing
    */

    LRESULT procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);

    void onNCPaint(HWND hwnd, HRGN hrgn);
    void onPaint(HWND hwnd);
    void onGetMinMaxInfo(HWND hWnd, LPMINMAXINFO lpMinMaxInfo);



    /*
    BattleMap Interface Functions
    */
    
    virtual HWND hwnd() const { return getHWND(); }

    BattleMapWind * getBattleMap(void) { return m_MapWind; }
    
    virtual bool setMapLocation(const BattleLocation& l);
            // Set centre of main map, return true if has changed
    virtual void mapAreaChanged(const BattleArea& area);
            // Sent by mapwind when it has changed its area
    virtual void mapZoomChanged();
            // Get mapwindow zoom buttons updated
public:
    virtual void onLButtonDown(const BattleMapSelect& info);
            // sent by mapwind when Button is pressed
    virtual void onLButtonUp(const BattleMapSelect& info);
            // sent by mapwind when Button is released while not dragging
    virtual void onRButtonDown(const BattleMapSelect& info);
            // sent by mapwind when Button is pressed
    virtual void onRButtonUp(const BattleMapSelect& info);
            // sent by mapwind when Button is released while not dragging
    virtual void onStartDrag(const BattleMapSelect& info);
            // sent by mapwind when Mouse is moved while button is held
    virtual void onEndDrag(const BattleMapSelect& info);
            // sent by mapwind when button is released while dragging
    virtual void onMove(const BattleMapSelect& info);
            // sent by mapwind when Mouse has moved
    virtual bool setCursor();
private:
    virtual void redrawMap(bool all);
    virtual void orderCP(CRefBattleCP cp);
    virtual void messageWindowUpdated();
    // virtual void windowDestroyed(HWND hwnd);
    virtual void positionWindows();
    virtual void updateAll();
    virtual bool updateTracking(const BattleMeasure::HexCord& hex);
    virtual const DrawDIBDC* mapDib() const;
    virtual BattleMapDisplay* mapDisplay() const;

    void setMapMode(BattleMapInfo::Mode mode);

};

#endif



#endif
    
