/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
#include "stdinc.hpp"
#include "d_plcsde.hpp"
#include "scenario.hpp"
#include "dib.hpp"
#include "app.hpp"
// #include "generic.hpp"
#include "dplyuser.hpp"

#include "fonts.hpp"
#include "cbutton.hpp"
#include "resstr.hpp"

#define FINISH_PLACEMENT_BUTTON       1
#define OOB_TREEVIEW 100


PlacementSideWindow::PlacementSideWindow(PlacementSideUser* user, Side side) :
    CustomBorderWindow(scenario->getBorderColors() ),
    BattleOBTreeView(user),
    m_ParentHwnd(user->hwnd()),
    m_WindowRect(),
    m_TitleRect(),
    m_OBRect(),
    m_OKRect(),
    m_TreeViewHwnd(NULL),
    m_OKButton(NULL),
    m_PlayerSide(side),
    m_lpBattleData(user->battleData()),
    m_Dib(0),
    d_owner(user)
{
    // m_lpWindowChangeUser = window_user;
    // m_ParentHwnd = hparent;
    // m_lpBattleData = batdata;
    // m_PlayerSide = side;

    m_Dib = new DrawDIBDC(64,64);

    CreateWindows();
}

PlacementSideWindow::~PlacementSideWindow(void)
{
    DestroyWindows();
    delete m_Dib;
}

void
PlacementSideWindow::CreateWindows(void) {

    createWindow(
        0,
        // GenericNoBackClass::className(),
        NULL,
        WS_CHILD,
        0,0,64,64,
        m_ParentHwnd,
        NULL
        // APP::instance()
    );

    SetWindowText(getHWND(), "SelectionWindowSideBar");


    // create TreeView
    m_TreeViewHwnd = createTV(getHWND(), OOB_TREEVIEW);
    createImageList(m_TreeViewHwnd);
    setTreeRoot(m_lpBattleData->ob()->getTop(m_PlayerSide) );
    addItems(m_TreeViewHwnd, m_lpBattleData);

   // ensure it's opened
   HTREEITEM hroot = TreeView_GetRoot(m_TreeViewHwnd);
   TreeView_Expand(m_TreeViewHwnd, hroot, TVE_EXPAND);
   HTREEITEM hchild = TreeView_GetChild(m_TreeViewHwnd, hroot);
   while(hchild) {
      TreeView_Expand(m_TreeViewHwnd, hchild, TVE_EXPAND);
      hchild = TreeView_GetNextSibling(m_TreeViewHwnd, hchild);
   }

    ShowWindow(m_TreeViewHwnd, SW_SHOW);


    // init custom buttons class
    CustomButton::initCustomButton(APP::instance() );

    // select buttons filldib
    const DrawDIB* fillDib = scenario->getScenarioBkDIB(Scenario::BackPatterns::HScrollThumbBackground);

    // select buttons font
    LogFont lf;
    lf.height(14);
    lf.weight(FW_MEDIUM);
    lf.face(scenario->fontName(Font_Bold));
    Font font;
    font.set(lf);

    // OK button
    m_OKButton = CreateWindow(
        CUSTOMBUTTONCLASS,
        InGameText::get(IDS_FinishDeployment),
        BS_PUSHBUTTON | WS_CHILD | WS_VISIBLE,
        0, 0, 64, 64,
        getHWND(),
        (HMENU) FINISH_PLACEMENT_BUTTON,
        APP::instance(),
        NULL);

    CustomButton cb(m_OKButton);
    cb.setFillDib(fillDib);
    cb.setBorderColours(scenario->getBorderColors());
    cb.setFont(font);

}








void
PlacementSideWindow::DestroyWindows(void) {

    if(m_OKButton) { DestroyWindow(m_OKButton); m_OKButton = 0; }

    selfDestruct();


}








void
PlacementSideWindow::SetSize(RECT rect)
{

    m_WindowRect.left = rect.left;
    m_WindowRect.top = rect.top;
    m_WindowRect.right = rect.right - rect.left;
    m_WindowRect.bottom = rect.bottom - rect.top;

    int h_inset = m_WindowRect.right / 16;
    int h_end = m_WindowRect.right - (h_inset*2);
    int v_inset = m_WindowRect.bottom / 16;
    int width = m_WindowRect.right;
    int height = m_WindowRect.bottom;

    m_TitleRect.left = 0;
    m_TitleRect.right = width;
    m_TitleRect.top = 0;
    m_TitleRect.bottom = v_inset;

    m_OBRect.left = h_inset;
    m_OBRect.right = width - (h_inset *2);
    m_OBRect.top = v_inset * 1;
    m_OBRect.bottom = v_inset * 12;



    m_OKRect.left = h_inset;
    m_OKRect.right = width - (h_inset *2);
    m_OKRect.top = ( (height / 8) * 7) + (height / 16);
    m_OKRect.bottom = height / 16;


    delete(m_Dib);
    m_Dib = new DrawDIBDC(m_WindowRect.right, m_WindowRect.bottom);
    DrawDib();

    MoveWindow(m_TreeViewHwnd, m_OBRect.left, m_OBRect.top, m_OBRect.right, m_OBRect.bottom, TRUE);
    MoveWindow(getHWND(), m_WindowRect.left, m_WindowRect.top, m_WindowRect.right, m_WindowRect.bottom, TRUE);


    // select buttons font
/*
    LogFont lf;
    lf.height(m_OKRect.bottom - m_OKRect.top);
    lf.weight(FW_MEDIUM);
    lf.face(scenario->fontName(Font_Bold));
    Font font;
    font.set(lf);
*/
   MoveWindow(m_OKButton, m_OKRect.left, m_OKRect.top, m_OKRect.right, m_OKRect.bottom, TRUE);
    CustomButton cb(m_OKButton);
   InvalidateRect(m_OKButton, NULL, true);
   UpdateWindow(m_OKButton);
      // cb.setFont(font);
}



void
PlacementSideWindow::DrawDib(void) {

    // fill background
    const DrawDIB* fillDib = scenario->getScenarioBkDIB(Scenario::BackPatterns::CherryWoodBackground);
    ASSERT(fillDib);

    m_Dib->rect(0, 0, m_WindowRect.right, m_WindowRect.bottom, fillDib);

    // battlefield info
    m_Dib->lightenRectangle(
        m_OBRect.left,
        m_OBRect.top,
        m_OBRect.right,
        m_OBRect.bottom,
        8);

    m_Dib->frame(
        m_OBRect.left,
        m_OBRect.top,
        m_OBRect.right,
        m_OBRect.bottom,
        0);


}



/*
Message Processing
*/

LRESULT
PlacementSideWindow::procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam) {

    LRESULT l;
    if(handlePalette(hWnd, msg, wParam, lParam, l))
    return l;

    switch(msg) {
//        HANDLE_MSG(hWnd, WM_CREATE,    onCreate);
        HANDLE_MSG(hWnd, WM_COMMAND, onCommand);
//        HANDLE_MSG(hWnd, WM_NCHITTEST, onNCHitTest);
        HANDLE_MSG(hWnd, WM_ERASEBKGND, onEraseBk);
        HANDLE_MSG(hWnd, WM_PAINT, onPaint);
        HANDLE_MSG(hWnd, WM_NOTIFY, onNotify);
//        HANDLE_MSG(hWnd, WM_GETMINMAXINFO, onGetMinMaxInfo);
//        HANDLE_MSG(hWnd, WM_NCPAINT, onNCPaint);
//        HANDLE_MSG(hWnd, WM_HSCROLL, onHScroll);

        default : return defProc(hWnd, msg, wParam, lParam);
    }
}




void
PlacementSideWindow::onCommand(HWND hWnd, int id, HWND hwndCtl, UINT codeNotify) {

    switch (id) {

        case FINISH_PLACEMENT_BUTTON : {
            // m_lpWindowChangeUser->NextScreen();
            d_owner->onFinish();
            return;
        }

    }

}



void
PlacementSideWindow::onNCPaint(HWND hwnd, HRGN hrgn) {

    FORWARD_WM_NCPAINT(hwnd, hrgn, defProc);

    HDC hdc = GetWindowDC(hwnd);
    RECT r;
    GetWindowRect(hwnd, &r);
    OffsetRect(&r, -r.left, -r.top);

    HPALETTE oldPal = SelectPalette(hdc, Palette::get(), FALSE);
    RealizePalette(hdc);

    drawThickBorder(hdc, r);

    SelectPalette(hdc, oldPal, FALSE);

    ReleaseDC(hwnd, hdc);
}



BOOL
PlacementSideWindow::onEraseBk(HWND hwnd, HDC hdc) {

    return True;

}



void
PlacementSideWindow::onPaint(HWND hwnd) {

  PAINTSTRUCT ps;
  HDC hdc = BeginPaint(hwnd, &ps);

  HPALETTE oldPal = SelectPalette(hdc, Palette::get(), FALSE);
  RealizePalette(hdc);

  BitBlt(hdc, 0, 0, m_WindowRect.right, m_WindowRect.bottom, m_Dib->getDC(), 0, 0, SRCCOPY);

  SelectPalette(hdc, oldPal, FALSE);

  EndPaint(hwnd, &ps);

}




LRESULT
PlacementSideWindow::onNotify(HWND hWnd, int id, NMHDR* lpNMHDR) {

    switch(lpNMHDR->idFrom) {
        /*
        Notify Message from Tree View
        */
        case OOB_TREEVIEW : {

            switch(lpNMHDR->code) {


                /*
                DELETEITEM message
                */

                case TVN_DELETEITEM : {
                    NM_TREEVIEW * nmtv = (NM_TREEVIEW*) lpNMHDR;
                    BattleOBTreeViewInfo * info = (BattleOBTreeViewInfo *) nmtv->itemOld.lParam;
                    delete info;
                    return 0;
                }


                /*
                GETDISPINFO message
                */

                case TVN_GETDISPINFO : {
                    TV_DISPINFO * tvdi = (TV_DISPINFO*) lpNMHDR;

                    /*
                    Request for Item Text
                    */

                    if(tvdi->item.mask & TVIF_TEXT) {

                        // cast lParam back into a CP or SP
                        BattleOBTreeViewInfo * info = (BattleOBTreeViewInfo *) tvdi->item.lParam;
                        BattleCP * cp = info->cp;

                        // this item is a CP
                        if(cp) {
                            RankEnum rank = cp->getRank().getRankEnum();

                            if(rank == Rank_President) {
                                Side side = cp->getSide();
                                lstrcpy(tvdi->item.pszText, scenario->getSideName(side));
                            }
                            else {
                                char buffer[1024];
                                lstrcpy(buffer, cp->getName() );
                                strcat(buffer, " : ");
                                strcat(buffer, cp->leader()->getName() );
                                lstrcpy(tvdi->item.pszText, buffer);
                            }

                            return 0;
                        }

                        BattleSP * sp = info->sp;

                        // this item is an SP
                        if(sp) {

//                            sp_index = d_ob->getIndex(sp);
                            int sp_type = sp->getUnitType();
                            char * sp_name = const_cast<char *> (m_lpBattleData->ob()->spName(sp) );

                            lstrcpy(tvdi->item.pszText, sp_name);
                            return 0;
                        }

                        return 0;
                    } // end of handling text request

                } // end of handling GETDISPINFO request


                /*
                Handle Selection change
                */

                case TVN_SELCHANGED : {

                    NM_TREEVIEW* pnmtv = (NM_TREEVIEW *) lpNMHDR;

                    m_hTreeItem = pnmtv->itemNew.hItem;

                    // cast lParam back into a CP or SP
                    BattleOBTreeViewInfo * info = (BattleOBTreeViewInfo *) pnmtv->itemNew.lParam;
                    BattleCP * cp = info->cp;

                    if(cp) {
                        m_Unit = cp;
                        m_OBUser->onSelectUnit(m_Unit);
                    }

                    BattleSP * sp = info->sp;

                    if(sp) {
                        m_Unit = sp;
                        m_OBUser->onSelectUnit(m_Unit);
                    }

                    return 0;
                } // end of handling selection change


            } // end of lpNMHDR->code switch

        } // end of TREEVIEW case

    }
return 0;
}




/*
 *----------------------------------------------------------------------
 * $Log$
 *----------------------------------------------------------------------
 */
