/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef B_INFO_HPP
#define B_INFO_HPP

#ifndef __cplusplus
#error b_info.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *	Battle Unit Info Tracking Window
 *
 * Not going to bother with seperate hidden implementation for this
 * since it is only included by one other module anyway (bw_unit)
 *
 * In many ways this is similar to campwind\infowind.cpp, and could
 * derived from a common base class to handle the transparency.
 *
 *----------------------------------------------------------------------
 */

#include "batdata.hpp"		// batdata/
#include "wind.hpp"


class BattleMapSelect;
class BTW_Imp;
class PixelPoint;
class BattleUnit;
class BattleWindowsInterface;

class BattleTrackingWindow : public Window
{
	  BTW_Imp* d_trackWindow;          // actual window

	public:

		/*
		 * Public Functions
		 */

        BattleTrackingWindow(HWND hParent, BattleWindowsInterface* bw, RCPBattleData batData);	// Construct as a child of hParent
        ~BattleTrackingWindow();

        void reset();    // Invalidate current hex

        bool update(const BattleMeasure::HexCord& hex);
        void toggle();
        void show(bool visible);
        int height();
        int width();

        bool isVisible() const;
        bool isEnabled() const;
        void enable(bool visible);
        HWND getHWND() const;

};


#endif /* B_INFO_HPP */

