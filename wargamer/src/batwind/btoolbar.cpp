/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
#include "stdinc.hpp"
#include "btoolbar.hpp"
#include "batres.h"                             // res: Resources
#include "batres_s.h"                   // res: Resource Strings
#include "scn_res.h"
#include "scenario.hpp"
#include "scn_img.hpp"
#include "btwin_i.hpp"
#include "tooltip.hpp"
#include "res_str.h"
#include "gtoolbar.hpp"
#include "fonts.hpp"

#define DEBUG_MTOOLBAR
#ifdef DEBUG_MTOOLBAR
#include "logwin.hpp"
#endif



/*------------------------------------------------------------
 *  The mother window
 */

class BattleToolBar_Imp : public GToolBar
{
    BattleWindowsInterface* d_batWind;

    /*
     * Each section has its own MToolBar
     */

    enum Sections {
       File,
       Zoom,
       MapDisplay,
//     Info,
//     Help,

       Sections_HowMany
    };

    /*
     * Each Section has its own Button enum
     */

    enum FileButtons
    {
      Open,
      Save,
      File_HowMany
    };

    enum ZoomButtons
    {
      ZoomIn,
      ZoomOverview,
      ZoomDetail,
      Zoom2Mile,
      Zoom4Mile,
      Zoom_HowMany
    };

    enum MapDisplayButtons
    {
      HexOutLine,
      TinyMap,
      MessageWindow,
      TrackingWindow,
      MapDisplay_HowMany
    };

    enum InfoButtons
    {
      OBWindow,
      FindLeader,
      Info_HowMany
    };

    enum HelpButtons
    {
      OHR,
      HelpButton,
      Help_HowMany
    };

    static TipData s_FileTipData[];
    static TipData s_ZoomTipData[];
    static TipData s_MapDisplayTipData[];
    static TipData s_InfoTipData[];
    static TipData s_HelpTipData[];

    static TipData* s_td[Sections_HowMany];

    static ToolButtonData s_FileBD[File_HowMany];
    static ToolButtonData s_ZoomBD[Zoom_HowMany];
    static ToolButtonData s_MapDisplayBD[MapDisplay_HowMany];
//  static ToolButtonData s_InfoBD[Info_HowMany];
//  static ToolButtonData s_HelpBD[Help_HowMany];

    static ToolButtonData* s_bd[Sections_HowMany];

    static int s_howMany[Sections_HowMany];
//  static const char* s_capNames[Sections_HowMany];
    static const InGameText::ID s_capNames[Sections_HowMany];
    static const char* s_regNames[Sections_HowMany];

  public:
    BattleToolBar_Imp(HWND hParent, BattleWindowsInterface* bw);
    ~BattleToolBar_Imp() {}

    void positionWindows() { d_batWind->positionWindows(); }
    void updateAll()       { d_batWind->updateAll(); }
};

/*------------------------------------------------------------------------
 * New Data
 */

/*
 * Buttons
 */

static const int ButtonMode_Normal = WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON;
static const int ButtonMode_OnOff = WS_CHILD | WS_VISIBLE | BS_AUTOCHECKBOX | BS_PUSHLIKE;

ToolButtonData BattleToolBar_Imp::s_FileBD[File_HowMany] = {
   { IDM_BAT_LOADGAME, ButtonMode_Normal, BTBI_Open },
   { IDM_BAT_SAVEGAME, ButtonMode_Normal, BTBI_Save }
};

ToolButtonData BattleToolBar_Imp::s_ZoomBD[Zoom_HowMany] = {
   { IDM_BAT_ZOOMIN,       ButtonMode_Normal, BTBI_ZoomIn },
   { IDM_BAT_ZOOMOVERVIEW, ButtonMode_OnOff,  BTBI_ZoomOverview },
   { IDM_BAT_ZOOMDETAIL,   ButtonMode_OnOff,  BTBI_ZoomDetail },
   { IDM_BAT_ZOOMTWOMILE,  ButtonMode_OnOff,  BTBI_Zoom2Mile },
   { IDM_BAT_ZOOMFOURMILE, ButtonMode_OnOff,  BTBI_Zoom4Mile }
};

ToolButtonData BattleToolBar_Imp::s_MapDisplayBD[MapDisplay_HowMany] = {
   { IDM_BAT_HEXOUTLINE,     ButtonMode_OnOff, BTBI_HexOutLine },
   { IDM_BAT_TINYMAPWINDOW,  ButtonMode_OnOff, BTBI_TinyMap },
   { IDM_BAT_MESSAGEWINDOW,  ButtonMode_OnOff, BTBI_MessageWindow },
   { IDM_BAT_TRACKINGWINDOW, ButtonMode_OnOff, BTBI_TrackingWindow },
};

//ToolButtonData BattleToolBar_Imp::s_InfoBD[Info_HowMany] = {
// { IDM_BAT_OB,         ButtonMode_OnOff, BTBI_OB        },
// { IDM_BAT_FINDLEADER, ButtonMode_OnOff, BTBI_FindLeader }
//};

//ToolButtonData  BattleToolBar_Imp::s_HelpBD[Help_HowMany] = {
// { IDM_BAT_HELP_HISTORY,  ButtonMode_Normal, BTBI_OHR },
// { IDM_BAT_HELP_CONTENTS, ButtonMode_Normal, BTBI_Help }
//};



/*
 * Tool Tips
 */

TipData BattleToolBar_Imp::s_FileTipData[] = {
   { Open, TTS_BTB_Open, IDM_BAT_LOADGAME },
   { Save, TTS_BTB_Save, IDM_BAT_SAVEGAME },
   EndTipData
};

TipData BattleToolBar_Imp::s_ZoomTipData[] = {
   { ZoomIn,       TTS_BTB_ZoomTo,       IDM_BAT_ZOOMIN  },
   { ZoomOverview, TTS_BTB_ZoomOverview, IDM_BAT_ZOOMOVERVIEW },
   { ZoomDetail,   TTS_BTB_ZoomDetail,   IDM_BAT_ZOOMDETAIL },
   { Zoom2Mile,    TTS_BTB_ZoomTwoMile,  IDM_BAT_ZOOMTWOMILE },
   { Zoom4Mile,    TTS_BTB_ZoomFourMile, IDM_BAT_ZOOMFOURMILE },
   EndTipData
};

TipData BattleToolBar_Imp::s_MapDisplayTipData[] = {
   { HexOutLine,     TTS_BTB_HexOutLine,     IDM_BAT_HEXOUTLINE },
   { TinyMap,        TTS_BTB_TinyMap,        IDM_BAT_TINYMAPWINDOW },
   { MessageWindow,  TTS_BTB_MessageWindow,  IDM_BAT_MESSAGEWINDOW },
   { TrackingWindow, TTS_BTB_TrackingWindow, IDM_BAT_TRACKINGWINDOW },
   EndTipData
};

//TipData BattleToolBar_Imp::s_InfoTipData[] = {
// { OBWindow,       TTS_BTB_OBWindow,    IDM_BAT_OB        },
// { FindLeader,     TTS_BTB_FindLeader,  IDM_BAT_FINDLEADER },
// EndTipData
//};

//TipData BattleToolBar_Imp::s_HelpTipData[] = {
// { OHR,         TTS_BTB_OHR,  IDM_BAT_HELP_HISTORY  },
// { HelpButton,  TTS_BTB_Help, IDM_BAT_HELP_CONTENTS },
// EndTipData
//};


ToolButtonData* BattleToolBar_Imp::s_bd[Sections_HowMany] = {
   s_FileBD,
   s_ZoomBD,
   s_MapDisplayBD,
// s_InfoBD,
// s_HelpBD
};

TipData* BattleToolBar_Imp::s_td[Sections_HowMany] = {
   s_FileTipData,
   s_ZoomTipData,
   s_MapDisplayTipData,
// s_InfoTipData,
// s_HelpTipData
};

int BattleToolBar_Imp::s_howMany[Sections_HowMany] = {
   File_HowMany,
   Zoom_HowMany,
   MapDisplay_HowMany,
// Info_HowMany,
// Help_HowMany
};

/*------------------------------------------------------------------------
 * Implementation
 */

#if 0
// caption names for each type
static const char* BattleToolBar_Imp::s_capNames[Sections_HowMany] = {
    "File",
    "Zoom",
    "Map-Display",
//  "Info",
//  "Help"
};
#endif

const InGameText::ID BattleToolBar_Imp::s_capNames[Sections_HowMany] = {
   IDS_File,            //  "File",
   IDS_Zoom,            // "Zoom",
   IDS_MapDisplay       //  "Map-Display",
//  "Info",
//  "Help"
};


// registry names. Note: these can remain local
const char* BattleToolBar_Imp::s_regNames[Sections_HowMany] = {
    "B_FileToolBar",
    "B_ZoomToolBar",
    "B_Map-DisplayToolBar",
//  "B_InfoToolBar",
//  "B_HelpToolBar"
};

BattleToolBar_Imp::BattleToolBar_Imp(HWND parent, BattleWindowsInterface* bw) :
  d_batWind(bw)
{
  static ToolBarInitData td;
  td.d_hParent = parent;
  td.d_nSections = Sections_HowMany;
  td.d_tipData = s_td;
  td.d_buttonData = s_bd;
  td.d_howMany = s_howMany;
  td.d_fillDib = scenario->getScenarioBkDIB(Scenario::BackPatterns::CherryWoodBackground);
  td.d_buttonFillDib = scenario->getScenarioBkDIB(Scenario::BackPatterns::ButtonBackground);
  td.d_buttonImages = ScenarioImageLibrary::get(ScenarioImageLibrary::B_ToolBarImages);
  td.d_capDib = scenario->getScenarioBkDIB(Scenario::BackPatterns::MenuBarBackground);
  int capCY = GetSystemMetrics(SM_CYCAPTION);
  const int fontHeight = capCY - 2;
  ASSERT(fontHeight >= 3);    // check for silly size?
  LogFont lf;
  lf.face(scenario->fontName(Font_Bold));
  lf.height(fontHeight);
  lf.weight(FW_MEDIUM);
  lf.charset(Greenius_System::ANSI);      // otherwise we may get symbols!
  td.d_capFont = lf;
  td.d_borderColors = scenario->getBorderColors();
  td.d_captionNames = s_capNames;
  td.d_registryNames = s_regNames;
  init(&td);
}

/*------------------------------------------------------------
 * Client access
 */

B_ToolBar::B_ToolBar(HWND parent, BattleWindowsInterface* bw) :
  d_toolBar(new BattleToolBar_Imp(parent, bw))
{
  ASSERT(d_toolBar);
}

B_ToolBar::~B_ToolBar()
{
    // destroy();
    delete d_toolBar;
}

void B_ToolBar::position(const PixelPoint& p)
{
  ASSERT(d_toolBar);
  d_toolBar->position(p);
}

void B_ToolBar::show(bool visible)
{
  // ASSERT(d_toolBar);
  // ShowWindow(d_toolBar->getHWND(), SW_SHOW);
  d_toolBar->show(visible);
}

// void B_ToolBar::destroy()
// {
//   if(d_toolBar)
//   {
//     DestroyWindow(d_toolBar->getHWND());
//     d_toolBar = 0;
//   }
// }

void B_ToolBar::setCheck(int menuID, Boolean f)
{
  ASSERT(d_toolBar);
  d_toolBar->setCheck(menuID, f);
}

void B_ToolBar::enable(int menuID, Boolean f)
{
  ASSERT(d_toolBar);
  d_toolBar->enable(menuID, f);
}

int B_ToolBar::height() const
{
  return d_toolBar->height();
}

Boolean B_ToolBar::isVisible() const
{
  return d_toolBar->isVisible();
}

Boolean B_ToolBar::isEnabled() const
{
  return d_toolBar->isEnabled();
}


void B_ToolBar::enable(bool visible)
{
   d_toolBar->enable(visible);
}

HWND B_ToolBar::getHWND() const
{
  return (d_toolBar) ? d_toolBar->getHWND() : 0;
}

/*
 *----------------------------------------------------------------------
 * $Log$
 *----------------------------------------------------------------------
 */
