/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
#include "stdinc.hpp"
#include "btw_info.hpp"
#include "scenario.hpp"
#include "fonts.hpp"
#include "tabwin.hpp"
#include "scn_img.hpp"
#include "scrnbase.hpp"
#include "bunitinf.hpp"
#include "resstr.hpp"

/*----------------------------------------------------------------------
 * Unit info window
 */

class BUnitInfoWindow : public TabbedInfoWindow {

   BUI_UnitInterface* d_owner;

    enum Tabs {
       SummaryTab,
       LeaderTab,
       ActivityTab,
       OrdersTab,
       OBTab,

       Tab_HowMany
    };

//  static const char* s_tabText[Tab_HowMany];
    static const int s_ids[Tab_HowMany];
    static const ResourceStrings s_strings;

    CRefBattleCP d_cpi;
    CPBattleData d_batData;
    BUnitInfo d_drawUtil;
  public:
    BUnitInfoWindow(BUI_UnitInterface* owner, HWND hParent, CPBattleData batData,
       const DrawDIB* infoFillDib, const DrawDIB* bkDib,
       const DrawDIB* bFillDib, const ImageLibrary* il, HFONT font,
       CustomBorderInfo bc) :
       TabbedInfoWindow(hParent, Tab_HowMany, s_strings, infoFillDib, bkDib, bFillDib, il, font, bc),
       d_owner(owner),
       d_cpi(NoBattleCP),
       d_batData(batData),
       d_drawUtil(batData) {}

    ~BUnitInfoWindow() { selfDestruct(); }

    void run(const CRefBattleCP& cpi, const PixelPoint& p);
    void onSelChange();
};


#if 0
// TODO: get this from string resource
const char* BUnitInfoWindow::s_tabText[Tab_HowMany] = {
  "Summary",
  "Leader",
  "Activity",
  "Orders",
  "OB"
};
#endif

const int BUnitInfoWindow::s_ids[Tab_HowMany] = {
   IDS_UI_SUMMARY,
   IDS_UI_LEADER,
   IDS_UI_ACTIVITY,
   IDS_UI_ORDERS,
   IDS_UI_OB
};

const ResourceStrings BUnitInfoWindow::s_strings(s_ids, Tab_HowMany);


void BUnitInfoWindow::onSelChange()
{
  Tabs whichTab = static_cast<Tabs>(currentTab());
  ASSERT(whichTab < Tab_HowMany);

  fillStatic();

  if(d_cpi != NoBattleCP)
  {
    // draw info here
    switch(whichTab)
    {
      case SummaryTab:
        d_drawUtil.drawSummaryTab(dib(), d_cpi, dRect());
        break;

      case LeaderTab:
        d_drawUtil.drawLeaderTab(dib(), d_cpi, dRect());
        break;

      case OBTab:
        d_drawUtil.drawOBTab(dib(), d_cpi, dRect());
        break;
    }
  }
}

void BUnitInfoWindow::run(const CRefBattleCP& cpi, const PixelPoint& p)
{
  d_cpi = cpi;
  onSelChange();
  TabbedInfoWindow::run(p);
}

/*---------------------------------------------------------------------
 * Outside access
 */

BUnitInfo_Int::BUnitInfo_Int(BUI_UnitInterface* owner, HWND hParent, CPBattleData batData) :
  d_infoWind(0) // new BUnitInfoWindow(owner, hParent, campData))
{
  LogFont lf;
  lf.height((6 * ScreenBase::dbY()) / ScreenBase::baseY());
  lf.weight(FW_MEDIUM);
  lf.face(scenario->fontName(Font_Decorative));

  Font font;
  font.set(lf);

  d_infoWind = new BUnitInfoWindow(owner, hParent, batData,
    scenario->getScenarioBkDIB(Scenario::BackPatterns::PaperBackground),
    scenario->getScenarioBkDIB(Scenario::BackPatterns::CherryWoodBackground),
    scenario->getScenarioBkDIB(Scenario::BackPatterns::WoodBackground),
    ScenarioImageLibrary::get(ScenarioImageLibrary::CTabImages), font,
    scenario->getBorderColors());

  ASSERT(d_infoWind);
}

BUnitInfo_Int::~BUnitInfo_Int()
{
  destroy();
}

void BUnitInfo_Int::run(const CRefBattleCP& cpi, const PixelPoint& p)
{
  if(d_infoWind)
  {
    d_infoWind->run(cpi, p);
  }
}

void BUnitInfo_Int::destroy()
{
  if(d_infoWind)
  {
    // d_infoWind->destroy();
     delete d_infoWind;
    d_infoWind = 0;
  }
}

/*
 *----------------------------------------------------------------------
 * $Log$
 *----------------------------------------------------------------------
 */
