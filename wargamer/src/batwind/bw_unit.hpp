/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef BW_UNIT_HPP
#define BW_UNIT_HPP

#ifndef __cplusplus
#error bw_unit.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *	Battle Window user interface for units
 * This is used by BattleWindows when something happens over the battlemap
 *
 *----------------------------------------------------------------------
 */

#include "batdata.hpp"	// for CPBattleData
#include "btwin_i.hpp"	// for PBattleWindows
//#include "bw_int.hpp"   // for BattleUI_Interface

//namespace BattleWindows_Internal
//{




class BattleMapSelect;

class BUI_UnitImp;	// Forward reference to Hidden Implementation

class BattleUnitUI {


		BUI_UnitImp* d_imp;			// Hidden Implementation

		// Disable Copying
		BattleUnitUI(const BattleUnitUI&);
		BattleUnitUI& operator = (const BattleUnitUI&);
	public:

		
		BattleUnitUI(RPBattleWindows batWind, RCPBattleData batData);
		~BattleUnitUI();


		/*
		 * BattleUI_Interface Implementation
		 */

		void onLButtonDown(const BattleMapSelect& info);
			// Button is pressed
		void onLButtonUp(const BattleMapSelect& info);
			// Button is released while not dragging
		void onRButtonDown(const BattleMapSelect& info);
			// Button is pressed
		void onRButtonUp(const BattleMapSelect& info);
			// Button is released while not dragging
		void onStartDrag(const BattleMapSelect& info);
			// Mouse is moved while button is held
		void onEndDrag(const BattleMapSelect& info);
			// button is released while dragging

		void onMove(const BattleMapSelect& info);
			// Mouse has moved
		void overNothing();
			// Mouse is not over anything (e.g. off map)
        bool setCursor(void);
		void destroy();

		// called when time changes up in batwind
		void timeChanged(void);

		void mapZoomChanged(void);

		void selectUnit(BattleCP * cp);



};

//};	// namespace BattleWindows_Internal

#endif /* BW_UNIT_HPP */

