/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
#include "stdinc.hpp"
#include "bstatwin.hpp"
#include "gsecwind.hpp"
#include "batres.h"                             // res: Resources
#include "batres_s.h"                   // res: Resource Strings
#include "btimctrl.hpp"
#include "wind.hpp"
#include "dib.hpp"
#include "dib_util.hpp"
#include "btwin_i.hpp"
#include "app.hpp"
#include "palette.hpp"
#include "wmisc.hpp"
#include "scrnbase.hpp"
// #include "generic.hpp"
#include "palwind.hpp"
#include "fonts.hpp"
#include "scenario.hpp"
#include "scn_res.h"
#include "imglib.hpp"
#include "cbutton.hpp"
#include "scn_img.hpp"
#include "resdef.h"
#include "tooltip.hpp"
#include "simpstr.hpp"
#include "bargraph.hpp"
#include "colours.hpp"
#include "bclock.hpp"
#include "bobutil.hpp"
#include "resstr.hpp"

/*-----------------------------------------------
 * Local utils
 */

class BSUtil {
public:
  static HWND createButton(HWND hParent, const char* text, int id, Boolean autoCheck = False, int imgID = -1);
};

HWND BSUtil::createButton(HWND hParent, const char* text, int id, Boolean autoCheck, int imgID)
{
  DWORD style = (autoCheck) ? WS_CHILD | BS_AUTOCHECKBOX | BS_PUSHLIKE :
       WS_CHILD | BS_PUSHBUTTON;

  HWND hButton = CreateWindow(CUSTOMBUTTONCLASS, text,
            style,
            0, 0, 0, 0,
            hParent, (HMENU)id, APP::instance(), NULL);

  ASSERT(hButton != 0);

  CustomButton cb(hButton);
  // cb.setFillDib(scenario->getScenarioBkDIB(Scenario::BackPatterns::WoodBackground));
  cb.setFillDib(scenario->getScenarioBkDIB(Scenario::BackPatterns::ButtonBackground));
  cb.setBorderColours(scenario->getBorderColors());

  if(imgID != -1)
    cb.setButtonImage(ScenarioImageLibrary::get(ScenarioImageLibrary::CToolBarImages), imgID);

  return hButton;
}



/*---------------------------------------------------------------------
 * Victory section.
 * Displays a victory barchart
 */

class BV_Section : public GSecWind {
    RCPBattleData d_batData;

  public:
    BV_Section(HWND hParent, RCPBattleData batData, const DrawDIB* fillDib);
    ~BV_Section() { selfDestruct(); }

    // virtual functions from GSecWind
    void drawDib();
    void enableControls() {}

  private:
    LRESULT procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
    BOOL onCreate(HWND hWnd, CREATESTRUCT* lpCreateStruct);
    void onDestroy(HWND hWnd) {}
};

BV_Section::BV_Section(HWND hParent, RCPBattleData bd, const DrawDIB* fillDib) :
  GSecWind(fillDib),
  d_batData(bd)
{
  HWND h = createWindow(
      0,
      // GenericNoBackClass::className(),
        NULL,
      WS_CHILD | WS_CLIPSIBLINGS,
      0, 0, 0, 0,
      hParent, NULL   // , APP::instance()
  );

  ASSERT(h != NULL);
}

LRESULT BV_Section::procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
//  LRESULT result;

  switch(msg)
  {
    HANDLE_MSG(hWnd, WM_CREATE, onCreate);
    HANDLE_MSG(hWnd, WM_DESTROY, onDestroy);
    HANDLE_MSG(hWnd, WM_PAINT, onPaint);

    default:
      return defProc(hWnd, msg, wParam, lParam);
  }
}

BOOL BV_Section::onCreate(HWND hwnd, CREATESTRUCT* cs)
{
  return TRUE;
}

void BV_Section::drawDib()
{
  /*
   * Get current size of client area
   */

  const int dbX = ScreenBase::dbX();
  const int dbY = ScreenBase::dbY();
  const int baseX = ScreenBase::baseX();
  const int baseY = ScreenBase::baseY();
  const int startX = ((2 * dbX) / baseX) + (2 * CustomBorderWindow::ThinBorder);
  const int sideNameCX = (75 * dbX) / baseX;
  const int xOffset = ((2 * dbX) / baseX);
  const int vBargraphCY = (7 * dbY) / baseY;

  /*
   * Allocate dib
   */

  dib()->setBkMode(TRANSPARENT);

  /*
   * Get fonts
   */

  // large font
  LogFont lf;
  lf.height((9 * dbY) / baseY);
  lf.weight(FW_MEDIUM);
  lf.face(scenario->fontName(Font_Decorative));

  Font lFont;
  lFont.set(lf);

  static TEXTMETRIC tm;
  static Boolean gotMetrics = False;
  if(!gotMetrics)
  {
    GetTextMetrics(dib()->getDC(), &tm);
    gotMetrics = True;
  }

  /*
   * Draw victory level bar
   */

  int y = 0;

  // fill in dib
  dib()->rect(0, 0, cx(), cy(), fillDib());

  // TODO: Victory points

  /*
  Calculate victory levels
  Victory level = (cp->strength() + cp->morale()) * victory points
  */

  unsigned int side1Victory;
  unsigned int side2Victory;

  const BattleData * bd = d_batData;
  BobUtility::approxVictoryLevels(const_cast<BattleData *>(bd), &side1Victory, &side2Victory);
  side1Victory += d_batData->ob()->getTop(0)->morale();
  side2Victory += d_batData->ob()->getTop(1)->morale();

  ULONG totalPoints = side1Victory + side2Victory;

  int x = startX;

  // TODO: get from resource
  const char* text = InGameText::get(IDS_VICTORYLEVEL);  // "Victory-Level";
//  const char* text = victoryText;

  dib()->setFont(lFont);
  dib()->setTextColor(scenario->getColour("MenuText"));


  int workingY = ((cy() - tm.tmHeight) / 2);
  // wTextOut(dib()->getDC(), x, workingY, text);
  wTextOut(dib()->getDC(), x, workingY, x + sideNameCX, text);

//  UINT align = dib()->setTextAlign(oldAlign);
//  ASSERT(align != GDI_ERROR);

  /*
   * The 2 side bars will be combined into one long bar
   */

  const ColourIndex frame1CI = dib()->getColour(PALETTERGB(0, 0, 0));
  const ColourIndex frame2CI = dib()->getColour(PALETTERGB(255, 255, 255));

  const ColourIndex side1CI = dib()->getColour(scenario->getSideColour(0));
  const ColourIndex side2CI = dib()->getColour(scenario->getSideColour(1));

  const int frameCX = 1;

  x += sideNameCX;

  // draw side 1 flag
  const ImageLibrary* il = scenario->getNationFlag(scenario->getDefaultNation(0), True);
  workingY = ((cy() - FI_Army_CY) / 2);
  il->blit(dib(), FI_Army, x, workingY, True);

  x += (FI_Army_CX + xOffset);
  workingY = ((cy() - vBargraphCY) / 2);
  const int barCX = cx() - (x + (4 * xOffset) + (2 * FI_Army_CX));

  // draw frame
  dib()->frame(x, workingY, barCX, vBargraphCY, frame1CI, frame2CI);

  // draw first sides bar
  const int side1W = (totalPoints > 0) ?
      (barCX * ((side1Victory * 100) / totalPoints)) / 100 : barCX / 2;


  dib()->rect(x + frameCX, workingY + frameCX,
      side1W, vBargraphCY - (2 * frameCX), side1CI);

  // draw second sides bar
  dib()->rect(x + frameCX + side1W, workingY + frameCX,
      (barCX - side1W) - (2 * frameCX), vBargraphCY - (2 * frameCX), side2CI);

  // draw side2 flag
  x += (barCX + xOffset);
  il = scenario->getNationFlag(scenario->getDefaultNation(1), True);
  workingY = y + ((cy() - FI_Army_CY) / 2);
  il->blit(dib(), FI_Army, x, workingY, True);

  drawBorder();
}

/*----------------------------------------------------------------------
 *  Nation Status window. Includes:
 *    1. morale-level for each major nationality,
 *    2. each sides victory point level
 */

class BV_Wind :
    public WindowBaseND
{
    HWND d_hParent;
    BattleWindowsInterface* d_batWind;
    RCPBattleData d_batData;

    int d_cx;
    int d_cy;

    Boolean d_shouldRedraw;

    enum Sections {
      VictoryLevel,

      Sections_HowMany
    };

    GSecWind* d_sections[Sections_HowMany];

     static const char s_regKey[];
     static WSAV_FLAGS s_saveFlags;

  public:
    BV_Wind(HWND hParent, BattleWindowsInterface* bw, RCPBattleData batData);
    ~BV_Wind();

    HWND getHWND() const { return WindowBaseND::getHWND(); }

    void position(const PixelPoint& p);
    void update();

    int height() const { return d_cy; }

  private:

    LRESULT procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
    BOOL onCreate(HWND hWnd, CREATESTRUCT* lpCreateStruct);
    void onDestroy(HWND hWnd);
    void onPaint(HWND hwnd);
    void redraw();
};

const char BV_Wind::s_regKey[] = "BattleVictoryWindow";
WSAV_FLAGS BV_Wind::s_saveFlags = WSAV_ENABLED;


BV_Wind::BV_Wind(HWND hParent, BattleWindowsInterface* bw, RCPBattleData batData) :
  d_hParent(hParent),
  d_batWind(bw),
  d_batData(batData),
  d_cx(0),
  d_cy((2 * CustomBorderWindow::ThinBorder) + (11 * ScreenBase::dbY()) / ScreenBase::baseY()),
  d_shouldRedraw(True)
{
  for(int i = 0; i < Sections_HowMany; i++)
    d_sections[i] = 0;

  HWND h = createWindow(
      0,
      // GenericNoBackClass::className(),
        NULL,
      WS_CHILD | WS_CLIPSIBLINGS,
      0, 0, 0, d_cy,
      d_hParent, NULL     // , APP::instance()
  );

  getState(s_regKey, s_saveFlags, true);

  ASSERT(h != NULL);
}

BV_Wind::~BV_Wind()
{
  for(int i = 0; i < Sections_HowMany; i++)
  {
    if(d_sections[i])
    {
      delete d_sections[i];
      d_sections[i] = 0;
    }
  }

    saveState(s_regKey, s_saveFlags);

    selfDestruct();
}

LRESULT BV_Wind::procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
//   LRESULT result;

  switch(msg)
  {
    HANDLE_MSG(hWnd, WM_CREATE, onCreate);
    HANDLE_MSG(hWnd, WM_DESTROY, onDestroy);
    HANDLE_MSG(hWnd, WM_PAINT, onPaint);

    default:
      return defProc(hWnd, msg, wParam, lParam);
  }
}

BOOL BV_Wind::onCreate(HWND hwnd, CREATESTRUCT* cs)
{
  /*
   * Create sections
   */

  d_sections[VictoryLevel] = new BV_Section(hwnd, d_batData, scenario->getSideBkDIB(SIDE_Neutral));
  ASSERT(d_sections[VictoryLevel]);

  return TRUE;
}

void BV_Wind::onDestroy(HWND hwnd)
{
#if 0  // Done in destructor
  for(int i = 0; i < Sections_HowMany; i++)
  {
    if(d_sections[i])
    {
      DestroyWindow(d_sections[i]->getHWND());
      d_sections[i] = 0;
    }
  }
#endif
}

void BV_Wind::onPaint(HWND hwnd)
{
  PAINTSTRUCT ps;
  HDC hdc = BeginPaint(hwnd, &ps);

  HPALETTE oldPal = SelectPalette(hdc, Palette::get(), FALSE);

  RECT r;
  GetClientRect(hwnd, &r);

  static CustomBorderWindow bw(scenario->getBorderColors());
  bw.drawThinBorder(hdc, r);

  // clean up
  SelectPalette(hdc, oldPal, FALSE);
  EndPaint(hwnd, &ps);
}

void BV_Wind::redraw()
{
  for(int i = 0; i < Sections_HowMany; i++)
  {
    if(d_sections[i])
      d_sections[i]->redraw();
  }
}

/*
 * Thread safe function
 */

void BV_Wind::update()
{
  for(int i = 0; i < Sections_HowMany; i++)
  {
    if(d_sections[i])
      d_sections[i]->update();
  }
}


void BV_Wind::position(const PixelPoint& p)
{
  d_shouldRedraw = True;

  RECT r;
  GetClientRect(d_hParent, &r);

  const int cx = r.right - r.left;
  const int secCX = cx - (2 * CustomBorderWindow::ThinBorder);
  const int secCY = (d_cy - (2 * CustomBorderWindow::ThinBorder)) / Sections_HowMany;

  SetWindowPos(getHWND(), HWND_TOP, p.getX(), p.getY(), cx, d_cy, 0);

  // set section positions
  int x = CustomBorderWindow::ThinBorder;
  int y = CustomBorderWindow::ThinBorder;
  for(int i = 0; i < Sections_HowMany; i++)
  {
    ASSERT(d_sections[i]);
    SetWindowPos(d_sections[i]->getHWND(), HWND_TOP, x, y, secCX, secCY, SWP_SHOWWINDOW);
    y += secCY;
  }
}

/*---------------------------------------------------------------
 * Client Access for Nation Status
 */

BattleVictory_Int::BattleVictory_Int(HWND hParent, BattleWindowsInterface* bw, RCPBattleData batData) :
  d_bvWind(new BV_Wind(hParent, bw, batData))
{
  ASSERT(d_bvWind);
}

BattleVictory_Int::~BattleVictory_Int()
{
    delete d_bvWind;
    // destroy();
}

void BattleVictory_Int::position(const PixelPoint& p)
{
  ASSERT(d_bvWind);
  d_bvWind->position(p);
}

void BattleVictory_Int::update()
{
  ASSERT(d_bvWind);
  d_bvWind->update();
}

void BattleVictory_Int::show(bool visible)
{
  ASSERT(d_bvWind);
  // ShowWindow(d_bvWind->getHWND(), SW_SHOW);
  d_bvWind->show(visible);
}

#if 0
void BattleVictory_Int::destroy()
{
    delete d_bvWind;
    d_bvWind = 0;

//  if(d_bvWind)
//      d_bvWind->destroy();
//
//    {
//     DestroyWindow(d_bvWind->getHWND());
//     d_bvWind = 0;
//    }
}
#endif

int BattleVictory_Int::height() const
{
  ASSERT(d_bvWind);
  return d_bvWind->height();
}

void BattleVictory_Int::enable(bool visible)
{
   d_bvWind->enable(visible);
}

bool BattleVictory_Int::isVisible() const
{
    return d_bvWind->isVisible();
}

bool BattleVictory_Int::isEnabled() const
{
    return d_bvWind->isEnabled();
}


HWND BattleVictory_Int::getHWND() const
{
    return d_bvWind->getHWND();
}

/*---------------------------------------------------------------------
 * Status Window
 */
/*---------------------------------------------------------------------
 * Victory section.
 * Displays a victory barchart
 */

class BMsg_Section : public GSecWind {
    BattleWindowsInterface* d_batWind;
    enum { ID_Button = 100 };
  public:
    BMsg_Section(HWND hParent, BattleWindowsInterface* bw, const DrawDIB* fillDib);
    ~BMsg_Section() { selfDestruct(); }

    // virtual functions from GSecWind
    void drawDib();
    void enableControls() {}

  private:
    LRESULT procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
    BOOL onCreate(HWND hWnd, CREATESTRUCT* lpCreateStruct);
    void onCommand(HWND hWnd, int id, HWND hwndCtl, UINT codeNotify);
    void onDestroy(HWND hWnd) {}
};

BMsg_Section::BMsg_Section(HWND hParent, BattleWindowsInterface* bw, const DrawDIB* fillDib) :
  GSecWind(fillDib),
  d_batWind(bw)
{
  HWND h = createWindow(
      0,
      // GenericNoBackClass::className(),
        NULL,
      WS_CHILD | WS_CLIPSIBLINGS,
      0, 0, 0, 0,
      hParent, NULL       //, APP::instance()
  );

  ASSERT(h != NULL);
}

LRESULT BMsg_Section::procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
//  LRESULT result;

  switch(msg)
  {
    HANDLE_MSG(hWnd, WM_CREATE, onCreate);
    HANDLE_MSG(hWnd, WM_DESTROY, onDestroy);
    HANDLE_MSG(hWnd, WM_COMMAND, onCommand);
    HANDLE_MSG(hWnd, WM_PAINT, onPaint);

    default:
      return defProc(hWnd, msg, wParam, lParam);
  }
}

BOOL BMsg_Section::onCreate(HWND hwnd, CREATESTRUCT* cs)
{
  const int imgID = TBI_MessageWindow;

  BSUtil::createButton(hwnd, NULL, ID_Button, True, imgID);
  return TRUE;
}

void BMsg_Section::onCommand(HWND hWnd, int id, HWND hwndCtl, UINT codeNotify)
{
  if(id == ID_Button)
  {
    SendMessage(APP::getMainHWND(), WM_COMMAND,
        MAKEWPARAM(IDM_BAT_MESSAGEWINDOW, codeNotify), reinterpret_cast<LPARAM>(hwndCtl));
  }
}

void BMsg_Section::drawDib()
{
  const int dbX = ScreenBase::dbX();
  const int dbY = ScreenBase::dbY();
  const int baseX = ScreenBase::baseX();
  const int baseY = ScreenBase::baseY();
  const int buttonX = ((2 * dbX) / baseX) + (2 * CustomBorderWindow::ThinBorder);
  const int buttonCX = cy() - ((2 * CustomBorderWindow::ThinBorder) + 2);
  const int startX = ((3 * dbX) / baseX) + buttonX + buttonCX;

  /*
   * if size has changed, position button
   */

  if(sizeChanged())
  {

    HWND h = GetDlgItem(getHWND(), ID_Button);
    ASSERT(h);

    int y = (cy() - buttonCX) / 2;
    SetWindowPos(h, HWND_TOP, buttonX, y, buttonCX, buttonCX, SWP_SHOWWINDOW);
    sizeChanged(False);
  }

  dib()->setBkMode(TRANSPARENT);

  /*
   * Get fonts
   */

  // large font
  LogFont lf;
  lf.height((9 * dbY) / baseY);
  lf.weight(FW_MEDIUM);
  lf.face(scenario->fontName(Font_Decorative));

  Font lFont;
  lFont.set(lf);

  static TEXTMETRIC tm;
  static Boolean gotMetrics = False;
  if(!gotMetrics)
  {
    GetTextMetrics(dib()->getDC(), &tm);
    gotMetrics = True;
  }

  /*
   * Draw Number of messages
   */

  int y = 0;

  // fill in dib
  dib()->rect(0, 0, cx(), cy(), fillDib());

  // TODO: sort out message counts int batwind
#if 0 // !defined(EDITOR)
  int numMessages = d_campWind->getNumCampaignMessages();
//  int  messagesRead = d_campWind->getCampaignMessagesRead();
#else
  int numMessages = 0;
//  int  messagesRead = 0;
#endif

  dib()->setFont(lFont);
  dib()->setTextColor(scenario->getColour("MenuText"));

  int workingY = ((cy() - tm.tmHeight) / 2);

  const char* strMessage = InGameText::get(IDS_Messages);  // "Messages";

  // TODO: get from resource
  wTextPrintf(dib()->getDC(),
      startX, workingY,
      "%s: %d",      // Unread: %d",
      strMessage,
      numMessages);    //, messagesRead);

  drawBorder();
}

/*----------------------------------------------------------------
 *  Hint-Line Section.
 */

class BHint_Section : public GSecWind, public HintLineBase {
    BattleWindowsInterface* d_batWind;
    String d_text;

    enum { ID_Button = 100 };
  public:
    BHint_Section(HWND hParent, BattleWindowsInterface* bw, const DrawDIB* fillDib);
    ~BHint_Section()
    {
      g_toolTip.setHintDisplay(0);
       selfDestruct();
    }

    // virtual functions from GSecWind
    void drawDib();
    void enableControls() {}

    // virtual functions from HintLineBase
    void showHint(const String& text);
    void clearHint();

  private:
    LRESULT procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
    BOOL onCreate(HWND hWnd, CREATESTRUCT* lpCreateStruct);
    void onDestroy(HWND hWnd) {}
};

BHint_Section::BHint_Section(HWND hParent, BattleWindowsInterface* bw, const DrawDIB* fillDib) :
  GSecWind(fillDib),
  d_batWind(bw)
{
  HWND h = createWindow(
      0,
      // GenericNoBackClass::className(),
        NULL,
      WS_CHILD | WS_CLIPSIBLINGS,
      0, 0, 0, 0,
      hParent, NULL   // , APP::instance()
  );

  ASSERT(h != NULL);
  g_toolTip.setHintDisplay(this);
}

LRESULT BHint_Section::procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
//  LRESULT result;

  switch(msg)
  {
    HANDLE_MSG(hWnd, WM_CREATE, onCreate);
    HANDLE_MSG(hWnd, WM_DESTROY, onDestroy);
    HANDLE_MSG(hWnd, WM_PAINT, onPaint);

    default:
      return defProc(hWnd, msg, wParam, lParam);
  }
}

BOOL BHint_Section::onCreate(HWND hwnd, CREATESTRUCT* cs)
{
  return TRUE;
}

void BHint_Section::drawDib()
{
  const int dbX = ScreenBase::dbX();
  const int dbY = ScreenBase::dbY();
  const int baseX = ScreenBase::baseX();
  const int baseY = ScreenBase::baseY();
  const int startX = ((2 * dbX) / baseX) + (2 * CustomBorderWindow::ThinBorder);

  dib()->setBkMode(TRANSPARENT);

  // fill in dib
  dib()->rect(0, 0, cx(), cy(), fillDib());

  if(!d_text.size()) // toStr())
  {
    /*
     * Get fonts
     */

    // large font
    LogFont lf;
    lf.height((9 * dbY) / baseY);
    lf.weight(FW_MEDIUM);
    lf.face(scenario->fontName(Font_Decorative));

    Font lFont;
    lFont.set(lf);

    static TEXTMETRIC tm;
    static Boolean gotMetrics = False;
    if(!gotMetrics)
    {
      GetTextMetrics(dib()->getDC(), &tm);
      gotMetrics = True;
    }

    /*
     * Draw Hint
     */

    int workingY = (cy() - tm.tmHeight) / 2;

    dib()->setTextColor(scenario->getColour("MenuText"));


    wTextOut(dib()->getDC(), startX, workingY,
        cx() - (startX + (2 * CustomBorderWindow::ThinBorder)), d_text.c_str());
  }

  drawBorder();
}

void BHint_Section::showHint(const String& text)
{
  d_text = text;
  update();
}

void BHint_Section::clearHint()
{
  d_text.erase();  // d_text.clear();
  update();
}

/*----------------------------------------------------------------
 *  Game Status window. includes:
 *    1. Nessage Section
 *    2. Hint-Line Section
 *    3. Clock Window
 */

class BGS_Wind : public WindowBaseND
{
    HWND d_hParent;
    BattleWindowsInterface* d_batWind;
    RCPBattleData d_batData;
    BatTimeControl& d_control;

    BattleClock_Int* d_cClockWind;

    int d_cx;
    int d_cy;

    enum Sections {
      MsgSection,
      HintSection,

      Sections_HowMany
    };

    GSecWind* d_sections[Sections_HowMany];
     static const char s_regKey[];
     static WSAV_FLAGS s_saveFlags;

  public:
    BGS_Wind(HWND hParent, BattleWindowsInterface* bw, RCPBattleData batData,
       BatTimeControl& tc);
    ~BGS_Wind();

    void position(const PixelPoint& p);
    void updateMessageWindow();
    void updateClock();

    int height() const { return d_cy; }
    HWND getHWND() const { return WindowBaseND::getHWND(); }

  private:

    LRESULT procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
    BOOL onCreate(HWND hWnd, CREATESTRUCT* lpCreateStruct);
    void onDestroy(HWND hWnd);
    void onPaint(HWND hwnd);
};

const char BGS_Wind::s_regKey[] = "BattleStatusWindow";
WSAV_FLAGS BGS_Wind::s_saveFlags = WSAV_ENABLED;

BGS_Wind::BGS_Wind(HWND hParent, BattleWindowsInterface* bw, RCPBattleData batData,
       BatTimeControl& tc) :
  d_hParent(hParent),
  d_batWind(bw),
  d_batData(batData),
  d_control(tc),
//  d_date(date),
  d_cClockWind(0),
  d_cx(0),
  d_cy((2 * CustomBorderWindow::ThinBorder) + (22 * ScreenBase::dbY()) / ScreenBase::baseY())
{
  for(int i = 0; i < Sections_HowMany; i++)
    d_sections[i] = 0;

  HWND h = createWindow(
      0,
      // GenericNoBackClass::className(),
        NULL,
      WS_CHILD | WS_CLIPSIBLINGS,
      0, 0, 0, d_cy,
      d_hParent, NULL // , APP::instance()
  );

  getState(s_regKey, s_saveFlags, true);

  ASSERT(h != NULL);
}

BGS_Wind::~BGS_Wind()
{
    saveState(s_regKey, s_saveFlags);

    for(int i = 0; i < Sections_HowMany; i++)
    {
      if(d_sections[i])
      {
         delete d_sections[i];
         d_sections[i] = 0;
      }

    }

    delete d_cClockWind;
     d_cClockWind = 0;

    selfDestruct();

}

LRESULT BGS_Wind::procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
//  LRESULT result;

  switch(msg)
  {
    HANDLE_MSG(hWnd, WM_CREATE, onCreate);
    HANDLE_MSG(hWnd, WM_DESTROY, onDestroy);
    HANDLE_MSG(hWnd, WM_PAINT, onPaint);

    default:
      return defProc(hWnd, msg, wParam, lParam);
  }
}

BOOL BGS_Wind::onCreate(HWND hwnd, CREATESTRUCT* cs)
{
  /*
   * Create sections
   */

  d_sections[MsgSection] = new BMsg_Section(hwnd, d_batWind, scenario->getSideBkDIB(SIDE_Neutral));
  ASSERT(d_sections[MsgSection]);

  d_sections[HintSection] = new BHint_Section(hwnd, d_batWind, scenario->getSideBkDIB(SIDE_Neutral));
  ASSERT(d_sections[HintSection]);

   /*
    * Status and clock windows
    */
  d_cClockWind = new BattleClock_Int(hwnd, d_batData, d_control);
  ASSERT(d_cClockWind);

  return TRUE;
}

void BGS_Wind::onDestroy(HWND hwnd)
{
#if 0  // Done in destructor
  for(int i = 0; i < Sections_HowMany; i++)
  {
    if(d_sections[i])
    {
      DestroyWindow(d_sections[i]->getHWND());
      d_sections[i] = 0;
    }

    d_cClockWind->destroy();
  }
#endif
}

void BGS_Wind::onPaint(HWND hwnd)
{
  PAINTSTRUCT ps;
  HDC hdc = BeginPaint(hwnd, &ps);

  HPALETTE oldPal = SelectPalette(hdc, Palette::get(), FALSE);

  RECT r;
  GetClientRect(hwnd, &r);

  static CustomBorderWindow bw(scenario->getBorderColors());
  bw.drawThinBorder(hdc, r);

  // clean up
  SelectPalette(hdc, oldPal, FALSE);
  EndPaint(hwnd, &ps);
}

void BGS_Wind::updateMessageWindow()
{
  if(d_sections[MsgSection])
    d_sections[MsgSection]->update();
}

void BGS_Wind::updateClock()
{
  if(d_cClockWind)
    d_cClockWind->update();
}

void BGS_Wind::position(const PixelPoint& p)
{
  // get parent rect
  RECT r;
  GetClientRect(d_hParent, &r);

#if 0
  // set clock width
  RECT rClock;
  GetWindowRect(d_cClockWind->getHWND(), &rClock);

  const int clockCX = rClock.right - rClock.left;

  SetWindowPos(d_cClockWind->getHWND(), HWND_TOP,
     r.right - (CustomBorderWindow::ThinBorder + clockCX), CustomBorderWindow::ThinBorder,
     clockCX, d_cy - (2 * CustomBorderWindow::ThinBorder), SWP_SHOWWINDOW);

  d_cClockWind->run();
#else

    int clockCX = d_cClockWind->getWidth();
    d_cClockWind->setPosition(
     r.right - (CustomBorderWindow::ThinBorder + clockCX),
      CustomBorderWindow::ThinBorder,
     clockCX,
      d_cy - (2 * CustomBorderWindow::ThinBorder) );

#endif

  const int cx = r.right - r.left;
  const int secCX = cx - ((2 * CustomBorderWindow::ThinBorder) + clockCX);
  const int secCY = (d_cy - (2 * CustomBorderWindow::ThinBorder)) / Sections_HowMany;

  SetWindowPos(getHWND(), HWND_TOP, p.getX(), p.getY(), cx, d_cy, 0);

  // set section positions
  int x = CustomBorderWindow::ThinBorder;
  int y = CustomBorderWindow::ThinBorder;
  for(int i = 0; i < Sections_HowMany; i++)
  {
    ASSERT(d_sections[i]);

    SetWindowPos(d_sections[i]->getHWND(), HWND_TOP, x, y, secCX, secCY, SWP_SHOWWINDOW);

    y += secCY;
  }
}

/*----------------------------------------------------------------
 * Client Access for status window
 */

BGameStatus_Int::BGameStatus_Int(HWND hParent, BattleWindowsInterface* bw, RCPBattleData batData,
       BatTimeControl& tc) :
  d_gsWind(new BGS_Wind(hParent, bw, batData, tc))
{
  ASSERT(d_gsWind);
}

BGameStatus_Int::~BGameStatus_Int()
{
    delete d_gsWind;    // destroy();
}

void BGameStatus_Int::position(const PixelPoint& p)
{
  ASSERT(d_gsWind);
  d_gsWind->position(p);
}

void BGameStatus_Int::show(bool visible)
{
  ASSERT(d_gsWind);
  d_gsWind->show(visible);
}


void BGameStatus_Int::enable(bool enabled)
{
  ASSERT(d_gsWind);
  d_gsWind->enable(enabled);
}


// void BGameStatus_Int::show()
// {
//   ASSERT(d_gsWind);
//   ShowWindow(d_gsWind->getHWND(), SW_SHOW);
// }
//
// void BGameStatus_Int::hide()
// {
//   ASSERT(d_gsWind);
//   ShowWindow(d_gsWind->getHWND(), SW_HIDE);
// }
//
// void BGameStatus_Int::destroy()
// {
//   if(d_gsWind)
//   {
//     DestroyWindow(d_gsWind->getHWND());
//     d_gsWind = 0;
//   }
// }

void BGameStatus_Int::updateMessageWindow()
{
  ASSERT(d_gsWind);
  d_gsWind->updateMessageWindow();
}

void BGameStatus_Int::updateClock()
{
  ASSERT(d_gsWind);
  d_gsWind->updateClock();
}

int BGameStatus_Int::height() const
{
  ASSERT(d_gsWind);
  return d_gsWind->height();
}


// void BGameStatus_Int::suspend(bool visible)
// {
//    d_gsWind->suspend(visible);
// }

bool BGameStatus_Int::isVisible() const
{
    return d_gsWind->isVisible();
}

bool BGameStatus_Int::isEnabled() const
{
    return d_gsWind->isEnabled();
}

HWND BGameStatus_Int::getHWND() const
{
    return d_gsWind->getHWND();
}


/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.3  2002/11/16 18:03:30  greenius
 * Various changes so that it will compile with Visual Studio .net
 *
 * Revision 1.2  2001/06/18 08:25:44  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
