/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
#ifndef BTW_INFO_HPP
#define BTW_INFO_HPP

//#include "cpidef.hpp"
#include "grtypes.hpp"
#include "batdata.hpp"
#include "bobdef.hpp"

class BUnitInfoWindow;
class BUI_UnitInterface;
//class ICommandPosition;

/*
 * Interface to unit-information window
 */

class BUnitInfo_Int {
    BUnitInfoWindow* d_infoWind;
  public:
    BUnitInfo_Int(BUI_UnitInterface* owner, HWND hParent, CPBattleData batData);
    ~BUnitInfo_Int();

    void run(const CRefBattleCP& cpi, const PixelPoint& p);
    void destroy();
};

#endif

/*
 *----------------------------------------------------------------------
 * $Log$
 *----------------------------------------------------------------------
 */
