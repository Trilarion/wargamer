/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
#ifndef BUNITINF_HPP
#define BUNITINF_HPP

#include "batdata.hpp"
#include "bobdef.hpp"
#include "gamedefs.hpp"
#include "scrnbase.hpp"

class DrawDIBDC;

class BUnitInfo {
    CPBattleData d_batData;

    const int d_dbX;
    const int d_dbY;
    const int d_baseX;
    const int d_baseY;

  public:
    BUnitInfo(CPBattleData batData) :
      d_batData(batData),
      d_dbX(ScreenBase::dbX()),
      d_dbY(ScreenBase::dbY()),
      d_baseX(ScreenBase::baseX()),
      d_baseY(ScreenBase::baseY()) {}

    void drawSummaryTab(DrawDIBDC* dib, const CRefBattleCP& cp, const RECT& dr);
    void drawLeaderTab(DrawDIBDC* dib, const CRefBattleCP& cp, const RECT& dr);
    void drawOBTab(DrawDIBDC* dib, const CRefBattleCP& cp, const RECT& dr);

  private:
//  bool screenBaseInitiated() const { return (d_dbX > 0); }
//  void initScreenBase();

    void drawNameDib(DrawDIBDC* dib, const CRefBattleCP& cp,
       const LONG xPos, const LONG yPos, Boolean forLeader);

    void drawAttributeGraph(DrawDIBDC* dib, const char* text,
       int x, int y, int barOffsetX, int barCX, int barCY, Attribute value, Boolean noInfo);
};

#endif

/*
 *----------------------------------------------------------------------
 * $Log$
 *----------------------------------------------------------------------
 */
