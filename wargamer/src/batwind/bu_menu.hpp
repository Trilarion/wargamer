/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			      *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								            *
 *----------------------------------------------------------------------------*/
#ifndef BATMENU_HPP
#define BATMENU_HPP

#include "grtypes.hpp"
#include "batdata.hpp"
#include "batunit.hpp"
//#include "bobdef.hpp"


class BUnitMenu_Imp;
class BattleWindowsInterface;
class BUI_UnitInterface;
class BattleOrder;

class BUnitMenu_Int {
	 BUnitMenu_Imp* d_unitMenu;

  public:
	 BUnitMenu_Int(BUI_UnitInterface* owner, HWND hParent, BattleWindowsInterface* batWind,
			RCPBattleData batData, BattleOrder& order);
	 ~BUnitMenu_Int();

	 void runMenu(const CRefBattleCP& cp, const PixelPoint& p);

	 void destroy();
};

#endif

/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.2  2001/06/18 08:25:44  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
