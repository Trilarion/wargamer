/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef BUD_ORDR_HPP
#define BUD_ORDR_HPP

#ifndef __cplusplus
#error bud_ordr.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *      Battle Unit Order Dialog
 *
 * Note: Redone on 6/15/98 to hide implementation
 *       Also, changed from a dialog to a window
 *
 * The complete Info and Ordering dialog presented to the player
 *
 * Not using a private hidden implementation, as the only place that
 * uses this is bw_unit.cpp.  We may change our mind if this
 * is dependant on too many Header files.
 *
 *----------------------------------------------------------------------
 */

#include "batdata.hpp"
class PixelPoint;
class BUI_UnitInterface;
struct BattleOrderInfo;
class BattleOrder;
class BM_Window;
class BMO_Window;
class BUD_Info;
//class RCPBattleData;

//namespace BattleWindows_Internal
//{



struct CtrlInfo {
  UWORD d_x;
  UWORD d_y;
  UWORD d_cx;
  UWORD d_cy;

  enum ModeFlags {
         SmallMode = 0x01,
         FullMode = 0x02
  };

  UBYTE d_flags;
};


/*
 * This window allows player to set Division deployment, SP formantions,
 * and other deployment related stuff such as 'Refuse Right Flank', etc.
 */

class BattleOrder_Int {
         BM_Window* d_wind;
  public:

         BattleOrder_Int(BUI_UnitInterface* owner, RCPBattleData batData, BattleOrder& order);
         ~BattleOrder_Int();

//       void init(StackedUnitList& units, int id);
         bool run(const PixelPoint& p, const BUD_Info& info, BattleOrderInfo* order, unsigned int flags);
         bool run(const PixelPoint& p, const BUD_Info& info, BattleOrderInfo* order);

        BattleOrder * getOrder(void);

         void hide();
         void destroy();

};

#if 0
class BattleMoveOrder_Int {
         BMO_Window* d_wind;
  public:

         BattleMoveOrder_Int(BUI_UnitInterface* owner, RCPBattleData batData, BattleOrder& order);
         ~BattleMoveOrder_Int();

         void run(const PixelPoint& p, const BUD_Info& info);
         void hide();
         void destroy();
};
#endif

#if 0  // old way
class BattleOrderDialog : public BattleUnitDial
{
        public:
                BattleOrderDialog(BUI_UnitInterface* owner);
                ~BattleOrderDialog();

        private:
                // Implement BattleUnitDial
                //
                // I leave the virtual keyword to remind that these implement
                // the virtual functions defined in BattleUnitDial
                //
                // The functions are private, because they are meant to be called
                // indirectly from BattleUnitDial's public functions.

                virtual void onCreate(HWND hParent);
                        // Create actual window
                virtual void onShow(const BUD_Info& info);
                        // Called from show() to initialise dialog box
                virtual void onHide();
                        // Called from hide() to free up resources

                /*
                 * Windows Message Handlers
                 */

                virtual BOOL procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
                        // Implements ModelessDialog
                BOOL onInitDialog(HWND hwnd, HWND hwndFocus, LPARAM lParam);
                void onCommand(HWND hwnd, int id, HWND hwndCtl, UINT codeNotify);
                void onDrawItem(HWND hwnd, const DRAWITEMSTRUCT* lpDrawItem);
};
#endif

//};            // namespace BattleWindows_Internal

#endif /* BUD_ORDR_HPP */

