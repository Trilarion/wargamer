/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
#include "stdinc.hpp"
#include "batw_oob.hpp"


#include "app.hpp"
#include "scenario.hpp"
#include "resdef.h"
#include "resdef.h"
#include "scenario.hpp"
#include "options.hpp"
#include "scn_res.h"
#include "wmisc.hpp"
#include "resdef.h"
#include "bobiter.hpp"

/*

        Battle OB Treeview class

*/



HWND
BattleOBTreeView::createTV(HWND hparent, int id) {

    HWND hwndTV = CreateWindowEx(
    0,
    "SysTreeView32",
    "BattleOB Tree View",
    WS_CHILD | WS_BORDER | TVS_HASLINES | TVS_HASBUTTONS | TVS_LINESATROOT,
    0, 0, 0, 0,
    hparent,
    (HMENU) id,
   APP::instance(),
   NULL);

    ASSERT(hwndTV != NULL);
    return hwndTV;
}


void
BattleOBTreeView::createImageList(HWND hwnd) {

    m_ImageList = ImageList_LoadBitmap(
        scenario->getScenarioResDLL(),
        MAKEINTRESOURCE(BM_OBICONS),
        OBI_CX,
        0,
        CLR_NONE);

    ASSERT(m_ImageList != NULL);
}


void
BattleOBTreeView::addItems(HWND hwndTV, BattleData * batdata) {

    TV_INSERTSTRUCT tvi;
    tvi.hInsertAfter = TVI_LAST;

    /*
    Start with root node
    */

    tvi.hParent = TVI_ROOT;
    tvi.item.pszText = LPSTR_TEXTCALLBACK;
    tvi.item.iImage = m_RootNode->getSide() * OBI_IconsPerSide + OBI_Side;
//    tvi.item.iSelectedImage = m_RootNode->getSide() * OBI_IconsPerSide + OBI_SideSelected;
    tvi.item.iSelectedImage = 1 * OBI_IconsPerSide + OBI_SideSelected;
    BattleOBTreeViewInfo * info = new BattleOBTreeViewInfo(m_RootNode);
    tvi.item.lParam = (LPARAM) info;
    tvi.item.mask = TVIF_IMAGE | TVIF_SELECTEDIMAGE | TVIF_TEXT | TVIF_PARAM;

    HTREEITEM hTopItem = TreeView_InsertItem(hwndTV, &tvi);

    // add any SPs
    if(m_RootNode->sp() ) {
        if(m_bShowStrengthPoints) addStrengthPoints(hwndTV, m_RootNode, tvi, hTopItem, batdata);
    }

    // add any child CPs
    if(m_RootNode->child() ) {
            addCommandPositions(hwndTV, m_RootNode->child(), tvi, hTopItem, batdata);
    }

}


/*
Add all CommandPositions under and including a given CP
*/
void
BattleOBTreeView::addCommandPositions(HWND hwndTV, BattleCP * cp, TV_INSERTSTRUCT& tvi, HTREEITEM hparent, BattleData * batdata) {

    tvi.hParent = hparent;
    BattleOBTreeViewInfo * info = new BattleOBTreeViewInfo(cp);
    tvi.item.lParam = (LPARAM) info;

    // add this CP
    HTREEITEM hti = addUnit(hwndTV, tvi, cp, batdata);

    // add any SPs under this CP
    if(cp->sp() ) {
        if(m_bShowStrengthPoints) addStrengthPoints(hwndTV, cp, tvi, hti, batdata);
    }

    // recursively add child CPs under this CP
    if(cp->child() ) {
        addCommandPositions(hwndTV, cp->child(), tvi, hti, batdata);
    }

    // recursively add sister CPs under parent CP
    if(cp->sister() ) {
        addCommandPositions(hwndTV, cp->sister(), tvi, hparent, batdata);
    }

}



/*
Add all StrengthPoints under a given CP
*/
void
BattleOBTreeView::addStrengthPoints(HWND hwndTV, BattleCP * cp, TV_INSERTSTRUCT& tvi, HTREEITEM hparent, BattleData * batdata) {

    if(!cp->sp() ) return;

    BattleSP * sp = cp->sp();

    while(sp) {

        tvi.hParent = hparent;
        BattleOBTreeViewInfo * info = new BattleOBTreeViewInfo(sp);
        tvi.item.lParam = (LPARAM) info;
    
        addUnit(hwndTV, tvi, sp, batdata);
        sp = sp->sister();
    }
}



HTREEITEM
BattleOBTreeView::addUnit(HWND hwndTV, TV_INSERTSTRUCT& tvi, BattleUnit * unit, BattleData * batdata) {

    /*
    Cast into CP or SP
    */

    BattleCP * cp = dynamic_cast<BattleCP *>(unit);
    // if this is a CP
    if(cp) {

        RankEnum rank = cp->getRank().getRankEnum();

        switch(rank) {
            case Rank_God :
            case Rank_President :
            case Rank_Army :
            case Rank_ArmyGroup : {
                tvi.item.iImage = cp->getSide() * OBI_IconsPerSide + OBI_Army;
                tvi.item.iSelectedImage = cp->getSide() * OBI_IconsPerSide + OBI_ArmySelected;
                break; }
            case Rank_Corps : {
                tvi.item.iImage = cp->getSide() * OBI_IconsPerSide + OBI_Corps;
                tvi.item.iSelectedImage = cp->getSide() * OBI_IconsPerSide + OBI_CorpsSelected;
                break; }
            case Rank_Division : {
                tvi.item.iImage = cp->getSide() * OBI_IconsPerSide + OBI_Division;
                tvi.item.iSelectedImage = cp->getSide() * OBI_IconsPerSide + OBI_DivisionSelected;
                break; }
            default : {
                FORCEASSERT("Oops - bad rank in BattleOB window");
                tvi.item.iImage = cp->getSide() * OBI_IconsPerSide + OBI_Brigade;
                tvi.item.iSelectedImage = cp->getSide() * OBI_IconsPerSide + OBI_BrigadeSelected;
                break; }
        }

        HTREEITEM ti = TreeView_InsertItem(hwndTV, &tvi);
        return ti;
    }

    BattleSP * sp = dynamic_cast<BattleSP *>(unit);
    // if this is a SP
    if(sp) {

        const UnitTypeItem& unitType = batdata->ob()->getUnitType(sp);
        BasicUnitType::value basicType = unitType.getBasicType();

        switch(basicType) {
            default : {
                tvi.item.iImage = sp->parent()->getSide() * OBI_IconsPerSide + OBI_Brigade;
                tvi.item.iSelectedImage = sp->parent()->getSide() * OBI_IconsPerSide + OBI_BrigadeSelected;
                break; }
        }

        HTREEITEM ti = TreeView_InsertItem(hwndTV, &tvi);
        return ti;
    }
FORCEASSERT("Oops - couldn't add item to Battle OB TreeView (invalid battleunit)");
return NULL;
}
        




void
BattleOBTreeView::SelectUnit(BattleCP * cp) {


}


void
BattleOBTreeView::DeselectUnit(BattleCP * cp) {


}


/*
 *----------------------------------------------------------------------
 * $Log$
 *----------------------------------------------------------------------
 */
