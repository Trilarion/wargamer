/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef BW_TINY_HPP
#define BW_TINY_HPP

#ifndef __cplusplus
#error bw_tiny.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *      Tiny Map Window with controls
 *
 *----------------------------------------------------------------------
 */

#include "batdata.hpp"
#include "btwin_i.hpp"
#include "batmap_i.hpp"
#include "wind.hpp"

namespace BattleWindows_Internal
{

class BW_LocatorImp;

class BW_Locator : public Window
{
        public:
                BW_Locator(RPBattleWindows batWind, RCPBattleData batData);
                ~BW_Locator();

                void reset();  // Map has changed

                void setZoomButtons(BattleMapInfo::Mode mode);
                        // update the zoom buttons
                void setLocation(const BattleArea& area);
                        // Set the location of the tiny map
                void update(bool all);
                        // Redraw the tiny map

                void toggle();  // Hide if showing, Show if hidden
                void show(bool visible);
                bool isVisible() const;
                bool isEnabled() const;
                void enable(bool visible);
                HWND getHWND() const;

        private:
                BW_LocatorImp* d_imp;
};

};      // namespace BattleWindows_Internal

#endif /* BW_TINY_HPP */

