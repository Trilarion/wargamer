/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
#include "stdinc.hpp"
#include "bunitinf.hpp"
#include "scenario.hpp"
#include "dib.hpp"
#include "dib_util.hpp"
#include "scn_img.hpp"
#include "wmisc.hpp"
#include "fonts.hpp"
#include "batarmy.hpp"
#include "bob_cp.hpp"
#include "bob_sp.hpp"
#include "batord.hpp"
#include "bobutil.hpp"
#include "leader.hpp"
#include "obdefs.hpp"
#include "imglib.hpp"
#include "scn_res.h"
#include "sllist.hpp"
#include "fsalloc.hpp"
#include "bobiter.hpp"
#include "tbldimg.hpp"
#include "resstr.hpp"

namespace Local {

// local list item for holdong SP info
struct SPItem : public SLink {
  UnitType             d_type;
  BasicUnitType::value d_basicType;
  const char*          d_name;
  int                  d_strength;
  Attribute            d_baseMorale;
  int                  d_count;

  SPItem(UnitType type, BasicUnitType::value bt, const char* name, int str, Attribute bm) :
    d_type(type),
    d_basicType(bt),
    d_name(name),
    d_strength(str),
    d_baseMorale(bm),
    d_count(1) {}
  ~SPItem() {}

  void* operator new(size_t size);
#ifdef _MSC_VER
  void operator delete(void* deadObject);
#else
  void operator delete(void* deadObject, size_t size);
#endif
};


/*-------------------------------------------------------------------
 * HexList class, used for storing routes, deployment hexes, etc.
 */

const int chunkSize = 50;

#ifdef DEBUG
static FixedSize_Allocator itemAlloc(sizeof(SPItem), chunkSize, "SPItem");
#else
static FixedSize_Allocator itemAlloc(sizeof(SPItem), chunkSize);
#endif

void* SPItem::operator new(size_t size)
{
  ASSERT(size == sizeof(SPItem));
  return itemAlloc.alloc(size);
}     

#ifdef _MSC_VER
void SPItem::operator delete(void* deadObject)
{
  itemAlloc.free(deadObject, sizeof(SPItem));
}
#else
void SPItem::operator delete(void* deadObject, size_t size)
{
  ASSERT(size == sizeof(SPItem));
  itemAlloc.free(deadObject, size);
}
#endif

class LSPList : public SList<SPItem> {
    CPBattleData d_batData;
  public:
    LSPList(CPBattleData bd) : d_batData(bd) {}

    void addToList(UnitType ut, BasicUnitType::value bt, const char* name, int str, Attribute bm);
};

void LSPList::addToList(UnitType ut, BasicUnitType::value bt, const char* name, int str, Attribute bm)
{
  bool inList = False;

  // if type is already in list, inc counts
  // add unit type to list if not already in list
  SListIter<SPItem> iter(this);
  while(++iter)
  {
    if(ut == iter.current()->d_type)
    {
      iter.current()->d_strength += str;
      iter.current()->d_count++;
      inList = True;
    }
  }

  if(!inList)
  {
    SPItem* si = new SPItem(ut, bt, name, str, bm);
    ASSERT(si);

    append(si);
  }
}

}; // end namespace

#if 0
void BUnitInfo::initScreenBase()
{
  d_dbX = ScreenBase::dbX();
  d_dbY = ScreenBase::dbY();
  d_baseX = ScreenBase::baseX();
  d_baseY = ScreenBase::baseY();
}
#endif

void BUnitInfo::drawNameDib(DrawDIBDC* dib, const CRefBattleCP& cp,
   const LONG xPos, const LONG yPos, Boolean forLeader)
{
  ASSERT(dib != 0);
  ASSERT(cp != NoBattleCP);

  /*
   * some useful values
   */

  RefGLeader leader = cp->leader();
//  Boolean limitedInfo = (CampaignOptions::get(OPT_LimitedUnitInfo) && !CampaignArmy_Util::isUnitOrderable(cpi));
  const ImageLibrary* specialistIcons = ScenarioImageLibrary::get(ScenarioImageLibrary::SpecialistIcons);

  // draw frame
  int frameX = xPos - ((2 * d_dbX) / d_baseX);
  int frameY = yPos - ((2 * d_dbY) / d_baseY);
  int frameCX = (147 * d_dbX) / d_baseX;
  int frameCY = (23 * d_dbY) / d_baseY;

  ColourIndex ci = dib->getColour(PALETTERGB(0, 0, 0));
  dib->frame(frameX, frameY, frameCX, frameCY, ci);

  LogFont lf;
  lf.height((10 * d_dbY) / d_baseY);
  lf.weight(FW_MEDIUM);
  lf.face(scenario->fontName(Font_Bold));
  lf.italic(true);
  lf.underline(true);

  Font font;
  font.set(lf);

  HFONT oldFont = dib->setFont(font);
  COLORREF oldColor = dib->setTextColor(scenario->getSideColour(cp->getSide()));

  /*
   * Put flag icon
   */

  const ImageLibrary* flagIcons = scenario->getNationFlag((forLeader) ? leader->getNation() : cp->getNation(), True);

  LONG x = xPos + ((1 * d_dbX) / d_baseX);
  LONG y = yPos + ((1 * d_dbY) / d_baseY);

  flagIcons->blit(dib, FI_Army, x,  y, True);

  /*
   * Put leader name
   */

  x = (20 * d_dbX) / d_baseX;

#if 0
  const char* leaderName = (!limitedInfo || cp->getInfoQuality() >= CommandPosition::VeryLimitedInfo)
     ? leader->getName() : s_strings.get(UnitInfoString::LeaderUnknown); //"Leader Unknown";

  const char* unitName = (!limitedInfo || cp->getInfoQuality() >= CommandPosition::VeryLimitedInfo)
     ? cp->getNameNotNull() : s_strings.get(UnitInfoString::UnidentifiedUnit);//"Unidentified Unit";
#endif

  if(forLeader)
    wTextPrintf(dib->getDC(), x, y, "%s - %s", leader->getName(), cp->getName());
  else
    wTextPrintf(dib->getDC(), x, y, "%s - %s", cp->getName(), leader->getName());

  /*
   * If leader is a specialist, put icon
   */

  y = yPos + ((12 * d_dbY) / d_baseY);

  /*
   * Put nationality
   */

  lf.height((8 * d_dbY) / d_baseY);
  lf.underline(false);
  lf.italic(false);

  font.set(lf);
  dib->setFont(font);
  dib->setTextColor(PALETTERGB(0, 0, 0));
  const char* text = 0;

  if(forLeader)
    text = InGameText::get(IDS_UI_LEADER);   // "Leader";
  else
  {
    static char buf[50];

    // local enum
    enum LUnitType {
      Inf,
      Cav,
      Art,
      CombinedArms,

      LUnitType_HowMany,
      LUnitType_Undefined
    } lUnitType = (cp->generic()->isCombinedArms()) ? CombinedArms :
                  (cp->generic()->isInfantry()) ? Inf :
                  (cp->generic()->isCavalry()) ? Cav :
                  (cp->generic()->isArtillery()) ? Art : LUnitType_Undefined;

    ASSERT(lUnitType != LUnitType_Undefined);

    // TODO get strings from resource
//     static const char* s_typeText[LUnitType_HowMany] = {
//       "Infantry",
//       "Cavalry",
//       "Artillery",
//       "Combined-Arms"
//     };
   static const InGameText::ID s_typeText[LUnitType_HowMany] =
   {
      IDS_UI_INFANTRY,
      IDS_UI_CAVALRY,
      IDS_UI_ARTY,
      IDS_UI_CombinedArms
   };

    wsprintf(buf, "%s %s", static_cast<const char*>(InGameText::get(s_typeText[lUnitType])), cp->getRank().getRankName());
    text = buf;
  }

  wTextPrintf(dib->getDC(), x, y, "%s %s", scenario->getNationAdjective(cp->getNation()), text);

  if(forLeader)
  {
    if(leader->isSpecialist())
    {
      x = (110 * d_dbX) / d_baseX;

      // temporary until this is straightened out in compos.hpp
      UWORD index = 0;

      if(leader->isSpecialistType(Specialist::Cavalry))
      {
        index = (leader->isCommandingType()) ? 0 : 2;
      }
      else if(leader->isSpecialistType(Specialist::Artillery))
      {
        index = (leader->isCommandingType()) ? 1 : 3;
      }

      specialistIcons->blit(dib, index, x, y);
    }
  }

  dib->setFont(oldFont);
  dib->setTextColor(oldColor);
}

void BUnitInfo::drawAttributeGraph(DrawDIBDC* dib, const char* text,
    int x, int y, int barOffsetX, int barCX, int barCY, Attribute value, Boolean noInfo)
{
  ASSERT(dib);
  wTextOut(dib->getDC(), x, y, text);

  static SIZE s;
  GetTextExtentPoint32(dib->getDC(), text, lstrlen(text), &s);

  const int barY = y + ((s.cy - barCY) / 2);
  dib->drawBarChart(barOffsetX, barY, (noInfo) ? 0 : value, Attribute_Range, barCX, barCY);

  if(noInfo)
    dib->darkenRectangle(barOffsetX, barY, barCX, barCY, 50);
}

void BUnitInfo::drawSummaryTab(DrawDIBDC* dib, const CRefBattleCP& cp, const RECT& dr)
{
  ASSERT(dib);
  ASSERT(cp != NoBattleCP);

//  if(!screenBaseInitiated())
//  initScreenBase();

  const int xMargin = dr.left + ((4 * d_dbX) / d_baseX);
  const int yMargin = dr.top  + ((4 * d_dbY) / d_baseY);

  int x = xMargin;
  int y = yMargin;

  drawNameDib(dib, cp, x, y, False);

  /*
   * Get fonts. One underlined, one not
   */

  // get normal
  LogFont lf;
  lf.height((8 * d_dbY) / d_baseY);
  lf.weight(FW_MEDIUM);
  lf.face(scenario->fontName(Font_Bold));
  lf.italic(true);

  Font font;
  font.set(lf);

  HFONT oldFont = dib->setFont(font);

  // get underlined
  lf.height((7 * d_dbY) / d_baseY);
  lf.italic(false);
  font.set(lf);
  HFONT nFont = font;

  lf.underline(true);
  font.set(lf);

  HFONT lFont = font;

  /*----------------
   * Put morale. Includes frame, text and bargraph
   */

  const int barOffsetX = x + ((30 * d_dbX) / d_baseX);
  const int barCX = ((40 * d_dbX) / d_baseX);
  const int barCY = ((5 * d_dbY) / d_baseY);

  int frameX = dr.left + ((2 * d_dbX) / d_baseX);
  int frameY = yMargin + ((23 * d_dbY) / d_baseY);
  int frameCX = (75 * d_dbX) / d_baseX;
  int frameCY = (40 * d_dbY) / d_baseY;

  // draw frame
  ColourIndex ci = dib->getColour(PALETTERGB(0, 0, 0));
  dib->frame(frameX, frameY, frameCX, frameCY, ci);

  dib->setFont(nFont);

  // put text
  y = yMargin + ((26 * d_dbY) / d_baseY);
  drawAttributeGraph(dib, InGameText::get(IDS_UI_MORALE), x, y, barOffsetX, barCX, barCY, cp->morale(), False);

  /*------------------------
   * Put Fatigue
   */

  // put text
  y = yMargin + ((34 * d_dbY) / d_baseY);
  drawAttributeGraph(dib, InGameText::get(IDS_UI_FATIGUE), x, y, barOffsetX, barCX, barCY, cp->fatigue(), False);

  /*------------------------------------------
   * Put Quality
   */

  // put text
  y = yMargin + ((42 * d_dbY) / d_baseY);
  Attribute bMorale = BobUtility::baseMorale(d_batData->ob(), cp);
  // Attribute bMorale = BobUtility::baseMorale(const_cast<BattleData*>(reinterpret_cast<const BattleData*>(d_batData))->ob(), const_cast<RefBattleCP>(cp));
  drawAttributeGraph(dib, InGameText::get(IDS_Quality), x, y, barOffsetX, barCX, barCY, bMorale, False);

  /*
   * Put Leader's initiative
   */

  // put text
  y = yMargin + ((50 * d_dbY) / d_baseY);
  drawAttributeGraph(dib, InGameText::get(IDS_UI_INITIATIVE), x, y, barOffsetX, barCX, barCY, cp->leader()->getInitiative(True), False);

  /*
   * Put brief order description
   */

  dib->setFont(lFont);

  frameY += frameCY + ((2 * d_dbY) / d_baseY); //yMargin + ((22 * d_dbY) / d_baseY);
  frameCY = (17 * d_dbY) / d_baseY;

  // draw frame
  ci = dib->getColour(PALETTERGB(0, 0, 0));
  dib->frame(frameX, frameY, frameCX, frameCY, ci);

  y = yMargin + ((66 * d_dbY) / d_baseY);

//  SimpleString text = s_strings.get(UnitInfoString::Orders);
  SimpleString text = InGameText::get(IDS_UI_ORDERS);
  ASSERT(text.toStr());
  text += ':';

  wTextOut(dib->getDC(), x, y, text.toStr());

  dib->setFont(nFont);

  y = yMargin + ((72 * d_dbY) / d_baseY);

  text = BattleOrderInfo::orderName(cp->getCurrentOrder().mode());
  wTextOut(dib->getDC(), x, y, text.toStr());
#if 0
  RECT r;
  SetRect(&r, x, y, x+ ((75 * d_dbX) / d_baseX), y + ((20 * d_dbY) / d_baseY));
  DrawText(dib->getDC(), text.toStr(), text.length(), &r, DT_WORDBREAK);
#endif
  /*
   * Put number of formations
   */

//  int dataOffsetX = xMargin + ((115 * d_dbX) / d_baseX);
  int dataOffsetX = xMargin + ((103 * d_dbX) / d_baseX);    // modified for german
  x = xMargin + ((77 * d_dbX) / d_baseX);
  y = yMargin + (24 * d_dbY) / d_baseY;

  frameX = x - ((2 * d_dbX) / d_baseX);
  frameY = yMargin + ((23 * d_dbY) / d_baseY);
  frameCX = (70 * d_dbX) / d_baseX;
  frameCY = (59 * d_dbY) / d_baseY;

  // draw frame
  ci = dib->getColour(PALETTERGB(0, 0, 0));
  dib->frame(frameX, frameY, frameCX, frameCY, ci);

  dib->setFont(lFont);

  text = InGameText::get(IDS_UI_FORMATIONS); //s_strings.get(UnitInfoString::Formations);
  wTextOut(dib->getDC(), x, y, text.toStr());

  dib->setFont(nFont);

  // corps
  x += ((3 * d_dbX) / d_baseX);
  y = yMargin + ((31 * d_dbY) / d_baseY);

  // count children
  wTextOut(dib->getDC(), x, y, InGameText::get(IDS_Corps));
  int nChildren = (cp->getRank().sameRank(Rank_Army)) ?
    BobUtility::countChildren(const_cast<RefBattleCP>(cp)) : 0;


  wTextPrintf(dib->getDC(), dataOffsetX, y, "%d", nChildren);

  // divisions
  y = yMargin + ((37 * d_dbY) / d_baseY);

  wTextOut(dib->getDC(), x, y, InGameText::get(IDS_Divs)); //s_strings.get(UnitInfoString::Divisions));
  nChildren = (cp->getRank().sameRank(Rank_Corps)) ?
    BobUtility::countUnits(const_cast<RefBattleCP>(cp)) - nChildren : 0;

  wTextPrintf(dib->getDC(), dataOffsetX, y, "%d", nChildren);

  /*
   * Put number of each basic unit types.
   * Infantry value is in # of men. Each SP = 1000 men
   * Cavalry value is in # of men. Each SP = 500 men
   * Artillery value is in # of guns. Each SP = 12 guns
   * Other value is in # of men. Each SP = 250 men
   */

  dib->setFont(lFont);

  x = xMargin + ((77 * d_dbX) / d_baseX);
  y = yMargin + ((47 * d_dbY) / d_baseY);
  text = InGameText::get(IDS_UI_STRENGTH); // s_strings.get(UnitInfoString::Strength);
  wTextOut(dib->getDC(), x, y, text.toStr());

  dib->setFont(nFont);

  BobUtility::UnitCounts uc;
  BobUtility::unitsInManpower(cp, d_batData, uc);

  // infantry
  x += ((3 * d_dbX) / d_baseX);
  y = yMargin + ((54 * d_dbY) / d_baseY);

  wTextOut(dib->getDC(), x, y, InGameText::get(IDS_Inf)); //s_strings.get(UnitInfoString::Infantry));
  wTextPrintf(dib->getDC(), dataOffsetX, y, "%d", uc.d_nInf);

  // cavalry
  y = yMargin + ((60 * d_dbY) / d_baseY);

  wTextOut(dib->getDC(), x, y, InGameText::get(IDS_Cav)); //s_strings.get(UnitInfoString::Cavalry));
  wTextPrintf(dib->getDC(), dataOffsetX, y, "%d", uc.d_nCav);

  // artillery
  y = yMargin + ((66 * d_dbY) / d_baseY);

  wTextOut(dib->getDC(), x, y, InGameText::get(IDS_Art));  // "Art"); //s_strings.get(UnitInfoString::Artillery));
  wTextPrintf(dib->getDC(), dataOffsetX, y, "%d %s", uc.d_nGuns,
       InGameText::get(IDS_Guns)); //s_strings.get(UnitInfoString::Guns));

  dib->setFont(font);
}

void BUnitInfo::drawLeaderTab(DrawDIBDC* dib, const CRefBattleCP& cp, const RECT& dr)
{
  ASSERT(dib != 0);
  ASSERT(cp != NoBattleCP);

//  if(!screenBaseInitiated())
//  initScreenBase();

  /*
   * some useful values
   */

  RefGLeader leader = cp->leader();

//  const Boolean limitedInfo = (CampaignOptions::get(OPT_LimitedUnitInfo) && !CampaignArmy_Util::isUnitOrderable(cpi));

  // make our unit attributes

  const int xMargin = dr.left + ((4 * d_dbX) / d_baseX);
  const int yMargin = dr.top  + ((4 * d_dbY) / d_baseY);
  int x = xMargin;
  int y = yMargin;

  /*--------------------------------------------
   * Put name header
   */

  drawNameDib(dib, cp, x, y, True);

  /*
   * Get fonts. One underlined, one not
   */

  // get italic
  LogFont lf;
  lf.height((8 * d_dbY) / d_baseY);
  lf.weight(FW_MEDIUM);
  lf.face(scenario->fontName(Font_Bold));
  lf.italic(true);

  Font font;
  font.set(lf);

  HFONT oldFont = dib->setFont(font);

  // get normal
  lf.height((7 * d_dbY) / d_baseY);
  lf.italic(false);
  font.set(lf);
  HFONT nFont = font;

  // get underlined
  lf.underline(true);
  font.set(lf);

  HFONT lFont = font;

  int frameX = dr.left + ((2 * d_dbX) / d_baseX);
  int frameY = yMargin + ((23 * d_dbY) / d_baseY);
  int frameCX = (147 * d_dbX) / d_baseX;
  int frameCY = (59 * d_dbY) / d_baseY;

  // draw frame
  ColourIndex ci = dib->getColour(PALETTERGB(0, 0, 0));
  dib->frame(frameX, frameY, frameCX, frameCY, ci);

  dib->setFont(nFont);

  /*
   * put number of formations unit can 'effectively' control
   * Formula is leader's initiative+staff / 100. (Rounded out to nearest)
   */

//  if(!limitedInfo)
  {
    y = yMargin + ((24 * d_dbY) / d_baseY);

    const int divisor = 100;
    const int roundOutValue = (divisor / 2) - 1;

    int nCanControl = (leader->getInitiative() + leader->getStaff() + roundOutValue) / divisor;

    const char* format = InGameText::get(IDS_HQCanControlUnits); //s_strings.get(UnitInfoString::CanControl);
    wTextPrintf(dib->getDC(), x, y, format, nCanControl);
  }

  /*---------------------------------------------
   * Put initiative. Includes text and bargraph
   */

  x += (6 * d_dbX) / d_baseX;

  const int barOffsetX = x + ((45 * d_dbX) / d_baseX);
  const int barCX = ((40 * d_dbX) / d_baseX);
  const int barCY = ((5 * d_dbY) / d_baseY);

  // put text
  y = yMargin + ((34 * d_dbY) / d_baseY);
  drawAttributeGraph(dib, InGameText::get(IDS_UI_INITIATIVE), //s_strings.get(UnitInfoString::Initiative),
     x, y, barOffsetX, barCX, barCY, leader->getInitiative(True), False); //(limitedInfo && cpi->getInfoQuality() < CommandPosition::FullInfo));

  /*------------------------
   * Put Staff
   */

  y = yMargin + ((40 * d_dbY) / d_baseY);
  drawAttributeGraph(dib, InGameText::get(IDS_UI_STAFF), //s_strings.get(UnitInfoString::Staff),
     x, y, barOffsetX, barCX, barCY, leader->getStaff(True), False); //(limitedInfo && cpi->getInfoQuality() < CommandPosition::FullInfo));


  /*-------------------------------------
   * Put Subordination
   */

  y = yMargin + ((46 * d_dbY) / d_baseY);
  drawAttributeGraph(dib, InGameText::get(IDS_UI_SUBORDINATION), //s_strings.get(UnitInfoString::Subordination),
     x, y, barOffsetX, barCX, barCY, leader->getSubordination(), False); //(limitedInfo && cpi->getInfoQuality() < CommandPosition::FullInfo));

  /*---------------------------------------
   * Put Aggression
   */

  y = yMargin + ((52 * d_dbY) / d_baseY);
  drawAttributeGraph(dib, InGameText::get(IDS_UI_AGGRESSION), //s_strings.get(UnitInfoString::Aggression),
     x, y, barOffsetX, barCX, barCY, leader->getAggression(True), False); //(limitedInfo && cpi->getInfoQuality() < CommandPosition::FullInfo));

  /*----------------------------------------
   * Charisma
   */

  y = yMargin + ((58 * d_dbY) / d_baseY);
  drawAttributeGraph(dib, InGameText::get(IDS_UI_CHARISMA), //s_strings.get(UnitInfoString::Charisma),
     x, y, barOffsetX, barCX, barCY, leader->getCharisma(True), False); //(limitedInfo && cpi->getInfoQuality() < CommandPosition::FullInfo));

  /*-----------------------------------------
   * Put Tactical Ability
   */

  y = yMargin + ((64 * d_dbY) / d_baseY);
  drawAttributeGraph(dib, InGameText::get(IDS_UI_ABILITY), // s_strings.get(UnitInfoString::Ability),
     x, y, barOffsetX, barCX, barCY, leader->getTactical(True), False); //(limitedInfo && cpi->getInfoQuality() < CommandPosition::FullInfo));

  /*-----------------------------------------
   * Put Command Ability
   */

  x = xMargin;
  y = yMargin + ((74 * d_dbY) / d_baseY);

  const char* text = leader->getRankLevel().getRankName(False);
  wTextPrintf(dib->getDC(), x, y, "%s %s",
    InGameText::get(IDS_UI_COMMANDABILITY), //s_strings.get(UnitInfoString::CommandAbility),
    text); //s_strings.get(UnitInfoString::Ability));

  /*
   * Put Icon if he is sick
   */

  if(leader->isSick())
  {


  }
}


void BUnitInfo::drawOBTab(DrawDIBDC* dib, const CRefBattleCP& cp, const RECT& dr)
{
  ASSERT(dib != 0);
  ASSERT(cp != NoBattleCP);

//  if(!screenBaseInitiated())
//  initScreenBase();

  /*
   * some useful values
   */

  const RefGLeader& leader = cp->leader();

//  const Boolean limitedInfo = (CampaignOptions::get(OPT_LimitedUnitInfo) && !CampaignArmy_Util::isUnitOrderable(cpi));

  // make our unit attributes

  const int xMargin = dr.left + ((4 * d_dbX) / d_baseX);
  const int yMargin = dr.top + ((4 * d_dbY) / d_baseY);
  int x = xMargin;
  int y = yMargin;

  // set bar char width and height
  const int barOffsetX = x + ((125 * d_dbX) / d_baseX);
  const int barCX = ((17 * d_dbX) / d_baseX);
  const int barCY = ((3 * d_dbY) / d_baseY);
  const int spacing = (7 * d_dbY) / d_baseY;
  const int itemSpacing = (16 * d_dbY) / d_baseY;

  /*--------------------------------------------
   * Put name header
   */

  drawNameDib(dib, cp, x, y, False);

  // put frame around info area
  int frameX = dr.left + ((2 * d_dbX) / d_baseX);
  int frameY = yMargin + ((23 * d_dbY) / d_baseY);
  int frameCX = (147 * d_dbX) / d_baseX;
  int frameCY = (59 * d_dbY) / d_baseY;

  // draw frame
  ColourIndex ci = dib->getColour(PALETTERGB(0, 0, 0));
  dib->frame(frameX, frameY, frameCX, frameCY, ci);

  /*
   * Get fonts. One underlined, one not
   */

  // get normal
  LogFont lf;
  lf.height((7 * d_dbY) / d_baseY);
  lf.weight(FW_MEDIUM);
  lf.face(scenario->fontName(Font_Bold));
  lf.italic(false);

  Font font;
  font.set(lf);
  HFONT nFont = font;

  // get underlined
  lf.underline(true);
  lf.height((8 * d_dbY) / d_baseY);
  font.set(lf);

  HFONT lFont = font;
  y += ((24 * d_dbY) / d_baseY);

  if(cp->getRank().sameRank(Rank_Division))
  {
    /*
     * Go through and display sp's by type, strength, and base morale
     */

    // get list of SP's by type
    Local::LSPList list(d_batData);
    for(ConstBattleSPIter spIter(d_batData->ob(), cp, BattleSPIter::All);
        !spIter.isFinished();
        spIter.next())
    {
      UnitType ut = spIter.sp()->getUnitType();

      int str = 0;
      const UnitTypeItem& uti = d_batData->ob()->getUnitType(spIter.sp());
      if(uti.getBasicType() == BasicUnitType::Infantry)
      {
        str =  MulDiv(UnitTypeConst::InfantryPerSP, spIter.sp()->strength(100), 100);
      }
      else if(uti.getBasicType() == BasicUnitType::Cavalry)
      {
        str = MulDiv(UnitTypeConst::CavalryPerSP, spIter.sp()->strength(100), 100);
      }
      else if(uti.getBasicType() == BasicUnitType::Artillery)
      {
        str += MulDiv(UnitTypeConst::GunsPerSP, spIter.sp()->strength(100), 100);
      }

      list.addToList(ut, uti.getBasicType(), uti.getName(), str, uti.getBaseMorale());
    }

    HFONT oldFont = 0;

    /*
     * now display list
     */

    // const ImageLibrary* images = ScenarioImageLibrary::get(ScenarioImageLibrary::TownBuildPageImages);
    // enum {
    //   InfIcon = 3,
    //   CavIcon,
    //   ArtIcon,
    //   OtherIcon,
     //
    //   Icon_Last
    // };

     TownBuildImages images;

    SListIterR<Local::SPItem> iter(&list);
    while(++iter)
    {
      x = xMargin;
      // UWORD imageID = (iter.current()->d_basicType == BasicUnitType::Infantry) ? InfIcon :
      //                 (iter.current()->d_basicType == BasicUnitType::Cavalry) ? CavIcon :
      //                 (iter.current()->d_basicType == BasicUnitType::Artillery) ? ArtIcon : OtherIcon;

        TownBuildImages::Value imageID = images.getUnitTypeIndex(iter.current()->d_basicType);

      int w;
      int h;
      images.getImageSize(imageID, w, h);

      // get stretch size
      int oldH = h;
      h = itemSpacing - 2;

      ASSERT(oldH);
      w = (w * h) / oldH;

      images.stretchBlit(dib, imageID, x, y, w, h);

      const int numOffsetX = (18 * d_dbX) / d_baseX;
      const int nameOffsetX = ((8 * d_dbX) / d_baseX);

      x += numOffsetX;
      wTextPrintf(dib->getDC(), x, y, "%d", iter.current()->d_count);

      x += nameOffsetX;
      wTextOut(dib->getDC(), x, y, iter.current()->d_name);

      if((y + itemSpacing) >= dr.bottom)
        break;

      y += itemSpacing;
    }

    if(oldFont)
      dib->setFont(oldFont);
  }
  else
  {

    /*
     * Go through and list each immediate subordinate
     * if we run out of room some of them will be left off
     */

    HFONT oldFont = 0;
    CRefBattleCP cpiChild = cp->child();
    while(cpiChild != NoBattleCP)
    {
//    const ImageLibrary* flagIcons = scenario->getNationFlag(cpiChild->getNation(),
//     (limitedInfo && cpi->getInfoQuality() == CommandPosition::NoInfo) ? False : True);

      const ImageLibrary* flagIcons = scenario->getNationFlag(cpiChild->getNation(), True);

      // TODO: get from resource
      const char* inf = InGameText::get(IDS_UI_INF);  // "Inf:";
      const char* cav = InGameText::get(IDS_UI_CAV);  // "Cav:";
      const char* guns = InGameText::get(IDS_GunsAbrev);   // "Guns:";
      const char* str = InGameText::get(IDS_UI_STR);  // "Str:";

      BobUtility::UnitCounts uc;
      BobUtility::unitsInManpower(cpiChild, d_batData, uc);

      x = xMargin;

      /*
       * Put flags
       */

      flagIcons->blit(dib, FI_Division, x, y, True);

      /*
       * Put name
       */

      x += (7 * d_dbX) / d_baseX;

      oldFont = dib->setFont(lFont);
      wTextPrintf(dib->getDC(), x, y, "%s (%s)",
        cpiChild->getName(), cpiChild->leader()->getName());

      /*
       * Put total strength
       */

      dib->setFont(nFont);
      int workX = x + ((105 * d_dbX) / d_baseX);
      wTextPrintf(dib->getDC(), workX, y, "%s %ld", str, uc.d_nInf + uc.d_nArtMP + uc.d_nCav);

      /*
       * Put type strengths
       */

      // inf
      const int spaceX = ((32 * d_dbX) / d_baseX);
      int workY = y + (spacing);
      workX = x;
      wTextPrintf(dib->getDC(), workX, workY, "%s %ld", inf, uc.d_nInf);

      // cav
      workX += spaceX;
      wTextPrintf(dib->getDC(), workX, workY, "%s %ld", cav, uc.d_nCav);

      // cav
      workX += spaceX;
      wTextPrintf(dib->getDC(), workX, workY, "%s %d", guns, uc.d_nGuns);

      workX += (spaceX - ((2 * d_dbX) / d_baseX));
      drawAttributeGraph(dib, InGameText::get(IDS_UI_MORALE), //s_strings.get(UnitInfoString::Morale),
         workX, workY, barOffsetX, barCX, barCY, cp->morale(), False); //(limitedInfo && cpi->getInfoQuality() < CommandPosition::FullInfo));

      cpiChild = cpiChild->sister();

      y += itemSpacing;

      if((y + itemSpacing) >= dr.bottom)
        break;
    }

    if(oldFont)
      dib->setFont(oldFont);
  }
}



/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.2  2001/06/18 08:25:44  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
