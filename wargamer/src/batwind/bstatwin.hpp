/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef BSTATWIN_HPP
#define BSTATWIN_HPP

/*---------------------------------------------------------------
 *  Nation Status window. Includes:
 *    1. morale-level for each major nationality,
 *    2. each sides victory point level
 */

#include "batdata.hpp"
#include "wind.hpp"

class BV_Wind;
class BattleWindowsInterface;
class PixelPoint;

class BattleVictory_Int : public Window
{
	 BV_Wind* d_bvWind;
  public:
	 BattleVictory_Int(HWND hParent, BattleWindowsInterface* bw, RCPBattleData batData);
	 ~BattleVictory_Int();

	 void position(const PixelPoint& p);
	 void show(bool visible);
	 void update();
	 int height() const;

     void enable(bool enabled);
     bool isVisible() const;
     bool isEnabled() const;

     HWND getHWND() const;
	 // void suspend(bool visible);
};

/*------------------------------------------------------------
 *  Game Status window. (what used to be StatusWindow in statwind.hpp/cpp) Includes:
 *     1. Message Section
 *     2. Hint-Line Section
 *     3. Game Clock
 */

class BGS_Wind;
class BatTimeControl;

class BGameStatus_Int : public Window
{
	 BGS_Wind* d_gsWind;
  public:
	 BGameStatus_Int(HWND hParent, BattleWindowsInterface* bw, RCPBattleData batData,
		 BatTimeControl& tc);
	 ~BGameStatus_Int();

	 void position(const PixelPoint& p);
	 void show(bool visible);
	 void updateMessageWindow();
	 void updateClock();
	 int height() const;

     void enable(bool enabled);
     bool isVisible() const;
     bool isEnabled() const;
     HWND getHWND() const;
};

#endif
