/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
#include "stdinc.hpp"
#include "bfacectl.hpp"
#include "batres.h"
#include "scenario.hpp"
#include "palette.hpp"
//#include "batarmy.hpp"
#include "app.hpp"
#include "scrnbase.hpp"
#include "scn_img.hpp"
#include "fonts.hpp"
#include "dib.hpp"
#include "dib_util.hpp"
#include "fillwind.hpp"
// #include "generic.hpp"
//#include "grtypes.hpp"
#include "imglib.hpp"
#include "scn_res.h"
#include "wmisc.hpp"
//#include "bobutil.hpp"
//#include "hexdata.hpp"
//#include "moveutil.hpp"
//namespace BattleWindows_Internal
//{


/*----------------------------------------------------------
 * Battle Clock Window
 *
 */

class BFacing_Imp : public WindowBaseND {
    HWND d_parent;
    HexPosition::Facing d_face;
    const int d_cx;
    const int d_cy;

    const int d_id;
    DrawDIBDC* d_dib;

    enum LocalFace {
      LF_NEP,
      LF_North,
      LF_NWP,
      LF_SWP,
      LF_South,
      LF_SEP,

      LF_HowMany,
      LF_Undefined
    } d_mouseOver;

    RECT d_rects[LF_HowMany];

  public:

    BFacing_Imp(HWND parent, int id);
    ~BFacing_Imp()
    {
        selfDestruct();
      if(d_dib)
        delete d_dib;
    }

    void position(const PixelPoint& p);
    void initFace(HexPosition::Facing f)
    {
      d_face = f;
      drawDib();
    }
    HexPosition::Facing currentFace() const { return d_face; }
    int width() const { return d_cx; }
    int height() const { return d_cy; }

  private:
    LRESULT procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
    void onCommand(HWND hwnd, int id, HWND hwndCtl, UINT codeNotify);
    void onPaint(HWND hwnd);
    BOOL onCreate(HWND hWnd, CREATESTRUCT* lpCreateStruct);
    BOOL onEraseBk(HWND hwnd, HDC hdc);
    void onDestroy(HWND hWnd) {}
    UINT onNCHitTest(HWND hwnd, int x, int y);
    void onMouseMove(HWND hwnd, int x, int y, UINT keyFlags);
    void onLButtonDown(HWND hwnd, BOOL fDoubleClick, int x, int y, UINT keyFlags);
//    void onLButtonUp(HWND hwnd, int x, int y, UINT keyFlags);

    void drawDib();
};

BFacing_Imp::BFacing_Imp(HWND parent, int id) :
  d_parent(parent),
  d_face(HexPosition::Facing_Default),
  d_cx((ScreenBase::dbX() * 58) / ScreenBase::baseX()),
  d_cy((ScreenBase::dbY() * 57) / ScreenBase::baseY()),
  d_id(id),
  d_dib(new DrawDIBDC(d_cx, d_cy)),
  d_mouseOver(LF_Undefined)
{
  ASSERT(d_dib);
  HWND hWnd = createWindow(
      0,
      // GenericNoBackClass::className(), 
        NULL,
      WS_CHILD,
      0, 0, d_cx, d_cy,
      parent, (HMENU)id       // , APP::instance()
  );
}

LRESULT BFacing_Imp::procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
  switch(msg)
  {
    HANDLE_MSG(hWnd, WM_CREATE,  onCreate);
    HANDLE_MSG(hWnd, WM_ERASEBKGND, onEraseBk);
    HANDLE_MSG(hWnd, WM_PAINT, onPaint);
    HANDLE_MSG(hWnd, WM_MOUSEMOVE, onMouseMove);
    HANDLE_MSG(hWnd, WM_LBUTTONDOWN, onLButtonDown);
    default:
      return defProc(hWnd, msg, wParam, lParam);
  }
}

void BFacing_Imp::onMouseMove(HWND hwnd, int x, int y, UINT keyFlags)
{
  bool found = False;
  bool redraw = False;
  for(int i = 0; i < LF_HowMany; i++)
  {
    POINT p;
    p.x = x;
    p.y = y;

    if(PtInRect(&d_rects[i], p))
    {
      found = True;
      if(i != d_mouseOver)
      {
        redraw = True;
        d_mouseOver = static_cast<LocalFace>(i);
      }

      break;
    }
  }

  if(!found && d_mouseOver != LF_Undefined)
  {
    redraw = True;
    d_mouseOver = LF_Undefined;
  }

  if(redraw)
  {
    drawDib();
    InvalidateRect(hwnd, NULL, FALSE);
    UpdateWindow(hwnd);
  }

}

void BFacing_Imp::onLButtonDown(HWND hwnd, BOOL fDoubleClick, int x, int y, UINT keyFlags)
{
  if(d_mouseOver != LF_Undefined)
  {
    HexPosition::Facing face = (d_mouseOver == LF_NEP)   ? HexPosition::NorthEastPoint :
                               (d_mouseOver == LF_North) ? HexPosition::North :
                               (d_mouseOver == LF_NWP)   ? HexPosition::NorthWestPoint :
                               (d_mouseOver == LF_SWP)   ? HexPosition::SouthWestPoint :
                               (d_mouseOver == LF_South) ? HexPosition::South :
                               (d_mouseOver == LF_SEP)   ? HexPosition::SouthEastPoint : HexPosition::Facing_Undefined;

    if(face != d_face)
    {
      d_face = face;
      ASSERT(d_face != HexPosition::Facing_Undefined);
      SendMessage(d_parent, WM_COMMAND, MAKEWPARAM(d_id, 0), reinterpret_cast<LPARAM>(hwnd));

      drawDib();
      InvalidateRect(hwnd, NULL, FALSE);
      UpdateWindow(hwnd);
    }
  }
}

BOOL BFacing_Imp::onCreate(HWND hWnd, CREATESTRUCT* lpCreateStruct)
{
  d_dib->setBkMode(TRANSPARENT);

  // set up quadrants for setting facing
  const int qSectionCX = (40 * ScreenBase::dbX()) / ScreenBase::baseX();
  const int qSectionCY = (40 * ScreenBase::dbX()) / ScreenBase::baseX();
  const int qSectionX  = (d_cx - qSectionCX) / 2;
  const int qSectionY  = (d_cy - qSectionCY) / 2;

  const int nCol = 3;
  const int nRow = 2;
  const int rectW = qSectionCX / nCol;
  const int rectH = qSectionCY / nRow;

  int i = 0;
  int start = nCol - 1;
  int stop = -1;
  int inc = -1;
  for(int r = 0; r < nRow; r++)
  {
    for(int c = start; c != stop; c += inc, i++)
    {
      int x = qSectionX + (c * rectW);
      int y = qSectionY + (r * rectH);
      SetRect(&d_rects[i], x, y, x + rectW, y + rectH);
    }

    start = 0;
    stop = nCol;
    inc = 1;
  }

  return TRUE;
}

void BFacing_Imp::onPaint(HWND hwnd)
{
  PAINTSTRUCT ps;
  HDC hdc = BeginPaint(hwnd, &ps);

  HPALETTE oldPal = SelectPalette(hdc, Palette::get(), FALSE);
  RealizePalette(hdc);

  if(d_dib)
    BitBlt(hdc, 0, 0, d_cx, d_cy, d_dib->getDC(), 0, 0, SRCCOPY);

  SelectPalette(hdc, oldPal, FALSE);

  EndPaint(hwnd, &ps);
}

struct TextPos {
   int d_x;
   int d_y;
   const char* d_text;
};

void BFacing_Imp::drawDib()
{
  ASSERT(d_dib);

  // fill background
  const DrawDIB* fillDib = scenario->getScenarioBkDIB(Scenario::BackPatterns::CherryWoodBackground);
  ASSERT(fillDib);

  d_dib->rect(0, 0, d_cx, d_cy, fillDib);

  const int dbX = ScreenBase::dbX();
  const int dbY = ScreenBase::dbY();
  const int baseX = ScreenBase::baseX();
  const int baseY = ScreenBase::baseY();
  const int graphCX = (dbX * 35) / baseX;
  const int graphCY = (dbY * 35) / baseY;

  const int graphX = (d_cx - graphCX) / 2;
  const int graphY = (d_cy - graphCY) / 2;

  // draw indented area
  d_dib->darkenRectangle(0, 0, d_cx, 2, 40);
  d_dib->darkenRectangle(0, 0, 2, d_cy, 40);
  d_dib->lightenRectangle(d_cx - 2, 0, 2, d_cy, 40);
  d_dib->lightenRectangle(0, d_cy - 2,  d_cx, 2, 40);

  d_dib->lightenRectangle(2, 2, d_cx - 4, d_cy - 4, 10);

  // draw image
  // first convert face to local enum
  LocalFace lface = (d_face == HexPosition::NorthEastPoint) ? LF_NEP :
                    (d_face == HexPosition::North)          ? LF_North :
                    (d_face == HexPosition::NorthWestPoint) ? LF_NWP :
                    (d_face == HexPosition::SouthWestPoint) ? LF_SWP :
                    (d_face == HexPosition::South)          ? LF_South :
                    (d_face == HexPosition::SouthEastPoint) ? LF_SEP : LF_Undefined;

  ASSERT(lface != LF_Undefined);

  const ImageLibrary* il = ScenarioImageLibrary::get(ScenarioImageLibrary::B_FaceImages);
  il->stretchBlit(d_dib, static_cast<UWORD>(lface), graphX, graphY, graphCX, graphCY);

  // now put direction abrevitions
  static const TextPos s_posData[LF_HowMany] = {
     { 45, 10, "NE" },
     { 28, 2,  "N"  },
     { 2,  10, "NW" },
     { 2,  35, "SW" },
     { 28, 47, "S"  },
     { 45, 35, "SE" }
  };

  LogFont lf;
  lf.height((7 * dbY) / baseY);
  lf.weight(FW_MEDIUM);
  lf.face(scenario->fontName(Font_Decorative));

  Font font;
  font.set(lf);
  HFONT oldFont = d_dib->setFont(font);
  COLORREF noSelColor = scenario->getColour("MenuText");
  COLORREF selColor = scenario->getColour("MenuHilite");
  COLORREF oldColor = d_dib->setTextColor(noSelColor);

  for(int i = 0; i < LF_HowMany; i++)
  {
    d_dib->setTextColor((i == lface || i == d_mouseOver) ? selColor : noSelColor);
    const TextPos* pos = &s_posData[i];
    wTextOut(d_dib->getDC(), (pos->d_x * dbX) / baseX, (pos->d_y * dbY) / baseY, pos->d_text);
  }

  d_dib->setFont(oldFont);
  d_dib->setTextColor(oldColor);
}

BOOL BFacing_Imp::onEraseBk(HWND hwnd, HDC hdc)
{
  return True;
}

void BFacing_Imp::position(const PixelPoint& p)
{
  SetWindowPos(getHWND(), HWND_TOP, p.getX(), p.getY(), 0, 0, SWP_NOSIZE | SWP_SHOWWINDOW);
}


/*---------------------------------------------------------
 * Client access
 */


BFacing_Ctl::BFacing_Ctl(HWND parent, int id) :
  d_wind(new BFacing_Imp(parent, id))
{
  ASSERT(d_wind);
}

BFacing_Ctl::~BFacing_Ctl()
{
  destroy();
}

void BFacing_Ctl::position(const PixelPoint& p)
{
  ASSERT(d_wind);
  d_wind->position(p);
}

void BFacing_Ctl::initFace(HexPosition::Facing f)
{
  ASSERT(d_wind);
  d_wind->initFace(f);
}

HexPosition::Facing BFacing_Ctl::currentFace() const
{
  ASSERT(d_wind);
  return d_wind->currentFace();
}

int BFacing_Ctl::width() const
{
  ASSERT(d_wind);
  return d_wind->width();
}

int BFacing_Ctl::height() const
{
  ASSERT(d_wind);
  return d_wind->height();
}

void BFacing_Ctl::destroy()
{
  if(d_wind)
  {
    DestroyWindow(d_wind->getHWND());
    d_wind = 0;
  }
}

void BFacing_Ctl::hide()
{
  if(d_wind)
  {
    ShowWindow(d_wind->getHWND(), SW_HIDE);
  }
}

HWND BFacing_Ctl::getHWND() const
{
  return (d_wind) ? d_wind->getHWND() : 0;
}

/*
 *----------------------------------------------------------------------
 * $Log$
 *----------------------------------------------------------------------
 */
