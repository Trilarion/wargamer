/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 * Battle Unit Dialog
 *
 * Similar to Campaign's UnitDialog
 *
 * It is a base class for some other dialogs, that provides support
 * for stacked units, and other common features
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "bu_dial.hpp"
#include "bud_info.hpp"    // User Dialog Info
#include "scenario.hpp"    // gamesup: Global Scenario Specific Data
#include "app.hpp"         // system: Application Global Generic Information
#include "grtypes.hpp"     // system: Graphic Types


namespace BattleWindows_Internal
{

#if 0
BattleUnitDial::BattleUnitDial(BUI_UnitInterface* owner) :
   d_shown(false),
   d_customDialog(scenario->getBorderColors()),
   d_owner(owner)
{
   ASSERT(owner != 0);
   setDeleteSelfMode(false);
}

BattleUnitDial::~BattleUnitDial()
{
   selfDestruct();   // remove Window if it is still up.
}

/*
 * Show dialog at given point
 */

void BattleUnitDial::show(const PixelPoint& p, const BUD_Info& info, const BattleOrder& order)
{
   ASSERT(!d_shown);

   d_order = order;

   if(getHWND() == NULL)
   {
      onCreate(APP::getMainHWND());
      ASSERT(getHWND() != NULL);
      d_customDialog.setBkDib(scenario->getSideBkDIB(SIDE_Neutral));
      d_customDialog.setCaptionBkDib(scenario->getScenarioBkDIB(Scenario::BackPatterns::WoodBackground));
      d_customDialog.init(getHWND());
   }

   ASSERT(getHWND() != NULL);

   if(getHWND() != NULL)
   {
      onShow(info);
      SetWindowPos(getHWND(), HWND_TOP, p.getX(), p.getY(), 0, 0, SWP_NOSIZE);
      ShowWindow(getHWND(), SW_SHOW);

      d_shown = true;
   }
}

/*
 * Hide dialog box
 */

void BattleUnitDial::hide()
{
   // ASSERT(getHWND() != NULL);
   // ASSERT(d_shown);


   if(d_shown && (getHWND() != NULL))
   {
      ShowWindow(getHWND(), SW_HIDE);
      onHide();
   }
   d_shown = false;
}
#endif
};    // namespace BattleWindows_Internal


/*
 *----------------------------------------------------------------------
 * $Log$
 *----------------------------------------------------------------------
 */
