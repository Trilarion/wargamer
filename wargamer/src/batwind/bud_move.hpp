/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef BUD_MOVE_HPP
#define BUD_MOVE_HPP

#ifndef __cplusplus
#error bud_move.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *	Battle Unit Move Dialog
 *
 * Presented to player after dragging a unit
 *
 * Not using a private hidden implementation, as the only place that
 * uses this is bw_unit.cpp.  We may change our mind if this
 * is dependant on too many Header files, or we start adding
 * lots of private functions and data.
 *
 *----------------------------------------------------------------------
 */

#include "bu_dial.hpp"

namespace BattleWindows_Internal
{

class BattleMoveDialog : public BattleUnitDial
{
	public:
		BattleMoveDialog(BUI_UnitInterface* owner);
		~BattleMoveDialog();

	private:
		// Implement BattleUnitDial
		//
		// I leave the virtual keyword to remind that these implement
		// the virtual functions defined in BattleUnitDial
		//
		// The functions are private, because they are meant to be called
		// indirectly from BattleUnitDial's public functions.

		virtual void onCreate(HWND hParent);
			// Create actual window
		virtual void onShow(const BUD_Info& info);
			// Called from show() to initialise dialog box
		virtual void onHide();
			// Called from hide() to free up resources

		/*
		 * Windows Message Handlers
		 */

		virtual BOOL procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
			// Implements ModelessDialog
		BOOL onInitDialog(HWND hwnd, HWND hwndFocus, LPARAM lParam);
		void onCommand(HWND hwnd, int id, HWND hwndCtl, UINT codeNotify);
		void onDrawItem(HWND hwnd, const DRAWITEMSTRUCT* lpDrawItem);

};

};		// namespace BattleWindows_Internal

#endif /* BUD_MOVE_HPP */

