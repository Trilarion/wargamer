# Microsoft Developer Studio Generated NMAKE File, Based on batwind.dsp
!IF "$(CFG)" == ""
CFG=batwind - Win32 Editor Debug
!MESSAGE No configuration specified. Defaulting to batwind - Win32 Editor Debug.
!ENDIF 

!IF "$(CFG)" != "batwind - Win32 Release" && "$(CFG)" != "batwind - Win32 Debug" && "$(CFG)" != "batwind - Win32 Editor Debug" && "$(CFG)" != "batwind - Win32 Editor Release"
!MESSAGE Invalid configuration "$(CFG)" specified.
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "batwind.mak" CFG="batwind - Win32 Editor Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "batwind - Win32 Release" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE "batwind - Win32 Debug" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE "batwind - Win32 Editor Debug" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE "batwind - Win32 Editor Release" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE 
!ERROR An invalid configuration is specified.
!ENDIF 

!IF "$(OS)" == "Windows_NT"
NULL=
!ELSE 
NULL=nul
!ENDIF 

CPP=cl.exe
MTL=midl.exe
RSC=rc.exe

!IF  "$(CFG)" == "batwind - Win32 Release"

OUTDIR=.\..\..\exe
INTDIR=.\o\release
# Begin Custom Macros
OutDir=.\..\..\exe
# End Custom Macros

!IF "$(RECURSE)" == "0" 

ALL : "$(OUTDIR)\batwind.dll"

!ELSE 

ALL : "batlogic - Win32 Release" "gwind - Win32 Release" "ob - Win32 Release" "gamesup - Win32 Release" "batdata - Win32 Release" "system - Win32 Release" "batdisp - Win32 Release" "$(OUTDIR)\batwind.dll"

!ENDIF 

!IF "$(RECURSE)" == "1" 
CLEAN :"batdisp - Win32 ReleaseCLEAN" "system - Win32 ReleaseCLEAN" "batdata - Win32 ReleaseCLEAN" "gamesup - Win32 ReleaseCLEAN" "ob - Win32 ReleaseCLEAN" "gwind - Win32 ReleaseCLEAN" "batlogic - Win32 ReleaseCLEAN" 
!ELSE 
CLEAN :
!ENDIF 
	-@erase "$(INTDIR)\b_info.obj"
	-@erase "$(INTDIR)\battool.obj"
	-@erase "$(INTDIR)\batw_oob.obj"
	-@erase "$(INTDIR)\batwind.obj"
	-@erase "$(INTDIR)\bclock.obj"
	-@erase "$(INTDIR)\bmsgwin.obj"
	-@erase "$(INTDIR)\bstatwin.obj"
	-@erase "$(INTDIR)\btoolbar.obj"
	-@erase "$(INTDIR)\btw_info.obj"
	-@erase "$(INTDIR)\bu_dial.obj"
	-@erase "$(INTDIR)\bu_menu.obj"
	-@erase "$(INTDIR)\bud_info.obj"
	-@erase "$(INTDIR)\bud_move.obj"
	-@erase "$(INTDIR)\bud_ordr.obj"
	-@erase "$(INTDIR)\bunitinf.obj"
	-@erase "$(INTDIR)\bw_tiny.obj"
	-@erase "$(INTDIR)\bw_unit.obj"
	-@erase "$(INTDIR)\d_place.obj"
	-@erase "$(INTDIR)\d_plcsde.obj"
	-@erase "$(INTDIR)\d_select.obj"
	-@erase "$(INTDIR)\d_selsde.obj"
	-@erase "$(INTDIR)\vc60.idb"
	-@erase "$(INTDIR)\vc60.pdb"
	-@erase "$(OUTDIR)\batwind.dll"
	-@erase "$(OUTDIR)\batwind.exp"
	-@erase "$(OUTDIR)\batwind.lib"
	-@erase "$(OUTDIR)\batwind.pdb"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

"$(INTDIR)" :
    if not exist "$(INTDIR)/$(NULL)" mkdir "$(INTDIR)"

CPP_PROJ=/nologo /MD /W3 /Gi /GR /GX /Zi /O2 /I "..\h" /I "..\..\res" /I "..\system" /I "..\gamesup" /I "..\ob" /I "..\batdata" /I "..\batdisp" /I "..\gwind" /I "..\batlogic" /D "NDEBUG" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "EXPORT_BATWIND_DLL" /Fp"$(INTDIR)\batwind.pch" /YX"stdinc.hpp" /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /c 
MTL_PROJ=/nologo /D "NDEBUG" /mktyplib203 /win32 
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\batwind.bsc" 
BSC32_SBRS= \
	
LINK32=link.exe
LINK32_FLAGS=user32.lib gdi32.lib comctl32.lib shell32.lib /nologo /dll /incremental:no /pdb:"$(OUTDIR)\batwind.pdb" /debug /machine:I386 /out:"$(OUTDIR)\batwind.dll" /implib:"$(OUTDIR)\batwind.lib" 
LINK32_OBJS= \
	"$(INTDIR)\b_info.obj" \
	"$(INTDIR)\battool.obj" \
	"$(INTDIR)\batw_oob.obj" \
	"$(INTDIR)\batwind.obj" \
	"$(INTDIR)\bclock.obj" \
	"$(INTDIR)\bmsgwin.obj" \
	"$(INTDIR)\bstatwin.obj" \
	"$(INTDIR)\btoolbar.obj" \
	"$(INTDIR)\btw_info.obj" \
	"$(INTDIR)\bu_dial.obj" \
	"$(INTDIR)\bu_menu.obj" \
	"$(INTDIR)\bud_info.obj" \
	"$(INTDIR)\bud_move.obj" \
	"$(INTDIR)\bud_ordr.obj" \
	"$(INTDIR)\bunitinf.obj" \
	"$(INTDIR)\bw_tiny.obj" \
	"$(INTDIR)\bw_unit.obj" \
	"$(INTDIR)\d_place.obj" \
	"$(INTDIR)\d_plcsde.obj" \
	"$(INTDIR)\d_select.obj" \
	"$(INTDIR)\d_selsde.obj" \
	"$(OUTDIR)\batdisp.lib" \
	"$(OUTDIR)\system.lib" \
	"$(OUTDIR)\batdata.lib" \
	"$(OUTDIR)\gamesup.lib" \
	"$(OUTDIR)\ob.lib" \
	"$(OUTDIR)\gwind.lib" \
	"$(OUTDIR)\batlogic.lib"

"$(OUTDIR)\batwind.dll" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

!ELSEIF  "$(CFG)" == "batwind - Win32 Debug"

OUTDIR=.\..\..\exe
INTDIR=.\o\debug
# Begin Custom Macros
OutDir=.\..\..\exe
# End Custom Macros

!IF "$(RECURSE)" == "0" 

ALL : "$(OUTDIR)\batwindDB.dll"

!ELSE 

ALL : "batlogic - Win32 Debug" "gwind - Win32 Debug" "ob - Win32 Debug" "gamesup - Win32 Debug" "batdata - Win32 Debug" "system - Win32 Debug" "batdisp - Win32 Debug" "$(OUTDIR)\batwindDB.dll"

!ENDIF 

!IF "$(RECURSE)" == "1" 
CLEAN :"batdisp - Win32 DebugCLEAN" "system - Win32 DebugCLEAN" "batdata - Win32 DebugCLEAN" "gamesup - Win32 DebugCLEAN" "ob - Win32 DebugCLEAN" "gwind - Win32 DebugCLEAN" "batlogic - Win32 DebugCLEAN" 
!ELSE 
CLEAN :
!ENDIF 
	-@erase "$(INTDIR)\b_info.obj"
	-@erase "$(INTDIR)\battool.obj"
	-@erase "$(INTDIR)\batw_oob.obj"
	-@erase "$(INTDIR)\batwind.obj"
	-@erase "$(INTDIR)\bclock.obj"
	-@erase "$(INTDIR)\bmsgwin.obj"
	-@erase "$(INTDIR)\bstatwin.obj"
	-@erase "$(INTDIR)\btoolbar.obj"
	-@erase "$(INTDIR)\btw_info.obj"
	-@erase "$(INTDIR)\bu_dial.obj"
	-@erase "$(INTDIR)\bu_menu.obj"
	-@erase "$(INTDIR)\bud_info.obj"
	-@erase "$(INTDIR)\bud_move.obj"
	-@erase "$(INTDIR)\bud_ordr.obj"
	-@erase "$(INTDIR)\bunitinf.obj"
	-@erase "$(INTDIR)\bw_tiny.obj"
	-@erase "$(INTDIR)\bw_unit.obj"
	-@erase "$(INTDIR)\d_place.obj"
	-@erase "$(INTDIR)\d_plcsde.obj"
	-@erase "$(INTDIR)\d_select.obj"
	-@erase "$(INTDIR)\d_selsde.obj"
	-@erase "$(INTDIR)\vc60.idb"
	-@erase "$(INTDIR)\vc60.pdb"
	-@erase "$(OUTDIR)\batwindDB.dll"
	-@erase "$(OUTDIR)\batwindDB.exp"
	-@erase "$(OUTDIR)\batwindDB.ilk"
	-@erase "$(OUTDIR)\batwindDB.lib"
	-@erase "$(OUTDIR)\batwindDB.pdb"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

"$(INTDIR)" :
    if not exist "$(INTDIR)/$(NULL)" mkdir "$(INTDIR)"

CPP_PROJ=/nologo /MDd /W3 /Gm /Gi /GR /GX /ZI /Od /I "..\h" /I "..\..\res" /I "..\system" /I "..\gamesup" /I "..\ob" /I "..\batdata" /I "..\batdisp" /I "..\gwind" /I "..\batlogic" /D "_DEBUG" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "EXPORT_BATWIND_DLL" /Fp"$(INTDIR)\batwind.pch" /YX"stdinc.hpp" /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /GZ /c 
MTL_PROJ=/nologo /D "_DEBUG" /mktyplib203 /win32 
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\batwind.bsc" 
BSC32_SBRS= \
	
LINK32=link.exe
LINK32_FLAGS=user32.lib gdi32.lib comctl32.lib shell32.lib /nologo /dll /incremental:yes /pdb:"$(OUTDIR)\batwindDB.pdb" /debug /machine:I386 /out:"$(OUTDIR)\batwindDB.dll" /implib:"$(OUTDIR)\batwindDB.lib" /pdbtype:sept 
LINK32_OBJS= \
	"$(INTDIR)\b_info.obj" \
	"$(INTDIR)\battool.obj" \
	"$(INTDIR)\batw_oob.obj" \
	"$(INTDIR)\batwind.obj" \
	"$(INTDIR)\bclock.obj" \
	"$(INTDIR)\bmsgwin.obj" \
	"$(INTDIR)\bstatwin.obj" \
	"$(INTDIR)\btoolbar.obj" \
	"$(INTDIR)\btw_info.obj" \
	"$(INTDIR)\bu_dial.obj" \
	"$(INTDIR)\bu_menu.obj" \
	"$(INTDIR)\bud_info.obj" \
	"$(INTDIR)\bud_move.obj" \
	"$(INTDIR)\bud_ordr.obj" \
	"$(INTDIR)\bunitinf.obj" \
	"$(INTDIR)\bw_tiny.obj" \
	"$(INTDIR)\bw_unit.obj" \
	"$(INTDIR)\d_place.obj" \
	"$(INTDIR)\d_plcsde.obj" \
	"$(INTDIR)\d_select.obj" \
	"$(INTDIR)\d_selsde.obj" \
	"$(OUTDIR)\batdispDB.lib" \
	"$(OUTDIR)\systemDB.lib" \
	"$(OUTDIR)\batdataDB.lib" \
	"$(OUTDIR)\gamesupDB.lib" \
	"$(OUTDIR)\obDB.lib" \
	"$(OUTDIR)\gwindDB.lib" \
	"$(OUTDIR)\batlogicDB.lib"

"$(OUTDIR)\batwindDB.dll" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

!ELSEIF  "$(CFG)" == "batwind - Win32 Editor Debug"

OUTDIR=.\..\..\exe
INTDIR=.\o\Editor\Debug
# Begin Custom Macros
OutDir=.\..\..\exe
# End Custom Macros

!IF "$(RECURSE)" == "0" 

ALL : "$(OUTDIR)\batwindEDDB.dll"

!ELSE 

ALL : "gwind - Win32 Editor Debug" "ob - Win32 Editor Debug" "gamesup - Win32 Editor Debug" "batdata - Win32 Editor Debug" "system - Win32 Editor Debug" "$(OUTDIR)\batwindEDDB.dll"

!ENDIF 

!IF "$(RECURSE)" == "1" 
CLEAN :"system - Win32 Editor DebugCLEAN" "batdata - Win32 Editor DebugCLEAN" "gamesup - Win32 Editor DebugCLEAN" "ob - Win32 Editor DebugCLEAN" "gwind - Win32 Editor DebugCLEAN" 
!ELSE 
CLEAN :
!ENDIF 
	-@erase "$(INTDIR)\b_info.obj"
	-@erase "$(INTDIR)\battool.obj"
	-@erase "$(INTDIR)\batw_oob.obj"
	-@erase "$(INTDIR)\batwind.obj"
	-@erase "$(INTDIR)\bclock.obj"
	-@erase "$(INTDIR)\bmsgwin.obj"
	-@erase "$(INTDIR)\bstatwin.obj"
	-@erase "$(INTDIR)\btoolbar.obj"
	-@erase "$(INTDIR)\btw_info.obj"
	-@erase "$(INTDIR)\bu_dial.obj"
	-@erase "$(INTDIR)\bu_menu.obj"
	-@erase "$(INTDIR)\bud_info.obj"
	-@erase "$(INTDIR)\bud_move.obj"
	-@erase "$(INTDIR)\bud_ordr.obj"
	-@erase "$(INTDIR)\bunitinf.obj"
	-@erase "$(INTDIR)\bw_tiny.obj"
	-@erase "$(INTDIR)\bw_unit.obj"
	-@erase "$(INTDIR)\d_place.obj"
	-@erase "$(INTDIR)\d_plcsde.obj"
	-@erase "$(INTDIR)\d_select.obj"
	-@erase "$(INTDIR)\d_selsde.obj"
	-@erase "$(INTDIR)\vc60.idb"
	-@erase "$(INTDIR)\vc60.pdb"
	-@erase "$(OUTDIR)\batwindEDDB.dll"
	-@erase "$(OUTDIR)\batwindEDDB.exp"
	-@erase "$(OUTDIR)\batwindEDDB.ilk"
	-@erase "$(OUTDIR)\batwindEDDB.lib"
	-@erase "$(OUTDIR)\batwindEDDB.pdb"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

"$(INTDIR)" :
    if not exist "$(INTDIR)/$(NULL)" mkdir "$(INTDIR)"

CPP_PROJ=/nologo /MDd /W3 /Gm /Gi /GR /GX /ZI /Od /I "..\h" /I "..\..\res" /I "..\system" /I "..\gamesup" /I "..\ob" /I "..\batdata" /I "..\batdisp" /I "..\gwind" /I "..\batlogic" /D "_USRDLL" /D "EXPORT_BATWIND_DLL" /D "_DEBUG" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /D "EDITOR" /Fp"$(INTDIR)\batwind.pch" /YX"stdinc.hpp" /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /GZ /c 
MTL_PROJ=/nologo /D "_DEBUG" /mktyplib203 /win32 
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\batwind.bsc" 
BSC32_SBRS= \
	
LINK32=link.exe
LINK32_FLAGS=user32.lib gdi32.lib comctl32.lib shell32.lib /nologo /dll /incremental:yes /pdb:"$(OUTDIR)\batwindEDDB.pdb" /debug /machine:I386 /out:"$(OUTDIR)\batwindEDDB.dll" /implib:"$(OUTDIR)\batwindEDDB.lib" /pdbtype:sept 
LINK32_OBJS= \
	"$(INTDIR)\b_info.obj" \
	"$(INTDIR)\battool.obj" \
	"$(INTDIR)\batw_oob.obj" \
	"$(INTDIR)\batwind.obj" \
	"$(INTDIR)\bclock.obj" \
	"$(INTDIR)\bmsgwin.obj" \
	"$(INTDIR)\bstatwin.obj" \
	"$(INTDIR)\btoolbar.obj" \
	"$(INTDIR)\btw_info.obj" \
	"$(INTDIR)\bu_dial.obj" \
	"$(INTDIR)\bu_menu.obj" \
	"$(INTDIR)\bud_info.obj" \
	"$(INTDIR)\bud_move.obj" \
	"$(INTDIR)\bud_ordr.obj" \
	"$(INTDIR)\bunitinf.obj" \
	"$(INTDIR)\bw_tiny.obj" \
	"$(INTDIR)\bw_unit.obj" \
	"$(INTDIR)\d_place.obj" \
	"$(INTDIR)\d_plcsde.obj" \
	"$(INTDIR)\d_select.obj" \
	"$(INTDIR)\d_selsde.obj" \
	"$(OUTDIR)\systemDB.lib" \
	"$(OUTDIR)\batdataEDDB.lib" \
	"$(OUTDIR)\gamesupDB.lib" \
	"$(OUTDIR)\obEDDB.lib" \
	"$(OUTDIR)\gwindEDDB.lib"

"$(OUTDIR)\batwindEDDB.dll" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

!ELSEIF  "$(CFG)" == "batwind - Win32 Editor Release"

OUTDIR=.\..\..\exe
INTDIR=.\o\Editor\Release
# Begin Custom Macros
OutDir=.\..\..\exe
# End Custom Macros

!IF "$(RECURSE)" == "0" 

ALL : "$(OUTDIR)\batwindED.dll"

!ELSE 

ALL : "gwind - Win32 Editor Release" "ob - Win32 Editor Release" "gamesup - Win32 Editor Release" "batdata - Win32 Editor Release" "system - Win32 Editor Release" "$(OUTDIR)\batwindED.dll"

!ENDIF 

!IF "$(RECURSE)" == "1" 
CLEAN :"system - Win32 Editor ReleaseCLEAN" "batdata - Win32 Editor ReleaseCLEAN" "gamesup - Win32 Editor ReleaseCLEAN" "ob - Win32 Editor ReleaseCLEAN" "gwind - Win32 Editor ReleaseCLEAN" 
!ELSE 
CLEAN :
!ENDIF 
	-@erase "$(INTDIR)\b_info.obj"
	-@erase "$(INTDIR)\battool.obj"
	-@erase "$(INTDIR)\batw_oob.obj"
	-@erase "$(INTDIR)\batwind.obj"
	-@erase "$(INTDIR)\bclock.obj"
	-@erase "$(INTDIR)\bmsgwin.obj"
	-@erase "$(INTDIR)\bstatwin.obj"
	-@erase "$(INTDIR)\btoolbar.obj"
	-@erase "$(INTDIR)\btw_info.obj"
	-@erase "$(INTDIR)\bu_dial.obj"
	-@erase "$(INTDIR)\bu_menu.obj"
	-@erase "$(INTDIR)\bud_info.obj"
	-@erase "$(INTDIR)\bud_move.obj"
	-@erase "$(INTDIR)\bud_ordr.obj"
	-@erase "$(INTDIR)\bunitinf.obj"
	-@erase "$(INTDIR)\bw_tiny.obj"
	-@erase "$(INTDIR)\bw_unit.obj"
	-@erase "$(INTDIR)\d_place.obj"
	-@erase "$(INTDIR)\d_plcsde.obj"
	-@erase "$(INTDIR)\d_select.obj"
	-@erase "$(INTDIR)\d_selsde.obj"
	-@erase "$(INTDIR)\vc60.idb"
	-@erase "$(OUTDIR)\batwindED.dll"
	-@erase "$(OUTDIR)\batwindED.exp"
	-@erase "$(OUTDIR)\batwindED.lib"
	-@erase "$(OUTDIR)\batwindED.pdb"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

"$(INTDIR)" :
    if not exist "$(INTDIR)/$(NULL)" mkdir "$(INTDIR)"

CPP_PROJ=/nologo /MD /W3 /Gi /GR /GX /Zd /O2 /I "..\h" /I "..\..\res" /I "..\system" /I "..\gamesup" /I "..\ob" /I "..\batdata" /I "..\batdisp" /I "..\gwind" /I "..\batlogic" /D "NDEBUG" /D "_USRDLL" /D "EXPORT_BATWIND_DLL" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /D "EDITOR" /Fp"$(INTDIR)\batwind.pch" /YX"stdinc.hpp" /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /c 
MTL_PROJ=/nologo /D "NDEBUG" /mktyplib203 /win32 
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\batwind.bsc" 
BSC32_SBRS= \
	
LINK32=link.exe
LINK32_FLAGS=user32.lib gdi32.lib comctl32.lib shell32.lib /nologo /dll /incremental:no /pdb:"$(OUTDIR)\batwindED.pdb" /debug /machine:I386 /out:"$(OUTDIR)\batwindED.dll" /implib:"$(OUTDIR)\batwindED.lib" 
LINK32_OBJS= \
	"$(INTDIR)\b_info.obj" \
	"$(INTDIR)\battool.obj" \
	"$(INTDIR)\batw_oob.obj" \
	"$(INTDIR)\batwind.obj" \
	"$(INTDIR)\bclock.obj" \
	"$(INTDIR)\bmsgwin.obj" \
	"$(INTDIR)\bstatwin.obj" \
	"$(INTDIR)\btoolbar.obj" \
	"$(INTDIR)\btw_info.obj" \
	"$(INTDIR)\bu_dial.obj" \
	"$(INTDIR)\bu_menu.obj" \
	"$(INTDIR)\bud_info.obj" \
	"$(INTDIR)\bud_move.obj" \
	"$(INTDIR)\bud_ordr.obj" \
	"$(INTDIR)\bunitinf.obj" \
	"$(INTDIR)\bw_tiny.obj" \
	"$(INTDIR)\bw_unit.obj" \
	"$(INTDIR)\d_place.obj" \
	"$(INTDIR)\d_plcsde.obj" \
	"$(INTDIR)\d_select.obj" \
	"$(INTDIR)\d_selsde.obj" \
	"$(OUTDIR)\system.lib" \
	"$(OUTDIR)\batdataED.lib" \
	"$(OUTDIR)\gamesup.lib" \
	"$(OUTDIR)\obED.lib" \
	"$(OUTDIR)\gwindED.lib"

"$(OUTDIR)\batwindED.dll" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

!ENDIF 

.c{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.c{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<


!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("batwind.dep")
!INCLUDE "batwind.dep"
!ELSE 
!MESSAGE Warning: cannot find "batwind.dep"
!ENDIF 
!ENDIF 


!IF "$(CFG)" == "batwind - Win32 Release" || "$(CFG)" == "batwind - Win32 Debug" || "$(CFG)" == "batwind - Win32 Editor Debug" || "$(CFG)" == "batwind - Win32 Editor Release"
SOURCE=.\b_info.cpp

"$(INTDIR)\b_info.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\battool.cpp

"$(INTDIR)\battool.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\batw_oob.cpp

"$(INTDIR)\batw_oob.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\batwind.cpp

"$(INTDIR)\batwind.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\bclock.cpp

"$(INTDIR)\bclock.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\bmsgwin.cpp

"$(INTDIR)\bmsgwin.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\bstatwin.cpp

"$(INTDIR)\bstatwin.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\btoolbar.cpp

"$(INTDIR)\btoolbar.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\btw_info.cpp

"$(INTDIR)\btw_info.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\bu_dial.cpp

"$(INTDIR)\bu_dial.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\bu_menu.cpp

"$(INTDIR)\bu_menu.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\bud_info.cpp

"$(INTDIR)\bud_info.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\bud_move.cpp

"$(INTDIR)\bud_move.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\bud_ordr.cpp

"$(INTDIR)\bud_ordr.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\bunitinf.cpp

"$(INTDIR)\bunitinf.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\bw_tiny.cpp

"$(INTDIR)\bw_tiny.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\bw_unit.cpp

"$(INTDIR)\bw_unit.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\d_place.cpp

"$(INTDIR)\d_place.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\d_plcsde.cpp

"$(INTDIR)\d_plcsde.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\d_select.cpp

"$(INTDIR)\d_select.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\d_selsde.cpp

"$(INTDIR)\d_selsde.obj" : $(SOURCE) "$(INTDIR)"


!IF  "$(CFG)" == "batwind - Win32 Release"

"batdisp - Win32 Release" : 
   cd "\src\sf\wargamer\src\batdisp"
   $(MAKE) /$(MAKEFLAGS) /F .\batdisp.mak CFG="batdisp - Win32 Release" 
   cd "..\batwind"

"batdisp - Win32 ReleaseCLEAN" : 
   cd "\src\sf\wargamer\src\batdisp"
   $(MAKE) /$(MAKEFLAGS) /F .\batdisp.mak CFG="batdisp - Win32 Release" RECURSE=1 CLEAN 
   cd "..\batwind"

!ELSEIF  "$(CFG)" == "batwind - Win32 Debug"

"batdisp - Win32 Debug" : 
   cd "\src\sf\wargamer\src\batdisp"
   $(MAKE) /$(MAKEFLAGS) /F .\batdisp.mak CFG="batdisp - Win32 Debug" 
   cd "..\batwind"

"batdisp - Win32 DebugCLEAN" : 
   cd "\src\sf\wargamer\src\batdisp"
   $(MAKE) /$(MAKEFLAGS) /F .\batdisp.mak CFG="batdisp - Win32 Debug" RECURSE=1 CLEAN 
   cd "..\batwind"

!ELSEIF  "$(CFG)" == "batwind - Win32 Editor Debug"

!ELSEIF  "$(CFG)" == "batwind - Win32 Editor Release"

!ENDIF 

!IF  "$(CFG)" == "batwind - Win32 Release"

"system - Win32 Release" : 
   cd "\src\sf\wargamer\SRC\SYSTEM"
   $(MAKE) /$(MAKEFLAGS) /F .\system.mak CFG="system - Win32 Release" 
   cd "..\batwind"

"system - Win32 ReleaseCLEAN" : 
   cd "\src\sf\wargamer\SRC\SYSTEM"
   $(MAKE) /$(MAKEFLAGS) /F .\system.mak CFG="system - Win32 Release" RECURSE=1 CLEAN 
   cd "..\batwind"

!ELSEIF  "$(CFG)" == "batwind - Win32 Debug"

"system - Win32 Debug" : 
   cd "\src\sf\wargamer\SRC\SYSTEM"
   $(MAKE) /$(MAKEFLAGS) /F .\system.mak CFG="system - Win32 Debug" 
   cd "..\batwind"

"system - Win32 DebugCLEAN" : 
   cd "\src\sf\wargamer\SRC\SYSTEM"
   $(MAKE) /$(MAKEFLAGS) /F .\system.mak CFG="system - Win32 Debug" RECURSE=1 CLEAN 
   cd "..\batwind"

!ELSEIF  "$(CFG)" == "batwind - Win32 Editor Debug"

"system - Win32 Editor Debug" : 
   cd "\src\sf\wargamer\SRC\SYSTEM"
   $(MAKE) /$(MAKEFLAGS) /F .\system.mak CFG="system - Win32 Editor Debug" 
   cd "..\batwind"

"system - Win32 Editor DebugCLEAN" : 
   cd "\src\sf\wargamer\SRC\SYSTEM"
   $(MAKE) /$(MAKEFLAGS) /F .\system.mak CFG="system - Win32 Editor Debug" RECURSE=1 CLEAN 
   cd "..\batwind"

!ELSEIF  "$(CFG)" == "batwind - Win32 Editor Release"

"system - Win32 Editor Release" : 
   cd "\src\sf\wargamer\SRC\SYSTEM"
   $(MAKE) /$(MAKEFLAGS) /F .\system.mak CFG="system - Win32 Editor Release" 
   cd "..\batwind"

"system - Win32 Editor ReleaseCLEAN" : 
   cd "\src\sf\wargamer\SRC\SYSTEM"
   $(MAKE) /$(MAKEFLAGS) /F .\system.mak CFG="system - Win32 Editor Release" RECURSE=1 CLEAN 
   cd "..\batwind"

!ENDIF 

!IF  "$(CFG)" == "batwind - Win32 Release"

"batdata - Win32 Release" : 
   cd "\src\sf\wargamer\SRC\batdata"
   $(MAKE) /$(MAKEFLAGS) /F .\batdata.mak CFG="batdata - Win32 Release" 
   cd "..\batwind"

"batdata - Win32 ReleaseCLEAN" : 
   cd "\src\sf\wargamer\SRC\batdata"
   $(MAKE) /$(MAKEFLAGS) /F .\batdata.mak CFG="batdata - Win32 Release" RECURSE=1 CLEAN 
   cd "..\batwind"

!ELSEIF  "$(CFG)" == "batwind - Win32 Debug"

"batdata - Win32 Debug" : 
   cd "\src\sf\wargamer\SRC\batdata"
   $(MAKE) /$(MAKEFLAGS) /F .\batdata.mak CFG="batdata - Win32 Debug" 
   cd "..\batwind"

"batdata - Win32 DebugCLEAN" : 
   cd "\src\sf\wargamer\SRC\batdata"
   $(MAKE) /$(MAKEFLAGS) /F .\batdata.mak CFG="batdata - Win32 Debug" RECURSE=1 CLEAN 
   cd "..\batwind"

!ELSEIF  "$(CFG)" == "batwind - Win32 Editor Debug"

"batdata - Win32 Editor Debug" : 
   cd "\src\sf\wargamer\SRC\batdata"
   $(MAKE) /$(MAKEFLAGS) /F .\batdata.mak CFG="batdata - Win32 Editor Debug" 
   cd "..\batwind"

"batdata - Win32 Editor DebugCLEAN" : 
   cd "\src\sf\wargamer\SRC\batdata"
   $(MAKE) /$(MAKEFLAGS) /F .\batdata.mak CFG="batdata - Win32 Editor Debug" RECURSE=1 CLEAN 
   cd "..\batwind"

!ELSEIF  "$(CFG)" == "batwind - Win32 Editor Release"

"batdata - Win32 Editor Release" : 
   cd "\src\sf\wargamer\SRC\batdata"
   $(MAKE) /$(MAKEFLAGS) /F .\batdata.mak CFG="batdata - Win32 Editor Release" 
   cd "..\batwind"

"batdata - Win32 Editor ReleaseCLEAN" : 
   cd "\src\sf\wargamer\SRC\batdata"
   $(MAKE) /$(MAKEFLAGS) /F .\batdata.mak CFG="batdata - Win32 Editor Release" RECURSE=1 CLEAN 
   cd "..\batwind"

!ENDIF 

!IF  "$(CFG)" == "batwind - Win32 Release"

"gamesup - Win32 Release" : 
   cd "\src\sf\wargamer\SRC\GAMESUP"
   $(MAKE) /$(MAKEFLAGS) /F .\gamesup.mak CFG="gamesup - Win32 Release" 
   cd "..\batwind"

"gamesup - Win32 ReleaseCLEAN" : 
   cd "\src\sf\wargamer\SRC\GAMESUP"
   $(MAKE) /$(MAKEFLAGS) /F .\gamesup.mak CFG="gamesup - Win32 Release" RECURSE=1 CLEAN 
   cd "..\batwind"

!ELSEIF  "$(CFG)" == "batwind - Win32 Debug"

"gamesup - Win32 Debug" : 
   cd "\src\sf\wargamer\SRC\GAMESUP"
   $(MAKE) /$(MAKEFLAGS) /F .\gamesup.mak CFG="gamesup - Win32 Debug" 
   cd "..\batwind"

"gamesup - Win32 DebugCLEAN" : 
   cd "\src\sf\wargamer\SRC\GAMESUP"
   $(MAKE) /$(MAKEFLAGS) /F .\gamesup.mak CFG="gamesup - Win32 Debug" RECURSE=1 CLEAN 
   cd "..\batwind"

!ELSEIF  "$(CFG)" == "batwind - Win32 Editor Debug"

"gamesup - Win32 Editor Debug" : 
   cd "\src\sf\wargamer\SRC\GAMESUP"
   $(MAKE) /$(MAKEFLAGS) /F .\gamesup.mak CFG="gamesup - Win32 Editor Debug" 
   cd "..\batwind"

"gamesup - Win32 Editor DebugCLEAN" : 
   cd "\src\sf\wargamer\SRC\GAMESUP"
   $(MAKE) /$(MAKEFLAGS) /F .\gamesup.mak CFG="gamesup - Win32 Editor Debug" RECURSE=1 CLEAN 
   cd "..\batwind"

!ELSEIF  "$(CFG)" == "batwind - Win32 Editor Release"

"gamesup - Win32 Editor Release" : 
   cd "\src\sf\wargamer\SRC\GAMESUP"
   $(MAKE) /$(MAKEFLAGS) /F .\gamesup.mak CFG="gamesup - Win32 Editor Release" 
   cd "..\batwind"

"gamesup - Win32 Editor ReleaseCLEAN" : 
   cd "\src\sf\wargamer\SRC\GAMESUP"
   $(MAKE) /$(MAKEFLAGS) /F .\gamesup.mak CFG="gamesup - Win32 Editor Release" RECURSE=1 CLEAN 
   cd "..\batwind"

!ENDIF 

!IF  "$(CFG)" == "batwind - Win32 Release"

"ob - Win32 Release" : 
   cd "\src\sf\wargamer\SRC\OB"
   $(MAKE) /$(MAKEFLAGS) /F .\ob.mak CFG="ob - Win32 Release" 
   cd "..\batwind"

"ob - Win32 ReleaseCLEAN" : 
   cd "\src\sf\wargamer\SRC\OB"
   $(MAKE) /$(MAKEFLAGS) /F .\ob.mak CFG="ob - Win32 Release" RECURSE=1 CLEAN 
   cd "..\batwind"

!ELSEIF  "$(CFG)" == "batwind - Win32 Debug"

"ob - Win32 Debug" : 
   cd "\src\sf\wargamer\SRC\OB"
   $(MAKE) /$(MAKEFLAGS) /F .\ob.mak CFG="ob - Win32 Debug" 
   cd "..\batwind"

"ob - Win32 DebugCLEAN" : 
   cd "\src\sf\wargamer\SRC\OB"
   $(MAKE) /$(MAKEFLAGS) /F .\ob.mak CFG="ob - Win32 Debug" RECURSE=1 CLEAN 
   cd "..\batwind"

!ELSEIF  "$(CFG)" == "batwind - Win32 Editor Debug"

"ob - Win32 Editor Debug" : 
   cd "\src\sf\wargamer\SRC\OB"
   $(MAKE) /$(MAKEFLAGS) /F .\ob.mak CFG="ob - Win32 Editor Debug" 
   cd "..\batwind"

"ob - Win32 Editor DebugCLEAN" : 
   cd "\src\sf\wargamer\SRC\OB"
   $(MAKE) /$(MAKEFLAGS) /F .\ob.mak CFG="ob - Win32 Editor Debug" RECURSE=1 CLEAN 
   cd "..\batwind"

!ELSEIF  "$(CFG)" == "batwind - Win32 Editor Release"

"ob - Win32 Editor Release" : 
   cd "\src\sf\wargamer\SRC\OB"
   $(MAKE) /$(MAKEFLAGS) /F .\ob.mak CFG="ob - Win32 Editor Release" 
   cd "..\batwind"

"ob - Win32 Editor ReleaseCLEAN" : 
   cd "\src\sf\wargamer\SRC\OB"
   $(MAKE) /$(MAKEFLAGS) /F .\ob.mak CFG="ob - Win32 Editor Release" RECURSE=1 CLEAN 
   cd "..\batwind"

!ENDIF 

!IF  "$(CFG)" == "batwind - Win32 Release"

"gwind - Win32 Release" : 
   cd "\src\sf\wargamer\src\gwind"
   $(MAKE) /$(MAKEFLAGS) /F .\gwind.mak CFG="gwind - Win32 Release" 
   cd "..\batwind"

"gwind - Win32 ReleaseCLEAN" : 
   cd "\src\sf\wargamer\src\gwind"
   $(MAKE) /$(MAKEFLAGS) /F .\gwind.mak CFG="gwind - Win32 Release" RECURSE=1 CLEAN 
   cd "..\batwind"

!ELSEIF  "$(CFG)" == "batwind - Win32 Debug"

"gwind - Win32 Debug" : 
   cd "\src\sf\wargamer\src\gwind"
   $(MAKE) /$(MAKEFLAGS) /F .\gwind.mak CFG="gwind - Win32 Debug" 
   cd "..\batwind"

"gwind - Win32 DebugCLEAN" : 
   cd "\src\sf\wargamer\src\gwind"
   $(MAKE) /$(MAKEFLAGS) /F .\gwind.mak CFG="gwind - Win32 Debug" RECURSE=1 CLEAN 
   cd "..\batwind"

!ELSEIF  "$(CFG)" == "batwind - Win32 Editor Debug"

"gwind - Win32 Editor Debug" : 
   cd "\src\sf\wargamer\src\gwind"
   $(MAKE) /$(MAKEFLAGS) /F .\gwind.mak CFG="gwind - Win32 Editor Debug" 
   cd "..\batwind"

"gwind - Win32 Editor DebugCLEAN" : 
   cd "\src\sf\wargamer\src\gwind"
   $(MAKE) /$(MAKEFLAGS) /F .\gwind.mak CFG="gwind - Win32 Editor Debug" RECURSE=1 CLEAN 
   cd "..\batwind"

!ELSEIF  "$(CFG)" == "batwind - Win32 Editor Release"

"gwind - Win32 Editor Release" : 
   cd "\src\sf\wargamer\src\gwind"
   $(MAKE) /$(MAKEFLAGS) /F .\gwind.mak CFG="gwind - Win32 Editor Release" 
   cd "..\batwind"

"gwind - Win32 Editor ReleaseCLEAN" : 
   cd "\src\sf\wargamer\src\gwind"
   $(MAKE) /$(MAKEFLAGS) /F .\gwind.mak CFG="gwind - Win32 Editor Release" RECURSE=1 CLEAN 
   cd "..\batwind"

!ENDIF 

!IF  "$(CFG)" == "batwind - Win32 Release"

"batlogic - Win32 Release" : 
   cd "\src\sf\wargamer\src\batlogic"
   $(MAKE) /$(MAKEFLAGS) /F .\batlogic.mak CFG="batlogic - Win32 Release" 
   cd "..\batwind"

"batlogic - Win32 ReleaseCLEAN" : 
   cd "\src\sf\wargamer\src\batlogic"
   $(MAKE) /$(MAKEFLAGS) /F .\batlogic.mak CFG="batlogic - Win32 Release" RECURSE=1 CLEAN 
   cd "..\batwind"

!ELSEIF  "$(CFG)" == "batwind - Win32 Debug"

"batlogic - Win32 Debug" : 
   cd "\src\sf\wargamer\src\batlogic"
   $(MAKE) /$(MAKEFLAGS) /F .\batlogic.mak CFG="batlogic - Win32 Debug" 
   cd "..\batwind"

"batlogic - Win32 DebugCLEAN" : 
   cd "\src\sf\wargamer\src\batlogic"
   $(MAKE) /$(MAKEFLAGS) /F .\batlogic.mak CFG="batlogic - Win32 Debug" RECURSE=1 CLEAN 
   cd "..\batwind"

!ELSEIF  "$(CFG)" == "batwind - Win32 Editor Debug"

!ELSEIF  "$(CFG)" == "batwind - Win32 Editor Release"

!ENDIF 


!ENDIF 

