/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#error "Obsolete"

#ifndef BResultUser_HPP
#define BResultUser_HPP

#ifndef __cplusplus
#error BResultUser.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 * Battle Result Window User
 *
 *
 *----------------------------------------------------------------------
 */

#include "b_result.hpp"

class BResultWindowUser : public BattleResultTypes
{
    public:
        virtual const BattleResults* battleResults() const = 0;
        virtual void resultsFinished(BattleContinueMode mode) = 0;
};

#endif /* BResultUser_HPP */

