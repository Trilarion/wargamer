/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 * Battle Unit Move Dialog
 *
 * Presented to player after dragging a unit
 *
 * Not using a private hidden implementation, as the only place that
 * uses this is bw_unit.cpp.  We may change our mind if this
 * is dependant on too many Header files, or we start adding
 * lots of private functions and data.
 *
 *----------------------------------------------------------------------
 */

#if 0
#include "stdinc.hpp"
#include "bud_move.hpp"
#include "scenario.hpp"       // gamesup: scenario global information
#include "tooltip.hpp"        // system: Tooltip helper
#include "cbutton.hpp"        // system: Custom Buttons
#include "palette.hpp"        // system: Colour Palette Helper
#include "batres.h"           // res: Dialogs are in Resource
#include "batres_s.h"         // res: Dialogs are in Resource
#ifdef DEBUG
#include "app.hpp"
#endif

namespace BattleWindows_Internal
{


BattleMoveDialog::BattleMoveDialog(BUI_UnitInterface* owner) :
   BattleUnitDial(owner)
{
}

BattleMoveDialog::~BattleMoveDialog()
{
}

/*
 * Create actual window
 */

void BattleMoveDialog::onCreate(HWND hParent)
{
#ifdef DEBUG_RESOURCES
   HRSRC hRes = FindResource(APP::instance(), BattleMoveOrderDialog, RT_DIALOG);
   ASSERT(hRes != NULL);
   HGLOBAL hGlob = LoadResource(APP::instance(), hRes);
   ASSERT(hGlob != NULL);
   LPVOID data = LockResource(hGlob);
   ASSERT(data != NULL);
#endif

   // Ensure Custom Buttons are initialised!

   CustomButton::initCustomButton(getInstance());

   HWND hwnd = createDialog(BattleMoveOrderDialog, hParent, false);
   ASSERT(hwnd != NULL);
}

/*
 * Called from show() to initialise dialog box
 */

void BattleMoveDialog::onShow(const BUD_Info& info)
{
}

/*
 * Called from hide() to free up resources
 */

void BattleMoveDialog::onHide()
{
}


/*
 * Windows message handlers
 */

BOOL BattleMoveDialog::procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
   switch(msg)
   {
      case WM_INITDIALOG:
         HANDLE_WM_INITDIALOG(hWnd, wParam, lParam, onInitDialog);
         break;
      case WM_COMMAND:
         HANDLE_WM_COMMAND(hWnd, wParam, lParam, onCommand);
         break;
      case WM_DRAWITEM:
         HANDLE_WM_DRAWITEM(hWnd, wParam, lParam, onDrawItem);
         break;
      default:
         return FALSE;
   }

   return TRUE;
}

BOOL BattleMoveDialog::onInitDialog(HWND hwnd, HWND hwndFocus, LPARAM lParam)
{
   // buttons

   const DrawDIB* fillDIB = scenario->getScenarioBkDIB(Scenario::BackPatterns::WoodBackground);
   const CustomBorderInfo& border = scenario->getBorderColors();

   for(int id = BUDM_BUTTONID_FIRST; id <= BUDM_BUTTONID_LAST; ++id)
   {
      CustomButton cb(hwnd, id);
      cb.setFillDib(fillDIB);
      cb.setBorderColours(border);
   }

   /*
    * Add tooltips
    */

   static TipData tipData[] = {
      { BUDM_UNITS,    TTS_BUDM_UNITS,    SWS_BUDM_UNITS    },
      { BUDM_SEND,     TTS_BUDM_SEND,     SWS_BUDM_SEND     },
      { BUDM_CANCEL,   TTS_BUDM_CANCEL,   SWS_BUDM_CANCEL   },
      { BUDM_FULLDIAL, TTS_BUDM_MAINDIAL, SWS_BUDM_MAINDIAL },
      EndTipData
   };

   g_toolTip.addTips(hWnd, tipData);

   return True;
}


void BattleMoveDialog::onCommand(HWND hwnd, int id, HWND hwndCtl, UINT codeNotify)
{
  switch(id)
  {
    case BUDM_UNITS:
    {
      if(codeNotify == STN_CLICKED)
      {
         //----- Todo
         // showComboList(hwndCtl);
      }
      break;
    }

    case BUDM_SEND:
    {
      sendOrder();
      break;
    }

    case BUDM_CANCEL:
    {
      cancelOrder();
      break;
    }

    case BUDM_FULLDIAL:
    {
      showOrderDial();
      break;
    }
  }
}

void BattleMoveDialog::onDrawItem(HWND hwnd, const DRAWITEMSTRUCT* lpDrawItem)
{
  HPALETTE oldPal = SelectPalette(lpDrawItem->hDC, Palette::get(), FALSE);
  RealizePalette(lpDrawItem->hDC);

  switch(lpDrawItem->CtlID)
  {
    case BUDM_UNITS:
      //----- Todo
      // drawCombo(d_fillDib, lpDrawItem);
      break;
  }

  SelectPalette(lpDrawItem->hDC, oldPal, FALSE);
}

};    // namespace BattleWindows_Internal
#endif



/*
 *----------------------------------------------------------------------
 * $Log$
 *----------------------------------------------------------------------
 */
