/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef BTOOLBAR_HPP
#define BTOOLBAR_HPP

#include "mytypes.h"
#include "wind.hpp"

class BattleToolBar_Imp;
class PixelPoint;
class BattleWindowsInterface;

class B_ToolBar : public Window
{
	 BattleToolBar_Imp* d_toolBar;
  public:
	 B_ToolBar(HWND parent, BattleWindowsInterface* bw);
	 ~B_ToolBar();

	 void position(const PixelPoint& p);
	 void show(bool visible);
     void enable(bool visible);
     bool isVisible() const;
     bool isEnabled() const;

	 // void destroy();
	 void setCheck(int menuID, Boolean f);
	 void enable(int menuID, Boolean f);
	 int height() const;
     HWND getHWND() const;
};


#endif
