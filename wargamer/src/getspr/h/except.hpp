/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef EXCEPT_HPP
#define EXCEPT_HPP

#ifndef __cplusplus
#error except.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *	Generic Exception
 *
 *----------------------------------------------------------------------
 */


#include "types.h"
#include <stdarg.h>


class GeneralError {
	static bool doingError;
	static bool firstError;
	char *describe;
public:
	GeneralError();
	GeneralError(const char* s, ...);

	const char* get();
protected:
	void setMessage(const char* s);
	void setMessage(const char* s, va_list& vaList);
};

/*
 * Virtual class that Application must provide an instance of
 */

class ExceptionUtility {
 public:
 	enum YesNo { Yes, No };		// Result of ask() function

 	virtual void logError(const char* title, const char* text) = 0;
		// Add title and text to a log file

#if defined(DEBUG)
 	virtual void logDebug(const char* text) = 0;
		// Add text to a debug File
#endif

	virtual YesNo ask(const char* title, const char* text) = 0;
		// Display title and text, and ask user if they want to quit
		// Return True to Quit, False to ignore

	virtual void alert(const char* title, const char* text) = 0;
		// Display Information to user and continue
};

extern ExceptionUtility* exceptionHandler;	// Must be provided by application

#endif /* EXCEPT_HPP */
