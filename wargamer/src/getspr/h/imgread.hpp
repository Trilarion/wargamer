/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef IMGREAD_HPP
#define IMGREAD_HPP

#ifndef __cplusplus
#error imgread.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *	Interface for Image Readers
 *
 *----------------------------------------------------------------------
 */

#include "types.h"
#include "palette.hpp"

class ImageReaderInterface
{
	public:
		virtual ~ImageReaderInterface() = 0;

		virtual UBYTE* init(UWORD width, UWORD height) = 0;
		virtual Palette& getPalette() = 0;
		virtual void setTransparent(Color c) = 0;
		virtual UWORD getStorageWidth() const = 0;

};

inline ImageReaderInterface::~ImageReaderInterface() { }

#endif /* IMGREAD_HPP */

