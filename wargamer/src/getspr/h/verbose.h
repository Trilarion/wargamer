/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef H_VERBOSE
#define H_VERBOSE
/*
 * $Id$
 *
 * Verbosity level and error reporting header file
 *
 * $Log$
 * Revision 1.1  2001/06/13 08:52:38  greenius
 * Initial Import
 *
 * Revision 1.1  1993/12/16  16:42:54  Steven_Green
 * Initial revision
 *
 * 
 *    Rev 1.1   25 Jun 1993 15:44:18   sgreen
 * No change.
 * 
 *    Rev 1.0   28 Apr 1993 16:33:54   sgreen
 * Initial revision.
 * Revision 1.1  1992/10/19  20:47:03  sgreen
 * Initial revision
 *
 */


#include "types.h"

typedef enum { V_SILENT, V_QUIET, V_VERBOSE, V_DEBUG } Verbose;
extern Verbose verbosity;

void report(Verbose level, const char *fmt, ...);
void error(int line, const String& fileName, const char *lineBuf, const char *fmt, ...);

void cleanup();		// application provided function

#endif	/* H_VERBOSE */
