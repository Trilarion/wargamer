/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef CPPFILE_HPP
#define CPPFILE_HPP

#ifndef __cplusplus
#error cppfile.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *	C STDIO implementation of FileBase
 *
 *----------------------------------------------------------------------
 */

#include "filebase.hpp"
#include "stdio.h"

class STDIOFile
{
		FILE* d_fp;
	protected:
		STDIOFile();
		~STDIOFile();


		bool seekTo(SeekPos where);
		SeekPos getPos() const;
		bool isOK() const;
		void fp(FILE* fp);
		FILE* fp();
		const FILE* fp() const;

		void checkValid() const;
};

class STDIOReader : public FileReader, public STDIOFile
{
	public:
		STDIOReader(const char* fname);
		~STDIOReader();

		bool seekTo(SeekPos where) { return STDIOFile::seekTo(where); }
		SeekPos getPos() const { return STDIOFile::getPos(); }
		bool isOK() const { return STDIOFile::isOK(); }
		bool isAscii() const { return false; }

		bool read(void* data, DataLength length);
		bool read(void* data, DataLength length, DataLength& bytesRead);
		char* readLine() { return 0; }
};

class STDIOWriter : public FileWriter, public STDIOFile
{
	public:
		STDIOWriter(const char* fName);
		~STDIOWriter();


		bool seekTo(SeekPos where) { return STDIOFile::seekTo(where); }
		SeekPos getPos() const { return STDIOFile::getPos(); }
		bool isOK() const { return STDIOFile::isOK(); }
		bool isAscii() const { return false; }

		bool write(const void* data, DataLength length);
};

#endif /* CPPFILE_HPP */

