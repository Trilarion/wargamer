/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef FILEDATA_H
#define FILEDATA_H

#ifndef __cplusplus
#error filedata.h is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Filedata
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/06/13 08:52:38  greenius
 * Initial Import
 *
 * Revision 1.1  1995/10/29 16:02:49  Steven_Green
 * Initial revision
 *
 * Revision 1.1  1993/12/16  22:20:15  Steven_Green
 * Initial revision
 *
 * Revision 1.1  1993/12/16  16:42:54  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#ifdef __cplusplus
extern "C" {
#endif

#include "types.h"

#ifdef _MSC_VER
#pragma pack (push, 1)
#endif
#ifndef __WATCOM_CPLUSPLUS__
#define _Packed
#endif

/*
 * Disk Format
 */

typedef _Packed union {
	UBYTE b[4];
} ILONG;

typedef _Packed union {
	UBYTE b[2];
} IWORD;

typedef _Packed struct {
	UBYTE b[1];
} IBYTE;

#ifdef _WATCOMC
#pragma pack ( pop )
#endif

void putWord(IWORD* dest, UWORD src);
void putLong(ILONG* dest, ULONG src);

inline void putByte(IBYTE* dest, UBYTE src)
{
	dest->b[0] = src;
}

UWORD getWord(IWORD* src);
ULONG getLong(ILONG* src);

inline UBYTE getByte(IBYTE* src)
{
 	return src->b[0];
}

#ifdef __cplusplus
};
#endif

#endif /* FILEDATA_H */
