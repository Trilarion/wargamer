/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef FILEBASE_H
#define FILEBASE_H

#ifndef __cplusplus
#error filebase.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1995, Steven Morle-Green, All Rights Reserved
 *----------------------------------------------------------------------
 *
 *	Virtual Class for File readers/writers
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/06/13 08:52:38  greenius
 * Initial Import
 *
 * Revision 1.1  1995/11/22 10:45:25  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#include "types.h"
#include "except.hpp"

typedef ULONG SeekPos;
typedef size_t DataLength;

/*---------------------------------------------------------------
 * Generic File Classes
 */

class FileBase {
public:
	virtual bool seekTo(SeekPos where) = 0;
	virtual SeekPos getPos() const = 0;
	virtual bool isOK() const = 0;

	virtual bool isAscii() const = 0;
		// Application defined value that returns whether file is written
		// in ascii or binary mode.

	virtual int getMode() const { return 0; }
		// Return an optional application defined value that shows what sort of
		// file is being read or written.  For example in Wargamer,
		// this shows whether a saved game or a mission is being read/written.
};

class FileReader : public FileBase {
public:
	// virtual ~FileReader() = 0;
	virtual bool read(void* data, DataLength length) = 0;
	virtual bool read(void* data, DataLength length, DataLength& bytesRead) = 0;

	/*
	 * Asci helper functions
	 */

	virtual char* readLine() = 0;

	/*
	 * Binary helper functions
	 */

	char* getString();
	bool getBool(bool& b);
	bool getUByte(UBYTE& b);
	bool getSByte(SBYTE& b) { return getUByte( (UBYTE&) b); }
	bool getUWord(UWORD& w);
	bool getSWord(SWORD& w) { return getUWord( (UWORD&) w); }
	bool getULong(ULONG& l);
	bool getSLong(SLONG& l) { return getULong( (ULONG&) l); }
};

class FileWriter : public FileBase {
	int indent;			// Used with ascii writer
public:
	FileWriter() { indent = 0; }
	// virtual ~FileWriter() = 0;
	virtual bool write(const void* data, DataLength length) = 0;

	/*
	 * Useful helper functions
	 */

	bool putString(const char* s);
	bool printf(const char* fmt, ...);

	bool putBool(bool b);
	bool putUByte(UBYTE b);
	bool putSByte(SBYTE b) { return putUByte(UBYTE(b)); }
	bool putUWord(UWORD w);
	bool putSWord(SWORD w) { return putUWord(UWORD(w)); }
	bool putULong(ULONG l);
	bool putSLong(SLONG l) { return putULong(ULONG(l)); }
};

inline bool operator << (FileReader& f, bool& b)  { return f.getBool(b);  }
inline bool operator << (FileReader& f, UBYTE& b) { return f.getUByte(b); }
inline bool operator << (FileReader& f, SBYTE& b) { return f.getSByte(b); }
inline bool operator << (FileReader& f, UWORD& w) { return f.getUWord(w); }
inline bool operator << (FileReader& f, SWORD& w) { return f.getSWord(w); }
inline bool operator << (FileReader& f, ULONG& l) { return f.getULong(l); }
inline bool operator << (FileReader& f, SLONG& l) { return f.getSLong(l); }

inline bool operator << (FileReader& f, int& i)
{
	SLONG l;
	bool result = f.getSLong(l); 
	i = static_cast<int>(l);
	return result;
}

inline bool operator << (FileReader& f, unsigned int& i) 
{
	ULONG l;
	bool result = f.getULong(l);
	i = static_cast<unsigned int>(l); 
	return result;
}

inline bool operator >> (FileWriter& f, bool b) { return f.putBool(b);  }
inline bool operator >> (FileWriter& f, UBYTE b) { return f.putUByte(b); }
inline bool operator >> (FileWriter& f, SBYTE b) { return f.putSByte(b); }
inline bool operator >> (FileWriter& f, UWORD w) { return f.putUWord(w); }
inline bool operator >> (FileWriter& f, SWORD w) { return f.putSWord(w); }
inline bool operator >> (FileWriter& f, ULONG l) { return f.putULong(l); }
inline bool operator >> (FileWriter& f, SLONG l) { return f.putSLong(l); }

inline bool operator >> (FileWriter& f, int l)   { return f.putSLong(static_cast<SLONG>(l)); }
inline bool operator >> (FileWriter& f, unsigned int l)   { return f.putULong(static_cast<SLONG>(l)); }

template<class T>
inline bool operator << (FileReader& f, T& ob)
{
	return ob.readData(f);
}

template<class T>
inline void operator >> (FileWriter& f, const T& ob)
{
	ob.writeData(f);
}


class FileError : public GeneralError
{
	public:
		FileError() : GeneralError("File Error") { }
		FileError(const char* s) : GeneralError(s) { }
};


#endif /* FILEBASE_H */

