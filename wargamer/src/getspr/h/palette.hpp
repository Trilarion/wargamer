/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef PALETTE_HPP
#define PALETTE_HPP

#ifndef __cplusplus
#error palette.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *	Palette
 *
 *----------------------------------------------------------------------
 */

#include "types.h"
#include <assert.h>

typedef UBYTE Color;

struct PaletteEntry
{
	UBYTE s_red;
	UBYTE s_green;
	UBYTE s_blue;

	PaletteEntry() { }

	PaletteEntry(UBYTE r, UBYTE g, UBYTE b) :
		s_red(r),
		s_green(g),
		s_blue(b)
	{
	}
};

class Palette
{
		int d_nColors;
		PaletteEntry* d_colors;
	public:
		Palette();
		~Palette();

		bool init(int nColors);

		PaletteEntry& operator[] (Color c)
		{
			assert(d_colors != 0); 
			assert(c < d_nColors);
			return d_colors[c]; 
		}
		
		const PaletteEntry& operator[] (Color c) const
		{
			assert(d_colors != 0); 
			assert(c < d_nColors);
			return d_colors[c]; 
		}

		void destroy();

		int nColors() const { return d_nColors; }
};

#endif /* PALETTE_HPP */

