/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef TYPES_H
#define TYPES_H
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Miscelleneous types
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/06/13 08:52:38  greenius
 * Initial Import
 *
 * Revision 1.2  1993/12/21  00:40:06  Steven_Green
 * Boolean defined
 *
 * Revision 1.1  1993/12/16  16:42:54  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#ifdef __cplusplus
// extern "C" {
#endif

// typedef short WORD;
typedef short SWORD;
typedef unsigned short UWORD;

typedef unsigned short uword;
typedef short word;

typedef unsigned int uint;

typedef unsigned char ubyte;
typedef unsigned char uchar;
typedef char byte;
// typedef char BYTE;
typedef signed char SBYTE;
typedef unsigned char UBYTE;

// typedef long LONG;
typedef long SLONG;
typedef unsigned long ULONG;
typedef unsigned long ulong;

#if 0
#ifndef TRUE
#define FALSE false
#define TRUE true
typedef bool BOOL;
#endif
#endif
typedef bool Boolean;
#ifndef _WATCOM_C
#define TRUE true
#define FALSE false
typedef bool BOOL;
#endif


#ifndef _SIZE_T_DEFINED
#define _SIZE_T_DEFINED
#define _SIZE_T_DEFINED_
typedef unsigned size_t;
#endif

#ifdef __cplusplus
// };
#endif

#ifndef _WATCOM_C
#include <string>
typedef std::string String;
#endif



#endif /* TYPES_H */

