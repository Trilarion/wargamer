##############################################################################
# Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)	 #
# This Software is subject to the GNU General Public License.  				 #
# For License information see the file COPYING in the project root directory #
# For more information see the file README.									 #
##############################################################################

#####################################################################
# $Id$
#####################################################################
#
# Include this to build NT files programs
#
#####################################################################
#
# $Log$
# Revision 1.1  1995/10/20 11:07:12  Steven_Green
# Initial revision
#
#
#####################################################################
#
# this make include file is used to build all the win examples
#
# It assumes that the following make variables are declared by the makefile
# that includes it:
#
#	CDIR			directories other than .. to be searched for
#				.c files
#	name			the name of the EXE file to build (without
#				any extension)
#	LNK			the name of the linker command file
#	OBJS			a list of the .obj files needed to build 
#				the EXE
#	lnk_dependencies	a list of files the linker command file
#				depends upon
#
# In addition the makefile must declare a procedure called linkit that
# that can be used to add commands to the linker command file
#
#####################################################################

.ERASE

CC = wcc386	
CPP = wpp386	

!ifndef CDIR
CDIR=.
!endif

!ifndef ODIR
ODIR=.
!endif

!ifndef HDIR
HDIR=.
!endif

CFLAGS += -bt=nt
CFLAGS += -oaext -5 -w4 -i=$(HDIR)
!ifdef NODEBUG
CFLAGS += -UDEBUG -DNDEBUG
!else
CFLAGS += -d2
CFLAGS += -DDEBUG -UNDEBUG
!endif
CPPFLAGS = $(CFLAGS) -xs

!ifndef LNK
LNK = $(name).lk
!endif

lnk_dependencies += dosnt.mif

$(name).exe : $(OBJS) $(LNK) .AUTODEPEND
    wlink @$(LNK)

$(LNK) : $(lnk_dependencies)
    %create $(LNK)
    @%append $(LNK) SYSTEM NT
    @%append $(LNK) debug all
    @%append $(LNK) name $(name)
    @%append $(LNK) PATH $(ODIR)
    @%make linkit
    @for %i in ($(OBJS)) do @%append $(LNK) file %i

#    @%append $(LNK) op map
#    @%append $(LNK) op sym

.EXTENSIONS:
.EXTENSIONS: .exe 
.EXTENSIONS: .obj
.EXTENSIONS: .cpp .c .h .dlg .rc .ico .bmp .cur



.c:.;$(CDIR)
.cpp:$(CDIR)
.obj:$(ODIR)

.c.obj : .AUTODEPEND
	$(CC) $(CFLAGS) $[* -fo$(ODIR)\$^.

.cpp.obj : .AUTODEPEND
	$(CPP) $(CPPFLAGS) $[* -fo$(ODIR)\$^.
	
	
clean: .symbolic
    @erase /s *.obj
    @erase /s *.err
    @erase /s *.$(LNKEXT)
    @erase /s *.res
    @erase /s *.pch
    @erase /s *.lib
    @erase /s *.map
    @erase /s *.sym
