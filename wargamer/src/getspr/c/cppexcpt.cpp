/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 * Text version of Exception handler
 *
 *----------------------------------------------------------------------
 */

#include "except.hpp"
#include <iostream.h>

class TextExceptionUtility : public ExceptionUtility
{
	public:
	 	void logError(const char* title, const char* text);
			// Add title and text to a log file

#if defined(DEBUG)
 		void logDebug(const char* text);
			// Add text to a debug File
#endif

		YesNo ask(const char* title, const char* text);
			// Display title and text, and ask user if they want to quit
			// Return True to Quit, False to ignore

		void alert(const char* title, const char* text);
};

ExceptionUtility* exceptionHandler = new TextExceptionUtility;



void TextExceptionUtility::logError(const char* title, const char* text)
{
}

#if defined(DEBUG)
void TextExceptionUtility::logDebug(const char* text)
{
}
#endif

ExceptionUtility::YesNo TextExceptionUtility::ask(const char* title, const char* text)
{
	return No;
}

void TextExceptionUtility::alert(const char* title, const char* text)
{
	cout << "Exception" << endl;
	cout << title << endl;
	cout << text << endl;
}

