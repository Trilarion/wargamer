/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *	C STDIO implementation of FileBase
 *
 *----------------------------------------------------------------------
 */

#include "cppfile.hpp"
#include "assert.h"

STDIOFile::STDIOFile() : d_fp(NULL)
{
}

STDIOFile::~STDIOFile()
{
	if(d_fp)
	{
		if(fclose(d_fp) != 0)
			throw FileError("Error closing file");
		d_fp = NULL;
	}
}

void STDIOFile::fp(FILE* fp)
{
	assert(d_fp == NULL);

	if(fp == NULL)
		throw FileError("Error opening File");

	d_fp = fp;
}

void STDIOFile::checkValid() const
{
	assert(d_fp != NULL);
	if(d_fp == NULL)
		throw FileError("NULL File Handle");
	if(!isOK())
		throw FileError("File Error");
}

FILE* STDIOFile::fp()
{
	return d_fp;
}

const FILE* STDIOFile::fp() const
{
	return d_fp;
}


bool STDIOFile::seekTo(SeekPos where)
{
	checkValid();

	if(fseek(d_fp, where, SEEK_SET) != 0)
		throw FileError("seek error");
	return true;
}

SeekPos STDIOFile::getPos() const
{
	checkValid();
	long pos = ftell(d_fp);
	if(pos == -1)
		throw FileError("getPos error");
	return pos;
}

bool STDIOFile::isOK() const
{
	if(d_fp != NULL)
		return (ferror(d_fp) == 0);
	else
		return false;
}

STDIOReader::STDIOReader(const char* fname)
{
	fp(fopen(fname, "rb"));
}

STDIOReader::~STDIOReader()
{
}

bool STDIOReader::read(void* data, DataLength length)
{
	checkValid();

	if(fread(data, 1, length, fp()) != length)
		throw FileError("Read error");
	return true;
}

bool STDIOReader::read(void* data, DataLength length, DataLength& bytesRead)
{
	checkValid();

	bytesRead = fread(data, 1, length, fp());
	if(ferror(fp()) != 0)
		throw FileError("Read error");
	return true;
}

STDIOWriter::STDIOWriter(const char* fName)
{
	fp(fopen(fName, "wb"));
}

STDIOWriter::~STDIOWriter()
{
}

bool STDIOWriter::write(const void* data, DataLength length)
{
	checkValid();

	if(fwrite(data, 1, length, fp()) != length)
		throw FileError("Read error");
	return true;
}

