/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *	Palette Implementation
 *
 *----------------------------------------------------------------------
 */

#include "palette.hpp"

/*
 * Palette Functions
 */

Palette::Palette() :
	d_colors(0),
	d_nColors(0)
{
}

Palette::~Palette()
{	
	delete[] d_colors;
}

void Palette::destroy()
{	
	delete[] d_colors;
	d_colors = 0;
	d_nColors = 0;
}

bool Palette::init(int nColors)
{
	if( (d_colors == 0) || (nColors != d_nColors) )
	{
		d_colors = new PaletteEntry[nColors];
		d_nColors = nColors;
	}

	return true;
}



