/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *	Load an Image from a file
 *
 * Uses filename to read either BMP or LBM
 *
 *----------------------------------------------------------------------
 */

#include "readimg.hpp"
#include "ilbm.h"
#include "bmp.hpp"
#include <string.h>			// For memcpy

Image::Image(const char* fname) :
	d_bits(0),
	d_width(0),
	d_height(0),
	d_transparent(0),
	d_palette()
{
	if(!read(fname))
		throw ImageReadError();
}

Image::~Image()
{
	delete[] d_bits;
}

bool Image::read(const char* fname)
{

	enum FileType
	{
		UNKNOWN,
		BMP,
		LBM
	};

	FileType fileType = UNKNOWN;

	/*
	 * Parse the filename to see whether it is
	 * LBM or BMP
	 */

	char* s = strrchr(fname, '.');
	if(s)
	{
		s++;

		if(stricmp(s, "BMP") == 0)
			fileType = BMP;
		else if(stricmp(s, "LBM") == 0)
			fileType = LBM;
		else
			fileType = UNKNOWN;
	}


	if(fileType == BMP)
	{
		return BMPReader::read(fname, this);
	}
	else if(fileType == LBM)
	{
		if(ilbm_to_raw(fname) == 0)
		{
			// Copy ilbm info into image

			d_storageWidth = bmhd.w;
			d_width = bmhd.w;
			d_height = bmhd.h;
			assert(bmhd.transparentColor <= UCHAR_MAX);
			d_transparent = static_cast<Color>(bmhd.transparentColor);

			d_bits = new UBYTE[bmhd.w * bmhd.h];
			memcpy(d_bits, ilbm_ptr, bmhd.w * bmhd.h);

			d_palette.init(256);
			for(int i = 0; i < 256; i++)
			{
				d_palette[i] = PaletteEntry(
						ilbm_palette[i][0],
						ilbm_palette[i][1],
						ilbm_palette[i][2]);
			}


			return true;
		}
	}

	destroy();
	return false;
}

void Image::destroy()
{
	delete[] d_bits;
	d_bits = 0;
	d_palette.destroy();
}

Color& Image::getPixel(int x, int y)
{
	assert(d_bits != 0);
	return d_bits[x + y * d_storageWidth];
}

Color Image::getPixel(int x, int y) const
{
	assert(d_bits != 0);
	return d_bits[x + y * d_storageWidth];
}


/*
 * Image Reader Interface Implementation
 */



UBYTE* Image::init(UWORD width, UWORD height)
{
	d_storageWidth = width;
	d_width = width;
	d_height = height;

	d_bits = new UBYTE[d_storageWidth * d_height];

	return d_bits;
}

