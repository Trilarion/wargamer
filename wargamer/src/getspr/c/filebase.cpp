/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1995, Steven Morle-Green, All Rights Reserved
 *----------------------------------------------------------------------
 *
 *	File Support Functions
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/06/13 08:52:38  greenius
 * Initial Import
 *
 * Revision 1.1  1995/11/22 10:43:46  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */


#include "filebase.hpp"
#include "assert.h"
#include "filedata.h"
#include <stdarg.h>
#include <string.h>
#include <stdio.h>

char* FileReader::getString()
{
	/*
	 * Read encoded length
	 */

	UBYTE bl;
	size_t l = 0;
	unsigned int shift = 0;

	do
	{
		assert(shift < 32);

		read(&bl, sizeof(bl));
		l |= (bl & 0x7f) << shift;
		shift += 7;
	} while(bl & 0x80);

	if(l)
	{
		char* s = new char[l + 1];
		assert(s != 0);
		read(s, l);
		s[l] = 0;

		return s;
	}
	else
		return 0;
}

bool FileWriter::putString(const char* s)
{
	if(s)
	{
		size_t l = strlen(s);

		if(l)
		{
	
			/*
	 		 * Encode length in groups of 7 bits (lo first)
	 		 * Hi-bit is set to indicate that there is more to follow
	 		 *
	 		 * e.g.    1 : 01
	 		 *     	127 : 7f
	 		 *       128 : 80 01
	 		 *    0x1234 : b4 24
    		 *			  0 : 00
	 		 */

			size_t l1 = l;

			// assert(l1 >= 0);

			do 
			{
				if(!putUByte(UBYTE(l1 & 0x7f)))
					return false;

				UBYTE bl = (UBYTE) (l1 & 0x7f);
				l1 >>= 7;
				if(l1)
					bl |= 0x80;

			} while(l1);

		 	return write(s, l);
		}
	}

	/*
	 * Gets to here if s==0 or length==0
	 */

	return putUByte(0);
}

bool FileWriter::printf(const char* fmt, ...)
{
	char buffer[500];

	char* dest = buffer;

	int useIndent = indent;

	if(fmt[0] == '{')
		indent++;
	else if(fmt[0] == '}')
	{
		indent--;
		useIndent = indent;
	}

	for(int i = 0; i < useIndent; i++)
		*dest++ = ' ';


	va_list vaList;
	va_start(vaList, fmt);
#ifdef _WATCOM_C
	_vbprintf(dest, 500, fmt, vaList);
#else
        vswprintf(dest, 500, fmt, vaList);
#endif
	va_end(vaList);

	return write(buffer, strlen(buffer));
}


bool FileWriter::putUByte(UBYTE b)
{
	IBYTE ib;

	putByte(&ib, b);
	return write(&ib, sizeof(ib));
}

bool FileWriter::putUWord(UWORD w)
{
	IWORD iw;

	putWord(&iw, w);
	return write(&iw, sizeof(iw));
}

bool FileWriter::putULong(ULONG l)
{
	ILONG il;

	putLong(&il, l);
	return write(&il, sizeof(il));
}


bool FileWriter::putBool(bool b)
{
	return putUByte(b);
}

bool FileReader::getUByte(UBYTE& b)
{
	IBYTE ib;

	if(!read(&ib, sizeof(ib)))
		return false;

	b = getByte(&ib);

	return true;
}

bool FileReader::getUWord(UWORD& w)
{
	IWORD iw;

	if(!read(&iw, sizeof(iw)))
		return false;

	w = getWord(&iw);

	return true;
}

bool FileReader::getULong(ULONG& l)
{
	ILONG il;

	if(!read(&il, sizeof(il)))
		return false;

	l = getLong(&il);

	return true;
}

bool FileReader::getBool(bool& b)
{
	UBYTE v;
	if(getUByte(v))
	{
		b = (v != 0);
		return true;
	}
	else
		return false;
}
