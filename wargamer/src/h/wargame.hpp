/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
#ifndef WARGAME_H
#define WARGAME_H

#ifndef __cplusplus
#error wargame.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1995, Steven Morle-Green, All Rights Reserved
 *----------------------------------------------------------------------
 *
 * Wargamer Class to control main game
 *
 *----------------------------------------------------------------------
 */

/*
 * WargameAPP is now defined in wargame.cpp
 * There is nothing to export to other modules 
 */

#if 0
class CampaignGame;
class FrontEndWindow;

/*
 * A simple return type
 */

enum WG_RESULT {
   WG_OK,
   WG_ERROR
};

/*
 * Global variables in use by game
 */


#endif

#endif /* WARGAME_H */

