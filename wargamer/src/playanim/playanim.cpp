/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *	Little Program to play an AVI file in a full screen window
 *
 *----------------------------------------------------------------------
 */

#include "app.hpp"
#include "wind.hpp"
#include "mciwind.hpp"
#include "except.hpp"
#include <vfw.h>
#include <string.hpp>

class AnimationUser
{
	public:
		virtual ~AnimationUser() { }
		virtual void windowDestroyed(HWND hwnd) = 0;
		virtual HWND getHWND() = 0;
};

/*
 * Animation Window class
 */


class AnimationWindow : public WindowBase {
	 AnimationUser* d_owner;
	 String d_fileName;
	 HWND d_hMCI;                         // Handle to MCI Window
  public:
	 AnimationWindow(AnimationUser* owner, const char* fileName);

	 void playAnimation();

  private:
	 LRESULT procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
	 BOOL onCreate(HWND hWnd, CREATESTRUCT* lpCreateStruct);
	 void onMCINotifyMode(HWND hwnd, HWND hwndMCI, LONG mode);
	 void onMove(HWND hwnd, int x, int y) {}
	 void onClose(HWND hwnd);
	 void onDestroy(HWND hwnd);

	 static const char s_className[];
	 static ATOM s_classAtom;
	 static ATOM registerClass();
	 static const char* className() { return s_className; }

};


static ATOM AnimationWindow::s_classAtom = 0;
static const char AnimationWindow::s_className[] = "AnimationWindow";

static ATOM AnimationWindow::registerClass()
{
	if(!s_classAtom)
	{
		WNDCLASS wc;

		wc.style = CS_HREDRAW | CS_VREDRAW;
		wc.lpfnWndProc = WindowBase::baseWindProc;
		wc.cbClsExtra = 0;
		wc.cbWndExtra = sizeof(AnimationWindow*);
		wc.hInstance = APP::instance();
		wc.hIcon = NULL;
		wc.hCursor = LoadCursor(NULL, IDC_ARROW);
		wc.hbrBackground = (HBRUSH) (COLOR_BACKGROUND + 1);
		wc.lpszMenuName = NULL;
		wc.lpszClassName = s_className;
		s_classAtom = RegisterClass(&wc);
	}

	ASSERT(s_classAtom != 0);

	return s_classAtom;
}



AnimationWindow::AnimationWindow(AnimationUser* owner, const char* fileName) :
  d_fileName(fileName),
  d_owner(owner),
  d_hMCI(0)
{
	ASSERT(fileName != 0);
	ASSERT(d_fileName.length() != 0);
	ASSERT(d_owner != 0);

//  RECT r;
//  GetClientRect(d_owner->getHWND(), &r);

	registerClass();

	String title;
	title = "PlayAnim - ";
	title += fileName;

  HWND hWnd = createWindow(
		0,
		className(),
			title,
			WS_OVERLAPPEDWINDOW |
			WS_MAXIMIZE	|
			WS_CLIPSIBLINGS,		/* Style */
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		owner->getHWND(),				   /* parent window */
		NULL,
		APP::instance()
	);

	ASSERT(hWnd != 0);

	SetWindowText(hWnd, title);
	ShowWindow(hWnd, SW_MAXIMIZE);

	ASSERT(d_fileName != static_cast<const char*>(0));

	d_hMCI = MCIWindow::create(hWnd, d_fileName);
}

LRESULT AnimationWindow::procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	switch(msg)
	{
		HANDLE_MSG(hWnd, WM_CREATE,	onCreate);
		HANDLE_MSG(hWnd, MCIWNDM_NOTIFYMODE, onMCINotifyMode);
		HANDLE_MSG(hWnd, WM_CLOSE, onClose);
		HANDLE_MSG(hWnd, WM_DESTROY, onDestroy);

		default:
		  return defProc(hWnd, msg, wParam, lParam);
	}
}

BOOL AnimationWindow::onCreate(HWND hwnd, CREATESTRUCT* lpCreateStruct)
{
  return TRUE;
}

void AnimationWindow::onDestroy(HWND hwnd)
{
	d_owner->windowDestroyed(hwnd);
}

void AnimationWindow::onClose(HWND hwnd)
{
	DestroyWindow(hwnd);
	PostQuitMessage(0);
}

void AnimationWindow::onMCINotifyMode(HWND hwnd, HWND hwndMCI, LONG mode)
{
  if(mode == MCI_MODE_STOP)  // device stopped
  {
	 ASSERT(d_hMCI != NULL);
	 ASSERT(hwndMCI == d_hMCI);

	 MCIWindow mciWnd(d_hMCI);
	 mciWnd.kill();

	 d_hMCI = 0;
	 // d_owner->killAnimation();
	 PostMessage(hwnd, WM_CLOSE, 0, 0);
  }
}

void AnimationWindow::playAnimation()
{
  ASSERT(d_hMCI != 0);

  MCIWindow mciWnd(d_hMCI);
  mciWnd.play();
}


/*===================================================================
 * Application Stuff
 */

/*
 * Application class to make it work with system
 */

class PlayAnimApp :
	public APP,
	public AnimationUser
{
	String startFileName;	// Name of File to load
	AnimationWindow* d_animWindow;

public:
	PlayAnimApp();
	~PlayAnimApp();

	/*
	 * Implementation of APP virtual functions
	 */

	BOOL initApplication();
	BOOL initInstance();
	void endApplication();
	BOOL doCmdLine(LPCSTR cmd);

	Boolean processMessage(MSG* msg);
		// Process Message.
		//   Return True if application dealt with it
		//   False to continue as usual


	void windowDestroyed(HWND);
	HWND getHWND() { return NULL; }

 private:
};


/*===========================================================
 * PlayAnim Application Functions
 */

PlayAnimApp::PlayAnimApp() :
	startFileName(),
	d_animWindow(0)
{
}

PlayAnimApp::~PlayAnimApp()
{
	endApplication();
}

BOOL PlayAnimApp::initApplication()
{
	return TRUE;
}


BOOL PlayAnimApp::initInstance()
{
	InitCommonControls();

	d_animWindow = new AnimationWindow(this, startFileName);
	ASSERT(d_animWindow != 0);

	d_animWindow->playAnimation();

	return TRUE;
}


/*
 * Parse command line for name of game to use
 *
 * Return TRUE if OK
 * Return FALSE to abort the program
 */

BOOL PlayAnimApp::doCmdLine(LPCSTR cmd)
{
	debugLog("Command Line = %s\n", cmd);

	if(cmd && cmd[0])
	{
		BOOL inQuotes = FALSE;
		// MemPtr<char> buf(MAX_PATH);
		char buf[MAX_PATH];

		const char* s = cmd;
		char* s1 = buf;

		while(*s)
		{
			char c = *s++;

			if(c == '\"')
			{
				inQuotes = !inQuotes;
				if(!inQuotes)
					break;
			}
			else if(!inQuotes && isspace(c))
			{
				break;
			}
			else
				*s1++ = c;
		}
		*s1 = 0;

		startFileName = buf;
	}

	return TRUE;
}

void PlayAnimApp::endApplication()
{
	if(d_animWindow)
	{
		delete d_animWindow;
		d_animWindow = 0;
	}
}


/*
 * Process Messages to ourself
 * if hwnd is NULL, then it is a private message to ourself.
 */

Boolean PlayAnimApp::processMessage(MSG* msg)
{
	return False;
}


void PlayAnimApp::windowDestroyed(HWND hwnd)
{
	if(hwnd == d_animWindow->getHWND())
		d_animWindow = 0;
}


/*========================================================================
 * Main Windows Function
 */

int PASCAL WinMain (HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int nCmdShow)
{
	int result = 0;
	try
	{
		PlayAnimApp application;
		result = application.execute(hInstance, hPrevInstance, lpszCmdLine, nCmdShow);
	}
#if defined(DEBUG)
	catch(AssertError e)
	{
		// gApp.pauseGame(TRUE);
		MessageBox(0, "Exception", "Quit Program", MB_ICONSTOP | MB_TASKMODAL | MB_SETFOREGROUND);
	}
#endif
	catch(GeneralError e)
	{
		// gApp.pauseGame(TRUE);
		MessageBox(0, "General Error Exception", e.get(), MB_ICONSTOP | MB_TASKMODAL | MB_SETFOREGROUND);
	}
	catch(...)
	{
		// gApp.pauseGame(TRUE);
		MessageBox(0, "Exception", "Unknown", MB_ICONSTOP | MB_TASKMODAL | MB_SETFOREGROUND);
	}

	return result;
}


