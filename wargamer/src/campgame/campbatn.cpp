/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 * Null Implementation of CampaignBattle
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "campbat.hpp"
#include "wg_msg.hpp"
#include "msgbox.hpp"
#include "cbattle.hpp"
#include "cpdef.hpp"
#include "campint.hpp"

CampaignBattleGame::CampaignBattleGame()
{
}

CampaignBattleGame::~CampaignBattleGame()
{
}

GameTick CampaignBattleGame::addTime(SysTick::Value ticks)
{
   return 0;
}

// void CampaignBattleGame::pause(bool state)
// {
// }


void CampaignBattleGame::start(CampaignBattle* battle, CampaignTacticalOwner* game)
{
   const char* title = "Tactical Battle";
   const char* text =
      "The Tactical battle is not available in this version of the program."
      "The Battle results will be calculated instead";

   messageBox(title, text, MB_ICONINFORMATION | MB_OK);

   battle->setMode(BattleMode::Calculated);
    //--- TODO
    // WargameMessage::postFinishBattle();
    game->tacticalOver();
}

// void CampaignBattleGame::finish()
// {
// }

bool CampaignBattleGame::readData(FileReader& f, CampaignBattle* battle, CampaignTacticalOwner* game)
{
    return false;
}

bool CampaignBattleGame::writeData(FileWriter& f)
{
    return false;
}

void CampaignBattleGame::resume()
{
}

CampaignBattle* CampaignBattleGame::campaignBattle()
{
    return 0;
}

void CampaignBattleGame::reinforce()
{
}

void CampaignBattleGame::processBattleOrder(MP_MSG_BATTLEORDER * msg)
{
}

/*
 *----------------------------------------------------------------------
 * $Log$
 *----------------------------------------------------------------------
 */
