# Microsoft Developer Studio Project File - Name="campgameNoBattle" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Dynamic-Link Library" 0x0102

CFG=campgameNoBattle - Win32 Editor Debug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "campgameNoBattle.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "campgameNoBattle.mak" CFG="campgameNoBattle - Win32 Editor Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "campgameNoBattle - Win32 Release" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE "campgameNoBattle - Win32 Debug" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE "campgameNoBattle - Win32 Editor Debug" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE "campgameNoBattle - Win32 Editor Release" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 1
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""
CPP=cl.exe
MTL=midl.exe
RSC=rc.exe

!IF  "$(CFG)" == "campgameNoBattle - Win32 Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "..\..\exe"
# PROP Intermediate_Dir "o\release"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MT /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "CAMPGAMENOBATTLE_EXPORTS" /YX /FD /c
# ADD CPP /nologo /MD /W3 /Gi /GR /GX /Zi /O2 /I "..\h" /I "..\..\res" /I "..\system" /I "..\gamesup" /I "..\ob" /I "..\campdata" /I "..\batdata" /I "..\batgame" /I "..\campwind" /I "..\mapwind" /I "..\gwind" /I "..\campLogic" /I "..\camp_ai" /D "NDEBUG" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "EXPORT_CAMPGAME_DLL" /YX"stdinc.hpp" /FD /c
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x809 /d "NDEBUG"
# ADD RSC /l 0x809 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /dll /machine:I386
# ADD LINK32 user32.lib /nologo /dll /debug /machine:I386

!ELSEIF  "$(CFG)" == "campgameNoBattle - Win32 Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "..\..\exe"
# PROP Intermediate_Dir "o\debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MTd /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "CAMPGAMENOBATTLE_EXPORTS" /YX /FD /GZ /c
# ADD CPP /nologo /MDd /W3 /Gm /Gi /GR /GX /ZI /Od /I "..\h" /I "..\..\res" /I "..\system" /I "..\gamesup" /I "..\ob" /I "..\campdata" /I "..\batdata" /I "..\batgame" /I "..\campwind" /I "..\mapwind" /I "..\gwind" /I "..\campLogic" /I "..\camp_ai" /D "_DEBUG" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "EXPORT_CAMPGAME_DLL" /YX"stdinc.hpp" /FD /GZ /c
# ADD BASE MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x809 /d "_DEBUG"
# ADD RSC /l 0x809 /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /dll /debug /machine:I386 /pdbtype:sept
# ADD LINK32 user32.lib /nologo /dll /debug /machine:I386 /out:"..\..\exe/campgameNoBattleDB.dll" /pdbtype:sept

!ELSEIF  "$(CFG)" == "campgameNoBattle - Win32 Editor Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "campgameNoBattle___Win32_Editor_Debug"
# PROP BASE Intermediate_Dir "campgameNoBattle___Win32_Editor_Debug"
# PROP BASE Ignore_Export_Lib 0
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "..\..\exe"
# PROP Intermediate_Dir "o\Editor\Debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MDd /W3 /Gm /Gi /GR /GX /ZI /Od /I "..\h" /I "..\..\res" /I "..\system" /I "..\gamesup" /I "..\ob" /I "..\campdata" /I "..\batdata" /I "..\batgame" /I "..\campwind" /I "..\mapwind" /I "..\gwind" /I "..\campLogic" /I "..\camp_ai" /D "_DEBUG" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "EXPORT_CAMPGAME_DLL" /YX"stdinc.hpp" /FD /GZ /c
# ADD CPP /nologo /MDd /W3 /Gm /Gi /GR /GX /ZI /Od /I "..\h" /I "..\..\res" /I "..\system" /I "..\gamesup" /I "..\ob" /I "..\campdata" /I "..\batdata" /I "..\batgame" /I "..\campwind" /I "..\mapwind" /I "..\gwind" /I "..\campLogic" /I "..\camp_ai" /D "_USRDLL" /D "EXPORT_CAMPGAME_DLL" /D "EDITOR" /D "_DEBUG" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /YX"stdinc.hpp" /FD /GZ /c
# ADD BASE MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x809 /d "_DEBUG"
# ADD RSC /l 0x809 /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 user32.lib /nologo /dll /debug /machine:I386 /out:"..\..\exe/campgameNoBattleDB.dll" /pdbtype:sept
# ADD LINK32 user32.lib /nologo /dll /debug /machine:I386 /out:"..\..\exe/campgameNoBattleEDDB.dll" /pdbtype:sept

!ELSEIF  "$(CFG)" == "campgameNoBattle - Win32 Editor Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "campgameNoBattle___Win32_Editor_Release"
# PROP BASE Intermediate_Dir "campgameNoBattle___Win32_Editor_Release"
# PROP BASE Ignore_Export_Lib 0
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "..\..\exe"
# PROP Intermediate_Dir "o\Editor\Release"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MD /W3 /Gi /GR /GX /Zd /O2 /I "..\h" /I "..\..\res" /I "..\system" /I "..\gamesup" /I "..\ob" /I "..\campdata" /I "..\batdata" /I "..\batgame" /I "..\campwind" /I "..\mapwind" /I "..\gwind" /I "..\campLogic" /I "..\camp_ai" /D "NDEBUG" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "EXPORT_CAMPGAME_DLL" /YX"stdinc.hpp" /FD /c
# ADD CPP /nologo /MD /W3 /Gi /GR /GX /Zd /O2 /I "..\h" /I "..\..\res" /I "..\system" /I "..\gamesup" /I "..\ob" /I "..\campdata" /I "..\batdata" /I "..\batgame" /I "..\campwind" /I "..\mapwind" /I "..\gwind" /I "..\campLogic" /I "..\camp_ai" /D "NDEBUG" /D "_USRDLL" /D "EXPORT_CAMPGAME_DLL" /D "EDITOR" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /YX"stdinc.hpp" /FD /c
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x809 /d "NDEBUG"
# ADD RSC /l 0x809 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 user32.lib /nologo /dll /debug /machine:I386
# ADD LINK32 user32.lib /nologo /dll /debug /machine:I386 /out:"..\..\exe/campgameNoBattleED.dll"

!ENDIF 

# Begin Target

# Name "campgameNoBattle - Win32 Release"
# Name "campgameNoBattle - Win32 Debug"
# Name "campgameNoBattle - Win32 Editor Debug"
# Name "campgameNoBattle - Win32 Editor Release"
# Begin Group "Source Files"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat"
# Begin Source File

SOURCE=.\campaign.cpp
# End Source File
# Begin Source File

SOURCE=.\campbatn.cpp

!IF  "$(CFG)" == "campgameNoBattle - Win32 Release"

!ELSEIF  "$(CFG)" == "campgameNoBattle - Win32 Debug"

!ELSEIF  "$(CFG)" == "campgameNoBattle - Win32 Editor Debug"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "campgameNoBattle - Win32 Editor Release"

# PROP Exclude_From_Build 1

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\campimp.cpp
# End Source File
# Begin Source File

SOURCE=.\clogic.cpp

!IF  "$(CFG)" == "campgameNoBattle - Win32 Release"

!ELSEIF  "$(CFG)" == "campgameNoBattle - Win32 Debug"

!ELSEIF  "$(CFG)" == "campgameNoBattle - Win32 Editor Debug"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "campgameNoBattle - Win32 Editor Release"

# PROP Exclude_From_Build 1

!ENDIF 

# End Source File
# End Group
# Begin Group "Header Files"

# PROP Default_Filter "h;hpp;hxx;hm;inl"
# Begin Source File

SOURCE=.\batentry.hpp
# End Source File
# Begin Source File

SOURCE=.\campaign.hpp
# End Source File
# Begin Source File

SOURCE=.\campbat.hpp
# End Source File
# Begin Source File

SOURCE=.\campimp.hpp
# End Source File
# Begin Source File

SOURCE=.\cbat_ob.hpp
# End Source File
# Begin Source File

SOURCE=.\clogic.hpp
# End Source File
# Begin Source File

SOURCE=.\stdinc.hpp
# End Source File
# End Group
# Begin Group "Resource Files"

# PROP Default_Filter "ico;cur;bmp;dlg;rc2;rct;bin;rgs;gif;jpg;jpeg;jpe"
# End Group
# End Target
# End Project
