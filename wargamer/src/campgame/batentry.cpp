/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
#include "stdinc.hpp"
#include "batentry.hpp"
#include "cbattle.hpp"
#include "campdint.hpp"
#include "measure.hpp"
#include "trig.hpp"
#include "bob_cp.hpp"
#ifdef DEBUG
#include "scenario.hpp"
#include "clog.hpp"
LogFile beLog("BatREntryLog.log");
#endif
namespace CBat_EntryPoint {

#ifdef DEBUG
const char* entryPointName(ReinforcementEntryPoint rep)
{
   ASSERT(rep < REP_HowMany);
   static const char* s_text[REP_HowMany] = {
      "Our Rear",
      "Near Left",
      "Far Left",
      "Near Right",
      "Far Right",
      "Enemy Rear",
      "None"
   };

   return s_text[rep];
}
#endif

inline Bangle locationToDirection(Location& l1, Location& l2)
{
   SLONG x = l1.getX() - l2.getX();
   SLONG y = l1.getY() - l2.getY();

   // get byte angle
   return wangleToBangle(direction(x, y));
}

inline Side otherSide(Side s)
{
   return (s == 0) ? 1 : 0;
}

inline ReinforcementEntryPoint bangleToEntryPoint(Bangle b)
{
   if(b <= 32 || b >= 246)
      return REP_NearRight;
   else if(b <= 96)
      return REP_Rear;
   else if(b <= 138)
      return REP_NearLeft;
   else if(b <= 182)
      return REP_FarLeft;
   else if(b <= 202)
      return REP_EnemyRear;
   else
      return REP_FarRight;
}

void setEntryPoint(CampaignData* campData, CampaignBattle* battle, CampaignBattleUnit* bu)
{
   // Get entry point by camparing map location angles of units already in the battle,
   // and the reinforcing unit
#ifdef DEBUG
   beLog.printf("\n-----------------------------------------");
   beLog.printf("%s %s is entering battle as reinforcement",
      scenario->getSideName(bu->getUnit()->getSide()),
      bu->getUnit()->getName());
#endif

   Side ourSide = bu->getUnit()->getSide();
   Side theirSide = otherSide(ourSide);

   CampaignBattleUnitList& ourUnits = battle->units(ourSide);
   CampaignBattleUnitList& theirUnits = battle->units(theirSide);

   if(ourUnits.entries() > 0 && theirUnits.entries() > 0)
   {
      ICommandPosition thisCPI = bu->getUnit();
      ICommandPosition ourCPI = ourUnits.first()->getUnit();
      ICommandPosition theirCPI = theirUnits.first()->getUnit();
      BattleCP* bcp = thisCPI->generic()->battleInfo();
      ASSERT(bcp);
      ASSERT(thisCPI != ourCPI);

      // get map locations
      Location l1;
      Location l2;
      Location l3;
      thisCPI->getLocation(l1);
      ourCPI->getLocation(l2);
      theirCPI->getLocation(l3);

      // get bangle
      Bangle thisDir = locationToDirection(l1, l3);
      Bangle thatDir = locationToDirection(l2, l3);

      // campare bangles
      const int maxB = 256;
      const int base = 64;
      const int dif = base - thatDir;

      if(thisDir + dif >= maxB || thisDir + dif < 0)
      {
         thisDir = (thisDir + dif >= maxB) ? (thisDir + dif) - maxB : maxB + (thisDir + dif);
      }
      else
         thisDir += dif;

      // convert to entry point
      ReinforcementEntryPoint rep = bangleToEntryPoint(thisDir);
      ASSERT(rep < REP_None);
#ifdef DEBUG
      beLog.printf("---- will enter at %s of the battle", entryPointName(rep));
#endif
      bcp->entryPoint(rep);
   }
}

};
/*
 *----------------------------------------------------------------------
 * $Log$
 *----------------------------------------------------------------------
 */
