/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *      Campaign Logic
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "clogic.hpp"
#include "campctrl.hpp"
#include "campdint.hpp"
#include "despatch.hpp"
#include "armies.hpp"
#include "town.hpp"
#include "cu_move.hpp"
#include "siege.hpp"
#include "c_supply.hpp"
#include "townprop.hpp"
#include "control.hpp"
#include "camp_snd.hpp"
#include "gthread.hpp"

#include "wg_msg.hpp"

#ifdef DEBUG
#include "myassert.hpp"
#endif

#include "MultiPlayerConnection.hpp"
#include "DirectPlay.hpp"
#include "MultiPlayerMsg.hpp"

#if !defined(NOLOG)
#include "clog.hpp"
LogFile CRCLog("CRC.log");
#endif

/*
 * Schedules
 */

/*
 * Game Specific Scheduler functions
 *
 * timing needs to be changed so that instead of just an interval,
 * a schedule is set up e.g.
 *              Process every friday at 3an
 *              Process every day at 7pm
 *              Process every day at 9am and 5pm
 *              Process every 4 hours
 */


//----------------------------------------------------------------

class UnitMoveSchedule : public GameSchedule {
   static const TimeTick Interval;

   CampaignLogicOwner* d_campGame;

   public:
   UnitMoveSchedule(CampaignLogicOwner* campGame) :
   GameSchedule(Interval),
   d_campGame(campGame)
   {
   }

   void process(TimeTick ticks)
   {
      // d_campGame->startWriteLock();
      CampUnitMovement cuMove(d_campGame);
      cuMove.hourlyUnits(ticks);
      // d_campGame->endWriteLock();
   }
};

const TimeTick UnitMoveSchedule::Interval = HoursToTicks(1);

//----------------------------------------------------------------
// Do garbage collection on army data

class ArmyCleanupSchedule : public GameSchedule {
   static const TimeTick Interval;

   CampaignLogicOwner* d_campGame;
   //        Armies* d_army;

   public:
   ArmyCleanupSchedule(CampaignLogicOwner* campGame) :
   GameSchedule(Interval),
   d_campGame(campGame)
   //              d_army(&campGame->campaignData()->getArmies())
   {
   }

   void process(TimeTick ticks)
   {
      Armies* army = &d_campGame->campaignData()->getArmies();
      army->startWrite();
      army->cleanup();
      army->endWrite();
   }
};

const TimeTick ArmyCleanupSchedule::Interval = DaysToTicks(7);

//----------------------------------------------------------------

class TownWeeklySchedule : public GameSchedule {
   static const TimeTick Interval;

   CampaignLogicOwner* d_campGame;

   public:
   TownWeeklySchedule(CampaignLogicOwner* campGame) :
   GameSchedule(Interval),
   d_campGame(campGame)
   {
   }

   void process(TimeTick interval)
   {

      // d_campGame->startWriteLock();
      Siege::weeklySiege(d_campGame);
      // d_campGame->endWriteLock();

      CampaignData* campData = d_campGame->campaignData();

      //                 campData->getTowns().startWrite();
      //                 campData->getProvinces().startWrite();

      TownProcess::weeklyResources(d_campGame, campData);

      //                 campData->getProvinces().endWrite();
      //                 campData->getTowns().endWrite();
   }
};

const TimeTick TownWeeklySchedule::Interval = DaysToTicks(7);

//----------------------------------------------------------------

class TownDailySchedule : public GameSchedule {
   static const TimeTick Interval;

   CampaignLogicOwner* d_campGame;
   public:
   TownDailySchedule(CampaignLogicOwner* campGame) :
   GameSchedule(Interval),
   d_campGame(campGame)
   {
   }

   void process(TimeTick interval)
   {
      // d_campGame->startWriteLock();

      // campaignData->dailyTowns();
      Siege::dailySiege(d_campGame);

      CampUnitMovement cuMove(d_campGame);
      cuMove.dailyUnits();

      CampaignData* campData = d_campGame->campaignData();
      campData->updateArmistice(campData->getTick());

      // d_campGame->endWriteLock();
   }
};

const TimeTick TownDailySchedule::Interval = DaysToTicks(1);

//----------------------------------------------------------------

class SupplySchedule : public GameSchedule {
   static const TimeTick Interval;

   CampaignLogicOwner* d_campGame;

   public:
   SupplySchedule(CampaignLogicOwner* campGame) :
   GameSchedule(Interval),
   d_campGame(campGame)
   {
   }

   void process(TimeTick interval)
   {
      CampaignData* campData = d_campGame->campaignData();

      campData->getTowns().startWrite();
      campData->getProvinces().startWrite();
      CampaignSupply::weeklySupply(d_campGame);
      campData->getProvinces().endWrite();
      campData->getTowns().endWrite();
   }
};

const TimeTick SupplySchedule::Interval = DaysToTicks(7);

//----------------------------------------------------------------

#if 0
class BattleSchedule : public GameSchedule {
   static const TimeTick BattleInterval;

   CampaignLogicOwner* d_campGame;
   public:
   BattleSchedule(CampaignLogicOwner* campGame) :
   GameSchedule(BattleInterval),
   d_campGame(campGame)
   {
   }

   void process(TimeTick ticks)
   {

      CampaignData* campData = d_campGame->campaignData();
      CampaignBattleList& batList = campData->getBattleList();

      CampaignBattleProc::procBattles(d_campGame, &batList);

      // campaignData->procBattles(ticks);
   }
};

static const TimeTick BattleSchedule::BattleInterval = HoursToTicks(3);

//----------------------------------------------------------------

class FatigueSchedule : public GameSchedule {
   static const TimeTick FatigueInterval;

   CampaignLogicOwner* d_campGame;
   public:
   FatigueSchedule(CampaignLogicOwner* campGame) :
   GameSchedule(FatigueInterval),
   d_campGame(campGame)
   {
   }

   void process(TimeTick ticks)
   {
      CampaignData* campData = d_campGame->campaignData();

      FatigueProc::procFatigue(campData);

   }
};

static const TimeTick FatigueSchedule::FatigueInterval = HoursToTicks(2);
#endif

/*===================================================================
 * Campaign logic
 */

CampaignLogic::CampaignLogic(CampaignLogicOwner* campGame) :
      Thread(GameThreads::instance()),
      d_campGame(campGame),
      d_campData(campGame->campaignData()),
      d_timePulse(),
      d_tickLock(),
//      d_timeTick(0),
      d_wantTime(),
      d_pauseCount(0),
      d_timeEvents(),
      d_processes(),
      d_ai(campGame->campaignData(), &d_timeEvents),
      d_unitMoveSchedule(new UnitMoveSchedule(campGame)),
      d_townWeeklySchedule(new TownWeeklySchedule(campGame)),
      d_supplySchedule(new SupplySchedule(campGame)),
      d_townDailySchedule(new TownDailySchedule(campGame)),
      d_armyCleanupSchedule(new ArmyCleanupSchedule(campGame))
{

   #ifdef DEBUG
      if(CMultiPlayerConnection::connectionType() == ConnectionMaster) {
        CRCLog.printf("CRC checksums for Master : \n\n");
      }
      if(CMultiPlayerConnection::connectionType() == ConnectionSlave) {
        CRCLog.printf("CRC checksums for Slave : \n\n");
      }
   #endif
}

CampaignLogic::~CampaignLogic()
{
   stop();
   Despatcher::clear();
   d_unitMoveSchedule = 0;
   d_townWeeklySchedule = 0;
   d_supplySchedule = 0;
   d_townDailySchedule = 0;
   d_armyCleanupSchedule = 0;
   d_processes.reset();

   d_campGame = 0;
   d_campData = 0;
}

void CampaignLogic::init()
{
   /*
    * Set up AI...
    *
    * This is done here rather than in the run() function
    * because, the AI sets up a debugging window.   If the
    * debug window is created by a different thread, then
    * it needs to handle Windows messages with MsgWaitForMultipleEvent,
    * which seems a bitpointless just for a debugging windows and
    * will overly complicate my synchronization classes (thread.hpp)
    */

   d_campData = d_campGame->campaignData();
   ASSERT(d_campData);

   /*
   * Do an initial pass here, to ensuere that the Despatcher
   * has a valid pointer to CampData
   */
   Despatcher::process(d_campData);

   const Armies* armies = &d_campData->getArmies();

   NationIter niter = armies;
   while(++niter)
   {
      Side s = niter.current();
      if(GamePlayerControl::getControl(s) == GamePlayerControl::AI)
         addAI(s);
   }

   start();
   d_ai.start();
}

void CampaignLogic::reset()
{
   stop();
}

void CampaignLogic::stopTime()
{
   LockData lock(&d_tickLock);
//   ++d_pauseCount;
   d_wantTime = d_campData->getCTime();
   d_pauseCount = true;
}

void CampaignLogic::startTime()
{
//   ASSERT(d_pauseCount > 0);
//   --d_pauseCount;
   d_pauseCount = false;
}

CampaignTime CampaignLogic::getWantTime() const
{
   LockData lock(&d_tickLock);
   return d_wantTime;
}

// void CampaignLogic::setTime(TimeTick timeTick)
void CampaignLogic::setTime(const CampaignTime& wantTime)
{
   /*
    * Set the time
    */

//    ASSERT(d_campData);
//    if(d_campData->isTimeRunning())
//    {
//       if(timeTick != d_timeTick)
//       {
//          d_tickLock.enter();
//          d_timeTick = timeTick;
//          d_tickLock.leave();
//       }
//    }


//   ASSERT(wantTime >= d_wantTime);
   CampaignTime ctime = d_campData->getCTime();
   ASSERT(wantTime >= ctime);

   {
      LockData lock(&d_tickLock);
      if (isTimeRunning())
      {
         d_wantTime = wantTime;
      }
   }

   /*
   * Notify thread that time has changed
   *
   * This has been moved outside of the above if otherwise
   * the logic occasionally freezes
   *
   * Pulse must still be set even if time is paused so that
   * the despatcher can be checked since that is where the
   * messages to unpause the game go to.
   */

   d_timePulse.set();      // Raise signal for Logic Thread
}


/*
 * Campaign Control Thread
 */

void CampaignLogic::run()
{
#ifdef DEBUG
   debugLog("gameLogic thread started\n");
#endif

   ASSERT(d_campData);

   /*
   * Start the game logic!
   */

   d_processes.add(d_unitMoveSchedule   );
   d_processes.add(d_townWeeklySchedule );
   d_processes.add(d_supplySchedule     );
   d_processes.add(d_townDailySchedule  );
   d_processes.add(d_armyCleanupSchedule);


   while(wait(d_timePulse) != TWV_Quit)
   {
      // d_waiting.set();                     // Tell other thread that we are waiting
      // d_timePulse.wait();          // Wait for the time to be updated
      // d_waiting.reset();           // Tell other thread that we are processing

      /*
      * Process Message Despatcher, since otherwise if game is paused
      * the orders stay in the despatch queue until it is unpaused.
      * Sort this out properly when we do the 2 player version, when
      * we'll have to change the way time is incremented anyway.
      *
      * Note that it is still in the inner loop as well
      */

      // Despatcher::process(d_campData);

      /*
      * Process all Campaign Logic from logic thread
      *
      * Return True if ticks had changed
      */

      // Convert wantTime to ticks

      for (;;)
      {
         if(wait(d_timePulse, 0) == TWV_Quit)
            break;

         /*
          * Process Message Despatcher
          */

         Despatcher::process(d_campData);

         /*
          * See if time has changed and process logic if it has
          */

         d_tickLock.enter();
         TimeTick tTicks = gameTicksToTicks(d_wantTime.toULONG());
         d_tickLock.leave();

         if(d_campData->getTick() >= tTicks)
            break;

         TimeTick cTick = d_campData->getTick();
         if(cTick == 0)
            cTick = tTicks;
         else
            cTick++;

         d_campData->setTimeTick(cTick);


         /*
         * Signal other threads that time has changed
         */

         d_timeEvents.process(d_campData->getTick());
         // d_ai.process();

         /*
         * Do a turn of the campaign Logic
         */

         d_processes.run(d_campData->getTick());

         /*
         Process the sound
         */

         d_campData->soundSystem()->ProcessSound();

         // Update tTicks since it may have been altered during the loop
         // and if we don't run it, the outer wait may have to wait an
         // extra tick.

//          d_tickLock.enter();
//          tTicks = d_timeTick;
//          d_tickLock.leave();


#ifdef DEBUG
      #ifdef MP_CRC_CHECK
      /*
      If we've reached our target time, then do a CRC checksum
      to make sure we're in th same state as the Master machine
      */
      unsigned int camp_ticks = d_campData->getTick();
      unsigned int game_ticks = gameTicksToTicks(d_wantTime.toULONG());

      if(camp_ticks == game_ticks) {

         unsigned int crc =  d_campData->getArmies().calculateChecksum();
         char tmp[256];

         sprintf(
            tmp,
            "CampTime : %i, WantTime : %i, CRC : %i\n",
            camp_ticks,
            game_ticks,
            crc
         );
         #ifdef DEBUG
         CRCLog.printf(tmp);
         #endif
      }
      #endif
#endif

      }
   }
}



void CampaignLogic::addAI(Side s)
{
   d_ai.addAI(s);
}


const char CampaignLogic::s_fileChunkName[] = "CampaignLogic";
const UWORD CampaignLogic::s_fileVersion = 0x0000;

bool CampaignLogic::readData(FileReader& f)
{
   // Look for CampaignLogic Chunk
   // If it doesn't exist then don't panic
   // it means this is either a stand-alone campaign scenario
   // or an older saved game

   {
      FileChunkReader fc(f, s_fileChunkName);
      if(fc.isOK())
      {
         UWORD version;
         f >> version;
         ASSERT(version <= s_fileVersion);


         d_unitMoveSchedule->readData(f);
         d_townWeeklySchedule->readData(f);
         d_supplySchedule->readData(f);
         d_townDailySchedule->readData(f);
         d_armyCleanupSchedule->readData(f);
      }
  }

  d_ai.readData(f);

   return true;
}

bool CampaignLogic::writeData(FileWriter& f) const
{
   {
      FileChunkWriter fc(f, s_fileChunkName);
      f << s_fileVersion;

      d_unitMoveSchedule->writeData(f);
      d_townWeeklySchedule->writeData(f);
      d_supplySchedule->writeData(f);
      d_townDailySchedule->writeData(f);
      d_armyCleanupSchedule->writeData(f);
   }

   d_ai.writeData(f);

   return f.isOK();
}


/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.2  2001/06/18 08:25:44  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
