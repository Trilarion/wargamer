/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *      Campaign Tactical game Implementation
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "campbat.hpp"
#include "batgame.hpp"
#include "bcreate.hpp"
#include "campdint.hpp"
#include "campint.hpp"
#include "cbattle.hpp"
#include "batdata.hpp"
#include "battime.hpp"
#include "cbat_ob.hpp"
#include "batctrl.hpp"
#include "cu_mode.hpp"
#include "batarmy.hpp"
#include "splash.hpp"
#include "batinfo.hpp"
#include "batowner.hpp"
#include "armies.hpp"
#include "tresult.hpp"
#include "town.hpp"
#include "scenario.hpp"
#include "terrain.hpp"
#include "weather.hpp"
#include "simpstr.hpp"
#include "bobutil.hpp"
#include "batentry.hpp"
#include "gamectrl.hpp"
//#include "bobiter.hpp"

#ifdef DEBUG
#include "msgbox.hpp"
#endif

using namespace CampaignBattleUtility;


class CampaignBattleGameImp : public BattleOwner
{
        public:
            CampaignBattleGameImp();
            ~CampaignBattleGameImp();

            GameTick addTime(SysTick::Value ticks);
            // void pauseGame(bool state);

            void start(CampaignBattle* battle, CampaignTacticalOwner* game);
            void reinforce();
            void newDay();

            // void finish();

            // void surrender();       // Implement BattleGameControl

            // BattleOwner Implementation

            HWND hwnd() const { return d_campGame->hwnd(); }
            void requestSaveGame(bool prompt) { d_campGame->requestSaveGame(prompt); }
         void multiplayerConnectionComplete(bool isMaster) {
            d_campGame->multiplayerConnectionComplete(isMaster);
         }
         void sendTimeSync(SysTick::Value ticks, unsigned int checksum) {
            d_campGame->sendTimeSync(ticks, checksum);
         }
         void sendBattleTimeSync(SysTick::Value ticks, unsigned int checksum) {
            // do nothing
            // BattleTime is incremented through the campaign
         }
         void sendSlaveReady(void) {
            d_campGame->sendSlaveReady();
         }


        void processBattleOrder(MP_MSG_BATTLEORDER * msg)
        {
         if(d_batGame)
            d_batGame->processBattleOrder(msg);
        }

            bool tacticalOver(const TacticalResults& results);

            bool readData(FileReader& f, CampaignBattle* battle, CampaignTacticalOwner* game);
            bool writeData(FileWriter& f);
            void resume();

            void startCampaignTime()
            {
               // if(!d_campGame->getCampaignData()->isTimeRunning())
                  d_campGame->startTime();
            }

            void stopCampaignTime()
            {
               // if(d_campGame->getCampaignData()->isTimeRunning())
                  d_campGame->stopTime();
            }

            CampaignBattle* campaignBattle() { return d_battle; }
        private:

            void calcBattleFieldSize(BattleInfo& batInfo, BattleOB* ob);
        private:
                BattleGame* d_batGame;
                CampaignBattle* d_battle;
                CampaignTacticalOwner* d_campGame;
};

CampaignBattleGameImp::CampaignBattleGameImp() :
        d_batGame(0),
        d_battle(0),
        d_campGame(0)
{
}

CampaignBattleGameImp::~CampaignBattleGameImp()
{
   BattleData* batData = d_batGame->battleData();
   BattleOB* ob = batData->ob();                   // Battle Order of Battle
   if (ob)
   {
      ob->removeBattleUnits();
      batData->ob(0);
      delete ob;
   }

   delete d_batGame;
}

GameTick CampaignBattleGameImp::addTime(SysTick::Value ticks)
{
    if(d_batGame)
    {
        d_batGame->addTime(ticks);

        // get battle time and convert to a campaign time

        const BattleData* batData = d_batGame->battleData();
        BattleMeasure::BattleTime::Tick batTick = batData->getTick();

        GameTick campTick = (batTick * CampaignTime::TicksPerSecond) / BattleMeasure::BattleTime::TicksPerSecond;

        return campTick;
    }
    else
        return 0;
}

// void CampaignBattleGameImp::pause(bool state)
// {
//        d_batGame->pauseGame(state);
// }

void CampaignBattleGameImp::start(CampaignBattle* battle, CampaignTacticalOwner* game)
{

//  static const char createBattleTxt[] = "Creating Battlefield";
    const char* createBattleTxt = InGameText::get(IDS_CreatingBattlefield);
    const char* convertOBTxt = InGameText::get(IDS_ConvertingOB);

    SplashScreen * splash = new SplashScreen(game->hwnd());
    splash->setText(convertOBTxt);

    d_battle = battle;
    d_campGame = game;
//  stopCampaignTime();

    CampaignData* campData = game->getCampaignData();
    Armies* army = &campData->getArmies();

    /*

     Transfer OB

   */


    BattleOB* ob = makeBattleOB(d_battle, army);

    /*

     Setup BattleInfo structure

   */

    BattleInfo battleInfo;

   /*
   town
   */

   ITown itown = d_battle->getCloseTown();
   const Town & town = campData->getTown(itown);
   TownSize townsize = town.getSize();

   // work out how far battle is from closest town
   Distance h_dist = d_battle->getHeadDistance();
   Distance t_dist = d_battle->getTailDistance();

   float min_dist = (float) minimum(h_dist, t_dist);
   float half_dist = (float) (h_dist + t_dist) / 2.0f;
   // s is scalar
   float s = 1.0f - (min_dist / half_dist);

   // NOTE : BattleInfo & TownInfo hold their population sizes in opposite enumeration orders !
   float pop_size = static_cast<float>(TownSize_MAX - townsize);
   pop_size *= s;

   BattleInfo::BattlePopulationEnum pop_enum = static_cast<BattleInfo::BattlePopulationEnum>((int)pop_size);
   if(pop_enum >= BattleInfo::BattlePopulationEnum_HowMany) pop_enum = BattleInfo::Dense;
   else if(pop_enum < BattleInfo::Sparse) pop_enum = BattleInfo::Sparse;

   battleInfo.population(pop_enum);


   /*
   terrain & height type
   */

   UBYTE iterrain = town.getTerrain();
   const TerrainTypeItem & terrain = campData->getTerrainType(iterrain);

   BattleInfo::BattleTerrainEnum bat_terrain;
   TerrainHeightFieldType bat_height;

   bat_terrain = static_cast<BattleInfo::BattleTerrainEnum>(terrain.getGroundType());
   ASSERT(bat_terrain >= BattleInfo::Open && bat_terrain <= BattleInfo::Marsh);
   if(bat_terrain < BattleInfo::Open) bat_terrain = BattleInfo::Open;
   if(bat_terrain > BattleInfo::Marsh) bat_terrain = BattleInfo::Marsh;

   switch(bat_terrain) {
      case BattleInfo::Open : { bat_height = HEIGHT_FLAT; break; }
      case BattleInfo::Rough : { bat_height = HEIGHT_ROLLING; break; }
      case BattleInfo::Wooded : { bat_height = HEIGHT_ROLLING; break; }
      case BattleInfo::Hilly : { bat_height = HEIGHT_HILLY; break; }
      case BattleInfo::WoodedHilly : { bat_height = HEIGHT_HILLY; break; }
      case BattleInfo::MountainPass : { bat_height = HEIGHT_STEEP; break; }
      case BattleInfo::Marsh : { bat_height = HEIGHT_FLAT; break; }
   }


   if(terrain.getRiverType() == Terrain::MinorRiver) {
      if(bat_terrain == BattleInfo::Open) bat_terrain = BattleInfo::OpenWithMinorRiver;
      else if(bat_terrain == BattleInfo::Wooded) bat_terrain = BattleInfo::WoodedWithMinorRiver;
      else if(bat_terrain == BattleInfo::Rough) bat_terrain = BattleInfo::RoughWithMinorRiver;
   }
   else if(terrain.getRiverType() == Terrain::MajorRiver) {
      if(bat_terrain == BattleInfo::Open) bat_terrain = BattleInfo::OpenWithMajorRiver;
      else if(bat_terrain == BattleInfo::Wooded) bat_terrain = BattleInfo::WoodedWithMajorRiver;
      else if(bat_terrain == BattleInfo::Rough) bat_terrain = BattleInfo::RoughWithMajorRiver;
   }

   battleInfo.terrain(bat_terrain);
   battleInfo.heightType(bat_height);



   /*
   province picture & description
   */

   IProvince iprovince = town.getProvince();
   const Province & province = campData->getProvince(iprovince);

   const DIB * prov_dib;

   SimpleString fileNameStr;
   fileNameStr << scenario->getProvincePicturePath() << "\\" << province.picture() << ".BMP";

   bool exists = FileSystem::fileExists(fileNameStr.toStr());
   ASSERT(exists);
   if(!exists) prov_dib = NULL;

   else {
      PicLibrary::Picture prov_pic = PicLibrary::get(fileNameStr.toStr());
      prov_dib = prov_pic;
   }

   battleInfo.provincePicture(const_cast<DIB*>(prov_dib));
   battleInfo.provinceDescription(const_cast<char*>(province.getShortName()));

   /*
   weather
   */
   CampaignWeather & weather = campData->getWeather();
   CampaignWeather::Weather weahter_type =  weather.getWeather();

   BattleInfo::BattleWeatherEnum bat_weather;

   if(weahter_type == CampaignWeather::Sunny) bat_weather = BattleInfo::SunnyDry;
   else if(weahter_type == CampaignWeather::LightRain) bat_weather = BattleInfo::RainyDry;
   else if(weahter_type == CampaignWeather::HeavyRain) bat_weather = BattleInfo::StormyDry;
   else { FORCEASSERT("ERROR - no good weather enum to fill in BattleInfo structure\n"); bat_weather = BattleInfo::SunnyDry; }

   if(weather.getGroundConditions() == CampaignWeather::Muddy || weather.getGroundConditions() == CampaignWeather::VeryMuddy) {
      INCREMENT(bat_weather);
   }

   if(bat_weather < BattleInfo::SunnyDry) bat_weather = BattleInfo::SunnyDry;
   else if(bat_weather > BattleInfo::StormyMuddy) bat_weather = BattleInfo::StormyMuddy;

   battleInfo.weather(bat_weather);

   /*
   map size & deployment size
   */

   calcBattleFieldSize(battleInfo, ob);

   /*
   Calculate which side has Initiative for battle selection
   This'll have to be done simulatneously
   */

   Side initiativeSide;

   int side0_advantage = 0;
   int side1_advantage = 0;

   BattleCP * battleCP_0 = ob->getTop(0);
   CommandPosition * campaignCP_0 = battleCP_0->generic()->campaignInfo();
   RefPtr<GLeader> leader_0 = battleCP_0->generic()->leader();

   BattleCP * battleCP_1 = ob->getTop(1);
   CommandPosition * campaignCP_1 = battleCP_1->generic()->campaignInfo();
   RefPtr<GLeader> leader_1 = battleCP_1->generic()->leader();

   int initiative_modifier_0;
   int initiative_modifier_1;

   // get postures
   Orders::Posture::Type posture_0 = campaignCP_0->posture();
   Orders::Posture::Type posture_1 = campaignCP_1->posture();

   if(posture_0 == Orders::Posture::Offensive) initiative_modifier_0 = 25;
   else initiative_modifier_0 = 15;

   if(posture_1 == Orders::Posture::Offensive) initiative_modifier_1 = 25;
   else initiative_modifier_1 = 15;

   // add advantage
   side0_advantage += (leader_0->getInitiative() / initiative_modifier_0);
   side1_advantage += (leader_1->getInitiative() / initiative_modifier_1);

   // get holding vs. moving modifier
   if(campaignCP_0->isHolding() && campaignCP_1->isMoving()) side0_advantage += 1;
   else if(campaignCP_1->isHolding() && campaignCP_0->isMoving()) side1_advantage += 1;

   // get lightCavalry advantage ratios
   unsigned int lightCavalryCount_0 = BobUtility::countUnitType(
      ob,
      battleCP_0,
      BasicUnitType::Cavalry,
      UnitTypeFlags::LightCavalry,
      false
   );

   unsigned int lightCavalryCount_1 = BobUtility::countUnitType(
      ob,
      battleCP_1,
      BasicUnitType::Cavalry,
      UnitTypeFlags::LightCavalry,
      false
   );

   float cavalryRatio_0to1 = ((float) lightCavalryCount_0) / ((float) lightCavalryCount_1);
   float cavalryRatio_1to0 = ((float) lightCavalryCount_1) / ((float) lightCavalryCount_0);

   int side0_cavalrybonus = 0;
   int side1_cavalrybonus = 0;

   // get side_0 defensive SP advantages
   if(posture_0 == Orders::Posture::Defensive) {
      // greater than 3:1 ratio
      if(cavalryRatio_0to1 > 3.0f) side0_cavalrybonus = 3;
      // greater than 2:1 ratio
      else if(cavalryRatio_0to1 > 2.0f) side0_cavalrybonus = 2;
      // greater than 3:2 ratio
      else if(cavalryRatio_0to1 > 1.5f) side0_cavalrybonus = 1;
   }
   // get side_0 offensive advantages
   else if(posture_0 == Orders::Posture::Offensive) {
      // greater than 4:1 ratio
      if(cavalryRatio_0to1 > 4.0f) side0_cavalrybonus = 2;
      // greater than 2:1 ratio
      else if(cavalryRatio_0to1 > 2.0f) side0_cavalrybonus = 1;
   }

   // get side_1 defensive SP advantages
   if(posture_1 == Orders::Posture::Defensive) {
      // greater than 3:1 ratio
      if(cavalryRatio_1to0 > 3.0f) side1_cavalrybonus = 3;
      // greater than 2:1 ratio
      else if(cavalryRatio_1to0 > 2.0f) side1_cavalrybonus = 2;
      // greater than 3:2 ratio
      else if(cavalryRatio_1to0 > 1.5f) side1_cavalrybonus = 1;
   }
   // get side_1 offensive advantages
   else if(posture_1 == Orders::Posture::Offensive) {
      // greater than 4:1 ratio
      if(cavalryRatio_1to0 > 4.0f) side1_cavalrybonus = 2;
      // greater than 2:1 ratio
      else if(cavalryRatio_1to0 > 2.0f) side1_cavalrybonus = 1;
   }

   // adjust for nasty terrain
   if(
      bat_terrain == BattleInfo::Wooded ||
      bat_terrain == BattleInfo::WoodedHilly ||
      bat_terrain == BattleInfo::Rough ||
      bat_terrain == BattleInfo::Marsh)
   {
      if(side0_cavalrybonus > 0) side0_cavalrybonus--;
      if(side1_cavalrybonus > 0) side1_cavalrybonus--;
   }

   // add adjustments
   side0_advantage += side0_cavalrybonus;
   side1_advantage += side1_cavalrybonus;

   // add random (1 to 10) value
   side0_advantage += CRandom::get(1,10);
   side1_advantage += CRandom::get(1,10);

   // does anyone have the clear initiative
   if((side0_advantage - side1_advantage) >= 3) initiativeSide = 0;
   else if((side1_advantage - side0_advantage) >= 3) initiativeSide = 1;

   // if there is no clear initiative winner, then flip a coin !
   else initiativeSide = CRandom::get(0,1);

   ASSERT(initiativeSide == 0 || initiativeSide == 1);
   battleInfo.initiativeSide(initiativeSide);


    // start battle game
//  SimpleString str = "Battle near ";
//  str += town.getName();
//  battleInfo.title(str.toStr());

   char buffer[200];
   sprintf(buffer, InGameText::get(IDS_BattleNear), town.getName());
   battleInfo.title(buffer);


    // Sync Campaign Time and Battle Time
    GameTick theTime = campData->getCTime().getTime();
    const Date& date = campData->getDate();
    Time time;
    time.set(theTime.getTime());
    Greenius_System::Date gdate(static_cast<Greenius_System::DateDefinitions::MonthEnum>(date.month), date.day, date.year);
    Greenius_System::Time gtime(time.hour, time.minute, time.second);
    BattleTime batTime(gdate, gtime);

    //splash.setText(createBattleTxt);
    delete splash;
    d_batGame = new BattleGame(this, ob, battleInfo, batTime);
}

void CampaignBattleGameImp::calcBattleFieldSize(BattleInfo& battleInfo, BattleOB* ob)
{
   int SPcount_side0 = BobUtility::countSP(ob->getTop(0));
   int SPcount_side1 = BobUtility::countSP(ob->getTop(1));
   int SPcount = maximum(SPcount_side0, SPcount_side1);

   int deploy_width;
   int deploy_height;

   // up to 60 SPs
   if(SPcount < 60) {
      deploy_width = 24;
      deploy_height = 12;
   }
   // up to 90 SPs
   else if(SPcount < 90) {
      deploy_width = 32;
      deploy_height = 16;
   }
   // up to 120 SPs
   else if(SPcount < 120) {
      deploy_width = 48;
      deploy_height = 24;
   }
   // up to 160 SPs
   else if(SPcount < 160) {
      deploy_width = 64;
      deploy_height = 32;
   }
   // up to 200 SPs
   else if(SPcount < 200) {
      deploy_width = 72;
      deploy_height = 32;
   }
   // up to 280 SPs
   else if(SPcount < 280) {
      deploy_width = 84;
      deploy_height = 32;
   }
   // over 280 SPs
   else {
      deploy_width = 96;
      deploy_height = 32;
   }


   // total height is both sides' deployment area, plus the 6 hexes in between
   int total_height = (deploy_height*2) + 6;
   // add 12 to allow AI to deploy properly
   total_height += 12;
   // dimensions must be >= 32 for map display
   if(deploy_width < 32) deploy_width = 32;
   if(total_height < 32) total_height = 32;

   // OK, this is a little confusing. In the BattleInfo structure,
   // d_deploymentWidth & d_deploymentHeight refer to the total selection area (ie. 128 x 128)
   // whereas d_battleWidth & d_battleHeight is the area you fight on - which we've just calculated
   battleInfo.deploymentWidth(128);
   battleInfo.deploymentHeight(128);
   battleInfo.battleWidth(deploy_width);
   battleInfo.battleHeight(total_height);
}


/*
 * transfer OB changes back to Campaign, adjust victory conditions, etc...
 */

bool CampaignBattleGameImp::tacticalOver(const TacticalResults& results)
{
    if(results.willContinue() == BattleResults::Continue)
    {
        // TODO:
        //  restoreOB(d_battle, &campData->getArmies(), batData);
        //  Close battle windows
        //  Tell campaign game to do overnight stuff
        //
        // Restoring the OB will be different to the battle ending one
        // The BattleOB must be updated if units are removed

#ifdef DEBUG
        messageBox("Campaign Battle", "Battle continues overnight", MB_ICONINFORMATION | MB_OK);
#endif
        return true;
    }
    else
    {
        // d_batGame->stop();

        CampaignData* campData = d_campGame->getCampaignData();

        {
            BattleData* batData = d_batGame->battleData();
            BattleOB* ob = batData->ob();                   // Battle Order of Battle
            restoreOB(d_battle, &campData->getArmies(), ob);
            ob->removeBattleUnits();
            batData->ob(0);
            delete ob;
        }

        /*
         * remove units inBattle flags
         */

        for(Side side = 0; side < scenario->getNumSides(); ++side)
        {
            CampaignBattleUnitListIterW iter(&d_battle->units(side));
            while(++iter)
            {
                CampaignBattleUnit* bu = iter.current();
                ICommandPosition cpi = bu->getUnit();

                // reset order
                cpi->getOrders().getCurrentOrder()->makeHold();
                if(results.winner() == side)
                  CP_ModeUtil::setMode(campData, bu->getUnit(), CampaignMovement::CPM_AfterWinningBattle);
                else
                  CP_ModeUtil::setMode(campData, bu->getUnit(), CampaignMovement::CPM_StartingRetreat);
            }
        }

        d_battle->turnMode(CampaignBattle::BattleTurnMode::BattleOver);

        d_campGame->tacticalOver();

        /*
        Put CD tracklist back to Campaign tracks
        */
        int num=2;
        GlobalSoundSystemObj.SetPlayList(num, 1+1, 2+1);
        GlobalSoundSystemObj.StartPlayList(false);

        return false;
    }
}

void CampaignBattleGameImp::reinforce()
{
   ASSERT(d_battle);
   ASSERT(d_campGame);
   ASSERT(d_batGame);

   // convert new units to battle OB
   CampaignData* campData = d_campGame->getCampaignData();
   Armies* army = &campData->getArmies();

   BattleOB::WriteLock lock(d_batGame->battleOB());

//   Boolean oldPause = GameControl::pause(True);
   addReinforcements(d_battle, army, d_batGame->battleOB());

   // figure out where we should come in at, i.e. the rear, flank, enemy rear etc.
   for(Side side = 0; side < 2; side++)
   {
      CampaignBattleUnitList& units = d_battle->units(side);
      SListIter<CampaignBattleUnit> iter(&units);
//    CampaignBattleUnitListIter iter(&units);
      while(++iter)
      {
         CampaignBattleUnit* bu = iter.current();
         if(bu->reinforcement())
         {
            CBat_EntryPoint::setEntryPoint(campData, d_battle, bu);
            bu->reinforcement(False);
         }
      }
   }

   // resize battlefield
   BattleInfo batInfo;
   calcBattleFieldSize(batInfo, d_batGame->battleOB());

   // if battle field size is unchanged, add 8 to width and height
   // to give some extra area for brining in the reinforcements
   const BattleInfo* oldBatInfo = d_batGame->battleInfo();
   if(oldBatInfo->battleWidth() >= batInfo.battleWidth())
   {
      int newW = minimum(96, oldBatInfo->battleWidth() + 8);
      batInfo.battleWidth(newW);
   }

   if(oldBatInfo->battleHeight() >= batInfo.battleHeight())
   {
      int newH = minimum(82, oldBatInfo->battleHeight() + 8);
      batInfo.battleHeight(newH);
   }

   // let battle game take over
   d_batGame->reinforced(batInfo);

//   GameControl::pause(oldPause);

}

void CampaignBattleGameImp::newDay()
{
    d_batGame->newDay();
}


bool CampaignBattleGameImp::readData(FileReader& f, CampaignBattle* battle, CampaignTacticalOwner* game)
{
    d_battle = battle;
    d_campGame = game;
    d_batGame = new BattleGame(this, f, game->getCampaignData()->getArmies().ob());

    // BattleData* batData = d_batGame->battleData();
    // CampaignData* campData = game->getCampaignData();

    // Read BattleData supplying our own Generic Order of Battle
    // And setup deployment map

    // batData->readData(f, campData->getArmies().ob(), true);

    return f.isOK();
}

bool CampaignBattleGameImp::writeData(FileWriter& f)
{
    return d_batGame->writeData(f, false);
}

void CampaignBattleGameImp::resume()
{
    // todo
    d_batGame->resume();
}



/*
 * Interface
 */

CampaignBattleGame::CampaignBattleGame()
{
        d_imp = new CampaignBattleGameImp;
}

CampaignBattleGame::~CampaignBattleGame()
{
        delete d_imp;
}

GameTick CampaignBattleGame::addTime(SysTick::Value ticks)
{
        return d_imp->addTime(ticks);
}

void CampaignBattleGame::start(CampaignBattle* battle, CampaignTacticalOwner* game)
{
        d_imp->start(battle, game);
}

void CampaignBattleGame::reinforce()
{
   d_imp->reinforce();
}

bool CampaignBattleGame::readData(FileReader& f, CampaignBattle* battle, CampaignTacticalOwner* game)
{
    return d_imp->readData(f, battle, game);
}

bool CampaignBattleGame::writeData(FileWriter& f)
{
    return d_imp->writeData(f);
}

void CampaignBattleGame::resume()
{
    d_imp->resume();
}

void CampaignBattleGame::newDay()
{
    d_imp->newDay();
}

CampaignBattle* CampaignBattleGame::campaignBattle()
{
    return d_imp->campaignBattle();
}

void CampaignBattleGame::processBattleOrder(MP_MSG_BATTLEORDER * msg)
{
   d_imp->processBattleOrder(msg);
}

/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.2  2001/06/18 08:25:44  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
