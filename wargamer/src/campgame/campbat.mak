# Microsoft Developer Studio Generated NMAKE File, Based on campbat.dsp
!IF "$(CFG)" == ""
CFG=campbat - Win32 Debug
!MESSAGE No configuration specified. Defaulting to campbat - Win32 Debug.
!ENDIF 

!IF "$(CFG)" != "campbat - Win32 Release" && "$(CFG)" != "campbat - Win32 Debug"
!MESSAGE Invalid configuration "$(CFG)" specified.
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "campbat.mak" CFG="campbat - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "campbat - Win32 Release" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE "campbat - Win32 Debug" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE 
!ERROR An invalid configuration is specified.
!ENDIF 

!IF "$(OS)" == "Windows_NT"
NULL=
!ELSE 
NULL=nul
!ENDIF 

CPP=cl.exe
MTL=midl.exe
RSC=rc.exe

!IF  "$(CFG)" == "campbat - Win32 Release"

OUTDIR=.\..\..\exe
INTDIR=.\o\release
# Begin Custom Macros
OutDir=.\..\..\exe
# End Custom Macros

!IF "$(RECURSE)" == "0" 

ALL : "$(OUTDIR)\campbat.dll"

!ELSE 

ALL : "gamesup - Win32 Release" "batgame - Win32 Release" "system - Win32 Release" "ob - Win32 Release" "campdata - Win32 Release" "batdata - Win32 Release" "$(OUTDIR)\campbat.dll"

!ENDIF 

!IF "$(RECURSE)" == "1" 
CLEAN :"batdata - Win32 ReleaseCLEAN" "campdata - Win32 ReleaseCLEAN" "ob - Win32 ReleaseCLEAN" "system - Win32 ReleaseCLEAN" "batgame - Win32 ReleaseCLEAN" "gamesup - Win32 ReleaseCLEAN" 
!ELSE 
CLEAN :
!ENDIF 
	-@erase "$(INTDIR)\batentry.obj"
	-@erase "$(INTDIR)\campbat.obj"
	-@erase "$(INTDIR)\cbat_ob.obj"
	-@erase "$(INTDIR)\vc60.idb"
	-@erase "$(OUTDIR)\campbat.dll"
	-@erase "$(OUTDIR)\campbat.exp"
	-@erase "$(OUTDIR)\campbat.lib"
	-@erase "$(OUTDIR)\campbat.pdb"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

"$(INTDIR)" :
    if not exist "$(INTDIR)/$(NULL)" mkdir "$(INTDIR)"

CPP_PROJ=/nologo /MD /W3 /GR /GX /Zd /O2 /I "..\h" /I "..\..\res" /I "..\system" /I "..\gamesup" /I "..\ob" /I "..\campdata" /I "..\batdata" /I "..\batgame" /D "NDEBUG" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "EXPORT_CAMPBAT_DLL" /Fp"$(INTDIR)\campbat.pch" /YX"stdinc.hpp" /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /c 
MTL_PROJ=/nologo /D "NDEBUG" /mktyplib203 /win32 
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\campbat.bsc" 
BSC32_SBRS= \
	
LINK32=link.exe
LINK32_FLAGS=/nologo /dll /incremental:no /pdb:"$(OUTDIR)\campbat.pdb" /debug /machine:I386 /out:"$(OUTDIR)\campbat.dll" /implib:"$(OUTDIR)\campbat.lib" 
LINK32_OBJS= \
	"$(INTDIR)\batentry.obj" \
	"$(INTDIR)\campbat.obj" \
	"$(INTDIR)\cbat_ob.obj" \
	"$(OUTDIR)\batdata.lib" \
	"$(OUTDIR)\campdata.lib" \
	"$(OUTDIR)\ob.lib" \
	"$(OUTDIR)\system.lib" \
	"$(OUTDIR)\batgame.lib" \
	"$(OUTDIR)\gamesup.lib"

"$(OUTDIR)\campbat.dll" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

!ELSEIF  "$(CFG)" == "campbat - Win32 Debug"

OUTDIR=.\..\..\exe
INTDIR=.\o\debug
# Begin Custom Macros
OutDir=.\..\..\exe
# End Custom Macros

!IF "$(RECURSE)" == "0" 

ALL : "$(OUTDIR)\campbatDB.dll"

!ELSE 

ALL : "gamesup - Win32 Debug" "batgame - Win32 Debug" "system - Win32 Debug" "ob - Win32 Debug" "campdata - Win32 Debug" "batdata - Win32 Debug" "$(OUTDIR)\campbatDB.dll"

!ENDIF 

!IF "$(RECURSE)" == "1" 
CLEAN :"batdata - Win32 DebugCLEAN" "campdata - Win32 DebugCLEAN" "ob - Win32 DebugCLEAN" "system - Win32 DebugCLEAN" "batgame - Win32 DebugCLEAN" "gamesup - Win32 DebugCLEAN" 
!ELSE 
CLEAN :
!ENDIF 
	-@erase "$(INTDIR)\batentry.obj"
	-@erase "$(INTDIR)\campbat.obj"
	-@erase "$(INTDIR)\cbat_ob.obj"
	-@erase "$(INTDIR)\vc60.idb"
	-@erase "$(INTDIR)\vc60.pdb"
	-@erase "$(OUTDIR)\campbatDB.dll"
	-@erase "$(OUTDIR)\campbatDB.exp"
	-@erase "$(OUTDIR)\campbatDB.ilk"
	-@erase "$(OUTDIR)\campbatDB.lib"
	-@erase "$(OUTDIR)\campbatDB.pdb"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

"$(INTDIR)" :
    if not exist "$(INTDIR)/$(NULL)" mkdir "$(INTDIR)"

CPP_PROJ=/nologo /MDd /W3 /Gm /GR /GX /ZI /Od /I "..\h" /I "..\..\res" /I "..\system" /I "..\gamesup" /I "..\ob" /I "..\campdata" /I "..\batdata" /I "..\batgame" /D "_DEBUG" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "EXPORT_CAMPBAT_DLL" /Fp"$(INTDIR)\campbat.pch" /YX"stdinc.hpp" /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /GZ /c 
MTL_PROJ=/nologo /D "_DEBUG" /mktyplib203 /win32 
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\campbat.bsc" 
BSC32_SBRS= \
	
LINK32=link.exe
LINK32_FLAGS=/nologo /dll /incremental:yes /pdb:"$(OUTDIR)\campbatDB.pdb" /debug /machine:I386 /out:"$(OUTDIR)\campbatDB.dll" /implib:"$(OUTDIR)\campbatDB.lib" /pdbtype:sept 
LINK32_OBJS= \
	"$(INTDIR)\batentry.obj" \
	"$(INTDIR)\campbat.obj" \
	"$(INTDIR)\cbat_ob.obj" \
	"$(OUTDIR)\batdataDB.lib" \
	"$(OUTDIR)\campdataDB.lib" \
	"$(OUTDIR)\obDB.lib" \
	"$(OUTDIR)\systemDB.lib" \
	"$(OUTDIR)\batgameDB.lib" \
	"$(OUTDIR)\gamesupDB.lib"

"$(OUTDIR)\campbatDB.dll" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

!ENDIF 

.c{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.c{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<


!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("campbat.dep")
!INCLUDE "campbat.dep"
!ELSE 
!MESSAGE Warning: cannot find "campbat.dep"
!ENDIF 
!ENDIF 


!IF "$(CFG)" == "campbat - Win32 Release" || "$(CFG)" == "campbat - Win32 Debug"
SOURCE=.\batentry.cpp

"$(INTDIR)\batentry.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\campbat.cpp

"$(INTDIR)\campbat.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\cbat_ob.cpp

"$(INTDIR)\cbat_ob.obj" : $(SOURCE) "$(INTDIR)"


!IF  "$(CFG)" == "campbat - Win32 Release"

"batdata - Win32 Release" : 
   d:
   cd "\SRC\SOURCEFORGE\WARGAMER\SRC\batdata"
   $(MAKE) /$(MAKEFLAGS) /F .\batdata.mak CFG="batdata - Win32 Release" 
   D:
   cd "..\campgame"

"batdata - Win32 ReleaseCLEAN" : 
   d:
   cd "\SRC\SOURCEFORGE\WARGAMER\SRC\batdata"
   $(MAKE) /$(MAKEFLAGS) /F .\batdata.mak CFG="batdata - Win32 Release" RECURSE=1 CLEAN 
   D:
   cd "..\campgame"

!ELSEIF  "$(CFG)" == "campbat - Win32 Debug"

"batdata - Win32 Debug" : 
   d:
   cd "\SRC\SOURCEFORGE\WARGAMER\SRC\batdata"
   $(MAKE) /$(MAKEFLAGS) /F .\batdata.mak CFG="batdata - Win32 Debug" 
   D:
   cd "..\campgame"

"batdata - Win32 DebugCLEAN" : 
   d:
   cd "\SRC\SOURCEFORGE\WARGAMER\SRC\batdata"
   $(MAKE) /$(MAKEFLAGS) /F .\batdata.mak CFG="batdata - Win32 Debug" RECURSE=1 CLEAN 
   D:
   cd "..\campgame"

!ENDIF 

!IF  "$(CFG)" == "campbat - Win32 Release"

"campdata - Win32 Release" : 
   d:
   cd "\SRC\SOURCEFORGE\WARGAMER\src\campdata"
   $(MAKE) /$(MAKEFLAGS) /F .\campdata.mak CFG="campdata - Win32 Release" 
   D:
   cd "..\campgame"

"campdata - Win32 ReleaseCLEAN" : 
   d:
   cd "\SRC\SOURCEFORGE\WARGAMER\src\campdata"
   $(MAKE) /$(MAKEFLAGS) /F .\campdata.mak CFG="campdata - Win32 Release" RECURSE=1 CLEAN 
   D:
   cd "..\campgame"

!ELSEIF  "$(CFG)" == "campbat - Win32 Debug"

"campdata - Win32 Debug" : 
   d:
   cd "\SRC\SOURCEFORGE\WARGAMER\src\campdata"
   $(MAKE) /$(MAKEFLAGS) /F .\campdata.mak CFG="campdata - Win32 Debug" 
   D:
   cd "..\campgame"

"campdata - Win32 DebugCLEAN" : 
   d:
   cd "\SRC\SOURCEFORGE\WARGAMER\src\campdata"
   $(MAKE) /$(MAKEFLAGS) /F .\campdata.mak CFG="campdata - Win32 Debug" RECURSE=1 CLEAN 
   D:
   cd "..\campgame"

!ENDIF 

!IF  "$(CFG)" == "campbat - Win32 Release"

"ob - Win32 Release" : 
   d:
   cd "\SRC\SOURCEFORGE\WARGAMER\SRC\OB"
   $(MAKE) /$(MAKEFLAGS) /F .\ob.mak CFG="ob - Win32 Release" 
   D:
   cd "..\campgame"

"ob - Win32 ReleaseCLEAN" : 
   d:
   cd "\SRC\SOURCEFORGE\WARGAMER\SRC\OB"
   $(MAKE) /$(MAKEFLAGS) /F .\ob.mak CFG="ob - Win32 Release" RECURSE=1 CLEAN 
   D:
   cd "..\campgame"

!ELSEIF  "$(CFG)" == "campbat - Win32 Debug"

"ob - Win32 Debug" : 
   d:
   cd "\SRC\SOURCEFORGE\WARGAMER\SRC\OB"
   $(MAKE) /$(MAKEFLAGS) /F .\ob.mak CFG="ob - Win32 Debug" 
   D:
   cd "..\campgame"

"ob - Win32 DebugCLEAN" : 
   d:
   cd "\SRC\SOURCEFORGE\WARGAMER\SRC\OB"
   $(MAKE) /$(MAKEFLAGS) /F .\ob.mak CFG="ob - Win32 Debug" RECURSE=1 CLEAN 
   D:
   cd "..\campgame"

!ENDIF 

!IF  "$(CFG)" == "campbat - Win32 Release"

"system - Win32 Release" : 
   d:
   cd "\SRC\SOURCEFORGE\WARGAMER\SRC\SYSTEM"
   $(MAKE) /$(MAKEFLAGS) /F .\system.mak CFG="system - Win32 Release" 
   D:
   cd "..\campgame"

"system - Win32 ReleaseCLEAN" : 
   d:
   cd "\SRC\SOURCEFORGE\WARGAMER\SRC\SYSTEM"
   $(MAKE) /$(MAKEFLAGS) /F .\system.mak CFG="system - Win32 Release" RECURSE=1 CLEAN 
   D:
   cd "..\campgame"

!ELSEIF  "$(CFG)" == "campbat - Win32 Debug"

"system - Win32 Debug" : 
   d:
   cd "\SRC\SOURCEFORGE\WARGAMER\SRC\SYSTEM"
   $(MAKE) /$(MAKEFLAGS) /F .\system.mak CFG="system - Win32 Debug" 
   D:
   cd "..\campgame"

"system - Win32 DebugCLEAN" : 
   d:
   cd "\SRC\SOURCEFORGE\WARGAMER\SRC\SYSTEM"
   $(MAKE) /$(MAKEFLAGS) /F .\system.mak CFG="system - Win32 Debug" RECURSE=1 CLEAN 
   D:
   cd "..\campgame"

!ENDIF 

!IF  "$(CFG)" == "campbat - Win32 Release"

"batgame - Win32 Release" : 
   d:
   cd "\SRC\SOURCEFORGE\WARGAMER\src\batgame"
   $(MAKE) /$(MAKEFLAGS) /F .\batgame.mak CFG="batgame - Win32 Release" 
   D:
   cd "..\campgame"

"batgame - Win32 ReleaseCLEAN" : 
   d:
   cd "\SRC\SOURCEFORGE\WARGAMER\src\batgame"
   $(MAKE) /$(MAKEFLAGS) /F .\batgame.mak CFG="batgame - Win32 Release" RECURSE=1 CLEAN 
   D:
   cd "..\campgame"

!ELSEIF  "$(CFG)" == "campbat - Win32 Debug"

"batgame - Win32 Debug" : 
   d:
   cd "\SRC\SOURCEFORGE\WARGAMER\src\batgame"
   $(MAKE) /$(MAKEFLAGS) /F .\batgame.mak CFG="batgame - Win32 Debug" 
   D:
   cd "..\campgame"

"batgame - Win32 DebugCLEAN" : 
   d:
   cd "\SRC\SOURCEFORGE\WARGAMER\src\batgame"
   $(MAKE) /$(MAKEFLAGS) /F .\batgame.mak CFG="batgame - Win32 Debug" RECURSE=1 CLEAN 
   D:
   cd "..\campgame"

!ENDIF 

!IF  "$(CFG)" == "campbat - Win32 Release"

"gamesup - Win32 Release" : 
   d:
   cd "\SRC\SOURCEFORGE\WARGAMER\SRC\GAMESUP"
   $(MAKE) /$(MAKEFLAGS) /F .\gamesup.mak CFG="gamesup - Win32 Release" 
   D:
   cd "..\campgame"

"gamesup - Win32 ReleaseCLEAN" : 
   d:
   cd "\SRC\SOURCEFORGE\WARGAMER\SRC\GAMESUP"
   $(MAKE) /$(MAKEFLAGS) /F .\gamesup.mak CFG="gamesup - Win32 Release" RECURSE=1 CLEAN 
   D:
   cd "..\campgame"

!ELSEIF  "$(CFG)" == "campbat - Win32 Debug"

"gamesup - Win32 Debug" : 
   d:
   cd "\SRC\SOURCEFORGE\WARGAMER\SRC\GAMESUP"
   $(MAKE) /$(MAKEFLAGS) /F .\gamesup.mak CFG="gamesup - Win32 Debug" 
   D:
   cd "..\campgame"

"gamesup - Win32 DebugCLEAN" : 
   d:
   cd "\SRC\SOURCEFORGE\WARGAMER\SRC\GAMESUP"
   $(MAKE) /$(MAKEFLAGS) /F .\gamesup.mak CFG="gamesup - Win32 Debug" RECURSE=1 CLEAN 
   D:
   cd "..\campgame"

!ENDIF 


!ENDIF 

