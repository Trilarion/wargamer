# Microsoft Developer Studio Generated NMAKE File, Based on campbatn.dsp
!IF "$(CFG)" == ""
CFG=campbatn - Win32 Debug
!MESSAGE No configuration specified. Defaulting to campbatn - Win32 Debug.
!ENDIF 

!IF "$(CFG)" != "campbatn - Win32 Release" && "$(CFG)" != "campbatn - Win32 Debug"
!MESSAGE Invalid configuration "$(CFG)" specified.
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "campbatn.mak" CFG="campbatn - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "campbatn - Win32 Release" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE "campbatn - Win32 Debug" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE 
!ERROR An invalid configuration is specified.
!ENDIF 

!IF "$(OS)" == "Windows_NT"
NULL=
!ELSE 
NULL=nul
!ENDIF 

CPP=cl.exe
MTL=midl.exe
RSC=rc.exe

!IF  "$(CFG)" == "campbatn - Win32 Release"

OUTDIR=.\..\..\exe
INTDIR=.\o\release
# Begin Custom Macros
OutDir=.\..\..\exe
# End Custom Macros

!IF "$(RECURSE)" == "0" 

ALL : "$(OUTDIR)\campbatn.dll"

!ELSE 

ALL : "system - Win32 Release" "campdata - Win32 Release" "ob - Win32 Release" "$(OUTDIR)\campbatn.dll"

!ENDIF 

!IF "$(RECURSE)" == "1" 
CLEAN :"ob - Win32 ReleaseCLEAN" "campdata - Win32 ReleaseCLEAN" "system - Win32 ReleaseCLEAN" 
!ELSE 
CLEAN :
!ENDIF 
	-@erase "$(INTDIR)\campbatn.obj"
	-@erase "$(INTDIR)\vc60.idb"
	-@erase "$(OUTDIR)\campbatn.dll"
	-@erase "$(OUTDIR)\campbatn.exp"
	-@erase "$(OUTDIR)\campbatn.lib"
	-@erase "$(OUTDIR)\campbatn.pdb"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

"$(INTDIR)" :
    if not exist "$(INTDIR)/$(NULL)" mkdir "$(INTDIR)"

CPP_PROJ=/nologo /MD /W3 /GR /GX /Zd /O2 /I "..\h" /I "..\..\res" /I "..\system" /I "..\gamesup" /I "..\ob" /I "..\campdata" /D "NDEBUG" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "EXPORT_CAMPBATN_DLL" /Fp"$(INTDIR)\campbatn.pch" /YX"stdinc.hpp" /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /c 
MTL_PROJ=/nologo /D "NDEBUG" /mktyplib203 /win32 
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\campbatn.bsc" 
BSC32_SBRS= \
	
LINK32=link.exe
LINK32_FLAGS=/nologo /dll /incremental:no /pdb:"$(OUTDIR)\campbatn.pdb" /debug /machine:I386 /out:"$(OUTDIR)\campbatn.dll" /implib:"$(OUTDIR)\campbatn.lib" 
LINK32_OBJS= \
	"$(INTDIR)\campbatn.obj" \
	"$(OUTDIR)\ob.lib" \
	"$(OUTDIR)\campdata.lib" \
	"$(OUTDIR)\system.lib"

"$(OUTDIR)\campbatn.dll" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

!ELSEIF  "$(CFG)" == "campbatn - Win32 Debug"

OUTDIR=.\..\..\exe
INTDIR=.\o\debug
# Begin Custom Macros
OutDir=.\..\..\exe
# End Custom Macros

!IF "$(RECURSE)" == "0" 

ALL : "$(OUTDIR)\campbatnDB.dll"

!ELSE 

ALL : "system - Win32 Debug" "campdata - Win32 Debug" "ob - Win32 Debug" "$(OUTDIR)\campbatnDB.dll"

!ENDIF 

!IF "$(RECURSE)" == "1" 
CLEAN :"ob - Win32 DebugCLEAN" "campdata - Win32 DebugCLEAN" "system - Win32 DebugCLEAN" 
!ELSE 
CLEAN :
!ENDIF 
	-@erase "$(INTDIR)\campbatn.obj"
	-@erase "$(INTDIR)\vc60.idb"
	-@erase "$(INTDIR)\vc60.pdb"
	-@erase "$(OUTDIR)\campbatnDB.dll"
	-@erase "$(OUTDIR)\campbatnDB.exp"
	-@erase "$(OUTDIR)\campbatnDB.ilk"
	-@erase "$(OUTDIR)\campbatnDB.lib"
	-@erase "$(OUTDIR)\campbatnDB.pdb"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

"$(INTDIR)" :
    if not exist "$(INTDIR)/$(NULL)" mkdir "$(INTDIR)"

CPP_PROJ=/nologo /MDd /W3 /Gm /GR /GX /ZI /Od /I "..\h" /I "..\..\res" /I "..\system" /I "..\gamesup" /I "..\ob" /I "..\campdata" /D "_DEBUG" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "EXPORT_CAMPBATN_DLL" /Fp"$(INTDIR)\campbatn.pch" /YX"stdinc.hpp" /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /GZ /c 
MTL_PROJ=/nologo /D "_DEBUG" /mktyplib203 /win32 
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\campbatn.bsc" 
BSC32_SBRS= \
	
LINK32=link.exe
LINK32_FLAGS=/nologo /dll /incremental:yes /pdb:"$(OUTDIR)\campbatnDB.pdb" /debug /machine:I386 /out:"$(OUTDIR)\campbatnDB.dll" /implib:"$(OUTDIR)\campbatnDB.lib" /pdbtype:sept 
LINK32_OBJS= \
	"$(INTDIR)\campbatn.obj" \
	"$(OUTDIR)\obDB.lib" \
	"$(OUTDIR)\campdataDB.lib" \
	"$(OUTDIR)\systemDB.lib"

"$(OUTDIR)\campbatnDB.dll" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

!ENDIF 

.c{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.c{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<


!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("campbatn.dep")
!INCLUDE "campbatn.dep"
!ELSE 
!MESSAGE Warning: cannot find "campbatn.dep"
!ENDIF 
!ENDIF 


!IF "$(CFG)" == "campbatn - Win32 Release" || "$(CFG)" == "campbatn - Win32 Debug"
SOURCE=.\campbatn.cpp

"$(INTDIR)\campbatn.obj" : $(SOURCE) "$(INTDIR)"


!IF  "$(CFG)" == "campbatn - Win32 Release"

"ob - Win32 Release" : 
   d:
   cd "\SRC\SOURCEFORGE\WARGAMER\SRC\OB"
   $(MAKE) /$(MAKEFLAGS) /F .\ob.mak CFG="ob - Win32 Release" 
   D:
   cd "..\campgame"

"ob - Win32 ReleaseCLEAN" : 
   d:
   cd "\SRC\SOURCEFORGE\WARGAMER\SRC\OB"
   $(MAKE) /$(MAKEFLAGS) /F .\ob.mak CFG="ob - Win32 Release" RECURSE=1 CLEAN 
   D:
   cd "..\campgame"

!ELSEIF  "$(CFG)" == "campbatn - Win32 Debug"

"ob - Win32 Debug" : 
   d:
   cd "\SRC\SOURCEFORGE\WARGAMER\SRC\OB"
   $(MAKE) /$(MAKEFLAGS) /F .\ob.mak CFG="ob - Win32 Debug" 
   D:
   cd "..\campgame"

"ob - Win32 DebugCLEAN" : 
   d:
   cd "\SRC\SOURCEFORGE\WARGAMER\SRC\OB"
   $(MAKE) /$(MAKEFLAGS) /F .\ob.mak CFG="ob - Win32 Debug" RECURSE=1 CLEAN 
   D:
   cd "..\campgame"

!ENDIF 

!IF  "$(CFG)" == "campbatn - Win32 Release"

"campdata - Win32 Release" : 
   d:
   cd "\SRC\SOURCEFORGE\WARGAMER\src\campdata"
   $(MAKE) /$(MAKEFLAGS) /F .\campdata.mak CFG="campdata - Win32 Release" 
   D:
   cd "..\campgame"

"campdata - Win32 ReleaseCLEAN" : 
   d:
   cd "\SRC\SOURCEFORGE\WARGAMER\src\campdata"
   $(MAKE) /$(MAKEFLAGS) /F .\campdata.mak CFG="campdata - Win32 Release" RECURSE=1 CLEAN 
   D:
   cd "..\campgame"

!ELSEIF  "$(CFG)" == "campbatn - Win32 Debug"

"campdata - Win32 Debug" : 
   d:
   cd "\SRC\SOURCEFORGE\WARGAMER\src\campdata"
   $(MAKE) /$(MAKEFLAGS) /F .\campdata.mak CFG="campdata - Win32 Debug" 
   D:
   cd "..\campgame"

"campdata - Win32 DebugCLEAN" : 
   d:
   cd "\SRC\SOURCEFORGE\WARGAMER\src\campdata"
   $(MAKE) /$(MAKEFLAGS) /F .\campdata.mak CFG="campdata - Win32 Debug" RECURSE=1 CLEAN 
   D:
   cd "..\campgame"

!ENDIF 

!IF  "$(CFG)" == "campbatn - Win32 Release"

"system - Win32 Release" : 
   d:
   cd "\SRC\SOURCEFORGE\WARGAMER\SRC\SYSTEM"
   $(MAKE) /$(MAKEFLAGS) /F .\system.mak CFG="system - Win32 Release" 
   D:
   cd "..\campgame"

"system - Win32 ReleaseCLEAN" : 
   d:
   cd "\SRC\SOURCEFORGE\WARGAMER\SRC\SYSTEM"
   $(MAKE) /$(MAKEFLAGS) /F .\system.mak CFG="system - Win32 Release" RECURSE=1 CLEAN 
   D:
   cd "..\campgame"

!ELSEIF  "$(CFG)" == "campbatn - Win32 Debug"

"system - Win32 Debug" : 
   d:
   cd "\SRC\SOURCEFORGE\WARGAMER\SRC\SYSTEM"
   $(MAKE) /$(MAKEFLAGS) /F .\system.mak CFG="system - Win32 Debug" 
   D:
   cd "..\campgame"

"system - Win32 DebugCLEAN" : 
   d:
   cd "\SRC\SOURCEFORGE\WARGAMER\SRC\SYSTEM"
   $(MAKE) /$(MAKEFLAGS) /F .\system.mak CFG="system - Win32 Debug" RECURSE=1 CLEAN 
   D:
   cd "..\campgame"

!ENDIF 


!ENDIF 

