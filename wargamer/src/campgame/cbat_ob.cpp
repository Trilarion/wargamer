/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 * Campaign To battle OB Conversions
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "cbat_ob.hpp"
#include "armies.hpp"
// #include "cbatint.hpp"
#include "b_ob_ran.hpp"
#include "myassert.hpp"
// #include "batdata.hpp"
#include "bobdef.hpp"
#include "batord.hpp"
#include "obdefs.hpp"
#include "batarmy.hpp"
#include "cp.hpp"
#include "cbattle.hpp"
#include "bobiter.hpp"
#include "wg_rand.hpp"

#ifdef DEBUG
#include "scenario.hpp"
#include "clog.hpp"
#endif

namespace CampaignBattleUtility
{

/*
 * Some useful inline functions
 */


inline int cb_MulDiv(unsigned int a, unsigned int b, unsigned int c)
{
    if(b == c)
        return a;
    else
        return (a * (1+b)) / (1+c);
}

inline BattleCP::Morale cToB_morale(Attribute m)
{
    return cb_MulDiv(m, BattleCP::Morale::MaxValue, Attribute_Range);
}

inline Attribute bToC_morale(BattleCP::Morale m)
{
    return cb_MulDiv(m.value(), Attribute_Range, BattleCP::Morale::MaxValue);
}

inline BattleCP::Fatigue cToB_fatigue(Attribute m)
{
    return cb_MulDiv(m, BattleCP::Fatigue::MaxValue, Attribute_Range);
}

inline Attribute bToC_fatigue(BattleCP::Fatigue m)
{
    return cb_MulDiv(m.value(), Attribute_Range, BattleCP::Fatigue::MaxValue);
}

inline int countSpecialUnits(ICommandPosition cp, OrderBattle* gob) {

   int num = 0;
   
   if(!cp->getRank().isHigher(Rank_Division)) {
      
      ASSERT(cp->getChild() == NoCommandPosition);

      G_StrengthPointIter d_spIter(gob);
      d_spIter.setSP(cp->getSPEntry());

      while(++d_spIter) {
         
         ISP sp = d_spIter.current();

         // if Engineer
         UnitType ut = sp->getUnitType();
         const UnitTypeItem& uti = gob->getUnitType(ut);
         
         if(uti.getBasicType() == BasicUnitType::Special) {

            num++;
         }
      }
   }

   return num;
}


inline int
countSP(ICommandPosition cp, OrderBattle* gob) {

   int num = 0;
   
   if(!cp->getRank().isHigher(Rank_Division)) {
      
      ASSERT(cp->getChild() == NoCommandPosition);

      G_StrengthPointIter d_spIter(gob);
      d_spIter.setSP(cp->getSPEntry());

      while(++d_spIter) { num++; }
   }

   return num;
}


#ifdef DEBUG
LogFile log("cbat_ob.log");
#endif

class UnitConvertor
{
   public:
      UnitConvertor(BattleOB* bob, OrderBattle* gob);
      void addUnits(RefBattleCP parent, ICommandPosition cp);
   private:
      G_StrengthPointIter d_spIter;
      BattleOB* d_bob;
};

/*
 * recursively add units
 */

UnitConvertor::UnitConvertor(BattleOB* bob, OrderBattle* gob) :
   d_spIter(gob),
   d_bob(bob)
{
}







void UnitConvertor::addUnits(RefBattleCP parent, ICommandPosition cp)
{
#ifdef DEBUG
    log << "Adding CP: " << cp->getName() << endl;
#endif

    RefGenericCP gcp = cp->generic();

    RefBattleCP bcp = d_bob->makeCP(gcp);
    d_bob->addChild(parent, bcp);
#if 0
    if(reinforcement)
    {
       // set a temporary entry point so campbat will know who are reinforcements
       bcp->entryPoint(REP_Rear);
    }
#endif

    /*
     * Todo:
     *      set Morale and fatigue levels
     *      Keep internal note of starting values or map to battle unit?
     */

    bcp->morale( cToB_morale(cp->getMorale()) );
    bcp->fatigue( cToB_fatigue(cp->getFatigue()) );

    // unfortunetly battle posture and campaign posture are reversed
    bcp->posture( (cp->posture() == Orders::Posture::Offensive) ? BattleOrderInfo::Offensive : BattleOrderInfo::Defensive);
    bcp->aggression( static_cast<BattleOrderInfo::Aggression>(cp->getActiveAggression()));

#ifdef DEBUG
    log << "Morale=" << static_cast<int>(bcp->morale()) << "/" << static_cast<int>(cp->getMorale());
    log << ", Fatigue=" << static_cast<int>(bcp->fatigue()) << "/" << static_cast<int>(cp->getFatigue());
    log << endl;
#endif

   // Add Strength Points

    /*
     * Temporary: ignore SPs attached directly to anything above division
     * Unless we can convince Ben otherwise, then a temporary new division
     * must be created for the battle.
     */


    if(cp->getRank().isHigher(Rank_Division))
    {
#ifdef DEBUG
        log << "Ignoring SPs attached to " << cp->getName() << endl;
#endif
    }
    else
    {
        ASSERT(cp->getChild() == NoCommandPosition);

       d_spIter.setSP(cp->getSPEntry());
       while(++d_spIter)
       {
          ISP sp = d_spIter.current();

            // Ignore if Engineer

            UnitType ut = sp->getUnitType();
            const UnitTypeItem& uti = d_bob->getUnitType(ut);

            if(uti.getBasicType() == BasicUnitType::Special)
            {
#ifdef DEBUG
                log << "Skipping";
#endif
            }
            else
            {
#ifdef DEBUG
                log << "Adding";
#endif

             RefBattleSP bsp = d_bob->makeSP(sp);
              bcp->addSP(bsp);

                /*
                 * Todo:
                 *      set Morale and fatigue levels
                 *      Keep internal note of starting values or map to battle unit?
                 */

            }
#ifdef DEBUG
            log << " SP: " << d_bob->getUnitType(sp->getUnitType()).getName() << endl;
#endif

       }
    }

   // Add children

   ICommandPosition child = cp->getChild();
   while(child != NoCommandPosition)
   {
      addUnits(bcp, child);
      child = child->getSister();
   }
}

/*
 * Create BattleOB
 */

BattleOB* makeBattleOB(CampaignBattle* campBat, Armies* campArmy)
{
   OrderBattle* gob = campArmy->ob();
   BattleOB* ob = new BattleOB(gob);

   UnitConvertor convertor(ob, gob);

   /*
    * Now copy stuff from CampaignBattle into the battle OB
    */

   for(Side side = 0; side < ob->sideCount(); ++side)
   {
#ifdef DEBUG
        log << "---------------------" << endl;
        log << "Exporting " << scenario->getSideName(side) << " side" << endl;
        log << "---------------------" << endl;
#endif
      const CampaignBattleUnitList& units = campBat->units(side);

      CampaignBattleUnitListIterR iter(&units);
      while(++iter)
      {
         const CampaignBattleUnit* bu = iter.current();
         ICommandPosition cp = bu->getUnit();

         convertor.addUnits(NoBattleCP, cp);
      }
   }

#ifdef DEBUG
    log.close();
#endif

    return ob;
}

void addReinforcements(CampaignBattle* campBat, Armies* campArmy, BattleOB* ob)
{
   OrderBattle* gob = campArmy->ob();
   UnitConvertor convertor(ob, gob);

   for(Side side = 0; side < ob->sideCount(); ++side)
   {
#ifdef DEBUG
        log << "---------------------" << endl;
        log << "Exporting " << scenario->getSideName(side) << " side" << endl;
        log << "---------------------" << endl;
#endif
      CampaignBattleUnitList& units = campBat->units(side);

      SListIter<CampaignBattleUnit> iter(&units);
      while(++iter)
      {
         CampaignBattleUnit* bu = iter.current();

         if(bu->reinforcement())
         {
            ICommandPosition cp = bu->getUnit();

            int num_special = countSpecialUnits(cp, gob);
            int num_sp = countSP(cp, gob);
            /*
            * If this unit's SP are entirely Engineers, etc, then don't add
            */
            if((num_special == num_sp) && (num_special > 0)) {

               bu->reinforcement(False);
            }

            /*
            * Otherwise, add units
            */
            else {
            
               convertor.addUnits(NoBattleCP, cp);
            }
         }
      }
   }

}




/*
 * Copy info back from Battle into Campaign
 *
 * This is not as easy as it sounds.
 *
 * When SP are killed in the battle, their SPs are removed from the BattleOB
 * When CP are killed in battle then their CPs are removed from the BattleOB
 *
 * Could it be possible for CampaignOB to get changed during a battle?
 * Logic should ensure that this is not possible... delay attach orders
 * if units are "inBattle"... or ensure newly attached units are also in OB.
 */

void restoreOB(CampaignBattle* campBat, Armies* campArmy, BattleOB* bob)
{
   OrderBattle* gob = campArmy->ob();

#ifdef DEBUG
    int spDead = 0;             // number of completely dead SPs
    int spFled = 0;             // number of fled SPs
    int spTotalStrength = 0;    // Total Strengths of all SPs
    int nSP = 0;                // Number of original battle SPs
#endif

    /*
     * List of Leaders to remove
     */

    // typedef vector<ILeader> LeaderList;
    // LeaderList killedLeaders;

    typedef std::vector<ICommandPosition> DeadCPList;
    DeadCPList killedCPs;

    /*
     * List of SPs to remove
     */

    typedef std::vector<RefBattleSP> KillSPList;
    KillSPList killedSPs;

   // for(Side side = 0; side < bob->sideCount(); ++side)

    OBSideIter sideIter(gob);
    while(++sideIter)
   {
        Side side = sideIter.current();

#ifdef DEBUG
        log << "---------------------" << endl;
        log << "Importing " << scenario->getSideName(side) << " side" << endl;
        log << "---------------------" << endl;
#endif

      const CampaignBattleUnitList& units = campBat->units(side);

      CampaignBattleUnitListIterR iter(&units);
      while(++iter)
      {
         const CampaignBattleUnit* bu = iter.current();

            UnitIter iter(campArmy, bu->getUnit());
            while(iter.next())
            {
                ICommandPosition cp = iter.current();
            RefGenericCP gcp = cp->generic();
                RefBattleCP bcp = battleCP(gcp);

                if(bcp != NoBattleCP) {

#ifdef DEBUG
               log << "Importing " << cp->getName() << endl;
               log << (bcp->isDead() ? " Dead" : " Alive");
               if(bcp->hasFled())
                  log << " Fled";
               log << ", Morale=" << static_cast<int>(bcp->morale()) << "/" << static_cast<int>(cp->getMorale());
               log << ", Fatigue=" << static_cast<int>(bcp->fatigue()) << "/" << static_cast<int>(cp->getFatigue());
               log << endl;
#endif

               if(bcp->isDead())
               {
                  // Remove Organization if dead?
                  // Or does this mean that leader is dead?
                  // if it means leader then:
                  //      add to list of dead leaders
                  //      remove dead leaders in a seperate pass at the end

                  // ILeader leader = cp->getLeader();
                  // killedLeaders.push_back(leader);

                  killedCPs.push_back(cp);

               }

                
               /*
                * Look at Battle SP's
                *
                * SPs will be marked as dead during the battle result calculation
                * so this is as simple as just removing SPs that are dead.
                *
                * Build up a list of SPs to delete so can be removed in seperate
                * phase later so as not to break the iterator.
                */


               for(TBattleSPIter<SPValidateAll> spIter(bob, bcp);
                  !spIter.isFinished();
                  spIter.next())
               {
                  RefBattleSP sp = spIter.sp();
                  // ISP gsp = sp->generic();

#ifdef DEBUG
                  log << "BatSP: " << bob->getUnitType(sp).getName() << endl;
                  log << (sp->isDead() ? " Dead" : " Alive");
                  if(sp->hasFled())
                     log << " Fled";
                  log << ", Strength=" << sp->strength(100) << "%";
                  log << endl;

                  if(sp->isDead())
                     ++spDead;
                  else if(sp->hasFled())
                     ++spFled;
                  else
                     spTotalStrength += sp->strength(100);

                  ++nSP;
#endif

                  /*
                   * See if SP should die by using strength as probability
                   *
                   * Artillery only die if strength is 0
                   */

                  if(!sp->isDead())
                  {
                     if(sp->strength() == 0)
                        sp->setDead();
                     else if(sp->strength() != BattleSP::SPStrength::MaxValue)
                     {
                        const UnitTypeItem& uti = bob->getUnitType(sp);
                        BasicUnitType::value ut = uti.getBasicType();
                        if(ut != BasicUnitType::Artillery)
                        {
                           int r = CRandom::get(BattleSP::SPStrength::MaxValue);

                           if(r > sp->strength())
                           {
                                       sp->setDead();
#ifdef DEBUG
                                      log << "Killed: ";
#endif
                                 }
#ifdef DEBUG
                           else
                              log << "Not killed: ";

                           log << "r = " << r << endl;
#endif
                               }
                          }
                     }



                  if(sp->isDead())
                  {
                     killedSPs.push_back(sp);
                  }

                   }


#ifdef DEBUG
               // Show SPs from Campaign point of view

               StrengthPointIter cspIter(campArmy, cp);
               while(++cspIter)
               {
                  ISP sp = cspIter.currentISP();
                  log << "CampSP: " << campArmy->getUnitType(sp->getUnitType()).getName();
                  if(!sp->battleInfo())
                     log << " Not";
                  log << " in battle" << endl;
                  // ASSERT(battleSP(sp) != 0);
               }
#endif
            } // if(bcp != NoBattleCP)
            }
      }
   }

#ifdef DEBUG
    // Display the lists
#if 0
    {
        log << "-------" << endl;
        log << killedLeaders.size() << " Leaders were killed" << endl;

        for(LeaderList::iterator it = killedLeaders.begin();
            it != killedLeaders.end();
            ++it)
        {
            ILeader leader = *it;
            log << leader->getName() << " of " << leader->command()->getName() << " was killed" << endl;
        }
    }
#endif
    {
        log << "-------" << endl;
        log << killedCPs.size() << " CPs were killed" << endl;

        for(DeadCPList::iterator it = killedCPs.begin();
            it != killedCPs.end();
            ++it)
        {
            ICommandPosition cp = *it;
            log << cp->getName() << " was killed" << endl;
        }
    }
    {
        log << "-------" << endl;
        log << killedSPs.size() << " SPs were killed" << endl;

        for(KillSPList::iterator it = killedSPs.begin();
            it != killedSPs.end();
            ++it)
        {
            RefBattleSP bsp = *it;
            RefBattleCP bcp = bsp->parent();

            log << bob->getUnitType(bsp).getName() << " of " << bcp->getName() << " was killed" << endl;
        }
    }
#endif

    /*
     * Do the killing... 
     */
#if 0
    {
        for(LeaderList::iterator it = killedLeaders.begin();
            it != killedLeaders.end();
            ++it)
        {
            ILeader leader = *it;

            campArmy->killAndReplaceLeader(leader);
        }
    }
#endif
    {
        for(KillSPList::iterator it = killedSPs.begin();
            it != killedSPs.end();
            ++it)
        {
            RefBattleSP bsp = *it;
            RefBattleCP bcp = bsp->parent();
            RefGenericCP gcp = bcp->generic();
            ICommandPosition cp = campCP(gcp);
            ISP sp = bsp->generic();

            campArmy->detachStrengthPoint(cp, sp);
            campArmy->deleteStrengthPoint(sp);
        }
    }
    {
        for(DeadCPList::iterator it = killedCPs.begin();
            it != killedCPs.end();
            ++it)
        {
            ICommandPosition cp = *it;
            campArmy->removeUnitAndChildren(cp);
        }
    }

#ifdef DEBUG
    log << "-------" << endl;
    log << "Summary" << endl;
    log << "-------" << endl;
    log << "Total SPs: " << nSP << endl;
    log << "Dead SPs: " << spDead << endl;
    log << "Fled SPs: " << spFled << endl;
    log << "Strength: " << spTotalStrength << " from " << (nSP * 100) << endl;
    log << "-------" << endl;

    log.close();
#endif
}

#if 0
void removeOB(CampaignBattle* campBat, Armies* campArmy, BattleData* batData)
{
   OrderBattle* gob = campArmy->ob();  // Generic Order of Battle
   BattleOB* ob = batData->ob();       // Battle Order of Battle

   ob->removeBattleUnits();

   batData->ob(0);
   delete ob;
}
#endif

};

/*
 *----------------------------------------------------------------------
 * $Log$
 *----------------------------------------------------------------------
 */
