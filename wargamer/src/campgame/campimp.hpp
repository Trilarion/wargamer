/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef CAMPIMP_HPP
#define CAMPIMP_HPP

#ifndef __cplusplus
#error campimp.hpp is foruse with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 * Campaign Implementation
 * This is analogous to the old RealCampaignGame class
 *
 *----------------------------------------------------------------------
 */

#include "campint.hpp"
#include "campctrl.hpp"
#include "ctimctrl.hpp"
#include "savegame.hpp"
#include "campwind.hpp"
#include "gamebase.hpp"
#include "sync.hpp"

class RealCampaignData;
class CampaignLogic;
// class ThreadManager;
class MainWindow;
class FrontEnd;
class CampaignBattle;
class CampaignBattleGame;

#ifdef MP_CRC_CHECK
// extern unsigned int g_masterCRC;
#endif

namespace WargameMessage { class WaitableMessage;
};

class StartTacticalInterface
{
   public:
   virtual void startTactical(CampaignBattle* battle) = 0;
   virtual void startReinforcement() = 0;
};


class CampaignImp :

    // Interfaces that other modules can use

    #if !defined(EDITOR)
        private CampaignLogicOwner,
        private StartTacticalInterface,
        private CampaignTacticalOwner,
    #endif
    private CampaignInterface
{
   RealCampaignData* d_campData;
   GameOwner* d_owner;
   HWND d_mainWindow;
   CampaignWindows*  d_windows;
#if !defined(EDITOR)
   CampaignLogic*    d_logicThread;
   CampaignTimeControl d_timeControl;
//   CampaignMode d_mode;       // This should be somewhere else!
   CampaignBattleGame* d_battle;
   bool d_campaignOver;
#endif

   Boolean d_needRedraw;         // Something needs redrawing?

   friend class VictoryMessage;

   // Unimplemented Functions

   CampaignImp(const CampaignImp& campaignImp);
   const CampaignImp& operator = (const CampaignImp& campaignImp);

   public:
   CampaignImp(GameOwner* owner);
   ~CampaignImp();
   void init(const char* fileName, SaveGame::FileFormat mode);


   /*
   * Functions Used from campaign wrapper
   */

   void pause(Boolean state);
   // Pause the system
   // This is different that freezeTime, which just stops the
   // flow of gametime.
   // pauseGame() must make sure all threads are in a paused state
   bool saveGame(bool prompt) const;
   void saveGame(const char* fileName) const;

#ifdef CUSTOMIZE
   void runDataCheck();
#endif

#if !defined(EDITOR)

   void addTime(SysTick::Value ticks);
   // Add SysTicks to the campaign time
   void finishBattle();

   /*
	* Multiplayer Syncing
	*/

   RWLock d_syncLock;

   void sendTimeSync(SysTick::Value ticks, unsigned int checksum);
   void syncSlave(SysTick::Value ticks);
   void timeSyncAcknowledged(SysTick::Value ticks);
   void slaveReady();
   void sendSlaveReady();
	void processBattleOrder(MP_MSG_BATTLEORDER * msg);
	void processCampaignOrder(MP_MSG_CAMPAIGNORDER *msg);

   bool d_slaveReady;
   // (host machine) : has slave machine acknowledged the last time sync ?
   bool d_lastSyncAcknowledged;
   // (host machine) : the last time sync which was sent to the slave
   ULONG d_lastSyncTime;
   // (slave machine) : the last sync-time reveived from the master
   ULONG d_slaveWantTicks;

#else // EDITOR
   void sendTimeSync(SysTick::Value ticks, unsigned int checksum) { }
   void syncSlave(SysTick::Value ticks) { }
   void timeSyncAcknowledged(SysTick::Value ticks) { }
   void slaveReady() { }
   void sendSlaveReady() { }

	void processBattleOrder(MP_MSG_BATTLEORDER * msg) { }
	void processCampaignOrder(MP_MSG_CAMPAIGNORDER *msg) { }
#endif   // EDITOR

   private:
   virtual bool requestSaveWorld() const
   { return saveWorld();
   }
   bool saveWorld() const;

   HWND hwnd() const
   { return d_mainWindow;
   }
   void requestSaveGame(bool prompt)
   { d_owner->requestSaveGame(prompt);
   }
   void multiplayerConnectionComplete(bool isMaster) {
	   d_owner->multiplayerConnectionComplete(isMaster);
   }

   /*
   * Implementation of CampaignInterface and CampaignLogicControl Virtual Functions
   */

#if !defined(EDITOR)
   void sendPlayerMessage(const CampaignMessageInfo& msg);

   // void sendPlayerMessage(Side s, ICommandPosition from, ITown town, const int msgID, const int count, ...);
   // void sendPlayerMessage(Side s, ICommandPosition from, ITown town, const char* text, ...);
   // Send Message to Player via message Window
#endif   // EDITOR

   Boolean readWorld(const char* fileName, SaveGame::FileFormat mode);
   // Read World from File
   Boolean writeWorld(const char* fileName, SaveGame::FileFormat mode) const;
   // Save Campaign to a file

   bool readTacticalBattle(FileReader& f);
   bool writeTacticalBattle(FileWriter& f) const;


   CampaignWindowsInterface* getCampaignWindows()
   { return d_windows;
   }
   // Get Campaign Windows

   CampaignData* getCampaignData()
   { return campaignData();
   }
   CampaignData* campaignData();
   const CampaignData* campaignData() const;

// #if !defined(EDITOR)
//    CampaignMode getMode() const
//    { return d_mode;
//    }
//    void setMode(CampaignMode newMode);
//    // Get and Set the campaign Mode
// #endif

   /*
   * Campaign Data Time Functions
   */

   const CampaignTime& getCTime();
   const Date& getDate() const;
   const Time& getTime() const;
   const char* asciiTime(char* buffer = 0) const;
   TimeTick getTick() const;


   void redrawMap()
   { d_needRedraw = True;
   }
   void repaintMap();
   // Redraw Current Map


#if !defined(EDITOR)

   virtual void moraleUpdated();
   virtual void updateWeather();        // weather has changed

   virtual void armisticeBegun();
   virtual void armisticeEnded();

   CampaignTimeControl* getTimeControl()
   { return &d_timeControl;
   }
#endif   // EDITOR

#if defined(CUSTOMIZE)
   void removeAllTowns();
   void removeAllUnits();
#endif


#if !defined(EDITOR)

   // void addCampaignEvent(CampaignEvent* event);
      // void removeCampaignEvent(CampaignEvent* event);

#ifdef DEBUG
      void showAI(DrawDIBDC* dib, ITown itown, const PixelPoint& p);
   void drawAI(const IMapWindow& mw);
   // Display AI on map
#endif

   void setController(Side side, GamePlayerControl::Controller control);

#endif   // !EDITOR

   // ThreadManager* threadManager() { return d_threadManager; }

   private:
   /*
   * Internal Private Functions
   */

   Boolean writeData(FileWriter& f) const;
   Boolean readData(FileReader& f);
   Boolean readAsciiWorld(const char* fileName, SaveGame::FileFormat mode);
   Boolean readBinaryWorld(const char* fileName, SaveGame::FileFormat mode);
   // Read World from file
   Boolean writeBinaryWorld(const char* fileName, SaveGame::FileFormat mode) const;
   Boolean writeAsciiWorld(const char* fileName, SaveGame::FileFormat mode) const;
   // Save Game to a file

   bool save(SaveGame::FileFormat mode, bool prompt) const;


#if !defined(EDITOR)
   /*
   * Stop Game Logic
   */

      void startLogic();
   // Start the game logic thread
   void stopLogic();
   // Stop game logic thread

   void addAI(Side s);
   // Set up AI for a given side
   void removeAI(Side s);
   // Remove AI for a given side

   void endCampaign(const GameVictory& victory);
   void endVictoryScreen();
   void runVictoryScreen();

   void startTactical(CampaignBattle* battle);
   // void stopTactical();
   void startReinforcement();

   // Implement CampaignTacticalOwner
   void tacticalOver();
   void stopTime();
   void startTime();

   virtual void requestTactical(CampaignBattle* battle);
   // Called from logic

	virtual void reinforceTactical();
	// reinforce an ongoing battle

	virtual void siegeResults(Side s, const SiegeInfoData& sd);
   // Tell Game about siege results

   virtual EndOfDayMode::Mode endBattleDay(Side side, const BattleInfoData& bd);
   // Ask Game what to do at end of Day

   virtual void battleResults(Side side, const BattleInfoData& bd);
   // tell game about Battle Results

   virtual BattleMode::Mode askBattleMode(const CampaignBattle* battle);
   // Ask what mode to do a battle

   bool getUserResponse(WargameMessage::WaitableMessage* msg);
   // Wait for a response from user...


#endif   // EDITOR
};



#endif /* CAMPIMP_HPP */


