/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1995, Steven Morle-Green, All Rights Reserved
 *----------------------------------------------------------------------
 *
 *	Bargraph and Font handling
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.2  2001/06/18 08:25:46  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 * Revision 1.1  2001/06/13 08:52:39  greenius
 * Initial Import
 *
 * Revision 1.1  1996/06/22 19:02:35  sgreen
 * Initial revision
 *
 * Created by Paul Sample
 * June 21 1996: Updated to stop fonts being deleted twice
 *
 *----------------------------------------------------------------------
 */



#include "stdinc.hpp"
#include "bargraph.hpp"
#include "dib.hpp"
#include "trig.hpp"
#include "misc.hpp"
#include "wmisc.hpp"
#include "fonts.hpp"
#include "bres.hpp"

void Lines :: putLine(HDC hdc, int y, int w)
{
  HPEN hPen, hOldPen;
  hPen = CreatePen(PS_SOLID, w, 0);
  ASSERT(hPen != 0);
  hOldPen = (HPEN)SelectObject(hdc, hPen);
  MoveToEx(hdc, 0, y, NULL);
  LineTo(hdc, 250, y);
  SelectObject(hdc, hOldPen);
  DeleteObject(hPen);
}

void Lines::putLine(HDC hdc, int sX, int sY, int fX, int fY, int w)
{
  HPEN hPen, hOldPen;
  hPen = CreatePen(PS_SOLID, w, 0);
  ASSERT(hPen != 0);
  hOldPen = (HPEN)SelectObject(hdc, hPen);
  MoveToEx(hdc, sX, sY, NULL);
  LineTo(hdc, fX, fY);
  SelectObject(hdc, hOldPen);
  DeleteObject(hPen);
}


void 

Lines :: putLine(HDC hdc, int sX, int sY, int fX, int fY, int w,
								 COLORREF color)
{
  HPEN hPen, hOldPen;
  hPen = CreatePen(PS_SOLID, w, color);
  hOldPen = (HPEN)SelectObject(hdc, hPen);

  MoveToEx(hdc, sX, sY, NULL);
  LineTo(hdc, fX, fY);

  SelectObject(hdc, hOldPen);
  DeleteObject(hPen);
}

void

Lines :: putRect(HDC hdc, int lX, int tY, int rX, int bY, int w,
							 COLORREF color)
{

  putLine(hdc, lX, tY, rX, tY, w, color);
  putLine(hdc, lX, tY, lX, bY, w, color);
  putLine(hdc, lX, bY, rX, bY, w, color);
  putLine(hdc, rX, tY, rX, bY, w, color);

}

void Lines :: putRect(DrawDIB *dDIB, LONG lX, LONG tY, LONG wX, LONG hY,
				 COLORREF color)
{
	dDIB->hLine(lX, tY, wX, dDIB->getColour(color));
	dDIB->vLine(lX, tY, hY, dDIB->getColour(color));
	dDIB->hLine(lX, tY + hY, wX, dDIB->getColour(color));
	dDIB->vLine(lX + wX, tY, hY, dDIB->getColour(color));
}


void Lines :: putRect(HDC hdc, int lX, int tY, int rX, int bY, int w,
							 COLORREF color1, COLORREF color2, COLORREF color3,
							 COLORREF color4)
{

  putLine(hdc, lX, tY, rX, tY, w, color1);
  putLine(hdc, rX, tY, rX, bY, w, color4);
  putLine(hdc, rX, bY, lX, bY, w, color3);
  putLine(hdc, lX, bY, lX, tY, w, color2);

}

/*
 * Draws 3 sides of a rect, open at top
 */

 void Lines :: putOpenTopRect(HDC hdc, int lX, int tY, int rX, int bY, int w,
							 COLORREF color1, COLORREF color2, COLORREF color3)
{
  putLine(hdc, lX, tY, lX, bY, w, color1);
  putLine(hdc, lX+1, bY, rX, bY, w, color2);
  putLine(hdc, rX, tY, rX, bY, w, color3);
}

/*
 * Draws 3 sides of a rect, open at bottom
 */

 void Lines :: putOpenBottomRect(HDC hdc, int lX, int tY, int rX, int bY, int w,
							 COLORREF color1, COLORREF color2, COLORREF color3)
{
  putLine(hdc, lX, tY, rX-1, tY, w, color1);
  putLine(hdc, lX, tY, lX, bY, w, color2);
  putLine(hdc, rX, tY, rX, bY, w, color3);
}

/*=========================================================================
 * Bargraph Implementation
 */

/*
 * Generic Barchart
 */

void BarGraph::putBarGraph(HDC hdc, int x, int y, unsigned int value, unsigned int maxValue, unsigned int width, unsigned int height, COLORREF color, Boolean showNumber)
{
	HPEN pen1 = CreatePen(PS_SOLID, 1, BLACK);
	HPEN oldPen = (HPEN) SelectObject(hdc, pen1);
	HBRUSH brush1 = CreateSolidBrush(color);
	HBRUSH brush2 = CreateSolidBrush(D_GREY);
	HBRUSH oldBrush = (HBRUSH) SelectObject(hdc, brush1);

	MoveToEx(hdc, x, y, NULL);
	LineTo(hdc, x+width, y);
	LineTo(hdc, x+width, y+height);
	LineTo(hdc, x, y+height);
	LineTo(hdc, x, y);

	if(value > maxValue)
		value = maxValue;

	unsigned int boundary = (value * width) / maxValue;

	if(boundary > 0)
		Rectangle(hdc, x, y, x+boundary, y+height);

	if(boundary < width)
	{
		SelectObject(hdc, brush2);
		Rectangle(hdc, x+boundary, y, x+width, y+height);
	}

	if(showNumber)
	{
		Fonts font;
		font.setFont(hdc, height, height/2);
		wTextPrintf(hdc, x+1, y, "%ud", value);
	}

	SelectObject(hdc, oldBrush);
	SelectObject(hdc, oldPen);
	DeleteObject(brush2);
	DeleteObject(brush1);
	DeleteObject(pen1);
}


#if 0
void BarGraph :: putLongLevel(HDC hdc, long level, int x, int y)
{

	COLORREF color;

	if(level <= 33)
		color = BLACK;
	else if(level <= 66)
		color = RED;
	else if(level <= 99)
		color = BLUE;
	else
		color = GREEN;

  putBarGraph(hdc, x,y+4, level,100, 120,8, color, True);

}
#endif

#if 0
void

BarGraph :: putAttribLevel(HDC hdc, Attribute level, int x, int y)
{
	COLORREF color;

	if(level <= 63)
		color = BLACK;
	else if(level <= 127)
		color = RED;
	else if(level <= 191)
		color = BLUE;
	else
		color = GREEN;

  putBarGraph(hdc, x,y+4, level,Attribute_Range+1, 120,8, color, True);
}
#endif

/*
 * Get colour based on level and range
 */

COLORREF BarGraph::getColor(long level, long maxValue)
{
	COLORREF color;

	if(level < (maxValue/4))
		color = BLACK;
	else if(level < (maxValue/2))
		color = RED;
	else if(level < ((3*maxValue)/4))
		color = BLUE;
	else
		color = GREEN;

	return color;
}


void BarGraph :: putTinyLevel(HDC hdc, long value, long maxValue, int x, int y, int w, int h)
{
	COLORREF color = getColor(value, maxValue);

	Lines::putLine(hdc, x, y, (value * w) / maxValue, h, color);
}


void BarGraph::putTinyLevel(DrawDIB* dib, long value, long maxValue, int x, int y, int w, int h)
{
	COLORREF color = getColor(value, UBYTE_MAX+1);
	ColourIndex cIndex = dib->getColour(color);

	if(value >= 0)
	{
		if(value < maxValue)
			w = (w * value) / maxValue;

		if(w > 0)
			dib->rect(x, y, w, h, cIndex);
	}
}


/*
 * Half Pie display
 */


void drawHalfPie(HDC hdc, const RECT* rect, ULONG amount, ULONG total, BOOL showPercent)
{
	RECT r = *rect;

	/*
	 * Adjust rectangle to be sensible proportions
	 */

	LONG w = r.right - r.left;
	LONG h = r.bottom - r.top;
	if(w > (2 * h))
	{	// Make it narrower
		LONG newW = h * 2;

		r.left += (w - newW) / 2;
		r.right = r.left + newW;

	}
	else
	{	// Make it shorter
		LONG newH = w / 2;

		r.top += (h - newH) / 2;
		r.bottom = r.top + newH;
	}



	LONG cx = (r.left + r.right) / 2;
	LONG cy = r.bottom;
	LONG radius = (r.right - r.left) / 2;

	LONG pieBottom = r.bottom + r.bottom - r.top + 1;

	/*
	 * Show it disabled if total==0
	 */

	if(total == 0)
	{
		HBRUSH greyBrush = (HBRUSH) GetStockObject(DKGRAY_BRUSH);
		HBRUSH oldBrush = (HBRUSH) SelectObject(hdc, greyBrush);
		Pie(hdc,
				r.left, r.top, r.right, pieBottom, 
				r.right, r.bottom, 
				r.left, r.bottom);
		SelectObject(hdc, oldBrush);
	}
	else
	{
		HBRUSH blackBrush = (HBRUSH) GetStockObject(BLACK_BRUSH);
		HBRUSH greenBrush = CreateSolidBrush(RGB(0, 255, 0));

		HBRUSH oldBrush = (HBRUSH) SelectObject(hdc, blackBrush);

		Pie(hdc,
				r.left, r.top, r.right, pieBottom, 
				r.right, r.bottom, 
				r.left, r.bottom);

		if(amount)
		{
			Wangle angle = (Wangle) MulDiv(amount, Degree(180), total);

			if(angle != 0)
			{
				LONG x1 = cx - mcos(radius, angle);
				LONG y1 = cy - msin(radius, angle);

				SelectObject(hdc, greenBrush);

				Pie(hdc, 
					r.left+1, r.top+1, 
					r.right-1, pieBottom-1, 
					x1, y1,
					r.left, r.bottom);
			}
		}

		if(showPercent)
		{
			int percent = MulDiv(amount, 100, total);

			int oldBkMode = SetBkMode(hdc, OPAQUE);
			UINT oldAlign = SetTextAlign(hdc, TA_TOP | TA_CENTER);
			COLORREF oldBkColor = SetBkColor(hdc, RGB(255, 255, 255));
			COLORREF oldTextColor = SetTextColor(hdc, RGB(0, 0, 0));
			Fonts font;
			font.setFont(hdc, 16, 6);

			wTextPrintf(hdc, cx, cy-18, "%d%%", percent);

			SetTextColor(hdc, oldTextColor);
			SetBkColor(hdc, oldBkColor);
			SetBkMode(hdc, oldBkMode);
			SetTextAlign(hdc, oldAlign);
		}

		SelectObject(hdc, oldBrush);
		DeleteObject(greenBrush);
		DeleteObject(blackBrush);
	}
}
#if 0
void drawHalfPie(HDC hdc, const RECT* rect, ULONG amount, ULONG total, BOOL showPercent)
{


}
#endif



/*
 * Draw a coloured bargraph
 *
 * It is given an array of colours of which the foreground canges between.
 */


#ifdef DEBUG
void BarChartInfo::check()
{
	ASSERT(d_width > 0);
	ASSERT(d_height > 0);
	ASSERT(d_maxValue > 0);
	ASSERT(d_value <= d_maxValue);
	ASSERT(d_value >= 0);
	ASSERT(d_colors != 0);
	ASSERT(d_nColors > 0);
}
#endif

void BarChartInfo::draw(DrawDIB* dib)
{
#ifdef DEBUG
	check();
#endif

	// Draw the borders...

	dib->frame(d_x, d_y, d_width, d_height, dib->getColour(d_border[0]), dib->getColour(d_border[1]));

	// calculate inner rectangle

	LONG x1 = d_x + 1;
	LONG y1 = d_y + 1;
	LONG w1 = d_width - 2;
	LONG h1 = d_height - 2;

	ASSERT(w1 > 0);
	ASSERT(h1 > 0);

	// calculate mid-point

	int middle = (d_value * w1) / d_maxValue;
	int leftOver = w1 - middle;

	// Fill first section

	if(middle > 0)
	{
		if(d_nColors == 1)
			dib->rect(x1, y1, middle, h1, dib->getColour(d_colors[0]));
		else
		{
			BresenhamIterator<LONG> colIt(0, w1, d_nColors - 1);
			
			LONG lastX = 0;
			int count = 0;
			while(lastX < middle)
			{
				LONG newX = ++colIt;
			  	LONG edgeX = minimum(newX, middle);

				if(edgeX != lastX)
				{
					COLORREF c1 = d_colors[count];	
					COLORREF c2 = d_colors[count+1];

					fillBurst(dib, lastX + x1, y1,  edgeX - lastX, h1, newX - lastX, c1, c2);
					// dib->rect(lastX + x1, y1, edgeX - lastX, h1, dib->getColour(c1));
				}

				lastX = newX;
				++count;
			}
		}
	}

	// Fill empty section

	if(leftOver > 0)
	{
		dib->rect(x1 + middle, y1, leftOver, h1, dib->getColour(d_background));
	}

}



void BarChartInfo::fillBurst(DrawDIB* dib, LONG x, LONG y, LONG w, LONG h, LONG fullWidth, COLORREF c1, COLORREF c2)
{
	/*
	 * Do some clipping of values so can use vlineFast
	 */

	if(y < 0)
	{
		h += y;
		y = 0;
	}

	if(y >= dib->getHeight())
		return;

	if( (y + h) >= dib->getHeight())
	{
		h = dib->getHeight() - y;
	}

	if(h <= 0)
		return;

	if(x >= dib->getWidth())
		return;
	if( (x + w) >= dib->getWidth())
	{
		w = dib->getWidth() - x;
	}

	if(w <= 0)
		return;


	BresenhamIterator<UBYTE> rIter(GetRValue(c1), GetRValue(c2), fullWidth);
	BresenhamIterator<UBYTE> gIter(GetGValue(c1), GetGValue(c2), fullWidth);
	BresenhamIterator<UBYTE> bIter(GetBValue(c1), GetBValue(c2), fullWidth);

	UBYTE* destAd = (x <= 0) ? dib->getAddress(0,y) : dib->getAddress(x,y);
	int lineWidth = dib->getStorageWidth();

	while(w--)
	{
		UBYTE r = rIter++;
		UBYTE g = gIter++;
		UBYTE b = bIter++;


		if(x >= 0)
		{
			ColourIndex colorIndex = dib->getColour(PALETTERGB(r,g,b));
			UBYTE* ad = destAd;

			int length = h;
			while(length--)
			{
				*ad = colorIndex;
				ad += lineWidth;
			}

			++destAd;
		}

		++x;
	}

	// dib->rect(x, y, w, h, dib->getColour(c1));
}

