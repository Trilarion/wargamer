/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef TODOLOG_HPP
#define TODOLOG_HPP

#ifndef __cplusplus
#error todolog.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1995, Steven Morle-Green, All Rights Reserved
 *----------------------------------------------------------------------
 *
 *	todo logfile
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 *
 *----------------------------------------------------------------------
 */

#include "sysdll.h"
#ifdef DEBUG
#include "mytypes.h"
#include "clog.hpp"

extern SYSTEM_DLL LogFileFlush todoLog;

class SYSTEM_DLL ToDoVar {
	Boolean done;
	const char* text;
	const char* srcFile;
	int srcLine;
public:
	ToDoVar(const char* function, const char* file, int line);
	~ToDoVar();
	// void invoke();
};

#define ToDo(fn)	{ static ToDoVar td(fn, __FILE__, __LINE__); }
// td.invoke(); }
#else		// !DEBUG

#define ToDo(s) ((void)0)
#endif

#endif /* TODOLOG_HPP */

