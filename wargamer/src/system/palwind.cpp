/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1995, Steven Morle-Green, All Rights Reserved
 *----------------------------------------------------------------------
 *
 *	Help for windows that update or use the palette
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/06/13 08:52:39  greenius
 * Initial Import
 *
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "palwind.hpp"
#include "palette.hpp"
#include "dib.hpp"
//#include "dib_util.hpp"
#include "bargraph.hpp"			// Lines class is in here.
#include "app.hpp"

 
PaletteWindow::~PaletteWindow()
{
}

BOOL PaletteWindow::handlePalette(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam, LRESULT& result)
{
	result = 0;

	switch(msg)
	{
		case WM_PALETTECHANGED:
		   if (wParam == (WPARAM)hWnd)            // Responding to own message.
				break;
		case WM_QUERYNEWPALETTE:
			if(Palette::get())
			{
				HDC hDC = GetDC(hWnd);
				HPALETTE hOldPal = SelectPalette(hDC, Palette::get(), FALSE);
				int i = RealizePalette(hDC);       // Realize drawing palette.

//				if(i)                         // Did the realization change?
					InvalidateRect(hWnd, NULL, TRUE);    // Yes, so force a repaint.
				// else
				//		SendMessage(hWmd, WM_PALETTECHANGED, hWnd, NULL);

				ReleaseDC(hWnd, hDC);
				result = i;
				return TRUE;
			}
	}
	 
	return FALSE;
}



/*============================================================
 * Custom Background Window
 */
/*
 * Custom Bk Window base Class
 */

const char CustomBkWindow::s_className[] = "CustomBkWindow";

ATOM CustomBkWindow::classAtom = 0;

ATOM CustomBkWindow::registerClass()
{
	if(!classAtom)
	{
		WNDCLASS wc;

		wc.style = CS_OWNDC | CS_DBLCLKS;
		wc.lpfnWndProc = baseWindProc;
		wc.cbClsExtra = 0;
		wc.cbWndExtra = sizeof(CustomBkWindow*);
		wc.hInstance = APP::instance();
		wc.hIcon = NULL;
		wc.hCursor = LoadCursor(NULL, IDC_ARROW);
		wc.hbrBackground = NULL;
		wc.lpszMenuName = NULL;
		wc.lpszClassName = s_className;
		classAtom = RegisterClass(&wc);
	}

	ASSERT(classAtom != 0);

	return classAtom;
}


 
CustomBkWindow::CustomBkWindow(const CustomBorderInfo& info) :
	CustomBorderWindow(info)
{
  bkDib = 0;
  mainDIB = 0;
  registerClass();
}

 
CustomBkWindow::~CustomBkWindow()
{
#if 0		// Owner responsible for deleting this
	if(bkDib)
   	delete bkDib;
#endif

	if(mainDIB != 0)
		delete mainDIB;
}

#if 0
static LPCSTR 

CustomBkWindow::className()
{
	return s_className;
}
#endif

BOOL

CustomBkWindow::onEraseBk(HWND hwnd, HDC hdc)
{
	if(bkDib != 0)
	{
		RECT r;
		GetClientRect(hwnd, &r);

//      DIB_Utility::fillDC(hdc, 0, 0, r.right-r.left, r.bottom-r.top, bkDib);
		if( (mainDIB == 0) ||
			(r.right != mainDIB->getWidth()) ||
			(r.bottom != mainDIB->getHeight()) )
		{
			if(mainDIB != 0)
				delete mainDIB;

			mainDIB = new DrawDIBDC(r.right, r.bottom);
			ASSERT(mainDIB != 0);
         // Unchecked
		   mainDIB->fill(bkDib);
		}

      // End
		HPALETTE oldPal = SelectPalette(hdc, Palette::get(), FALSE);
		RealizePalette(hdc);

		ASSERT(mainDIB->getDC() != 0);
		BitBlt(hdc, 0, 0, r.right, r.bottom, mainDIB->getDC(), 0, 0, SRCCOPY);

		return TRUE;
	}

	return FALSE;
}

/*
 *  Border drawing routines. Probably should go elsewhere
 */


CustomBorderWindow::CustomBorderWindow(const CustomBorderInfo& info)
{
	d_info = info;		// Structure Copy

#if 0
  lightBrown = scenario->getBorderColour(LightBrown);
  offWhite = scenario->getBorderColour(OffWhite);
  tan = scenario->getBorderColour(Tan);
  darkBrown = scenario->getBorderColour(DarkBrown);
#endif
}

/*
 *  Following 4 functions draws a complete 4-sided border
 */

// 4 pixel border
void

CustomBorderWindow::drawThickBorder(HDC hdc, RECT& r)
{
  static Lines line;
  line.putRect(hdc, r.left, r.top, r.right-1, r.bottom-1, 1,
					d_info.colours[CustomBorderInfo::LightBrown], d_info.colours[CustomBorderInfo::LightBrown], d_info.colours[CustomBorderInfo::DarkBrown], d_info.colours[CustomBorderInfo::DarkBrown]);
  line.putRect(hdc, r.left+1, r.top+1, r.right-2, r.bottom-2, 1,
					d_info.colours[CustomBorderInfo::OffWhite], d_info.colours[CustomBorderInfo::OffWhite], d_info.colours[CustomBorderInfo::LightBrown], d_info.colours[CustomBorderInfo::LightBrown]);
  line.putRect(hdc, r.left+2, r.top+2, r.right-3, r.bottom-3, 1,
					d_info.colours[CustomBorderInfo::Tan], d_info.colours[CustomBorderInfo::Tan], d_info.colours[CustomBorderInfo::Tan], d_info.colours[CustomBorderInfo::Tan]);
  line.putRect(hdc, r.left+3, r.top+3, r.right-4, r.bottom-4, 1,
					d_info.colours[CustomBorderInfo::LightBrown],  d_info.colours[CustomBorderInfo::LightBrown], d_info.colours[CustomBorderInfo::LightBrown], d_info.colours[CustomBorderInfo::LightBrown]);
}

// 2 pixel border
void

CustomBorderWindow::drawThinBorder(HDC hdc, RECT& r)
{
  static Lines line;
  line.putRect(hdc, r.left, r.top, r.right-1, r.bottom-1, 1,
					d_info.colours[CustomBorderInfo::LightBrown],  d_info.colours[CustomBorderInfo::LightBrown], d_info.colours[CustomBorderInfo::DarkBrown], d_info.colours[CustomBorderInfo::DarkBrown]);
  line.putRect(hdc, r.left+1, r.top+1, r.right-2, r.bottom-2, 1,
					d_info.colours[CustomBorderInfo::OffWhite], d_info.colours[CustomBorderInfo::OffWhite], d_info.colours[CustomBorderInfo::LightBrown], d_info.colours[CustomBorderInfo::LightBrown]);
}

// 1 pixel border
void

CustomBorderWindow::drawSeperator(HDC hdc, RECT& r)
{
  static Lines line;
  line.putRect(hdc, r.left, r.top, r.right-1, r.bottom-1, 1,
					d_info.colours[CustomBorderInfo::DarkBrown], d_info.colours[CustomBorderInfo::DarkBrown], d_info.colours[CustomBorderInfo::DarkBrown], d_info.colours[CustomBorderInfo::DarkBrown]);
}

// 1 pixel 3-d border
void CustomBorderWindow::draw3DSeperator(HDC hdc, RECT& r)
{
  static Lines line;
  line.putRect(hdc, r.left, r.top, r.right-1, r.bottom-1, 1,
					d_info.colours[CustomBorderInfo::OffWhite], d_info.colours[CustomBorderInfo::OffWhite], d_info.colours[CustomBorderInfo::DarkBrown], d_info.colours[CustomBorderInfo::DarkBrown]);
}

/*
 *  These functions draw a border on 1 side only
 */

void CustomBorderWindow::drawTopSeperator(HDC hdc, RECT& r)
{
  static Lines line;
  line.putLine(hdc, r.left, r.top, r.right, r.top, 1, d_info.colours[CustomBorderInfo::DarkBrown]);
}

void CustomBorderWindow::drawLeftSeperator(HDC hdc, RECT& r)
{
  static Lines line;
  line.putLine(hdc, r.left, r.top, r.left, r.bottom, 1, d_info.colours[CustomBorderInfo::DarkBrown]);
}
