/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#include "stdinc.hpp"
#include "trackwin.hpp"
#include "dib.hpp"
#include "dib_util.hpp"
#include "scrnbase.hpp"
// #include "generic.hpp"
#include "app.hpp"
#include "palette.hpp"
#include "cbutton.hpp"

#ifdef DEBUG
#include "logwin.hpp"
#endif

namespace	// private namespace
{

class UtilPixelRect : public PixelRect
{
	public:
		UtilPixelRect(const PixelRect& r) : PixelRect(r) { }

		void moveX(PixelDimension x)
		{
			left(left()+x);
			right(right()+x);
		}

		void moveY(PixelDimension y)
		{
			top(top()+y);
			bottom(bottom()+y);
		}


	private:
};

PixelRect fitToScreen(const PixelRect& screenRect, const PixelRect& insertRect)
{
	ASSERT(insertRect.width() <= screenRect.width());
	ASSERT(insertRect.height() <= screenRect.height());

	LONG x = insertRect.left();
	LONG y = insertRect.top();

	UtilPixelRect result = insertRect;

	if(result.right() > screenRect.right())
		result.moveX(screenRect.right() - result.right());
	if(result.bottom() > screenRect.bottom())
		result.moveY(screenRect.bottom() - result.bottom());
	if(result.left() < screenRect.left())
		result.moveX(screenRect.left() - result.left());
	if(result.top() < screenRect.top())
		result.moveY(screenRect.top() - result.top());

	return result;
}

class IW_Util {
public:
  static HWND IW_Util::createButton(HWND hParent, const IW_ButtonData& ct, const DrawDIB* fillDib, const CustomBorderInfo& bi);
};

HWND IW_Util::createButton(HWND hParent, const IW_ButtonData& ct, const DrawDIB* fillDib, const CustomBorderInfo& bi)
{
  HWND hButton = CreateWindow(CUSTOMBUTTONCLASS, ct.d_text,
				WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON,
				(ScreenBase::dbX() * ct.d_x) / ScreenBase::baseX(),
				(ScreenBase::dbY() * ct.d_y) / ScreenBase::baseY(),
				(ScreenBase::dbX() * ct.d_cx) / ScreenBase::baseX(),
				(ScreenBase::dbY() * ct.d_cy) / ScreenBase::baseY(),
				hParent, (HMENU)ct.d_id, APP::instance(), NULL);

  ASSERT(hButton != 0);

  CustomButton cb(hButton);
  cb.setFillDib(fillDib); //scenario->getScenarioBkDIB(Scenario::BackPatterns::WoodBackground));
  cb.setBorderColours(bi); //scenario->getBorderColors());

  return hButton;
}

};		// private namespace


// DrawDIBDC* InsertWind::s_dib = 0;
// int InsertWind::s_instanceCount = 0;
//static DibPal InsertWind::s_dib;

InsertWind::InsertWind() :
   d_dib(0),
   d_flags(0)
{
//  d_dib.addRef();
}

InsertWind::~InsertWind()
{
    selfDestruct();

//	d_dib.delRef();
   if(d_dib)
      delete d_dib;
#if 0
  ASSERT((s_instanceCount - 1) >= 0);

  if(--s_instanceCount == 0)
  {
	 if(s_dib)
		delete s_dib;

	 s_dib = 0;
  }
#endif
}

int InsertWind::shadowWidth()
{
	return (3 * ScreenBase::dbX()) / ScreenBase::baseX();
}

int InsertWind::shadowHeight()
{
	return shadowWidth();
}



void InsertWind::create(const IW_Data& data)
{
  d_data = data;

  HWND hWnd = createWindow(
		0,
		// GenericNoBackClass::className(),
        NULL,
		WS_POPUP,
		0, 0, d_data.d_cx, d_data.d_cy,
		d_data.d_hParent, NULL  // , APP::instance()
  );
  ASSERT(hWnd != NULL);

  d_seeThroughWindow.parent(d_data.d_hParent);
}

BOOL InsertWind::onCreate(HWND hwnd, CREATESTRUCT* lpCreateStruct)
{
   d_seeThroughWindow.allocateDib(lpCreateStruct->cx, lpCreateStruct->cy);

   const int dbX = ScreenBase::dbX();
   const int dbY = ScreenBase::dbY();
   const int baseX = ScreenBase::baseX();
   const int baseY = ScreenBase::baseY();

   // create buttons as needed

   if(d_data.d_buttonData)
   {
      /*
       * Replace IDs with text and count buttons
       */

      int nButtons = 0;
      for(;;)
      {
         const IW_ButtonData* bd = &d_data.d_buttonData[nButtons];
         if((bd->d_text == 0) && (bd->d_textID != InGameText::Null))
         {
            IW_ButtonData* ncBD = const_cast<IW_ButtonData*>(bd);
            ncBD->d_text = InGameText::get(bd->d_textID);
         }

         if(bd->d_text == 0)
            break;

         IW_Util::createButton(hwnd, *bd, d_data.d_bFillDib, d_data.d_bBorderColors);

         ++nButtons;
      }

//       int i = 0;
//       while(d_data.d_buttonData[i].d_text)
//       {
//          IW_Util::createButton(hwnd, d_data.d_buttonData[i], d_data.d_bFillDib, d_data.d_bBorderColors);
//          i++;
//       }
   }

   return TRUE;
}

/*
 * This is used to delay showing of the window for about a half second
 * in order to avoid overlapping another instance of this window
 */

void InsertWind::onTimer(HWND hwnd, UINT id)
{
#ifdef DEBUG
   g_logWindow.printf("InsertWind(%p)::onTimer(%d)", getHWND(), id);
#endif

  if(d_flags & TimerActive)
  {
#ifdef DEBUG
   g_logWindow.printf("InsertWind(%p):: timerActive", getHWND());
#endif

	 ASSERT(id == TimerID);

	 drawBackground();

	 /*
	  * Convert to screen and show
	  */

	 POINT p2;
	 p2.x = d_point.getX();
	 p2.y = d_point.getY();

	 ClientToScreen(d_data.d_hParent, &p2);
#ifdef DEBUG
      g_logWindow.printf("InsertWind(%p):: ScreenPosition=%d,%d", getHWND(), (int)p2.x, (int)p2.y);
#endif
	 SetWindowPos(getHWND(), HWND_TOPMOST, p2.x, p2.y, 0, 0, SWP_NOSIZE);
    ShowWindow(getHWND(), SW_SHOW);

	 BOOL result = KillTimer(hwnd, id);
	 ASSERT(result);

	 d_flags &= ~TimerActive;
  }
}

void InsertWind::onPaint(HWND hwnd)
{
  PAINTSTRUCT ps;
  HDC hdc = BeginPaint(hwnd, &ps);

  HPALETTE oldPal = SelectPalette(hdc, Palette::get(), False);
  RealizePalette(hdc);

  BitBlt(hdc, 0, 0, d_data.d_cx, d_data.d_cy, d_dib->getDC(), 0, 0, SRCCOPY);

  SelectPalette(hdc, oldPal, False);

  EndPaint(hwnd, &ps);
}

BOOL InsertWind::onEraseBk(HWND hwnd, HDC hdc)
{
  return TRUE;
}

void InsertWind::onMouseMove(HWND hwnd, int x, int y, UINT keyFlags)
{
  // ShowWindow(hwnd, SW_HIDE);
  show(false);
}

/*
 * Position window near a point
 * default to top right of point, but moves if won't fit
 */

void InsertWind::position(const PixelPoint& p)
{
#ifdef DEBUG
   g_logWindow.printf("InsertWind(%p)::position(%d,%d)", getHWND(), p.x(), p.y());
#endif
	PixelRect parentRect;
	GetClientRect(d_data.d_hParent, &parentRect);

	int offsetX = 20;	// give a 20 pixel margin
	int offsetY = 20;

	ASSERT(p.x() >= parentRect.left());
	ASSERT(p.x() < parentRect.right());
	ASSERT(p.y() >= parentRect.top());
	ASSERT(p.y() < parentRect.bottom());

	/*
	 * Default position to top right
	 */

	PixelDimension x = p.x() + offsetX;
	PixelDimension y = p.y() - offsetY - d_data.d_cy;

	if(y < parentRect.top())
		y = p.y() + offsetY;

	PixelDimension x1 = x + d_data.d_cx;
	PixelDimension y1 = y + d_data.d_cy;

	if(x1 > parentRect.right())
		x -= x1 - parentRect.right();

	show(PixelPoint(x, y));
}


/*
 * Place Window at specified location
 *
 * Clips it so fits on screen
 */

void InsertWind::show(const PixelPoint& p)
{
#ifdef DEBUG
   g_logWindow.printf("InsertWind(%p)::show(%d,%d)", getHWND(), p.x(), p.y());
#endif


  	if(d_flags & TimerActive)
	 	killTimer();

  	/*
    * get parent rectangle
    */

	PixelRect parentRect;
	GetClientRect(d_data.d_hParent, &parentRect);

	PixelRect ourRect(p.x(), p.y(), p.x() + d_data.d_cx, p.y() + d_data.d_cy);

	ourRect = fitToScreen(parentRect, ourRect);

  	d_point = PixelPoint(ourRect.left(), ourRect.top());

#ifdef DEBUG
   g_logWindow.printf("InsertWind(%p)::point=%d,%d", getHWND(), d_point.x(), d_point.y());
#endif

  	if (d_data.d_flags & IW_Data::DelayShow)
   {
#ifdef DEBUG
   g_logWindow.printf("InsertWind(%p)::Setting timer", getHWND());
#endif

  	   d_flags |= TimerActive;

  	   // UWORD time = (d_data.d_flags & IW_Data::DelayShow) ? 250 : 1;
      UWORD time = 250; // 1/4 second
  	   int result = SetTimer(getHWND(), TimerID, time, NULL);
  	   ASSERT(result);
   }
   else
   {
#ifdef DEBUG
      g_logWindow.printf("InsertWind(%p)::Instant show", getHWND());
#endif

	    drawBackground();

	    /*
	     * Convert to screen and show
	     */

	    POINT p2;
	    p2.x = d_point.getX();
	    p2.y = d_point.getY();


	    ClientToScreen(d_data.d_hParent, &p2);

#ifdef DEBUG
      g_logWindow.printf("InsertWind(%p):: ScreenPosition=%d,%d", getHWND(), (int)p2.x, (int)p2.y);
#endif

	    // SetWindowPos(getHWND(), HWND_TOPMOST, p2.x, p2.y, 0, 0, SWP_NOSIZE | SWP_SHOWWINDOW);
	    SetWindowPos(getHWND(), HWND_TOPMOST, p2.x, p2.y, 0, 0, SWP_NOSIZE);
       ShowWindow(getHWND(), SW_SHOW);
   }
}

void InsertWind::update()
{
  drawInfo();
//  InvalidateRect(getHWND(), NULL, FALSE);
  UpdateWindow(getHWND());
}

void InsertWind::drawBackground()
{
#ifdef DEBUG
   g_logWindow.printf("drawBackground(%p)", getHWND());
#endif
  {


	 /*
	  * First transfer bits from mainwind hdc to our local dib
	  */

	 d_seeThroughWindow.transferBits(d_point);

	 /*
	  * Draw pop-up insert
	  */

	 DIB_Utility::InsertData id;
	 id.d_cRef = d_data.d_insertColor;
	 id.d_x = 0;
	 id.d_y = 0;
	 id.d_cx = d_data.d_cx;
	 id.d_cy = d_data.d_cy;
	 id.d_shadowCX = shadowWidth();
	 id.d_shadowCY = shadowHeight();
	 id.d_mode = DIB_Utility::InsertData::Transparent;

	 DIB_Utility::drawInsert(d_seeThroughWindow.dib(), id);
  }

  /*
	* draw info
	*/

  drawInfo();
}

void InsertWind::drawInfo()
{
  // allocate dib if needed
  DIB_Utility::allocateDib(&d_dib, d_data.d_cx, d_data.d_cy);
  //d_dib->allocate(d_data.d_cx, d_data.d_cy);

  /*
	* Copy background
	*/

  d_dib->blit(0, 0, d_data.d_cx, d_data.d_cy, d_seeThroughWindow.dib(), 0, 0);

  /*
	* Draw info to dib -- via virtual fuction
	*/

	PixelRect rect(0,0, d_data.d_cx - shadowWidth(), d_data.d_cy - shadowHeight());

  drawInsertInfo(rect);
  InvalidateRect(getHWND(), NULL, FALSE);
}

void InsertWind::hide()
{
#ifdef DEBUG
   g_logWindow.printf("InsertWind(%p)::hide()", getHWND());
#endif
  // ShowWindow(getHWND(), SW_HIDE);
  WindowBaseND::show(false);
  killTimer();
}

void InsertWind::killTimer()
{
  if(d_flags & TimerActive)
  {
#ifdef DEBUG
   g_logWindow.printf("InsertWind(%p)::killTimer(%d)", getHWND(), TimerID);
#endif

	 // kill timer
	 int result = KillTimer(getHWND(), TimerID);
	 ASSERT(result);

	 d_flags &= ~TimerActive;
  }
}


