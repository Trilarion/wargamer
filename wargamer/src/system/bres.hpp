/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef BRES_HPP
#define BRES_HPP

#ifndef __cplusplus
#error bres.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *	Bresenham Style Iterator
 *
 * Given a start, finish and number of samples
 * each ++ will advance to the next point
 *
 *----------------------------------------------------------------------
 */

#include "sysdll.h"
// #include <algo.h>		// for STL's swap()

#include <stdlib.h>


template<class T>
class BresenhamIterator
{
	public:
		BresenhamIterator(T start, T finish, int count);

		T operator ++() { increment(); return d_value; }
		T operator ++(int) { T old = d_value; increment(); return old; }

		T value() const { return d_value; }

		T operator*() const { return d_value; }

		class Error
		{
		};

	private:
		void increment();

#ifdef DEBUG
		bool valueValid()
		{
			return (d_sign < 0) ? (d_value >= d_end) : (d_value <= d_end);
		}
#endif

	private:
		T d_value;
		T d_whole;
		T d_numerator;
		T d_denominator;
		T d_remainder;
		int d_sign;			// +1 or -1

#ifdef DEBUG
		T d_end;
		int d_count;
#endif
};

template<class T>
BresenhamIterator<T>::BresenhamIterator(T start, T finish, int count)
{
	ASSERT(count > 0);
	if(count <= 0)
		throw Error();

	// if(start > finish)
	// {
	// 	swap(start, finish);
	// }

	d_value = start;

	T diff;

	if(finish >= start)
	{
		d_sign = +1;
		diff = finish - start;
	}
	else
	{
		d_sign = -1;
		diff = start - finish;
	}


	// T diff = finish - start;
	ASSERT(diff >= 0);

	d_whole = diff / count;
	// d_numerator = abs(diff - (d_whole * count));
	d_numerator = diff - (d_whole * count);
	d_denominator = count;
	// d_sign = (diff < 0) ? -1 : +1;

	d_remainder = d_denominator / 2;		// This causes rounded result, 0 would truncate

#ifdef DEBUG
	d_end = finish;
	d_count = count;
#endif
}

template<class T>
void BresenhamIterator<T>::increment()
{
#ifdef DEBUG
	ASSERT(d_count > 0);
	--d_count;
	ASSERT(valueValid());
#endif

	if(d_sign > 0)
		d_value += d_whole;
	else
		d_value -= d_whole;

	d_remainder += d_numerator;
	if(d_remainder >= d_denominator)
	{
		d_remainder -= d_denominator;
		if(d_sign > 0)
			++d_value;
		else
			--d_value;
		// d_value += d_sign;
	}

	ASSERT(d_remainder < d_denominator);
	ASSERT(valueValid());
}


#endif /* BRES_HPP */

