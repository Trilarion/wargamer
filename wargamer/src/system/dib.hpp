/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef DIB_H
#define DIB_H

#ifndef __cplusplus
#error dib.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1995, Steven Morle-Green, All Rights Reserved
 *----------------------------------------------------------------------
 *
 * Device Independant BitMap support functions
 *
 *----------------------------------------------------------------------
 */

#include "sysdll.h"
#include "grtypes.hpp"
#include "dc_man.hpp"
#include "refptr.hpp"
#include "palette.hpp"
#ifdef DEBUG
// #include "StringPtr.hpp"
#endif

class SYSTEM_DLL RawDIB
{
      RawDIB(const RawDIB& dib);
      RawDIB& operator =(const RawDIB& dib);
   public:
      RawDIB();
      // RawDIB(PixelDimension width, PixelDimension height);
      virtual ~RawDIB();

      virtual void resize(PixelDimension width, PixelDimension height);

      void setPalette(const CPalette* pal);
         // Set the DIB's palette to pal... no remapping.
      void remapTo(const CPalette* pal);
         // Remap DIB to the pallete pointed to by pal.
      const PalettePtr& getPalette() const { return d_palette; }

      // void remap(const RGBQUAD* col);
         // Remap Image from given palette to system palette
//
//    void makeBitmapInfo(UINT width, UINT height);
//       // Setup bitmapInfo for given size
//       // NOTE: Does not clear out old values!
//
//    bool operator ! () const { return d_handle == NULL; }
//
//    /*
//     * Access Functions
//     */
//
      void setMaskColor(ColorIndex c) { d_hasMask = True; d_mask = c; }
      void setMaskColour(ColorIndex c) { setMaskColor(c); }

      Boolean hasMask() const { return d_hasMask; }
      ColorIndex getMask() const { return d_mask; }

      ColorIndex* getBits()
      {
         ASSERT(d_bits != 0);
         return d_bits;
      }

      const ColorIndex* getBits() const
      {
         ASSERT(d_bits != 0);
         return d_bits;
      }

      ColorIndex* getAddress(PixelDimension x, PixelDimension y);
      const ColorIndex* getAddress(PixelDimension x, PixelDimension y) const;

      PixelDimension getWidth() const { return d_dimension.x(); }
      PixelDimension getStorageWidth() const { return (getWidth() + 3) & ~3; }
      PixelDimension getHeight() const { return d_dimension.y(); }

      PixelRect getRect() const
      {
         return PixelRect(0, 0, getWidth(), getHeight());
      }

//    int getPlanes() const
//    {
//       ASSERT(d_binfo != 0);
//       return d_binfo->bmiHeader.biPlanes;
//    }
//
//    int getBitCount() const
//    {
//       ASSERT(d_binfo != 0);
//       return d_binfo->bmiHeader.biBitCount;
//    }
//
//    int getClrUsed() const
//    {
//       ASSERT(d_binfo != 0);
//       return d_binfo->bmiHeader.biClrUsed;
//    }
//
//    int getColors() const     // Get number of colors
//    {
//       if(getClrUsed() == 0)
//          return 1 << getBitCount();
//       else
//          return getClrUsed();
//    }
//
//
//    const LPRGBQUAD getColorTable() const // Get the Color Table
//    {
//       ASSERT(d_binfo != 0);
//       return d_binfo->bmiColors;
//    }
//
      HBITMAP getHandle() const;       // Get the DIB Handle
//    {
//       ASSERT(d_handle != NULL);
//       return d_handle;
//    }
//
//    BITMAPINFO * getBitmapInfo(void) const {
//
//       ASSERT(d_binfo != 0);
//       return d_binfo;
//    }
//

   private:
      /*
       * Private Functions
       */

      void releaseDIBSection();
      void releaseMemBitmap();
         // Deletes bitmap (used by ~DIB())

   protected:
      /*
       * Create / Recreate Functions
       */

      void makeMemBitmap(PixelDimension width, PixelDimension height);
      void makeDIBSection(PixelDimension width, PixelDimension height);

   private:
      PixelPoint     d_dimension;   // width & height
      ColorIndex*   d_bits;
      HBITMAP        d_handle;
      PalettePtr     d_palette;     // Reference counted palette
      Boolean        d_hasMask;
      ColorIndex    d_mask;
      UBYTE          d_bitCount;
};

#if 0    // Old Version

class SYSTEM_DLL RawDIB {
   UBYTE* d_bits;
   HBITMAP d_handle;
   BITMAPINFO* d_binfo;

   Boolean d_hasMask;
   ColorIndex d_mask;

    //--- Disallow copying
   RawDIB(const RawDIB& dib);
   RawDIB& operator =(const RawDIB& dib);
 public:
   RawDIB();
   RawDIB(PixelDimension width, PixelDimension height);
   ~RawDIB();
      // COnstructor does nothing, because this structure is only
      // used internally by DIB and it's derivatives

   /*
    * Manipulator Functions
    */

   void release();
      // Deletes bitmap (used by ~DIB())

   void remap(const RGBQUAD* col);
      // Remap Image from given palette to system palette

   void makeDIBSection(UINT width, UINT height);
      // Setup bitmapInfo for given size
      // NOTE: Does not clear out old values!

   bool operator ! () const { return d_handle == NULL; }

   /*
    * Access Functions
    */

   void setMaskColor(ColorIndex c) { d_hasMask = True; d_mask = c; }

   Boolean hasMask() const { return d_hasMask; }
   ColorIndex getMask() const { return d_mask; }

   ColorIndex* getBits()
   {
      ASSERT(d_bits != 0);
      return d_bits;
   }

   const ColorIndex* getBits() const
   {
      ASSERT(d_bits != 0);
      return d_bits;
   }

   ColorIndex* getAddress(PixelDimension x, PixelDimension y);
   const ColorIndex* getAddress(PixelDimension x, PixelDimension y) const;

   PixelDimension getWidth() const
   {
       if(d_binfo)
         return d_binfo->bmiHeader.biWidth;
       else
          return 0;
   }

   PixelDimension getStorageWidth() const { return (getWidth() + 3) & ~3; }

   PixelDimension getHeight() const
   {
      if(d_binfo)
       {
         if(d_binfo->bmiHeader.biHeight < 0)
            return -d_binfo->bmiHeader.biHeight;
         else
            return d_binfo->bmiHeader.biHeight;
       }
       else
          return 0;
   }

   PixelRect getRect() const
   {
      return PixelRect(0, 0, getWidth(), getHeight());
   }

   int getPlanes() const
   {
      ASSERT(d_binfo != 0);
      return d_binfo->bmiHeader.biPlanes;
   }

   int getBitCount() const
   {
      ASSERT(d_binfo != 0);
      return d_binfo->bmiHeader.biBitCount;
   }

   int getClrUsed() const
   {
      ASSERT(d_binfo != 0);
      return d_binfo->bmiHeader.biClrUsed;
   }

   int getColors() const     // Get number of colors
   {
      if(getClrUsed() == 0)
         return 1 << getBitCount();
      else
         return getClrUsed();
   }


   const LPRGBQUAD getColorTable() const // Get the Color Table
   {
      ASSERT(d_binfo != 0);
      return d_binfo->bmiColors;
   }

   HBITMAP getHandle() const     // Get the DIB Handle
   {
      ASSERT(d_handle != NULL);
      return d_handle;
   }

   BITMAPINFO * getBitmapInfo(void) const {

      ASSERT(d_binfo != 0);
      return d_binfo;
   }


};

#endif

inline const ColorIndex* RawDIB::getAddress(PixelDimension x, PixelDimension y) const
{
   ASSERT(x >= 0);
   ASSERT(x < getWidth());
   ASSERT(y >= 0);
   ASSERT(y < getHeight());

   /*
    * Tell GDI to finish anything it's still doing
    * I may be slow to put it here, in which case anything that
    * does things to bitmaps should call GdiFlush() and remove
    * it from here.
    */

   // GdiFlush();

   ASSERT(d_bits != 0);

   return d_bits + x + y * getStorageWidth();
}

inline ColorIndex* RawDIB::getAddress(PixelDimension x, PixelDimension y)
{
   ASSERT(x >= 0);
   ASSERT(x < getWidth());
   ASSERT(y >= 0);
   ASSERT(y < getHeight());

   /*
    * Tell GDI to finish anything it's still doing
    * I may be slow to put it here, in which case anything that
    * does things to bitmaps should call GdiFlush() and remove
    * it from here.
    */

   if(d_handle)
      GdiFlush();

   ASSERT(d_bits != 0);
   return d_bits + x + y * getStorageWidth();
}


class SYSTEM_DLL DIB : public RawDIB {
   // Unimplemented Constructor
   DIB(const DIB& dib);
   DIB& operator = (const DIB& dib);
public:
   DIB() : RawDIB() { }
   DIB(PixelDimension width, PixelDimension height) : RawDIB() { makeMemBitmap(width, height); }
   ~DIB() { }

   PixelRect clip(const PixelRect& rect) const;
   BOOL clip(PixelDimension& x, PixelDimension& y, PixelDimension& width, PixelDimension& height) const;
};

/*
 * Class for DIBS that can be drawn in
 */

class SYSTEM_DLL DrawDIB : virtual public DIB {
   DrawDIB& operator =(const DrawDIB& dib);
   DrawDIB(const DrawDIB& dib);
public:
   DrawDIB() : DIB() { }
   DrawDIB(PixelDimension width, PixelDimension height) : DIB(width, height) { }

   void hLineFast(PixelDimension x, PixelDimension y, PixelDimension length, ColorIndex color);
   void vLineFast(PixelDimension x, PixelDimension y, PixelDimension length, ColorIndex color);
   void hLine(PixelDimension x, PixelDimension y, PixelDimension length, ColorIndex color);
   void vLine(PixelDimension x, PixelDimension y, PixelDimension length, ColorIndex color);
   void rect(PixelDimension x, PixelDimension y, PixelDimension width, PixelDimension height, ColorIndex color);
   void rect(PixelDimension x, PixelDimension y, PixelDimension width, PixelDimension height, const DIB* srcPtr, PixelDimension srcX = 0, PixelDimension srcY = 0);
   void frame(PixelDimension x, PixelDimension y, PixelDimension width, PixelDimension height, ColorIndex color);
   void frame(PixelDimension x, PixelDimension y, PixelDimension width, PixelDimension height, ColorIndex color1, ColorIndex color2);

   void fill(ColorIndex color);
   void fill(const DrawDIB* srcPtr);

   void circle(PixelDimension x, PixelDimension y, PixelDimension radius, ColorIndex color);
   void circle(PixelDimension x, PixelDimension y, PixelDimension radius, ColorIndex color1, ColorIndex color2);
   void disc(PixelDimension x, PixelDimension y, PixelDimension radius, ColorIndex color);

   void line(PixelDimension x1, PixelDimension y1, PixelDimension x2, PixelDimension y2, ColorIndex color);

   void plot(PixelDimension x, PixelDimension y, ColorIndex color);

   void rightAngleLines(const PixelDimension width, const PixelDimension space, PixelPoint& start, PixelPoint& end, ColorIndex color);

   void drawBarChart(PixelDimension x, PixelDimension y, int value, int maxValue, PixelDimension width, PixelDimension height);
   void drawBarChart(const DrawDIB* bkScr, const DrawDIB* fillSrc, PixelDimension x, PixelDimension y, int value, int maxValue, PixelDimension width, PixelDimension height);


   // const ColorIndex* getAddress(PixelDimension x, PixelDimension y) const;
   // ColorIndex* getAddress(PixelDimension x, PixelDimension y);
   ColorIndex getColor(COLORREF cRef);
   ColorIndex getColour(COLORREF cRef) { return getColor(cRef); }
   ColorIndex getColor(const LPRGBQUAD rgb, const UBYTE r, const UBYTE g, const UBYTE b, const int nColors);

   void blitFrom(DIB* from);     // Complete copy

   void blit(const DIB* src, PixelDimension destX, PixelDimension destY) { blit(destX, destY, src->getWidth(), src->getHeight(), src, 0, 0); }

   void blit(PixelDimension destX, PixelDimension destY, PixelDimension w, PixelDimension h, const DIB* src, PixelDimension srcX, PixelDimension srcY);

   void flipBlit(PixelDimension destX, PixelDimension destY, PixelDimension w, PixelDimension h, const DIB* src, PixelDimension srcX, PixelDimension srcY);
      // draws a reversed(right to left) image

   void darken(int percent, PixelDimension borderWidth = 0)
   {
     darkenRectangle(borderWidth, borderWidth, getWidth()-(2*borderWidth), getHeight()-(2*borderWidth), percent);
   }
      // darken entire dib (minus border, if any)

   void lighten(int percent, PixelDimension borderWidth = 0)
   {
     lightenRectangle(borderWidth, borderWidth, getWidth()-(2*borderWidth), getHeight()-(2*borderWidth), percent);
   }
     // lighten entire dib (minus border, if any)

   void darken(DIB* src, PixelDimension destX, PixelDimension destY, int percent)
   {
     darken(src, destX, destY, src->getWidth(), src->getHeight(), 0, 0, percent);
   }

   void darken(DIB* src, PixelDimension destX, PixelDimension destY, PixelDimension w, PixelDimension h, PixelDimension srcX, PixelDimension srcY, int percent);
     // darken specified section of dib using a dib mask

   void lighten(DIB* src, PixelDimension destX, PixelDimension destY, int percent)
   {
     lighten(src, destX, destY, src->getWidth(), src->getHeight(), 0, 0, percent);
   }

   void lighten(DIB* src, PixelDimension destX, PixelDimension destY, PixelDimension w, PixelDimension h, PixelDimension srcX, PixelDimension srcY, int percent);
     // lighten specified section of dib using a dib mask

   void darkenRectangle(PixelDimension x, PixelDimension y, PixelDimension w, PixelDimension h, int percent);
     // darken specified section of dib

   void lightenRectangle(PixelDimension x, PixelDimension y, PixelDimension w, PixelDimension h, int percent);
     // lighten specified section of dib

   void brightenRectangle(PixelDimension x, PixelDimension y, PixelDimension w, PixelDimension h, int percent);
     // lighten specified section of dib, maintaining hue/saturation

   void remapRectangle(const ColorRemapTable* remapTable, PixelDimension X, PixelDimension Y, PixelDimension w, PixelDimension h);

private:
   UBYTE getOutCode(PixelDimension x, PixelDimension y);
   Boolean clipLine(PixelDimension& x1, PixelDimension& y1, PixelDimension& x2, PixelDimension& y2);
protected:

   void remapDIB(const DIB* maskDIB, const ColorRemapTable* remap, PixelDimension dextX, PixelDimension dextY)
   {
     remapDIB(maskDIB, remap, dextX, dextY, maskDIB->getWidth(), maskDIB->getHeight(), 0, 0);
   }

public:  // Used by DIB_Utility functions
   void remapDIB(const DIB* maskDIB, const ColorRemapTable* remap, PixelDimension destX, PixelDimension destY, PixelDimension w, PixelDimension h, PixelDimension srcX, PixelDimension srcY);
};


/*
 * HDC that can be used to draw into a DrawDIB
 */

class SYSTEM_DLL DCObject
{
   public:
      DCObject();
      virtual ~DCObject();

      HDC getDC() const { return d_hdc; }

      void saveDC()  { d_save.save(d_hdc); }        // Push current values
      void restoreDC() { d_save.restore(d_hdc); }   // Pop values

      /*
       * functions to wrap around the windows Select functions
       */

      HGDIOBJ selectObject(HGDIOBJ obj);
      int setBkMode(int bkMode);
      COLORREF setTextColor(COLORREF col);
      COLORREF setBkColor(COLORREF col);
      UINT setTextAlign(UINT textAlign);
      HFONT setFont(HFONT font);
      HPEN setPen(HPEN pen);
      HBRUSH setBrush(HBRUSH brush);

   protected:
      void init(HBITMAP bm);
      void reset();

   protected:
      HDC d_hdc;
   private:
      DCsave d_save;
      HBITMAP d_originalBM;
};

class SYSTEM_DLL DIBDC : virtual public DIB, public DCObject
{
   typedef DrawDIB Super;

    DIBDC& operator=(const DIBDC&);
   DIBDC(const RawDIB& dib);  // : DIB(dib) { init(getHandle()); }
public:
   DIBDC() : DIB() { }
   DIBDC(PixelDimension width, PixelDimension height); // : DIB(width, height) { init(getHandle()); }
   ~DIBDC();

   void resize(PixelDimension width, PixelDimension height);
};

class SYSTEM_DLL DrawDIBDC : public DrawDIB, public DIBDC
{
   typedef DrawDIB Super;

   DrawDIBDC& operator =(const DrawDIBDC& dib);
   DrawDIBDC(const DrawDIBDC& dib); // : DrawDIB(dib) { init(getHandle()); }
public:
   DrawDIBDC() : DrawDIB() { }
   DrawDIBDC(PixelDimension width, PixelDimension height);   // : DrawDIB(width, height); { init(getHandle()); }
   ~DrawDIBDC();

   void resize(PixelDimension width, PixelDimension height) { DIBDC::resize(width, height); }

   void line(PixelDimension x1, PixelDimension y1, PixelDimension x2, PixelDimension y2, ColorIndex color, PixelDimension width);
   void line(PixelDimension x1, PixelDimension y1, PixelDimension x2, PixelDimension y2, ColorIndex color) { Super::line(x1,y1,x2,y2,color); }

   void lightenedText(const char* text, PixelDimension x, PixelDimension y, int percent);
   void drawBargraphText(HFONT hFont, PixelDimension dextX, PixelDimension dextY, const char* text, ...);
   void drawBargraphText(const char* text, HFONT hFont, PixelDimension dextX, PixelDimension dextY, int width = 0);

#ifdef DEBUG
   BITMAPINFO* getBitmapInfo() const;
#endif


};

#endif /* DIB_H */

/*
 *----------------------------------------------------------------------
 * $Log$
 *----------------------------------------------------------------------
 */
