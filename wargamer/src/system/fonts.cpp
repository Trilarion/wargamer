/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *	Font Management (Removed from bargraph)
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "fonts.hpp"
#include "myassert.hpp"
#include "sllist.hpp"
#include "StringPtr.hpp"
#include "winfile.hpp"
#include "except.hpp"

// Ensure intialised AFTER logfile
#define STATIC_INIT_FONT
#include "staticInit.h"

// Enable Log file

#if defined(DEBUG) && !defined(NOLOG)
#define FONT_LOG
#endif

#ifdef FONT_LOG
#include "audit.hpp"
#endif

namespace		// Un-named private namespace
{

/*=====================================================================
 *  GlobalFont class. For storing and creating game fonts
 */

class RealGlobalFonts {

	struct FontInfo : public SLink {
		StringPtr fileName;
		// StringPtr faceName;
	};

	SList<FontInfo> installedFonts;

	class FontCacheItem : public SLink {
#ifdef FONT_LOG
         friend class FontAudit;
#endif

			StringPtr faceName;
			LONG height;
			LONG weight;
			BYTE italic;
			BYTE style;
			BYTE underline;
			HFONT d_handle;

		public:
			FontCacheItem(HFONT h, const LOGFONT* lf);
			~FontCacheItem();

			bool matches(const LOGFONT* lf) const
			{
				return (lstrcmp(faceName, lf->lfFaceName) == 0)
					&& (height == lf->lfHeight)
					&& (weight == lf->lfWeight)
					&& (italic == lf->lfItalic)
					&& (underline == lf->lfUnderline)
					&& (style == lf->lfPitchAndFamily);
			}

			HFONT handle() const { return d_handle; }

#ifdef FONT_LOG
         bool operator <(const FontCacheItem& item) const { return(d_handle < item.d_handle); }
         bool operator ==(const FontCacheItem& item) const { return(d_handle == item.d_handle); }
#endif

	};

	SList<FontCacheItem> createdFonts;

	public:
		RealGlobalFonts() {}
		~RealGlobalFonts();

		Boolean addFontToResource(const char* fileName, CString& faceName);	// const char* faceName);
		// Adds font to system and retreives typeface name

		// HFONT getFont(const char* name, int height, int weight, BOOL italic);
			// returns handle to font of given style
		// HFONT getFont(BYTE style, int height, int weight, BOOL italic);
		HFONT getFont(const LOGFONT& lf);

		void release(HFONT h) { }
			// does nothing, but could be used later to store a reference
			// count for fonts and free up fonts that are not in use
			// from time to time.

	private:
		static char* getFaceName(const char* name);
		// extract TTF facename from file
		// caller is responsible for deleting memory

		static int CALLBACK enumFontsProc(LOGFONT* lplf, TEXTMETRIC* lptm, DWORD dwType, LPARAM lpData);

      #ifdef FONT_LOG
         friend class FontAudit;
      #endif

};


#ifdef FONT_LOG
class FontAudit : public AuditLog<const RealGlobalFonts::FontCacheItem*>
{
   public:
      FontAudit() : AuditLog<const RealGlobalFonts::FontCacheItem*>("Font.log") { }
      ~FontAudit()
      {
         showState();
      }

      virtual void logItem(const RealGlobalFonts::FontCacheItem* font)
      {
         char buffer[20];
         sprintf(buffer, "%p", font->d_handle);
         *d_log << buffer;
         const char* name = font->faceName;
         if(name && name[0])
            *d_log << ", " << name;
         if(font->height)
            *d_log << ", h=" << font->height;
         *d_log << ", wt=" << font->weight;
         *d_log << ", style=" << int(font->style);
         if(font->italic)
            *d_log << ", Italic";
         if(font->underline)
            *d_log << ", Underline";
      }
};

static FontAudit s_fontLog;
#endif

static RealGlobalFonts globalFonts;     // static instance of RealGlobalFonts


RealGlobalFonts::FontCacheItem::FontCacheItem(HFONT h, const LOGFONT* lf)
{
  	faceName = lf->lfFaceName;
  	height = lf->lfHeight;
  	weight = lf->lfWeight;
  	d_handle = h;
  	italic = lf->lfItalic;
	underline = lf->lfUnderline;
  	style = lf->lfPitchAndFamily;
#ifdef FONT_LOG
   s_fontLog.logNew(this);
#endif
}

RealGlobalFonts::FontCacheItem::~FontCacheItem()
{
#ifdef FONT_LOG
   s_fontLog.logDelete(this);
#endif
	if(d_handle)
		DeleteObject(d_handle);
}


RealGlobalFonts::~RealGlobalFonts()
{
   /*
    * call FontCacheItem destructors, which delete HFONT
    */

   createdFonts.reset();

   /*
    * call RemoveFontResources for all fonts added
    */

   bool fontsRemoved = false;

   SListIter<FontInfo> fiIter(&installedFonts);

   while(++fiIter)
   {
      FontInfo* fi = fiIter.current();
      int result = RemoveFontResource(fi->fileName);
      ASSERT(result != 0);
      fontsRemoved = true;


   }

   /*
    * Send message to all top-level windows
    */

   if(fontsRemoved)
      SendMessage(HWND_BROADCAST, WM_FONTCHANGE, 0, 0);     // Moved from inner loop
}


/*
 * Get facename
 * If file not found or other error, then an exception is thrown
 */

static void readMULong(FileReader& f, ULONG& l)
{
	UBYTE b[4];
	f.getUByte(b[0]);
	f.getUByte(b[1]);
	f.getUByte(b[2]);
	f.getUByte(b[3]);
	l = (b[0] << 24) + (b[1] << 16) + (b[2] << 8) + b[3];
}

static void readMUWord(FileReader& f, UWORD& w)
{
	UBYTE b[2];
	f.getUByte(b[0]);
	f.getUByte(b[1]);
	w = (b[0] << 8) + b[1];
}



char* RealGlobalFonts::getFaceName(const char* name)
{
	WinFileBinaryReader f(name);

	ASSERT(f.isOK());

	struct TTFHeader
	{
		ULONG version;
		UWORD nTables;
		UWORD searchRange;
		UWORD entrySelector;
		UWORD rangeShift;
	};

	struct TTFTableDirectory
	{
		ULONG tag;
		ULONG checksum;
		ULONG offset;
		ULONG length;
	};

	TTFHeader header;
	readMULong(f, header.version);
	readMUWord(f, header.nTables);
	readMUWord(f, header.searchRange);
	readMUWord(f, header.entrySelector);
	readMUWord(f, header.rangeShift);

	ASSERT(header.version >= 0x00010000);
	while(header.nTables-- > 0)
	{
		const ULONG nameTag = 0x6e616d65;	// "name"

		TTFTableDirectory table;
		readMULong(f, table.tag);
		readMULong(f, table.checksum);
		readMULong(f, table.offset);
		readMULong(f, table.length);

		if(table.tag == nameTag)
		{
			f.seekTo(table.offset);

			struct NameHeader
			{
				UWORD format;
				UWORD nRecords;
				UWORD offset;
			} nameHead;

			readMUWord(f, nameHead.format);
			readMUWord(f, nameHead.nRecords);
			readMUWord(f, nameHead.offset);

			while(nameHead.nRecords-- > 0)
			{
				struct NameEntry
				{
					UWORD id;
					UWORD platform;
					UWORD language;
					UWORD nameID;
					UWORD length;
					UWORD offset;
				} nameEntry;

				readMUWord(f, nameEntry.id);
				readMUWord(f, nameEntry.platform);
				readMUWord(f, nameEntry.language);
				readMUWord(f, nameEntry.nameID);
				readMUWord(f, nameEntry.length);
				readMUWord(f, nameEntry.offset);

				// Look for a non-Unicode full font name

				if( (nameEntry.nameID == 4) && (nameEntry.platform == 0) )
				{
					// We've got it!

					ULONG offset = table.offset + nameHead.offset + nameEntry.offset;

					ASSERT( (offset + nameEntry.length) <= (table.offset + table.length));

					f.seekTo(offset);

					char* faceName = new char[nameEntry.length + 1];
					f.read(faceName, nameEntry.length);
					faceName[nameEntry.length] = 0;
					return faceName;
				}
			}

			throw GeneralError("face name not found in  font file %s", name);
		}
	}

	throw GeneralError("name tag not found in  font file %s", name);
}

int CALLBACK RealGlobalFonts::enumFontsProc(LOGFONT* lplf, TEXTMETRIC* lptm, DWORD dwType, LPARAM dwData)
{
  /*
	*  Note: dwData is a Boolean
	*/

  if(dwType & TRUETYPE_FONTTYPE)
  {
	 Boolean* found = (Boolean*)dwData;

	 *found = True;
	 return FALSE;
  }
  else
	 return TRUE;
}

Boolean RealGlobalFonts::addFontToResource(const char* fileName, CString& faceName)	// const char* faceName)
{

	ASSERT(fileName != 0);
   // ASSERT(faceName != 0);

	/*
	 *  See if font file has already been added list
	 */

	if(fileName != 0)
	{
		/*
		 * Extract the facename from the TTF file
		 */

		faceName = getFaceName(fileName);


		SListIter<FontInfo> fiIter(&installedFonts);

		Boolean found = False;

		while(++fiIter)
		{
			FontInfo* fi = fiIter.current();

			if(lstrcmp(fi->fileName, fileName) == 0)
			{
				found = True;
				break;
			}
		}

		/*
		 * If not found call enumfonts and see if font is already in the system.
		 * If not add font to system fonts and
		 * create new FontInfo structure
		 */

		if(!found)
		{

			HDC hdc = GetDC(NULL);
			ASSERT(hdc != 0);

			Boolean allReadyInSystem = False;
			EnumFonts(hdc, faceName, (FONTENUMPROC)enumFontsProc, (LPARAM)&allReadyInSystem);

			ReleaseDC(NULL, hdc);

			if(!allReadyInSystem)
			{

				int result = AddFontResource(fileName);
				if(result == 0)
					return False;

				/*
			 	 * Send message to all top-level windows
			 	 */

            //-------- This is slowing down the program too much if Outlook is running
				// SendMessage(HWND_BROADCAST, WM_FONTCHANGE, 0, 0);


				/*
			 	 * create new FontInfo and append to list
			 	 */

				FontInfo* fi = 0;
				fi = new FontInfo;
				ASSERT(fi != 0);

				fi->fileName = fileName;

				installedFonts.append(fi);
			}
		}
		return True;
	}
	else
		return False;
}

HFONT RealGlobalFonts::getFont(const LOGFONT& lf)
{
  /*
	* Check for exact font match.
	* If so, return handle
	*/

  Boolean found = False;

  SListIter<FontCacheItem> fcIter(&createdFonts);
  while(++fcIter)
  {
		FontCacheItem* item = fcIter.current();

		if(item->matches(&lf))
		{
		  return item->handle();
		}
  }

  /*
	* If not, create font
	*/

  HFONT hFont = CreateFontIndirect(&lf);
  ASSERT(hFont != 0);

  /*
	* Create new FontCacheItem
	*/

  FontCacheItem* fci = new FontCacheItem(hFont, &lf);
  ASSERT(fci != 0);
  createdFonts.append(fci);

  return hFont;
}

};		// un-named namespace

namespace Greenius_System
{

/*====================================================================
 * Publicly accessible classes
 */

/*--------------------------------------------------------------------
 * Global Font class
 */


Boolean GlobalFonts::addFontToResource(const char* fileName, CString& faceName)
{
  return globalFonts.addFontToResource(fileName, faceName);
}

/*=================================================================
 * Font Class: Todo
 */


Font::~Font()
{
	reset();
}

void Font::setFace(const char* faceName, int height, int width, int weight)
{
   reset();
	LogFont lf;
	lf.height(height);
	lf.face(faceName);
	lf.width(width);
	lf.weight(weight);
	d_handle = globalFonts.getFont(lf);
}

void Font::setStyle(FontFamily style, int height, int width, int weight)
{
   reset();
	LogFont lf;
	lf.height(height);
	lf.style(style);
	lf.width(width);
	lf.weight(weight);
	d_handle = globalFonts.getFont(lf);
}

void Font::set(const LogFont& lf)
{
   reset();
	d_handle = globalFonts.getFont(lf);
}

void Font::reset()
{
	if(d_handle != NULL)
	{
		globalFonts.release(d_handle);
		d_handle = NULL;
	}
}

/*=================================================================
 * Fonts Class: Todo
 */

Fonts::Fonts() : d_font(), d_hdc(NULL), d_oldFont(NULL)
{
}

Fonts::~Fonts()
{
	deleteFont();
}

void Fonts::deleteFont()
{
	if( (d_hdc != NULL) && (d_oldFont != NULL) )
	{
		SelectObject(d_hdc, d_oldFont);

		d_font.reset();
		d_oldFont = NULL;
	}
}


void Fonts::setFace(HDC hdc, const char* faceName, int height, int width, int weight)
{
	ASSERT(hdc != NULL);

	deleteFont();

	d_hdc = hdc;
	d_font.setFace(faceName, height, width, weight);
	d_oldFont = static_cast<HFONT>(SelectObject(d_hdc, d_font));
}

void Fonts::setStyle(HDC hdc, FontFamily style, int height, int width, int weight)
{
	ASSERT(hdc != NULL);

	deleteFont();

	d_hdc = hdc;
	d_font.setStyle(style, height, width, weight);
	d_oldFont = static_cast<HFONT>(SelectObject(d_hdc, d_font));
}

void Fonts::set(HDC hdc, const LogFont& lf)
{
	ASSERT(hdc != NULL);
	deleteFont();
	d_hdc = hdc;
	d_font.set(lf);
	d_oldFont = static_cast<HFONT>(SelectObject(d_hdc, d_font));
}

/*
 * Backward compatible functions
 */

HFONT FontManager::getFont(int h)
{
	LogFont lf;
	lf.height(h);
	return globalFonts.getFont(lf);
}

HFONT FontManager::getFont(const char* faceName, int h)
{
	LogFont lf;
	lf.height(h);
	lf.face(faceName);
	return globalFonts.getFont(lf);
}


};	// namespace Greenius_System

