/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef RESSTR_HPP
#define RESSTR_HPP

#include "sysdll.h"
#include "simpstr.hpp"
#include "StringPtr.hpp"
#include "res_str.h"
#include <map>


/*
 * Singleton style access to Resource Strings
 */

class SYSTEM_DLL InGameText
{
   public:
      typedef UINT ID;

      enum { Null = UINT_MAX };

      static const char* get(ID id);
   private:

      typedef std::map<ID,StringPtr,std::less<ID> > Container;

      static Container s_items;
};


/*
 *  Classes to hold resource strings. Each source file that uses
 *  resource strings should have a static instance of this class
 */


class SYSTEM_DLL ResourceStrings {
	 const int d_entries;
	 SimpleString* d_strings;
  public:
	 ResourceStrings(const int* ids, int entries);
	 ~ResourceStrings()
	 {
		if(d_strings)
		  delete[] d_strings;
	 }

	 const char* get(int id) const
	 {
		ASSERT(id < d_entries);
		ASSERT(d_strings);

		return d_strings[id].toStr();
	 }
};


#endif

/*---------------------------------------------------------------
 * $Log$
 *---------------------------------------------------------------
 */
