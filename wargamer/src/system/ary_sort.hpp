/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef ARY_SORT_HPP
#define ARY_SORT_HPP

#ifndef __cplusplus
#error ary_sort.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *	Array Shell Sorter
 *
 *----------------------------------------------------------------------
 */

#include "sysdll.h"
#include "array.hpp"

template<class T>
class SortCompare
{
 public:
 	virtual int operator()(const T& t1, const T& t2) const = 0;
};

template<class T>
class ArrayShellSort
{
	Array<T>& target;
 public:
 	ArrayShellSort(Array<T>& a) : target(a) { }
	void sort(const SortCompare<T>& compare);
};

// Disable warnings because of int to ArrayIndex (UWORD) conversion
// #pragma warning 389 9;

template<class T>
void ArrayShellSort<T>::sort(const SortCompare<T>& compare)
{
	size_t len = target.entries();

	int inc;
	for(inc = 1; inc < len / 9; inc = 3 * inc + 1)
		;

	for(; inc > 0; inc /= 3)
	{
		for(int i = inc + 1; i <= len; i += inc)
		{
			int j = i - 1;
			T v = target[j];

			while( (j >= inc) && (compare(target[j - inc], v) > 0) )
			{
				target[j] = target[j - inc];
				j -= inc;
			}
			target[j] = v;
		}
	}
}


#endif /* ARY_SORT_HPP */

