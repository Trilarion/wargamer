/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1995, Steven Morle-Green, All Rights Reserved
 *----------------------------------------------------------------------
 *
 * Windows File Reader/Writer classes Implementation
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "winfile.hpp"
#include "registry.hpp"
#include "pathdial.hpp"
#include "myassert.hpp"
#include "simpstr.hpp"
#ifdef DEBUG
#include "myassert.hpp"          // Needed for debugMessage
#endif
#include <stdio.h>

#define STATIC_INIT_WINFILE
#include "staticInit.h"

#include <windows.h>


using namespace FileSystem;


DirName& DirName::operator = (const char* name)
{
   SimpleString::operator = (name);
   size_t l = strlen(name);
   if( (l > 0) && (name[l-1] != '\\') )
      *this += '\\';
   return *this;
}



FileError::FileError()
{
#ifdef DEBUG
// throw GeneralError("FileError");
#endif
}


WinFileRWBase::WinFileRWBase()
{
   error = FALSE;
#ifdef DEBUG
   fName = NULL;
#endif
   h =INVALID_HANDLE_VALUE;
}

WinFileRWBase::~WinFileRWBase()
{
   if(h != INVALID_HANDLE_VALUE)
      CloseHandle(h);
}

Boolean WinFileRWBase::getError()
{
   return error;
}

Boolean WinFileReader::rewind()
{
   clearError();
   return seekTo(0);
}

Boolean

WinFileRWBase::seekTo(SeekPos where)
{
   if(error)
      return FALSE;

   if(SetFilePointer(h, where, NULL, FILE_BEGIN) == INVALID_FILE_SIZE)
   {
      error = TRUE;
#ifdef DEBUG
      debugMessage("Error seeking %s to %ld", fName, where);
#endif
      throw FileError();
      // return FALSE;
   }
   return TRUE;
}

SeekPos

WinFileRWBase::getPos()
{
   if(error)
      return -1;

   DWORD value = SetFilePointer(h, 0, NULL, FILE_CURRENT);

   if(value == INVALID_FILE_SIZE)
   {
      error = TRUE;
#ifdef DEBUG
      debugMessage("Error in getPos() %s", fName);
#endif
      throw FileError();
      // return -1;
   }
   else
      return value;
}

/*
 * Helper for file reading
 */



DirName WinFileReader::d_cdPath;
const char WinFileReader::registryName[] = "\\CD_Path";



WinFileReader::WinFileReader(LPCSTR name)
{
   ASSERT(name != 0);

#ifdef DEBUG
   fName = name;
   debugLog("WinFileReader::Opening '%s'\n", name);
#endif

   SimpleString fullPath;

   makeFullPath(fullPath, name);

   // char fullPath[200];

   /*
    *  if file doesn't exist, search cd
    */

   if(!fileExists(fullPath.toStr()))
   {

      Boolean fileFound = False;

      while(!fileFound)
      {
         /*
          *  merge name with cd path
          */

         makeCDFileName(fullPath, name);

         if(!fileExists(fullPath.toStr()))
         {
            /*
             *  If we're here then either player doesn't have cd in drive
             *  or registry file has incorrect drive.
             */

            char newPath[200];
            int result = pathDialog(newPath, d_cdPath);

            /*
             *  If OK, player has selected new cd drive
             */

            if(result == IDOK)
            {
#if 0
              if(d_cdPath != 0)
              {
                delete[] d_cdPath;
                d_cdPath = 0;
              }

              d_cdPath = new char[lstrlen(newPath)+1];
              ASSERT(d_cdPath != 0);

              lstrcpy(d_cdPath, newPath);
#else
               d_cdPath = newPath;
#endif

              /*
               *  Reset registry with new drive path
               */

              if(!setRegistryString("CD_Path", d_cdPath, registryName))
                throw FileError();

            }

            /*
             *  else throw error
             */

            else
              throw FileError();
         }
         else
           fileFound = True;
      }
   }
#if 0
   else
     lstrcpy(fullPath, name);
#endif

   h = CreateFile(fullPath.toStr(),
      GENERIC_READ,
      FILE_SHARE_READ,
      (LPSECURITY_ATTRIBUTES) NULL,
      OPEN_EXISTING,
      // FILE_ATTRIBUTE_READONLY | FILE_FLAG_SEQUENTIAL_SCAN,
      FILE_ATTRIBUTE_NORMAL,
      (HANDLE) NULL);

   ASSERT(h != INVALID_HANDLE_VALUE);

   if(h == INVALID_HANDLE_VALUE)
   {
      error = TRUE;
#ifdef DEBUG
      debugMessage("Can't open %s", fName);
#endif
      throw FileError();
   }
}
WinFileReader::~WinFileReader()
{
}

/*
 * Return size of file
 */

DWORD WinFileReader::fileSize() const
{
   DWORD result = GetFileSize(h, NULL);
   if(result == -1)
      throw FileError();
   return result;
}

const char* WinFileReader::getCDPath()
{
   return d_cdPath;
}

Boolean WinFileReader::read(LPVOID ad, DataLength length)
{
   if(error)
      return FALSE;

   DWORD bytesRead;
   if(!ReadFile(h, ad, length, &bytesRead, NULL) ||
      (bytesRead != length))
   {
      error = TRUE;
#ifdef DEBUG
      debugMessage("Error reading %s", fName);
#endif
      throw FileError();
      // return FALSE;
   }
   return TRUE;
}

Boolean

WinFileReader::read(LPVOID ad, DataLength length, DataLength& bytesRead)
{
   if(error)
      return FALSE;

   DWORD amountRead;
   if(!ReadFile(h, ad, length, &amountRead, NULL) ||
      (amountRead <= 0))
   {
      error = TRUE;
#ifdef DEBUG
      // debugMessage("Error reading %s", fName);
#endif
      // throw FileError();
      return FALSE;
   }

   bytesRead = amountRead;
   return TRUE;
}

/*
 * Make CD Path name
 */

void WinFileReader::makeCDFileName(SimpleString& dir, const char* fileName)
{
   /*
    *  if d_cdPath = 0, search registry for path
    */

   if(d_cdPath.toStr() == 0)
   {
      char path[200];
      if(!getRegistryString("CD_Path", path, registryName))
        throw FileError();

      d_cdPath = path;

#if 0
      size_t l = strlen(path);
      if(l != 0)
      {
         if(path[l-1] != '\\')
         {
            path[l-1] = '\\';
            path[l] = 0;
         }
      }

      d_cdPath = copyString(path);

      // d_cdPath = new char[lstrlen(path)+1];
      ASSERT(d_cdPath != 0);
      // lstrcpy(d_cdPath, path);
#endif
   }


   ASSERT(d_cdPath.toStr() != 0);

   if(d_cdPath.toStr() != 0)
   {
      dir = d_cdPath.toStr();
      dir += fileName;

     // lstrcpy(dir, d_cdPath);

#if 0    // this is done during the cd_path setup
     /*
      *  search for trailing backslash. if none, add one
      */

     char* c = dir;

     while(*c != '\0')
       c++;

     c--;

     if(*c != '\\')
       lstrcat(dir, "\\");

#endif

     // lstrcat(dir, fileName);
   }
}

/*
 * Helper for file writing
 */

WinFileWriter::WinFileWriter(LPCSTR name)
{

#ifdef DEBUG
   fName = name;
#endif

   SimpleString fullName;
   makeFullPath(fullName, name);


   h = CreateFile(fullName.toStr(),
      GENERIC_WRITE,
      0,
      NULL,
      CREATE_ALWAYS,
      FILE_ATTRIBUTE_NORMAL,
      (HANDLE) NULL);

   if(h == INVALID_HANDLE_VALUE)
   {
      error = TRUE;
#ifdef DEBUG
      debugMessage("Can't open %s", fName);
#endif
      throw FileError();
   }
}


WinFileWriter::~WinFileWriter()
{
}

Boolean

WinFileWriter::write(const void* ad, DataLength length)
{
   DWORD bytesWritten;
   if(!WriteFile(h, ad, length, &bytesWritten, NULL) ||
      (bytesWritten == 0))
   {
      error = TRUE;
#ifdef DEBUG
      debugMessage("Error Writing %s", fName);
#endif
      throw FileError();
      // return FALSE;
   }
   return TRUE;
}

/*
 * Ascii Reader
 */

const int WinFileAsciiReader::bufLength = 500;
const int WinFileAsciiReader::lineBufLength = 200;

WinFileAsciiReader::WinFileAsciiReader(LPCSTR name) :
   WinFileReader(name)
{
   buffer = new char[bufLength];
   ASSERT(buffer != 0);
   lineBuf = new char[lineBufLength];
   ASSERT(lineBuf != 0);
   bufIndex = 0;
   bufRead = 0;
   lastPos = 0;
}

/*
 * Ascii seek position is a bit more complex because it must take into
 * account where it is in the buffer
 */

Boolean

WinFileAsciiReader::seekTo(SeekPos where)
{
   /*
    * Seek directly into buffer if it is in memory
    * otherwise do a physical seek.
    */

   if( (where >= lastPos) && (where < (lastPos + bufRead)))
   {
      bufIndex = where - lastPos;
      return True;

   }
   else
   {
      bufIndex = 0;
      bufRead = 0;
      lastPos = where;
      return WinFileReader::seekTo(where);
   }
}

SeekPos

WinFileAsciiReader::getPos()
{
   return lastPos + bufIndex;
}

/*
 * These functions are errors... they should never be called!
 * In Ascii mode, only readLine() is valid
 */

Boolean

WinFileAsciiReader::read(LPVOID ad, DataLength length)
{
#ifdef DEBUG
   throw GeneralError("WinFileAsciiReader(%p,%lu) was called", ad, (ULONG) length);
#else
   return False;
#endif
}

Boolean

WinFileAsciiReader::read(LPVOID ad, DataLength length, DataLength& bytesRead)
{
#ifdef DEBUG
   throw GeneralError("WinFileAsciiReader(%p,%lu) was called", ad, (ULONG) length);
#else
   return False;
#endif
}

char* WinFileAsciiReader::readLine()
{
   int lineLength = 0;
   char prevC = 0;
//   char eol = 0;

   lineBuf[0] = 0;

   for(;;)
   {

      if(bufIndex >= bufRead)
      {
         DataLength bytesRead;

         lastPos = WinFileReader::getPos();

         if(!WinFileReader::read(buffer, bufLength, bytesRead) ||
            (bytesRead <= 0) )
         {
//            if(eol)
//               return lineBuf;
//            else
               return 0;
         }

         bufRead = bytesRead;
         bufIndex = 0;
      }


//       if(eol)
//       {
//          char c = buffer[bufIndex];
//          if( (c == eol) || ((c != '\r') && (c != '\n') ))
//             return lineBuf;
//          else
//             bufIndex++;
//       }
//       else
//       {
         char c= buffer[bufIndex++];


         switch(c)
         {

            // Handle any variant of end of line
            // possibilities are:
            //   \n\r
            //   \r\n
            //   \r
            //   \n

         case '\r':
         case '\n':
            while( (lineLength > 0) && (lineBuf[lineLength-1] == ' ') )
               lineLength--;

            if(lineLength)
            {
               lineBuf[lineLength] = 0;
               return lineBuf;
            }
//            eol = c;
            break;

         case '\t':
            c = ' ';
            // Drop through
         case ' ':
            if(!isspace(prevC))
               goto addChar;
            break;

         default:
         addChar:
            if(lineLength >= lineBufLength)
               return 0;
            else
               lineBuf[lineLength++] = c;
            break;
         }

         prevC = c;
//      }
   }
}

namespace FileSystem {

class FileSystemInfo
{

public:
   FileSystemInfo();
   ~FileSystemInfo() { }

   void makeFullPath(SimpleString& s, const char* fileName);
private:
   void init();

   SimpleString d_currentDir;
   bool d_initialised;
};

static FileSystemInfo fileSystem;

FileSystemInfo::FileSystemInfo() :
   d_initialised(false)
{
}

void FileSystemInfo::init()
{
   d_initialised = true;
   /*
   * NOTE : no longer putting current dir as suffix
   * we'll assume that the current path is correct
   */

   /*
   char buffer[MAX_PATH];
   size_t len = GetCurrentDirectory(MAX_PATH, buffer);

   if( (len == 0) || (buffer[len-1] != '\\') )
   {
      buffer[len] = '\\';
      buffer[len+1] = '\0';
   }

   d_currentDir += buffer;
   */



   /*
   * Make sure that either Wargamer.exe is local
   * or set current directory to the InstallPath directory
   *
   * @note Do not modify buffer returned from GetCommandLine
   * as it is the same buffer as passed to WinMain
   */

   std::string cmdLine = GetCommandLine();
   int startIndex = 0;
   char endChar = ' ';
   if(cmdLine.at(startIndex) == '\"') 
   {
      endChar = cmdLine.at(startIndex++);
   }
   int endIndex = cmdLine.find(endChar, startIndex);
   if(endIndex == std::string::npos) 
      endIndex = cmdLine.size();
   // char* s = strchr(cmdLine, endChar);
   // if(s) *s = 0;

   cmdLine = cmdLine.substr(startIndex, endIndex - startIndex);

   int endPath = cmdLine.find_last_of('\\');
   // char* endPath = strrchr(cmdLine, '\\');
   if(endPath == std::string::npos)
   // if(!endPath)
   {
      endPath = cmdLine.find_last_of(':');
      // endPath = strrchr(cmdLine,':');
      // if(endPath) ++endPath;
      if(endPath != std::string::npos)
         ++endPath;
   }

   if(endPath != std::string::npos)
   {
      // *endPath++ = 0;

      SetCurrentDirectory(cmdLine.substr(0, endPath).c_str());
      ++endPath;
   }
   else
   {  
      endPath = 0; // cmdLine;
   }

   // CommandLine did not have a path
   // so take alternative steps
   

   std::string wg_filename = cmdLine.substr(endPath, cmdLine.size());
   // const char * wg_filename = endPath; //"Wargamer.exe";
   char current_dir[MAX_PATH];
   GetCurrentDirectory(MAX_PATH, current_dir);

   char find_filename[MAX_PATH];
   sprintf(find_filename, "%s\\%s", current_dir, wg_filename.c_str());

   WIN32_FIND_DATA w32fd;
   HANDLE find_retval = FindFirstFile(find_filename, &w32fd);

   // file not found locally
   if(find_retval == INVALID_HANDLE_VALUE) {

      char wg_dir[MAX_PATH];
      if(!getRegistryString("InstallPath", wg_dir, ""))
      {
         throw GeneralError(std::string("Can't find where " + wg_filename + " was installed").c_str());
      }

      SetCurrentDirectory(wg_dir);
      GetCurrentDirectory(MAX_PATH, current_dir);

      find_retval = FindFirstFile(wg_filename.c_str(), &w32fd);
      if(find_retval == INVALID_HANDLE_VALUE)
      {
         FORCEASSERT(std::string("ERROR - Can't find where " + wg_filename + " is located\n").c_str());
         throw GeneralError(std::string("Can't find where " + wg_filename + " was installed").c_str());
      }
   }

   d_currentDir = current_dir;
}




void FileSystemInfo::makeFullPath(SimpleString& s, const char* fileName)
{
   if(!d_initialised)
   {
      init();
   }

   if(fileName[0] == '\\')
   {
      if(fileName[1] != '\\')
      {
         // Add drivename
      }
   }
   else if(!strchr(fileName, ':'))
   {
      s += d_currentDir.toStr();
      s += "\\";
   }


   s += fileName;
}

/*
 * Check if a folder exists
 *
 * Return TRUE if it exists, FALSE if not.
 */

bool dirExists(LPCSTR name)
{
   ASSERT(name != 0);

   DWORD attr = GetFileAttributes(name);

   /*
    * if it doesn't exist check cd drive
    */

   if((attr == 0xffffffff) || (!(attr & FILE_ATTRIBUTE_DIRECTORY)))
   {
      if(WinFileReader::getCDPath())
      {
        SimpleString fullPath;
        WinFileReader::makeCDFileName(fullPath, name);

        attr = GetFileAttributes(fullPath.toStr());
      }
   }

   if(attr == 0xffffffff)
      return FALSE;
   if(!(attr & FILE_ATTRIBUTE_DIRECTORY))
      return FALSE;

   return TRUE;

}

/*
 * Check if a file exists
 * Return TRUEif it exists, FALSE if not.
 */

bool fileExists(LPCSTR name)
{
   ASSERT(name != 0);
#if 0
   OFSTRUCT openBuf;

   HFILE h = OpenFile(name, &openBuf, OF_EXIST);

   return (h != HFILE_ERROR);
#endif

   DWORD attr = GetFileAttributes(name);

   if(attr == 0xffffffff)
      return FALSE;
   if(attr & FILE_ATTRIBUTE_DIRECTORY)
      return FALSE;

   return TRUE;
}



void makeFullPath(SimpleString& s, const char* fileName)
{
   fileSystem.makeFullPath(s, fileName);
}

}; // namespace FileSystem

/*
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.3  2001/07/02 15:23:18  greenius
 * Battle, Campaign, Combined game and Campaign Editor now compile and run... mostly
 *
 * Revision 1.2  2001/06/18 08:25:46  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 * Revision 1.1  2001/06/13 08:52:39  greenius
 * Initial Import
 *
 * Revision 1.2  1996/06/22 19:06:06  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.1  1995/11/22 10:43:46  Steven_Green
 * Initial revision
 *
 *----------------------------------------------------------------------
 */
