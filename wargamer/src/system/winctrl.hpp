/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
#ifndef WINCTRL_HPP
#define WINCTRL_HPP

#ifndef __cplusplus
#error winctrl.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1995, Steven Morle-Green, All Rights Reserved
 *----------------------------------------------------------------------
 *
 * Helper classes for Window Controls
 *
 *----------------------------------------------------------------------
 */

#include "sysdll.h"
#ifndef MYTYPES_H
#include "mytypes.h"
#endif
#include <windows.h>
#include <commctrl.h>
#include "StringPtr.hpp"

/*
 * A debugging aid...
 */

#ifdef DEBUG

#ifndef MYASSERT_H
#include "myassert.hpp"
#endif

SYSTEM_DLL void assertClass(HWND h, LPCSTR name, LPCSTR file, int line);
#define ASSERT_CLASS(h, name) assertClass(h, name, __FILE__, __LINE__)

#else

#define ASSERT_CLASS(h, name) { }

#endif


/*
 * Base class for controls
 */

class SYSTEM_DLL WindowControl {
protected:
   HWND hwnd;
public:
   WindowControl() : hwnd(NULL) { }
   WindowControl(HWND h);
   WindowControl(HWND h, int id);

    WindowControl& operator=(HWND h);
    WindowControl& operator=(const WindowControl& w);

   void enable(bool flag);
   void show(bool flag);
   void move(const RECT& r);     // Set window's location
   HWND getHWND() const { return hwnd; }
};

/*
 * Combo Box
 */

/*
 * Pairs of value / string for initComboItems
 */

struct ComboInit {
   int value;
   const char* name;
};


class SYSTEM_DLL ComboBox : public WindowControl {
public:
   ComboBox(HWND h);
   ComboBox(HWND h, int id);

   void reset();
   void add(const char* name, int value);
   void insert(const char* name, int id, int value);
   void deleteItem(int index);
   int getValue();
   int getIndex();
   int getValue(int index);
   int getNItems();
   void setValue(int value);
   void set(int index);
   void initItems(const ComboInit* data);

   StringPtr getText();
   StringPtr getText(int index);
   void setText(const char* text);  // set item to value with given text
};


/*
 * Similar functions for List boxes
 */


class SYSTEM_DLL ListBox : public WindowControl  {
public:
   ListBox() : WindowControl() { }
   ListBox(HWND h);
   ListBox(HWND h, int id);

   void reset();
   void remove(int index);
   void add(const char* name, int id);
   void insert(const char* name, int id, int value);
   int getValue() const;
   int getValue(int index) const;
   void setValue(int value);
   void set(int index);
   void setTop(int index);
   int getIndex() const;
   int getItemFromPoint(int x, int y) const;
   int getTop() const;
   int getNItems() const;
   int getMultiSel(int nItems, LPINT items) const;

   void create(int id, HWND hParent, DWORD exStyle, DWORD style, const RECT& r);
};



/*
 * Image List class
 */

class SYSTEM_DLL ImageList {
   HIMAGELIST image;
   bool owned;
   bool gotSize;
   int imgWidth;
   int imgHeight;
public:
   ImageList();
   ImageList(HIMAGELIST img);
   ~ImageList();

   bool loadBitmap(HINSTANCE inst, LPCSTR lpbmp, int cx, COLORREF mask = CLR_NONE);
   void unloadBitmap();
   bool addBitmap(HINSTANCE inst, LPCSTR lpbmp, COLORREF mask = CLR_NONE);

   bool isInitialised() const;

   bool draw(HDC hdc, int i, int x, int y) const;
   bool draw(HDC hdc, int i, int x, int y, int w, int h) const;
   HICON getIcon(int i) const;
   int getImgHeight();
   int getImgWidth();

   int getCount() const;

   HIMAGELIST getHandle() const { return image; }
private:
   void setSize();
};

/*
 * Track Bar class
 */

class SYSTEM_DLL TrackBar : public WindowControl  {
public:
   TrackBar() : WindowControl() { }
   TrackBar(HWND h);
   TrackBar(HWND h, int id);

   int getValue();
   void setValue(int value);
   void setRange(UWORD minRange, UWORD maxRange);
   void setTickFreq(int value);

   void create(int id, LPCTSTR text, HWND hParent, DWORD exStyle, DWORD style, const RECT& r);
};

/*
 * Button Class
 */

class SYSTEM_DLL Button : public WindowControl  {
public:
   Button() : WindowControl() { }
   Button(HWND h);
   Button(HWND h, int id);

   void setCheck(bool flag);
   bool getCheck();
   void setImage(HBITMAP image);
   void setIcon(HICON image);

   void create(int id, LPCTSTR text, HWND hParent, DWORD exStyle, DWORD style, const RECT& r);
};

class SYSTEM_DLL ListView : public WindowControl {
public:
   ListView(HWND h);
   ListView(HWND h, int id);

   void reset();
   void setCount(int n);
   void addItem(const LV_ITEM& item);
   long getItem(int item);
   void setImageList(HIMAGELIST img);
};

class SYSTEM_DLL ScrollBar : public WindowControl {
public:
   ScrollBar(HWND h);
   ScrollBar(HWND h, int id);

   void setRange(int nMin, int nMax, UINT nPage);
   void setPos(int nPos);
   int getPos();
};

/*
 * Spinner Class
 */

class SYSTEM_DLL Spinner : public WindowControl {
public:
   Spinner(HWND h);
   Spinner(HWND h, int id);

   void setRange(int nMin, int nMax);
   void setValue(int value);
   int getValue();
};

/*
 * TreeView class...
 */


// A class that items passed to addItem should be derived from

class TreeViewItem
{
   public:
      TreeViewItem() { }
      ~TreeViewItem() { }
};

class SYSTEM_DLL TreeView : public WindowControl
{
   public:
      TreeView() : WindowControl() { }
      TreeView(HWND h);
      TreeView(HWND h, int id);

      void reset();
         // Clear all items
      // HTREEITEM addItem(HTREEITEM parent, UINT after, LPCSTR text, int image, int selImage, int children, TreeViewItem* data);
      HTREEITEM addItem(const TV_INSERTSTRUCT& tvi);
         // Add a new item
      void setImageList(HIMAGELIST img);
      void setImageList(const ImageList& imgList);
         // Set the imagelist

      void deleteItem(HTREEITEM item);
         // Delete item and it's children

      void showItem(HTREEITEM item);
         // Ensure item is visible

      void create(int id, HWND hParent, DWORD exStyle, DWORD style, const RECT& r);
         // Create a TreeView Control
};

/*
 * A Static Control
 */

class SYSTEM_DLL StaticWindow : public WindowControl
{
   public:
      StaticWindow() : WindowControl() { }
         // Construct as empty control
      StaticWindow(HWND h);
         // Construct with control's HWND
      StaticWindow(HWND h, int id);
         // Construct from parent window and id

      void create(int id, HWND hParent, DWORD exStyle, DWORD style, const RECT& r);
         // Create a TreeView Control

      void setText(LPCTSTR text);
};

/*
 * Some class names to avoid duplicating strings in program
 */

extern const char SYSTEM_DLL COMBOCLASS[];
extern const char SYSTEM_DLL LISTBOXCLASS[];
extern const char SYSTEM_DLL TRACKBARCLASS[];
extern const char SYSTEM_DLL BUTTONCLASS[];
extern const char SYSTEM_DLL STATICCLASS[];
extern const char SYSTEM_DLL MDICLIENTCLASS[];
extern const char SYSTEM_DLL LISTVIEWCLASS[];
extern const char SYSTEM_DLL SCROLLBARCLASS[];
extern const char SYSTEM_DLL TOOLTIPSCLASS[];
extern const char SYSTEM_DLL SPINNERCLASS[];
extern const char SYSTEM_DLL TREEVIEWCLASS[];

/*
 * Some support Functions
 */

HWND SYSTEM_DLL addBorder(HWND hWnd, int x, int y, int w, int h);

// HWND addOwnerDrawButton(HWND hWnd, HWND hTooltip, int id, int x, int y, int w, int h);
   // Obsolete

HWND SYSTEM_DLL addOwnerDrawButton(HWND hWnd, int id, int x, int y, int w, int h);

// HWND addOwnerDrawButton(HWND hWnd, DWORD exStyle, HWND hTooltip, int id, int x, int y, int w, int h);
   // Obsolete

HWND SYSTEM_DLL addOwnerDrawButton(HWND hWnd, DWORD exStyle, int id, int x, int y, int w, int h);

// HWND addOwnerDrawStatic(HWND hWnd, HWND hTooltip, int id, int x, int y, int w, int h);
   // Obsolete

HWND SYSTEM_DLL addOwnerDrawStatic(HWND hWnd, int id, int x, int y, int w, int h);


#endif /* WINCTRL_HPP */

/*
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/06/13 08:52:39  greenius
 * Initial Import
 *
 *----------------------------------------------------------------------
 */
