/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef MEMPTR_HPP
#define MEMPTR_HPP

#ifndef __cplusplus
#error memptr.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1995, Steven Morle-Green, All Rights Reserved
 *----------------------------------------------------------------------
 *
 *	Memory Pointer Template
 * as defined in Stroustrup's "The C++ Programming Language"
 * in the chapter about exceptions.
 *
 *----------------------------------------------------------------------
 */

#include "sysdll.h"
#include "myassert.hpp"

template<class T>
class MemPtr {
	T* ptr;
public:
	MemPtr(size_t n)
	{
		ptr = new T[n];

		ASSERT(ptr != 0);
	};

	~MemPtr()
	{
		ASSERT(ptr != 0);

		delete[] ptr;
		ptr = 0;
	};

	const T* get() const
	{
		ASSERT(ptr != 0);
		return ptr;
	};

	T* get()
	{
		ASSERT(ptr != 0);
		return ptr;
	};



	T& operator[] (int n)
	{
		return ptr[n];
	}

#ifdef DEBUG
private:
	MemPtr(MemPtr<T>& m)
	{
		throw GeneralError("MemPtr<T> is being copy constructed in " __FILE__ ", Line %d\nTry casting it to (T*)", __LINE__);
	}

	MemPtr<T>& operator = (MemPtr<T>& m)
	{
		throw GeneralError("MemPtr<T> is being copied with =() in " __FILE__ ", Line %d\nTry casting it to (T*)", __LINE__);
	}
#endif

};

#endif


/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.2  2001/06/18 08:25:46  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 * Revision 1.1  2001/06/13 08:52:39  greenius
 * Initial Import
 *----------------------------------------------------------------------
 */