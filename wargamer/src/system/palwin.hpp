/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef PALWIN_HPP
#define PALWIN_HPP

#ifndef __cplusplus
#error palwin.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *	Debug Window that displays a 256 colour palette
 *
 *----------------------------------------------------------------------
 */

#include "sysdll.h"
#include "palwind.hpp"

class DrawDIBDC;

/*
 * A Debug window that displays a palette
 */

class SYSTEM_DLL PalWindow :
	public WindowBaseND, 
	public PaletteWindow
{
	static ATOM classAtom;
	static const char s_className[];

	HDC hdc;
	DrawDIBDC* dib;

public:
	PalWindow(HWND parent);
	~PalWindow() { selfDestruct(); }


private:
	static ATOM registerClass();
	LRESULT procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
	const char* className() const { return s_className; }

	void onDestroy(HWND hWnd);
	void onPaint(HWND hWnd);
	BOOL onCreate(HWND hWnd, CREATESTRUCT* lpCreateStruct);
    // void onClose(HWND hwnd);

#ifdef DEBUG
	void checkPalette() { }
#endif
};


#endif /* PALWIN_HPP */

