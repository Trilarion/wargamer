/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef BARGRAPH_HPP
#define BARGRAPH_HPP

#ifndef __cplusplus
#error bargraph.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1995, Steven Morle-Green, All Rights Reserved
 *
 *----------------------------------------------------------------------
 */

#include "sysdll.h"
#include <windows.h>
#include "mytypes.h"
// #include "gamedefs.hpp"




class DrawDIB;

#define GREEN   RGB(0, 255, 0)
#define BLUE    RGB(0, 0, 255)
#define RED     RGB(255, 0, 0)
#define BLACK   RGB(0, 0, 0)
#define L_GREY  RGB(230, 230, 230)
#define D_GREY  RGB(100, 100, 100)
#define WHITE   RGB(255, 255, 255)

#define S_BAR    0
#define L_BAR    1
#define TINY_BAR 2

enum Level {NO_LEVEL, BLACK_LEVEL, RED_LEVEL, BLUE_LEVEL, GREEN_LEVEL};



class SYSTEM_DLL BarGraph {
	BarGraph(const BarGraph&);
	BarGraph& operator = (const BarGraph&);
  public:
	 BarGraph() {}
	 ~BarGraph() {}


#if 0		// Simplified
	 // Generic Bargraph

	 // void putBarGraph(HDC hdc, int x, int y, int yStart, int xW, int yW, COLORREF color);
	 static void putBarGraph(HDC hdc, int x, int y, unsigned int value, unsigned int maxValue, unsigned int width, unsigned int height, COLORREF color, Boolean showNumber);

	 /*
	  * Specific Bargraphs
	  */

	 static void putAttribLevel(HDC hdc, Attribute level, int x, int y);
	 static void putLongLevel(HDC hdc, long level, int x, int y);
	 static void putUWORDLevel(HDC hdc, AttributePoints level, int x, int y);
	 static void putSmAttribLevel(HDC hdc, Attribute level, int x, int y);
	 static void putSmLongLevel(HDC hdc, long level, int x, int y);
	 static void putTinyAttribLevel(HDC hdc, Attribute level, int x, int y);
	 static void putTinyLongLevel(HDC hdc, long level, int x, int y);
	 static void putTinyAttribLevel(DrawDIB* dib, Attribute level, LONG x, LONG y);
	 // static Level getAttribLevel(Attribute level);
	 // static Level getUWORDLevel(AttributePoints level);
#else
	 static void putBarGraph(HDC hdc, int x, int y, unsigned int value, unsigned int maxValue, unsigned int width, unsigned int height, COLORREF color, Boolean showNumber);
	 	// Generic Bargraph Function

	 static void putAttribLevel(HDC hdc, UBYTE level, int x, int y)
	 {
	 	putBarGraph(hdc, x,y, level,UBYTE_MAX+1, 120,8, getColor(level, UBYTE_MAX+1), True);
		// was y+4
	 }

	static void putSmAttribLevel(HDC hdc, UBYTE level, int x, int y)
	{
	 	putBarGraph(hdc, x,y, level,UBYTE_MAX+1, 50,5, getColor(level, UBYTE_MAX+1), False);
	}

	static void putTinyAttribLevel(HDC hdc, UBYTE level, int x, int y)
	{
		putTinyLevel(hdc, level, UBYTE_MAX+1, x, y, 20, 2);
	}

	static void putTinyAttribLevel(DrawDIB* dDIB, UBYTE level, LONG x, LONG y)
	{
		putTinyLevel(dDIB, level, UBYTE_MAX+1, x, y, 20, 2);
	}

	 private:
	 	static COLORREF getColor(long value, long maxValue);
		static void putTinyLevel(HDC hdc, long value, long maxValue, int x, int y, int w, int h);
		static void putTinyLevel(DrawDIB* dib, long value, long maxValue, int x, int y, int w, int h);
#endif
};

class SYSTEM_DLL Lines {
  public:
	 static void putLine(HDC hdc, int y, int w);
	 static void putLine(HDC hdc, int sX, int sY, int fX, int fY, int w);

	 static void putLine(HDC hdc, int sX, int sY, int fX, int fY, int w,
					  COLORREF color);
	 static void putRect(HDC hdc, int tX, int tY, int bX, int bY, int w,
					  COLORREF color);
	 static void putRect(DrawDIB *dDIB, LONG x, LONG y, LONG width, LONG height,
					  COLORREF color);
	 static void putRect(HDC hdc, int lX, int tY, int rX, int bY, int w,
					  COLORREF color1, COLORREF color2, COLORREF color3,
					  COLORREF color4);
	 static void putOpenTopRect(HDC hdc, int lX, int tY, int rX, int bY, int w,
					  COLORREF color1, COLORREF color2, COLORREF color3);
	 static void putOpenBottomRect(HDC hdc, int lX, int tY, int rX, int bY, int w,
					  COLORREF color1, COLORREF color2, COLORREF color3);
};


void SYSTEM_DLL drawHalfPie(HDC hdc, const RECT* r, ULONG amount, ULONG total, BOOL percent);









struct SYSTEM_DLL BarChartInfo
{
	public:
		BarChartInfo() :
			d_x(0),
			d_y(0),
			d_width(0),
			d_height(0),
			d_value(0),
			d_maxValue(0),
			d_colors(0),
			d_nColors(0),
			d_background(PALETTERGB(64,64,64))
		{
			d_border[0] = PALETTERGB(0,0,0);
			d_border[1] = PALETTERGB(255,255,255);
		}

		void setLocation(LONG x, LONG y, LONG w, LONG h) 
		{
			d_x = x;
			d_y = y; 
			d_width = w; 
			d_height = h; 
		}

		void setValue(int value, int maxValue) { d_value = value; d_maxValue = maxValue; }
		void setColors(int n, const COLORREF* col) { d_nColors = n; d_colors = col; }
		void setBorder(COLORREF b1, COLORREF b2) { d_border[0] = b1; d_border[1] = b2; }
		void setBackground(COLORREF b) { d_background = b; }
		
		void draw(DrawDIB* dib);

	private:

	#ifdef DEBUG
		void check();
	#endif

		void fillBurst(DrawDIB* dib, LONG x, LONG y, LONG w, LONG h, LONG fullWidth, COLORREF c1, COLORREF c2);

		LONG d_x;
		LONG d_y;
		LONG d_width;
		LONG d_height;
		int d_value;
		int d_maxValue;
		const COLORREF* d_colors;
		int d_nColors;
		COLORREF d_border[2];
		COLORREF d_background;
};






#endif /* BARGRAPH_HPP */

