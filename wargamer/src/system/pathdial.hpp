/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef PATHDIAL_HPP
#define PATHDIAL_HPP

#include "sysdll.h"
#include "dialog.hpp"


class SYSTEM_DLL PathDial : public ModalDialog {

	  char* d_path;
	  char* d_oldPath;

	public:
	  PathDial();
	  ~PathDial();

	  int displayPathDial(char* path, const char* oldPath);

	private:
	  BOOL procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
	  void onCommand(HWND hWnd, int id, HWND hwndCtl, UINT codeNotify);
	  BOOL onInitDialog(HWND hwnd, HWND hwndFocus, LPARAM lParam);
	  void onDestroy(HWND hwnd) {}
	  void onClose(HWND hwnd);

	  void onBrowse();
	  void onCancel();

};

SYSTEM_DLL int pathDialog(char* path, const char* oldPath);


#endif
