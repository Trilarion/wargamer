/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef CDIALOG_HPP
#define CDIALOG_HPP

/*
 * Class for customizing the appearance of a Dialog Box
 */

#include "sysdll.h"
#include "palwind.hpp"
#include "simpstr.hpp"
#include "fonts.hpp"
#include "grtypes.hpp"

struct CustomBorderInfo;

class DrawDIB;
class DrawDIBDC;

namespace Greenius_System
{


struct SystemButton {
	enum Type {
		Min,
		Max,
		Close,

		HowMany,

		Inactive
	} d_type;

	RECT d_rect;
	Boolean d_selected;

	SystemButton() :
	  d_type(Inactive),
	  d_selected(False)
	{
	  SetRect(&d_rect, 0, 0, 0, 0);
	}
};

class SYSTEM_DLL CustomDialog : public SubClassWindowBase {
	 HWND d_hwnd;

	 SimpleString d_caption;
	 const DrawDIB* d_captionBkDib;
	 const DrawDIB* d_bkDib;

	 static DrawDIBDC* s_captionDIB;
	 static DrawDIBDC* s_mainDIB;
	 static UWORD s_instanceCount;

	 Font d_captionFont;

	 SystemButton d_sysButtons[SystemButton::HowMany];
	 CustomBorderInfo d_bc;

  public:
	 enum Styles {
		EtchedFrame = 0x01,  // draws an etched-frame around client area
		MinMax      = 0x02,  // has min-max buttons
		Close       = 0x04   // has close button
	 };
  private:

	 UBYTE d_style;
  public:
	 CustomDialog(const CustomBorderInfo& border);
	 ~CustomDialog();

	 void init(HWND hwnd);

	 void setBkDib(const DrawDIB* dib) { d_bkDib = dib; }
	 void setCaption(const char* text);
	 void setCaptionBkDib(const DrawDIB* dib) { d_captionBkDib = dib; }
	 void setCaptionFont(const LogFont& lf) { d_captionFont.set(lf); }
	 void setStyle(UBYTE style);

  private:
	 LRESULT procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
	 BOOL onEraseBk(HWND hwnd, HDC hdc);
	 void onNCPaint(HWND hwnd, HRGN hrgn);
   UINT onNCCalcSize(HWND hwnd, BOOL fCalcValidRects, NCCALCSIZE_PARAMS * lpcsp);

	 BOOL onNCActivate(HWND hwnd, BOOL fActive, HWND hwndActDeact, BOOL fMinimized);
	 UINT onNCHitTest(HWND hwnd, int x, int y);
	 void onNCLButtonDown(HWND hwnd, BOOL fDoubleClick, int x, int y, UINT codeHitTest);
    void onNCLButtonUp(HWND hwnd, int x, int y, UINT codeHitTest);
	 void onSize(HWND hwnd, UINT state, int cx, int cy);
	 void onMove(HWND hwnd, int cx, int cy);
     void onDestroy(HWND hwnd);

	 void sysButtonClicked(SystemButton::Type t);
	 void setSysButtonPos();
	 void drawMinButton(int x, int y, int cx, int cy, ColourIndex color);
	 void drawMaxButton(int x, int y, int cx, int cy, ColourIndex color);
	 void drawCloseButton(int x, int y, int cx, int cy, COLORREF color);
    int getHitTestID(UINT codeHitTest);

};


};	// namespace Greenius_System


#endif
