/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
#ifndef FTOL_HPP
#define FTOL_HPP


/*
        STORE & POP A FLOAT OFF THE FPU STACK
*/


#ifdef __WATCOM_CPLUSPLUS
// this is faster than a (int) float conversion
void FloatToLong(long * dst, float src);
#pragma aux FloatToLong= \
    "   fistp  dword [eax]  "\
parm [eax] [8087] modify [8087];

// #elif defined(_MSC_VER)
#else

inline void FloatToLong(long * dst, float src)
{
   *dst = static_cast<long>(src);
}

#endif



#endif

/*
 *----------------------------------------------------------------------
 * $Log$
 *----------------------------------------------------------------------
 */
