/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef MCIWIND_HPP
#define MCIWIND_HPP

#include "sysdll.h"
#include "winctrl.hpp"

/* void onMCINotifyMode(HWND hwnd, HWND hwndMCI, LONG mode) */
#define HANDLE_MCIWNDM_NOTIFYMODE(hwnd, wParam, lParam, fn) \
	 ((fn)((hwnd), (HWND)(wParam), (LONG)(lParam)), 0L)


class SYSTEM_DLL MCIWindow : public WindowControl {
  public:
	 MCIWindow(HWND hwnd) : WindowControl(hwnd) {}

	 void play();
	 void play(const char* fileName);
	 void rewind();
	 void closeDevice();
	 void kill();

	 static HWND create(HWND parent, const char* fileName);
	 static void onMCINotifyMode(HWND hwnd, HWND hwndMCI, LONG mode);
};


#endif
