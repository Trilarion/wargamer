/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 * Synchronization objects
 *
 * This used to be part of thread, but thread was getting complicated
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "sync.hpp"
#ifdef __WATCOM_CPLUSPLUS__
#include <string.hpp>
#else
#include <string>
typedef std::string String;
#endif
#include "app.hpp"
#include "myassert.hpp"
#include "except.hpp"
// #include <algobase.h>         // STL min/max/swap functions
// #include <algo.h>             // STL find_if

// #define DEBUG_THREAD


namespace Greenius_System
{

const int s_timeOut = 20000;


// SyncObjectH::~SyncObjectH()
SyncObject::~SyncObject()
{
   if(d_handle != NULL)
   {
      CloseHandle(d_handle);
      d_handle = NULL;
   }
}

/*
* Make up a name for some object
*/

static String makeObjectName(const char* baseName)
{
   static int count = 0;

   String s = baseName;

   char buffer[20];     // Big enough for integer+Hex long

   wsprintf(buffer, "%lx_%d", (long) APP::instance(), count++);

   s += buffer;

   return s;
}

/*
* Event Class for sending signals between threads
*/

/*
* Constructor:
*    Creates Event
*    Starts in a non-signalled State
*    It is Manual Event, so after being signalled, it must
*      be manually reset.
*    Makes a name by incrementing a static count.
*
* In a more general case, we'll need a constructor that
* is given a name, so that seperate processes can find
* an Event without access to a shared Event class instance.
*/

void Event::init(BOOL manualState, BOOL initialState)
{
   String name = makeObjectName("WargamerEvent");

   handle(CreateEvent(NULL, manualState, initialState, name.c_str()));

   if(handle() == NULL)
      throw EventError();

#ifdef DEBUG_EVENT
   debugLog("Created Event %s at %p\n", (const char*) name, (void*) handle());
#endif

}


/*
* Signal an event
*/

void Event::set() const
{
   ASSERT(handle() != NULL);

#ifdef DEBUG_EVENT
   debugLog("Event::set %p\n", (void*) handle());
#endif

   if(SetEvent(handle()) == 0)
      throw EventError();
}

void Event::reset() const
{
   ASSERT(handle() != NULL);

#ifdef DEBUG_EVENT
   debugLog("Event::reset %p\n", (void*) handle());
#endif

   if(ResetEvent(handle()) == 0)
      throw EventError();
}

void Event::pulse() const
{
   ASSERT(handle() != NULL);

#ifdef DEBUG_EVENT
   debugLog("Event::pulse %p\n", (void*) handle());
#endif

   if(PulseEvent(handle()) == 0)
      throw EventError();
}


DWORD Event::wait(DWORD dwMilliseconds) const
{
   ASSERT(handle() != NULL);


#ifdef DEBUG_EVENT
   Boolean infinite = False;
   if(dwMilliseconds == INFINITE)
   {
      dwMilliseconds = 60000;    // 60 seconds?
      infinite = True;

      for(;;)
      {
#ifdef DEBUG_EVENT
         debugLog("+Event::wait %p\n", (void*) handle());
#endif

         DWORD result = WaitForSingleObject(handle(), dwMilliseconds);

#ifdef DEBUG_EVENT
         debugLog("-Event::wait %p... finished\n", (void*) handle());
#endif

         if(result == WAIT_TIMEOUT)
         {
            int whatDo = messageBox("Event::Wait Timeout",
               "Event Timed out\r\r"
               "Do you want to continue waiting?",
               MB_YESNO);

            if(whatDo == IDNO)
               throw EventError();
         }

         // ASSERT(result != WAIT_TIMEOUT);

         if(result == WAIT_OBJECT_0)
            return result;
         else if(result != WAIT_TIMEOUT)
            throw EventError();
      }
   }
#endif      // DEBUG Version


#ifdef DEBUG_EVENT
   debugLog("+Event::wait %p\n", (void*) handle());
#endif

   DWORD result = WaitForSingleObject(handle(), dwMilliseconds);

#ifdef DEBUG_EVENT
   debugLog("-Event::wait %p... finished\n", (void*) handle());
#endif

   if((result != WAIT_OBJECT_0) && (result != WAIT_TIMEOUT))
      throw EventError();

   return result;
}



Boolean Event::isSet() const
{
   return (wait(0) == WAIT_OBJECT_0);
}


/*
* Mutex class
*/

Mutex::Mutex()
{
   String name = makeObjectName("WargamerMutex");
   handle(CreateMutex(NULL, FALSE, name.c_str()));

   if(handle() == NULL)
      throw MutexError();

#ifdef DEBUG_EVENT
   debugLog("Created Mutex %s at %p\n", (const char*) name, (void*) handle());
#endif
}

void Mutex::get() const
{
   ASSERT(handle() != NULL);

#ifdef DEBUG_EVENT
   debugLog("+GetMutex %p\n", (void*) handle());
#endif

#ifdef DEBUG
   DWORD result;

   do
   {
      result = WaitForSingleObject(handle(), s_timeOut);

      if(result != WAIT_OBJECT_0)
      {
         static char title[] = "Mutex Timeout";
         static char message[] = "Mutex timed out\r\n\r\n"
            "Click Yes to ignore the timeout and continue"
            "Click No to continue waiting";
         ExceptionUtility::YesNo answer = ExceptionUtility::ask(title, message);
         if(answer == ExceptionUtility::Yes)
            result = WAIT_OBJECT_0;
      }

   } while(result != WAIT_OBJECT_0);

#else
   DWORD result = WaitForSingleObject(handle(), INFINITE);
   if(result != WAIT_OBJECT_0)
      throw MutexError();
#endif

#ifdef DEBUG_EVENT
   debugLog("-GetMutex %p... finished\n", (void*) handle());
#endif

}

void Mutex::release() const
{
   ASSERT(handle() != NULL);

#ifdef DEBUG_EVENT
   debugLog("ReleaseMutex %p\n", (void*) handle());
#endif

   if(!ReleaseMutex(handle()))
      throw MutexError();
}

/*
 * Read/Write Access Lock
 */

RWLock::RWLock() :
#ifdef DEBUG
   d_writeCount(0),
   d_readCount(0),
#endif
   d_inUse(),
   d_writeLock(),
   d_readCounters(),
   d_counterCleared()
{
   d_counterCleared.set();
}

RWLock::~RWLock() 
{ 
}


void RWLock::startRead() const
{
#ifdef DEBUG
   ++d_readCount;
#endif
   d_writeLock.get();
   {
      d_inUse.get();
      {
         ++d_readCounters[GetCurrentThreadId()];
      }
      d_inUse.release();
   }
   d_writeLock.release();
}

void RWLock::endRead() const
{
   ASSERT(d_readCounters[GetCurrentThreadId()] > 0);
   d_inUse.get();
   {
      if(--d_readCounters[GetCurrentThreadId()] == 0)
         d_counterCleared.set();
   }
   d_inUse.release();
#ifdef DEBUG
   --d_readCount;
#endif
}

inline Boolean RWLock::isReading() const
{
   struct CounterSet
   {
      CounterSet() : d_currentID(GetCurrentThreadId()) { }

      bool operator()(ReadCounters::const_reference rc) const
      {
         return ((rc.second != 0) && (rc.first != d_currentID));
      }

      DWORD d_currentID;
   };

   bool result = false;
   d_inUse.get();
   {
//swg:19may2001: find_if<map...> seems to be broken for Watcom with STLport
//      result = (find_if(d_readCounters.begin(), d_readCounters.end(), CounterSet()) != d_readCounters.end());
      CounterSet pred;
      ReadCounters::const_iterator it(d_readCounters.begin());
      while( (it != d_readCounters.end()) && !pred(*it))
         ++it;
      result = (it != d_readCounters.end());
   }
   d_inUse.release();
   return result;
}



void RWLock::startWrite()
{
#ifdef DEBUG
   ++d_writeCount;
#endif
   d_writeLock.get();

   while(isReading())
   {
      d_writeLock.release();
      d_counterCleared.wait();
      d_writeLock.get();
   }
}

void RWLock::endWrite()
{
   d_writeLock.release();
#ifdef DEBUG
   --d_writeCount;
#endif
}


/*
* Simulates NT's WaitableTimer object
*/

/* @todo: This code is bad!
 * Constructor does too much and can throw exceptions
 * Remove the started parameter and force caller to call start themselves
 */

WaitableTimer::WaitableTimer(UINT ms, Boolean started) :
   AutoEvent(),
   MMTimer(ms, Repeat, false, this)
   // MMTimer(ms, Repeat, false, 0)
{
   if(started)
      start();
}

WaitableTimer::~WaitableTimer()
{
   stop();
}

void WaitableTimer::start()
{
   try
   {
      MMTimer::start();     // Do a EventSet timer
   }
   catch(MMTimerError e)
   {
      throw WaitableTimerError(e.get());
   }
}

void WaitableTimer::stop()
{
   MMTimer::stop();
}


void WaitableTimer::proc()
{
   //--- Do nothing...
   // AutoEvent::set();
}


WaitableCounter::WaitableCounter(UINT ms, bool autoStart) :
   AutoEvent(),
   MMTimer(ms, Repeat, false, 0),
   d_value(0)
{
   if(autoStart)
      start();
}

WaitableCounter::~WaitableCounter()
{
   stop();
}

int WaitableCounter::getAndClear()
{
   d_lock.enter();
   int value = d_value;
   d_value = 0;
   // d_event.reset();
   AutoEvent::reset();
   d_lock.leave();
   return value;
}

void WaitableCounter::proc()
{
   d_lock.enter();
   // d_event.set();
   AutoEvent::set();
   ++d_value;
   d_lock.leave();
}

}; // namespace Greenius_System


/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.3  2001/07/30 09:27:23  greenius
 * Various bug fixes
 *
 *----------------------------------------------------------------------
 */
