/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef CSCROLL_HPP
#define CSCROLL_HPP

#ifndef __cplusplus
#error cscroll.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *
 *
 *----------------------------------------------------------------------
 */

#include "sysdll.h"
#include "mytypes.h"

// #include "palwind.hpp"

class DrawDIB;
class ImageLibrary;
struct CustomBorderInfo;


namespace CustomScrollBar
{
	SYSTEM_DLL int csb_getPosition(HWND hwnd);
	SYSTEM_DLL void csb_setPosition(HWND hwnd, int pos);
	SYSTEM_DLL void csb_getRange(HWND hwnd, LPINT min, LPINT max);
	SYSTEM_DLL void csb_setRange(HWND hwnd, int min, int max);
	SYSTEM_DLL void csb_setButtonIcons(HWND hwnd, const ImageLibrary* il);
	SYSTEM_DLL void csb_setThumbBk(HWND hwnd, const DrawDIB* thumbBk);
	SYSTEM_DLL void csb_setScrollBk(HWND hwnd, const DrawDIB* scrollBk);
//	SYSTEM_DLL void csb_setThumbBk(HWND hwnd, HINSTANCE inst, int id);
//	SYSTEM_DLL void csb_setScrollBk(HWND hwnd, HINSTANCE inst, int id);
	SYSTEM_DLL int csb_getPage(HWND hwnd);
	SYSTEM_DLL void csb_setPage(HWND hwnd, int page);
	SYSTEM_DLL void initCustomScrollBar(HINSTANCE instance);
	SYSTEM_DLL void csb_setBorder(HWND hwnd, const CustomBorderInfo* border);

	SYSTEM_DLL HWND csb_create(
		DWORD  dwExStyle,
		DWORD  dwStyle,
		int  x,
		int  y,
		int  nWidth,
		int  nHeight,
		HWND  hWndParent,
		HINSTANCE  hInstance);

	extern SYSTEM_DLL const char CUSTOMSCROLLBARCLASS[];
};			// namespace CustomScrollBar

using namespace CustomScrollBar;


#endif /* CSCROLL_HPP */

