/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#error "Obsolete"

#ifndef _GENERIC_HPP
#define _GENERIC_HPP

#include "sysdll.h"
#include "wind.hpp"

#if 0       // Obsolete...
// Generic Window Class

class GenericClass
{
	public:
		// GenericClass() { }
		SYSTEM_DLL static const char* className();
    private:
		static ATOM registerClass();

	   static const char s_className[];
		static ATOM classAtom;
};

class GenericNoBackClass
{
	public:
		// GenericNoBackClass() { }
		SYSTEM_DLL static const char* className();
    private:
	 	static ATOM registerClass();

   	   static const char s_className[];
	 	static ATOM classAtom;

};

class GenericWindowBaseND : public WindowBaseND 
{

	public:
	 	GenericWindowBaseND() { }
		virtual ~GenericWindowBaseND() { }
		SYSTEM_DLL static const char* className();

	private:
	 	static ATOM registerClass();
    	static const char s_className[];
		static ATOM classAtom;
};

#endif

#endif
