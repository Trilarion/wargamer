/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 * Custom Dialog... based on Paul's Campaign version, but
 * removed use of game specific information.
 *
 *----------------------------------------------------------------------
 */


#include "stdinc.hpp"
#include "cust_dlg.hpp"
#include "dib.hpp"
#include "dib_util.hpp"
#include "bmp.hpp"
#include "palette.hpp"
#include "misc.hpp"
#include "wmisc.hpp"
#include "fonts.hpp"


#ifdef DEBUG

#include "logwin.hpp"
#endif
namespace Greenius_System
{

DrawDIBDC* CustomDialog::s_mainDIB = 0;
DrawDIBDC* CustomDialog::s_captionDIB = 0;
UWORD CustomDialog::s_instanceCount = 0;

CustomDialog::CustomDialog(const CustomBorderInfo& border) :
   d_hwnd(NULL),
   d_caption(),
   d_captionBkDib(0),
   d_bkDib(0),
   d_captionFont(),
   d_style(0),
   d_bc(border)
{
   s_instanceCount++;
}

CustomDialog::~CustomDialog()
{
   ASSERT((s_instanceCount - 1) >= 0);

   if(d_hwnd)
   {
      clear(d_hwnd);
      d_hwnd = NULL;
   }

   if(--s_instanceCount == 0)
   {
      if(s_mainDIB)
      {
         delete s_mainDIB;
         s_mainDIB = 0;
      }

      if(s_captionDIB)
      {
         delete s_captionDIB;
         s_captionDIB = 0;
      }
   }

   d_captionBkDib = 0;
   d_bkDib = 0;
}

void CustomDialog::init(HWND hwnd)
{
   d_hwnd = hwnd;
   SubClassWindowBase::init(hwnd);
}


LRESULT CustomDialog::procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
   if(d_style & MinMax || d_style & Close)
   {
      switch(msg)
      {
      HANDLE_MSG(hWnd, WM_ERASEBKGND, onEraseBk);
      HANDLE_MSG(hWnd, WM_NCPAINT, onNCPaint);
      HANDLE_MSG(hWnd, WM_NCACTIVATE, onNCActivate);
      HANDLE_MSG(hWnd, WM_NCHITTEST, onNCHitTest);
      HANDLE_MSG(hWnd, WM_NCLBUTTONDOWN, onNCLButtonDown);
      HANDLE_MSG(hWnd, WM_NCLBUTTONUP, onNCLButtonUp);
//      HANDLE_MSG(hWnd, WM_NCCALCSIZE, onNCCalcSize);
      HANDLE_MSG(hWnd, WM_SIZE, onSize);
      HANDLE_MSG(hWnd, WM_MOVE, onMove);
      HANDLE_MSG(hWnd, WM_DESTROY, onDestroy);

      default:
         return defProc(hWnd, msg, wParam, lParam);
      }
   }
   else
   {
      switch(msg)
      {
      HANDLE_MSG(hWnd, WM_ERASEBKGND, onEraseBk);
      HANDLE_MSG(hWnd, WM_NCPAINT, onNCPaint);
      HANDLE_MSG(hWnd, WM_NCACTIVATE, onNCActivate);
      HANDLE_MSG(hWnd, WM_DESTROY, onDestroy);
//      HANDLE_MSG(hWnd, WM_NCCALCSIZE, onNCCalcSize);
      default:
         return defProc(hWnd, msg, wParam, lParam);
      }
   }
}

void CustomDialog::onDestroy(HWND hwnd)
{
   if(d_hwnd)
   {
      clear(d_hwnd);
      d_hwnd = NULL;
      FORWARD_WM_DESTROY(hwnd, SendMessage);   // Let subclassed window handle the message
   }
}



BOOL CustomDialog::onEraseBk(HWND hwnd, HDC hdc)
{
   if(d_bkDib != 0)
   {
      RECT r;
      GetClientRect(hwnd, &r);

      const int cx = r.right - r.left;
      const int cy = r.bottom - r.top;

      if((s_mainDIB == 0) ||
         (s_mainDIB->getWidth() < cx) ||
         (s_mainDIB->getHeight() < cy))
      {
         int oldCX = 0;
         int oldCY = 0;

         if(s_mainDIB != 0)
         {
            oldCX = s_mainDIB->getWidth();
            oldCY = s_mainDIB->getHeight();

            delete s_mainDIB;
            s_mainDIB = 0;
         }
         s_mainDIB = new DrawDIBDC(maximum(oldCX, cx), maximum(oldCY, cy));
         ASSERT(s_mainDIB != 0);
      }

      s_mainDIB->rect(0, 0, cx, cy, d_bkDib);

      if(d_style & EtchedFrame)
      {
         const int remapPer = 70;

         /*
         * draw etched border
         */

         // top
         s_mainDIB->darkenRectangle(0, 0, cx, 1, remapPer);
         s_mainDIB->lightenRectangle(0, 1, cx, 1, remapPer);

         // bottom
         s_mainDIB->darkenRectangle(0, cy - 2, cx, 1, remapPer);
         s_mainDIB->lightenRectangle(0, cy - 1, cx, 1, remapPer);

         // left side
         s_mainDIB->darkenRectangle(0, 0, 1, cy, remapPer);
         s_mainDIB->lightenRectangle(1, 0, 1, cy, remapPer);

         // right side
         s_mainDIB->darkenRectangle(cx - 2, 0, 1, cy, remapPer);
         s_mainDIB->lightenRectangle(cx - 1, 0, 1, cy, remapPer);
      }

      HPALETTE oldPal = SelectPalette(hdc, Palette::get(), FALSE);
      RealizePalette(hdc);

      ASSERT(s_mainDIB->getDC() != 0);
      BitBlt(hdc, 0, 0, cx, cy, s_mainDIB->getDC(), 0, 0, SRCCOPY);

      SelectPalette(hdc, oldPal, FALSE);
   }

   return TRUE;
}

void CustomDialog::onNCPaint(HWND hwnd, HRGN hrgn)
{
   if(d_captionBkDib)
   {
      HDC hdc = GetWindowDC(hwnd);
      ASSERT(hdc != 0);

      HPALETTE oldPal = SelectPalette(hdc, Palette::get(), FALSE);
      RealizePalette(hdc);

      /*
      * Draw Borders
      */

      static RECT r;
      GetWindowRect(hwnd, &r);
      SetRect(&r, 0, 0, r.right-r.left, r.bottom-r.top);

      static CustomBorderWindow bw(d_bc);
      bw.drawThickBorder(hdc, r);

      const int captionHeight = GetSystemMetrics(SM_CYCAPTION);
      const int captionWidth = r.right-(CustomBorderWindow::ThickBorder*2);

      if(d_captionFont.getHandle() == 0)
      {
         const int fontHeight = captionHeight - 2;
         ASSERT(fontHeight >= 3);      // check for silly size?


         LogFont lf;
         lf.height(fontHeight);
         lf.weight(FW_MEDIUM);
         lf.charset(ANSI);    // otherwise we may get symbols!

         d_captionFont.set(lf);

      }

      /*
      * Draw Caption
      */

      if((s_captionDIB == 0) ||
         (s_captionDIB->getWidth() < captionWidth))
      {
         int oldCX = 0;
         int oldCY = 0;

         if(s_captionDIB != 0)
         {
            oldCX = s_captionDIB->getWidth();
            oldCY = s_captionDIB->getHeight();
            delete s_captionDIB;
            s_captionDIB = 0;
         }

         s_captionDIB = new DrawDIBDC(maximum(oldCX, captionWidth), maximum(oldCY, captionHeight));
         ASSERT(s_captionDIB != 0);

         s_captionDIB->setBkMode(TRANSPARENT);
      }

      if(d_captionBkDib != 0)
      {
         s_captionDIB->rect(0, 0, captionWidth, captionHeight, d_captionBkDib);
      }

      /*
      *  Draw Caption Text
      */


      if(d_caption.toStr())
      {
         HFONT oldFont = 0;
         oldFont = s_captionDIB->setFont(d_captionFont);

         TEXTMETRIC tm;
         GetTextMetrics(s_captionDIB->getDC(), &tm);

         const int x = 1;
         const int y = (captionHeight - (tm.tmHeight+2)) / 2;

         COLORREF oldColor = s_captionDIB->setTextColor(d_bc.colours[CustomBorderInfo::DarkBrown]);
         wTextOut(s_captionDIB->getDC(), x, y, captionWidth - x, d_caption.toStr());

         s_captionDIB->setTextColor(oldColor);
         s_captionDIB->setFont(oldFont);
      }

      /*
      * draw buttons
      */

      for(int i = 0; i < SystemButton::HowMany; i++)
      {
         if(d_sysButtons[i].d_type != SystemButton::Inactive)
         {
            ColourIndex c1 = s_captionDIB->getColour(d_bc.colours[CustomBorderInfo::OffWhite]);
            ColourIndex c2 = s_captionDIB->getColour(d_bc.colours[CustomBorderInfo::DarkBrown]);

            COLORREF imgColorRef = PALETTERGB(0, 0, 0);
            ColourIndex  imgColor = s_captionDIB->getColour(imgColorRef);

            POINT p;
            p.x = d_sysButtons[i].d_rect.left;
            p.y = 0;

            // convert x from screen
            ScreenToClient(hwnd, &p);
            const int cx = d_sysButtons[i].d_rect.right - d_sysButtons[i].d_rect.left;
            const int cy = d_sysButtons[i].d_rect.bottom - d_sysButtons[i].d_rect.top;

            // set y coord
            const int capCY = GetSystemMetrics(SM_CYCAPTION);
            p.y = (capCY - cy) / 2;

            if(i == SystemButton::Min)
               drawMinButton(p.x, p.y, cx, cy, imgColor);
            else if(i == SystemButton::Max)
               drawMaxButton(p.x, p.y, cx, cy, imgColor);
            else
               drawCloseButton(p.x, p.y, cx, cy, imgColorRef);

            if(d_sysButtons[i].d_selected)
               s_captionDIB->frame(p.x, p.y, cx, cy, c2, c1);
            else
               s_captionDIB->frame(p.x, p.y, cx, cy, c1, c2);
         }
      }

      BitBlt(hdc, CustomBorderWindow::ThickBorder, CustomBorderWindow::ThickBorder,
         captionWidth, captionHeight,
         s_captionDIB->getDC(), 0, 0, SRCCOPY);


      SelectPalette(hdc, oldPal, FALSE);
      ReleaseDC(hwnd, hdc);
   }
}

BOOL CustomDialog::onNCActivate(HWND hwnd, BOOL fActive, HWND hwndActDeact, BOOL fMinimized)
{
   return TRUE;
}

void CustomDialog::setCaption(const char* text)
{
   d_caption = text;
}

void CustomDialog::setStyle(UBYTE style)
{
   d_style = style;

   if(d_style & MinMax)
   {
      d_sysButtons[SystemButton::Min].d_type = SystemButton::Min;
      d_sysButtons[SystemButton::Max].d_type = SystemButton::Max;
   }
   else if(d_style & Close)
      d_sysButtons[SystemButton::Close].d_type = SystemButton::Close;
}

void CustomDialog::onSize(HWND hwnd, UINT state, int cx, int cy)
{
   setSysButtonPos();
   SendMessage(hwnd, WM_NCPAINT, 0, 0);
}

void CustomDialog::onMove(HWND hwnd, int cx, int cy)
{
   setSysButtonPos();
   SendMessage(hwnd, WM_NCPAINT, 0, 0);
}

void CustomDialog::setSysButtonPos()
{
   RECT r;
   GetWindowRect(d_hwnd, &r);

   const int capCY = GetSystemMetrics(SM_CYCAPTION);
   const int yOffset = 2;
   const int rxOffset = 2;
   const int spaceCX = 1;
   const int buttonCX = capCY - 4;

   int x = r.right - ((SystemButton::HowMany * buttonCX) + CustomBorderWindow::ThickBorder + (2 * spaceCX) + rxOffset);
   int y = r.top + CustomBorderWindow::ThickBorder + yOffset;
   for(SystemButton::Type t = SystemButton::Min; t < SystemButton::HowMany; INCREMENT(t))
   {
      if(d_sysButtons[t].d_type != SystemButton::Inactive)
      {
         SetRect(&d_sysButtons[t].d_rect, x, y, x + buttonCX, y + buttonCX);
      }

      x += buttonCX + spaceCX;
   }
}

void CustomDialog::drawMinButton(int x, int y, int cx, int cy, ColourIndex color)
{
   ASSERT(s_captionDIB);

   // we are drawing a -

   const int xOffset = 3;
   int yOffset = y + (cy - 7);
   s_captionDIB->hLine(x + xOffset, y + yOffset++, cx - (2 * xOffset), color);
   s_captionDIB->hLine(x + xOffset, y + yOffset, cx - (2 * xOffset), color);
}

void CustomDialog::drawMaxButton(int x, int y, int cx, int cy, ColourIndex color)
{
   ASSERT(s_captionDIB);

   /*
   * See if we are maximized or not
   */

   if(IsZoomed(d_hwnd))
   {
      /*
      * Draw 2 overlapping rects
      */

      const int xOffset = 3;
      const int yOffset = 2;
      const int frameCX = 5;
      const int frameCY = 5;

      s_captionDIB->frame(x + xOffset, y + yOffset, frameCX, frameCY, color);
      s_captionDIB->frame((x + xOffset) - 1, (y + yOffset) + 3, frameCX, frameCY, color);
   }
   else
   {
      /*
      * Draw a line and a single rect
      */

      const int xOffset = 3;
      const int yOffset = 3;
      const int frameCX = 6;
      const int frameCY = 5;

      s_captionDIB->hLine(x + xOffset, y + yOffset, frameCX, color);
      s_captionDIB->frame((x + xOffset), (y + yOffset) + 1, frameCX, frameCY, color);
   }
}

void CustomDialog::drawCloseButton(int x, int y, int cx, int cy, COLORREF color)
{
   /*
   * Draw a x
   */

   static const char* c = "x";

   SIZE s;
   GetTextExtentPoint32(s_captionDIB->getDC(), c, lstrlen(c), &s);

   const int xOffset = 2;
   const int yOffset = ((cy - s.cy) / 2) - 1;

   COLORREF oldColor = s_captionDIB->setTextColor(color);
   wTextOut(s_captionDIB->getDC(), x + xOffset, y + yOffset, c);

   s_captionDIB->setTextColor(oldColor);
}

static const UINT s_hitTest[SystemButton::HowMany] = {
   HTREDUCE,
   HTZOOM,
   HTSYSMENU
};

UINT CustomDialog::onNCHitTest(HWND hwnd, int x, int y)
{
   for(int i = 0; i < SystemButton::HowMany; i++)
   {
      POINT p;
      p.x = x;
      p.y = y;

      if(PtInRect(&d_sysButtons[i].d_rect, p))
      {
#ifdef DEBUG
         g_logWindow.printf("Hit onHitTest");
#endif
         return s_hitTest[i];
      }
   }

   return defProc(hwnd, WM_NCHITTEST, 0, MAKELPARAM(x, y));

}

void CustomDialog::onNCLButtonDown(HWND hwnd, BOOL fDoubleClick, int x, int y, UINT codeHitTest)
{
   int id = getHitTestID(codeHitTest);

   ASSERT(id == -1 || id < SystemButton::HowMany);

   if(id != -1)
   {
      d_sysButtons[id].d_selected = True;
      SendMessage(hwnd, WM_NCPAINT, 0, 0);
   }
   else
      defProc(hwnd, WM_NCLBUTTONDOWN, static_cast<WPARAM>(codeHitTest), MAKELPARAM(x, y));
}

void CustomDialog::sysButtonClicked(SystemButton::Type t)
{
   switch(t)
   {
   case SystemButton::Min:
      //    ShowWindow(d_hMain, SW_MINIMIZE);
      CloseWindow(d_hwnd);
      break;

   case SystemButton::Max:
      {
         ShowWindow(d_hwnd, (IsZoomed(d_hwnd)) ? SW_SHOWNORMAL : SW_MAXIMIZE);
         break;
      }

   case SystemButton::Close:
      SendMessage(d_hwnd, WM_CLOSE, 0, 0);
      break;

#ifdef DEBUG
   default:
      FORCEASSERT("Improper type");
#endif
   }
}

void CustomDialog::onNCLButtonUp(HWND hwnd, int x, int y, UINT codeHitTest)
{
   int id = getHitTestID(codeHitTest);

   ASSERT(id == -1 || id < SystemButton::HowMany);

   if(id != -1)
   {
      d_sysButtons[id].d_selected = False;
      SendMessage(hwnd, WM_NCPAINT, 0, 0);
      sysButtonClicked(static_cast<SystemButton::Type>(id));
   }
   else
      defProc(hwnd, WM_NCLBUTTONDOWN, static_cast<WPARAM>(codeHitTest), MAKELPARAM(x, y));
}

int CustomDialog::getHitTestID(UINT codeHitTest)
{
   int id = -1;

   if(codeHitTest == HTREDUCE)
   {
#ifdef DEBUG
      g_logWindow.printf("Button up on Min");
#endif
      id = SystemButton::Min;
   }
   else if(codeHitTest == HTZOOM)
   {
#ifdef DEBUG
      g_logWindow.printf("Button up on Max");
#endif
      id = SystemButton::Max;
   }
   else if(codeHitTest == HTSYSMENU)
   {
#ifdef DEBUG
      g_logWindow.printf("Button up on Close");
#endif
      id = SystemButton::Close;
   }

   ASSERT(id == -1 || id < SystemButton::HowMany);
   return id;
}

/*
 * This doesn't seem to work!
 */

UINT CustomDialog::onNCCalcSize(HWND hwnd, BOOL fCalcValidRects, NCCALCSIZE_PARAMS * lpcsp)
{
   if (fCalcValidRects)
   {
      return 0;
   }
   else
   {
      RECT* rect = reinterpret_cast<RECT*>(lpcsp);

      int capHeight = GetSystemMetrics(SM_CYCAPTION);
      int borderWidth = GetSystemMetrics(SM_CXSIZEFRAME);
      int borderHeight = GetSystemMetrics(SM_CYSIZEFRAME);

      rect->left = rect->left + CustomBorderWindow::ThickBorder - borderWidth;
      rect->right = rect->right - (CustomBorderWindow::ThickBorder - borderWidth);
      rect->top = rect->top + CustomBorderWindow::ThickBorder - borderHeight;
      rect->bottom = rect->bottom - (CustomBorderWindow::ThickBorder - borderHeight);


//       RECT winRect;
//       GetWindowRect(hwnd, &winRect);
//       rect->left = winRect.left + CustomBorderWindow::ThickBorder;
//       rect->right = winRect.right - CustomBorderWindow::ThickBorder;
//       rect->top = winRect.top + CustomBorderWindow::ThickBorder + GetSystemMetrics(SM_CYCAPTION);
//       rect->bottom = winRect.bottom - CustomBorderWindow::ThickBorder;

      return 0;
   }


}

}; // namespace Greenius_System



