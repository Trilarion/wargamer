/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1995, Steven Morle-Green, All Rights Reserved
 *----------------------------------------------------------------------
 *
 *	Pairing of enumerations and strings
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/06/13 08:52:39  greenius
 * Initial Import
 *
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "idstr.hpp"
#include "myassert.hpp"
#include <string.h>
#include <stdlib.h>
#include <ctype.h>

Boolean 

mpGetName(const MessagePair* mp, const char*& s, int id)
{
	for(; mp->name; mp++)
	{
		if(mp->id == id)
		{
			s = mp->name;
			return True;
		}
	}

	return False;
} 


Boolean

mpGetID(const MessagePair* mp, const char* s, int& id)
{
	for(; mp->name; mp++)
	{
		if(stricmp(mp->name, s) == 0)
		{
			id = mp->id;
			return True;
		}
	}

	return False;
} 

const char* mpGetNameNotNull(MessagePair* mp, int id, const char* def)
{
	const char* s = def;
	mpGetName(mp, s, id);
	return s;
}

Boolean StringParse::isSpace(char c) const
{
	return isspace(c) || (c == ',');
}

/*
 * Extract next token from string
 */


char*

StringParse::getToken()
{
	ASSERT(text != 0);

	const char QUOTE = '\"';

	char* s = text;

	/*
	 * Skip leading spaces
	 */

	while(*s && isSpace(*s))
		s++;

	Boolean quote = False;

	if(*s == QUOTE)
	{
		quote = True;
		s++;
	}

	char* s1;
	for(s1 = s; *s1; s1++)
	{
		if(quote)
		{
			if(*s1 == QUOTE)
			{
				*s1++ = 0;
				quote = False;
				break;
			}
		}
		else if(isSpace(*s1))
		{
			*s1++ = 0;
			break;
		}
	}
	text = s1;

	ASSERT(!quote);

	if(s && s[0])
		return s;
	else
		return 0;
}

Boolean 

StringParse::get(long& n)
{
#if 0
	Boolean negative = False;

	const char SIGN = '-';

	char* s = getToken();
	if(s)
	{

		if(*s == SIGN)
		{
		  s++;
		  negative = True;
#ifdef DEBUG
		  char* s1 = s;
		  while(*s1)
		  {
			 ASSERT(isdigit(*s1));
			 s1++;
		  }
#endif
		}

		n = strtol(s, NULL, 10);

		if(negative)
		  n = -n;

		return True;
	}
	else
		return False;
#else

	char* s = getToken();
	if(s)
	{
		char* s2;
		n = strtol(s, &s2, 10);
		ASSERT(n != LONG_MAX);
		ASSERT(errno == 0);
		ASSERT((*s2 == 0) || (isspace(*s2)));
		return True;
	}
	else
		return False;


#endif
}

Boolean

StringParse::get(unsigned long& n)
{
	char* s = getToken();
	if(s)
	{
#ifdef DEBUG
		char* s1 = s;
		while(*s1)
		{
			ASSERT(isdigit(*s1));
			s1++;
		}
#endif

		char* s2;
		n = strtoul(s, &s2, 10);

		ASSERT(n != ULONG_MAX);
		ASSERT(errno == 0);
		ASSERT((*s2 == 0) || (isspace(*s2)));
		return True;
	}
	else
		return False;
}

Boolean

StringParse::getInt(unsigned int& n)
{
#if 0
	char* s = getToken();
	if(s)
	{
#ifdef DEBUG
		char* s1 = s;
		while(*s1)
		{
			ASSERT(isdigit(*s1));
			s1++;
		}
#endif

		n = atoi(s);
		return True;
	}
	else
		return False;
#else

	unsigned long l;
	if(get(l))
	{
		ASSERT(l < UINT_MAX);
		n = (unsigned int) l;
		return True;
	}
	else
		return False;

#endif
}

/*
 *  Get a signed int
 */
Boolean 

StringParse::getSInt(int& n)
{
#if 0
	Boolean negative = False;

	const char SIGN = '-';

	char* s = getToken();
	if(s)
	{

		if(*s == SIGN)
		{
		  s++;
		  negative = True;
#ifdef DEBUG
		  char* s1 = s;
		  while(*s1)
		  {
			 ASSERT(isdigit(*s1));
			 s1++;
		  }
#endif
		}

		n = atoi(s);

		if(negative)
		  n = -n;

		return True;
	}
	else
		return False;
#else

	long l;
	if(get(l))
	{
		ASSERT( (l <= INT_MAX) && (l >= INT_MIN));
		n = (int) l;
		return True;
	}
	else
		return False;
#endif
}


Boolean StringParse::getEnumeration(const MessagePair* mp, int& num)
{
	char* s = getToken();
	if(s)
		return mpGetID(mp, s, num);
	else
		return False;
}

/*
 * Return fixed point number
 * Resolution is the multiplier
 * e.g. if you want values in 100ths, resolution=100
 * then: 1.23 is returned as 123.
 */


Boolean 

StringParse::getFixed(unsigned int& n, unsigned int resolution)
{
	unsigned long value = 0;
	unsigned int divisor = 1;
	Boolean gotPoint = False;

	char* s = getToken();
	if(s)
	{
		while(*s && (divisor <= resolution))
		{
			char c = *s++;
			if(c == '.')
				gotPoint = True;
			else if(isdigit(c))
			{
				if(gotPoint)
					divisor *= 10;
			  	value = value * 10 + (c - '0');
			}
			else
				throw StringParseError();
		}

		n = (value * resolution) / divisor;
		return True;
	}
	else
		return False;
}

