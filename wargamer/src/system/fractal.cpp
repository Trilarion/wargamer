/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *	Fractal Engine Implementation
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "fractal.hpp"
#include "pt_util.hpp"
#include "mytypes.h"
#include "random.hpp"
#include "simpstk.hpp"
#include "misc.hpp"
#include <algorithm>    // For swap()

FractalEngine::FractalEngine(GraphicEngine& output, UBYTE bendy, int resolution) :
	d_nPoints(0),
	d_output(output),
	d_bendiness(bendy),
	d_resolution(resolution),
	d_clipRect(0,0,0,0),
	d_pointAdded(false)
{
}

FractalEngine::~FractalEngine()
{
}


void FractalEngine::setClip(const GRect& r)
{
	d_clipRect = r;
}

void FractalEngine::setValues(UBYTE bendiness, int resolution)
{
	d_bendiness = bendiness;
	d_resolution = resolution;
}

void FractalEngine::addPoint(const GPoint& point)
{
	if(d_nPoints == 0)
	{
		// d_output.addPoint(point);
		d_lastPoint = point;
		++d_nPoints;
		// d_rand.seed(point.x() + point.y());
	}
	else
	{
		doFractal(d_lastPoint, point);
		d_lastPoint = point;
	}
}

void FractalEngine::close()
{
	if(d_pointAdded)
	{
		d_output.close();
		d_pointAdded = false;
	}
	d_nPoints = 0;
}

#ifdef USE_RECURSION
void FractalEngine::doFractal(const GPoint& p1, const GPoint& p2)
{
	int length = lineLength(p1, p2);

	if(length <= d_resolution)
	{
		d_output.addPoint(p2);
	}
	else
	{
		/*
		 * Pick midpoint between p1,p2 adjusted by random amount
		 */

		GPoint mid = midPoint(p1, p2);

		int dx = p2.x() - p1.x();
		int dy = p2.y() - p1.y();

		// Pick a random number between +/- length * bendiness
		// biassed towards 0.

		int maxOffset = (length * d_bendiness) / BendRange;

		if(maxOffset <= 1)
		{
			d_output.addPoint(p2);
		}
		else
		{
			RandomNumber r(p1.x() + p1.y() + p2.x() + p2.y());

			int offset = r.gauss(maxOffset);

			GPoint newPoint;

			newPoint.x( mid.x() - (offset * dy) / length );
			newPoint.y( mid.y() + (offset * dx) / length );

			doFractal(p1, newPoint);
			doFractal(newPoint, p2);
		}
	}
}
#else	// Iterative version

/*
 * We can further optimise this, by including maxOffset in the stack
 * and just dividing by 2 each iteration.
 */

#ifdef _MSC_VER  // Visual C++ can't use local classes for template types
struct StackItem
{
   FractalEngine::GPoint d_p1;
	FractalEngine::GPoint d_p2;
	int d_length;
	int d_maxOffset;

	StackItem() { }
	// StackItem(GPoint p1, GPoint p2) : d_p1(p1), d_p2(p2) { }
	StackItem(FractalEngine::GPoint p1, FractalEngine::GPoint p2, int length, int maxOffset) 
		: d_p1(p1), d_p2(p2), d_length(length), d_maxOffset(maxOffset)
	{
	}
};
#endif


void FractalEngine::doFractal(const GPoint& p1, const GPoint& p2)
{
#ifndef _MSC_VER  // Visual C++ can't use local classes for template types
	struct StackItem
	{
		GPoint d_p1;
		GPoint d_p2;
		int d_length;
		int d_maxOffset;

		StackItem() { }
		// StackItem(GPoint p1, GPoint p2) : d_p1(p1), d_p2(p2) { }
		StackItem(GPoint p1, GPoint p2, int length, int maxOffset) 
			: d_p1(p1), d_p2(p2), d_length(length), d_maxOffset(maxOffset)
		{
		}
	};
#endif

	SimpleStack<StackItem,20> stack;

	int length = lineLength(p1, p2);
	int maxOffset = (length * d_bendiness) / BendRange;

	StackItem item(p1,p2,length,maxOffset);

	for(;;)
	{
		// Check for clipping

		if(d_clipRect.width() != 0)
		{
			int minX = p1.x();
			int maxX = p2.x();
			if(minX > maxX)
            std::swap(minX, maxX);

			int minY = p1.y();
			int maxY = p2.y();
			if(minY > maxY)
            std::swap(minY, maxY);

			if( (minX > d_clipRect.right()) ||
				 (maxX < d_clipRect.left()) ||
				 (minY > d_clipRect.bottom()) ||
				 (maxY < d_clipRect.top()) )
			{
				if(stack.isEmpty())
					break;
				else
				{
					stack.pop(item);
					continue;
				}
			}
		}

		length = item.d_length;
		maxOffset = item.d_maxOffset;

		if( (item.d_length <= d_resolution) || (item.d_maxOffset <= 1) )
		{
			if(!d_pointAdded)
			{
				d_output.addPoint(item.d_p1);
				d_pointAdded = true;
			}

			d_output.addPoint(item.d_p2);
			if(stack.isEmpty())
				break;
			else
				stack.pop(item);
		}
		else
		{
			/*
		 	 * Pick midpoint between p1,p2 adjusted by random amount
		 	 */

			GPoint mid = midPoint(item.d_p1, item.d_p2);

			int dx = item.d_p2.x() - item.d_p1.x();
			int dy = item.d_p2.y() - item.d_p1.y();

			// Pick a random number between +/- length * bendiness
			// biassed towards 0.

			RandomNumber r(item.d_p1.x() + item.d_p1.y() + item.d_p2.x() + item.d_p2.y());

			int offset = r.gauss(item.d_maxOffset);

			GPoint newPoint;

			newPoint.x( mid.x() - (offset * dy) / item.d_length );
			newPoint.y( mid.y() + (offset * dx) / item.d_length );

			item.d_length >>= 1;
			item.d_maxOffset >>= 1;

			stack.push(StackItem(newPoint, item.d_p2, item.d_length, item.d_maxOffset));

			item.d_p2 = newPoint;
		}
	}
}

#endif
