/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *	DIB pointer that is palette aware and counts instances
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "dibref.hpp"
#include "dib_util.hpp"

void DibPal::allocate(int cx, int cy)
{
  if(d_palID != Palette::paletteID())
  {
		delete d_dib;
		d_dib = 0;
  }

  DIB_Utility::allocateDib(&d_dib, cx, cy);
  ASSERT(d_dib);

  d_palID = Palette::paletteID();
}

