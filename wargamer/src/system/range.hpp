/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef RANGE_HPP
#define RANGE_HPP

#ifndef __cplusplus
#error range.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *      Store a value within a specified range
 *
 *----------------------------------------------------------------------
 */

#include "sysdll.h"

/*
 * Class for values within a range
 *
 * Maximum is the maximum value (not the range)
 * e.g. Range<unsigned char, 255> allows 0..255 inclusive
 * Range<unsigned char, 256> should give a compiler warning
 */

template<class Type, int Maximum>
class Range
{
    typedef Range<Type,Maximum> ThisType;
  public:
    enum { MaxValue = Maximum };
    typedef Type Value;

    Range() : d_value() { }
    Range(Type t) : d_value(t) { }

    Type value() const { return d_value; }

    int value(int range) const
    {
      return (range * d_value) / Maximum;
    }

    float normalise() const
    {
      return static_cast<float>(d_value) / (Maximum+1);
    }

    operator float() const
    {
      return normalise();
    }

    operator Type() const
    {
      return value();
    }

    ThisType& operator = (Type t)
    {
      d_value = t;
      return *this;
    }

    ThisType& operator = (const ThisType& t)
    {
      d_value = t.d_value;
      return *this;
    }

    /**
     * Add to value, clipping it at Maximum
     * @return true if value was clipped
     */
    bool add(Type n)
    {
      if(d_value >= (Maximum - n))
      {
        d_value = Maximum;
        return true;
      }
      else
      {
        d_value += n;
        return false;
      }
    }

    /**
     * Subtract from value, clipping it at Minimum(0)
     * @return true if value was clipped
     */
    bool remove(Type n)
    {
      if((d_value  - n) < 0)
      {
        d_value = 0;
      }
      else
      {
        d_value -= n;
      }

      return (d_value == 0);
    }

    ThisType& operator+=(Type n)
    {
      add(n);
      return *this;
    }

    ThisType& operator-=(Type n)
    {
      remove(n);
      return *this;
    }

  private:
    Type d_value;
};

#endif /* RANGE_HPP */

/*
 *----------------------------------------------------------------------
 * $Log$
 *----------------------------------------------------------------------
 */
