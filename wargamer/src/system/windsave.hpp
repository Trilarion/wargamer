/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#error "Obsolete"

#ifndef WINDSAVE_HPP
#define WINDSAVE_HPP

#ifndef __cplusplus
#error windsave.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1995, Steven Morle-Green, All Rights Reserved
 *----------------------------------------------------------------------
 *
 *	Save and Restore Window states to registry
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 *
 *----------------------------------------------------------------------
 */

#include "sysdll.h"
#include "mytypes.h"
#include <windows.h>

/*
 * Flags to define what to save
 */

#define WSAV_POSITION	1		// Save Position
#define WSAV_SIZE			2		// Save Size
#define WSAV_MINMAX		4		// Save whether it is minimized/maximized/restored
#define WSAV_RESTSIZE	8		// Save Restore Min/Max

#define WSAV_ALL			0xff

typedef UBYTE WSAV_FLAGS;

SYSTEM_DLL BOOL restoreWindowState(const char* name, HWND hWnd, WSAV_FLAGS flags, const char* subDir);
SYSTEM_DLL BOOL saveWindowState(const char* name, HWND hWnd, WSAV_FLAGS flags, const char* subDir);

#endif /* WINDSAVE_HPP */

