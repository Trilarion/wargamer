/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef NAMETABLE_HPP
#define NAMETABLE_HPP

/*
 * Name Table
 *
 * Names are stored in a text file with an ID
 *
 */

#include "mytypes.h"
#include <vector>
#include "sysdll.h"
#include "StringPtr.hpp"

class StringFileTable
{
    public:
         typedef UWORD ID;
         enum
         {
            ID_MAX = UWORD_MAX,
            Null = ID_MAX
         };

         SYSTEM_DLL StringFileTable(const char* fileName);
         SYSTEM_DLL ~StringFileTable();

         SYSTEM_DLL void writeFile(); // Save ALL items back to file

         SYSTEM_DLL const char* get(ID id);
         SYSTEM_DLL ID add(const char* s);
         SYSTEM_DLL ID update(ID id, const char* s);

         SYSTEM_DLL bool isNew(ID id);


         SYSTEM_DLL bool isValid(ID id);

    private:
         void readFile();

    private:
         struct Item
         {
            mutable bool d_new;   // Item was created since table was intialized
            char* d_name;

            Item() : d_new(false), d_name(0) { }
            Item(char* s) : d_new(true), d_name(s) { }
         };


         typedef std::vector<Item> Container;

         Container d_items;
         bool d_changed;
         StringPtr d_fileName;

         static const char s_id[];
         static const UWORD s_version;
};


#endif      // NAMETABLE_HPP
