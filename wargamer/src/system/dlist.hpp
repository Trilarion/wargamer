/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef DLIST_HPP
#define DLIST_HPP

#ifndef __cplusplus
#error dlist.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *	Intrusive Doubly linked List
 *
 *----------------------------------------------------------------------
 */

#include "sysdll.h"

class DLink 
{
	friend class DiListBase;
	friend class DiListIterBase;
	friend class DiListIterBaseWrite;

	DLink* d_next;
	DLink* d_prev;
 public:
 	DLink() : d_next(0), d_prev(0) { }
	virtual ~DLink() { }
};

class SYSTEM_DLL DiListBase
{
	friend class DiListIterBase;
	friend class DiListIterBaseWrite;

	DLink d_link;
	int d_count;

	DiListBase(const DiListBase&);
	DiListBase& operator = (const DiListBase&);

 public:
 	DiListBase();
	~DiListBase();

 	void append(DLink* ob) { insertAfter(d_link.d_prev, ob); }

	int entries() const;

	void reset();
	void unlink(DLink* ob);
	void insertAfter(DLink* prev, DLink* ob);

	DLink* first() { return (d_link.d_next == &d_link) ? 0 : d_link.d_next; }
	const DLink* first() const { return (d_link.d_next == &d_link) ? 0 : d_link.d_next; }
	DLink* last() { return (d_link.d_prev == &d_link) ? 0 : d_link.d_prev; }
	const DLink* last() const { return (d_link.d_prev == &d_link) ? 0 : d_link.d_prev; }

	DLink* next(const DLink* link) { return (link->d_next == &d_link) ? 0 : link->d_next; }
	const DLink* next(const DLink* link) const { return (link->d_next == &d_link) ? 0 : link->d_next; }
	DLink* prev(const DLink* link) { return (link->d_prev == &d_link) ? 0 : link->d_prev; }
	const DLink* prev(const DLink* link) const { return (link->d_prev == &d_link) ? 0 : link->d_prev; }
};

template<class T>
class DiList : public DiListBase
{
	DiList(const DiList<T>&);
	DiList<T>& operator = (const DiList<T>&);
 public:
	DiList() : DiListBase() { }
	~DiList() { }

	void append(T* ob) { DiListBase::append((DLink*)ob); }
	void unlink(T* ob) { DiListBase::unlink((DLink*)ob); }
	int entries() const { return DiListBase::entries(); }
	void reset() { DiListBase::reset(); }

	void insertAfter(T* prev, T* ob) { DiListBase::insertAfter((DLink*)prev, (DLink*)ob); }

	T* first() { return (T*) DiListBase::first(); }
	const T* first() const { return (T*) DiListBase::first(); }
	T* last() { return (T*) DiListBase::last(); }
	const T* last() const { return (T*) DiListBase::last(); }

	T* next(const DLink* link) { return (T*) DiListBase::next(link); }
	const T* next(const DLink* link) const { return (T*) DiListBase::next(link); }
	T* prev(const DLink* link) { return (T*) DiListBase::prev(link); }
	const T* prev(const DLink* link) const { return (T*) DiListBase::prev(link); }

};

class SYSTEM_DLL DiListIterBase
{
	const DLink* d_current;
	const DiListBase* d_container;

	DiListIterBase(const DiListIterBase&);
	DiListIterBase& operator = (const DiListIterBase&);

 public:
 	DiListIterBase(const DiListBase* container);
	// DiListIterBase& operator = (const DiListBase* container);
	int next();
	const DLink* current() const;
};

template<class T>
class DiListIter : public DiListIterBase
{
	DiListIter<T>(const DiListIter<T>&);
	DiListIter<T>& operator = (const DiListIter<T>&);
 public:
 	DiListIter(const DiList<T>* container) : DiListIterBase(container) { }
	// DiListIter<T>& operator = (const DiList<T>* container);


	int operator ++() { return DiListIterBase::next(); }
	const T* current() const { return (const T*) DiListIterBase::current(); }
};

class SYSTEM_DLL DiListIterBaseWrite
{
	DLink* d_current;
	DiListBase* d_container;

	DiListIterBaseWrite(const DiListIterBase&);
	DiListIterBaseWrite& operator = (const DiListIterBase&);

 public:
 	DiListIterBaseWrite(DiListBase* container);
	int next();
	DLink* current() const;
};

template<class T>
class DiListIterW : public DiListIterBaseWrite
{
	// DiListIter<T>(const DiListIter<T>&);
	// DiListIter<T>& operator = (const DiListIter<T>&);
 public:
 	DiListIterW(DiList<T>* container) : DiListIterBaseWrite(container) { }
	int operator ++() { return DiListIterBaseWrite::next(); }
	T* current() const { return (T*) DiListIterBaseWrite::current(); }
};


#endif /* DLIST_HPP */

