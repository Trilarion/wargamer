/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef RANDOM_H
#define RANDOM_H

#ifndef __cplusplus
#error random.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1995, Steven Morle-Green, All Rights Reserved
 *----------------------------------------------------------------------
 *
 *	Random Number Generator
 *
 *----------------------------------------------------------------------
 */

#include "sysdll.h"
#include "mytypes.h"

/*
 * Super-Duper method
 */

class SYSTEM_DLL RandomNumber {
	ULONG d_mulValue;
	ULONG d_shiftValue;

public:
	RandomNumber(ULONG start);		// Initialise with a seed
	RandomNumber();					// Initialise without a seed

	void seed(ULONG start);			// Set a new seed

	ULONG getL();						// Get a random number
	UWORD getW() { return static_cast<UWORD>(getL()); }
	UBYTE getB() { return static_cast<UBYTE>(getL()); }

	int get(int range);

	ULONG getL(ULONG range)
	{
		return get(range);
	}

	UWORD getW(UWORD range)
	{
		return static_cast<UWORD>(get(range));
	}

	UBYTE getB(UBYTE range)
	{
		return static_cast<UBYTE>(get(range));
	}

	ULONG operator () () { return getL(); }
	int operator () (int n) { return get(n); }

	int operator () (int from, int to);

	int gauss(int range);

	ULONG getSeed() const			// Get value without changing it
	{
		return d_mulValue ^ d_shiftValue;
	}

private:
	void shuffle();
};

/*
 * Simple random number call that returns a number between n1 and n2-1
 * Included for backwards compatibility
 */
 
int SYSTEM_DLL random(int n1, int n2);

#endif /* RANDOM_H */

