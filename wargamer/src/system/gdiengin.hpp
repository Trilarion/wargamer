/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef GDIENGIN_HPP
#define GDIENGIN_HPP

#ifndef __cplusplus
#error gdiengin.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *	GDI simple Graphics Engine to test Bezier
 *
 *----------------------------------------------------------------------
 */

#include "sysdll.h"
#include "gengine.hpp"

class GDIEngine : public GraphicEngine
{
	public:
		GDIEngine(HDC dc);	// CDC& dc);
		~GDIEngine();

		void addPoint(const GPoint& point);
		void close();

		void setPen(int style, int width, COLORREF col);

	private:
		// CDC* d_dc;
		HDC d_dc;

		// CPen d_pen;
		HPEN d_pen;
#if 0
		CPen d_markerPen;
		CBrush d_markerBrush;
		CBrush* d_oldBrush;
#endif
		// CPen* d_oldPen;
		HPEN d_oldPen;

		bool d_closed;
		GPoint d_lastPoint;
};

#endif /* GDIENGIN_HPP */

