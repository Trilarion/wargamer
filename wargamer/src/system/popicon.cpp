/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1995, Steven Morle-Green, All Rights Reserved
 *----------------------------------------------------------------------
 *
 *	Popup Icon control
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/06/13 08:52:39  greenius
 * Initial Import
 *
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "popicon.hpp"
// #include "app.hpp"
#include "palette.hpp"
#include "winctrl.hpp"
#include "misc.hpp"
#include "dynarray.hpp"
// #include "globwin.hpp"
#include "imglib.hpp"
// #include "wind.hpp"
#include "wmisc.hpp"
#include "tooltip.hpp"

#ifdef DEBUG
#define DEBUG_POPUPICON

#include "msgenum.hpp"

#ifdef DEBUG_POPUPICON
#include "clog.hpp"
LogFile blog("popicon.log");
#endif
#endif

const char POPUPICONCLASS[] = "POPUPICON";
const char POPUPWINDCLASS[] = "POPUPICONWINDOW";

static BOOL initialised = FALSE;

static BOOL mouseNotMoved = False;

static HWND hToolTip = 0;


/*
 * Crackers for our own messages
 */

// void onSetImageList(HWND hwnd, ImageLibrary* img)

#define HANDLE_PIM_SETIMAGELIST(hwnd, wParam, lParam, fn)	\
	((fn)((hwnd), (ImageLibrary*) (lParam)), 0L)

#if 0
// void onSetHelpIDs(HWND hwnd, DWORD* ids)

#define HANDLE_PIM_SETHELPIDS(hwnd, wParam, lParam, fn)	\
	((fn)((hwnd), (DWORD*) (lParam)), 0L)

#endif

// void onSetCursel(HWND hwnd, int index)

#define HANDLE_PIM_SETCURSEL(hwnd, wParam, lParam, fn)	\
	((fn)(hwnd, (int) wParam), 0L)

// int onGetCurSel(HWND hwnd)

#define HANDLE_PIM_GETCURSEL(hwnd, wParam, lParam, fn)	\
	(LRESULT)(fn)(hwnd)

// int onAddItem(HWND hwnd, int index, LPARAM lParam)

#define HANDLE_PIM_ADDITEM(hwnd, wParam, lParam, fn)	\
	(LRESULT)(fn)((hwnd), (const POPICONITEM*) (lParam))

// void onRemoveItem(HWND hwnd, int index)

#define HANDLE_PIM_REMOVEITEM(hwnd, wParam, lParam, fn)	\
	((fn)(hwnd, (int) wParam), 0L)

// void onResetContent(HWND hwnd)

#define HANDLE_PIM_RESETCONTENT(hwnd, wParam, lParam, fn)	\
	((fn)(hwnd), 0L)

// int onGetValue(HWND hwnd, int index)

#define HANDLE_PIM_GETVALUE(hwnd, wParam, lParam, fn)		\
	(LRESULT)(fn)((hwnd), (int) (wParam))

#define HANDLE_PIM_SETVALUE(hwnd, wParam, lParam, fn)		\
	(LRESULT)(fn)((hwnd), (int) (lParam))

/*
 * Constants
 */

const int xBorder = 2;
const int yBorder = 2;
const int xGap = 2;
const int yGap = 2;
const int xEdge = 1;
const int yEdge = 1;

/*
 * Structure for storing data
 */

struct PopupItem {
	RECT r;
	int index;
	int imageIndex;
	int value;
	int tip;
	int hint;
};

struct PopupIconData {
	ImageLibrary* imageList;		// Image List
//	DWORD* helpIDs;
	int select;						// Current selection
	int imgWidth;
	int imgHeight;

	HWND parent;               // handle to parent, used for tooltips
	HWND hControl;					// the control's handle

	HWND hPopup;					// Handle to popup window (NULL if not up)
	int columns;
	int rows;
	int mouseOver;

	ResizeArray<PopupItem> items;

	PopupIconData();
	~PopupIconData();
};

PopupIconData::PopupIconData()
{
	imageList = NULL;
//	helpIDs = NULL;
	select = -1;
	imgWidth = 0;
	imgHeight = 0;
	hPopup = NULL;
	columns = 0;
	rows = 0;
	mouseOver = -1;
	hControl = NULL;
	parent = NULL;
}

PopupIconData::~PopupIconData()
{
}

static PopupIconData* pi_getData(HWND hwnd)
{
	PopupIconData* td = (PopupIconData*) GetWindowLong(hwnd, GWL_USERDATA);
	ASSERT(td != 0);
	return td;
}

static PopupIconData* piw_getData(HWND hwnd)
{
	PopupIconData* td = (PopupIconData*) GetWindowLong(hwnd, GWL_USERDATA);
	ASSERT(td != 0);
	return td;
}

/*
 * WM_CREATE handler
 */

BOOL pi_onCreate(HWND hwnd, LPCREATESTRUCT lpCreateStruct)
{
#ifdef DEBUG_POPUPICON
	blog.printf("pi_onCreate(%p)", hwnd);
	blog.close();
#endif

	PopupIconData* td = new PopupIconData;
	ASSERT(td != 0);
	if(td == 0)
		return FALSE;

	SetWindowLong(hwnd, GWL_USERDATA, (LONG) td);

	td->parent = (HWND)GetWindowLong(hwnd, GWL_HWNDPARENT);
	td->hControl = hwnd;

	return TRUE;
}

void pi_onDestroy(HWND hwnd)
{
#ifdef DEBUG_POPUPICON
	blog.printf("pi_onDestroy(%p)", hwnd);
	blog.close();
#endif

	PopupIconData* td = pi_getData(hwnd);
	delete td;
	SetWindowLong(hwnd, GWL_USERDATA, 0);
}

void pi_onPaint(HWND hwnd)
{
#ifdef DEBUG_POPUPICON
	blog.printf("pi_onPaint(%p)", hwnd);
	blog.close();
#endif

	PopupIconData* td = pi_getData(hwnd);

	PAINTSTRUCT ps;

	HDC hdc = BeginPaint(hwnd, &ps);

	HPALETTE oldPal = SelectPalette(hdc, Palette::get(), FALSE);
	RealizePalette(hdc);


	/*
	 * Draw a button outline
	 */

	RECT r;
	GetClientRect(hwnd, &r);

	HBRUSH brush = GetSysColorBrush(COLOR_3DFACE);
	ASSERT(brush != NULL);

//	HBRUSH brush =
	HBRUSH blackBrush = (HBRUSH) GetStockObject(BLACK_BRUSH);

	HPEN pen = CreatePen(PS_SOLID, 0, GetSysColor(COLOR_3DHILIGHT));
	ASSERT(pen != NULL);
	HPEN pen1 = CreatePen(PS_SOLID, 0, GetSysColor(COLOR_3DSHADOW));
	ASSERT(pen1 != NULL);
	HPEN oldPen = (HPEN) SelectObject(hdc, pen);

	FrameRect(hdc, &r, blackBrush);
	InflateRect(&r, -1, -1);

	LONG x = r.left;
	LONG y = r.top;
	LONG x1 = r.right - 1;
	LONG y1 = r.bottom - 1;



	MoveToEx(hdc, x1, y, NULL);
	LineTo(hdc, x, y);
	LineTo(hdc, x, y1);
	SelectObject(hdc, pen1);
	LineTo(hdc, x1, y1);
	LineTo(hdc, x1, y);

	InflateRect(&r, -1, -1);
	FillRect(hdc, &r, brush);

	/*
	 * Put image on
	 */


	if(td->select >= 0)
	{
		ASSERT(td->imageList != NULL);

		ASSERT(td->select < td->items.getCount());

		UWORD imgNum = (UWORD)(td->items[td->select].imageIndex);
//		ASSERT(imgNum < ImageList_GetImageCount(td->imageList));
		ASSERT(imgNum < td->imageList->getImageCount());

		int x = r.left + (r.right - r.left - td->imgWidth) / 2;
		int y = r.top + (r.bottom - r.top - td->imgHeight) / 2;

		ImageLibrary* il = td->imageList;
//		il.draw(hdc, imgNum, x, y);
		il->blit(hdc, imgNum, x, y);
	}

	SelectObject(hdc, oldPen);

	DeleteObject(brush);
	DeleteObject(pen);
	DeleteObject(pen1);

	SelectPalette(hdc, oldPal, FALSE);

	EndPaint(hwnd, &ps);
}

int pi_onAddItem(HWND hwnd, const POPICONITEM* item)
{
  PopupIconData* td = pi_getData(hwnd);

  PopupItem pi;
  pi.index = td->items.getCount() + 1;
  pi.imageIndex = item->imageIndex;
  pi.value = item->value;
  pi.tip = item->tipID;
  pi.hint = item->hintID;
  td->items.add(pi);
  return pi.index;
}

void pi_onRemoveItem(HWND hwnd, int index)
{
  ASSERT(hwnd != NULL);

  PopupIconData* td = pi_getData(hwnd);

  ASSERT(index < td->items.getCount());

  if(index < td->items.getCount())
	 td->items.remove(index);
}

void pi_onResetContent(HWND hwnd)
{
  // g_toolTip.delTips(hwnd);
  PopupIconData* td = pi_getData(hwnd);
  td->items.reset();
}

void pi_onEnable(HWND hwnd, BOOL fEnable)
{
#ifdef DEBUG_POPUPICON
	blog.printf("pi_onEnable(%p, %d)", hwnd, fEnable);
	blog.close();
#endif
}


void pi_onLButtonDown(HWND hwnd, BOOL fDoubleClick, int x, int y, UINT keyFlags)
{
#ifdef DEBUG_POPUPICON
	blog.printf("pi_onLButtonDown(%p, %d,%d)", hwnd, x,y);
	blog.close();
#endif

	/*
	 * If popup is not up, then create it
	 */

	PopupIconData* td = pi_getData(hwnd);
	if((td->hPopup == NULL) && (td->items.getCount() != 0))
	{
		RECT r;
		BOOL result = GetWindowRect(hwnd, &r);
		ASSERT(result);

		x += r.left;
		y += r.top;

		td->columns = squareRoot(td->items.getCount());
		td->rows = 1 + (td->items.getCount() - 1) / td->columns;

		int w = td->columns * (td->imgWidth + xGap + xEdge * 2) + xBorder * 2 - xGap;
		int h = td->rows * (td->imgHeight + yGap + yEdge * 2) + yBorder * 2 - yGap;

		r.left = x;
		r.right = w + x;
		r.top = y;
		r.bottom = h + y;


		DWORD dwStyle = WS_VISIBLE | WS_POPUP | WS_BORDER;
		AdjustWindowRect(&r, dwStyle, FALSE);

		td->hPopup = CreateWindowEx(
				WS_EX_TOPMOST,
				POPUPWINDCLASS,
				"",
				dwStyle,
				r.left, r.top, r.right - r.left, r.bottom - r.top,
				hwnd,
				0,
				wndInstance(hwnd),
				td);

		ASSERT(td->hPopup != NULL);

		SetCapture(td->hPopup);
	}




	// FORWARD_BM_SETSTATE(hwnd, TRUE, SendMessage);
}

void pi_onLButtonUp(HWND hwnd, int x, int y, UINT keyFlags)
{
#ifdef DEBUG_POPUPICON
	blog.printf("pi_onLButtonUp(%p, %d,%d)", hwnd, x,y);
	blog.close();
#endif
}

void pi_OnSetImageList(HWND hwnd, ImageLibrary* img)
{
	ASSERT(img != NULL);

	PopupIconData* td = pi_getData(hwnd);

	td->imageList = img;

	int w;
	int h;
//	BOOL result = ImageList_GetIconSize(img, &w, &h);

   Boolean result = img->getImageSize(0, w, h);
	ASSERT(result == True);
	if(result)
	{
		td->imgWidth = w;
		td->imgHeight = h;
	}
}

#if 0
void pi_OnSetHelpIDs(HWND hwnd, DWORD* ids)
{
	ASSERT(ids != NULL);

	PopupIconData* td = pi_getData(hwnd);

	td->helpIDs = ids;
}
#endif
void pi_OnSetCurSel(HWND hwnd, int index)
{
	PopupIconData* td = pi_getData(hwnd);

	if(index != td->select)
	{
		td->select = index;
		InvalidateRect(hwnd, NULL, TRUE);
	}
}

int pi_OnGetCurSel(HWND hwnd)
{
	PopupIconData* td = pi_getData(hwnd);
	ASSERT(td->items.getCount() > 0);
	return td->select;
}

int pi_onGetValue(HWND hwnd, int index)
{
	PopupIconData* td = pi_getData(hwnd);
	return td->items[index].value;
}

int pi_onSetValue(HWND hwnd, int value)
{
  PopupIconData* td = pi_getData(hwnd);

  int howMany = td->items.getCount();

  	for(int i = 0; i < howMany; i++)
	{
		if(td->items[i].value == value)
		{
			td->select = i;
			InvalidateRect(hwnd, NULL, TRUE);
			return i;
		}
  	}

#ifdef DEBUG
#ifdef __WATCOM_CPLUSPLUS__
#pragma warning 368 9
#endif
	ASSERT(FALSE);
#ifdef __WATCOM_CPLUSPLUS__
#pragma warning 368 8
#endif
#endif

	return -1;
}


void pi_onCommand(HWND hwnd, int id, HWND hwndCtl, UINT codeNotify)
{
#ifdef DEBUG
	blog.printf("pi_onCommand(%p, %d, %p, %u", hwnd, id, hwndCtl, codeNotify);
#endif
	FORWARD_WM_COMMAND(GetParent(hwnd), GetWindowLong(hwnd, GWL_ID), hwnd, PIN_SELCHANGED, SendMessage);
}

/*
 * Handle messages
 */

LRESULT CALLBACK popupIconProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
#ifdef DEBUG_POPUPICON
	blog.printf("popupIconProc::procMessage(%s)",
		getWMdescription(hWnd, msg, wParam, lParam));
#endif

	switch(msg)
	{
		HANDLE_MSG(hWnd, WM_CREATE,	pi_onCreate);
		HANDLE_MSG(hWnd, WM_DESTROY,	pi_onDestroy);
		HANDLE_MSG(hWnd, WM_PAINT,		pi_onPaint);
		HANDLE_MSG(hWnd, WM_ENABLE,	pi_onEnable);
		HANDLE_MSG(hWnd, WM_LBUTTONDOWN, pi_onLButtonDown);
		HANDLE_MSG(hWnd, WM_LBUTTONUP, pi_onLButtonUp);
		HANDLE_MSG(hWnd, WM_COMMAND, pi_onCommand);

		HANDLE_MSG(hWnd, PIM_SETIMAGELIST, pi_OnSetImageList);
//		HANDLE_MSG(hWnd, PIM_SETHELPIDS, pi_OnSetHelpIDs);
		HANDLE_MSG(hWnd, PIM_SETCURSEL, pi_OnSetCurSel);
		HANDLE_MSG(hWnd, PIM_GETCURSEL, pi_OnGetCurSel);
		HANDLE_MSG(hWnd, PIM_ADDITEM, pi_onAddItem);
		HANDLE_MSG(hWnd, PIM_REMOVEITEM, pi_onRemoveItem);
		HANDLE_MSG(hWnd, PIM_RESETCONTENT, pi_onResetContent);
		HANDLE_MSG(hWnd, PIM_GETVALUE, pi_onGetValue);
		HANDLE_MSG(hWnd, PIM_SETVALUE, pi_onSetValue);
	default:
		return DefWindowProc(hWnd, msg, wParam, lParam);
	}
}


/*
 * PopWindow Class functions
 */

BOOL piw_onCreate(HWND hwnd, LPCREATESTRUCT lpCreateStruct)
{
#ifdef DEBUG_POPUPICON
	blog.printf("piw_onCreate(%p)", hwnd);
	blog.close();
#endif

#ifdef OLD_TOOLTIP
	hToolTip = CreateWindowEx( 0, TOOLTIPS_CLASS, (LPSTR)NULL,
	TTS_ALWAYSTIP,
	0, 0, 10, 10,
	hwnd, (HMENU)NULL, wndInstance(hwnd), NULL);
	ASSERT(hToolTip != 0);

	ToolTip tt(hToolTip);
#endif	// OLD_TOOLTIP

	SetWindowLong(hwnd, GWL_USERDATA, (LONG) lpCreateStruct->lpCreateParams);

	PopupIconData* td = piw_getData(hwnd);

	td->mouseOver = -1;

	for(int i = 0; i < td->items.getCount(); i++)
	{
		PopupItem& p = td->items[i];

		p.r.left = (i % td->columns) * (td->imgWidth + xGap + xEdge * 2) + xBorder;
		p.r.top = (i / td->columns) * (td->imgHeight + yGap + yEdge * 2) + yBorder;
		p.r.right = p.r.left + td->imgWidth + xEdge * 2;
		p.r.bottom = p.r.top + td->imgHeight + yEdge * 2;

		p.index = i;
		// tt.add(hwnd, p.imageIndex, p.r);
#ifdef OLD_TOOLTIP
		tt.add(hwnd, p.value, p.r);
#else
		g_toolTip.addTip(hwnd, i, p.r, p.tip, p.hint);
#endif
	}

	return TRUE;
}

void piw_onDestroy(HWND hwnd)
{
#ifdef DEBUG_POPUPICON
	blog.printf("piw_onDestroy(%p)", hwnd);
	blog.close();
#endif

	g_toolTip.delTips(hwnd);

	PopupIconData* td = piw_getData(hwnd);
#if 0
	ASSERT(td->items != 0);

	if(td->items != 0)
	{
		delete[] td->items;
		td->items = 0;
	}
#endif
	hToolTip = 0;
}

void piw_onPaint(HWND hwnd)
{
#ifdef DEBUG_POPUPICON
	blog.printf("piw_onPaint(%p)", hwnd);
	blog.close();
#endif

	PopupIconData* td = piw_getData(hwnd);

	PAINTSTRUCT ps;

	HDC hdc = BeginPaint(hwnd, &ps);

	HPALETTE oldPal = SelectPalette(hdc, Palette::get(), FALSE);
	RealizePalette(hdc);

	RECT r;
	GetClientRect(hwnd, &r);

	ImageLibrary* il = td->imageList;


	HBRUSH blackBrush = (HBRUSH) GetStockObject(BLACK_BRUSH);
	HBRUSH whiteBrush = (HBRUSH) GetStockObject(WHITE_BRUSH);


	for(int i = 0; i < td->items.getCount(); i++)
	{
		PopupItem& p = td->items[i];

		FrameRect(hdc, &p.r, (i == td->mouseOver) ? whiteBrush : blackBrush);
//		il.draw(hdc, p.imageIndex, p.r.left + xEdge, p.r.top + yEdge);
		il->blit(hdc, (UWORD)p.imageIndex, p.r.left + xEdge, p.r.top + yEdge);
	}

	SelectPalette(hdc, oldPal, FALSE);

	EndPaint(hwnd, &ps);
}

void piw_repaintIcon(HWND hwnd, int i, BOOL hl)
{
	if(i >= 0)
	{
		PopupIconData* td = piw_getData(hwnd);

		HDC hdc = GetDC(hwnd);
		ASSERT(hdc != NULL);

		HBRUSH brush = (HBRUSH) GetStockObject(hl ? WHITE_BRUSH : BLACK_BRUSH);

		PopupItem& p = td->items[i];

		HPALETTE oldPal = SelectPalette(hdc, Palette::get(), FALSE);
		RealizePalette(hdc);

		FrameRect(hdc, &p.r, brush);

		SelectPalette(hdc, oldPal, FALSE);

		ReleaseDC(hwnd, hdc);
	}
}


void piw_onLButtonDown(HWND hwnd, BOOL fDoubleClick, int x, int y, UINT keyFlags)
{
#ifdef DEBUG_POPUPICON
	blog.printf("piw_onLButtonDown(%p, %d,%d)", hwnd, x,y);
	blog.close();
#endif
	mouseNotMoved = False;
}

void piw_onLButtonUp(HWND hwnd, int x, int y, UINT keyFlags)
{
#ifdef DEBUG_POPUPICON
	blog.printf("piw_onLButtonUp(%p, %d,%d)", hwnd, x,y);
	blog.close();
#endif
	if((x <= 5 && x >= -5) && (y <= 5 && y >= -5))
	  mouseNotMoved = TRUE;
//     ReleaseCapture();

	else if(GetCapture())
	{
		ReleaseCapture();

		PopupIconData* td = piw_getData(hwnd);


		POINT p;
		p.x = x;
		p.y = y;
		RECT r;
		GetClientRect(hwnd, &r);
		if(PtInRect(&r, p))
		{
			if(td->mouseOver >= 0)
			{
				td->select = td->mouseOver;

#ifdef DEBUG
				blog.printf("Sending message to %p", GetParent(hwnd));
#endif

				InvalidateRect(td->hControl, NULL, TRUE);
				FORWARD_WM_COMMAND(td->hControl, GetWindowLong(hwnd, GWL_ID), hwnd, PIN_SELCHANGED, SendMessage);
			}
		}
#ifdef DEBUG_POPUPICON
		else
		{
			blog.printf("Mouse not over button");
			blog.close();
		}
#endif

		td->hPopup = NULL;
		DestroyWindow(hwnd);

	}
}

/*
 * WM_MOUSEMOVE message
 */

void piw_onMouseMove(HWND hwnd, int x, int y, UINT keyFlags)
{
	POINT pt;
	pt.x = x;
	pt.y = y;

	int over = -1;

	PopupIconData* td = piw_getData(hwnd);

	for(int i = 0; i < td->items.getCount(); i++)
	{
		PopupItem& p = td->items[i];

		if(PtInRect(&p.r, pt))
		{
			over = i;
			break;
		}
	}

	if(over != td->mouseOver)
	{
		piw_repaintIcon(hwnd, td->mouseOver, FALSE);
		td->mouseOver = over;
		piw_repaintIcon(hwnd, td->mouseOver, TRUE);

		// InvalidateRect(hwnd, NULL, TRUE);
	}
}

/*
 * WM_NOTIFY
 * Forwards on Tool Tip Text Requests
 * Replacing idFrom with the item's value
 */

#ifdef OLD_TOOLTIP
LRESULT piw_onNotify(HWND hwnd, int id, NMHDR* lpNMHDR)
{
  // if(lpNMHDR->hwndFrom == hToolTip)
  // {
	 switch(lpNMHDR->code)
	 {
		case TTN_NEEDTEXT:
		{
		  PopupIconData* td = piw_getData(hwnd);

		  lpNMHDR->idFrom = td->items[lpNMHDR->idFrom].value;

		  SendMessage(td->parent, WM_NOTIFY, (WPARAM) hwnd, (LPARAM)lpNMHDR);
		}

	 }
  // }
  return TRUE;
}
#endif	// OLD_TOOLTIP


LRESULT CALLBACK popupWindowProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
#ifdef DEBUG_POPUPICON
	blog.printf("popupIconProc::procMessage(%s)",
		getWMdescription(hWnd, msg, wParam, lParam));
#endif

	switch(msg)
	{
		HANDLE_MSG(hWnd, WM_CREATE,	piw_onCreate);
		HANDLE_MSG(hWnd, WM_DESTROY,	piw_onDestroy);
		HANDLE_MSG(hWnd, WM_PAINT,		piw_onPaint);
		HANDLE_MSG(hWnd, WM_LBUTTONDOWN, piw_onLButtonDown);
		HANDLE_MSG(hWnd, WM_LBUTTONUP, piw_onLButtonUp);
		HANDLE_MSG(hWnd, WM_MOUSEMOVE, piw_onMouseMove);
//		HANDLE_MSG(hWnd, WM_CONTEXTHELP, piw_onContextHelp);
#ifdef OLD_TOOLTIP
		HANDLE_MSG(hWnd, WM_NOTIFY, piw_onNotify);
#endif	// OLD_TOOLTIP
	default:
		return DefWindowProc(hWnd, msg, wParam, lParam);
	}
}


/*
 * Initialise class
 */

void 

initPopupIcon(HINSTANCE instance)
{
	if(!initialised)
	{
		initialised = TRUE;

		WNDCLASSEX wc;

		wc.cbSize			= sizeof(WNDCLASSEX);
		wc.style 			= CS_DBLCLKS | CS_PARENTDC;
		wc.lpfnWndProc 	= popupIconProc;
		wc.cbClsExtra 		= 0;
		// wc.cbWndExtra 		= sizeof(PopupIconData*);
		wc.cbWndExtra 		= 0;
		wc.hInstance 		= instance;
		wc.hIcon 			= NULL;
		wc.hCursor 			= LoadCursor(NULL, IDC_ARROW);
		wc.hbrBackground 	= (HBRUSH) (1 + COLOR_3DFACE);
		wc.lpszMenuName 	= NULL;
		wc.lpszClassName 	= POPUPICONCLASS;
		wc.hIconSm 			= NULL;

		RegisterClassEx(&wc);

		wc.style 			= CS_DBLCLKS | CS_PARENTDC | CS_SAVEBITS;
		wc.lpfnWndProc 	= popupWindowProc;
		wc.lpszClassName 	= POPUPWINDCLASS;
		// wc.hbrBackground 	= (HBRUSH) (1 + COLOR_WINDOW);
		RegisterClassEx(&wc);
	}
}


/*
 * Helper class functions
 */


PopupIcon::PopupIcon(HWND h) : WindowControl(h)
{
	ASSERT_CLASS(hwnd, POPUPICONCLASS);
}


PopupIcon::PopupIcon(HWND h, int id) : WindowControl(h, id)
{
	ASSERT_CLASS(hwnd, POPUPICONCLASS);
}

void PopupIcon::setImageList(ImageLibrary* img)
{
	ASSERT(img != NULL);
	ASSERT(hwnd != NULL);

	SendMessage(hwnd, PIM_SETIMAGELIST, 0, (LPARAM) (ImageLibrary*) img);
}
#if 0
void PopupIcon::setHelpIDs(DWORD* ids)
{
	ASSERT(ids != NULL);
	ASSERT(hwnd != NULL);

	SendMessage(hwnd, PIM_SETHELPIDS, 0, (LPARAM) (DWORD*) ids);
}
#endif

void

PopupIcon::set(int index)
{
	ASSERT(hwnd != NULL);

	SendMessage(hwnd, PIM_SETCURSEL, index, 0);
}

int PopupIcon::get()
{
	ASSERT(hwnd != NULL);

	return SendMessage(hwnd, PIM_GETCURSEL, 0, 0);
}

int 

PopupIcon::getValue()
{
	ASSERT(hwnd != NULL);

	int index = SendMessage(hwnd, PIM_GETCURSEL, 0, 0);
	ASSERT(index >= 0);
	return SendMessage(hwnd, PIM_GETVALUE, index, 0);
}


int

PopupIcon::addItem(int index, int value, int tipID, int hintID)
{
	ASSERT(hwnd != NULL);

	POPICONITEM item;
	item.imageIndex = index;
	item.value = value;
	item.tipID = tipID;
	item.hintID = hintID;

	return SendMessage(hwnd, PIM_ADDITEM, 0, (LPARAM)(const POPICONITEM*) &item);	// index, value);
}

void PopupIcon::removeItem(int index)
{
	ASSERT(hwnd != 0);

	SendMessage(hwnd, PIM_REMOVEITEM, index, 0);
}

void

PopupIcon::reset()
{
	ASSERT(hwnd != 0);

	SendMessage(hwnd, PIM_RESETCONTENT, 0, 0);
}

void

PopupIcon::setValue(int value)
{
	ASSERT(hwnd != 0);

	SendMessage(hwnd, PIM_SETVALUE, 0, value);
}

#if 0
HWND addPopupIcon(HWND hWnd, HWND hTooltip, int id, int x, int y, int w, int h, ImageLibrary* il)
{
  ASSERT(hWnd != 0);

  HWND hPopupIcon = CreateWindow( POPUPICONCLASS, "Order", WS_CHILD |
		WS_VISIBLE | WS_TABSTOP | BS_PUSHBUTTON | BS_ICON | BS_PUSHLIKE,
		80, 3, 20, 20, hWnd, (HMENU)id, wndInstance(hWnd), NULL);
  ASSERT(hPopupIcon != 0);

#ifdef OLD_TOOLTIP
  if(hTooltip != NULL)
  {
	 ToolTip tt(hTooltip);
	 tt.add(hWnd, hPopupIcon);
  }
#elif 0
	TipData tipData[2] = {
		{ id, 
		EndData
	};
#endif

  PopupIcon pui(hWnd, id);
  pui.setImageList(il);

  return hPopupIcon;

}
#endif


HWND

addPopupIcon(HWND hWnd, int id, int x, int y, int w, int h, ImageLibrary* il)
{
  ASSERT(hWnd != 0);

  HWND hPopupIcon = CreateWindow( POPUPICONCLASS, "Order", WS_CHILD |
		WS_VISIBLE | WS_TABSTOP | BS_PUSHBUTTON | BS_ICON | BS_PUSHLIKE,
		x, y, w, h, hWnd, (HMENU)id, wndInstance(hWnd), NULL);
  ASSERT(hPopupIcon != 0);

  PopupIcon pui(hWnd, id);
  pui.setImageList(il);
//  pui.setHelpIDs(helpIDs);

  return hPopupIcon;
}

