# Microsoft Developer Studio Project File - Name="system" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Dynamic-Link Library" 0x0102

CFG=system - Win32 Editor Debug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "system.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "system.mak" CFG="system - Win32 Editor Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "system - Win32 Release" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE "system - Win32 Debug" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE "system - Win32 Editor Debug" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE "system - Win32 Editor Release" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""
CPP=cl.exe
MTL=midl.exe
RSC=rc.exe

!IF  "$(CFG)" == "system - Win32 Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "..\..\exe"
# PROP Intermediate_Dir "o\release"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MT /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "SYSTEM_EXPORTS" /YX /FD /c
# ADD CPP /nologo /MD /W3 /Gi /GR /GX /Zi /O2 /I "..\h" /I "..\..\res" /D "NDEBUG" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "EXPORT_SYSTEM_DLL" /YX"stdinc.hpp" /FD /c
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x809 /d "NDEBUG"
# ADD RSC /l 0x809 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /dll /machine:I386
# ADD LINK32 winmm.lib gdi32.lib user32.lib vfw32.lib ole32.lib dplay.lib comctl32.lib advapi32.lib dsound.lib shell32.lib /nologo /dll /debug /machine:I386

!ELSEIF  "$(CFG)" == "system - Win32 Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "..\..\exe"
# PROP Intermediate_Dir "o\debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MTd /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "SYSTEM_EXPORTS" /YX /FD /GZ /c
# ADD CPP /nologo /MDd /W3 /Gm /Gi /GR /GX /ZI /Od /I "..\h" /I "..\..\res" /D "_DEBUG" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "EXPORT_SYSTEM_DLL" /YX"stdinc.hpp" /FD /GZ /c
# ADD BASE MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x809 /d "_DEBUG"
# ADD RSC /l 0x809 /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /dll /debug /machine:I386 /pdbtype:sept
# ADD LINK32 winmm.lib gdi32.lib user32.lib vfw32.lib ole32.lib dplayx.lib comctl32.lib advapi32.lib dsound.lib shell32.lib /nologo /dll /debug /machine:I386 /out:"..\..\exe/systemDB.dll" /pdbtype:sept

!ELSEIF  "$(CFG)" == "system - Win32 Editor Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "system___Win32_Editor_Debug"
# PROP BASE Intermediate_Dir "system___Win32_Editor_Debug"
# PROP BASE Ignore_Export_Lib 0
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "..\..\exe"
# PROP Intermediate_Dir "o\Debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MDd /W3 /Gm /Gi /GR /GX /ZI /Od /I "..\h" /I "..\..\res" /D "_DEBUG" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "EXPORT_SYSTEM_DLL" /YX"stdinc.hpp" /FD /GZ /c
# ADD CPP /nologo /MDd /W3 /Gm /Gi /GR /GX /ZI /Od /I "..\h" /I "..\..\res" /D "_USRDLL" /D "EXPORT_SYSTEM_DLL" /D "_DEBUG" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /D "EDITOR" /YX"stdinc.hpp" /FD /GZ /c
# ADD BASE MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x809 /d "_DEBUG"
# ADD RSC /l 0x809 /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 winmm.lib gdi32.lib user32.lib vfw32.lib ole32.lib dplayx.lib comctl32.lib advapi32.lib dsound.lib shell32.lib /nologo /dll /debug /machine:I386 /out:"..\..\exe/systemDB.dll" /pdbtype:sept
# ADD LINK32 winmm.lib gdi32.lib user32.lib vfw32.lib ole32.lib dplayx.lib comctl32.lib advapi32.lib dsound.lib shell32.lib /nologo /dll /debug /machine:I386 /out:"..\..\exe/systemDB.dll" /pdbtype:sept

!ELSEIF  "$(CFG)" == "system - Win32 Editor Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "system___Win32_Editor_Release"
# PROP BASE Intermediate_Dir "system___Win32_Editor_Release"
# PROP BASE Ignore_Export_Lib 0
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "..\..\exe"
# PROP Intermediate_Dir "o\Release"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MD /W3 /Gi /GR /GX /Zd /O2 /I "..\h" /I "..\..\res" /D "NDEBUG" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "EXPORT_SYSTEM_DLL" /YX"stdinc.hpp" /FD /c
# ADD CPP /nologo /MD /W3 /Gi /GR /GX /Zd /O2 /I "..\h" /I "..\..\res" /D "NDEBUG" /D "_USRDLL" /D "EXPORT_SYSTEM_DLL" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /D "EDITOR" /YX"stdinc.hpp" /FD /c
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x809 /d "NDEBUG"
# ADD RSC /l 0x809 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 winmm.lib gdi32.lib user32.lib vfw32.lib ole32.lib dplay.lib comctl32.lib advapi32.lib dsound.lib shell32.lib /nologo /dll /debug /machine:I386
# ADD LINK32 winmm.lib gdi32.lib user32.lib vfw32.lib ole32.lib dplay.lib comctl32.lib advapi32.lib dsound.lib shell32.lib /nologo /dll /debug /machine:I386

!ENDIF 

# Begin Target

# Name "system - Win32 Release"
# Name "system - Win32 Debug"
# Name "system - Win32 Editor Debug"
# Name "system - Win32 Editor Release"
# Begin Group "Source Files"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat"
# Begin Source File

SOURCE=.\alertbox.cpp
# End Source File
# Begin Source File

SOURCE=.\animate.cpp
# End Source File
# Begin Source File

SOURCE=.\app.cpp
# End Source File
# Begin Source File

SOURCE=.\array.cpp
# End Source File
# Begin Source File

SOURCE=.\bargraph.cpp
# End Source File
# Begin Source File

SOURCE=.\bezier.cpp
# End Source File
# Begin Source File

SOURCE=.\bmp.cpp
# End Source File
# Begin Source File

SOURCE=.\button.cpp
# End Source File
# Begin Source File

SOURCE=.\c_combo.cpp
# End Source File
# Begin Source File

SOURCE=.\capstest.cpp
# End Source File
# Begin Source File

SOURCE=.\cbutton.cpp
# End Source File
# Begin Source File

SOURCE=.\cedit.cpp
# End Source File
# Begin Source File

SOURCE=.\clog.cpp

!IF  "$(CFG)" == "system - Win32 Release"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "system - Win32 Debug"

!ELSEIF  "$(CFG)" == "system - Win32 Editor Debug"

!ELSEIF  "$(CFG)" == "system - Win32 Editor Release"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\cmenu.cpp
# End Source File
# Begin Source File

SOURCE=.\config.cpp
# End Source File
# Begin Source File

SOURCE=.\crc.cpp
# End Source File
# Begin Source File

SOURCE=.\critical.cpp
# End Source File
# Begin Source File

SOURCE=.\crossref.cpp
# End Source File
# Begin Source File

SOURCE=.\cscroll.cpp
# End Source File
# Begin Source File

SOURCE=.\ctab.cpp
# End Source File
# Begin Source File

SOURCE=.\ctbar.cpp
# End Source File
# Begin Source File

SOURCE=.\cust_dlg.cpp
# End Source File
# Begin Source File

SOURCE=.\date.cpp
# End Source File
# Begin Source File

SOURCE=.\datetime.cpp
# End Source File
# Begin Source File

SOURCE=.\dc_man.cpp
# End Source File
# Begin Source File

SOURCE=.\dialog.cpp
# End Source File
# Begin Source File

SOURCE=.\dib.cpp
# End Source File
# Begin Source File

SOURCE=.\dib_blt.cpp
# End Source File
# Begin Source File

SOURCE=.\dib_poly.cpp
# End Source File
# Begin Source File

SOURCE=.\dib_util.cpp
# End Source File
# Begin Source File

SOURCE=.\dibref.cpp
# End Source File
# Begin Source File

SOURCE=.\directplay.cpp
# End Source File
# Begin Source File

SOURCE=.\dlist.cpp
# End Source File
# Begin Source File

SOURCE=.\dragwin.cpp
# End Source File
# Begin Source File

SOURCE=.\except.cpp
# End Source File
# Begin Source File

SOURCE=.\filebase.cpp
# End Source File
# Begin Source File

SOURCE=.\filecnk.cpp
# End Source File
# Begin Source File

SOURCE=.\filedata.cpp
# End Source File
# Begin Source File

SOURCE=.\fileiter.cpp
# End Source File
# Begin Source File

SOURCE=.\filepack.cpp
# End Source File
# Begin Source File

SOURCE=.\fillwind.cpp
# End Source File
# Begin Source File

SOURCE=.\fonts.cpp
# End Source File
# Begin Source File

SOURCE=.\fractal.cpp
# End Source File
# Begin Source File

SOURCE=.\framewin.cpp
# End Source File
# Begin Source File

SOURCE=.\fsalloc.cpp
# End Source File
# Begin Source File

SOURCE=.\gamectrl.cpp
# End Source File
# Begin Source File

SOURCE=.\gdiengin.cpp
# End Source File
# Begin Source File

SOURCE=.\grtypes.cpp
# End Source File
# Begin Source File

SOURCE=.\gsecwind.cpp
# End Source File
# Begin Source File

SOURCE=.\gtoolbar.cpp
# End Source File
# Begin Source File

SOURCE=.\idstr.cpp
# End Source File
# Begin Source File

SOURCE=.\imglib.cpp
# End Source File
# Begin Source File

SOURCE=.\itemwind.cpp
# End Source File
# Begin Source File

SOURCE=.\logw_imp.cpp

!IF  "$(CFG)" == "system - Win32 Release"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "system - Win32 Debug"

!ELSEIF  "$(CFG)" == "system - Win32 Editor Debug"

!ELSEIF  "$(CFG)" == "system - Win32 Editor Release"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\logwin.cpp
# End Source File
# Begin Source File

SOURCE=.\lzdecode.cpp
# End Source File
# Begin Source File

SOURCE=.\lzencode.cpp
# End Source File
# Begin Source File

SOURCE=.\lzmisc.cpp
# End Source File
# Begin Source File

SOURCE=.\makename.cpp
# End Source File
# Begin Source File

SOURCE=.\mciwind.cpp
# End Source File
# Begin Source File

SOURCE=.\misc.cpp
# End Source File
# Begin Source File

SOURCE=.\mmtimer.cpp
# End Source File
# Begin Source File

SOURCE=.\msgbox.cpp
# End Source File
# Begin Source File

SOURCE=.\msgenum.cpp
# End Source File
# Begin Source File

SOURCE=.\myassert.cpp
# End Source File
# Begin Source File

SOURCE=.\nametable.cpp
# End Source File
# Begin Source File

SOURCE=.\palette.cpp
# End Source File
# Begin Source File

SOURCE=.\palwin.cpp
# End Source File
# Begin Source File

SOURCE=.\palwind.cpp
# End Source File
# Begin Source File

SOURCE=.\pathdial.cpp
# End Source File
# Begin Source File

SOURCE=.\piclib.cpp
# End Source File
# Begin Source File

SOURCE=.\poolarry.cpp
# End Source File
# Begin Source File

SOURCE=.\popicon.cpp
# End Source File
# Begin Source File

SOURCE=.\ptrlist.cpp
# End Source File
# Begin Source File

SOURCE=.\random.cpp
# End Source File
# Begin Source File

SOURCE=.\registry.cpp
# End Source File
# Begin Source File

SOURCE=.\resstr.cpp
# End Source File
# Begin Source File

SOURCE=.\scrnbase.cpp
# End Source File
# Begin Source File

SOURCE=.\simpstr.cpp
# End Source File
# Begin Source File

SOURCE=.\sllist.cpp
# End Source File
# Begin Source File

SOURCE=.\sounds.cpp
# End Source File
# Begin Source File

SOURCE=.\soundsys.cpp
# End Source File
# Begin Source File

SOURCE=.\splash.cpp
# End Source File
# Begin Source File

SOURCE=.\sprlib.cpp
# End Source File
# Begin Source File

SOURCE=.\sync.cpp
# End Source File
# Begin Source File

SOURCE=.\tables.cpp
# End Source File
# Begin Source File

SOURCE=.\tabwin.cpp
# End Source File
# Begin Source File

SOURCE=.\thicklin.cpp
# End Source File
# Begin Source File

SOURCE=.\thread.cpp
# End Source File
# Begin Source File

SOURCE=.\threadmsg.cpp
# End Source File
# Begin Source File

SOURCE=.\timer.cpp
# End Source File
# Begin Source File

SOURCE=.\todolog.cpp
# End Source File
# Begin Source File

SOURCE=.\tooltip.cpp
# End Source File
# Begin Source File

SOURCE=.\trackwin.cpp
# End Source File
# Begin Source File

SOURCE=.\transwin.cpp
# End Source File
# Begin Source File

SOURCE=.\trig.cpp
# End Source File
# Begin Source File

SOURCE=.\w95excpt.cpp
# End Source File
# Begin Source File

SOURCE=.\wavefile.cpp
# End Source File
# Begin Source File

SOURCE=.\winctrl.cpp
# End Source File
# Begin Source File

SOURCE=.\wind.cpp
# End Source File
# Begin Source File

SOURCE=.\winerror.cpp
# End Source File
# Begin Source File

SOURCE=.\winfile.cpp
# End Source File
# Begin Source File

SOURCE=.\wmisc.cpp
# End Source File
# End Group
# Begin Group "Header Files"

# PROP Default_Filter "h;hpp;hxx;hm;inl"
# Begin Source File

SOURCE=.\alertbox.hpp
# End Source File
# Begin Source File

SOURCE=.\animate.hpp
# End Source File
# Begin Source File

SOURCE=.\app.hpp
# End Source File
# Begin Source File

SOURCE=.\array.hpp
# End Source File
# Begin Source File

SOURCE=.\ary_sort.hpp
# End Source File
# Begin Source File

SOURCE=.\audit.hpp
# End Source File
# Begin Source File

SOURCE=.\autoptr.hpp
# End Source File
# Begin Source File

SOURCE=.\bargraph.hpp
# End Source File
# Begin Source File

SOURCE=.\bezier.hpp
# End Source File
# Begin Source File

SOURCE=.\BitSet.hpp
# End Source File
# Begin Source File

SOURCE=.\bmp.hpp
# End Source File
# Begin Source File

SOURCE=.\bres.hpp
# End Source File
# Begin Source File

SOURCE=.\button.hpp
# End Source File
# Begin Source File

SOURCE=.\bw_int.hpp
# End Source File
# Begin Source File

SOURCE=.\c_combo.hpp
# End Source File
# Begin Source File

SOURCE=.\capstest.hpp
# End Source File
# Begin Source File

SOURCE=.\cbutton.hpp
# End Source File
# Begin Source File

SOURCE=.\cedit.hpp
# End Source File
# Begin Source File

SOURCE=.\clistbox.hpp
# End Source File
# Begin Source File

SOURCE=.\clog.hpp
# End Source File
# Begin Source File

SOURCE=.\cmenu.hpp
# End Source File
# Begin Source File

SOURCE=.\colours.hpp
# End Source File
# Begin Source File

SOURCE=.\config.hpp
# End Source File
# Begin Source File

SOURCE=.\crc.hpp
# End Source File
# Begin Source File

SOURCE=.\critical.hpp
# End Source File
# Begin Source File

SOURCE=.\crossref.hpp
# End Source File
# Begin Source File

SOURCE=.\cscroll.hpp
# End Source File
# Begin Source File

SOURCE=.\ctab.hpp
# End Source File
# Begin Source File

SOURCE=.\ctbar.hpp
# End Source File
# Begin Source File

SOURCE=.\cust_dlg.hpp
# End Source File
# Begin Source File

SOURCE=.\custbord.hpp
# End Source File
# Begin Source File

SOURCE=.\date.hpp
# End Source File
# Begin Source File

SOURCE=.\datetime.hpp
# End Source File
# Begin Source File

SOURCE=.\dc_hold.hpp
# End Source File
# Begin Source File

SOURCE=.\dc_man.hpp
# End Source File
# Begin Source File

SOURCE=.\dialog.hpp
# End Source File
# Begin Source File

SOURCE=.\dib.hpp
# End Source File
# Begin Source File

SOURCE=.\dib_blt.hpp
# End Source File
# Begin Source File

SOURCE=.\dib_poly.hpp
# End Source File
# Begin Source File

SOURCE=.\dib_util.hpp
# End Source File
# Begin Source File

SOURCE=.\dibref.hpp
# End Source File
# Begin Source File

SOURCE=.\directplay.hpp
# End Source File
# Begin Source File

SOURCE=.\dlist.hpp
# End Source File
# Begin Source File

SOURCE=.\dragwin.hpp
# End Source File
# Begin Source File

SOURCE=.\dynarray.hpp
# End Source File
# Begin Source File

SOURCE=.\except.hpp
# End Source File
# Begin Source File

SOURCE=.\filebase.hpp
# End Source File
# Begin Source File

SOURCE=.\filecnk.hpp
# End Source File
# Begin Source File

SOURCE=.\filedata.hpp
# End Source File
# Begin Source File

SOURCE=.\fileiter.hpp
# End Source File
# Begin Source File

SOURCE=.\filepack.hpp
# End Source File
# Begin Source File

SOURCE=.\fillwind.hpp
# End Source File
# Begin Source File

SOURCE=.\fixpoint.hpp
# End Source File
# Begin Source File

SOURCE=.\fonts.hpp
# End Source File
# Begin Source File

SOURCE=.\fractal.hpp
# End Source File
# Begin Source File

SOURCE=.\framewin.hpp
# End Source File
# Begin Source File

SOURCE=.\fsalloc.hpp
# End Source File
# Begin Source File

SOURCE=.\ftol.hpp
# End Source File
# Begin Source File

SOURCE=.\gamectrl.hpp
# End Source File
# Begin Source File

SOURCE=.\gdiengin.hpp
# End Source File
# Begin Source File

SOURCE=.\gengine.hpp
# End Source File
# Begin Source File

SOURCE=.\grtypes.hpp
# End Source File
# Begin Source File

SOURCE=.\gsecwind.hpp
# End Source File
# Begin Source File

SOURCE=.\gtoolbar.hpp
# End Source File
# Begin Source File

SOURCE=.\idstr.hpp
# End Source File
# Begin Source File

SOURCE=.\imglib.hpp
# End Source File
# Begin Source File

SOURCE=.\itemwind.hpp
# End Source File
# Begin Source File

SOURCE=.\llist.hpp
# End Source File
# Begin Source File

SOURCE=.\logw_imp.hpp
# End Source File
# Begin Source File

SOURCE=.\logwin.hpp
# End Source File
# Begin Source File

SOURCE=.\lzhuf.h
# End Source File
# Begin Source File

SOURCE=.\lzmisc.h
# End Source File
# Begin Source File

SOURCE=.\makename.hpp
# End Source File
# Begin Source File

SOURCE=.\mciwind.hpp
# End Source File
# Begin Source File

SOURCE=.\memptr.hpp
# End Source File
# Begin Source File

SOURCE=.\minmax.hpp
# End Source File
# Begin Source File

SOURCE=.\misc.hpp
# End Source File
# Begin Source File

SOURCE=.\mmtimer.hpp
# End Source File
# Begin Source File

SOURCE=.\msgbox.hpp
# End Source File
# Begin Source File

SOURCE=.\msgenum.hpp
# End Source File
# Begin Source File

SOURCE=.\muldiv.hpp
# End Source File
# Begin Source File

SOURCE=.\myassert.hpp
# End Source File
# Begin Source File

SOURCE=.\mytypes.h
# End Source File
# Begin Source File

SOURCE=.\nametable.hpp
# End Source File
# Begin Source File

SOURCE=.\palette.hpp
# End Source File
# Begin Source File

SOURCE=.\palwin.hpp
# End Source File
# Begin Source File

SOURCE=.\palwind.hpp
# End Source File
# Begin Source File

SOURCE=.\pathdial.hpp
# End Source File
# Begin Source File

SOURCE=.\permbuf.hpp
# End Source File
# Begin Source File

SOURCE=.\pick.hpp
# End Source File
# Begin Source File

SOURCE=.\piclib.hpp
# End Source File
# Begin Source File

SOURCE=.\point.hpp
# End Source File
# Begin Source File

SOURCE=.\poolarry.hpp
# End Source File
# Begin Source File

SOURCE=.\popicon.hpp
# End Source File
# Begin Source File

SOURCE=.\pt_util.hpp
# End Source File
# Begin Source File

SOURCE=.\ptrlist.hpp
# End Source File
# Begin Source File

SOURCE=.\random.hpp
# End Source File
# Begin Source File

SOURCE=.\range.hpp
# End Source File
# Begin Source File

SOURCE=.\refptr.hpp
# End Source File
# Begin Source File

SOURCE=.\refptrim.hpp
# End Source File
# Begin Source File

SOURCE=.\registry.hpp
# End Source File
# Begin Source File

SOURCE=.\resstr.hpp
# End Source File
# Begin Source File

SOURCE=.\scrnbase.hpp
# End Source File
# Begin Source File

SOURCE=.\simpstk.hpp
# End Source File
# Begin Source File

SOURCE=.\simpstr.hpp
# End Source File
# Begin Source File

SOURCE=.\sllist.hpp
# End Source File
# Begin Source File

SOURCE=.\sounds.hpp
# End Source File
# Begin Source File

SOURCE=.\soundsys.hpp
# End Source File
# Begin Source File

SOURCE=.\spacing.hpp
# End Source File
# Begin Source File

SOURCE=.\splash.hpp
# End Source File
# Begin Source File

SOURCE=.\sprlib.hpp
# End Source File
# Begin Source File

SOURCE=.\staticinit.h
# End Source File
# Begin Source File

SOURCE=.\stdinc.hpp
# End Source File
# Begin Source File

SOURCE=.\swgspr.hpp
# End Source File
# Begin Source File

SOURCE=.\sync.hpp
# End Source File
# Begin Source File

SOURCE=.\sysdll.h
# End Source File
# Begin Source File

SOURCE=.\systick.hpp
# End Source File
# Begin Source File

SOURCE=.\tables.hpp
# End Source File
# Begin Source File

SOURCE=.\tabwin.hpp
# End Source File
# Begin Source File

SOURCE=.\thicklin.hpp
# End Source File
# Begin Source File

SOURCE=.\thread.hpp
# End Source File
# Begin Source File

SOURCE=.\threadmsg.hpp
# End Source File
# Begin Source File

SOURCE=.\timer.hpp
# End Source File
# Begin Source File

SOURCE=.\todolog.hpp
# End Source File
# Begin Source File

SOURCE=.\tooltip.hpp
# End Source File
# Begin Source File

SOURCE=.\trackwin.hpp
# End Source File
# Begin Source File

SOURCE=.\transwin.hpp
# End Source File
# Begin Source File

SOURCE=.\trig.hpp
# End Source File
# Begin Source File

SOURCE=.\typedef.hpp
# End Source File
# Begin Source File

SOURCE=.\w95excpt.hpp
# End Source File
# Begin Source File

SOURCE=.\wavefile.hpp
# End Source File
# Begin Source File

SOURCE=.\winctrl.hpp
# End Source File
# Begin Source File

SOURCE=.\wind.hpp
# End Source File
# Begin Source File

SOURCE=.\windsave.hpp
# End Source File
# Begin Source File

SOURCE=.\winerror.hpp
# End Source File
# Begin Source File

SOURCE=.\winfile.hpp
# End Source File
# Begin Source File

SOURCE=.\wmisc.hpp
# End Source File
# End Group
# Begin Group "Resource Files"

# PROP Default_Filter "ico;cur;bmp;dlg;rc2;rct;bin;rgs;gif;jpg;jpeg;jpe"
# End Group
# Begin Source File

SOURCE=..\h\globals.hpp
# End Source File
# End Target
# End Project
