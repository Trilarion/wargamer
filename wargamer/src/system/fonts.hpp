/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef FONTS_HPP
#define FONTS_HPP

#ifndef __cplusplus
#error fonts.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *	Fonts Management
 * Removed from bargraph
 *
 *----------------------------------------------------------------------
 */

#include "sysdll.h"
#include <windef.h>
#include "mytypes.h"

#define UNCHECKED_FONT_TYPES		// Include backward compatible functions
// #undef UNCHECKED_FONT_TYPES

class CString;

namespace Greenius_System
{

/*
 * Some types
 */

enum FontFamily
{
	Decorative	= FF_DECORATIVE,
	DontCare		= FF_DONTCARE,
	Modern		= FF_MODERN,
	Roman			= FF_ROMAN,
	Script		= FF_SCRIPT,
	Swiss			= FF_SWISS,
};

enum FontPitch
{
	DefaultPitch	= DEFAULT_PITCH,
	FixedPitch 	= FIXED_PITCH,
	VariablePitch = VARIABLE_PITCH,
};

enum Charset
{
	ANSI 				= ANSI_CHARSET,
	DefaultCharset	= DEFAULT_CHARSET,
	Symbol 			= SYMBOL_CHARSET,
	ShiftJIS 		= SHIFTJIS_CHARSET,
	GB2312 			= GB2312_CHARSET,
	Hangeul 			= HANGEUL_CHARSET,
	ChineseBig5 	= CHINESEBIG5_CHARSET,
	OEM 				= OEM_CHARSET,
	Johab 			= JOHAB_CHARSET,
	Hebrew 			= HEBREW_CHARSET,
	Arabic 			= ARABIC_CHARSET,
	Greek 			= GREEK_CHARSET,
	Turkish 			= TURKISH_CHARSET,
	Thai 				= THAI_CHARSET,
	EastEurope 		= EASTEUROPE_CHARSET,
	Russian 			= RUSSIAN_CHARSET,
	Mac 				= MAC_CHARSET,
	Baltic 			= BALTIC_CHARSET,

};


/*
 *  Scenario should be the only class accessing this class
 */


class SYSTEM_DLL GlobalFonts {
  	// friend class Scenario;		// SYSTEM.LIB does not know about scenario
	public:
	  static Boolean addFontToResource(const char* fileName, CString& faceName);	//, const char* faceName);
  			// installs font
			// set faceName to the TTF's typeface name
			// returns True if succesful
};


/*
 * wrapper for Windows LOGFONT structure
 */

class SYSTEM_DLL LogFont
{
		LOGFONT d_logFont;
	public:
		LogFont()	// Set values to sensible default
		{
			d_logFont.lfHeight = 0;
			d_logFont.lfWidth = 0;
			d_logFont.lfEscapement = 0;
			d_logFont.lfOrientation = 0;
			d_logFont.lfWeight = 0;
			d_logFont.lfItalic = FALSE;
			d_logFont.lfUnderline = FALSE;
			d_logFont.lfStrikeOut = FALSE;
			d_logFont.lfCharSet = ANSI_CHARSET;    // DEFAULT_CHARSET;
			d_logFont.lfOutPrecision = OUT_DEFAULT_PRECIS;
			d_logFont.lfClipPrecision = CLIP_DEFAULT_PRECIS;
			d_logFont.lfQuality = PROOF_QUALITY;	// DEFAULT_QUALITY?
			d_logFont.lfPitchAndFamily = DEFAULT_PITCH | FF_SWISS;      // FF_DONTCARE;
			memset(d_logFont.lfFaceName, 0, sizeof(d_logFont.lfFaceName));
		}

		~LogFont() { }

		void height(LONG h) { d_logFont.lfHeight = h; }
		void width(LONG w) { d_logFont.lfWidth = w; }
		void weight(LONG wt) { d_logFont.lfWeight = wt; }
		void italic(bool i) { d_logFont.lfItalic = i ? TRUE : FALSE; }
		void underline(bool i) { d_logFont.lfUnderline = i ? TRUE : FALSE; }

		void style(FontFamily f) { d_logFont.lfPitchAndFamily = f | pitch(); }
#ifdef UNCHECKED_FONT_TYPES
		void style(int f) { style(FontFamily(f)); }
#endif

		void pitch(FontPitch p) { d_logFont.lfPitchAndFamily = p | style(); }
		void face(const char* s)
		{
			if(s == 0)
				d_logFont.lfFaceName[0] = 0;
			else
				lstrcpy(d_logFont.lfFaceName, s);
		}

		void charset(Charset c) { d_logFont.lfCharSet = c; }

		operator const LOGFONT& () const { return d_logFont; }

	private:
		FontPitch pitch() const { return FontPitch(d_logFont.lfPitchAndFamily & 0x03); }
		FontFamily style() const { return FontFamily(d_logFont.lfPitchAndFamily & 0xf0); }
};


/*
 * Holder for a HFONT that automatically works with FontManager
 */


class SYSTEM_DLL Font {
	HFONT d_handle;

	Font(const Font&);
	Font& operator = (const Font&);
public:
	Font() : d_handle(NULL) { }
	~Font();

	// Simple set commands

	void setFace(const char* faceName, int height, int width, int weight);
	void setStyle(FontFamily style, int height, int width, int weight);

#ifdef UNCHECKED_FONT_TYPES
	// For backwards compatibilty... no type checking
	void setStyle(BYTE style, int height, int width, int weight)
	{
		setStyle(FontFamily(style), height, width, weight);
	}
#endif

	void set(const LogFont& lf);
		// More advanced setup

	void set(int h, int w = 0, int weight = FW_DONTCARE) { setStyle(DontCare, h, w, weight); }
		// For backwards compatibility

	void reset();

	HFONT getHandle() const { return d_handle; }
	operator HFONT() const { return d_handle; }
};

/*
 * Helper class to auto-select into DC
 * we should probably add some more setFont functions to match up with
 * the font classes.
 */

class SYSTEM_DLL Fonts
{
		Font d_font;
		HDC d_hdc;
		HFONT d_oldFont;

		Fonts(const Fonts&);
		Fonts& operator = (const Fonts&);
	public:
		Fonts();
		~Fonts();

		// These functions are for backwards compatibilty

		void setFont(HDC hdc, int h, int w, int weight = FW_NORMAL)
		{
			setStyle(hdc, DontCare, h, w, weight);
		}

		void deleteFont();

		// These 3 are the preferred method of access

		void setFace(HDC hdc, const char* faceName, int height, int width, int weight);
		void setStyle(HDC hdc, FontFamily style, int height, int width, int weight);
		void set(HDC hdc, const LogFont& lf);

#ifdef UNCHECKED_FONT_TYPES
		// For backwards compatibility... no type checking
		void setStyle(HDC hdc, BYTE style, int height, int width, int weight)
		{
			setStyle(hdc, FontFamily(style), height, width, weight);
		}
#endif

		HFONT handle() const { return d_font.getHandle(); }

};

/*
 * For backwards compatibility
 * This should be replaced by Fonts
 */

class SYSTEM_DLL FontManager
{
		Fonts d_fonts;
	public:
		FontManager() : d_fonts() { }
		~FontManager() { }

		HFONT getFont() const { return d_fonts.handle(); }
		void setFont(HDC hdc, const char* face, int h)
		{
			d_fonts.setFace(hdc, face, h, 0, FW_NORMAL);
		}

		static HFONT getFont(int h);
			// should be obsolete
			// used by calctrl

		static HFONT getFont(const char* faceName, int h);
			// used by unitdial
};


};		// namespace Greenius_System

// For backwards Compatibility

using Greenius_System::GlobalFonts;
using Greenius_System::LogFont;
using Greenius_System::Font;
using Greenius_System::Fonts;
using Greenius_System::FontManager;


#endif /* FONTS_HPP */

