/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef SYSDLL_H
#define SYSDLL_H
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 * Included by all files in system DLL
 *
 * Defines 
 *
 *
 *----------------------------------------------------------------------
 */

#if defined(NO_WG_DLL)
    #define SYSTEM_DLL
#elif defined(EXPORT_SYSTEM_DLL)
	// #pragma message("Exporting SYSYTEM_DLL");
    #define SYSTEM_DLL __declspec(dllexport)
#else
	// #pragma message("Importing SYSYTEM_DLL");
    #define SYSTEM_DLL __declspec(dllimport)
#endif

#endif /* SYSDLL_H */


