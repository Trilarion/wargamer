/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef TYPEDEF_HPP
#define TYPEDEF_HPP

#ifndef __cplusplus
#error typedef.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *	Template for distinguishing typedefs of the same type
 *
 * e.g. insted of saying:
 *			typedef int MyInt;
 *			typedef int YourInt;
 * you say:
 * 		TypeDef(int, MyInt);
 *			TypeDef(int, YourInt);
 *
 * Now it is illegal to do the following:
 *		MyInt a;
 *		YourInt b;
 *		a = b;		// Illegal
 *		func(MyInt i) { }
 *		func(b);			// Illegal
 *
 * If you use DTypeDef, then it only uses the safe method if DEBUG is defined
 * and a normal unsafe typedef otherwise
 *----------------------------------------------------------------------
 */

#include "sysdll.h"

template<class Type>
class SafeValue
{
	public:
		SafeValue<Type>() { }
		SafeValue<Type>(Type v) { d_value = v; }
		SafeValue<Type>(const SafeValue<Type>& a) { d_value = a.d_value; }
		
		SafeValue<Type>& operator = (const SafeValue<Type>& a) { d_value = a.d_value; return *this; }
		// SafeValue<Type>& operator = (Type v) { d_value = v; return *this; }
		operator Type() const { return d_value; }

	private:
		Type d_value;
};					

#define TypeDef(type, name) \
	class name : public SafeValue<type> \
	{ \
		public: \
		name() : SafeValue<type>() { } \
		name(type v) : SafeValue<type>(v) { } \
	}

#ifdef DEBUG
#define DTypeDef(type, name) TypeDef(type, name)
#else
#define DTypeDef(type, name) typedef type, name
#endif


#endif /* TYPEDEF_HPP */

