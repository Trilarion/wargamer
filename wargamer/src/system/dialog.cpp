/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			      *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								            *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1995, Steven Morle-Green, All Rights Reserved
 *----------------------------------------------------------------------
 *
 *	Modeless Dialogue box
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.2  2001/06/18 08:25:46  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 * Revision 1.1  2001/06/13 08:52:39  greenius
 * Initial Import
 *
 * Revision 1.10  1996/06/22 19:06:06  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.9  1996/02/23 14:08:35  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.8  1996/02/22 10:28:36  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.7  1995/12/11 11:50:58  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.6  1995/12/07 09:15:20  Steven_Green
 * GetDLgItemText can return NULL if string is 0 bytes long.
 *
 * Revision 1.5  1995/12/05 09:19:20  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.4  1995/11/29 12:13:46  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.3  1995/11/22 10:43:46  Steven_Green
 * WM_DESTROY passed on to class's function.
 *
 * Revision 1.2  1995/11/14 11:24:47  Steven_Green
 * Combo box helper functions
 *
 * Revision 1.1  1995/11/13 11:08:49  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "dialog.hpp"

#include "misc.hpp"
#include "app.hpp"
#include "winctrl.hpp"
#include "msgenum.hpp"
#include "memptr.hpp"

// #define DEBUG_MESSAGES
#ifdef DEBUG_MESSAGES
#include "myassert.hpp"
#endif


HWND

ModelessDialog::createDialog(LPCTSTR dialName, HWND parent, Boolean showDialog)
{
	ASSERT(dialName != NULL);

#ifdef DEBUG
	HRSRC hRes = FindResource(APP::instance(), dialName, RT_DIALOG);
	ASSERT(hRes != NULL);
#endif

	HWND dhwnd = ::CreateDialogParam(APP::instance(), dialName, parent, baseDialogProc, (LPARAM) this);
	ASSERT(dhwnd != NULL);
	if(dhwnd == NULL)
		throw ErrorCreateWindow("Error creating dialog");

	if(showDialog)
	  ShowWindow(dhwnd, SW_SHOW);

	return dhwnd;
}

HWND

ModelessDialog::createDialogIndirect(LPCDLGTEMPLATE dialog, HWND parent)
{
	ASSERT(dialog != NULL);
	HWND dhwnd = ::CreateDialogIndirectParam(APP::instance(), dialog, parent, baseDialogProc, (LPARAM) this);
	ASSERT(dhwnd != NULL);
	return dhwnd;
}

/*
 * Call back function for dialogue
 */

BOOL CALLBACK ModelessDialog::baseDialogProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
#ifdef DEBUG_MESSAGES
	debugLog("ModelessDialog::baseDialogProc(%s)\n", 
		getWMdescription(hWnd, msg, wParam, lParam));
#endif

	ModelessDialog* dial = (ModelessDialog*) GetWindowLong(hWnd, DWL_USER);

	if(dial == 0)
	{
		if(msg == WM_INITDIALOG)
		{
			dial = (ModelessDialog*) lParam;
			ASSERT(dial != 0);
			SetWindowLong(hWnd, DWL_USER, (LONG) dial);
			dial->hWnd = hWnd;
			return dial->procMessage(hWnd, msg, wParam, lParam);
		}
		else
		{
#ifdef DEBUG
			debugLog("ModelessDialog:: Ignoring %s\n", getWMdescription(hWnd, msg, wParam, lParam));
#endif
			return FALSE;
		}
	}
	else
	{
		if(msg == WM_NCDESTROY)
		{
			dial->procMessage(hWnd, msg, wParam, lParam);
			APP::clearActiveDial(hWnd);

			if(dial->hWnd != NULL)
			{
				dial->notifyParent();
				dial->hWnd = NULL;
				if(dial->d_deleteSelf)
					delete dial;
			}
			dial = NULL;
			SetWindowLong(hWnd, DWL_USER, (LONG) dial);
			return TRUE;
		}
		else
		{
			if(msg == WM_ACTIVATE)
			{
				UINT state = LOWORD(wParam);
				if((state == WA_ACTIVE) || (state == WA_CLICKACTIVE))
					APP::setActiveDial(hWnd);
				else if(state == WA_INACTIVE)
					APP::clearActiveDial(hWnd);
				dial->procMessage(hWnd, msg, wParam, lParam);
				return TRUE;
			}

			return dial->procMessage(hWnd, msg, wParam, lParam);
		}
	}
}

BOOL CALLBACK ModelessDialog::baseModalDialogProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
#ifdef DEBUG_MESSAGES
	debugLog("ModelessDialog::baseModalDialogProc(%s)\n",
		getWMdescription(hWnd, msg, wParam, lParam));
#endif

	ModelessDialog* dial = (ModelessDialog*) GetWindowLong(hWnd, DWL_USER);

	if(dial == 0)
	{
		if(msg == WM_INITDIALOG)
		{
			dial = (ModelessDialog*) lParam;
			ASSERT(dial != 0);
			SetWindowLong(hWnd, DWL_USER, (LONG) dial);
			dial->hWnd = hWnd;
			return dial->procMessage(hWnd, msg, wParam, lParam);
		}
		else
		{
#ifdef DEBUG
			debugLog("ModelessDialog:: Ignoring %s\n", getWMdescription(hWnd, msg, wParam, lParam));
#endif
			return FALSE;
		}
	}
	else
	{
		if(msg == WM_NCDESTROY)
		{
			dial->procMessage(hWnd, msg, wParam, lParam);
			APP::clearActiveDial(hWnd);
			dial->hWnd = NULL;
			return TRUE;
		}
		else
		{
			if(msg == WM_ACTIVATE)
			{
				UINT state = LOWORD(wParam);
				if((state == WA_ACTIVE) || (state == WA_CLICKACTIVE))
					APP::setActiveDial(hWnd);
				else if(state == WA_INACTIVE)
					APP::clearActiveDial(hWnd);
				dial->procMessage(hWnd, msg, wParam, lParam);
				return TRUE;
			}

			return dial->procMessage(hWnd, msg, wParam, lParam);
		}
	}
}

BOOL CALLBACK ModelessDialog::basePropsheetProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	ModelessDialog* dial = (ModelessDialog*) GetWindowLong(hWnd, DWL_USER);

	if(dial == 0)
	{
		if(msg == WM_INITDIALOG)
		{
			PROPSHEETPAGE* psp = (PROPSHEETPAGE*) lParam;
			ASSERT(psp != 0);
			dial = (ModelessDialog*) psp->lParam;
			ASSERT(dial != 0);
			SetWindowLong(hWnd, DWL_USER, (LONG) dial);
			dial->hWnd = hWnd;
			return dial->procMessage(hWnd, msg, wParam, lParam);
		}
		else
		{
#ifdef DEBUG
			debugLog("ModelessPropSheet:: Ignoring %s\n", getWMdescription(hWnd, msg, wParam, lParam));
#endif
			return FALSE;
		}
	}
	else
	{
		if(msg == WM_NCDESTROY)
		{
			dial->procMessage(hWnd, msg, wParam, lParam);
			// gApp.clearActiveDial(hWnd);

			if(dial->hWnd != NULL)
			{
				dial->notifyParent();
				dial->hWnd = NULL;
				if(dial->d_deleteSelf)
					delete dial;
				dial = NULL;
			}
			SetWindowLong(hWnd, DWL_USER, (LONG) dial);
			return TRUE;
		}
		else
			return dial->procMessage(hWnd, msg, wParam, lParam);
	}
}


char* ModelessDialog::getItemIDText(int id)
{
 	const int MaxBuf = 100;
 	
 	MemPtr<char> buf(MaxBuf);

 	if(GetDlgItemText(hWnd, id, buf.get(), MaxBuf))
 		return copyString(buf.get());
	else
		return 0;
}


void

ModelessDialog::initComboIDItems(int id, ComboInit* data)
{
	initComboItems(getHWND(), id, data);
}

void

ModelessDialog::setComboIDValue(int id, int value)
{
	setComboValue(getHWND(), id, value);
}

int

ModelessDialog::getComboIDValue(int id)
{
	return getComboValue(getHWND(), id);
}

int

ModelessDialog::getSpinIDValue(int id)
{
	return SendDlgItemMessage(getHWND(), id, UDM_GETPOS, 0, 0);
}

void 

ModelessDialog::setSpinIDValue(int id, int value)
{
	SendDlgItemMessage(getHWND(), id, UDM_SETPOS, 0, MAKELONG(value, 0));
}

void 

ModelessDialog::setButtonIDCheck(int id, Boolean flag)
{
	CheckDlgButton(hWnd, id, flag ? BST_CHECKED : BST_UNCHECKED);
}

Boolean

ModelessDialog::getButtonIDCheck(int id)
{
	return (IsDlgButtonChecked(hWnd, id) == BST_CHECKED);
}

void ModelessDialog::enableIDControl(int id, BOOL flag)
{
	HWND hControl = GetDlgItem(getHWND(), id);
	ASSERT(hControl != NULL);
	if(hControl != NULL)
		EnableWindow(hControl, flag);
}

void ModelessDialog::showIDControl(int id, BOOL flag)
{
	HWND hControl = GetDlgItem(getHWND(), id);
	ASSERT(hControl != NULL);
	if(hControl != NULL)
		ShowWindow(hControl, flag ? SW_SHOW : SW_HIDE);
}

void ModelessDialog::setIDText(int id, LPCTSTR text)
{
	ASSERT(text != 0);
	HWND hControl = GetDlgItem(getHWND(), id);
	ASSERT(hControl != NULL);
	if(hControl != NULL)
		SetWindowText(hControl, text);
}



void initComboItems(HWND dialog, int id, ComboInit* data)
{
	HWND hCombo = GetDlgItem(dialog, id);
	ASSERT(hCombo != NULL);
	ASSERT_CLASS(hCombo, COMBOCLASS);

	for(; data->name; data++)
	{
		int index = SendMessage(hCombo, CB_ADDSTRING, 0, (LPARAM) data->name);
		ASSERT(index != CB_ERR);
		ASSERT(index != CB_ERRSPACE);
		SendMessage(hCombo, CB_SETITEMDATA, index, data->value);
	}
}

void setComboValue(HWND dialog, int id, int value)
{
	HWND hCombo = GetDlgItem(dialog, id);
	ASSERT(hCombo != NULL);
	ASSERT_CLASS(hCombo, COMBOCLASS);
	int nItems = SendMessage(hCombo, CB_GETCOUNT, 0, 0);
	for(int index = 0; index < nItems; index++)
	{
		int itemData = SendMessage(hCombo, CB_GETITEMDATA, index, 0);
		if(itemData == value)
		{
			SendMessage(hCombo, CB_SETCURSEL, index, 0);
			break;
		}
	}

	ASSERT(index < nItems);
}

int 

getComboValue(HWND hCombo)
{
	ASSERT(hCombo != NULL);
	ASSERT_CLASS(hCombo, COMBOCLASS);
	int index = SendMessage(hCombo, CB_GETCURSEL, 0, 0);

	ASSERT(index != CB_ERR);
	if(index != CB_ERR)
		return SendMessage(hCombo, CB_GETITEMDATA, index, 0);
 	else
		return 0;
}

int getComboValue(HWND dialog, int id)
{
	HWND hCombo = GetDlgItem(dialog, id);
	ASSERT_CLASS(hCombo, COMBOCLASS);
	return getComboValue(hCombo);
}

void 

setButtonCheck(HWND dialog, int id, Boolean flag)
{
	CheckDlgButton(dialog, id, flag ? BST_CHECKED : BST_UNCHECKED);
}




void addComboItem(HWND hCombo, const char* name, int id)
{
	ASSERT(hCombo != NULL);
	ASSERT_CLASS(hCombo, COMBOCLASS);
	ASSERT(name != 0);
	int index = SendMessage(hCombo, CB_ADDSTRING, 0, (LPARAM) name);
	ASSERT(index != CB_ERR);
	ASSERT(index != CB_ERRSPACE);
	SendMessage(hCombo, CB_SETITEMDATA, index, id);
}

void removeComboItems(HWND hCombo)
{
	ASSERT(hCombo != NULL);
	ASSERT_CLASS(hCombo, COMBOCLASS);
	SendMessage(hCombo, CB_RESETCONTENT, 0, 0);
}


int 

ModalDialog::createDialog(LPCTSTR dlgName, HWND parent)
{
	ASSERT(dlgName != 0);
	int result = DialogBoxParam(APP::instance(), dlgName, parent, baseModalDialogProc, (LPARAM) this);
	ASSERT(result != -1);
	return result;
}

/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.2  2001/06/18 08:25:46  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */

