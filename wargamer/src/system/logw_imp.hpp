/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
#ifndef LOGW_IMP_HPP
#define LOGW_IMP_HPP

#ifndef __cplusplus
#error logw_imp.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *
 *
 *----------------------------------------------------------------------
 */

#include "sysdll.h"

#if !defined(NOLOG)

#include "logwin.hpp"         // Implements this


#include "wind.hpp"           // Derived from WindowBase
#include "clog.hpp"           // Derived from LogFile
#include "fonts.hpp"
#include "dlist.hpp"
#include "StringPtr.hpp"
#include "sync.hpp"

class SYSTEM_DLL LogWindowText : public DLink
{
   StringPtr d_text;
 public:
   LogWindowText(const char* s) : DLink(), d_text(s) { }
   const char* text() const { return d_text; }
};

class SYSTEM_DLL WindowsLogWindow : 
   public LogWindow,
   public WindowBaseND,
   public LogFileFlush
{
   typedef LogFileFlush superLog;

    static const char s_className[];
    static ATOM classAtom;

    // LogWinPtr* d_owner;
    StringPtr d_title;        // Title
    Fonts font;
    // Boolean showFlag;

      RWLock d_lock;          // To prevent data structures from being modified

    int d_lineHeight;         // Height of a line of text
    int d_clientHeight;       // Height of client area
    int d_linesOnDisplay;     // Number of lines currently being displayed
    LogWindowText* d_topLine; // Top Line to display
    int d_topLineNumber;      // Line number of top line
    Boolean d_atBottom;       // Set if window should scroll as new lines are added

    DiList<LogWindowText> d_textList;  // Linked list of text
    int d_maxLines;                    // Maximum number of lines in history

  private:
      // constructor is private because they can only be created
      // by WindowsLogWindow::make()

    // WindowsLogWindow(LogWinPtr* owner, HWND parent, const char* title, const char* fileName);
    WindowsLogWindow(HWND parent, const char* title, const char* fileName);
  public:
    // WindowsLogWindow(HWND parent, const char* title, const char* fileName);

    ~WindowsLogWindow();

     virtual const char* className() const { return s_className; }

    // static void make(LogWinPtr* owner, HWND parent, const char* title, const char* fileName);
    static LogWindow* make(HWND parent, const char* title, const char* fileName);

    void vprintf(const char* fmt, va_list vaList);

    void show(bool visible) { WindowBaseND::show(visible); }
    void show() { show(true); }
    void hide() { show(false); }

  private:
    static ATOM registerClass();

    void init(HWND parent, const char* title, const char* fileName);
    LRESULT procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
    BOOL onCreate(HWND hWnd, CREATESTRUCT* lpCreateStruct);
    void onSize(HWND hWnd, UINT state, int cx, int cy);
    BOOL onQuery(HWND hWnd);
    void onShow(HWND hWnd, BOOL fShow, UINT status);
    void onVScroll(HWND hWnd, HWND hCntrl, UINT code, int pos);
    void onPaint(HWND hWnd);
    void onDestroy(HWND hWnd);
    void onSysCommand(HWND hwnd, UINT cmd, int x, int y);

    void setScrollBar();
    void setTopLine();
};

#endif   // NOLOG

#endif /* LOGW_IMP_HPP */

/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.3  2001/07/02 15:23:18  greenius
 * Battle, Campaign, Combined game and Campaign Editor now compile and run... mostly
 *
 *----------------------------------------------------------------------
 */
