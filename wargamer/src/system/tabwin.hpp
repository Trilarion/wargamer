/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef TABWIN_HPP
#define TABWIN_HPP

#include "sysdll.h"
#include "ctab.hpp"
#include "wind.hpp"
#include "palwind.hpp"
#include "grtypes.hpp"
#include "timer.hpp"

class DrawDIB;
class DrawDIBDC;
class ImageLibrary;
class ResourceStrings;

class SYSTEM_DLL TabbedInfoWindow : public WindowBaseND {
	 HWND d_hParent;
	 WMTimer d_timer;
	 const DrawDIB* d_infoFillDib;
	 const DrawDIB* d_bkDib;
	 const DrawDIB* d_buttonFillDib;
	 const ImageLibrary* d_images;
	 HFONT d_tabFont;
	 CustomBorderInfo d_borderColors;

	 int d_nItems;
	 const char** d_tabText;
	 const ResourceStrings* d_resText;

	 static DrawDIBDC* s_dib;
	 static int s_instanceCount;

	 RECT d_dRect;     // display area rect

	 CustomDrawnTab d_cTab;

	 enum ID {
//		 TimerID = 50,
		 ID_Tab = 100,
		 ID_Close

	 };

//	 Boolean d_timerActive;

	 const int d_winCX;
	 const int d_winCY;
	 const int d_offsetX;
	 const int d_offsetY;
	 const int d_shadowWidth;

  public:

	 TabbedInfoWindow(HWND hParent, int nItems, const char** text,
		 const DrawDIB* infoFillDib, const DrawDIB* bkDib,
		 const DrawDIB* buttonFillDib, const ImageLibrary* il,
		 HFONT tabFont, CustomBorderInfo bc);

	 TabbedInfoWindow(HWND hParent, int nItems, const ResourceStrings& text,
		 const DrawDIB* infoFillDib, const DrawDIB* bkDib,
		 const DrawDIB* buttonFillDib, const ImageLibrary* il,
		 HFONT tabFont, CustomBorderInfo bc);

	 ~TabbedInfoWindow();

//	 void setFillDib(const DrawDIB* fillDib) { d_fillDib = fillDib; }
	 void run(const PixelPoint& p);
	 void destroy();
	 void hide();

	 virtual void onSelChange() = 0;
    virtual void onRefresh();
    virtual int getRefreshRate() const;

  protected:

	 DrawDIBDC* dib() const { return s_dib; }
	 int currentTab();
	 void fillStatic();
	 void forceRedraw();
	 const RECT& dRect() const { return d_dRect; }

  private:
	 LRESULT procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
	 void onCommand(HWND hwnd, int id, HWND hwndCtl, UINT codeNotify);
	 BOOL onCreate(HWND hWnd, CREATESTRUCT* lpCreateStruct);
	 BOOL onEraseBk(HWND hwnd, HDC hdc);
	 void onPaint(HWND hWnd);
	 void onDestroy(HWND hWnd);
	 LRESULT onNotify(HWND hWnd, int id, NMHDR* lpNMHDR);
	 UINT onNCHitTest(HWND hwnd, int x, int y);
	 void onMove(HWND hwnd, int x, int y);
	 void onTimer(HWND hwnd, UINT id) { onRefresh(); }

};



#endif
