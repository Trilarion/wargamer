/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1995, Steven Morle-Green, All Rights Reserved
 *----------------------------------------------------------------------
 *
 * Device Independant Bitmap support functions
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "dib.hpp"
#include "myassert.hpp"
#include "palette.hpp"
#include "misc.hpp"
#include "wmisc.hpp"
#include "trig.hpp"
#include "colours.hpp"
#include <memory>      // from STL Library
#include <stdio.h>         // For variable arguments

#define STATIC_INIT_DIB
#include "staticInit.h"


#if defined(DEBUG) && !defined(NOLOG)
#define DIB_LOG
#define DEBUG_DIBDC

#include "audit.hpp"

#ifdef DIB_LOG

class DIBAudit : public AuditLog<const RawDIB*>
{
    public:
       DIBAudit() : AuditLog<const RawDIB*>("DIB.log") { }
       ~DIBAudit() { showState(); }

       virtual void logItem(const RawDIB* dib)
       {
          char buffer[10];
          sprintf(buffer, "%p", dib);

          *d_log << buffer << " (" << dib->getWidth() << "," << dib->getHeight() << ")";
       }
};

static DIBAudit s_dibLog;

#endif   // DIB_LOG

#ifdef DEBUG_DIBDC

class DC_Audit : public AuditLog<HDC>
{
    public:
       DC_Audit() : AuditLog<HDC>("HDC.log") { }
       ~DC_Audit() { showState(); }

       virtual void logItem(HDC hdc)
       {
          char buffer[10];
          sprintf(buffer, "%p", hdc);

          // Would be nice to get more info about the DC
          // eg. call GetObject() to get DIBSection and get size from it.

          *d_log << buffer;
       }
};

static DC_Audit s_dcLog;

#endif   // DEBUG_DIBDC

#endif      // defined(DEBUG) && !defined(NOLOG)

/*
 * Some handy utility functions
 */

inline hasSamePalette(const RawDIB* dib1, const RawDIB* dib2)
{
   return (*dib1->getPalette() == *dib2->getPalette());
}

/*
 * RawDIB functions
 */

RawDIB::RawDIB() :
   d_dimension(0,0),
   d_bits(0),
   d_handle(NULL),
   d_palette(0),
   d_hasMask(False),
   d_mask(0),
   d_bitCount(8)           // Hard wired 256 color format
{
   d_palette = CPalette::defaultPalette();
}

#if 0
RawDIB::RawDIB(PixelDimension width, PixelDimension height) :
   d_bits(0),
   d_handle(NULL),
   d_binfo(0),
   d_hasMask(False),
   d_mask(0)
{
   makeBitmapInfo(width, height);
}
#endif

#if 0
RawDIB::RawDIB(const RawDIB& dib) :
   d_bits(dib.d_bits),
   d_handle(dib.d_handle),
   d_binfo(dib.d_binfo),
   d_hasMask(dib.d_hasMask),
   d_mask(dib.d_mask)
{
}
#endif

RawDIB::~RawDIB()
{
   // release();

   if(d_handle)
   {
      releaseDIBSection();
   }
   else
   {
      releaseMemBitmap();
   }

   d_palette = 0;       // Reference pointed palette

   ASSERT(d_bits == 0);
   ASSERT(d_handle == 0);
}


void RawDIB::makeMemBitmap(PixelDimension width, PixelDimension height)
{
   ASSERT(d_bits == 0);
   ASSERT(width > 0);
   ASSERT(height > 0);

   d_dimension = PixelPoint(width, height);
   PixelDimension storageWidth = getStorageWidth();
   d_bits = new ColorIndex[storageWidth * height];

#ifdef DIB_LOG
   s_dibLog.logNew(this);
#endif
}

void RawDIB::releaseMemBitmap()
{
   // ASSERT(d_bits);

   if (d_bits)
   {
      delete[] d_bits;
      d_bits = 0;
#ifdef DIB_LOG
      s_dibLog.logDelete(this);
#endif
   }
}

#ifdef _MSC_VER
   struct BitmapInfo
   {
      BITMAPINFOHEADER  bmiHeader;
      RGBQUAD           bmiColors[256];

      const BITMAPINFO* getBITMAPINFO() const
      {
         return reinterpret_cast<const BITMAPINFO*>(this);
      }
   };
#endif


void RawDIB::makeDIBSection(PixelDimension width, PixelDimension height)
{
   ASSERT(width > 0);
   ASSERT(height > 0);

   d_dimension = PixelPoint(width, height);

   // if( (d_binfo != 0) && (getWidth() == width) && (getHeight() == height))
   // if(d_handle && (getWidth() == width) && (getHeight() == height))
   // if(d_handle && (getWidth() == width) && (getHeight() == height))
   //    return;

   /*
    * release old bitmap
    */

   // release();
   if(d_handle != NULL)
   {
      releaseDIBSection();
   }

   ASSERT(d_handle == NULL);
   ASSERT(d_bits == 0);

   /*
    * Set up bitmapinfo
    */
#ifndef _MSC_VER  // Visual C++ can't use local classes as template types
   struct BitmapInfo
   {
      BITMAPINFOHEADER  bmiHeader;
      RGBQUAD           bmiColors[256];

      const BITMAPINFO* getBITMAPINFO() const
      {
         return reinterpret_cast<const BITMAPINFO*>(this);
      }
   };
#endif

   std::auto_ptr<BitmapInfo> binfo(new BitmapInfo);

   binfo->bmiHeader.biSize = sizeof(BITMAPINFOHEADER);
   binfo->bmiHeader.biWidth = width;
   binfo->bmiHeader.biHeight = -height;      // -ve to make it top down
   binfo->bmiHeader.biPlanes = 1;
   binfo->bmiHeader.biBitCount = 8;
   binfo->bmiHeader.biCompression = BI_RGB;
   binfo->bmiHeader.biSizeImage = 0;
   binfo->bmiHeader.biXPelsPerMeter = 0;
   binfo->bmiHeader.biYPelsPerMeter = 0;
   binfo->bmiHeader.biClrUsed = d_palette->getNColors();     // 0
   binfo->bmiHeader.biClrImportant = 0;

   // TODO: Sort out palettes
   /*
    * Get a d_handle
    */

   HDC hdc = GetDC(NULL);
   ASSERT(hdc != NULL);

#if 0
   /*
    * Set up identity table (0..256)
    */

   PUSHORT col = (PUSHORT) binfo->bmiColors;
   for(int i = 0; i < 256; i++)
      *col++ = (USHORT) i;

   HPALETTE oldPal = SelectPalette(hdc, Palette::get(), FALSE);
   RealizePalette(hdc);
   d_handle = CreateDIBSection(hdc, binfo.get()->getBITMAPINFO(), DIB_PAL_COLORS, (VOID**)&d_bits, NULL, 0);

#else
   /*
    * Copy Palette
    */


   const RGBQUAD* cols = d_palette->colors();
   for (int i = 0; i < d_palette->getNColors(); ++i)
   {
      binfo->bmiColors[i] = cols[i];
   }

   d_handle = CreateDIBSection(hdc, binfo.get()->getBITMAPINFO(), DIB_RGB_COLORS, (VOID**)&d_bits, NULL, 0);

#endif

#if 0
   SelectPalette(hdc, oldPal, FALSE);
#endif
   ReleaseDC(NULL, hdc);

   ASSERT(d_handle != NULL);
   ASSERT(d_bits != NULL);

#ifdef DIB_LOG
   s_dibLog.logNew(this);
#endif

}

void RawDIB::releaseDIBSection()
{
   // ASSERT(d_handle != NULL);

   if (d_handle)
   {
#ifdef DIB_LOG
      s_dibLog.logDelete(this);
#endif

      DeleteObject(d_handle);
      d_handle = NULL;
      d_bits = 0;    //lint !e672 ... possible memory leak.
   }
}

void RawDIB::resize(PixelDimension width, PixelDimension height)
{
   if ((getWidth() != width) || (getHeight() != height))
   {
      if (d_handle != NULL)
      {
         releaseDIBSection();
         makeDIBSection(width, height);
      }
      else
      {
         releaseMemBitmap();
         makeMemBitmap(width, height);
      }
   }
}

HBITMAP RawDIB::getHandle() const
{
   if (d_handle == NULL)
   {
      // Convert MemBitmap into a DIBSection

      // Log deletion because makeDIBSection logs a creation
#ifdef DIB_LOG
      s_dibLog.logDelete(this);
#endif
      // Need to be naughty and get a non-const pointer

      RawDIB* ncThis = const_cast<RawDIB*>(this);

      ASSERT(ncThis->d_bits);
      ColorIndex* oldBits = ncThis->d_bits;
      ncThis->d_bits = 0;
      ncThis->makeDIBSection(d_dimension.x(), d_dimension.y());
      ASSERT(ncThis->d_bits);
      memcpy(ncThis->d_bits, oldBits, getHeight() * getStorageWidth());
      delete[] oldBits;
   }

   ASSERT(d_handle != NULL);
   return d_handle;
}

// Set the DIB's palette to pal... no remapping.

void RawDIB::setPalette(const CPalette* pal)
{
   // TODO:
   //    If a DIB Section has been created then need to do something?

   ASSERT(d_handle == NULL);  // only work for MemBitmaps
   d_palette = pal;
}

// Remap DIB to the pallete pointed to by pal.

void RawDIB::remapTo(const CPalette* pal)
{
   // Probably only works with Memory DIBs
   // otherwise need to recreate DIBSection

   ASSERT(!d_handle);

   CPalette::RemapTablePtr imap = CPalette::makeRemapTable(d_palette, pal);

   ColorIndex* p = getBits();
   PixelDimension l = getStorageWidth() * getHeight();
   while(l--)
   {
      *p = (*imap)[*p];
      p++;
   }

   if(d_hasMask)
      d_mask = (*imap)[d_mask];

   d_palette = pal;
}


#if 0 // TODO
void RawDIB::remap(const RGBQUAD* col)
{
   /*
    * Make remap table
    */

#ifdef DEBUG_PALETTE
      char buffer[100];
      OutputDebugString("Remap Table:\n");
#endif

   UCHAR imap[256];

   HPALETTE sysPal = Palette::get();

   for(int i = 0; i < 256; i++)
   {
      imap[i] = (UCHAR) GetNearestPaletteIndex(sysPal, RGB(col->rgbRed, col->rgbGreen, col->rgbBlue));

#ifdef DEBUG_PALETTE
      wsprintf(buffer, "%d: %d (%d,%d,%d)\n",
         i, imap[i],
         (int) col->rgbRed,
         (int) col->rgbGreen,
         (int) col->rgbBlue);
      OutputDebugString(buffer);
#endif

      col++;
   }

   /*
    * Remap bits
    */

   ColorIndex* p = getBits();
   PixelDimension l = getStorageWidth() * getHeight();
   while(l--)
   {
      *p = imap[*p];
      p++;
   }

   if(d_hasMask)
      d_mask = imap[d_mask];
}
#endif

#if 0
/*
 * To make it top down, set height negative.
 */

void RawDIB::makeBitmapInfo(UINT width, UINT height)
{
   ASSERT(width > 0);
   ASSERT(height > 0);

   if( (d_binfo != 0) && (getWidth() == width) && (getHeight() == height))
      return;

   height = -height;

   /*
    * release old bitmap
    */

   // release();
   if(d_handle != NULL)
   {
#ifdef DIB_LOG
        s_dibLog.logDelete(this);
#endif

      DeleteObject(d_handle);
      d_handle = NULL;
      d_bits = 0;
   }

   /*
    * Set up bitmapinfo
    */

   if(d_binfo == 0)
   {
      PixelDimension storage = sizeof(BITMAPINFOHEADER) + 256 * sizeof(RGBQUAD);
      d_binfo = (BITMAPINFO*) GlobalAllocPtr(GMEM_MOVEABLE, storage);
   }

   ASSERT(d_binfo != 0);

   if(!d_binfo)
      return;

   d_binfo->bmiHeader.biSize = sizeof(BITMAPINFOHEADER);
   d_binfo->bmiHeader.biWidth = width;
   d_binfo->bmiHeader.biHeight = height;
   d_binfo->bmiHeader.biPlanes = 1;
   d_binfo->bmiHeader.biBitCount = 8;
   d_binfo->bmiHeader.biCompression = BI_RGB;
   d_binfo->bmiHeader.biSizeImage = 0;
   d_binfo->bmiHeader.biXPelsPerMeter = 0;
   d_binfo->bmiHeader.biYPelsPerMeter = 0;
   d_binfo->bmiHeader.biClrUsed = 0;
   d_binfo->bmiHeader.biClrImportant = 0;

   /*
    * Set up identity table (0..256)
    */

   PUSHORT col = (PUSHORT) d_binfo->bmiColors;
   for(int i = 0; i < 256; i++)
      *col++ = (USHORT) i;

   /*
    * Get a d_handle
    */

   HDC hdc = GetDC(NULL);
   ASSERT(hdc != NULL);
   HPALETTE oldPal = SelectPalette(hdc, Palette::get(), FALSE);
   RealizePalette(hdc);
   d_handle = CreateDIBSection(hdc, d_binfo, DIB_PAL_COLORS, (VOID**)&d_bits, NULL, 0);
   SelectPalette(hdc, oldPal, FALSE);
   ReleaseDC(NULL, hdc);

   ASSERT(d_handle != NULL);
   ASSERT(d_bits != NULL);

#ifdef DIB_LOG
    s_dibLog.logNew(this);
#endif

}

#endif      // 0

/*======================================================
 * DIB Functions
 */

/*
 * Constructor from RawDIB Simply copies data fields
 */

/*
 * DIB delete, actually frees up resources
 */

// DIB::~DIB()
// {
//    release();
// }


/*=====================================================
 * DrawDIB Functions
 */

/*
 * Do some clipping
 */

// const ColorIndex* DrawDIB::getAddress(PixelDimension x, PixelDimension y) const
// {
//    return RawDIB::getAddress(x, y);
// }
//
// /*
//  * Get pixel address of a given coordinate
//  */
//
// ColorIndex* DrawDIB::getAddress(PixelDimension x, PixelDimension y)
// {
//    ASSERT(x >= 0);
//    ASSERT(x < getWidth());
//    ASSERT(y >= 0);
//    ASSERT(y < getHeight());
//
//    /*
//     * Tell GDI to finish anything it's still doing
//     * It may be slow to put it here, in which case anything that
//     * does things to bitmaps should call GdiFlush() and remove
//     * it from here.
//     */
//
//    GdiFlush();
//
//    return getBits() + x + y * getStorageWidth();
// }


ColorIndex DrawDIB::getColor(COLORREF cRef)
{
   return (ColorIndex) GetNearestPaletteIndex(Palette::get(), cRef);
}

ColorIndex DrawDIB::getColor(const LPRGBQUAD pal, const UBYTE r, const UBYTE g, const UBYTE b, const int nColors)
{
  PixelDimension bestValue = 256*256*256;
  ColorIndex bestID = 0;

  for(int i = 0; i < nColors; i++)
  {
    int dr = r - pal[i].rgbRed;
    int dg = g - pal[i].rgbGreen;
    int db = b - pal[i].rgbBlue;

    PixelDimension value = (dr*dr) + (dg*dg) + (db*db);

    if(value < bestValue)
    {
      bestValue = value;
      bestID = ColorIndex(i);
    }
  }

  return bestID;
}


void DrawDIB::darken(DIB* src, PixelDimension destX, PixelDimension destY, PixelDimension w, PixelDimension h, PixelDimension srcX, PixelDimension srcY, int percent)
{
   CPalette::RemapTablePtr remapTable = getPalette()->getDarkRemapTable(percent);
   remapDIB(src, remapTable, destX, destY, w, h, srcX, srcY);
}

void DrawDIB::lighten(DIB* src, PixelDimension destX, PixelDimension destY, PixelDimension w, PixelDimension h, PixelDimension srcX, PixelDimension srcY, int percent)
{
   CPalette::RemapTablePtr remapTable = getPalette()->getLightRemapTable(percent);
   remapDIB(src, remapTable, destX, destY, w, h, srcX, srcY);
}


void DrawDIB::lightenRectangle(PixelDimension x, PixelDimension y, PixelDimension w, PixelDimension h, int percent)
{
   CPalette::RemapTablePtr remapTable = getPalette()->getLightRemapTable(percent);
   remapRectangle(remapTable, x, y, w, h);
}

void DrawDIB::brightenRectangle(PixelDimension x, PixelDimension y, PixelDimension w, PixelDimension h, int percent)
{
   CPalette::RemapTablePtr remapTable = getPalette()->getBrightRemapTable(percent);
   remapRectangle(remapTable, x, y, w, h);
}

void DrawDIB::darkenRectangle(PixelDimension x, PixelDimension y, PixelDimension w, PixelDimension h, int percent)
{
   CPalette::RemapTablePtr remapTable = getPalette()->getDarkRemapTable(percent);
   remapRectangle(remapTable, x, y, w, h);
}

void DrawDIB::remapDIB(const DIB* maskDIB, const ColorRemapTable* remap, PixelDimension destX, PixelDimension destY, PixelDimension w, PixelDimension h, PixelDimension srcX, PixelDimension srcY)
{

   PixelDimension x = destX;
   PixelDimension y = destY;
   if(clip(x, y, w, h))
   {
      srcX += x - destX;   // adjust if any clipping
      srcY += y - destY;

      const ColorIndex* srcLine = maskDIB->getAddress(srcX, srcY);
      ColorIndex* destLine = getAddress(x, y);

      PixelDimension height = h;
      while(height--)
      {
         ColorIndex* dest = destLine;
         const ColorIndex* src = srcLine;

         PixelDimension width = w;
         while(width--)
         {

            // if(!hasMask() || *src != getMask())
            if(!maskDIB->hasMask() || *src != maskDIB->getMask())
              *dest = (*remap)[*dest];

            dest++;
            src++;
         }

         destLine += getStorageWidth();
         srcLine += maskDIB->getStorageWidth();
      }
   }

}

void DrawDIB::remapRectangle(const ColorRemapTable* remapTable, PixelDimension x, PixelDimension y, PixelDimension w, PixelDimension h)
{

   if(clip(x, y, w, h))
   {

      ColorIndex* line = getAddress(x, y);

      PixelDimension height = h;
      while(height--)
      {
         ColorIndex* dest = line;

         PixelDimension width = w;
         while(width--)
         {
            *dest = (*remapTable)[*dest];

            dest++;
         }

         line += getStorageWidth();
      }
   }

}

PixelRect DIB::clip(const PixelRect& rect) const
{
   return PixelRect::intersect(getRect(), rect);
}

BOOL DIB::clip(PixelDimension& x, PixelDimension& y, PixelDimension& width, PixelDimension& height) const
{
   PixelRect r = PixelRect::intersect(getRect(), PixelRect(x,y,x+width,y+height));

   if(r.width() > 0)
   {
      x = r.left();
      y = r.top();
      width = r.width();
      height = r.height();
      return TRUE;

   }
   else
      return FALSE;

#if 0

   RECT r1;

   r1.left = x;
   r1.right = x + width;
   r1.top = y;
   r1.bottom = y + height;

   RECT r2;

   r2.left = 0;
   r2.right = getWidth();
   r2.top = 0;
   r2.bottom = getHeight();

   RECT r3;

   if(IntersectRect(&r3, &r1, &r2))
   {
      x = r3.left;
      y = r3.top;
      width = r3.right - r3.left;
      height = r3.bottom - r3.top;

      /*
       * Check that values are indeed clipped!
       */

      ASSERT(x >= 0);
      ASSERT((x + width) <= getWidth());
      ASSERT(y >= 0);
      ASSERT((y + height) <= getHeight());

      return TRUE;
   }
   else
      return FALSE;
#endif
}

/*
 * Rather pathetic plot pixel routine for anything that
 * wants to use it.
 *
 * It will be useful for prototyping graphics routines because at least
 * it is safe and won't write all over memory if the coordinates are bad.
 */

void
DrawDIB::plot(PixelDimension x, PixelDimension y, ColorIndex color)
{
   if( (x >= 0) && (x < getWidth()) && (y >= 0) && (y < getHeight()))
   {
      ColorIndex* dest =  getBits() + x + y * getStorageWidth();
      *dest = color;
   }
}


/*
 * Draw a Horizontal line in given color
 *
 * WARNING: No clipping is done!
 */

void DrawDIB::hLineFast(PixelDimension x, PixelDimension y, PixelDimension length, ColorIndex color)
{
   ASSERT(x >= 0);
   ASSERT((x + length) <= getWidth());
   ASSERT(y >= 0);
   ASSERT(y < getHeight());
   ASSERT(length > 0);

   ColorIndex* pb = getAddress(x,y);
   memset(pb, color, length);
}

/*
 * Draw a vertical line in given color
 *
 * WARNING: No clipping is done!
 */

void DrawDIB::vLineFast(PixelDimension x, PixelDimension y, PixelDimension length, ColorIndex color)
{
   ASSERT(x >= 0);
   ASSERT(x < getWidth());
   ASSERT(y >= 0);
   ASSERT((y + length) <= getHeight());
   ASSERT(length > 0);

   ColorIndex* pb = getAddress(x,y);

   while(length--)
   {
      *pb = color;
      pb += getStorageWidth();
   }
}

/*
 * Clipped versions of hLine and vLine
 */

void
DrawDIB::hLine(PixelDimension x, PixelDimension y, PixelDimension length, ColorIndex color)
{
   PixelDimension height = 1;

   ASSERT(length > 0);

   if(clip(x, y, length, height))
      hLineFast(x, y, length, color);
}

void
DrawDIB::vLine(PixelDimension x, PixelDimension y, PixelDimension length, ColorIndex color)
{
   PixelDimension width = 1;

   ASSERT(length > 0);

   if(clip( x, y, width, length))
      vLineFast(x, y, length, color);
}

/*
 * Draw a filled rectangle
 *
 * WARNING: No clipping is done!
 *
 */

void
DrawDIB::rect(PixelDimension x, PixelDimension y, PixelDimension width, PixelDimension height, ColorIndex color)
{
   if(clip(x, y, width, height))
   {
      ASSERT(x >= 0);
      ASSERT((x + width) <= getWidth());
      ASSERT(y >= 0);
      ASSERT((y + height) <= getHeight());

      ColorIndex* pb = getAddress(x,y);

      while(height--)
      {
         memset(pb, color, width);
         pb += getStorageWidth();
      }
   }
}

// fill in a rect with a bitmap pattern

void DrawDIB::rect(PixelDimension x, PixelDimension y, PixelDimension width, PixelDimension height, const DIB* srcDib, PixelDimension srcX, PixelDimension srcY)
{
   PixelDimension realX = x;
   PixelDimension realY = y;

   if(clip(x, y, width, height))
   {
      /*
       * get some useful values
       */

      ColorIndex* destPtr = getAddress(x, y);
      size_t destStorageWidth = getStorageWidth();

      size_t srcStorageWidth = srcDib->getStorageWidth();
      PixelDimension srcHeight = srcDib->getHeight();
      PixelDimension srcWidth = srcDib->getWidth();

      ASSERT(srcWidth > 0);
      ASSERT(srcHeight > 0);

      // Adjust source coordinates for clipping

      srcX += x - realX;
      srcY += y - realY;

      // Adjust source coordinates to fit into source DIB

      if(srcX < 0)
         srcX += srcWidth * (1 + (-srcX-1) / srcWidth);
      else
         srcX %= srcWidth;

      if(srcY < 0)
         srcY += srcHeight * (1 + (-srcY-1) / srcHeight);
      else
         srcY %= srcHeight;

      ASSERT(srcX >= 0);
      ASSERT(srcY >= 0);
      ASSERT(srcX < srcWidth);
      ASSERT(srcY < srcHeight);

      const ColorIndex* srcPtr = srcDib->getAddress(0, srcY);
      const ColorIndex* srcTop = srcDib->getAddress(0, 0);

      PixelDimension sX = srcX;

      if(hasSamePalette(srcDib, this))
      {
         while(height--)
         {
            ColorIndex* dPtr = destPtr;

            sX = srcX;

            PixelDimension xCount = width;
            while(xCount--)
            {
               *dPtr = srcPtr[sX];
               ++dPtr;
               ++sX;
               if(sX >= srcWidth)
               {
                  sX = 0;
               }
            }

            srcPtr += srcStorageWidth;
            destPtr += destStorageWidth;

            ++srcY;
            if(srcY >= srcHeight)
            {
               srcY = 0;
               srcPtr = srcTop;
            }
         }
      }
      else
      {
         CPalette::RemapTablePtr remapPtr = CPalette::makeRemapTable(srcDib->getPalette(), getPalette());
         const ColorRemapTable* remap = remapPtr;

         while(height--)
         {
            ColorIndex* dPtr = destPtr;

            sX = srcX;

            PixelDimension xCount = width;
            while(xCount--)
            {
               *dPtr = (*remap)[srcPtr[sX]];
               ++dPtr;
               ++sX;
               if(sX >= srcWidth)
               {
                  sX = 0;
               }
            }

            srcPtr += srcStorageWidth;
            destPtr += destStorageWidth;

            ++srcY;
            if(srcY >= srcHeight)
            {
               srcY = 0;
               srcPtr = srcTop;
            }
         }
      }
   }
}

void
DrawDIB::fill(const DrawDIB* srcDib)
{
  rect(0, 0, getWidth(), getHeight(), srcDib);
}

void
DrawDIB::fill(ColorIndex color)
{
   memset(getBits(), color, getHeight() * getStorageWidth());
}

/*
 * Draw a rectangle
 */

void
DrawDIB::frame(PixelDimension x, PixelDimension y, PixelDimension width, PixelDimension height, ColorIndex color)
{
   PixelDimension oldX = x;
   PixelDimension oldY = y;
   // PixelDimension oldW = width;
   // PixelDimension oldH = height;

   /*
    * Clip it and do the job
    */

   if(clip(x, y, width, height))
   {
      if(oldY == y)
         hLineFast(       x,          y,  width, color);
      if( (y + height) <= getHeight())
         hLineFast(       x, y+height-1,  width, color);
      if(oldX == x)
         vLineFast(       x,          y, height, color);
      if( (x + width) <= getWidth())
         vLineFast(x+width-1,            y, height, color);
   }
}

/*
 * Draw a rectangle, but do top/left in 1 color and bottom/right in another
 */

void DrawDIB::frame(PixelDimension x, PixelDimension y, PixelDimension width, PixelDimension height, ColorIndex color1, ColorIndex color2)
{
   PixelDimension oldX = x;
   PixelDimension oldY = y;
   // PixelDimension oldW = width;
   // PixelDimension oldH = height;

   /*
    * Clip it and do the job
    */

   if(clip(x, y, width, height))
   {
      if(oldY == y)
         hLineFast(       x,          y,  width, color1);
      if( (y + height) <= getHeight())
         hLineFast(       x, y+height-1,  width, color2);
      if(oldX == x)
         vLineFast(       x,          y, height, color1);
      if( (x + width) <= getWidth())
         vLineFast(x+width-1,            y, height, color2);
   }
}

/*
 * Draw an outline circle
 */

void
DrawDIB::circle(PixelDimension x, PixelDimension y, PixelDimension radius, ColorIndex color)
{
   PixelDimension x1 = x;
   PixelDimension y1 = y;
   PixelDimension w1 = radius;
   PixelDimension h1 = radius;

   if(clip(x1, y1, w1, h1))
   {
      if((x1 != x) || (y1 != y) || (w1 != radius) || (h1 != radius))
      {
         frame(x - radius, y - radius, radius*2, radius*2, color);
         return;
      }

      /*
       * Proper Circle Routine
       */

      PixelDimension dx = radius;
      PixelDimension dy = 0;

      PixelDimension value = dx/2;

      /*
       * Fill Middle Pixels in in advance to save time
       */

      plot(x - dx, y, color);
      plot(x + dx, y, color);
      plot(x, y + dx, color);
      plot(x, y - dx, color);

      while(dx > dy)    // While slope < 45 degrees
      {
         value += dy;
         while(value >= dx)
         {
            value -= dx;
            dx--;
         }

         dy++;

         if(dx < dy)
            break;

         /*
          * Fill in pixels
          */

         plot(x - dx, y + dy, color);
         plot(x + dx, y + dy, color);
         plot(x - dx, y - dy, color);
         plot(x + dx, y - dy, color);
         plot(x - dy, y + dx, color);
         plot(x + dy, y + dx, color);
         plot(x - dy, y - dx, color);
         plot(x + dy, y - dx, color);


      }
   }
}

/*
 * Draw an outline circle
 */

void DrawDIB::circle(PixelDimension x, PixelDimension y, PixelDimension radius, ColorIndex color1, ColorIndex color2)
{
   PixelDimension x1 = x;
   PixelDimension y1 = y;
   PixelDimension w1 = radius;
   PixelDimension h1 = radius;

   if(clip(x1, y1, w1, h1))
   {
      if((x1 != x) || (y1 != y) || (w1 != radius) || (h1 != radius))
      {
         frame(x - radius, y - radius, radius*2, radius*2, color1, color2);
         return;
      }

      /*
       * Proper Circle Routine
       */

      PixelDimension dx = radius;
      PixelDimension dy = 0;

      PixelDimension value = dx/2;

      /*
       * Fill Middle Pixels in in advance to save time
       */

      plot(x - dx, y, color1);
      plot(x + dx, y, color2);
      plot(x, y + dx, color2);
      plot(x, y - dx, color1);

      while(dx > dy)    // While slope < 45 degrees
      {
         value += dy;
         while(value >= dx)
         {
            value -= dx;
            dx--;
         }

         dy++;

         if(dx < dy)
            break;

         /*
          * Fill in pixels
          */

         plot(x - dx, y + dy, color1);
         plot(x + dx, y + dy, color2);
         plot(x - dx, y - dy, color1);
         plot(x + dx, y - dy, color2);
         plot(x - dy, y + dx, color2);
         plot(x + dy, y + dx, color2);
         plot(x - dy, y - dx, color1);
         plot(x + dy, y - dx, color1);


      }
   }
}


/*
 * Draw a filled circle
 *
 * Diameter is (radius * 2) + 1, because this means it is more
 * easily centred on the given coordinates.
 *
 * e.g.          ***
 *  r=2         *****
 *              **+**
 *              *****
 *               ***
 */

void
DrawDIB::disc(PixelDimension x, PixelDimension y, PixelDimension radius, ColorIndex color)
{
   /*
    * For debugging when we are not bothered about clipping
    * call the frame function if it needs to be clipped
    */

   PixelDimension x1 = x;
   PixelDimension y1 = y;
   PixelDimension w1 = radius;
   PixelDimension h1 = radius;

   if(clip(x1, y1, w1, h1))
   {
      if((x1 != x) || (y1 != y) || (w1 != radius) || (h1 != radius))
      {
         rect(x - radius, y - radius, radius*2, radius*2, color);
         return;
      }

      /*
       * This is the proper circle routine
       * To get it up and running it will be calling hLine
       * Later on we'll copy the scan line filling here
       *
       * Use a bresenam circle algorithm
       * This works by calculating values for a 45 degree segment
       * and then drawing 4 scan lines for each of these based
       * on knowing that a circle is symetrical
       *
       * Basic algorithm is:
       *    For each scan line:
       *       y := y + 1
       *       x := x - y/x
       *
       * y/x is always less than 1 for angles below 45 degrees
       * so a bresenham style remainder is used.
       */

      PixelDimension dx = radius;
      PixelDimension dy = 0;

      PixelDimension value = dx/2;

      /*
       * Fill Middle line in in advance to save time
       */

      hLine(x - dx, y, dx+dx+1, color);

      while(dx > 0)     // While slope < 90 degrees
      {
         value += dy;
         while(value >= dx)
         {
            value -= dx;
            dx--;
            if(dx < 0)
               break;
         }
         if(dx < 0)
            break;

         dy++;


         /*
          * Fill in a scan line
          */

         hLine(x - dx, y + dy, dx+dx+1, color);
         hLine(x - dx, y - dy, dx+dx+1, color);


      }
   }
}

/*
 * Straight line between (and including) 2 points
 */

void
DrawDIB::line(PixelDimension x1, PixelDimension y1, PixelDimension x2, PixelDimension y2, ColorIndex color)
{


   /*
    * Pull out special cases of horizontal or vertical
    */

   if(x1 == x2)
   {
      if(y1 > y2)
         qswap(y1, y2);
      vLine(x1, y1, y2-y1+1, color);
   }
   else if(y1 == y2)
   {
      if(x1 > x2)
         qswap(x1, x2);
      hLine(x1, y1, x2-x1+1, color);
   }
   else
   {
      /*
       * The real thing
       */


      if(clipLine(x1, y1, x2, y2))
      {

         // For now just put a dot at each end

         // plot(x1, y1, color);
         // plot(x2, y2, color);

         /*
          * Work out Bresenham terms
          */

         PixelDimension dx = x2 - x1;
         PixelDimension dy = y2 - y1;

         PixelDimension adx = (dx < 0) ? -dx : +dx;
         PixelDimension ady = (dy < 0) ? -dy : +dy;

         /*
          * Which is the major axis?
          */

         if(adx > ady)
         {
            // X is the main axis

            // Make sure we are going from left to right

            if(dx < 0)
            {
               // swap(p1, p2);
               std::swap(x1, x2);
               std::swap(y1, y2);
               dy = -dy;
            }

            ColorIndex* ad = getAddress(x1,y1);

            *ad = color;

            if(x1 != x2)
            {
               PixelDimension value = 0x8000;                      // Initial Bresenham term
               PixelDimension add = (ady * 0x10000) / adx;            // Amount to add
               PixelDimension ddy = (dy < 0) ? -getStorageWidth() : +getStorageWidth();    // Line offset

               while(x1++ < x2)
               {
                  value += add;
                  if(value >= 0x10000)
                  {
                     ad += ddy;
                     value -= 0x10000;
                  }
                  ad++;

                  *ad = color;
               }
            }
         }
         else
         {
            // Y is the main axis

            // Make sure we are going from Top to Bottom

            if(dy < 0)
            {
               std::swap(x1,x2);
               std::swap(y1,y2);
               // Point tPoint = p1;
               // p1 = p2;
               // p2 = tPoint;

               dx = -dx;
            }

            ColorIndex* ad = getAddress(x1,y1);

            *ad = color;

            if(y1 != y2)
            {
               PixelDimension value = 0x8000;                      // Initial Bresenham term
               PixelDimension add = (adx * 0x10000) / ady;            // Amount to add
               PixelDimension ddx = (dx < 0) ? -1 : +1;               // Offset to move left/right

               while(y1++ < y2)
               {
                  value += add;
                  if(value >= 0x10000)
                  {
                     ad += ddx;
                     value -= 0x10000;
                  }
                  ad += getStorageWidth();

                  *ad = color;
               }
            }
         }
      }
   }

}

/*
 *  GDI version of line(). allows variable width lines
 */

void
DrawDIBDC::line(PixelDimension x1, PixelDimension y1, PixelDimension x2, PixelDimension y2, ColorIndex color, PixelDimension width)
{
   saveDC();

   SelectObject(d_hdc, getHandle());

   HPEN hNewPen = CreatePen(PS_SOLID, width, (COLORREF)color);
   HPEN hOldPen = (HPEN)SelectObject(d_hdc, hNewPen);

   MoveToEx(d_hdc, x1, y1, NULL);
   LineTo(d_hdc, x2, y2);

   restoreDC();

   SelectObject(d_hdc, hOldPen);
   DeleteObject(hNewPen);

}


/*
 * Line Clipping
 */


static PixelDimension clipEdge(PixelDimension edge, PixelDimension x1, PixelDimension y1, PixelDimension x2, PixelDimension y2)
{
   PixelDimension leftX;
   PixelDimension leftY;
   PixelDimension rightX;
   PixelDimension rightY;

   if(x1 > x2)
   {
      leftX = x2;
      leftY = y2;
      rightX = x1;
      rightY = y1;
   }
   else
   {
      leftX = x1;
      leftY = y1;
      rightX = x2;
      rightY = y2;
   }

   while( (leftX != edge) && (leftY != rightY))
   {
      // Point mid = left + ((right - left + Point(1,1)) >> 1);
      PixelDimension midX;
      PixelDimension midY;

      midX = leftX + ((rightX - leftX + 1) >> 1);
      midY = leftY + ((rightY - leftY + 1) >> 1);

      if(midX <= edge)
      {
         leftX = midX;
         leftY = midY;
      }
      else
      {
         rightX = midX;
         rightY = midY;
      }

   }

   return leftY;
}

/*
 * Clip a line to a region's boundary
 * return True if anything left on screen
 *        False if nothing left
 *
 * Copy CohenSutherland (see Foley/VanDam/etc pg. 116)
 *
 * Note that points are relative to window, so are clipped
 * to (0,0,w,h) rather than (x,y,maxX,maxY)
 */

#define CLIP_TOP     1
#define CLIP_BOTTOM  2
#define CLIP_LEFT    4
#define CLIP_RIGHT   8

inline UBYTE DrawDIB::getOutCode(PixelDimension x, PixelDimension y)
{
   UBYTE value = 0;

   if(y >= getHeight())
      value = CLIP_TOP;
   else if(y < 0)
      value = CLIP_BOTTOM;

   if(x >= getWidth())
      value |= CLIP_RIGHT;
   else if(x < 0)
      value |= CLIP_LEFT;

   return value;
}

Boolean DrawDIB::clipLine(PixelDimension& x1, PixelDimension& y1, PixelDimension& x2, PixelDimension& y2)
{
#ifdef DEBUG_CLIP
   debugLog("Clip(%ld,%ld -> %ld,%ld)\n",
      x1, y1, x2, y2);
#endif

   for(;;)
   {
      UBYTE outcode0 = getOutCode(x1, y1);
      UBYTE outcode1 = getOutCode(x2, y2);

      /*
       * Both points on screen if outcodes are both 0
       */

      if( (outcode0 | outcode1) == 0)
      {
#ifdef DEBUG_CLIP
         debugLog("Clip Passed: %ld,%ld -> %ld,%ld\n",
            x1, y1, x2, y2);
#endif
         return True;
      }

      /*
       * Points in same section, so it is all clipped!
       */

      if( (outcode0 & outcode1) != 0)
      {
#ifdef DEBUG_CLIP
         debugLog("Clip Failed: %ld,%ld -> %ld,%ld\n",
            x1, y1, x2, y2);
#endif
         return False;
      }

      /*
       * Find one of the points outside the rectangle
       */

      UBYTE outCode;

      PixelDimension* x;
      PixelDimension* y;

      if(outcode0)
      {
         outCode = outcode0;
         x = &x1;
         y = &y1;
      }
      else
      {
         outCode = outcode1;
         x = &x2;
         y = &y2;
      }

      if(outCode & CLIP_TOP)
      {
         *x = clipEdge(getHeight() - 1, y1,x1, y2,x2);
         *y = getHeight() - 1;
      }
      else if(outCode & CLIP_BOTTOM)
      {
         *x = clipEdge(0, y1,x1, y2,x2);
         *y = 0;
      }
      else if(outCode & CLIP_RIGHT)
      {
         *y = clipEdge(getWidth() - 1, x1,y1, x2,y2);
         *x = getWidth() - 1;
      }
      else if(outCode & CLIP_LEFT)
      {
         *y = clipEdge(0, x1,y1, x2,y2);
         *x = 0;
      }
   }  // Loop again with clipped segment
}


void DrawDIB::blitFrom(DIB* from)
{
   ASSERT(this != 0);
   ASSERT(from != 0);
   ASSERT(getWidth() == from->getWidth());
   ASSERT(getHeight() == from->getHeight());

   if (hasSamePalette(this, from))
   {
      ColorIndex* dest = getBits();
      ColorIndex* src = from->getBits();

      ASSERT(src != 0);
      ASSERT(dest != 0);

      memcpy(dest, src, getStorageWidth() * getHeight());
   }
   else
   {
      blit(from, 0, 0);
   }
}

class WriteMaskNoRemap
{
   public:
      WriteMaskNoRemap(ColourIndex mask) : d_mask(mask) { }
      void operator()(UBYTE* dest, const UBYTE* src, ULONG w)
      {
	      while(w--)
	      {
		      if(*src != d_mask)
			      *dest = *src;
		      ++dest;
		      ++src;
	      }
      }

   private:
      ColourIndex d_mask;
};


class WriteMaskRemap
{
   public:
      WriteMaskRemap(ColorIndex mask, const CPalette::RemapTablePtr& remap) : d_mask(mask), d_remap(remap) { }
      void operator()(UBYTE* dest, const UBYTE* src, ULONG w)
      {
	      while(w--)
	      {
		      if(*src != d_mask)
			      *dest = (*d_remap)[*src];
		      ++dest;
		      ++src;
	      }
      }

   private:
      ColourIndex d_mask;
      // const CPalette::RemapTablePtr& d_remap;
      const ColorRemapTable* d_remap;
};


class WriteNoMaskNoRemap
{
   public:
      WriteNoMaskNoRemap() { }
      void operator()(UBYTE* dest, const UBYTE* src, ULONG w)
      {
         memcpy(dest, src, w);
      }
};

class WriteNoMaskRemap
{
   public:
      WriteNoMaskRemap(const CPalette::RemapTablePtr& remap) : d_remap(remap) { }
      void operator()(UBYTE* dest, const UBYTE* src, ULONG w)
      {
	      while(w--)
	      {
 		      *dest = (*d_remap)[*src];
		      ++dest;
		      ++src;
	      }
      }

   private:
      // const CPalette::RemapTablePtr& d_remap;
      const ColorRemapTable* d_remap;
};

template<class WriteFunction>
inline void doBlit(DrawDIB* destDIB,
   PixelDimension destX, PixelDimension destY,
   PixelDimension w, PixelDimension h,
   const DIB* srcDIB,
   PixelDimension srcX, PixelDimension srcY,
   WriteFunction writeLine)
{
   PixelDimension x = destX;
   PixelDimension y = destY;
   if(destDIB->clip(x, y, w, h))
   {
      srcX += x - destX;   // adjust if any clipping
      srcY += y - destY;

      // TODO: Clip src co-ordinates as well!

      ASSERT(srcX >= 0);
      ASSERT((srcX + w) <= srcDIB->getWidth());
      ASSERT(srcY >= 0);
      ASSERT((srcY + h) <= srcDIB->getHeight());

      ASSERT(x >= 0);
      ASSERT((x + w) <= destDIB->getWidth());
      ASSERT(y >= 0);
      ASSERT((y + h) <= destDIB->getHeight());


      const ColorIndex* srcLine = srcDIB->getAddress(srcX, srcY);
      ColorIndex* destLine = destDIB->getAddress(x, y);

      while(h--)
      {
         writeLine(destLine, srcLine, w);
         srcLine += srcDIB->getStorageWidth();
         destLine += destDIB->getStorageWidth();
      }
   }
}

void DrawDIB::blit(PixelDimension destX, PixelDimension destY, PixelDimension w, PixelDimension h, const DIB* srcDIB, PixelDimension srcX, PixelDimension srcY)
{
   PalettePtr srcPal = srcDIB->getPalette();
   PalettePtr destPal = getPalette();

   if (*srcPal == *destPal)
   {
      if(srcDIB->hasMask())
         doBlit(this, destX, destY, w, h, srcDIB, srcX, srcY, WriteMaskNoRemap(srcDIB->getMask()));
      else
         doBlit(this, destX, destY, w, h, srcDIB, srcX, srcY, WriteNoMaskNoRemap());
   }
   else
   {
      // Remap
      CPalette::RemapTablePtr remapTablePtr = CPalette::makeRemapTable(srcPal, destPal);
      if(srcDIB->hasMask())
         doBlit(this, destX, destY, w, h, srcDIB, srcX, srcY, WriteMaskRemap(srcDIB->getMask(), remapTablePtr));
      else
         doBlit(this, destX, destY, w, h, srcDIB, srcX, srcY, WriteNoMaskRemap(remapTablePtr));
   }
}

/*
 *  draw a reversed(right to left) image
 */

class FlipWriteMaskNoRemap
{
   public:
      FlipWriteMaskNoRemap(ColourIndex mask) : d_mask(mask) { }
      void operator()(UBYTE* dest, const UBYTE* src, ULONG w)
      {
         src += w;
	      while(w--)
	      {
            --src;
		      if(*src != d_mask)
			      *dest = *src;
		      ++dest;
	      }
      }

   private:
      ColourIndex d_mask;
};


class FlipWriteMaskRemap
{
   public:
      FlipWriteMaskRemap(ColorIndex mask, const CPalette::RemapTablePtr& remap) : d_mask(mask), d_remap(remap) { }
      void operator()(UBYTE* dest, const UBYTE* src, ULONG w)
      {
         src += w;
	      while(w--)
	      {
            --src;
		      if(*src != d_mask)
			      *dest = (*d_remap)[*src];
		      ++dest;
	      }
      }

   private:
      ColourIndex d_mask;
      const ColorRemapTable* d_remap;
      // const CPalette::RemapTablePtr& d_remap;
};


class FlipWriteNoMaskNoRemap
{
   public:
      FlipWriteNoMaskNoRemap() { }
      void operator()(UBYTE* dest, const UBYTE* src, ULONG w)
      {
         src += w;
	      while(w--)
	      {
            --src;
		      *dest = *src;
		      ++dest;
	      }
      }
};

class FlipWriteNoMaskRemap
{
   public:
      FlipWriteNoMaskRemap(const CPalette::RemapTablePtr& remap) : d_remap(remap) { }
      void operator()(UBYTE* dest, const UBYTE* src, ULONG w)
      {
         src += w;
	      while(w--)
	      {
            --src;
 		      *dest = (*d_remap)[*src];
		      ++dest;
	      }
      }

   private:
      // const CPalette::RemapTablePtr& d_remap;
      const ColorRemapTable* d_remap;
};

void DrawDIB::flipBlit(PixelDimension destX, PixelDimension destY, PixelDimension w, PixelDimension h, const DIB* srcDIB, PixelDimension srcX, PixelDimension srcY)
{
   PalettePtr srcPal = srcDIB->getPalette();
   PalettePtr destPal = getPalette();

   if (*srcPal == *destPal)
   {
      if(srcDIB->hasMask())
         doBlit(this, destX, destY, w, h, srcDIB, srcX, srcY, FlipWriteMaskNoRemap(srcDIB->getMask()));
      else
         doBlit(this, destX, destY, w, h, srcDIB, srcX, srcY, FlipWriteNoMaskNoRemap());
   }
   else
   {
      // Remap
      CPalette::RemapTablePtr remapTablePtr = CPalette::makeRemapTable(srcPal, destPal);
      if(srcDIB->hasMask())
         doBlit(this, destX, destY, w, h, srcDIB, srcX, srcY, FlipWriteMaskRemap(srcDIB->getMask(), remapTablePtr));
      else
         doBlit(this, destX, destY, w, h, srcDIB, srcX, srcY, FlipWriteNoMaskRemap(remapTablePtr));
   }
#if 0
   PixelDimension x = destX;
   PixelDimension y = destY;
   if(clip(x, y, w, h))
   {
      srcX += x - destX;   // adjust if any clipping
      srcY += y - destY;

      // TODO: Clip src co-ordinates as well!

      ASSERT(srcX >= 0);
      ASSERT((srcX + w) <= srcDIB->getWidth());
      ASSERT(srcY >= 0);
      ASSERT((srcY + h) <= srcDIB->getHeight());

      ASSERT(x >= 0);
      ASSERT((x + w) <= getWidth());
      ASSERT(y >= 0);
      ASSERT((y + h) <= getHeight());


      const ColorIndex* srcLine = srcDIB->getAddress(srcX, srcY);
      ColorIndex* destLine = getAddress(x, y);

      if(srcDIB->hasMask())
      {
         while(h--)
         {
            const ColorIndex* src = srcLine;
            src += w;

            ColorIndex* dest = destLine;

            PixelDimension x = w;
            while(x--)
            {
               // if(!hasMask() || *src != getMask())
               if(*src != srcDIB->getMask())
                  *dest = *src;
               dest++;
               src--;
            }

            srcLine += srcDIB->getStorageWidth();
            destLine += getStorageWidth();
         }
      }
      else
      {
         while(h--)
         {
            const ColorIndex* src = srcLine;
            src += w;

            ColorIndex* dest = destLine;

            PixelDimension x = w;
            while(x--)
            {
               *dest++ = *src--;
            }

            srcLine += srcDIB->getStorageWidth();
            destLine += getStorageWidth();
         }
      }
   }
#endif
}




// draws lines at a right angle and centered on given axis
void
DrawDIB::rightAngleLines(const PixelDimension width, const PixelDimension space, PixelPoint& start, PixelPoint& end, ColorIndex color)
{

  const  PixelDimension y = end.getY() - start.getY();
  const  PixelDimension x = end.getX() - start.getX();

  const  PixelDimension dist = aproxDistance(x, y);

  if(dist > 0)  // make sure we don't divide by 0!
  {
    PixelDimension s = space;

    while(s < dist)
    {

      const PixelDimension startX = ((s * x) / dist) + start.getX();
      const PixelDimension startY = ((s * y) / dist) + start.getY();

      POINT p1;
      p1.x = startX + ((width * y) / (2 * dist));
      p1.y = startY - ((width * x) / (2 * dist));

      POINT p2;
      p2.x = startX - ((width * y) / (2 * dist));
      p2.y = startY + ((width * x) / (2 * dist));

      line(p1.x, p1.y, p2.x, p2.y, color);

      s += space;
    }
  }
}


void
DrawDIB::drawBarChart(PixelDimension x, PixelDimension y, int value, int maxValue, PixelDimension width, PixelDimension height)
{
   PixelDimension x1 = x + 1;
   PixelDimension y1 = y + 1;
   PixelDimension w1 = width - 2;
   PixelDimension h1 = height - 2;

   ASSERT(w1 > 0);
   ASSERT(h1 > 0);

   PixelDimension middle = (value * w1) / maxValue;
   PixelDimension leftover = w1 - middle;


   /*
    * Draw a border
    */

   ColorIndex border1 = getColor(PaletteColors::Black);
   ColorIndex border2 = getColor(PaletteColors::LightGrey); //(PALETTERGB(192, 192, 192));
   ColorIndex backColor = getColor(PaletteColors::DarkGrey); //(PALETTERGB(64, 64, 64));
// ColorIndex fillColor = getColor(PALETTERGB(255, 192, 0));

   frame(x, y, width, height, border1, border2);

   /*
    * Get Fill Color
    */

   int percent = MulDiv(value, 100, maxValue);
   ColorIndex fillColor = (percent < 25) ? getColor(PaletteColors::Yellow) :
                            (percent < 50) ? getColor(PaletteColors::Red) :
                            (percent < 75) ? getColor(PaletteColors::Blue) :
                            getColor(PaletteColors::Green);
   /*
    * Draw used area
    */

   if(middle)
      rect(x1, y1, middle, h1, fillColor);

   /*
    * Draw unused area
    */

   if(leftover)
      rect(x1 + middle, y1, leftover, h1, backColor);

}

void
DrawDIB::drawBarChart(const DrawDIB* bkSrc, const DrawDIB* fillSrc, PixelDimension x, PixelDimension y, int value, int maxValue, PixelDimension width, PixelDimension height)
{
   ASSERT(bkSrc != 0);
   ASSERT(fillSrc != 0);

   PixelDimension x1 = x + 2;
   PixelDimension y1 = y + 2;
   PixelDimension w1 = width - 4;
   PixelDimension h1 = height - 3;

   ASSERT(w1 > 0);
   ASSERT(h1 > 0);
   ASSERT(maxValue != 0);

   PixelDimension middle = (value * w1) / maxValue;
   PixelDimension leftover = w1 - middle;


   /*
    * Draw a border
    *
    * Border is 2 pixels wide except the bottom, which is 1 pixel wide
    */

   ColorIndex border1 = getColor(PaletteColors::Black);
   ColorIndex border2 = getColor(PaletteColors::DarkBrown);
   ColorIndex border3 = getColor(PaletteColors::OffWhite); //(PALETTERGB(192, 192, 192));

   frame(x, y, width, height, border1, border3);
   frame(x+1, y+1, width-2, height-2, border2, border3);

   /*
    * Draw used area
    */

   if(middle)
      rect(x1, y1, middle, h1, fillSrc);

   /*
    * Draw unused area
    */

   if(leftover)
      rect(x1 + middle, y1, leftover, h1, bkSrc);

}

void
DrawDIBDC::lightenedText(const char* text, PixelDimension x, PixelDimension y, int percent)
{
  SIZE s;

  BOOL result = GetTextExtentPoint32(getDC(), text, lstrlen(text), &s);
  ASSERT(result != False);

  if(result)
  {
    setBkMode(TRANSPARENT);

    setTextColor(RGB(0, 0, 0));

    lightenRectangle(x, y, s.cx, s.cy, percent);

    wTextOut(getDC(), x, y, text);
  }

}

/*
 * Draw Text onto a bargraph with a DIB mask and Black\White remap table
 */

void
DrawDIBDC::drawBargraphText(const char* text, HFONT hFont, PixelDimension destX, PixelDimension destY, int width)
{
  ASSERT(text != 0);
  ASSERT(hFont != 0);

  /*
   *  Get global palette entries
   */

  const int paletteEntries = 256;
  PALETTEENTRY pal[paletteEntries];

  int result = GetPaletteEntries(Palette::get(), 0, paletteEntries, pal);
  ASSERT(result == paletteEntries);


  /*
   *  Get text width and height
   */

  SIZE s;

  result = GetTextExtentPoint32(getDC(), text, lstrlen(text), &s);
  ASSERT(result != False);

  if( (width != 0) && (s.cx > width) )
      s.cx = width;

  /*
   *  create a DIB mask
   */

  DrawDIBDC dibMask(s.cx, s.cy);
  dibMask.setFont(hFont);
  dibMask.setBkMode(TRANSPARENT);

  /*
   * Set this->mask to first index  that is not RGB(0, 0, 0) or RGB(255, 255, 255)
   */

#ifdef DEBUG
  Boolean found = False;
#endif

  ColorIndex i = 0;
  for( ; i < paletteEntries; i++)
  {
    if(!Palette::isBlack(pal[i]) && !Palette::isWhite(pal[i]))
    {
      setMaskColor(i);
#ifdef DEBUG
      found = True;
#endif
      break;
    }
  }

#ifdef DEBUG
  if(!found)
    FORCEASSERT("Non-Black palette entry not found");
#endif

  /*
   *     Set Text Color to next index that isn't white or black
   */

#ifdef DEBUG
  found = False;
#endif

  i++;

  for( ; i < paletteEntries; i++)
  {
    if(!Palette::isWhite(pal[i]) && !Palette::isBlack(pal[i]))
    {
      dibMask.setTextColor(PALETTERGB(pal[i].peRed, pal[i].peGreen, pal[i].peBlue));
#ifdef DEBUG
      found = True;
#endif
      break;
    }
  }

#ifdef DEBUG
  if(!found)
    FORCEASSERT("Non-White palette entry not found");
#endif


  /*
   *  Fill DIB mask with this->mask index and draw text
   */

  dibMask.fill(getMask());
  dibMask.setMaskColor(getMask());

  wTextOut(dibMask.getDC(), 0, 0, s.cx, text);

  /*
   *  Get remap table and call remapDIB
   */

  CPalette::RemapTablePtr remapTable = getPalette()->getBlackWhiteRemapTable();
  remapDIB(&dibMask, remapTable, destX, destY);
}

/*
 *  Same as above with variable arguments
 */

void
DrawDIBDC::drawBargraphText(HFONT hFont, PixelDimension destX, PixelDimension destY, const char* format, ...)
{
   char buffer[1024];

   va_list vaList;
   va_start(vaList, format);
   vbprintf(buffer, 1024, format, vaList);
   va_end(vaList);

   drawBargraphText(buffer, hFont, destX, destY);
}

#if 0
void DrawDIBDC::resize(PixelDimension width, PixelDimension height)
{
   // DrawDIB::resize(width, height);
   if ((getWidth() != width) || (getHeight() != height))
   {
      DCObject::reset();
      makeDIBSection(width, height);
      DCObject::init(getHandle());
   }
}
#endif

void DIBDC::resize(PixelDimension width, PixelDimension height)
{
   if ((getWidth() != width) || (getHeight() != height))
   {
      DCObject::reset();
      makeDIBSection(width, height);
      DCObject::init(getHandle());
   }
}

DIBDC::DIBDC(PixelDimension width, PixelDimension height) :
   DIB(),
   DCObject()
{
   DIB::makeDIBSection(width, height);
   DCObject::init(getHandle());
}

DIBDC::~DIBDC()
{
   // ensure that Bitmap is not selected into DC
   // before the DIBSection is deleted

   DCObject::reset();
}

DrawDIBDC::~DrawDIBDC()
{
   // this must be here because otherwise
   // DrawDIB is deleted before DIBDC

   DCObject::reset();
}

DrawDIBDC::DrawDIBDC(PixelDimension width, PixelDimension height) :
   DrawDIB(),
   DIBDC(width, height)
{
}

#ifdef DEBUG
BITMAPINFO* DrawDIBDC::getBitmapInfo() const
{
   static BITMAPINFO inf;

   inf.bmiHeader.biSize = sizeof(BITMAPINFOHEADER);
   inf.bmiHeader.biWidth = getWidth();
   inf.bmiHeader.biHeight = -getHeight();      // -ve to make it top down
   inf.bmiHeader.biPlanes = 1;
   inf.bmiHeader.biBitCount = 8;
   inf.bmiHeader.biCompression = BI_RGB;
   inf.bmiHeader.biSizeImage = 0;
   inf.bmiHeader.biXPelsPerMeter = 0;
   inf.bmiHeader.biYPelsPerMeter = 0;
   inf.bmiHeader.biClrUsed = 0;
   inf.bmiHeader.biClrImportant = 0;

   return &inf;
}
#endif


/*==============================================================
 * DC Object
 */

/*
 * DrawDIB with DC
 */

// #ifdef DEBUG_DIBDC
// static int nDCCreated = 0;
// static int nDCDeleted = 0;
// #endif

DCObject::DCObject() :
   d_save(),
   d_originalBM(0),
   d_hdc(CreateCompatibleDC(NULL))
{
   if(d_hdc == NULL)
      throw WinError("Couldn't create DC for DrawDIB");

#ifdef DEBUG_DIBDC
   s_dcLog.logNew(d_hdc);
#endif
}

DCObject::~DCObject()
{
   if(d_hdc)
   {
      if(d_originalBM)
         reset();

      ASSERT(d_originalBM == 0);

      DeleteDC(d_hdc);
#ifdef DEBUG_DIBDC
      s_dcLog.logDelete(d_hdc);

      // nDCDeleted++;
      // dcLog->printf("--------------------- %p %d DC's deleted (%d active)",
      //      (void*)hdc,
      //      nDCDeleted, nDCCreated - nDCDeleted);
      // if(debugText.toStr())
      //   dcLog->printf("                       (deleted)-------=>  %s ", debugText.toStr());
#endif
      d_hdc = NULL;
   }
}

void DCObject::init(HBITMAP bm)
{
   ASSERT(d_hdc != NULL);
   ASSERT(d_originalBM == NULL);
   ASSERT(bm != NULL);

   d_originalBM = static_cast<HBITMAP>(SelectObject(d_hdc, bm));
   if(d_originalBM == NULL)
      throw WinError("couldn't select DIB into DC");
}

void DCObject::reset()
{
   // ASSERT(d_originalBM != NULL);
   ASSERT(d_hdc != NULL);

   if(d_originalBM != NULL)
   {
      HBITMAP bm = static_cast<HBITMAP>(SelectObject(d_hdc, d_originalBM));
      ASSERT(bm != NULL);
      d_originalBM = NULL;
   }
}


HGDIOBJ DCObject::selectObject(HGDIOBJ obj)
{
   ASSERT(d_hdc != NULL);
   HGDIOBJ ob = SelectObject(d_hdc, obj);
   if(ob == NULL)
      throw WinError("Could not select Object into DIB");
   return ob;
}

int DCObject::setBkMode(int bkMode)
{
   ASSERT(d_hdc != NULL);
   int bk = SetBkMode(d_hdc, bkMode);
   return bk;
}

COLORREF
DCObject::setTextColor(COLORREF col)
{
   ASSERT(d_hdc != NULL);
   COLORREF c = SetTextColor(d_hdc, col);
   return c;
}

COLORREF
DCObject::setBkColor(COLORREF col)
{
   ASSERT(d_hdc != NULL);
   COLORREF c = SetBkColor(d_hdc, col);
   return c;
}

UINT
DCObject::setTextAlign(UINT textAlign)
{
   ASSERT(d_hdc != NULL);
   UINT align = SetTextAlign(d_hdc, textAlign);
   return align;
}

HFONT
DCObject::setFont(HFONT font)
{
   ASSERT(d_hdc != NULL);
   return (HFONT) selectObject(font);
}

HPEN
DCObject::setPen(HPEN pen)
{
   ASSERT(d_hdc != NULL);
   return (HPEN) selectObject(pen);
}

HBRUSH
DCObject::setBrush(HBRUSH brush)
{
   ASSERT(d_hdc != NULL);
   return (HBRUSH) selectObject(brush);
}

/*----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/06/13 08:52:39  greenius
 * Initial Import
 *
 * Revision 1.8  1995/11/22 10:43:46  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.7  1995/11/07 10:39:01  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.6  1995/11/03 13:26:14  Steven_Green
 * Line Drawing and Clipping added
 *
 * Revision 1.5  1995/10/29 16:01:13  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.4  1995/10/25 09:52:13  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.3  1995/10/18 10:59:19  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.2  1995/10/17 12:02:04  Steven_Green
 * Works properly with palette manager and BMP loader
 *
 * Revision 1.1  1995/10/16 11:31:51  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */