/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef BMP_H
#define BMP_H

#ifndef __cplusplus
#error bmp.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1995, Steven Morle-Green, All Rights Reserved
 *----------------------------------------------------------------------
 *
 *	BMP File Loader
 *
 *----------------------------------------------------------------------
 */

#include "sysdll.h"
#include <windows.h>
#include "except.hpp"


class DIB;
class DIBDC;
class DrawDIB;
class DrawDIBDC;


class SYSTEM_DLL BMP {
 public:
	enum ReadMode {
		RBMP_Normal,				// Load picture and remap from global palette
		RBMP_ForcePalette,		// Load picture and set global palette
		RBMP_OnlyPalette,			// Just set the global palette to the BMP's palette

		RBMP_HowMany
	};

	static void getPalette(const char* fileName);
	static void getPalette(HINSTANCE hinst, const char* resName);

	static DIB* newDIB(const char* fileName, ReadMode mode);
	static DrawDIB* newDrawDIB(const char* fileName, ReadMode mode);
	static DIBDC* newDIBDC(const char* fileName, ReadMode mode);
	static DrawDIBDC* newDrawDIBDC(const char* fileName, ReadMode mode);

	static DIB* newDIB(HINSTANCE hinst, const char* resName, ReadMode mode);
	static DrawDIB* newDrawDIB(HINSTANCE hinst, const char* resName, ReadMode mode);
	static DIBDC* newDIBDC(HINSTANCE hinst, const char* resName, ReadMode mode);
	static DrawDIBDC* newDrawDIBDC(HINSTANCE hinst, const char* resName, ReadMode mode);

	class BMPError : public GeneralError {
 	public:
 		BMPError() : GeneralError("Error reading BMP") { }
	};
};




#endif /* BMP_H */

/*---------------------------------------------------------------
 * $Log$
 * Revision 1.1  2001/06/13 08:52:39  greenius
 * Initial Import
 *
 * Revision 1.2  1995/10/25 09:52:49  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.1  1995/10/17 12:03:08  Steven_Green
 * Initial revision
 *
 *---------------------------------------------------------------
 */
