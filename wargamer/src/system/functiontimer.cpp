/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
/*
 * Function Timer Implementation
 *
 * swg:17jun2001: This is incomplete... removed from project
 */


#include "stdinc.hpp"
#include "FunctionTimer.hpp"
#include "sync.hpp"
#include "thread.hpp"
#include <map>

/*
 * Internal HiRes Timer Thread
 */

class HiresTimer : public Thread
{
    public:
        static HiresTimer* instance();

        void start(FunctionTimer* timer);
        void stop(FunctionTimer* timer);
    private:
        HiresTimer();
        ~HiresTimer();

        typedef std::map<const char*, TimerInfo> Container;

        Container d_items;
        LARGE_INTEGER d_frequence;
        FunctionTimer* d_current;

        static HiresTimer* s_instance;
};

static HiresTimer* HiresTimer::s_instance = 0;


HiresTimer* HiresTimer::instance()
{
    if(s_instance == 0)
        s_instance = new HiresTimer;
    return s_instance;
}

HiresTimer::HiresTimer() :
    d_items()
{
   bool result = QueryPerformanceFrequency(&d_frequency);
   ASSERT(result);
}

HiresTimer::~HiresTimer()
{
}

void HiresTimer::stop(FunctionTimer* timer)
{
}


/*
 * Global Functions to wrap around the functions to time.
 */

FunctionTimer::FunctionTimer(const char* id) : d_id(id)
{
   HiresTimer* hi = HiresTimer::instance();

   LARGE_INTEGER startTime;
   bool result = QueryPerformanceCounter(startTime);
   ASSERT(result);

   d_lastTimer = hi->currentTime();
   if(d_lastTimer)
      d_lastTimer->stop(startTime);

   start(startTime);
}

FunctionTimer::~FunctionTimer()
{
   LARGE_INTEGER endTime;
   bool result = QueryPerformanceCounter(&endtime);
   HiresTimer::instance()->log(d_id, endTime - d_startTime);
   if(d_lastTimer)
      d_lastTimer->start(endTime);
}

