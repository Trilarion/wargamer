/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef CSTRING_HPP
#define CSTRING_HPP

#ifndef __cplusplus
#error StringPtr.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1995, Steven Morle-Green, All Rights Reserved
 *----------------------------------------------------------------------
 *
 *	Simple String that is deleted when destruvtructed
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/06/13 08:52:39  greenius
 * Initial Import
 *
 *
 *----------------------------------------------------------------------
 */

#include "sysdll.h"
#include "misc.hpp"

/*
 * Class to hold a string
 * Auto delete on destruct
 */

class CString {
	char* value;
private:
	SYSTEM_DLL void kill()
	{
		if(value)
		{
			delete[] value;
			value = 0;
		}
	}

protected:
	const char* get() const 
	{
		return value; 
	}

	void set(char* s) 
	{
		kill();
		value = s; 
	}

 private:
	CString(CString& s);
	CString& operator = (CString& s);
public:
	CString() { value = 0; }
	CString(char* s) { value = s; }
	~CString() { kill(); }

	operator const char* () const { return get(); }
	CString& operator = (char* s) { set(s); return *this; }

	const char* toStr() const { return get(); }
};


/*
 * StringPtr copies text when it is moved about
 */

class StringPtr {
	char* text;
public:
	StringPtr() : text(0) { }
	StringPtr(const char* s) : text(copyString(s)) { }
	StringPtr(const StringPtr& s) : text(copyString(s.text)) { }
	~StringPtr() { if(text != 0) delete[] text; }

	operator const char* () const { return text; }
	const char* toStr() const { return text; }

	size_t length() const { return text ? strlen(text) : 0; }

	StringPtr& operator = (const char* s) { set(s); return *this; }
	StringPtr& operator = (const StringPtr& s) { set(s.text); return *this; }
private:
	void set(const char* s)
	{
		if(text != 0)
			delete[] text;
		text = copyString(s);
	}
};

#endif /* CSTRING_HPP */

