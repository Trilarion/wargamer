/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef _CTBAR_HPP
#define _CTBAR_HPP

#include "sysdll.h"
#include "winctrl.hpp"

class DrawDIBDC;
class DrawDIB;
struct CustomBorderInfo;

class SYSTEM_DLL CustomTrackBar  {
	 POINT* tics;
	 int numTics;
	 static WNDPROC lpfnTBWndProc;
	 HWND parent;
	 DrawDIBDC* dDib;
	 const DrawDIB* bkDib;
	 // HWND hToolTip;
//	 ImageList image;
	 HWND hWnd;

	 COLORREF lightBrown;
	 COLORREF offWhite;
	 COLORREF tan;
	 COLORREF darkBrown;

	 enum {
		 LightBrown,
		 OffWhite,
		 Tan,
		 DarkBrown,
		 HowMany
	  };

  public:
	 // CustomTrackBar(HWND p, HWND hwnd, HWND hTTip, const DrawDIB* dib);
	 CustomTrackBar(HWND p, HWND hwnd, const DrawDIB* dib, const CustomBorderInfo& info);
	 ~CustomTrackBar();
  private:
	 static LRESULT CALLBACK tbSubClassProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam);
	 void onPaint(HWND hWnd);
	 // void relayToolTip();
	 void onLButtonDown(HWND hwnd, BOOL fDoubleClick, int x, int y, UINT keyFlags);
	 void onLButtonUp(HWND hwnd, int x, int y, UINT keyFlags);

	 void drawChannel(HWND hwnd);
	 void drawTics();
	 void drawThumb(HWND hwnd);
};


#endif
