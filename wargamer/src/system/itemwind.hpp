/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef ITEMWIND_HPP
#define ITEMWIND_HPP

#include "sysdll.h"
#include "wind.hpp"
#include "palwind.hpp"
#include "grtypes.hpp"
#include "winctrl.hpp"

/*---------------------------------------------------------------
 * Base class for combo-like popup list window
 */

/*
 * First, a data struct
 */

class DrawDIB;
class DrawDIBDC;
class ImageLibrary;
class DragWindowObject;
struct ItemWindowLayout;

struct SYSTEM_DLL ItemWindowData {
	 HWND d_hParent;
	 DWORD d_exStyle;
	 DWORD d_style;
	 UWORD d_id;
	 HFONT d_hFont;
	 UWORD d_itemCY;                  // height of each list item
	 UWORD d_scrollCX;                // width of scroll
	 UWORD d_maxItemsShowing;         // maximumn items to show
	 UBYTE d_flags;
	 // int d_dbX;
	 // int d_dbY;
	 const DrawDIB* d_windowFillDib;  // to fill in window
	 const DrawDIB* d_fillDib;
	 const ImageLibrary* d_scrollBtns;
	 const DrawDIB* d_scrollFillDib;
	 const DrawDIB* d_scrollThumbDib;
	 CustomBorderInfo d_bColors;
	 COLORREF d_colorize;

	 enum {
		HasScroll             = 0x01,
		NoAutoHide            = 0x02,
		ShadowBackground      = 0x04,
		TextHeader            = 0x08,
		NoBorder              = 0x10,
		DragList              = 0x20,
		OwnerDraw             = 0x40,
		InclusiveSize			 = 0x80		// Size given includes borders and header
	 };

	 ItemWindowData() :
		d_hParent(0),
		d_style(WS_POPUP | WS_CLIPSIBLINGS), // default style
		d_exStyle(0),
		d_id(0),
		d_hFont(0),
		d_itemCY(10),
		d_scrollCX(10),
		d_maxItemsShowing(10),
		d_flags(0),
		// d_dbX(0),
        // d_dbY(0),
		d_windowFillDib(0),
		d_fillDib(0),
		d_scrollBtns(0),
		d_scrollFillDib(0),
		d_scrollThumbDib(0),
		d_colorize(PALETTERGB(128, 128, 128)) {}

   int borderWidth() const;
   int borderHeight() const;

};

/*
 * Info used by drawBackground...
 * Too confusing... would be clearer if it stored RECT for each element
 */

struct SYSTEM_DLL ItemWindowLayout
{
	int s_listCX;                       // Sze of ListBox
	int s_listCY;
	int s_shadowWidth;                  // Size of Shadow
	int s_shadowHeight;
	int s_headerCY;                     // Size of Header
	int s_headerOffset;                 // Coordinate of BOTTOM of header?
	int s_scrollW;                      // Scroll Bar Width
	LONG s_offsetX;                     // Position of topleft inside border
	LONG s_offsetY;
	int s_totalW;                       // Total Size
	int s_totalH;
	POINT s_p;                          // Pixel Coordinate of TopLeft on parent

	ItemWindowLayout();
};

/*
 * Subclassed ListBox, simply passes on getTrackDIB to parent
 */

class ItemWindowListBox : public SubClassWindowBase
{
    public:
        ItemWindowListBox() : SubClassWindowBase(), d_hwnd(NULL) { }

        ~ItemWindowListBox()
        {
            ASSERT(d_hwnd == NULL);     // must call clear() before destruction
        }

        void set(HWND h) { d_hwnd = h; init(h); }
        void clear() { SubClassWindowBase::clear(d_hwnd); d_hwnd = NULL; }

        HWND getHWND() const { return d_hwnd; }

	    virtual LRESULT procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);

    private:
        HWND d_hwnd;
};

//class CampaignComboData;
class SYSTEM_DLL ItemWindow : public WindowBaseND  {

	 static DrawDIBDC* s_windowDib;        // only one background Dib for all instances to share
	 static DrawDIBDC* s_itemDib;
	 static UWORD s_instanceCount;

	 HWND d_hScroll;
     ItemWindowListBox d_listBox;

	 ItemWindowData d_data;

	 Boolean d_showing;

	 mutable ULONG d_currentValue;
	 mutable UWORD d_topItem;        // index of top item in list

	 static UINT s_dragMsgID;

  public:
		enum IDs {
			ItemListBox = 100,
			ListHeader
		};

		ItemWindow(const ItemWindowData& data, const SIZE& s);
		~ItemWindow();

        void show(bool visible) { WindowBaseND::show(visible); }

		void show(const PixelPoint& p);
		void hide();
		void move(const PixelRect& r);	// { MoveWindow(getHWND(), r.left(). r.top(), r.width(), r.height(), TRUE);

		virtual void init(const void* data) = 0;
		virtual void drawListItem(const DRAWITEMSTRUCT* lpDrawItem) = 0;
		virtual void drawCombo(DrawDIBDC* dib, const void* data) = 0;
		virtual void drawHeader(const DRAWITEMSTRUCT* lpDrawItem) {}
		virtual void drawBackground(DrawDIBDC* dib, const ItemWindowLayout& info);

		ItemWindowData& data() { return d_data; }
		Boolean hasScroll() const { return (d_data.d_flags & ItemWindowData::HasScroll); }
		Boolean showing() const { return d_showing; }
		ULONG currentValue() const { return d_currentValue; }
		int currentIndex();
		int entries();
		void setCurrentIndex(int index);
		void setCurrentValue(ULONG currentValue);
		bool overItem(const PixelPoint& p) const;
		bool overItem(const PixelPoint& p, DragWindowObject* dropObject) const;
		void updateScroll();
		DrawDIBDC* allocateItemDib(LONG w, LONG h);
		void blitText(const DRAWITEMSTRUCT* lpDrawItem, const char* text);

		void resetItems();
		void addItem(const char* name, int id);
		int getNItems() { return entries(); }

		ListBox listBox();

        static DrawDIBDC* getDragDIB(HWND hListBox, int id);

  protected:
	 UWORD id() const { return d_data.d_id; }
	 const DrawDIB* fillDib() const { return d_data.d_fillDib; }
	 DrawDIBDC* itemDib() const { return s_itemDib; }
	 // int dbX() const { return d_data.d_dbX; }
     // int dbY() const { return d_data.d_dbY; }

	 void setCurrentValue();
	 void allocateWindowDib(LONG w, LONG h);

	 void showScroll();

	 virtual void onMeasureItem(HWND hwnd, MEASUREITEMSTRUCT* lpMeasureItem);
  private:

	 /*
	  *   Message Functions
	  */

	 LRESULT procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
	 BOOL onCreate(HWND hWnd, CREATESTRUCT* lpCreateStruct);
	 void onCommand(HWND hWnd, int id, HWND hwndCtl, UINT codeNotify);
	 void onDrawItem(HWND hwnd, const DRAWITEMSTRUCT* lpDrawItem);
//	 virtual void onMeasureItem(HWND hwnd, MEASUREITEMSTRUCT* lpMeasureItem);
//	 void onPaint(HWND hWnd);
	 BOOL onEraseBk(HWND hwnd, HDC hdc);
	 void onDestroy(HWND hWnd);
	 void onVScroll(HWND hwnd, HWND hwndCtl, UINT code, int pos);
	 void onLButtonDown(HWND hwnd, BOOL fDoubleClick, int x, int y, UINT keyFlags);

     DrawDIBDC* onGetDragDIB(HWND h, int id);
     virtual void drawDragItem(DrawDIBDC* dib, ListBox& lb, int id);

	 // other stuff
	 void transferBits(const PixelPoint& p, int cx, int cy, int shadowCX, int shadowCY);
	 void enableControls();
	 void blitText(const DRAWITEMSTRUCT* lpDrawItem);
	 void clipPoint(const RECT& r, POINT& p);
};



#endif


/*---------------------------------------------------------------
 * $Log$
 *---------------------------------------------------------------
 */
