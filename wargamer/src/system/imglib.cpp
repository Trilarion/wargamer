/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *	Image Library
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "imglib.hpp"

#include "dib.hpp"
#include "bmp.hpp"
#include "myassert.hpp"
#include "dib_blt.hpp"

DCObject* ImageLibrary::s_dibDC = 0;
UWORD ImageLibrary::s_instanceCount = 0;

ImageLibrary::ImageLibrary(HINSTANCE hInst, LPCTSTR bmpResName, UWORD n, const ImagePos* pos, Boolean dc) :
	dib(0),
	nImages(n),
	positions(pos),
	d_inst(hInst),
	d_resName(bmpResName),
	d_paletteID(0),
	mask(0)
{
	s_instanceCount++;

	if(s_dibDC == 0)
	{
	  s_dibDC = new DCObject;
	}

	ASSERT(s_dibDC);

	loadImage();
}


ImageLibrary::~ImageLibrary()
{
	ASSERT(dib != 0);
	ASSERT((s_instanceCount - 1) >= 0);

	if(dib != 0)
	  delete dib;

	if(--s_instanceCount == 0)
	{
	  delete s_dibDC;
	  s_dibDC = 0;
	}
}

void

ImageLibrary::blit(DrawDIB* dest, UWORD imgNum, LONG x, LONG y, Boolean shadow, UINT shadowOffset) const
{
	ASSERT(dib != 0);
	ASSERT(dest != 0);
	ASSERT(imgNum < nImages);

	update();

	if(imgNum < nImages)
	{
		const ImagePos* pos = &positions[imgNum];

		// dest->setMaskColour(mask);
		if(shadow)
		{
		  dest->darken(dib, (x - pos->hotX) + shadowOffset, (y - pos->hotY) + shadowOffset, pos->w, pos->h, pos->x, pos->y, 50);
		}

		dest->blit(x - pos->hotX, y - pos->hotY, pos->w, pos->h, dib, pos->x, pos->y);
	}
}

void

ImageLibrary::stretchBlit(DrawDIB* dest, UWORD imgNum, LONG x, LONG y, LONG w, LONG h) const
{
	ASSERT(dib != 0);
	ASSERT(dest != 0);
	ASSERT(imgNum < nImages);

	update();


	if(imgNum < nImages)
	{
		const ImagePos* pos = &positions[imgNum];

		DIB_Utility::stretchDIB(dest, x - pos->hotX, y - pos->hotY, w, h,
			dib, pos->x, pos->y, pos->w, pos->h);
	}
}

void ImageLibrary::stretchBlitShadow(DrawDIB* dest, UWORD imgNum, LONG x, LONG y, LONG w, LONG h, UINT shadowOffset) const
{
	ASSERT(dib != 0);
	ASSERT(dest != 0);
	ASSERT(imgNum < nImages);

	update();


	if(imgNum < nImages)
	{
		const ImagePos* pos = &positions[imgNum];

		dest->darken(dib, (x - pos->hotX) + shadowOffset, (y - pos->hotY) + shadowOffset, pos->w, pos->h, pos->x, pos->y, 50);

		DIB_Utility::stretchDIB(dest, x - pos->hotX, y - pos->hotY, w, h,
			dib, pos->x, pos->y, pos->w, pos->h);
	}
}

void

ImageLibrary::darkenBlit(DrawDIB* dest, UWORD imgNum, LONG x, LONG y, int percent) const
{
	ASSERT(dib != 0);
	ASSERT(dest != 0);
	ASSERT(imgNum < nImages);

	update();


	if(imgNum < nImages)
	{
	  const ImagePos* pos = &positions[imgNum];
	  dest->darken(dib, (x - pos->hotX), (y - pos->hotY), pos->w, pos->h, pos->x, pos->y, percent);
	}

}

void

ImageLibrary::flipBlit(DrawDIB* dest, UWORD imgNum, LONG x, LONG y, Boolean darken) const
{
	ASSERT(dib != 0);
	ASSERT(dest != 0);
	ASSERT(imgNum < nImages);

	update();


	if(imgNum < nImages)
	{
		const ImagePos* pos = &positions[imgNum];
		dest->flipBlit(x - pos->hotX, y - pos->hotY, pos->w, pos->h, dib, pos->x, pos->y);
	}
}

void

ImageLibrary::blit(HDC destDC, UWORD imgNum, LONG x, LONG y) const
{
	ASSERT(dib != 0);
	ASSERT(s_dibDC != 0);
	ASSERT(destDC != NULL);
	ASSERT(s_dibDC->getDC() != NULL);

	ASSERT(imgNum < nImages);

	update();


	if(imgNum < nImages)
	{
		s_dibDC->selectObject(dib->getHandle());

		const ImagePos* pos = &positions[imgNum];
		BitBlt(destDC, x - pos->hotX, y - pos->hotY, pos->w, pos->h, s_dibDC->getDC(), pos->x, pos->y, SRCCOPY);
	}
}

void

ImageLibrary::stretchBlit(HDC destDC, UWORD imgNum, LONG x, LONG y, LONG w, LONG h) const
{
	ASSERT(s_dibDC != 0);
	ASSERT(destDC != NULL);
	ASSERT(s_dibDC->getDC() != NULL);

	ASSERT(imgNum < nImages);

	update();


	if(imgNum < nImages)
	{
		s_dibDC->selectObject(dib->getHandle());

		const ImagePos* pos = &positions[imgNum];
		int oldStretchMode = SetStretchBltMode(destDC, COLORONCOLOR);
		ASSERT(oldStretchMode != 0);

		StretchBlt(destDC, x - pos->hotX, y - pos->hotY, w, h, s_dibDC->getDC(), pos->x, pos->y, pos->w, pos->h, SRCCOPY);
		SetStretchBltMode(destDC, oldStretchMode);
	}
}

Boolean

ImageLibrary::getImageSize(UWORD n, int& w, int& h) const
{
  ASSERT(n < nImages);

  if(n < nImages)
  {
	 const ImagePos* pos = &positions[n];

	 w = pos->w;
	 h = pos->h;

	 return True;
  }
  else
	 return False;
}

Boolean 

ImageLibrary::getHotSpots(UWORD n, int& x, int& y) const
{
  ASSERT(n < nImages);

  if(n < nImages)
  {
	 const ImagePos* pos = &positions[n];

	 x = pos->hotX;
	 y = pos->hotY;

	 return True;
  }
  else
	 return False;
}

void ImageLibrary::update() const
{
	if(d_paletteID != Palette::paletteID())
	{
		delete dib;
		dib = 0;

		loadImage();
	}
}

void ImageLibrary::loadImage() const
{
	ASSERT(dib == 0);
	dib = BMP::newDIB(d_inst, d_resName, BMP::RBMP_Normal);
	ASSERT(dib != 0);

	d_paletteID = Palette::paletteID();

	if(dib == 0)
		throw GeneralError("Unable to create Image Library");

	mask = (dib->getBits())[0];		// 1st pixel is mask colour
	dib->setMaskColour(mask);
}
