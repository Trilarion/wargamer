/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef DC_MAN_HPP
#define DC_MAN_HPP

#ifndef __cplusplus
#error dc_man.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *	Device Context Manager
 *
 *----------------------------------------------------------------------
 */

#include "sysdll.h"
#include "winerror.hpp"
#include "myassert.hpp"
#include <windows.h>

// #include "myassert.hpp"

// #define INCLUDE_DEVICE_CONTEXT

class DCsave {
public:
#ifdef DEBUG
   DCsave() : d_saveCount(0), d_failCount(0) { }
   ~DCsave() { ASSERT(d_saveCount == 0); ASSERT(d_failCount == 0); }
#endif


	void save(HDC hdc)
	{
		int result = SaveDC(hdc);
      ASSERT(result != 0);
#ifdef DEBUG
      if(result == 0)
         ++d_failCount;
      else
         ++d_saveCount;
#endif
	}

	void restore(HDC hdc) 
	{
      ASSERT((d_saveCount + d_failCount) > 0);
#ifdef DEBUG
      if(d_failCount)
         --d_failCount;
      else
      {
         --d_saveCount;
#endif

		int result = RestoreDC(hdc, -1);
      ASSERT(result != 0);
#ifdef DEBUG
      }
#endif
	}

#ifdef DEBUG
private:
   int d_saveCount;
   int d_failCount;
#endif


#if 0
	void save(HDC hdc)
	{
		if(SaveDC(hdc) == 0)
			throw WinError("DCsave::save");
	}

	void restore(HDC hdc) 
	{
		if(RestoreDC(hdc, -1) == 0)
			throw WinError("DCsave::restore");
	}
#endif

};



class DeviceContextItem;

class DeviceContext {
	DeviceContextItem* item;
public:
	SYSTEM_DLL DeviceContext();
	SYSTEM_DLL ~DeviceContext();

	SYSTEM_DLL operator HDC() const;
};

class DeviceContextError : public GeneralError {
public:
	DeviceContextError() : GeneralError("DeviceContext error") { }
};


#endif /* DC_MAN_HPP */


