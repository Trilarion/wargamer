/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1995, Steven Morle-Green, All Rights Reserved
 *----------------------------------------------------------------------
 *
 *	Pool Array
 *
 * A data structure for storing constant sized objects in an array
 * like manner, such that they can be accessed efficiently with
 * an index, and allowing efficent adding and removing of items.
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/06/13 08:52:39  greenius
 * Initial Import
 *
 * Revision 1.1  1996/01/19 11:30:05  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "poolarry.hpp"



PoolBase::PoolBase()
{
	usedMap = 0;
	howMany = 0;
	usedCount = 0;
}


PoolBase::PoolBase(PoolIndex count)
{
	ASSERT(count != 0);

	usedMap = 0;
	howMany = count;
	usedCount = 0;
	size_t mapSize = (count + 7) / 8;
	usedMap = new UBYTE[(count+7)/8];
	ASSERT(usedMap != 0);
	memset(usedMap, 0, mapSize);
}


PoolBase::~PoolBase()
{
	if(usedMap)
	{
		delete[] usedMap;
		usedMap = 0;
	}
	howMany = 0;
	usedCount = 0;
}

void

PoolBase::markAllUsed()
{
	ASSERT(usedMap != 0);

	size_t mapSize = (howMany + 7) / 8;

	memset(usedMap, 0xff, mapSize);

	usedCount = howMany;
}

void

PoolBase::markUsed(PoolIndex i)
{
	ASSERT(usedMap != 0);
	ASSERT(i < howMany);

	int byte = i / 8;
	UBYTE mask = (UBYTE) (1 << (i & 7));

	ASSERT( (usedMap[byte] & mask) == 0);

	usedMap[byte] |= mask;
	usedCount++;
}

void

PoolBase::clearUsed(PoolIndex i)
{
	ASSERT(usedMap != 0);
	ASSERT(i < howMany);

	int byte = i / 8;
	UBYTE mask = (UBYTE) (1 << (i & 7));

	ASSERT( (usedMap[byte] & mask) == mask);

	usedMap[byte] &= ~mask;
	usedCount--;
}

Boolean

PoolBase::isUsed(PoolIndex i)
{
	ASSERT(usedMap != 0);
	ASSERT(i < howMany);

	int byte = i / 8;
	UBYTE mask = (UBYTE) (1 << (i & 7));

	return (usedMap[byte] & mask) != 0;
}




#ifdef TESTPOOL

#include <stdio.h>
#include <stdlib.h>

void main()
{
	PoolArray<long> array(32, 100);

	printf("Created\n");
	array.print();

	for(PoolIndex i = 1; i <= array.entries(); i++)
	{
		array[i] = i;
	}

	printf("Set values\n");
	array.print();

	for(i = 1; i <= array.entries(); i++)
	{
		printf("%d = %ld\n", (int) i, array[i]);
	}

	printf("Accessed Values\n");
	array.print();

	array.remove(4);
	printf("Removed\n");
	array.print();

	array.remove(31);
	printf("Removed\n");
	array.print();

	PoolIndex newItem = array.add();
	printf("Added\n");
	array.print();
	printf("newItem = %d\n", (int) newItem);

	newItem = array.add();
	printf("Added\n");
	array.print();
	printf("newItem = %d\n", (int) newItem);

	newItem = array.add();
	printf("Added\n");
	array.print();
	printf("newItem = %d\n", (int) newItem);

	newItem = array.add();
	printf("Added\n");
	array.print();
	printf("newItem = %d\n", (int) newItem);

	array.remove(newItem);
	printf("Removed\n");
	array.print();
}


void _myAssert(const char* expr, const char* file, int line)
{
	printf("Assertion failed in\nFile: %s Line: %d\n`%s'\n", file, line, expr);
	exit(-1);
}

#endif
