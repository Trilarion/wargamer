/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1995, Steven Morle-Green, All Rights Reserved
 *----------------------------------------------------------------------
 *
 *	Greenius Data File Implementation
 *
 * This version uses Window's file handles
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/06/13 08:52:39  greenius
 * Initial Import
 *
 * Revision 1.1  1995/10/29 16:01:13  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "filepack.hpp"

/*
 * Identifier that goes onto start of file
 */

static char filePackID[FilePackIDLength] = "Greenius Packed Data File";

/*
 * Base Class Implementation
 */

PackFileBase::PackFileBase()
{
}

PackFileBase::~PackFileBase()
{
}

FilePackError PackFileBase::setPos(ChunkPos pos)
{
	return CE_OK;
}

ChunkPos PackFileBase::getPos()
{
	return CE_OK;
}

FilePackError PackFileBase::getError() const
{
	return CE_OK;
}

Boolean PackFileBase::isGood() const
{
	return True;
}

void PackFileBase::setError(FilePackError err)
{
}

PackFileBase::PackFileError::PackFileError(const char* fmt, ...)
{
}


/*
 * Reader class
 */

PackFileRead::PackFileRead(const char* fileName)
{
}

PackFileRead::~PackFileRead()
{
}

FilePackError PackFileRead::startReadChunk(BlockID id)
{
	return CE_OK;
}

FilePackError PackFileRead::read(void* ad, size_t size)
{
	return CE_OK;
}

char* PackFileRead::readString()
{
	return 0;
}

/*
 * Writer Class
 */


PackFileWrite::PackFileWrite(const char* fileName)
{
}

PackFileWrite::~PackFileWrite()
{
}

FilePackError PackFileWrite::startWriteChunk(BlockID id)
{
	return CE_OK;
}

FilePackError PackFileWrite::endWriteChunk()
{
	return CE_OK;
}

FilePackError PackFileWrite::write(const void* ad, size_t size)
{
	return CE_OK;
}

FilePackError PackFileWrite::writeString(const char* s)
{
	return CE_OK;
}

