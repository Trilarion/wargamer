/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1995, Steven Morle-Green, All Rights Reserved
 *----------------------------------------------------------------------
 *
 *	Todo log
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.2  2001/06/18 08:25:46  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 * Revision 1.1  2001/06/13 08:52:39  greenius
 * Initial Import
 *
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#ifdef DEBUG
#include "todolog.hpp"

#ifdef __WATCOMC__
#pragma initialise before program
#endif

LogFileFlush  todoLog("todo.log");




ToDoVar::ToDoVar(const char* function, const char* file, int line)
{
	done = False;
	text = function;
	srcFile = file;
	srcLine = line;

	todoLog.printf("TODO: %s, File: %s, Line: %d", text, srcFile, srcLine);
}


ToDoVar::~ToDoVar()
{
	if(!done)
	{
		todoLog.printf("Not Invoked: %s, File: %s, Line: %d", text, srcFile, srcLine);
	}
}

#if 0
void ToDoVar::invoke()
{
	if(!done)
	{
		done = True;
		todoLog.printf("invoked todo: %s in %s line %d", text, srcFile, srcLine);
	}
}
#endif


#endif
