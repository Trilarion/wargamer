/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *	Doubly Linked Intrusive List
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "dlist.hpp"
#include "myassert.hpp"


DiListBase::DiListBase()
{
	d_count = 0;
	d_link.d_next = &d_link;
	d_link.d_prev = &d_link;
}


DiListBase::~DiListBase()
{
  reset();
#if 0
	DLink* node = d_link.d_next;
	while(node != &d_link)
	{
		DLink* next = node->d_next;
		delete node;
		node = next;
	}
#endif
}

void

DiListBase::reset()
{
	DLink* node = d_link.d_next;
	while(node != &d_link)
	{
		DLink* next = node->d_next;
		delete node;
		node = next;
	}
   d_count = 0;
}

void

DiListBase::unlink(DLink* ob)
{
	ASSERT(d_count > 0);
	ASSERT(ob != 0);
	ASSERT(ob->d_next != 0);
	ASSERT(ob->d_prev != 0);
	ASSERT(ob != &d_link);

	ob->d_prev->d_next = ob->d_next;
	ob->d_next->d_prev = ob->d_prev;
	d_count--;
}

int

DiListBase::entries() const
{
	return d_count;
}

void 

DiListBase::insertAfter(DLink* prev, DLink* ob)
{
	ASSERT(prev != 0);
	ASSERT(ob != 0);
	ASSERT(prev != ob);
	ASSERT(ob->d_next == 0);
	ASSERT(ob->d_prev == 0);

	ob->d_next = prev->d_next;
	ob->d_prev = prev;

	prev->d_next = ob;
	ob->d_next->d_prev = ob;

	d_count++;
}



DiListIterBase::DiListIterBase(const DiListBase* container)
{
	d_container = container;
	d_current = &container->d_link;
}

int

DiListIterBase::next()
{
	if(d_current->d_next == &d_container->d_link)
		return 0;
	else
	{
		d_current = d_current->d_next;
		return 1;
	}
}

const DLink*

DiListIterBase::current() const
{
	ASSERT(d_current != &d_container->d_link);
	return d_current;
}

