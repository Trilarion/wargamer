/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *	Utilities for GRtypes
 *
 *----------------------------------------------------------------------
 */


#include "stdinc.hpp"
#include "grtypes.hpp"

PixelRect PixelRect::intersect(const PixelRect& r1, const PixelRect& r2)
{
	PixelRect r;
	if(IntersectRect(&r, &r1, &r2))
	{
		return r;
	}
	else
		return PixelRect(0,0,0,0);
}
