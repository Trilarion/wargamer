##############################################################################
# Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)	 #
# This Software is subject to the GNU General Public License.  				 #
# For License information see the file COPYING in the project root directory #
# For more information see the file README.									 #
##############################################################################

#####################################
# Makefile Include for System.DLL
#
# Execute from within System directory
#####################################


NAME = syslib
lnk_dependencies = syslib.mk

!include ..\wgpaths.mif

OBJS =                  &
#       app.obj 		&
#	bmp.obj		&
#	dialog.obj 	&
#	wind.obj 	&
#	dc_man.obj 	&
#	winctrl.obj 	&
#	msgbox.obj 	&
#	ctbar.obj	&
#	windsave.obj 	&
#	thread.obj 	&
#	sync.obj 	&
#	button.obj 	&
#	popicon.obj 	&
#	grtypes.obj	&
#	dib.obj 	&
#	dib_blt.obj 	&
#	dib_util.obj	&
#	dib_poly.obj	&
#	dibref.obj	&
#	fractal.obj	&
#	bezier.obj	&
#	gdiengin.obj	&
#	thicklin.obj	&
#	palette.obj 	&
#	imglib.obj 	&
#	date.obj 	&
#	datetime.obj	&
#	timer.obj 	&
#	mmtimer.obj	&
#	registry.obj 	&
#	wmisc.obj 	&
#	myassert.obj 	&
#	w95excpt.obj 	&
#	winerror.obj 	&
#	except.obj 	&
#	trig.obj 	&
#	misc.obj 	&
#	simpstr.obj 	&
#	ptrlist.obj 	&
#	poolarry.obj 	&
#	random.obj 	&
#	msgenum.obj 	&
#	idstr.obj 	&
#	bargraph.obj 	&
#	fonts.obj 	&
#	critical.obj 	&
#	sllist.obj 	&
#	dlist.obj 	&
#	array.obj 	&
#	sounds.obj 	&
#	generic.obj 	&
#	filecnk.obj 	&
#	winfile.obj 	&
#	filepack.obj 	&
#	filedata.obj 	&
#	filebase.obj 	&
#	tables.obj   	&
#	gamectrl.obj 	&
#	tooltip.obj 	&
#	logwin.obj	&
#	logw_imp.obj	&
#	palwind.obj 	&
#	framewin.obj 	&
#	pathdial.obj 	&
#	mciwind.obj 	&
#	cscroll.obj 	&
#	fsalloc.obj 	&
#	makename.obj 	&
#	sprlib.obj	&
#	lzmisc.obj	&
#	lzencode.obj	&
#	lzdecode.obj 	&
#	config.obj 	&
#	cbutton.obj 	&
#	cust_dlg.obj	&
#	resstr.obj  	&
#	animate.obj 	&
#	transwin.obj 	&
#	cmenu.obj 	&
#	fillwind.obj 	&
#	scrnbase.obj	&
#	cedit.obj 	&
#	trackwin.obj 	&
#	itemwind.obj 	&
#	splash.obj	&
#	clistbox.obj 	&
# 	c_combo.obj	&
#	fileiter.obj	&
#	soundsys.obj	&
#	wavefile.obj   	&
#	gtoolbar.obj 	&
#	gsecwind.obj 	&
#	ctab.obj 	&
#	tabwin.obj	&
#	crossref.obj	&
#	piclib.obj

!ifndef NODEBUG
#OBJS +=	palwin.obj	&
#	todolog.obj	&
#	memdebug.obj
!endif


!ifndef NOLOG
#OBJS +=	clog.obj
!endif


CFLAGS += -i=$(ROOT)\res

all :: $(TARGETS) .SYMBOLIC
	@%null

!include ..\lib95.mif


.before:
	@if exist uinfowin.* del /z uinfowin.*

.before:
        @echo Making $(TARGETS)

