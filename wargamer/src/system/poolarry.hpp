/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
#ifndef POOLARRY_H
#define POOLARRY_H

#ifndef __cplusplus
#error poolarry.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1995, Steven Morle-Green, All Rights Reserved
 *----------------------------------------------------------------------
 *
 * Pool Array
 *
 * A data structure for storing constant sized objects in an array
 * like manner, such that they can be accessed efficiently with
 * an index, and allowing efficent adding and removing of items.
 *
 *----------------------------------------------------------------------
 */

#include "sysdll.h"
#include "mytypes.h"
#include <string.h>
#include "myassert.hpp"

/*
 * Only allow 65534 items in array
 * This means indexes are only 2 bytes long.
 *
 * an index of 0xffff means no item
 * The first item will be items[0]
 *
 * This shouldn't be much of a problem if iterator classes are used
 * instead of for loops, which will be more efficient anyway.
 */

typedef unsigned short PoolIndex;
#define PoolIndex_First 0
#define PoolIndex_MAX (USHRT_MAX-1)
#define PoolIndex_NULL USHRT_MAX

#ifdef __WATCOM_CPLUSPLUS__
#pragma warning 549 9;     // Disable sizeof(Type) contains compiler information warning
#endif

/*
 * Everything in Pool is private, accessible only from PoolArray
 */

class SYSTEM_DLL PoolBase {
#ifdef _MSC_VER     
   public:
#else
   protected:
#endif
      PoolIndex howMany;
      PoolIndex usedCount;
      UBYTE* usedMap;

   public:
      PoolBase();
      PoolBase(PoolIndex count);
      ~PoolBase();

#ifdef _MSC_VER
   public:
#else
   protected:
#endif
      void markAllUsed();
      void markUsed(PoolIndex i);
      void clearUsed(PoolIndex i);
      Boolean isUsed(PoolIndex i);
};


template<class Type>
class Pool : public PoolBase 
{
#ifdef _MSC_VER     
   public:
#else
   // Visual C++ won't allow this, so make things public instead!
   friend class PoolArray<Type>;
#endif

   Pool<Type>* next;
   Type* items;

public:
   Pool();
   Pool(PoolIndex count);
   ~Pool();

#ifdef TESTPOOL
   void print();
#endif
};

/*
 * The templated container class
 */
 
template<class Type>
class PoolArray {
   Pool<Type> entry;             // A dummy first node in linked list
   PoolIndex blockSize;          // How many to allocate at a time
public:
   PoolArray(PoolIndex bSize = 256, PoolIndex sSize = 0);
   virtual ~PoolArray();

   // void init(PoolIndex sSize);
   void reserve(PoolIndex sSize, bool inUse);
   void reset();

   PoolIndex entries() const;
   PoolIndex used() const;

   PoolIndex add();
      // Create a new item

   void add(PoolIndex i);
      // Mark a particular item as being used
      // Typically this is used when loading in data where the
      // indeces must remain the same

   void remove(PoolIndex i);
      // Mark item as unused

   Type& get(PoolIndex i);
   const Type& get(PoolIndex i) const;
   Type& operator [] (PoolIndex i) { return get(i); }
   const Type& operator [] (PoolIndex i) const { return get(i); }

   Boolean isUsed(PoolIndex i) const;

   PoolIndex getIndex(const Type* ptr) const;

private:
   Pool<Type>* addPool(PoolIndex count);
   Boolean findItem(Pool<Type>*&pool, PoolIndex& i) const;
   void markUsed(PoolIndex i);
   void clearUsed(PoolIndex i);

#ifdef TESTPOOL
public:
   void print();
#endif
};

/*
 * Implementation
 */

template<class Type>
Pool<Type>::Pool()
{
   next = 0;
   items = 0;
}


template<class Type>
Pool<Type>::Pool(PoolIndex count) : PoolBase(count)
{
   items = 0;
   next = 0;

   ASSERT(count != 0);

   // items = new Type[count];

   items = static_cast<Type*>(::operator new(count * sizeof(Type)));

   ASSERT(items != 0);
}


template<class Type>
Pool<Type>::~Pool()
{
   if(items)
   {
      for(int i = 0; i < howMany; i++)
      {
         if(isUsed(i))
         {
            clearUsed(i);
            items[i].~Type();
         }
      }

      // delete[] items;
      ::operator delete(items);
      items = 0;
   }
}

/*
 * Construct a Pool Array
 *
 * bSize is the block size and defaults to 256
 * sSize is the number to intially reserve defaulting to 0
 */


template<class Type>
PoolArray<Type>::PoolArray(PoolIndex bSize, PoolIndex sSize)
{
   blockSize = bSize;
   if(sSize != 0)
      reserve(sSize, false);
      // init(sSize);
}

#if 0
template<class Type>
void PoolArray<Type>::init(PoolIndex sSize)
{
   ASSERT(entry.next == 0);
   ASSERT(entry.howMany == 0);

   if(sSize != 0)
   {
      Pool<Type>* p = addPool(sSize);
      p->markAllUsed();
      entry.usedCount += sSize;
   }
}
#else
template<class Type>
void PoolArray<Type>::reserve(PoolIndex sSize, bool inUse)
{
   ASSERT(entry.next == 0);
   ASSERT(entry.howMany == 0);

   if(sSize != 0)
   {
      Pool<Type>* p = addPool(sSize);
      if(inUse)
      {
         for(int i = 0; i < p->howMany; i++)
            new(&p->items[i]) Type;
         p->markAllUsed();
         entry.usedCount += sSize;
      }
   }
}
#endif

template<class Type>
PoolArray<Type>::~PoolArray()
{
   reset();
}


template<class Type>
void PoolArray<Type>::reset()
{
   while(entry.next)
   {
      Pool<Type>* p = entry.next;
      entry.next = p->next;
      entry.howMany -= p->howMany;
      entry.usedCount -= p->usedCount;
      delete p;
   }
   ASSERT(entry.howMany == 0);
   ASSERT(entry.usedCount == 0);
}


template<class Type>
Pool<Type>* PoolArray<Type>::addPool(PoolIndex count)
{
   ASSERT(count != 0);

   Pool<Type>* p = new Pool<Type>(count);
   Pool<Type>* p1 = &entry;
   while(p1->next)
      p1 = p1->next;
   p1->next = p;
   entry.howMany += count;
   return p;
}

template<class Type>
inline
PoolIndex PoolArray<Type>::entries() const
{
   return entry.howMany;
}

template<class Type>
inline
PoolIndex PoolArray<Type>::used() const
{
   return entry.usedCount;
}

template<class Type>
Boolean PoolArray<Type>::findItem(Pool<Type>*&pool, PoolIndex& i) const
{
   ASSERT(i != PoolIndex_NULL);

   // i--;        // Adjust because 1st item is [1]

   Pool<Type>* p = entry.next;
   while(p)
   {
      if(i < p->howMany)
      {
         pool = p;
         return True;
      }

      i -= p->howMany;
      p = p->next;
   }
   return False;
}

template<class Type>
void PoolArray<Type>::markUsed(PoolIndex i)
{
   Pool<Type>* pool;
   if(findItem(pool, i))
   {
      ASSERT(pool != 0);
      pool->markUsed(i);
      entry.usedCount++;
   }
}

template<class Type>
void PoolArray<Type>::clearUsed(PoolIndex i)
{
   Pool<Type>* pool;
   if(findItem(pool, i))
   {
      ASSERT(pool != 0);
      pool->clearUsed(i);

      entry.usedCount--;

      pool->items[i].~Type();

      // Call items destructor

      // pool->items[i].destroy();
   }
}

template<class Type>
PoolIndex PoolArray<Type>::add()
{
   /*
    * If all used up, add a new block
    */

   if(entry.howMany == entry.usedCount)
      addPool(blockSize);

   ASSERT(entry.howMany > entry.usedCount);

   /*
    * Find a free block
    */

   // PoolIndex i = 1;
   PoolIndex i = 0;
   Pool<Type>* p = entry.next;
   while(p)
   {
      if(p->howMany > p->usedCount)
      {
         for(PoolIndex i1 = 0; i1 < p->howMany; i1++)
         {
            if(!p->isUsed(i1))
            {
               p->markUsed(i1);
               entry.usedCount++;

               // Call item's constructor

               new(&p->items[i1]) Type;

               // p->items[i1].init();
               // p->items[i1].Type();

               return (PoolIndex) (i + i1);
            }
         }
      }

      i += p->howMany;
      p = p->next;
   }

   return PoolIndex_NULL;
}

template<class Type>
void PoolArray<Type>::add(PoolIndex i)
{
   ASSERT(i < entries());
   // It may be worth expanding the pool if i is bigger the entries()

   Pool<Type>* pool;
   Boolean found = findItem(pool, i);

   ASSERT(found);
   ASSERT(pool != 0);
   ASSERT(!pool->isUsed(i));

   pool->markUsed(i);
   entry.usedCount++;
   new(&pool->items[i]) Type;
}

template<class Type>
void PoolArray<Type>::remove(PoolIndex i)
{
   clearUsed(i);
}

template<class Type>
Type& PoolArray<Type>::get(PoolIndex i)
{
   ASSERT(i != PoolIndex_NULL);
   ASSERT(i < entry.howMany);

   Pool<Type>* pool;
   Boolean found = findItem(pool, i);

   ASSERT(found);
   ASSERT(pool != 0);
   ASSERT(pool->isUsed(i));

   return pool->items[i];
}

template<class Type>
const Type& PoolArray<Type>::get(PoolIndex i) const
{
   ASSERT(i != PoolIndex_NULL);
   ASSERT(i < entry.howMany);

   Pool<Type>* pool;
   Boolean found = findItem(pool, i);

   ASSERT(found);
   ASSERT(pool != 0);
   ASSERT(pool->isUsed(i));

   return pool->items[i];
}

template<class Type>
Boolean PoolArray<Type>::isUsed(PoolIndex i) const
{
   ASSERT(i != PoolIndex_NULL);
   ASSERT(i < entry.howMany);

   Pool<Type>* pool;
   Boolean found = findItem(pool, i);

   ASSERT(found);
   ASSERT(pool != 0);
   
   
   return pool->isUsed(i);
}


template<class Type>
PoolIndex PoolArray<Type>::getIndex(const Type* ptr) const
{
   // ASSERT(ptr != 0);
   if(ptr == 0)
      return PoolIndex_NULL;

   Pool<Type>* p = entry.next;
   int i = 0;
   while(p)
   {
      if( (ptr >= p->items) && (ptr < (p->items + p->howMany) ) )
      {
         return i + (ptr - p->items);
      }

      i += p->howMany;
      p = p->next;
   }

   FORCEASSERT("ptr is not in PoolArray");

   return PoolIndex_NULL;
}

#ifdef TESTPOOL

template<class Type>
void Pool<Type>::print()
{
   printf("Pool: howMany=%d, usedCount = %d\n",
      (int) howMany,
      (int) usedCount);
   if(usedMap)
   {
      int col = 0;

      for(PoolIndex i = 0; i < howMany; i++)
      {
         if(col++ >= 70)
         {
            col = 1;
            printf("\n");
         }
         printf("%c", isUsed(i) ? '1' : '0');
      }

      if(col != 0)
         printf("\n");
   }
}

template<class Type>
void PoolArray<Type>::print()
{
   printf("PoolArray\n");

   Pool<Type>* p = &entry;
   while(p)
   {
      p->print();
      p = p->next;
   }
}

#pragma warning 549 5;     // Disable sizeof(Type) contains compiler information warning

#endif   // TESTPOOL

#endif /* POOLARRY_H */

/*
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/06/13 08:52:39  greenius
 * Initial Import
 *
 * Revision 1.3  1996/02/16 17:34:14  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.2  1996/01/26 11:04:02  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.1  1996/01/19 11:30:29  Steven_Green
 * Initial revision
 *
 *----------------------------------------------------------------------
 */
