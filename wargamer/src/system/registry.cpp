/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1995, Steven Morle-Green, All Rights Reserved
 *----------------------------------------------------------------------
 *
 *	Win95 System Registry Interface
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/06/13 08:52:39  greenius
 * Initial Import
 *
 * Revision 1.1  1996/03/04 17:11:45  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "registry.hpp"

#include "myassert.hpp"
#include "sllist.hpp"
#include "misc.hpp"

#ifdef __WATCOM_CPLUSPLUS__
#include <string.hpp>
#else
#include <string>
typedef std::string String;
#endif

#include <windows.h>

static const char registryID[] = "Software\\Greenius\\Wargamer";

static String makeRegistryName(const char* subDir)
{
	String s = registryID;

	if(subDir && subDir[0])
    {
	    if(subDir[0] != '\\')
		    s += '\\';
	    s += subDir;
    }

	return s;
}


bool setRegistry(const char* key, const void* data, size_t length, const char* subDir)
{
	String registry = makeRegistryName(subDir);

	HKEY hkey;
    DWORD disposition;

	LONG ret = RegCreateKeyEx(
		HKEY_CURRENT_USER,
		registry.c_str(),
		0,
		NULL,
		REG_OPTION_NON_VOLATILE,
		KEY_ALL_ACCESS,		// KEY_SET_VALUE,
		NULL,
		&hkey,
		&disposition);

#ifdef DEBUG
	debugLog("setRegistry(%s, %s) = %ld %ld (%s)\n",
		static_cast<const char*>(registry.c_str()),
		static_cast<const char*>(key),
		ret,
       disposition,
       (disposition == REG_CREATED_NEW_KEY) ? "Created" :
          ( (disposition == REG_OPENED_EXISTING_KEY) ? "Opened" : "Unknown result"  )
       );
#endif


	ASSERT(ret == ERROR_SUCCESS);
	ASSERT(hkey != NULL);

	if(ret == ERROR_SUCCESS)
	{
		ret = RegSetValueEx(hkey, key, 0, REG_BINARY, static_cast<const BYTE*>(data), length);
		ASSERT(ret == ERROR_SUCCESS);
		LONG err = RegCloseKey(hkey);
		ASSERT(err == ERROR_SUCCESS);
		if(err != ERROR_SUCCESS)
          ret = err;
	}

	return (ret == ERROR_SUCCESS);
}

bool getRegistry(const char* key, void* data, size_t length, const char* subDir)
{
	String registry = makeRegistryName(subDir);

	HKEY hkey;
	DWORD type;
	DWORD lbuf = length;

	LONG ret = RegOpenKeyEx(HKEY_CURRENT_USER,
		registry.c_str(),
		0,
		KEY_ALL_ACCESS,		// KEY_QUERY_VALUE,
		&hkey);

#ifdef DEBUG
	debugLog("getRegistry(%s, %s) = %ld\n",
		(const char*) registry.c_str(),
		(const char*) key,
		ret);
#endif


	if(ret == ERROR_SUCCESS)
	{
		ret = RegQueryValueEx(hkey, (LPTSTR) key, 0, &type, (BYTE*) data, &lbuf);
#ifdef DEBUG
		if(ret == ERROR_SUCCESS)
       {
			ASSERT(lbuf == length);
          ASSERT(type == REG_BINARY);
       }
#endif
		LONG err = RegCloseKey(hkey);
       ASSERT(err == ERROR_SUCCESS);
		if(err != ERROR_SUCCESS)
          ret = err;
	}

	return (ret == ERROR_SUCCESS);
}


bool setRegistryString(const char* key, const char* string, const char* subDir)
{
	String registry = makeRegistryName(subDir);

	HKEY hkey;

	LONG ret = RegCreateKeyEx(
		HKEY_CURRENT_USER,
		registry.c_str(),
		0,
		NULL,
		REG_OPTION_NON_VOLATILE,
		KEY_ALL_ACCESS,		// KEY_SET_VALUE,
		NULL,
		&hkey,
		NULL);

#ifdef DEBUG
	debugLog("setRegistryString(%s, %s) = %ld\n",
		(const char*) registry.c_str(),
		(const char*) key,
		ret);
#endif


	ASSERT(ret == ERROR_SUCCESS);
	ASSERT(hkey != NULL);

	if(ret == ERROR_SUCCESS)
	{
		size_t length = lstrlen(string);

		ret = RegSetValueEx(hkey, key, 0, REG_SZ, (const BYTE*) string, length);
		ASSERT(ret == ERROR_SUCCESS);
		LONG err = RegCloseKey(hkey);
		ASSERT(err == ERROR_SUCCESS);
		if(err != ERROR_SUCCESS)
          ret = err;

	}

	return (ret == ERROR_SUCCESS);
}


bool getRegistryString(const char* key, char* string, const char* subDir)
{
	String registry = makeRegistryName(subDir);

	HKEY hkey;
	DWORD type = REG_SZ;
	DWORD lbuf = 200;

	LONG ret = RegOpenKeyEx(HKEY_CURRENT_USER,
		registry.c_str(),
		0,
		KEY_ALL_ACCESS,		// KEY_QUERY_VALUE,
		&hkey);

#ifdef DEBUG
	debugLog("getRegistry(%s, %s) = %ld\n",
		(const char*) registry.c_str(),
		(const char*) key,
		ret);
#endif


	if(ret == ERROR_SUCCESS)
	{
		ret = RegQueryValueEx(hkey, (LPTSTR) key, 0, &type, (BYTE*) string, &lbuf);

		LONG err = RegCloseKey(hkey);
		ASSERT(err == ERROR_SUCCESS);
		if(err != ERROR_SUCCESS)
          ret = err;
	}

	return (ret == ERROR_SUCCESS);
}


#if 0    // Moved to wind.cpp as part of save/restoreState
/*
 * Clip a rectangle and point so that they are on screen?
 */

void checkResolution(RECT& r, POINT& p)
{
  int w = GetSystemMetrics(SM_CXSCREEN);
  int h = GetSystemMetrics(SM_CYSCREEN);
  if(w < p.x || h < p.y)
  {
	 if((r.left + r.right) > w)
	 {
		r.left -= (p.x - w);
		if(r.left < 0)
		  r.left = 0;
	 }
	 if((r.top + r.bottom) > h)
	 {
		r.top -= (p.y - h);
		if(r.top < 0)
			r.top = 0;
	 }
  }
}
#endif

/*
 *  RegistryFile implementation
 *
 *  Keys will be stored in registry as file0, file1, file2, etc.
 */


RegistryFile::RegistryFile(const char* registryName)
{
  reset(registryName);
}

void RegistryFile::reset(const char* registryName)
{
  ASSERT(registryName != 0);
  d_registryName = registryName;

  lstrcpy(d_key, "file");

  d_fileNumber = 0;
}

void RegistryFile::reset()
{
  lstrcpy(d_key, "file");

  d_fileNumber = 0;
}

bool RegistryFile::getFileName(char* fileName)
{
  ASSERT(fileName != 0);

  char* c = d_key+4;

  itoa(d_fileNumber++, c, 10);

  char buf[200];

  if(getRegistryString(d_key, buf, d_registryName))
  {
	 lstrcpy(fileName, buf);
	 return True;
  }

  return False;
}

const UWORD FileNameEntryLimit = 10;

#ifdef _MSC_VER  // Visual C++ can't use local classes as template types
  struct FileList : public SLink {
	  char* fileName;
	  FileList(const char* name) { ASSERT(name != 0); fileName = copyString(name); }
	  ~FileList() { delete[] fileName; }
  };
#endif


bool RegistryFile::setFileName(const char* fileName)
{
  ASSERT(fileName != 0);

  /*
	*  fileName becomes file0. So we need to extract any current files
	*  from registry to increment their file number
	*/
#ifndef _MSC_VER  // Visual C++ can't use local classes as template types
  struct FileList : public SLink {
	  char* fileName;
	  FileList(const char* name) { ASSERT(name != 0); fileName = copyString(name); }
	  ~FileList() { delete[] fileName; }
  };
#endif

  SList<FileList> fileList;

  FileList* item = new FileList(fileName);
  ASSERT(item != 0);

  fileList.append(item);

  char buf[200];
  while(getFileName(buf))
  {
	 /*
	  *  If file = current file don't put it in twice
	  */

	 if(lstrcmp(buf, fileName) != 0)
	 {
		FileList* item = new FileList(buf);
		ASSERT(item != 0);

		fileList.append(item);
	 }
  }

  /*
	*  Reset data items
	*/

  reset();

  char* c = d_key+4;

  /*
	*  Write list back to registry, limit it to 10 entries
	*/

  SListIter<FileList> iter(&fileList);

  int i = 0;
  while(++iter)
  {
	 if(i < FileNameEntryLimit)
	 {
		itoa(d_fileNumber++, c, 10);

		FileList* item = iter.current();

		if(!setRegistryString(d_key, item->fileName, d_registryName))
		  return False;
	 }
	 else
		break;

	 i++;
  }

  return True;
}
