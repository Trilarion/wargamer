/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef CRC_HPP
#define CRC_HPP

/*

  Filename : CRC.hpp

  Description : Some CRC generating utilities

*/

#include "sysdll.h"

SYSTEM_DLL unsigned short makeCRC(unsigned char * data, int length);
SYSTEM_DLL unsigned short addBufferCRC(unsigned short CRC, unsigned char * data, int length);
SYSTEM_DLL unsigned short addByteCRC(unsigned short CRC, unsigned char b);
SYSTEM_DLL unsigned short addShortCRC(unsigned short CRC, unsigned short s);
SYSTEM_DLL unsigned short addIntCRC(unsigned short CRC, unsigned int i);

#endif

/*----------------------------------------------------------------------------
 * $Log$
 *----------------------------------------------------------------------------
 */
