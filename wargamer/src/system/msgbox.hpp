/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef MSGBOX_HPP
#define MSGBOX_HPP

#ifndef __cplusplus
#error msgbox.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *	Simple Message Box
 *
 *----------------------------------------------------------------------
 */

#include "sysdll.h"

int SYSTEM_DLL messageBox(const char* title, const char* text, unsigned int uType);

#endif /* MSGBOX_HPP */

