/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef FILEPACK_H
#define FILEPACK_H

#ifndef __cplusplus
#error filepack.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1995, Steven Morle-Green, All Rights Reserved
 *----------------------------------------------------------------------
 *
 *	Files containing chunks of Data
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  1995/10/29 16:02:49  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#include "sysdll.h"
#include <windows.h>
#include "filedata.hpp"
#include "myassert.hpp"
#include "except.hpp"

/*
 * Chunk Id's
 */

typedef char BlockID[8];

/*
 * Errors
 */

enum FilePackError {
	CE_OK,
	CE_GeneralError
};

/*
 * Other Types
 */

typedef ULONG ChunkPos;				// Position in File
typedef HANDLE FileHandle;

#define FilePackIDLength 32

struct ChunkDir {
	BlockID id;
	ChunkPos pos;
	size_t length;
};

struct D_ChunkDir {
	BlockID id;
	ILONG pos;
	ILONG length;
};

struct D_FileHeader {
	IBYTE id[FilePackIDLength];	 // Way of checking that this is a valid file
	IWORD version;					// Version Number
	IWORD nChunks;					// Number of Chunks in directory
	ILONG dirOffset;				// Where in the file is the directory?
	UBYTE description[24];		// A description (size is such that header rounded up to 64 bytes)
};


class SYSTEM_DLL PackFileBase {
protected:
	FileHandle fp;
	FilePackError lastError;
public:
	PackFileBase();
	~PackFileBase();
	FilePackError setPos(ChunkPos pos);
	ChunkPos getPos();
	FilePackError getError() const;
	Boolean isGood() const;

	virtual void showInfo(const char* fmt) { }
#ifdef DEBUG
	virtual void putInfo(const char* fmt) { }
#endif

	/*
	 * Exception handling
	 */

	class PackFileError : public GeneralError {
	public:
		PackFileError() : GeneralError("Chunk File Error") {	}
		PackFileError(const char* fmt, ...);
	};

protected:
	void setError(FilePackError err);
};

/*
 * Class Used for reading Binary files
 *
 * We'll use Standard C Buffered Files, because they are easier to
 * work with and quicker than C++ streams.
 */

class SYSTEM_DLL PackFileRead : public PackFileBase {
	int nChunks;
	ChunkDir* dir;
	char* id;
public:
	// FileType fileType;
	UWORD fileVersion;
public:
	// PackFileRead(const char* fileName, FileType wantType);
	PackFileRead(const char* fileName);
	~PackFileRead();

	FilePackError startReadChunk(BlockID id);
	FilePackError read(void* ad, size_t size);
	char* readString();
};

/*
 * Class Used for writing Binary Files
 */

class SYSTEM_DLL PackFileWrite : public PackFileBase {
	int chunkCount;
	int nextChunk;
	D_ChunkDir* dir;

public:
	// PackFileWrite(const char* fileName, FileType fType, const char* fileID, UWORD version, int nChunks);
	PackFileWrite(const char* fileName);
	~PackFileWrite();

	FilePackError startWriteChunk(BlockID id);
	FilePackError endWriteChunk();

	FilePackError write(const void* ad, size_t size);
	FilePackError writeString(const char* s);
};

SYSTEM_DLL void putName(IBYTE* dest, const char* src, size_t length);

#endif /* FILEPACK_H */

