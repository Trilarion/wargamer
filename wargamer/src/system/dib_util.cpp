/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 * DIB Utilities
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "dib_util.hpp"
#include "dib.hpp"
#include "palette.hpp"
#include "wmisc.hpp"
#include "custbord.hpp"
#include "misc.hpp"
#include <math.h>

namespace DIB_Utility
{


/*
 * Color destDIB in non-masked area of src
 * resultant colour is somewhere between original colour and rgb
 * e.g. if percent = 50, then it is exactly midway.
 * if percent = 100, then it is totally rgb.
 */

void colorize(DrawDIB* destDIB, const DIB* src, COLORREF rgb, int percent, LONG destX, LONG destY, LONG w, LONG h, LONG srcX, LONG srcY)
{
   // Make a remap table

   // const UBYTE* remapTable = Palette::getColorizeTable(rgb, percent);
   CPalette::RemapTablePtr remapTable = destDIB->getPalette()->getColorizeTable(rgb, percent);
   destDIB->remapDIB(src, remapTable, destX, destY, w, h, srcX, srcY);

   // Palette::releaseTable(remapTable);
}

/*
 * Modify bitmap so that only specified edge is left
 *
 * xOffset,yOffset determine which edge
 *
 * Apply pixel If pixel is not mask colour AND pixel[xOffset,yOffset] is mask
 *
 * -1, 0 : Left:
 * -1,-1 : TopLeft:
 *  0,-1 : Top:
 * +1,-1 : TopRight:
 * +1, 0 : Right:
 * +1,+1 : BottomRight:
 *  0,+1 : Bottom:
 * -1,+1 : BottomLeft:
 *
 * We could speed this up by having seperate loops for each direction (4 in all)
 * instead of using dx, dy.
 */

void edge(DrawDIB* dib, int xOffset, int yOffset)
{
   ASSERT(dib);
   if(!dib)
      return;

   ASSERT(dib->hasMask());
   if(dib->hasMask())
   {
      unsigned int ax = abs(xOffset);
      unsigned int ay = abs(yOffset);

      // Throw out silly values

      ASSERT((xOffset != 0) || (yOffset != 0));
      ASSERT(ax < dib->getWidth());
      ASSERT(ay < dib->getHeight());

      if((xOffset == 0) && (yOffset == 0))
         return;


      if(ax >= dib->getWidth())
         return;
      if(ay >= dib->getHeight())
         return;


      ColourIndex mask = dib->getMask();

      UBYTE* bits = dib->getBits();
      int storageWidth = dib->getStorageWidth();

      int offset = xOffset + yOffset * storageWidth;

      int dx = 1;
      int w = dib->getWidth() - ax;

      if(xOffset < 0)
      {
         dx = -1;
         // bits += w;
         bits += dib->getWidth() - 1;  // Bugfix SWG: 9June1999 : Only works if xOffset=1
      }

      int dy = storageWidth;

      int h = dib->getHeight() - ay;

      if(yOffset < 0)
      {
         dy = -dy;
         // bits += (h - 1) * storageWidth;
         bits += (dib->getHeight() - 1) * storageWidth; // Bugfix SWG: 9Jun1999 (h-1 should have been h)
      }

      if(dx < 0)
         dy += w;
      else
         dy -= w;

      ASSERT(w > 0);
      ASSERT(h > 0);

      while(h--)
      {
         int x = w;
         while(x--)
         {
            if(bits[offset] != mask)
               bits[0] = mask;

            bits += dx;
         }

         bits += dy;
      }
   }
}

/*
 * New version.  Rule for color change to take place is:
 *    bits[offset] == mask
 *    bits[0] = non-mask
 *    bits[-offset] = non-mask
 * pixels off the edge are counted as mask.
 *
 * Also allow optional dib rectangle to speed things up.
 */

void colorEdge(DrawDIB* dib, int xOffset, int yOffset, ColourIndex index, const Rect<int>& rect)
{
   ASSERT(dib != 0);
   ASSERT(dib->hasMask());

   if(!dib->hasMask())
      return;

   LONG srcX = rect.left();
   LONG srcY = rect.top();
   LONG srcW = rect.width();
   LONG srcH = rect.height();

   if(!dib->clip(srcX, srcY, srcW, srcH))
      return;

   if( (srcW <= 2) || (srcH <= 2) )
      return;

   unsigned int ax = abs(xOffset);
   unsigned int ay = abs(yOffset);

   // Throw out silly values

   ASSERT((xOffset != 0) || (yOffset != 0));
   ASSERT(ax < srcW);
   ASSERT(ay < srcH);

   if((xOffset == 0) && (yOffset == 0))
      return;

   if(ax >= srcW)
      return;
   if(ay >= srcH)
      return;

   /*
    * Set up for loop
    */

   ColourIndex mask = dib->getMask();

   UBYTE* bits = dib->getBits();

   ASSERT(bits != 0);


   int storageWidth = dib->getStorageWidth();

   int offset = xOffset + yOffset * storageWidth;

   int dx = 1;
   int w = srcW;

   bits += srcX + srcY * storageWidth;

   if(xOffset < 0)
   {
      dx = -dx;
      w -= ax * 2;
      bits += srcW - ax - 1;
   }
   else if(xOffset > 0)
   {
      w -= ax * 2;
      bits += ax;
   }

   int dy = storageWidth;

   int h = srcH;

   if(yOffset < 0)
   {
      dy = -dy;
      h -= ay * 2;
      bits += (srcH - ay - 1) * storageWidth;
   }
   else if(yOffset > 0)
   {
      h -= ay * 2;
      bits += (ay * storageWidth);
   }

   ASSERT(w > 0);
   ASSERT(h > 0);

   while(h--)
   {
      int x = w;
      UBYTE* lineAd = bits;
      while(x--)
      {
         if( (bits[offset] == mask) && (bits[0] != mask) && (bits[-offset] != mask) )
            bits[0] = index;

         bits += dx;
      }

      bits = lineAd + dy;
   }

}


void colorEdge(DrawDIB* dib, int xOffset, int yOffset, ColourIndex index)
{
   colorEdge(dib, xOffset, yOffset, index, Rect<int>(0,0, dib->getWidth(), dib->getHeight()));
}


// void remapColor(DrawDIB* destDIB, const DIB* src, const UBYTE* remap, ColourIndex index, LONG destX, LONG destY, LONG w, LONG h, LONG srcX, LONG srcY)
void remapColor(DrawDIB* destDIB, const DIB* src, const ColorRemapTable* remap, ColourIndex index, LONG destX, LONG destY, LONG w, LONG h, LONG srcX, LONG srcY)
{

   LONG x = destX;
   LONG y = destY;
   if(destDIB->clip(x, y, w, h))
   {
      srcX += x - destX;   // adjust if any clipping
      srcY += y - destY;

      const UBYTE* srcLine = src->getAddress(srcX, srcY);
      UBYTE* destLine = destDIB->getAddress(x, y);

      LONG height = h;
      while(height--)
      {
         UBYTE* dest = destLine;
         const UBYTE* srcAd = srcLine;

         LONG width = w;
         while(width--)
         {
            if(*srcAd == index)
              *dest = (*remap)[*dest];

            dest++;
            srcAd++;
         }

         destLine += destDIB->getStorageWidth();
         srcLine += src->getStorageWidth();
      }
   }

}

/*
 * Local class to hold text mask DrawDIBDC
 *
 */

class TextMask {
    DrawDIBDC* d_textMask;
  public:
    TextMask() :
      d_textMask(0) {}
    ~TextMask()
    {
      if(d_textMask)
        delete d_textMask;
    }

    void allocateTextMask(const SIZE& s, HFONT hFont, const char* text);
    DrawDIBDC* textMask() const { return d_textMask; }
};

void TextMask::allocateTextMask(const SIZE& s, HFONT hFont, const char* text)
{
  ASSERT(text);

  if(!d_textMask ||
     (d_textMask->getWidth() < s.cx || d_textMask->getHeight() < s.cy))
  {
    int oldCX = 0;
    int oldCY = 0;

    if(d_textMask)
    {
      oldCX = d_textMask->getWidth();
      oldCY = d_textMask->getHeight();

      delete d_textMask;
      d_textMask = 0;
    }

    d_textMask = new DrawDIBDC(maximum(oldCX, s.cx), maximum(oldCY, s.cy));
    ASSERT(d_textMask != 0);

#ifdef DEBUG_DIBDC
    static int nAllocated = 0;
    static char buf[200];
    wsprintf(buf, "Text Mask #%d", ++nAllocated);
    d_textMask->setDebugText(buf);
#endif
  }

  if(hFont)
    d_textMask->setFont(hFont);
}


void draw3DText(DrawDIBDC* destDib, const Draw3DTextData& data) //HFONT hFont, const char* text, int x, int y, Draw3DTextHow::How drawHow, const int remapPercent)
{
  ASSERT(data.d_text);
  ASSERT(data.d_hFont);
  ASSERT(destDib);

// static instance
   static TextMask s_tm;

  /*
   * First determine size of text and allocate textmask if needed
   */

  SIZE s;
  GetTextExtentPoint32(destDib->getDC(), data.d_text, lstrlen(data.d_text), &s);

  s_tm.allocateTextMask(s, data.d_hFont, data.d_text);
  ASSERT(s_tm.textMask());

  ColourIndex ci = s_tm.textMask()->getColour(PALETTERGB(255, 255, 255));
  s_tm.textMask()->rect(0, 0, s.cx, s.cy, ci);
  s_tm.textMask()->setMaskColour(ci);

  wTextOut(s_tm.textMask()->getDC(), 0, 0, data.d_text);

  destDib->setMaskColour(ci);

  if(data.d_flags & Draw3DTextData::Imbedded)
  {
    if(data.d_flags & Draw3DTextData::Colorize)
    {
      DIB_Utility::colorize(destDib, s_tm.textMask(), data.d_color, data.d_remapPercent,
         data.d_x, data.d_y, s.cx, s.cy, 0, 0);
    }
    else
    {
      destDib->lighten(s_tm.textMask(), data.d_x+2, data.d_y+2, s.cx, s.cy, 0, 0, data.d_remapPercent);
      destDib->darken(s_tm.textMask(), data.d_x, data.d_y, s.cx, s.cy, 0, 0, data.d_remapPercent);
    }
  }
  else
  {
    destDib->lighten(s_tm.textMask(), data.d_x, data.d_y, s.cx, s.cy, 0, 0, data.d_remapPercent);
    destDib->darken(s_tm.textMask(), data.d_x+2, data.d_y+2, s.cx, s.cy, 0, 0, data.d_remapPercent);
  }
}


void drawGroupBox(DrawDIBDC* dib, HFONT hFont, const RECT& cRect,  const char* text, const CustomBorderInfo& bc)
{
  ASSERT(dib);

  RECT r = cRect;

  if(text)
  {

    HFONT oldFont = 0;

    if(hFont)
      oldFont = dib->setFont(hFont);

    SIZE s;
    GetTextExtentPoint32(dib->getDC(), text, lstrlen(text), &s);

    // adjust top of rect to bottom of text
    const int spacing = 5;
    int y = r.top-(s.cy+spacing);
    if(y < 0)
      y = 0;

    // return if out of bounds
    if(y < 0)
      return;

    HFONT font = (hFont != 0) ? hFont : reinterpret_cast<HFONT>(GetStockObject(SYSTEM_FONT));

    Draw3DTextData data;
    data.d_text = text;
    data.d_remapPercent = 70;
    data.d_hFont = font;
    data.d_x = r.left;
    data.d_y = y;
    data.d_flags = Draw3DTextData::Imbedded;

    DIB_Utility::draw3DText(dib, data);

    if(oldFont)
      dib->setFont(oldFont);

  }

  // return RECT is out of bounds
  if(r.left < 0 || r.top < 0 || r.right > dib->getWidth() || r.bottom > dib->getHeight())
    return;


  /*
   * Now draw our lines
   */

  ColourIndex c1 = dib->getColour(bc.colours[CustomBorderInfo::DarkBrown]);
  ColourIndex c2 = dib->getColour(bc.colours[CustomBorderInfo::OffWhite]);

  dib->frame(r.left, r.top, (r.right-r.left)-1, (r.bottom-r.top)-1, c1);
  dib->frame(r.left+1, r.top+1, (r.right-r.left)-1, (r.bottom-r.top)-1, c2);
}

Boolean allocateDib(DrawDIBDC** ppDib, const int cx, const int cy)
{
  DrawDIBDC* dib = *ppDib;

  if(!dib ||
     dib->getWidth() < cx ||
     dib->getHeight() < cy)
  {
    if(dib)
    {
      int oldCX = dib->getWidth();
      int oldCY = dib->getHeight();

      dib->resize(maximum(oldCX, cx), maximum(oldCY, cy));
      // delete dib;
      // dib = 0;
    }
    else
    {
       dib = new DrawDIBDC(cx, cy);
    }

    ASSERT(dib);

    *ppDib = dib;
    return True;
  }

  return False;
}

void drawInsert(DrawDIBDC* dib, const InsertData& data)
{
  ASSERT(dib);
  const LONG sectionCX = data.d_cx - data.d_shadowCX;
  const LONG sectionCY = data.d_cy - data.d_shadowCY;

  ColourIndex insertBottomBorder = dib->getColour(Palette::brightColour(data.d_cRef, 0, 128));
  ColourIndex insertTopBorder = dib->getColour(Palette::brightColour(data.d_cRef, 32, 255));

  if(data.d_mode == InsertData::Transparent)
  {
    // const UBYTE* insertRemapTable = Palette::getColorizeTable(data.d_cRef, 80);
    CPalette::RemapTablePtr insertRemapTable = dib->getPalette()->getColorizeTable(data.d_cRef, 80);
    dib->remapRectangle(insertRemapTable, data.d_x+1, data.d_y+1, sectionCX-2, sectionCY-2);

//     if(insertRemapTable)
//     {
//       Palette::releaseTable(insertRemapTable);
//       insertRemapTable = 0;
//     }
  }

  // const UBYTE* insertShadow = Palette::getDarkRemapTable(30);
  CPalette::RemapTablePtr insertShadow = dib->getPalette()->getDarkRemapTable(30);

  if(data.d_shadowCX > 0)
  {
    dib->remapRectangle(insertShadow, data.d_x+data.d_shadowCX, data.d_y+sectionCY, sectionCX, data.d_shadowCY);
    dib->remapRectangle(insertShadow, data.d_x+sectionCX, data.d_y+data.d_shadowCY, data.d_shadowCX, sectionCY-data.d_shadowCY);
  }

  dib->frame(data.d_x, data.d_y, sectionCX, sectionCY, insertTopBorder, insertBottomBorder);

//   if(insertShadow)
//   {
//     Palette::releaseTable(insertShadow);
//     insertShadow = 0;
//   }
}


}; // namespace DIB_Utility

