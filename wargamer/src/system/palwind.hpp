/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef PALWIND_HPP
#define PALWIND_HPP

#ifndef __cplusplus
#error palwind.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1995, Steven Morle-Green, All Rights Reserved
 *----------------------------------------------------------------------
 *
 *	Base class for Window that changes the palette to avoid
 * duplicating the onQueryNewPalette message handler
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 *
 *----------------------------------------------------------------------
 */

#include "sysdll.h"
#include "wind.hpp"
#include "custbord.hpp"

class PaletteWindow {
public:
	SYSTEM_DLL static BOOL handlePalette(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam, LRESULT& result);
protected:
	SYSTEM_DLL virtual ~PaletteWindow() = 0;
private:
};

/*
 *  Base class for windows that draw their own border
 *  Made this seperate class rather than part of CustomBkWindow
 *  since MainWind and MsgWin are not CustomBkWindows but need to draw their
 *  own borders
 *
 * You can get one of these from scenario, using:
 *			scenario->getBorderColors()
 */



class CustomBorderWindow {
	  	CustomBorderInfo d_info;

	public:
	  enum {
		 Seperator = 1,
		 ThinBorder = 2,
		 ThickBorder = 4
	  };

	  SYSTEM_DLL CustomBorderWindow(const CustomBorderInfo& info);

	  /*
		*  Following 4 functions draws a complete 4-sided border
		*/
	  SYSTEM_DLL void drawThickBorder(HDC hdc, RECT& r); // Draws 4 pixel border
	  SYSTEM_DLL void drawThinBorder(HDC hdc, RECT& r);  // Draws 2 pixel border
	  SYSTEM_DLL void drawSeperator(HDC hdc, RECT& r);   // Draws 1 pixel border
	  SYSTEM_DLL void draw3DSeperator(HDC hdc, RECT& r); // Draws 3d 1 pixel border

	  /*
		*  These functions draw a border on 1 side only
		*/
	  SYSTEM_DLL void drawTopSeperator(HDC hdc, RECT& r);
	  SYSTEM_DLL void drawLeftSeperator(HDC hdc, RECT& r);
};

/*
 *  A basic class for windows with our own background
 *
 */

class CustomBkWindow :
    public WindowBaseND,
    public PaletteWindow,
    public CustomBorderWindow 
{
	SYSTEM_DLL static const char s_className[];
	static ATOM classAtom;
protected:
	const DrawDIB* bkDib;
	DrawDIBDC* mainDIB;
public:
	SYSTEM_DLL CustomBkWindow(const CustomBorderInfo& info);
	SYSTEM_DLL ~CustomBkWindow();

	SYSTEM_DLL BOOL onEraseBk(HWND hwnd, HDC hdc);

	SYSTEM_DLL const char* className() const { return s_className; }
	// SYSTEM_DLL const char* getClassName() const { return className(); }
private:
    static ATOM registerClass(); // automatic
};


#endif /* PALWIND_HPP */

