/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *	Display a splash (intro) screen
 *
 *----------------------------------------------------------------------
 */
#include "stdinc.hpp"
#include "splash.hpp"

#include "wind.hpp"
// #include "bargraph.hpp"
#include "fonts.hpp"
#include "wmisc.hpp"
#include "app.hpp"
// #include "sounds.hpp"
#include "myassert.hpp"


//#include "wind.hpp"

//SplashWindow* splashWindow = 0;

class SplashWindow : public WindowBaseND {
	 static ATOM classAtom;
	 static const char s_className[];
	 HWND parent;
	 const char* d_text;
  public:
	 SplashWindow(HWND hParent);
	 ~SplashWindow();
	 static ATOM registerClass();
	 void destroy();
	 void setText(const char* text);
     const char* className() const { return s_className; }

  private:
	 LRESULT procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
	 BOOL onCreate(HWND hWnd, CREATESTRUCT* lpCreateStruct);
	 void onClose(HWND hWnd);
	 void onPaint(HWND hWnd);
//    BOOL onSetCursor(HWND hwnd, HWND hwndCursor, UINT codeHitTest, UINT msg);

};

const char SplashWindow::s_className[] = "SplashWindow";

ATOM SplashWindow::classAtom = 0;

ATOM SplashWindow::registerClass()
{
	if(!classAtom)
	{
		WNDCLASS wc;

		wc.style = CS_OWNDC | CS_DBLCLKS;
		wc.lpfnWndProc = baseWindProc;
		wc.cbClsExtra = 0;
		wc.cbWndExtra = sizeof(SplashWindow*);
		wc.hInstance = APP::instance();
		wc.hIcon = NULL;
		wc.hCursor = LoadCursor(NULL, IDC_WAIT);
		wc.hbrBackground = (HBRUSH) GetStockObject(BLACK_BRUSH); //(1 + COLOR_3DFACE);
		wc.lpszMenuName = NULL;
		wc.lpszClassName = s_className;
		classAtom = RegisterClass(&wc);
	}

	ASSERT(classAtom != 0);

	return classAtom;
}

SplashWindow::SplashWindow(HWND hParent)
  : d_text(0)
{
  registerClass();

  RECT r;
  GetClientRect(hParent, &r);

//  int w = GetSystemMetrics(SM_CXSCREEN);
//  int h = GetSystemMetrics(SM_CYSCREEN);

	HWND hWnd = createWindow(
		WS_EX_TOPMOST, 
        // className, 
        NULL,
		WS_CHILD | WS_VISIBLE | WS_MAXIMIZE, r.left, r.top, r.right-r.left, r.bottom-r.top,
		hParent, NULL       // , APP::instance()
	);

	ASSERT(hWnd != 0);

	SetWindowPos(hWnd, HWND_TOPMOST, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_SHOWWINDOW);
//   SetActiveWindow(hWnd);
//	UpdateWindow(hWnd);
}

SplashWindow::~SplashWindow()
{
    selfDestruct();
}

LRESULT SplashWindow::procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	switch(msg)
	{
		HANDLE_MSG(hWnd, WM_CREATE,	onCreate);
		HANDLE_MSG(hWnd, WM_PAINT, onPaint);
//		HANDLE_MSG(hWnd, WM_SETCURSOR, onSetCursor);

	default:
		return defProc(hWnd, msg, wParam, lParam);
	}
}

BOOL SplashWindow::onCreate(HWND hWnd, CREATESTRUCT* lpCreateStruct)
{

  return TRUE;
}

#if 0
BOOL SplashWindow::onSetCursor(HWND hwnd, HWND hwndCursor, UINT codeHitTest, UINT msg)
{
  static HCURSOR hCursor = 0;

  if(!hCursor)
  {
	 hCursor = LoadCursor(NULL, IDC_WAIT);
	 ASSERT(hCursor);
  }

  SetCursor(hCursor);

  return True;
}
#endif

void SplashWindow::onPaint(HWND hWnd)
{
  PAINTSTRUCT ps;

  HDC hdc = BeginPaint(hWnd, &ps);

  if(d_text)
  {
	 SIZE s;
	 GetTextExtentPoint32(hdc, d_text, lstrlen(d_text), &s);

	 RECT r;
	 GetClientRect(hWnd, &r);

	 const int x = (r.right-s.cx)/2;
	 const int y = (r.bottom-s.cy)/2;

	 COLORREF oldColor = SetTextColor(hdc, RGB(255, 255, 255));
	 ASSERT(oldColor != CLR_INVALID);

	 SetBkMode(hdc, TRANSPARENT);

	 wTextOut(hdc, x, y, d_text);

	 SetTextColor(hdc, oldColor);
  }

  EndPaint(hWnd, &ps);
}

void SplashWindow::destroy()
{
  ShowWindow(getHWND(), SW_HIDE);
  DestroyWindow(getHWND());
}

void SplashWindow::setText(const char* text)
{
  ASSERT(text);
  d_text = text;

  InvalidateRect(getHWND(), NULL, TRUE);
  UpdateWindow(getHWND());
}


/*======================================================
 * Public Functions
 */


SplashScreen::SplashScreen(HWND hParent) :
  d_splashWindow(new SplashWindow(hParent))
{
  ASSERT(d_splashWindow);
}

SplashScreen::~SplashScreen()
{
    // destroy();
    delete d_splashWindow;
}

void SplashScreen::destroy()
{
    delete d_splashWindow;
    d_splashWindow = 0;
  // if(d_splashWindow)
  // {
  // 	 d_splashWindow->destroy();
  // 	 d_splashWindow = 0;
  // }
}

void SplashScreen::setText(const char* text)
{
  ASSERT(d_splashWindow);

  if(d_splashWindow)
	 d_splashWindow->setText(text);
}


