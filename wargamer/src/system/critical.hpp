/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef CRITICAL_H
#define CRITICAL_H

#ifndef __cplusplus
#error critical.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1995, Steven Morle-Green, All Rights Reserved
 *----------------------------------------------------------------------
 *
 *	Base class for shared data
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  1996/06/22 19:04:48  sgreen
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#include "sysdll.h"
#include <windows.h>

// #define DEBUG_CRITICAL

class SharedData {
	mutable CRITICAL_SECTION critic;

#ifdef DEBUG_CRITICAL
	int id;
	int count;
#endif

public:
	SYSTEM_DLL SharedData();
	SYSTEM_DLL virtual ~SharedData();

	SYSTEM_DLL void enter() const;
	SYSTEM_DLL void leave() const;
};

/*
 * Class for automatic call of leave
 *
 * Usage is:
 *		{
 *		  LockData lock(&dataStructure);
 *		  do things to dataStructure
 *		}
 *		// Destructor automatically called when out of scope
 *
 * Note the bodge where a constant value is assigned to a non-constant
 * This is intentional, as it means LockData can be used in a class
 * function which is otherwise constant.
 */

class LockData {
	const SharedData* data;
public:
	LockData(const SharedData* d) : data(d) { d->enter(); }
	// LockData(const SharedData* d) { data = (SharedData*) d; data->enter(); }
	~LockData() { data->leave(); }
};




/*
 * Shared Data Item
 */

template<class T>
class Shared
{
	public:
		Shared() : d_value() { }
		Shared(T v) : d_value(v) { }

		T get() const
		{
			d_lock.enter();
			T value = d_value;
			d_lock.leave();
			return value;
		}

		void set(T v)
		{
			d_lock.enter();
			d_value = v;
			d_lock.leave();
		}

		operator T() const { return get(); }

		Shared<T>& operator = (T v)
		{
			set(v);
			return *this;
		}

		Shared<T>& operator += (T v)
		{
			d_lock.enter();
			d_value += v;
			d_lock.leave();
			return *this;
		}

		Shared<T>& operator -= (T v)
		{
			d_lock.enter();
			d_value -= v;
			d_lock.leave();
			return *this;
		}

		T getAndClear()
		{
			d_lock.enter();
			T value = d_value;
			d_value = 0;
			d_lock.leave();
			return value;
		}

	private:
		T d_value;
		SharedData d_lock;
};


#endif /* CRITICAL_H */

