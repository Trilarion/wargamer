/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#include "stdinc.hpp"
#include "mciwind.hpp"
#include "myassert.hpp"
#include "app.hpp"
#include <vfw.h>


HWND MCIWindow::create(HWND parent, const char* fileName)
{
  ASSERT(fileName != 0);

  HWND hwnd = MCIWndCreate(parent, APP::instance(),
				  WS_CHILD | WS_VISIBLE | WS_MAXIMIZE |
				  MCIWNDF_NOAUTOSIZEWINDOW | MCIWNDF_NOMENU |
				  MCIWNDF_NOOPEN | MCIWNDF_NOPLAYBAR | MCIWNDF_NOTIFYMODE,
				  fileName);

  ASSERT(hwnd != 0);
  return hwnd;
}

void

MCIWindow::play()
{
  ASSERT(hwnd != 0);
  if(hwnd != 0)
  {
	 MCIWndPlay(hwnd);
  }
}

void

MCIWindow::play(const char* fileName)
{
  ASSERT(hwnd != 0);
  if(hwnd != 0)
  {
	 MCIWndOpen(hwnd, fileName, NULL);
	 MCIWndPlay(hwnd);
  }
}

void MCIWindow::closeDevice()
{
  ASSERT(hwnd != 0);
  if(hwnd != 0)
  {
	 /*
	  * MCIWndClose closes the file or device associated with the MCIWindow
	  * but not the window itself
	  */

	 MCIWndClose(hwnd);
  }
}

void MCIWindow::rewind()
{
  ASSERT(hwnd != 0);
  if(hwnd != 0)
  {
	 LONG pos = MCIWndGetStart(hwnd);
	 pos = MCIWndSeek(hwnd, pos);
	 ASSERT(pos == 0);
  }
}

void 

MCIWindow::kill()
{
  ASSERT(hwnd != 0);
  if(hwnd != 0)
  {
	 MCIWndDestroy(hwnd);
  }
}


