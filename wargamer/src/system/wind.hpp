/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef WIND_H
#define WIND_H

#ifndef __cplusplus
#error wind.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1995, Steven Morle-Green, All Rights Reserved
 *----------------------------------------------------------------------
 *
 *	Base Class for handling Windows call back functions
 *
 *----------------------------------------------------------------------
 */

#include "sysdll.h"
#include <windows.h>
#include <windowsx.h>
#include "mytypes.h"
#include "myassert.hpp"
#include "winerror.hpp"

// #include "refptr.hpp"

class DrawDIB;
class DrawDIBDC;

/*
 * Message cracker for WM_HELP & WM_CONTEXTMENU
 */

#if !defined(HANDLE_WM_HELP)

// BOOL onHelp(HWND hwnd, (LPHELPINFO) lParam);
#define HANDLE_WM_HELP(hwnd, wParam, lParam, fn) \
		(BOOL)(fn)((hwnd), (LPHELPINFO)(lParam))

#endif

#if !defined(HANDLE_WM_CONTEXTMENU)

// void onContextMenu(HWND hwnd, HWND hwndCtl, int x, int y);
#define HANDLE_WM_CONTEXTMENU(hwnd, wParam, lParam, fn) \
		((fn)((hwnd), (HWND)(wParam), (int)LOWORD(lParam), (int)HIWORD(lParam)), 0L)

#endif

/*
 * Virtual Class that acts like a window
 */

class Window
{
    public:
        virtual ~Window() = 0;

	    virtual HWND getHWND() const = 0;
        virtual bool isVisible() const = 0;
        virtual bool isEnabled() const = 0;
        virtual void show(bool visible) = 0;
        virtual void enable(bool enable) = 0;
        virtual void toggle() { enable(!isEnabled()); }
};

inline Window::~Window() { }

/*
 * Flags for saving state to registry
 */

enum {
   WSAV_POSITION = 1,		// Save Position
   WSAV_SIZE	  = 2,		// Save Size
   WSAV_MINIMIZE = 4,		// Save whether it is minimized/maximized/restored
   WSAV_RESTORE  = 8,		// Save Restore Min/Max
   WSAV_ENABLED  = 0x10
};

typedef UBYTE WSAV_FLAGS;

/*
 * Class for anything with a HWND
 */

class HWNDbase : public Window {
    public:
	    HWNDbase() : hWnd(NULL), d_enabled(true), d_shown(false) { }
	    HWNDbase(HWND h) : hWnd(h), d_enabled(true), d_shown(false) { }
	    SYSTEM_DLL virtual ~HWNDbase();

	    SYSTEM_DLL void selfDestruct();
		    // Derived destructor should call this to get the window destroyed

	    HWND getHWND() const { return hWnd; }
	    HINSTANCE getInstance() const { return reinterpret_cast<HINSTANCE>(GetWindowLong(hWnd, GWL_HINSTANCE)); }

	    SYSTEM_DLL bool notifyParent();

        SYSTEM_DLL bool isVisible() const;
        bool isEnabled() const { return d_enabled; }
        SYSTEM_DLL void enable(bool enable);
        SYSTEM_DLL void show(bool visible);
        SYSTEM_DLL void saveState(const char* regKey, WSAV_FLAGS flags) const;
        SYSTEM_DLL bool getState(const char* regKey, WSAV_FLAGS flags, bool defValue=true);
            // return true if it was restored

	    struct SYSTEM_DLL ErrorCreateWindow : public WinError { ErrorCreateWindow(const char* s) : WinError(s) { } };

    protected:
	    HWND hWnd;			// Handle, or set to NULL when destroyed

    private:
        bool d_enabled;
        bool d_shown;

        static const char s_regEnabled[];
        static const char s_regPosition[];
        static const char s_regSize[];
        static const char s_regShow[];
};

/*
 * Common parts of WindowBase and WindowBaseND
 */

class CommonWindowBase : public HWNDbase
{
	public:
		CommonWindowBase() { }
		virtual ~CommonWindowBase() { destroy(); }

		/*
	 	 * Overload CreateWindow function
	 	 * to make sure that this pointer is passed to it.
	 	 * Calling it with the derived base's "this" does not work.
	 	 */

		SYSTEM_DLL HWND createWindow(
			DWORD  dwExStyle,
			// LPCTSTR  lpClassName,
			LPCTSTR  lpWindowName,
			DWORD  dwStyle,
			int  x,
			int  y,
			int  nWidth,
			int  nHeight,
			HWND  hWndParent,
			HMENU  hMenu);
			// HINSTANCE  hInstance);

        void destroy()
        {
            if(getHWND())
                DestroyWindow(getHWND());
        }

        virtual const char* className() const = 0;

	protected:
		// SYSTEM_DLL virtual LRESULT defProc(HWND h, UINT m, WPARAM w, LPARAM l) { return DefWindowProc(h, m, w, l); }
		SYSTEM_DLL virtual LRESULT defProc(HWND h, UINT m, WPARAM w, LPARAM l);
	protected:
		SYSTEM_DLL virtual LRESULT procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);

};

#if 0
/*
 * This is used by:
 *		register a class with:
 *			wc.cbWndExtra = sizeof(WindowBase*)
 *			wc.lpfnWndProc = baseWindProc;
 *
 *		Set up a window setting the lpParam as a pointer to a WindowBase
 *
 *	e.g.
 *
 * WindowBase* newWindow = new WindowBase;
 *
 *	HWND hWnd = CreateWindow(
 *		szMapClass,
 *			"MyClass",
 *			WS_CHILD | WS_CAPTION | WS_BORDER | WS_SYSMENU | WS_VISIBLE | WS_MINIMIZEBOX,
 *			10,
 *			10,
 *			128,
 *			128,
 *			parent,
 *			(HMENU) WID_MyID,
 *			instance,
 *			newWindow
 *		);
 *
 */


class WindowBase : public CommonWindowBase {
	// friend class GenericClass;
	// friend class GenericNoBackClass;

public:
	WindowBase() { }
	virtual ~WindowBase() { }

protected:
	SYSTEM_DLL static LRESULT CALLBACK baseWindProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
};
#endif

/*
 * WM_USER_WINDOWDESTROYED has:
 *      wParam.hi = child id of window
 *      lParam    = hwnd of window to be destroyed
 * Receiver should return TRUE if it wants the window deleted
 *
 * bool onUserWindowDestroyed(HWND hwnd, HWND hChild, int idChild);
 */

#define WM_USER_WINDOWDESTROYED (WM_USER+400)

#define HANDLE_WM_USER_WINDOWDESTROYED(hwnd, wParam, lParam, fn) \
    (LRESULT)(bool)((fn)((hwnd), HWND(lParam), HIWORD(wParam)))

inline bool send_WM_USER_WINDOWDESTROYED(HWND hwnd, HWND hChild, int id)
{
    return SendMessage(hwnd, WM_USER_WINDOWDESTROYED, MAKEWPARAM(0, id), LPARAM(hChild)) != 0;
}

/*
 * Version of WindowBase that does not delete itself when window closes
 * owner can use isValid() function to determine if window has been
 * created yet.
 */

class WindowBaseND :
	public CommonWindowBase
{
	// Disable copying

	WindowBaseND(const WindowBaseND&);
	WindowBaseND& operator=(const WindowBaseND&);

public:
	SYSTEM_DLL WindowBaseND();
	SYSTEM_DLL virtual ~WindowBaseND();

	bool isValid() const { return getHWND() != NULL; }

    SYSTEM_DLL virtual const char* className() const;

protected:
	SYSTEM_DLL static LRESULT CALLBACK baseWindProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
};

/*
 * Window with background
 */

class WindowWithBackGround : public WindowBaseND
{
    public:
        SYSTEM_DLL virtual const char* className() const;
};


/*
 * Add this to a window class if it is a child
 */

class ChildWindow {
protected:
	SYSTEM_DLL int onMouseActivate(HWND hwnd, HWND hwndTopLevel, UINT codeHitTest, UINT msg);
};

SYSTEM_DLL void alterWindowSize(HWND hWnd, LONG lWidth, LONG lHeight, CREATESTRUCT* lpCreateStruct);


/*
 * A Basic Class to use with Popup Windows
 */

class PopupWindow : public WindowWithBackGround
{
    public:
        PopupWindow() { }
        ~PopupWindow() { selfDestruct(); }
};

#if 0
class PopupWindow : public WindowBaseND {
public:
	PopupWindow() { }
	SYSTEM_DLL ~PopupWindow() { selfDestruct(); }
	SYSTEM_DLL const char* className() const;
};
#endif


/*
 * Window Subclassing Class
 * There are 2 ways of using this:
 *		1) Create a Window as usual, and then construct a derived class using
 *			the HWND.
 *		2) Construct without any parameters, but call init with a HWND later on.
 *
 * The derived class will work similar to a Window, in that a procMessage
 * is called.  Unhandled messages should be passed on to defProc.
 */

class SubClassWindowBase
{
	WNDPROC d_defProc;		// Default Window Procedure
   LONG d_defData;         // Default Window Data. Usually 0 unless in a nested subclass
	// Disallow copying
 	SubClassWindowBase(const SubClassWindowBase&);
 	SubClassWindowBase& operator = (const SubClassWindowBase&);

 public:
 	SYSTEM_DLL SubClassWindowBase();					// Initialise to NULL values
 	SubClassWindowBase(HWND hwnd) { init(hwnd); }		// Subclass a given window
	SYSTEM_DLL void init(HWND hwnd);
	SYSTEM_DLL void clear(HWND hwnd);

	virtual ~SubClassWindowBase() { }

	SYSTEM_DLL LRESULT defProc(HWND h, UINT m, WPARAM w, LPARAM l);
		// Call default procedure

 protected:
	WNDPROC defProc() const { return d_defProc; }

 private:
	SYSTEM_DLL virtual LRESULT procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
		// Derived Class defined message processor

	static LRESULT CALLBACK subClassProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam);
		// Replacement Procedure
};

class SubClassWindow :
	public SubClassWindowBase,
	public HWNDbase
{
	public:
		SubClassWindow(HWND h) : SubClassWindowBase(h), HWNDbase(h) { }
		~SubClassWindow() { SubClassWindowBase::clear(getHWND()); hWnd = 0; }
};


/*
 * Useful class for hiding and restorng windows
 *
 * show() makes a window logically shown or hidden
 * suspend() physically hides or unhides
 */

#if 0
class SuspendableWindow
{
	public:
		SYSTEM_DLL SuspendableWindow();
		SYSTEM_DLL void suspend(bool visible);
        SYSTEM_DLL void show(bool visible);
        void toggle() { show(!isShown()); }

        bool isVisible() const { return d_unsuspended && d_shown; }
        bool isShown() const { return d_shown; }

        void saveState(const char* regKey) const;
        void getState(const char* regKey, bool defValue);

	private:
		virtual HWND getHWND() const = 0;
        void show();
	private:
		bool d_unsuspended;     // Window is suspended
        bool d_shown;           // User enabled
        static const char s_regName[];
};
#endif





#endif /* WIND_H */

/*---------------------------------------------------------------
 * $Log$
 *---------------------------------------------------------------
 */
