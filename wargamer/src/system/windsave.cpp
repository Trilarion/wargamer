/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1995, Steven Morle-Green, All Rights Reserved
 *----------------------------------------------------------------------
 *
 *	Save and restore Window States
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/06/13 08:52:39  greenius
 * Initial Import
 *
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "windsave.hpp"
#include "registry.hpp"
#include "myassert.hpp"

struct WindowState {
	UINT flags;
	UINT showCmd;
	RECT position;
};


BOOL

restoreWindowState(const char* name, HWND hWnd, WSAV_FLAGS flags, const char* subDir)
{
#ifdef DEBUG
	debugLog("restoreWindowState(%s, %p)\n", name, hWnd);
#endif

	/*
	 * Modify some mutual flags
	 */

	if(flags & WSAV_SIZE)
		flags |= WSAV_POSITION;
	if(flags & WSAV_RESTSIZE)
		flags |= WSAV_MINMAX;



	WINDOWPLACEMENT wp;
	wp.length = sizeof(WINDOWPLACEMENT);
	GetWindowPlacement(hWnd, &wp);

	WindowState ws;
	if(getRegistry(name, &ws, sizeof(ws), subDir))
	{
		if(flags & WSAV_MINMAX)
		{
			wp.flags = ws.flags;
			wp.showCmd = ws.showCmd;
		}

		if(flags & WSAV_SIZE)
			wp.rcNormalPosition = ws.position;
		else if(flags & WSAV_POSITION)
		{
			wp.rcNormalPosition.right = ws.position.left + wp.rcNormalPosition.right - wp.rcNormalPosition.left;
			wp.rcNormalPosition.bottom = ws.position.top + wp.rcNormalPosition.bottom - wp.rcNormalPosition.top;
			wp.rcNormalPosition.left = ws.position.left;
			wp.rcNormalPosition.top = ws.position.top;
		}

#ifdef DEBUG
		debugLog("+WindowState: flags=%u, showCmd=%u, position=%ld,%ld,%ld,%ld\n",
			ws.flags,
			ws.showCmd,
			ws.position.left,
			ws.position.top,
			ws.position.right,
			ws.position.bottom);
#endif

		SetWindowPlacement(hWnd, &wp);

#ifdef DEBUG
		debugLog("-WindowState: finish\n");
#endif

		return TRUE;
	}

	return FALSE;
}

BOOL

saveWindowState(const char* name, HWND hWnd, WSAV_FLAGS flags, const char* subDir)
{
	WINDOWPLACEMENT wp;

	wp.length = sizeof(WINDOWPLACEMENT);
	GetWindowPlacement(hWnd, &wp);
	WindowState ws;
	ws.flags = wp.flags;
	ws.showCmd = wp.showCmd;
	ws.position = wp.rcNormalPosition;
	return setRegistry(name, &ws, sizeof(ws), subDir);
}



