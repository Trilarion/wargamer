/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#include "stdinc.hpp"
#define INITGUID
#include "DirectPlay.hpp"
#include <stdio.h>
#include <cguid.h>
#ifdef DEBUG
#include "clog.hpp"
LogFile directPlayLog("directplay.log");
#endif
/*

  Filename : DirectPlay.hpp

  Description : Class to encapsulate DPLAY object, and setup process

*/

CDirectPlay * g_directPlay = NULL;

LPDIRECTPLAY4 CDirectPlay::s_directPlay = NULL;
LPGUID CDirectPlay::d_enumConnectionsGUID;
DPlayConnectionDesc * CDirectPlay::d_selectedConnection;
SListS<DPlayConnectionDesc> CDirectPlay::d_connectionsList;
LPGUID CDirectPlay::d_enumSessionsApplicationGUID;
DPlaySessionDesc * CDirectPlay::d_selectedSession;
SListS<DPlaySessionDesc> CDirectPlay::d_sessionsList;
SListS<DPlayPlayerDesc> CDirectPlay::d_playersList;
char CDirectPlay::s_msgEventName[] = "DIRECTPLAY_MSG_THREAD";



CDirectPlay::CDirectPlay(void) {

	#ifdef DEBUG
	directPlayLog.printf("Creating CDirectPlay\n");
	#endif

   ASSERT(s_directPlay == NULL);


	s_directPlay = NULL;
	d_connectionType = NotConnected;
	d_user = NULL;

	d_enumConnectionsGUID = NULL;
	d_enumSessionsApplicationGUID = NULL;

	d_connectionsList.reset();
	d_selectedConnection = NULL;
	d_sessionsList.reset();
	d_selectedSession = NULL;

	d_msgEvent = 0;
	d_msgThread = 0;

	d_receiveBuffer = NULL;
	d_receiveBufferLength = 0;

	CoInitialize(NULL);
//	createObject();

}

CDirectPlay::~CDirectPlay(void) {

	#ifdef DEBUG
	directPlayLog.printf("Destroying CDirectPlay\n");
	#endif

	destroyObject();

	CoUninitialize();

}

bool
CDirectPlay::registerUser(CDirectPlayUser * user) {

	#ifdef DEBUG
	directPlayLog.printf("Registering CDirectPlayUser user\n");
	#endif

	d_user = user;
	return true;
}


LPDIRECTPLAY4 CDirectPlay::instance()
{
   if(!s_directPlay)
      createObject();
   return s_directPlay;
}

bool
CDirectPlay::createObject(void) {

   ASSERT(!s_directPlay);

	if(s_directPlay) return false;

	HRESULT retval;

	retval = CoCreateInstance(
		CLSID_DirectPlay,
		NULL,
		CLSCTX_INPROC_SERVER,
		IID_IDirectPlay4A,
		(LPVOID*)&s_directPlay
	);

	if(retval != DP_OK) { reportError("CoCreateInstance failed", retval); return false; }
	else return true;
}


bool
CDirectPlay::destroyObject(void) {

	if(!s_directPlay) return false;

	HRESULT retval;

	retval = s_directPlay->Release();
	s_directPlay = NULL;

	if(retval != DP_OK) { reportError("Release DirectPlay failed", retval); return false; }
	else return true;
}



bool
CDirectPlay::enumerateConnections(void) {

#ifdef DEBUG
	directPlayLog.printf("Enumerating connections :\n");
#endif

	HRESULT retval;

	d_connectionsList.reset();

	retval = instance()->EnumConnections(
		d_enumConnectionsGUID,
		(LPDPENUMCONNECTIONSCALLBACK) enumConnectionsCallback,
		this,
		DPCONNECTION_DIRECTPLAY | DPCONNECTION_DIRECTPLAYLOBBY
	);

	if(retval != DP_OK) { reportError("Enumerating Connections", retval); return false; }
	else return true;
}


BOOL FAR PASCAL
CDirectPlay::enumConnectionsCallback(
	LPCGUID lpguidSP,
	LPVOID lpConnection,
	DWORD dwConnectionSize,
	LPCDPNAME lpName,
	DWORD dwFlags,
	LPVOID lpContext) {

	CDirectPlay * objptr = (CDirectPlay *) lpContext;
	DPlayConnectionDesc * desc = new DPlayConnectionDesc;

	memcpy(&desc->Guid, lpguidSP, sizeof(GUID));

	desc->Connection = malloc(dwConnectionSize);
	desc->ConnectionSize = dwConnectionSize;
	memcpy(desc->Connection, lpConnection, dwConnectionSize);

	desc->Name = strdup((const char *)lpName->lpszShortNameA);
	desc->Flags = dwFlags;
	desc->InitializedOK = false;

	objptr->d_connectionsList.append(desc);

#ifdef DEBUG
	directPlayLog.printf("+ %s", desc->Name);
#endif

	return TRUE;
}


bool
CDirectPlay::connect(void) {

	#ifdef DEBUG
	directPlayLog.printf("Connecting to %s service\n", d_selectedConnection->Name);
	#endif

	HRESULT retval;

	retval = instance()->InitializeConnection(
		d_selectedConnection->Connection,
		0
	);

	if(retval == DP_OK || retval == DPERR_ALREADYINITIALIZED) {
		d_selectedConnection->InitializedOK = true;
		return true;
	}
	else {
		d_selectedConnection->InitializedOK = false;
		return false;
	}
}


bool
CDirectPlay::enumerateSessions(void) {

#ifdef DEBUG
	directPlayLog.printf("Enumerating sessions :\n");
#endif

	DPSESSIONDESC2 desc;
	ZeroMemory(&desc, sizeof(DPSESSIONDESC2));
	desc.dwSize = sizeof(DPSESSIONDESC2);
	desc.guidApplication = GUID_NULL;
	desc.lpszPasswordA = NULL;

	HRESULT retval;

	d_sessionsList.reset();

	retval = instance()->EnumSessions(
		&desc,
		1000,
		enumSessionsCallback,
		this,
		DPENUMSESSIONS_ALL | DPENUMSESSIONS_PASSWORDREQUIRED
	);

	if(retval != DP_OK) { return false; }
	else return true;
}

BOOL FAR PASCAL
CDirectPlay::enumSessionsCallback(
	LPCDPSESSIONDESC2 lpThisSD,
	LPDWORD lpdwTimeOut,
	DWORD dwFlags,
	LPVOID lpContext) {

	if(dwFlags & DPESC_TIMEDOUT) return FALSE;

	CDirectPlay * objptr = (CDirectPlay *) lpContext;

	DPlaySessionDesc * desc = new DPlaySessionDesc;

	desc->Desc = strdup(lpThisSD->lpszSessionNameA);

 	memcpy(&desc->SessionGuid, &lpThisSD->guidInstance, sizeof(GUID));
	memcpy(&desc->ApplicationGuid, &lpThisSD->guidApplication, sizeof(GUID));

	desc->MaxPlayers = lpThisSD->dwMaxPlayers;
	desc->NumPlayers = lpThisSD->dwCurrentPlayers;
	desc->Flags = lpThisSD->dwFlags;

	objptr->d_sessionsList.append(desc);

#ifdef DEBUG
	directPlayLog.printf("+ %s", desc->Desc);
#endif

	return TRUE;
}


bool
CDirectPlay::createSession(DPlaySessionDesc * desc, SessionCaps * caps) {

	#ifdef DEBUG
	directPlayLog.printf("Hosting a new session\n");
	#endif

	DPSESSIONDESC2 s_desc;
	ZeroMemory(&s_desc, sizeof(DPSESSIONDESC2));
	s_desc.dwSize = sizeof(DPSESSIONDESC2);
	memcpy(&s_desc.guidApplication, &d_enumSessionsApplicationGUID, sizeof(GUID));
	s_desc.dwMaxPlayers = desc->MaxPlayers;
	s_desc.dwCurrentPlayers = 0;
	s_desc.lpszSessionNameA = desc->Desc;
	s_desc.lpszPasswordA = desc->Password;

	s_desc.dwFlags = caps->ToDWORD();

	HRESULT retval;

	retval = instance()->Open(
		&s_desc,
		DPOPEN_CREATE
	);

	if(retval != DP_OK) { reportError("Open Session for hosting", retval); return false; }
	else return true;
}


bool
CDirectPlay::joinSession(void) {

	#ifdef DEBUG
	directPlayLog.printf("Joining an existing session\n");
	#endif

	DPSESSIONDESC2 s_desc;
	ZeroMemory(&s_desc, sizeof(DPSESSIONDESC2));
	s_desc.dwSize = sizeof(DPSESSIONDESC2);
	s_desc.dwFlags = 0;
	memcpy((LPVOID)&s_desc.guidInstance, (LPVOID)&d_selectedSession->SessionGuid, sizeof(GUID));


	HRESULT retval;

	retval = instance()->Open(
		&s_desc,
		DPOPEN_JOIN
	);

	if(retval != DP_OK) { reportError("Open Session for joining", retval); return false; }
	else return true;
}



bool
CDirectPlay::closeSession(void) {

	#ifdef DEBUG
	directPlayLog.printf("Closing session\n");
	#endif

	HRESULT retval;

	retval = instance()->Close();

	if(retval != DP_OK) { reportError("Couldn't close session", retval); return false; }
	else return true;
}




bool
CDirectPlay::createPlayer(void) {

	#ifdef DEBUG
	directPlayLog.printf("Creating player\n");
	#endif

	HRESULT retval = instance()->CreatePlayer(
		&d_localPlayerID,
		NULL,
		d_msgEvent,
		NULL, 0,
		0
	);

	if(retval != DP_OK) { reportError("Couldn't create player", retval); return false; }
	else return true;
}



bool
CDirectPlay::destroyPlayer(void) {

	#ifdef DEBUG
	directPlayLog.printf("Destroying player\n");
	#endif

	HRESULT retval;

	retval = instance()->DestroyPlayer(d_localPlayerID);

	if(retval != DP_OK) { reportError("Couldn't destroy player", retval); return false; }
	else return true;
}



bool
CDirectPlay::enumeratePlayers(void) {

#ifdef DEBUG
	directPlayLog.printf("Enumerating players :\n");
#endif

	d_localPlayerID = 0;
	d_remotePlayerID = 0;

	HRESULT retval;

	/*
	Enumerate local players
	*/
	d_playersList.reset();
	retval = instance()->EnumPlayers(
		NULL,
		enumPlayersCallback,
		this,
		DPENUMPLAYERS_LOCAL
	);
	if(retval != DP_OK) { reportError("Enumerate Local Players", retval); return false; }

	DPlayPlayerDesc * local_desc = d_playersList.first();
	d_localPlayerID = local_desc->Id;

	/*
	Enumerate local players
	*/
	d_playersList.reset();
	retval = instance()->EnumPlayers(
		NULL,
		enumPlayersCallback,
		this,
		DPENUMPLAYERS_REMOTE
	);
	if(retval != DP_OK) { reportError("Enumerate Remote Players", retval); return false; }

	DPlayPlayerDesc * remote_desc = d_playersList.first();
	d_remotePlayerID = remote_desc->Id;

	/*
	Enumerate all players for the static list
	*/
	d_playersList.reset();
	retval = instance()->EnumPlayers(
		NULL,
		enumPlayersCallback,
		this,
		DPENUMPLAYERS_ALL
	);
	if(retval != DP_OK) { reportError("Enumerate Local Players", retval); return false; }

	return true;
}



BOOL FAR PASCAL
CDirectPlay::enumPlayersCallback(
	DPID dpId,
	DWORD dwPlayerType,
	LPCDPNAME lpName,
	DWORD dwFlags,
	LPVOID lpContext) {

	CDirectPlay * objptr = (CDirectPlay *) lpContext;

	DPlayPlayerDesc * desc = new DPlayPlayerDesc;

	desc->Id = dpId;
	desc->Flags = dwFlags;

	objptr->d_playersList.append(desc);

#ifdef DEBUG
	char tmp[1024];
	sprintf(tmp, "+ %i ", desc->Id);
	strcat(tmp, "(");
	if(dwFlags & DPENUMPLAYERS_LOCAL) strcat(tmp, "LOCAL ");
	if(dwFlags & DPENUMPLAYERS_REMOTE) strcat(tmp, "REMOTE ");
	if(dwFlags & DPENUMPLAYERS_SESSION ) strcat(tmp, "SESSION ");
	if(dwFlags & DPENUMPLAYERS_SPECTATOR  ) strcat(tmp, "SPECTATOR ");
	strcat(tmp, ")");

	directPlayLog.printf(tmp);

#endif

	return TRUE;
}




bool
CDirectPlay::sendMessage(LPVOID data, DWORD data_length) {

	#ifdef DEBUG
	directPlayLog.printf("Sending GameMsg\n");
	#endif

	HRESULT retval;

	retval = instance()->SendEx(
		d_localPlayerID,
		d_remotePlayerID,
		DPSEND_ASYNC | DPSEND_GUARANTEED,
		data,
		data_length,
		0, // priority of zero
		0, // no timeout
		this, // context ptr
		NULL // pointer to DWORD to receive DPlay MsgID for async completiong msg
	);

	if(retval == DPERR_PENDING) {

		// this is here as a temporary debugging measure
		bool message_not_sent_yet = true;

	}
	else if(retval != DP_OK) { reportError("SendEx ASYNC", retval); return false; }

	return true;
}




bool
CDirectPlay::createMsgThread(void) {

	#ifdef DEBUG
	directPlayLog.printf("Creating msg event\n");
	#endif

	d_msgEvent = CreateEvent(
		NULL,
		TRUE, // manual reset required
		FALSE, // initial state is nonsignaled
		s_msgEventName
	);

	if(!d_msgEvent) { reportError("ERROR - couldn't create message event.\n", 0); return FALSE; }


	#ifdef DEBUG
	directPlayLog.printf("Creating msg thread\n");
	#endif

	d_msgThread = CreateThread(
		NULL,
		0,
		(LPTHREAD_START_ROUTINE) msgThreadFunction,
		(LPVOID) this, // DWORD argument
		0, // creation flags
		&d_msgThreadID
	);

	if(!d_msgThread) { reportError("ERROR - couldn't create message thread.\n", 0); return FALSE; }
	else return TRUE;
}




bool
CDirectPlay::destroyMsgThread(void) {

	#ifdef DEBUG
	directPlayLog.printf("Creating msg thread & event\n");
	#endif


	if(d_msgThread) CloseHandle(d_msgThread);
	d_msgThread = 0;

	if(d_msgEvent) CloseHandle(d_msgEvent);
	d_msgEvent = 0;

	return true;
}


bool
CDirectPlay::createReceiveBuffer(DWORD size) {

	#ifdef DEBUG
	directPlayLog.printf("Creating receive buffer for %i bytes\n", size);
	#endif

	if(d_receiveBuffer) delete[] d_receiveBuffer;

	d_receiveBuffer = new char[size];
	d_receiveBufferLength = size;

	return true;
}


bool
CDirectPlay::destroyReceiveBuffer(void) {

	#ifdef DEBUG
	directPlayLog.printf("Destroying receive buffer\n");
	#endif

	if(d_receiveBuffer) delete[] d_receiveBuffer;
	d_receiveBuffer = 0;
	d_receiveBufferLength = 0;

	return true;
}



DWORD WINAPI
CDirectPlay::msgThreadFunction(LPVOID lpParam) {

	CDirectPlay * objptr = (CDirectPlay *) (lpParam);

	bool finished = false;

	while(!finished) {

		DWORD hwait = WaitForSingleObject(objptr->d_msgEvent, INFINITE);

		bool all_messages_read = false;

		do {

			DPID fromID;
			DPID toID;
			DWORD data_length = objptr->d_receiveBufferLength;
			bool receive_ok;

			do {

				receive_ok = true;
				toID = objptr->d_localPlayerID;

				HRESULT retval = objptr->instance()->Receive(
					&fromID,
					&toID,
					DPRECEIVE_TOPLAYER,
					(LPVOID) objptr->d_receiveBuffer,
					&data_length
				);

				if(retval == DPERR_NOMESSAGES) all_messages_read = true;

				if(retval == DPERR_BUFFERTOOSMALL) {
					objptr->createReceiveBuffer(data_length);
					receive_ok = false;
				}

			} while(!receive_ok);


			if(!all_messages_read) {

				if(fromID == DPID_SYSMSG) {

					DPMSG_GENERIC * generic_msg = (DPMSG_GENERIC *) objptr->d_receiveBuffer;
					finished = (!(objptr->processSystemMessage(generic_msg)));
				}

				else {

					finished = (!(objptr->processGameMessage(objptr->d_receiveBuffer, data_length)));
				}
			}

		} while(!all_messages_read);

		ResetEvent(objptr->d_msgEvent);
	}

	return 1;
}





bool
CDirectPlay::processSystemMessage(DPMSG_GENERIC * generic_msg) {

	switch(generic_msg->dwType) {

		case DPSYS_ADDGROUPTOGROUP : {

			DPMSG_ADDGROUPTOGROUP * msg = (DPMSG_ADDGROUPTOGROUP *) generic_msg;
			return true;
		}
		case DPSYS_ADDPLAYERTOGROUP : {

			DPMSG_ADDPLAYERTOGROUP * msg = (DPMSG_ADDPLAYERTOGROUP *) generic_msg;
			return true;
		}
		/*
		Received a chat message
		*/
		case DPSYS_CHAT : {

			DPMSG_CHAT * msg = (DPMSG_CHAT *) generic_msg;
			if(d_user) {
				d_user->onChatMessage(msg->lpChat->lpszMessageA);
			}
			return true;
		}
		/*
		Received notification of a new player being created - so enumerate & classify all players within session
		*/
		case DPSYS_CREATEPLAYERORGROUP : {
			#ifdef DEBUG
			directPlayLog.printf("SystemMessage : CreatePlayerOrGroup\n");
			#endif

			DPMSG_CREATEPLAYERORGROUP * msg = (DPMSG_CREATEPLAYERORGROUP *) generic_msg;
			if(msg->dwPlayerType == DPPLAYERTYPE_PLAYER) {
				enumeratePlayers();
				if(d_remotePlayerID != 0) {
					if(d_user) d_user->onOpponentJoin();
				}
			}
			return true;
		}
		case DPSYS_DELETEGROUPFROMGROUP : {

			DPMSG_DELETEGROUPFROMGROUP * msg = (DPMSG_DELETEGROUPFROMGROUP *) generic_msg;
			return true;
		}
		case DPSYS_DELETEPLAYERFROMGROUP : {

			DPMSG_DELETEPLAYERFROMGROUP * msg = (DPMSG_DELETEPLAYERFROMGROUP *) generic_msg;
			return true;
		}
		/*
		Other player has left session
		*/
		case DPSYS_DESTROYPLAYERORGROUP : {
			#ifdef DEBUG
			directPlayLog.printf("SystemMessage : DestroyPlayerOrGroup\n");
			#endif

			DPMSG_DESTROYPLAYERORGROUP * msg = (DPMSG_DESTROYPLAYERORGROUP *) generic_msg;
			if(d_user) {
				d_user->onOpponentQuit();
			}
			return false;
		}
		case DPSYS_HOST : {

			DPMSG_HOST * msg = (DPMSG_HOST *) generic_msg;
			return true;
		}
		case DPSYS_SECUREMESSAGE : {

			DPMSG_SECUREMESSAGE * msg = (DPMSG_SECUREMESSAGE *) generic_msg;
			return true;
		}
		case DPSYS_SENDCOMPLETE : {

			DPMSG_SENDCOMPLETE * msg = (DPMSG_SENDCOMPLETE *) generic_msg;
			return true;
		}
		/*
		Session has been disconnected, or closed
		*/
		case DPSYS_SESSIONLOST : {

			#ifdef DEBUG
			directPlayLog.printf("SystemMessage : SessionLost\n");
			#endif

			DPMSG_SESSIONLOST * msg = (DPMSG_SESSIONLOST *) generic_msg;
			if(d_user) {
				d_user->onConnectionLost();
			}
			return false;
		}
		case DPSYS_SETGROUPOWNER : {

			DPMSG_SETGROUPOWNER * msg = (DPMSG_SETGROUPOWNER *) generic_msg;
			return true;
		}
		case DPSYS_SETPLAYERORGROUPDATA : {

			DPMSG_SETPLAYERORGROUPDATA * msg = (DPMSG_SETPLAYERORGROUPDATA *) generic_msg;
			return true;
		}
		case DPSYS_SETPLAYERORGROUPNAME : {

			DPMSG_SETPLAYERORGROUPNAME * msg = (DPMSG_SETPLAYERORGROUPNAME *) generic_msg;
			return true;
		}
		case DPSYS_SETSESSIONDESC : {

			DPMSG_SETSESSIONDESC * msg = (DPMSG_SETSESSIONDESC *) generic_msg;
			return true;
		}
		case DPSYS_STARTSESSION : {

			DPMSG_STARTSESSION * msg = (DPMSG_STARTSESSION *) generic_msg;
			return true;
		}

	}

	return true;
}

bool
CDirectPlay::processGameMessage(LPVOID data, DWORD data_length) {

	#ifdef DEBUG
	directPlayLog.printf("GameMessage received\n");
	#endif

	// get owner to put new data into msg queue
	if(d_user) {
		d_user->onNewMessage(data, data_length);
	}

	return true;
}



bool
CDirectPlay::sendChatMessage(LPDPID to, char * text) {

	#ifdef DEBUG
	directPlayLog.printf("ChatMessage received\n");
	#endif

	DPCHAT chatmsg;
	chatmsg.dwSize = sizeof(DPCHAT);
	chatmsg.dwFlags = 0;
	chatmsg.lpszMessageA = text;

	HRESULT retval;

	retval = instance()->SendChatMessage(
		d_localPlayerID,
		d_remotePlayerID,
		DPSEND_GUARANTEED,
		&chatmsg
	);

	if(retval != DP_OK) { reportError("Send Chat Message", retval); return false; }
	else return true;
}



bool
CDirectPlay::reportError(char * context, HRESULT retval) {

	char * desc;

	switch(retval) {

		case CO_E_NOTINITIALIZED : { desc = "Win32 COM not initialized"; break; }
		case DPERR_CANTADDPLAYER : { desc = "Can't add player"; break; }
		case DPERR_CANTCREATEPLAYER	: { desc = "Can't create player"; break; }
		case DPERR_CONNECTIONLOST : { desc = "connection lost"; break; }
		case DPERR_INVALIDFLAGS : { desc = "invalid flags"; break; }
		case DPERR_INVALIDPARAMS : { desc = "invalid params"; break; }
		case DPERR_NOCONNECTION : { desc = "no connection"; break; }
		case DPERR_ACCESSDENIED : { desc = "access denied"; break; }
		case DPERR_ALREADYINITIALIZED : { desc = "access denied"; break; }
  		case DPERR_AUTHENTICATIONFAILED : { desc = "authentification failed"; break; }
    	case DPERR_CANNOTCREATESERVER : { desc = "cannot create server"; break; }
      	case DPERR_CANTLOADCAPI : { desc = "can't load API"; break; }
		case DPERR_CANTLOADSECURITYPACKAGE : { desc = "can't load security package"; break; }
		case DPERR_CANTLOADSSPI : { desc = "can't load SSPI"; break; }
		case DPERR_CONNECTING : { desc = "connecting..."; break; }
  		case DPERR_ENCRYPTIONFAILED : { desc = "encryption failed"; break; }
  		case DPERR_ENCRYPTIONNOTSUPPORTED : { desc = "encryption not supported"; break; }
  		case DPERR_INVALIDPASSWORD : { desc = "invalid password"; break; }
  		case DPERR_LOGONDENIED : { desc = "logon denied"; break; }
  		case DPERR_NONEWPLAYERS : { desc = "no new players"; break; }
  		case DPERR_NOSESSIONS : { desc = "no sessions"; break; }
  		case DPERR_SIGNFAILED : { desc = "sign failed"; break; }
  		case DPERR_TIMEOUT : { desc = "timeout"; break; }
  		case DPERR_UNINITIALIZED : { desc = "uninitialized"; break; }
  		case DPERR_USERCANCEL : { desc = "user cancel"; break; }

		default : desc = "Unidentified error";
	}

	if(d_user) {
		d_user->onError(context, desc);
	}

	char tmp[1024];
	sprintf(tmp, "CDirectPlay error : %s - %s\n", context, desc);

#ifdef DEBUG
	directPlayLog.printf(tmp);
#endif

	return true;
}








