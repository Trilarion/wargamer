/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef DRAGWIN_HPP
#define DRAGWIN_HPP

#ifndef __cplusplus
#error dragwin.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 * Base Class for window that can contain draggable ItemWindows
 *
 *----------------------------------------------------------------------
 */

#include "sysdll.h"
#include "wind.hpp"
#include "fonts.hpp"
#include "grtypes.hpp"

class DrawDIBDC;

class DragWindowObject
{
    public:
        DragWindowObject() : d_hwnd(NULL), d_id(-1) { }
        DragWindowObject(HWND h, int id) : d_hwnd(h), d_id(id) { }

        HWND hwnd() const { return d_hwnd; }
        int id() const { return d_id; }

    private:
        HWND d_hwnd;
        int d_id;
};



class DragWindow : public WindowBaseND
{
        typedef WindowBaseND Super;

    public:
        SYSTEM_DLL DragWindow();
        SYSTEM_DLL ~DragWindow();

    protected:
        SYSTEM_DLL void setDragFont(const LogFont& lf);
		SYSTEM_DLL virtual LRESULT procMessage(HWND h, UINT m, WPARAM w, LPARAM l);

      SYSTEM_DLL void startDraw();  // hide the drag cursor while we draw on the screen
      SYSTEM_DLL void endDraw();    // re-enable the drag-cursor

    private:
	    LRESULT onDragOp(HWND h, int id, DRAGLISTINFO* dli);
	    LRESULT doDragging(int id, DRAGLISTINFO* dli);
	    BOOL doBeginDrag(int id, DRAGLISTINFO* dli);
	    void doDropped(int id, DRAGLISTINFO* dli);
        void clearDrag();

        virtual void dropObject(const DragWindowObject& from, const DragWindowObject& dest) = 0;
        virtual bool overObject(const PixelPoint& p, const DragWindowObject& from, DragWindowObject* object) = 0;
        virtual bool canDrag(const DragWindowObject& ob) = 0;
               // return true if object can be dragged


        void makeDragFont();

        enum { NoItem = -1 };

        /*
         * Data members
         */

	    // DrawDIBDC* d_dragItemDib;
        HIMAGELIST d_dragImageList;

        int d_dragItem;     // ID of item being dragged
	    Font d_dragFont;    // font used for drag item
        PixelPoint d_lastP;

        static UINT s_dragMsgID;
};

#endif /* DRAGWIN_HPP */

