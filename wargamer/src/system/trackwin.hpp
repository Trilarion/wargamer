/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef TRACKWIN_HPP
#define TRACKWIN_HPP

/*
 * Base window class used by Campaign and Battle pop-up tracking windows
 */

#include "sysdll.h"
#include "wind.hpp"
#include "palwind.hpp"
#include "transwin.hpp"
#include "grtypes.hpp"
#include "resstr.hpp"

class DrawDIBDC;

struct IW_ButtonData {
   InGameText::ID d_textID;   // One of text or textID must be set.
	const char* d_text;        //
	int d_id;
	int d_x;
	int d_y;
	int d_cx;
	int d_cy;
};

struct IW_Data {
  enum Flags {
	 DelayShow   = 0x01
  };

  HWND d_hParent;
  int d_cx;
  int d_cy;
  COLORREF d_insertColor;
  const IW_ButtonData* d_buttonData;
  CustomBorderInfo d_bBorderColors;
  const DrawDIB* d_bFillDib;
  UBYTE d_flags;

  IW_Data() :
	 d_hParent(0),
	 d_cx(0),
	 d_cy(0),
	 d_insertColor(PALETTERGB(128, 128, 128)),
	 d_buttonData(0),
	 d_bFillDib(0),
	 d_flags(0) {}
};

class SYSTEM_DLL InsertWind : public WindowBaseND {
	 SeeThroughWindow d_seeThroughWindow;
	 PixelPoint d_point;

	 enum Flags {
		 TimerActive = 0x01
	 };

	 UBYTE d_flags;

	 IW_Data d_data;

	 DrawDIBDC* d_dib;

	 enum ID { TimerID = 100 };
  public:
	 InsertWind();
	 ~InsertWind();

     void show(bool visible) { WindowBaseND::show(visible); }

	 void position(const PixelPoint& p);
	 	// Display near point, top right if possible
	 void show(const PixelPoint& p);
	 	// Display with topleft at p.
	 void hide();
	 void update();

	 int width() const { return d_data.d_cx; }
	 int height() const { return d_data.d_cy; }

	 virtual	void drawInsertInfo(const PixelRect& r) = 0;

  private:

	 void drawBackground();
	 void killTimer();
	 void drawInfo();

	 static int shadowWidth();
	 static int shadowHeight();

  protected:
	 BOOL onCreate(HWND hwnd, CREATESTRUCT* lpCreateStruct);
	 void onPaint(HWND hwnd);
	 void onMouseMove(HWND hwnd, int x, int y, UINT keyFlags);
	 BOOL onEraseBk(HWND hwnd, HDC hdc);
	 void onTimer(HWND hwnd, UINT id);

	 void create(const IW_Data& data);

#if 0
    // TODO: These need doing
	 void alterButton(const IW_ButtonData& d) {}
		// alter position, or text
	 void enableButton(int id, Boolean f) {}
      // set as enabled or not
#endif

	 DrawDIBDC* dib() const { return d_dib; }
};


#endif
