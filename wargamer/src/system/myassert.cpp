/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	My Assertion Function
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#ifdef DEBUG

#include "myassert.hpp"
#include "except.hpp"
#include <stdio.h>
#include <string.h>
#include <stdarg.h>

/*
 * Exception Class Implementation
 */

AssertError::AssertError(const char* buf) :
	GeneralError(buf)
{
}

/*
 * myAssert function
 */

void
_myAssert(const char* expr, const char* file, int line)
{
	static Boolean doingAssert = False;
#if defined(__WINDOWS__) || defined(_WINDOWS) || defined(__WINDOWS_386__) || defined(__NT__)
	DWORD err = GetLastError();
#endif

  debugLog("Assertion in %s, line %d, %s\n", file, line, expr);

#if defined(__WINDOWS__) || defined(_WINDOWS) || defined(__WINDOWS_386__) || defined(__NT__)

  if(err != 0)
  {
    LPVOID lpMsgBuf;

    FormatMessage(
                  FORMAT_MESSAGE_ALLOCATE_BUFFER |
                      FORMAT_MESSAGE_FROM_SYSTEM,
                      NULL,
                      err,
                      MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), // Default language
                      (LPTSTR) &lpMsgBuf,
                      0,
                      NULL);

    debugLog("Assertion WinErr %d: %s\n",
             static_cast<int>(err), static_cast<const char*>(lpMsgBuf));

		LocalFree(lpMsgBuf);
  }
#endif


	if(!doingAssert)
	{
		doingAssert = True;

		char buf[500];

		// Generic Version using ExceptionUtility

		sprintf(buf, "Assertion Failed in\r\n"
					  "File:\t%s\r\n"
					  "Line:\t%d\r\n"
					  "Why?:\t\"%s\"\r\n\r\n",
            file, line, expr);

#if defined(__WINDOWS__) || defined(_WINDOWS) || defined(__WINDOWS_386__) || defined(__NT__)

    if(err != 0)
    {
      wsprintf(buf + strlen(buf), "Last error:\t%lu\r\n", err);

      FormatMessage(
                    // FORMAT_MESSAGE_ALLOCATE_BUFFER |
                    FORMAT_MESSAGE_FROM_SYSTEM,
                        NULL,
                        err,
                        MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), // Default language
                        // (LPTSTR) &lpMsgBuf,
                        buf + strlen(buf),
                        0,
                        NULL);
    }

		// LocalFree(lpMsgBuf);

#endif


    strcat(buf, "\r\n\r\nDo you want to Quit the program?");

    ExceptionUtility::YesNo result = ExceptionUtility::ask("Assertion Failure", buf);

    if(result == ExceptionUtility::Yes)
      throw AssertError(buf);
    else
      doingAssert = False;
	}
}

/*
 * A Simple message box
 */

void

alert(const char* title, const char* fmt, ...)
{
	char buffer[1000];

	va_list vaList;
	va_start(vaList, fmt);
	vsprintf(buffer, fmt, vaList);

	// messageBox(title, buffer, MB_ICONSTOP);
	ExceptionUtility::alert(title, buffer);
}

/*
 * A Debug Message in a window
 */

static char* d_file = 0;
static int d_line = 0;

void

_setDebugInfo(char* file, int line)
{
	d_file = file;
	d_line = line;
}

void

_myDebugMessage(const char* fmt, ...)
{
	char buffer[1000];
	char buf2[200];

	va_list vaList;
	va_start(vaList, fmt);
	// wvsprintf(buffer, fmt, vaList);
	vsprintf(buffer, fmt, vaList);

	sprintf(buf2, "Error in %s, line %d", d_file, d_line);

#if !defined(NOLOG)
	ExceptionUtility::logError(buf2, buffer);
#endif

	ExceptionUtility::alert(buf2, buffer);
}


/*
 * Add message to debug log
 */

void
debugLog(const char* fmt, ...)
{
#if !defined(NOLOG)
	static int indent = 0;
	int useIndent = indent;

	char buffer[1000];

	char* ptr = buffer;

	char newIndent = 0;

	if(fmt[0] == '+')
	{
		indent++;
		fmt++;
		newIndent = '/';
	}
	else if(fmt[0] == '-')
	{
		indent--;
		useIndent = indent;
		fmt++;
		newIndent = '\\';
	}

	for(int i = 0; i < useIndent; i++)
	{
		*ptr++ = '|';
		*ptr++ = ' ';
	}

	if(newIndent != 0)
	{
		*ptr++ = newIndent;
	}

	va_list vaList;
	va_start(vaList, fmt);
	vsprintf(ptr, fmt, vaList);

	// OutputDebugString(buffer);
	ExceptionUtility::logDebug(buffer);
#endif
}


#endif

/*
 *----------------------------------------------------------------------
 * $Log$
 *----------------------------------------------------------------------
 */
