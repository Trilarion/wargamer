/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
#ifndef BUTTON_H
#define BUTTON_H

#ifndef __cplusplus
#error button.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1995, Steven Morle-Green, All Rights Reserved
 *----------------------------------------------------------------------
 *
 * A replacement to the button class.
 * It emulates a User Drawn Button, but allows the checked state
 * to be set.
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/06/13 08:52:39  greenius
 * Initial Import
 *
 * Revision 1.1  1996/02/15 10:51:21  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#include "sysdll.h"
#include "winctrl.hpp"

/*
 * Globals
 */

void SYSTEM_DLL initToolButton(HINSTANCE instance);

// HWND addToolButton(HWND hWnd, HWND hTooltip, int id, int x, int y, int w, int h);
   // Obsolete

HWND SYSTEM_DLL addToolButton(HWND hWnd, int id, int x, int y, int w, int h);

extern SYSTEM_DLL const char TOOLBUTTONCLASS[];

/*
 * Helper class
 */
   
class SYSTEM_DLL ToolButton : public WindowControl  {
public:
   ToolButton(HWND h);
   ToolButton(HWND h, int id);

   void setCheck(bool flag);
   bool getCheck();
};


#endif /* ToolButton_H */

/*
 *----------------------------------------------------------------------
 * $Log$
 *----------------------------------------------------------------------
 */
