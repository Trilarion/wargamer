/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef CBUTTON_HPP
#define CBUTTON_HPP

#include "sysdll.h"
#include "winctrl.hpp"

class DIB;
struct CustomBorderInfo;
class ImageLibrary;

class SYSTEM_DLL CustomButton : public WindowControl {
public:
	CustomButton(HWND h) :
	  WindowControl(h) {}

	CustomButton(HWND h, int id) :
	  WindowControl(h, id) {}

    CustomButton& operator=(HWND h)
    {
        WindowControl::operator=(h);
        return *this;
    }

    CustomButton& operator=(const CustomButton& w)
    {
        WindowControl::operator=(w);
        return *this;
    }


	static void initCustomButton(HINSTANCE instance);

	void setFillDib(const DIB* fillDib);
	void setBorderColours(const CustomBorderInfo& info);
	void setCheck(Boolean f);
	Boolean getCheck();
	void setCheckBoxImages(const ImageLibrary* il);
	void setCheckBoxImages(const ImageLibrary* il, UWORD uncheckedImg, UWORD checkedImg);
	void setFont(HFONT hFont);
	void setButtonImage(const ImageLibrary* il, UWORD imgID);
   void setFocus();
};

extern const char SYSTEM_DLL CUSTOMBUTTONCLASS[];

#endif
