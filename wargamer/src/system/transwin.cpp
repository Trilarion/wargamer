/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#include "stdinc.hpp"
#include "transwin.hpp"
#include "dib.hpp"
#include "dib_util.hpp"
#include "app.hpp"


/*---------------------------------------------------
 * A see-through window
 * Actually it holds a static dib and copys directly from the screen.
 * Owners can then use dib for creating various colorized effects, etc.
 */

//DrawDIBDC* SeeThroughWindow::s_dib = 0;
//int SeeThroughWindow::s_instanceCount = 0;

SeeThroughWindow::~SeeThroughWindow()
{
#if 0
  ASSERT((s_instanceCount - 1) >= 0);

  if(--s_instanceCount == 0)
  {
	 if(s_dib)
		delete s_dib;

	 s_dib = 0;
  }
#endif
   if(d_dib)
      delete d_dib;
}

void SeeThroughWindow::allocateDib(int cx, int cy)
{
  d_size.cx = cx;
  d_size.cy = cy;

  DIB_Utility::allocateDib(&d_dib, cx, cy);
  ASSERT(d_dib);
}

/*
 * Transfer bits from srcdib to our local dib
 */

void SeeThroughWindow::transferBits(PixelPoint& p)
{
  ASSERT(d_dib);
  ASSERT(d_hParent);

  POINT p2;
  p2.x = p.getX();
  p2.y = p.getY();

  ClientToScreen(d_hParent, &p2);

  HWND hDesk = GetDesktopWindow();
  ASSERT(hDesk);

  HDC hdc = GetWindowDC(hDesk);
  ASSERT(hdc);

  BitBlt(d_dib->getDC(), 0, 0, d_size.cx, d_size.cy,
	  hdc, p2.x, p2.y, SRCCOPY);

  ReleaseDC(APP::getMainHWND(), hdc);
}

void SeeThroughWindow::paint(HDC hdc)
{
  if(d_dib)
  {
	 BitBlt(hdc, 0, 0, d_size.cx, d_size.cy,
		 d_dib->getDC(), 0, 0, SRCCOPY);
  }
}

