/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef IMGLIB_HPP
#define IMGLIB_HPP

#ifndef __cplusplus
#error imglib.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *	Image Library
 *
 *----------------------------------------------------------------------
 */

#include "sysdll.h"
#include "grtypes.hpp"
#include "palette.hpp"

class DrawDIB;
class DIB;
class DCObject;

struct ImagePos {
	USHORT x;
	USHORT y;
	USHORT w;
	USHORT h;
	USHORT hotX;
	USHORT hotY;
};

class SYSTEM_DLL ImageLibrary {
	static DCObject* s_dibDC;
	static UWORD s_instanceCount;

	mutable DIB* dib;
	UWORD nImages;
	const ImagePos* positions;
	mutable ColourIndex mask;
	HINSTANCE d_inst;
	LPCTSTR d_resName;
	mutable Palette::PaletteID d_paletteID;
public:
	// ImageLibrary(const char* bmpResName, UWORD n, const ImagePos* pos, Boolean dc = False);
	ImageLibrary(HINSTANCE hInst, LPCTSTR bmpResName, UWORD n, const ImagePos* pos, Boolean dc = False);
	~ImageLibrary();


	void blit(DrawDIB* dest, UWORD imgNum, LONG x, LONG y, Boolean darken = False) const
	{
     blit(dest, imgNum, x, y, darken, 2);
	}

	void blit(DrawDIB* dest, UWORD imgNum, LONG x, LONG y, Boolean shadow, UINT shadowCX) const;
	void stretchBlit(DrawDIB* dest, UWORD imgNum, LONG x, LONG y, LONG w, LONG h) const;
	void darkenBlit(DrawDIB* dest, UWORD imgNum, LONG x, LONG y, int percent) const;
	void flipBlit(DrawDIB* dest, UWORD imgNum, LONG x, LONG y, Boolean darken = False) const;

	void blit(HDC destDC, UWORD imgNum, LONG x, LONG y) const;
	void stretchBlit(HDC destDC, UWORD imgNum, LONG x, LONG y, LONG w, LONG h) const;

	void stretchBlitShadow(DrawDIB* dest, UWORD imgNum, LONG x, LONG y, LONG w, LONG h, UINT shadowCX) const;


	Boolean getImageSize(UWORD n, int& w, int &h) const;
	Boolean getHotSpots(UWORD n, int& x, int& y) const;

	UWORD getImageCount() const { return nImages; }

	private:
		void update() const;
		void loadImage() const;
};


/*
 * Until Paul finishes with unittb.cpp
 */

#if defined(UNITTB_HPP)

#define ImageLibrary(res, icon, iPos, flag) \
	ImageLibrary(scenario->getScenarioResDLL(), res, icon, iPos, flag)

#endif

#endif /* IMGLIB_HPP */

