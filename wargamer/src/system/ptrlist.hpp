/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef PTRLIST_H
#define PTRLIST_H

#ifndef __cplusplus
#error ptrlist.h is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Simplified Pointer Linked List
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  1995/11/22 10:45:25  Steven_Green
 * Initial revision
 *
 * Revision 1.3  1994/03/10  14:29:08  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.2  1994/02/17  15:01:13  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.1  1994/02/15  23:24:13  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#include "sysdll.h"
#include "mytypes.h"

/*
 * Forward references
 */

class DPtrListBase;
class DPtrListBaseIter;

typedef void(*ForAllFunction)(void*, void*);

/*
 * Doubly Linked Node
 */

class DPtrLink {
friend class DPtrListBase;
friend class DPtrListBaseIter;
	DPtrLink* next;
	DPtrLink* prev;
	void* data;
public:
};

class SYSTEM_DLL DPtrListBase {
friend class DPtrListBaseIter;
	DPtrLink* first;
	DPtrLink* last;
	int count;
public:
	DPtrListBase();
	~DPtrListBase() { clear(); }
	void clear();


	int entries() const { return count; }
	int isEmpty() const { return (count == 0); }

protected:
	void append(void* data);
	void insert(void* data);
	void remove(void* data);

	Boolean isMember(void* data);

	void* find(int n = 0) const;
	void* findLast() const;
	int getID(void* el) const;

	void* get();	// Should get abitrary node with (int=0)
	void forAll(ForAllFunction function, void* data);
protected:
	void removeNode(DPtrLink* node);
};

template<class Type>
class PtrDList : public DPtrListBase {
public:
	Type* find(int n = 0) 	const { return (Type*)DPtrListBase::find(n); }
	Type* findLast() 			const { return (Type*)DPtrListBase::findLast(); }
	Type* get() { return (Type*)DPtrListBase::get(); }	// Should get abitrary node with (int=0)
	void forAll(void(*function)(Type* node, void* data), void* data) {
		DPtrListBase::forAll((ForAllFunction)function, data);
	}
	int getID(Type* el) const { return DPtrListBase::getID(el); }

	void append(Type* data) { DPtrListBase::append(data); }
	void insert(Type* data) { DPtrListBase::insert(data); }
	void remove(Type* data) { DPtrListBase::remove(data); }

	Boolean isMember(Type* data) { return DPtrListBase::isMember(data); }

	void clearAndDestroy();
};

template<class Type>
void PtrDList<Type>::clearAndDestroy()
{
	while(entries())
		delete get();
}


class DPtrListBaseIter {
	DPtrListBase* container;
	DPtrLink* link;
public:
	DPtrListBaseIter(DPtrListBase& list);

	int operator ++();
	int operator --();

	void remove();					// Delete current entry
protected:
	void insert(void* data);	// Insert before current entry
	void append(void* data);	// Append after current entry

	void* current() const;

	int find(void* data);	// Set current to data's value, return nonzero if found
};

template<class Type>
class PtrDListIter : public DPtrListBaseIter {
public:
	PtrDListIter(PtrDList<Type>& list) : DPtrListBaseIter(list) { }

	Type* current() const { return (Type*)(DPtrListBaseIter::current()); }
	
	void insert(Type* data) { DPtrListBaseIter::insert(data); }	// Insert before current entry
	void append(Type* data) { DPtrListBaseIter::append(data); }	// Append after current entry
	int find(Type* data) { return DPtrListBaseIter::find(data); }
};


/*
 * The same again for single linked lists
 */

/*
 * Forward references
 */

class SPtrListBase;
class SPtrListBaseIter;

/*
 * Singly Linked Node
 */

class SPtrLink {
friend class SPtrListBase;
friend class SPtrListBaseIter;
	SPtrLink* next;
	void* data;
public:
};


class SYSTEM_DLL SPtrListBase {
friend class SPtrListBaseIter;
	SPtrLink* first;
	SPtrLink* last;
	int count;
public:
	SPtrListBase();
	~SPtrListBase() { clear(); }

	void clear();

	int entries() const { return count; }
	int isEmpty() const { return (count == 0); }

protected:
	void append(void* data);
	void insert(void* data);

	void* find(int n = 0) const;
	void* findLast() const;

	void* get();	// Should get abitrary node with (int=0)
	Boolean get(void*& value);

	void forAll(ForAllFunction function, void* data);
};

template<class Type>
class PtrSList : public SPtrListBase {
public:
	Type* find(int n = 0) 	const { return (Type*)SPtrListBase::find(n); }
	Type* findLast() 			const { return (Type*)SPtrListBase::findLast(); }
	Type* get() { return (Type*)SPtrListBase::get(); }	// Should get abitrary node with (int=0)

	Boolean get(Type*& value) { return SPtrListBase::get((void*&)value); }

	void append(Type* data) { SPtrListBase::append(data); }
	void insert(Type* data) { SPtrListBase::insert(data); }


	void forAll(void(*function)(Type* node, void* data), void* data) {
		SPtrListBase::forAll((ForAllFunction)function, data);
	}
	void clearAndDestroy();
};


class SPtrListBaseIter {
	SPtrListBase* container;
	SPtrLink* link;
public:
	SPtrListBaseIter(SPtrListBase& list);

	// void insert(void* data);	// Insert before current entry
	// void remove();					// Delete current entry

	int operator ++();
	// int operator --();
protected:
	void append(void* data);	// Append after current entry
	void* current() const;
};

template<class Type>
class PtrSListIter : public SPtrListBaseIter {
public:
	PtrSListIter(PtrSList<Type>& list) : SPtrListBaseIter(list) { }

	Type* current() const { return (Type*)(SPtrListBaseIter::current()); }
	void append(Type* data) { SPtrListBaseIter::append(data); }
};

template<class Type>
void PtrSList<Type>::clearAndDestroy()
{
	while(entries())
		delete get();
}

#endif /* PTRLIST_H */

