/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			      *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								            *
 *----------------------------------------------------------------------------*/
#ifndef MISC_H
#define MISC_H

#ifndef __cplusplus
#error misc.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1995, Steven Morle-Green, All Rights Reserved
 *----------------------------------------------------------------------
 *
 *	Miscellaneous support stuff
 *
 *----------------------------------------------------------------------
 */

#include "sysdll.h"
#include "mytypes.h"
#include "myassert.hpp"
// #include <algobase.h>			// STL min/max/swap functions
#include <algorithm>


// #include "memptr.hpp"

#if 0	// !defined(ALGOBASE_H) && !defined(__STL_SGI_ALGOBASE.H)
// STL has a swap that is identical to this

/*
 * Swap 2 variables
 */

template<class T>
inline void swap(T& t1, T& t2)
{
	T temp = t1;
	t1 = t2;
	t2 = temp;
}

#endif

/*
 * Based on graphics GEM algorithm using XOR
 */

template<class T>
inline void qswap(T& t1, T& t2)
{
	t1 ^= t2;
	t2 ^= t1;
	t1 ^= t2;
}


/*
 * These are similar to STL's min and max templates
 */

template<class T, class W>
inline T minimum(T t1, W t2)
{
	if(t1 < t2)
		return t1;
	else
		return t2;
}

template<class T, class W>
inline T maximum(T t1, W t2)
{
	if(t1 > t2)
		return t1;
	else
		return t2;
}

#ifdef __WATCOM_CPLUSPLUS__
/*
 * To help STL template work better when using
 * min and max with ambiguous values
 * e.g. min(0, int)
 */

inline int min(int n1, int n2)
{
	return minimum(n1, n2);
}

inline int max(int n1, int n2)
{
	return maximum(n1, n2);
}
#endif	// watcom


/**
 * Clip a value between min and max values
 * @note value parameter may be a different type than min/max/retval
 * This allows it to be used e.g. clipValue(int(256), UBYTE(0), UBYTE(255))
 */

template<class T, class V>
inline T clipValue(V value, T minValue, T maxValue)
{
	ASSERT(minValue <= maxValue);

	if(value < minValue)
		value = minValue;
	else if(value > maxValue)
		value = maxValue;

	return value;
}

// Specialization for int
template<>
inline int clipValue(int value, int minValue, int maxValue)
{
	ASSERT(minValue <= maxValue);

	if(value < minValue)
		value = minValue;
	else if(value > maxValue)
		value = maxValue;

	return value;
}



/*
 * Increment and Decrement Enumerations without type problems
 */

template<class T>
inline T INCREMENT(T& t)
{
	t = static_cast<T>(t + 1);
	return t;
}

template<class T>
inline T DECREMENT(T& t)
{
	t = static_cast<T>(t - 1);
	return t;
}

template<class T>
inline T INCREMENT(const T& t)
{
	return static_cast<T>(t + 1);
}

template<class T>
inline T DECREMENT(const T& t)
{
	return static_cast<T>(t - 1);
}


/*
 * Support Functions
 */

SYSTEM_DLL char* copyString(const char* s);
SYSTEM_DLL Boolean stringsDiffer(const char* s1, const char* s2);

SYSTEM_DLL const char* getNths(int number);

#ifdef DEBUG
SYSTEM_DLL const char* boolToAscii(Boolean b);
#endif


SYSTEM_DLL int squareRoot(int n);

/*
 * Compiler specific function for writing va_list arguments to buffer
 * Note the generic version doesn't take a buffer size so could overwrite buffer
 */

inline size_t vbprintf(char* buffer, size_t bufSize, const char* text, va_list vaList)
{
#ifdef __WATCOM_CPLUSPLUS
	return vbprintf(buffer, bufSize, text, vaList);
#elif defined(_MSC_VER)
	return _vsnprintf(buffer, bufSize, text, vaList);
#else
    return vsprintf(buffer, text, vaList);
    // wvsprintf(buffer, text, vaList);
#endif
}





#endif /* MISC_H */


/*----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.4  2003/02/15 19:06:54  greenius
 * Replaced usage of min/max with minimum and maximum to avoid
 * conflicts between boost: Windows.h and std:: all of which want
 * to do something to the definitions of min/max.
 *
 * Revision 1.3  2001/07/02 15:23:18  greenius
 * Battle, Campaign, Combined game and Campaign Editor now compile and run... mostly
 *
 * Revision 1.2  2001/06/18 08:25:46  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 * Revision 1.6  1996/02/29 18:09:32  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.5  1996/01/26 11:04:02  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.4  1995/12/07 09:17:07  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.3  1995/11/13 11:11:05  Steven_Green
 * Added MemPtr<> template
 *
 * Revision 1.2  1995/10/29 16:02:49  Steven_Green
 * Added CopyString function
 *
 * Revision 1.1  1995/10/25 09:52:49  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */
