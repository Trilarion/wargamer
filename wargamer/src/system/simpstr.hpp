/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef SIMPSTR_HPP
#define SIMPSTR_HPP

#ifndef __cplusplus
#error simpstr.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *	A very simple little string class
 *
 * All it allows is for characters or const char* to be appended
 * and then a char* to retrieved
 *
 *----------------------------------------------------------------------
 */

#include "sysdll.h"
#include "myassert.hpp"

class SYSTEM_DLL SimpleString 
{
	char* d_text;
	int d_alloced;
	int d_used;
	int d_blockSize;

	// Don't allow these until I get round to writing them

 public:
 	SimpleString() : d_text(0), d_alloced(0), d_used(0), d_blockSize(8) { }
	SimpleString(const SimpleString&);
 	SimpleString(const char* s);
	~SimpleString();

	void allocSize(int n) { ASSERT(n > 0); d_blockSize = n; }
	SimpleString& operator += (char c);
	SimpleString& operator += (const char* s);
	SimpleString& operator += (const SimpleString& s);

	SimpleString& operator = (const char* s);
	SimpleString& operator = (const SimpleString&);

	const char* toStr() const;

	int length() const { return d_used; }

 private:
	void expandTo(int wantSize);
};

/*
 * Allow chaining of strings similar to streams
 */

inline SimpleString& operator << (SimpleString& str, const char* s)
{
	str += s;
	return str;
}

inline SimpleString& operator << (SimpleString& str, char c)
{
	str += c;
	return str;
}

inline SimpleString& operator << (SimpleString& str, const SimpleString& s)
{
	str += s;
	return str;
}

#endif /* SIMPSTR_HPP */

