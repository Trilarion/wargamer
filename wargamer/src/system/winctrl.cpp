/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1995, Steven Morle-Green, All Rights Reserved
 *----------------------------------------------------------------------
 *
 * Helper classes for Windows controls
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "winctrl.hpp"
#include "wind.hpp"        // For wndInstance
#include "wmisc.hpp"
#include "myassert.hpp"
#include "except.hpp"

/*
 * Class names to avoid duplicating strings in program
 */

const char COMBOCLASS[] = "COMBOBOX";
const char  LISTBOXCLASS[] = "LISTBOX";
const char LISTVIEWCLASS[] = WC_LISTVIEW;
const char TRACKBARCLASS[] = TRACKBAR_CLASS;
const char BUTTONCLASS[] = "BUTTON";
const char STATICCLASS[] = "STATIC";
const char MDICLIENTCLASS[] = "MDICLIENT";
const char SCROLLBARCLASS[] = "SCROLLBAR";
const char TOOLTIPSCLASS[] = TOOLTIPS_CLASS;
const char SPINNERCLASS[] = UPDOWN_CLASS;
const char TREEVIEWCLASS[] = WC_TREEVIEW;

/*
 * Base class functions
 */


WindowControl::WindowControl(HWND h)
{
   // ASSERT(h != NULL);
   hwnd = h;
}


WindowControl::WindowControl(HWND h, int id)
{
   ASSERT(h != NULL);

   hwnd = GetDlgItem(h, id);
   ASSERT(hwnd != NULL);
}

WindowControl& WindowControl::operator=(HWND h)
{
    hwnd = h;
    return *this;
}

WindowControl& WindowControl::operator=(const WindowControl& w)
{
    hwnd = w.hwnd;
    return *this;
}

void 
WindowControl::enable(bool flag)
{
   ASSERT(hwnd != NULL);
   EnableWindow(hwnd, flag);
}

void 
WindowControl::show(bool flag)
{
   ASSERT(hwnd != NULL);
   ShowWindow(hwnd, flag ? SW_SHOW : SW_HIDE);
}

void
WindowControl::move(const RECT& r)
{
   ASSERT(hwnd != NULL);
   MoveWindow(hwnd, r.left, r.top, r.right-r.left, r.bottom-r.top, TRUE);
}

/*
 * Combo Box function
 */



ComboBox::ComboBox(HWND h) : WindowControl(h)
{
   ASSERT_CLASS(hwnd, COMBOCLASS);
}


ComboBox::ComboBox(HWND h, int id) : WindowControl(h, id)
{
   ASSERT_CLASS(hwnd, COMBOCLASS);
}



void 

ComboBox::reset()
{
   ASSERT(hwnd != NULL);
   SendMessage(hwnd, CB_RESETCONTENT, 0, 0);

}



void

ComboBox::add(const char* name, int value)
{
   ASSERT(hwnd != NULL);
   ASSERT(name != 0);
   int index = SendMessage(hwnd, CB_ADDSTRING, 0, (LPARAM) name);
   ASSERT(index != CB_ERR);
   ASSERT(index != CB_ERRSPACE);
   SendMessage(hwnd, CB_SETITEMDATA, index, value);
}

void ComboBox::insert(const char* name, int id, int value)
{
   ASSERT(hwnd != NULL);
   ASSERT(name != 0);
   int index = SendMessage(hwnd, CB_INSERTSTRING, id, (LPARAM) name);
   ASSERT(index != CB_ERR);
   ASSERT(index != CB_ERRSPACE);
   SendMessage(hwnd, CB_SETITEMDATA, index, value);
}

void ComboBox::deleteItem(int index)
{
   ASSERT(hwnd != NULL);
   int i = SendMessage(hwnd, CB_DELETESTRING, index, 0);
   ASSERT(i != CB_ERR);
}

int

ComboBox::getValue()
{
   ASSERT(hwnd != NULL);
   int index = SendMessage(hwnd, CB_GETCURSEL, 0, 0);

   ASSERT(index != CB_ERR);
   if(index != CB_ERR)
      return SendMessage(hwnd, CB_GETITEMDATA, index, 0);
   else
      return 0;
}

int 

ComboBox::getValue(int index)
{
   ASSERT(hwnd != NULL);
   ASSERT(index < SendMessage(hwnd, CB_GETCOUNT, 0, 0));
   return SendMessage(hwnd, CB_GETITEMDATA, index, 0);
}

int

ComboBox::getIndex()
{
   ASSERT(hwnd != NULL);
   int index = SendMessage(hwnd, CB_GETCURSEL, 0, 0);
   ASSERT(index != CB_ERR);
   return index;
}

int 

ComboBox::getNItems()
{
  ASSERT(hwnd != NULL);
  return SendMessage(hwnd, CB_GETCOUNT, 0, 0);
}

void

ComboBox::setValue(int value)
{
   ASSERT(hwnd != NULL);
   int nItems = SendMessage(hwnd, CB_GETCOUNT, 0, 0);
   for(int index = 0; index < nItems; index++)
   {
      int itemData = SendMessage(hwnd, CB_GETITEMDATA, index, 0);
      if(itemData == value)
      {
         SendMessage(hwnd, CB_SETCURSEL, index, 0);
         break;
      }
   }

   ASSERT(index < nItems);
}


void 

ComboBox::set(int i)
{
   ASSERT(hwnd != NULL);
#ifdef DEBUG
   int nItems = SendMessage(hwnd, CB_GETCOUNT, 0, 0);
   ASSERT(i < nItems);
#endif
   SendMessage(hwnd, CB_SETCURSEL, i, 0);
}

void 

ComboBox::initItems(const ComboInit* data)
{
   for(; data->name; data++)
      add(data->name, data->value);
}

StringPtr ComboBox::getText()
{
   return getText(getIndex());
}

StringPtr ComboBox::getText(int index)
{
   int length = SendMessage(hwnd, CB_GETLBTEXTLEN, index, 0);
   ASSERT(length > 0);

   char* buffer = new char[length+1];
   int l2 = SendMessage(hwnd, CB_GETLBTEXT, index, reinterpret_cast<LPARAM>(buffer));
   ASSERT(l2 <= length);
   buffer[l2] = 0;

   StringPtr retVal(buffer);
   delete[] buffer;
   return retVal;
}

void ComboBox::setText(const char* text)
{
   int index = SendMessage(hwnd, CB_SELECTSTRING, -1, reinterpret_cast<LPARAM>(text));
   ASSERT(index != CB_ERR);
}




/*
 * List Box function
 */


ListBox::ListBox(HWND h) : WindowControl(h)
{
   ASSERT_CLASS(hwnd, LISTBOXCLASS);
}


ListBox::ListBox(HWND h, int id) : WindowControl(h, id)
{
   ASSERT_CLASS(hwnd, LISTBOXCLASS);
}

void ListBox::create(int id, HWND hParent, DWORD exStyle, DWORD style, const RECT& r)
{
   ASSERT(hwnd == NULL);

   style |= WS_CHILD;

   hwnd = CreateWindowEx(
      exStyle,
      LISTBOXCLASS,
      "ListBox",
      style,
      r.left, r.top, r.right-r.left, r.bottom-r.top,
      hParent,
      (HMENU) id,
      wndInstance(hParent),
      NULL
   );
}


void

ListBox::reset()
{
   ASSERT(hwnd != NULL);
   SendMessage(hwnd, LB_RESETCONTENT, 0, 0);

}

void

ListBox::add(const char* name, int id)
{
   ASSERT(hwnd != NULL);
   ASSERT(name != 0);
   int index = SendMessage(hwnd, LB_ADDSTRING, 0, (LPARAM) name);
   ASSERT(index != LB_ERR);
   ASSERT(index != LB_ERRSPACE);
   SendMessage(hwnd, LB_SETITEMDATA, index, id);
}

void 

ListBox::remove(int index)
{
   ASSERT(hwnd != NULL);
   ASSERT(index < SendMessage(hwnd, LB_GETCOUNT, 0, 0));
   int result = SendMessage(hwnd, LB_DELETESTRING, (WPARAM)index, 0);
   ASSERT(result != LB_ERR);
}

int

ListBox::getValue() const
{
   ASSERT(hwnd != NULL);
   int index = SendMessage(hwnd, LB_GETCURSEL, 0, 0);

   ASSERT(index != LB_ERR);
   if(index != LB_ERR)
      return SendMessage(hwnd, LB_GETITEMDATA, index, 0);
   else
      return 0;
}

int 

ListBox::getValue(int index) const
{
  ASSERT(hwnd != 0);
  ASSERT(index < SendMessage(hwnd, LB_GETCOUNT, 0, 0));
  return SendMessage(hwnd, LB_GETITEMDATA, index, 0);
}

void ListBox::setValue(int value)
{
   ASSERT(hwnd != NULL);
   int nItems = SendMessage(hwnd, LB_GETCOUNT, 0, 0);
   for(int index = 0; index < nItems; index++)
   {
      int itemData = SendMessage(hwnd, LB_GETITEMDATA, index, 0);
      if(itemData == value)
      {
         SendMessage(hwnd, LB_SETCURSEL, index, 0);
         break;
      }
   }

   ASSERT(index < nItems);
}

void 

ListBox::set(int i)
{
   ASSERT(hwnd != NULL);
#ifdef DEBUG
   int nItems = SendMessage(hwnd, LB_GETCOUNT, 0, 0);
   ASSERT(i < nItems);
#endif
   SendMessage(hwnd, LB_SETCURSEL, i, 0);
}

void ListBox::setTop(int index)
{
  ASSERT(hwnd != NULL);
  ASSERT(index < getNItems());
  int result = SendMessage(hwnd, LB_SETTOPINDEX, index, 0);
  ASSERT(result != LB_ERR);
}

int

ListBox::getIndex() const
{
  ASSERT(hwnd != NULL);
  return SendMessage(hwnd, LB_GETCURSEL, 0, 0);
}

void

ListBox::insert(const char* name, int id, int value)
{
   ASSERT(hwnd != NULL);
   ASSERT(name != 0);
   int index = SendMessage(hwnd, LB_INSERTSTRING, id, (LPARAM) name);
   ASSERT(index != LB_ERR);
   ASSERT(index != LB_ERRSPACE);
   SendMessage(hwnd, LB_SETITEMDATA, index, value);

}

int ListBox::getTop() const 
{
  ASSERT(hwnd != 0);
  return SendMessage(hwnd, LB_GETTOPINDEX, 0, 0);
}

int

ListBox::getItemFromPoint(int x, int y) const
{
  ASSERT(hwnd != NULL);
  return SendMessage(hwnd, LB_ITEMFROMPOINT, 0, MAKELPARAM(x, y));
}

int 

ListBox::getNItems() const
{
  ASSERT(hwnd != NULL);
  return SendMessage(hwnd, LB_GETCOUNT, 0, 0);
}

int ListBox::getMultiSel(int nItems, LPINT items) const
{
   ASSERT(hwnd != NULL);
   ASSERT(items != 0);
   return  SendMessage(hwnd, LB_GETSELITEMS, nItems, (LPARAM)items);
}


/*
 * ImageList helper class
 */


ImageList::ImageList()
{
   image = NULL;
   owned = false;
   gotSize = false;
}


ImageList::ImageList(HIMAGELIST img)
{
   image = img;
   owned = false;
   gotSize = false;
}


ImageList::~ImageList()
{
   if(owned && (image != NULL))
   {
      /*
       * image is set to NULL before it is destroyed
       * just in case another thread tries to access it while it is
       * being destroying
       */

      HIMAGELIST list = image;
      image = NULL;
      ImageList_Destroy(list);
   }
}

bool

ImageList::loadBitmap(HINSTANCE inst, LPCSTR lpbmp, int cx, COLORREF mask)
{
   ASSERT(image == NULL);
   if(image != NULL)
   {
      ASSERT(owned);

      if(owned)
      {
         HIMAGELIST list = image;
         image = NULL;
         ImageList_Destroy(list);
         owned = false;
      }
   }

   gotSize = false;

   image = ImageList_LoadBitmap(inst, lpbmp, cx, 0, mask);
   ASSERT(image != NULL);

   if(image != NULL)
      owned = true;

   return (image != NULL);
}

bool ImageList::addBitmap(HINSTANCE inst, LPCSTR lpbmp, COLORREF mask)
{
   HBITMAP img = LoadBitmap(inst, lpbmp);
   ASSERT(img != NULL);
   if(img != NULL)
      ImageList_AddMasked(image, img, mask);
   return img != NULL;
}

void

ImageList::unloadBitmap()
{
   ASSERT(image != NULL);
   ASSERT(owned);

   if((image != NULL) && owned)
   {
      HIMAGELIST list = image;
      image = NULL;
      ImageList_Destroy(list);
      owned = false;
      gotSize = false;
   }
}

bool

ImageList::isInitialised() const
{
   return image != NULL;
}

int ImageList::getCount() const
{
   ASSERT(image != NULL);
   return ImageList_GetImageCount(image);
}

bool ImageList::draw(HDC hdc, int i, int x, int y) const
{
   ASSERT(image != NULL);

   if(image)
   {
      ASSERT(i < ImageList_GetImageCount(image));

      bool result = ImageList_Draw(image, i, hdc, x, y, ILD_NORMAL) != 0;
      ASSERT(result);
      return result;
   }
   else
      return false;
}

bool 

ImageList::draw(HDC hdc, int i, int x, int y, int w, int h) const
{
   ASSERT(image != NULL);

   if(image)
   {
      bool result = ImageList_DrawEx(image, i, hdc, x, y, w, h, CLR_NONE, CLR_DEFAULT, ILD_NORMAL) != 0;
      ASSERT(result);
      return result;
   }
   else
      return false;
}

HICON

ImageList::getIcon(int i) const
{
   ASSERT(image != NULL);

   if(image)
   {
      HICON icon = ImageList_GetIcon(image, i, ILD_NORMAL);
      ASSERT(icon != NULL);
      return icon;
   }
   else
      return NULL;
}

void ImageList::setSize()
{
   ASSERT(image != NULL);

   int w;
   int h;

   bool result = ImageList_GetIconSize(image, &w, &h) != 0;

   ASSERT(result);

   if(result)
   {
      imgWidth = w;
      imgHeight = h;

      gotSize = true;
   }
   else
      throw GeneralError("Error in ImageList::setSize()\n");
}

int

ImageList::getImgWidth()
{
   ASSERT(image != NULL);

   if(!gotSize)
      setSize();

   return imgWidth;
}

int 

ImageList::getImgHeight()
{
   ASSERT(image != NULL);

   if(!gotSize)
      setSize();

   return imgHeight;
}


/*
 * Track Bar class
 */


TrackBar::TrackBar(HWND h) : WindowControl(h)
{
   ASSERT_CLASS(hwnd, TRACKBARCLASS);
}


TrackBar::TrackBar(HWND h, int id) : WindowControl(h, id)
{
   ASSERT_CLASS(hwnd, TRACKBARCLASS);
}


int TrackBar::getValue()
{
   ASSERT(hwnd != NULL);
   return SendMessage(hwnd, TBM_GETPOS, 0, 0);
}

void TrackBar::setValue(int value)
{
   ASSERT(hwnd != NULL);
   SendMessage(hwnd, TBM_SETPOS, TRUE, value);
}

void TrackBar::setRange(UWORD minRange, UWORD maxRange)
{
   ASSERT(hwnd != NULL);
   SendMessage(hwnd, TBM_SETRANGE, TRUE, MAKELONG(minRange, maxRange));
}

void TrackBar::setTickFreq(int value)
{
   ASSERT(hwnd != NULL);
   SendMessage(hwnd, TBM_SETTICFREQ, value, 0);
}

void TrackBar::create(int id, LPCTSTR text, HWND hParent, DWORD exStyle, DWORD style, const RECT& r)
{
   ASSERT(hwnd == NULL);

   style |= WS_CHILD;

   hwnd = CreateWindowEx(
      exStyle,
      TRACKBARCLASS,
      text,
      style,
      r.left, r.top, r.right-r.left, r.bottom-r.top,
      hParent,
      (HMENU) id,
      wndInstance(hParent),
      NULL
   );
}

/*
 * Button Class
 */


Button::Button(HWND h) : WindowControl(h)
{
   ASSERT_CLASS(hwnd, BUTTONCLASS);
}


Button::Button(HWND h, int id) : WindowControl(h, id)
{
   ASSERT_CLASS(hwnd, BUTTONCLASS);
}

void

Button::create(int id, LPCTSTR text, HWND hParent, DWORD exStyle, DWORD style, const RECT& r)
{
   ASSERT(hwnd == NULL);

   style |= WS_CHILD;

   hwnd = CreateWindowEx(
      exStyle,
      BUTTONCLASS,
      text,
      style,
      r.left, r.top, r.right-r.left, r.bottom-r.top,
      hParent,
      (HMENU) id,
      wndInstance(hParent),
      NULL
   );
}


void

Button::setCheck(bool flag)
{
   ASSERT(hwnd != NULL);
   SendMessage(hwnd, BM_SETCHECK, flag ? BST_CHECKED : BST_UNCHECKED, 0);
}

bool

Button::getCheck()
{
   ASSERT(hwnd != NULL);
   return (SendMessage(hwnd, BM_GETCHECK, 0, 0) == BST_CHECKED);
}

void Button::setImage(HBITMAP image)
{
   ASSERT(hwnd != NULL);
   ASSERT(image != NULL);
   SendMessage(hwnd, BM_SETIMAGE, IMAGE_BITMAP, (LPARAM) image);
}

void

Button::setIcon(HICON image)
{
   ASSERT(hwnd != NULL);
   ASSERT(image != NULL);
   SendMessage(hwnd, BM_SETIMAGE, IMAGE_ICON, (LPARAM) image);
}

/*
 * ListView control
 */


ListView::ListView(HWND h) : WindowControl(h)
{
   ASSERT_CLASS(hwnd, LISTVIEWCLASS);
}


ListView::ListView(HWND h, int id) : WindowControl(h, id)
{
   ASSERT_CLASS(hwnd, LISTVIEWCLASS);
}

void

ListView::setImageList(HIMAGELIST img)
{
   ASSERT(img != NULL);
   ASSERT(hwnd != NULL);

   HIMAGELIST result = ListView_SetImageList(hwnd, img, LVSIL_SMALL);

   ASSERT(result == NULL);    // Check that not already assigned an image list
}

void ListView::reset()
{
   ASSERT(hwnd != NULL);
   bool result = ListView_DeleteAllItems(hwnd) != 0;
   ASSERT(result);
}

void ListView::setCount(int n)
{
   ASSERT(hwnd != NULL);
   ASSERT(n > 0);
   ListView_SetItemCount(hwnd, n);
}

void

ListView::addItem(const LV_ITEM& item)
{
   ASSERT(hwnd != NULL);
   int result = ListView_InsertItem(hwnd, &item);
   ASSERT(result >= 0);
}

long

ListView::getItem(int item)
{
   ASSERT(hwnd != NULL);
   LV_ITEM lvi;
   lvi.mask = LVIF_PARAM;
   lvi.iItem = item;
   lvi.iSubItem = 0;
   bool result = ListView_GetItem(hwnd, &lvi) != 0;
   ASSERT(result);   // != false);
   return (long)lvi.lParam;
}

/*
 * Scroll Bar
 */


ScrollBar::ScrollBar(HWND h) : WindowControl(h)
{
   ASSERT_CLASS(hwnd, SCROLLBARCLASS);
}

ScrollBar::ScrollBar(HWND h, int id) : WindowControl(h, id)
{
   ASSERT_CLASS(hwnd, SCROLLBARCLASS);
}

void ScrollBar::setRange(int nMin, int nMax, UINT nPage)
{
   ASSERT(hwnd != NULL);

   SCROLLINFO info;

   info.cbSize = sizeof(SCROLLINFO);
   info.fMask = SIF_PAGE | SIF_RANGE;
   info.nMin = nMin;
   info.nMax = nMax;
   info.nPage = nPage;
   info.nPos = nMin;       // Fill this in anyway
   info.nTrackPos = 0;     // Fill this in anyway

   SetScrollInfo(hwnd, SB_CTL, &info, TRUE);
}

void ScrollBar::setPos(int nPos)
{
   ASSERT(hwnd != NULL);

   SCROLLINFO info;

   info.cbSize = sizeof(SCROLLINFO);
   info.fMask = SIF_POS;
   info.nMin = 0;          // Fill this in anyway
   info.nMax = 0;          // Fill this in anyway
   info.nPage = 0;         // Fill this in anyway
   info.nPos = nPos;
   info.nTrackPos = 0;     // Fill this in anyway

   SetScrollInfo(hwnd, SB_CTL, &info, TRUE);
}

int ScrollBar::getPos()
{
   ASSERT(hwnd != NULL);

   return GetScrollPos(hwnd, SB_CTL);
}

/*
 * Spinner
 */

Spinner::Spinner(HWND h) : WindowControl(h)
{
   ASSERT_CLASS(hwnd, SPINNERCLASS);
}


Spinner::Spinner(HWND h, int id) : WindowControl(h, id)
{
   ASSERT_CLASS(hwnd, SPINNERCLASS);
}

void

Spinner::setRange(int nMin, int nMax)
{
   ASSERT(hwnd != NULL);
   SendMessage(hwnd, UDM_SETRANGE, 0, MAKELONG(nMax, nMin));
}

void

Spinner::setValue(int value)
{
   ASSERT(hwnd != NULL);
   SendMessage(hwnd, UDM_SETPOS, 0, MAKELONG(value, 0));
}

int 

Spinner::getValue()
{
   ASSERT(hwnd != NULL);
   return SendMessage(hwnd, UDM_GETPOS, 0, 0);
}


#ifdef OLD_TOOLTIP
/*
 * Add Tooltip control to a window
 */

HWND addToolTip(HWND hWnd)
{
   HWND hToolTip = CreateWindowEx( 0, TOOLTIPS_CLASS, (LPSTR)NULL,
      TTS_ALWAYSTIP, 0, 0, 0, 0,
      hWnd, (HMENU)NULL, wndInstance(hWnd), NULL);
   ASSERT(hToolTip != 0);

   return hToolTip;
}

/*
 *  ToolTip class
 */

ToolTip::ToolTip(HWND h) : WindowControl(h)
{
   ASSERT_CLASS(hwnd, TOOLTIPSCLASS);
}

void ToolTip::add(HWND parent, HWND hControl, bool subClass)
{
   ASSERT(hwnd != 0);
   ASSERT(parent != NULL);

   TOOLINFO ti;

   ti.lpszText = LPSTR_TEXTCALLBACK;
   ti.cbSize = sizeof(TOOLINFO);
   ti.uFlags = subClass ? TTF_IDISHWND | TTF_SUBCLASS : TTF_IDISHWND;
   ti.hwnd = parent;
   ti.uId = (UINT)(HWND)hControl;
   ti.hinst = wndInstance(parent);  // APP:instance();

   bool result = SendMessage(hwnd, TTM_ADDTOOL, 0, (LPARAM)(LPTOOLINFO)&ti);
   ASSERT(result);   // == true);
}

void ToolTip::add(HWND parent, int id, RECT& r)
{
   ASSERT(hwnd != 0);
   ASSERT(parent != NULL);

   TOOLINFO ti;

   ti.lpszText = LPSTR_TEXTCALLBACK;
   ti.cbSize = sizeof(TOOLINFO);
   ti.uFlags = TTF_SUBCLASS;
   ti.hwnd = parent;
   ti.uId = id;
   ti.rect = r;
   ti.hinst = wndInstance(parent);  // APP:instance();

   bool result = SendMessage(hwnd, TTM_ADDTOOL, 0, (LPARAM)(LPTOOLINFO)&ti);
   ASSERT(result);   // == true);
}


void ToolTip::deleteTool(HWND parent, HWND hControl) //, UINT id)
{
   ASSERT(hwnd != 0);

   TOOLINFO ti;
   ti.cbSize = sizeof(TOOLINFO);
   ti.hwnd = parent;
   ti.uId = (UINT)hControl;

   SendMessage(hwnd, TTM_DELTOOL, 0, (LPARAM)(LPTOOLINFO)&ti);
}

void ToolTip::activate(bool flag)
{
   SendMessage(hwnd, TTM_ACTIVATE, (WPARAM) (bool) flag, 0);
}

#endif   // OLD_TOOLTIP

#ifdef DEBUG

/*
 * Debugging function to check that a window belongs to a specified class!
 */

void assertClass(HWND h, LPCSTR name, LPCSTR file, int line)
{
   // if(h == NULL)
   //    _myAssert("h != NULL", file, line);
   // else
   if(h != NULL)
   {
      const size_t cSize = 50;
      char className[cSize + 1];
      int result = GetClassName(h, className, cSize);
      if(result == 0)
         _myAssert("Can't get class name", file, line);
      else if(lstrcmpi(className, name) != 0)
      {
         char buffer[200];
         wsprintf(buffer, "Class name \"%s\" != \"%s\"", className, name);
         _myAssert(buffer, file, line);
      }
   }
}

#endif

/*
 * Miscellaneous Functions for adding controls to windows
 * They are defined here rather than wmisc, because otherwise
 * it turns wmisc into a low level support modules into a high
 * level component that depends on lots of things.
 */


HWND addBorder(HWND hWnd, int x, int y, int w, int h)
{
   HWND hButton = CreateWindowEx(
      0,
      STATICCLASS, NULL,
      WS_VISIBLE | WS_CHILD | SS_ETCHEDFRAME,
      x,y,w,h,
      hWnd, (HMENU) 0, wndInstance(hWnd), NULL);
   ASSERT(hButton != NULL);
   return hButton;
}

#if 0
HWND 
addOwnerDrawButton(HWND hWnd, HWND hTooltip, int id, int x, int y, int w, int h)
{
   ASSERT(hWnd != 0);

   HWND hButton = CreateWindowEx(
      0,
      "BUTTON", NULL,
      WS_VISIBLE | WS_CHILD | BS_OWNERDRAW,
      x,y,w,h,
      hWnd, (HMENU) id, wndInstance(hWnd), NULL);
   ASSERT(hButton != NULL);

#ifdef OLD_TOOLTIP
   if(hTooltip != NULL)
   {
     ToolTip tt(hTooltip);
     tt.add(hWnd, hButton);
   }
#endif

   return hButton;

}
#endif

HWND 

addOwnerDrawButton(HWND hWnd, int id, int x, int y, int w, int h)
{
   ASSERT(hWnd != 0);

   HWND hButton = CreateWindowEx(
      0,
      "BUTTON", NULL,
      WS_VISIBLE | WS_CHILD | BS_OWNERDRAW,
      x,y,w,h,
      hWnd, (HMENU) id, wndInstance(hWnd), NULL);
   ASSERT(hButton != NULL);

   return hButton;
}

#if 0
HWND addOwnerDrawButton(HWND hWnd, DWORD exStyle, HWND hTooltip, int id, int x, int y, int w, int h)
{
   ASSERT(hWnd != 0);

   HWND hButton = CreateWindowEx(
      exStyle,
      "BUTTON", NULL,
      WS_VISIBLE | WS_CHILD | BS_OWNERDRAW,
      x,y,w,h,
      hWnd, (HMENU) id, wndInstance(hWnd), NULL);
   ASSERT(hButton != NULL);

#ifdef OLD_TOOLTIP
   if(hTooltip != NULL)
   {
     ToolTip tt(hTooltip);
     tt.add(hWnd, hButton);
   }
#endif

   return hButton;

}
#endif

HWND

addOwnerDrawButton(HWND hWnd, DWORD exStyle, int id, int x, int y, int w, int h)
{
   ASSERT(hWnd != 0);

   HWND hButton = CreateWindowEx(
      exStyle,
      "BUTTON", NULL,
      WS_VISIBLE | WS_CHILD | BS_OWNERDRAW,
      x,y,w,h,
      hWnd, (HMENU) id, wndInstance(hWnd), NULL);
   ASSERT(hButton != NULL);

#ifdef OLD_TOOLTIP
   if(hTooltip != NULL)
   {
     ToolTip tt(hTooltip);
     tt.add(hWnd, hButton);
   }
#endif

   return hButton;

}

#if 0
HWND addOwnerDrawStatic(HWND hWnd, HWND hTooltip, int id, int x, int y, int w, int h)
{
  ASSERT(hWnd != 0);

  HWND hStatic = CreateWindow("STATIC", (LPSTR)NULL,
      WS_CHILD | WS_VISIBLE | SS_OWNERDRAW,
      x, y, w, h,
      hWnd, (HMENU)id, wndInstance(hWnd), NULL);
  ASSERT(hStatic != 0);

#ifdef OLD_TOOLTIP
  if(hTooltip != NULL)
  {
    ToolTip tt(hTooltip);
    tt.add(hWnd, hStatic);
  }
#endif

  return hStatic;

}

#endif

HWND addOwnerDrawStatic(HWND hWnd, int id, int x, int y, int w, int h)
{
  ASSERT(hWnd != 0);

  HWND hStatic = CreateWindow("STATIC", (LPSTR)NULL,
      WS_CHILD | WS_VISIBLE | SS_OWNERDRAW | SS_NOTIFY,
      x, y, w, h,
      hWnd, (HMENU)id, wndInstance(hWnd), NULL);
  ASSERT(hStatic != 0);

  return hStatic;
}

/*
 * TreeView Helper class
 */

TreeView::TreeView(HWND h) : WindowControl(h)
{
   ASSERT_CLASS(hwnd, TREEVIEWCLASS);
}

TreeView::TreeView(HWND h, int id) : WindowControl(h, id)
{
   ASSERT_CLASS(hwnd, TREEVIEWCLASS);
}

void TreeView::create(int id, HWND hParent, DWORD exStyle, DWORD style, const RECT& r)
{
   ASSERT(hwnd == NULL);

   style |= WS_CHILD;

   hwnd = CreateWindowEx(
      exStyle,
      TREEVIEWCLASS,
      "TreeView",
      style,
      r.left, r.top, r.right-r.left, r.bottom-r.top,
      hParent,
      (HMENU) id,
      wndInstance(hParent),
      NULL
   );
}


HTREEITEM TreeView::addItem(const TV_INSERTSTRUCT& tvi)
{
   HTREEITEM ti = TreeView_InsertItem(hwnd, &tvi);
   ASSERT(ti != NULL);
   return ti;
}

void TreeView::setImageList(HIMAGELIST img)
{
   ASSERT(img != NULL);
   TreeView_SetImageList(hwnd, img, TVSIL_NORMAL);
}

void TreeView::setImageList(const ImageList& imgList)
{
   setImageList(imgList.getHandle());
}

void TreeView::reset()
{
   ASSERT(hwnd != NULL);

   bool result = TreeView_DeleteAllItems(hwnd) != 0;
   ASSERT(result);
}

void

TreeView::deleteItem(HTREEITEM item)
{
   ASSERT(hwnd != NULL);
   ASSERT(item != NULL);

   bool result = TreeView_DeleteItem(hwnd, item) != 0;
   ASSERT(result);
}

void

TreeView::showItem(HTREEITEM item)
{
   ASSERT(hwnd != NULL);
   ASSERT(item != NULL);

   TreeView_EnsureVisible(hwnd, item);
}


/*
 * Static Window helper class
 */


/*
 * TreeView Helper class
 */

StaticWindow::StaticWindow(HWND h) : WindowControl(h)
{
   ASSERT_CLASS(hwnd, STATICCLASS);
}

StaticWindow::StaticWindow(HWND h, int id) : WindowControl(h, id)
{
   ASSERT_CLASS(hwnd, STATICCLASS);
}

void StaticWindow::create(int id, HWND hParent, DWORD exStyle, DWORD style, const RECT& r)
{
   ASSERT(hwnd == NULL);

   style |= WS_CHILD;

   hwnd = CreateWindowEx(
      exStyle,
      STATICCLASS,
      "StaticWindow",
      style,
      r.left, r.top, r.right-r.left, r.bottom-r.top,
      hParent,
      (HMENU) id,
      wndInstance(hParent),
      NULL
   );
}



void StaticWindow::setText(LPCTSTR text)
{
   ASSERT(hwnd != NULL);
   SetWindowText(hwnd, text);
}

/*
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.2  2001/06/18 08:25:46  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 * Revision 1.1  2001/06/13 08:52:39  greenius
 * Initial Import
 *
 *----------------------------------------------------------------------
 */
