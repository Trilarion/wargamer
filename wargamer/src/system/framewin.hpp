/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef FRAMEWIN_HPP
#define FRAMEWIN_HPP

#ifndef __cplusplus
#error framewin.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *	Simple Window Class that will be used to hold several child windows
 *
 *----------------------------------------------------------------------
 */


#include "sysdll.h"
#include "wind.hpp"
#include "palwind.hpp"

#if 0   // unused

class FrameWindow : public WindowBaseND {
	protected:
		HWND hwndParent;

		SYSTEM_DLL FrameWindow(HWND p);
		virtual ~FrameWindow() = 0;
		SYSTEM_DLL virtual LRESULT procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
		virtual void getInitialPosition(RECT& r) = 0;
		SYSTEM_DLL void init();
	private:
		void onParentNotify(HWND hwnd, UINT msg, HWND hwndChild, int idChild);
		BOOL onEraseBk(HWND hwnd, HDC hdc);
#ifdef DEBUG
		virtual void onPaint(HWND hWnd);
#endif
};

#endif

/*
 * Simple windows until the child windows are set up properly
 */


class CustomBorderColors;

class SimpleChildWindow : 
	public WindowBaseND,
	public CustomBorderWindow
{
		static ATOM s_classAtom;
		SYSTEM_DLL static const char s_className[];

		SimpleChildWindow(const SimpleChildWindow&);
		SimpleChildWindow& operator = (const SimpleChildWindow&);
	public:
		SYSTEM_DLL SimpleChildWindow(const CustomBorderInfo& borderColors);
		SYSTEM_DLL ~SimpleChildWindow();

        SYSTEM_DLL const char* className() const { return s_className; }

		SYSTEM_DLL HWND create(HWND hParent);
	protected:
		SYSTEM_DLL virtual LRESULT procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);

	private:
		SYSTEM_DLL virtual void onPaint(HWND hWnd);
		SYSTEM_DLL virtual BOOL onEraseBk(HWND hwnd, HDC hdc);

		static ATOM registerClass();
};

#endif /* FRAMEWIN_HPP */

