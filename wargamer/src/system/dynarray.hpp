/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef DYNARRAY_H
#define DYNARRAY_H

#ifndef __cplusplus
#error dynarray.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1995, Steven Morle-Green, All Rights Reserved
 *----------------------------------------------------------------------
 *
 *	Resizeable Array
 *
 *----------------------------------------------------------------------
 */

#include "sysdll.h"
#include "myassert.hpp"

template<class Type>
class ResizeArray {
	Type* items;
	unsigned int nItems;
	unsigned int nAlloced;
	unsigned int blockSize;
public:
	ResizeArray();
	ResizeArray(const ResizeArray<Type>& src);

	~ResizeArray() { reset(); }

	void setBlockSize(unsigned int n) { blockSize = n; }
	void reset();

	unsigned int getCount() const { return nItems; }
	unsigned int entries() const { return nItems; }

	void add(const Type& data);
	void remove(unsigned int i);

	Type* get(unsigned int i);
	const Type* get(unsigned int i) const;
	const Type& operator[] (unsigned int i) const;
	Type& operator[] (unsigned int i);

	ResizeArray<Type>& operator = (const ResizeArray<Type>& src);
};

template<class Type>
class ResizeArrayIter
{
	const ResizeArray<Type>* container;
	int index;
public:
	ResizeArrayIter(const ResizeArray<Type>* c) { container = c; index = -1; }

	void reset() { index = -1; }
		// Start at the beginning again

	int operator ++();
	const Type& current() const { return container->operator[](index); }
};

template<class Type>
int ResizeArrayIter<Type>::operator ++()
{
	return (++index < container->getCount());
}


template<class Type>
ResizeArray<Type>::ResizeArray()
{
	items = 0;
	nItems = 0;
	nAlloced = 0;
	blockSize = 8;
}

/*
 * Copy Constructor
 */

template<class Type>
ResizeArray<Type>::ResizeArray(const ResizeArray<Type>& src)
{
	nItems = src.nItems;
	nAlloced = nItems;
	blockSize = src.blockSize;

	items = new Type[nAlloced];

	for(unsigned int i = 0; i < nItems; i++)
		items[i] = src.items[i];
}


template<class Type>
void ResizeArray<Type>::reset()
{
	if(items)
	{
		delete[] items;
		items = 0;
		nItems = 0;
		nAlloced = 0;
	}
}

template<class Type>
void ResizeArray<Type>::add(const Type& data)
{
	if(nItems >= nAlloced)
	{
		nAlloced += blockSize;
		Type* newItems = new Type[nAlloced];
		ASSERT(newItems != 0);
		for(unsigned int i = 0; i < nItems; i++)
			newItems[i] = items[i];
		delete[] items;
		items = newItems;
	}

	Type* ptr = &items[nItems++];
	*ptr = data;
}

template<class Type>
void ResizeArray<Type>::remove(unsigned int i)
{
	while(++i < nItems)
		items[i-1] = items[i];
	nItems--;

	// Could consider resizing
}


template<class Type>
const Type* ResizeArray<Type>::get(unsigned int i) const
{
	ASSERT(i < nItems);
	return &items[i];
}

template<class Type>
Type* ResizeArray<Type>::get(unsigned int i)
{
	ASSERT(i < nItems);
	return &items[i];
}

template<class Type>
const Type& ResizeArray<Type>::operator[] (unsigned int i) const
{
	ASSERT(i < nItems);
	return items[i];
}

template<class Type>
Type& ResizeArray<Type>::operator[] (unsigned int i)
{
	ASSERT(i < nItems);
	return items[i];
}

/*
 * Copy Function
 *
 * Only copies used items
 */

template<class Type>
ResizeArray<Type>& ResizeArray<Type>::operator = (const ResizeArray<Type>& src)
{
	if(items)
		delete[] items;

	nItems = src.nItems;
	nAlloced = nItems;
	blockSize = src.blockSize;

	items = new Type[nAlloced];

	for(unsigned int i = 0; i < nItems; i++)
		items[i] = src.items[i];

	return *this;
}


#endif /* DYNARRAY_H */


