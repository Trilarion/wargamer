/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 * Base Class for window that can contain draggable ItemWindows
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "dragwin.hpp"
#include "myassert.hpp"
#include "scrnbase.hpp"
#include "dib.hpp"
#include "palette.hpp"
#include "winctrl.hpp"
#include "wmisc.hpp"
#include "colours.hpp"
#include "itemwind.hpp"
#ifdef DEBUG
#include "logwin.hpp"
#endif


#define HANDLE_DRAGLISTMSG(h, w, l, fn) \
        (fn)(h, static_cast<int>(w), reinterpret_cast<DRAGLISTINFO*>(l))


UINT DragWindow::s_dragMsgID = 0;

DragWindow::DragWindow() :
    d_dragItem(NoItem),
    // d_dragItemDib(0),
    d_dragImageList(NULL),
    d_dragFont(),
    d_lastP(-1,-1)
{
    if(!s_dragMsgID)
        s_dragMsgID = RegisterWindowMessage(DRAGLISTMSGSTRING);
    ASSERT(s_dragMsgID != 0);
}


DragWindow::~DragWindow()
{
    selfDestruct();
    // delete d_dragItemDib;
    if(d_dragImageList)
    {
        ImageList_Destroy(d_dragImageList);
        d_dragImageList = NULL;
    }
}


void DragWindow::setDragFont(const LogFont& lf)
{
    d_dragFont.set(lf);
}

  /*
   * Set default item font
   */

void DragWindow::makeDragFont()
{
    if(d_dragFont.getHandle() == NULL)
    {
        LogFont lf;
        lf.height(ScreenBase::y(7));
        lf.weight(FW_MEDIUM);
        // lf.face(scenario->fontName(Font_Bold));
        lf.style(Greenius_System::Roman);
        lf.italic(false);
        lf.underline(false);

        setDragFont(lf);
    }
}

LRESULT DragWindow::procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
    if(msg == s_dragMsgID)
    {
        return HANDLE_DRAGLISTMSG(hWnd, wParam, lParam, onDragOp);
    }

    return Super::procMessage(hWnd, msg, wParam, lParam);
}


LRESULT DragWindow::onDragOp(HWND h, int id, DRAGLISTINFO* dli)
{
  switch(dli->uNotification)
  {
	 case DL_BEGINDRAG:
	 {
		return doBeginDrag(id, dli);
	 }

	 case DL_DRAGGING:
	 {
		return doDragging(id, dli);
	 }

	 case DL_DROPPED:
	 {
		doDropped(id, dli);
		return 0;
	 }

     case DL_CANCELDRAG:
#ifdef DEBUG
        g_logWindow.printf("--- DL_CANCELDRAG x = %ld, y = %ld", dli->ptCursor.x, dli->ptCursor.y);
#endif
        clearDrag();
        return 0;
  }

  return 1;
}


BOOL DragWindow::doBeginDrag(int id, DRAGLISTINFO* dli)
{
#ifdef DEBUG
  g_logWindow.printf("--- DL_BEGINDRAG x = %ld, y = %ld", dli->ptCursor.x, dli->ptCursor.y);
#endif

  d_dragItem = LBItemFromPt(dli->hWnd, dli->ptCursor, FALSE);

  ASSERT(d_dragItem >= 0);
  if(d_dragItem < 0)
	 return FALSE;

#ifdef DEBUG
  g_logWindow.printf("---------- itemID = %d", d_dragItem);
#endif

   if(!canDrag(DragWindowObject(dli->hWnd, d_dragItem)))
      return FALSE;

    /*
     * Create ImageList and start the dragging process
     */

    ASSERT(d_dragImageList == NULL);

    /*
     * Can't seem to get this to work properly!
     */

    /*
     * get the control to draw itself into a DIB for us
     */

    DrawDIBDC* dib = ItemWindow::getDragDIB(dli->hWnd, d_dragItem);

#ifdef DEBUG_IMAGELIST
    {
        HDC tempDC = GetWindowDC(getHWND());
        BitBlt(tempDC, dib->getWidth()+4, 0, dib->getWidth(), dib->getHeight(), dib->getDC(), 0,0, SRCCOPY);
        ReleaseDC(getHWND(), tempDC);
    }
#endif

    int imgIndex;

    /*
     * Bodge to get around Windows limitiation of a Bitmap only
     * being allowed to be selected into one DC.
     * Imagelist uses DC's internally so we must remove our DIB
     * from its DC.
     */

    HBITMAP hTempBM = CreateCompatibleBitmap(dib->getDC(), 1,1);

    if(dib->hasMask())
    {
        // Find our Mask Color as a COLORREF

        ColourIndex col = dib->getMask();

        RGBQUAD color;
        UINT nColors = GetDIBColorTable(dib->getDC(), col, 1, &color);
        ASSERT(nColors == 1);

        COLORREF backColor = PALETTERGB(
                color.rgbRed,
                color.rgbGreen,
                color.rgbBlue);

        d_dragImageList = ImageList_Create(dib->getWidth(), dib->getHeight(),
                ILC_MASK | ILC_COLOR8 | ILC_PALETTE,
                1, 0);

        ASSERT(d_dragImageList);

        dib->selectObject(hTempBM);
        imgIndex = ImageList_AddMasked(d_dragImageList, dib->getHandle(), backColor);
        dib->selectObject(dib->getHandle());
    }
    else
    {
        d_dragImageList = ImageList_Create(dib->getWidth(), dib->getHeight(),
                ILC_COLOR8 | ILC_PALETTE,
                1, 0);

        ASSERT(d_dragImageList);

        dib->selectObject(hTempBM);
        imgIndex = ImageList_Add(d_dragImageList, dib->getHandle(), 0);
        dib->selectObject(dib->getHandle());
    }

    DeleteObject(hTempBM);

    ASSERT(imgIndex == 0);

    delete dib;

    ASSERT(ImageList_GetImageCount(d_dragImageList) == 1);

#ifdef DEBUG_IMAGELIST
    {
        HDC tempDC = GetWindowDC(getHWND());
        ImageList_Draw(d_dragImageList, imgIndex, tempDC, 0,0, ILD_NORMAL);
        ReleaseDC(getHWND(), tempDC);
    }
#endif

    /*
     * Start the Imagelist dragging process
     */

    POINT p = dli->ptCursor;
    ScreenToClient(dli->hWnd, &p);

    PixelRect itemRect;
    ListBox_GetItemRect(dli->hWnd, d_dragItem, &itemRect);

    int hotX = p.x - itemRect.left();   // calculate these
    int hotY = p.y - itemRect.top();

    BOOL result = ImageList_BeginDrag(d_dragImageList, imgIndex, hotX, hotY);
    ASSERT(result);

    int screenX = dli->ptCursor.x; // probably need converting to screen coords
    int screenY = dli->ptCursor.y;

    // result = ImageList_DragEnter(getHWND(), screenX, screenY);
    result = ImageList_DragEnter(NULL, screenX, screenY);
    ASSERT(result);
    d_lastP = dli->ptCursor;

    return TRUE;
}




LRESULT DragWindow::doDragging(int id, DRAGLISTINFO* dli)
{
#ifdef DEBUG
  g_logWindow.printf("--- DL_DRAGGING x = %ld, y = %ld", dli->ptCursor.x, dli->ptCursor.y);
#endif

  ASSERT(d_dragItem >= 0);
  if(d_dragItem < 0)
	 return DL_STOPCURSOR;

  /*
   * Convert to client cordinates
   */

    POINT p = dli->ptCursor;
    // ScreenToClient(getHWND(), &p);
    ASSERT(d_dragImageList != NULL);
    ImageList_DragMove(p.x, p.y);
    d_lastP = p;

    /*
     * See if we are over anything, and highlight cursor, etc
     */

    DragWindowObject dropOb;
    if(overObject(dli->ptCursor, DragWindowObject(dli->hWnd, d_dragItem), &dropOb))
    {
		return DL_MOVECURSOR;
    }
    else
    {
		return DL_STOPCURSOR;
    }
}

void DragWindow::doDropped(int id, DRAGLISTINFO* dli)
{
#ifdef DEBUG
  g_logWindow.printf("--- DL_DROPPED x = %ld, y = %ld", dli->ptCursor.x, dli->ptCursor.y);
  g_logWindow.printf("------ DragItem=%d", d_dragItem);
#endif

    ASSERT(d_dragItem >= 0);
    if(d_dragItem >= 0)
    {
        DragWindowObject dropOb;
        if(overObject(dli->ptCursor, DragWindowObject(dli->hWnd, d_dragItem), &dropOb))
        {
#ifdef DEBUG
            g_logWindow.printf("------ DropItem=%d", dropOb.id());
#endif

            dropObject(DragWindowObject(dli->hWnd, d_dragItem), dropOb);
        }
    }

    clearDrag();
}




void DragWindow::clearDrag()
{
    // ASSERT(d_dragImageList != NULL);

    if(d_dragImageList != NULL)
    {
        // BOOL result = ImageList_DragLeave(getHWND());
        BOOL result = ImageList_DragLeave(NULL);
        ASSERT(result);
        ImageList_EndDrag();        // NB... returns void
        ImageList_Destroy(d_dragImageList);
        d_dragImageList = NULL;
    }
    d_lastP.x(-1);
    d_lastP.y(-1);
    d_dragItem = -1;
}

void DragWindow::startDraw()
{
//   ImageList_DragLeave();
   ImageList_DragLeave(NULL);
}

void DragWindow::endDraw()
{
//   ImageList_DrageEnter();
   ImageList_DragEnter(NULL, d_lastP.x(), d_lastP.y());
}
