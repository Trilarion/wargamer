/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef CEDIT_HPP
#define CEDIT_HPP

#include "sysdll.h"
#include "palwind.hpp"

class CEdit_Imp;
class DrawDIB;

struct CEditData {
  HWND d_parent;
  const DrawDIB* d_fillDib;
  CustomBorderInfo d_bc;
  int d_id;
  int d_x;
  int d_y;
  int d_cx;
  int d_cy;

  CEditData() :
	 d_parent(0),
	 d_fillDib(0),
	 d_id(-1),
	 d_x(0),
	 d_y(0),
	 d_cx(0),
	 d_cy(0) {}
};

class SYSTEM_DLL CEdit {
	 CEdit_Imp* d_edit;
  public:
	 CEdit();
	 ~CEdit();

	 void create(const CEditData& ed);
	 void setFont(HFONT hFont);
	 void setBorderColors(const CustomBorderInfo& bi);
	 void destroy();

    void setText(const String& text);
	 const String& getText();
};

#endif
