/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *	DIB Stretcher
 *
 * updated: 13th November, so seperate optimised functions for
 * - with mask
 * - without mask
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "dib_blt.hpp"
#include "dib.hpp"
#include "misc.hpp"
#include "memptr.hpp"

namespace DIB_Utility
{

inline void getLine(UBYTE* dest, ULONG finalDestW, ULONG srcW, ULONG destW, const UBYTE* srcAd, ULONG skipBits)
{
	if(srcW == destW)
		memcpy(dest, srcAd, finalDestW);
	else if(srcW < destW)		// stretch
	{
		ULONG value = 0;	// destW - srcW;

		if(skipBits)
		{
			value += srcW * skipBits;
			srcAd += value / destW;
			value %= destW;
		}

		ULONG bitCount = finalDestW;

		*dest++ = *srcAd;

		while(--bitCount)
		{
			value += srcW;
			if(value >= destW)
			{
				value -= destW;
				++srcAd;
			}

			*dest++ = *srcAd;
		}
	}
	else	// squash
	{
		ULONG value = srcW - destW;
		while(skipBits)
		{
			value += destW;
			if(value >= srcW)
			{
				value -= srcW;
				--skipBits;
			}
			++srcAd;
		}

		while(finalDestW)
		{
			value += destW;
			if(value >= srcW)
			{
				value -= srcW;
				*dest++ = *srcAd;
				--finalDestW;
			}

			++srcAd;
		}
	}
}

#if 0    // See template function
inline void putLineMask(UBYTE* dest, const UBYTE* src, ULONG w, ColourIndex mask)
{
	while(w--)
	{
		if(*src != mask)
			*dest = *src;
		++dest;
		++src;
	}
}
#endif

inline bool clip(DrawDIB* destDIB, LONG& destX, LONG& destY, ULONG& destW, ULONG& destH, ULONG& finalDestW, ULONG& finalDestH, ULONG& skipBits, ULONG& skipRows)
{
	// Pull out completely off screen cases

	if(static_cast<LONG>(destX + destW) <= 0)
		return false;
	if(static_cast<LONG>(destY + destH) <= 0)
		return false;
	if(destX >= destDIB->getWidth())
		return false;
	if(destY >= destDIB->getHeight())
		return false;

	finalDestW = destW;
	finalDestH = destH;

	if( (destX + destW) >= destDIB->getWidth())
	{
		finalDestW = destDIB->getWidth() - destX;
	}

	if( (destY + destH) >= destDIB->getHeight())
	{
		finalDestH = destDIB->getHeight() - destY;
	}

	skipBits = 0;		// Number of dest bits skipped

	if(destX < 0)
	{
		skipBits = -destX;
		finalDestW += destX;
		destX = 0;
	}

	skipRows = 0;

	if(destY < 0)
	{
		skipRows = -destY;
		finalDestH += destY;
		destY = 0;
	}

	return true;
}


template<class WriteFunction>
void stretch(DrawDIB* destDIB, LONG destX, LONG destY, ULONG destW, ULONG destH, const DIB* srcDIB, LONG srcX, LONG srcY, ULONG srcW, ULONG srcH, WriteFunction writeLine)
{
	ASSERT(destH != 0);
	ASSERT(destW != 0);
	ASSERT(srcW != 0);
	ASSERT(srcH != 0);

	// Pull off non-stretch case

	if((destW == srcW) && (destH == srcH))
	{
		destDIB->blit(destX, destY, destW, destH, srcDIB, srcX, srcY);
		return;
	}

	ULONG finalDestW;
	ULONG finalDestH;
	ULONG skipBits;
	ULONG skipRows;

	if(!clip(destDIB, destX, destY, destW, destH, finalDestW, finalDestH, skipBits, skipRows))
		 return;

	ASSERT(srcX >= 0);
	ASSERT(srcY >= 0);
	ASSERT( (srcX + srcW) <= srcDIB->getWidth() );
	ASSERT( (srcY + srcH) <= srcDIB->getHeight() );

	/*
	 * Line buffer
	 */

	MemPtr<UBYTE> lineBuf(finalDestW);

	if(destH == srcH)
	{
		const UBYTE* srcAd = srcDIB->getAddress(srcX, srcY);
		UBYTE* destAd = destDIB->getAddress(destX, destY);

		// Always need 1st line!


		ULONG lineCount = finalDestH;

		while(lineCount--)
		{
			// Copy lineBuf to screen

			getLine(lineBuf.get(), finalDestW, srcW, destW, srcAd, skipBits);
			srcAd += srcDIB->getStorageWidth();
			writeLine(destAd, lineBuf.get(), finalDestW);
			destAd += destDIB->getStorageWidth();
		}
	}
   else if(destH > srcH)	// Y Expand
	{

		ULONG value = 0;
		if(skipRows)
		{
			value += skipRows * srcH;
			srcY += value / destH;
			value %= destH;
		}

		const UBYTE* srcAd = srcDIB->getAddress(srcX, srcY);
		UBYTE* destAd = destDIB->getAddress(destX, destY);

		// Always need 1st line!

		getLine(lineBuf.get(), finalDestW, srcW, destW, srcAd, skipBits);
		srcAd += srcDIB->getStorageWidth();

		ULONG lineCount = finalDestH;

		while(lineCount--)
		{
			// Copy lineBuf to screen

			writeLine(destAd, lineBuf.get(), finalDestW);

			destAd += destDIB->getStorageWidth();

			// See if we need another line
			// Do test to prevent reading past end on last line

			if(lineCount)
			{
				value += srcH;
				if(value >= destH)
				{
					value -= destH;

					// Get new line

					getLine(lineBuf.get(), finalDestW, srcW, destW, srcAd, skipBits);
					srcAd += srcDIB->getStorageWidth();
				}
			}
		}
	}
	else		// Y Squash
	{
		ULONG value = srcH - destH;
		while(skipRows)
		{
			value += destH;
			if(value >= srcH)
			{
				value -= srcH;
				++srcY;
				--skipRows;
			}
		}

		const UBYTE* srcAd = srcDIB->getAddress(srcX, srcY);
		UBYTE* destAd = destDIB->getAddress(destX, destY);

		ULONG lineCount = finalDestH;

		while(lineCount)
		{
			value += destH;
			if(value >= srcH)
			{
				value -= srcH;

				getLine(lineBuf.get(), finalDestW, srcW, destW, srcAd, skipBits);
				writeLine(destAd, lineBuf.get(), finalDestW);
				destAd += destDIB->getStorageWidth();
				--lineCount;
			}
			srcAd += srcDIB->getStorageWidth();
		}
	}
}


/*========================================================================
 * Copy part of DIB to another, stretching or squashing as neccessary
 * If srcDIB has a mask, then that will be used appropriately
 */

class WriteMaskNoRemap
{
   public:
      WriteMaskNoRemap(ColourIndex mask) : d_mask(mask) { }
      void operator()(UBYTE* dest, const UBYTE* src, ULONG w)
      {
	      while(w--)
	      {
		      if(*src != d_mask)
			      *dest = *src;
		      ++dest;
		      ++src;
	      }
      }

   private:
      ColourIndex d_mask;
};


class WriteMaskRemap
{
   public:
      WriteMaskRemap(ColorIndex mask, const CPalette::RemapTablePtr& remap) : d_mask(mask), d_remap(remap) { }
      void operator()(UBYTE* dest, const UBYTE* src, ULONG w)
      {
	      while(w--)
	      {
		      if(*src != d_mask)
			      *dest = (*d_remap)[*src];
		      ++dest;
		      ++src;
	      }
      }

   private:
      ColourIndex d_mask;
      // const CPalette::RemapTablePtr& d_remap;
      const ColorRemapTable* d_remap;
};


class WriteNoMaskNoRemap
{
   public:
      WriteNoMaskNoRemap() { }
      void operator()(UBYTE* dest, const UBYTE* src, ULONG w)
      {
         memcpy(dest, src, w);
      }
};

class WriteNoMaskRemap
{
   public:
      WriteNoMaskRemap(const CPalette::RemapTablePtr& remap) : d_remap(remap) { }
      void operator()(UBYTE* dest, const UBYTE* src, ULONG w)
      {
	      while(w--)
	      {
 		      *dest = (*d_remap)[*src];
		      ++dest;
		      ++src;
	      }
      }

   private:
      const ColorRemapTable* d_remap;
      // const CPalette::RemapTablePtr& d_remap;
};

void DIB_Utility::stretchDIBMask(DrawDIB* destDIB, LONG destX, LONG destY, ULONG destW, ULONG destH, const DIB* srcDIB, LONG srcX, LONG srcY, ULONG srcW, ULONG srcH)
{
	ASSERT(srcDIB->hasMask());

   PalettePtr srcPal = srcDIB->getPalette();
   PalettePtr destPal = destDIB->getPalette();

   if (*srcPal == *destPal)
   {
      // No remap

      stretch(destDIB, destX, destY, destW, destH, srcDIB, srcX, srcY, srcW, srcH, WriteMaskNoRemap(srcDIB->getMask()));
   }
   else
   {
      // Remap
      CPalette::RemapTablePtr remapTablePtr = CPalette::makeRemapTable(srcPal, destPal);
      stretch(destDIB, destX, destY, destW, destH, srcDIB, srcX, srcY, srcW, srcH, WriteMaskRemap(srcDIB->getMask(), remapTablePtr));
   }

#if 0    // Do it as a template
	ASSERT(destH != 0);
	ASSERT(destW != 0);
	ASSERT(srcW != 0);
	ASSERT(srcH != 0);
	ASSERT(srcDIB->hasMask());

	// Pull off non-stretch case

	if((destW == srcW) && (destH == srcH))
	{
		destDIB->blit(destX, destY, destW, destH, srcDIB, srcX, srcY);
		return;
	}

	ULONG finalDestW;
	ULONG finalDestH;
	ULONG skipBits;
	ULONG skipRows;

	if(!clip(destDIB, destX, destY, destW, destH, finalDestW, finalDestH, skipBits, skipRows))
		 return;

	ASSERT(srcX >= 0);
	ASSERT(srcY >= 0);
	ASSERT( (srcX + srcW) <= srcDIB->getWidth() );
	ASSERT( (srcY + srcH) <= srcDIB->getHeight() );

	/*
	 * Line buffer
	 */

	MemPtr<UBYTE> lineBuf(finalDestW);

	if(destH > srcH)	// Y Expand
	{

		ULONG value = 0;
		if(skipRows)
		{
			value += skipRows * srcH;
			srcY += value / destH;
			value %= destH;
		}

		const UBYTE* srcAd = srcDIB->getAddress(srcX, srcY);
		UBYTE* destAd = destDIB->getAddress(destX, destY);

		// Always need 1st line!

		getLine(lineBuf, finalDestW, srcW, destW, srcAd, skipBits);
		srcAd += srcDIB->getStorageWidth();

		ULONG lineCount = finalDestH;

		while(lineCount--)
		{
			// Copy lineBuf to screen

			putLineMask(destAd, lineBuf, finalDestW, srcDIB->getMask());

			destAd += destDIB->getStorageWidth();

			// See if we need another line
			// Do test to prevent reading past end on last line

			if(lineCount)
			{
				value += srcH;
				if(value >= destH)
				{
					value -= destH;

					// Get new line

					getLine(lineBuf, finalDestW, srcW, destW, srcAd, skipBits);
					srcAd += srcDIB->getStorageWidth();
				}
			}
		}
	}
	else		// Y Squash
	{
		ULONG value = srcH - destH;
		while(skipRows)
		{
			value += destH;
			if(value >= srcH)
			{
				value -= srcH;
				++srcY;
				--skipRows;
			}
		}

		const UBYTE* srcAd = srcDIB->getAddress(srcX, srcY);
		UBYTE* destAd = destDIB->getAddress(destX, destY);

		ULONG lineCount = finalDestH;

		while(lineCount)
		{
			value += destH;
			if(value >= srcH)
			{
				value -= srcH;

				getLine(lineBuf, finalDestW, srcW, destW, srcAd, skipBits);
				putLineMask(destAd, lineBuf, finalDestW, srcDIB->getMask());
				destAd += destDIB->getStorageWidth();
				--lineCount;
			}
			srcAd += srcDIB->getStorageWidth();
		}
	}
#endif   // Use template version
}

void DIB_Utility::stretchDIBNoMask(DrawDIB* destDIB, LONG destX, LONG destY, ULONG destW, ULONG destH, const DIB* srcDIB, LONG srcX, LONG srcY, ULONG srcW, ULONG srcH)
{
   PalettePtr srcPal = srcDIB->getPalette();
   PalettePtr destPal = destDIB->getPalette();

   if (*srcPal == *destPal)
   {
      // No remap

      stretch(destDIB, destX, destY, destW, destH, srcDIB, srcX, srcY, srcW, srcH, WriteNoMaskNoRemap());
   }
   else
   {
      // Remap
      CPalette::RemapTablePtr remapTablePtr = CPalette::makeRemapTable(srcPal, destPal);
      stretch(destDIB, destX, destY, destW, destH, srcDIB, srcX, srcY, srcW, srcH, WriteNoMaskRemap(remapTablePtr));
   }

#if 0    // Use template version
	ASSERT(destH != 0);
	ASSERT(destW != 0);
	ASSERT(srcW != 0);
	ASSERT(srcH != 0);

#if 0		// Bug!  blit will copy mask!
	// Pull off non-stretch case

	if((destW == srcW) && (destH == srcH))
	{
		destDIB->blit(destX, destY, destW, destH, srcDIB, srcX, srcY);
		return;
	}
#endif

	ULONG finalDestW;
	ULONG finalDestH;
	ULONG skipBits;
	ULONG skipRows;

	if(!clip(destDIB, destX, destY, destW, destH, finalDestW, finalDestH, skipBits, skipRows))
		 return;


	ASSERT(srcX >= 0);
	ASSERT(srcY >= 0);
	ASSERT( (srcX + srcW) <= srcDIB->getWidth() );
	ASSERT( (srcY + srcH) <= srcDIB->getHeight() );

	/*
	 * Line buffer
	 */

	MemPtr<UBYTE> lineBuf(finalDestW);

	if(destH == srcH)
	{
		const UBYTE* srcAd = srcDIB->getAddress(srcX, srcY);
		UBYTE* destAd = destDIB->getAddress(destX, destY);

		// Always need 1st line!


		ULONG lineCount = finalDestH;

		while(lineCount--)
		{
			// Copy lineBuf to screen

			getLine(lineBuf, finalDestW, srcW, destW, srcAd, skipBits);
			srcAd += srcDIB->getStorageWidth();
			memcpy(destAd, lineBuf, finalDestW);
			destAd += destDIB->getStorageWidth();
		}
	}
	else if(destH > srcH)	// Y Expand
	{

		ULONG value = 0;
		if(skipRows)
		{
			value += skipRows * srcH;
			srcY += value / destH;
			value %= destH;
		}

		const UBYTE* srcAd = srcDIB->getAddress(srcX, srcY);
		UBYTE* destAd = destDIB->getAddress(destX, destY);

		// Always need 1st line!

		getLine(lineBuf, finalDestW, srcW, destW, srcAd, skipBits);
		srcAd += srcDIB->getStorageWidth();

		ULONG lineCount = finalDestH;

		while(lineCount--)
		{
			// Copy lineBuf to screen

			memcpy(destAd, lineBuf, finalDestW);

			destAd += destDIB->getStorageWidth();

			// See if we need another line
			// Do test to prevent reading past end on last line

			if(lineCount)
			{
				value += srcH;
				if(value >= destH)
				{
					value -= destH;

					// Get new line

					getLine(lineBuf, finalDestW, srcW, destW, srcAd, skipBits);
					srcAd += srcDIB->getStorageWidth();
				}
			}
		}
	}
	else		// Y Squash
	{
		ULONG value = srcH - destH;
		while(skipRows)
		{
			value += destH;
			if(value >= srcH)
			{
				value -= srcH;
				++srcY;
				--skipRows;
			}
		}

		const UBYTE* srcAd = srcDIB->getAddress(srcX, srcY);
		UBYTE* destAd = destDIB->getAddress(destX, destY);

		ULONG lineCount = finalDestH;

		while(lineCount)
		{
			value += destH;
			if(value >= srcH)
			{
				value -= srcH;

				getLine(lineBuf, finalDestW, srcW, destW, srcAd, skipBits);
				memcpy(destAd, lineBuf, finalDestW);
				destAd += destDIB->getStorageWidth();
				--lineCount;
			}
			srcAd += srcDIB->getStorageWidth();
		}
	}
#endif
}



void DIB_Utility::stretchDIB(DrawDIB* destDIB, LONG destX, LONG destY, ULONG destW, ULONG destH, const DIB* srcDIB, LONG srcX, LONG srcY, ULONG srcW, ULONG srcH)
{
	if(srcDIB->hasMask())
		stretchDIBMask(destDIB, destX, destY, destW, destH, srcDIB, srcX, srcY, srcW, srcH);
	else
		stretchDIBNoMask(destDIB, destX, destY, destW, destH, srcDIB, srcX, srcY, srcW, srcH);
}


/*
 * Stretch a DIB to centre into a rectangle keeping same aspect ratio
 */

void stretchFit(DrawDIB* destDIB, LONG destX, LONG destY, LONG destW, LONG destH, const DIB* src)
{
		// adjust coordinates to fit picture in at same aspect ratio

		int srcW = src->getWidth();
		int srcH = src->getHeight();

		if( (srcW * destH) < (srcH * destW) )
		{
			// fit into X

			int h1 = destH;
			destH = MulDiv(destW, srcH, srcW);
			destY += (h1 - destH) / 2;

		}
		else
		{
			// fit into Y

			int w1 = destW;
			destW = MulDiv(destH, srcW, srcH);
			destX += (w1 - destW) / 2;
		}


		DIB_Utility::stretchDIBNoMask(destDIB, destX,destY,destW,destH,
				src, 0,0,srcW,srcH );
}

};	// namespace DIB_Utility
