##############################################################################
# Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)	 #
# This Software is subject to the GNU General Public License.  				 #
# For License information see the file COPYING in the project root directory #
# For more information see the file README.									 #
##############################################################################

#####################################
# Makefile Include for System.DLL
#
# Execute from within System directory
#####################################


FNAME = wg_system
lnk_dependencies = sysdll.mk

!include ..\wgpaths.mif

CFLAGS += -i=$(ROOT)\res
CFLAGS += -DEXPORT_SYSTEM_DLL


#############################
# Debugging and Error modules

OBJS = except.obj       &
        w95excpt.obj    &
        winerror.obj    &
        myassert.obj    &
	logwin.obj	&
	logw_imp.obj

#############################
# User Interface Modules

OBJS +=	wind.obj 	&
	winctrl.obj 	&
	windsave.obj 	&
	button.obj 	&
	popicon.obj 	&
	generic.obj 	&
	palwind.obj 	&
	framewin.obj 	&
	pathdial.obj 	&
	mciwind.obj 	&
	cscroll.obj 	&
	cbutton.obj 	&
	cust_dlg.obj	&
	cmenu.obj 	&
	fillwind.obj 	&
	scrnbase.obj	&
	cedit.obj 	&
	trackwin.obj 	&
	itemwind.obj 	&
	splash.obj	&
 	c_combo.obj	&
	gtoolbar.obj 	&
	gsecwind.obj 	&
	ctab.obj 	&
	tabwin.obj	&
	dialog.obj 	&
	dc_man.obj 	&
	msgbox.obj 	&
	ctbar.obj	&
	tooltip.obj 	&
	animate.obj 	&
	transwin.obj

#############################
# O/S Support Modules

OBJS += app.obj         &
	thread.obj 	&
	sync.obj 	&
	timer.obj 	&
	mmtimer.obj	&
	registry.obj 	&
	wmisc.obj 	&
	critical.obj    &
	msgenum.obj 	&
	idstr.obj 	&
	filecnk.obj 	&
	winfile.obj 	&
	filepack.obj 	&
	filedata.obj 	&
	filebase.obj 	&
	fileiter.obj	&
	soundsys.obj	&
	wavefile.obj   	&
	sounds.obj 	&
	gamectrl.obj 	&
	resstr.obj

#############################
# Graphic System

OBJS +=	grtypes.obj	&
	dib.obj 	&
	dib_blt.obj 	&
	dib_util.obj	&
	dib_poly.obj	&
	dibref.obj	&
	fractal.obj	&
	bezier.obj	&
	gdiengin.obj	&
	thicklin.obj	&
	palette.obj 	&
	imglib.obj 	&
	bargraph.obj 	&
	fonts.obj 	&
	sprlib.obj	&
	piclib.obj      &
	bmp.obj	

#############################
# Miscellaneous stuff

OBJS += trig.obj        &
        misc.obj        &
        random.obj      &
        sllist.obj      &
        dlist.obj       &
        array.obj       &
	date.obj 	&
	datetime.obj    &
	simpstr.obj 	&
	ptrlist.obj 	&
	poolarry.obj 	&
	fsalloc.obj 	&
	makename.obj 	&
	lzmisc.obj	&
	lzencode.obj	&
	lzdecode.obj    &
	crossref.obj	&
	tables.obj   	&
	config.obj


!ifndef NODEBUG
OBJS +=	palwin.obj	&
	todolog.obj	&
	memdebug.obj
!endif

!ifndef NOLOG
OBJS +=	clog.obj
!endif





SYSLIBS += COMCTL32.LIB VFW32.LIB DSOUND.LIB

!include ..\dll95.mif

.before:
        @echo Making $(TARGETS)
