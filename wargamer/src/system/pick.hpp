/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef PICK_HPP
#define PICK_HPP

#ifndef __cplusplus
#error pick.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 * Pick one of a choice of options
 *
 *----------------------------------------------------------------------
 */

// #include <algo.h>
// #include <algo_fwd.h>
// #include <stl_numeric.h>
#include "random.hpp"
#include "myassert.hpp"
#include <numeric>

/*
 * options is an STL compliant container of values that represent the
 * probability of it being picked... a vector is good.
 *
 * returns index of item in options.
 *
 * each option has a chance of probability / total probabilities
 *
 * Even though this is templated, it will only work if type T::value_type can
 * be converted to an int for the random number generator.
 */


template<class T>
inline int pickOption(const T& options, RandomNumber& random)
{
   T::value_type total = std::accumulate(options.begin(), options.end(), 0);
    if(total == 0)
      return 0;

    T::value_type value = random(total);
    int result = 0;
    T::const_iterator it = options.begin();

    while(it != options.end())
    {
        value -= *it;
        if(value < 0)
            return result;

        ++it;
        ++result;
    }
    ASSERT(it != options.end());
    return -1;
}


#endif /* PICK_HPP */

