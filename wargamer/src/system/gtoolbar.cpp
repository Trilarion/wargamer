/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#include "stdinc.hpp"
#include "gtoolbar.hpp"
#include "resdef.h"
#include "app.hpp"
#include "dib.hpp"
#include "palette.hpp"
#include "bargraph.hpp"
// #include "generic.hpp"
#include "cbutton.hpp"
#include "scrnbase.hpp"
#include "dib_util.hpp"
#include "cust_dlg.hpp"
#include "registry.hpp"
#include "fillwind.hpp"
#include "tooltip.hpp"
#include "logwin.hpp"
#include <algorithm>         // STL min/max/swap functions

/*-----------------------------------------------
 * Local utils
 */

class TBUtil {
public:
  static HWND createButton(HWND hParent, int style, int id, int imgID, int cx, int cy,
	  const DrawDIB* fillDib, const ImageLibrary* il, CustomBorderInfo bi);
};

HWND TBUtil::createButton(HWND hParent, int style, int id, int imgID, int cx, int cy,
	  const DrawDIB* fillDib, const ImageLibrary* il, CustomBorderInfo bi)
{
  HWND hButton = CreateWindow(CUSTOMBUTTONCLASS, NULL,
				style,
				0, 0, cx, cy,
				hParent, (HMENU)id, APP::instance(), NULL);

  ASSERT(hButton != 0);

  CustomButton cb(hButton);
  // cb.setFillDib(scenario->getScenarioBkDIB(Scenario::BackPatterns::WoodBackground));
  cb.setFillDib(fillDib);
  cb.setBorderColours(bi);
  cb.setButtonImage(il, imgID);

  return hButton;
}


// local abstract base class for parent to derive from
class LToolBarBase {
public:
	 LToolBarBase() {}
	 virtual ~LToolBarBase() {};

	 virtual void attachToolBar(int id, const RECT& d_rect) = 0;
};


/*
 * A movable toolbar. It can be anchored to its parent toolbar, or detached
 * by dragging and made a pop-up and vice-versa
 */

class MToolBar : public WindowBaseND {
  public:

	 enum Flags {
		Detached     = 0x01,
		ShouldDetach = 0x02,
		Dragging     = 0x04,
		ShouldAttach = 0x08,
		ShouldResize = 0x10
	 };

  private:
	 LToolBarBase* d_owner;
	 HWND d_hParent;
	 HWND d_hCommand;        // window that gets the menu command
	 int d_id;

	 const int d_buttonCX;   // width of buttons
	 const int d_xOffset;    // button start offset for child version
	 const int d_yOffset;    // button start offset for child version

	 int d_nColumns;         // number of buttons in a column
	 int d_nRows;            // number of buttons in a row

	 int d_dragOffsetX;      // offset used for moving drag rect
	 int d_dragOffsetY;      // offset used for moving drag rect

	 SIZE d_size;            // current size of window
	 RECT d_dRect;           // drag rect

	 UBYTE d_flags;
	 LONG d_hitTest;

	 const ToolButtonData* d_bd;
	 const TipData* d_td;
	 const int d_howMany;

	 const char* d_registryName;  // for the registry, this needs to be unique for each instance
	 const char* d_capName;

	 Greenius_System::CustomDialog d_cd;           // custimized caption etc.

	 const DrawDIB* d_fillDib;
	 const DrawDIB* d_buttonFillDib;
	 const DrawDIB* d_capDib;
	 const ImageLibrary* d_buttonImages;
	 LogFont d_capFont;
	 CustomBorderInfo d_bc;
  public:
	 MToolBar(LToolBarBase* owner, HWND hParent, HWND hCommand, int id, const ToolButtonData* bd,
		  const TipData* td, int howMany,
		  const char* registryName, const char* capName,
		  CustomBorderInfo bc, const DrawDIB* fillDib,
		  const DrawDIB* capDib, const DrawDIB* buttonFillDib, const ImageLibrary* il,
		  const LogFont& capFont);
	 ~MToolBar();

     void show(bool visible) { WindowBaseND::show(visible); }
     void enable(bool visible) { WindowBaseND::enable(visible); }

	 void show(const PixelPoint& p);

	 int width() const { return d_size.cx; }
	 int height() const { return d_size.cy; }
	 UBYTE flags() const { return d_flags; }
	 void setCheck(int id, Boolean check);
	 void enable(int id, Boolean check);

  private:
	 LRESULT procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
	 BOOL onCreate(HWND hwnd, CREATESTRUCT* cs);
	 void onDestroy(HWND hwnd);
	 void onCommand(HWND hwnd, int id, HWND hwndCtl, UINT codeNotify);
	 void onLButtonUp(HWND hwnd, int x, int y, UINT keyFlags);
	 void onLButtonDown(HWND hwnd, BOOL fDoubleClick, int x, int y, UINT keyFlags);
	 void onMouseMove(HWND hwnd, int x, int y, UINT keyFlags);
	 void onNCLButtonDown(HWND hwnd, BOOL fDoubleClick, int x, int y, UINT codeHitTest);
//	 void onNCLButtonUp(HWND hwnd, int x, int y, UINT codeHitTest);

	 void createFrame();        // create window
	 void createToolButtons();      // create the tool buttons
	 Boolean isBelowParent(int x, int y);
	 void drawRect(const RECT& r);
	 void startDrag(int x, int y);
	 Boolean onMoveDrag(int x, int y);
	 Boolean onSizeDrag(int x, int y);
	 void getSize(SIZE& s, Boolean popup, const int nColumns, const int nRows);
	 void calcRowsAndColumns(int& nColumns, int& nRows, const int cx, const int cy, Boolean popup);
	 void calcButtonPos();
	 void toggleStyles();
	 Boolean shouldResize(const int dragCX, const int dragCY, int& cx, int& cy);
	 void setRowsAndColumnsDefault(int& nColumns, int& nRows, Boolean popup);
	 void detachFromParent();
	 void attachToParent();
	 void resizeWindow();
};

//const char* MToolBar::s_registryName = "\\MToolbar Instance";
//DrawDIBDC* MToolBar::s_dib = 0;
//int MToolBar::s_instanceCount = 0;

MToolBar::MToolBar(LToolBarBase* owner, HWND hParent, HWND hCommand, int id,
	  const ToolButtonData* bd, const TipData* td, int howMany, const char* registryName,
	  const char* capName, CustomBorderInfo bc, const DrawDIB* fillDib, const DrawDIB* capDib,
	  const DrawDIB* buttonFillDib, const ImageLibrary* il, const LogFont& capFont) :
  d_owner(owner),
  d_hParent(hParent),
  d_hCommand(hCommand),
  d_id(id),
  d_buttonCX((12 * ScreenBase::dbX()) / ScreenBase::baseX()),
  d_xOffset((2 * ScreenBase::dbX()) / ScreenBase::baseX()),
  d_yOffset((1 * ScreenBase::dbY()) / ScreenBase::baseY()),
  d_nColumns(0),
  d_nRows(0),
  d_dragOffsetX(0),
  d_dragOffsetY(0),
  d_flags(0),
  d_hitTest(-1),
  d_bd(bd),
  d_td(td),
  d_howMany(howMany),
  d_registryName(registryName),
  d_bc(bc),
  d_capName(capName),
  d_cd(bc),
  d_fillDib(fillDib),
  d_buttonFillDib(buttonFillDib),
  d_buttonImages(il),
  d_capDib(capDib),
  d_capFont(capFont)
{
//  SetRect(&d_cRect, -1, 0, 0, 0);
//  SetRect(&d_pRect, -1, 0, 0, 0);
//  SetRect(&d_dRect, -1, 0, 0, 0);

  setRowsAndColumnsDefault(d_nColumns, d_nRows, False);
  getSize(d_size, False, d_nColumns, d_nRows);

#if 0
  /*
	* Get registry data
	*/

  if(!getRegistry("flags", &d_flags, sizeof(UBYTE), d_registryName))
	 d_flags = 0;  // do nothing

  if(!getRegistry("ChildRect", &d_cRect, sizeof(RECT), d_registryName))
  {
	 SIZE s;
	 getDefaultSize(s, False);
	 SetRect(&d_cRect, 0, 0, s.cx, s.cy);
  }

  if(!getRegistry("PopupRect", &d_pRect, sizeof(RECT), d_registryName))
  {
	 SIZE s;
	 getDefaultSize(s, True);
	 SetRect(&d_pRect, -1, 0, s.cx, s.cy);
  }
#endif

  createFrame();
}

MToolBar::~MToolBar()
{
    selfDestruct();
}

void MToolBar::createFrame()
{
  if(hWnd)
  {
	 for(int i = 0; i < d_howMany; i++)
	 {
		HWND h = GetDlgItem(hWnd, i);
		ASSERT(h);

		DestroyWindow(h);
	 }

	 DestroyWindow(hWnd);
	 hWnd = 0;
  }

  ASSERT(d_hParent);

  DWORD style = (d_flags & Detached) ?
	  WS_POPUP | WS_CLIPSIBLINGS | WS_CAPTION : // | WS_THICKFRAME :
	  WS_CHILD | WS_CLIPSIBLINGS;

  HWND h = createWindow(
		0,
		// GenericWindowBaseND::className(),
        NULL,
		style,
		0, 0, 0, 0,
		d_hParent, NULL // , APP::instance()
  );

  ASSERT(h != NULL);
  ASSERT(h == hWnd);

  /*
	* init custom non-client area
	*/

  // caption font
  d_cd.setCaptionFont(d_capFont);
  d_cd.setBkDib(d_fillDib);
  const DrawDIB* cdib = (d_flags & Detached) ?
	  d_capDib : 0;

  d_cd.setCaptionBkDib(cdib);
  d_cd.setStyle(Greenius_System::CustomDialog::EtchedFrame);
  d_cd.init(h);
  d_cd.setCaption(d_capName);
}

void MToolBar::createToolButtons()
{
  /*
	* create buttons
	*/

  for(int i = 0; i < d_howMany; i++)
  {
	 TBUtil::createButton(getHWND(), d_bd[i].d_style, i, d_bd[i].d_imgID, d_buttonCX, d_buttonCX,
		 d_buttonFillDib, d_buttonImages, d_bc);
  }
}

LRESULT MToolBar::procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
//  LRESULT result;

  switch(msg)
  {
	 HANDLE_MSG(hWnd, WM_CREATE, onCreate);
	 HANDLE_MSG(hWnd, WM_DESTROY, onDestroy);
	 HANDLE_MSG(hWnd, WM_COMMAND, onCommand);
	 HANDLE_MSG(hWnd, WM_LBUTTONDOWN, onLButtonDown);
	 HANDLE_MSG(hWnd, WM_LBUTTONUP, onLButtonUp);
	 HANDLE_MSG(hWnd, WM_NCLBUTTONDOWN, onNCLButtonDown);
	 HANDLE_MSG(hWnd, WM_MOUSEMOVE, onMouseMove);
	 default:
		return defProc(hWnd, msg, wParam, lParam);
  }
}

BOOL MToolBar::onCreate(HWND hwnd, CREATESTRUCT* cs)
{
  createToolButtons();
  calcButtonPos();

  g_toolTip.addTips(hwnd, d_td);

  return TRUE;
}

void MToolBar::onDestroy(HWND hwnd)
{
  g_toolTip.delTips(hwnd);

#if 0
  /*
	* set registry
	*/

  // position
  POINT p;
  p.x = d_x;
  p.y = d_y;
  setRegistry("position", &p, sizeof(POINT), s_registryName);

  // size
  SIZE s;
  s.cx = d_cx;
  s.cy = d_cy;
  setRegistry("size", &s, sizeof(SIZE), s_registryName);

  // flags
  setRegistry("flags", &d_flags, sizeof(UBYTE), s_registryName);
#endif
}

void MToolBar::onCommand(HWND hwnd, int id, HWND hwndCtl, UINT codeNotify)
{
  ASSERT(id >= 0 && id < d_howMany);

  if(id >= 0 && id < d_howMany)
  {
	 // relay to parent
	 SendMessage(d_hCommand, WM_COMMAND, MAKEWPARAM(d_bd[id].d_menuID, 0), reinterpret_cast<LPARAM>(hwnd));
  }
}

Boolean MToolBar::isBelowParent(int x, int y)
{
  /*
	* See if we are below client window of parent,
	*/

  POINT p;
  p.x = x;
  p.y = y;

  ClientToScreen(getHWND(), &p);

  RECT r;
  GetWindowRect(d_hParent, &r);

  return (p.y > r.bottom);
}

/*
 * Draw (xor) the moving / sizing rect onto the screen
 */

void MToolBar::drawRect(const RECT& r)
{
#ifdef DEBUG_MTOOLBAR
  logWindow->printf("draw rect - l = %ld, r = %ld, w = %ld, h = %ld",
	  r.left, r.top, r.right - r.left, r.bottom - r.top);
#endif

  /*
	* get dc to draw size / move rect on
	*/

  HDC hdc = GetWindowDC(GetDesktopWindow()); //APP::getMainHWND());
  ASSERT(hdc);

  /*
	* Set mix mode to xor
	*/

  int oldMode = SetROP2(hdc, R2_NOTXORPEN);

  // draw rect
  Rectangle(hdc, r.left, r.top, r.right, r.bottom);

  // clean up
  SetROP2(hdc, oldMode);
  ReleaseDC(APP::getMainHWND(), hdc);
}

void MToolBar::onLButtonDown(HWND hwnd, BOOL fDoubleClick, int x, int y, UINT keyFlags)
{
  d_dragOffsetX = x;
  d_dragOffsetY = y;

#ifdef DEBUG_MTOOLBAR
  logWindow->printf("on lButtonDown - dragOffsetX = %d, dragOffsetY = %d",
	  d_dragOffsetX, d_dragOffsetY);
#endif

  GetWindowRect(hwnd, &d_dRect);
  drawRect(d_dRect);

  SetCapture(hwnd);
}

void MToolBar::onNCLButtonDown(HWND hwnd, BOOL fDoubleClick, int x, int y, UINT codeHitTest)
{
  switch(codeHitTest)
  {
	 case HTCAPTION:
	 case HTRIGHT:
	 case HTLEFT:
	 case HTTOPRIGHT:
	 case HTTOPLEFT:
	 case HTTOP:
	 case HTBOTTOMRIGHT:
	 case HTBOTTOMLEFT:
	 case HTBOTTOM:
	 {
#ifdef DEBUG
		g_logWindow.printf("Hit Test = %d", codeHitTest);
#endif
		SetCapture(hwnd);

		if(codeHitTest == HTCAPTION)
		  d_hitTest = -1;
		else
		  d_hitTest = codeHitTest;

		POINT p;
		p.x = x;
		p.y = y;
		ScreenToClient(hwnd, &p);

		SendMessage(hwnd, WM_LBUTTONDOWN, static_cast<WPARAM>(MK_LBUTTON), MAKELPARAM(p.x, p.y));
		break;
	 }

	 default:
		d_hitTest = -1;
  }
}

void MToolBar::onMouseMove(HWND hwnd, int x, int y, UINT keyFlags)
{
  /*
	* if dragging draw moving / sizing rect
	*/

  if( (keyFlags & MK_LBUTTON) )
  {
	 RECT oldRect = d_dRect;
	 Boolean shouldDraw = True;

	 // set dragging set flags as needed
	 if( !(d_flags & Dragging) )
	 {
		startDrag(x, y);
	 }

	 if(d_hitTest == -1)
		shouldDraw = onMoveDrag(x, y);
	 else
		shouldDraw = onSizeDrag(x, y);

	 if(shouldDraw)
	 {
		drawRect(oldRect);
		drawRect(d_dRect);
	 }
  }
}

void MToolBar::startDrag(int x, int y)
{
  d_flags |= Dragging;
}

Boolean MToolBar::onMoveDrag(int x, int y)
{
#ifdef DEBUG_MTOOLBAR
  logWindow->printf("onMoveDrag --- x = %d, y = %d", x, y);
#endif

  ASSERT(d_hitTest == -1);

  // current size remains the same
  const int cx = d_dRect.right - d_dRect.left;
  const int cy = d_dRect.bottom - d_dRect.top;

  SetRect(&d_dRect, d_dRect.left + (x - d_dragOffsetX), d_dRect.top + (y - d_dragOffsetY),
		  d_dRect.left + (x - d_dragOffsetX) + cx, d_dRect.top + (y - d_dragOffsetY) + cy);

  // are we below parent?
  Boolean isBelow = isBelowParent(x, y);

  if(d_flags & Detached)
  {

	 if( !(isBelow) && !(d_flags & ShouldAttach) )
	 {
#ifdef DEBUG_MTOOLBAR
		logWindow->printf("Over Parent - Adjusting Rect");
#endif

		d_flags |= ShouldAttach;
		// adjust rect back to borderless window
		int nColumns;
		int nRows;
		setRowsAndColumnsDefault(nColumns, nRows, False);

		SIZE s;
		getSize(s, False, nColumns, nRows);

		d_dRect.right = d_dRect.left + s.cx;
		d_dRect.bottom = d_dRect.top + s.cy;
	 }

	 else if( (isBelow) && (d_flags & ShouldAttach) )
	 {
		d_flags &= ~ShouldAttach;

		SIZE s;
		getSize(s, True, d_nColumns, d_nRows);

		d_dRect.right = d_dRect.left + s.cx;
		d_dRect.bottom = d_dRect.top + s.cy;

		// adjust rect to account for non-client area
		AdjustWindowRect(&d_dRect, WS_POPUP | WS_CAPTION | WS_THICKFRAME, FALSE);
	 }
  }
  else
  {
	 // if we're below parent, set flags
	 if(isBelow && !(d_flags & ShouldDetach))
	 {
#ifdef DEBUG_MTOOLBAR
		logWindow->printf("Below Parent - Adjusting Rect");
#endif

		d_flags |= ShouldDetach;

		SIZE s;
		getSize(s, True, d_nColumns, d_nRows);

		d_dRect.right = d_dRect.left + s.cx;
		d_dRect.bottom = d_dRect.top + s.cy;

		// adjust rect to account for non-client area
		AdjustWindowRect(&d_dRect, WS_POPUP | WS_CAPTION | WS_THICKFRAME, FALSE);
	 }
	 else if(!isBelow && (d_flags & ShouldDetach))
	 {
#ifdef DEBUG_MTOOLBAR
		logWindow->printf("Over Parent - Adjusting Rect");
#endif

		d_flags &= ~ShouldDetach;

		// adjust rect back to borderless window
		int nColumns;
		int nRows;
		setRowsAndColumnsDefault(nColumns, nRows, False);

		SIZE s;
		getSize(s, False, nColumns, nRows);

		d_dRect.right = d_dRect.left + s.cx;
		d_dRect.bottom = d_dRect.top + s.cy;
	 }
  }

#ifdef DEBUG_MTOOLBAR
  logWindow->printf("Moving -- l = %ld, t = %ld, w = %ld, h = %ld",
	  d_dRect.left, d_dRect.top, d_dRect.right - d_dRect.left, d_dRect.bottom - d_dRect.top);
#endif

  d_dragOffsetX = x;
  d_dragOffsetY = y;

  return True;
}

Boolean MToolBar::onSizeDrag(int x, int y)
{
#ifdef DEBUG_MTOOLBAR
//  logWindow->printf("onSizeDrag --- x = %d, y = %d", x, y);
#endif

  ASSERT(d_hitTest != -1);
  ASSERT(d_hitTest != HTCAPTION);

  return False;
}

Boolean MToolBar::shouldResize(const int dragCX, const int dragCY, int& cx, int& cy)
{
  ASSERT(d_flags & Detached);

  int nColumns = 0;
  int nRows = 0;
  calcRowsAndColumns(nColumns, nRows, dragCX, dragCY, True);

  if(nColumns > 0 && nRows > 0)
  {
	 if( (nColumns != d_nColumns) || (nRows != d_nRows) )
	 {
		const int captionCY = GetSystemMetrics(SM_CYCAPTION);
		const int borderCX = CustomBorderWindow::ThickBorder;

		cx = (nColumns * d_buttonCX) + (2 * borderCX);
		cy = (nRows * d_buttonCX) + (2 * borderCX) + captionCY;

		return True;
	 }
  }

  return False;
}

void MToolBar::onLButtonUp(HWND hwnd, int x, int y, UINT keyFlags)
{
  // xor the size / move rect
  drawRect(d_dRect);

  if( (d_flags & Dragging) )
  {
	 /*
	  * if we're sizing
	  */

	 if(d_flags & ShouldResize)
	 {
		resizeWindow();

	 }

	 // or detaching
	 else if(d_flags & ShouldDetach)
	 {
		detachFromParent();
	 }

	 // or attaching back to parent
	 else if(d_flags & ShouldAttach)
	 {
		attachToParent();
	 }

	 else if(d_flags & Detached)
	 {
		SetWindowPos(getHWND(), HWND_TOP, d_dRect.left, d_dRect.top, 0, 0, SWP_NOSIZE | SWP_SHOWWINDOW);
	 }

	 d_flags &= ~Dragging;
  }

  SetRect(&d_dRect, 0, 0, 0, 0);
  ReleaseCapture();
}

void MToolBar::detachFromParent()
{
  ASSERT(d_flags & ShouldDetach);
  ASSERT(!(d_flags & Detached));

  d_flags &= ~ShouldDetach;

  ShowWindow(getHWND(), SW_HIDE);
  toggleStyles();
  d_cd.setCaptionBkDib(d_capDib);

  const int cx = d_dRect.right - d_dRect.left;
  const int cy = d_dRect.bottom - d_dRect.top;

#ifdef DEBUG_MTOOLBAR
  logWindow->printf("New Window size -- w = %d, h = %d", cx, cy);
#endif

  calcRowsAndColumns(d_nColumns, d_nRows, cx, cy, True);
  calcButtonPos();

  BOOL result = SetWindowPos(getHWND(), HWND_TOP, d_dRect.left, d_dRect.top, cx, cy, SWP_SHOWWINDOW);
  ASSERT(result);

  d_owner->attachToolBar(d_id, d_dRect);

#ifdef DEBUG_MTOOLBAR
  RECT r;
  GetWindowRect(getHWND(), &r);

  const int actualCX = r.right - r.left;
  const int actualCY = r.bottom - r.top;

  logWindow->printf("New Window size(actual) -- w = %d, h = %d", actualCX, actualCY);
#endif
}

void MToolBar::attachToParent()
{
  ASSERT(d_flags & ShouldAttach);
  ASSERT(d_flags & Detached);

  d_flags &= ~ShouldAttach;
  ShowWindow(getHWND(), SW_HIDE);

  d_cd.setCaptionBkDib(0);

  const int cx = d_dRect.right - d_dRect.left;
  const int cy = d_dRect.bottom - d_dRect.top;

#ifdef DEBUG_MTOOLBAR
  logWindow->printf("New Window size -- w = %d, h = %d", cx, cy);
#endif

  calcRowsAndColumns(d_nColumns, d_nRows, cx, cy, False);
  ASSERT(d_nColumns > 0);
  ASSERT(d_nRows > 0);

  toggleStyles();

  calcButtonPos();

  d_owner->attachToolBar(d_id, d_dRect);
}

void MToolBar::resizeWindow()
{
  ASSERT(d_flags & ShouldResize);

  d_flags &= ~ShouldResize;

  const int cx = d_dRect.right - d_dRect.left;
  const int cy = d_dRect.bottom - d_dRect.top;

#ifdef DEBUG_MTOOLBAR
  logWindow->printf("New Window size -- w = %d, h = %d", cx, cy);
#endif

  calcButtonPos();

  BOOL result = SetWindowPos(getHWND(), HWND_TOP, d_dRect.left, d_dRect.top, cx, cy, SWP_SHOWWINDOW);
  ASSERT(result);
}

void MToolBar::show(const PixelPoint& p)
{
  ASSERT(d_nColumns > 0);
  ASSERT(d_nRows > 0);
  ASSERT(!(d_flags & Detached));

  SetWindowPos(getHWND(), HWND_TOP, p.getX(), p.getY(), d_size.cx, d_size.cy, SWP_SHOWWINDOW);
}

/*
 * Calc default size of child (attached) window
 * This is buttonWidth X nButtons + offsets for width
 *         buttonHeight + offsets for height
 */

void MToolBar::getSize(SIZE& s, Boolean popup, const int nColumns, const int nRows)
{
  ASSERT(nColumns > 0);
  ASSERT(nRows > 0);

  s.cx = (popup) ? (d_buttonCX * nColumns) :
		(d_buttonCX * nColumns) + (2 * d_xOffset) + (2 * CustomBorderWindow::ThinBorder);

  s.cy = (popup) ? (d_buttonCX * nRows) :
		(d_buttonCX * nRows) + (2 * d_yOffset) + (2 * CustomBorderWindow::ThinBorder);
}

/*
 * Calc number of rows and columns
 */

void MToolBar::calcRowsAndColumns(int& nColumns, int& nRows, const int cx, const int cy, Boolean popup)
{
  ASSERT(d_buttonCX > 0);

  const int offsetX = (popup) ? (2 * CustomBorderWindow::ThickBorder) :
	 (2 * d_xOffset) + (2 * CustomBorderWindow::ThinBorder);
  const int offsetY = (popup) ? (2 * CustomBorderWindow::ThickBorder) :
	 (2 * d_yOffset) + (2 * CustomBorderWindow::ThinBorder);

  const int maxPerColumn = maximum(1, (cx - offsetX) / d_buttonCX);
  const int maxPerRow = maximum(1, (cy - offsetY) / d_buttonCX);

  nRows = ((d_howMany % maxPerColumn) != 0) ?
		 (d_howMany / maxPerColumn) + 1 : (d_howMany / maxPerColumn);
  ASSERT(nRows != 0);

  nColumns = ((d_howMany % nRows) != 0) ?
		 (d_howMany / nRows) + 1 : (d_howMany / nRows);
}

#if 0
int MToolBar::calcColumns(const int cx)
{
  const int offsetX = (popup) ? (2 * CustomBorderWindow::ThickBorder) :
	 (2 * d_xOffset) + (2 * CustomBorderWindow::ThinBorder);

  const int maxPerRow = maximum(1, (cy - offsetY) / d_buttonCX);

  nColumns = ((d_howMany % nRows) != 0) ?
		 (d_howMany / nRows) + 1 : (d_howMany / nRows);

}

int MToolBar::calcRows(const int cy)
{
  const int offsetY = (popup) ? (2 * CustomBorderWindow::ThickBorder) :
	 (2 * d_yOffset) + (2 * CustomBorderWindow::ThinBorder);


}
#endif

void MToolBar::setRowsAndColumnsDefault(int& nColumns, int& nRows, Boolean popup)
{
  const int nMaxPer = (popup) ? 5 : 10;

  nRows = ((d_howMany % nMaxPer) != 0) ?
		 (d_howMany / nMaxPer) + 1 : (d_howMany / nMaxPer);
  ASSERT(nRows != 0);

  nColumns = ((d_howMany % nRows) != 0) ?
		 (d_howMany / nRows) + 1 : (d_howMany / nRows);
  ASSERT(nColumns != 0);
}

void MToolBar::calcButtonPos()
{
  int id = 0;
  int x = (d_flags & Detached) ? 0 : CustomBorderWindow::ThinBorder + d_xOffset;
  int y = (d_flags & Detached) ? 0 : CustomBorderWindow::ThinBorder + d_yOffset;

  for(int r = 0; r < d_nRows; r++, id++)
  {
	 for(int c = 0; c < d_nColumns; c++, id++)
	 {
		if(id < d_howMany)
		{
		  HWND h = GetDlgItem(getHWND(), id);
		  ASSERT(h);

		  SetWindowPos(h, HWND_TOP, x, y, 0, 0, SWP_NOSIZE);
		  x += d_buttonCX;
		}
		else
		  break;
	 }

	 if(id >= d_howMany)
		break;

	 x = (d_flags & Detached) ? 0 : CustomBorderWindow::ThinBorder + d_xOffset;;
	 y += d_buttonCX;
  }
}

/*
 * Change style from child to popup or vice versa
 */

void MToolBar::toggleStyles()
{
  if(d_flags & Detached)
	 d_flags &= ~Detached;
  else
	 d_flags |= Detached;

  // create new window
  createFrame();
}

void MToolBar::setCheck(int id, Boolean check)
{
  ASSERT(id < d_howMany);
  ASSERT(d_bd);
  ASSERT(d_bd[id].d_style & BS_AUTOCHECKBOX);
  ASSERT(d_bd[id].d_style & BS_PUSHLIKE);

  CustomButton b(getHWND(), id);
  b.setCheck(check);
}

void MToolBar::enable(int id, Boolean f)
{
  ASSERT(id < d_howMany);
  ASSERT(d_bd);

  CustomButton b(getHWND(), id);
  b.enable(f);
}

//------------------- Mother Window


class ToolBar_Imp : public WindowBaseND, public LToolBarBase
{
	 GToolBar* d_owner;
	 const ToolBarInitData* d_td;
	 FillWindow d_fillWind;

	 /*
	  * Each section has its own MToolBar
	  */

	 MToolBar** d_toolBars;

	 int d_cy;

	 enum Flags {
		// DontShow = 0x01,
		Changed  = 0x02
	 };

	 UBYTE d_flags;


  public:
	 ToolBar_Imp(GToolBar* owner, const ToolBarInitData* td);
	 ~ToolBar_Imp()
	 {
        selfDestruct();

		if(d_toolBars)
       {
          for(int i = 0; i < d_td->d_nSections; i++)
          {
	         delete d_toolBars[i];
	         d_toolBars[i] = 0;
          }

		   delete[] d_toolBars;
       }
	 }

	 HWND getHWND() const { return WindowBaseND::getHWND(); }
     void enable(bool visible) { WindowBaseND::enable(visible); }

	 // functions called by mananger class
	 void setCheck(int menuID, Boolean f);
	 void enable(int menuID, Boolean f);
	 void position(const PixelPoint& p);
	 void showToolBars(const int cx);
	 // void dontShow() { d_flags |= DontShow; }
	 // void doShow() { d_flags &= ~DontShow; }

	 int height() const { return d_cy; }
	 // Boolean shouldShow() const { return !(d_flags & DontShow); }

	 // virtual function from ToolBarBase
	 void attachToolBar(int id, const RECT& d_rect);

  private:
	 LRESULT procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
	 BOOL onCreate(HWND hwnd, CREATESTRUCT* cs);
	 BOOL onEraseBk(HWND hwnd, HDC hdc);
	 void onDestroy(HWND hwnd);
};

/*------------------------------------------------------------------------
 * Implementation
 */

ToolBar_Imp::ToolBar_Imp(GToolBar* owner, const ToolBarInitData* td) :
  d_owner(owner),
  d_td(td),
  d_cy(0),
  d_flags(0)
{
  ASSERT(owner);
  ASSERT(d_td);
  ASSERT(d_td->d_hParent);
  ASSERT(d_td->d_fillDib);

  d_toolBars = new MToolBar*[d_td->d_nSections];
  ASSERT(d_toolBars);

  for(int i = 0; i < d_td->d_nSections; i++)
  {
	 d_toolBars[i] = 0;
  }

  HWND hWnd = createWindow(
		0,
		// GenericNoBackClass::className(),
        NULL,
		WS_CHILD,
		0, 0, 0, 0,
		d_td->d_hParent, NULL   // , APP::instance()
  );

  ASSERT(hWnd != NULL);
}

LRESULT ToolBar_Imp::procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
//  LRESULT result;

  switch(msg)
  {
	 HANDLE_MSG(hWnd, WM_CREATE, onCreate);
	 HANDLE_MSG(hWnd, WM_ERASEBKGND, onEraseBk);
//	 HANDLE_MSG(hWnd, WM_COMMAND, onCommand);
	 HANDLE_MSG(hWnd, WM_DESTROY, onDestroy);
	 default:
		return defProc(hWnd, msg, wParam, lParam);
  }
}

BOOL ToolBar_Imp::onCreate(HWND hwnd, CREATESTRUCT* cs)
{
  /*
	* init background dib
	*/

  d_fillWind.setShadowWidth(0);
  d_fillWind.setFillDib(d_td->d_fillDib);
  d_fillWind.setBorderColors(d_td->d_borderColors);

  /*
	* create toolbars
	*/

  ASSERT(d_toolBars);
  for(int i = 0; i < d_td->d_nSections; i++)
  {
	 ToolButtonData* bd = d_td->d_buttonData[i];
	 TipData* td = d_td->d_tipData[i];
	 int howMany = d_td->d_howMany[i];

	 d_toolBars[i] = new MToolBar(this, hwnd, d_td->d_hParent,
		  i, bd, td, howMany, d_td->d_registryNames[i], InGameText::get(d_td->d_captionNames[i]),
		  d_td->d_borderColors, d_td->d_fillDib, d_td->d_capDib,
		  d_td->d_buttonFillDib, d_td->d_buttonImages, d_td->d_capFont);
	 ASSERT(d_toolBars[i]);
  }

  return TRUE;
}

void ToolBar_Imp::onDestroy(HWND hwnd)
{
#if 0     // Done in destructor
  for(int i = 0; i < d_td->d_nSections; i++)
  {
	 if(d_toolBars[i])
	 {
		DestroyWindow(d_toolBars[i]->getHWND());
		d_toolBars[i] = 0;
	 }
	}
#endif
}

BOOL ToolBar_Imp::onEraseBk(HWND hwnd, HDC hdc)
{
	RECT r;
#if 0
	GetClientRect(hwnd, &r);

	const int cx = r.right - r.left;
	const int cy = r.bottom - r.top;

	d_fillWind.allocateDib(cx, cy);
#endif
	if(GetUpdateRect(hwnd, &r, FALSE))
		d_fillWind.paint(hdc, r);

	return TRUE;
}

void ToolBar_Imp::position(const PixelPoint& p)
{
	RECT r;
	GetClientRect(d_td->d_hParent, &r);

	const int cx = r.right - r.left;
	const int yOffset = 2;

	d_cy = d_toolBars[0]->height() + (2 * CustomBorderWindow::ThinBorder) + (2 * yOffset);
	MoveWindow(getHWND(), 0, p.getY(), cx, d_cy, FALSE);
  d_fillWind.allocateDib(cx, d_cy);
	showToolBars(cx);
}

void ToolBar_Imp::showToolBars(const int cx)
{
	const int yOffset = 2;
	const int space = (3 * ScreenBase::dbX()) / ScreenBase::baseX();

	// show sections
	int x = CustomBorderWindow::ThinBorder + space;
	int y = CustomBorderWindow::ThinBorder + yOffset;

	for(int i = 0; i < d_td->d_nSections; i++)
	{
	 PixelPoint p(x, y);

	 if( !(d_toolBars[i]->flags() & MToolBar::Detached) )
	 {
		d_toolBars[i]->show(p);
		x += (d_toolBars[i]->width() + space);
	 }
	}

	// if no toolbars are added, hide window
	if(x == (CustomBorderWindow::ThinBorder + space))
	{
	 if(isEnabled())    // shouldShow())
	 {
		enable(false);     // dontShow();
		d_flags |= Changed;
	 }
	}
	else
	{
	 if(!isEnabled())   // shouldShow())
	 {
		enable(true);   // doShow();
		d_flags |= Changed;
	 }
	}
}

void ToolBar_Imp::attachToolBar(int id, const RECT& d_rect)
{
	RECT r;
	GetClientRect(d_td->d_hParent, &r);

	const int cx = r.right - r.left;
	showToolBars(cx);

	if(d_flags & Changed)
  {
	 // SetWindowPos(getHWND(), (shouldShow()) ? HWND_TOP : HWND_BOTTOM,
	 //	 0, 0, 0, 0, SWP_NOSIZE | SWP_NOMOVE);
	 SetWindowPos(getHWND(), isEnabled() ? HWND_TOP : HWND_BOTTOM,
		 0, 0, 0, 0, SWP_NOSIZE | SWP_NOMOVE);

	 // if(shouldShow())
     if(isEnabled())
		InvalidateRect(getHWND(), NULL, TRUE);

	 d_owner->positionWindows();

	 d_flags &= ~Changed;
  }

  d_owner->updateAll();
}

void ToolBar_Imp::setCheck(int menuID, Boolean f)
{
  Boolean found = False;

  // search through data and find menu item
  for(int s = 0; s < d_td->d_nSections; s++)
  {
	 for(int t = 0; t < d_td->d_howMany[s]; t++)
	 {
		if(d_td->d_buttonData[s][t].d_menuID == menuID)
		{
		  d_toolBars[s]->setCheck(t, f);

		  found = True;
		  break;
		}
	 }

	 if(found)
		break;
  }
}

void ToolBar_Imp::enable(int menuID, Boolean f)
{
  Boolean found = False;

  // search through data and find menu item
  for(int s = 0; s < d_td->d_nSections; s++)
  {
	 for(int t = 0; t < d_td->d_howMany[s]; t++)
	 {
		if(d_td->d_buttonData[s][t].d_menuID == menuID)
		{
		  d_toolBars[s]->enable(t, f);

		  found = True;
		  break;
		}
	 }

	 if(found)
		break;
  }
}

/*------------------------------------------------------------
 * Client access
 */

GToolBar::GToolBar() :
  d_toolBar(0)
{
}

GToolBar::~GToolBar()
{
  // destroy();
  delete d_toolBar;
}

void GToolBar::init(const ToolBarInitData* td)
{
  d_toolBar = new ToolBar_Imp(this, td);
  ASSERT(d_toolBar);
}

void GToolBar::position(const PixelPoint& p)
{
  ASSERT(d_toolBar);
  d_toolBar->position(p);
}

void GToolBar::show(bool visible)
{
  ASSERT(d_toolBar);
  d_toolBar->show(visible);
}

// void GToolBar::destroy()
// {
//   if(d_toolBar)
//   {
// 	 // DestroyWindow(d_toolBar->getHWND());
//      delete d_toolBar;
// 	 d_toolBar = 0;
//   }
// }

void GToolBar::setCheck(int menuID, Boolean f)
{
  ASSERT(d_toolBar);
  d_toolBar->setCheck(menuID, f);
}

void GToolBar::enable(int menuID, Boolean f)
{
  ASSERT(d_toolBar);
  d_toolBar->enable(menuID, f);
}

int GToolBar::height() const
{
  return d_toolBar->height();
}

bool GToolBar::isVisible() const
{
    return d_toolBar->isVisible();
}

bool GToolBar::isEnabled() const
{
    return d_toolBar->isEnabled();
}

void GToolBar::enable(bool visible)
{
    d_toolBar->enable(visible);
}

HWND GToolBar::getHWND() const
{
  return (d_toolBar) ? d_toolBar->getHWND() : 0;
}


