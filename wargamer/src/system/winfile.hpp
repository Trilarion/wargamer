/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
#ifndef WINFILE_H
#define WINFILE_H

#ifndef __cplusplus
#error winfile.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1995, Steven Morle-Green, All Rights Reserved
 *----------------------------------------------------------------------
 *
 * Classes to simplify File Handling
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.2  2001/06/18 08:25:46  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 * Revision 1.1  2001/06/13 08:52:39  greenius
 * Initial Import
 *
 * Revision 1.4  1996/06/22 19:06:39  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.3  1995/11/22 10:45:25  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.2  1995/10/29 16:02:49  Steven_Green
 * Use of Base Class to reduce duplicated code.
 *
 * Revision 1.1  1995/10/25 09:52:49  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#include "sysdll.h"

#include "filebase.hpp"
#include "simpstr.hpp"
#include <windef.h>


/*---------------------------------------------------------------
 * Windows File Handling Implementations
 *
 * Base Class for File operations
 */

class SYSTEM_DLL WinFileRWBase {
protected:
   HANDLE h;
   Boolean error;
#ifdef DEBUG
   LPCSTR fName;
#endif

public:
   WinFileRWBase();
   virtual ~WinFileRWBase();
   Boolean getError();
   virtual Boolean seekTo(SeekPos where);
   virtual SeekPos getPos();
   Boolean isOK() { return !error; }
   void clearError() { error = False; }
};

class SYSTEM_DLL FileError {
   public:
     FileError();
};

/*
 * Directory name storage
 * string has trailing backslash automatically added
 */

class SYSTEM_DLL DirName : public SimpleString
{
public:
   DirName() { }
   ~DirName() { }

   DirName& operator = (const char* name);

   operator const char* () const { return toStr(); }

};

/*
 * Helper for file reading
 */

class SYSTEM_DLL WinFileReader : public FileReader, public WinFileRWBase {

   static DirName d_cdPath;
   static const char registryName[];

public:
   WinFileReader(LPCSTR name);
   ~WinFileReader();
   virtual Boolean read(LPVOID ad, DataLength length);
   virtual Boolean read(LPVOID ad, DataLength length, DataLength& bytesRead);

   static const char* getCDPath();  // { return d_cdPath; }
   static void makeCDFileName(SimpleString& dir, const char* fileName);

   Boolean seekTo(SeekPos where) { return WinFileRWBase::seekTo(where); }
   SeekPos getPos() { return WinFileRWBase::getPos(); }
   Boolean isOK() { return WinFileRWBase::isOK(); }
   Boolean rewind();

   DWORD fileSize() const;
};

/*
 * Helper for file writing
 */

class SYSTEM_DLL WinFileWriter : public FileWriter, public WinFileRWBase {
public:
   WinFileWriter(LPCSTR name);
   ~WinFileWriter();
   Boolean write(const void* ad, DataLength length);

   Boolean seekTo(SeekPos where) { return WinFileRWBase::seekTo(where); }
   SeekPos getPos() { return WinFileRWBase::getPos(); }
   Boolean isOK() { return WinFileRWBase::isOK(); }
};

class SYSTEM_DLL WinFileAsciiReader : public WinFileReader {
   char* buffer;
   char* lineBuf;
   int bufIndex;
   int bufRead;
   SeekPos lastPos;

   static const int bufLength;
   static const int lineBufLength;
public:
   WinFileAsciiReader(LPCSTR name);
   Boolean isAscii() { return True; }
   virtual char* readLine();
   Boolean seekTo(SeekPos where);
   SeekPos getPos();
   Boolean read(LPVOID ad, DataLength length);
   Boolean read(LPVOID ad, DataLength length, DataLength& bytesRead);
};

class SYSTEM_DLL WinFileAsciiWriter : public WinFileWriter {
public:
   WinFileAsciiWriter(LPCSTR name) : WinFileWriter(name) { }
   Boolean isAscii() { return True; }
};

class SYSTEM_DLL WinFileBinaryReader : public WinFileReader {
public:
   WinFileBinaryReader(LPCSTR name) : WinFileReader(name) { }
   Boolean isAscii() { return False; }
   char* readLine() { return 0; }
};

class SYSTEM_DLL WinFileBinaryWriter : public WinFileWriter {
public:
   WinFileBinaryWriter(LPCSTR name) : WinFileWriter(name) { }
   Boolean isAscii() { return False; }
};

/*
 * Stand alone helper functions
 */

namespace FileSystem {

   SYSTEM_DLL bool dirExists(LPCSTR name);      // Return TRUE if exists
   SYSTEM_DLL bool fileExists(LPCSTR name);     // Return TRUE if exists

   SYSTEM_DLL void makeFullPath(SimpleString& s, const char* fileName);
      // Convert filename to a fully specified filename
      // If fileName does not include ":" or starts with "\" then
      // the current directory is added.
      // If starts with \ then current drive is added.

}; // namespace FileSystem

#endif /* WINFILE_H */

/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.2  2001/06/18 08:25:46  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
