/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *	Windows 95 Exception Utility Imnplementation
 *
 *----------------------------------------------------------------------
 */


#include "stdinc.hpp"
#include "w95excpt.hpp"
#include "except.hpp"
#include "misc.hpp"
#include <time.h>
#include <stdio.h>
#include <windows.h>

/*
 * Ensure static variables are initialised before the main program's statics
 */

#ifdef __WATCOM_CPLUSPLUS__
#pragma initialise library;
#endif

/*
 * Exception Utility class
 */

class WindowsExceptionUtility : public ExceptionUtility
{
#if !defined(NOLOG)
	static char s_logFileName[];
#endif
#if defined(DEBUG)
	static char s_debugFileName[];
#endif

 public:
 	WindowsExceptionUtility();

 	void logError(const char* title, const char* text);
		// Add title and text to a log file

	YesNo ask(const char* title, const char* text);
		// Display title and text, and ask user if they want to quit
		// Return True to Quit, False to ignore

	void alert(const char* title, const char* text);
		// Display Information to user and continue

#if defined(DEBUG)
 	void logDebug(const char* text);
		// Add text to a debug File
#endif

 private:
	void wfprintf(HANDLE fp, LPSTR fmt, ...)
	{
		char buffer[500];

		va_list vaList;
		va_start(vaList, fmt);
		vbprintf(buffer, 500, fmt, vaList);
		va_end(vaList);

		DWORD bw;

		WriteFile(fp, buffer, strlen(buffer), &bw, NULL);
	}

	int messageBox(const char* title, const char* text, UINT uType);

};

/*
 * Static variables
 */

#if !defined(NOLOG)
char WindowsExceptionUtility::s_logFileName[] = "error.log";
#endif

#if defined(DEBUG)
char WindowsExceptionUtility::s_debugFileName[] = "debug.log";
#endif

/*
 * Constructor
 */

WindowsExceptionUtility::WindowsExceptionUtility()
{
#ifdef DEBUG
	DeleteFile(s_debugFileName);
#endif
}

/*
 * Add title and text to a log file
 */

void WindowsExceptionUtility::logError(const char* title, const char* text)
{
#if !defined(NOLOG)
#ifdef NOT_WINDOWS
	FILE* fp = fopen(s_logFileName, "a");

	if(fp)
	{
		time_t t;
		time(&t);

		fprintf(fp, "------------------------------------------\r\n");
		fprintf(fp, "General Error Report\r\n");
		fprintf(fp, "on %s\r\n", ctime(&t));
		fprintf(fp, "%s\r\n", text);
		fclose(fp);
	}
#else
	HANDLE fp = CreateFile(
		s_logFileName,
		GENERIC_WRITE, 
		0, 
		NULL, 
		OPEN_ALWAYS,
		FILE_ATTRIBUTE_NORMAL,
		NULL);

	if(fp)
	{
		DWORD pos = SetFilePointer(fp, 0, (LPLONG) NULL, FILE_END);
		time_t t;
		time(&t);

		if(pos == 0)
			wfprintf(fp, "------------------------------------------\r\n");

		wfprintf(fp, "%s\r\n", title);
		wfprintf(fp, "on %s\r\n", ctime(&t));
		wfprintf(fp, "%s\r\n", text);
		wfprintf(fp, "------------------------------------------\r\n");
		CloseHandle(fp);
	}
#endif
#endif	// NOLOG
}

#ifdef DEBUG
/*
 * Add text to debug Log
 */

void WindowsExceptionUtility::logDebug(const char* text)
{
#ifdef NOT_WINDOWS
	FILE* fp = fopen(s_debugFileName, "a");

	if(fp)
	{
		time_t t;
		time(&t);

		fprintf(fp, "%s", s);
		fclose(fp);
	}
#else

	/*
	 * These 2 lines ensure that 2 threads can't write to 
	 * the logfile at the same time
	 */

#if 0
	// static SharedData critic;
	// LockData lock(&critic);
	// critic.enter();
	static CRITICAL_SECTION critic;
	static BOOL init = FALSE;
	if(!init)
		InitializeCriticalSection(&critic);
	EnterCriticalSection(&critic);
#endif

	HANDLE fp = CreateFile(
	  	s_debugFileName,
		GENERIC_WRITE, 
		0, 
		NULL, 
		OPEN_ALWAYS,
		FILE_ATTRIBUTE_NORMAL,
		NULL);

	if(fp)
	{
		SetFilePointer(fp, 0, (LPLONG) NULL, FILE_END);

		DWORD bw;

		while(*text)
		{
			if(*text == '\n')
			{
				static char eol[] = { '\r', '\n' };

				WriteFile(fp, eol, 2, &bw, NULL);
			}
			else
				WriteFile(fp, text, 1, &bw, NULL);

			text++;
		}

		CloseHandle(fp);
	}

#if 0
	// critic.leave();
	LeaveCriticalSection(&critic);
#endif

#endif
}

#endif


/*
 * Display title and text and give user Yes/No Choice
 * Returns Yes or No
 */

ExceptionUtility::YesNo WindowsExceptionUtility::ask(const char* title, const char* text)
{
	int result = messageBox(title, text, MB_ICONEXCLAMATION | MB_YESNO);
	return (result == IDYES) ? Yes : No;
}

/*
 * Display Information to user and continue
 */

void WindowsExceptionUtility::alert(const char* title, const char* text)
{
	messageBox(title, text, MB_ICONSTOP);
}


int WindowsExceptionUtility::messageBox(const char* title, const char* text, UINT uType)
{
	// BOOL oldPause = gApp.pauseGame(TRUE);
	// int result = MessageBox(gApp.getHwnd(), text, title, uType | MB_SETFOREGROUND);
	int result = MessageBox(NULL, text, title, uType | MB_SETFOREGROUND);
	// gApp.pauseGame(oldPause);

	return result;
}




/*
 * Global Instance for except and myassert to use
 */

// static ExceptionUtility exceptionHandler;
// static WindowsExceptionUtility w95ExceptionUtility;
// ExceptionUtility* exceptionHandler = &w95ExceptionUtility;
// ExceptionHolder exceptionHandler(&w95ExceptionUtility);

static WindowsExceptionUtility w95ExceptionUtility;

void ExceptionUtility::logError(const char* title, const char* text)
{
	w95ExceptionUtility.logError(title, text);
}

ExceptionUtility::YesNo ExceptionUtility::ask(const char* title, const char* text)
{
	return w95ExceptionUtility.ask(title, text);
}

void ExceptionUtility::alert(const char* title, const char* text)
{
	w95ExceptionUtility.alert(title, text);
}

#if defined(DEBUG)
void ExceptionUtility::logDebug(const char* text)
{
	w95ExceptionUtility.logDebug(text);
}
#endif



