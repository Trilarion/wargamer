/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef SYNC_HPP
#define SYNC_HPP

#ifndef __cplusplus
#error sync.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *	Synchronization objects
 *
 * This used to be part of thread, but thread was getting complicated
 *
 *----------------------------------------------------------------------
 */

#include "sysdll.h"
#include "mytypes.h"
#include <windows.h>
#include <map>
#include "winerror.hpp"
#include "mmtimer.hpp"
#include "critical.hpp"

namespace Greenius_System
{

#if 0    // Doesn't need to be virtual HANDLE
/*
 * Virtual SyncObject
 */

class SyncObject {
 public:
 	virtual ~SyncObject() { }
	virtual HANDLE handle() const = 0;

};

/*
 * Base class... contains handle
 */

class SyncObjectH : public SyncObject
{
	HANDLE d_handle;
 public:
 	SYSTEM_DLL SyncObjectH() { d_handle = NULL; }
	SYSTEM_DLL virtual ~SyncObjectH();

	SYSTEM_DLL HANDLE handle() const { return d_handle; }

 protected:		// Normally only the derived class should set the handle!
	void handle(HANDLE h) { d_handle = h; }
};
#else

class SyncObject
{
	HANDLE d_handle;
 public:
 	SYSTEM_DLL SyncObject() : d_handle(NULL) { }
	SYSTEM_DLL virtual ~SyncObject();

	SYSTEM_DLL HANDLE handle() const { return d_handle; }

 protected:		// Normally only the derived class should set the handle!
	void handle(HANDLE h) { d_handle = h; }
};

class SyncObjectH : public SyncObject
{
};


#endif
/*
 * Signal for sending messages to other threads
 */

class Event : public SyncObjectH {
protected:
	Event(BOOL manualEvent, BOOL initialState) { init(manualEvent, initialState); }
private:
	SYSTEM_DLL void init(BOOL manualEvent, BOOL initialState);
public:
	Event() { init(TRUE, FALSE); }
	~Event() { }

	SYSTEM_DLL void set() const;
		// Signal the event

	SYSTEM_DLL void reset() const;
		// Reset event

	SYSTEM_DLL void pulse() const;
		// Pulse event, allowing anything waiting for it to get released

	/*
	 * Functions used to wait and test if it has been signalled
	 */

	SYSTEM_DLL DWORD wait(DWORD milliSeconds) const;
		// Wait for event
		// Returns WAIT_OBJECT_0 if it was signalled
		// Returns WAIT_TIMEOUT if it timed out

	DWORD wait() const { return wait(INFINITE); }
		// Wait forever for event to be signalled

	SYSTEM_DLL Boolean isSet() const;
		// Return True if Event is set

	// DWORD waitFor(Event& waitEvent, DWORD milliSeconds);

	class EventError : public WinError {
	public:
		SYSTEM_DLL EventError() : WinError("Event Error") { }
	};
};

/*
 * Same as Event, but it is automatic
 * i.e. after a wait, the event is automatically reset
 */

class AutoEvent : public Event {
public:
	SYSTEM_DLL AutoEvent() : Event(FALSE, FALSE) { }
};

/*
 * Mutex object: used to controlling access of data between threads
 */

class Mutex : public SyncObjectH {
public:
	SYSTEM_DLL Mutex();
	SYSTEM_DLL ~Mutex() { }

	SYSTEM_DLL void get() const;
	SYSTEM_DLL void release() const;

	class MutexError : public WinError {
	public:
		SYSTEM_DLL MutexError() : WinError("Mutex Error") { }
	};
};

/*
 * Read/Write Access
 */

class RWLock {
	typedef std::map<DWORD, LONG, std::less<DWORD> > ReadCounters;

	Mutex d_inUse;
   Mutex d_writeLock;
   // SharedData d_inUse;
   // SharedData d_writeLock;
	AutoEvent d_counterCleared;
   mutable ReadCounters d_readCounters;
	// ULONG d_writeCounter;

#ifdef DEBUG
   mutable int d_writeCount;    // Useful in debugger for seeing how many times it has been locked
   mutable int d_readCount;
#endif

public:
	SYSTEM_DLL RWLock();
	SYSTEM_DLL virtual ~RWLock();

	SYSTEM_DLL void startRead() const;
	SYSTEM_DLL void endRead() const;

	SYSTEM_DLL void startWrite();
	SYSTEM_DLL void endWrite();
private:
	// ReadCount* getCount();
	bool isReading() const;
};

/*
 * Utilities to work with RWLock
 */

class Reader
{
	const RWLock* d_lock;
 public:
 	SYSTEM_DLL Reader(const RWLock* lock) : d_lock(lock)
 	{
 	   if(lock)
 	      lock->startRead();
 	}

	SYSTEM_DLL ~Reader()
	{
	   if(d_lock)
	   {
	      d_lock->endRead();
	      d_lock = 0;
	   }
	}
};

class Writer
{
	RWLock* d_lock;
 public:
 	SYSTEM_DLL Writer(RWLock* lock) : d_lock(lock)
 	{
 	   if(lock)
 	      lock->startWrite();
 	}

	SYSTEM_DLL ~Writer()
	{
	   if(d_lock)
	   {
	      d_lock->endWrite();
	      d_lock = 0;
	   }
   }
};

/*
 * Waitable Timer
 * Note that NT4.0 has a set of WaitableTimer functions, but W95 doesn't
 * So we wil emulate it.
 *
 * Uses MultiMedia Timer, because otherwise any threads that use it must
 * provide a message queue.
 */

// class WaitableTimer : public SyncObject, private MMTimer {
class WaitableTimer : public AutoEvent, private MMTimer {
		// Disable copy

		WaitableTimer(const WaitableTimer&);
		WaitableTimer& operator = (const WaitableTimer&);

	public:
 		SYSTEM_DLL WaitableTimer(UINT ms, Boolean started);
			// Create Timer Object with rate in milliseconds
			// if started is True, then timer immediately starts
			// otherwise you must call start to get it going

		SYSTEM_DLL ~WaitableTimer();
			// Destructor

		// SYSTEM_DLL HANDLE handle() const { return d_event.handle(); }
			// Implementation of SyncObject::handle()

		SYSTEM_DLL void setRate(UINT ms) { MMTimer::setRate(ms); }
			// Set up a new rate
		SYSTEM_DLL int getRate() const { return MMTimer::getRate(); }

		SYSTEM_DLL void start();
			// Start the timer

		SYSTEM_DLL void stop();
			// Stop the timer

		class WaitableTimerError : public GeneralError
		{
	 	public:
	 		SYSTEM_DLL WaitableTimerError(const char* s) : GeneralError(s) { }
		};

	private:
		virtual void proc();
			// Implement MMTimer function

	private:
		// AutoEvent d_event;			// Object to signal
};

/*
 * Counting timer
 * Similar to WaitableTimer but keeps a counter so that if the
 * receiving thread can not respond quick enough, it can get a
 * count of howmany interrupts have occured
 */

// class WaitableCounter : public SyncObject, private MMTimer
class WaitableCounter : public AutoEvent, private MMTimer
{
      WaitableCounter(const WaitableCounter&);
      WaitableCounter& operator=(const WaitableCounter&);
	public:
		SYSTEM_DLL WaitableCounter(UINT ms, bool autoStart);
			// Create counter with a period of ms milliseconds
		SYSTEM_DLL ~WaitableCounter();

		SYSTEM_DLL void start() { MMTimer::start(); }
		SYSTEM_DLL void stop() { MMTimer::stop(); }

		// SYSTEM_DLL HANDLE handle() const { return d_event.handle(); }
			// Implement SuncObject

		SYSTEM_DLL int getAndClear();
			// Get the counter value and clear it

	private:
		virtual void proc();
			// Implement MMTimer

	private:
		// AutoEvent	d_event;		// Object to wait on
		int			d_value;		// Counter Value
		SharedData	d_lock;		// Make it thread safe
};

};	// namespace Greenius_System

// For backwards compatibility

using Greenius_System::SyncObject;
using Greenius_System::Event;
using Greenius_System::AutoEvent;
using Greenius_System::Mutex;
using Greenius_System::RWLock;
using Greenius_System::Reader;
using Greenius_System::Writer;
using Greenius_System::WaitableTimer;


#endif /* SYNC_HPP */

/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.2  2001/06/18 08:25:46  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
