/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1995, Steven Morle-Green, All Rights Reserved
 *----------------------------------------------------------------------
 * Palette Manager for Windows 95
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "palette.hpp"
#include "myassert.hpp"
#include "sllist.hpp"
#include "misc.hpp"
#include "refptr.hpp"
#include <map>     // STL class
// #include "wargame.hpp"

#define STATIC_INIT_PALETTE
#include "staticInit.h"

#ifdef DEBUG
// #define PALETTE_DEBUG

#ifdef PALETTE_DEBUG

#include "clog.hpp"
LogFile palLog("palette.log");

#endif

#endif


#if defined(DEBUG) && !defined(NOLOG)
#define PALETTE_LOG

#include "audit.hpp"

#ifdef PALETTE_LOG

class PALAudit : public AuditLog<const CPalette*>
{
   public:
   PALAudit() : AuditLog<const CPalette*>("Palette.log") { }
   ~PALAudit() { showState(); }

   virtual void logItem(const CPalette* pal)
   {
      char buffer[10];
      sprintf(buffer, "%p", pal);

      *d_log << buffer << " (" << pal->id() << ")";
   }
};

static PALAudit s_palLog;

#endif   // PALETTE_LOG
#endif      // defined(DEBUG) && !defined(NOLOG)

/*
 * Global Palette
 */

// static HPalette Palette::s_globalPalette;
// static Palette::PaletteID Palette::s_id = 0;

/*
 * global Palette Manipulators
 */

HPalette::~HPalette()
{
   if (d_hPalette != NULL)
   {
      DeleteObject(d_hPalette);
      d_hPalette = 0;
   }
}


void HPalette::set(HPALETTE newPal)
{
   if(d_hPalette != NULL)
      DeleteObject(d_hPalette);
   d_hPalette = newPal;
}

HPALETTE HPalette::get() const
{
   if(d_hPalette != NULL)
      return d_hPalette;
   else
      return (HPALETTE) GetStockObject(DEFAULT_PALETTE);
}


HPALETTE Palette::get()
{
   if(CPalette::defaultPalette())
      return CPalette::defaultPalette()->handle();
   else
      return (HPALETTE) GetStockObject(DEFAULT_PALETTE);
}


CPalette::ID Palette::paletteID()
{
   // ASSERT(CPalette::defaultPalette());
   if(CPalette::defaultPalette())
      return CPalette::defaultPalette()->id();
   else
      return 0;
}

#if 0    // This might have some use later on...
/*
 * Clear system palette so that makeIndentityPalette will work
 */

static void Palette::clearSystemPalette()
{
#ifdef DEBUG
   debugLog("clearSystemPalette()\n");
#endif

   struct {
      WORD version;
      WORD nEntries;
      PALETTEENTRY aEntries[256];
   } pal = {
      0x300,
      256
   };

   for(int count = 0; count < 256; count++)
   {
      pal.aEntries[count].peRed = 0;
      pal.aEntries[count].peGreen = 0;
      pal.aEntries[count].peBlue = 0;
      pal.aEntries[count].peFlags = PC_NOCOLLAPSE;
   }

   HDC hdc = GetDC(NULL);
   HPALETTE blackP = CreatePalette((LOGPALETTE*) &pal);
   if(blackP)
   {
      HPALETTE oldPal = SelectPalette(hdc, blackP, FALSE);
      RealizePalette(hdc);
      SelectPalette(hdc, oldPal, FALSE);
      DeleteObject(blackP);
   }
   ReleaseDC(NULL, hdc);
}

/*
 * Set up identity palette that can be selected into a DC
 */

HPALETTE Palette::makeIdentityPalette(LPRGBQUAD aRGB, int nColors)
{
#ifdef PALETTE_DEBUG
   palLog.printf("+makeIdentityPalette()");
#endif

   clearSystemPalette();

   struct Palette {
      WORD version;
      WORD entryCount;
      PALETTEENTRY aEntries[256];
   };

   Palette palette = { 0x300, 256 };

   HDC hdc = GetDC(NULL);

   int nStaticColors = GetDeviceCaps(hdc, NUMCOLORS);
   GetSystemPaletteEntries(hdc, 0, 256, palette.aEntries);
   nStaticColors = nStaticColors / 2;
   for(int i = 0; i < nStaticColors; i++)
      palette.aEntries[i].peFlags = 0;

   int nUsableColors = nColors - nStaticColors;
   LPRGBQUAD lc = aRGB;
   int nc = 0;

   for(; (i < nUsableColors) && (nc < nColors); lc++, nc++)
   {
      /*
      * Check if color already added
      */

      BOOL exists = FALSE;
      for(int n = 0; n < i; n++)
      {
         if( (palette.aEntries[n].peRed == lc->rgbRed) &&
            (palette.aEntries[n].peGreen == lc->rgbGreen) &&
            (palette.aEntries[n].peBlue == lc->rgbBlue) )
         {
            exists = TRUE;
            break;
         }
      }

      if(exists)
         continue;

      /*
      * Check Top set of system colors
      */

      for(n = 256 - nStaticColors; n < nColors; n++)
      {
         if( (palette.aEntries[n].peRed == lc->rgbRed) &&
            (palette.aEntries[n].peGreen == lc->rgbGreen) &&
            (palette.aEntries[n].peBlue == lc->rgbBlue) )
         {
            exists = TRUE;
            break;
         }
      }

      if(exists)
         continue;

      palette.aEntries[i].peRed = lc->rgbRed;
      palette.aEntries[i].peGreen = lc->rgbGreen;
      palette.aEntries[i].peBlue = lc->rgbBlue;
      palette.aEntries[i].peFlags = PC_NOCOLLAPSE;
      i++;
   }

   for(; i < (256 - nStaticColors); i++)
      palette.aEntries[i].peFlags = 0;

#ifdef PALETTE_DEBUG
   // OutputDebugString("Palette\n");

   palLog.printf("Palette");
   for(i = 0; i < 256; i++)
   {
      // char buffer[100];
      palLog.printf("%d: %d,%d,%d [%d]",
         i,
         (int) palette.aEntries[i].peRed,
         (int) palette.aEntries[i].peGreen,
         (int) palette.aEntries[i].peBlue,
         (int) palette.aEntries[i].peFlags);
   }
#endif

   /*
   * Right... we've finished fiddling with the palette
   *
   * Make a HPALETTE
   * Realize it and find what colors we really have.
   */

   HPALETTE hPal = CreatePalette((LOGPALETTE*) &palette);

   SelectPalette(hdc, hPal, FALSE);
   RealizePalette(hdc);


   ReleaseDC(NULL, hdc);

#ifdef PALETTE_DEBUG
   palLog.printf("-makeIdentityPalette()");
#endif

   return hPal;

}
#endif      // Old functions to make identity palette

/*
 * Compatibility Functions
 */

ColorIndex Palette::getColorIndex(COLORREF cRef)
{
   ASSERT(CPalette::defaultPalette());
   if(CPalette::defaultPalette())
      return CPalette::defaultPalette()->getIndex(cRef);
   else
   {
      HPALETTE hPal = reinterpret_cast<HPALETTE>(GetStockObject(DEFAULT_PALETTE));
      return static_cast<ColorIndex>(GetNearestPaletteIndex(hPal, cRef));
   }
}

/*===========================================================
 * Miscellaneous color remapping functions
 */

/*
 * Alter base color
 */

inline UBYTE alterColorChannel(UBYTE original, UBYTE bright, UBYTE contrast)
{
   int newVal = (original * contrast) / 256u;
   newVal += bright - contrast;
   if(newVal < 0)
      newVal = 0;
   else if(newVal >= 256)
      newVal = 255;

   return UBYTE(newVal);
}

COLORREF Palette::brightColor(COLORREF rgb, UBYTE bright, UBYTE contrast)
{
   UBYTE r = alterColorChannel(GetRValue(rgb), bright, contrast);
   UBYTE g = alterColorChannel(GetGValue(rgb), bright, contrast);
   UBYTE b = alterColorChannel(GetBValue(rgb), bright, contrast);
   return PALETTERGB(r, g, b);
}


COLORREF Palette::brighten(COLORREF rgb, int percent)
{
   ASSERT(percent >= 0);
   ASSERT(percent <= 100);

   int r = GetRValue(rgb);
   int g = GetGValue(rgb);
   int b = GetBValue(rgb);

   r += ((255 - r) * percent) / 100;
   g += ((255 - g) * percent) / 100;
   b += ((255 - b) * percent) / 100;

   ASSERT(r < 256);
   ASSERT(g < 256);
   ASSERT(b < 256);

   r = minimum(0xff, r);
   g = minimum(0xff, g);
   b = minimum(0xff, b);

   return PALETTERGB(r,g,b);
}

COLORREF Palette::darken(COLORREF rgb, int percent)
{
   ASSERT(percent >= 0);
   ASSERT(percent <= 100);

   int r = GetRValue(rgb);
   int g = GetGValue(rgb);
   int b = GetBValue(rgb);

   r -= (r * percent) / 100;
   g -= (g * percent) / 100;
   b -= (b * percent) / 100;

   ASSERT(r >= 0);
   ASSERT(g >= 0);
   ASSERT(b >= 0);

   r = maximum(0, r);
   g = maximum(0, g);
   b = maximum(0, b);

   return PALETTERGB(r,g,b);
}

/*===========================================================
 * Color Remap Table Manager and cacheing system
 */

#ifdef _MSC_VER  // visual C++ gets confused

struct RemapTypes
{
      typedef int Percent;

      enum Style
      {
         STYLE_Remap,
         STYLE_Light,
         STYLE_Dark,
         STYLE_Bright,
         STYLE_Colorize,
         STYLE_BlackWhite,

         STYLE_HowMany
      };
};

struct TableInfo : private RemapTypes
{
   Style d_style;          // What type of map is it
   CPalette::ID d_id;      // Which palette it belongs to
   CPalette::ID d_mapToID; // Palette Remapped to if remap
   Percent d_percent;      // Amount of effect
   COLORREF d_rgb;         // Color for Colorize

   TableInfo() :
      d_style(STYLE_HowMany),
      d_id(0),
      d_mapToID(0),
      d_percent(0),
      d_rgb(RGB(0,0,0))
   {
   }

   TableInfo(Style style, CPalette::ID id, CPalette::ID id2, Percent p, COLORREF rgb) :
      d_style(style),
      d_id(id),
      d_mapToID(id2),
      d_percent(p),
      d_rgb(rgb)
   {
   }

   friend bool operator == (const TableInfo& inf1, const TableInfo& inf2);
   friend bool operator < (const TableInfo& inf1, const TableInfo& inf2);
};
#endif   // visual c++


class RemapManager
#ifdef _MSC_VER
   : private RemapTypes
#endif
{
   public:
#ifndef _MSC_VER
      typedef int Percent;

      enum Style
      {
         STYLE_Remap,
         STYLE_Light,
         STYLE_Dark,
         STYLE_Bright,
         STYLE_Colorize,
         STYLE_BlackWhite,

         STYLE_HowMany
      };
#endif

      class TableMaker
      {
         public:
            virtual void makeTable(ColorRemapTable* table, const CPalette* srcPal) const = 0;
      };

#ifndef _MSC_VER  // visual C++ gets confused
   private:
      struct TableInfo
      {
         Style d_style;          // What type of map is it
         CPalette::ID d_id;      // Which palette it belongs to
         CPalette::ID d_mapToID; // Palette Remapped to if remap
         Percent d_percent;      // Amount of effect
         COLORREF d_rgb;         // Color for Colorize

         TableInfo() :
            d_style(STYLE_HowMany),
            d_id(0),
            d_mapToID(0),
            d_percent(0),
            d_rgb(RGB(0,0,0))
         {
         }

         TableInfo(Style style, CPalette::ID id, CPalette::ID id2, Percent p, COLORREF rgb) :
            d_style(style),
            d_id(id),
            d_mapToID(id2),
            d_percent(p),
            d_rgb(rgb)
         {
         }

         friend bool operator == (const TableInfo& inf1, const TableInfo& inf2);
         friend bool operator < (const TableInfo& inf1, const TableInfo& inf2);
      };
#endif   // visual c++

      RemapManager();
      ~RemapManager() { }
   public:

      static RemapManager& instance();

      // Get/Create functions

      const ColorRemapTable* getRemapTable(const CPalette* srcPal, const CPalette* destPal);
      const ColorRemapTable* getLightTable(const CPalette* srcPal, Percent percent);
      const ColorRemapTable* getDarkTable(const CPalette* srcPal, Percent percent);
      const ColorRemapTable* getBrightTable(const CPalette* srcPal, Percent percent);
      const ColorRemapTable* getColorizeTable(const CPalette* srcPal, Percent percent, COLORREF rgb);
      const ColorRemapTable* getMonoTable(const CPalette* srcPal);

      // Remove all tables that use given palette

      void remove(CPalette::ID id);

   private:
      const ColorRemapTable* getTable(const CPalette* srcPal, const TableInfo& info, const TableMaker& maker);

   private:
      typedef std::map<TableInfo, ColorRemapTable, std::less<TableInfo> > Container;
      Container d_items;
};


RemapManager::RemapManager() : 
   d_items() 
{ 
}

RemapManager& RemapManager::instance()
{
//   static RemapManager s_instance;
//   return s_instance;
   // Temporarily use a pointer to stop it from being deleted
   // because of problems of order of static destruction.
   // Replace with a ref counted object... or some other way
   // of ensuring it is not used after being deleted.
   static RemapManager* s_instance;
   if(!s_instance) s_instance = new RemapManager;
   return *s_instance;
}



#ifdef _MSC_VER
inline bool operator == (const TableInfo& inf1, const TableInfo& inf2)
#else
inline bool operator == (const RemapManager::TableInfo& inf1, const RemapManager::TableInfo& inf2)
#endif
{
   return (inf1.d_style == inf2.d_style)
      &&  (inf1.d_id == inf2.d_id)
      &&  (inf1.d_mapToID == inf2.d_mapToID)
      &&  (inf1.d_percent == inf2.d_percent)
      &&  (inf1.d_rgb == inf2.d_rgb);
}

#ifdef _MSC_VER
inline bool operator < (const TableInfo& inf1, const TableInfo& inf2)
#else
inline bool operator < (const RemapManager::TableInfo& inf1, const RemapManager::TableInfo& inf2)
#endif
{
   if(inf1.d_style != inf2.d_style)
      return (inf1.d_style < inf2.d_style);
   if(inf1.d_id != inf2.d_id)
      return (inf1.d_id < inf2.d_id);
   if(inf1.d_mapToID != inf2.d_mapToID)
      return (inf1.d_mapToID < inf2.d_mapToID);
   if(inf1.d_percent != inf2.d_percent)
      return (inf1.d_percent < inf2.d_percent);
   return (inf1.d_rgb < inf2.d_rgb);
}

void RemapManager::remove(CPalette::ID id)
{
   Container::iterator it = d_items.begin();
   while (it != d_items.end())
   {
      if ( ((*it).first.d_id == id)
       || ((*it).first.d_mapToID == id) )
      {
         ASSERT( (*it).second.refCount() == 0);

         d_items.erase(it);

         /*
          * Restart iterator because erase probably
          * invalidates iterators
          */

         it = d_items.begin();
      }
      else
         ++it;
   }
}

#ifdef _MSC_VER
const ColorRemapTable* RemapManager::getTable(const CPalette* srcPal, const TableInfo& info, const TableMaker& maker)
#else
const ColorRemapTable* RemapManager::getTable(const CPalette* srcPal, const RemapManager::TableInfo& info, const TableMaker& maker)
#endif
{
   Container::const_iterator it = d_items.find(info);
   if(it != d_items.end())
   {
      return &(*it).second;
   }
   else
   {
      ColorRemapTable* table = &d_items[info];
      maker.makeTable(table, srcPal);
      return table;
   }
}

namespace
{

inline UBYTE lightenRGBValue(UBYTE v, int percent)
{
   return v + ((255 - v) * percent) / 100;
}

inline UBYTE darkenRGBValue(UBYTE v, int percent)
{
   return v - (v * percent) / 100;
}

};

const ColorRemapTable* RemapManager::getRemapTable(const CPalette* srcPal, const CPalette* destPal)
{
   class RemapTableMaker : public RemapManager::TableMaker
   {
      public:
         RemapTableMaker(const CPalette* pal) : d_pal(pal) { }

         void makeTable(ColorRemapTable* table, const CPalette* srcPal) const
         {
            if (*srcPal == *d_pal)
            {
               for(unsigned int i = 0; i < srcPal->getNColors(); ++i)
               {
                  (*table)[static_cast<ColorIndex>(i)] = static_cast<ColorIndex>(i);
               }
            }
            else
            {
               const RGBQUAD* colors = srcPal->colors();
               for(unsigned int i = 0; i < srcPal->getNColors(); ++i)
               {
                  const RGBQUAD& rgb = colors[i];
                  COLORREF colRef = RGB(rgb.rgbRed, rgb.rgbGreen, rgb.rgbBlue);
                  (*table)[static_cast<ColorIndex>(i)] = d_pal->getIndex(colRef);
               }
            }
         }
      private:
         const CPalette* d_pal;
   };

   TableInfo info(STYLE_Remap, srcPal->id(), destPal->id(), 0, RGB(0,0,0));
   return getTable(srcPal, info, RemapTableMaker(destPal));
}

const ColorRemapTable* RemapManager::getLightTable(const CPalette* srcPal, Percent percent)
{
   class RemapTableMaker : public RemapManager::TableMaker
   {
      public:
         RemapTableMaker(Percent percent) : d_percent(percent) { }
         void makeTable(ColorRemapTable* table, const CPalette* srcPal) const
         {
            const RGBQUAD* colors = srcPal->colors();
            for(unsigned int i = 0; i < srcPal->getNColors(); ++i)
            {
               const RGBQUAD& rgb = colors[i];

               COLORREF colRef = RGB(
                  lightenRGBValue(rgb.rgbRed, d_percent),
                  lightenRGBValue(rgb.rgbGreen, d_percent),
                  lightenRGBValue(rgb.rgbBlue, d_percent));

               (*table)[static_cast<ColorIndex>(i)] = srcPal->getIndex(colRef);
            }
         }
      private:
         Percent d_percent;
   };

   TableInfo info(STYLE_Light, srcPal->id(), 0, percent, RGB(0,0,0));
   return getTable(srcPal, info, RemapTableMaker(percent));
}

const ColorRemapTable* RemapManager::getDarkTable(const CPalette* srcPal, Percent percent)
{
   class RemapTableMaker : public RemapManager::TableMaker
   {
      public:
         RemapTableMaker(Percent percent) : d_percent(percent) { }
         void makeTable(ColorRemapTable* table, const CPalette* srcPal) const
         {
            const RGBQUAD* colors = srcPal->colors();
            for(unsigned int i = 0; i < srcPal->getNColors(); ++i)
            {
               const RGBQUAD& rgb = colors[i];

               COLORREF colRef = RGB(
                  darkenRGBValue(rgb.rgbRed, d_percent),
                  darkenRGBValue(rgb.rgbGreen, d_percent),
                  darkenRGBValue(rgb.rgbBlue, d_percent));

               (*table)[static_cast<ColorIndex>(i)] = srcPal->getIndex(colRef);
            }
         }
      private:
         Percent d_percent;
   };

   TableInfo info(STYLE_Dark, srcPal->id(), 0, percent, RGB(0,0,0));
   return getTable(srcPal, info, RemapTableMaker(percent));
}

const ColorRemapTable* RemapManager::getBrightTable(const CPalette* srcPal, Percent percent)
{
   class RemapTableMaker : public RemapManager::TableMaker
   {
      public:
         RemapTableMaker(Percent percent) : d_percent(percent) { }
         void makeTable(ColorRemapTable* table, const CPalette* srcPal) const
         {
            const RGBQUAD* colors = srcPal->colors();
            for(unsigned int i = 0; i < srcPal->getNColors(); ++i)
            {
               const RGBQUAD& rgb = colors[i];
               UBYTE r = rgb.rgbRed;
               UBYTE g = rgb.rgbGreen;
               UBYTE b = rgb.rgbBlue;

               // find brightest of primary colors

               int maxVal = maximum(maximum(r, g), b);
               int height = maxVal + ((255 - maxVal) * d_percent) / 100;
               if(maxVal != 0)
               {
                  r = (r * height) / maxVal;
                  g = (g * height) / maxVal;
                  b = (b * height) / maxVal;
               }

               COLORREF colRef = RGB(r, g, b);

               (*table)[static_cast<ColorIndex>(i)] = srcPal->getIndex(colRef);
            }
         }
      private:
         Percent d_percent;
   };

   TableInfo info(STYLE_Bright, srcPal->id(), 0, percent, RGB(0,0,0));
   return getTable(srcPal, info, RemapTableMaker(percent));
}

const ColorRemapTable* RemapManager::getColorizeTable(const CPalette* srcPal, Percent percent, COLORREF rgb)
{
   class RemapTableMaker : public RemapManager::TableMaker
   {
      public:
         RemapTableMaker(Percent percent, COLORREF rgb) : d_percent(percent), d_rgb(rgb) { }
         void makeTable(ColorRemapTable* table, const CPalette* srcPal) const
         {
            BYTE red = GetRValue(d_rgb);
            BYTE green = GetGValue(d_rgb);
            BYTE blue = GetBValue(d_rgb);

            const RGBQUAD* colors = srcPal->colors();
            for(unsigned int i = 0; i < srcPal->getNColors(); ++i)
            {
               const RGBQUAD& rgb = colors[i];

               BYTE r = rgb.rgbRed;
               BYTE g = rgb.rgbGreen;
               BYTE b = rgb.rgbBlue;

               r += ((red - r) * d_percent) / 100;
               g += ((green - g) * d_percent) / 100;
               b += ((blue - b) * d_percent) / 100;

               COLORREF colRef = RGB(r, g, b);

               (*table)[static_cast<ColorIndex>(i)] = srcPal->getIndex(colRef);
            }
         }
      private:
         Percent d_percent;
         COLORREF d_rgb;
   };

   TableInfo info(STYLE_Colorize, srcPal->id(), 0, percent, rgb);
   return getTable(srcPal, info, RemapTableMaker(percent, rgb));
}

const ColorRemapTable* RemapManager::getMonoTable(const CPalette* srcPal)
{
   class RemapTableMaker : public RemapManager::TableMaker
   {
      public:
         RemapTableMaker() { }
         void makeTable(ColorRemapTable* table, const CPalette* srcPal) const
         {
            const ColorIndex black = srcPal->getIndex(PALETTERGB(0, 0, 0));
            const ColorIndex white = srcPal->getIndex(PALETTERGB(255, 255, 255));

            const RGBQUAD* colors = srcPal->colors();
            for(unsigned int i = 0; i < srcPal->getNColors(); ++i)
            {
               const RGBQUAD& rgb = colors[i];
               UBYTE r = rgb.rgbRed;
               UBYTE g = rgb.rgbGreen;
               UBYTE b = rgb.rgbBlue;

               const int cutoff = (0x80 * 0x80 * 3) / 2;
               int intensity = r*r + g*g + b*b;

               (*table)[static_cast<ColorIndex>(i)] = (intensity < cutoff) ? white : black;
            }
         }
   };

   TableInfo info(STYLE_BlackWhite, srcPal->id(), 0, 0, RGB(0,0,0));
   return getTable(srcPal, info, RemapTableMaker());
}

/*==============================================================
 * CPalette Implementation
 */



CPalette::ID CPalette::s_nextID = 1;
PalettePtr CPalette::s_default = 0;         // Default Palette

CPalette::CPalette(unsigned int nColors, const RGBQUAD* colors) :
   d_handle(),
   d_nColors(nColors),
   d_id(s_nextID++)
{
#ifdef PALETTE_LOG
   s_palLog.logNew(this);
#endif

   ASSERT(nColors > 0);
   ASSERT(nColors <= MaxColors);

   /*
   * Copy RGB Values
   */

   {
      for (unsigned int i = 0; i < nColors; ++i)
         d_colors[i] = colors[i];
   }

   /*
   * Create a handle
   */

   struct LogPalette {
      WORD version;
      WORD entryCount;
      PALETTEENTRY aEntries[MaxColors];

      LOGPALETTE* getLOGPALETTE()
      {
         return reinterpret_cast<LOGPALETTE*>(this);
      }
   };

   LogPalette logPal;
   logPal.version = 0x300;
   logPal.entryCount = static_cast<WORD>(nColors);
   {
      for (unsigned int i = 0; i <nColors; ++i)
      {
         logPal.aEntries[i].peRed = colors[i].rgbRed;
         logPal.aEntries[i].peGreen = colors[i].rgbGreen;
         logPal.aEntries[i].peBlue = colors[i].rgbBlue;
         logPal.aEntries[i].peFlags = PC_NOCOLLAPSE;
      }
   }
   HPALETTE hPal = CreatePalette(logPal.getLOGPALETTE());
   ASSERT(hPal != 0);
   d_handle.set(hPal);
}

CPalette::CPalette(HPALETTE hPal) :
   d_handle(),
   d_nColors(0),
   d_id(0)
{
#ifdef PALETTE_LOG
   s_palLog.logNew(this);
#endif

   d_handle.set(hPal);
}

CPalette::~CPalette()
{
#ifdef PALETTE_LOG
   s_palLog.logDelete(this);
#endif
}

PalettePtr CPalette::create(unsigned int nColors, RGBQUAD* colors)
{
   CPalette* pal = new CPalette(nColors, colors);     //lint !e429 ... custodial pointer
   // PaletteManager::instance()->add(pal);
   return pal;
}

PalettePtr CPalette::defaultPalette()
{
   if (!s_default)
   {
#if 0
      PALETTEENTRY pal[MaxColors];
      HDC hdc = GetDC(NULL);
      UINT n = GetSystemPaletteEntries(hdc, 0, MaxColors, pal);
      ReleaseDC(NULL, hdc);
      ASSERT(n != 0);
      ASSERT(n <= MaxColors);
      RGBQUAD rgb[MaxColors];
      for (unsigned int i = 0; i < n; ++i)
      {
         rgb[i].rgbRed = pal[i].peRed;
         rgb[i].rgbGreen = pal[i].peGreen;
         rgb[i].rgbBlue = pal[i].peBlue;
         rgb[i].rgbReserved = 0;
      }
      s_default = create(n, rgb);
#else
      HPALETTE hPal = static_cast<HPALETTE>(GetStockObject(DEFAULT_PALETTE));
      ASSERT(hPal != NULL);
      CPalette* pal = new CPalette(hPal);
      return pal;;
#endif
   }

   return s_default;
}

// Make this palette the default
void CPalette::makeDefault() const
{
   s_default = this;

   // Realize the screen's palette

   // HDC hdc = CreateCompatibleDC(NULL);
   HDC hdc = GetDC(NULL);
   HPALETTE oldPal = SelectPalette(hdc, d_handle.get(), TRUE);
   RealizePalette(hdc);
   SelectPalette(hdc, oldPal, FALSE);
   ReleaseDC(NULL, hdc);
   // DeleteDC(hdc);


}

// Get closest color
ColorIndex CPalette::getIndex(COLORREF rgb) const
{
   ASSERT(d_handle.get());

   UINT col = GetNearestPaletteIndex(d_handle.get(), rgb);
   ASSERT(col != CLR_INVALID);
   // ASSERT(col >= 0);
   ASSERT((d_nColors == 0) || (col < d_nColors));
   return static_cast<ColorIndex>(col);
}

void CPalette::deleteMe()
{
   RemapManager::instance().remove(d_id);

   delete this;
}



/*
 * Called when reference count gets to zero
 */

void ColorRemapTable::deleteMe() const
{
   // delete const_cast<ColorRemapTable*>(this);
}

/*
 * Obtain Remap tables from the Remap Manager
 */

CPalette::RemapTablePtr CPalette::makeRemapTable(const PalettePtr& from, const PalettePtr& to)
{
   return RemapManager::instance().getRemapTable(from, to);
}
CPalette::RemapTablePtr CPalette::getDarkRemapTable(int percent) const
{
   return RemapManager::instance().getDarkTable(this, percent);
}

CPalette::RemapTablePtr CPalette::getLightRemapTable(int percent) const
{
   return RemapManager::instance().getLightTable(this, percent);
}

CPalette::RemapTablePtr CPalette::getBrightRemapTable(int percent) const
{
   return RemapManager::instance().getBrightTable(this, percent);
}

CPalette::RemapTablePtr CPalette::getBlackWhiteRemapTable() const
{
   return RemapManager::instance().getMonoTable(this);
}

CPalette::RemapTablePtr CPalette::getColorizeTable(COLORREF rgb, int percent) const
{
   return RemapManager::instance().getColorizeTable(this, percent, rgb);
}


