/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
#ifndef FSALLOC_HPP
#define FSALLOC_HPP

#ifdef __WATCOM_CPLUSPLUS__
#pragma 549 9     // diable sizeof warnings
#pragma 549 5
#endif

#include "sysdll.h"

/*
 * Users of this class should be at least = to sizeof(void*)
 */

class BlockList;

class SYSTEM_DLL FixedSize_Allocator {
    BlockList* d_blockAllocator;     // Memory Pool
    const size_t d_size;
    const size_t d_chunkSize;
#ifdef DEBUG
    int d_instanceCount;             // Help detect memory leaks
    char* d_className;
#endif
    struct Link {
       Link* next;
    } *d_freeList;

    FixedSize_Allocator(const FixedSize_Allocator&);           // unimplemented
    FixedSize_Allocator& operator=(const FixedSize_Allocator); // unimplemented
  public:
#ifdef DEBUG
    FixedSize_Allocator(size_t size, size_t chunkSize, const char* className);
#else
    FixedSize_Allocator(size_t size, size_t chunkSize);
#endif
    ~FixedSize_Allocator();

    void* alloc(size_t size);
    void free(void* deadObject, size_t size);

    void dryUp();                  // release allocated memory from this pool

  private:
    void replenish();

};

inline void* FixedSize_Allocator::alloc(size_t size)
{
  if(size != d_size)
    return new char[size];

  if(!d_freeList)
    replenish();

  Link* p = d_freeList;
  d_freeList = p->next;
#ifdef DEBUG
  ++d_instanceCount;
#endif
  return p;
}

inline void FixedSize_Allocator::free(void* deadObject, size_t size)
{
  if(deadObject == 0)
    return;

  if(size != d_size)
  {
    delete[] static_cast<char*>(deadObject);
    return;
  }

  Link* p = static_cast<Link*>(deadObject);
  p->next = d_freeList;
  d_freeList = p;
#ifdef DEBUG
  --d_instanceCount;
#endif
}


template<class T>
class FixedSizeAlloc
{
   public:
#ifdef DEBUG
      FixedSizeAlloc(const char* describe) : m_alloc(sizeof(T), T::ChunkSize, describe) { }
#else
      FixedSizeAlloc() : m_alloc(sizeof(T), T::ChunkSize) { }
#endif

      void* alloc(size_t size)
      {
         ASSERT(size == sizeof(T));
         return m_alloc.alloc(size);
      }

      void release(void* deadObject, size_t size = sizeof(T))
      {
         ASSERT(size == sizeof(T));
         m_alloc.free(deadObject, size);
      }

      void dryUp()
      {
         m_alloc.dryUp();
      }

   private:
      FixedSize_Allocator m_alloc;
};



#endif

/*
 *----------------------------------------------------------------------
 * $Log$
 *----------------------------------------------------------------------
 */
