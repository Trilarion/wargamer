/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef FILEITER_HPP
#define FILEITER_HPP

#ifndef __cplusplus
#error fileiter.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *	Iterate through wildcards in directory
 *
 *----------------------------------------------------------------------
 */

#include "sysdll.h"

class SYSTEM_DLL FindFileIterator
{
	public:
		FindFileIterator(const char* wild);
			// constructor
		~FindFileIterator();
			// destructor

		const char* name();
			// get name of last file

		bool operator ++();
		bool operator ++(int) { return operator ++(); }
			// Increment

		bool operator()() const { return d_valid; }
			// check if still valid

	private:
		HANDLE d_handle;
		WIN32_FIND_DATA d_data;
		bool d_valid;
};


#endif /* FILEITER_HPP */

