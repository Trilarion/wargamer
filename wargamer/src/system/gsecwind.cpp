/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#include "stdinc.hpp"
#include "gsecwind.hpp"
#include "dib.hpp"
#include "dib_util.hpp"
#include "palette.hpp"

//DrawDIBDC* GSecWind::s_dib = 0;
//DCObject* GSecWind::s_dc = 0;
UWORD GSecWind::s_instanceCount = 0;

GSecWind::~GSecWind()
{
	ASSERT((s_instanceCount - 1) >= 0);

    selfDestruct();

	if(--s_instanceCount == 0)
	{
#if 0
	 if(s_ddc)
	 {
		delete s_ddc;
		s_ddc = 0;
	 }
#endif
	}

	if(d_dib)
		delete d_dib;
}

void GSecWind::onPaint(HWND hwnd)
{
	RECT r;
	GetClientRect(hwnd, &r);

	PAINTSTRUCT ps;
	HDC hdc = BeginPaint(hwnd, &ps);

	HPALETTE oldPal = SelectPalette(hdc, Palette::get(), FALSE);


	if( (d_cx != r.right - r.left) || (d_cy != r.bottom - r.top) )
	{
		d_cx = r.right - r.left;
		d_cy = r.bottom - r.top;
		d_flags |= SizeChanged;

		if(!d_dib || d_dib->getWidth() != (r.right - r.left) || d_dib->getHeight() != (r.bottom - r.top) )
		{
			if(d_dib)
			{
				delete d_dib;
				d_dib = 0;
			}

			d_dib = new DrawDIBDC(d_cx, d_cy);
			ASSERT(d_dib);

			drawDib();
		}
	}

//	DIB_Utility::allocateDib(&s_dib, d_cx, d_cy);
//	ASSERT(s_dib);

//	drawDib();

	if(d_dib)
	{
		BitBlt(hdc, ps.rcPaint.left, ps.rcPaint.top,
				ps.rcPaint.right - ps.rcPaint.left, ps.rcPaint.bottom - ps.rcPaint.top,
				d_dib->getDC(), ps.rcPaint.left, ps.rcPaint.top, SRCCOPY);
	}

	// clean up
	SelectPalette(hdc, oldPal, FALSE);

	EndPaint(hwnd, &ps);

	enableControls();
}

void GSecWind::drawBorder()
{
	ASSERT(d_dib);
	ASSERT(d_cx > 0);
	ASSERT(d_cy > 0);
	ASSERT(d_cx <= d_dib->getWidth());
	ASSERT(d_cy <= d_dib->getHeight());
	/*
	* Draw border around entire dib
	*/

	const int remapPer = 60;

	// leftside
	d_dib->darkenRectangle(0, 0, 1, d_cy, remapPer);
	// top
	d_dib->darkenRectangle(0, 0, d_cx, 1, remapPer);
	// right side
	d_dib->lightenRectangle(d_cx - 1, 0, 1, d_cy, remapPer);
	// bottom
	d_dib->lightenRectangle(0, d_cy - 1, d_cx, 1, remapPer);
}

void GSecWind::redraw()
{
    if(d_dib)
    	drawDib();
	InvalidateRect(getHWND(), NULL, FALSE);
	UpdateWindow(getHWND());
}

void GSecWind::update()
{
    if(d_dib)
	    drawDib();
	InvalidateRect(getHWND(), NULL, FALSE);
	UpdateWindow(getHWND());
}

