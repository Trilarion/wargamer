/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1995, Steven Morle-Green, All Rights Reserved
 *----------------------------------------------------------------------
 *
 *	Base Class for handling Windows call back functions
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "wind.hpp"
#include "wmisc.hpp"
#include "app.hpp"
#include "myassert.hpp"
#include "registry.hpp"

#if defined(DEBUG) && !defined(NOLOG)

#define COUNT_WINDOWS

#ifdef COUNT_WINDOWS
#include "clog.hpp"
#include <set>
// #include <multiset>
#endif


// #define DEBUG_MESSAGES
#ifdef DEBUG_MESSAGES
#include "msgenum.hpp"
#endif
#endif

#ifdef COUNT_WINDOWS

#ifdef __WATCOM_CPLUSPLUS__
#pragma initialize after library
#endif

class WindowAudit
{
   public:
   WindowAudit();
   ~WindowAudit();
   void logCreate(const CommonWindowBase* wind, HWND hwnd);
   void logDestroy(const CommonWindowBase* wind, HWND hwnd);
   void logNew(const CommonWindowBase* wind);
   void logDelete(const CommonWindowBase* wind);
   void logSubclass(HWND hwnd);
   void logEndSubclass(HWND hwnd);

   void showActive();

   void printf(const char* fmt, ...)
   {
      va_list vaList;
      va_start(vaList, fmt);
      vprintf(fmt, vaList);
      va_end(vaList);
   }

   void vprintf(const char* fmt, va_list args)
   {
      d_log.vprintf(fmt, args);
   }

   private:
   void logTitle(HWND hwnd);
   void logWind(const CommonWindowBase* wind);

   typedef std::set<HWND, std::less<HWND> > HWNDList;
   typedef std::multiset<HWND, std::less<HWND> > HWNDMultiList;
   typedef std::set<const CommonWindowBase*, std::less<const CommonWindowBase*> > WindList;
//   typedef set<HWND> HWNDList;
//   typedef multiset<HWND> HWNDMultiList;
//   typedef set<const CommonWindowBase*> WindList;

   LogFileFlush d_log;
   int d_totalCount;
   HWNDList d_activeList;

   WindList d_windList;
   int d_totalNew;

   HWNDMultiList d_subList;
   int d_totalSub;
};

WindowAudit s_audit;

WindowAudit::WindowAudit() :
    d_log("HWND.log"),
    d_totalCount(0),
    d_activeList(),
    d_windList(),
    d_totalNew(0),
    d_subList(),
    d_totalSub(0)
{
}

WindowAudit::~WindowAudit()
{
   showActive();
}


void WindowAudit::logTitle(HWND hwnd)
{
   const int TitleLength = 80;
   char windowTitle[TitleLength];

   sprintf(windowTitle, "%p", hwnd);
   d_log << windowTitle;

   // d_log << (int) hwnd;
   d_log << " (";
   if(GetClassName(hwnd, windowTitle, TitleLength))
      d_log << windowTitle;
   if(GetWindowText(hwnd, windowTitle, TitleLength))
      d_log << ":" << windowTitle;
   d_log << ")";

}

void WindowAudit::logCreate(const CommonWindowBase* wind, HWND hwnd)
{
   // HWND hwnd = wind->getHWND();

   std::pair<HWNDList::iterator, bool> iResult = d_activeList.insert(hwnd);
   ASSERT(iResult.second);

   ++d_totalCount;

   d_log << "Created ";
   logWind(wind);
   if(wind->getHWND() != hwnd)
   {
      d_log << " ";
      logTitle(hwnd);
   }
   d_log << ", total=" << d_totalCount;
   d_log << ", current=" << d_activeList.size() << endl;
}

void WindowAudit::logDestroy(const CommonWindowBase* wind, HWND hwnd)
{
   // HWND hwnd = wind->getHWND();

   d_log << "Destroy ";
   logWind(wind);
   if(wind->getHWND() != hwnd)
   {
      d_log << " ";
      logTitle(hwnd);
   }

   HWNDList::size_type nItems = d_activeList.erase(hwnd);
   ASSERT(nItems == 1);

   d_log << ", total=" << d_totalCount;
   d_log << ", current=" << d_activeList.size() << endl;
}

void WindowAudit::logWind(const CommonWindowBase* wind)
{
   char buf[20];
   sprintf(buf, "%p", wind);
   d_log << buf;

   if(wind->getHWND())
   {
      d_log << " [";
      logTitle(wind->getHWND());
      d_log << "]";
   }
}

void WindowAudit::logNew(const CommonWindowBase* wind)
{
   std::pair<WindList::iterator, bool> iResult = d_windList.insert(wind);
   ASSERT(iResult.second);

   ++d_totalNew;

   d_log << "New: ";
   logWind(wind);
   d_log << ", totalNew=" << d_totalNew;
   d_log << ", activeNew=" << d_windList.size() << endl;
}

void WindowAudit::logDelete(const CommonWindowBase* wind)
{
   WindList::size_type nItems = d_windList.erase(wind);
   ASSERT(nItems == 1);

   d_log << "Delete: ";
   logWind(wind);
   d_log << ", totalNew=" << d_totalNew;
   d_log << ", activeNew=" << d_windList.size() << endl;
}

void WindowAudit::logSubclass(HWND hwnd)
{
   ++d_totalSub;

   HWNDMultiList::iterator it = d_subList.insert(hwnd);

   d_log << "Subclass: ";
   logTitle(hwnd);
   d_log << ", total=" << d_totalSub;
   d_log << ", active=" << d_subList.size();
   d_log << endl;
}

void WindowAudit::logEndSubclass(HWND hwnd)
{
   // HWNDMultiList::size_type nItems = d_subList.erase(hwnd);
   // ASSERT(nItems == 1);
   HWNDMultiList::iterator it = d_subList.find(hwnd);
   ASSERT(it != d_subList.end());
   d_subList.erase(it);

   d_log << "EndSubclass: ";
   logTitle(hwnd);
   d_log << ", total=" << d_totalSub;
   d_log << ", active=" << d_subList.size();
   d_log << endl;
}

void WindowAudit::showActive()
{
   d_log << endl;
   d_log << "----------------------------------------" << endl;
   d_log << "Unclosed windows: " << d_activeList.size() << endl;
   d_log << endl;

   {
      for(HWNDList::const_iterator it = d_activeList.begin();
         it != d_activeList.end();
         ++it)
      {
         HWND hwnd = *it;
         logTitle(hwnd);
         d_log << endl;
      }
   }

   d_log << endl;
   d_log << "----------------------------------------" << endl;
   d_log << "Undeleted windows: " << d_windList.size() << endl;
   d_log << endl;

   {
      for(WindList::const_iterator it = d_windList.begin();
         it != d_windList.end();
         ++it)
      {
         const CommonWindowBase* wind = *it;
         logWind(wind);
         d_log << endl;
      }
   }

   d_log << endl;
   d_log << "----------------------------------------" << endl;
   d_log << "Subclassed windows: " << d_subList.size() << endl;
   d_log << endl;

   {
      for(HWNDMultiList::const_iterator it = d_subList.begin();
         it != d_subList.end();
         ++it)
      {
         HWND hwnd = *it;
         logTitle(hwnd);
         d_log << endl;
      }
   }
}

#endif


/*
 * Some useful values
 */


/*
 * Base Constructor
 * Defined inline
 */

// WindowBase::WindowBase()
// {
// }

HWNDbase::~HWNDbase()
{
   /*
    * If hWnd is Not NULL, then it means the window has not been destroyed
    */

   ASSERT(hWnd == NULL);
}


/*
 * Window destructors can call this if they want.
 * This will allow them to be killed by deleting them.
 */

void
HWNDbase::selfDestruct()
{
   if(hWnd != NULL)
   {
      notifyParent();
      HWND oldHwnd = hWnd;
      // hWnd = NULL;
      DestroyWindow(oldHwnd);
      hWnd = NULL;
   }
}

#if 0
/*
 * Window CallBack function for any window derived from WindowBase
 */

LRESULT CALLBACK
WindowBase::baseWindProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
#ifdef DEBUG_MESSAGES
   debugLog("WindowBase::baseWindProc(%s)\n",
      getWMdescription(hWnd, msg, wParam, lParam));
#endif

   WindowBase* wBase = reinterpret_cast<WindowBase*>(GetWindowLong(hWnd, 0));

   /*
    * Try and retrieve the class's address from the WC_NCCREATE
    * message.
    */

   if(wBase == 0)
   {
      if(msg == WM_NCCREATE)
      {
         LPCREATESTRUCT lpcs = (LPCREATESTRUCT) lParam;
         wBase = reinterpret_cast<WindowBase*>(lpcs->lpCreateParams);

         if(wBase == NULL)
            return FALSE;

         wBase->hWnd = hWnd;
         SetWindowLong(hWnd, 0, (LONG) wBase);

#ifdef COUNT_WINDOWS
         s_audit.logCreate(hWnd);
#endif

         return TRUE;
      }
      else
         return DefWindowProc(hWnd, msg, wParam, lParam);
   }

   if(msg == WM_NCDESTROY)
   {
#ifdef COUNT_WINDOWS
      s_audit.logDestroy(hWnd);
#endif

      /*
       * By setting hWnd to NULL, a derived destructor can
       * do:  if(getHWND() != NULL) DestroyWindow(getHWND());
       */

      if(wBase->hWnd != NULL)
      {
         wBase->notifyParent();
         wBase->hWnd = NULL;
         delete wBase;
      }

      wBase = NULL;
      SetWindowLong(hWnd, 0, (LONG) wBase);
      return 0;
   }

#if 0
      /*
    * Handle problem of activating window and subsequent button press
    *
    * Ignore button press that immediately follows WM_MOUSEACTIVATE
    */

      else if(msg == WM_MOUSEACTIVATE)
   {
      debugLog("WM_MOUSEACTIVATE: %08x\n", (ULONG) hWnd);

      HWND hwndTop = (HWND) wParam;

      if(hwndTop == hWnd)
         return MA_ACTIVATEANDEAT;
   }
#endif

   /*
    * If class already set up, then process messages
    */

   return wBase->procMessage(hWnd, msg, wParam, lParam);
}
#endif

/*
 * Default message processor for WindowBase
 */

LRESULT CommonWindowBase::defProc(HWND h, UINT msg, WPARAM w, LPARAM l)
{
#ifdef DEBUG_MESSAGES
   debugLog("CommonWindowBase::defProc(%s)\n",
      getWMdescription(h, msg, w, l));
#endif

   switch(msg)
   {
   case WM_USER_WINDOWDESTROYED:
      return false;
   default:
      return DefWindowProc(h, msg, w, l);
   }
}

LRESULT CommonWindowBase::procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
   return defProc(hWnd, msg, wParam, lParam);
}

bool HWNDbase::notifyParent()
{
   if(hWnd != NULL)
   {
      HWND parent = GetParent(hWnd);
      if(parent != NULL)
      {
         FORWARD_WM_PARENTNOTIFY(parent, WM_DESTROY, hWnd, 0, SendMessage);

         int id = GetWindowLong(hWnd, GWL_ID);
         return send_WM_USER_WINDOWDESTROYED(parent, hWnd, id);
      }
   }
   return false;
}


/*--------------------------------------------------------------------
 * No Auto-Delete version of WindowBase
 */

WindowBaseND::WindowBaseND()
{
#ifdef COUNT_WINDOWS
   s_audit.logNew(this);
#endif

}

WindowBaseND::~WindowBaseND()
{
#ifdef COUNT_WINDOWS
   s_audit.logDelete(this);
#endif

   selfDestruct();
   // if(isValid())
   // {
   // 	destroy();
   // }
}

/*
 * Window CallBack function for any window derived from WindowBase
 */

LRESULT CALLBACK WindowBaseND::baseWindProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
#ifdef DEBUG_MESSAGES
   debugLog("WindowBaseND::baseWindProc(%s)\n",
      getWMdescription(hWnd, msg, wParam, lParam));
#endif

   WindowBaseND* wBase = reinterpret_cast<WindowBaseND*>(GetWindowLong(hWnd, 0));

   /*
    * Try and retrieve the class's address from the WC_NCCREATE
    * message.
    */

   if(wBase == 0)
   {
      if(msg == WM_NCCREATE)
      {
         LPCREATESTRUCT lpcs = (LPCREATESTRUCT) lParam;
         wBase = reinterpret_cast<WindowBaseND*>(lpcs->lpCreateParams);

         if(wBase == 0)
            return FALSE;

         wBase->hWnd = hWnd;
         SetWindowLong(hWnd, 0, reinterpret_cast<LONG>(wBase));
         // wBase->d_valid = true;

#ifdef COUNT_WINDOWS
         s_audit.logCreate(wBase, hWnd);
#endif

         return TRUE;
      }
      else
         return DefWindowProc(hWnd, msg, wParam, lParam);
   }

   if(msg == WM_NCDESTROY)
   {
#ifdef COUNT_WINDOWS
      s_audit.logDestroy(wBase, hWnd);
#endif

      /*
       * By setting hWnd to NULL, a derived destructor can
       * do:  if(getHWND() != NULL) DestroyWindow(getHWND());
       */

      // if(wBase->d_valid)	// wBase->hWnd != NULL)
      {
         bool killMe = wBase->notifyParent();
         wBase->hWnd = NULL;
         if(killMe)
            delete wBase;
      }

      SetWindowLong(hWnd, 0, 0);
      return 0;
   }

   /*
    * If class already set up, then process messages
    */

   return wBase->procMessage(hWnd, msg, wParam, lParam);
}

/*----------------------------------------------------------------------
 * Child Window Class
 */

int ChildWindow::onMouseActivate(HWND hwnd, HWND hwndTopLevel, UINT codeHitTest, UINT msg)
{

   SetWindowPos(hwnd, HWND_TOP, 0,0,0,0, SWP_NOMOVE | SWP_NOSIZE);

   return MA_ACTIVATE;
}

/*
 * Help Function to set a window's size
 */

void
alterWindowSize(HWND hWnd, LONG lWidth, LONG lHeight, CREATESTRUCT* lpCreateStruct)
{
   RECT r;
   GetClientRect(hWnd, &r);

   r.right = r.left + lWidth;
   r.bottom = r.top + lHeight;

   AdjustWindowRectEx(&r, lpCreateStruct->style, FALSE, lpCreateStruct->dwExStyle);

   int w = r.right - r.left;
   int h = r.bottom - r.top;

   SetWindowPos(hWnd, HWND_TOP, 0, 0, w, h, SWP_NOMOVE | SWP_NOZORDER);
}


/*
 * Popup Window base Class
 */


#if 0   // Identical to GenericWithBackground!
const char* PopupWindow::className() const
{
   static const char PopupWindow::s_className[] = "PopupWindow";
   static ATOM PopupWindow::classAtom = 0;

   if(!classAtom)
   {
      WNDCLASS wc;

      wc.style = CS_OWNDC | CS_DBLCLKS;
      wc.lpfnWndProc = baseWindProc;
      wc.cbClsExtra = 0;
      wc.cbWndExtra = sizeof(this);
      wc.hInstance = APP::instance();
      wc.hIcon = NULL;
      wc.hCursor = LoadCursor(NULL, IDC_ARROW);
      wc.hbrBackground = (HBRUSH) (1 + COLOR_3DFACE);
      wc.lpszMenuName = NULL;
      wc.lpszClassName = s_className;
      classAtom = RegisterClass(&wc);
   }

   ASSERT(classAtom != 0);

   return className;
}
#endif


/*
 * Subclass Base Window
 */



/*
 * Constructor before window is created
 */

SubClassWindowBase::SubClassWindowBase()
{
   d_defProc = 0;
}

/*
 * Subclass an existing window
 */

void
SubClassWindowBase::init(HWND hwnd)
{
#ifdef COUNT_WINDOWS
   s_audit.logSubclass(hwnd);
#endif

   ASSERT(hwnd != NULL);
   d_defProc = (WNDPROC) SetWindowLong(hwnd, GWL_WNDPROC, (LONG) subClassProc);
   ASSERT(d_defProc != 0);
   // ASSERT(d_defProc != reinterpret_cast<WNDPROC>(subClassProc));	// would be recursive!

   d_defData = SetWindowLong(hwnd, GWL_USERDATA, reinterpret_cast<LONG>(this));
   ASSERT(d_defData != reinterpret_cast<LONG>(this));
}

void
SubClassWindowBase::clear(HWND hwnd)
{
#ifdef COUNT_WINDOWS
   s_audit.logEndSubclass(hwnd);
#endif

   ASSERT(d_defProc != 0);
   WNDPROC oldProc = (WNDPROC) SetWindowLong(hwnd, GWL_WNDPROC, (LONG) d_defProc);
   ASSERT(oldProc == reinterpret_cast<WNDPROC>(subClassProc));
   LONG oldData = SetWindowLong(hwnd, GWL_USERDATA, d_defData);
   ASSERT(oldData == reinterpret_cast<LONG>(this));
}

/*
 * Default is to call subclassed function
 *
 * Change Data back to old data in case it has been subclassed twice
 * The problem with this is that if the message causes more messages
 * to be processed then our virtual functions won't get called.
 */

LRESULT SubClassWindowBase::defProc(HWND h, UINT m, WPARAM w, LPARAM l)
{
#ifdef DEBUG_MESSAGES
   debugLog("SubClassWindowBase::defProc(%s)\n",
      getWMdescription(h, m, w, l));
#endif

   ASSERT(d_defProc != 0);

   ASSERT( (d_defProc != subClassProc) || (d_defData != reinterpret_cast<LONG>(this)));
   ASSERT( (d_defProc != subClassProc) || (d_defData != 0));

   if(d_defProc == subClassProc)
   {
      SubClassWindowBase* wBase = reinterpret_cast<SubClassWindowBase*>(d_defData);
      ASSERT(wBase != 0);
      if(wBase != 0)
         return wBase->procMessage(h, m, w, l);
      else
         return CallWindowProc(d_defProc, h, m, w, l);
   }
   else
   {
      // SetWindowLong(h, GWL_USERDATA, d_defData);
      return CallWindowProc(d_defProc, h, m, w, l);
      // SetWindowLong(h, GWL_USERDATA, reinterpret_cast<LONG>(this));
   }
}

LRESULT CALLBACK SubClassWindowBase::subClassProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
#ifdef DEBUG_MESSAGES
   debugLog("SubClassWindowBase::subClassProc(%s)\n",
      getWMdescription(hwnd, msg, wParam, lParam));
#endif

   SubClassWindowBase* base = reinterpret_cast<SubClassWindowBase*>(GetWindowLong(hwnd, GWL_USERDATA));
   ASSERT(base != 0);
   if(base != 0)
      return base->procMessage(hwnd, msg, wParam, lParam);
   else
      return DefWindowProc(hwnd, msg, wParam, lParam);
}

LRESULT SubClassWindowBase::procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
   return defProc(hWnd, msg, wParam, lParam);
}


const char HWNDbase::s_regEnabled[] = "Enabled";
const char HWNDbase::s_regPosition[] = "Position";
const char HWNDbase::s_regSize[] = "Size";
const char HWNDbase::s_regShow[] = "Show";

void HWNDbase::saveState(const char* regKey, WSAV_FLAGS flags) const
{
   if(flags & WSAV_ENABLED)
      setRegistry(s_regEnabled, &d_enabled, sizeof(d_enabled), regKey);

   if (flags & (WSAV_POSITION | WSAV_SIZE | WSAV_RESTORE))
   {
      WINDOWPLACEMENT wp;
      memset(&wp, 0, sizeof(wp));
      wp.length = sizeof(WINDOWPLACEMENT);
      GetWindowPlacement(getHWND(), &wp);

      POINT pos;
      pos.x = wp.rcNormalPosition.left;
      pos.y = wp.rcNormalPosition.top;

      POINT size;
      size.x = wp.rcNormalPosition.right - wp.rcNormalPosition.left;
      size.y = wp.rcNormalPosition.bottom - wp.rcNormalPosition.top;

      if (flags & WSAV_POSITION)
      {
         setRegistry(s_regPosition, &pos, sizeof(pos), regKey);
      }
      if (flags & WSAV_SIZE)
      {
         setRegistry(s_regSize, &size, sizeof(size), regKey);
      }
      if (flags & WSAV_RESTORE)
      {
         setRegistry(s_regShow, &wp.showCmd, sizeof(wp.showCmd), regKey);
      }
   }
}

bool HWNDbase::getState(const char* regKey, WSAV_FLAGS flags, bool defShowValue)
{
   bool success = true;

   if (flags & WSAV_ENABLED)
   {
      bool value;
      if(!getRegistry(s_regEnabled, &value, sizeof(value), regKey))
         value = defShowValue;
      enable(value);
   }

   if (flags & (WSAV_POSITION | WSAV_SIZE | WSAV_RESTORE | WSAV_MINIMIZE))
   {
      WINDOWPLACEMENT wp;
      memset(&wp, 0, sizeof(wp));
      wp.length = sizeof(WINDOWPLACEMENT);
      GetWindowPlacement(getHWND(), &wp);

      POINT pos;
      pos.x = wp.rcNormalPosition.left;
      pos.y = wp.rcNormalPosition.top;

      POINT size;
      size.x = wp.rcNormalPosition.right - wp.rcNormalPosition.left;
      size.y = wp.rcNormalPosition.bottom - wp.rcNormalPosition.top;

      if (flags & WSAV_POSITION)
      {
         if(!getRegistry(s_regPosition, &pos, sizeof(pos), regKey))
            return false;
      }
      if (flags & WSAV_SIZE)
      {
         if(!getRegistry(s_regSize, &size, sizeof(size), regKey))
            return false;
      }
      if(flags & WSAV_RESTORE)
      {
         if(!getRegistry(s_regShow, &wp.showCmd, sizeof(wp.showCmd), regKey))
            return false;
      }

      if (!(flags & WSAV_MINIMIZE))
      {
         if ( (wp.showCmd == SW_MINIMIZE) ||
             (wp.showCmd == SW_SHOWMINIMIZED) ||
             (wp.showCmd == SW_SHOWMINNOACTIVE) )
         {
               wp.showCmd = SW_RESTORE;
         }
      }

      wp.rcNormalPosition.left = pos.x;
      wp.rcNormalPosition.top = pos.y;
      wp.rcNormalPosition.right = pos.x + size.x;
      wp.rcNormalPosition.bottom = pos.y + size.y;


      SetWindowPlacement(hWnd, &wp);
   }

   // Check that it is on screen...

   return true;
}

//lint -save -e578 ... Disable 'enable' hides HWNDbase::enable
void HWNDbase::enable(bool enable)
{
   d_enabled = enable;
   if(hWnd)
      ShowWindow(hWnd, (d_shown && d_enabled) ? SW_SHOW : SW_HIDE);
}
//lint -restore

void HWNDbase::show(bool visible)
{
   d_shown = visible;
   if(hWnd)
      ShowWindow(hWnd, (d_shown && d_enabled) ? SW_SHOW : SW_HIDE);
}

bool HWNDbase::isVisible() const
{
   return (hWnd != NULL) && ::IsWindowVisible(hWnd);
}


HWND CommonWindowBase::createWindow(
	DWORD  dwExStyle,
	// LPCTSTR  lpClassName,
	LPCTSTR  lpWindowName,
	DWORD  dwStyle,
	int  x,
	int  y,
	int  nWidth,
	int  nHeight,
	HWND  hWndParent,
	HMENU  hMenu)
	// HINSTANCE  hInstance)
{
   hWnd = ::CreateWindowEx(
      dwExStyle,
      className(),
      lpWindowName,
      dwStyle & ~WS_VISIBLE,
      x,
      y,
      nWidth,
      nHeight,
      hWndParent,
      hMenu,
      wndInstance(hWndParent),        // APP::instance(),
      this);

   ASSERT(hWnd != NULL);

   if(hWnd == NULL)
      throw ErrorCreateWindow("CommonWindowBase:: Error creating window");

   CommonWindowBase::show( (dwStyle & WS_VISIBLE) != 0);

   return hWnd;
}


/*
 * Default Class to use
 */

const char* WindowBaseND::className() const
{
   static ATOM classAtom = NULL;
   static const char s_className[] = "WindowBaseND";

   if(!classAtom)
   {
      WNDCLASS wc;

      wc.style = CS_OWNDC | CS_NOCLOSE;
      wc.lpfnWndProc = baseWindProc;
      wc.cbClsExtra = 0;
      wc.cbWndExtra = sizeof(this);
      wc.hInstance = APP::instance();
      wc.hIcon = NULL;
      wc.hCursor = LoadCursor(NULL, IDC_ARROW);
      wc.hbrBackground = NULL;	// (HBRUSH) (1 + COLOR_3DFACE);
      wc.lpszMenuName = NULL;
      wc.lpszClassName = s_className;
      classAtom = RegisterClass(&wc);
   }

   ASSERT(classAtom != 0);

   return s_className;
}


const char* WindowWithBackGround::className() const
{
   static ATOM classAtom = NULL;
   static const char s_className[] = "WindowWithBackGround";

   if(!classAtom)
   {
      WNDCLASS wc;

      wc.style = CS_OWNDC | CS_NOCLOSE;
      wc.lpfnWndProc = baseWindProc;
      wc.cbClsExtra = 0;
      wc.cbWndExtra = sizeof(this);
      wc.hInstance = APP::instance();
      wc.hIcon = NULL;
      wc.hCursor = LoadCursor(NULL, IDC_ARROW);
      wc.hbrBackground = (HBRUSH) (1 + COLOR_3DFACE);
      wc.lpszMenuName = NULL;
      wc.lpszClassName = s_className;
      classAtom = RegisterClass(&wc);
   }

   ASSERT(classAtom != 0);

   return s_className;
}


