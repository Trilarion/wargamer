/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#if !defined(FunctionTimer_HPP)
#define FunctionTimer_HPP


/*
 * High Resolution Timer Functions
 */

class FunctionTimer
{
      friend class HiresTimer;
    public:
        FunctionTimer(const char* id);
        ~FunctionTimer();

    private:
         void start(LARGE_INTEGER t);
         void stop(LARGE_INTEGER t);


        const char* d_id;
        LARGE_INTEGER d_startTime;
        FunctionTimer* d_lastTimer;
};


#endif  // FunctionTimer_HPP
