/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef TIMER_H
#define TIMER_H

#ifndef __cplusplus
#error timer.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1995, Steven Morle-Green, All Rights Reserved
 *----------------------------------------------------------------------
 *
 *	Timer used to kee game running
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.4  1996/06/22 19:06:39  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.3  1996/02/28 09:33:33  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.2  1996/02/15 10:51:21  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.1  1995/11/07 10:39:54  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#include "sysdll.h"
#include <windef.h>

#undef USE_MM_TIMER			// Don't use Multimedia Timer!

class SYSTEM_DLL WMTimer {
	UINT d_id;
	UINT d_tickRate;		// Milliseconds per tick
	HWND d_hWnd;			// Owner window
public:
	WMTimer();
	virtual ~WMTimer();

	void initWMTimer(HWND hwnd, int tickRate=100);
	// virtual void procTimer() { }

	void stopWMTimer();

	UINT getWMTickRate() const { return d_tickRate; }		// Return milliseconds per tick
   bool isRunning() const { return d_id != 0; }

	// friend VOID CALLBACK timerProc(HWND  hwnd,	UINT  uMsg,	UINT  idEvent,	DWORD  dwTime);
};

#if defined(USE_MM_TIMER)

class SYSTEM_DLL MMTimer {
	MMRESULT id;
	UINT tickRate;		// Milliseconds per tick
public:
	MMTimer();
	virtual ~MMTimer();

	void initMMTimer();
	virtual void mmProcTimer() = 0;

	void stopMMTimer();

	UINT getMMTickRate() const { return tickRate; }		// Return milliseconds per tick

	friend VOID CALLBACK mmTimerProc(HWND  hwnd,	UINT  uMsg,	UINT  idEvent,	DWORD  dwTime);
};

#endif	// USE_MM_TIMER

#endif /* TIMER_H */

