/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef FILECNK_HPP
#define FILECNK_HPP

#ifndef __cplusplus
#error filecnk.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1995, Steven Morle-Green, All Rights Reserved
 *----------------------------------------------------------------------
 *
 *	File Chunk
 * This is a method of combining lots of data into a single file
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/06/13 08:52:39  greenius
 * Initial Import
 *
 *
 *----------------------------------------------------------------------
 */

//	#include "filebase.hpp"		// If not including Windows File handler
#include "sysdll.h"
#include "winfile.hpp"

struct MessagePair;
class StringParse;

/*
 * Base Chunk File class
 */

class SYSTEM_DLL ChunkWriter {
};

class SYSTEM_DLL ChunkReader {
};

class SYSTEM_DLL ChunkFileError {
public:
	ChunkFileError();
};

/*
 * Class for a Windows Chunk File
 */

class SYSTEM_DLL WinFileBinaryChunkWriter :
	public WinFileBinaryWriter,
	public ChunkWriter
{
public:
	WinFileBinaryChunkWriter(LPCSTR name);
	~WinFileBinaryChunkWriter();
};

class SYSTEM_DLL WinFileAsciiChunkWriter :
	public WinFileAsciiWriter,
	public ChunkWriter
{
public:
	WinFileAsciiChunkWriter(LPCSTR name);
	~WinFileAsciiChunkWriter();
};

class SYSTEM_DLL WinFileBinaryChunkReader :
	public WinFileBinaryReader,
	public ChunkReader
{
	ULONG headerLength;
public:
	WinFileBinaryChunkReader(LPCSTR name);
	~WinFileBinaryChunkReader();

	SeekPos skipHeader();
};


class SYSTEM_DLL WinFileAsciiChunkReader :
	public WinFileAsciiReader,
	public ChunkReader
{
public:
	WinFileAsciiChunkReader(LPCSTR name);
	~WinFileAsciiChunkReader();

private:
	char* readLine();
};

/*
 * Class for adding chunk information to files
 */

class SYSTEM_DLL FileChunkWriter {
	SeekPos pos;
	FileWriter& f;
public:
	FileChunkWriter(FileWriter& file, const char* name);
	~FileChunkWriter();
};



class SYSTEM_DLL FileChunkReader {
	Boolean ok;
	FileReader& f;
public:
	FileChunkReader(FileReader& file, const char* name);

	Boolean isOK() { return ok; }

	int countEntries();

	// These are used for reading tables
	int countColumns();
	int countRows();

	Boolean startItem();
	Boolean getItem(int& token, StringParse& parameters, const MessagePair* mp);

	// used for reading table data
	Boolean getItem(StringParse& parameters);
};

extern SYSTEM_DLL const char endChunkName[];

template<class T>
class FileWithMode : public T
{
		// SaveGame::FileFormat d_mode;
		int d_mode;
	public:
		// FileWithMode(const char* fileName, SaveGame::FileFormat mode) :
		FileWithMode(const char* fileName, int mode) :
			T(fileName),
			d_mode(mode)
		{
		}

		virtual ~FileWithMode() { }

		virtual int getMode() const { return d_mode; }
};



#endif /* FILECNK_HPP */

