/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef REFPTRIM_HPP
#define REFPTRIM_HPP

#ifndef __cplusplus
#error refptrim.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *	Reference Counting Pointers Implementation
 *
 * Making this seperate means that refptr.hpp can be included
 * without the definitions of the types used in the template,
 *
 * e.g. the following is allowed
 * 	class CommandPosition;
 *		typedef RefPtr<CommandPosition> ptrCP;
 * 
 * To make use of ptrCP, then you include this file.
 *
 *----------------------------------------------------------------------
 */

#include "sysdll.h"

#endif /* CRefPtrIM_HPP */

