/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *	Draw Filled Polygon into a DIB
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "dib_poly.hpp"
#ifdef __WATCOM_CPLUSPLUS__
#include <stdlib.h>
#else
#include <math.h>
#endif

namespace DIB_Utility
{

/*
 * 2D Clipping
 */

class PolyClip;
class PolyClipEdge {
	PixelPoint first;
	PixelPoint last;
	Boolean needFirst;
public:
				PolyClipEdge() : first(), last() { needFirst = True; };

	friend class PolyClip;
};

class PolyClip { 
	Polygon2D* destPoly;
	const Region* bitmap;
	enum { None, Left, Right, Top, Bottom } closing;
	PolyClipEdge top;
	PolyClipEdge bottom;
	PolyClipEdge left;
	PolyClipEdge right;

	void clipLeft(PixelPoint p);
	void clipRight(PixelPoint p);
	void clipTop(PixelPoint p);
	void clipBottom(PixelPoint p);
	
public:
				PolyClip(Polygon2D* destPoly, const Region* bitmap)
				{
					closing = None;
					PolyClip::destPoly = destPoly;
					PolyClip::bitmap = bitmap;
				};

				~PolyClip();

		void	clipPoint(PixelPoint p);



};


static const PixelDimension MaxPixelDimension = LONG_MAX;

Polygon2D::Polygon2D(PolyPointIndex nPoints, const PixelPoint* points)
{
	maxPoints = nPoints;

	if(points)
	{
		Polygon2D::points = const_cast<PixelPoint*>(points);
		Polygon2D::nPoints = nPoints;
		pointsAlloced = False;
	}
	else
	{
		Polygon2D::points = new PixelPoint[nPoints];

#if 0
		if(!Polygon2D::points)
			throw NoMemory();
#endif

		Polygon2D::nPoints = 0;
		pointsAlloced = True;
	}
}


Polygon2D::~Polygon2D()
{
	if(pointsAlloced)
		delete[] points;
}





/*
 * 2D Clipping
 */

void PolyClip::clipLeft(PixelPoint p)
{
	if(left.needFirst)
	{
		left.needFirst = False;
		left.first = p;
		left.last = p;
	}

	// Moving from outside In?

	else if( ((p.x() >= bitmap->left()) && (left.last.x() < bitmap->left()) ) ||
			   ((p.x() < bitmap->left()) && (left.last.x() >= bitmap->left()) ) )
	{
		clipRight(PixelPoint(bitmap->left(), p.y() + ((p.y() - left.last.y()) * (bitmap->left()- p.x())) / (p.x() - left.last.x())));
	}

	left.last = p;

	if( (p.x() >= bitmap->left()) && (closing != Left) )
		clipRight(p);

}

void PolyClip::clipRight(PixelPoint p)
{
	PixelDimension limit = PixelDimension(bitmap->right());

	if(right.needFirst)
	{
		right.needFirst = False;
		right.first = p;
		right.last = p;
	}

	// Moving from outside In?

	else if( ((p.x() <  limit) && (right.last.x() >= limit) ) ||
			   ((p.x() >= limit) && (right.last.x() <  limit) ) )
	{
 		clipBottom(PixelPoint(limit, p.y() + ((p.y() - right.last.y()) * (limit - p.x())) / (p.x() - right.last.x())));
	}

	right.last = p;

	if( (p.x() < limit) && (closing != Right) )
		clipBottom(p);

}

void PolyClip::clipBottom(PixelPoint p)
{
	if(bottom.needFirst)
	{
		bottom.needFirst = False;
		bottom.first = p;
		bottom.last = p;
	}

	// Moving from outside In?

	else if( ((p.y() >= bitmap->top()) && (bottom.last.y() < bitmap->top()) ) ||
			   ((p.y() < bitmap->top()) && (bottom.last.y() >= bitmap->top()) ) )
	{
		clipTop(PixelPoint( p.x() + ( (p.x() - bottom.last.x()) * (bitmap->top() - p.y())) / (p.y() - bottom.last.y()), bitmap->top()));
	}

	bottom.last = p;

	if( (p.y() >= bitmap->top()) && (closing != Bottom) )
		clipTop(p);

}

void PolyClip::clipTop(PixelPoint p)
{
	PixelDimension limit = PixelDimension(bitmap->bottom());
	
	if(top.needFirst)
	{
		top.needFirst = False;
		top.first = p;
		top.last = p;
	}

	// Moving from outside In?

	else if( ((p.y() <  limit) && (top.last.y() >= limit) ) ||
			   ((p.y() >= limit) && (top.last.y() <  limit) ) )
	{
		destPoly->add(PixelPoint( p.x() + ( (p.x() - top.last.x()) * (limit - p.y())) / (p.y() - top.last.y()), limit));
	}

	top.last = p;

	if( (p.y() < limit) && (closing != Top) )
		destPoly->add(p);

}

void PolyClip::clipPoint(PixelPoint p)
{
	clipLeft(p);
}

/*
 * Close the polygon
 */

PolyClip::~PolyClip()
{
	if(!left.needFirst)
	{
		closing = Left;
		clipLeft(left.first);
	}

	if(!right.needFirst)
	{
		closing = Right;
		clipRight(right.first);
	}

	if(!bottom.needFirst)
	{
		closing = Bottom;
		clipBottom(bottom.first);
	}

	if(!top.needFirst)
	{
		closing = Top;
		clipTop(top.first);
	}
}

void Polygon2D::clipPoly(Polygon2D* destPoly, const Region* bitmap) const
{
	PolyClip clip(destPoly, bitmap);

	/*
	 * Go through each point
	 */

	PolyPointIndex i = nPoints;
	PixelPoint* p = points;

	while(i--)
		clip.clipPoint(*p++);

	// Closing is done automatically in the destructor

}


/*
 * Scan line class for storing min/max values
 */

class Scan2D {
	
	struct ScanLine {
		PixelDimension min;
		PixelDimension max;
	} *lines;

	PixelDimension count;
	PixelDimension yOffset;

public:
			Scan2D(const PixelDimension count, const PixelDimension yOffset = 0);
		  ~Scan2D();
	ScanLine*	get(PixelDimension y) const { return &lines[y - yOffset]; };
	ScanLine*	get() const { return lines; };

	void			setMin(PixelDimension y, PixelDimension x) { get(y)->min = x; };
	void			setMax(PixelDimension y, PixelDimension x) { get(y)->max = x; };

	PixelDimension	getH() const { return count; };
	PixelDimension  getY() const { return yOffset; };

	void			addLine(PixelPoint p1, PixelPoint p2);
	void			addPoint(PixelPoint p);

	void 			draw(Region* image, ColourIndex color) const;
};


inline Scan2D::Scan2D(const PixelDimension count, const PixelDimension yOffset)
{
	Scan2D::count = count;
	Scan2D::yOffset = yOffset;
	lines = new ScanLine[count];

#if 0
	if(!lines)
		throw NoMemory();
#endif

	int i = count;
	ScanLine* line = lines;
	while(i--)
	{
		line->min = MaxPixelDimension;
		line->max = 0;

		line++;
	}

}

inline Scan2D::~Scan2D()
{
	delete[] lines;
}

/*
 * Update a scan line
 */

void Scan2D::addPoint(PixelPoint p)
{
	ScanLine* line = get(p.y());

	if(p.x() < line->min)
		line->min = p.x();

	if(p.x() > line->max)
		line->max = p.x();
}

/*
 * Draw a line into the scanline buffer
 */

template<class T>
T sign(T v)
{
	if(v < 0) return -1;
	else if(v > 0) return 1;
	else return 0;
}

void Scan2D::addLine(PixelPoint from, PixelPoint to)
{
	PixelPoint diff = to - from;
   PixelPoint absDiff = PixelPoint(abs(diff.x()), abs(diff.y()));
	PixelPoint dir = PixelPoint(sign(diff.x()), sign(diff.y()));

	/*
	 * Which is major axis?
	 */

	if(absDiff.x() > absDiff.y())
	{
		// X is main axis

		addPoint(from);

		if(from.x() != to.x())
		{
			int value = 0x8000;
			int add = (absDiff.y() * 0x10000) / absDiff.x();

			while(from.x() != to.x())
			{
				value += add;
				if(value >= 0x10000)
				{
					from.y( from.y() + dir.y() );
					value -= 0x10000;
				}

				from.x( from.x() + dir.x() );

				addPoint(from);
			}
		}
	}
	else
	{
		// Y is main axis

		addPoint(from);

		if(from.y() != to.y())
		{
			int value = 0x8000;
			int add = (absDiff.x() * 0x10000) / absDiff.y();

			while(from.y() != to.y())
			{
				value += add;
				if(value >= 0x10000)
				{
					from.x(from.x() + dir.x());
					value -= 0x10000;
				}

				from.y(from.y() + dir.y());

				addPoint(from);
			}
		}
	}
}



/*
 * Draw the scan lines
 */

void Scan2D::draw(Region* image, ColourIndex color) const
{
	ScanLine* line = get();
	int count = getH();
	PixelDimension y = getY();

#if 0
	if(texture)
	{
		while(count--)
		{
			if(line->min <= line->max)
				image->HLineFast(line->min, y, line->max - line->min + 1, color, texture);

			line++;
			y++;
		}
	}
	else
#endif
	{
		while(count--)
		{
			if(line->min <= line->max)
				image->dib()->hLineFast(line->min, y, line->max - line->min + 1, color);

			line++;
			y++;
		}
	}
}

/*
 * Draw a polygon onto a bitmap
 *
 * This is confusing now:
 *   Should a routine like this belong to the BitMap class or the
 *   Polygon class?  Throughout the rest of the program, I have
 *   made functions belong to the class that they are written to,
 *   but for this function we have the strangeness that it is more
 *   logical to place it in this module in the polygon implementation.
 */

void Polygon2D::render(Region* bm) const
{
#ifdef DEBUG1
	/*
	 * Draw outline for testing
	 */

	{
		Rect oldClip = *this;
		setClip();


		PolyPointIndex i = poly.nPoints;
		PixelPoint* p = poly.points;
		PixelPoint* lastPoint = &p[i-1];
		while(i--)
		{
			line(*lastPoint, *p, color+1);
			lastPoint = p;

			p++;
		}

		setClip(oldClip.getX(), oldClip.getY(), oldClip.getW(), oldClip.getH());

	}
#endif

	/*
	 * Clip it
	 */

	Polygon2D clippedPoly(nPoints * 2 + 4);		// Rest of function will use this

	clipPoly(&clippedPoly, bm);

	if(clippedPoly.nPoints < 3)
		return;

#ifdef DEBUG1
	/*
	 * Draw outline for testing
	 */

	{
		PolyPointIndex i = clippedPoly.nPoints;
		PixelPoint* p = clippedPoly.points;
		PixelPoint* lastPoint = &p[i-1];
		while(i--)
		{
			line(*lastPoint, *p, color+1);
			lastPoint = p;

			p++;
		}
	}
#endif

	/*
	 * Work out rectangle enclosing the polygon
	 */

	PixelPoint minP = PixelPoint(bm->left(), bm->bottom());		// Start with clipping rectangle extremes
	PixelPoint maxP(0,0);

	{
		PolyPointIndex i = clippedPoly.nPoints;
		PixelPoint* p = clippedPoly.points;
		while(i--)
		{
			if(p->x() < minP.x())
				minP.x( p->x() );
			if(p->x() > maxP.x())
				maxP.x( p->x() );
			if(p->y() < minP.y())
				minP.y( p->y() );
			if(p->y() > maxP.y())
				maxP.y( p->y() );

			p++;
		}
	}

	PixelPoint size = maxP - minP + PixelPoint(1,1);

	/*
	 * Make up scan line table
	 */

#ifdef DEBUG1
	// Draw a frame for testing

	frame(minP, size + PixelPoint(1,1), 64);
#endif

	Scan2D lines(size.y(), minP.y());

	{
		PolyPointIndex i = clippedPoly.nPoints;
		PixelPoint* p = clippedPoly.points;
		PixelPoint* lastPoint = &p[i-1];
		while(i--)
		{
			lines.addLine(*lastPoint, *p);
			lastPoint = p;

			p++;
		}
	}

	lines.draw(bm, colour);
}

Region::Region(DrawDIB* dib) : 
	d_dib(dib),
	d_rect(0,0,dib->getWidth(),dib->getHeight())
{
}


void drawPoly(DrawDIB* dib, const PixelPoint* points, int nPoints, ColourIndex col)
{
	ASSERT(nPoints > 2);
	ASSERT(points != 0);
	ASSERT(dib != 0);

#ifdef DEBUG1
	// test by drawing lines between points

	PixelPoint lastPoint = points[nPoints-1];
	{
		for(int i = 0; i < nPoints; ++i)
		{
			dib->line(lastPoint.x(), lastPoint.y(), points[i].x(), points[i].y(), col);
			lastPoint = points[i];
		}
	}
#endif

	// Split into Triangles

	PixelPoint tPoints[3];

	tPoints[0] = points[0];

	{
		for(int i = 1; i < (nPoints-1); ++i)
		{
			tPoints[1] = points[i];
			tPoints[2] = points[i+1];

			drawFilledTriangle(dib, tPoints, col);
		}
	}
}

void drawFilledTriangle(DrawDIB* dib, const PixelPoint* points, ColourIndex col)
{
	ASSERT(points != 0);
	ASSERT(dib != 0);

#ifdef DEBUG1
	PixelPoint lastPoint = points[2];
	for(int i = 0; i < 3; ++i)
	{
		dib->line(lastPoint.x(), lastPoint.y(), points[i].x(), points[i].y(), col);
		lastPoint = points[i];
	}
#endif

	Polygon2D poly(3, points);
	poly.setColour(col);
	DIB_Utility::Region region(dib);
	poly.render(&region);
}

};	// namespace DIB_Utility

