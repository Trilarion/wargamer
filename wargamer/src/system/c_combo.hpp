/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef C_COMBO_HPP
#define C_COMBO_HPP

#include "sysdll.h"
#include "mytypes.h"
#include "palwind.hpp"
#include "resstr.hpp"

/*
 * A Custom drawn combo
 * Utilizes ItemWindow Class as a popup combo window
 */

class CCombo_Imp;
class DrawDIB;
class DrawDIBDC;
class ImageLibrary;

struct SYSTEM_DLL CComboData {
	 HWND d_hParent;
	 UWORD d_id;
	 HFONT d_hFont;
	 UWORD d_cx;                      // width of edit box
	 UWORD d_cy;                      // height of edit box
	 UWORD d_itemCY;                  // height of each list item
	 UWORD d_maxItemsShowing;         // maximumn items to show
	 UBYTE d_flags;
	 const DrawDIB* d_windowFillDib;  // to fill in window
	 const DrawDIB* d_fillDib;
	 const ImageLibrary* d_scrollBtns;
	 const ImageLibrary* d_comboBtn;
	 const DrawDIB* d_scrollFillDib;
	 const DrawDIB* d_scrollThumbDib;
	 CustomBorderInfo d_bColors;
    COLORREF d_colorize;

	 enum {
		HasScroll             = 0x01,
		TextHeader            = 0x02,
		OwnerDraw             = 0x04
	 };

	 CComboData() :
		d_hParent(0),
		d_id(0),
		d_hFont(0),
		d_cx(0),
      d_cy(0),
		d_itemCY(10),
		d_maxItemsShowing(10),
		d_flags(0),
		d_windowFillDib(0),
		d_fillDib(0),
		d_scrollBtns(0),
		d_comboBtn(0),
		d_scrollFillDib(0),
		d_scrollThumbDib(0),
		d_colorize(PALETTERGB(128, 128, 128))
	 {
	 }
};


struct CComboInit {
  const char* d_text;
  LPARAM d_param;
};

struct CComboResInit {
  InGameText::ID d_textID;
  LPARAM d_param;
};


class SYSTEM_DLL CCombo {
	 CCombo_Imp* d_combo;
  public:
	 CCombo() :
		d_combo(0) {}

	 ~CCombo();

    enum { CB_NoIndex = -1 };
	 void init(const CComboData& cd);
	 void position(int x, int y);
	 // void destroy();

	 void reset();
	 void add(const char* text, LPARAM param);
	 void add(const CComboInit* ci);
	 void add(const CComboResInit* ci);
	 void setValue(LPARAM param);
	 void setIndex(int id);
	 LPARAM getValue();
	 int getIndex();
	 DrawDIBDC* dib();

    HWND getHWND() const;
};


#endif
