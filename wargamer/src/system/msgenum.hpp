/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef MSGENUM_H
#define MSGENUM_H

#ifndef __cplusplus
#error msgenum.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1995, Steven Morle-Green, All Rights Reserved
 *----------------------------------------------------------------------
 *
 *	Useful info for debug messages
 *
 *----------------------------------------------------------------------
 */

#include "sysdll.h"
#include <windef.h>

SYSTEM_DLL const char* getWMname(int message);		// return "WM_CLOSE"... etc
SYSTEM_DLL const char* getWMdescription(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);

SYSTEM_DLL const char* getNotifyName(int value);

#endif /* MSGENUM_H */

/*
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/06/13 08:52:39  greenius
 * Initial Import
 *
 * Revision 1.2  1996/06/22 19:06:39  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.1  1995/11/14 11:25:57  Steven_Green
 * Initial revision
 *
 *----------------------------------------------------------------------
 */
