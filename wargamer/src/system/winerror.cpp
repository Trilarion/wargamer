/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *	Windows Exception
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "winerror.hpp"
#ifdef __WATCOM_CPLUSPLUS__
#include <string.hpp>
#else
#include <string>
typedef std::string String;
#endif
#include <windows.h>


WinError::WinError(const char* s)
{
	DWORD err = GetLastError();

	char* lpMsgBuf;
 
	FormatMessage( 
		FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM,
		NULL,
		GetLastError(),
		MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), // Default language
		(LPTSTR) &lpMsgBuf,
		0,
		NULL 
	);


	String str;

	str = "Windows Error: ";
	str += s;
	str += ".  Last Error: ";
	// str += err;
	// str += " (";
	str += lpMsgBuf;
	// str += ")";

	LocalFree(lpMsgBuf);

	setMessage(str.c_str());
}


