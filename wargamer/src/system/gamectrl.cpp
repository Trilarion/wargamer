/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *	Game Control
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "gamectrl.hpp"
#include "myassert.hpp"
#include "thread.hpp"

/*
 * Static current pointer, will get filled in later when Dummy is created
 */

GameControlBase* GameControlBase::s_current = 0;

/*
 * GameControlBase Implememtation
 */


GameControlBase::GameControlBase() :
	d_paused(False),
	d_procCount(0),
	d_firstMM(True),
	d_lastMMtime(0)
{
    // GameControl::setController(this);
	// ASSERT(GameControl::s_control == 0);
	// GameControl::s_control = this;

    d_prev = s_current;
    s_current = this;
}


GameControlBase::~GameControlBase() 
{ 
    // GameControl::removeController(this);
	// ASSERT(GameControl::s_control == this);
	// GameControl::s_control = 0;

    ASSERT(s_current == this);
    s_current = d_prev;
}

/*
 * Set pause state to given value, and return old value
 */

bool GameControlBase::pause(bool onOff)
{
	bool oldPause = d_paused;

	if(onOff != d_paused)
	{
		d_paused = onOff;
		pauseGame(onOff);       // this is a pure virtual function that the derived class must provide
	}

	return oldPause;
}

/*
 * Call this every time a WM_TIMER message arrives
 */

void GameControlBase::onTimer()
{
	static int lock = 0;

	if(++lock == 1)
	{
#ifdef DEBUG
		static BOOL doneOnce = False;

		if(!doneOnce)
		{
			doneOnce = True;
			debugLog("WM_TIMER was called at least once\n");
		}
#endif

		/*
	 	 * Update clock based on MultiMedia Time
	 	 */

		DWORD newTime = timeGetTime();

		if(d_firstMM)
		{
			d_firstMM = False;
			d_lastMMtime = newTime;
		}
		else
		{
			DWORD change = newTime - d_lastMMtime;

			const int resolution = 1000 / SysTick::TicksPerSecond;

			if(change >= resolution)
			{
				int ticks = change / resolution;
				d_lastMMtime += ticks * resolution;

				d_procCount += ticks;

#ifdef DEBUG_TIMER
				debugLog("WM_TIMER: newTime=%ld, change=%ld, ticks=%d\n",
					(long) newTime, (long) change, ticks);
#endif

				updateTime(ticks);		// Call virtual function
			}
		}
	}
#ifdef DEBUG
	else
		debugLog("WM_TIMER message re-entered\n");
#endif
	--lock;
}

/*
 * Dummy GameControlBase to avoid having to do if(s_control) in all the functions
 */

class DefaultGameControlBase : public GameControlBase
{
    public:
	    virtual void pauseGame(bool paused) { }
	    virtual void updateTime(SysTick::Value ticks) { }

    private:
        static DefaultGameControlBase s_instance;
};

DefaultGameControlBase DefaultGameControlBase::s_instance;

/*----------------------------------------------------------------------
 * $Log$
 *----------------------------------------------------------------------
 */