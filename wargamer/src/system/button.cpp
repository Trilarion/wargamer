/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1995, Steven Morle-Green, All Rights Reserved
 *----------------------------------------------------------------------
 *
 * A replacement to the button class.
 * It emulates a User Drawn Button, but allows the checked state
 * to be set.
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/06/13 08:52:39  greenius
 * Initial Import
 *
 * Revision 1.3  1996/06/22 19:06:06  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.2  1996/02/22 10:28:36  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.1  1996/02/15 10:50:34  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "button.hpp"
#include <windowsx.h>
#include "wmisc.hpp"
#include "myassert.hpp"

#ifdef DEBUG_BUTTON
#include "clog.hpp"

LogFile blog("button.log");
#endif

static BOOL initialised = FALSE;

const char TOOLBUTTONCLASS[] = "TOOLBUTTON";

struct ToolButtonData {
   UINT state;
};

#define HANDLE_BM_SETCHECK(hwnd, wParam, lParam, fn) \
   ((fn)(hwnd, wParam), 0L)
#define HANDLE_BM_SETSTATE(hwnd, wParam, lParam, fn) \
   ((fn)(hwnd, wParam), 0L)
#define FORWARD_BM_SETSTATE(hwnd, flag, fn) \
    (void)(fn)((hwnd), BM_SETSTATE, (WPARAM)(BOOL)(flag), 0L)

static ToolButtonData* getData(HWND hwnd)
{
   ToolButtonData* td = (ToolButtonData*) GetWindowLong(hwnd, 0);
   ASSERT(td != 0);
   return td;
}

BOOL tbt_onCreate(HWND hwnd, LPCREATESTRUCT lpCreateStruct)
{
#ifdef DEBUG_BUTTON
   blog.printf("tbt_onCreate(%p)", hwnd);
   blog.close();
#endif

   ToolButtonData* td = new ToolButtonData;
   ASSERT(td != 0);
   if(td == 0)
      return FALSE;

   td->state = 0;

   SetWindowLong(hwnd, 0, (LONG) td);

   return TRUE;
}

void tbt_onDestroy(HWND hwnd)
{
#ifdef DEBUG_BUTTON
   blog.printf("tbt_onDestroy(%p)", hwnd);
   blog.close();
#endif

   ToolButtonData* td = getData(hwnd);
   delete td;
   SetWindowLong(hwnd, 0, 0);
}

void tbt_onPaint(HWND hwnd)
{
#ifdef DEBUG_BUTTON
   blog.printf("tbt_onPaint(%p)", hwnd);
   blog.close();
#endif

   ToolButtonData* td = getData(hwnd);

#ifdef DEBUG_BUTTON
   blog.printf("state = %d", (int) td->state);
   blog.close();
#endif

   PAINTSTRUCT ps;
   
   HDC hdc = BeginPaint(hwnd, &ps);

   DRAWITEMSTRUCT di;

   di.CtlType     = ODT_BUTTON;
   di.CtlID       = GetWindowLong(hwnd, GWL_ID);
   di.itemID      = 0;
   di.itemAction  = ODA_DRAWENTIRE;
   di.itemState   = td->state;
   di.hwndItem    = hwnd;
   di.hDC         = hdc;
   // CopyRect(&di.rcItem, &ps.rcPaint);
   GetClientRect(hwnd, &di.rcItem);
   di.itemData    = 0;

   FORWARD_WM_DRAWITEM(GetParent(hwnd), &di, SendMessage);

   EndPaint(hwnd, &ps);
}

BOOL tbt_onEraseBkgnd(HWND hwnd, HDC hdc)
{
   // Don't do anything, but let system think it has been erased

   return TRUE;
}

void tbt_onCheck(HWND hwnd, int fcheck)
{
#ifdef DEBUG_BUTTON
   blog.printf("tbt_onCheck(%p, %d)", hwnd, fcheck);
   blog.close();
#endif

   ToolButtonData* td = getData(hwnd);

   UINT oldState = td->state;

   if(fcheck == BST_CHECKED)
      td->state |= ODS_CHECKED;
   else
      td->state &= ~ODS_CHECKED;

#ifdef DEBUG_BUTTON
   blog.printf("oldstate = %d, state = %d", (int) oldState, (int) td->state);
   blog.close();
#endif

   if(oldState != td->state)
      InvalidateRect(hwnd, NULL, TRUE);
}

void tbt_onState(HWND hwnd, int fstate)
{
#ifdef DEBUG_BUTTON
   blog.printf("tbt_onState(%p, %d)", hwnd, fstate);
   blog.close();
#endif

   ToolButtonData* td = getData(hwnd);

   UINT oldState = td->state;

   if(fstate)
      td->state |= ODS_SELECTED;
   else
      td->state &= ~ODS_SELECTED;

#ifdef DEBUG_BUTTON
   blog.printf("oldstate = %d, state = %d", (int) oldState, (int) td->state);
   blog.close();
#endif

   if(oldState != td->state)
      InvalidateRect(hwnd, NULL, TRUE);
}

void tbt_onEnable(HWND hwnd, BOOL fEnable)
{
#ifdef DEBUG_BUTTON
   blog.printf("tbt_onEnable(%p, %d)", hwnd, fEnable);
   blog.close();
#endif

   ToolButtonData* td = getData(hwnd);

   UINT oldState = td->state;

   if(!fEnable)
      td->state |= ODS_DISABLED;
   else
      td->state &= ~ODS_DISABLED;

#ifdef DEBUG_BUTTON
   blog.printf("oldstate = %d, state = %d", (int) oldState, (int) td->state);
   blog.close();
#endif

   if(oldState != td->state)
      InvalidateRect(hwnd, NULL, TRUE);
}

void tbt_onLButtonDown(HWND hwnd, BOOL fDoubleClick, int x, int y, UINT keyFlags)
{
#ifdef DEBUG_BUTTON
   blog.printf("tbt_onLButtonDown(%p, %d,%d)", hwnd, x,y);
   blog.close();
#endif
   SetCapture(hwnd);

   FORWARD_BM_SETSTATE(hwnd, TRUE, SendMessage);
}

void tbt_onLButtonUp(HWND hwnd, int x, int y, UINT keyFlags)
{
#ifdef DEBUG_BUTTON
   blog.printf("tbt_onLButtonUp(%p, %d,%d)", hwnd, x,y);
   blog.close();
#endif

   if(GetCapture() == hwnd)
   {
      ReleaseCapture();

      FORWARD_BM_SETSTATE(hwnd, FALSE, SendMessage);

      POINT p;
      p.x = x;
      p.y = y;
      RECT r;
      GetClientRect(hwnd, &r);
      if(PtInRect(&r, p))
         FORWARD_WM_COMMAND(GetParent(hwnd), GetWindowLong(hwnd, GWL_ID), hwnd, BN_CLICKED, SendMessage);
#ifdef DEBUG_BUTTON
      else
      {
         blog.printf("Mouse not over button");
         blog.close();
      }
#endif
   }
}

LRESULT CALLBACK toolButtonProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
   switch(msg)
   {
      HANDLE_MSG(hWnd, WM_CREATE,   tbt_onCreate);
      HANDLE_MSG(hWnd, WM_DESTROY,  tbt_onDestroy);
      HANDLE_MSG(hWnd, WM_PAINT,    tbt_onPaint);
      HANDLE_MSG(hWnd, BM_SETCHECK, tbt_onCheck);
      HANDLE_MSG(hWnd, BM_SETSTATE, tbt_onState);
      HANDLE_MSG(hWnd, WM_ENABLE,   tbt_onEnable);
      HANDLE_MSG(hWnd, WM_LBUTTONDOWN, tbt_onLButtonDown);
      HANDLE_MSG(hWnd, WM_LBUTTONUP, tbt_onLButtonUp);
      HANDLE_MSG(hWnd, WM_ERASEBKGND, tbt_onEraseBkgnd);
   default:
      return DefWindowProc(hWnd, msg, wParam, lParam);
   }
}

void 

initToolButton(HINSTANCE instance)
{
   if(!initialised)
   {
      initialised = TRUE;

      WNDCLASSEX wc;

      wc.cbSize         = sizeof(WNDCLASSEX);
      wc.style          = CS_DBLCLKS | CS_PARENTDC;
      wc.lpfnWndProc    = toolButtonProc;
      wc.cbClsExtra     = 0;
      wc.cbWndExtra     = sizeof(ToolButtonData*);
      wc.hInstance      = instance;
      wc.hIcon          = NULL;
      wc.hCursor        = LoadCursor(NULL, IDC_ARROW);
      wc.hbrBackground  = NULL;     // (HBRUSH) (1 + COLOR_3DFACE);
      wc.lpszMenuName   = NULL;
      wc.lpszClassName  = TOOLBUTTONCLASS;
      wc.hIconSm        = NULL;

      RegisterClassEx(&wc);
   }
}

/*
 * Helper Class
 */


ToolButton::ToolButton(HWND h) : WindowControl(h)
{
   ASSERT_CLASS(hwnd, TOOLBUTTONCLASS);
}


ToolButton::ToolButton(HWND h, int id) : WindowControl(h, id)
{
   ASSERT_CLASS(hwnd, TOOLBUTTONCLASS);
}


void ToolButton::setCheck(bool flag)
{
   ASSERT(hwnd != NULL);
   SendMessage(hwnd, BM_SETCHECK, flag ? BST_CHECKED : BST_UNCHECKED, 0);
}

bool ToolButton::getCheck()
{
   ASSERT(hwnd != NULL);
   return (SendMessage(hwnd, BM_GETCHECK, 0, 0) == BST_CHECKED);
}


HWND addToolButton(HWND hWnd, int id, int x, int y, int w, int h)
{
   ASSERT(hWnd != 0);

   HWND hButton = CreateWindowEx(
      0,
      TOOLBUTTONCLASS, NULL,
      WS_VISIBLE | WS_CHILD | BS_OWNERDRAW | BS_CHECKBOX,
      x,y,w,h,
      hWnd, (HMENU) id, wndInstance(hWnd), NULL);
   ASSERT(hButton != NULL);

   return hButton;
}



/*
 *----------------------------------------------------------------------
 * $Log$
 *----------------------------------------------------------------------
 */
