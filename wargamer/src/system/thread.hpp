/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef THREAD_HPP
#define THREAD_HPP

#ifndef __cplusplus
#error thread.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *	Class for Threads
 *
 *----------------------------------------------------------------------
 */

#include "sysdll.h"
#include "sync.hpp"
#include "critical.hpp"
#include "ptrlist.hpp"

namespace Greenius_System
{


#ifdef __WATCOMC__
typedef unsigned int ThreadID;
typedef unsigned int ThreadReturn;
#else
typedef DWORD ThreadID;
typedef DWORD ThreadReturn;
#endif

/*
 * Thread class
 * Includes Events for pausing and Quitting
 */

class ThreadManager;

class Thread : public SyncObjectH, public SharedData
{

  public:
    SYSTEM_DLL Thread(ThreadManager* tManager);		// Create thread (but don't start)
    SYSTEM_DLL virtual ~Thread();						// Destroy Thread

    SYSTEM_DLL void start();			// Start thread
    SYSTEM_DLL void stop();			// Ask thread to terminate
    // void terminate();

    enum {
      TimeOutMs = 60000  //!< Wait up to 60 seconds for thread to start (was 20s)
    };

    enum Priority
    {
      Lowest			= THREAD_PRIORITY_LOWEST,
      BelowNormal		= THREAD_PRIORITY_BELOW_NORMAL,
      Normal			= THREAD_PRIORITY_NORMAL,
      Highest			= THREAD_PRIORITY_HIGHEST,
      AboveNormal		= THREAD_PRIORITY_ABOVE_NORMAL,
      TimeCritical	= THREAD_PRIORITY_TIME_CRITICAL,
      Idle		 		= THREAD_PRIORITY_IDLE,
    };

		//! Set Windows thread priority
    SYSTEM_DLL void setPriority(Priority priority);

    /**
     * Pause game if pauseMode True
     * Unpause if pauseMode False
     */
    SYSTEM_DLL void pause(Boolean pauseMode);

    /**
     * Ask thread to pause
     * returns immediately
     */
    SYSTEM_DLL void initPause();
    /**
     * handle to test for when paused
     * Used with initPause()
     * e.g.  initPause(); wait(pausedHandle());
     */
    SyncObject& pausedHandle() { return d_isPaused; }

		//! Ask thread to terminate
    SYSTEM_DLL void initExit();
		//! Handle to test whether thread has terminated yet.
    SyncObject& exittedHandle() { return *this; }

		//! Terminates thread, waits until it finishes
    SYSTEM_DLL void terminate();

    enum WaitValue { TWV_Quit, TWV_Event, TWV_TimeOut };
	
		/**
     * Function that thread::run should use to wait for an event
		 * e.g. run should do:
		 *				while(wait(event) != ThreadQuit) { ... }
     * Calling this automatically takes care of pausing and quitting
     */
    SYSTEM_DLL WaitValue wait(SyncObject& syncOb, DWORD ms = INFINITE);


    /*
     * Exceptions
     */

    struct ThreadError : public WinError {
        ThreadError() : WinError("Thread Error") { }
    };

    struct ThreadBadPriority : public ThreadError { };
    struct ThreadCantStart : public ThreadError { };
    struct ThreadCantCreate : public ThreadError { };

  private:
    virtual void run() = 0;		//!< User defined function for thread

    static ThreadReturn WINAPI threadProc(LPVOID param);

#ifdef DEBUG		
    //! Used to chack that we really are a Thread
    Boolean isAThread() const { return magic == Magic; }
#endif

    //------------------------------------------------------
    // Data

    ThreadID idThread;

    AutoEvent d_wantPause;	//!< Raised when system wants thread to pause
    Event	d_wantQuit;		    //!< Raised when thread should quit
    Event	d_isPaused;		    //!< Raised while thread is paused
    int d_pauseCount;		    //!< Counter of number of times paused
    bool d_running;		      //!< true if thread is running

    ThreadManager* d_tManager;	//!< Thread manager

#ifdef DEBUG		
    //! Used to chack that we really are a Thread
    enum MagicEnum { Magic = 'THRD' };
    MagicEnum magic;
#endif
};

class ThreadManager
{
  public:
    SYSTEM_DLL ThreadManager();
    SYSTEM_DLL ~ThreadManager();

    SYSTEM_DLL void add(Thread* thread);
    SYSTEM_DLL void remove(Thread* thread);

		//! Pause all threads
    SYSTEM_DLL void pause();
		//! unpause all threads
    SYSTEM_DLL void unpause();
		//! terminate all threads
    SYSTEM_DLL void terminate();

  private:
    PtrDList<Thread> d_threadList;
    RWLock	d_lock;

#ifdef DEBUG
    static int s_instanceCount;
    enum { MaxInstances = 1 };
#endif
};

}	// namespace Greenius_System

// For backwards compatibility

// using Greenius_System::Thread;

// class ThreadManager : public Greenius_System::ThreadManager
// {
// };



#endif /* THREAD_HPP */

/*----------------------------------------------------------------------------
 * $Log$
 *----------------------------------------------------------------------------
 */
