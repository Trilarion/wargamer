/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *	Very Simple little String class
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "simpstr.hpp"

SimpleString::SimpleString(const char* s) : 
	d_text(0), 
	d_alloced(0), 
	d_used(0),
	d_blockSize(8) 
{
	if(s != 0)
		operator += (s);
}

SimpleString::SimpleString(const SimpleString& s) :
	d_text(0), 
	d_alloced(0), 
	d_used(0),
	d_blockSize(s.d_blockSize) 
{
	operator = (s);
}

SimpleString::~SimpleString() 
{
	delete[] d_text; 
}

void SimpleString::expandTo(int wantSize)
{
	if(wantSize > d_alloced)
	{
		int extra = wantSize - d_alloced;
		if(extra < d_blockSize)
			wantSize = d_alloced + d_blockSize;

		char* newText = new char[wantSize];

		ASSERT(newText != 0);

		if(d_text != 0)
			memcpy(newText, d_text, d_alloced);
		delete[] d_text;
		d_text = newText;
		d_alloced = wantSize;
	}
}

SimpleString&

SimpleString::operator += (char c)
{
	// ASSERT(c != 0);

	int wantSize = d_used + 2;
	if(wantSize >= d_alloced)
	{
		expandTo(wantSize);
	}
	d_text[d_used++] = c;
	d_text[d_used] = 0;
	return *this;
}

SimpleString& 

SimpleString::operator += (const char* s)
{
	if(s != 0)
	{
		int slen = strlen(s);

		if(slen > 0)
		{
			int wantSize = d_used + slen + 1;
			if(wantSize > d_alloced)
			{
				expandTo(wantSize);
			}

			memcpy(d_text + d_used, s, slen + 1);		// Note: Does copy last nul:
			d_used += slen;
		}
	}
	return *this;
}

SimpleString& SimpleString::operator += (const SimpleString& s)
{
	// return operator+=(s.tostr());

	int wantSize = d_used + s.d_used + 1;
	expandTo(wantSize);
	memcpy(d_text + d_used, s.d_text, s.d_used+1);
	d_used += s.d_used;
	return *this;
}

SimpleString&

SimpleString::operator = (const char* s)
{
	d_used = 0;

	return operator += (s);
}

SimpleString& SimpleString::operator = (const SimpleString& s)
{
	d_used = 0;

	if(s.d_used != 0)
	{
		expandTo(s.d_used+1);
		d_used = s.d_used;
		memcpy(d_text, s.d_text, d_used+1);
	}
	return *this;
}


const char*

SimpleString::toStr() const
{
#if 0
	if( (d_text == 0) || (d_used == 0) )
		return 0;
	else
	{
		// if(d_text[d_used - 1] != 0)
		if( (d_used == d_alloced) || ((d_text[d_used] != 0))
		{
			expandTo(d_used + 1);
			d_text[d_used] = 0;
		}

		return d_text;
	}
#else
	if( (d_text == 0) || (d_used == 0) )
		return 0;
	else
		return d_text;
#endif
}

