/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef TRIG_H
#define TRIG_H

#ifndef __cplusplus
#error trig.h is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 * Trigonometry Definition
 *
 *----------------------------------------------------------------------
 */

#include "sysdll.h"
#include "mytypes.h"

typedef UBYTE Bangle;
typedef UWORD Wangle;
typedef UBYTE Qangle;	// Quadrant (0,1,2,3)

SYSTEM_DLL Wangle direction(SLONG x, SLONG y);		// arctan function

inline Wangle Degree(int d)
{
	return UWORD(( (d << 16) + 180) / 360);
}

inline Wangle bangleToWangle(Bangle b)
{
 	return Wangle(b << 8);
}

inline Bangle wangleToBangle(Wangle w)
{
 	return Bangle(w >> 8);
}

inline Wangle qangleToWangle(Qangle q)
{
	return Wangle(q << 14);
}

inline Qangle wangleToQangle(Wangle w)
{
 	return Qangle(w >> 14);
}


SYSTEM_DLL SLONG msin(SLONG value, Wangle angle);
SYSTEM_DLL SLONG mcos(SLONG value, Wangle angle);

/*
 * Quick and dirty distance routine
 * SWG:19May2001: Renamed from distance() to avoid confusion with stl::distance(Iterator...)
 */

template<class T>
inline T aproxDistance(T x, T y)
{
	if(x < 0) x = -x;
	if(y < 0) y = -y;

	if(x > y)
			return x + y / 2;
	else
			return y + x / 2;
}

#endif /* TRIG_H */


/*----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.2  1995/10/20 11:05:41  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.1  1995/10/18 10:59:53  Steven_Green
 * Initial revision
 *
 */
