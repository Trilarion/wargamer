/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *	Generic Exception Class Implementation
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "except.hpp"
#include "misc.hpp"
#include <stdarg.h>
#include <stdio.h>
#include <string.h>


/*
 * Exception Handling
 */

Boolean GeneralError::doingError = False;
Boolean GeneralError::firstError = False;


GeneralError::GeneralError()
{
	describe = 0;
}


GeneralError::GeneralError(const char* fmt, ...)
{
	char buffer[500];

	va_list vaList;
	va_start(vaList, fmt);
	vbprintf(buffer, 500, fmt, vaList);
	va_end(vaList);

	/*
	 * Convert any \r's into \r\n
	 */

	char* s = buffer;
	while(*s)
	{
		if(*s++ == '\r')
		{
			char* s1 = s;
			char last = '\n';

			do
			{
				char c = *s1;
				*s1 = last;
				last = c;

				s1++;
			}  while(last);
			*s1 = 0;
		}
	}

	setMessage(buffer);
	// messageBox("Fatal Error!", buffer, MB_ICONSTOP);
	ExceptionUtility::alert("Fatal Error!", buffer);
}

void

GeneralError::setMessage(const char* s)
{
	firstError = doingError;
	doingError = True;

	describe = strdup(s);

#if !defined(NOLOG)
	if(!firstError)
		ExceptionUtility::logError("General Error Report", s);
#endif
}

const char* GeneralError::get() const
{
	return describe;
}

/*
 *----------------------------------------------------------------------
 * $Log$
 *----------------------------------------------------------------------
 */
