/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef FIXPOINT_HPP
#define FIXPOINT_HPP

#ifndef __cplusplus
#error fixpoint.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1995, Steven Morle-Green, All Rights Reserved
 *----------------------------------------------------------------------
 *
 *	Fixed Point 16.16 class
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 *
 *----------------------------------------------------------------------
 */

#include "sysdll.h"
#include "mytypes.h"

class UFixed16_16 {
	enum {
		mask = (1 << 16) - 1
	};

	// static const ULONG mask;	// = (1 << 16) - 1;		// 0xffff


	ULONG value;
public:
	/*
	 * Constructors
	 */

	UFixed16_16() { }

	UFixed16_16(const UFixed16_16& v) 
	{
		*this = v;	// value = v.value; 
	}

	UFixed16_16(UWORD n) 
	{
		*this = n;	// value = n << 16; 
	}

	UFixed16_16(UWORD i, UWORD n, UWORD d)
	{
		set(i, n, d);
	}

	/*
	 * Assignments
	 */

	UFixed16_16& operator = (const UFixed16_16& v) 
	{
		value = v.value; 
		return *this;
	}
	
	UFixed16_16& operator = (UWORD n)
	{
		value = n << 16; 
		return *this;
	}

	/*
	 * Assign to i + n/d
	 * e.g. set(1,2,3), sets it to one and two thirds (0x0001AAAA)
	 */

	UFixed16_16& set(UWORD i, UWORD n, UWORD d)
	{
		ASSERT(d != 0);

		value = (i << 16) + (n << 16) / d; 
		return *this;
	}

	/*
	 * Some arithmetic operations...
	 */

	UFixed16_16& operator += (const UFixed16_16& v) 
	{
		value += v.value;
		return *this;
	}

	UFixed16_16 operator + (const UFixed16_16& v)
	{
		UFixed16_16 temp = *this;
		temp += v;
		return temp;
	}

	UFixed16_16& operator -= (const UFixed16_16& v) 
	{
		value -= v.value;
		return *this;
	}

	UFixed16_16 operator - (const UFixed16_16& v)
	{
		UFixed16_16 temp = *this;
		temp -= v;
		return temp;
	}

	UFixed16_16& operator *= (const UFixed16_16& v)
	{
		// Must be careful to avoid overflows...

#if 0
		UWORD a = UWORD(value >> 16);
		UWORD b = UWORD(value & mask);
		UWORD c = UWORD(v.value >> 16);
		UWORD d = UWORD(v.value & mask);

		if( (a == 0) && (c == 0) )
			value = (b * d) >> 16;
		else
			value = ((a * c) << 16) +
					 (a * d) +
					 (b * c) +
					 ((b * d) >> 16);
#else
		UWORD a = UWORD(value / 65536U);
		UWORD b = UWORD(value & mask);
		UWORD c = UWORD(v.value / 65536U);
		UWORD d = UWORD(v.value & mask);

		if( (a == 0) && (c == 0) )
			value = (b * d) / 65536U;
		else
			value = ((a * c) * 65536U) +
					 (a * d) +
					 (b * c) +
					 ((b * d) / 65536U);
#endif
		return *this;
	}

	UFixed16_16 operator * (const UFixed16_16& v)
	{
		UFixed16_16 temp = *this;
		temp *= v;
		return temp;
	}

	UFixed16_16& operator /= (UWORD n)
	{
		return operator *= (UFixed16_16(0, 1, n));
	}

	UFixed16_16 operator / (UWORD n)
	{
		UFixed16_16 temp = *this;
		temp /= n;
		return temp;
	}

	UFixed16_16& operator *= (UWORD n)
	{
#if 0
		UWORD a = UWORD(value >> 16);
		UWORD b = UWORD(value & mask);

		value = ((a * n) << 16) + (b * n);
#else
		UWORD a = UWORD(value / 65536U);
		UWORD b = UWORD(value & mask);

		value = ((a * n) * 65536U) + (b * n);
#endif

		return *this;
	}

	Boolean operator > (const UFixed16_16& v) { return value > v.value; }

	Boolean operator != (UWORD n)
	{
#if 0
		return value != (n << 16);
#else
		return value != (n * 65536U);
#endif
	}

	/*
	 * Retrieval Functions
	 */

#if 0
	UWORD getInt() const { return UWORD(value >> 16); }
#else
	UWORD getInt() const { return UWORD(value / 65536U); }
#endif
	UWORD getFraction() const { return UWORD(value & mask); }

	// Raw Access

	ULONG getValue() const { return value; }
	void setValue(ULONG v) { value = v; }

};

// static const ULONG UFixed16_16::mask = (1 << 16) - 1;		// 0xffff


#endif /* FIXPOINT_HPP */

