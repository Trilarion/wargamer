/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef GAMETICK_HPP
#define GAMETICK_HPP

#ifndef __cplusplus
#error gametick.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *	Game Ticks Definition
 *
 *----------------------------------------------------------------------
 */

#include "sysdll.h"
#include <limits.h>

class SysTick {
 public:

 	// Some constant values to do with time

	enum {
		DaysPerYear 	  = 365,
		DaysPerWeek		  = 7,
		HoursPerDay		  = 24,
		MinutesPerHour	  = 60,
		MinutesPerDay	  = (MinutesPerHour * HoursPerDay),
		SecondsPerMinute = 60,
		SecondsPerHour	  = (MinutesPerHour * SecondsPerMinute),
		SecondsPerDay	  = (SecondsPerHour * HoursPerDay),

		TicksPerSecond = 10,

		TicksPerMinute = (TicksPerSecond * SecondsPerMinute),
		TicksPerHour	= (TicksPerMinute * MinutesPerHour),
		TicksPerDay		= (TicksPerSecond * SecondsPerDay)
	};

	typedef ULONG Value;
	enum { Max = ULONG_MAX };
};



#endif /* GAMETICK_HPP */

