/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef DIB_UTIL_HPP
#define DIB_UTIL_HPP

#ifndef __cplusplus
#error dib_util.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *	DIB Utilities
 *
 *----------------------------------------------------------------------
 */

#include "sysdll.h"
#include "mytypes.h"
#include "grtypes.hpp"
#include "point.hpp"

class DIB;
class DrawDIB;
class DrawDIBDC;
struct CustomBorderInfo;
class ColorRemapTable;

namespace DIB_Utility
{

SYSTEM_DLL void colorize(DrawDIB* destDIB, const DIB* src, COLORREF rgb, int percent, LONG destX, LONG destY, LONG w, LONG h, LONG srcX, LONG srcY);
SYSTEM_DLL void edge(DrawDIB* dib, int xOffset, int yOffset);

SYSTEM_DLL void colorEdge(DrawDIB* dib, int xOffset, int yOffset, ColourIndex index);
SYSTEM_DLL void colorEdge(DrawDIB* dib, int xOffset, int yOffset, ColourIndex index, const Rect<int>& rect);

// SYSTEM_DLL void remapColor(DrawDIB* destDIB, const DIB* src, const UBYTE* table, ColourIndex index, LONG destX, LONG destY, LONG w, LONG h, LONG srcX, LONG srcY);
SYSTEM_DLL void remapColor(DrawDIB* destDIB, const DIB* src, const ColorRemapTable* table, ColourIndex index, LONG destX, LONG destY, LONG w, LONG h, LONG srcX, LONG srcY);

struct Draw3DTextData {
  enum {
	 Imbedded = 0x01,
	 Colorize = 0x02,
	 Lighten  = 0x04
  };

  const char* d_text;
  int d_remapPercent;
  HFONT d_hFont;
  int d_x;
  int d_y;
  UBYTE d_flags;
  COLORREF d_color;
};

SYSTEM_DLL void draw3DText(DrawDIBDC* destDib, const Draw3DTextData& data); //HFONT hFont, const char* text, int x, int y, Draw3DTextHow::How drawHow, const int remapPercent);
SYSTEM_DLL void drawGroupBox(DrawDIBDC* dib, HFONT hFont, const RECT& cRect,  const char* text, const CustomBorderInfo& bc);
SYSTEM_DLL Boolean allocateDib(DrawDIBDC** dib, const int cx, const int cy);

struct InsertData {
	COLORREF d_cRef;
	int d_x;
	int d_y;
	int d_cx;
	int d_cy;
	int d_shadowCX;
	int d_shadowCY;

	enum Mode {
	  Transparent,
	  ShadowOnly
	} d_mode;

	InsertData() :
	  d_x(0),
	  d_y(0),
	  d_cx(0),
	  d_cy(0),
	  d_shadowCX(0),
	  d_shadowCY(0),
	  d_mode(Transparent) {}
};

SYSTEM_DLL void drawInsert(DrawDIBDC* dib, const InsertData& data);


};	// namespace DIB_Utility

#endif /* DIB_UTIL_HPP */

/*---------------------------------------------------------------
 * $Log$
 *---------------------------------------------------------------
 */
