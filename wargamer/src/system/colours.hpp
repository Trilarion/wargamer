/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef COLOURS_HPP
#define COLOURS_HPP

#ifndef __cplusplus
#error colours.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *	Definition of some useful colours
 *
 *----------------------------------------------------------------------
 */

#include "sysdll.h"

class Colors {
public:
	enum {
		Black = RGB(  0,   0,   0),
		White = RGB(255, 255, 255)
	};
};

class PaletteColors {
public:
	enum {
		Black      = PALETTERGB(0, 0, 0),
		White      = PALETTERGB(255, 255, 255),
		Red        = PALETTERGB(150, 0, 0),
		Green      = PALETTERGB(0, 150, 0),
		Blue       = PALETTERGB(0, 0, 220),
		Yellow     = PALETTERGB(255, 192, 0),
		LightGrey  = PALETTERGB(192, 192, 192),
		DarkGrey   = PALETTERGB(64, 64, 64),
		LightBrown = PALETTERGB(189, 165, 123),
		OffWhite   = PALETTERGB(255, 247, 215),
		Tan        = PALETTERGB(230, 214, 123),
		DarkBrown  = PALETTERGB(115, 90, 66)
	};
};

typedef Colors Colours;
typedef PaletteColors PaletteColours;

#endif /* COLOURS_HPP */

