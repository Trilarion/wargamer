/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
#ifndef MINMAX_HPP
#define MINMAX_HPP

#ifndef __cplusplus
#error minmax.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 * Minimum/Maximum classes
 *
 *----------------------------------------------------------------------
 */

#include "sysdll.h"
#include <algorithm> // for min/max
#include "point.hpp"

template<class T>
class MinMax
{
   public:
      MinMax() :
         d_initialised(false),
         d_min(), d_max() { }

      MinMax(T value) :
         d_initialised(true),
         d_min(value), d_max(value) { }

      void clear()
      {
         d_initialised = false;
      }

      MinMax<T>& operator = (T value)
      {
         d_initialised = true;
         d_min = d_max = value;
         return *this; 
      }

      MinMax<T>& operator = (const MinMax<T>& m) 
      {
         d_min = m.d_min;
         d_max = m.d_max;
         d_initialised = m.d_initialised;
         return *this; 
      }


      MinMax<T>& operator += (T value)
      {
         if(!d_initialised)
            return operator = (value);
         else
         {
            d_min = minimum(value, d_min); 
            d_max = maximum(value, d_max); 
         }
         return *this;
      }

      MinMax<T>& operator += (const MinMax<T>& mm)
      {
         if(d_initialised)
         {
            if(mm.d_initialised)
            {
               d_min = minimum(d_min, mm.d_min);
               d_max = maximum(d_max, mm.d_max);
            }
         }
         else
            operator = (mm);
         return *this;
      }

      T minValue() const { return d_min; }
      T maxValue() const { return d_max; }

   private:
      T d_min;
      T d_max;
      bool d_initialised;
};


template<class T>
class MinMaxRect
{
   public:
      MinMaxRect() : d_x(), d_y() { }

      void set(const Point<T>& p)
      {
         d_x = p.x();
         d_y = p.y();
      }

      void updateX(T v) { d_x += v; }
      void updateY(T v) { d_y += v; }

      void update(const Point<T>& p)
      {
         updateX(p.x());
         updateY(p.y());
      }

      void combine(const MinMaxRect<T>& r)
      {
         d_x += r.d_x;
         d_y += r.d_y;
      }

      MinMaxRect<T>& operator += (const Point<T>& p) { update(p); return *this; }
      MinMaxRect<T>& operator += (const MinMaxRect<T>& p) { combine(p); return *this; }

      T minX() const { return d_x.minValue(); }
      T maxX() const { return d_x.maxValue(); }
      T minY() const { return d_y.minValue(); }
      T maxY() const { return d_y.maxValue(); }

      const MinMax<T>& x() const { return d_x; }
      MinMax<T>& x() { return d_x; }
      const MinMax<T>& y() const { return d_y; }
      MinMax<T>& y() { return d_y; }

   private:
      MinMax<T> d_x;
      MinMax<T> d_y;
};

#ifdef DEBUG

#include "clog.hpp"

template<class T>
inline LogFile& operator << (LogFile& file, const MinMax<T>& p)
{
   return file << "(" << p.minValue() << ".." << p.maxValue() << ")" ;
}

template<class T>
inline LogFile& operator << (LogFile& file, const MinMaxRect<T>& r)
{
   return file << r.x() << "," << r.y();
}

// Specialised UBYTE version to prevent silly characters

inline LogFile& operator << (LogFile& file, const MinMax<UBYTE>& p)
{
   return file << "(" << 
      static_cast<int>(p.minValue()) << ".." << 
      static_cast<int>(p.maxValue()) << ")" ;
}


#endif

#endif /* MINMAX_HPP */

/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.2  2001/06/18 08:25:46  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
