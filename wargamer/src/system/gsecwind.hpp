/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef GSECWIND_HPP
#define GSECWIND_HPP

#include "sysdll.h"
#include "wind.hpp"

/*----------------------------------------------------------------------
 * base class for window sections
 *
 */

class DrawDIB;
class DrawDIBDC;
class SYSTEM_DLL GSecWind : public WindowBaseND
{
	 DrawDIBDC* d_dib;
	 const DrawDIB* d_fillDib;

	 int d_cx;
	 int d_cy;

	 static UWORD s_instanceCount;

	 enum SecFlags {
		SizeChanged = 0x01,
		ContentChanged = 0x02
	 };

	 UBYTE d_flags;

	public:
	 GSecWind(const DrawDIB* fillDib) :
		d_dib(0),
		d_fillDib(fillDib),
		d_cx(0),
		d_cy(0),
		d_flags(SizeChanged)
	 {
		s_instanceCount++;
		ASSERT(d_fillDib);
	 }

	 ~GSecWind();

	 virtual void drawDib() = 0;
	 virtual void enableControls() = 0;

	 const DrawDIB* fillDib() const { return d_fillDib; }
	 DrawDIBDC* dib() const { return d_dib; }

	 int cx() const { return d_cx; }
	 int cy() const { return d_cy; }

	 void sizeChanged(Boolean f)
	 {
		if(f)
			d_flags |= SizeChanged;
		else
			d_flags &= ~SizeChanged;
	 }

	 Boolean sizeChanged() const { return (d_flags & SizeChanged); }

	 void update();
	 void redraw();
	 void drawBorder();
	 void onPaint(HWND hwnd);

};
#endif
