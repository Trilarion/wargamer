/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *	Simple GDI line drawing engine
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#if !defined(NOMINMAX)
#define NOMINMAX 1
#endif
// #include <afxwin.h>
#include <algorithm>	// stl algorithms


#include "gdiengin.hpp"

// GDIEngine::GDIEngine(CDC& dc) :
GDIEngine::GDIEngine(HDC dc) :
	d_dc(dc),
	d_pen(NULL),
	d_oldPen(NULL),
	d_closed(true)
{
	d_pen = CreatePen(PS_SOLID, 1, RGB(0,0,0));
	d_oldPen = static_cast<HPEN>(SelectObject(d_dc, &d_pen));
}

GDIEngine::~GDIEngine()
{
	if(d_oldPen)
		SelectObject(d_dc, d_oldPen);
	DeleteObject(d_pen);

#if 0
	if(d_oldBrush)
		d_dc->SelectObject(d_oldBrush);
#endif
}

void GDIEngine::setPen(int style, int width, COLORREF col)
{
	SelectObject(d_dc, d_oldPen);
	DeleteObject(d_pen);
	d_pen = CreatePen(style, width, col);
	SelectObject(d_dc, d_pen);
}

void GDIEngine::addPoint(const GPoint& point)
{
#if 0
	/*
	 * Put a marker on
	 */

	d_dc->SelectObject(&d_markerPen);
	d_dc->SelectObject(&d_markerBrush);
	d_dc->Rectangle(point.x()-1,point.y()-1,point.x()+1,point.y()+1);
#endif

	if(d_closed)
	{
		d_closed = false;

		MoveToEx(d_dc, point.x(), point.y(), NULL);
		d_lastPoint = point;
	}
	else
	{
		// if(point != d_lastPoint)
		if(!(point == d_lastPoint))
		{
			SelectObject(d_dc, d_pen);
			LineTo(d_dc, point.x(), point.y());
			d_lastPoint = point;
		}
	}
}

void GDIEngine::close()
{
	// ASSERT(!d_closed);
	d_closed = true;
}

