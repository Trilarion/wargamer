/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef POPICON_HPP
#define POPICON_HPP

#ifndef __cplusplus
#error popicon.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1995, Steven Morle-Green, All Rights Reserved
 *----------------------------------------------------------------------
 *
 *	User defined Window control
 *
 * This is like a combo box but works with images instead
 * Normally it looks like a button with an icon on it
 * Pressing the button brings up a popup box containing other icons
 * to select from.  Releasing the button selects whatever icon the mouse
 * is over.
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 *
 *----------------------------------------------------------------------
 */

#include "sysdll.h"
#include "winctrl.hpp"

class ImageLibrary;

SYSTEM_DLL void initPopupIcon(HINSTANCE instance);

// HWND addPopupIcon(HWND hWnd, HWND hTooltip, int id, int x, int y, int w, int h, ImageLibrary* il);
	// Obsolete

SYSTEM_DLL HWND addPopupIcon(HWND hWnd, int id, int x, int y, int w, int h, ImageLibrary* il);

extern SYSTEM_DLL const char POPUPICONCLASS[];

/*
 * Messages
 */

#define PIM_FIRST			(WM_USER)
#define PIN_FIRST			(0)

/*
 * Messages sent to window
 */

#define PIM_SETIMAGELIST 	(PIM_FIRST + 0)		// wparam=0,		lparam=ImageLibrary*
//#define PIM_SETHELPIDS 	   (PIM_FIRST + 1)		// wparam=0,		lparam=DWORD*
#define PIM_SETCURSEL		(PIM_FIRST + 2)		// wparam=index,	lparam=0
#define PIM_GETCURSEL		(PIM_FIRST + 3)		// wparam=0,		lparam=0,		return index
#define PIM_ADDITEM        (PIM_FIRST + 4)      // wparam=0,		lparam=POPICONITEM,	return index
#define PIM_REMOVEITEM     (PIM_FIRST + 5)      // wparam=index,	lparam=0
#define PIM_RESETCONTENT   (PIM_FIRST + 6)      // wparam=0,		lparam=0
#define PIM_GETVALUE			(PIM_FIRST + 7)		// wparam=index,	lparam=0,		return value
#define PIM_SETVALUE			(PIM_FIRST + 8)		// wparam=0,		lparam=value,	return index

/*
 * Notify messages sent via WM_COMMAND
 */

#define PIN_SELCHANGED		(PIN_FIRST + 0)

/*
 * Data sent to ADDITEM
 */

struct POPICONITEM {
	int imageIndex;
	int value;
	int tipID;
	int hintID;
};

/*
 * Class to simplify things
 */

class ImageLibrary;

class SYSTEM_DLL PopupIcon : public WindowControl {
public:
	PopupIcon(HWND hwnd);
	PopupIcon(HWND hwnd, int id);

	void setImageList(ImageLibrary* img);
//	void setHelpIDs(DWORD* ids);
	void set(int index);
	void setValue(int value);
	int get();
	int getValue();
	int addItem(int imageIndex, int value, int tipID, int statusID);
	void removeItem(int index);
	void reset();
};


#endif /* POPICON_HPP */

