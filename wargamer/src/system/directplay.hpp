/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef DIRECTPLAY_HPP
#define DIRECTPLAY_HPP

#include <dplay.h>
#include "Sllist.hpp"
#include "sysdll.h"

/*

  Filename : DirectPlay.hpp

  Description : Class to encapsulate DPLAY object, and setup process

  Notes : Sets up DirectPlay using static members to describe the connection details
		  Any dialogs wishing to setup DPLAY should change these static members to alter the connection

		  System messages are received and virtual functions called for them - this lets important events
		  such as the connectino being lost, get propogated up to the game quickly

		  Game messages are sent & received via a void * to the data.

		  Classes for getting & sending messages should be derived from CDirectPlayUser. Only one can be active
		  at a time - and must by set by calling RegisterUser().
 *
 *------------------------------------------------------
 * Updated: 16dec99 SWG.
 *   Removed g_directPlay and replaced with a Singleton
*/


class DPlayPlayerDesc : public SLink {

public:

	DPID Id;
	DWORD Flags;

	DPlayPlayerDesc(void) {
		Id = 0;
		Flags = 0;
	}
	~DPlayPlayerDesc(void) {
	}
};




class DPlayConnectionDesc : public SLink {

public:

	GUID Guid;
	LPVOID Connection;
	DWORD ConnectionSize;
	char * Name;
	DWORD Flags;

	bool InitializedOK;

	DPlayConnectionDesc(void) {
		Connection = 0;
		Name = 0;
	}
	~DPlayConnectionDesc(void) {
		if(Connection) free((LPVOID)Connection);
		if(Name) free((LPVOID)Name);
	}

};

typedef SListS<DPlayConnectionDesc> DPlayConnectionList;


class DPlaySessionDesc : public SLink {

public:

	char * Desc;
	GUID SessionGuid;
	GUID ApplicationGuid;

	DWORD Flags;

	DWORD MaxPlayers;
	DWORD NumPlayers;

	char * Password;

	DPlaySessionDesc(void) {
		Desc = 0;
		Flags = 0;
		MaxPlayers = 0;
		NumPlayers = 0;
		Password = 0;
	}
	~DPlaySessionDesc(void) {
		if(Desc) free((LPVOID)Desc);
		if(Password) free((LPVOID)Password);
	}

};

typedef SListS<DPlaySessionDesc> DPlaySessionList;

struct SessionCaps {

	bool ClientServer;
	bool DirectPlayProtocol;
	bool JoinDisabled;
	bool KeepAlive;
	bool MigrateHost;
	bool MulticastServer;
	bool NewPlayersDisabled;
	bool NoDataMessages;
	bool NoMessageID;
	bool NoPreserveOrder;
	bool OptimizeLatency;
	bool PasswordRequired;
	bool Private;
	bool SecureServer;


	SessionCaps(void) {
		FromDWORD(0);
	}
	SessionCaps(DWORD flags) {
		FromDWORD(flags);
	}
	~SessionCaps(void) { }

	void FromDWORD(DWORD flags) {
		ClientServer = ((flags & DPSESSION_CLIENTSERVER) != 0);
		DirectPlayProtocol = ((flags & DPSESSION_DIRECTPLAYPROTOCOL) != 0);
		JoinDisabled = ((flags & DPSESSION_JOINDISABLED) != 0);
		KeepAlive = ((flags & DPSESSION_KEEPALIVE) != 0);
		MigrateHost = ((flags & DPSESSION_MIGRATEHOST) != 0);
		MulticastServer = ((flags & DPSESSION_MULTICASTSERVER) != 0);
		NewPlayersDisabled = ((flags & DPSESSION_NEWPLAYERSDISABLED) != 0);
		NoDataMessages = ((flags & DPSESSION_NODATAMESSAGES) != 0);
		NoMessageID = ((flags & DPSESSION_NOMESSAGEID) != 0);
		NoPreserveOrder = ((flags & DPSESSION_NOPRESERVEORDER) != 0);
		OptimizeLatency = ((flags & DPSESSION_OPTIMIZELATENCY) != 0);
		PasswordRequired = ((flags & DPSESSION_PASSWORDREQUIRED) != 0);
		Private = ((flags & DPSESSION_PRIVATE) != 0);
		SecureServer = ((flags & DPSESSION_SECURESERVER) != 0);
	}

	DWORD ToDWORD(void) {

		DWORD flags = 0;

		if(ClientServer) flags |= DPSESSION_CLIENTSERVER;
		if(DirectPlayProtocol) flags |= DPSESSION_DIRECTPLAYPROTOCOL;
		if(JoinDisabled) flags |= DPSESSION_JOINDISABLED;
		if(KeepAlive) flags |= DPSESSION_KEEPALIVE;
		if(MigrateHost) flags |= DPSESSION_MIGRATEHOST;
		if(MulticastServer) flags |= DPSESSION_MULTICASTSERVER;
		if(NewPlayersDisabled) flags |= DPSESSION_NEWPLAYERSDISABLED;
		if(NoDataMessages) flags |= DPSESSION_NODATAMESSAGES;
		if(NoMessageID) flags |= DPSESSION_NOMESSAGEID;
		if(NoPreserveOrder) flags |= DPSESSION_NOPRESERVEORDER;
		if(OptimizeLatency) flags |= DPSESSION_OPTIMIZELATENCY;
		if(PasswordRequired) flags |= DPSESSION_PASSWORDREQUIRED;
		if(Private) flags |= DPSESSION_PRIVATE;
		if(SecureServer) flags |= DPSESSION_SECURESERVER;

		return flags;
	}
};


/*

  Classes which use CDirectPlay should be derived from this, and should
  implement member functions for these events

*/
class CDirectPlayUser {

public:

	SYSTEM_DLL CDirectPlayUser(void) { }
	virtual ~CDirectPlayUser(void) { }

	// sent when a new message has arrived in the DirectPlay message queue from the remote player
	// the message is removed from the DirectPlay message queue, and passed to the app via this function
	virtual void onNewMessage(LPVOID data, DWORD data_length) = 0;

	// the remote opponent has joined a locally hosted game
	virtual void onOpponentJoin(void) = 0;
	// the opponent has left the game
	virtual void onOpponentQuit(void) = 0;
	// the connection has somehow terminated
	virtual void onConnectionLost(void) = 0;
	// chat message received
	virtual void onChatMessage(LPSTR text) = 0;

	// some internal error has occured
	virtual void onError(char * context, char * desc) = 0;
};



class CDirectPlay {

public:

	SYSTEM_DLL  CDirectPlay(void);
	SYSTEM_DLL  ~CDirectPlay(void);

	enum ConnectionTypeEnum {
		NotConnected,
		Host,
		Client
	};

	SYSTEM_DLL bool registerUser(CDirectPlayUser * user);
	SYSTEM_DLL LPVOID getMessage(void);
	SYSTEM_DLL bool sendMessage(LPVOID data, DWORD data_length);
	SYSTEM_DLL LPDIRECTPLAY4 getDirectPlay(void);

	SYSTEM_DLL bool createObject(void);
	SYSTEM_DLL bool destroyObject(void);

	SYSTEM_DLL bool enumerateConnections(void);
	SYSTEM_DLL bool connect(void);
	SYSTEM_DLL bool enumerateSessions(void);
	SYSTEM_DLL bool createSession(DPlaySessionDesc * desc, SessionCaps * caps);
	SYSTEM_DLL bool joinSession(void);
	SYSTEM_DLL bool closeSession(void);

	SYSTEM_DLL bool createPlayer(void);
	SYSTEM_DLL bool destroyPlayer(void);
	SYSTEM_DLL bool enumeratePlayers(void);

	SYSTEM_DLL bool createMsgThread(void);
	SYSTEM_DLL bool destroyMsgThread(void);

	SYSTEM_DLL bool createReceiveBuffer(DWORD size);
	SYSTEM_DLL bool destroyReceiveBuffer(void);

	SYSTEM_DLL bool sendChatMessage(LPDPID to, char * text);

	SYSTEM_DLL DPID * getLocalPlayer(void) { return &d_localPlayerID; }
	SYSTEM_DLL DPID * getRemotePlayer(void) { return &d_remotePlayerID; }

   static SYSTEM_DLL DPlayConnectionList& connectionsList() { return d_connectionsList; }
	static SYSTEM_DLL selectedConnection(DPlayConnectionDesc *con) { d_selectedConnection = con; }
   static SYSTEM_DLL DPlaySessionList& sessionsList() { return d_sessionsList; }
   static SYSTEM_DLL selectedSession(DPlaySessionDesc* ses) { d_selectedSession = ses; }


private:

	static DWORD WINAPI msgThreadFunction(LPVOID lpParam);
	bool processSystemMessage(DPMSG_GENERIC * generic_msg);
	bool processGameMessage(LPVOID data, DWORD data_length);

	bool reportError(char * context, HRESULT retval);

private:
   LPDIRECTPLAY4 instance();    // Return instance of DIRECTPLAY



	ConnectionTypeEnum d_connectionType;
	CDirectPlayUser * d_user;

	static LPDIRECTPLAY4 s_directPlay;  // This is returned by instance()

	HANDLE d_msgEvent;
	static char s_msgEventName[];
	HANDLE d_msgThread;
	DWORD d_msgThreadID;

	char * d_receiveBuffer;
	DWORD d_receiveBufferLength;

	DPID d_localPlayerID;
	DPID d_remotePlayerID;

// public:
private:

	static LPGUID d_enumConnectionsGUID;
	static SYSTEM_DLL DPlayConnectionDesc * d_selectedConnection;
	static SYSTEM_DLL DPlayConnectionList d_connectionsList;

	static LPGUID d_enumSessionsApplicationGUID;
	static SYSTEM_DLL DPlaySessionDesc * d_selectedSession;
	static SYSTEM_DLL DPlaySessionList d_sessionsList;

	static SListS<DPlayPlayerDesc> d_playersList;

   static BOOL FAR PASCAL
   enumConnectionsCallback(
	   LPCGUID lpguidSP,
	   LPVOID lpConnection,
	   DWORD dwConnectionSize,
	   LPCDPNAME lpName,
	   DWORD dwFlags,
	   LPVOID lpContext
   );

   static BOOL FAR PASCAL
   enumSessionsCallback(
	   LPCDPSESSIONDESC2 lpThisSD,
	   LPDWORD lpdwTimeOut,
	   DWORD dwFlags,
	   LPVOID lpContext
   );

   static BOOL FAR PASCAL
   enumPlayersCallback(
	   DPID dpId,
	   DWORD dwPlayerType,
	   LPCDPNAME lpName,
	   DWORD dwFlags,
	   LPVOID lpContext
   );

};


// global pointer
extern SYSTEM_DLL CDirectPlay * g_directPlay;





#endif

/*
 *----------------------------------------------------------------------
 * $Log$
 *----------------------------------------------------------------------
 */

