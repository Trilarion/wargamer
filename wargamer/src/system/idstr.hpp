/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef IDSTR_HPP
#define IDSTR_HPP

#ifndef __cplusplus
#error idstr.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1995, Steven Morle-Green, All Rights Reserved
 *----------------------------------------------------------------------
 *
 *	Way of getting a string from an enumeration
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 *
 *----------------------------------------------------------------------
 */

#include "sysdll.h"
#include "mytypes.h"
#include "myassert.hpp"
#include "except.hpp"

/*
 * Structure matching text to id
 */
 	
struct MessagePair {
	int id;
	const char* name;
};

#ifndef _WINNT_
typedef const char* PCSTR;		// Constant String
#endif


SYSTEM_DLL Boolean mpGetName(const MessagePair* mp, PCSTR& s, int id);
SYSTEM_DLL Boolean mpGetID(const MessagePair* mp, const char* s, int& id);

SYSTEM_DLL const char* mpGetNameNotNull(MessagePair* mp, int id, const char* def);

/*
 * Macro to create a MessagePair from an enumerated name
 */

#define MP(m) { m, #m }

#define MP_Illegal -1
#define MP_End { MP_Illegal, 0 }

/*
 * Class for parsing ascii file lines
 *
 * Note that the string given to StringParse WILL be modified by
 * having 0's added in after each token.
 */

class SYSTEM_DLL StringParse {
	char* text;
public:
	StringParse() { text = 0; }
	StringParse(char* s) { init(s); }
	void init(char* s) { text = s; }

	char* getToken();
	Boolean getInt(unsigned int& n);
   Boolean getSInt(int& n);
	Boolean getFixed(unsigned int& n, unsigned int resolution);
	Boolean getEnumeration(const MessagePair* mp, int& num);

	Boolean get(int& n) { return getSInt(n); }
	Boolean get(unsigned int& n) { return getInt(n); }
	Boolean get(long& n);
	Boolean get(unsigned long& n);

	operator const char*() { return text; }

private:

	Boolean isSpace(char c) const;

public:

	class StringParseError : public GeneralError {
	public:
		StringParseError() : GeneralError("String Parsing Error") { }
	};
};


#endif /* IDSTR_HPP */

