/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef SOUNDS_HPP
#define SOUNDS_HPP

#ifndef __cplusplus
#error sounds.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1995, Steven Morle-Green, All Rights Reserved
 *----------------------------------------------------------------------
 *
 *	Sound Effect Playback
 *
 * Since there can be only one Sound class in an application,
 * it uses static functions, which are accessed using:
 *		Sound::playSound(soundName);
 *
 * The implementation details are hidden  inside sounds.cpp

 *----------------------------------------------------------------------
 */

#include "sysdll.h"
#include "mytypes.h"
#include "except.hpp"

class SYSTEM_DLL Sound {
	// Unimplemented functions to prevent anything declaring
	// an instance of Sound

	 Sound();
	 Sound(const Sound& sound);
	 ~Sound();
	 Sound& operator = (const Sound& sound);

  public:
	 static void playSound(const char* fileName);
	 	// Throws a SoundError exception if there is an error


	class SoundError : public GeneralError {
	 public:
	 	SoundError() : GeneralError("Error playing sound") { }
	};
};


#endif /* SOUNDS_HPP */

