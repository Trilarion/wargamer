/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *	Implemenation of Array
 * Note that since this is templated, most of it is in the header file!
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "array.hpp"

ArrayBase::ArrayError::ArrayError(const char* s) : 
	GeneralError(s) 
{
}

ArrayBase::ArrayError::ArrayError(const char* s, int i) : 
	GeneralError(s, i) 
{
}


ArrayBase::ArrayOutOfRange::ArrayOutOfRange(int i) : 
	ArrayError("Array out of range (id=%d)", i) 
{
}


ArrayBase::ArrayNoMemory::ArrayNoMemory() :
	ArrayError("Can't allocate memory for Array") 
{
}

