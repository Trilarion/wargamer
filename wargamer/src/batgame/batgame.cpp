/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *      Battle Game
 *
 *----------------------------------------------------------------------
 */


#include "stdinc.hpp"
#include "batgame.hpp"
#include "refptr.hpp"
#include "batwind.hpp"
#include "batdata.hpp"
#include "bcreate.hpp"
#include "batlogic.hpp"
#include "batai.hpp"
#include "gthread.hpp"
#include "thread.hpp"
#include "btimctrl.hpp"
#include "sync.hpp"
#include "batmap.hpp"
#include "batdisp.hpp"
#include "wg_msg.hpp"
#include "msgbox.hpp"
#include "splash.hpp"
#include "control.hpp"
#include "batmsg.hpp"
// #include "string.hpp"
#include "filecnk.hpp"
#include "gamectrl.hpp"
#include "initunit.hpp"
#include "batinfo.hpp"
#include "bobutil.hpp"
#include "batowner.hpp"
#include "tresult.hpp"
#include "bobiter.hpp"
#include "b_send.hpp"
#include "overnight.hpp"
#include "d_select.hpp"
#include "wmisc.hpp"
#include "res_str.h"
#include "b_send.hpp"

#include "MultiplayerConnection.hpp"
#include "DirectPlay.hpp"
#include "MultiPlayerMsg.hpp"

#if !defined(NOLOG)
#include "clog.hpp"
LogFile bgLog("batgame.log");
#endif

using WG_BattleWindows::BattleWindows;
using WG_BattleWindows::BattleWindowUser;
using Greenius_System::Thread;

/*=================================================================
 * Battle Game Implementation
 */


class BattleGameImp :
    private Thread,
    private BattleGameInterface,
    private BattleWindowUser
{

      // Unimplemented copy

      BattleGameImp(const BattleGame&);
      BattleGameImp& operator =(const BattleGameImp&);

   public:

      BattleGameImp(BattleOwner* owner);
      ~BattleGameImp();

      /*
      * Initialisation methods
      */


      void init(BattleOB* ob, const BattleInfo& batInfo, BattleTime& theTime);
      // Start Campaign Battle

      void init(const char* fileName, SaveGame::FileFormat mode);
      // Start Historical battles

      void newDay();
      // Start the next day's battle

      void resume();
      // Continue after loading a saved game

      void startCampaignTime()
      {
         d_owner->startCampaignTime();
      }

      void stopCampaignTime()
      {
         d_owner->stopCampaignTime();
      }

      // Load from a combined Campaign Battle

      void saveBattle(bool promptName);
      void saveBattle(const char* fileName);
      // Save stand-alone battle game

      void readData(FileReader& file, OrderBattle* ob, bool mode);
      bool writeData(FileWriter& file, bool mode);

      // Used by Messages

      void battleOver(const BattleFinish& results);
      virtual void selectMapFinished();
      virtual void deployFinished();

      /*
      * Implement BattleGameInterface (Public)
      */

      BattleData* battleData()
      { return d_battleData;
      }
      const BattleData* battleData() const
      { return d_battleData;
      }
      void pauseGame(bool pauseMode);
      BattleOB* battleOB() const
      { return (d_battleData) ? d_battleData->ob() : 0;
      }
      void reinforced(const BattleInfo& batInfo);
      const BattleInfo* battleInfo() const { return &d_battleInfo; }

      void stopLogic();

     /*
     Multiplayer time-control
     */
     RWLock d_syncLock;

     bool d_slaveReady;
     bool d_lastSyncAcknowledged;
     ULONG d_lastSyncTime;
     ULONG d_slaveWantTicks;

     void syncSlave(SysTick::Value ticks);
     void timeSyncAcknowledged(SysTick::Value ticks);
     void slaveReady(void);

     void processBattleOrder(MP_MSG_BATTLEORDER * msg);


   private:
      virtual void run();
     void multiPlayerRun(void);

      void makeWindows();
      void removeWindows();

      void startMapSelect();
      void startDeploy(bool resuming = False);
      void startGame();
      void startResults();


      void startLogic();

      void restartLogic();
      void pauseLogic();

      /*
      * Functions called by messages
      */

      void resultsFinished(TacticalResults::BattleContinueMode mode);
      // const TacticalResults* battleResults() const
      const BattleResults* battleResults() const
      { return d_results;
      }
      bool isHistorical() const
      {
            return(d_battleData->type() == BattleData::Historical);
      }

      private:

      /*
      * Implement BattleWindowUser
      */

      BatTimeControl* timeControl()
      { return &d_timeControl;
      }
      const BatTimeControl* timeControl() const
      { return &d_timeControl;
      }
      void sendPlayerMessage(const BattleMessageInfo&);

      void requestBattleOver(const BattleFinish& results);
      void requestOpenBattle();
      void requestSaveBattle(bool prompt);

      void setController(Side side, GamePlayerControl::Controller control);

      /*
      * Functions used by BattleTimeThread
      */

      virtual BattleMeasure::BattleTime::Tick addTicks(int ticks);

      virtual bool isOver()
      {
         return d_isOver;
      }

      void setBattleStartTime();

      /*
      * Private Functions
      */

      void addAI(Side s);
      void removeAI(Side s);
      void loadBattle(const char* fileName, SaveGame::FileFormat mode);

   private:

      enum Phase {
         Phase_MapSelect,
         Phase_Deployment,
         Phase_Playing
      };

      /*
       * data
       */


      BattleData* d_battleData;
      BattleWindows* d_windows;

      BatTimeControl d_timeControl;

      BattleLogic*    d_logic;
      BattleAI*       d_ai;

      String d_fileName;
      String d_title;
      bool d_ownsOB;   // set if random OB was made
      BattleOwner* d_owner;

      BattleInfo d_battleInfo;
      TacticalResults* d_results;

      bool          d_isOver;
      Phase         d_phase;

      static UWORD s_fileVersion;
      static const char s_chunkName[];
};

/*--------------------------------------------------------------------------
 * BattleGameImp Functions
 */

BattleGameImp::BattleGameImp(BattleOwner* owner) :
    Thread(GameThreads::instance()),
    d_battleData(new BattleData()),
    d_windows(0),
    d_timeControl(),
    d_logic(0),
    d_ai(0),
    d_fileName(),
    d_title(),
    d_ownsOB(false),
    d_owner(owner),
    d_results(0),
    d_isOver(false),
    d_phase(Phase_MapSelect),
   d_slaveReady(false),
   d_lastSyncAcknowledged(true),
   d_lastSyncTime(0),
   d_slaveWantTicks(0) {

}


// static const char createWindTxt[] =    "Creating Battle Windows";
// static const char createBattleTxt[] =  "Creating Battlefield";
// static const char loadBattleTxt[] =    "Reading Battlefield";
// static const char deployTxt[] =        "Deploying Troops";
// static const char resizingTxt[] =      "Resizing Map! Reinforcements have arrived";
static ResString createWindTxt   (IDS_CreatingBattleWindows);
static ResString createBattleTxt (IDS_CreatingBattlefield);
static ResString loadBattleTxt   (IDS_ReadingBattlefield);
static ResString deployTxt       (IDS_DeployingTroops);
static ResString resizingTxt     (IDS_ResizingMapReinforcementsArrived);

/*
 *   Start from a Saved Game, Historical Battle or Random
 *
 *    Start-up course depends upon fileName.
 *    If a filename is supplied -
 *        BattleGame initializes as a historical battle
 *        File is loaded
 *        There is no deployment phase
 *        Battle commences
 *    If no filename is supplied -
 *        BattleGame initializes as a campaign battle
 *        Random battlefield is created (BattleInfo * battleinfo should be passed from campaign)
 *        Deployment phase occurs
 *        Battle commences
 */

void BattleGameImp::init(const char* fileName, SaveGame::FileFormat mode)
{
   HWND hParent = d_owner->hwnd();

   // Let the Campaign be responsible for setting window title
/*
   SetWindowText(hParent, "Wargamer - Napoleon 1813");
*/

   ShowWindow(hParent, SW_SHOW);   // force main window to be displayed!

   SplashScreen splash(hParent);
   splash.setText(createWindTxt.c_str());


   makeWindows();

   d_ownsOB = true;     // generic OB is ours to delete.

   if(fileName == 0)
   {

      d_title = "Random Battle";
      // BattleInfo battleInfo;

      splash.setText(createBattleTxt.c_str());
      BattleCreate::createRandomBattle(battleData(), d_battleInfo);

      splash.setText(deployTxt.c_str());
      startMapSelect();

   }
   else    // otherwise do a load game
   {
      d_title = fileName;

      splash.setText(loadBattleTxt.c_str());

      loadBattle(fileName, mode);
      splash.setText(createWindTxt.c_str());

     // start Vis. checks
//   if(!d_windows->getBattleMap()->isVisibilityRunning()) d_windows->getBattleMap()->startVisibilityChecks();
//      d_windows->getBattleMap()->getDisplay()->showHexVisibility(true);

     if(mode == SaveGame::Battle)
     {
       BattleMeasure::BattleTime::Tick bat_ticks = d_battleData->getTick();

       /*
       Jim's change to test the time
       */
#ifdef DEBUG_TIME
       {
          int days, hours, mins, secs;
          ticksToTime(bat_ticks, &days, &hours, &mins, &secs);
          bgLog.printf("Battle Start Time (ticks:%i, days:%i, hours:%i, mins:%i, secs:%i)\n", bat_ticks, days, hours, mins, secs);
       }
#endif
       /*
       bodge reinforcement / endtimes
       a bodge because the editor doesn't have its own preprocessor defines
       */
       B_ReinforceItem * item = d_battleData->ob()->reinforceList().first();
       while(item != NULL)
       {

          /*
          Jim's change to test the time
          */
#ifdef DEBUG_TIME
       {
          int days, hours, mins, secs;
          ticksToTime(item->d_time, &days, &hours, &mins, &secs);
          bgLog.printf("%s will arrive %i days, %i hours and %i minutes ater the start of battle, ",
             item->d_cp->getName(), days, hours, mins, secs);
       }
#endif

          /*
          Add on battle start-time
          */
          item->d_time += bat_ticks;

#ifdef DEBUG_TIME
       {
          int days, hours, mins, secs;
          ticksToTime(item->d_time, &days, &hours, &mins, &secs);
          bgLog.printf("on day %i, at %i:%i hrs.\n", days, hours, mins);
       }
#endif

          item = d_battleData->ob()->reinforceList().next();
       }

       d_battleData->addStartTimeToEndTime();

#ifdef DEBUG
        bgLog.printf("Game ends in %d hours", BattleMeasure::hoursPerTick(d_battleData->endGameIn() - d_battleData->getTick()));
#endif
     }

     startGame();
   }

//   SetWindowText(hParent, d_title.c_str());
   ShowWindow(hParent, SW_SHOW);   // force main window to be displayed!
}


void BattleGameImp::init(BattleOB* ob, const BattleInfo& batInfo, BattleTime& theTime)
{

   // Let the Campaign be responsible for setting window title
/*
   d_battleInfo = batInfo;
   d_title = batInfo.title();
   SetWindowText(d_owner->hwnd(), d_title.c_str());
*/
   SplashScreen splash(d_owner->hwnd());
   splash.setText(createBattleTxt.c_str());

   ASSERT(d_battleData->ob() == 0);
   d_battleData->ob(ob);

   d_battleData->setDateAndTime(theTime);
   BattleCreate::createBattleField(d_battleData, batInfo);

   splash.setText(createWindTxt.c_str());
   makeWindows();


#if !defined(NOLOG)
   bgLog << "STARTING BATTLEGAME\n";
#endif
   // if we don't have initiative, then crop centre area
   // and skip to deployment screen
#if 1
   if(batInfo.playerSide() != batInfo.initiativeSide())
   {
      d_phase = Phase_Deployment;
#if !defined(NOLOG)
      bgLog << "STARTING BATTLEGAME WITHOUT INITIATIVE - STRAIGHT TO DEPLOYMENT\n";
#endif

      splash.setText(deployTxt.c_str());

      HexCord size = HexCord(batInfo.battleWidth(), batInfo.battleHeight());
      HexCord bottomleft((batInfo.deploymentWidth()/2) - (batInfo.battleWidth()/2),
         (batInfo.deploymentHeight()/2) - (batInfo.battleHeight()/2));
      d_battleData->setPlayingArea(bottomleft, size);
      startDeploy();
   }
   // otherwise start selection phase as normal
   else
#endif
   {
#if !defined(NOLOG)
      bgLog << "STARTING BATTLEGAME WITH INITIATIVE - GOING TO SELECTION SCREEN\n";
#endif
      d_phase = Phase_MapSelect;
      startMapSelect();
   }
}


void BattleGameImp::reinforced(const BattleInfo& batInfo)
{
   // What do we do???
   // Stop logic for one thing
//   Boolean oldPause = GameControl::pause(True);

   /*
   * play drum-march to notify player
   */
   d_battleData->soundSystem()->triggerSound(SOUNDTYPE_DRUMS1);


   // see if we may need to resize map
   if(batInfo.battleWidth() != d_battleInfo.battleWidth() ||
      batInfo.battleHeight() != d_battleInfo.battleHeight())
   {
#ifdef DEBUG
      HexCord bLeft;
      HexCord mSize;
      d_battleData->getPlayingArea(&bLeft, &mSize);
      bgLog.printf("Resizing battlefield, reinforcements have arrived");
      bgLog.printf("------ Old bottom-left=(%d, %d), Old top-right=(%d, %d)",
         static_cast<int>(bLeft.x()), static_cast<int>(bLeft.y()),
         static_cast<int>(bLeft.x() + mSize.x()), static_cast<int>(bLeft.y() + mSize.y()));
      bgLog.printf("------ Old Size - w=%d, h=%d", d_battleInfo.battleWidth(), d_battleInfo.battleHeight());
      bgLog.printf("------ New Size - w=%d, h=%d", batInfo.battleWidth(), batInfo.battleHeight());
#endif

      {
         // resize map
#if 0
         SplashScreen splash(d_owner->hwnd());
         splash.setText(resizingTxt);
#endif
         HexCord newSize(batInfo.battleWidth(), batInfo.battleHeight());
         d_battleData->expandPlayingArea(newSize);

#ifdef DEBUG
         d_battleData->getPlayingArea(&bLeft, &mSize);
         bgLog.printf("------ Old bottom-left=(%d, %d), Old top-right=(%d, %d)",
            static_cast<int>(bLeft.x()), static_cast<int>(bLeft.y()),
            static_cast<int>(bLeft.x() + mSize.x()), static_cast<int>(bLeft.y() + mSize.y()));
#endif
      }

      d_battleInfo.battleWidth(batInfo.battleWidth());
      d_battleInfo.battleHeight(batInfo.battleHeight());

   }

   if(d_logic)
      d_logic->reinforced();

   // restart logic
//   GameControl::pause(oldPause);

}

BattleGameImp::~BattleGameImp()
{
   stopLogic();

   // Most things are destroyed automatically with RefPtr
   // but seeing as there as several cross-references, it is
   // important to delete things in the correct order
   // e.g. BattleWindows contains a pointer to the BattleData

   if(d_ai)
      delete d_ai;
   d_ai = 0;

   if(d_logic)
      delete d_logic;
   d_logic = 0;

   if(d_windows)
      delete d_windows;
   d_windows = 0;

   delete d_results;
   d_results = 0;

   if(d_ownsOB)
      BattleCreate::deleteRandomBattle(battleData());

   if(d_battleData)
      delete d_battleData;
   d_battleData = 0;

}

void BattleGameImp::makeWindows()
{
   ASSERT(d_windows == 0);
   ASSERT(d_battleData != 0);
   if(d_windows == 0)
   {
      d_windows = new BattleWindows(d_owner->hwnd(), this);
   }
}

void BattleGameImp::removeWindows()
{
   delete d_windows;
   d_windows = 0;
}


void BattleGameImp::startMapSelect()
{
   d_windows->setMapSelectMode(d_battleInfo);
}

void BattleGameImp::startDeploy(bool resuming)
{
   // Do initial deployment and setup units

   if(!resuming)
   {
      BobUtility::initUnitTypeFlags(d_battleData);
      for(Side s = 0; s < scenario->getNumSides(); ++s)
      {
         addAI(s);
         d_ai->doDeploy(s);
         if(GamePlayerControl::getControl(s) != GamePlayerControl::AI)
            removeAI(s);
      }
      BatUnitUtils::InitUnits(d_battleData);
   }


   d_windows->setDeployMode(d_battleInfo);
}

/*
 * Set the battle time
 *
 * If after EndOfDay then increment day and set to StartOfDay
 * else if before StartOfDay then set to StartOfDay
 */

void BattleGameImp::setBattleStartTime()
{
   BattleMeasure::BattleTime theTime = d_battleData->getDateAndTime();
   theTime.startNewDay();
   d_battleData->setDateAndTime(theTime);
}

void BattleGameImp::startGame()
{
   d_phase = Phase_Playing;
   d_windows->setGameMode();
   startLogic();
}

void BattleGameImp::startResults()
{
   d_windows->showResults(this);
}

void BattleGameImp::startLogic()
{
   ASSERT(d_windows != 0);
   ASSERT(d_battleData != 0);
   ASSERT(d_battleData->hexSize().x() != 0);
   ASSERT(d_battleData->hexSize().y() != 0);

   ASSERT(d_logic == 0);

   d_isOver = false;

//   if(d_battleData->type() == BattleData::Historical)
//      d_battleData->addStartTimeToEndTime();

   d_logic = new BattleLogic(this);
   d_logic->start();

   start();

   BattleSideIter niter(d_battleData->ob());
   while(++niter)
   {
      Side s = niter.current();
      if(GamePlayerControl::getControl(s) == GamePlayerControl::AI)
      {
         addAI(s);
         ASSERT(d_ai);
      }
   }

   if(d_ai)
      d_ai->start();

   startCampaignTime();
}

void BattleGameImp::stopLogic()
{
   stopCampaignTime();

   GameControl::Pauser pause;    // pauses game until destructed
//   Boolean oldPause = GameControl::pause(True);

   if(d_ai)
   {

      d_ai->kill();
      delete d_ai;
      d_ai = 0;

   }

   stop();

   clearBattleDespatch();

   delete d_logic;
   d_logic = 0;

   // GameControl::pause(oldPause);
}

/*
 * Restart Logic for new day
 */

void BattleGameImp::restartLogic()
{
   if(d_ai)
      d_ai->newDay();

   d_isOver = false;
   // d_logic = new BattleLogic(this);
   // d_logic->start();

   pause(false);

   startCampaignTime();
}

/*
 * Pause the Game for the end of day
 */

void BattleGameImp::pauseLogic()
{
   GameControl::Pauser pauser;    // pauses game until destructed
   // Boolean oldPause = GameControl::pause(True);

   stopCampaignTime();
   pause(true);
   clearBattleDespatch();     // Clear out any orders in transit
   // delete d_logic;
   // d_logic = 0;

   if(d_ai)
      d_ai->endDay();

   // GameControl::pause(oldPause);
}


/*
 * resume from a loaded saved game
 */

void BattleGameImp::resume()
{
   ASSERT(d_owner != 0);
   ASSERT(d_battleData != 0);
   ASSERT(d_windows == 0);
   ASSERT(d_logic == 0);

   makeWindows();
   switch(d_phase)
   {
      case Phase_MapSelect:
         startMapSelect();
         break;

      case Phase_Deployment:
         startDeploy(True);
         break;

      case Phase_Playing:
         startGame();
         break;

#ifdef DEBUG
      default:
         FORCEASSERT("Invalide phase in resume()");
#endif
   }
}


void BattleGameImp::pauseGame(bool pauseMode)
{
   GameThreads::pause(pauseMode);
}

/*
 * Battle is over
 * This is always called from the user interface thread
 */

void BattleGameImp::battleOver(const BattleFinish& finish)
{
   ASSERT(d_isOver);

   /*
   * Kill the logic and AI and reset the despatcher
   */

   // stopLogic();
   pauseLogic();

   /*
   * Calculates results
   */

   d_results = new TacticalResults(finish);
   d_results->setTitle(d_title);
   d_results->calculate(d_battleData);


   /*
   If this is the end of battle, play the appropriate piece of music
   */
   if(!d_results->willContinue()) {

      VictoryLevel victoryLevel = d_results->level();
      Side victorySide = d_results->winner();
      Side playerSide = GamePlayerControl::getSide(GamePlayerControl::Player);
      int track_no = 0;

      /*
      Draw
      */
      if(victoryLevel == BattleResultTypes::Draw) {
         track_no = 5;
      }

      else {
         /*
         Player is French
         */
         if(playerSide == 0) {
            // French victory
            if(victorySide == 0) track_no = 9;
            // French defeat
            else track_no = 8;
         }
         /*
         Player is Allies
         */
         else {
            // Allied victory
            if(victorySide == 1) track_no = 7;
            // Allied defeat
            else track_no = 6;
         }
      }

      ASSERT(track_no != 0);
      int num = 1;
      GlobalSoundSystemObj.StopCDPlaying();
      GlobalSoundSystemObj.SetPlayList(num, track_no+1);
      GlobalSoundSystemObj.StartPlayList(true);
   }

   /*
   * Show results and ask player if he wants to continue for another day
   */

   startResults();
}

void BattleGameImp::resultsFinished(TacticalResults::BattleContinueMode mode)
{
   d_results->shouldContinue(mode);

   /*
   * Also ask AI side if he wants to continue
   * and set shouldContinue mode appropriately
   */

   //-- Todo

   removeWindows();

   /*
   * Inform Campaign game: needs to know if battle is over
   * and what to do to each side (withdraw, etc).
   */

   if(d_owner->tacticalOver(*d_results) && (d_results->willContinue() == BattleResults::Continue))
   {
      newDay();
   }

   /*
   * If results->shouldContinue() then the owner should either return
   * true or call newDay() when it is ready.
   */

   else {
//    pause(false);
//    startCampaignTime();
   }


}

/*
 * Restart Battle for a new day
 */

void BattleGameImp::newDay()
{
   // Do overnight recovery / cleanup
#ifdef DEBUG
   bgLog.printf("Starting new day before new start time");
#endif

   // bodge for reinforcement times
   B_ReinforceItem * item = d_battleData->ob()->reinforceList().first();
   while(item != NULL)
   {
#ifdef DEBUG
      bgLog.printf("%s will arrive in %d hours", item->d_cp->getName(), BattleMeasure::hoursPerTick(item->d_time - d_battleData->getTick()));
#endif
      item->d_time = maximum(0, (item->d_time - (d_battleData->getTick() + BattleMeasure::hours(13))));
      item = d_battleData->ob()->reinforceList().next();
   }

   // bodge for battleend time
   if(d_battleData->type() == BattleData::Historical)
   {
      BattleMeasure::BattleTime::Tick endGameIn = d_battleData->endGameIn();
#ifdef DEBUG
      bgLog.printf("Game ends in %d hours", BattleMeasure::hoursPerTick(endGameIn - d_battleData->getTick()));
#endif
      endGameIn = maximum(0, (endGameIn - d_battleData->getTick()));
      d_battleData->endGameIn(endGameIn);
   }

   setBattleStartTime();

#ifdef DEBUG
   bgLog.printf("Starting new day after new start time");
#endif
   // rebodge for reinforcement times
   item = d_battleData->ob()->reinforceList().first();
   while(item != NULL)
   {
      item->d_time += d_battleData->getTick();
#ifdef DEBUG
      bgLog.printf("%s will arrive in %d hours", item->d_cp->getName(), BattleMeasure::hoursPerTick(item->d_time - d_battleData->getTick()));
#endif
      item = d_battleData->ob()->reinforceList().next();
   }

   if(d_battleData->type() == BattleData::Historical)
   {
      d_battleData->addStartTimeToEndTime();
#ifdef DEBUG
      bgLog.printf("Game ends in %d hours", BattleMeasure::hoursPerTick(d_battleData->endGameIn() - d_battleData->getTick()));
#endif
   }

   B_Logic::overnightRecovery(d_battleData);
   d_logic->newDay(); // reset time check
   // Update the time to be 7am the next morning


   // recreate windows
   // do deployment, restart logic and ai

   makeWindows();
//   startDeploy();
   // startGame();
   d_windows->setGameMode();
   restartLogic();
}

 void BattleGameImp::selectMapFinished()
{
   startDeploy();
}

 void BattleGameImp::deployFinished()
{
   startGame();
}


void BattleGameImp::sendPlayerMessage(const BattleMessageInfo& msg)
{
   switch(GamePlayerControl::getControl(msg.side()))
   {
   case GamePlayerControl::Player:
      {
         if(d_windows)
            d_windows->sendPlayerMessage(msg);
         break;
      }

   case GamePlayerControl::AI:
      {
         if(d_ai)
            d_ai->sendMessage(msg);
         break;
      }
   }
}

void BattleGameImp::setController(Side side, GamePlayerControl::Controller control)
{
   GamePlayerControl::Controller oldControl = GamePlayerControl::getControl(side);

   if(oldControl != control)
   {
      if(oldControl == GamePlayerControl::AI)
         removeAI(side);

      if(control == GamePlayerControl::AI)
      {
         addAI(side);
      }

      GamePlayerControl::setControl(side, control);
   }
}

void BattleGameImp::addAI(Side s)
{
   if(!d_ai)
      d_ai = new BattleAI(d_battleData, d_title);
   d_ai->add(s);
}

void BattleGameImp::removeAI(Side s)
{
   ASSERT(d_ai);
   d_ai->remove(s);
}

/*
 * Load and save game
 */

UWORD BattleGameImp::s_fileVersion = 0x0006;
const char BattleGameImp::s_chunkName[] = "BattleGame";

void BattleGameImp::loadBattle(const char* fileName, SaveGame::FileFormat mode)
{
   ASSERT(fileName != 0);
   d_fileName = fileName;

   FileWithMode<WinFileBinaryChunkReader> file(fileName, mode);

   // Read BattleData: load GenericOB and initialise deployment map

   readData(file, 0, mode == SaveGame::Battle);

   d_battleData->ob()->removeOrphans();
}

void BattleGameImp::readData(FileReader& file, OrderBattle* ob, bool mode)
{

   d_battleData->readData(file, ob, mode);
   {
      FileChunkReader fc(file, s_chunkName);
      if(fc.isOK())
      {
         UWORD version;
         file >> version;
         ASSERT(version <= s_fileVersion);


         if(!mode)
         {
            UBYTE b = 1;

            if(version >= 0x0006)
               file >> b;

            if(b)
            {
               ASSERT(!d_ai);
               d_ai = new BattleAI(d_battleData);
               ASSERT(d_ai);
               if(version >= 0x0002)
                  d_ai->readData(file);
            }

            if(version >= 0x0003)
            {
               int value;
               file >> value;
               d_battleInfo.battleWidth(value);
               file >> value;
               d_battleInfo.battleHeight(value);
            }

            if(version >= 0x0004)
            {
               UBYTE b;
               file >> b;
               d_phase = static_cast<Phase>(b);
            }

            if(version >= 0x0005)
            {
               GamePlayerControl::readData(file);
            }
         }
      }
      else
      {
         if(!mode)
         {
            ASSERT(!d_ai);
            d_ai = new BattleAI(d_battleData);
         }
      }
   }

}

void BattleGameImp::saveBattle(const char* fileName)
{
   GameControl::Pauser pause;    // pauses game until destructed

   /*
   If we're the Master in a multiplayer game, then send the SAVEGAME prompt to the Slave
   */
   if(CMultiPlayerConnection::connectionType() == ConnectionMaster) {

      MP_MSG_SAVEGAME savegame_msg;
      savegame_msg.msg_type = MP_MSG_SaveGame;

      // copy the filename - without the path
      char * fname = const_cast<char *>(fileName);
      char * strptr = &fname[strlen(fname)-1];
      while(*strptr != '\\') strptr--;
      strptr++;
      // add "SavedGames\" path
      strcpy(savegame_msg.savefilename, "SavedGames\\");
      // add filename
      strcat(savegame_msg.savefilename, strptr);

      if(g_directPlay) {

         g_directPlay->sendMessage(
            (LPVOID) &savegame_msg,
            sizeof(MP_MSG_SAVEGAME)
         );
      }
   }


   FileWithMode<WinFileBinaryChunkWriter> file(fileName, SaveGame::SaveGame);
   FileTypeChunk ftype(SaveGame::Battle);
   ftype.writeData(file);
   writeData(file, true);

//   GameControl::pause(oldPause);
}

void BattleGameImp::saveBattle(bool promptName)
{
   GameControl::Pauser pause;    // pauses game until destructed
//   Boolean oldPause = GameControl::pause(True);

   SimpleString fileName;

   if(promptName || (d_fileName.length() == 0))
   {
      if(SaveGame::askSaveName(fileName, SaveGame::SaveGame))
      {
         d_fileName = fileName.toStr();
      }
      else
      {
//         GameControl::pause(oldPause);
         return;
      }
   }

   saveBattle(d_fileName.c_str());

//   GameControl::pause(oldPause);
}

bool BattleGameImp::writeData(FileWriter& file, bool mode)
{
   if(!d_battleData->writeData(file, mode))
      return False;
   {
      FileChunkWriter fc(file, s_chunkName);

      file << s_fileVersion;

      UBYTE b = (d_ai != 0);
      file << b;
      if(d_ai)
      {
         if(!d_ai->writeData(file))
            return False;
      }

      int value = d_battleInfo.battleWidth();
      file << value;
      value = d_battleInfo.battleHeight();
      file << value;

      b = static_cast<UBYTE>(d_phase);
      file << b;

      GamePlayerControl::writeData(file);
   }

   return True;
}


/*
 * Functions used by BattleTimeThread
 */


BattleMeasure::BattleTime::Tick BattleGameImp::addTicks(int ticks)
{
   // Bodge, so display knows ratio for animation effects

   d_battleData->timeRatio(d_timeControl.ratio());

   if(d_isOver)
      return 0;
   else
      return d_timeControl.addTicks(ticks);
}


 void BattleGameImp::run()
{
   /*
   If we are running a multiplayer game
   Then use a separate time-control loop
   */
   if(CMultiPlayerConnection::connectionType() != ConnectionNone) {
      multiPlayerRun();
      return;
   }

   Greenius_System::WaitableCounter timer(1000/SysTick::TicksPerSecond, true);

   // while(!d_isOver && (wait(timer) != TWV_Quit))
   while(wait(timer) != TWV_Quit)
   {
      if(!d_isOver)
      {
         int ticks = timer.getAndClear();

         /*
          * Convert SysTick into BattleTick using the current
          * realTime to GameTime ratio.
          * Tell Logic how much time has passed.
          */

   #if !defined(NOLOG)
         static int count = 0;
         bgLog << ">addTick " << count << ", ticks=" << ticks << ", " << sysTime << endl;
   #endif

         BattleMeasure::BattleTime::Tick battleTicks = addTicks(ticks);

         if((battleTicks != 0) && (d_logic != 0))
         {
            d_logic->updateTime(battleTicks);
            if(d_windows != 0) d_windows->timeChanged();
          /*
          Process SFX
          Pass in HEX position & ZOOM level (this must be between [0...3])
          */
            if(d_battleData->soundSystem())
            {
               d_battleData->soundSystem()->processSound(
                  d_battleData->getHex(d_windows->getBattleMap()->getDisplay()->getCentre(), BattleData::Mid),
                  static_cast<BattleSoundZoomEnum>(d_windows->getBattleMap()->mode())
               );
            }
         }

   #if !defined(NOLOG)
         bgLog << "<addTick " << count << ", battleTicks=" << battleTicks << ", " << sysTime << endl;
         ++count;
   #endif
      }
   }

   timer.stop();
}





void BattleGameImp::multiPlayerRun(void) {

#ifdef DEBUG
   if(CMultiPlayerConnection::connectionType() == ConnectionMaster) {
      bgLog.printf(">> Starting BattleGameImp multiPlayerRun() loop for Master machine\n\n");
      bgLog.printf(">> maxSyncInterval = %i ticks\n\n", BattleDespatcher::instance()->maxSyncInterval);
   }
   else if(CMultiPlayerConnection::connectionType() == ConnectionSlave) {
      bgLog.printf(">> Starting BattleGameImp multiPlayerRun() loop for Slave machine\n\n");
   }
   ULONG counter=0;
#endif



   // this needn't be waitable...
   Greenius_System::WaitableCounter timer(1000/SysTick::TicksPerSecond, true);
   ULONG ticks_so_far = 0;

   // if we're the Slave, we need to tell the Master that we've loaded up Ok
   // and are ready to start advancing time
   if(CMultiPlayerConnection::connectionType() == ConnectionSlave) {
      // wait until logic is fired up
      while(!d_logic);
      // send ready msg
      d_owner->sendSlaveReady();
   }


   while(!d_isOver) {

      // advances time in accordance with time-ratio slider
      BattleMeasure::BattleTime::Tick logicTicks = 0;

      /*
      Master syncing
      */
      if(GameControl::isPaused()) timer.getAndClear();

      if(CMultiPlayerConnection::connectionType() == ConnectionMaster && (d_logic != 0) && (d_slaveReady) && (!GameControl::isPaused()) ) {

         ULONG ticks = timer.getAndClear();
         ticks_so_far += ticks;

         // The sync-time must never exceed 'd_maxSyncInterval'
         // This is
         // a) to stop the game 'running away' is the host machine stalls
         // b) to ensure that all orders send during the current 'turn' are processed in the next turn
         // (d_maxSyncInterval is added to the activation time of all despatch messages)

         // NOTE : 'd_maxSyncInterval' could itself change dynamically dependent upon connection speed

         if(ticks_so_far > BattleDespatcher::instance()->maxSyncInterval ) ticks_so_far = BattleDespatcher::instance()->maxSyncInterval;

         if(ticks_so_far) {

            d_syncLock.startRead();
            bool ackn = d_lastSyncAcknowledged;
            d_syncLock.endRead();

            if(ackn) {

               logicTicks = addTicks(ticks_so_far);

               d_syncLock.startWrite();
               d_lastSyncAcknowledged = false;
               d_syncLock.endWrite();

               #ifdef DEBUG
               bgLog.printf("[sending sync command to run for %i ticks]\n", logicTicks);
               #endif

               d_owner->sendBattleTimeSync(logicTicks, 0);
            }

            ticks_so_far = 0;
         }
      }
      /*
      Slave Syncing [NOTE : we don't go through the 'addTicks()' function here
      */
      else if(d_logic != 0) {

         d_syncLock.startRead();

         if(d_slaveWantTicks != 0) {

            logicTicks = d_slaveWantTicks;

            d_slaveWantTicks = 0;
         }

         d_syncLock.endRead();
      }

      /*
      If time has moved on, then update logic's time & process SFX & windows
      */
      if((logicTicks != 0) && (d_logic != 0)) {

#ifdef DEBUG
         bgLog.printf("%i> running logic for %i ticks\n", counter, logicTicks);
         counter++;
#endif

         d_logic->updateTime(logicTicks);
            if(d_windows != 0) d_windows->timeChanged();
          /*
          Process SFX - Pass in HEX position & ZOOM level (this must be between [0...3])
          */
            if(d_battleData->soundSystem())
            {
               d_battleData->soundSystem()->processSound(
                 d_battleData->getHex(d_windows->getBattleMap()->getDisplay()->getCentre(), BattleData::Mid),
                  static_cast<BattleSoundZoomEnum>(d_windows->getBattleMap()->mode())
               );
            }
         }

   }

   timer.stop();
}


void BattleGameImp::syncSlave(SysTick::Value ticks) {

   /*
   Block this receiving thread if
   a) d_logic hasn't been created yet
   b) d_slaveWantTicks hasn't been run yet
   */
   while(!d_logic || (d_slaveWantTicks != 0));

   d_syncLock.startWrite();
   d_slaveWantTicks = ticks;
   d_syncLock.endWrite();

#ifdef DEBUG
   bgLog.printf("[received sync command to run for %i ticks]\n", ticks);
#endif
}

void BattleGameImp::timeSyncAcknowledged(SysTick::Value ticks) {

   d_syncLock.startWrite();
   d_lastSyncAcknowledged = true;
   d_syncLock.endWrite();
}


void BattleGameImp::slaveReady(void) {

   d_slaveReady = true;

}



void BattleGameImp::processBattleOrder(MP_MSG_BATTLEORDER * msg)
{
   DS_BattleUnitOrder * order = new DS_BattleUnitOrder;
   order->unpack(msg->data, d_battleData);
   slaveSendBattleOrder(order, msg->activation_time);
}

/*
 * Request functions called from Game Logic or wherever
 *
 * They work by sending a message to the Main Window, which is
 * then processed by the User Interface Thread.
 */


class BattleOverMessage : public WargameMessage::MessageBase
{
   public:
   static void send(BattleGameImp* batGame, BattleFinish results)
   {
      WargameMessage::postMessage(new BattleOverMessage(batGame, results));
   }

   private:
   BattleOverMessage(BattleGameImp* batGame, const BattleFinish& results) :
   d_batGame(batGame),
   d_results(results)
   {
   }

   void run()
   { d_batGame->battleOver(d_results);
   }
   void clear()
   { delete this;
   }

   private:
   BattleGameImp* d_batGame;
   BattleFinish d_results;
};

void BattleGameImp::requestBattleOver(const BattleFinish& results)
{
   ASSERT(!d_isOver);
   d_isOver = true;

   BattleOverMessage::send(this, results);
}


void BattleGameImp::requestOpenBattle()
{
   WargameMessage::postLoadGame();
}

void BattleGameImp::requestSaveBattle(bool prompt)
{
   d_owner->requestSaveGame(prompt);
}

/*==========================================================================
 * Class Interface
 */

BattleGame::BattleGame(BattleOwner* owner, const char* fileName, SaveGame::FileFormat mode)
{
   d_imp = new BattleGameImp(owner);
   d_imp->init(fileName, mode);
}

BattleGame::BattleGame(BattleOwner* owner, BattleOB* ob, const BattleInfo& batInfo, BattleTime& theTime)
{
   d_imp = new BattleGameImp(owner);
   d_imp->init(ob, batInfo, theTime);
}

BattleGame::BattleGame(BattleOwner* owner, FileReader& f, OrderBattle* ob)
{
   d_imp = new BattleGameImp(owner);
   d_imp->readData(f, ob, false);
}


BattleGame::~BattleGame()
{
   delete d_imp;
}

BattleData* BattleGame::battleData()
{
   ASSERT(d_imp != 0);
   return d_imp->battleData();
}

const BattleData* BattleGame::battleData() const
{
   ASSERT(d_imp != 0);
   return d_imp->battleData();
}

/*
 * Send message to BattleGame so it knows about reinforcements
 * arriving.
 */

void BattleGame::reinforced(const BattleInfo& batInfo)
{
#if 0
   class ReinforceMessage : public WargameMessage::MessageBase
   {
      public:
         ReinforceMessage(BattleGameImp* batGame, const BattleInfo& batInfo) :
            d_batGame(batGame),
            d_batInfo(batInfo)
         {
         }

         void run()
         {
            d_batGame->reinforced(d_batInfo);
         }

      private:
         BattleGameImp* d_batGame;
         BattleInfo d_batInfo;
   };
#endif
   ASSERT(d_imp);
   d_imp->reinforced(batInfo);

//   WargameMessage::postMessage(new ReinforceMessage(d_imp, batInfo));

}

BattleOB* BattleGame::battleOB() const
{
   ASSERT(d_imp);
   return d_imp->battleOB();
}

const BattleInfo* BattleGame::battleInfo() const
{
   ASSERT(d_imp);
   return d_imp->battleInfo();
}


void BattleGame::pauseGame(bool pauseMode)
{
   ASSERT(d_imp != 0);
   d_imp->pauseGame(pauseMode);
}

void BattleGame::addTime(SysTick::Value ticks)
{
   ASSERT(d_imp != 0);
#if 0
      d_imp->addTime(ticks);
#endif
}

// void BattleGame::makeWindows()
// {
//         d_imp->makeWindows();
// }
//
// void BattleGame::removeWindows()
// {
//         d_imp->removeWindows();
// }

// void BattleGame::startDeployment(HWND hMain, BattleInfo * battle_info)
// {
//     d_imp->startDeployment(hMain, battle_info);
// }
//
// void BattleGame::start()
// {
//                 d_imp->startLogic();
// }
//
// void BattleGame::stop()
// {
//                 d_imp->stopLogic();
//                 d_imp->removeWindows();
// }

 bool BattleGame::saveGame(bool prompt) const
{
   try
   {
      d_imp->saveBattle(prompt);
      return true;
   }
   catch(...)
   {
      return false;
   }
}

 void BattleGame::saveGame(const char* fileName) const
{
   d_imp->saveBattle(fileName);
}

 void BattleGame::doNewGame(const char * current, SaveGame::FileFormat format) {
   // do nothing

}

 void BattleGame::syncSlave(SysTick::Value ticks) {
   // In Campaign-Battles time will be handled through the CampaignGame
   // In standalone battles, this syncing will be used
   d_imp->syncSlave(ticks);

}

 void BattleGame::timeSyncAcknowledged(SysTick::Value ticks) {
   // In Campaign-Battles time will be handled through the CampaignGame
   // In standalone battles, this syncing will be used
   d_imp->timeSyncAcknowledged(ticks);

}

 void BattleGame::slaveReady(void) {
   d_imp->slaveReady();
}


 void BattleGame::processBattleOrder(MP_MSG_BATTLEORDER * msg)
{
   d_imp->processBattleOrder(msg);
}

 void BattleGame::processCampaignOrder(MP_MSG_CAMPAIGNORDER *msg)
{
   FORCEASSERT("Multiplayer Campaign Order passed to BattleGame!!!");
}


bool BattleGame::writeData(FileWriter& file, bool mode)
{
   return d_imp->writeData(file, mode);
}


void BattleGame::resume()
{
   d_imp->resume();
}

void BattleGame::newDay()
{
   d_imp->newDay();
}

void BattleGame::stop()
{
   d_imp->stopLogic();
}






/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.5  2003/02/15 14:42:17  greenius
 * Fixed a possible deadlock situation when starting a thread.
 * Tidied up source code, and removed using namespace from header file.
 *
 * Revision 1.4  2002/11/16 18:03:29  greenius
 * Various changes so that it will compile with Visual Studio .net
 *
 * Revision 1.3  2001/11/26 10:34:41  greenius
 * Current Directory detection improved
 *
 * Battlegame sound system initialised properly
 *
 * BattleAI Message queue problems fixed
 *
 * Order of Battles loading
 *
 * HexMap uses reference counted units
 *
 * BattleEditor open-file dialogs use wrapped classes
 *
 * PathFind compiler warnings removed
 *
 * Orphan detection in BattleOB detected and removed
 *
 * Revision 1.2  2001/06/18 08:25:43  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
