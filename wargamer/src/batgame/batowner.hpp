/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef BATOWNER_HPP
#define BATOWNER_HPP

#ifndef __cplusplus
#error batowner.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 * Battle Controller
 *
 *----------------------------------------------------------------------
 */

#include "gamebase.hpp"

class BattleGame;
class TacticalResults;

class BattleOwner : public virtual GameOwner
{
    public:
        virtual ~BattleOwner() = 0;
        virtual HWND hwnd() const = 0;
        virtual void requestSaveGame(bool prompt) = 0;
		  virtual bool tacticalOver(const TacticalResults& results) = 0;
		  virtual void stopCampaignTime() = 0;
        virtual void startCampaignTime() = 0;
            // return true if should immediately start new day

		virtual void sendBattleTimeSync(SysTick::Value ticks, unsigned int checksum) = 0;
};

inline BattleOwner::~BattleOwner() 
{
}


#endif /* BATOWNER_HPP */

