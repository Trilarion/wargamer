# Microsoft Developer Studio Generated NMAKE File, Based on batgame.dsp
!IF "$(CFG)" == ""
CFG=batgame - Win32 Debug
!MESSAGE No configuration specified. Defaulting to batgame - Win32 Debug.
!ENDIF 

!IF "$(CFG)" != "batgame - Win32 Release" && "$(CFG)" != "batgame - Win32 Debug"
!MESSAGE Invalid configuration "$(CFG)" specified.
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "batgame.mak" CFG="batgame - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "batgame - Win32 Release" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE "batgame - Win32 Debug" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE 
!ERROR An invalid configuration is specified.
!ENDIF 

!IF "$(OS)" == "Windows_NT"
NULL=
!ELSE 
NULL=nul
!ENDIF 

CPP=cl.exe
MTL=midl.exe
RSC=rc.exe

!IF  "$(CFG)" == "batgame - Win32 Release"

OUTDIR=.\..\..\exe
INTDIR=.\o\release
# Begin Custom Macros
OutDir=.\..\..\exe
# End Custom Macros

!IF "$(RECURSE)" == "0" 

ALL : "$(OUTDIR)\batgame.dll"

!ELSE 

ALL : "batdisp - Win32 Release" "batwind - Win32 Release" "batlogic - Win32 Release" "batai - Win32 Release" "system - Win32 Release" "ob - Win32 Release" "gwind - Win32 Release" "gamesup - Win32 Release" "batdata - Win32 Release" "$(OUTDIR)\batgame.dll"

!ENDIF 

!IF "$(RECURSE)" == "1" 
CLEAN :"batdata - Win32 ReleaseCLEAN" "gamesup - Win32 ReleaseCLEAN" "gwind - Win32 ReleaseCLEAN" "ob - Win32 ReleaseCLEAN" "system - Win32 ReleaseCLEAN" "batai - Win32 ReleaseCLEAN" "batlogic - Win32 ReleaseCLEAN" "batwind - Win32 ReleaseCLEAN" "batdisp - Win32 ReleaseCLEAN" 
!ELSE 
CLEAN :
!ENDIF 
	-@erase "$(INTDIR)\batgame.obj"
	-@erase "$(INTDIR)\vc60.idb"
	-@erase "$(INTDIR)\vc60.pdb"
	-@erase "$(OUTDIR)\batgame.dll"
	-@erase "$(OUTDIR)\batgame.exp"
	-@erase "$(OUTDIR)\batgame.lib"
	-@erase "$(OUTDIR)\batgame.pdb"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

"$(INTDIR)" :
    if not exist "$(INTDIR)/$(NULL)" mkdir "$(INTDIR)"

CPP_PROJ=/nologo /MD /W3 /Gi /GR /GX /Zi /O2 /I "..\h" /I "..\..\res" /I "..\system" /I "..\gamesup" /I "..\ob" /I "..\batdata" /I "..\batwind" /I "..\batdisp" /I "..\batlogic" /I "..\batai" /D "NDEBUG" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "EXPORT_BATGAME_DLL" /Fp"$(INTDIR)\batgame.pch" /YX"stdinc.hpp" /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /c 
MTL_PROJ=/nologo /D "NDEBUG" /mktyplib203 /win32 
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\batgame.bsc" 
BSC32_SBRS= \
	
LINK32=link.exe
LINK32_FLAGS=User32.lib /nologo /dll /incremental:no /pdb:"$(OUTDIR)\batgame.pdb" /debug /machine:I386 /out:"$(OUTDIR)\batgame.dll" /implib:"$(OUTDIR)\batgame.lib" 
LINK32_OBJS= \
	"$(INTDIR)\batgame.obj" \
	"$(OUTDIR)\batdata.lib" \
	"$(OUTDIR)\gamesup.lib" \
	"$(OUTDIR)\gwind.lib" \
	"$(OUTDIR)\ob.lib" \
	"$(OUTDIR)\system.lib" \
	"$(OUTDIR)\batai.lib" \
	"$(OUTDIR)\batlogic.lib" \
	"$(OUTDIR)\batwind.lib" \
	"$(OUTDIR)\batdisp.lib"

"$(OUTDIR)\batgame.dll" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

!ELSEIF  "$(CFG)" == "batgame - Win32 Debug"

OUTDIR=.\..\..\exe
INTDIR=.\o\debug
# Begin Custom Macros
OutDir=.\..\..\exe
# End Custom Macros

!IF "$(RECURSE)" == "0" 

ALL : "$(OUTDIR)\batgameDB.dll"

!ELSE 

ALL : "batdisp - Win32 Debug" "batwind - Win32 Debug" "batlogic - Win32 Debug" "batai - Win32 Debug" "system - Win32 Debug" "ob - Win32 Debug" "gwind - Win32 Debug" "gamesup - Win32 Debug" "batdata - Win32 Debug" "$(OUTDIR)\batgameDB.dll"

!ENDIF 

!IF "$(RECURSE)" == "1" 
CLEAN :"batdata - Win32 DebugCLEAN" "gamesup - Win32 DebugCLEAN" "gwind - Win32 DebugCLEAN" "ob - Win32 DebugCLEAN" "system - Win32 DebugCLEAN" "batai - Win32 DebugCLEAN" "batlogic - Win32 DebugCLEAN" "batwind - Win32 DebugCLEAN" "batdisp - Win32 DebugCLEAN" 
!ELSE 
CLEAN :
!ENDIF 
	-@erase "$(INTDIR)\batgame.obj"
	-@erase "$(INTDIR)\vc60.idb"
	-@erase "$(INTDIR)\vc60.pdb"
	-@erase "$(OUTDIR)\batgameDB.dll"
	-@erase "$(OUTDIR)\batgameDB.exp"
	-@erase "$(OUTDIR)\batgameDB.ilk"
	-@erase "$(OUTDIR)\batgameDB.lib"
	-@erase "$(OUTDIR)\batgameDB.pdb"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

"$(INTDIR)" :
    if not exist "$(INTDIR)/$(NULL)" mkdir "$(INTDIR)"

CPP_PROJ=/nologo /MDd /W3 /Gm /Gi /GR /GX /ZI /Od /I "..\batai" /I "..\h" /I "..\..\res" /I "..\system" /I "..\gamesup" /I "..\ob" /I "..\batdata" /I "..\batwind" /I "..\batdisp" /I "..\batlogic" /D "_DEBUG" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "EXPORT_BATGAME_DLL" /Fp"$(INTDIR)\batgame.pch" /YX"stdinc.hpp" /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /GZ /c 
MTL_PROJ=/nologo /D "_DEBUG" /mktyplib203 /win32 
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\batgame.bsc" 
BSC32_SBRS= \
	
LINK32=link.exe
LINK32_FLAGS=User32.lib /nologo /dll /incremental:yes /pdb:"$(OUTDIR)\batgameDB.pdb" /debug /machine:I386 /out:"$(OUTDIR)\batgameDB.dll" /implib:"$(OUTDIR)\batgameDB.lib" /pdbtype:sept 
LINK32_OBJS= \
	"$(INTDIR)\batgame.obj" \
	"$(OUTDIR)\batdataDB.lib" \
	"$(OUTDIR)\gamesupDB.lib" \
	"$(OUTDIR)\gwindDB.lib" \
	"$(OUTDIR)\obDB.lib" \
	"$(OUTDIR)\systemDB.lib" \
	"$(OUTDIR)\bataiDB.lib" \
	"$(OUTDIR)\batlogicDB.lib" \
	"$(OUTDIR)\batwindDB.lib" \
	"$(OUTDIR)\batdispDB.lib"

"$(OUTDIR)\batgameDB.dll" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

!ENDIF 

.c{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.c{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<


!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("batgame.dep")
!INCLUDE "batgame.dep"
!ELSE 
!MESSAGE Warning: cannot find "batgame.dep"
!ENDIF 
!ENDIF 


!IF "$(CFG)" == "batgame - Win32 Release" || "$(CFG)" == "batgame - Win32 Debug"
SOURCE=.\batgame.cpp

"$(INTDIR)\batgame.obj" : $(SOURCE) "$(INTDIR)"


!IF  "$(CFG)" == "batgame - Win32 Release"

"batdata - Win32 Release" : 
   cd "\src\sf\wargamer\SRC\batdata"
   $(MAKE) /$(MAKEFLAGS) /F .\batdata.mak CFG="batdata - Win32 Release" 
   cd "..\batgame"

"batdata - Win32 ReleaseCLEAN" : 
   cd "\src\sf\wargamer\SRC\batdata"
   $(MAKE) /$(MAKEFLAGS) /F .\batdata.mak CFG="batdata - Win32 Release" RECURSE=1 CLEAN 
   cd "..\batgame"

!ELSEIF  "$(CFG)" == "batgame - Win32 Debug"

"batdata - Win32 Debug" : 
   cd "\src\sf\wargamer\SRC\batdata"
   $(MAKE) /$(MAKEFLAGS) /F .\batdata.mak CFG="batdata - Win32 Debug" 
   cd "..\batgame"

"batdata - Win32 DebugCLEAN" : 
   cd "\src\sf\wargamer\SRC\batdata"
   $(MAKE) /$(MAKEFLAGS) /F .\batdata.mak CFG="batdata - Win32 Debug" RECURSE=1 CLEAN 
   cd "..\batgame"

!ENDIF 

!IF  "$(CFG)" == "batgame - Win32 Release"

"gamesup - Win32 Release" : 
   cd "\src\sf\wargamer\SRC\GAMESUP"
   $(MAKE) /$(MAKEFLAGS) /F .\gamesup.mak CFG="gamesup - Win32 Release" 
   cd "..\batgame"

"gamesup - Win32 ReleaseCLEAN" : 
   cd "\src\sf\wargamer\SRC\GAMESUP"
   $(MAKE) /$(MAKEFLAGS) /F .\gamesup.mak CFG="gamesup - Win32 Release" RECURSE=1 CLEAN 
   cd "..\batgame"

!ELSEIF  "$(CFG)" == "batgame - Win32 Debug"

"gamesup - Win32 Debug" : 
   cd "\src\sf\wargamer\SRC\GAMESUP"
   $(MAKE) /$(MAKEFLAGS) /F .\gamesup.mak CFG="gamesup - Win32 Debug" 
   cd "..\batgame"

"gamesup - Win32 DebugCLEAN" : 
   cd "\src\sf\wargamer\SRC\GAMESUP"
   $(MAKE) /$(MAKEFLAGS) /F .\gamesup.mak CFG="gamesup - Win32 Debug" RECURSE=1 CLEAN 
   cd "..\batgame"

!ENDIF 

!IF  "$(CFG)" == "batgame - Win32 Release"

"gwind - Win32 Release" : 
   cd "\src\sf\wargamer\src\gwind"
   $(MAKE) /$(MAKEFLAGS) /F .\gwind.mak CFG="gwind - Win32 Release" 
   cd "..\batgame"

"gwind - Win32 ReleaseCLEAN" : 
   cd "\src\sf\wargamer\src\gwind"
   $(MAKE) /$(MAKEFLAGS) /F .\gwind.mak CFG="gwind - Win32 Release" RECURSE=1 CLEAN 
   cd "..\batgame"

!ELSEIF  "$(CFG)" == "batgame - Win32 Debug"

"gwind - Win32 Debug" : 
   cd "\src\sf\wargamer\src\gwind"
   $(MAKE) /$(MAKEFLAGS) /F .\gwind.mak CFG="gwind - Win32 Debug" 
   cd "..\batgame"

"gwind - Win32 DebugCLEAN" : 
   cd "\src\sf\wargamer\src\gwind"
   $(MAKE) /$(MAKEFLAGS) /F .\gwind.mak CFG="gwind - Win32 Debug" RECURSE=1 CLEAN 
   cd "..\batgame"

!ENDIF 

!IF  "$(CFG)" == "batgame - Win32 Release"

"ob - Win32 Release" : 
   cd "\src\sf\wargamer\SRC\OB"
   $(MAKE) /$(MAKEFLAGS) /F .\ob.mak CFG="ob - Win32 Release" 
   cd "..\batgame"

"ob - Win32 ReleaseCLEAN" : 
   cd "\src\sf\wargamer\SRC\OB"
   $(MAKE) /$(MAKEFLAGS) /F .\ob.mak CFG="ob - Win32 Release" RECURSE=1 CLEAN 
   cd "..\batgame"

!ELSEIF  "$(CFG)" == "batgame - Win32 Debug"

"ob - Win32 Debug" : 
   cd "\src\sf\wargamer\SRC\OB"
   $(MAKE) /$(MAKEFLAGS) /F .\ob.mak CFG="ob - Win32 Debug" 
   cd "..\batgame"

"ob - Win32 DebugCLEAN" : 
   cd "\src\sf\wargamer\SRC\OB"
   $(MAKE) /$(MAKEFLAGS) /F .\ob.mak CFG="ob - Win32 Debug" RECURSE=1 CLEAN 
   cd "..\batgame"

!ENDIF 

!IF  "$(CFG)" == "batgame - Win32 Release"

"system - Win32 Release" : 
   cd "\src\sf\wargamer\SRC\SYSTEM"
   $(MAKE) /$(MAKEFLAGS) /F .\system.mak CFG="system - Win32 Release" 
   cd "..\batgame"

"system - Win32 ReleaseCLEAN" : 
   cd "\src\sf\wargamer\SRC\SYSTEM"
   $(MAKE) /$(MAKEFLAGS) /F .\system.mak CFG="system - Win32 Release" RECURSE=1 CLEAN 
   cd "..\batgame"

!ELSEIF  "$(CFG)" == "batgame - Win32 Debug"

"system - Win32 Debug" : 
   cd "\src\sf\wargamer\SRC\SYSTEM"
   $(MAKE) /$(MAKEFLAGS) /F .\system.mak CFG="system - Win32 Debug" 
   cd "..\batgame"

"system - Win32 DebugCLEAN" : 
   cd "\src\sf\wargamer\SRC\SYSTEM"
   $(MAKE) /$(MAKEFLAGS) /F .\system.mak CFG="system - Win32 Debug" RECURSE=1 CLEAN 
   cd "..\batgame"

!ENDIF 

!IF  "$(CFG)" == "batgame - Win32 Release"

"batai - Win32 Release" : 
   cd "\src\sf\wargamer\src\batai"
   $(MAKE) /$(MAKEFLAGS) /F .\batai.mak CFG="batai - Win32 Release" 
   cd "..\batgame"

"batai - Win32 ReleaseCLEAN" : 
   cd "\src\sf\wargamer\src\batai"
   $(MAKE) /$(MAKEFLAGS) /F .\batai.mak CFG="batai - Win32 Release" RECURSE=1 CLEAN 
   cd "..\batgame"

!ELSEIF  "$(CFG)" == "batgame - Win32 Debug"

"batai - Win32 Debug" : 
   cd "\src\sf\wargamer\src\batai"
   $(MAKE) /$(MAKEFLAGS) /F .\batai.mak CFG="batai - Win32 Debug" 
   cd "..\batgame"

"batai - Win32 DebugCLEAN" : 
   cd "\src\sf\wargamer\src\batai"
   $(MAKE) /$(MAKEFLAGS) /F .\batai.mak CFG="batai - Win32 Debug" RECURSE=1 CLEAN 
   cd "..\batgame"

!ENDIF 

!IF  "$(CFG)" == "batgame - Win32 Release"

"batlogic - Win32 Release" : 
   cd "\src\sf\wargamer\src\batlogic"
   $(MAKE) /$(MAKEFLAGS) /F .\batlogic.mak CFG="batlogic - Win32 Release" 
   cd "..\batgame"

"batlogic - Win32 ReleaseCLEAN" : 
   cd "\src\sf\wargamer\src\batlogic"
   $(MAKE) /$(MAKEFLAGS) /F .\batlogic.mak CFG="batlogic - Win32 Release" RECURSE=1 CLEAN 
   cd "..\batgame"

!ELSEIF  "$(CFG)" == "batgame - Win32 Debug"

"batlogic - Win32 Debug" : 
   cd "\src\sf\wargamer\src\batlogic"
   $(MAKE) /$(MAKEFLAGS) /F .\batlogic.mak CFG="batlogic - Win32 Debug" 
   cd "..\batgame"

"batlogic - Win32 DebugCLEAN" : 
   cd "\src\sf\wargamer\src\batlogic"
   $(MAKE) /$(MAKEFLAGS) /F .\batlogic.mak CFG="batlogic - Win32 Debug" RECURSE=1 CLEAN 
   cd "..\batgame"

!ENDIF 

!IF  "$(CFG)" == "batgame - Win32 Release"

"batwind - Win32 Release" : 
   cd "\src\sf\wargamer\src\batwind"
   $(MAKE) /$(MAKEFLAGS) /F .\batwind.mak CFG="batwind - Win32 Release" 
   cd "..\batgame"

"batwind - Win32 ReleaseCLEAN" : 
   cd "\src\sf\wargamer\src\batwind"
   $(MAKE) /$(MAKEFLAGS) /F .\batwind.mak CFG="batwind - Win32 Release" RECURSE=1 CLEAN 
   cd "..\batgame"

!ELSEIF  "$(CFG)" == "batgame - Win32 Debug"

"batwind - Win32 Debug" : 
   cd "\src\sf\wargamer\src\batwind"
   $(MAKE) /$(MAKEFLAGS) /F .\batwind.mak CFG="batwind - Win32 Debug" 
   cd "..\batgame"

"batwind - Win32 DebugCLEAN" : 
   cd "\src\sf\wargamer\src\batwind"
   $(MAKE) /$(MAKEFLAGS) /F .\batwind.mak CFG="batwind - Win32 Debug" RECURSE=1 CLEAN 
   cd "..\batgame"

!ENDIF 

!IF  "$(CFG)" == "batgame - Win32 Release"

"batdisp - Win32 Release" : 
   cd "\src\sf\wargamer\src\batdisp"
   $(MAKE) /$(MAKEFLAGS) /F .\batdisp.mak CFG="batdisp - Win32 Release" 
   cd "..\batgame"

"batdisp - Win32 ReleaseCLEAN" : 
   cd "\src\sf\wargamer\src\batdisp"
   $(MAKE) /$(MAKEFLAGS) /F .\batdisp.mak CFG="batdisp - Win32 Release" RECURSE=1 CLEAN 
   cd "..\batgame"

!ELSEIF  "$(CFG)" == "batgame - Win32 Debug"

"batdisp - Win32 Debug" : 
   cd "\src\sf\wargamer\src\batdisp"
   $(MAKE) /$(MAKEFLAGS) /F .\batdisp.mak CFG="batdisp - Win32 Debug" 
   cd "..\batgame"

"batdisp - Win32 DebugCLEAN" : 
   cd "\src\sf\wargamer\src\batdisp"
   $(MAKE) /$(MAKEFLAGS) /F .\batdisp.mak CFG="batdisp - Win32 Debug" RECURSE=1 CLEAN 
   cd "..\batgame"

!ENDIF 


!ENDIF 

