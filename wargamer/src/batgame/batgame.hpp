/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef BATGAME_HPP
#define BATGAME_HPP

#ifndef __cplusplus
#error batgame.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *      Battle Game
 *
 *----------------------------------------------------------------------
 */

#include "refptr.hpp"
#include "systick.hpp"
#include "gamebase.hpp"
#include "batctrl.hpp"
#include "savegame.hpp"
#include "batinfo.hpp"
#include "battime.hpp"
// #include "batdata.hpp"

#if defined(NO_WG_DLL)
    #define BATGAME_DLL
#elif defined(EXPORT_BATGAME_DLL)
    #define BATGAME_DLL __declspec(dllexport)
#else
    #define BATGAME_DLL __declspec(dllimport)
#endif


class BattleGameImp;
class BattleData;
class BattleOB;
class OrderBattle;
using BattleMeasure::BattleTime;

namespace BattleCreate { class BattleCreateInfo; };
class BattleOwner;
class FileWriter;

class BATGAME_DLL BattleGame :
		  public GameBase,
		  public RefBaseDel
{
		  BattleGameImp* d_imp;           // Implementation class

		  // Unimplemented copy

		  BattleGame(const BattleGame&);
		  BattleGame& operator =(const BattleGame&);

	 public:
		  BattleGame(BattleOwner* owner, const char* fileName, SaveGame::FileFormat mode);
		  BattleGame(BattleOwner* owner, BattleOB* ob, const BattleInfo& batInfo, BattleTime& theTime);
		  BattleGame(BattleOwner* owner, FileReader& f, OrderBattle* ob);
		  ~BattleGame();

		  BattleData* battleData();
		  const BattleData* battleData() const;

		  BattleOB* battleOB() const;
		  void reinforced(const BattleInfo& batInfo);

        const BattleInfo* battleInfo() const;

		  /*
			* GameBase Implementation
			*/

        virtual void pauseGame(bool pauseMode);
                // Pause or unpause the game
        virtual void stop();
               // Force the battlegame to stop
               // so that it is safe to delete the OB

        virtual void addTime(SysTick::Value ticks);
                // Add on some time

        virtual bool saveGame(bool prompt) const;
        virtual void saveGame(const char* fileName) const;

		virtual void doNewGame(const char * current, SaveGame::FileFormat format);
		virtual void syncSlave(SysTick::Value ticks);
		virtual void timeSyncAcknowledged(SysTick::Value ticks);
		virtual void slaveReady(void);

	  virtual void processBattleOrder(MP_MSG_BATTLEORDER * msg);
	  virtual void processCampaignOrder(MP_MSG_CAMPAIGNORDER *msg);


        virtual bool writeData(FileWriter& f, bool ownsOB);

        /*
         * Deployment Interface
         */

        // void startDeployment(HWND hMain, BattleInfo * battle_info);

        /*
         * CampaignBattle Interface
         */

        void newDay();  // start a new day

        // void makeMap(const BattleCreate::BattleCreateInfo& info);
        // void makeWindows();
        // void removeWindows();

        // void start();
        // void stop();

        void resume();
};


#endif /* BATGAME_HPP */

