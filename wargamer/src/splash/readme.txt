---------------------------------------------------------------------
The information in this article applies to:

 - Microsoft Win32 Software Development Kit (SDK)
 - Microsoft Windows 95
 - Microsoft Windows NT 4.0
---------------------------------------------------------------------

SUMMARY
=======

Splash.exe is a sample application that demonstrates: 

 - How to display a splash screen for an application.

 - How to load resource bitmaps without losing color information.

MORE INFORMATION
================

The following file is available for download from the Microsoft Software Library: 

 ~ Splash.exe

Release Date: 06-01-1998

For more information about downloading files from the Microsoft Software Library, please see the following article in the Microsoft Knowledge Base:

   ARTICLE-ID: Q119591
   TITLE : How to Obtain Microsoft Support Files from Online Services

The following files are a part of the Splash.exe:

 - Splash.c: Contains all of the functions relating to creating and 
   displaying a splash bitmap. 

 - Splash.rc: Contains the resource for the sample including references to 
   the bitmap that is displayed. 

Splash.exe provides a simple way to display a splash screen for any application without modifying the sources for that application. You can avoid modifying the sources for the application because the user starts the Splash application, while the Splash application displays the splash bitmap and starts the real application. 

Splash.exe is configured to launch Notepad.exe while displaying the Windows NT logo bitmap for five seconds. To customize Splash with your own bitmaps, simply change the bitmap corresponding to IDB_BITMAP in the resources for the SPLASH project. You can further customize the sample by changing the following values in Splash.c: 

 - lpszApplication: Determines the name of the application that the splash 
   screen application starts. It defaults to Notepad.exe for demonstration 
   purposes. 

 - bMakeSplashTopmost: Determines whether or not the splash screen is 
   displayed (and kept) in front of other windows until it is canceled or 
   times out.

 - dwSplashDuration: Determines how long (in milliseconds) the splash 
   screen is displayed. Note that the user can cancel the splash screen by 
   clicking on it or pressing a key while it is active. 
