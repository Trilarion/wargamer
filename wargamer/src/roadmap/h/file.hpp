/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef FILE_HPP
#define FILE_HPP

#include "filebase.hpp"
#include <stdio.h>

/*
 *  File reader class
 */

class AppFileReader : public FileReader {

	 FILE* d_file;
	 const char* d_fileName;

  public:

	 AppFileReader(const char* fileName);
	 ~AppFileReader();

	 /*
	  *  virtual functions from filebase
	  */

	 Boolean seekTo(SeekPos where);
	 SeekPos getPos() { return 1;}
	 Boolean isAscii() { return False; }
	 Boolean isOK() { return True; }

	 /*
	  * virtual functions from filereader
	  */

	 Boolean read(void* data, DataLength length);
	 Boolean read(void* data, DataLength length, DataLength& bytesRead) { return True; }

	 char* readLine() { return 0; }

	 /*
	  * virtual functions from filewriter
	  */
	 Boolean write(const void* data, DataLength length);

};

class AppFileWriter : public FileWriter {

	 FILE* d_file;
	 const char* d_fileName;

  public:

	 AppFileWriter(const char* fileName);
	 ~AppFileWriter();

	 /*
	  *  virtual functions from filebase
	  */

	 Boolean seekTo(SeekPos where) { return True;}
	 SeekPos getPos() { return 1;}
	 Boolean isAscii() { return False; }
	 Boolean isOK() { return True; }

	 /*
	  * virtual functions from filewriter
	  */

	 Boolean write(const void* data, DataLength length);

};

class ChunkFileError {
public:
	ChunkFileError();
};


/*
 * Class for a Chunk File
 */

class FileChunkReader {
	Boolean ok;
	FileReader& f;
	ULONG headerLength;
public:
	FileChunkReader(FileReader& file, const char* name);

	Boolean isOK() { return ok; }
};

#endif
