/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef MAPDRAW_HPP
#define MAPDRAW_HPP

#include "mappoint.hpp"
#include "measure.hpp"
#include "grtypes.hpp"

class DrawDIBDC;
class Connection;
class CampData;


//typedef long LONG;

class MapDraw {

	DrawDIBDC* dib;

	CampData* d_campData;

	// The visible area

	MapPoint top;
	MapPoint size;
	MapPoint total;

	// The corners of the physical world in MapCord units

	MapPoint p1;
	MapPoint p2;
	MapPoint p3;
	MapPoint p4;
	Distance physicalWidth;		// what total.x represents in real world coordinates
	Distance physicalHeight;		// what total.x represents in real world coordinates
	LONG pixelWidth;				// Current magnification level... pixels for entire map width

	// Magnification level enumerated in levels

//	MagEnum magEnum;

//	static const LONG magLevels[];
//	static const MagEnum MaxMagLevel;

public:
	MapDraw(const char* fileName, CampData* d_campData);
	~MapDraw();

	void drawConnections();

	DrawDIBDC* getMap() { return dib; }

private:
	/*
	 * Information Functions
	 */

	MapCord getFullWidth() const { return total.getX(); }
	MapCord getFullHeight() const { return total.getY(); }
	MapCord getViewWidth() const { return size.getX(); }
	MapCord getViewHeight() const { return size.getY(); }
	MapCord getTopX() const { return top.getX(); }
	MapCord getTopY() const { return top.getY(); }
	MapCord getMidX() const;
	MapCord getMidY() const;

	Distance getPhysicalHeight() const { return physicalWidth; }
	Distance getPhysicalWidth() const { return physicalWidth; }

	/*
	 * Update/Set functions
	 */

	Boolean setMapX(MapCord x);
	Boolean setMapY(MapCord y);
	void setViewSize(const MapPoint& m) { size = m; }

	/*
	 * Coordinate conversion functions
	 */

	MapCord pixelToMap(LONG p) const;
	LONG mapToPixel(MapCord m) const;

	LONG distanceToPixel(Distance d) const;

	void mapToPixel(const MapPoint& mc, PixelPoint& pc) const;
	void pixelToMap(const PixelPoint& pc, MapPoint& mc) const;
	void pixelSizeToMap(const PixelPoint& pc, MapPoint& mc) const;
	void mapToLocation(const MapPoint& mc, Location& lc) const;
	void locationToMap(const Location& lc, MapPoint& mc) const;
	void pixelToLocation(const PixelPoint& pc, Location& lc) const;
	void locationToPixel(const Location& lc, PixelPoint& pc) const;

	/*
	 * Magnification functions
	 */
#if 0
	Boolean canZoomOut() const;
	Boolean canZoomIn() const;
	MagEnum getZoomEnum() const { return magEnum; }
	void setZoomEnum(MagEnum m);
	MagEnum getZoomOutEnum() const;
	MagEnum getZoomInEnum() const;
	LONG getZoomLevel() const;
#endif

	/*
	 * Draw roads onto dib
	 */

	void drawRiver(const PixelPoint& p1, const PixelPoint& p2, ColourIndex colour, const Connection* con);
	void drawRoad(const PixelPoint& p1, const PixelPoint& p2, ColourIndex colour, const Connection* con);
	void drawRailLine(const PixelPoint& p1, const PixelPoint& p2, ColourIndex colour, const Connection* con);




};








#endif
