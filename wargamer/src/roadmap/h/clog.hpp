/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/

#ifndef CLOG_H
#define CLOG_H

#ifndef __cplusplus
#error clog.h is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Log File
 *
 *----------------------------------------------------------------------
 */

#include <stdarg.h>

class _Log;

class LogFile {
protected:
	_Log* logFile;
public:
	LogFile(const char* name);
	virtual ~LogFile();

	void printf(const char* fmt, ...);
	void close();			// Temporarily close log file

	void lock();
		// Stop any other thread from writing to logfile
		// Useful if about to log several lines and you don't want
		// it all mixed up with other thread's output

	void unLock();
		// Reluinquish Lock

protected:
	void vprintf(const char* fmt, va_list args);
	void write(const char* s);
};

class LogFileFlush : public LogFile
{
public:
	LogFileFlush(const char* name) : LogFile(name) { }
	void printf(const char* fmt, ...);
	void vprintf(const char* fmt, va_list args);
};

#endif /* CLOG_H */

