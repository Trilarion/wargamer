/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef ROADMAP_HPP
#define ROADMAP_HPP

class CampData;

class RoadMapApp {
	const char* d_dataFile;
	const char* d_srcFile;
	const char* d_destFile;

   CampData* d_campData;

public:
	RoadMapApp();
	~RoadMapApp();

   void execute(const char* dataFile, const char* srcFile, const char* destFile);

   void drawConnections();

	void endApplication();


};













#endif