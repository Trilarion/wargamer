/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef WLDFILE_H
#define WLDFILE_H

#ifndef __cplusplus
#error wldfile.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1995, Steven Morle-Green, All Rights Reserved
 *----------------------------------------------------------------------
 *
 *	World File reading and writing: Use by objects that read/write data
 *
 *----------------------------------------------------------------------
 */

#include "mytypes.h"

class FileReader;
class FileWriter;

/*
 * Virtual Function for reading/writing to a file
 * All objects should implement this.
 * However, some simple objects do not derive from it, so as to avoid
 * having a vTable.
 */

class FileObject {
public:
	virtual ~FileObject() { }
	virtual Boolean readData(FileReader& f) = 0;
	virtual Boolean writeData(FileWriter& f) const = 0;
};

/*
 * Miscelaneous Functions
 */

void addExtension(char* buf, const char* src, const char* ext);

/*
 * Useful functions to check for valid values
 */

#define CHECKUBYTE(b) ASSERT((b >= 0) && (b <= UBYTE_MAX))
#define CHECKUWORD(w) ASSERT((w >= 0) && (w <= UWORD_MAX))

/*
 * Strings, to avoid duplicating them in different object files
 */

/*
 * Chunk Names
 */

extern const char campaignChunkName[];
extern const char townChunkName[];
extern const char connectionChunkName[];
extern const char provinceChunkName[];
extern const char armyChunkName[];
extern const char nationChunkName[];
extern const char commandChunkName[];
extern const char leaderChunkName[];
extern const char spChunkName[];
extern const char iLeaderChunkName[];

/*
 * Keywords
 */

extern const char scenarioToken[];
extern const char entriesToken[];
extern const char nameToken[];
extern const char sideToken[];
extern const char locationToken[];
extern const char connectionToken[];
extern const char provinceToken[];
extern const char nameAlignToken[];
extern const char sizeToken[];
extern const char victoryToken[];
extern const char politicalToken[];
extern const char manPowerToken[];
extern const char resourceToken[];
extern const char supplyToken[];
extern const char stackingToken[];
extern const char terrainToken[];
extern const char flagsToken[];
extern const char fortificationsToken[];
extern const char capitalToken[];
extern const char abrevToken[];
extern const char nodeToken[];
extern const char howToken[];
extern const char qualityToken[];
extern const char capacityToken[];
extern const char townsToken[];
extern const char presidentToken[];
extern const char controlToken[];
extern const char rankCountToken[];
extern const char unusedToken[];
extern const char leaderToken[];
extern const char moraleToken[];
extern const char fatigueToken[];
extern const char initiativeToken[];
extern const char staffToken[];
extern const char subordinationToken[];
extern const char aggressionToken[];
extern const char charismaToken[];
extern const char tacticalToken[];
extern const char commandRadiusToken[];
extern const char rankRatingToken[];
extern const char healthToken[];
extern const char nationToken[];
extern const char commandToken[];
extern const char positionToken[];
extern const char rankToken[];
extern const char linkToken[];
extern const char nextToken[];
extern const char strengthPointToken[];
extern const char offScreenToken[];
extern const char distanceToken[];

extern const char startItemString[];
extern const char endItemString[];
extern const char eolString[];

extern const char fmt_sddddn[];
extern const char fmt_sdddn[];
extern const char fmt_sddn[];
extern const char fmt_sdn[];
extern const char fmt_ssn[];
extern const char fmt_slln[];
extern const char fmt_sln[];
extern const char fmt_sn[];


#endif /* WLDFILE_H */

