/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#include "roadmap.hpp"
#include "campdata.hpp"
#include "mapdraw.hpp"
#include "mapwrite.hpp"
#include "myassert.hpp"
#include "wmisc.hpp"

#include <stdio.h>


void usage()
{
	printf("Incorrect number of command-line arguments\n");
	printf("Usage:\n");
	printf("\troadmap dataFile srcFile destFile\n\n");
}



/*========================================================================
 * Main Function
 */


void main(int argc, char* argv[])
{
  const char* dataFile = 0;
  const char* srcFile = 0;
  const char* destFile = 0;


//  ASSERT(argc == 4);
  if(argc != 4)
  {
  	 usage();
	 return;
  }

  int i = 0;

  while(++i < argc)
  {
	 char* arg = argv[i];

	 if(dataFile == 0)
		dataFile = arg;

	 else if(srcFile == 0)
		srcFile = arg;

	 else if(destFile == 0)
		destFile = arg;

	 else
	 {
		printf("Too many parameters given");
		usage();
		return;
	 }
  }

  if(destFile == 0)
  {
  	usage();
	return;
  }

  RoadMapApp application;
  application.execute(dataFile, srcFile, destFile);
}


/*===========================================================
 * RoadMap Application Functions
 */

RoadMapApp::RoadMapApp()
{
	d_dataFile = 0;

	d_srcFile = 0;

	d_destFile = 0;

	d_campData = 0;
}

RoadMapApp::~RoadMapApp()
{
	endApplication();
}


void RoadMapApp::execute(const char* dataFile, const char* srcFile, const char* destFile)
{
	ASSERT(dataFile != 0);
	ASSERT(srcFile != 0);
	ASSERT(destFile != 0);

	d_dataFile = dataFile;

	d_srcFile = srcFile;

	d_destFile = destFile;


	printf("Reading Town\Connection data!\n");
	d_campData = new CampData(d_dataFile);
	ASSERT(d_campData != 0);

	drawConnections();

}


void RoadMapApp::drawConnections()
{

	printf("Reading map file!\n");
	MapDraw mapDraw(d_srcFile, d_campData);

	printf("Drawing Connections!\n");
	mapDraw.drawConnections();

	printf("Writing new map file to disk!\n");
	DrawDIBDC* map = mapDraw.getMap();
	MapWrite mapWrite(map, d_destFile);
}

void RoadMapApp::endApplication()
{
  printf("Succesfully completed operation!\n");
}


/*
 * Dummy function to keep linker happy
 * DIB::lightenedText uses this function
 */

void WindowsMisc::wTextOut(HDC hdc, int x, int y, LPCTSTR text)
{
	printf("wTextOut: %s", text);
}


