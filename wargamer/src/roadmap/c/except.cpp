/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#include "except.hpp"
#include "myassert.hpp"
#include <stdio.h>
#include <stdarg.h>


GeneralError::GeneralError(const char* fmt, ...)
{
	char buffer[1000];

	va_list vaList;
	va_start(vaList, fmt);
	vsprintf(buffer, fmt, vaList);
	va_end(vaList);

   error(buffer);

}

void GeneralError::setMessage(const char* s)
{


}
