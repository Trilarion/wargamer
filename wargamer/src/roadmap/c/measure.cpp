/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *	Implementation of Measurements
 *
 *----------------------------------------------------------------------
 */

#include "measure.hpp"
#include "filebase.hpp"

Boolean Location::writeData(FileWriter& f) const
{
	f.putULong(x);
	f.putULong(y);
	return f.isOK();
}

Boolean Location::readData(FileReader& f)
{
	f.getSLong(x);
	f.getSLong(y);
	return f.isOK();
}


