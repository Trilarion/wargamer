/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1995, Steven Morle-Green, All Rights Reserved
 *----------------------------------------------------------------------
 *
 *	Provinces, Towns and Connections Imlementation
 *
 *----------------------------------------------------------------------
 */

#include "town.hpp"
#include "file.hpp"
// #include "campdint.hpp"
#include "wldfile.hpp"
//#include "filecnk.hpp"

/*------------------------------------------------------------------
 * Code for Towns
 */

Town::Town()
{
	for(int i = 0; i < MaxConnections; i++)
		connections[i] = NoConnection;
	province = NoProvince;
	nameAlign = NA_TOP;
	nameX = 0;
	nameY = 0;

	// startSide = SIDE_Neutral;
	size = TOWN_Other;
	victory[0] = 0;
	victory[1] = 0;
	political = 0;
#if defined(NO_PAUL_TEST)
	manPowerRate = 0;
	resourceRate = 0;
#else
	manPowerRate = 200;
	resourceRate = 200;
#endif
	supplyRate = 0;
	forage = 0;
	stacking = UBYTE_MAX;
	terrain = NoTerrainType;

	flags = 0
		// | mpTransferable
		// | isSupplySource
		// | isDepot
		// | isCapital
		// | siegeable
		;

	strength = 0;

	fortifications = 0;
	supplyLevel = 0;
	fortUpgrade = NoCommandPosition;
	supplyInstall = NoCommandPosition;

	garrison = False;
	siege = False;
	raid = False;
	occupied = False;

	breached = False;
	devBomb = False;
	weeksUnderSiege = 0;

	offScreen = False;

	helpID = NoHelpID;
}

static const UWORD Town::fileVersion = 0x0006;

Boolean Town::readData(FileReader& f)
{
	if(f.isAscii())
	{
	}
	else
	{
		UWORD version;
		f.getUWord(version);

		ASSERT(version <= fileVersion);

		setName(f.getString());

		Side newSide;
		f.getUByte(newSide);
		setSide(newSide);

		LocationObject::readData(f);
		// Location l;
		// readLocation(f, l);
		// setLocation(l);

		for(int i = 0; i < MaxConnections; i++)
			f.getUWord(connections[i]);
		f.getUWord(province);

		UBYTE tb;
		f.getUByte(tb);
		nameAlign = NameAlign(tb);

		f.getSByte(nameX);
		f.getSByte(nameY);

		if(version <= 0x0001)
		{
			UBYTE junk;
			f.getUByte(junk);
			// f.getUByte(newSide);
			// setStartSide(newSide);
		}

		f.getUByte(tb);
		size = TownSize(tb);

		f.getUByte(victory[0]);
		f.getUByte(victory[1]);
		f.getUByte(political);
		f.getUByte(manPowerRate);
		f.getUByte(resourceRate);
		f.getUByte(supplyRate);
		if(version >= 0x0006)
			f.getUByte(forage);
		else
			forage = 1;
		f.getUByte(stacking);
		f.getUByte(terrain);
		f.getUByte(flags);
		f.getUByte(fortifications);

		if(version >= 0x0003)
			f >> offScreen;
		  // f.getUByte(offScreen);
		else
		  offScreen = False;

		if(version >= 0x0004)
		  f.getUWord(helpID);
		else
		  helpID = NoHelpID;

		if(version >= 0x0005)
		  f.getUByte(strength);
		else
		  strength = 0;
	}

	return f.isOK();
}

/*
 * Connections
 */


static const UWORD Connection::fileVersion = 0x0002;

Boolean Connection::readData(FileReader& f)
{
	if(f.isAscii())
	{
		return False;
	}
	else
	{
		UWORD version;
		f.getUWord(version);
		ASSERT(version <= fileVersion);
		f.getUWord(node1);
		f.getUWord(node2);

		UBYTE b;
		f.getUByte(b);
		how = ConnectType(b);

		f.getUByte(b);
		quality = ConnectQuality(b);

		f.getUByte(capacity);

		if(version >= 0x0001)
			f >> offScreen;
		  // f.getUByte(offScreen);
		else
		  offScreen = False;

		if(version >= 0x0001)
		  f.getSLong(distance);
		else
		  distance = 0;

		if(version >= 0x0002)
		{
		  f.getUByte(b);
		  whichSide = static_cast<WhichSideChokePoint::Type>(b);
		}

		return f.isOK();
	}
}

/*================================================================
 * Lists
 */

static const UWORD arrayListVersion = 0x0000;

template<class T>
Boolean readData(Array<T>& list, FileReader& f, const char* chunkName)
{
	FileChunkReader fr(f, chunkName);
	if(!fr.isOK())
		return False;

	if(f.isAscii())
	{
		return False;
	}
	else
	{
		UWORD version;
		f.getUWord(version);
		ASSERT(version <= arrayListVersion);
		UWORD count;
		f.getUWord(count);
		list.init(count);
	}

	for(ArrayIndex i = 0; i < list.entries(); i++)
	{
		T& item = list[i];

		item.readData(f);
	}

	return f.isOK();
}



/*========================================================
 * Connections
 */

// const UWORD DConnectionListVersion	= 0x0000;

//const char connectionChunkName[]	= "Connections";

Boolean ConnectionList::readData(FileReader& f)
{
	return ::readData(*this, f, connectionChunkName);
}




/*========================================================
 * Towns
 */

// const UWORD DTownListVersion	= 0x0000;
//const char townChunkName[]			= "Locations";

Boolean TownList::readData(FileReader& f)
{
	return ::readData(*this, f, townChunkName);
}





