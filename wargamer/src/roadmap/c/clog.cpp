/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Combat Log
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.2  1996/02/08 17:33:46  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.1  1996/01/19 11:30:05  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#include "clog.hpp"
#include <windows.h>


LogFile::LogFile(const char* name)
{
}

LogFile::~LogFile()
{
}

void LogFile::printf(const char* fmt, ...)
{
}

void LogFile::close()
{
}


void LogFile::vprintf(const char* fmt, va_list args)
{
}

void LogFile::write(const char* s)
{
}

void LogFileFlush::printf(const char* fmt, ...)
{
}

void LogFile::lock()
{
}

void LogFile::unLock()
{
}


