/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#include "file.hpp"
#include "myassert.hpp"
#include "except.hpp"

#include <string.h>

const char ChunkFileID[]				= "SWG_ChunkFile";
static const UWORD DChunkFileVersion = 0x0000;
const char endChunkName[]				= "End";

ChunkFileError::ChunkFileError()
{
  GeneralError error("Error reading Chunk File!");

}

AppFileReader::AppFileReader(const char* fileName)
{
  ASSERT(fileName != 0);

  d_fileName = fileName;

  d_file = fopen(d_fileName, "rb");
  if(d_file == 0)
	 error("error opening %s", d_fileName);

}

AppFileReader::~AppFileReader()
{
  fclose(d_file);
}

Boolean AppFileReader::read(void* data, DataLength length)
{
  if(fread(data, length, 1, d_file) != 1)
  {
	 error("Error reading from %s", d_fileName);
  }

  return True;
}

Boolean AppFileReader::seekTo(SeekPos where)
{
	if(fseek(d_file, where, SEEK_SET) != 0)
	{
	  error("Error seeking from %s", d_fileName);
	}
   return True;
}

AppFileWriter::AppFileWriter(const char* fileName)
{
  ASSERT(fileName != 0);

  d_fileName = fileName;

  d_file = fopen(d_fileName, "wb");
  if(d_file == 0)
	 error("error creating %s", d_fileName);

}

AppFileWriter::~AppFileWriter()
{
  fclose(d_file);
}

Boolean AppFileWriter::write(const void* data, DataLength length)
{
  if(fwrite(data, length, 1, d_file) != 1)
  {
	 error("Error writing to %s", d_fileName);
  }

  return True;
}


FileChunkReader::FileChunkReader(FileReader& file, const char* name) :
	f(file)
{
	ok = False;

   file.seekTo(0);

	/*
	 * Read header
	 */

	file.getULong(headerLength);
   SeekPos pos = headerLength;

	UWORD version;
	file.getUWord(version);
	ASSERT(version == DChunkFileVersion);

	char id[sizeof(ChunkFileID)];
	file.read(id, sizeof(ChunkFileID));

	if(memcmp(id, ChunkFileID, sizeof(ChunkFileID)) != 0)
	{
		throw ChunkFileError();
	}

	ULONG l;

	for(;;)
	{
	  if(!file.getULong(l))
		 return;

	  const char* s = file.getString();

	  if(s == 0)
		 return;

	  if(strcmp(s, name) == 0)
	  {
		 ok = True;
		 return;
	  }

	  if(strcmp(s, endChunkName) == 0)
		 return;

	  pos += l;

	  file.seekTo(pos);
	}
}

