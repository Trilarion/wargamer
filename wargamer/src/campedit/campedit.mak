# Microsoft Developer Studio Generated NMAKE File, Based on campedit.dsp
!IF "$(CFG)" == ""
CFG=campedit - Win32 Editor Release
!MESSAGE No configuration specified. Defaulting to campedit - Win32 Editor Release.
!ENDIF 

!IF "$(CFG)" != "campedit - Win32 Editor Debug" && "$(CFG)" != "campedit - Win32 Editor Release"
!MESSAGE Invalid configuration "$(CFG)" specified.
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "campedit.mak" CFG="campedit - Win32 Editor Release"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "campedit - Win32 Editor Debug" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE "campedit - Win32 Editor Release" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE 
!ERROR An invalid configuration is specified.
!ENDIF 

!IF "$(OS)" == "Windows_NT"
NULL=
!ELSE 
NULL=nul
!ENDIF 

CPP=cl.exe
MTL=midl.exe
RSC=rc.exe

!IF  "$(CFG)" == "campedit - Win32 Editor Debug"

OUTDIR=.\..\..\exe
INTDIR=.\o\Editor\Debug
# Begin Custom Macros
OutDir=.\..\..\exe
# End Custom Macros

!IF "$(RECURSE)" == "0" 

ALL : "$(OUTDIR)\campeditEDDB.dll"

!ELSE 

ALL : "gamesup - Win32 Editor Debug" "mapwind - Win32 Editor Debug" "campdata - Win32 Editor Debug" "ob - Win32 Editor Debug" "system - Win32 Editor Debug" "$(OUTDIR)\campeditEDDB.dll"

!ENDIF 

!IF "$(RECURSE)" == "1" 
CLEAN :"system - Win32 Editor DebugCLEAN" "ob - Win32 Editor DebugCLEAN" "campdata - Win32 Editor DebugCLEAN" "mapwind - Win32 Editor DebugCLEAN" "gamesup - Win32 Editor DebugCLEAN" 
!ELSE 
CLEAN :
!ENDIF 
	-@erase "$(INTDIR)\condedit.obj"
	-@erase "$(INTDIR)\conedit.obj"
	-@erase "$(INTDIR)\editdate.obj"
	-@erase "$(INTDIR)\editinfo.obj"
	-@erase "$(INTDIR)\editnat.obj"
	-@erase "$(INTDIR)\editsup.obj"
	-@erase "$(INTDIR)\mw_edit.obj"
	-@erase "$(INTDIR)\obedit.obj"
	-@erase "$(INTDIR)\obwin.obj"
	-@erase "$(INTDIR)\provedit.obj"
	-@erase "$(INTDIR)\townedit.obj"
	-@erase "$(INTDIR)\vc60.idb"
	-@erase "$(INTDIR)\vc60.pdb"
	-@erase "$(OUTDIR)\campeditEDDB.dll"
	-@erase "$(OUTDIR)\campeditEDDB.exp"
	-@erase "$(OUTDIR)\campeditEDDB.ilk"
	-@erase "$(OUTDIR)\campeditEDDB.lib"
	-@erase "$(OUTDIR)\campeditEDDB.pdb"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

"$(INTDIR)" :
    if not exist "$(INTDIR)/$(NULL)" mkdir "$(INTDIR)"

CPP_PROJ=/nologo /MDd /W3 /Gm /Gi /GR /GX /ZI /Od /I "..\h" /I "..\..\res" /I "..\system" /I "..\gamesup" /I "..\ob" /I "..\campdata" /I "..\mapwind" /I "..\campwind" /D "_USRDLL" /D "EXPORT_CAMPEDIT_DLL" /D "_DEBUG" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /D "EDITOR" /Fp"$(INTDIR)\campedit.pch" /YX"stdinc.hpp" /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /GZ /c 
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\campedit.bsc" 
BSC32_SBRS= \
	
LINK32=link.exe
LINK32_FLAGS=gdi32.lib user32.lib comctl32.lib /nologo /dll /incremental:yes /pdb:"$(OUTDIR)\campeditEDDB.pdb" /debug /machine:I386 /out:"$(OUTDIR)\campeditEDDB.dll" /implib:"$(OUTDIR)\campeditEDDB.lib" /pdbtype:sept 
LINK32_OBJS= \
	"$(INTDIR)\condedit.obj" \
	"$(INTDIR)\conedit.obj" \
	"$(INTDIR)\editdate.obj" \
	"$(INTDIR)\editinfo.obj" \
	"$(INTDIR)\editnat.obj" \
	"$(INTDIR)\editsup.obj" \
	"$(INTDIR)\mw_edit.obj" \
	"$(INTDIR)\obedit.obj" \
	"$(INTDIR)\obwin.obj" \
	"$(INTDIR)\provedit.obj" \
	"$(INTDIR)\townedit.obj" \
	"$(OUTDIR)\systemDB.lib" \
	"$(OUTDIR)\obEDDB.lib" \
	"$(OUTDIR)\campdataEDDB.lib" \
	"$(OUTDIR)\mapwindEDDB.lib" \
	"$(OUTDIR)\gamesupDB.lib"

"$(OUTDIR)\campeditEDDB.dll" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

!ELSEIF  "$(CFG)" == "campedit - Win32 Editor Release"

OUTDIR=.\..\..\exe
INTDIR=.\o\Editor\Release
# Begin Custom Macros
OutDir=.\..\..\exe
# End Custom Macros

!IF "$(RECURSE)" == "0" 

ALL : "$(OUTDIR)\campeditED.dll"

!ELSE 

ALL : "gamesup - Win32 Editor Release" "mapwind - Win32 Editor Release" "campdata - Win32 Editor Release" "ob - Win32 Editor Release" "system - Win32 Editor Release" "$(OUTDIR)\campeditED.dll"

!ENDIF 

!IF "$(RECURSE)" == "1" 
CLEAN :"system - Win32 Editor ReleaseCLEAN" "ob - Win32 Editor ReleaseCLEAN" "campdata - Win32 Editor ReleaseCLEAN" "mapwind - Win32 Editor ReleaseCLEAN" "gamesup - Win32 Editor ReleaseCLEAN" 
!ELSE 
CLEAN :
!ENDIF 
	-@erase "$(INTDIR)\condedit.obj"
	-@erase "$(INTDIR)\conedit.obj"
	-@erase "$(INTDIR)\editdate.obj"
	-@erase "$(INTDIR)\editinfo.obj"
	-@erase "$(INTDIR)\editnat.obj"
	-@erase "$(INTDIR)\editsup.obj"
	-@erase "$(INTDIR)\mw_edit.obj"
	-@erase "$(INTDIR)\obedit.obj"
	-@erase "$(INTDIR)\obwin.obj"
	-@erase "$(INTDIR)\provedit.obj"
	-@erase "$(INTDIR)\townedit.obj"
	-@erase "$(INTDIR)\vc60.idb"
	-@erase "$(OUTDIR)\campeditED.dll"
	-@erase "$(OUTDIR)\campeditED.exp"
	-@erase "$(OUTDIR)\campeditED.lib"
	-@erase "$(OUTDIR)\campeditED.pdb"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

"$(INTDIR)" :
    if not exist "$(INTDIR)/$(NULL)" mkdir "$(INTDIR)"

CPP_PROJ=/nologo /MD /W3 /Gi /GR /GX /Zd /O2 /I "..\h" /I "..\..\res" /I "..\system" /I "..\gamesup" /I "..\ob" /I "..\campdata" /I "..\mapwind" /I "..\campwind" /D "NDEBUG" /D "_USRDLL" /D "EXPORT_CAMPEDIT_DLL" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /D "EDITOR" /Fp"$(INTDIR)\campedit.pch" /YX"stdinc.hpp" /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /c 
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\campedit.bsc" 
BSC32_SBRS= \
	
LINK32=link.exe
LINK32_FLAGS=gdi32.lib user32.lib comctl32.lib /nologo /dll /incremental:no /pdb:"$(OUTDIR)\campeditED.pdb" /debug /machine:I386 /out:"$(OUTDIR)\campeditED.dll" /implib:"$(OUTDIR)\campeditED.lib" 
LINK32_OBJS= \
	"$(INTDIR)\condedit.obj" \
	"$(INTDIR)\conedit.obj" \
	"$(INTDIR)\editdate.obj" \
	"$(INTDIR)\editinfo.obj" \
	"$(INTDIR)\editnat.obj" \
	"$(INTDIR)\editsup.obj" \
	"$(INTDIR)\mw_edit.obj" \
	"$(INTDIR)\obedit.obj" \
	"$(INTDIR)\obwin.obj" \
	"$(INTDIR)\provedit.obj" \
	"$(INTDIR)\townedit.obj" \
	"$(OUTDIR)\system.lib" \
	"$(OUTDIR)\obED.lib" \
	"$(OUTDIR)\campdataED.lib" \
	"$(OUTDIR)\mapwindED.lib" \
	"$(OUTDIR)\gamesup.lib"

"$(OUTDIR)\campeditED.dll" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

!ENDIF 

.c{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.c{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

MTL_PROJ=/nologo /D "_DEBUG" /mktyplib203 /win32 

!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("campedit.dep")
!INCLUDE "campedit.dep"
!ELSE 
!MESSAGE Warning: cannot find "campedit.dep"
!ENDIF 
!ENDIF 


!IF "$(CFG)" == "campedit - Win32 Editor Debug" || "$(CFG)" == "campedit - Win32 Editor Release"
SOURCE=.\condedit.cpp

"$(INTDIR)\condedit.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\conedit.cpp

"$(INTDIR)\conedit.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\editdate.cpp

"$(INTDIR)\editdate.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\editinfo.cpp

"$(INTDIR)\editinfo.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\editnat.cpp

"$(INTDIR)\editnat.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\editsup.cpp

"$(INTDIR)\editsup.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\mw_edit.cpp

"$(INTDIR)\mw_edit.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\obedit.cpp

"$(INTDIR)\obedit.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=..\campwind\obwin.cpp

"$(INTDIR)\obwin.obj" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=.\provedit.cpp

"$(INTDIR)\provedit.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\townedit.cpp

"$(INTDIR)\townedit.obj" : $(SOURCE) "$(INTDIR)"


!IF  "$(CFG)" == "campedit - Win32 Editor Debug"

"system - Win32 Editor Debug" : 
   cd "\src\sf\wargamer\SRC\SYSTEM"
   $(MAKE) /$(MAKEFLAGS) /F .\system.mak CFG="system - Win32 Editor Debug" 
   cd "..\campedit"

"system - Win32 Editor DebugCLEAN" : 
   cd "\src\sf\wargamer\SRC\SYSTEM"
   $(MAKE) /$(MAKEFLAGS) /F .\system.mak CFG="system - Win32 Editor Debug" RECURSE=1 CLEAN 
   cd "..\campedit"

!ELSEIF  "$(CFG)" == "campedit - Win32 Editor Release"

"system - Win32 Editor Release" : 
   cd "\src\sf\wargamer\SRC\SYSTEM"
   $(MAKE) /$(MAKEFLAGS) /F .\system.mak CFG="system - Win32 Editor Release" 
   cd "..\campedit"

"system - Win32 Editor ReleaseCLEAN" : 
   cd "\src\sf\wargamer\SRC\SYSTEM"
   $(MAKE) /$(MAKEFLAGS) /F .\system.mak CFG="system - Win32 Editor Release" RECURSE=1 CLEAN 
   cd "..\campedit"

!ENDIF 

!IF  "$(CFG)" == "campedit - Win32 Editor Debug"

"ob - Win32 Editor Debug" : 
   cd "\src\sf\wargamer\SRC\OB"
   $(MAKE) /$(MAKEFLAGS) /F .\ob.mak CFG="ob - Win32 Editor Debug" 
   cd "..\campedit"

"ob - Win32 Editor DebugCLEAN" : 
   cd "\src\sf\wargamer\SRC\OB"
   $(MAKE) /$(MAKEFLAGS) /F .\ob.mak CFG="ob - Win32 Editor Debug" RECURSE=1 CLEAN 
   cd "..\campedit"

!ELSEIF  "$(CFG)" == "campedit - Win32 Editor Release"

"ob - Win32 Editor Release" : 
   cd "\src\sf\wargamer\SRC\OB"
   $(MAKE) /$(MAKEFLAGS) /F .\ob.mak CFG="ob - Win32 Editor Release" 
   cd "..\campedit"

"ob - Win32 Editor ReleaseCLEAN" : 
   cd "\src\sf\wargamer\SRC\OB"
   $(MAKE) /$(MAKEFLAGS) /F .\ob.mak CFG="ob - Win32 Editor Release" RECURSE=1 CLEAN 
   cd "..\campedit"

!ENDIF 

!IF  "$(CFG)" == "campedit - Win32 Editor Debug"

"campdata - Win32 Editor Debug" : 
   cd "\src\sf\wargamer\src\campdata"
   $(MAKE) /$(MAKEFLAGS) /F .\campdata.mak CFG="campdata - Win32 Editor Debug" 
   cd "..\campedit"

"campdata - Win32 Editor DebugCLEAN" : 
   cd "\src\sf\wargamer\src\campdata"
   $(MAKE) /$(MAKEFLAGS) /F .\campdata.mak CFG="campdata - Win32 Editor Debug" RECURSE=1 CLEAN 
   cd "..\campedit"

!ELSEIF  "$(CFG)" == "campedit - Win32 Editor Release"

"campdata - Win32 Editor Release" : 
   cd "\src\sf\wargamer\src\campdata"
   $(MAKE) /$(MAKEFLAGS) /F .\campdata.mak CFG="campdata - Win32 Editor Release" 
   cd "..\campedit"

"campdata - Win32 Editor ReleaseCLEAN" : 
   cd "\src\sf\wargamer\src\campdata"
   $(MAKE) /$(MAKEFLAGS) /F .\campdata.mak CFG="campdata - Win32 Editor Release" RECURSE=1 CLEAN 
   cd "..\campedit"

!ENDIF 

!IF  "$(CFG)" == "campedit - Win32 Editor Debug"

"mapwind - Win32 Editor Debug" : 
   cd "\src\sf\wargamer\src\mapwind"
   $(MAKE) /$(MAKEFLAGS) /F .\mapwind.mak CFG="mapwind - Win32 Editor Debug" 
   cd "..\campedit"

"mapwind - Win32 Editor DebugCLEAN" : 
   cd "\src\sf\wargamer\src\mapwind"
   $(MAKE) /$(MAKEFLAGS) /F .\mapwind.mak CFG="mapwind - Win32 Editor Debug" RECURSE=1 CLEAN 
   cd "..\campedit"

!ELSEIF  "$(CFG)" == "campedit - Win32 Editor Release"

"mapwind - Win32 Editor Release" : 
   cd "\src\sf\wargamer\src\mapwind"
   $(MAKE) /$(MAKEFLAGS) /F .\mapwind.mak CFG="mapwind - Win32 Editor Release" 
   cd "..\campedit"

"mapwind - Win32 Editor ReleaseCLEAN" : 
   cd "\src\sf\wargamer\src\mapwind"
   $(MAKE) /$(MAKEFLAGS) /F .\mapwind.mak CFG="mapwind - Win32 Editor Release" RECURSE=1 CLEAN 
   cd "..\campedit"

!ENDIF 

!IF  "$(CFG)" == "campedit - Win32 Editor Debug"

"gamesup - Win32 Editor Debug" : 
   cd "\src\sf\wargamer\SRC\GAMESUP"
   $(MAKE) /$(MAKEFLAGS) /F .\gamesup.mak CFG="gamesup - Win32 Editor Debug" 
   cd "..\campedit"

"gamesup - Win32 Editor DebugCLEAN" : 
   cd "\src\sf\wargamer\SRC\GAMESUP"
   $(MAKE) /$(MAKEFLAGS) /F .\gamesup.mak CFG="gamesup - Win32 Editor Debug" RECURSE=1 CLEAN 
   cd "..\campedit"

!ELSEIF  "$(CFG)" == "campedit - Win32 Editor Release"

"gamesup - Win32 Editor Release" : 
   cd "\src\sf\wargamer\SRC\GAMESUP"
   $(MAKE) /$(MAKEFLAGS) /F .\gamesup.mak CFG="gamesup - Win32 Editor Release" 
   cd "..\campedit"

"gamesup - Win32 Editor ReleaseCLEAN" : 
   cd "\src\sf\wargamer\SRC\GAMESUP"
   $(MAKE) /$(MAKEFLAGS) /F .\gamesup.mak CFG="gamesup - Win32 Editor Release" RECURSE=1 CLEAN 
   cd "..\campedit"

!ENDIF 


!ENDIF 

