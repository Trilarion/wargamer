/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef TOWNEDIT_H
#define TOWNEDIT_H

#ifndef __cplusplus
#error townedit.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1995, Steven Morle-Green, All Rights Reserved
 *----------------------------------------------------------------------
 *
 *	Town Editting dialogue
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.7  1996/02/19 16:17:35  Steven_Green
 * Use Town Index instead of pointers
 *
 * Revision 1.6  1995/12/07 09:17:07  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.5  1995/12/05 09:20:07  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.4  1995/11/29 12:14:04  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.3  1995/11/22 10:45:25  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.2  1995/11/14 11:25:57  Steven_Green
 * Extra controls added
 *
 * Revision 1.1  1995/11/07 18:06:58  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#include "dialog.hpp"
#include "town.hpp"
#include "grtypes.hpp"
// #include "mapwind.hpp"
#include "mw_user.hpp"
// #include "mwed_int.hpp"

class MWEditInterface;
class TownEditPage;
class MapWindow;
class CampaignData;

class TownEdit : public ModelessDialog, public EditDialog {
	MWEditInterface* d_editControl;

	Town* town;
	ITown townID;
	Town oldValues;

	CampaignData* campData;
	MapWindow* parent;
	PixelPoint position;				// Where it starts

	static const int nPages;
	TownEditPage** pages;
	HWND tabHwnd;						// Tab Dialog handle
	int curPage;						// Current tabbed dialog Page
	RECT tdRect;						// Area for tabbed dialogues to fit into

public:
	// TownEdit(CampaignData* cData, MapWindow* p, ITown it, const PixelPoint& pos);
	TownEdit(MWEditInterface* editControl, CampaignData* cData, MapWindow* p, ITown it, const PixelPoint& pos);
	~TownEdit();

	CampaignData* getCampaignData() const { return campData; }

	// Callback functions from MapWindow

	void kill();
	// void gotPosition(Location* l);
	TrackMode onSelect(TrackMode mode, const MapSelect& info);
	// MapObject* getItem() const { return town; }
	// Town& getOldValues() { return oldValues; }

	// Functions used by Child Dialog controls

	Town* getTown() const { return town; }
	const RECT& getDialogRect() const { return tdRect; }

	void onCommand(HWND hwnd, int id, HWND hwndCtl, UINT codeNotify);

private:
	BOOL procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);

	BOOL onInitDialog(HWND hwnd, HWND hwndFocus, LPARAM lParam);
	void onDestroy(HWND hwnd);
	void onClose(HWND hwnd);
	LRESULT onNotify(HWND hWnd, int id, NMHDR* lpNMHDR);
	void onActivate(HWND hwnd, UINT state, HWND hwndActDeact, BOOL fMinimized);

	void initControls();
	void updateValues();
	void onTownName(HWND hwnd, UINT codeNotify);
	void onAlign(HWND hCombo, UINT codeNotify);
	void onOffset(HWND hDlg, int id, HWND hwnd, UINT codeNotify);
	void onTerrain(HWND hCombo, UINT codeNotify);
	void onMove(HWND hButton, UINT codeNotify);
	void onState(HWND hCombo, UINT codeNotify);
	void onSide(HWND hCombo, UINT codeNotify);
#if 0
	void onStartSide(HWND hCombo, UINT codeNotify);
#endif
	void onSize(HWND hCombo, UINT codeNotify);
	void onContextMenu(HWND hwnd, HWND hwndCtl, int x, int y);
	BOOL onHelp(HWND hwnd, LPHELPINFO lParam);
	void onSelChanged();

	void setProvince();
   Boolean checkTownHelpID();
};

TownEdit* editTown(ITown town, PixelPoint& p);


#endif /* TOWNEDIT_H */

