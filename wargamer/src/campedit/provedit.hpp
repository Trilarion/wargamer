/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef PROVEDIT_H
#define PROVEDIT_H

#ifndef __cplusplus
#error provedit.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1995, Steven Morle-Green, All Rights Reserved
 *----------------------------------------------------------------------
 *
 *	Province Editor
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.3  1996/02/19 16:17:35  Steven_Green
 * Use ITown and IProvince for selected objects instead of pointers
 *
 * Revision 1.2  1995/12/07 09:17:07  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.1  1995/12/05 09:20:07  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#include "dialog.hpp"
#include "town.hpp"
#include "grtypes.hpp"
#include "mw_user.hpp"

class CampaignData;
class MapWindow;
class MWEditInterface;
class Province;

class ProvinceEdit : public ModelessDialog, public EditDialog {
	MWEditInterface* d_editControl;

	IProvince provinceID;
	Province* province;
	Province oldValues;

	CampaignData* campData;

	MapWindow* parent;
	PixelPoint position;

	enum {
		Get_Nothing,
		Get_Position,
		Get_Capital,
		Get_Towns
	} mode;

	static const char s_defaultPicture[];

public:
	ProvinceEdit(MWEditInterface* editControl, CampaignData* data, MapWindow* p, IProvince iprov, const PixelPoint& pos);
	~ProvinceEdit();

	/*
	 * Virtual functions from EditDialog
	 */

	void kill();
	// void gotPosition(Location* l);
	TrackMode onSelect(TrackMode mode, const MapSelect& info);

private:
	BOOL procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);

	BOOL onInitDialog(HWND hwnd, HWND hwndFocus, LPARAM lParam);
	void onDestroy(HWND hwnd);
	void onClose(HWND hwnd);
	void onActivate(HWND hwnd, UINT state, HWND hwndActDeact, BOOL fMinimized);
	void onCommand(HWND hwnd, int id, HWND hwndCtl, UINT codeNotify);
	void onContextMenu(HWND hwnd, HWND hwndCtl, int x, int y);
	BOOL onHelp(HWND hwnd, LPHELPINFO lParam);

	void updateValues();

	void onStateName(HWND hwnd, UINT codeNotify);
	void onShortName(HWND hwnd, UINT codeNotify);
	void onDelete(HWND hwnd);

	void setCapitalText();

   Boolean checkProvinceHelpID();
};
#endif /* PROVEDIT_H */

