/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef EDITSUP_H
#define EDITSUP_H

#ifndef __cplusplus
#error editsup.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1995, Steven Morle-Green, All Rights Reserved
 *----------------------------------------------------------------------
 *
 *	Support for editor dialogs
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  1995/12/07 09:17:07  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#include <windef.h>

void makeSideCombo(HWND hCombo);
void makeNationCombo(HWND hCombo);

#endif /* EDITSUP_H */

