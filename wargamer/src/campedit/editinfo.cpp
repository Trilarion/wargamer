/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
#include "stdinc.hpp"
#include "editinfo.hpp"
#include "campinfo.hpp"
#include "campdint.hpp"
#include "dialog.hpp"
#include "resdef.h"
#include "myassert.hpp"
#include "savegame.hpp"
#include "gamectrl.hpp"
#include "scenario.hpp"

const int MaxDescriptionLength = 500;

class EditInfo : public ModalDialog {
    CampaignInfo* d_info;
  public:
    EditInfo(CampaignInfo* info);
    ~EditInfo() {}

   int run(HWND hParent);
private:
   BOOL procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);

   BOOL EditInfo::onInitDialog(HWND hwnd, HWND hwndFocus, LPARAM lParam);
   void EditInfo::onDestroy(HWND hwnd);
   void EditInfo::onClose(HWND hwnd);
   void EditInfo::onCommand(HWND hwnd, int id, HWND hwndCtl, UINT codeNotify);
// void EditInfo::onActivate(HWND hwnd, UINT state, HWND hwndActDeact, BOOL fMinimized);

   void browseForRTF();
   void browseForGraphic();
   void browseForSide0AVI();
   void browseForSide1AVI();
   void browseForDrawAVI();
   void onOK();

};

EditInfo::EditInfo(CampaignInfo* info) :
  d_info(info)
{
  ASSERT(d_info != 0);
}

int EditInfo::run(HWND parent)
{
   int result = createDialog(campaignInfoEditDlg, parent);

   return result;

}

BOOL EditInfo::procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
   switch(msg)
   {
      HANDLE_DLG_MSG(WM_INITDIALOG, onInitDialog);
      HANDLE_DLG_MSG(WM_DESTROY, onDestroy);
      HANDLE_DLG_MSG(WM_COMMAND, onCommand);
      HANDLE_DLG_MSG(WM_CLOSE, onClose);
//    HANDLE_DLG_MSG(WM_ACTIVATE, onActivate);
      default:
         return FALSE;
   }
   return TRUE;
}

BOOL EditInfo::onInitDialog(HWND hwnd, HWND hwndFocus, LPARAM lParam)
{
  SetDlgItemText(hwnd, CIE_SCENARIODESCRIPTION, d_info->getDescription());
  SetDlgItemText(hwnd, CIE_RTFFILENAME, d_info->getRTFFileName());
  SetDlgItemText(hwnd, CIE_GRAPHICFILENAME, d_info->getGraphicFileName());

  char buf[100];
  wsprintf(buf, "%s Victory AVI File", scenario->getSideName(0));
  SetDlgItemText(hwnd, CIE_SIDE0NAME, buf);
  SetDlgItemText(hwnd, CIE_SIDE0AVIFILENAME, d_info->getSide0AVIFileName());

  wsprintf(buf, "%s Victory AVI File", scenario->getSideName(1));
  SetDlgItemText(hwnd, CIE_SIDE1NAME, buf);
  SetDlgItemText(hwnd, CIE_SIDE1AVIFILENAME, d_info->getSide1AVIFileName());

  SetDlgItemText(hwnd, CIE_DRAWAVIFILENAME, d_info->getDrawAVIFileName());

  return TRUE;
}

void EditInfo::onDestroy(HWND hwnd)
{

}

void EditInfo::onClose(HWND hwnd)
{

}

void EditInfo::onCommand(HWND hwnd, int id, HWND hwndCtl, UINT codeNotify)
{
  switch(id)
  {
    case CIE_BROWSEFORRTF:
      browseForRTF();
      break;

    case CIE_BROWSEFORGRAPHIC:
      browseForGraphic();
      break;

    case CIE_BROWSEFORSIDE0AVI:
      browseForSide0AVI();
      break;

    case CIE_BROWSEFORSIDE1AVI:
      browseForSide1AVI();
      break;

    case CIE_BROWSEFORDRAWAVI:
      browseForDrawAVI();
      break;

    case IDOK:
      if(codeNotify == BN_CLICKED)
      {
        onOK();
      }
      else
        break;

    case IDCANCEL:
      EndDialog(hwnd, id);
      break;
  }

}

static char rtfFilter[] = "RTF Files\0*.rtf\0";
static char graphicFilter[] = "BMP Files\0*.bmp\0AVI Files\0*.avi\0";


void EditInfo::browseForRTF()
{
  char buf[MAX_PATH];

  if(BrowseForFile::load(buf, rtfFilter))
  {
//  parseFileName(buf);
    d_info->setRTFFileName(buf);
    SetDlgItemText(getHWND(), CIE_RTFFILENAME, d_info->getRTFFileName());
  }
}

void EditInfo::browseForGraphic()
{
  char buf[MAX_PATH];

  if(BrowseForFile::load(buf, graphicFilter))
  {
//  parseFileName(buf);
    d_info->setGraphicFileName(buf);
    SetDlgItemText(getHWND(), CIE_GRAPHICFILENAME, d_info->getGraphicFileName());
  }
}

void EditInfo::browseForSide0AVI()
{
  char buf[MAX_PATH];

  if(BrowseForFile::load(buf, graphicFilter))
  {
    d_info->setSide0AVIFileName(buf);
    SetDlgItemText(getHWND(), CIE_SIDE0AVIFILENAME, d_info->getSide0AVIFileName());
  }
}

void EditInfo::browseForSide1AVI()
{
  char buf[MAX_PATH];

  if(BrowseForFile::load(buf, graphicFilter))
  {
    d_info->setSide1AVIFileName(buf);
    SetDlgItemText(getHWND(), CIE_SIDE1AVIFILENAME, d_info->getSide1AVIFileName());
  }
}

void EditInfo::browseForDrawAVI()
{
  char buf[MAX_PATH];

  if(BrowseForFile::load(buf, graphicFilter))
  {
    d_info->setDrawAVIFileName(buf);
    SetDlgItemText(getHWND(), CIE_DRAWAVIFILENAME, d_info->getDrawAVIFileName());
  }
}

#if 0
/*
 *
 */

void EditInfo::parseFileName(char* fileName)
{

}
#endif
void EditInfo::onOK()
{
  char buf[500];
  GetDlgItemText(getHWND(), CIE_SCENARIODESCRIPTION, buf, MaxDescriptionLength);
  d_info->setDescription(buf);
}

//void EditInfo::onActivate(HWND hwnd, UINT state, HWND hwndActDeact, BOOL fMinimized);


Boolean CampaignInfoEditor::edit(HWND parent, CampaignData* campData)
{
   Boolean oldPause = GameControl::pause(True);

   CampaignInfo& info  = campData->getCampaignInfo();

   CampaignInfo oldInfo = info;

   EditInfo* edit = new EditInfo(&info);
   ASSERT(edit != 0);

   int result = edit->run(parent);

   if(result != IDOK)
   {
      campData->setCampaignInfo(oldInfo);
   }

   delete edit;

   GameControl::pause(oldPause);

   return (result == IDOK);

}
/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.2  2001/06/18 08:25:44  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
