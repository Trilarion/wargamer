/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef MW_EDIT_HPP
#define MW_EDIT_HPP

#ifndef __cplusplus
#error mw_edit.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 * Map Window Editor Creator
 * By putting edit creators in here, it makes mapwind closer to
 * being isolated from the editors.
 *
 *----------------------------------------------------------------------
 */

#include "mwed_int.hpp"
#include "c_cond.hpp"
#include "campEditDll.h"

class PixelPoint;
class MapWindow;
class CampaignData;
// class MainCampaignWindow;
class CampaignWindowsInterface;

class CAMPEDIT_DLL MapWindowEditors : public MWEditInterface {
   MapWindow*           d_mapWind;
   CampaignData*        d_campData;
   CampaignWindowsInterface* d_campWind;

 public:
   MapWindowEditors(MapWindow* mapWind, CampaignData* campData, CampaignWindowsInterface* d_campWind);

   /*
    * Implementation of MWEditInterface
    */

   void editDestroyed(EditDialog* edit);
   void itemChanged();
   void townSelected(ITown ob);
   void setTrackMode(TrackMode mode);

#ifdef CUSTOMIZE   // used by Data Sanity Check routines
   void runOBEdit(ICommandPosition cpi);
   void runTownEdit(ITown iTown);
   void runProvinceEdit(IProvince iProv);
   void runConnectionEdit(ITown iTown);
   void runConditionEdit(WG_CampaignConditions::Conditions* condition);
#endif

   /*
    * Used by Editors
    */

   void toggleEditMode(EditMode mode);
   void newObject();
   void provinceSelected(IProvince ob);
   void editObject();
   void editObject(const MapSelect& info, const PixelPoint& p);


 private:   
   void setEditMode(EditMode mode);
   void newTown(const Location& l);
   void newProvince(const Location& l);
   void newOB();
   void editConditions(WG_CampaignConditions::Conditions* c = 0);
   void enableEditModes();
   void editTown(ITown itown, const PixelPoint& p);
   void editProvince(IProvince iprov, const PixelPoint& p);
   void editConnection(ITown itown, const PixelPoint& p);
   void editOB(const StackedUnitList* unitList, const PixelPoint& p);
};

#endif /* MW_EDIT_HPP */

