/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1995, Steven Morle-Green, All Rights Reserved
 *----------------------------------------------------------------------
 *
 * Support for editor dialogs
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/06/13 08:52:37  greenius
 * Initial Import
 *
 * Revision 1.1  1995/12/07 09:15:20  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "editsup.hpp"
// #include "wargame.hpp"
#include "scenario.hpp"

void makeSideCombo(HWND hCombo)
{
   ASSERT(hCombo != NULL);
   int index = SendMessage(hCombo, CB_ADDSTRING, 0, (LPARAM) "Neutral");
   ASSERT(index != CB_ERR);
   ASSERT(index != CB_ERRSPACE);
   SendMessage(hCombo, CB_SETITEMDATA, index, SIDE_Neutral);

   for(int side = 0; side < scenario->getNumSides(); side++)
   {
      int index = SendMessage(hCombo, CB_ADDSTRING, 0, (LPARAM) scenario->getSideName(Side(side)));
      ASSERT(index != CB_ERR);
      ASSERT(index != CB_ERRSPACE);
      SendMessage(hCombo, CB_SETITEMDATA, index, side);
   }
}

void makeNationCombo(HWND hCombo)
{
   ASSERT(hCombo != NULL);

   for(Nationality n = 0; n < scenario->getNumNations(); n++)
   {
      int index = SendMessage(hCombo, CB_ADDSTRING, 0, (LPARAM) scenario->getNationName(n));
      ASSERT(index != CB_ERR);
      ASSERT(index != CB_ERRSPACE);
      SendMessage(hCombo, CB_SETITEMDATA, index, n);
   }
}




/*
 *----------------------------------------------------------------------
 * $Log$
 *----------------------------------------------------------------------
 */
