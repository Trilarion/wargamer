/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1995, Steven Morle-Green, All Rights Reserved
 *----------------------------------------------------------------------
 *
 * Edit Town Dialogue
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "townedit.hpp"
#include "mwed_int.hpp"
#include "mapwind.hpp"     // This may be replaced by MWEditInterface

#include "app.hpp"
#include "resdef.h"
#include "campdint.hpp"
#include "scenario.hpp"
#include "misc.hpp"
#include "grtypes.hpp"
#include "editsup.hpp"
#include "winctrl.hpp"
#include "terrain.hpp"
#include "help.h"
#include "memptr.hpp"
#include "random.hpp"
// #include <string.hpp>

/*
 * New Version that uses my own custom property sheet style dialogues
 */


enum {
   TEP_General,
   TEP_Position,
   TEP_Resource,

   TEP_HowMany
};

/*
 * Classes for each page
 */

class TownEditPage : public ModelessDialog {
   DLGTEMPLATE* dialog;
protected:
   TownEdit* edit;         // Pointer to master data
public:
   TownEditPage(TownEdit* p, const char* dlgName);

   DLGTEMPLATE* getDialog() const { return dialog; }
   virtual const char* getTitle() const = 0;
   HWND create();
   // void enable();
   // void disable();
   virtual void updateValues() = 0;
protected:
   void setPosition();
   Town* getTown() { return edit->getTown(); }

   CampaignData* getCampaignData() const { return edit->getCampaignData(); }
};


class TownEditGeneral : public TownEditPage {
public:
   TownEditGeneral(TownEdit* p) : TownEditPage(p, townEditGeneralPage) { }
   const char* getTitle() const { return "General"; }

   void setProvince();

private:
   void initControls();
   void updateValues();

   BOOL procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
   BOOL onInitDialog(HWND hwnd, HWND hwndFocus, LPARAM lParam);
   void onDestroy(HWND hwnd);
};

class TownEditPosition : public TownEditPage {
public:
   TownEditPosition(TownEdit* p) : TownEditPage(p, townEditPositionPage) { }
   const char* getTitle() const { return "Position"; }
   void updateValues();
private:
   void initControls();

   BOOL procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
   BOOL onInitDialog(HWND hwnd, HWND hwndFocus, LPARAM lParam);
   void onDestroy(HWND hwnd);
};

class TownEditResource : public TownEditPage {
public:
   TownEditResource(TownEdit* p) : TownEditPage(p, townEditResourcePage) { }
   const char* getTitle() const { return "Resources"; }
   void updateValues();
private:
   void initControls();

   BOOL procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
   BOOL onInitDialog(HWND hwnd, HWND hwndFocus, LPARAM lParam);
   void onDestroy(HWND hwnd);
};

/*=========================================================================
 * Useful support for drop down Combo controls
 */

static ComboInit comboAlignData[] = {
   { NA_TOPLEFT,     "Top Left"     },
   { NA_TOP,         "Top"          },
   { NA_TOPRIGHT,    "Top Right"    },
   { NA_RIGHT,       "Right"        },
   { NA_BOTTOMRIGHT, "Bottom Right" },
   { NA_BOTTOM,      "Bottom"       },
   { NA_BOTTOMLEFT,  "Bottom Left"  },
   { NA_LEFT,        "Left"         },
   { -1, 0                          }
};

static ComboInit comboSizeData[] = {
   { TOWN_Capital,   "Capital"      },
   { TOWN_City,      "City"         },
   { TOWN_Town,      "Town"         },
   { TOWN_Other,     "Other"        },
   { -1, 0                          }
};

static ComboInit comboTerrainData[] = {
   { 0,              "Default"      },
   { -1, 0                          }
};


/*=========================================================================
 * TownEdit class
 */

const int TownEdit::nPages = TEP_HowMany;

// TownEdit::TownEdit(CampaignData* cData, MapWindow* p, ITown it, PixelPoint& pos)
TownEdit::TownEdit(MWEditInterface* editControl, CampaignData* cData, MapWindow* p, ITown it, const PixelPoint& pos) :
   d_editControl(editControl)
{
   campData = cData;
   parent = p;
   townID = it;
   town = &campData->getTown(it);
   oldValues = *town;

   POINT pt = pos;
   ClientToScreen(parent->getHWND(), &pt);
   position = pt;

   pages = new TownEditPage*[nPages];

   for(int i = 0; i < nPages; i++)
      pages[i] = 0;
   tabHwnd = NULL;                  // Tab Dialog handle
   curPage = 0;
   SetRectEmpty(&tdRect);

   // Start in move mode
   d_editControl->setTrackMode(MWTM_Position);

   HWND dhwnd = createDialog(townEditDialogName, parent->getHWND());
   ASSERT(dhwnd != NULL);
}

TownEdit::~TownEdit()
{
   if(pages)
   {
      delete[] pages;
      pages = 0;
   }
}

TrackMode TownEdit::onSelect(TrackMode mode, const MapSelect& info)
{
   if(mode == MWTM_Position)
   {
      town->setLocation(info.mouseLocation);

      for(IConnection i = 0; i < MaxConnections; i++)
      {
        IConnection ic = town->getConnection(i);
        if(ic != NoConnection)
          campaignData->calculateConnectionLength(ic);
      }

      d_editControl->itemChanged();
   }
   else if(mode == MWTM_Province)
   {
      campData->moveTownToProvince(info.selectedProvince, townID);
      d_editControl->itemChanged();
      setProvince();
   }
#ifdef DEBUG
   else if(mode != MWTM_None)
      FORCEASSERT("Unexpected TrackMode");
#endif

   return mode;
}

#if 0 // Replace this with onSelect
void TownEdit::gotPosition(Location* l)
{
   if(l)
   {
      debugLog("Location changed\n");
      town->setLocation(*l);
      d_editControl->itemChanged();
      // setModify();
      // setApply();
   }
}
#endif

BOOL TownEdit::procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
   switch(msg)
   {
      case WM_INITDIALOG:
         HANDLE_WM_INITDIALOG(hWnd, wParam, lParam, onInitDialog);
         break;

      case WM_DESTROY:
         HANDLE_WM_DESTROY(hWnd, wParam, lParam, onDestroy);
         break;
      case WM_COMMAND:
         HANDLE_WM_COMMAND(hWnd, wParam, lParam, onCommand);
         break;

      case WM_CLOSE:
         HANDLE_WM_CLOSE(hWnd, wParam, lParam, onClose);
         break;

      case WM_NOTIFY:
         return HANDLE_WM_NOTIFY(hWnd, wParam, lParam, onNotify);

      case WM_ACTIVATE:
         HANDLE_WM_ACTIVATE(hWnd, wParam, lParam, onActivate);
         break;

      case WM_CONTEXTMENU:
         HANDLE_WM_CONTEXTMENU(hWnd, wParam, lParam, onContextMenu);
         break;

      case WM_HELP:
         HANDLE_WM_HELP(hWnd, wParam, lParam, onHelp);
         break;

      default:
         return FALSE;
   }

   return TRUE;
}

static const DWORD ids[] = {

   /*
    * from main dialog
    */

   IDTE_TOWN_NAME,     IDH_TE_TownName,
   IDTE_DELETE,        IDH_TE_Delete,
   IDTE_CANCEL,        IDH_TE_Cancel,
   IDTE_OK,            IDH_TE_Ok,

   /*
    * from General tab
    */

   IDTE_STATE_NAME,    IDH_TE_Province,
   IDTE_SET_STATE,     IDH_TE_Set,
   IDTE_OFFSCREEN,     IDH_TE_OffScreen,
   IDTE_SIDE,          IDH_TE_Owner,
   IDTE_TYPE,          IDH_TE_TownType,
   IDTE_SIEGABLE,      IDH_TE_Siegeable,
   IDTE_VICTORYA,      IDH_TE_Victory,
   IDTE_VICTORYB,      IDH_TE_Victory,
   IDTE_STRENGTH,      IDH_TE_Strength,
   IDTE_STACKING,      IDH_TE_Stacking,
   IDTE_FORTIFICATION, IDH_TE_Fortifications,
   IDTE_POLITICAL,     IDH_TE_Political,
   IDTE_HELPID,        IDH_EDIT_HelpIDEdit,
   IDTE_DEFAULTHELPID, IDH_EDIT_DefaultHelpID,

   /*
    * from Position tab
    */

   IDTE_ALIGN,         IDH_TE_Align,
   IDTE_XOFFSET,       IDH_TE_XPosition,
   IDTE_YOFFSET,       IDH_TE_YPosition,
   IDTE_TERRAIN,       IDH_TE_Terrain,
   IDTE_MOVE,          IDH_TE_MoveTown,

   /*
    * from Resources tab
    */

   IDTE_SUPPLY,        IDH_TE_SupplyRate,
   IDTE_FORAGE,        IDH_TE_Forage,
   IDTE_SUPPLYSOURCE,  IDH_TE_SupplySource,
   IDTE_SUPPLYDEPOT,   IDH_TE_SupplyDepot,
   IDTE_MANPOWER,      IDH_TE_ManpowerRate,
   IDTE_MPTRANSFER,    IDH_TE_Transferable,
   IDTE_RESOURCE,      IDH_TE_ResourceRate,

   0, 0
};

void TownEdit::onContextMenu(HWND hwnd, HWND hwndCtl, int x, int y)
{
//  WinHelp(hwndCtl, "help\\Wg95Help.hlp", HELP_CONTEXTMENU, (DWORD)(LPVOID)ids);
}

BOOL TownEdit::onHelp(HWND hwnd, LPHELPINFO lparam)
{
  LPHELPINFO lphi = (LPHELPINFO)lparam;
  if (lphi->iContextType == HELPINFO_WINDOW)   // must be for a control
  {
//  WinHelp ((HWND)lphi->hItemHandle, "help\\Wg95Help.hlp", HELP_WM_HELP, (DWORD)(LPVOID)ids);
  }
  return TRUE;
}


void TownEdit::initControls()
{
   const char* s = town->getName();

   if(s == 0)
      s = "New Town";

   SetDlgItemText(getHWND(), IDTE_TOWN_NAME, s);
}

BOOL TownEdit::onInitDialog(HWND hwnd, HWND hwndFocus, LPARAM lParam)
{
#ifdef DEBUG
   debugLog("TownEdit::onInitDialog()\n");
#endif

   /*
    * Setup the tabbled dialog
    */

   /*
    * Set up a minimum size (this is in dialog units)
    */

   RECT rcTab;
   rcTab.left = 0;
   rcTab.top = 0;
   rcTab.right = 170;
   rcTab.bottom = 80;

   pages[TEP_General] = new TownEditGeneral(this);
   pages[TEP_Position] = new TownEditPosition(this);
   pages[TEP_Resource] = new TownEditResource(this);

   for(int i = 0; i < nPages; i++)
   {
      DLGTEMPLATE* dialog = pages[i]->getDialog();

      ASSERT(dialog != NULL);

      /*
       * Adjust for maximum size
       */

      if(dialog->cx > rcTab.right)
         rcTab.right = dialog->cx;
      if(dialog->cy > rcTab.bottom)
         rcTab.bottom = dialog->cy;
   }

#ifdef DEBUG
   debugLog("Largest Dialog (Dialog Units): %ld,%ld,%ld,%ld\n",
      rcTab.left, rcTab.top, rcTab.right, rcTab.bottom);
#endif

   // Get the position of the box (convert from dialog units)

   LONG dbUnits = GetDialogBaseUnits();
   LONG dbX = LOWORD(dbUnits);
   LONG dbY = HIWORD(dbUnits);
   LONG xMargin = (3 * dbX) / 4;
   LONG yTop = (20 * dbY) / 8;
   LONG yMargin = (4 * dbY) / 8;

   // Convert to pixel coordinates

   MapDialogRect(hwnd, &rcTab);

#ifdef DEBUG
   debugLog("Largest Dialog (Pixels): %ld,%ld,%ld,%ld\n",
      rcTab.left, rcTab.top, rcTab.right, rcTab.bottom);
#endif

   /*
    * Create a tabbed window
    * Don't be concerened with the size, because that will
    * be set up in a little while, but we need a handle to
    * a tabbed control to use AdjustRect
    *
    * Do need to set up width, so that AdjustRect doesn't think
    * it needs to do several rows of buttons
    */

   tabHwnd = CreateWindow(
      WC_TABCONTROL, "",
      WS_CHILD | WS_CLIPSIBLINGS | WS_VISIBLE |
         TCS_TOOLTIPS | TCS_RIGHTJUSTIFY | TCS_MULTILINE,
      0, 0, rcTab.right, 100,
      hwnd,
      (HMENU) IDTE_TABBED,
      APP::instance(),
      NULL);

   ASSERT(tabHwnd != NULL);

   debugLog("TabHwnd = %08lx\n", (ULONG) tabHwnd);

   /*
    * Set up the tabbed titles
    * If possible try and get the title from the Dialog's CAPTION
    *
    * This must be done before using AdjustRect
    */

   TC_ITEM tie;
   tie.mask = TCIF_TEXT | TCIF_IMAGE;
   tie.iImage = -1;

   for(i = 0; i < nPages; i++)
   {
      tie.pszText = (char*) pages[i]->getTitle();

      TabCtrl_InsertItem(tabHwnd, i, &tie);
   }

   /*
    * Convert coordinates to pixel coordinates
    * and Move the window
    */

   // Work out complete size with tabs, and shift to desired location

   TabCtrl_AdjustRect(tabHwnd, TRUE, &rcTab);
#ifdef DEBUG
   debugLog("Tab Size: %ld,%ld,%ld,%ld\n",
      rcTab.left, rcTab.top, rcTab.right, rcTab.bottom);
#endif
   OffsetRect(&rcTab, xMargin - rcTab.left, yTop - rcTab.top);
#ifdef DEBUG
   debugLog("Offset Tab: %ld,%ld,%ld,%ld\n",
      rcTab.left, rcTab.top, rcTab.right, rcTab.bottom);
#endif

   // Move tabbed Window

   SetWindowPos(tabHwnd, NULL,
      rcTab.left, rcTab.top,
      rcTab.right - rcTab.left, rcTab.bottom - rcTab.top,
      // rcTab.right - rcTab.left, 50,
      SWP_NOZORDER);

   // Get display area

   CopyRect(&tdRect, &rcTab);
   TabCtrl_AdjustRect(tabHwnd, FALSE, &tdRect);    // Get display area
#ifdef DEBUG
   debugLog("Display Area: %ld,%ld,%ld,%ld\n",
      tdRect.left, tdRect.top, tdRect.right, tdRect.bottom);
#endif

   /*
    * Move the lower buttons (to rcTab.bottom + a bit)
    */

   RECT rcButton;
   LONG x = rcTab.right;
   LONG y = rcTab.bottom + yMargin;

   HWND hwndButton = GetDlgItem(hwnd, IDTE_OK);
   GetWindowRect(hwndButton, &rcButton);
   x -= rcButton.right - rcButton.left;
   SetWindowPos(hwndButton, NULL, x, y, 0, 0, SWP_NOSIZE | SWP_NOZORDER);

#ifdef DEBUG
   debugLog("OK: %ld,%ld,%ld,%ld\n",
      x, y, rcButton.right-rcButton.left, rcButton.bottom-rcButton.top);
#endif


   hwndButton = GetDlgItem(hwnd, IDTE_CANCEL);
   GetWindowRect(hwndButton, &rcButton);
   x -= rcButton.right - rcButton.left + xMargin;
   SetWindowPos(hwndButton, NULL, x, y, 0, 0, SWP_NOSIZE | SWP_NOZORDER);
#ifdef DEBUG
   debugLog("Cancel: %ld,%ld,%ld,%ld\n",
      x, y, rcButton.right-rcButton.left, rcButton.bottom-rcButton.top);
#endif

   hwndButton = GetDlgItem(hwnd, IDTE_DELETE);
   GetWindowRect(hwndButton, &rcButton);
   SetWindowPos(hwndButton, NULL, xMargin, y, 0, 0, SWP_NOSIZE | SWP_NOZORDER);
#ifdef DEBUG
   debugLog("Delete: %ld,%ld,%ld,%ld\n",
      xMargin, y, rcButton.right-rcButton.left, rcButton.bottom-rcButton.top);
#endif




   /*
    * Adjust size of overall dialogue box
    *
    * Would be a good idea to adjust coordinates
    * so that box is on screen.
    *
    * e.g. if off right, adjust coords to be left of given position
    * if off bottom, adjust to be above.
    */

   rcTab.bottom = y + yMargin + rcButton.bottom - rcButton.top;
   rcTab.bottom += GetSystemMetrics(SM_CYCAPTION);
   rcTab.bottom += GetSystemMetrics(SM_CYDLGFRAME) * 2;
   // rcTab.bottom -= rcTab.top;

   rcTab.right += xMargin * 2;
   rcTab.right += GetSystemMetrics(SM_CXDLGFRAME) * 2;
   rcTab.right -= rcTab.left;

#ifdef DEBUG
   debugLog("Overall Window: %ld,%ld,%ld,%ld\n",
      rcTab.left, rcTab.top, rcTab.right, rcTab.bottom);
#endif

   SetWindowPos(hwnd, NULL,
      position.getX(),
      position.getY(),
      rcTab.right,
      rcTab.bottom,
      SWP_NOZORDER);

   /*
    * Initialise pages
    */

   for(i = 0; i < nPages; i++)
      pages[i]->create();

   /*
    * For testing: create an initial dialog
    */

   onSelChanged();

   /*
    * End of tabbed dialog setup
    * Now get on with positioning ourselves and setting the controls
    */

   /*===============================================================
    * Set the position
    * Ought to do a bit of checking to see if it will fit on screen
    */

   initControls();

   return TRUE;
}

void TownEdit::onDestroy(HWND hwnd)
{
   d_editControl->editDestroyed(this);
}

/*
 * All the child handlers are done here as well... to make moving
 * things about easier
 */

void TownEdit::onCommand(HWND hwnd, int id, HWND hwndCtl, UINT codeNotify)
{
   switch(id)
   {


      case IDTE_OK:
         if(checkTownHelpID())
         {
           updateValues();
           d_editControl->itemChanged();
           campData->sortTowns();
           campData->setChanged();
           DestroyWindow(hwnd);
         }
         break;

      case IDTE_CANCEL:
         // restoreValues();
         *town = oldValues;
         d_editControl->itemChanged();
         DestroyWindow(hwnd);
         break;

      case IDTE_DELETE:
         {
            d_editControl->townSelected(0);
            TownList& tl = campData->getTowns();
            campData->removeTown(tl.getID(town));
            campData->setChanged();
            parent->requestRedraw(TRUE);
            DestroyWindow(hwnd);
         }
         break;

      case IDTE_TOWN_NAME:
         onTownName(hwndCtl, codeNotify);
         break;

      /*
       * General sub-dialog
       */

      case IDTE_STATE_NAME:
         onState(hwndCtl, codeNotify);
         break;

      case IDTE_SET_STATE:
         d_editControl->setTrackMode(MWTM_Province);  // wantProvince(this);
         break;

      case IDTE_SIDE:
         onSide(hwndCtl, codeNotify);
         break;
#if 0
      case IDTE_START_SIDE:
         onStartSide(hwndCtl, codeNotify);
         break;
#endif
      case IDTE_TYPE:
         onSize(hwndCtl, codeNotify);
         break;
#if 0
      case IDTE_CAPITAL:
         break;
#endif
      case IDTE_SIEGABLE:
         break;
      case IDTE_VICTORYA:
         break;
      case IDTE_VICTORYASPIN:
         break;
      case IDTE_VICTORYB:
         break;
      case IDTE_VICTORYBSPIN:
         break;
      case IDTE_STACKING:
         break;
      case IDTE_STACKINGSPIN:
         break;
      case IDTE_FORTIFICATION:
         break;
      case IDTE_FORTIFICATIONSPIN:
         break;
      case IDTE_POLITICAL:
         break;
      case IDTE_POLITICALSPIN:
         break;

      /*
       * Position
       */

      case IDTE_ALIGN:
         onAlign(hwndCtl, codeNotify);
         break;
      case IDTE_XOFFSET:
      case IDTE_YOFFSET:
         onOffset(hwnd, id, hwndCtl, codeNotify);
         break;
      case IDTE_XUPDOWN:
      case IDTE_YUPDOWN:
         break;
      case IDTE_TERRAIN:
         onTerrain(hwndCtl, codeNotify);
         break;
      case IDTE_MOVE:
         onMove(hwndCtl, codeNotify);
         break;

      /*
       * Resource
       */

      case IDTE_SUPPLY:
      case IDTE_FORAGE:
         break;
      case IDTE_SUPPLYSPIN:
      case IDTE_FORAGESPIN:
         break;
      case IDTE_SUPPLYSOURCE:
         break;
      case IDTE_SUPPLYDEPOT:
         break;
      case IDTE_MANPOWER:
         break;
      case IDTE_MANPOWERSPIN:
         break;
#if 0
      case IDTE_FREEMANPOWER:
         break;
      case IDTE_FREEMANPOWERSPIN:
         break;
#endif
      case IDTE_MPTRANSFER:
         break;
      case IDTE_RESOURCE:
         break;
      case IDTE_RESOURCESPIN:
         break;

      case IDTE_DEFAULTHELPID:
      {
         if(codeNotify == BN_CLICKED)
         {
            SetDlgItemInt(pages[TEP_General]->getHWND(), IDTE_HELPID, NoHelpID, FALSE);
            if(town)
              town->setHelpID(NoHelpID);
         }
         break;
      }

      case IDTE_HELPID:
      {
         if(codeNotify == EN_CHANGE)
         {
           ASSERT(town != 0);

           if(town)
           {
             HelpID helpID = (HelpID)GetDlgItemInt(pages[TEP_General]->getHWND(), id, NULL, FALSE);
             town->setHelpID(helpID);
           }
         }

         break;
      }

#if 0
      case IDTE_FREERESOURCE:
         break;
      case IDTE_FREERESOURCESPIN:
         break;
#endif
#ifdef DEBUG
      default:
         debugLog("TownEdit::onCommand(), unknown id %d\n", id);
#endif
   }
}

/*
 * Check town help id value to make sure it is in range
 *
 * Each help id must be unique. Town IDs will for now be in the range of 1500-1999
 */

Boolean TownEdit::checkTownHelpID()
{

  const UINT minRange = 1500;
  const UINT maxRange = 1999;

  static const char msgOutOfRange[] = "Towns's Help ID must be in the range of 1500 and 1999, or press default-ID button";

  if(town)
  {
    HelpID helpID = (HelpID)GetDlgItemInt(pages[TEP_General]->getHWND(), IDTE_HELPID, NULL, FALSE);

    if( (helpID != NoHelpID) && (helpID < minRange || helpID > maxRange) )
    {
      int result = MessageBox(getHWND(), msgOutOfRange, "Out of Range", MB_OK);
      ASSERT(result == IDOK);
      return False;
    }

    /*
     * Go through all towns and make sure Help ID is not duplicated
     */

//  const Armies* armies = &campData->getArmies();

    static const char msgIDRepeated[] = "Towns's HelpID(%d) has already been defined. Choose a different value in the range of 1500 to 1999, or press default-ID button";

    const TownList& tl = campData->getTowns();

    TownIter tIter(tl);

    while(++tIter)
    {
      const Town& town = tIter.current();

      if( (town.getHelpID() != NoHelpID) && (helpID == town.getHelpID() && townID != tIter.currentID()) )
      {
        char buf[200];
        wsprintf(buf, msgIDRepeated, helpID);

        int result = MessageBox(getHWND(), buf, "ID Duplicated", MB_OK);
        ASSERT(result == IDOK);
        return False;
      }
    }
  }

  return True;
}


void TownEdit::onClose(HWND hwnd)
{
   onCommand(hwnd, IDTE_CANCEL, 0, 0);
}

void TownEdit::kill()
{
   onClose(getHWND());
}

void TownEdit::onActivate(HWND hwnd, UINT state, HWND hwndActDeact, BOOL fMinimized)
{
   if((state == WA_ACTIVE) || (state == WA_CLICKACTIVE))
      d_editControl->townSelected(townID);
}

LRESULT TownEdit::onNotify(HWND hWnd, int id, NMHDR* lpNMHDR)
{
#ifdef DEBUG
   debugLog("TownEdit::onNotify(%08lx %d %08lx %d %d)\n",
      (ULONG) hWnd,
      (int) id,
      (ULONG) lpNMHDR->hwndFrom,
      (int) lpNMHDR->idFrom,
      (int) lpNMHDR->code);
#endif

   // Assume its from the tabbed dialog

   if(lpNMHDR->idFrom == IDTE_TABBED)
   {
      switch(lpNMHDR->code)
      {
      case TCN_SELCHANGING:
         return FALSE;

      case TCN_SELCHANGE:
         onSelChanged();
         break;
      }
   }

   return TRUE;
}

void TownEdit::onSelChanged()
{
   int iSel = TabCtrl_GetCurSel(tabHwnd);
   if(curPage != iSel)
      pages[curPage]->show(false);

   pages[iSel]->show(true);
   curPage = iSel;
}


void TownEdit::updateValues()
{
   for(int i = 0; i < nPages; i++)
      pages[i]->updateValues();
}


void TownEdit::onTownName(HWND hwnd, UINT codeNotify)
{
   if(codeNotify == EN_CHANGE)
   {
      char* newName = getItemIDText(IDTE_TOWN_NAME);
      const char* oldName = town->getName();

      if(stringsDiffer(newName, oldName))
         town->setName(newName);

      debugLog("Name changed\n");

      parent->requestRedraw(FALSE);
      // setApply();
   }
}

void TownEdit::onAlign(HWND hCombo, UINT codeNotify)
{
   if(codeNotify == CBN_SELCHANGE)
   {
      int index = SendMessage(hCombo, CB_GETCURSEL, 0, 0);

      debugLog("TownEdit::onAlign: index=%d\n", index);

      if(index != CB_ERR)
      {
         NameAlign newAlign = (NameAlign) SendMessage(hCombo, CB_GETITEMDATA, index, 0);
         town->setNameAlign(newAlign);

         debugLog("newAlign=%d\n", (int) newAlign);


         if(newAlign != oldValues.getNameAlign())
         {
            debugLog("Name Alignment changed\n");

            // setModify();
            // setApply();
         }
         parent->requestRedraw(FALSE);
      }
   }
}

void TownEdit::onOffset(HWND hDlg, int id, HWND hEdit, UINT codeNotify)
{
   if(codeNotify == EN_CHANGE)
   {
      debugLog("TownEdit::onOffset\n");

      SBYTE newVal;
      BOOL success;
      newVal = (SBYTE) GetDlgItemInt(hDlg, id, &success, TRUE);
      if(success)
      {
         if(id == IDTE_XOFFSET)
            town->setNameX(newVal);
         else if(id == IDTE_YOFFSET)
            town->setNameY(newVal);
#ifdef DEBUG
         else
            debugLog("onNameOffset called with illegal id (%d)\n", id);
#endif
         parent->requestRedraw(FALSE);

      }
   }
}

void TownEdit::onTerrain(HWND hCombo, UINT codeNotify)
{
   {
      int index = SendMessage(hCombo, CB_GETCURSEL, 0, 0);
      if(index != CB_ERR)
      {
         UBYTE newTerrain = (UBYTE) SendMessage(hCombo, CB_GETITEMDATA, index, 0);
         town->setTerrain(newTerrain);
         if(newTerrain != oldValues.getTerrain())
         {
            debugLog("Terrain changed\n");
         }
      }
   }
}

void TownEdit::onMove(HWND hCombo, UINT codeNotify)
{
   d_editControl->setTrackMode(MWTM_Position);  // wantPosition(this);
}


void TownEdit::onSide(HWND hCombo, UINT codeNotify)
{
   if(codeNotify == CBN_SELCHANGE)
   {
      int index = SendMessage(hCombo, CB_GETCURSEL, 0, 0);
      if(index != CB_ERR)
      {
         Side newSide = (Side) SendMessage(hCombo, CB_GETITEMDATA, index, 0);
         town->setSide(newSide);
         if(newSide != oldValues.getSide())
         {
            debugLog("Side changed\n");
         }
         parent->requestRedraw(FALSE);
      }
   }
}

#if 0
void TownEdit::onStartSide(HWND hCombo, UINT codeNotify)
{
   if(codeNotify == CBN_SELCHANGE)
   {
      int index = SendMessage(hCombo, CB_GETCURSEL, 0, 0);
      if(index != CB_ERR)
      {
         Side newSide = (Side) SendMessage(hCombo, CB_GETITEMDATA, index, 0);
         town->setStartSide(newSide);
         if(newSide != oldValues.getStartSide())
         {
            debugLog("Start Side changed\n");
         }
      }
   }
}
#endif

void TownEdit::onState(HWND hCombo, UINT codeNotify)
{
   if(codeNotify == CBN_SELCHANGE)
   {
      int index = SendMessage(hCombo, CB_GETCURSEL, 0, 0);
      if(index != CB_ERR)
      {
         IProvince newState = (IProvince) SendMessage(hCombo, CB_GETITEMDATA, index, 0);

         debugLog("Index %d, newState=%d, oldState=%d\n",
            (int) index,
            (int) newState,
            (int) oldValues.getProvince());

         campData->moveTownToProvince(newState, townID);
         d_editControl->itemChanged();
         setProvince();

         // town->setProvince(newState);

#ifdef DEBUG
         if(newState != oldValues.getProvince())
         {
            debugLog("Province changed\n");
         }
#endif
      }
   }
}

void TownEdit::setProvince()
{
   TownEditGeneral* page = (TownEditGeneral*) pages[TEP_General];
   page->setProvince();
}

void TownEdit::onSize(HWND hCombo, UINT codeNotify)
{
   if(codeNotify == CBN_SELCHANGE)
   {
      int index = SendMessage(hCombo, CB_GETCURSEL, 0, 0);
      if(index != CB_ERR)
      {
         /*
          * TODO: Add code to handle capitals in here!
          */

         if(town->getIsCapital() && (town->getProvince() != NoProvince))
            campData->setCapital(town->getProvince(), NoTown);

         TownSize newSize = (TownSize) SendMessage(hCombo, CB_GETITEMDATA, index, 0);
         town->setSize(newSize);

         if(town->getIsCapital() && (town->getProvince() != NoProvince))
            campData->setCapital(town->getProvince(), townID);

         if(newSize != oldValues.getSize())
         {
            debugLog("Size changed\n");
         }

         parent->requestRedraw(FALSE);
      }
   }
}

/*=================================================================
 * Sub-Dialog Implementation
 */

TownEditPage::TownEditPage(TownEdit* p, const char* dlgName)
{
   edit = p;

   HRSRC rhTep = FindResource(NULL, dlgName, RT_DIALOG);
   ASSERT(rhTep != NULL);
   HGLOBAL lphTep = (HGLOBAL) LoadResource(NULL, rhTep);
   ASSERT(lphTep != NULL);
   dialog = (LPDLGTEMPLATE) LockResource(lphTep);
   ASSERT(dialog != NULL);

   /*
    * Make sure it has suitable flags
    */

#if 0       // For some reason I can't write to this any more!
   dialog->style &= ~(
         WS_CAPTION |
         WS_POPUP |
         WS_SYSMENU |
         WS_VISIBLE
         );
   dialog->style |=
         WS_CHILD |
         // WS_VISIBLE |
         // WS_DISABLED |
         DS_3DLOOK |
         DS_CONTROL |
         // DS_RECURSE |
         WS_DLGFRAME;
   // dialog->dwExtendedStyle &= ~0;
   // dialog->dwExtendedStyle |= 0;
#endif
}

HWND TownEditPage::create()
{
   ASSERT(hWnd == NULL);      // Already created?

#if 0
   HWND hDialog = ::CreateDialogIndirectParam(
      APP::instance(),
      dialog,
      edit->getHWND(),
      baseDialogProc,
      (LPARAM) this);
#else
   HWND hDialog = createDialogIndirect(dialog, edit->getHWND());
#endif

   ASSERT(hDialog != NULL);

   return hDialog;
}

// void TownEditPage::enable()
// {
//    ASSERT(hWnd != NULL);
//    ShowWindow(hWnd, SW_SHOW);
// }
//
// void TownEditPage::disable()
// {
//    ASSERT(hWnd != NULL);
//    ShowWindow(hWnd, SW_HIDE);
// }


void TownEditPage::setPosition()
{
   const RECT& r = edit->getDialogRect();

#ifdef DEBUG
   debugLog("setPosition(%ld,%ld,%ld,%ld)\n",
      r.left, r.top, r.right, r.bottom);
#endif

   SetWindowPos(getHWND(), HWND_TOP, r.left, r.top, r.right-r.left, r.bottom-r.top, 0);
}

/*=========================================================================
 * General sub-Dialog
 */

BOOL TownEditGeneral::procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
   switch(msg)
   {
      case WM_INITDIALOG:
         HANDLE_WM_INITDIALOG(hWnd, wParam, lParam, onInitDialog);
         break;

      case WM_DESTROY:
         HANDLE_WM_DESTROY(hWnd, wParam, lParam, onDestroy);
         break;
      case WM_COMMAND:
         HANDLE_WM_COMMAND(hWnd, wParam, lParam, edit->onCommand);
         break;

      default:
         return FALSE;
   }

   return TRUE;
}

void TownEditGeneral::updateValues()
{
   Town* town = edit->getTown();

   // town->setIsCapital(getButtonIDCheck(IDTE_CAPITAL));
   town->setSiegeable(getButtonIDCheck(IDTE_SIEGABLE));
   town->setOffScreen(getButtonIDCheck(IDTE_OFFSCREEN));

   town->setVictory(0, (Attribute) SendDlgItemMessage(hWnd, IDTE_VICTORYASPIN, UDM_GETPOS, 0, 0));
   town->setVictory(1, (Attribute) SendDlgItemMessage(hWnd, IDTE_VICTORYBSPIN, UDM_GETPOS, 0, 0));
   town->setStacking((Attribute) SendDlgItemMessage(hWnd, IDTE_STACKINGSPIN, UDM_GETPOS, 0, 0));
   town->setFortifications((Attribute) SendDlgItemMessage(hWnd, IDTE_FORTIFICATIONSPIN, UDM_GETPOS, 0, 0));
   town->setPolitical((Attribute) SendDlgItemMessage(hWnd, IDTE_POLITICALSPIN, UDM_GETPOS, 0, 0));
   town->setStrength((Attribute) SendDlgItemMessage(hWnd, IDTE_STRENGTHSPIN, UDM_GETPOS, 0, 0));

   debugLog("TownEditGeneral: Restored values\n");
}

void TownEditGeneral::initControls()
{
   Town* town = edit->getTown();

   // setComboIDValue(IDTE_STATE_NAME, town->getProvince());
   setProvince();
   setComboIDValue(IDTE_SIDE, town->getSide());
#if 0
   setComboIDValue(IDTE_START_SIDE, town->getStartSide());
#endif
   setComboIDValue(IDTE_TYPE, town->getSize());

   // setButtonIDCheck(IDTE_CAPITAL, town->getIsCapital());
   setButtonIDCheck(IDTE_SIEGABLE, town->getSiegeable());

   setButtonIDCheck(IDTE_OFFSCREEN, town->isOffScreen());

   SetDlgItemInt(getHWND(), IDTE_HELPID, town->getHelpID(), FALSE);

#if 0
   SendDlgItemMessage(hWnd, IDTE_VICTORYASPIN, UDM_SETPOS, 0, MAKELONG(town->getVictory(0), 0));
   SendDlgItemMessage(hWnd, IDTE_VICTORYBSPIN, UDM_SETPOS, 0, MAKELONG(town->getVictory(1), 0));
   SendDlgItemMessage(hWnd, IDTE_STACKINGSPIN, UDM_SETPOS, 0, MAKELONG(town->getStacking(), 0));
   SendDlgItemMessage(hWnd, IDTE_FORTIFICATIONSPIN, UDM_SETPOS, 0, MAKELONG(town->getFortifications(), 0));
   SendDlgItemMessage(hWnd, IDTE_POLITICALSPIN, UDM_SETPOS, 0, MAKELONG(town->getPolitical(), 0));
#endif
   setSpinIDValue(IDTE_VICTORYASPIN, town->getVictory(0));
   setSpinIDValue(IDTE_VICTORYBSPIN, town->getVictory(1));
   setSpinIDValue(IDTE_STACKINGSPIN, town->getStacking());
   setSpinIDValue(IDTE_FORTIFICATIONSPIN, town->getFortifications());
   setSpinIDValue(IDTE_POLITICALSPIN, town->getPolitical());
   setSpinIDValue(IDTE_STRENGTHSPIN, town->getStrength());
}

void TownEditGeneral::setProvince()
{
   const Town* town = edit->getTown();
   IProvince iProv = town->getProvince();

   setComboIDValue(IDTE_STATE_NAME, town->getProvince());


   String info;

   if(iProv != NoProvince)
   {
      const Province& prov = getCampaignData()->getProvince(iProv);

      Nationality nationality = prov.getNationality();

      info = scenario->getNationName(nationality);
      info += ", ";
      // info += scenario->getSideName(scenario->nationToSide(nationality));
      info += scenario->getSideName(prov.getStartSide());
   }
   else
      info = "No Province!";

   SetDlgItemText(getHWND(), IDTE_STATE_INFO, (const char*) info.c_str());


}




BOOL TownEditGeneral::onInitDialog(HWND hwnd, HWND hwndFocus, LPARAM lParam)
{
   debugLog("TownEditGeneral::onInit()\n");
   setPosition();

   /*
    * State Combo Box
    */

   HWND hStateCombo = GetDlgItem(hwnd, IDTE_STATE_NAME);

   int index = SendMessage(hStateCombo, CB_ADDSTRING, 0, (LPARAM) "None");
   ASSERT(index != CB_ERR);
   ASSERT(index != CB_ERRSPACE);
   SendMessage(hStateCombo, CB_SETITEMDATA, index, NoProvince);

   ProvinceList& provList = getCampaignData()->getProvinces();
   MemPtr<char> provBuf(200);    /// @todo Replace with String
   for(IProvince pi = 0; pi < provList.entries(); pi++)
   {
      Province& prov = provList[pi];

      const char* name = prov.getName();
      ASSERT(name != 0);
      if(name)
         lstrcpy(provBuf.get(), prov.getName());
      else
         lstrcpy(provBuf.get(), "Unnamed");

      name = prov.getShortName();
      if(name)
         wsprintf((char*) provBuf.get() + lstrlen(provBuf.get()), " (%s)", (const char*) name);
      else
         lstrcat(provBuf.get(), " (?)");

      int index = SendMessage(hStateCombo, CB_ADDSTRING, 0, (LPARAM) (char*)provBuf.get());
      ASSERT(index != CB_ERR);
      ASSERT(index != CB_ERRSPACE);
      SendMessage(hStateCombo, CB_SETITEMDATA, index, pi);

      debugLog("State %d -> %d, %s\n",
         (int) pi,
         index,
         (char*) provBuf.get());
   }

   /*
    * Side and Start Side Combo box is a bit more complex
    * because it must use the sides from the scenario specific data
    */

   makeSideCombo(GetDlgItem(hwnd, IDTE_SIDE));
#if 0
   makeSideCombo(GetDlgItem(hwnd, IDTE_START_SIDE));
#endif
#if 0
   HWND hSideCombo = GetDlgItem(hwnd, IDTE_SIDE);
   ASSERT(hSideCombo != NULL);
   HWND hStartSideCombo = GetDlgItem(hwnd, IDTE_START_SIDE);
   ASSERT(hStartSideCombo != NULL);

   index = SendMessage(hSideCombo, CB_ADDSTRING, 0, (LPARAM) "Neutral");
   ASSERT(index != CB_ERR);
   ASSERT(index != CB_ERRSPACE);
   SendMessage(hSideCombo, CB_SETITEMDATA, index, SIDE_Neutral);

   index = SendMessage(hStartSideCombo, CB_ADDSTRING, 0, (LPARAM) "Neutral");
   ASSERT(index != CB_ERR);
   ASSERT(index != CB_ERRSPACE);
   SendMessage(hStartSideCombo, CB_SETITEMDATA, index, SIDE_Neutral);

   for(int side = 0; side < scenario->getNumSides(); side++)
   {
      int index = SendMessage(hSideCombo, CB_ADDSTRING, 0, (LPARAM) scenario->getSideName(Side(side)));
      ASSERT(index != CB_ERR);
      ASSERT(index != CB_ERRSPACE);
      SendMessage(hSideCombo, CB_SETITEMDATA, index, side);

      index = SendMessage(hStartSideCombo, CB_ADDSTRING, 0, (LPARAM) scenario->getSideName(Side(side)));
      ASSERT(index != CB_ERR);
      ASSERT(index != CB_ERRSPACE);
      SendMessage(hStartSideCombo, CB_SETITEMDATA, index, side);
   }
#endif

   initComboIDItems(IDTE_TYPE, comboSizeData);
   SendDlgItemMessage(hwnd, IDTE_VICTORYASPIN, UDM_SETRANGE, 0, MAKELONG(UBYTE_MAX, 0));
   SendDlgItemMessage(hwnd, IDTE_VICTORYBSPIN, UDM_SETRANGE, 0, MAKELONG(UBYTE_MAX, 0));
   SendDlgItemMessage(hwnd, IDTE_STACKINGSPIN, UDM_SETRANGE, 0, MAKELONG(UBYTE_MAX, 0));
   SendDlgItemMessage(hwnd, IDTE_FORTIFICATIONSPIN, UDM_SETRANGE, 0, MAKELONG(UBYTE_MAX, 0));
   SendDlgItemMessage(hwnd, IDTE_POLITICALSPIN, UDM_SETRANGE, 0, MAKELONG(UBYTE_MAX, 0));
   SendDlgItemMessage(hwnd, IDTE_STRENGTHSPIN, UDM_SETRANGE, 0, MAKELONG(UBYTE_MAX, 0));

   initControls();
   return TRUE;
}

void TownEditGeneral::onDestroy(HWND hwnd)
{
   debugLog("TownEditGeneral::onDestroy()\n");
}


/*======================================================================
 * Position Sub-Dialog
 */


BOOL TownEditPosition::procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
   switch(msg)
   {
      case WM_INITDIALOG:
         HANDLE_WM_INITDIALOG(hWnd, wParam, lParam, onInitDialog);
         break;

      case WM_DESTROY:
         HANDLE_WM_DESTROY(hWnd, wParam, lParam, onDestroy);
         break;

      case WM_COMMAND:
         HANDLE_WM_COMMAND(hWnd, wParam, lParam, edit->onCommand);
         break;

      default:
         return FALSE;
   }

   return TRUE;
}

void TownEditPosition::updateValues()
{
}

void TownEditPosition::initControls()
{
   Town* town = edit->getTown();

   // Alignment Combo

   setComboIDValue(IDTE_ALIGN, town->getNameAlign());

   // XAdjust

   SendDlgItemMessage(hWnd, IDTE_XUPDOWN, UDM_SETPOS, 0, MAKELONG(town->getNameX(), 0));

   // YAdjust

   SendDlgItemMessage(hWnd, IDTE_YUPDOWN, UDM_SETPOS, 0, MAKELONG(town->getNameY(), 0));

   // Terrain Combo

   /*
    * if this is a newly created town then terrain will be set to UBYTE_MAX
    * if so create random terrain
    */
   CampaignData* campData = getCampaignData();

   if(town->getTerrain() > campData->getMaxTerrainType())
     town->setTerrain((TerrainType)random(0, campData->getMaxTerrainType()-1));


   setComboIDValue(IDTE_TERRAIN, town->getTerrain());
}

BOOL TownEditPosition::onInitDialog(HWND hwnd, HWND hwndFocus, LPARAM lParam)
{
   debugLog("TownEditPosition::onInit()\n");
   setPosition();

   // Alignment Combo

   initComboIDItems(IDTE_ALIGN, comboAlignData);

   // XAdjust

   SendDlgItemMessage(hWnd, IDTE_XUPDOWN, UDM_SETRANGE, 0, MAKELONG(SBYTE_MAX, SBYTE_MIN));

   // YAdjust

   SendDlgItemMessage(hWnd, IDTE_YUPDOWN, UDM_SETRANGE, 0, MAKELONG(SBYTE_MIN, SBYTE_MAX));

   // Terrain Combo

// initComboIDItems(IDTE_TERRAIN, comboTerrainData);

   ComboBox combo(hWnd, IDTE_TERRAIN);
   CampaignData* campData = edit->getCampaignData();

   const TerrainTypeTable& tt = campData->getTerrainTypes();
   for(TerrainType i = 0; i < tt.entries(); i++)
   {
     const TerrainTypeItem& item = tt[i];
     combo.add(item.getName(), i);
   }

   initControls();
   return TRUE;
}

void TownEditPosition::onDestroy(HWND hwnd)
{
   debugLog("TownEditPosition::onDestroy()\n");
}

/*=========================================================================
 * Resource sub-Dialog
 */

BOOL TownEditResource::procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
   switch(msg)
   {
      case WM_INITDIALOG:
         HANDLE_WM_INITDIALOG(hWnd, wParam, lParam, onInitDialog);
         break;

      case WM_DESTROY:
         HANDLE_WM_DESTROY(hWnd, wParam, lParam, onDestroy);
         break;
      case WM_COMMAND:
         HANDLE_WM_COMMAND(hWnd, wParam, lParam, edit->onCommand);
         break;

      default:
         return FALSE;
   }

   return TRUE;
}


void TownEditResource::updateValues()
{
   Town* town = edit->getTown();

   int v = GetDlgItemInt(hWnd, IDTE_SUPPLYPILE, NULL, FALSE);
   town->setSupplyLevel(v);
   town->setSupplyRate( (Attribute) SendDlgItemMessage(hWnd, IDTE_SUPPLYSPIN, UDM_GETPOS, 0, 0));
   town->setForageBase( (Attribute) SendDlgItemMessage(hWnd, IDTE_FORAGESPIN, UDM_GETPOS, 0, 0));
   town->setIsSupplySource(getButtonIDCheck(IDTE_SUPPLYSOURCE));
   town->setIsDepot(getButtonIDCheck(IDTE_SUPPLYDEPOT));
   town->setManPowerRate( (Attribute) SendDlgItemMessage(hWnd, IDTE_MANPOWERSPIN, UDM_GETPOS, 0, 0));
   // town->setFreeManPower( (Attribute) SendDlgItemMessage(hWnd, IDTE_FREEMANPOWERSPIN, UDM_GETPOS, 0, 0));
   town->setMpTransferable(getButtonIDCheck(IDTE_MPTRANSFER));
   town->setResourceRate( (Attribute) SendDlgItemMessage(hWnd, IDTE_RESOURCESPIN, UDM_GETPOS, 0, 0));
   // town->setFreeResources( (Attribute) SendDlgItemMessage(hWnd, IDTE_FREERESOURCESPIN, UDM_GETPOS, 0, 0));
}

void TownEditResource::initControls()
{
   Town* town = edit->getTown();

   SetDlgItemInt(hWnd, IDTE_SUPPLYPILE, town->getSupplyLevel(), FALSE);
// SendDlgItemMessage(hWnd, IDTE_SUPPLYPILESPIN, UDM_SETPOS, 0, MAKELONG(town->getSupplyLevel(), 0));
   SendDlgItemMessage(hWnd, IDTE_SUPPLYSPIN, UDM_SETPOS, 0, MAKELONG(town->getSupplyRate(), 0));
   SendDlgItemMessage(hWnd, IDTE_FORAGESPIN, UDM_SETPOS, 0, MAKELONG(town->getForageBase(), 0));
   setButtonIDCheck(IDTE_SUPPLYSOURCE, town->getIsSupplySource());
   setButtonIDCheck(IDTE_SUPPLYDEPOT, town->getIsDepot());
   SendDlgItemMessage(hWnd, IDTE_MANPOWERSPIN, UDM_SETPOS, 0, MAKELONG(town->getManPowerRate(), 0));
   // SendDlgItemMessage(hWnd, IDTE_FREEMANPOWERSPIN, UDM_SETPOS, 0, MAKELONG(town->getFreeManPower(), 0));
   setButtonIDCheck(IDTE_MPTRANSFER, town->getMpTransferable());
   SendDlgItemMessage(hWnd, IDTE_RESOURCESPIN, UDM_SETPOS, 0, MAKELONG(town->getResourceRate(), 0));
   // SendDlgItemMessage(hWnd, IDTE_FREERESOURCESPIN, UDM_SETPOS, 0, MAKELONG(town->getFreeResources(), 0));
}

BOOL TownEditResource::onInitDialog(HWND hwnd, HWND hwndFocus, LPARAM lParam)
{
   debugLog("TownEditResource::onInitDialog()\n");
   setPosition();

   SendDlgItemMessage(hwnd, IDTE_SUPPLYSPIN,       UDM_SETRANGE, 0, MAKELONG(UBYTE_MAX, 0));
// SendDlgItemMessage(hwnd, IDTE_SUPPLYPILESPIN,   UDM_SETRANGE, 0, MAKELONG(UWORD_MAX, 0));
   SendDlgItemMessage(hwnd, IDTE_FORAGESPIN,       UDM_SETRANGE, 0, MAKELONG(Attribute_Range, 0));
   SendDlgItemMessage(hwnd, IDTE_MANPOWERSPIN,     UDM_SETRANGE, 0, MAKELONG(UBYTE_MAX, 0));
   SendDlgItemMessage(hwnd, IDTE_RESOURCESPIN,     UDM_SETRANGE, 0, MAKELONG(UBYTE_MAX, 0));

   initControls();
   return TRUE;
}

void TownEditResource::onDestroy(HWND hwnd)
{
   debugLog("TownEditResource::onInitDestroy()\n");
}

#if 0
/*============================================================
 * Class for controlling TownEdit Dialogs
 */

TownEditList::TownEditList()
{
   edittingTown = FALSE;
}

TownEditList::~TownEditList()
{
}

TownEdit* TownEditList::editTown(Town* town, PixelPoint& p)
{
#if 0
   TownEdit* edit = find(town);
   if(edit)
   {
      SetActiveWindow(edit->getHWND());
   }
   else
   {
      edit = new TownEdit(this, town, p);
      ASSERT(edit != 0);
      if(edit)
         add(edit);
   }
   return edit;
#endif

   ASSERT(edittingTown == FALSE);

   TownEdit* edit = new TownEdit(this, town, p);
   ASSERT(edit != 0);

   edittingTown = TRUE;
   return edit;
}


/*
 * A Town Editor dialogue is being destroyed
 * .. Remove it from editList
 */

void TownEditList::dialogueDestroyed(TownEdit* edit)
{
   // remove(edit);
   edittingTown = FALSE;
}

#if 0

TownEdit* TownEditList::find(const Town* town)
{
   return 0;
}

void TownEditList::add(TownEdit* edit)
{
}

void TownEditList::remove(TownEdit* edit)
{
}

#endif

#endif

/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.2  2001/06/18 08:25:44  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
