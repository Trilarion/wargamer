/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
#ifndef EDITINFO_HPP
#define EDITINFO_HPP

#include "mytypes.h"
#include <windef.h>
#include "campEditDll.h"

class CampaignData;

class CAMPEDIT_DLL CampaignInfoEditor {
 public:
   static Boolean edit(HWND parent, CampaignData* campData);
};






#endif

/*
 *----------------------------------------------------------------------
 * $Log$
 *----------------------------------------------------------------------
 */
