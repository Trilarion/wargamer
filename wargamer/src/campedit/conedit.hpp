/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef CONEDIT_H
#define CONEDIT_H

#ifndef __cplusplus
#error conedit.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1995, Steven Morle-Green, All Rights Reserved
 *----------------------------------------------------------------------
 *
 *	Connection Editor
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.4  1996/02/19 16:17:35  Steven_Green
 * Use ITown and IProvince instead of Pointers
 *
 * Revision 1.3  1996/01/19 11:30:29  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.2  1995/12/11 11:51:17  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.1  1995/12/07 09:17:07  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#include "dialog.hpp"
#include "town.hpp"
#include "grtypes.hpp"
#include "mw_user.hpp"

class MWEditInterface;
class CampaignData;

class ConnectionEdit : public ModelessDialog, public EditDialog {
	MWEditInterface* d_editControl;
	CampaignData* d_campData;

	ITown townID;
	Town* town;

	MapWindow* parent;
	PixelPoint position;

	Connection* current;
	int lbSel;					// Number in list box
	int tiSel;					// Entry in town connection array

	ConnectType oldHow;					// What type of connection (road/rail/river)
	ConnectQuality oldQuality;			// Poor, Average, Good
	ConnectCapacity oldCapacity;		// How many units can use it in a given time
	Boolean oldOffScreen;
	WhichSideChokePoint::Type oldWhichSide;
	Distance oldDistance;


	ConnectType defaultHow;					// What type of connection (road/rail/river)
	ConnectQuality defaultQuality;			// Poor, Average, Good
	ConnectCapacity defaultCapacity;		// How many units can use it in a given time
	Boolean defaultOffScreen;
	WhichSideChokePoint::Type defaultWhichSide;
	Distance defaultDistance;

public:
	ConnectionEdit(MWEditInterface* editControl, CampaignData* cData, MapWindow* p, ITown it, const PixelPoint& pos);
	~ConnectionEdit();

	/*
	 * Virtual functions from EditDialog
	 */

	void kill();
	// void gotTown(ITown t);
	TrackMode onSelect(TrackMode mode, const MapSelect& info);

private:
	BOOL procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);

	BOOL onInitDialog(HWND hwnd, HWND hwndFocus, LPARAM lParam);
	void onDestroy(HWND hwnd);
	void onClose(HWND hwnd);
	void onActivate(HWND hwnd, UINT state, HWND hwndActDeact, BOOL fMinimized);
	void onCommand(HWND hwnd, int id, HWND hwndCtl, UINT codeNotify);
	void onContextMenu(HWND hwnd, HWND hwndCtl, int x, int y);
	BOOL onHelp(HWND hwnd, LPHELPINFO lParam);

	void setupListBox();
	void select(int sel);
	void onListBox(HWND hwnd, int id, HWND hwndCtl, UINT codeNotify);
	void updateCurrent();
	void onDelete(UINT codeNotify);

	ITown getOtherTown(const Connection* c);
	void makeConnectionName(char* buffer, const Connection* c);
	void updateConnectionName();
	// void insertConnection(IConnection iCon);

	void getOldValues();
};

#endif /* CONEDIT_H */

