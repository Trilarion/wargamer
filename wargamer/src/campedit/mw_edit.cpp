/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 * Map Window Editor Controller
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "mw_edit.hpp"
#include "cwin_int.hpp"
#include "mapwind.hpp"
#include "gamectrl.hpp"
#include "town.hpp"
#include "campdint.hpp"
#include "townedit.hpp"
#include "provedit.hpp"
#include "conedit.hpp"
#include "obedit.hpp"
#include "condedit.hpp"
#include "resdef.h"

// MapWindowEditors::MapWindowEditors(MapWindow* mapWind, CampaignData* campData, MainCampaignWindow* mainWind) :
MapWindowEditors::MapWindowEditors(MapWindow* mapWind, CampaignData* campData, CampaignWindowsInterface* campWind) :
   d_mapWind(mapWind),
   d_campData(campData),
   // d_mainWind(mainWind)
   d_campWind(campWind)
   // edittingTown(NoTown),
#if defined(EDITOR)
   // editMode(PM_EditTown),
#else
   // editMode(PM_Playing),
#endif
   // editDial(0)
{
}

/*
 * Implementation of MWEditInterface
 */

void MapWindowEditors::editDestroyed(EditDialog* edit)
{
   // editDial = 0;
   // edittingTown = NoTown;
   // setTrackMode(MWTM_None);
   d_mapWind->editDestroyed();
   enableEditModes();
}

void MapWindowEditors::itemChanged()
{
   d_mapWind->requestRedraw(True);
}

void MapWindowEditors::townSelected(ITown ob)
{
   d_mapWind->townSelected(ob);
}

void MapWindowEditors::setTrackMode(TrackMode mode)
{
   d_mapWind->setTrackMode(mode);
}

/*
 * Used by Editors
 */

void MapWindowEditors::toggleEditMode(EditMode mode)
{
   if(d_mapWind->getEditDial())
      d_mapWind->getEditDial()->kill();
   d_mapWind->setEditDial(0);

#if !defined(EDITOR)
   if(mode == d_mapWind->getEditMode())
      mode = PM_Playing;
#endif

   setEditMode(mode);
}

void MapWindowEditors::newObject()
{
   Location l = d_mapWind->getClickLocation();

   switch(d_mapWind->getEditMode())
   {
#if !defined(EDITOR)
   case PM_Playing:
      break;
#endif   // !EDITOR
#if defined(CUSTOMIZE)
   case PM_EditTown:
      newTown(l);
      break;
   case PM_EditProvince:
      newProvince(l);
      break;
   case PM_EditConnection:
      break;
   case PM_EditOB:
      newOB();
      break;
   case PM_EditConditions:
      editConditions();
      break;
#endif   // CUSTOMIZE
   }
}

void MapWindowEditors::provinceSelected(IProvince ob)
{
   d_mapWind->provinceSelected(ob);
}

/*
 * Private Functions
 */

void MapWindowEditors::setEditMode(EditMode mode)
{
   /*
    * Close down old mode
    */

   switch(mode)
   {
#if !defined(EDITOR)
   case PM_Playing:
      break;
#endif   // !EDITOR
#if defined(CUSTOMIZE)
   case PM_EditTown:
      break;
   case PM_EditProvince:
      break;
   case PM_EditConnection:
      break;
   case PM_EditOB:
      break;
   case PM_EditConditions:
      break;
#endif   // CUSTOMIZE
   }

   d_mapWind->setEditMode(mode);

   d_campWind->checkMenu(IDM_EDIT_TOWNS,        mode == PM_EditTown);
   d_campWind->checkMenu(IDM_EDIT_PROVINCES,    mode == PM_EditProvince);
   d_campWind->checkMenu(IDM_EDIT_CONNECTIONS,  mode == PM_EditConnection);
   d_campWind->checkMenu(IDM_EDIT_OB,           mode == PM_EditOB);
   d_campWind->checkMenu(IDM_EDITCONDITIONS,    mode == PM_EditConditions);
   d_campWind->enableMenu(IDM_NEW_OBJECT, 
      (mode == PM_EditTown) ||
      (mode == PM_EditProvince) ||
      (mode == PM_EditOB));

#if !defined(EDITOR)
      d_campWind->checkMenu(IDM_EDIT_NOTHING,          mode == PM_Playing);
#endif

#if !defined(EDITOR)
   /*
    * Bodge to force game into pause mode when going into an edit mode
    */

   if(mode != PM_Playing)
      GameControl::pause(True);
#endif   // !EDITOR

   /*
    * Set up new mode
    */

   switch(mode)
   {
#if !defined(EDITOR)
   case PM_Playing:
      break;
#endif   // !EDITOR
#if defined(CUSTOMIZE)
   case PM_EditTown:
      break;
   case PM_EditProvince:
      break;
   case PM_EditConnection:
      break;
   case PM_EditOB:
      break;
#endif   // CUSTOMIZE
   }
   d_mapWind->townSelected(NoTown);
   d_mapWind->provinceSelected(NoProvince);
}

void MapWindowEditors::newTown(const Location& l)
{
   if(d_mapWind->getEditDial() == 0)   // edittingTown == NoTown)
   {
      // Create a new blank town

      TownList& tl = d_campData->getTowns();
      ITown ti = tl.addNew();
      Town* newTown = &tl[ti];
      ASSERT(newTown != 0);
      newTown->setLocation(l);      // Mid point

      /*
       * Set up seonsible default values
       */

      switch(d_mapWind->getZoomEnum())
      {
      case 0:
         newTown->setSize(TOWN_City);
         newTown->setVictory(100);
         newTown->setPolitical(50);
         newTown->setManPowerRate(200);
         newTown->setResourceRate(200);
         newTown->setSupplyRate(100);
         newTown->setStrength(50);
         newTown->setSiegeable(True);
         break;
      case 1:
         newTown->setSize(TOWN_City);
         newTown->setVictory(50);
         newTown->setManPowerRate(100);
         newTown->setResourceRate(100);
         newTown->setSupplyRate(50);
         newTown->setStrength(20);
         newTown->setSiegeable(True);
         break;
      case 2:
      default:
         newTown->setSize(TOWN_Town);
         newTown->setVictory(0);
         newTown->setManPowerRate(200);
         newTown->setResourceRate(200);
         newTown->setSupplyRate(10);
         newTown->setStrength(10);
         break;
      }


      PixelPoint p(0,0);
      editTown(ti, p);
      d_campData->setChanged();
   }
}

void MapWindowEditors::newProvince(const Location& l)
{
   if(d_mapWind->getEditDial() == 0)   // !edittingProvince)
   {
      ProvinceList& pl = d_campData->getProvinces();
      IProvince pi = pl.addNew();
      ASSERT(pi != NoProvince);
      Province* newProv = &pl[pi];
      ASSERT(newProv != 0);
      newProv->setLocation(l);      // Mid point
      PixelPoint p(0,0);
      editProvince(pi, p);
      d_campData->setChanged();
   }
}

void MapWindowEditors::newOB()
{
  if(d_mapWind->getEditDial() == 0) // !edittingOB)
  {
    StackedUnitList unitList;
    PixelPoint p(0,0);
    editOB(&unitList, p);
  }
}

void MapWindowEditors::enableEditModes()
{
   bool allowChange = (d_mapWind->getEditDial() == 0);
   d_campWind->enableMenu(IDM_EDIT_TOWNS,       allowChange);
   d_campWind->enableMenu(IDM_EDIT_PROVINCES,      allowChange);
   d_campWind->enableMenu(IDM_EDIT_CONNECTIONS, allowChange);
   d_campWind->enableMenu(IDM_EDIT_OB,            allowChange);
   d_campWind->enableMenu(IDM_EDITCONDITIONS,  allowChange);
#if !defined(EDITOR)
   d_campWind->enableMenu(IDM_EDIT_NOTHING,       allowChange);
#endif
}

void MapWindowEditors::editTown(ITown itown, const PixelPoint& p)
{
   ASSERT(d_mapWind->getEdittingTown() == NoTown);
   ASSERT(d_mapWind->getEditDial() == 0);

   EditDialog* editDial = new TownEdit(this, d_campData, d_mapWind, itown, p);
   ASSERT(editDial != 0);
   d_mapWind->setEditDial(editDial);
   d_mapWind->setEdittingTown(itown);
   // d_mapWind->setTrackMode(MWTM_None);
   d_mapWind->setTrackMode(MWTM_Position);

   enableEditModes();
}

void MapWindowEditors::editProvince(IProvince iprov, const PixelPoint& p)
{
   // ASSERT(edittingProvince == FALSE);
   ASSERT(d_mapWind->getEditDial() == 0);

   EditDialog* editDial = new ProvinceEdit(this, d_campData, d_mapWind, iprov, p);
   ASSERT(editDial != 0);
   d_mapWind->setEditDial(editDial);
   // d_mapWind->setTrackMode(MWTM_None);
   d_mapWind->setTrackMode(MWTM_Position);

   enableEditModes();
}

void MapWindowEditors::editConnection(ITown itown, const PixelPoint& p)
{
   ASSERT(d_mapWind->getEdittingTown() == NoTown);
   ASSERT(d_mapWind->getEditDial() == 0);

   EditDialog* editDial = new ConnectionEdit(this, d_campData, d_mapWind, itown, p);
   ASSERT(editDial != 0);
   d_mapWind->setEditDial(editDial);
   d_mapWind->setEdittingTown(itown);
   d_mapWind->setTrackMode(MWTM_Town);

   enableEditModes();
}

void MapWindowEditors::editOB(const StackedUnitList* unitList, const PixelPoint& p)
{
  // ASSERT(edittingOB == FALSE);
  ASSERT(d_mapWind->getEditDial() == 0);
  EditDialog* editDial = new OBEdit(this, d_campData, d_mapWind, unitList, p);
  ASSERT(editDial != 0);
  d_mapWind->setEditDial(editDial);
  // edittingOB = TRUE;
   d_mapWind->setTrackMode(MWTM_None);

  enableEditModes();
}

void MapWindowEditors::editConditions(WG_CampaignConditions::Conditions* condition)
{
  ASSERT(d_mapWind->getEditDial() == 0);
  EditDialog* editDial = 0;
  if(condition)
     editDial = new ConditionEdit(d_mapWind->getHWND(), this, d_campData, condition);
  else
     editDial = new ConditionEdit(d_mapWind->getHWND(), this, d_campData);
  ASSERT(editDial != 0);
  d_mapWind->setEditDial(editDial);
  d_mapWind->setTrackMode(MWTM_None);

  enableEditModes();
}

#ifdef CUSTOMIZE
void MapWindowEditors::runOBEdit(ICommandPosition cpi)
{
//  ASSERT(cpi != NoCommandPosition);
//  CommandPosition* cp = d_campData->getCommand(cpi);

  if(d_mapWind)
  {
    StackedUnitList ul;
    PixelPoint p;

    if(cpi != NoCommandPosition)
    {
      CommandPosition* cp = d_campData->getCommand(cpi);
      Location l;
      cp->getLocation(l);

      d_mapWind->centerOnMap(l, static_cast<MagEnum>(1));

      d_mapWind->locationToPixel(l, p);

      ul.addUnit(cpi);
    }
    else
    {
      p.setX(0);
      p.setY(0);
    }

    editOB(&ul, p);
  }

}

void MapWindowEditors::runTownEdit(ITown iTown)
{
  ASSERT(iTown != NoTown);
  Town& town = d_campData->getTown(iTown);

  MagEnum magLevel = (town.getSize() == TOWN_Capital)?(MagEnum)(town.getSize() + 1):(MagEnum)town.getSize();

  if(d_mapWind)
  {

    const Location& l = town.getLocation();

    d_mapWind->centerOnMap(l, magLevel);

    PixelPoint p;
    d_mapWind->locationToPixel(l, p);

    editTown(iTown, p);
  }

}

void MapWindowEditors::runProvinceEdit(IProvince iProv)
{
  ASSERT(iProv != NoProvince);
  Province& prov = d_campData->getProvince(iProv);

  if(d_mapWind)
  {

    const Location& l = prov.getLocation();

    d_mapWind->centerOnMap(l, 0);

    PixelPoint p;
    d_mapWind->locationToPixel(l, p);

    editProvince(iProv, p);
  }
}

void MapWindowEditors::runConnectionEdit(ITown iTown)
{
  ASSERT(iTown != NoTown);
  Town& town = d_campData->getTown(iTown);

  MagEnum magLevel = (town.getSize() == TOWN_Capital)?(MagEnum)(town.getSize() + 1):(MagEnum)town.getSize();

  if(d_mapWind)
  {

    const Location& l = town.getLocation();

    d_mapWind->centerOnMap(l, magLevel);

    PixelPoint p;
    d_mapWind->locationToPixel(l, p);

    editConnection(iTown, p);
  }

}


void MapWindowEditors::runConditionEdit(WG_CampaignConditions::Conditions* condition)
{
   if(d_mapWind->getEditDial())
     d_mapWind->setEditDial(0);
     
   editConditions(condition);
}
#endif

void MapWindowEditors::editObject()
{
  MapSelect ms;
  PixelPoint p;
  editObject(ms, p);
}

void MapWindowEditors::editObject(const MapSelect& info, const PixelPoint& p)
{
   switch(d_mapWind->getEditMode())
   {
   case PM_EditTown:
      if(info.selectedTown != NoTown)
         editTown(info.selectedTown, p);
      break;
   case PM_EditProvince:
      if(info.selectedProvince != NoProvince)
         editProvince(info.selectedProvince, p);
      break;
   case PM_EditConnection:
      if(info.selectedTown != NoTown)
         editConnection(info.selectedTown, p);
      break;
   case PM_EditOB:
      if(info.trackedUnits->unitCount() > 0)
         editOB(info.trackedUnits, p);
      break;
   case PM_EditConditions:
      editConditions();
      break;
   }
}


/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.2  2001/06/18 08:25:44  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
