/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef CONDEDIT_HPP
#define CONDEDIT_HPP

#include "dialog.hpp"
#include "c_cond.hpp"
#include "mw_user.hpp"

using namespace WG_CampaignConditions;

class CampaignData;
class MWEditInterface;
class ConditionEditPage;
class ActionEditPage;

class ConditionEdit : public ModelessDialog, public EditDialog {

    MWEditInterface* d_editControl;
    CampaignData* d_campData;
    ConditionList* d_conditionList;
    Conditions* d_currentCondition;

    class ConditionTypePages {
    public:
      enum Pages {
        First = 0,
        TownChangesSidePage = First,
        CasualtyLevelReachedPage,
        MovesIntoNeutralPage,
        LeaderIsDeadPage,
        DateReachedPage,
        NationAtWarPage,
        SideVPTotalPage,
        SideOwnsTownPage,
        ArmisticeEndsPage,
        ArmisticeStartsPage,  
        Pages_HowMany
      };
    };

    class ActionTypePages {
    public:
      enum Pages {
        First = 0,
        SideHasVictoryPage = First,
        ArmisticePage,
        NationEntersWarPage,
        ReinforcementsPage,
        LeaderLeavesPage,
        AllyDefectsPage,
        AdjustResourcesPage,
        LeaderEntersPage,
        HowMany

      };
    };

    ConditionTypePages::Pages d_curTypePage;
    ConditionEditPage** d_conditionPages;
    HWND d_conditionTabHwnd;                 // Tab Dialog handle
    RECT d_conditionTDRect;                  // Area for tabbed dialogues to fit into

    ActionTypePages::Pages d_curActionPage;
    ActionEditPage** d_actionPages;
    HWND d_actionTabHwnd;
    RECT d_actionTDRect;                     // Area for tabbed dialogues to fit into

//  RECT d_tdRect;                  // Area for tabbed dialogues to fit into

  public:
    ConditionEdit(HWND parent, MWEditInterface* editControl, CampaignData* campData);
    ConditionEdit(HWND parent, MWEditInterface* editControl, CampaignData* campData, Conditions* condition);
    ~ConditionEdit(); // {}

    /*
     *  Virtual functions from EditDialog
     */

    void kill();
    TrackMode onSelect(TrackMode mode, const MapSelect& info) { return MWTM_None; }

    /*
     * Functions used by tab dialogs
     */

    CampaignData* getCampaignData() const { return d_campData; }
    Conditions* getCurrentCondition() const { return d_currentCondition; }
    ConditionTypeList* getTypes() const;
    ActionTypeList* getCurrentActions() const;
    ConditionType* getFirstType(ConditionType::Type typeID);
    ActionType* getFirstActionType(ActionType::Type typeID);

    Boolean hasNext(ConditionType::Type typeID, ConditionType* current);
    Boolean hasPrev(ConditionType::Type typeID, ConditionType* current);

    void getConditionTypeFlags(ConditionType::Type typeID, Boolean& hasCondition, Boolean& hasType);
    void getActionTypeFlags(ActionType::Type typeID, Boolean& hasCondition, Boolean& hasType);

    const RECT& getConditionDialogRect() const { return d_conditionTDRect; }
    const RECT& getActionDialogRect() const { return d_actionTDRect; }

    void onCommand(HWND hwnd, int id, HWND hwndCtl, UINT codeNotify);

  private:
    BOOL procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);

    BOOL onInitDialog(HWND hwnd, HWND hwndFocus, LPARAM lParam);
    void onDestroy(HWND hwnd);
    void onClose(HWND hwnd);
    LRESULT onNotify(HWND hWnd, int id, NMHDR* lpNMHDR);

    void onTypeSelChanged();
    void onActionSelChanged();
    int typeToPage(ConditionType::Type typeID);
    int actionTypeToPage(ActionType::Type typeID);
    void setCurrentType(ConditionType::Type typeID, ConditionType* type);
    void setCurrentActionType(ActionType::Type typeID, ActionType* type);

    void enableControls();
    void enableTypeControl(ConditionType::Type type);

    void initControls();

    void createConditionTypePages(LONG xMargin, LONG yTop, RECT& rcTab);
    void createActionTypePages(LONG xMargin, LONG yTop, RECT& rcTab);

    /*
     *  onCommand functions
     */

    void onNewCondition();
    void onNewConditionType(ConditionType::Type type);
    void onNewActionType(ActionType::Type type);
    void onNext();
    void onPrevType(ConditionType::Type typeID);
    void onNextType(ConditionType::Type typeID);
    void onPrev();
    void onOK();
    void onDelete();
    void onDeleteType(ConditionType::Type typeID);
    void onDeleteActionType(ActionType::Type typeID);

    /*
     * Update functions
     */

    void update();
};



#endif
