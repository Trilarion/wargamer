/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef EDITDATE_HPP
#define EDITDATE_HPP

#ifndef __cplusplus
#error editdate.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *	Date Editor
 *
 *----------------------------------------------------------------------
 */

#include "campEditDll.h"
#include "myTypes.h"
#include <windef.h>

class CampaignTimeData;

class CAMPEDIT_DLL DateEditor {
 public:
	static Boolean edit(HWND parent, CampaignTimeData* campaignDate);
};

#endif /* EDITDATE_HPP */

