/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
#include "stdinc.hpp"
#include "editnat.hpp"
#include "dialog.hpp"
#include "campdint.hpp"
#include "myassert.hpp"
#include "wind.hpp"
#include "app.hpp"
#include "scenario.hpp"
#include "resdef.h"
#include "winctrl.hpp"
#include "nations.hpp"
#include "gamectrl.hpp"

class NationListBox : public SubClassWindowBase {
  public:
    NationListBox() {}
    ~NationListBox() {}

    void set(HWND hwnd) { init(hwnd); }

   private:
      LRESULT procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);

//    void onMouseMove(HWND hwnd, int x, int y, UINT keyFlags);
//    void onLButtonDown(HWND hwnd, BOOL fDoubleClick, int x, int y, UINT keyFlags);
      void onRButtonDown(HWND hwnd, BOOL fDoubleClick, int x, int y, UINT keyFlags);
//    void onLButtonUp(HWND hwnd, int x, int y, UINT keyFlags);
//    void onRButtonUp(HWND hwnd, int x, int y, UINT keyFlags);
};

VOID APIENTRY allegianceEditPopupMenu(HWND hwnd, POINT& pt, int id)
{
   HMENU hmenu;            // top-level menu
   HMENU hmenuTrackPopup;  // pop-up menu

   // Load the menu resource.

   hmenu = LoadMenu(APP::instance(), MAKEINTRESOURCE(id));

   ASSERT(hmenu != NULL);

   // TrackPopupMenu cannot display the top-level menu, so get
   // the handle of the first pop-up menu.

   hmenuTrackPopup = GetSubMenu(hmenu, 0);
   ASSERT(hmenuTrackPopup != NULL);

   /*
    * Add Side names to menuitems
    */

   char buffer[100];
   lstrcpy(buffer, scenario->getSideName(0));

   MENUITEMINFO mi;
   mi.cbSize = sizeof(MENUITEMINFO);
   mi.fMask = MIIM_STATE | MIIM_TYPE;
   mi.fType = MFT_STRING;
   mi.fState = MFS_ENABLED;
   mi.dwTypeData = buffer;
   mi.cch = lstrlen(buffer)+1;

   BOOL result = SetMenuItemInfo(hmenuTrackPopup, IDM_NAE_SIDE0, FALSE, &mi);
   ASSERT(result);

   lstrcpy(buffer, scenario->getSideName(1));
   mi.wID = IDM_NAE_SIDE1;
   mi.cch = lstrlen(buffer);

   result = SetMenuItemInfo(hmenuTrackPopup, IDM_NAE_SIDE1, FALSE, &mi);
   ASSERT(result);

   // Display the floating pop-up menu. Track the right mouse
   // button on the assumption that this function is called
   // during WM_CONTEXTMENU processing.

   POINT p = pt;
   ListBox box(hwnd, NAE_NATIONLIST);

   ClientToScreen(box.getHWND(), &p);

   TrackPopupMenuEx(hmenuTrackPopup,
         TPM_LEFTALIGN | TPM_RIGHTBUTTON,
         p.x, p.y, hwnd, NULL);


   DestroyMenu(hmenu);
}


LRESULT NationListBox::procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
   switch(msg)
   {
      HANDLE_MSG(hWnd, WM_RBUTTONDOWN, onRButtonDown);

   default:
      return defProc(hWnd, msg, wParam, lParam);
   }

}

void NationListBox::onRButtonDown(HWND hwnd, BOOL fDoubleClick, int x, int y, UINT keyFlags)
{
   ListBox list(hwnd);

   POINT p;
   p.x = x;
   p.y = y;

   int index = list.getItemFromPoint(x, y);

   if(index < list.getNItems())
   {
     list.set(index);
     HWND parent = GetParent(hwnd);
     ASSERT(parent != NULL);
     allegianceEditPopupMenu(parent, p, MENU_EDITALLEGIANCE);
   }
}

class NationAllegianceEdit : public ModalDialog {
    NationsList* d_list;
    NationListBox d_nationListBox;
    Nationality d_nation;
  public:
    NationAllegianceEdit(NationsList* list);
    ~NationAllegianceEdit() {}

   int run(HWND hParent);
private:
   BOOL procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);

   BOOL onInitDialog(HWND hwnd, HWND hwndFocus, LPARAM lParam);
   void onDestroy(HWND hwnd);
   void onClose(HWND hwnd);
   void onCommand(HWND hwnd, int id, HWND hwndCtl, UINT codeNotify);

   void initControls();
   void updateValues();
   void onOK();

};

NationAllegianceEdit::NationAllegianceEdit(NationsList* list) :
  d_list(list),
  d_nation(NATION_Neutral)
{
  ASSERT(d_list != 0);
  ASSERT(d_list->entries() == scenario->getNumNations());
}

int NationAllegianceEdit::run(HWND parent)
{
   int result = createDialog(allegianceEditDlg, parent);

   return result;

}

BOOL NationAllegianceEdit::procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
   switch(msg)
   {
      HANDLE_DLG_MSG(WM_INITDIALOG, onInitDialog);
      HANDLE_DLG_MSG(WM_DESTROY, onDestroy);
      HANDLE_DLG_MSG(WM_COMMAND, onCommand);
      HANDLE_DLG_MSG(WM_CLOSE, onClose);
      default:
         return FALSE;
   }
   return TRUE;
}


BOOL NationAllegianceEdit::onInitDialog(HWND hwnd, HWND hwndFocus, LPARAM lParam)
{
  HWND hBox = GetDlgItem(hwnd, NAE_NATIONLIST);
  ASSERT(hBox != 0);

  d_nationListBox.set(hBox);

  initControls();
  return TRUE;
}

void NationAllegianceEdit::initControls()
{
  ASSERT(d_list != 0);

  ListBox b(getHWND(), NAE_NATIONLIST);
  b.reset();
  for(Nationality n = 0; n < d_list->entries(); n++)
  {
    char text[100];
    wsprintf(text, "%s (%s)",
       scenario->getNationName(n),
       scenario->getSideName(d_list->getAllegiance(n)) );

    b.add(text, n);
  }

  if(d_nation == NATION_Neutral)
    d_nation = 0;
  b.set(d_nation);

  SendDlgItemMessage(getHWND(), NAE_NDEPOTSSPIN, UDM_SETRANGE, 0, MAKELONG(UBYTE_MAX, 0));
  SendDlgItemMessage(getHWND(), NAE_NDEPOTSSPIN, UDM_SETPOS, 0, MAKELONG(d_list->getNDepotsAllowed(d_nation), 0));
  
}

void NationAllegianceEdit::updateValues()
{
   if(d_nation != NATION_Neutral)
     d_list->setNDepotsAllowed(d_nation, (UBYTE)SendDlgItemMessage(getHWND(), NAE_NDEPOTSSPIN, UDM_GETPOS, 0, 0));

}

void NationAllegianceEdit::onDestroy(HWND hwnd)
{

}

void NationAllegianceEdit::onClose(HWND hwnd)
{

}

void NationAllegianceEdit::onCommand(HWND hwnd, int id, HWND hwndCtl, UINT codeNotify)
{
  switch(id)
  {
    case IDM_NAE_SIDE0:
    {
      ListBox b(hwnd, NAE_NATIONLIST);
      updateValues();
      d_nation = static_cast<Nationality>(b.getValue());
      d_list->setAllegiance(d_nation, 0);
      initControls();
      break;
    }

    case IDM_NAE_SIDE1:
    {
      ListBox b(hwnd, NAE_NATIONLIST);
      updateValues();
      d_nation = static_cast<Nationality>(b.getValue());
      d_list->setAllegiance(d_nation, 1);
      initControls();
      break;
    }

    case IDM_NAE_SIDENEUTRAL:
    {
      ListBox b(hwnd, NAE_NATIONLIST);
      updateValues();
      d_nation = static_cast<Nationality>(b.getValue());
      d_list->setAllegiance(d_nation, SIDE_Neutral);
      initControls();
      break;
    }

    case NAE_NATIONLIST:
    {
       ListBox b(hwndCtl);
       updateValues();
       d_nation = static_cast<Nationality>(b.getValue());
       initControls();
       break;
    }

    case IDOK:
      if(codeNotify == BN_CLICKED)
      {
        updateValues();
        onOK();
      }
      else
        break;

    case IDCANCEL:
      EndDialog(hwnd, id);
      break;
  }

}

void NationAllegianceEdit::onOK()
{

}

Boolean NationAllegianceEditor::edit(HWND parent, CampaignData* campData)
{
   Boolean oldPause = GameControl::pause(True);


   NationAllegianceEdit* edit = new NationAllegianceEdit(&campData->getNations());
   ASSERT(edit != 0);

   int result = edit->run(parent);

   delete edit;

   GameControl::pause(oldPause);

   return (result == IDOK);
}

/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.2  2001/06/18 08:25:44  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
