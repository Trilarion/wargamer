/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1995, Steven Morle-Green, All Rights Reserved
 *----------------------------------------------------------------------
 *
 * Province Editor
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.2  2001/06/18 08:25:44  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 * Revision 1.1  2001/06/13 08:52:37  greenius
 * Initial Import
 *
 * Revision 1.5  1996/06/22 19:06:06  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.4  1996/02/19 16:16:55  Steven_Green
 * Use TownID and ProvinceID rather than Pointers
 *
 * Revision 1.3  1996/01/19 11:30:05  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.2  1995/12/07 09:15:20  Steven_Green
 * Delete Province implemented.
 * All basically working now,
 * ,.
 * ,
 *
 * Revision 1.1  1995/12/05 09:19:20  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "provedit.hpp"
#include "mwed_int.hpp"
#include "mapwind.hpp"

#include "resdef.h"
#include "scenario.hpp"
#include "misc.hpp"
#include "campdint.hpp"
#include "editsup.hpp"
#include "help.h"
#include "winctrl.hpp"
#include "fileiter.hpp"

const char ProvinceEdit::s_defaultPicture[] = "<None>";

ProvinceEdit::ProvinceEdit(MWEditInterface* editControl, CampaignData* data, MapWindow* p, IProvince iprov, const PixelPoint& pos) :
   d_editControl(editControl)
{
   campData = data;
   parent = p;
   provinceID = iprov;
   province = &campData->getProvince(iprov);
   oldValues = *province;
   POINT pt = pos;
   ClientToScreen(parent->getHWND(), &pt);
   position = pt;
   mode = Get_Nothing;

   HWND dhwnd = createDialog(provinceEditDialogName, parent->getHWND());
   ASSERT(dhwnd != NULL);
}

ProvinceEdit::~ProvinceEdit()
{
}

BOOL ProvinceEdit::procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
   switch(msg)
   {
      case WM_INITDIALOG:
         HANDLE_WM_INITDIALOG(hWnd, wParam, lParam, onInitDialog);
         break;

      case WM_DESTROY:
         HANDLE_WM_DESTROY(hWnd, wParam, lParam, onDestroy);
         break;
      case WM_COMMAND:
         HANDLE_WM_COMMAND(hWnd, wParam, lParam, onCommand);
         break;

      case WM_CLOSE:
         HANDLE_WM_CLOSE(hWnd, wParam, lParam, onClose);
         break;

      case WM_ACTIVATE:
         HANDLE_WM_ACTIVATE(hWnd, wParam, lParam, onActivate);
         break;

      case WM_CONTEXTMENU:
         HANDLE_WM_CONTEXTMENU(hWnd, wParam, lParam, onContextMenu);
         break;

      case WM_HELP:
         HANDLE_WM_HELP(hWnd, wParam, lParam, onHelp);
         break;

      default:
         return FALSE;
   }

   return TRUE;
}

static const ids[] = {
    IDPE_STATE_NAME,    IDH_PE_ProvinceName,
    IDPE_SHORT_NAME,    IDH_PE_Abreviated,
    IDPE_OFFSCREEN,     IDH_PE_OffScreen,
    IDPE_SIDE,          IDH_PE_Owner,
    IDPE_START_SIDE,    IDH_PE_Allegiance,
    IDPE_SET_CAPITAL,   IDH_PE_SetCapital,
    IDPE_SET_TOWNS,     IDH_PE_SetTown,
    IDPE_NATIONALITY,   IDH_PE_Nationality,
    IDPE_MOVE,          IDH_PE_Move,
    IDPE_DELETE,        IDH_PE_Delete,
    IDPE_HELPID,        IDH_EDIT_HelpIDEdit,
    IDPE_DEFAULTHELPID, IDH_EDIT_DefaultHelpID,

    0,  0
};

void ProvinceEdit::onContextMenu(HWND hwnd, HWND hwndCtl, int x, int y)
{
//  WinHelp(hwndCtl, "help\\Wg95Help.hlp", HELP_CONTEXTMENU, (DWORD)(LPVOID)ids);
}

BOOL ProvinceEdit::onHelp(HWND hwnd, LPHELPINFO lparam)
{
  LPHELPINFO lphi = (LPHELPINFO)lparam;
  if (lphi->iContextType == HELPINFO_WINDOW)   // must be for a control
  {
//  WinHelp ((HWND)lphi->hItemHandle, "help\\Wg95Help.hlp", HELP_WM_HELP, (DWORD)(LPVOID)ids);
  }
  return TRUE;
}

BOOL ProvinceEdit::onInitDialog(HWND hwnd, HWND hwndFocus, LPARAM lParam)
{
   // Setup controls

   /*
    * Name
    */

   const char* s = province->getName();
   if(s == 0)
      s = "New Province";
   SetDlgItemText(getHWND(), IDPE_STATE_NAME, s);

   /*
    * Short name
    */

   s = province->getShortName();
   if(s == 0)
      s = "??";
   SetDlgItemText(getHWND(), IDPE_SHORT_NAME, s);

   /*
    * Side
    */

   HWND hSideCombo = GetDlgItem(hwnd, IDPE_SIDE);
   makeSideCombo(hSideCombo);
   setComboIDValue(IDPE_SIDE, province->getSide());

   /*
    * Start side
    */

   hSideCombo = GetDlgItem(hwnd, IDPE_START_SIDE);
   makeSideCombo(hSideCombo);
   setComboIDValue(IDPE_START_SIDE, province->getStartSide());

   /*
    * Nationality
    */

   hSideCombo = GetDlgItem(hwnd, IDPE_NATIONALITY);
   makeNationCombo(hSideCombo);
   setComboIDValue(IDPE_NATIONALITY, province->getNationality());

   setButtonIDCheck(IDPE_OFFSCREEN, province->isOffScreen());

   SetDlgItemInt(hwnd, IDPE_HELPID, province->getHelpID(), FALSE);
#if 0
   /*
    * Resource Value
    */

   SendDlgItemMessage(hwnd, IDPE_RESOURCESPIN, UDM_SETRANGE, 0, MAKELONG(AttributePoint_Range, 0));
   SendDlgItemMessage(hWnd, IDPE_RESOURCESPIN, UDM_SETPOS, 0, MAKELONG(province->getResource(), 0));

   /*
    * ManPower
    */

   SendDlgItemMessage(hwnd, IDPE_MANPOWERSPIN, UDM_SETRANGE, 0, MAKELONG(AttributePoint_Range, 0));
   SendDlgItemMessage(hWnd, IDPE_MANPOWERSPIN, UDM_SETPOS, 0, MAKELONG(province->getManPower(), 0));
#endif

   setCapitalText();

   /*
    * Picture
    */

   ComboBox pCombo(hWnd, IDPE_PICTURE);

   int index = SendMessage(pCombo.getHWND(), CB_ADDSTRING, 0, reinterpret_cast<LPARAM>(s_defaultPicture));
   ASSERT(index != CB_ERR);
   ASSERT(index != CB_ERRSPACE);

   SimpleString path = scenario->getProvincePicturePath();
   path += "\\*.bmp";

   for(FindFileIterator fIter(path.toStr()); fIter(); ++fIter)
   {
      const char* name = fIter.name();
      char buffer[MAX_PATH];
      strcpy(buffer, name);
      char* s = strrchr(buffer, '.');
      if(s)
         *s = 0;

      int index = SendMessage(pCombo.getHWND(), CB_ADDSTRING, 0, reinterpret_cast<LPARAM>(buffer));
      ASSERT(index != CB_ERR);
      ASSERT(index != CB_ERRSPACE);
   }

   if(province->picture() == 0)
      pCombo.setText(s_defaultPicture);
   else
      pCombo.setText(province->picture());

   return TRUE;
}

void ProvinceEdit::onDestroy(HWND hwnd)
{
   d_editControl->editDestroyed(this);
}

void ProvinceEdit::onClose(HWND hwnd)
{
   onCommand(hwnd, IDPE_CANCEL, 0, 0);
}

void ProvinceEdit::onActivate(HWND hwnd, UINT state, HWND hwndActDeact, BOOL fMinimized)
{
}


void ProvinceEdit::onCommand(HWND hwnd, int id, HWND hwndCtl, UINT codeNotify)
{
   switch(id)
   {
      case IDPE_OK:
         if(checkProvinceHelpID())
         {
           updateValues();
           d_editControl->itemChanged();
           // campaign->sortTowns();
           campData->setChanged();
           DestroyWindow(hwnd);
         }
         break;

      case IDPE_HELPID:
      {
         if(codeNotify == EN_CHANGE)
         {
           ASSERT(province != 0);

           if(province)
           {
             HelpID helpID = (HelpID)GetDlgItemInt(getHWND(), id, NULL, FALSE);
             province->setHelpID(helpID);
           }
         }

         break;
      }

      case IDPE_DEFAULTHELPID:
         if(codeNotify == BN_CLICKED)
         {
           if(province)
           {
             SetDlgItemInt(getHWND(), IDPE_HELPID, NoHelpID, FALSE);
             province->setHelpID(NoHelpID);
           }

         }
         break;

      case IDPE_CANCEL:
         // restoreValues();
         *province = oldValues;
         d_editControl->itemChanged();
         DestroyWindow(hwnd);
         break;
      case IDPE_DELETE:
         onDelete(hwnd);
         break;
      case IDPE_MOVE:
         d_editControl->setTrackMode(MWTM_Position);  // wantPosition(this);
         break;
      case IDPE_STATE_NAME:
         onStateName(hwnd, codeNotify);
         break;
      case IDPE_SHORT_NAME:
         onShortName(hwnd, codeNotify);
         break;
      case IDPE_SIDE:
         break;
      case IDPE_START_SIDE:
         break;
      case IDPE_NATIONALITY:
         break;

      case IDPE_SET_CAPITAL:
         mode = Get_Capital;
         d_editControl->setTrackMode(MWTM_Town);
         break;

      case IDPE_SET_TOWNS:
         mode = Get_Towns;
         d_editControl->setTrackMode(MWTM_Town);
         break;
#if 0
      case IDPE_RESOURCE:
         break;
      case IDPE_RESOURCESPIN:
         break;
      case IDPE_MANPOWER:
         break;
      case IDPE_MANPOWERSPIN:
         break;
#endif

#ifdef DEBUG
      default:
         debugLog("ProvinceEdit::onCommand(), unknown id %d\n", id);
#endif
   }

}

/*
 * Check province help id value to make sure it is in range
 *
 * Each help id must be unique. Province IDs will for now be in the range of 2000-2099
 */

Boolean ProvinceEdit::checkProvinceHelpID()
{

  const UINT minRange = 2000;
  const UINT maxRange = 2099;

  static const char msgOutOfRange[] = "Province's Help ID must be in the range of 2000 and 2099, or press default-ID button";

  if(province)
  {
    HelpID helpID = (HelpID)GetDlgItemInt(getHWND(), IDPE_HELPID, NULL, FALSE);

    if( (helpID != NoHelpID) && (helpID < minRange || helpID > maxRange) )
    {
      int result = MessageBox(getHWND(), msgOutOfRange, "Out of Range", MB_OK);
      ASSERT(result == IDOK);
      return False;
    }

    /*
     * Go through all provinces and make sure Help ID is not duplicated
     */

//  const Armies* armies = &campData->getArmies();

    static const char msgIDRepeated[] = "Provinces's HelpID(%d) has already been defined. Choose a different value in the range of 2000 to 2099, or press default-ID button";

    const ProvinceList& pl = campData->getProvinces();

    ProvinceIter pIter(pl);

    while(++pIter)
    {
      const Province& prov = pIter.current();

      if( (prov.getHelpID() != NoHelpID) && (helpID == prov.getHelpID() && provinceID != pIter.currentID()) )
      {
        char buf[200];
        wsprintf(buf, msgIDRepeated, helpID);

        int result = MessageBox(getHWND(), buf, "ID Duplicated", MB_OK);
        ASSERT(result == IDOK);
        return False;
      }
    }
  }

  return True;
}


/*
 * Got Position called from MapWindow
 */

TrackMode ProvinceEdit::onSelect(TrackMode trackMode, const MapSelect& info)
{
   if(trackMode == MWTM_Position)
   {
      province->setLocation(info.mouseLocation);
      d_editControl->itemChanged();
   }
   else if(trackMode == MWTM_Town)
   {
      if(mode == Get_Capital)
      {
         campData->setCapital(provinceID, info.selectedTown);
         campData->setChanged();
         setCapitalText();
         d_editControl->itemChanged();
      }
      else if(mode == Get_Towns)
      {
         campData->moveTownToProvince(provinceID, info.selectedTown);
         campData->sortTowns();
         campData->setChanged();
         setCapitalText();    // Capital might change!
         d_editControl->itemChanged();
      }
   }
#ifdef DEBUG
   else if(trackMode != MWTM_None)
      FORCEASSERT("Unexpected TrackMode");
#endif

   return trackMode;
}

#if 0 // Replace this with onSelect
void ProvinceEdit::gotPosition(Location* l)
{
   if(l)
   {
      debugLog("Province Location changed\n");
      province->setLocation(*l);
      d_editControl->itemChanged();
   }
}
#endif

/*
 * Update name
 */

void ProvinceEdit::onStateName(HWND hwnd, UINT codeNotify)
{
   if(codeNotify == EN_CHANGE)
   {
      char* newName = getItemIDText(IDPE_STATE_NAME);
      const char* oldName = province->getName();
      if(stringsDiffer(newName, oldName))
         province->setName(newName);

#ifdef DEBUG
      debugLog("Name changed\n");
      // setApply();
#endif

      parent->requestRedraw(FALSE);
   }
}

void ProvinceEdit::onShortName(HWND hwnd, UINT codeNotify)
{
   if(codeNotify == EN_CHANGE)
   {
      char* newName = getItemIDText(IDPE_SHORT_NAME);
      const char* oldName = province->getShortName();
      if(stringsDiffer(newName, oldName))
         province->setShortName(newName);

#ifdef DEBUG
      debugLog("Abreviation changed\n");
      // setApply();
#endif

      parent->requestRedraw(FALSE);

   }
}

/*
 * Delete button pressed
 */

void ProvinceEdit::onDelete(HWND hwnd)
{
   ASSERT(province != 0);
   ASSERT(hwnd != NULL);
   ASSERT(parent != 0);
   ASSERT(campData != 0);

   parent->provinceSelected(0);
   ProvinceList& pl = campData->getProvinces();
   campData->removeProvince(pl.getID(province));
   parent->requestRedraw(TRUE);
   campData->setChanged();
   DestroyWindow(hwnd);
}

/*
 * Update values from dialog box fields
 */

void ProvinceEdit::updateValues()
{
   /*
    * Side
    */

   int index = SendDlgItemMessage(hWnd, IDPE_SIDE, CB_GETCURSEL, 0, 0);
   if(index != CB_ERR)
   {
      Side newSide = (Side) SendDlgItemMessage(hWnd, IDPE_SIDE, CB_GETITEMDATA, index, 0);
      province->setSide(newSide);
   }

   /*
    * Start Side
    */

   index = SendDlgItemMessage(hWnd, IDPE_START_SIDE, CB_GETCURSEL, 0, 0);
   if(index != CB_ERR)
   {
      Side newSide = (Side) SendDlgItemMessage(hWnd, IDPE_START_SIDE, CB_GETITEMDATA, index, 0);
      province->setStartSide(newSide);
   }

   /*
    * Nationality
    */

   index = SendDlgItemMessage(hWnd, IDPE_NATIONALITY, CB_GETCURSEL, 0, 0);
   if(index != CB_ERR)
   {
      Nationality newNation = (Nationality) SendDlgItemMessage(hWnd, IDPE_NATIONALITY, CB_GETITEMDATA, index, 0);
      province->setNationality(newNation);
   }

   /*
    * Is offscreen?
    */

   province->setOffScreen(getButtonIDCheck(IDPE_OFFSCREEN));

#if 0
   /*
    * Resource
    */

   province->setResource((AttributePoints) SendDlgItemMessage(hWnd, IDPE_RESOURCESPIN, UDM_GETPOS, 0, 0));

   /*
    * Manpower
    */

   province->setManPower((AttributePoints) SendDlgItemMessage(hWnd, IDPE_MANPOWERSPIN, UDM_GETPOS, 0, 0));
#endif

   /*
    * Picture
    */

   ComboBox pCombo(hWnd, IDPE_PICTURE);
   StringPtr portText = pCombo.getText();
   if(strcmp(portText, s_defaultPicture) == 0)
      province->picture(0);
   else
      province->picture(copyString(portText));

}

void ProvinceEdit::kill()
{
   onClose(getHWND());
}


void ProvinceEdit::setCapitalText()
{
   ITown iCapital = province->getCapital();
   const char* s;
   if(iCapital != NoTown)
      s = campData->getTownName(iCapital);
   else
      s = "<Not Set>";

   SetDlgItemText(getHWND(), IDPE_CAPITAL, s);
}

/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.2  2001/06/18 08:25:44  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
