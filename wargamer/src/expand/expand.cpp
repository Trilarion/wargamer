/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1995, Steven Morle-Green, All Rights Reserved
 *----------------------------------------------------------------------
 *
 *	Program to expand the size of a BMP image
 * 
 * Usage:
 *   expand srcName destName width
 *
 * e.g.
 *		expand map.bmp bigmap.bmp 4096
 *
 * When it is finished, added pixels should be calculated by using
 * the nearest colour in the palette to an averaged colour between
 * its surrounding pixels.
 *
 * Will only work with 256 colour RGB encoded BMPs.
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.3  1995/11/14 11:25:51  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.2  1995/10/25 09:52:44  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.1  1995/10/20 11:05:28  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#define WIN32_LEAN_AND_MEAN		// Only include basic Windows stuff
#include <windows.h>

#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>
#include <ctype.h>
#include "mytypes.h"
#include "myassert.hpp"
#include "misc.hpp"

#define SMOOTH_X
// #define SMOOTH_Y

#if defined(SMOOTH_X) || defined(SMOOTH_Y)
#define SMOOTH
#endif

/*
 * Global Variables
 */

#if defined(SMOOTH)
LPRGBQUAD palette = 0;
#endif

/*
 * Helper Functions
 */

void error(const char* fmt, ...)
{
	char* buffer = new char[1000];

	va_list vaList;
	va_start(vaList, fmt);
	vsprintf(buffer, fmt, vaList);
	va_end(vaList);

	printf("\nError:\n%s\n\n", buffer);

	delete[] buffer;
}

void usage()
{
	puts("Usage: expand srcName destName width");
	puts("e.g.: expand pic.bmp bigpic.bmp 4096\n");
}

/*
 * My Assertion function
 */

void _myAssert(const char* expr, const char* file, int line)
{
	error("Assertion failed in\nFile: %s Line: %d\n`%s'", file, line, expr);
}

static Boolean GeneralError::doingError = False;
static Boolean GeneralError::firstError = False;

GeneralError::GeneralError(const char* fmt, ...)
{
	char buffer[500];

	va_list vaList;
	va_start(vaList, fmt);
	_vbprintf(buffer, 500, fmt, vaList);
	va_end(vaList);

	setMessage(buffer);
}

void GeneralError::setMessage(const char* s)
{
	firstError = doingError;
	doingError = True;
	describe = strdup(s);
}


/*
 * Class to assist File reading
 */

class FileReader {
	FILE* h;
	BOOL gotError;
	LPCSTR fName;

public:
	FileReader(LPCSTR name)
	{
		gotError = FALSE;

		fName = name;

		h = fopen(name, "rb");

		if(h == 0)
		{
			gotError = TRUE;
			error("Can't open %s", fName);
		}
	}

	~FileReader()
	{
		if(h != 0)
			fclose(h);
	}

	BOOL getError()
	{
		return gotError;
	}

	BOOL read(LPVOID ad, DWORD length)
	{
		if(fread(ad, length, 1, h) != 1)
		{
			gotError = TRUE;
			error("Error reading from %s", fName);
			return FALSE;
		}
		return TRUE;
	}
};


class FileWriter {
	FILE* h;
	BOOL gotError;
	LPCSTR fName;

public:
	FileWriter(LPCSTR name)
	{
		gotError = FALSE;

		fName = name;

		h = fopen(name, "wb");

		if(h == 0)
		{
			gotError = TRUE;
			error("Can't create %s", fName);
		}
	}

	~FileWriter()
	{
		if(h != 0)
			fclose(h);
	}

	BOOL getError()
	{
		return gotError;
	}

	BOOL write(LPVOID ad, DWORD length)
	{
		if(fwrite(ad, length, 1, h) != 1)
		{
			gotError = TRUE;
			error("Error writing to %s", fName);
			return FALSE;
		}
		return TRUE;
	}
};

#if defined(SMOOTH)
/*
 * Find close colour in global palette
 */

UBYTE findCloseColour(UBYTE red, UBYTE green, UBYTE blue)
{
	LPRGBQUAD lc = palette;

	UBYTE bestCol = 0;
	int bestDist;

	for(int i = 0; i < 256; i++, lc++)
	{
		int dRed = lc->rgbRed - red;
		int dGreen = lc->rgbGreen - green;
		int dBlue = lc->rgbBlue - blue;

		if( (dRed == 0) && (dGreen == 0) && (dBlue == 0))
		{
			bestCol = (UBYTE) i;
			break;
		}

		int dist = dRed*dRed + dGreen*dGreen + dBlue*dBlue;

		if( (i == 0) || (dist < bestDist))
		{
			bestCol = (UBYTE) i;
			bestDist = dist;
		}
	}

#if 0
	printf("\nClosest colour to %d,%d,%d = %d [%d,%d,%d]\n",
		(int) red,
		(int) green,
		(int) blue,
		(int) bestCol,
		(int) palette[bestCol].rgbRed,
		(int) palette[bestCol].rgbGreen,
		(int) palette[bestCol].rgbBlue);
#endif

	return bestCol;
}


/*
 * Duplicate pixels, using in between colours where appropriate
 */

void putPixels(UBYTE* dest, int count, UBYTE c1, UBYTE c2)
{
	if(count == 1)
		*dest = c1;
	else if(c1 == c2)
		memset(dest, c1, count);
	else
	{
		// For debugging... just put 1st pixel

		// *dest = c1;
		// memset(dest+1, Purple, count-1);

		UBYTE r1 = palette[c1].rgbRed;
		UBYTE g1 = palette[c1].rgbGreen;
		UBYTE b1 = palette[c1].rgbBlue;
		UBYTE r2 = palette[c2].rgbRed;
		UBYTE g2 = palette[c2].rgbGreen;
		UBYTE b2 = palette[c2].rgbBlue;

		int dR = r2 - r1;
		int dG = g2 - g1;
		int dB = b2 - b1;

		*dest++ = c1;

		for(int i = 1; i < count; i++)
		{
			UBYTE r = (UBYTE) (r1 + (dR * i) / count);
			UBYTE g = (UBYTE) (g1 + (dG * i) / count);
			UBYTE b = (UBYTE) (b1 + (dB * i) / count);

			*dest++ = findCloseColour(r, g, b);
		}
	}
}
#endif

/*
 * Copy a source line to destination
 * expanding it as necessary
 */

void expandLine(UBYTE* dest, int dWidth, UBYTE* src, int sWidth)
{
	ASSERT(dWidth > sWidth);

#if !defined(SMOOTH_X)
	int val = 0;
	int dCount = dWidth - 1;

	while(dCount--)
	{
		*dest++ = *src;

		val += sWidth;
		if(val >= dWidth)
		{
			val -= dWidth;
			src++;
		}
	}

	*dest++ = *src;

#else			// In between colours

	int val = 0;
	int dCount = dWidth - 1;

	int xCount = 0;

	while(dCount--)
	{
		// *dest++ = *src;
		xCount++;

		val += sWidth;
		if(val >= dWidth)
		{
			putPixels(dest, xCount, src[0], src[1]);
			dest += xCount;

			val -= dWidth;
			src++;
			xCount = 0;
		}
	}

	if(xCount)
		putPixels(dest, xCount, src[0], src[0]);

#endif
}

BOOL expandBMP(LPCSTR srcName, LPCSTR destName, int width)
{
	/*
	 * Open the file (Use a class so automatically closed)
	 */

	FileReader fr(srcName);

	if(fr.getError())
	{
		error("Can't open %s", srcName);
		return FALSE;
	}

	/*
	 * Read BITMAPFILEHEADER
	 */

	BITMAPFILEHEADER header;
	if(!fr.read(&header, sizeof(header)))
		return FALSE;

	printf("\nBITMAPFILEHEADER:\n");
	printf("%10s = `%c%c'\n", "Type", (char) header.bfType & 0xff, (char) (header.bfType >> 8));
	printf("%10s = %ld\n", "Size",		header.bfSize);
	printf("%10s = %hd\n", "Reserved1", header.bfReserved1);
	printf("%10s = %hd\n", "Reserved2", header.bfReserved2);
	printf("%10s = %ld\n", "OffBits",	header.bfOffBits);

	/*
	 * Read BITMAPINFO
	 */

	BITMAPINFOHEADER bmh;
	if(!fr.read(&bmh, sizeof(bmh)))
		return FALSE;

	printf("\nBITMAPINFOHEADER:\n");
	printf("%15s = %ld\n", "Size",				bmh.biSize);
	printf("%15s = %ld\n", "Width",				bmh.biWidth);
	printf("%15s = %ld\n", "Height",				bmh.biHeight);
	printf("%15s = %hd\n", "Planes",				bmh.biPlanes);
	printf("%15s = %hd\n", "BitCount",			bmh.biBitCount);
	printf("%15s = %ld\n", "Compression",		bmh.biCompression);
	printf("%15s = %ld\n", "SizeImage",			bmh.biSizeImage);
	printf("%15s = %ld\n", "XPelsPerMeter",	bmh.biXPelsPerMeter);
	printf("%15s = %ld\n", "YPelsPerMeter",	bmh.biYPelsPerMeter);
	printf("%15s = %ld\n", "ClrUsed",			bmh.biClrUsed);
	printf("%15s = %ld\n", "ClrImportant", 	bmh.biClrImportant);

	/*
	 * Check for valid type
	 */

	if((bmh.biPlanes != 1) ||
	   (bmh.biBitCount != 8) ||
		(bmh.biCompression != BI_RGB))
	{
		error("%s is not 256 colour RGB Windows BMP", srcName);
		return FALSE;
	}

	/*
	 * Read and Copy Colour Table
	 */

	RGBQUAD pal[256];

	if(!fr.read(pal, sizeof(pal)))
		return FALSE;

	printf("\nColours:\n");
	for(int i = 0; i < 256; i++)
	{
		if( (i & 7) == 0)
			printf("\n%3d:", i);
		printf(" %02x,%02x,%02x", pal[i].rgbRed, pal[i].rgbGreen, pal[i].rgbBlue);
	}
	printf("\n");

#if defined(SMOOTH)
	palette = pal;
#endif

	/*
	 * Start writing the new file
	 */

	FileWriter fw(destName);

	if(fw.getError())
		return FALSE;

	BITMAPFILEHEADER destHeader;
	destHeader = header;

	BITMAPINFOHEADER destbmh;
	destbmh = bmh;

	if(bmh.biWidth >= width)
	{
		error("%s's width (%d) must be smaller than %d", srcName, bmh.biWidth, width);
		return FALSE;
	}


	destbmh.biWidth = width;
 	destbmh.biHeight = (width * bmh.biHeight) / bmh.biWidth;
	destbmh.biSizeImage = destbmh.biWidth * destbmh.biHeight;
	destHeader.bfSize = destbmh.biSizeImage + sizeof(BITMAPFILEHEADER) + sizeof(BITMAPINFOHEADER) + sizeof(pal);

	if(!fw.write(&destHeader, sizeof(destHeader)))
		return FALSE;
	if(!fw.write(&destbmh,	 sizeof(destbmh)))
		return FALSE;
	if(!fw.write(pal,			 sizeof(pal)))
		return FALSE;

	/*
	 * OK, we've done the headers and colour table
	 * let's get down to the real business of expanding
	 * the bitmap.
	 */

	MemPtr<UBYTE> srcBuffer1(bmh.biWidth);
#if defined(SMOOTH_Y)
	MemPtr<UBYTE> srcBuffer2(bmh.biWidth);
#endif
	MemPtr<UBYTE> destBuffer(width);

	UBYTE* buf1 = srcBuffer1;
#if defined(SMOOTH_Y)
	UBYTE* buf2 = srcBuffer2;
#endif

	/*
	 * Prefill the 1st buffer
	 */

	int sLineCount = bmh.biHeight;

	if(!fr.read(buf1, bmh.biWidth))
		return FALSE;
#if defined(SMOOTH_Y)
	if(!fr.read(buf2, bmh.biWidth))
		return FALSE;
#endif
	sLineCount--;

	int val = 0;

	int h = destbmh.biHeight - 1;
	while(h--)
	{
		printf("Rows: %5d %5d\r", h, sLineCount);

#if defined(SMOOTH_Y)
		expandLine(destBuffer, width, buf1, bmh.biWidth, buf2);
#else
		expandLine(destBuffer, width, buf1, bmh.biWidth);
#endif

		if(!fw.write(destBuffer, width))
			return FALSE;

		val += bmh.biHeight;
		if(val >= destbmh.biHeight)
		{
			val -= destbmh.biHeight;

#if defined(SMOOTH_Y)
			swap(buf1, buf2);
#endif

#if defined(SMOOTH_Y)
			if(h)
				if(!fr.read(buf1, bmh.biWidth))
					return FALSE;
			else
				memcpy(buf2, buf1, bmh.biWidth);
#else
			if(!fr.read(buf1, bmh.biWidth))
				return FALSE;
#endif


			sLineCount--;
		}

	}

#if defined(SMOOTH_Y)
	expandLine(destBuffer, width, buf1, bmh.biWidth, buf2);
#else
	expandLine(destBuffer, width, buf1, bmh.biWidth);
#endif

	if(!fw.write(destBuffer, width))
		return FALSE;

	printf("\n");

	return TRUE;
}




Boolean aToInt(const char* s, int& n)
{
	/*
	 * Check for s being digits
	 */

	const char* s1 = s;
	while(*s1)
	{
		if(!isdigit(*s1))
			return False;
		s1++;
	}

	n = atoi(s);

	return True;
}


void main(int argc, char* argv[])
{
	char* srcName = 0;
	char* destName = 0;
	int width = 0;

	int i = 0;
	while(++i < argc)
	{
		char* arg = argv[i];

		if(srcName == 0)
			srcName = arg;
		else if(destName == 0)
			destName = arg;
		else if(width == 0)
		{
			if(!aToInt(arg, width))
			{
				error("width parameter '%s' is not a number", arg);
				usage();
				return;
			}
		}
		else
		{
			error("Too Many parameters given");
			usage();
			return;
		}
	}

 	if(!srcName || !destName || !width)
	{
		error("Not enough parameters on command line");
		usage();
		return;
	}
	
	printf("\nExpanding '%s' into '%s' with new width of %d\n",
		srcName, destName, width);

	if(!expandBMP(srcName, destName, width))
		error("Error expanding %s into %s", srcName, destName);
	else
		printf("Expansion completed\n");
}
