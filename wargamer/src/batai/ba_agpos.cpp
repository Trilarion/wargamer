/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
#include "stdinc.hpp"
#include "ba_agpos.hpp"
#include "filebase.hpp"
namespace WG_BattleAI_Internal
{

static UWORD s_version = 0x0000;
bool BAI_AggressPosStruct::readData(FileReader& f)
{
   UWORD version;
   f >> version;
   ASSERT(version <= s_version);

   UBYTE b;
   f >> b;
   d_aggression = static_cast<BattleOrderInfo::Aggression>(b);

   f >> b;
   d_posture = static_cast<BattleOrderInfo::Posture>(b);

   return f.isOK();
}

bool BAI_AggressPosStruct::writeData(FileWriter& f) const
{
   f << s_version;

   UBYTE b = static_cast<UBYTE>(d_aggression);
   f << b;

   b = static_cast<UBYTE>(d_posture);
   f << b;

   return f.isOK();
}
};
/*
 *----------------------------------------------------------------------
 * $Log$
 *----------------------------------------------------------------------
 */
