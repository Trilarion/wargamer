/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef AIB_HI_HPP
#define AIB_HI_HPP

#ifndef __cplusplus
#error aib_hi.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 * High Level Planner
 *
 *----------------------------------------------------------------------
 */

#include "aib_info.hpp"
//#include "aib_sobj.hpp"
#include "batunit.hpp"
#include "hiopplan.hpp"
#include "sync.hpp"
#include "battime.hpp"
class FileReader;
class FileWriter;

namespace WG_BattleAI_Internal
{

class HiZoneList;
class HiUnitList;
class HiCorpsAssigner;
class HiZone;

class HighLevelPlanner : public GameInfoUser
{
			  friend class BAI_ScriptReader;
	 public:
		  HighLevelPlanner(GameInfo* gameInfo);
		  virtual ~HighLevelPlanner();

		  virtual void process();
		  virtual void procMessage(const BattleMessageInfo& msg);

		  void plan(Hi_OpPlan::Plan p);
        void newDay();

#ifdef DEBUG
        void logHistorical();
#endif
		  // file interface
		  bool readData(FileReader& f);
        bool writeData(FileWriter& f) const;
		private:
        bool checkZones();
		  bool needPlan();// { return (d_plan == Hi_OpPlan::Plan_None); }

		  void updateArmyHQs();
        bool moveArmyHQ(const RefBattleCP& cp);
		  void updatePointers();
        HiZone* findCloseZone(HexCord hex);

		  // ai routines
		  HiZoneList* d_zones;
		  HiUnitList* d_units;
		  HiCorpsAssigner* d_assigner;

        BattleMeasure::BattleTime::Tick d_nextReinforcementCheck;
        BattleMeasure::BattleTime::Tick d_nextZoneCheck;
        BattleMeasure::BattleTime::Tick d_nextPlanCheck;
        BattleMeasure::BattleTime::Tick d_nextProcess;
        bool d_firstUpdate;
};

};  // namespace WG_BattleAI_Internal

#endif /* AIB_HI_HPP */

