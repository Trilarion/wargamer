/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef CONTAIN_HPP
#define CONTAIN_HPP

#ifndef __cplusplus
#error contain.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 * Templated generic Container
 *
 * usage:
 *   class MyClass : public Container<stlContainer<Type> >
 *   {
 *   }
 *
 *----------------------------------------------------------------------
 */


template<class T>
class STLContainer : protected T
{
    protected:
        typedef T Container;

    public:
        typedef Container::iterator iterator;
        typedef Container::const_iterator const_iterator;
        iterator begin() { return Container::begin(); }
        iterator end() { return Container::end(); }
        const_iterator begin() const { return Container::begin(); }
        const_iterator end() const { return Container::end(); }
        int size() const { return Container::size(); }
};

#endif /* CONTAIN_HPP */

