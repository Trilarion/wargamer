/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
#ifndef BAITABLE_HPP
#define BAITABLE_HPP

#include "tables.hpp"

class BattleAITables {
  public:

    /*-----------------------------------------------------
     * UpdateZoneModifier
     */

    static const Table1D<SWORD>& updateZoneModifiers();
    static const Table1D<SWORD>& considerRetreatModifiers();
    static const Table1D<SWORD>& replaceDivModifiers();
    static const Table1D<SWORD>& shouldSupportModifiers();
};

#endif

/*
 *----------------------------------------------------------------------
 * $Log$
 *----------------------------------------------------------------------
 */
