/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 * Hex Area
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "aib_area.hpp"
#include "filebase.hpp"
#include <iostream>

HexArea::HexArea(const HexCord& p1, const HexCord& p2, const HexCord& p3, const HexCord& p4)
{
    d_points[0] = p1;
    d_points[1] = p2;
    d_points[2] = p3;
    d_points[3] = p4;
}

HexArea::HexArea(const HexArea& hexArea)
{
    for(int i = 0; i < 4; ++i)
        d_points[i] = hexArea.d_points[i];
}

HexArea& HexArea::operator=(const HexArea& area)
{
    for(int i = 0; i < 4; ++i)
        d_points[i] = area.d_points[i];
    return *this;
}

bool HexArea::operator==(const HexArea& area) const
{
    for(int i = 0; i < 4; ++i)
    {
        if(!(d_points[i] == area.d_points[i]))
            return false;
    }
    return true;
}

bool HexArea::operator<(const HexArea& area) const
{
    for(int i = 0; i < 4; ++i)
    {
        if(!(d_points[i] < area.d_points[i]))
            return false;
    }
    return true;
}

std::ostream& operator << (std::ostream& str, const HexArea& area)
{
    for(int i = 0; i < 4; ++i)
    {
        str << '(' << area[i] << ')';
    }

    return str;
}

static UWORD s_version = 0x0000;
bool HexArea::readData(FileReader& f)
{
   UWORD version;
   f >> version;
   ASSERT(version <= s_version);

   for(int i = 0; i < 4; ++i)
   {
      d_points[i].readData(f);
   }

   return f.isOK();
}

bool HexArea::writeData(FileWriter& f) const
{
   f << s_version;

   for(int i = 0; i < 4; ++i)
   {
      d_points[i].writeData(f);
   }

   return f.isOK();
}


/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.2  2001/06/18 08:25:43  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
