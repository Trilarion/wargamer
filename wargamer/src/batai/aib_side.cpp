/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 * Battle AI for a particular side
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "aib_side.hpp"
#include "aib_data.hpp"
#include "gthread.hpp"
#include "aib_log.hpp"
#include "aib_hi.hpp"
#include "scenario.hpp"
#include "batdata.hpp"
#include "aib_msg.hpp"
#include "aisreadr.hpp"
#include "control.hpp"
#include "filebase.hpp"
#include "misc.hpp"
#include "except.hpp"
#include "aib_util.hpp"
#ifdef DEBUG
#include "bairestr.hpp"
#include "batmsg.hpp"
#endif

namespace WG_BattleAI_Internal
{
  using Greenius_System::Thread;


const char s_dot = '.';
const char s_null = '\0';
const char* s_ext = "hbs";
const char* s_biExt = "hbb";
const char* s_hext = "wgb";

bool isHistorical(const char* title)
{
   const char* c = title;
   while(*c != s_dot && *c != s_null)
      c++;

   if(*c == s_null)
      return False;

   c++;
   const char* ce = s_hext;
   int loop = 0;
   while(*c != s_null)
   {
      if(loop++ >= 3)
         return False;

      int ch = tolower(*c);
      if(ch != *ce)
         return False;

      c++;
      ce++;
   }

   return True;
}

bool convertToScriptFile(String& scriptFile, const char* title)
{
   const char* c = title;
   while(*c != s_dot && *c != s_null)
   {
      scriptFile += *c;
      c++;
   }

   if(*c == s_null)
      return False;

   scriptFile += s_dot;
   for(int i = 0; i < 3; i++)
   {
      scriptFile += s_ext[i];
   }

   return True;
}

bool convertToBinaryScriptFile(String& scriptFile, const char* title)
{
   const char* c = title;
   while(*c != s_dot && *c != s_null)
   {
      scriptFile += *c;
      c++;
   }

   if(*c == s_null)
      return False;

   scriptFile += s_dot;
   for(int i = 0; i < 3; i++)
   {
      scriptFile += s_biExt[i];
   }

   return True;
}

int BattleSideAI::s_defaultThinkSpeed = 1000;        // 1 second per thought
int BattleSideAI::s_minThinkSpeed = 1;               // 1 millisecond
int BattleSideAI::s_maxThinkSpeed = 20000;           // 20 Seconds

BattleSideAI::BattleSideAI(const BattleData* batData) :
    Thread(GameThreads::instance()),
    d_batData(batData),
    d_timer(s_defaultThinkSpeed, false),
    d_rate(s_defaultThinkSpeed),
#ifdef HAS_CONTROL_PANEL
    d_panel(),
#endif
    d_aiData(0),
    d_planner(0),
   d_messages(0)
{
}

BattleSideAI::BattleSideAI(Side s, const BattleData* batData, String title) :
    Thread(GameThreads::instance()),
    d_batData(batData),
    d_timer(s_defaultThinkSpeed, false),
    d_rate(s_defaultThinkSpeed),
#ifdef HAS_CONTROL_PANEL
    d_panel(),
#endif
    d_aiData(0),
    d_planner(0),
    d_messages(0)
{
   d_aiData = new BattleAIData(s, batData);

   d_messages = new BattleMessageQueue();
   d_planner = new HighLevelPlanner(d_aiData);
#if 1
   if(isHistorical(title.c_str()))
   {
      String scriptFile;
      if(convertToBinaryScriptFile(scriptFile, title.c_str()))
      {
         BAI_ScriptReader(const_cast<BattleData*>(batData), scriptFile.c_str(), this);//d_planner);
#ifdef DEBUG
         d_aiData->log("\nHistorical BattleAI for %s", title.c_str());
         d_aiData->log("-----------------------------------------------");
         d_planner->logHistorical();
#endif
      }
      else
      {
         throw GeneralError("Bad conversion to AI Script file! Battle=%s, Script=%s", title.c_str(), scriptFile.c_str());
      }
   }
   else
#endif
   {
      // set side boundries
      const HexCord::Cord ourEdge = d_aiData->getBattleEdge(s);
      HexCord mapSize = d_aiData->map()->getSize();

      HexCord leftHex((ourEdge == 0) ? 0 : mapSize.x() -1, (ourEdge == 0) ? 1 : mapSize.y() - 2);
      HexCord rightHex((ourEdge == 0) ?  mapSize.x() - 1 : 0, (ourEdge == 0) ? 1 : mapSize.y() - 2);

      const_cast<BattleData*>(batData)->setSideLeftRight(s, leftHex, rightHex);
      // Unchecked
      setAggPosture();
      // End
   }

}

BattleSideAI::BattleSideAI(Side s, const BattleData* batData, const char* batFile, FileWriter& f) :
    Thread(GameThreads::instance()),
    d_batData(batData),
    d_timer(s_defaultThinkSpeed, false),
    d_rate(s_defaultThinkSpeed),
#ifdef HAS_CONTROL_PANEL
    d_panel(),
#endif
    d_aiData(0),
    d_planner(0),
    d_messages(0)
{
   d_aiData = new BattleAIData(s, batData);
   d_messages = new BattleMessageQueue();
// d_planner = new HighLevelPlanner(d_aiData);

   if(isHistorical(batFile))
   {
      String scriptFile;
      if(convertToScriptFile(scriptFile, batFile))
      {
         BAI_ScriptReader(const_cast<BattleData*>(batData), scriptFile.c_str(), this, f);
      }
      else
      {
         throw GeneralError("Bad conversion to AI Script file! Battle=%s, Script=%s", batFile, scriptFile.c_str());
      }
   }
   else
   {
      throw GeneralError("%s is not a historical battle file!", batFile);
   }
}

BattleSideAI::~BattleSideAI()
{
    Thread::stop();
#ifdef HAS_CONTROL_PANEL
   d_panel.clear();
#endif
   if(d_planner)
      delete d_planner;
   if(d_messages)
      delete d_messages;
   if(d_aiData)
      delete d_aiData;
}

// Unchecked
void BattleSideAI::setAggPosture()
{
   CRefBattleCP cinc = d_aiData->cinc();
   if(cinc != NoBattleCP)
   {
      // get side ratios
      WG_BattleAI_Utils::StrRatio sr = WG_BattleAI_Utils::getSideStrRatio(d_aiData->battleData(), cinc);
      ASSERT(sr < WG_BattleAI_Utils::Str_HowMany);
      if(sr >= WG_BattleAI_Utils::Str_HowMany)
         return;

#ifdef DEBUG
      d_aiData->log("Setting Posture / Aggression for %s", cinc->getName());
      const char* srName = WG_BattleAI_Utils::strRatioText(sr);
      d_aiData->log("Side ratio = %s", srName);
#endif

      // test for changing to offensive posture
      if(cinc->posture() == BattleOrderInfo::Defensive)
      {
         int chance = (cinc->leader()->getAggression() * 100) / 255;
#ifdef DEBUG
         d_aiData->log("Raw chance for changing posture = %d", chance);
#endif

         // modify for force ratio
         const UWORD s_mult[WG_BattleAI_Utils::Str_HowMany] = {
            250, 200, 150, 120, 80, 50, 40, 25, 20
         };

         const UBYTE s_div = 100;

         chance = (chance * s_mult[sr]) / s_div;

#ifdef DEBUG
         d_aiData->log("Chance after force ratio modifier = %d", chance);
#endif
         if(d_aiData->random(100) <= chance)
         {
#ifdef DEBUG
            d_aiData->log("%s is changing posture to offensive", cinc->getName());
#endif
            const_cast<BattleCP*>(cinc)->posture(BattleOrderInfo::Offensive);
            const_cast<BattleCP*>(cinc)->getCurrentOrder().posture(BattleOrderInfo::Offensive);
         }
      }
      // test for increasing aggression
      if(cinc->aggression() <= 2)
      {
         int chance = (cinc->leader()->getAggression() * 100) / 255;
#ifdef DEBUG
         d_aiData->log("Raw chance for changing aggression = %d", chance);
#endif

         // modify for force ratio
         const UWORD s_mult[WG_BattleAI_Utils::Str_HowMany] = {
            250, 200, 150, 120, 100, 80, 60, 40, 30
         };

         const UBYTE s_div = 100;

         chance = (chance * s_mult[sr]) / s_div;

#ifdef DEBUG
         d_aiData->log("Chance after force ratio modifier = %d", chance);
#endif
         if(d_aiData->random(100) <= chance)
         {
#ifdef DEBUG
            d_aiData->log("%s is adding 1 aggression point", cinc->getName());
#endif
            const_cast<BattleCP*>(cinc)->aggression(static_cast<BattleOrderInfo::Aggression>(cinc->aggression() + 1));
            const_cast<BattleCP*>(cinc)->getCurrentOrder().aggression(cinc->aggression());
         }
      }
   }
}
//end

void BattleSideAI::start()
{
#ifdef DEBUG
    d_aiData->log("start()");
#endif

      Thread::start();

#ifdef HAS_CONTROL_PANEL
      d_panel.init(this);
#endif
}

void BattleSideAI::newDay()      // restart new day
{
   pause(false);
   d_planner->newDay();
}

void BattleSideAI::doDeploy()
{
#ifdef DEBUG
      d_aiData->log("doDeploy()");
#endif

    d_aiData->deploying(true);
    d_aiData->process();
    d_planner->process();
    d_aiData->deploying(false);
}

void BattleSideAI::sendMessage(const BattleMessageInfo& msg)
{
#ifdef DEBUG
    String text = BattleMessageFormat::messageToText(msg, d_aiData->battleData());
    d_aiData->log("received Message: %s", (const char*) text.c_str());
#endif

    if(d_messages)
        d_messages->push(msg);
}

Side BattleSideAI::side() const
{
    return d_aiData->side();
}

void BattleSideAI::run()
{
#ifdef DEBUG
   bataiLog.printf("BattleAI thread started");
   d_aiData->log("Battle AI started for %s", scenario->getSideName(side()));
#endif

   d_timer.start();

   for(;;)
   {
      WaitValue result = wait(d_timer, INFINITE);
      if(result == TWV_Quit)
         break;
      else if(result == TWV_Event)
      {
#ifdef DEBUG
            d_aiData->log("Thinking %d", (int) d_aiData->battleData()->getTick());
#endif
            // Process Message queue

            while(!d_messages->isEmpty())
            {
                BattleData::ReadLock lock(d_aiData->battleData());
                const BattleMessageInfo& msg = d_messages->get();

#ifdef DEBUG
                String text = BattleMessageFormat::messageToText(msg, d_aiData->battleData());
                d_aiData->log("Processing Message: %s", (const char*) text.c_str());
#endif
                d_aiData->procMessage(msg);
                d_planner->procMessage(msg);
                d_messages->pop();
            }

            // Process planners

            {
                BattleData::ReadLock lock(d_aiData->battleData());
                d_aiData->process();
            }
            {
                BattleData::ReadLock lock(d_aiData->battleData());
                d_planner->process();
            }
      }
#ifdef DEBUG
      else
         FORCEASSERT("Illegal WaitValue");
#endif
   }
#ifdef DEBUG
    d_aiData->log("Finished");
#endif
}

#ifdef HAS_CONTROL_PANEL

String BattleSideAI::getTitle() const
{
    String result = scenario->getSideName(side());
   result += " Battle AI Control Panel";
    return result;
}

#endif

static UWORD s_version = 0x0000;
bool BattleSideAI::readData(FileReader& f)
{
   UWORD version;
   f >> version;
   ASSERT(version <= s_version);

   Side s;
   f >> s;
   if(!d_aiData)
      d_aiData = new BattleAIData(s, d_batData);
   d_aiData->readData(f);

   if(!d_planner)
      d_planner = new HighLevelPlanner(d_aiData);
   d_planner->readData(f);

   if(!d_messages)
      d_messages = new BattleMessageQueue();
   d_messages->readData(f);

   f >> d_rate;
   d_timer.setRate(d_rate);

#ifdef DEBUG
   d_planner->logHistorical();
#endif
   return f.isOK();
}

bool BattleSideAI::writeData(FileWriter& f) const
{
   f << s_version;

   Side s = d_aiData->side();
   f << s;

   d_aiData->writeData(f);
   d_planner->writeData(f);
   d_messages->writeData(f);

   f << d_rate;
   return f.isOK();
}

};      // namespace WG_BattleAI_Internal


/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.2  2001/06/18 08:25:43  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
