/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef AIB_HIOB_HPP
#define AIB_HIOB_HPP

#ifndef __cplusplus
#error aib_hiob.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 * Battle AI : Hi level Objectives
 *
 *----------------------------------------------------------------------
 */

// #include <set>        // STL set<T>
//#include <list>        // STL set<T>
#include "bobdef.hpp"
#include "leader.hpp"
#include "aib_mid.hpp"
#include "contain.hpp"
#include "ba_agpos.hpp"
/*
 * The actual classes
 */

class FileReader;
class FileWriter;
class OrderBattle;

namespace WG_BattleAI_Internal
{

class RealHiObjective : public HiObjective
{
			 friend class HiObjectiveList;
			 friend class BAI_ScriptReader;
		public:
				// RealHiObjective();
				RealHiObjective(GameInfo* gameInfo);
				RealHiObjective(GameInfo* gameInfo, Mode mode);
				virtual ~RealHiObjective();

				void process();

				/*
				 * Functions accessible by mid-level
				 */

				virtual const HexArea& area() const //{ return d_area; }
				{
//          return 0;
					ASSERT(d_list.size() > 0);
					return *(d_list.begin());
				}

				virtual const UnitList& units() const { return d_units; }
				virtual Mode mode() const { return d_mode; }
            virtual void mode(Mode m) { d_mode = m; }
				virtual BattleOrderInfo::Aggression aggression() const { return d_agPos.d_aggression; }
				virtual BattleOrderInfo::Posture posture() const { return d_agPos.d_posture; }
				virtual int nBoundries() const { return d_list.size(); }

				/*
				 * Functions accessible by Hi-Level
				 */

				void area(const HexArea& area, bool removeOld = True) //{ d_area = area; }
				{
					if(removeOld && d_list.size() > 0)
					{
						while(d_list.size())
							d_list.pop_front();
					}

					d_list.push_back(area);
				}

				void removeBoundry()
				{
					if(d_list.size() > 0)
						d_list.pop_front();
				}

				void addUnit(CRefBattleCP cp) { d_units.insert(d_units.begin(), cp); }
				void removeUnit(CRefBattleCP cp) { d_units.erase(cp); }
				UnitList& units() { return d_units; }

				void aggression(BattleOrderInfo::Aggression ag) { d_agPos.d_aggression = ag; }
				void posture(BattleOrderInfo::Posture pos) { d_agPos.d_posture = pos; }
				void sendCPAggPos();

            int frontWidth() const { return d_planner->frontWidth(); }

            HexCord leftFront() const { return d_planner->leftFront(); }
            HexCord rightFront() const { return d_planner->rightFront(); }

            void setFlag(UWORD mask, bool f)
            {
               if(f)
                  d_flags |= mask;
               else
                  d_flags &= ~mask;
            }
            enum Flags {
               GetNewObjective       = 0x0001,
               TakingHeavyLosses     = 0x0002,
               InflictingHeavyLosses = 0x0004,
               Retreating            = 0x0008,
               EnemyRetreating       = 0x0010,
               RoutingFleeing        = 0x0020,
               MovingToFront         = 0x0040,  // a reserve obj that has been order up front
               Reinforcement         = 0x0080,  // unit has just arrived as reinforcement
               FlankedRight          = 0x0100,
               FlankedLeft           = 0x0200,
               FlankedRear           = 0x0400,
               LaunchingCounter      = 0x0800,
               Reorienting           = 0x1000  // a corp is reorienting its facing
            };

            void getNewObjective(bool f)       { setFlag(GetNewObjective, f); }
            void takingHeavyLosses(bool f)     { setFlag(TakingHeavyLosses, f); }
            void inflictingHeavyLosses(bool f) { setFlag(InflictingHeavyLosses, f); }
            void retreating(bool f)            { setFlag(Retreating, f); }
            void enemyRetreating(bool f)       { setFlag(EnemyRetreating, f); }
            void routingFleeing(bool f)        { setFlag(RoutingFleeing, f); }
            void movingToFront(bool f)         { setFlag(MovingToFront, f); }
            void reinforcement(bool f)         { setFlag(Reinforcement, f); }
            void flankedLeft(bool f)           { setFlag(FlankedLeft, f); }
            void flankedRight(bool f)          { setFlag(FlankedRight, f); }
            void flankedRear(bool f)           { setFlag(FlankedRear, f); }
            void launchingCounter(bool f)      { setFlag(LaunchingCounter, f); }
            void reorienting(bool f)           { setFlag(Reorienting, f); }

            bool getNewObjective()       const { return (d_flags & GetNewObjective) != 0; }
            bool takingHeavyLosses()     const { return (d_flags & TakingHeavyLosses) != 0; }
            bool inflictingHeavyLosses() const { return (d_flags & InflictingHeavyLosses) != 0; }
            bool retreating()            const { return (d_flags & Retreating) != 0; }
            bool enemyRetreating()       const { return (d_flags & EnemyRetreating) != 0; }
            bool routingFleeing()        const { return (d_flags & RoutingFleeing) != 0; }
            bool movingToFront()         const { return (d_flags & MovingToFront) != 0; }
            bool reinforcement()         const { return (d_flags & Reinforcement) != 0; }
            bool flankedLeft()           const { return (d_flags & FlankedLeft) != 0; }
            bool flankedRight()          const { return (d_flags & FlankedRight) != 0; }
            bool flankedRear()           const { return (d_flags & FlankedRear) != 0; }
            bool launchingCounter()      const { return (d_flags & LaunchingCounter) != 0; }
            bool reorienting()      const { return (d_flags & Reorienting) != 0; }

				friend bool operator==(const RealHiObjective&lhs, const RealHiObjective& rhs);
				friend bool operator<(const RealHiObjective&lhs, const RealHiObjective& rhs);

            void objectiveAchieved(bool f) { d_planner->objectiveAchieved(True); }
				bool objectiveAchieved() const { return d_planner->objectiveAchieved(); }//(d_flags & ObjectivedAchieved); }
				void procMessage(const BattleMessageInfo& msg);

            MidLevelPlanner* planner() const { return d_planner; }
            HexAreaList& areaList() { return d_list; }

            void newDay() { d_planner->newDay(); }
#ifdef DEBUG
            void logHistorical(GameInfo* gameInfo);
#endif

				// file interface
				bool readData(FileReader& f, OrderBattle* ob);
				bool writeData(FileWriter& f, OrderBattle* ob) const;
		private:
				HexAreaList d_list;
				UnitList d_units;
				MidLevelPlanner* d_planner;
				Mode d_mode;
				BAI_AggressPosStruct d_agPos;
            UWORD d_flags;
};

inline bool operator==(const RealHiObjective& lhs, const RealHiObjective& rhs)
{
		return lhs.area() == rhs.area();
}

inline bool operator<(const RealHiObjective& lhs, const RealHiObjective& rhs)
{
		return lhs.area() < rhs.area();
}



class HiObjectiveList :
		public STLContainer<std::vector<RealHiObjective*> >
{
		public:
         HiObjectiveList() { }
        ~HiObjectiveList();

         void reserve(Container::size_type n) { Container::reserve(n); }
		   // RealHiObjective* add(const RealHiObjective& ob);
		   // void add(Container::size_type n, const RealHiObjective& ob);
		   RealHiObjective* create(GameInfo* gameInfo, HiObjective::Mode mode);

         RealHiObjective* operator[](Container::size_type n) { return Container::operator[](n); }
         const RealHiObjective* operator[](Container::size_type n) const { return Container::operator[](n); }

			// file interface
			bool readData(FileReader& f, GameInfo* gameInfo);
			bool writeData(FileWriter& f, const GameInfo* gameInfo) const;
	 private:
};

};  // namespace WG_BattleAI_Internal


#endif /* AIB_HIOB_HPP */

