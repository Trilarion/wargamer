/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 * Player Message Queue
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "aib_msg.hpp"
#include "batmsg.hpp"
#include "filebase.hpp"
//#include "sync.hpp"
#include <list>

namespace WG_BattleAI_Internal
{

class RealQueue : 
   public std::list<BattleMessageInfo*>,
   public SharedData
{
};

BattleMessageQueue::BattleMessageQueue() :
   d_queue(new RealQueue)
{
   ASSERT(d_queue);
}

BattleMessageQueue::~BattleMessageQueue()
{
   if(d_queue)
   {
      while(!isEmpty())
      {
         pop();
      }
      delete d_queue;
   }
}

void BattleMessageQueue::push(const BattleMessageInfo& msg)
{
   if(d_queue)
   {
      LockData lock(d_queue);

      BattleMessageInfo* newMsg = new BattleMessageInfo(msg);
      ASSERT(newMsg);

      //--- Useless code... queue is already locked
      // BattleMessageInfo::WriteLock msgLock(newMsg);
      d_queue->insert(d_queue->begin(), newMsg);
   }
}

bool BattleMessageQueue::isEmpty() const
{
      LockData lock(d_queue);
      return (!d_queue || d_queue->empty());
}

void BattleMessageQueue::pop()
{
      LockData lock(d_queue);
      ASSERT(!isEmpty());

      RealQueue::iterator bmsg = d_queue->begin();
      BattleMessageInfo* msg= *bmsg;
      // ASSERT((*bmsg));
      ASSERT(msg);

      d_queue->pop_front();
      // delete (*bmsg);
      delete msg;
}

const BattleMessageInfo& BattleMessageQueue::get() const
{
      LockData lock(d_queue);
      ASSERT(!isEmpty());

//    static BattleMessageInfo dummy;
      RealQueue::iterator bmsg = d_queue->begin();
      BattleMessageInfo* msg = *bmsg;
      //--- This doesn't do anything useful!
      // BattleMessageInfo::ReadLock msgLock((*bmsg));

      // ASSERT((*bmsg));
      // return *(*bmsg);
      ASSERT(msg);
      return *msg;
}

static UWORD s_version = 0x0000;
bool BattleMessageQueue::readData(FileReader& f)
{
   UWORD version;
   f >> version;
   ASSERT(version <= s_version);

   // TODO: read message data
   // Do we really need to do this??
   return f.isOK();
}

bool BattleMessageQueue::writeData(FileWriter& f) const
{
   f << s_version;
   // TODO: read message data
   // Do we really need to do this??
   return f.isOK();
}


};  // namespace WG_BattleAI_Internal


/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.3  2001/07/30 09:27:23  greenius
 * Various bug fixes
 *
 * Revision 1.2  2001/06/18 08:25:43  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
