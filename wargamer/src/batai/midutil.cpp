/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
#include "stdinc.hpp"
#include "midutil.hpp"
#include "bob_cp.hpp"
#include "bobiter.hpp"
#include "batcord.hpp"
#include "aib_area.hpp"
#include "m_cp.hpp"
#include "aib_util.hpp"
#include "batdata.hpp"
#include "bobutil.hpp"
#include "pdeploy.hpp"
#include "bobdef.hpp"

using namespace BOB_Definitions;


namespace WG_BattleAI_Internal {

inline int squared(int v)
{
   return (v * v);
}

inline double getDist(const HexCord& srcHex, const HexCord& destHex)
{
   return sqrt(squared(destHex.x() - srcHex.x()) + squared(destHex.y() - srcHex.y()));
}

// is hex2 in the boundry of hex1?
bool inBoundrySide(
   HexPosition::Facing facing,
   const HexCord& hex1,
   const HexCord& hex2,
   bool left)
{
   WG_BattleAI_Utils::MoveOrientation mo1 = WG_BattleAI_Utils::moveOrientation(
      facing, hex1, hex2, WG_BattleAI_Utils::ObjectiveAngle);

   WG_BattleAI_Utils::MoveOrientation mo2 = WG_BattleAI_Utils::moveOrientation(
      facing, hex1, hex2, WG_BattleAI_Utils::ShortAngle);

   WG_BattleAI_Utils::MoveOrientation mo3;
   if(left)
   {
      mo3 = (mo1 == WG_BattleAI_Utils::MO_Front) ?  WG_BattleAI_Utils::MO_Left : WG_BattleAI_Utils::MO_Right;
   }
   else
   {
      mo3 = (mo1 == WG_BattleAI_Utils::MO_Front) ?  WG_BattleAI_Utils::MO_Right : WG_BattleAI_Utils::MO_Left;
   }

   return (mo2 != mo3);
}

// is CP in objective boundry??
bool inBoundry(const CRefBattleCP& cp, const HexArea& objArea)
{
   if(cp->getRank().isHigher(Rank_Division))
      return False;

   HexPosition::Facing ourFace = WG_BattleAI_Utils::frontFacing(objArea[0], objArea[1]);

   HexCord enemyCenter = const_cast<BattleCP*>(cp)->centerHex();

   if(!inBoundrySide(ourFace, objArea[0], enemyCenter, True) ||
      !inBoundrySide(ourFace, objArea[1], enemyCenter, False))
   {
      return False;
   }

   return True;
}

bool allHolding(const CRefBattleCP& corpCP)
{
   for(ConstBattleUnitIter iter(corpCP); !iter.isFinished(); iter.next())
   {
      if( (iter.cp()->getRank().sameRank(Rank_Division)) &&
          (!iter.cp()->holding() || iter.cp()->getCurrentOrder().wayPoints().entries() > 0) )
      {
         return False;
      }
   }

   return True;
}

bool frontHolding(MidUnitInfo& mi)
{
   for(int c = 0; c < mi.map().nFront(); c++)
   {
      MU_DeployItem& di = mi.map().item(c, 0);
      if(di.cp()->hasQuitBattle())
         continue;

      if( (di.cp()->getRank().sameRank(Rank_Division)) &&
          (di.bombarding() || di.attacking() || !di.cp()->holding() || di.cp()->getCurrentOrder().wayPoints().entries() > 0) )
      {
         return False;
      }
   }

   return True;
}

void clearMove(MU_DeployItem& di)
{
   di.attacking(False);
   di.order().wayPoints().reset();
   di.order().order().d_whatOrder.reset();
   di.order().mode(BattleOrderInfo::Hold);
}

void setHoldOrder(MU_DeployItem& di)
{
   clearMove(di); // clear any move orders

   di.ordered(False);
   di.order().mode(BattleOrderInfo::Hold);
}

void clearAllMoves(MidUnitInfo& mi)
{
   MidUnitDeployMap& map = mi.map();

   for(int r = 0; r < 2; r++)
   {
      int nCols = (r == 0) ? map.nFront() : map.nRear();
      for(int c = 0; c < nCols; c++)
      {
         MU_DeployItem& di = map.item(c, r);
         if(!di.cp()->hasQuitBattle())
            clearMove(di);
      }
   }
}

bool enemyInSight(const CRefBattleCP& cp, const BattleData* bd)
{
   for(ConstBattleUnitIter iter(cp); !iter.isFinished(); iter.next())
   {
      if(!iter.cp()->hasQuitBattle())
      {
         // is enemy in our sights??
         if(iter.cp()->targetList().entries() > 0)
            return True;

         // if not, cheat a little
         for(ConstBattleUnitIter eIter(bd->ob(), (cp->getSide() == 0) ? 1 : 0);
             !eIter.isFinished();
             eIter.next())
         {
            if(BobUtility::distanceFromUnit(const_cast<BattleCP*>(iter.cp()), const_cast<BattleCP*>(eIter.cp())) <= 10)
            {
               return True;
            }
         }
      }
   }
   return False;
}

bool corpsInLine(MidUnitInfo& mi, bool resetOnly)
{
   // first, reset flags
   for(int r = 0; r < 2; r++)
   {
      int nCols = (r == 0) ?  mi.map().nFront() : mi.map().nRear();
      for(int c = 0; c < nCols; c++)
      {
         MU_DeployItem& di1 = mi.map().item(c, r);
         if(!di1.cp()->hasQuitBattle())
         {
            di1.adjustCorpHolding(False);
//            di1.adjustCorpMoving(False);
         }
      }
   }

   if(resetOnly)
      return True;

   // now test to see if corp is in proper formation
   int inLine = 0;
   for(r = 0; r < 2; r++)
   {
      //if(frontOnly & r == 1)
      //   break;

      int nCols = (r == 0) ? mi.map().nFront() : mi.map().nRear();
      for(int c = 0; c < nCols; c++)
      {
         MU_DeployItem& di1 = mi.map().item(c, r);
#ifdef DEBUG
         const char* name1 = di1.cp()->getName();
#endif
         if(!di1.cp()->hasQuitBattle())
         {
            // check to see if we are aligned to the left
            if(c + 1 < nCols)
            {
               MU_DeployItem* di2 = &mi.map().item(c + 1, r);
#ifdef DEBUG
               const char* name2 = di2->cp()->getName();
#endif
               //HexCord ourLeft = di1.cp()->leftHex();
               //HexCord nextLeft = di2.cp()->leftHex();
               bool isScout = (r == 0 && (di1.cp()->generic()->isCavalry()));
               bool shouldSet = True;
               if(isScout)
               {
                  WG_BattleAI_Utils::MoveOrientation moR = WG_BattleAI_Utils::moveOrientation(di1.cp()->facing(), di1.cp()->rightHex(), di2->cp()->leftHex(), WG_BattleAI_Utils::ObjectiveAngle);
                  MU_DeployItem* di3 = (c > 0) ? &mi.map().item(c - 1, r) : 0;
                  WG_BattleAI_Utils::MoveOrientation moL = (di3) ? WG_BattleAI_Utils::moveOrientation(di1.cp()->facing(), di1.cp()->leftHex(), di3->cp()->rightHex(), WG_BattleAI_Utils::ObjectiveAngle) : WG_BattleAI_Utils::MO_Front;
                  if( (moR == WG_BattleAI_Utils::MO_Rear && BobUtility::distanceFromUnit(const_cast<BattleCP*>(di1.cp()), const_cast<BattleCP*>(di2->cp())) <= 3) ||
                      (di3 && moL == WG_BattleAI_Utils::MO_Rear && BobUtility::distanceFromUnit(const_cast<BattleCP*>(di1.cp()), const_cast<BattleCP*>(di3->cp())) <= 3) )
                  {
                     shouldSet = False;
                     di1.adjustCorpHolding(False);
                     inLine = maximum(0, inLine - 1);
                  }
               }
               if(shouldSet)
               {
                  while(di2 &&
                        r == 0 &&
                        di2->cp()->generic()->isCavalry())
                  {
                     if(c + 2 >= nCols)
                        di2 = 0;
                     else
                     {
                        di2 = &mi.map().item(c + 2, r);
                     }
                  }

                  if(di2)
                  {
                     WG_BattleAI_Utils::AngleType at = (isScout) ? //WG_BattleAI_Utils::WideAngle;//(r == 0 || di1.cp()->generic()->isCavalry() || di2.cp()->generic()->isCavalry()) ?
                        WG_BattleAI_Utils::NormalAngle : WG_BattleAI_Utils::WideAngle;

                     WG_BattleAI_Utils::MoveOrientation mo = WG_BattleAI_Utils::moveOrientation(di1.cp()->facing(), di1.cp()->rightHex(), di2->cp()->leftHex(), at);
                     if(mo == WG_BattleAI_Utils::MO_Front)
                     {
                        di2->adjustCorpHolding(True);
                        inLine++;
                     }
                     else if(mo == WG_BattleAI_Utils::MO_Rear)
                     {
                        di1.adjustCorpHolding(True);
                        inLine++;
                     }
                  }
               }
            }

            // checke to see if we are aligned to the rear
            if(//!frontOnly &&
               r == 0 &&
               c < mi.map().nRear() &&
               !di1.cp()->generic()->isCavalry())
            {
               MU_DeployItem& di2 = mi.map().item(c, 1);
#ifdef DEBUG
               const char* name2 = di2.cp()->getName();
#endif
               HexCord ourLeft = di1.cp()->leftHex();
               HexCord nextLeft = di2.cp()->leftHex();

               //int dist = static_cast<int>(getDist(ourLeft, nextLeft));
               BattleCP* cp1 = const_cast<BattleCP*>(di1.cp());
               BattleCP* cp2 = const_cast<BattleCP*>(di2.cp());
               int dist = BobUtility::distanceFromUnit(cp1, cp2);
               WG_BattleAI_Utils::MoveOrientation mo = WG_BattleAI_Utils::moveOrientation(di1.cp()->facing(), ourLeft, nextLeft, WG_BattleAI_Utils::ObjectiveAngle);
               if(dist < 2 ||
                  mo == WG_BattleAI_Utils::MO_Front)
               {
                  di2.adjustCorpHolding(True);
                  inLine++;
               }
               else if(dist > 6)
               {
                  di1.adjustCorpHolding(True);
                  inLine++;
               }
            }
         }
      }
   }

   return (inLine == 0);
}

void setMoveOrder(MU_DeployItem& di, HexCord hex, HexPosition::Facing facing, bool clearOld)
{
#ifdef DEBUG
   if(lstrcmpi("VI Corps Artillery", di.cp()->getName()) == 0)
   {
      int i = 0;
   }
#endif
   if(clearOld)
      clearMove(di);
   di.ordered(False);
   di.objHex(hex);
   WG_BattleAI_Utils::setMoveOrder(&di.order(), hex, facing);
}
void setAttachOrder(MU_DeployItem& di, const CRefBattleCP& cp)
{
   clearMove(di); // clear any move orders

   di.ordered(False);
   di.order().mode(BattleOrderInfo::Attach);
   di.order().attachTo(const_cast<BattleCP*>(cp));
}

bool canDeploy(const BattleData* bd, MU_DeployItem& di, CPFormation divFor)
{
   if(divFor == di.cp()->formation())
      return False;

   int c = 0;
   int r = 0;
   int spCount = di.cp()->spCount();
   BobUtility::getRowsAndColoumns(divFor, spCount, c, r);

   int goLeft = 0;
   if(di.order().deployHow() == CPD_DeployCenter)
   {
      goLeft = (c % 2 == 0) ? maximum(0, (c / 2) - 1) : c / 2;
   }
   else if(di.order().deployHow() == CPD_DeployLeft)
   {
      goLeft = c - di.cp()->columns() + 1;
   }

   HexCord left = di.cp()->leftHex();
   if(goLeft > 0)
   {
      HexCord hex;
      if(WG_BattleAI_Utils::moveHex(bd, BattleMeasure::leftFlank(di.cp()->facing()), goLeft, left, hex))
      {
         left = hex;
      }
      else
         return False;
   }

   return PlayerDeploy::playerDeploy(const_cast<BattleData*>(bd), const_cast<BattleCP*>(di.cp()), left, 0, PlayerDeploy::TERRAINONLY);
}
}; // End namespace MidOpUtil

/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.3  2002/11/24 13:17:47  greenius
 * Added Tim Carne's editor changes including InitEditUnitTypeFlags.
 *
 * Revision 1.2  2001/06/18 08:25:43  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
