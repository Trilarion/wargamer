/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef AIB_UTIL_HPP
#define AIB_UTIL_HPP

#include "batcord.hpp"
#include "batunit.hpp"
#include "batdata.hpp"
#include "batord.hpp"
#include "hiopplan.hpp"
#include <math.h>
#include "sllist.hpp"
#include "aib_sobj.hpp"
#include "aib_info.hpp"
using BattleMeasure::HexPosition;
using BattleMeasure::HexCord;
using WG_BattleAI_Internal::Hi_OpPlan;
using WG_BattleAI_Internal::BAI_StrObjList;
using WG_BattleAI_Internal::BAI_StrObj;
using WG_BattleAI_Internal::GameInfo;
class RandomNumber;
//class BAI_StrObjList;
namespace WG_BattleAI_Utils
{
struct LocalCP : public SLink {
   CRefBattleCP d_cp;

   LocalCP(CRefBattleCP cp) : d_cp(cp) {}

   void* operator new(size_t size);
#ifdef _MSC_VER
   void operator delete(void* deadObject);
#else
   void operator delete(void* deadObject, size_t size);
#endif
};

class LocalCPList : public SList<LocalCP> {
public:
   LocalCP* add(const CRefBattleCP& cp)
   {
      LocalCP* lcp = find(cp);
      if(!lcp)
      {
         lcp = new LocalCP(cp);
         ASSERT(lcp);

         append(lcp);
      }

      return lcp;
   }

   LocalCP* addInsert(const CRefBattleCP& cp)
   {
		LocalCP* lcp = find(cp);
		if(!lcp)
      {
         lcp = new LocalCP(cp);
				 ASSERT(lcp);

         insert(lcp);
      }

      return lcp;
   }

   LocalCP* addInsertAfter(const CRefBattleCP lastCP, const CRefBattleCP& cp)
   {
      LocalCP* lcp = find(cp);
      if(!lcp)
      {
         lcp = new LocalCP(cp);
         ASSERT(lcp);

         LocalCP* llastCP = find(lastCP);
         if(llastCP)
            insertAfter(llastCP, lcp);
         else
            append(lcp);
      }

      return lcp;
   }

   void removeCP(const CRefBattleCP& cp)
   {
      LocalCP* lcp = find(cp);
      if(lcp)
         remove(lcp);
   }

   LocalCP* find(const CRefBattleCP& cp)
   {
      for(LocalCP* lcp = first(); lcp != 0; lcp = next())
      {
         if(lcp->d_cp == cp)
            return lcp;
      }

      return 0;
   }

   bool inList(const CRefBattleCP& cp)
   {
      return (find(cp) != 0);
   }

   LocalCPList& operator = (const LocalCPList& l)
	 {
			reset();
      SListIterR<LocalCP> iter(&l);
			while(++iter)
			{
//				 if(!find(iter.current()->d_cp))
						add(iter.current()->d_cp);
			}

			return *this;
	 }

	 LocalCPList& operator += (const LocalCPList& l)
	 {
//    reset();
			SListIterR<LocalCP> iter(&l);
			while(++iter)
			{
//				 if(!find(iter.current()->d_cp))
						add(iter.current()->d_cp);
			}

			return *this;
	 }
};


inline int squared(int v) { return (v * v); }
inline int getDist(const HexCord& srcHex, const HexCord& destHex)
{
   return static_cast<int>(sqrt(squared(destHex.x() - srcHex.x()) + squared(destHex.y() - srcHex.y())));
}

HexPosition::Facing frontFacing(const HexCord& leftHex, const HexCord& rightHex);
   // Which direction does the front face? (as defined by the line leftHex - rightHex)

enum GoWhichWay {
  GoLeft,
   GoRight,
  GoNone
};
GoWhichWay whichWay(HexPosition::Facing cf, HexPosition::Facing wf);

enum MoveOrientation {
   MO_Front,
   MO_Left,
   MO_Right,
   MO_Rear
};
enum AngleType {
	NormalAngle,
	ShortAngle,
	WideAngle,
   ObjectiveAngle,  // returns either front or rear only
   BoundryAngle,    // returns either left or right only
   LessExtremeShortAngle  
};

MoveOrientation moveOrientation(
	 const HexPosition::Facing facing,
	 const HexCord& srcHex,
	 const HexCord& destHex,
	 AngleType type = NormalAngle);
   // in what general orientation are we moving?
   // i.e. either Front, Left, Right, or Rear

bool moveHex(
   RCPBattleData bd,
   HexCord::HexDirection hd,
   int howMany,
   const HexCord& startHex,
   HexCord& hex);

bool moveHex(
   RCPBattleData bd,
   HexPosition::Facing facing,
   int howManyLR,
   int howManyFB,
   const HexCord& startHex,
   HexCord& hex);

int countSP(RCPBattleData bd, const CRefBattleCP& cp, bool deploying = False);

bool needsOrder(const CRefBattleCP& cp);
   // see if all units are just setting around

HexPosition::Facing closeFace(HexPosition::Facing f);
HexPosition::Facing averageFacing(RCPBattleData bd, Side s);
HexPosition::Facing averageFacing(const CRefBattleCP& cp);
//bool unitsOnMap(RCPBattleData bd, Side side);

   // Are units already on map at AI init (a historical battle, i.e.)
Hi_OpPlan::Plan selectPlan(
//	 BAI_StrObjList& obl,
//   HexCord mapSize,
	 GameInfo* gi,
	 int nCorps,
	 int& nZones);
//   RandomNumber& lRand,
//	 BattleOrderInfo::Posture pos);

void getCorpLeftRightBoundry(
   const CRefBattleCP& cp,
   HexPosition::Facing avgFace,
   HexCord& leftHex,
   HexCord& rightHex,
   LocalCPList* cpList = 0);

bool findDeployPosition(
      RCPBattleData bd,
      const CRefBattleCP& cp,
      HexPosition::Facing facing,
      HexCord& hex,
      bool isDeploying);

int getRange(RPBattleData bd, const CRefBattleCP& cp, bool current = True);
void setMoveOrder(
	 BattleOrder* batOrder,
	 const HexCord& tHex,
	 const HexPosition::Facing facing);

bool unitIsFacing(HexPosition::Facing facing, const CRefBattleCP& cp2);
bool unitsAreFacing(const CRefBattleCP& cp1, const CRefBattleCP& cp2);
int distanceFromUnit(const HexCord& hex, const RefBattleCP& cp);
bool hasGuard(const CRefBattleCP& cp);
	// Our 2 opposing units facing each other?

enum StrRatio {
   Str3To1,
   Str2To1,
   Str1_5To1,
   Str1_2To1,
   Str_Equal,
   Str1To1_2,
   Str1To1_5,
   Str1To2,
   Str1To3,

   Str_HowMany
};
StrRatio getSideStrRatio(RCPBattleData bd, const CRefBattleCP& cinc);
#ifdef DEBUG
const char* strRatioText(StrRatio r);
#endif
}; // end namespace WG_BattleAI_Utils

#endif
