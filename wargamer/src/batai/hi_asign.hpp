/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef HI_ASIGN_HPP
#define HI_ASIGN_HPP

#ifndef __cplusplus
#error hi_asign.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 * Hi Level Corps Asigner
 *
 *----------------------------------------------------------------------
 */

#include "aib_info.hpp"
#include "batcord.hpp"
using BattleMeasure::HexCord;
using BattleMeasure::HexPosition;
namespace WG_BattleAI_Internal
{

class HiZoneList;
class HiUnitList;
class HiUnitInfo;
class HiZone;

/*==========================================================
 * High level Corps Assigner
 */

class HiCorpsAssigner : public GameInfoUser
{
		public:
				HiCorpsAssigner(GameInfo* gameInfo, HiZoneList* zones, HiUnitList* units);
				~HiCorpsAssigner();

				void assignUnits();

		private:
		        void assignUnitPriority(HiUnitInfo* unit);
				void assignUnit(HiUnitInfo* unit);
				void assignUnit(HiUnitInfo* unit, HiZone* zone);
				void unassignUnit(HiUnitInfo* unit);
				void getCorpLeftRightBoundry(const CRefBattleCP& cp, HexPosition::Facing avgFace, HexCord& leftHex, HexCord& rightHex);
				void assignUnitAlreadyOnMap(HiUnitInfo* unit);
				bool HiCorpsAssigner::inZoneBoundry(
						const HiZone* zone,
						HexPosition::Facing corpAvgFacing,
						const HexCord& corpLeft,
						const HexCord& corpRight);

    private:
        HiZoneList* d_zones;
        HiUnitList* d_units;
};


};  // namespace WG_BattleAI_Internal


#endif /* HI_ASIGN_HPP */

