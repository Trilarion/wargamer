/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef AIB_DATA_HPP
#define AIB_DATA_HPP

#ifndef __cplusplus
#error aib_data.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 * Common Shared Data accessed by most levels of the AI
 *
 *----------------------------------------------------------------------
 */

#include "aib_info.hpp"
#include "batdata.hpp"
#include "aib_sobj.hpp"
//#include "sync.hpp"

class FileReader;
class FileWriter;
class BattleMessageInfo;

namespace WG_BattleAI_Internal
{

/*
 * Top Level class accessible by controllers
 */

class BattleAIData : public GameInfo
{
		public:
				BattleAIData(Side s, const BattleData* batData);
				~BattleAIData();

				// Processing functions, called from BattleAISide

				void procMessage(const BattleMessageInfo& msg);
				void process();

				/*
				 * Information Functions to implement GameInfo
				 */

				virtual ConstBattleUnitIter ourUnits() const;
				virtual const BattleMap* map() const { return d_batData->map(); }
				virtual const BattleData* battleData() const { return d_batData; }
				virtual const BAI_StrObjList& strObjectives() const { return d_strObjectives; }
				virtual const CRefBattleCP& cinc() const { return d_cinc; }
				virtual Hi_OpPlan::Plan plan() const { return d_plan; }

				virtual void plan(Hi_OpPlan::Plan plan) { d_plan = plan; }

				// TODO:
				//  get info about enemy, etc...
				void setFlag(UBYTE mask, bool f)
				{
					if(f)
						d_flags |= mask;
					else
						d_flags &= ~mask;
				}

            // file interface
				bool readData(FileReader& f);
            bool writeData(FileWriter& f) const;
		private:
				// private functions
				void updateStrObjectives();
				void setCinc();
				void getPlan();
		private:
				const BattleData* d_batData;
				BAI_StrObjList    d_strObjectives; // strategic objectives(s) for this side
				Hi_OpPlan::Plan   d_plan;
				enum Flags {
					UpdateStrObjectives = 0x01
				};
				UBYTE d_flags;
				CRefBattleCP d_cinc; // Cinc of AI

//        RWLock d_lock;  //thread lock
};


};  // namespace WG_BattleAI_Internal


#endif /* AIB_DATA_HPP */

