/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef HI_ZONE_HPP
#define HI_ZONE_HPP

#ifndef __cplusplus
#error hi_zone.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 * High level AI Zones
 *
 *----------------------------------------------------------------------
 */

#include "aib_hiob.hpp"     // Objectives
#include "aib_info.hpp"
#include "battime.hpp"
#include "batdata.hpp"
#include "sllist.hpp"
#include "ba_agpos.hpp"
#ifdef DEBUG
// #include <string.hpp>
#endif

class FileReader;
class FileWriter;
class BattleMessageInfo;
namespace WG_BattleAI_Internal
{
#include "plist.hpp"		// if not in namespace, the compiler locks up

class GameInfo;

/*
 * forward declerations
 */

class HiUnitInfo;
class HiLeaderInfo;
class HiZone;

/*
 * Some typedefs to reduce typing
 */

typedef PtrList<HiUnitInfo> HiUnitPList;
typedef PtrList<HiLeaderInfo> HiLeaderPList;
// typedef PtrList<HiZone> HiZonePList;
// typedef PtrList<RealHiObjective> HiObjectivePList;

class Boundry : public SLink {
		HexCord d_leftHex;
		HexCord d_rightHex;
	public:
		Boundry() :
			d_leftHex(),
			d_rightHex() {}
		Boundry(const HexCord& leftHex, const HexCord& rightHex) :
			d_leftHex(leftHex),
			d_rightHex(rightHex) {}
		Boundry(const Boundry& b) :
			d_leftHex(b.d_leftHex),
			d_rightHex(b.d_rightHex) {}

		Boundry& operator = (const Boundry& b)
		{
			d_leftHex = b.d_leftHex;
			d_rightHex = b.d_rightHex;
		return *this;
		}

		void leftHex(const HexCord& hex) { d_leftHex = hex; }
		void rightHex(const HexCord& hex) { d_rightHex = hex; }
		HexCord leftHex() const { return d_leftHex; }
		HexCord rightHex() const { return d_rightHex; }

		bool readData(FileReader& f);
		bool writeData(FileWriter& f) const;
};

class BoundryList : public SList<Boundry> {
public:
	Boundry* add(const HexCord& leftHex, const HexCord& rightHex)
	{
		Boundry* b = new Boundry(leftHex, rightHex);
		ASSERT(b);
		append(b);
		return b;
	}

	HexCord leftHex() const { return (entries() > 0) ? head()->leftHex() : HexCord(0, 0); }
	HexCord rightHex() const { return (entries() > 0) ? head()->rightHex() : HexCord(0, 0); }

	void removeTop()
	{
		if(entries() > 0)
		{
			Boundry* b = first();
			remove(b);
		}
	}

	bool readData(FileReader& f);
	bool writeData(FileWriter& f) const;

};

enum ZoneOrientation {
	ZO_Centered,
	ZO_Left,
	ZO_Right,

	ZO_HowMany,
	ZO_Default = ZO_Centered,
};

/*
 * A High level Zone is a strip of battlefield
 *
 * It may contain:
 *      a list of units
 *      a list of leaders
 *      a list of objectives
 */

class HiZone : public GameInfoUser
{
			 friend class BAI_ScriptReader;
		public:
				~HiZone();
				// HiZone();   // empty constructor to keep STL happy
				HiZone(GameInfo* gameInfo, const HexCord& left, const HexCord& right);
				HiZone(GameInfo* gameInfo);

				/*
				 * Algorithms
				 */

				void updateObjectives();
						// Assign objectives to units
				void executeObjectives();
						// get Mid Leve stuff to do its magic...

				void procMessage(const BattleMessageInfo& msg);

            void newDay();
#ifdef DEBUG
            void logHistorical();
#endif
				/*
				 * Accessors
				 */

				void addUnit(HiUnitInfo* unit);
				void removeUnit(HiUnitInfo* unit);
				HiUnitPList* units() { return &d_units; }
				const HiUnitPList* units() const { return &d_units; }

				void addBoundry(const HexCord& leftHex, const HexCord& rightHex)
				{
					d_boundries.add(leftHex, rightHex);
				}
				void removeTopBoundry()
				{
					d_boundries.removeTop();
				}
				int nBoundries() const { return d_boundries.entries(); }

//				void leftHex(const HexCord& hex) { d_left = hex; }
//				void rightHex(const HexCord& hex) { d_right = hex; }

				HexCord leftHex()         const { return d_boundries.leftHex(); } //d_left; }
				HexCord rightHex()        const { return d_boundries.rightHex(); } //d_right; }
				HexCord::Cord  leftX()    const { return d_boundries.leftHex().x(); } //d_left.x(); }
				HexCord::Cord  rightX()   const { return d_boundries.rightHex().x(); }//d_right.x(); }
				HexCord::Cord  leftY()    const { return d_boundries.leftHex().y(); } //d_left.y(); }
				HexCord::Cord  rightY()   const { return d_boundries.rightHex().y(); } //d_right.y(); }
            HexCord leftFront() const;
            HexCord rightFront() const;

//				HexArea& area() { return d_area; }
//				const HexArea& area() const { return d_area; }

				int priority() const { return d_priority; }
				void priority(int p) { d_priority = p; }

				int spCount() const;
						// number of SPs in zone

				bool objectiveAchieved() const;

				void startWhen(BattleMeasure::BattleTime::Tick sw) { d_startWhen = sw; }
				bool canStart(BattleMeasure::BattleTime::Tick time) const { return (time >= d_startWhen); }
				BattleMeasure::BattleTime::Tick startWhen() const { return d_startWhen; }

				void aggression(BattleOrderInfo::Aggression ag) { d_agPos.d_aggression = ag; }
				void posture(BattleOrderInfo::Posture pos) { d_agPos.d_posture = pos; }

				BattleOrderInfo::Aggression aggression() const { return d_agPos.d_aggression; }
				BattleOrderInfo::Posture posture() const { return d_agPos.d_posture; }

				void zoneOrientation(ZoneOrientation zo) { d_zoneOrientation = zo; }
				ZoneOrientation zoneOrientation() const { return d_zoneOrientation; }
				HiUnitInfo* getUnit(CRefBattleCP cp);

				const HiObjectiveList& objectives() const { return d_objectives; }
				HiObjectiveList& objectives() { return d_objectives; }
				bool readData(FileReader& f);
				bool writeData(FileWriter& f) const;
            void initObjectives();

#ifdef DEBUG
				const char* name() const { return d_name.c_str(); }
#endif

	private:
			 void setObjectiveArea(RealHiObjective* obj, HexCord leftHex, HexCord rightHex, int depth, bool downMap);
			 void addUnitToObjective(HiUnitInfo* unit, RealHiObjective* obj);
			 void removeUnitFromObjective(HiUnitInfo* unit, RealHiObjective* obj);
			 void moveUnitToObjective(HiUnitInfo* unit, RealHiObjective* from, RealHiObjective* to);
//			 void assignUnitsOnMap();
			 void assignOrder(HiUnitInfo* unit);
			 void countObjTypes(int& nFrontLine, int& nReserve);
          void getNewObjective(RealHiObjective* obj, bool removeOld = True);
          bool shouldUpdateObjective(RealHiObjective* obj, bool allAchieved, bool& removeOld);
          void checkReserves();
          void bringUpReserve(RealHiObjective* rObj, RealHiObjective* fObj);
          void addNewUnits();
          void addNewUnit(HiUnitInfo* unit);
          public:
          void setZoneFlags();
          bool updateZone();
#if 0
          void moveLR(int howMany) { d_moveLR = howMany; }
          void moveFB(int howMany) { d_moveFB = howMany; }
          int moveLR() const { return d_moveLR; }
          int moveFB() const { return d_moveFB; }
#endif
          private:
          bool launchCounterAttack(HexCord hex);
          bool launchCounterAttack(RealHiObjective* obj);
          RealHiObjective* findAttackObj(HexCord hex);
          bool engaged(RealHiObjective* obj);

          void setFlag(UWORD mask, bool f)
          {
            if(f)
               d_flags |= mask;
            else
               d_flags &= ~mask;
          }

          void setHiFlag(UBYTE mask, bool f)
          {
            if(f)
               d_hiFlags |= mask;
            else
               d_hiFlags &= ~mask;
          }
          public:
          bool lastObjReached() const;
          enum GoWhichWay {
            Left,
            Front,
            Right,
            WhichWay_Undefined,
            WhichWay_HowMany
          };
          void whichWay(GoWhichWay whichWay) { d_whichWay = whichWay; }
          GoWhichWay whichWay() const { return d_whichWay; }

          void unitsAdded(bool f)            { setFlag(UnitsAdded, f); }
          void inflictedCriticalLoss(bool f) { setFlag(InflictedCriticalLoss, f); }
          void inflictedLoss(bool f)         { setFlag(InflictedLoss, f); }
          void enemyRetreating(bool f)       { setFlag(EnemyRetreating, f); }
          void enemyLowMorale(bool f)        { setFlag(EnemyLowMorale, f); }
          void enemyWavering(bool f)         { setFlag(EnemyWavering, f); }
          void takenHeavyLoss(bool f)        { setFlag(TakenHeavyLoss, f); }
          void retreating(bool f)            { setFlag(Retreating, f); }
          void lowMorale(bool f)             { setFlag(LowMorale, f); }
          void flankedLeft(bool f)           { setFlag(FlankedLeft, f); }
          void flankedRight(bool f)          { setFlag(FlankedRight, f); }
          void flankedRear(bool f)         { setFlag(FlankedRear, f); }
          void wavering(bool f)         { setFlag(Wavering, f); }
          void noEnemyNear(bool f)         { setFlag(NoEnemyNear, f); }
          void inCombat(bool f) { setFlag(Engaged, f); }

          void considerUpdate(bool f)         { setHiFlag(ConsiderUpdate, f); }
          void recalcBoundries(bool f)         { setHiFlag(RecalcBoundries, f); }

          bool unitsAdded() const { return (d_flags & UnitsAdded); }
          bool inflictedCriticalLoss() const { return (d_flags & InflictedCriticalLoss) != 0; }
          bool inflictedLosses() const { return (d_flags & InflictedLoss) != 0; }
          bool enemyRetreating() const { return (d_flags & EnemyRetreating) != 0; }
          bool enemyLowMorale() const  { return (d_flags & EnemyLowMorale) != 0; }
          bool enemyWavering() const { return (d_flags & EnemyWavering) != 0; }
          bool takenHeavyLoss() const { return (d_flags & TakenHeavyLoss) != 0; }
          bool retreating()      const { return (d_flags & Retreating) != 0; }
          bool lowMorale()       const { return (d_flags & LowMorale) != 0; }
          bool flankedLeft() const { return (d_flags & FlankedLeft) != 0; }
          bool flankedRight() const { return (d_flags & FlankedRight) != 0; }
          bool flankedRear() const { return (d_flags & FlankedRear) != 0; }
          bool wavering() const { return (d_flags & Wavering) != 0; }
          bool noEnemyNear() const { return (d_flags & NoEnemyNear) != 0; }
          bool inCombat() const { return (d_flags & Engaged) != 0; }

          bool considerUpdate() const { return (d_hiFlags & ConsiderUpdate) != 0; }
          bool recalcBoundries() const { return (d_hiFlags & RecalcBoundries) != 0; }
		private:
          bool HiZone::shouldReorient(
               RealHiObjective* obj,
               BattleMeasure::HexPosition::Facing& f,
               BattleMeasure::HexPosition::Facing& newFace);
          bool shouldCreateNewBoundries(RealHiObjective* obj);
          bool procConsiderUpdate();
            // flags set by zone
            enum {
               UnitsAdded            = 0x0001,
               InflictedCriticalLoss = 0x0002,
               InflictedLoss         = 0x0004,
               EnemyRetreating       = 0x0008,
               EnemyLowMorale        = 0x0010,
               EnemyWavering         = 0x0020,
               Retreating            = 0x0040,
               LowMorale             = 0x0080,
               TakenHeavyLoss        = 0x0100,
               FlankedLeft           = 0x0200,
               FlankedRight          = 0x0400,
               FlankedRear           = 0x0800,
               Wavering              = 0x1000,
               NoEnemyNear           = 0x2000,
               Engaged               = 0x4000
            };

            // flags set by high level
            enum {
               ConsiderUpdate        = 0x01, // Set by hi-level
               RecalcBoundries       = 0x02
            };

				BoundryList      d_boundries;
				HiUnitPList      d_units;
				HiObjectiveList  d_objectives;
				ZoneOrientation  d_zoneOrientation;

				// Other Info, such as front line area, reserve area, etc...
				int d_priority;
				BattleMeasure::BattleTime::Tick d_startWhen;
				BattleMeasure::BattleTime::Tick d_nextReserveCheck;
				BattleMeasure::BattleTime::Tick d_nextUpdate;
				BAI_AggressPosStruct d_agPos;
            UWORD d_flags;
            UBYTE d_hiFlags;
//            int d_moveLR;
//            int d_moveFB;
            GoWhichWay d_whichWay;
#ifdef DEBUG
				String d_name;
#endif
};


inline bool operator < (const HiZone& l, const HiZone& r)
{
		return (l.leftX() < r.leftX()) ||
						( (l.leftX() == r.leftX()) && (l.rightX() < r.rightX()) );
}

inline bool operator == (const HiZone& l, const HiZone& r)
{
		return (l.leftX() == r.leftX()) && (l.rightX() == r.rightX()) && (l.leftY() == r.leftY()) && (l.rightY() == r.rightY());
}


};  // namespace WG_BattleAI_Internal


#endif /* HI_ZONE_HPP */

