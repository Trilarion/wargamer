/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef HIOPPLAN_HPP
#define HIOPPLAN_HPP
#include "batord.hpp"
#include "batcord.hpp"
using BattleMeasure::HexCord;

namespace WG_BattleAI_Internal
{
class GameInfo;
class BAI_StrObjList;
class Hi_OpPlan {
	public:

		enum Plan {
			// Offensive plans
			Off_SingleLeft, 	 // A single envelopement from the left-wing(angling to the right)
			Off_SingleRight,   // A single envelement from the right-wing(angling to the left)
			Off_Double,        // A double envelopement fron left and right
			Off_Center, 		   // An attack up the center, with covering forces on the wings
			Off_BroadFront,    // A broad front attack. Very unsubtle, Eisenhower-like
			Off_EchelonLeft,   // Left wing moves forward
			Off_EchelonRight,  // Right wing moves forward

		 // Defensive plans
			Def_Centered,    // Evenly spaced defensive.
			Def_Left,
			Def_Right,

			// Others...?
			Plan_HowMany,
			Plan_Undefined,
			Plan_FirstOffensive = Off_SingleLeft,
			Plan_LastOffensive = Off_EchelonRight,
			Plan_FirstDefensive = Def_Centered,
			Plan_LastDefensive = Def_Right,

			Plan_None = Plan_HowMany
		};

		static bool canIssue(
//			BAI_StrObjList& obl,
//			HexCord mapSize,
			GameInfo* gi,
			Plan plan,
			int nZones);
//			BattleOrderInfo::Posture pos);
#ifdef DEBUG
		static const TCHAR* planName(Plan op);
#endif		
#if 0
	public:
	 Hi_OpPlan() {}
	 virtual ~Hi_OpPlan() {}

		virtual bool allocZones(int nUnits) = 0;
		virtual bool allocUnits() = 0;
 
		
	 static Hi_OpPlan* alloc(Off_Plan p);
#endif
};
	
}; // end namespace

#endif
