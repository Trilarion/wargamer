/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 * Battle AI Hi-level Data
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "hi_unit.hpp"
#include "filebase.hpp"
#include "bobutil.hpp"
// #include "hi_zone.hpp"

#include "batdata.hpp"

namespace WG_BattleAI_Internal
{

static UWORD s_version = 0x0000;
bool HiUnitInfo::readData(FileReader& f, OrderBattle* ob)
{
   UWORD version;
   f >> version;
   ASSERT(version <= s_version);

   CPIndex cpi;
   f >> cpi;
   d_cp = BobUtility::cpIndexToCP(cpi, *ob);

   f >> d_priority;

   return f.isOK();
}

bool HiUnitInfo::writeData(FileWriter& f, OrderBattle* ob) const
{
   f << s_version;

   CPIndex cpi = BobUtility::cpToCPIndex(const_cast<BattleCP*>(d_cp), *ob);
   f << cpi;

   f << d_priority;
   return f.isOK();
}


HiUnitInfo::~HiUnitInfo() 
{ 
   ASSERT(d_zone == 0);
   ASSERT(d_objective == 0);
//   d_zone->removeUnit(this);
}

};  // namespace WG_BattleAI_Internal

/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.2  2001/06/18 08:25:43  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
