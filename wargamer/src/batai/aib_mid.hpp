/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef AIB_MID_HPP
#define AIB_MID_HPP

#ifndef __cplusplus
#error aib_mid.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 * Mid Level AI
 *
 *----------------------------------------------------------------------
 */

#include "aib_area.hpp"
#include "aib_info.hpp"
#include "bobdef.hpp"
#include "leader.hpp"
#include "corpdepl.hpp"
#include "unittype.hpp"
#include "ba_agpos.hpp"
#include "batlist.hpp"
#include "m_cp.hpp"
#include "battime.hpp"
#include "batdata.hpp"
#include <set>        // STL set<T>

class HexArea;
class BattleOrder;
class FileReader;
class FileWriter;

/*
 * A couple of things to keep STL happy
 */

template<class T>
struct RefPtrLess : public std::binary_function<T, T, bool>
{
		bool operator()(const T& x, const T& y) const
		{
      return x.value() < y.value();
		}
};

template<class T>
bool operator< (const RefPtr<T>& left, const RefPtr<T>& right)
{
  return left.value() < right.value();
}

namespace WG_BattleAI_Internal
{

class HexAreaList : public std::list<HexArea> {
  public:
    bool readData(FileReader& f);
    bool writeData(FileWriter& f) const;
};

class MidUnitList;

/*
 * What is passed onto mid-level planner
 */

class HiObjective
{
  public:
    enum Mode
    {
      FrontLine,
      Reserves,

      ModeCount,
    };


    typedef std::set<CRefBattleCP, std::less<CRefBattleCP> > UnitList;

    virtual const HexArea& area() const = 0;
    virtual const UnitList& units() const = 0;
    virtual void mode(Mode m) = 0;
    virtual Mode mode() const = 0;
    virtual BattleOrderInfo::Aggression aggression() const = 0;
    virtual BattleOrderInfo::Posture posture() const = 0;
    virtual int nBoundries() const = 0;
    virtual void removeBoundry() = 0;

    virtual void getNewObjective(bool f) = 0;
    virtual void takingHeavyLosses(bool f) = 0;
    virtual void inflictingHeavyLosses(bool f) = 0;
    virtual void retreating(bool f) = 0;
    virtual void enemyRetreating(bool f) = 0;
    virtual void routingFleeing(bool f) = 0;
    virtual void movingToFront(bool f) = 0;
    virtual void flankedLeft(bool f) = 0;
    virtual void flankedRight(bool f) = 0;
    virtual void flankedRear(bool f) = 0;
    virtual void reorienting(bool f) = 0;//          { setFlag(Reorienting, f); }

    virtual bool getNewObjective()       const = 0;
    virtual bool takingHeavyLosses()     const = 0;
    virtual bool inflictingHeavyLosses() const = 0;
    virtual bool retreating()            const = 0;
    virtual bool enemyRetreating()       const = 0;
    virtual bool routingFleeing()        const = 0;
    virtual bool movingToFront()         const = 0;
    virtual bool flankedLeft()           const = 0;
    virtual bool flankedRight()          const = 0;
    virtual bool flankedRear()           const = 0;
    virtual bool reorienting() const = 0;//          { setFlag(Reorienting, f); }

    virtual HexAreaList& areaList() = 0;
#ifdef DEBUG
    const char* modeString() const;
#endif

  protected:
    virtual ~HiObjective() { }      // can't delete from this level
};

class MidLevelPlanner : public GameInfoUser, public BattleMessageProcessor
{
    friend class MidLevel_Op;
    friend class BAI_ScriptReader;
    typedef HiObjective::UnitList UnitList;
  public:
    MidLevelPlanner(GameInfo* gameInfo, HiObjective* ob);
    virtual ~MidLevelPlanner();

    virtual void process();
    virtual void procMessage(const BattleMessageInfo& msg);

    bool objectiveAchieved() const { return (d_flags & ObjectiveAchieved) != 0; }
    bool enemyToFront() const { return (d_flags & EnemyToFront) != 0; }
    bool moveUpRear() const { return (d_flags & MoveUpRear) != 0; }
    bool considerRetreat() const { return (d_flags & ConsiderRetreat) != 0; }

    void objectiveAchieved(bool f) { setFlag(ObjectiveAchieved, f); }
    void enemyToFront(bool f) { setFlag(EnemyToFront, f); }
    void moveUpRear(bool f) { setFlag(MoveUpRear, f); }
    void considerRetreat(bool f) { setFlag(ConsiderRetreat, f); }
    void sendCPAggPos(bool playing = False);
    void bombardFor(BattleMeasure::BattleTime::Tick time)
    {
      d_bombardTime = time;
    }

    bool bombarding()
    {
      return (gameInfo()->battleData()->getTick() < d_bombardTime);
    }
    int frontWidth() const;

    bool finishedBombard()
    {
      return (!bombarding() && d_bombardTime != 0);
    }


    UWORD flags() const { return d_flags; }
    BattleList& targets() { return d_targets; }
    const BattleList& targets() const { return d_targets; }
    void newDay()
    {
      d_nextReinforcementCheck = 0;
      d_bombardTime = 0;
    }

    HexCord leftFront() const;
    HexCord rightFront() const;

    const MU_DeployItem& cincDI() const { return d_cincDI; }
    // file interface
    bool readData(FileReader& f);
    bool writeData(FileWriter& f) const;
#ifdef DEBUG
    void logHistorical();
#endif
  private:

    void setFlag(UWORD mask, bool f)
    {
      if(f)
        d_flags |= mask;
      else
        d_flags &= ~mask;
    }

    void procDeploy();
    void procGameInProgress();
//					void objTypes(UBYTE& nInfXXX, UBYTE& nCavXXX, UBYTE& nArtXXX, int& totalSP);
    void assignDivisionalPositions(MidUnitInfo* mi,
                                   CorpDeployment& cd,
                                   CorpDeployment::Where where,
                                   const HexCord& leftHex,
                                   const HexCord& rightHex,
                                   BattleMeasure::HexPosition::Facing facing,
                                   BOB_Definitions::CPFormation divFor,
                                   BOB_Definitions::SPFormation spFor,
                                   BasicUnitType::value bt);

    void MidLevelPlanner::makeOrder(
                                    MU_DeployItem& di,
                                        BOB_Definitions::CPFormation divFor,
                                        BOB_Definitions::SPFormation spFor,
                                        BOB_Definitions::CPFormation destDivFor,
                                        BOB_Definitions::SPFormation destSPFor,
                                        BattleMeasure::HexPosition::Facing facing,
                                        const HexCord* destHex,
                                        bool delaySend);

    void issueOrder(const CRefBattleCP& cp, BattleOrder& batOrder);
    void onObjectiveAchieved();

    bool issueOrders();
    bool doNewObjective();
    bool checkForObjectiveAchieved();
    bool shouldMove(const MU_DeployItem& di, const HexArea& area);
    bool settingAround();
    bool evaluateObj();

    void clearAllMoves();
    void addToDeployMapOffMap(
                              MidUnitInfo* mi,
                                  CorpDeployment& cd,
                                  CorpDeployment::Where where,
                                  BasicUnitType::value bt);
    void addToDeployMapOnMap(
                             MidUnitInfo* mi); //,
    void initDeployMap();
    void addUnits();
    void checkForReinforcements();
#ifdef DEBUG
    void logObjective();
#endif

  private:
    HiObjective* d_objective;
    MidUnitList* d_units;
    MU_DeployItem d_cincDI;
    BattleList d_targets;
    enum Flags {
      ObjectiveAchieved = 0x0001,
      EnemyToFront      = 0x0002,
      MoveUpRear        = 0x0004,
      ConsiderRetreat   = 0x0008,
      Reacting          = 0x0010,
      NewEnemySighted   = 0x0020,
      FinishedBombard   = 0x0040,
      Retreating        = 0x0080,
      CatchingUp        = 0x0100,
      Deploying         = 0x0200,
      NoCheckAlignment  = 0x0400
    };

    UWORD d_flags;
    UWORD d_oldFlags;
    BattleMeasure::BattleTime::Tick d_nextReinforcementCheck;
    BattleMeasure::BattleTime::Tick d_bombardTime;
};

}  // namespace WG_BattleAI_Internal



#endif /* AIB_MID_HPP */

/*----------------------------------------------------------------------------
 * $Log$
 *----------------------------------------------------------------------------
 */
