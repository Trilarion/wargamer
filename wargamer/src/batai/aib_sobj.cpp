/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
#include "stdinc.hpp"
#include "aib_sobj.hpp"

#include "fsalloc.hpp"

namespace WG_BattleAI_Internal
{
#if 1

/*-------------------------------------------------------------------
 * HexList class, used for storing routes, deployment hexes, etc.
 */

const int chunkSize = 10;

#ifdef DEBUG
static FixedSize_Allocator str_itemAlloc(sizeof(BAI_StrObj), chunkSize, "BAI_StrObj");
#else
static FixedSize_Allocator str_itemAlloc(sizeof(BAI_StrObj), chunkSize);
#endif

void* BAI_StrObj::operator new(size_t size)
{
   ASSERT(size == sizeof(BAI_StrObj));
   return str_itemAlloc.alloc(size);
}

#ifdef _MSC_VER
void BAI_StrObj::operator delete(void* deadObject)
{
   str_itemAlloc.free(deadObject, sizeof(BAI_StrObj));
}
#else
void BAI_StrObj::operator delete(void* deadObject, size_t size)
{
   ASSERT(size == sizeof(BAI_StrObj));
   str_itemAlloc.free(deadObject, size);
}
#endif

static UWORD s_version = 0x0000;
bool BAI_StrObj::readData(FileReader& f)
{
   UWORD version;
   f >> version;
   ASSERT(version <= s_version);

   d_hex.readData(f);
   UBYTE b;
   f >> b;
   d_posture = static_cast<BattleOrderInfo::Posture>(b);
   return f.isOK();
}

bool BAI_StrObj::writeData(FileWriter& f) const
{
   f << s_version;
   d_hex.writeData(f);

   UBYTE b = static_cast<UBYTE>(d_posture);
   f << b;

   return f.isOK();
}



static UWORD s_listVersion = 0x0000;
bool BAI_StrObjList::readData(FileReader& f)
{
   UWORD version;
   f >> version;
   ASSERT(version <= s_listVersion);

   int items;
   f >> items;
   while(items--)
   {
     BAI_StrObj* obj = new BAI_StrObj;
     ASSERT(obj);

     obj->readData(f);
     append(obj);
   }

   return f.isOK();
}

bool BAI_StrObjList::writeData(FileWriter& f) const
{
   f << s_listVersion;

   int items = entries();
   f << items;

   SListIterR<BAI_StrObj> iter(this);
   while(++iter)
   {
      iter.current()->writeData(f);
   }

   return f.isOK();
}

#endif
}; // end namespace WG_BattleAI_Internal


/*
 *----------------------------------------------------------------------
 * $Log$
 *----------------------------------------------------------------------
 */
