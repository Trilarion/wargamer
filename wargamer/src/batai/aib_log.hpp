/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef AIB_LOG_HPP
#define AIB_LOG_HPP

#ifndef __cplusplus
#error aib_log.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 * Logfile for Battle AI
 *
 *----------------------------------------------------------------------
 */

#ifdef DEBUG
#include "clog.hpp"

namespace WG_BattleAI_Internal
{
extern LogFileFlush bataiLog;

};   // namespace WG_BattleAI_Internal

#endif

#endif /* AIB_LOG_HPP */

