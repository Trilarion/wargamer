/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 * High Level Planner
 *
 *----------------------------------------------------------------------
 */


#include "stdinc.hpp"
#include "aib_hi.hpp"
#include "aib_data.hpp"
#include "hi_asign.hpp"
#include "h_zlist.hpp"
#include "h_cplist.hpp"
#include "pdeploy.hpp"
#include "aib_util.hpp"
#include "hexdata.hpp"
#include "batmsg.hpp"
#include "bobiter.hpp"
#include "filebase.hpp"
#include "baitable.hpp"
#include "b_send.hpp"
//#include "aisreadr.hpp"

#ifdef _MSC_VER
using namespace std::rel_ops;
#endif

namespace WG_BattleAI_Internal
{

/*===============================================================
 * High Level Planner Implementation
 */


HighLevelPlanner::HighLevelPlanner(GameInfo* gameInfo) :
    GameInfoUser(gameInfo),
    d_zones(0),
    d_units(0),
    d_assigner(0),
    d_nextReinforcementCheck(0),
    d_nextZoneCheck(0),
    d_nextPlanCheck(0),
    d_nextProcess(0),
    d_firstUpdate(False)
{
      d_zones = new HiZoneList(gameInfo);
      d_units = new HiUnitList(gameInfo);
      d_assigner = new HiCorpsAssigner(gameInfo, d_zones, d_units);
}

HighLevelPlanner::~HighLevelPlanner()
{
   delete d_assigner;
   delete d_zones;      // Note: zones must be deleted before units
   delete d_units;
}

// test to see if we need to change posture and update plan
bool HighLevelPlanner::needPlan()
{
#if 0
   if(!gameInfo()->isDeploying() &&
      gameInfo()->battleData()->getTick() >= d_nextZoneCheck &&
      gameInfo()->battleData()->getTick() >= d_nextPlanCheck)
   {
      CRefBattleCP cinc = gameInfo()->cinc();
      if(cinc)
      {
#ifdef DEBUG
         gameInfo()->log("Testing %s for changing posture", cinc->getName());
#endif
         int chance = 0;
         BattleOrderInfo::Aggression ag = cinc->aggression();
         BattleOrderInfo::Posture pos = cinc->posture();
         WG_BattleAI_Utils::StrRatio sr = WG_BattleAI_Utils::getSideStrRatio(gameInfo()->battleData(), cinc);

         // test for changing to offensive
         if(pos == BattleOrderInfo::Defensive)
         {
            int points = 0;

            // evaluate status of zones
            for(HiZoneList::iterator zoneIt = d_zones->begin();
                zoneIt != d_zones->end();
                ++zoneIt)
            {
               HiZone* z = (*zoneIt);
               if(z->inflictedCriticalLoss())
                  points += 10;
               else if(z->inflictedLosses())
                  points += 5;

               if(z->enemyRetreating())
                  points += 10;
               if(z->enemyLowMorale())
                  points += 10;
               if(z->enemyWavering())
                  points += 5;
               if(z->retreating())
                  points -= 10;
               if(z->lowMorale())
                  points -= 10;
               if(z->takenHeavyLoss())
                  points -= 10;
               if(z->wavering())
                  points -= 5;
            }

            const int s_table[WG_BattleAI_Utils::Str_HowMany] = {
               100, 80, 60, 40, 0, -10, -20, -40, -80
            };
            points += s_table[sr];
            chance = points / 20;
         }

         else
         {
            int points = 0;

            // evaluate status of zones
            for(HiZoneList::iterator zoneIt = d_zones->begin();
                zoneIt != d_zones->end();
                ++zoneIt)
            {
               HiZone* z = (*zoneIt);
               if(z->inflictedCriticalLoss())
                  points -= 10;
               if(z->enemyRetreating())
                  points -= 10;
               if(z->enemyLowMorale())
                  points -= 10;
               if(z->enemyWavering())
                  points -= 5;
               if(z->retreating())
                  points += 10;
               if(z->lowMorale())
                  points += 10;
               if(z->takenHeavyLoss())
                  points += 10;
               if(z->wavering())
                  points += 5;
            }

            const int s_table[WG_BattleAI_Utils::Str_HowMany] = {
               -80, -40, -20, -10, 0, 10, 20, 40, 80
            };

            chance = points / 20;
         }

#ifdef DEBUG
         gameInfo()->log("Chance for change = %d", chance);
#endif
         if(gameInfo()->random(100) < chance)
         {
            if(pos == BattleOrderInfo::Defensive)
            {
               pos = BattleOrderInfo::Offensive;
            }
            else
            {
               pos = BattleOrderInfo::Defensive;
            }
#ifdef DEBUG
            gameInfo()->log("Changing posture to %s", BattleOrderInfo::postureName(pos));
#endif
            const_cast<BattleCP*>(cinc)->aggression(ag);
            const_cast<BattleCP*>(cinc)->getCurrentOrder().aggression(ag);
            const_cast<BattleCP*>(cinc)->posture(pos);
            const_cast<BattleCP*>(cinc)->getCurrentOrder().posture(pos);
            gameInfo()->plan(Hi_OpPlan::Plan_None);
            d_nextPlanCheck = gameInfo()->battleData()->getTick() + BattleMeasure::minutes(60);
         }
      }
   }
#endif
   return (gameInfo()->plan() == Hi_OpPlan::Plan_None);
}


// determine if a zone needs to be updated, i.e.
// If it needs new objective boundries if its setting around doing nothing
bool HighLevelPlanner::checkZones()
{
   // First counts zones that are engaged with enemy
   int zonesEngaged = 0;
   for(HiZoneList::iterator zoneIt = d_zones->begin();
         zoneIt != d_zones->end();
         ++zoneIt)
   {
      (*zoneIt)->setZoneFlags();
      if((*zoneIt)->inCombat())
         zonesEngaged++;
   }

#ifdef DEBUG
   gameInfo()->log("Check Zones --> nEngaged = %d, nZones = %d",
      zonesEngaged, d_zones->size());

#endif
   int nZone = 0;
   // Now test each zone
   for(zoneIt = d_zones->begin();
       zoneIt != d_zones->end();
       ++zoneIt, nZone++)
   {
      HiZone* z = (*zoneIt);
      if(z->considerUpdate() || z->inCombat())
         continue;

      const Table1D<SWORD>& mTable = BattleAITables::updateZoneModifiers();
      enum {
         HalfOrMoreZonesEngaged,
         OneOtherZoneEngaged,
         NoZoneEngaged,
         HalfOrMoreZonesEngagedNear,
         OneOtherZoneEngagedNear,
         NoZoneEngagedNear,
         DefensivePosture,
         EnemyWavering,
         EnemyRetreating,
         EnemyCriticalLoss,
         EnemyLoss,
         EnemyLowMorale
      };

      int chance = 0;
      bool noEnemyNear = z->noEnemyNear();
      if(noEnemyNear)
      {
         chance += (zonesEngaged > 1 || (d_zones->size() == 2 && zonesEngaged > 0)) ? mTable.getValue(HalfOrMoreZonesEngaged) :
                   (zonesEngaged > 0) ? mTable.getValue(OneOtherZoneEngaged) : mTable.getValue(NoZoneEngaged);
      }
      else
      {
         chance += (zonesEngaged > 1 || (d_zones->size() == 2 && zonesEngaged > 0)) ? mTable.getValue(HalfOrMoreZonesEngagedNear) :
                   (zonesEngaged > 0) ? mTable.getValue(OneOtherZoneEngagedNear) : mTable.getValue(NoZoneEngagedNear);
      }

      //if(z->posture() == BattleOrderInfo::Defensive ||
      //   z->lastObjReached())
      {
         //chance += (z->aggression() >= 3) ? mTable.getValue(DefensivePosture) : 0;

         if(z->enemyWavering())
            chance += mTable.getValue(EnemyWavering);
         if(z->enemyRetreating())
            chance += mTable.getValue(EnemyRetreating);
         if(z->inflictedCriticalLoss())
            chance += mTable.getValue(EnemyCriticalLoss);
         else if(z->inflictedLosses())
            chance += mTable.getValue(EnemyLoss);
         if(z->enemyLowMorale())
            chance += mTable.getValue(EnemyLowMorale);
      }
#ifdef DEBUG
      gameInfo()->log("Chance for zone%d to update objectives is %d",   nZone, chance);
#endif
      if(gameInfo()->random(100) < chance)
      {
#ifdef DEBUG
         gameInfo()->log("Will update!");
#endif
         z->considerUpdate(True);

         HiZone::GoWhichWay whichWay = HiZone::Front;
         if(noEnemyNear && nZone == 0)
            whichWay = HiZone::Right;
         else if(noEnemyNear && nZone == d_zones->size() - 1)
            whichWay = HiZone::Left;

         z->whichWay(whichWay);
#if 0
         int fb = 10;
         int lr = (whichWay == Left) ? -2 :
                  (whichWay == Right) ? 2 : 0;
         z->moveLR(lr);
         z->moveFB(fb);
#endif
         return True;
      }
   }

   return False;
}

void HighLevelPlanner::process()
{
   bool newUnits = False;
   if(gameInfo()->isDeploying() ||
      gameInfo()->battleData()->getTick() >= d_nextReinforcementCheck)
   {
      newUnits = d_units->updateUnits(); // Check for reinforcements

      // check again in 2 minutes
      d_nextReinforcementCheck = gameInfo()->battleData()->getTick() + BattleMeasure::seconds(30);
   }

   if(needPlan())
   {
      int nZones = 0;
      WG_BattleAI_Utils::selectPlan(gameInfo(), d_units->corpsCount(), nZones);
      ASSERT(gameInfo()->plan() != Hi_OpPlan::Plan_Undefined);
      Hi_OpPlan::Plan plan = gameInfo()->plan();
      d_zones->updateZones(d_units->corpsCount(), nZones, plan);
   }

   if(newUnits)
      d_assigner->assignUnits();        // Assign units to zones

   //if(!gameInfo()->isDeploying())
   {
      if(!gameInfo()->isDeploying() &&
         gameInfo()->battleData()->getTick() >= d_nextZoneCheck)
      {
         checkZones();
         d_nextZoneCheck = (gameInfo()->battleData()->getTick() + BattleMeasure::minutes(5));
      }

      if(gameInfo()->isDeploying() ||
         gameInfo()->battleData()->getTick() >= d_nextProcess)
      {
         /*
          * Assign / Update Objectives for units in zones
          */

         for(HiZoneList::iterator zoneIt = d_zones->begin();
             zoneIt != d_zones->end();
             ++zoneIt)
         {
            // running the game
            //  if(!gameInfo()->isDeploying())
            {
               (*zoneIt)->updateObjectives();
               (*zoneIt)->executeObjectives();
            }
         }

         if(d_firstUpdate)
            updateArmyHQs();

         if(!gameInfo()->isDeploying())
         {
            d_nextProcess = gameInfo()->battleData()->getTick() + BattleMeasure::seconds(90);
            d_firstUpdate = True;
         }
      }
   }
}

void HighLevelPlanner::newDay()
{
   d_nextReinforcementCheck = 0;
   d_nextZoneCheck = 0;
   d_nextPlanCheck = 0;
   d_nextProcess = 0;
   for(HiZoneList::iterator zoneIt = d_zones->begin();
         zoneIt != d_zones->end();
         ++zoneIt)
   {
      (*zoneIt)->newDay();
   }
}

void HighLevelPlanner::plan(Hi_OpPlan::Plan p)
{
   gameInfo()->plan(p);
}

void HighLevelPlanner::updateArmyHQs()
{
   if(gameInfo()->isDeploying())
   {
      // go through each Army level and higher
      ConstBattleUnitIter iter = gameInfo()->ourUnits();
//    const_cast<BattleUnitIter*>(reinterpret_cast<BattleUnitIter*>(&iter))->setBottomUp();
      BattleData* bdnc = const_cast<BattleData*>(reinterpret_cast<const BattleData*>(gameInfo()->battleData()));
      HexCord bLeft;
      HexCord mSize;
      bdnc->getPlayingArea(&bLeft, &mSize);
      HexCord tRight(bLeft.x() + mSize.x(), bLeft.y() + mSize.y());

      int nZones = d_zones->size();
      ASSERT(nZones > 0);
      while(!iter.isFinished())
      {
         if(iter.cp()->getRank().isHigher(Rank_Corps))
         {
                int bestZone = -1;
                int bestPriority = -1;
                for(BattleUnitIter cpIter(const_cast<BattleCP*>(iter.cp())); !cpIter.isFinished(); cpIter.next())
                {
                  if( (cpIter.cp()->getRank().sameRank(Rank_Corps)) ||
                      (cpIter.cp()->getRank().sameRank(Rank_Division) && (cpIter.cp()->parent() == NoBattleCP || cpIter.cp()->parent()->getRank().isHigher(Rank_Corps))) )
                  {
                     for(int z = 0; z < nZones; z++)
                    {
                        if((*d_zones)[z]->getUnit(cpIter.cp()))
                        {
                           if(bestZone == -1 || (*d_zones)[z]->priority() > bestPriority)
                            {
                              bestZone = z;
                                bestPriority = (*d_zones)[z]->priority();
                            }
                        }
                    }
                  }
                }

                ASSERT(bestZone != -1);
                ASSERT(bestZone < nZones);
                //const HexCord& size = gameInfo()->map()->getSize();

                const HexCord::Cord ourEdge = gameInfo()->whichEdge();
                const HexCord::Cord otherEdge = gameInfo()->otherEdge();
                int y = (ourEdge > otherEdge) ? tRight.y() - 5 : bLeft.y() + 4;
                HexCord hex(((*d_zones)[bestZone]->leftHex().x() + (*d_zones)[bestZone]->rightHex().x()) / 2, y);
#ifdef DEBUG
                gameInfo()->log("Deploying Army HQ(%s) at hex(%d, %d)",
                     iter.cp()->getName(), static_cast<int>(hex.x()), static_cast<int>(hex.y()));
#endif

                const HexPosition::Facing facing = WG_BattleAI_Utils::frontFacing((*d_zones)[bestZone]->leftHex(), (*d_zones)[bestZone]->rightHex());
                if(!WG_BattleAI_Utils::findDeployPosition(gameInfo()->battleData(), const_cast<BattleCP*>(iter.cp()), facing, hex, gameInfo()->isDeploying()))
                     FORCEASSERT("Could deploy unit");

         }

         iter.next();
      }
   }
#if 0
   // TODO: move army HQs
   else
   {
      for(BattleUnitIter iter(const_cast<BattleData*>(gameInfo()->battleData())->ob(), gameInfo()->side());
          !iter.isFinished();
          iter.next())
      {
         if(iter.cp()->getRank().isHigher(Rank_Corps))
            moveArmyHQ(iter.cp());
      }
   }
#endif
}

bool HighLevelPlanner::moveArmyHQ(const RefBattleCP& cp)
{
   // Find a subordinate HQ to hang around
#ifdef DEBUG
   gameInfo()->log("Testing %s for move ----- ", cp->getName());
#endif
   HiUnitInfo* bestCorp = 0;
   int bestPriority = -1;
   for(BattleUnitIter cpIter(const_cast<BattleCP*>(cp)); !cpIter.isFinished(); cpIter.next())
   {
      // check only XXX's or independent XX's
      if( (cpIter.cp()->getRank().sameRank(Rank_Corps)) ||
          (cpIter.cp()->getRank().sameRank(Rank_Division) && (cpIter.cp()->parent() == NoBattleCP || cpIter.cp()->parent()->getRank().isHigher(Rank_Corps))) )
      {
         for(int z = 0; z < d_zones->size(); z++)
         {
            HiUnitInfo* hi = (*d_zones)[z]->getUnit(cpIter.cp());
            if(hi)
            {
               // find best corp
               if(bestCorp == 0)
               {
                  bestCorp = hi;
               }
               else
               {
                  if( (bestCorp->objective()->mode() == HiObjective::Reserves && hi->objective()->mode() == HiObjective::FrontLine) ||
                      ((*d_zones)[z]->priority() > bestPriority) )
                  {
                     bestCorp = hi;
                  }
               }

               if(bestCorp == hi)
                  bestPriority = (*d_zones)[z]->priority();

               break;
            }
         }
      }
   }

   if(bestCorp)
   {
      if(bestCorp->objective()->planner()->cincDI().objHex() == HexCord(0, 0))
         return False;

      if(cp->moving() ||
         cp->getCurrentOrder().wayPoints().entries() > 0)
      {
         if(gameInfo()->random(100) > 2)
            return False;
      }

      const HexArea& area = bestCorp->objective()->area();
      HexPosition::Facing corpFace = WG_BattleAI_Utils::frontFacing(area[0], area[1]);

      //HexCord srcHex = (bestCorp->cp()->getCurrentOrder().wayPoints().entries() > 0) ?
         //bestCorp->cp()->getCurrentOrder().wayPoints().getLast()->d_hex : bestCorp->cp()->leftHex();

      HexCord srcHex = bestCorp->objective()->planner()->cincDI().objHex();
      HexCord destHex;
      if(WG_BattleAI_Utils::moveHex(gameInfo()->battleData(), corpFace, gameInfo()->random(-4, 4), -gameInfo()->random(1, 5), srcHex, destHex) &&
         destHex != cp->hex())
      {

#ifdef DEBUG
         gameInfo()->log("%s is moving to %s ----> hex(%d,%d)",
            cp->getName(),
            bestCorp->cp()->getName(),
            static_cast<int>(destHex.x()),
            static_cast<int>(destHex.y()));
#endif
         BattleOrder order;
         cp->lastOrder(&order);
         order.wayPoints().reset();
         WG_BattleAI_Utils::setMoveOrder(&order, destHex, corpFace);
         // TODO: determine if we should move on road
         cp->noRoadMove(True);
         sendBattleOrder(cp, &order);
         return True;
      }
   }

   return False;
}

HiZone* HighLevelPlanner::findCloseZone(HexCord hex)
{
   if(hex.x() == 0 && hex.y() == 0)
      return 0;

   int bestDist = -1;
   HiZone* bestZone = 0;
   for(HiUnitList::iterator unitIt = d_units->begin(); unitIt != d_units->end(); ++unitIt)
   {
      HiUnitInfo& unitInfo = *unitIt;
      if(unitInfo.inZone())
      {
         for(ConstBattleUnitIter iter(unitInfo.cp()); !iter.isFinished(); iter.next())
         {
            if(iter.cp()->getRank().sameRank(Rank_Division))
            {
               int dist = WG_BattleAI_Utils::distanceFromUnit(hex, const_cast<BattleCP*>(iter.cp()));
               if(bestDist == -1 || dist < bestDist)
               {
                  bestDist = dist;
                  bestZone = unitInfo.zone();
               }
            }
         }
      }
   }

   return bestZone;
}

void HighLevelPlanner::procMessage(const BattleMessageInfo& msg)
{
   // if this is from a XX, find where he is and pass it along to MidlevelPlanner
   if(msg.cp() != NoBattleCP)
   {
      BattleMessageID::BMSG_ID id = msg.id().id();
      if(id == BattleMessageID::BMSG_EnemyTookVP)
      {
         HiZone* z = findCloseZone(msg.hex());
         if(z)
            z->procMessage(msg);
      }
      else
      {
         for(HiUnitList::iterator unitIt = d_units->begin(); unitIt != d_units->end(); ++unitIt)
         {
            HiUnitInfo& unitInfo = *unitIt;
            if(unitInfo.inZone())
            {
               for(ConstBattleUnitIter iter(unitInfo.cp()); !iter.isFinished(); iter.next())
               {
                  if(iter.cp() == msg.cp())
                  {
                     if(unitInfo.objective())
                        unitInfo.objective()->procMessage(msg);

                     return;
                  }
               }
            }
         }
      }
   }
}

void HighLevelPlanner::updatePointers()
{
   // if a saved game we need to set pointers in HiUnitInfo (HiZone and RealHiObjective)
   // and add units to HiZones pointer list
   for(HiUnitList::iterator unitIt = d_units->begin();
       unitIt != d_units->end();
       ++unitIt)
   {
      HiUnitInfo& unitInfo = *unitIt;
      ASSERT(!unitInfo.inZone());

      // find unit's zone
      int nZones = d_zones->size();
      for(int z = 0; z < nZones; z++)
      {
         HiZone* zone = (*d_zones)[z];
         HiObjectiveList& objs = zone->objectives();
         for(HiObjectiveList::iterator iter = objs.begin();
             iter != objs.end();
             ++iter)
         {
            HiObjective::UnitList& units = (*iter)->units();
            for(HiObjective::UnitList::iterator uit = units.begin();
                uit != units.end();
                ++uit)
            {
               if(unitInfo.cp() == (*uit))
               {
                  unitInfo.zone(zone);
                  unitInfo.objective((*iter));
                  zone->addUnit(&unitInfo);
               }
            }
         }
      }

      ASSERT(unitInfo.inZone());
      ASSERT(unitInfo.objective());
   }

}

static UWORD s_version = 0x0005;
bool HighLevelPlanner::readData(FileReader& f)
{
   UWORD version;
   f >> version;
   ASSERT(version <= s_version);
   OrderBattle* ob = const_cast<OrderBattle*>(gameInfo()->battleData()->ob()->ob());
   d_units->readData(f, ob);
   d_zones->readData(f);
   if(version >= 0x0001)
      f >> d_nextReinforcementCheck;

   if(version >= 0x0002)
      f >> d_nextZoneCheck;

   if(version >= 0x0003)
      f >> d_nextPlanCheck;

   if(version >= 0x0004)
      f >> d_nextProcess;

   if(version >= 0x0005)
      f >> d_firstUpdate;

   if(!f.isOK())
      return False;

   updatePointers();
   return True;
}

bool HighLevelPlanner::writeData(FileWriter& f) const
{
   f << s_version;
   OrderBattle* ob = const_cast<OrderBattle*>(gameInfo()->battleData()->ob()->ob());
   d_units->writeData(f, ob);
   d_zones->writeData(f);
   f << d_nextReinforcementCheck;
   f << d_nextZoneCheck;
   f << d_nextPlanCheck;
   f << d_nextProcess;
   f << d_firstUpdate;
   return f.isOK();
}

#ifdef DEBUG
void HighLevelPlanner::logHistorical()
{
   for(HiUnitList::iterator unitIt = d_units->begin();
       unitIt != d_units->end();
       ++unitIt)
   {
      HiUnitInfo& unitInfo = *unitIt;
      gameInfo()->log("----- %s is added to AI", unitInfo.cp()->getName());
   }

   for(HiZoneList::iterator zoneIt = d_zones->begin();
         zoneIt != d_zones->end();
         ++zoneIt)
   {
      // running the game
      //  if(!gameInfo()->isDeploying())
      {
         (*zoneIt)->logHistorical();
      }
   }
}
#endif
};  // namespace WG_BattleAI_Internal



/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.2  2001/06/18 08:25:43  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
