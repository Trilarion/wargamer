/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1995, Steven Morle-Green, All Rights Reserved
 *----------------------------------------------------------------------
 *
 * Mid Level Unit Info
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "m_cp.hpp"
#include "bob_cp.hpp"
#include "batord.hpp"
#include "bobutil.hpp"
#include "batdata.hpp"
// #include <algo.h>

namespace WG_BattleAI_Internal
{

MU_DeployItem::MU_DeployItem(const CRefBattleCP& cp) :
         d_cp(cp),
         d_order(),
         d_sendWhen(0),
         d_flags(0)
{
   d_cp->lastOrder(&d_order);
}

MU_DeployItem::~MU_DeployItem()
{
}

MU_DeployItem::MU_DeployItem(const MU_DeployItem& di) :
         d_cp(di.d_cp),
         d_order(di.d_order),
         d_sendWhen(di.d_sendWhen),
         d_flags(di.d_flags),
         d_objHex(di.d_objHex)
{
}


MU_DeployItem& MU_DeployItem::operator = (const MU_DeployItem  & di)
{
   d_cp = di.d_cp;
   d_order = di.d_order;
   d_sendWhen = di.d_sendWhen;
   d_flags = di.d_flags;
   d_objHex = di.d_objHex;

   return *this;
}

void MU_DeployItem::updateOrder()
{
  d_cp->lastOrder(&d_order);
  d_order.wayPoints().reset();
}

static UWORD s_diVersion = 0x0002;
bool MU_DeployItem::readData(FileReader& f, OrderBattle* ob)
{
   UWORD version;
   f >> version;
   ASSERT(version <= s_diVersion);

   CPIndex cpi;
   f >> cpi;
   d_cp = BobUtility::cpIndexToCP(cpi, *ob);
   d_order.readData(f, ob);
   f >> d_sendWhen;
   // Unchecked
   if(version < 0x0002)
   {
      UBYTE b;
      f >> b;
      d_flags = b;
   }
   else
      f >> d_flags;
   // End

   if(version >= 0x0001)
      d_objHex.readData(f);

   return f.isOK();
}

bool MU_DeployItem::writeData(FileWriter& f, OrderBattle* ob) const
{
   f << s_diVersion;
   CPIndex cpi = BobUtility::cpToCPIndex(const_cast<BattleCP*>(d_cp), *ob);
   f << cpi;
   d_order.writeData(f, ob);
   f << d_sendWhen;
   f << d_flags;
   d_objHex.writeData(f);
   return f.isOK();
}

MU_DeployItem* MidUnitDeployMap::add(const CRefBattleCP& cp, int r)
{
   ASSERT(r < 2);

   // Increment respective row
   if(r == 0)
   {
//    MU_DeployItem& di = d_container[d_nFront];
      d_container.insert(d_container.begin() + d_nFront, MU_DeployItem(cp));
     d_nFront++;
   }
   else
   {
      d_container.push_back(MU_DeployItem(cp));
      d_nRear++;
   }

   return &item((r == 0) ? d_nFront - 1 : d_nRear - 1, r);
}

MU_DeployItem* MidUnitDeployMap::find(const CRefBattleCP& cp)
{
      for(Container::iterator it = d_container.begin();
          it != d_container.end();
          ++it)
      {
         if(cp == (*it).cp())
            return &(*it);
      }

      return 0;
}

bool MidUnitDeployMap::inMap(const CRefBattleCP& cp)
{
    return (find(cp) != 0);
}

static UWORD s_dmVersion = 0x0000;
bool MidUnitDeployMap::readData(FileReader& f, OrderBattle* ob)
{
   UWORD version;
   f >> version;
   ASSERT(version <= s_dmVersion);

   int items;
   f >> items;

   while(items--)
   {
      d_container.push_back(MU_DeployItem());
      MU_DeployItem& di = d_container.back();
      di.readData(f, ob);
   }

   f >> d_nFront;
   f >> d_nRear;
   return f.isOK();
}

bool MidUnitDeployMap::writeData(FileWriter& f, OrderBattle* ob) const
{
   f << s_dmVersion;

   int items = d_container.size();
   f << items;

   for(Container::const_iterator it = d_container.begin();
       it != d_container.end();
       ++it)
   {
     (*it).writeData(f, ob);
   }

   f << d_nFront;
   f << d_nRear;
   return f.isOK();
}


static UWORD s_infoVersion = 0x0000;
bool MidUnitInfo::readData(FileReader& f, OrderBattle* ob)
{
   UWORD version;
   f >> version;
   ASSERT(version <= s_infoVersion);

   CPIndex cpi;
   f >> cpi;
   d_cp = BobUtility::cpIndexToCP(cpi, *ob);

   bool b;
   f >> b;
   d_ordered = b;

   d_map.readData(f, ob);
   return f.isOK();
}

bool MidUnitInfo::writeData(FileWriter& f, OrderBattle* ob) const
{
   f << s_infoVersion;

   CPIndex cpi = BobUtility::cpToCPIndex(const_cast<BattleCP*>(d_cp), *ob);
   f << cpi;

   UBYTE b = static_cast<UBYTE>(d_ordered);
   f << b;

   d_map.writeData(f, ob);
   return f.isOK();
}

MidUnitList::MidUnitList()
{
}

void MidUnitList::addUnit(CRefBattleCP cp)
{
    ASSERT(!inList(cp));
    push_back(MidUnitInfo(cp));
}

#ifdef _MSC_VER
    struct isCP
    {
       isCP(CRefBattleCP cp) : d_cp(cp) { }

       bool operator()(const MidUnitInfo& info) { return info.cp() == d_cp; }

       CRefBattleCP d_cp;
    };
#endif

MidUnitInfo* MidUnitList::find(CRefBattleCP cp)
{
#ifndef _MSC_VER
    struct isCP
    {
       isCP(CRefBattleCP cp) : d_cp(cp) { }

       bool operator()(const MidUnitInfo& info) { return info.cp() == d_cp; }

       CRefBattleCP d_cp;
    };
#endif

//--- Bug in Watcom, prevents following line from working, SWG: 7Jun2001
#if defined(__WATCOM_CPLUSPLUS__) && (__WATCOM_CPLUSPLUS__ <= 1100)
   // special version for Watcom!
   iterator it(begin());
   while((it != end()) && ((*it).cp() != cp))
      ++it;
#else
    iterator it = std::find_if(begin(), end(), isCP(cp));
#endif

    if(it != end())
       return &(*it);
    else
       return 0;
}

bool MidUnitList::inList(CRefBattleCP cp)
{
    return (find(cp) != 0);
}

static UWORD s_listVersion = 0x0000;
bool MidUnitList::readData(FileReader& f, OrderBattle* ob)
{
   UWORD version;
   f >> version;
   ASSERT(version <= s_listVersion);

   int items;
   f >> items;
   while(items--)
   {
     push_back(MidUnitInfo());
     MidUnitInfo& ui = back();

     ui.readData(f, ob);
   }
   return f.isOK();
}

bool MidUnitList::writeData(FileWriter& f, OrderBattle* ob) const
{
   f << s_listVersion;

   int items = size();
   f << items;

   for(std::list<MidUnitInfo>::const_iterator it = begin();
       it != end();
       ++it)
   {
      (*it).writeData(f, ob);
   }

   return f.isOK();
}

};  // namespace WG_BattleAI_Internal

/*----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.2  2001/06/18 08:25:43  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 * Revision 1.1  2001/06/13 08:52:36  greenius
 * Initial Import
 *
 *
 *----------------------------------------------------------------------
 */

/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.2  2001/06/18 08:25:43  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
