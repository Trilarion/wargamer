# Microsoft Developer Studio Generated NMAKE File, Based on batai.dsp
!IF "$(CFG)" == ""
CFG=batai - Win32 Debug
!MESSAGE No configuration specified. Defaulting to batai - Win32 Debug.
!ENDIF 

!IF "$(CFG)" != "batai - Win32 Release" && "$(CFG)" != "batai - Win32 Debug"
!MESSAGE Invalid configuration "$(CFG)" specified.
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "batai.mak" CFG="batai - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "batai - Win32 Release" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE "batai - Win32 Debug" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE 
!ERROR An invalid configuration is specified.
!ENDIF 

!IF "$(OS)" == "Windows_NT"
NULL=
!ELSE 
NULL=nul
!ENDIF 

CPP=cl.exe
MTL=midl.exe
RSC=rc.exe

!IF  "$(CFG)" == "batai - Win32 Release"

OUTDIR=.\..\..\exe
INTDIR=.\o\release
# Begin Custom Macros
OutDir=.\..\..\exe
# End Custom Macros

!IF "$(RECURSE)" == "0" 

ALL : "$(OUTDIR)\batai.dll"

!ELSE 

ALL : "gwind - Win32 Release" "gamesup - Win32 Release" "ob - Win32 Release" "batdata - Win32 Release" "system - Win32 Release" "$(OUTDIR)\batai.dll"

!ENDIF 

!IF "$(RECURSE)" == "1" 
CLEAN :"system - Win32 ReleaseCLEAN" "batdata - Win32 ReleaseCLEAN" "ob - Win32 ReleaseCLEAN" "gamesup - Win32 ReleaseCLEAN" "gwind - Win32 ReleaseCLEAN" 
!ELSE 
CLEAN :
!ENDIF 
	-@erase "$(INTDIR)\aib_area.obj"
	-@erase "$(INTDIR)\aib_data.obj"
	-@erase "$(INTDIR)\aib_hi.obj"
	-@erase "$(INTDIR)\aib_hiob.obj"
	-@erase "$(INTDIR)\aib_info.obj"
	-@erase "$(INTDIR)\aib_log.obj"
	-@erase "$(INTDIR)\aib_mid.obj"
	-@erase "$(INTDIR)\aib_msg.obj"
	-@erase "$(INTDIR)\aib_side.obj"
	-@erase "$(INTDIR)\aib_sobj.obj"
	-@erase "$(INTDIR)\aib_util.obj"
	-@erase "$(INTDIR)\aisreadr.obj"
	-@erase "$(INTDIR)\ba_agpos.obj"
	-@erase "$(INTDIR)\bairestr.obj"
	-@erase "$(INTDIR)\baitable.obj"
	-@erase "$(INTDIR)\batai.obj"
	-@erase "$(INTDIR)\h_cplist.obj"
	-@erase "$(INTDIR)\h_ldlist.obj"
	-@erase "$(INTDIR)\h_zlist.obj"
	-@erase "$(INTDIR)\hi_asign.obj"
	-@erase "$(INTDIR)\hi_unit.obj"
	-@erase "$(INTDIR)\hi_zone.obj"
	-@erase "$(INTDIR)\hiopplan.obj"
	-@erase "$(INTDIR)\m_cp.obj"
	-@erase "$(INTDIR)\mid_op.obj"
	-@erase "$(INTDIR)\midutil.obj"
	-@erase "$(INTDIR)\vc60.idb"
	-@erase "$(INTDIR)\vc60.pdb"
	-@erase "$(OUTDIR)\batai.dll"
	-@erase "$(OUTDIR)\batai.exp"
	-@erase "$(OUTDIR)\batai.lib"
	-@erase "$(OUTDIR)\batai.pdb"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

"$(INTDIR)" :
    if not exist "$(INTDIR)/$(NULL)" mkdir "$(INTDIR)"

CPP_PROJ=/nologo /MD /W3 /Gi /GR /GX /Zi /O2 /I "..\h" /I "..\..\res" /I "..\system" /I "..\gamesup" /I "..\ob" /I "..\batdata" /I "..\gwind" /D "NDEBUG" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "EXPORT_BATAI_DLL" /Fp"$(INTDIR)\batai.pch" /YX"stdinc.hpp" /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /c 
MTL_PROJ=/nologo /D "NDEBUG" /mktyplib203 /win32 
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\batai.bsc" 
BSC32_SBRS= \
	
LINK32=link.exe
LINK32_FLAGS=user32.lib /nologo /dll /incremental:no /pdb:"$(OUTDIR)\batai.pdb" /debug /machine:I386 /out:"$(OUTDIR)\batai.dll" /implib:"$(OUTDIR)\batai.lib" 
LINK32_OBJS= \
	"$(INTDIR)\aib_area.obj" \
	"$(INTDIR)\aib_data.obj" \
	"$(INTDIR)\aib_hi.obj" \
	"$(INTDIR)\aib_hiob.obj" \
	"$(INTDIR)\aib_info.obj" \
	"$(INTDIR)\aib_log.obj" \
	"$(INTDIR)\aib_mid.obj" \
	"$(INTDIR)\aib_msg.obj" \
	"$(INTDIR)\aib_side.obj" \
	"$(INTDIR)\aib_sobj.obj" \
	"$(INTDIR)\aib_util.obj" \
	"$(INTDIR)\aisreadr.obj" \
	"$(INTDIR)\ba_agpos.obj" \
	"$(INTDIR)\bairestr.obj" \
	"$(INTDIR)\baitable.obj" \
	"$(INTDIR)\batai.obj" \
	"$(INTDIR)\h_cplist.obj" \
	"$(INTDIR)\h_ldlist.obj" \
	"$(INTDIR)\h_zlist.obj" \
	"$(INTDIR)\hi_asign.obj" \
	"$(INTDIR)\hi_unit.obj" \
	"$(INTDIR)\hi_zone.obj" \
	"$(INTDIR)\hiopplan.obj" \
	"$(INTDIR)\m_cp.obj" \
	"$(INTDIR)\mid_op.obj" \
	"$(INTDIR)\midutil.obj" \
	"$(OUTDIR)\system.lib" \
	"$(OUTDIR)\batdata.lib" \
	"$(OUTDIR)\ob.lib" \
	"$(OUTDIR)\gamesup.lib" \
	"$(OUTDIR)\gwind.lib"

"$(OUTDIR)\batai.dll" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

!ELSEIF  "$(CFG)" == "batai - Win32 Debug"

OUTDIR=.\..\..\exe
INTDIR=.\o\debug
# Begin Custom Macros
OutDir=.\..\..\exe
# End Custom Macros

!IF "$(RECURSE)" == "0" 

ALL : "$(OUTDIR)\bataiDB.dll"

!ELSE 

ALL : "gwind - Win32 Debug" "gamesup - Win32 Debug" "ob - Win32 Debug" "batdata - Win32 Debug" "system - Win32 Debug" "$(OUTDIR)\bataiDB.dll"

!ENDIF 

!IF "$(RECURSE)" == "1" 
CLEAN :"system - Win32 DebugCLEAN" "batdata - Win32 DebugCLEAN" "ob - Win32 DebugCLEAN" "gamesup - Win32 DebugCLEAN" "gwind - Win32 DebugCLEAN" 
!ELSE 
CLEAN :
!ENDIF 
	-@erase "$(INTDIR)\aib_area.obj"
	-@erase "$(INTDIR)\aib_data.obj"
	-@erase "$(INTDIR)\aib_hi.obj"
	-@erase "$(INTDIR)\aib_hiob.obj"
	-@erase "$(INTDIR)\aib_info.obj"
	-@erase "$(INTDIR)\aib_log.obj"
	-@erase "$(INTDIR)\aib_mid.obj"
	-@erase "$(INTDIR)\aib_msg.obj"
	-@erase "$(INTDIR)\aib_side.obj"
	-@erase "$(INTDIR)\aib_sobj.obj"
	-@erase "$(INTDIR)\aib_util.obj"
	-@erase "$(INTDIR)\aisreadr.obj"
	-@erase "$(INTDIR)\ba_agpos.obj"
	-@erase "$(INTDIR)\bairestr.obj"
	-@erase "$(INTDIR)\baitable.obj"
	-@erase "$(INTDIR)\batai.obj"
	-@erase "$(INTDIR)\h_cplist.obj"
	-@erase "$(INTDIR)\h_ldlist.obj"
	-@erase "$(INTDIR)\h_zlist.obj"
	-@erase "$(INTDIR)\hi_asign.obj"
	-@erase "$(INTDIR)\hi_unit.obj"
	-@erase "$(INTDIR)\hi_zone.obj"
	-@erase "$(INTDIR)\hiopplan.obj"
	-@erase "$(INTDIR)\m_cp.obj"
	-@erase "$(INTDIR)\mid_op.obj"
	-@erase "$(INTDIR)\midutil.obj"
	-@erase "$(INTDIR)\vc60.idb"
	-@erase "$(INTDIR)\vc60.pdb"
	-@erase "$(OUTDIR)\bataiDB.dll"
	-@erase "$(OUTDIR)\bataiDB.exp"
	-@erase "$(OUTDIR)\bataiDB.ilk"
	-@erase "$(OUTDIR)\bataiDB.lib"
	-@erase "$(OUTDIR)\bataiDB.pdb"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

"$(INTDIR)" :
    if not exist "$(INTDIR)/$(NULL)" mkdir "$(INTDIR)"

CPP_PROJ=/nologo /MDd /W3 /Gm /Gi /GR /GX /ZI /Od /I "..\h" /I "..\..\res" /I "..\system" /I "..\gamesup" /I "..\ob" /I "..\batdata" /I "..\gwind" /D "_DEBUG" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "EXPORT_BATAI_DLL" /Fp"$(INTDIR)\batai.pch" /YX"stdinc.hpp" /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /GZ /c 
MTL_PROJ=/nologo /D "_DEBUG" /mktyplib203 /win32 
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\batai.bsc" 
BSC32_SBRS= \
	
LINK32=link.exe
LINK32_FLAGS=user32.lib /nologo /dll /incremental:yes /pdb:"$(OUTDIR)\bataiDB.pdb" /debug /machine:I386 /out:"$(OUTDIR)\bataiDB.dll" /implib:"$(OUTDIR)\bataiDB.lib" /pdbtype:sept 
LINK32_OBJS= \
	"$(INTDIR)\aib_area.obj" \
	"$(INTDIR)\aib_data.obj" \
	"$(INTDIR)\aib_hi.obj" \
	"$(INTDIR)\aib_hiob.obj" \
	"$(INTDIR)\aib_info.obj" \
	"$(INTDIR)\aib_log.obj" \
	"$(INTDIR)\aib_mid.obj" \
	"$(INTDIR)\aib_msg.obj" \
	"$(INTDIR)\aib_side.obj" \
	"$(INTDIR)\aib_sobj.obj" \
	"$(INTDIR)\aib_util.obj" \
	"$(INTDIR)\aisreadr.obj" \
	"$(INTDIR)\ba_agpos.obj" \
	"$(INTDIR)\bairestr.obj" \
	"$(INTDIR)\baitable.obj" \
	"$(INTDIR)\batai.obj" \
	"$(INTDIR)\h_cplist.obj" \
	"$(INTDIR)\h_ldlist.obj" \
	"$(INTDIR)\h_zlist.obj" \
	"$(INTDIR)\hi_asign.obj" \
	"$(INTDIR)\hi_unit.obj" \
	"$(INTDIR)\hi_zone.obj" \
	"$(INTDIR)\hiopplan.obj" \
	"$(INTDIR)\m_cp.obj" \
	"$(INTDIR)\mid_op.obj" \
	"$(INTDIR)\midutil.obj" \
	"$(OUTDIR)\systemDB.lib" \
	"$(OUTDIR)\batdataDB.lib" \
	"$(OUTDIR)\obDB.lib" \
	"$(OUTDIR)\gamesupDB.lib" \
	"$(OUTDIR)\gwindDB.lib"

"$(OUTDIR)\bataiDB.dll" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

!ENDIF 

.c{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.c{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<


!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("batai.dep")
!INCLUDE "batai.dep"
!ELSE 
!MESSAGE Warning: cannot find "batai.dep"
!ENDIF 
!ENDIF 


!IF "$(CFG)" == "batai - Win32 Release" || "$(CFG)" == "batai - Win32 Debug"
SOURCE=.\aib_area.cpp

"$(INTDIR)\aib_area.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\aib_data.cpp

"$(INTDIR)\aib_data.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\aib_hi.cpp

"$(INTDIR)\aib_hi.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\aib_hiob.cpp

"$(INTDIR)\aib_hiob.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\aib_info.cpp

"$(INTDIR)\aib_info.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\aib_log.cpp

"$(INTDIR)\aib_log.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\aib_mid.cpp

"$(INTDIR)\aib_mid.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\aib_msg.cpp

"$(INTDIR)\aib_msg.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\aib_side.cpp

"$(INTDIR)\aib_side.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\aib_sobj.cpp

"$(INTDIR)\aib_sobj.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\aib_util.cpp

"$(INTDIR)\aib_util.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\aisreadr.cpp

"$(INTDIR)\aisreadr.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\ba_agpos.cpp

"$(INTDIR)\ba_agpos.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\bairestr.cpp

"$(INTDIR)\bairestr.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\baitable.cpp

"$(INTDIR)\baitable.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\batai.cpp

"$(INTDIR)\batai.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\h_cplist.cpp

"$(INTDIR)\h_cplist.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\h_ldlist.cpp

"$(INTDIR)\h_ldlist.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\h_zlist.cpp

"$(INTDIR)\h_zlist.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\hi_asign.cpp

"$(INTDIR)\hi_asign.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\hi_unit.cpp

"$(INTDIR)\hi_unit.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\hi_zone.cpp

"$(INTDIR)\hi_zone.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\hiopplan.cpp

"$(INTDIR)\hiopplan.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\m_cp.cpp

"$(INTDIR)\m_cp.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\mid_op.cpp

"$(INTDIR)\mid_op.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\midutil.cpp

"$(INTDIR)\midutil.obj" : $(SOURCE) "$(INTDIR)"


!IF  "$(CFG)" == "batai - Win32 Release"

"system - Win32 Release" : 
   cd "\src\sf\wargamer\SRC\SYSTEM"
   $(MAKE) /$(MAKEFLAGS) /F .\system.mak CFG="system - Win32 Release" 
   cd "..\batai"

"system - Win32 ReleaseCLEAN" : 
   cd "\src\sf\wargamer\SRC\SYSTEM"
   $(MAKE) /$(MAKEFLAGS) /F .\system.mak CFG="system - Win32 Release" RECURSE=1 CLEAN 
   cd "..\batai"

!ELSEIF  "$(CFG)" == "batai - Win32 Debug"

"system - Win32 Debug" : 
   cd "\src\sf\wargamer\SRC\SYSTEM"
   $(MAKE) /$(MAKEFLAGS) /F .\system.mak CFG="system - Win32 Debug" 
   cd "..\batai"

"system - Win32 DebugCLEAN" : 
   cd "\src\sf\wargamer\SRC\SYSTEM"
   $(MAKE) /$(MAKEFLAGS) /F .\system.mak CFG="system - Win32 Debug" RECURSE=1 CLEAN 
   cd "..\batai"

!ENDIF 

!IF  "$(CFG)" == "batai - Win32 Release"

"batdata - Win32 Release" : 
   cd "\src\sf\wargamer\SRC\batdata"
   $(MAKE) /$(MAKEFLAGS) /F .\batdata.mak CFG="batdata - Win32 Release" 
   cd "..\batai"

"batdata - Win32 ReleaseCLEAN" : 
   cd "\src\sf\wargamer\SRC\batdata"
   $(MAKE) /$(MAKEFLAGS) /F .\batdata.mak CFG="batdata - Win32 Release" RECURSE=1 CLEAN 
   cd "..\batai"

!ELSEIF  "$(CFG)" == "batai - Win32 Debug"

"batdata - Win32 Debug" : 
   cd "\src\sf\wargamer\SRC\batdata"
   $(MAKE) /$(MAKEFLAGS) /F .\batdata.mak CFG="batdata - Win32 Debug" 
   cd "..\batai"

"batdata - Win32 DebugCLEAN" : 
   cd "\src\sf\wargamer\SRC\batdata"
   $(MAKE) /$(MAKEFLAGS) /F .\batdata.mak CFG="batdata - Win32 Debug" RECURSE=1 CLEAN 
   cd "..\batai"

!ENDIF 

!IF  "$(CFG)" == "batai - Win32 Release"

"ob - Win32 Release" : 
   cd "\src\sf\wargamer\SRC\OB"
   $(MAKE) /$(MAKEFLAGS) /F .\ob.mak CFG="ob - Win32 Release" 
   cd "..\batai"

"ob - Win32 ReleaseCLEAN" : 
   cd "\src\sf\wargamer\SRC\OB"
   $(MAKE) /$(MAKEFLAGS) /F .\ob.mak CFG="ob - Win32 Release" RECURSE=1 CLEAN 
   cd "..\batai"

!ELSEIF  "$(CFG)" == "batai - Win32 Debug"

"ob - Win32 Debug" : 
   cd "\src\sf\wargamer\SRC\OB"
   $(MAKE) /$(MAKEFLAGS) /F .\ob.mak CFG="ob - Win32 Debug" 
   cd "..\batai"

"ob - Win32 DebugCLEAN" : 
   cd "\src\sf\wargamer\SRC\OB"
   $(MAKE) /$(MAKEFLAGS) /F .\ob.mak CFG="ob - Win32 Debug" RECURSE=1 CLEAN 
   cd "..\batai"

!ENDIF 

!IF  "$(CFG)" == "batai - Win32 Release"

"gamesup - Win32 Release" : 
   cd "\src\sf\wargamer\SRC\GAMESUP"
   $(MAKE) /$(MAKEFLAGS) /F .\gamesup.mak CFG="gamesup - Win32 Release" 
   cd "..\batai"

"gamesup - Win32 ReleaseCLEAN" : 
   cd "\src\sf\wargamer\SRC\GAMESUP"
   $(MAKE) /$(MAKEFLAGS) /F .\gamesup.mak CFG="gamesup - Win32 Release" RECURSE=1 CLEAN 
   cd "..\batai"

!ELSEIF  "$(CFG)" == "batai - Win32 Debug"

"gamesup - Win32 Debug" : 
   cd "\src\sf\wargamer\SRC\GAMESUP"
   $(MAKE) /$(MAKEFLAGS) /F .\gamesup.mak CFG="gamesup - Win32 Debug" 
   cd "..\batai"

"gamesup - Win32 DebugCLEAN" : 
   cd "\src\sf\wargamer\SRC\GAMESUP"
   $(MAKE) /$(MAKEFLAGS) /F .\gamesup.mak CFG="gamesup - Win32 Debug" RECURSE=1 CLEAN 
   cd "..\batai"

!ENDIF 

!IF  "$(CFG)" == "batai - Win32 Release"

"gwind - Win32 Release" : 
   cd "\src\sf\wargamer\src\gwind"
   $(MAKE) /$(MAKEFLAGS) /F .\gwind.mak CFG="gwind - Win32 Release" 
   cd "..\batai"

"gwind - Win32 ReleaseCLEAN" : 
   cd "\src\sf\wargamer\src\gwind"
   $(MAKE) /$(MAKEFLAGS) /F .\gwind.mak CFG="gwind - Win32 Release" RECURSE=1 CLEAN 
   cd "..\batai"

!ELSEIF  "$(CFG)" == "batai - Win32 Debug"

"gwind - Win32 Debug" : 
   cd "\src\sf\wargamer\src\gwind"
   $(MAKE) /$(MAKEFLAGS) /F .\gwind.mak CFG="gwind - Win32 Debug" 
   cd "..\batai"

"gwind - Win32 DebugCLEAN" : 
   cd "\src\sf\wargamer\src\gwind"
   $(MAKE) /$(MAKEFLAGS) /F .\gwind.mak CFG="gwind - Win32 Debug" RECURSE=1 CLEAN 
   cd "..\batai"

!ENDIF 


!ENDIF 

