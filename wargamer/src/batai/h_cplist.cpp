/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 * Hi Level CP List
 *
 *----------------------------------------------------------------------
 */


#include "stdinc.hpp"
#include "h_cplist.hpp"

namespace WG_BattleAI_Internal
{


HiUnitList::HiUnitList(GameInfo* gameInfo) : 
    GameInfoUser(gameInfo),
    d_corpsCount(0),
    d_divisionCount(0)
{
}

void HiUnitList::addUnit(CRefBattleCP cp)
{
    // pair<iterator, bool> result = insert(HiUnitInfo(cp));
    // ASSERT(result.second);

    Container::push_back(HiUnitInfo(cp));
}

HiUnitInfo* HiUnitList::find(CRefBattleCP cp)
{
   for(Container::iterator hi = begin();
         hi != end();
         ++hi)
   {
      if((*hi).cp() == cp)
         return &(*hi);
   }

  return 0;
}

/*
 * Convert Order of Battle into unitlist if not already done so
 * TODO: Check for reinforcements as the game progresses
 *
 * What it should do is:
 *      iterate through order of battle for all corps / unattached divisions
 *          If unit is alive and not already in list then add it
 *      Also check units in list, and if they are dead then remove them
 *
 */

bool HiUnitList::updateUnits()
{
   if(Container::size() == 0)
   {
//      ASSERT(gameInfo()->isDeploying());
      d_corpsCount = 0;
      d_divisionCount = 0;

      // Iterate through units down to corps and unattached division
      ConstBattleUnitIter unitIter = gameInfo()->ourUnits();
      while(!unitIter.isFinished())
      {
         CRefBattleCP cp = unitIter.cp();

         // add corps or unattached XX's
         if( (cp->getRank().sameRank(Rank_Corps)) ||
             (cp->getRank().sameRank(Rank_Division) && (cp->parent() == NoBattleCP || cp->parent()->getRank().isHigher(Rank_Corps))) )
         {
#ifdef DEBUG
            gameInfo()->log("Added %s", cp->getName());
#endif
            addUnit(cp);

            ASSERT(cp->getRank().sameRank(Rank_Corps) || cp->getRank().sameRank(Rank_Division) );

            if(cp->getRank().sameRank(Rank_Corps))
               ++d_corpsCount;
            else if(cp->getRank().sameRank(Rank_Division))
               ++d_divisionCount;
         }

         unitIter.next();
      }

      return True;
   }

   else
   {
      // add reinforcements
      bool added = False;
      ConstBattleUnitIter unitIter = gameInfo()->ourUnits();
      while(!unitIter.isFinished())
      {
         // add corps or unattached XX's
         if( ((unitIter.cp()->getRank().sameRank(Rank_Corps)) ||
              (unitIter.cp()->getRank().sameRank(Rank_Division) && (unitIter.cp()->parent() == NoBattleCP || unitIter.cp()->parent()->getRank().isHigher(Rank_Corps)))) &&
             (!find(unitIter.cp())) )
         {
            bool add = False;
            int nDiv = 0;
            int nDivOnMap = 0;
            // add only if leading units are on the map
            // first count units on map
            for(ConstBattleUnitIter cIter(unitIter.cp()); !cIter.isFinished(); cIter.next())
            {
               if(cIter.cp()->getRank().sameRank(Rank_Division))
               {
                  nDiv++;

                  if(!cIter.cp()->offMap())
                     nDivOnMap++;
               }
            }

            if( (nDiv < 3 && nDivOnMap == nDiv) || (nDivOnMap >= 3) )
            {
#ifdef DEBUG
               gameInfo()->log("Added %s as reinforcement", unitIter.cp()->getName());
#endif
               addUnit(unitIter.cp());
               added = True;
            }
         }
         unitIter.next();
      }

      return added;
   }
}

static UWORD s_version = 0x0000;
bool HiUnitList::readData(FileReader& f, OrderBattle* ob)
{
   UWORD version;
   f >> version;
   ASSERT(version <= s_version);

   int items;
   f >> items;

   while(items--)
   {
      HiUnitInfo hu;
      hu.readData(f, ob);
      Container::push_back(hu);
   }

   return f.isOK();
}

bool HiUnitList::writeData(FileWriter& f, OrderBattle* ob) const
{
   f << s_version;

   int items = Container::size();
   f << items;

   for(Container::const_iterator hi = begin();
         hi != end();
         ++hi)
   {
      (*hi).writeData(f, ob);
   }

   return f.isOK();
}

};  // namespace WG_BattleAI_Internal



/*
 *----------------------------------------------------------------------
 * $Log$
 *----------------------------------------------------------------------
 */
