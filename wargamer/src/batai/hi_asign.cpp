/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 * Hi Level Corps Assigner
 * Assigns Corps and Unattached Divisions to Zones
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "hi_asign.hpp"
#include "aib_data.hpp"
#include "h_zlist.hpp"
#include "h_cplist.hpp"
#include <vector>
#include "pick.hpp"
#include "aib_util.hpp"
#include "bobutil.hpp"
#include <numeric>
#include <algorithm>

namespace WG_BattleAI_Internal
{

namespace       // Private namespace
{

const int NormalTotal = 1024;

/*
 * Caculate update priorities for all zones:
 * This is:
 *      updatePriority = zoneStrength / totalStrength - zonePriority / totalPriority
 */

std::vector<int> calcUpdatePriorities(HiZoneList* zones)
{
    /*
     * Calculate Totals
     */

    int totalStrength = 0;
    int totalPriority = 0;

    for(HiZoneList::iterator it = zones->begin(); it != zones->end(); ++it)
    {
        totalStrength += (*it)->spCount();
        totalPriority += (*it)->priority();
      }

    /*
     * Calculate individuals
     */

    std::vector<int> result;

    result.reserve(zones->size());

    int zoneIndex = 0;
    for(it = zones->begin(); it != zones->end(); ++it, ++zoneIndex)
      {
        int normStrength;
        if(totalStrength)
            normStrength = MulDiv( (*it)->spCount(), NormalTotal, totalStrength);
        else
            normStrength = 0;

        int normPriority;
        if(totalPriority)
            normPriority = MulDiv((*it)->priority(), NormalTotal, totalPriority);
        else
            normPriority = 0;

        result.push_back(normPriority - normStrength);
    }

    return result;
}


};


HiCorpsAssigner::HiCorpsAssigner(GameInfo* data, HiZoneList* zones, HiUnitList* units) :
    GameInfoUser(data),
    d_zones(zones),
    d_units(units)
{
#ifdef DEBUG
      gameInfo()->log("HiCorpsAssigner constructed");
#endif
}

HiCorpsAssigner::~HiCorpsAssigner()
{
#ifdef DEBUG
      gameInfo()->log("HiCorpsAssigner destructed");
#endif
}

// Assign a priority to a XXX or unattached XX
void HiCorpsAssigner::assignUnitPriority(HiUnitInfo* unit)
{
   // give some random points
    int points = gameInfo()->random(50);

    // go through each XX and assign points based on strength, base morale, and leader attribs
    for(ConstBattleUnitIter   iter(unit->cp()); !iter.isFinished(); iter.next())
    {
      if(iter.cp()->getRank().sameRank(Rank_Division))
        {
           BobUtility::UnitCounts uc;
           BobUtility::unitsInManpower(iter.cp(), gameInfo()->battleData(), uc);

           points += (uc.d_nInf / 50);
           points += (uc.d_nCav / 25);
           points += (uc.d_nArtMP / 15);

           points += (.5 * BobUtility::baseMorale(const_cast<BattleOB*>(gameInfo()->battleData()->ob()), const_cast<BattleCP*>(iter.cp())));
           points += (iter.cp()->leader()->getInitiative() + iter.cp()->leader()->getTactical()) / 20;
        }
    }

#ifdef DEBUG
    gameInfo()->log("%d priority points assigned to %s", points, unit->cp()->getName());
#endif
    unit->priority(points);
}

void HiCorpsAssigner::assignUnits()
{
   // initial deployment
   if(gameInfo()->isDeploying())
   {
#ifdef DEBUG
      gameInfo()->log("HiCorpsAssigner::assignUnits() -> Deploying");
#endif

      /*
       * Assign any unattached units
       */

      // first, assign unit priorities
      for(HiUnitList::iterator unitIt = d_units->begin(); unitIt != d_units->end(); ++unitIt)
      {
         HiUnitInfo& unitInfo = *unitIt;
         assignUnitPriority(&unitInfo);
      }

      // now add units to a local list, in order of priority
      WG_BattleAI_Utils::LocalCPList cpList;
      while(cpList.entries() != d_units->size())
      {
         int bestPriority = -1;
         CRefBattleCP bestCP = NoBattleCP;
         for(HiUnitList::iterator unitIt = d_units->begin(); unitIt != d_units->end(); ++unitIt)
         {
            HiUnitInfo& unitInfo = *unitIt;
            if(!cpList.find(unitInfo.cp()) && unitInfo.priority() > bestPriority)
            {
               bestCP = unitInfo.cp();
               bestPriority = unitInfo.priority();
            }
         }

         ASSERT(bestCP != NoBattleCP);
         cpList.add(bestCP);
      }

#ifdef DEBUG
      SListIter<WG_BattleAI_Utils::LocalCP> iter(&cpList);
      gameInfo()->log("Order of priority -------------");
      while(++iter)
         gameInfo()->log("%s", iter.current()->d_cp->getName());
#endif

      // now assign to zones
      while(cpList.entries() > 0)
      {
         WG_BattleAI_Utils::LocalCP* lcp = cpList.first();
         HiUnitInfo* unit = d_units->find(lcp->d_cp);
         ASSERT(unit);
         ASSERT(!unit->inZone());
         assignUnit(unit);
         cpList.remove(lcp);
      }
   }

   // game in progress
   else
   {
      // first, assign unit priorities
      for(HiUnitList::iterator unitIt = d_units->begin(); unitIt != d_units->end(); ++unitIt)
      {
         HiUnitInfo& unitInfo = *unitIt;
         if(!unitInfo.inZone())
            assignUnitAlreadyOnMap(&unitInfo);
      }
   }
}

template<class T>
struct ClipLow : public std::unary_function<T, T>
{
      ClipLow(T clip) : d_clip(clip) { }

      T operator()(T n)
    {
        return (n < d_clip) ? d_clip : n;
    }

    T d_clip;
};

void HiCorpsAssigner::assignUnit(HiUnitInfo* unit)
{
   ASSERT(!unit->inZone());

   // Choose a random zone
   int zoneIndex;
   if(d_zones->size() == 1)
      zoneIndex = 0;
   else
   {
      std::vector<int> priorities = calcUpdatePriorities(d_zones);

      // remove -ve priorities
      std::transform(priorities.begin(), priorities.end(), priorities.begin(), ClipLow<int>(0));
      zoneIndex = pickOption(priorities, gameInfo()->random());
   }

   ASSERT(zoneIndex < d_zones->size());
   assignUnit(unit, (*d_zones)[zoneIndex]);
}

bool HiCorpsAssigner::inZoneBoundry(
      const HiZone* zone,
      HexPosition::Facing corpAvgFacing,
      const HexCord& corpLeft,
      const HexCord& corpRight)
{
   HexCord zLeft = zone->leftHex();
   HexCord zRight = zone->rightHex();
   WG_BattleAI_Utils::MoveOrientation moL = WG_BattleAI_Utils::moveOrientation(corpAvgFacing, corpLeft, zLeft);
   WG_BattleAI_Utils::MoveOrientation moR = WG_BattleAI_Utils::moveOrientation(corpAvgFacing, corpRight, zRight);

   return ( (moL == WG_BattleAI_Utils::MO_Front || moL == WG_BattleAI_Utils::MO_Rear || moL == WG_BattleAI_Utils::MO_Left) &&
                (moR == WG_BattleAI_Utils::MO_Front || moR == WG_BattleAI_Utils::MO_Rear || moR == WG_BattleAI_Utils::MO_Right) );
}

void HiCorpsAssigner::assignUnitAlreadyOnMap(HiUnitInfo* unit)
{
   // assign to the closest zone
   ASSERT(!unit->inZone());
   HexPosition::Facing corpAvgFace = WG_BattleAI_Utils::averageFacing(unit->cp());

   HexCord corpLeft;
   HexCord corpRight;
   WG_BattleAI_Utils::getCorpLeftRightBoundry(unit->cp(), corpAvgFace, corpLeft, corpRight);

#ifdef DEBUG
   gameInfo()->log("Assigning zone to reinforcement %s", unit->cp()->getName());
   gameInfo()->log("%s -- Left Boundry-> (%d, %d), Right Boundry-> (%d, %d)",
            unit->cp()->getName(),
            static_cast<int>(corpLeft.x()), static_cast<int>(corpLeft.y()),
            static_cast<int>(corpRight.x()), static_cast<int>(corpRight.y()));
#endif

   HiZone* bestZone = 0;
   int bestDist = UWORD_MAX;
   for(int z = 0; z < d_zones->size(); z++)
   {
      HiZone* zone = (*d_zones)[z];
      HexCord zLeft = zone->leftHex();
      HexCord zRight = zone->rightHex();

      int dist = minimum(
         minimum(WG_BattleAI_Utils::getDist(corpLeft, zLeft), WG_BattleAI_Utils::getDist(corpRight, zRight)),
         minimum(WG_BattleAI_Utils::getDist(corpRight, zLeft), WG_BattleAI_Utils::getDist(corpLeft, zRight)));

      if(!bestZone || dist < bestDist)
      {
         bestZone = zone;
         bestDist = dist;
      }
   }

   ASSERT(bestZone);
   if(bestZone)
   {
      assignUnit(unit, bestZone);
   }
}

void HiCorpsAssigner::assignUnit(HiUnitInfo* unit, HiZone* zone)
{
   ASSERT(unit);
   ASSERT(zone);
   ASSERT(!unit->inZone());


#ifdef DEBUG
   gameInfo()->log("Assigning %s to %s",
      unit->cp()->getName(),
      (const char*)zone->name());

#endif

   zone->addUnit(unit);
}


void HiCorpsAssigner::unassignUnit(HiUnitInfo* unit)
{
      ASSERT(unit->inZone());

      HiZone* zone = unit->zone();

#ifdef DEBUG
      gameInfo()->log("Removing %s from %s",
            unit->cp()->getName(),
            (const char*)zone->name());

#endif

      zone->removeUnit(unit);
}



};  // namespace WG_BattleAI_Internal


/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.2  2001/06/18 08:25:43  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
