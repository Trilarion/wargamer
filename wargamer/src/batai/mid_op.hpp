/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef MID_OP_HPP
#define MID_OP_HPP

/*
 * Mid-Level Operations. Basically reactions to nearby enemies
 */

#include "aib_info.hpp"
#include "batord.hpp"

class HexArea;
class BattleMessageInfo;
namespace WG_BattleAI_Internal
{

class MidLevelPlanner;
class MidUnitInfo;
class MU_DeployItem;
class MidUnitDeployMap;

class MidLevel_Op : public GameInfoUser {
			MidLevelPlanner* d_planner;
	 public:
			MidLevel_Op(GameInfo* gameInfo, MidLevelPlanner* planner) :
				 GameInfoUser(gameInfo),
				 d_planner(planner) {}

			bool doEnemyToFront(bool alwaysCheck = False);
			bool checkForEnemyToFront();
         bool checkFlanks();
			bool considerHold();
			bool procMessage(const BattleMessageInfo& msg);
         bool process();
         bool updateFrontLine();
	 private:

			// Possible reactions to nearby enemy
			enum Reactions {
					None,      // No reaction at all, continue on present course
					AdvToCC,   // Advance to close combat
					AdvToFC,   // Advance to fire combat
					AdvToBomb, // Advance to bombardment range
					Halt,      // Stop and do nothing
					Retreat,   // Decline to engage

					Reactions_HowMany,
					Reactions_Default = None
			};

			// reaction implementation functions
			bool moveUpRear();
			bool moveUpSupport();
			bool considerRetreat();
         bool processAttacking();
         bool processBombarding();
         bool processPursue(CRefBattleCP enemyCP);
			bool doAdvanceToEnemy(Reactions how);
         bool doRetreatFromEnemy(int range);
			bool calcEnemyArea(HexArea& area);
			bool doHold();
			bool findReplacement(MidUnitDeployMap& map, MU_DeployItem& di, int col);
			bool findSupport(MidUnitDeployMap& map, MU_DeployItem& di, int col);
         bool willBombard();
         bool countArtyAvailable(bool& hasArtyXX, bool& hasAttachedArty, int& nInXX, int& nAttached);
         bool movingOutOfRange();
         bool enemyCavNear();
         enum AgainstWhat {
            AW_Any,
            AW_Inf,
            AW_Cav,
            AW_Art,
            AW_FiringArt
         };
         CRefBattleCP closeEnemy(AgainstWhat w);
         bool launchSortie(AgainstWhat w);
         bool launchSortie(
            MU_DeployItem* di,
            const HexCord targetHex,    // target dest
            const HexCord destHex,      // return dest
            BattleMeasure::HexPosition::Facing facing);
         MU_DeployItem* findReserve(BasicUnitType::value t, BattleMeasure::HexCord target);


			// misc. utilities
			void setAggPosture(BattleOrderInfo::Aggression ag, BattleOrderInfo::Posture pos, bool frontRowOnly);
			void setAggPosture(MU_DeployItem& di, BattleOrderInfo::Aggression ag, BattleOrderInfo::Posture pos);
			CRefBattleCP objLeader();
			Reactions reactHow();
			int totalFrontage(int& nUnits, int& gap);
			int getShortestRange();
			MU_DeployItem* findCloseType(
				BasicUnitType::value t,
				MidUnitDeployMap& map,
				int col,
				int& newC);
			bool shouldReplace(MU_DeployItem& di);
			bool shouldSupport(MU_DeployItem& di);
		   bool canBringUp(MU_DeployItem& di);//const CRefBattleCP& cp);
			void getOurStrVsEnemy(int& ourStr, int& enemyStr);
         enum EnemyRatio {
            Ratio_GreaterThan3,
            Ratio_GreaterThan2,
            Ratio_GreaterThan1_5,
            Ratio_GreaterThan1_2,
            Ratio_Equal,
            Ratio_LessThan1_2,
            Ratio_LessThan1_5,
            Ratio_LessThan2,
            Ratio_LessThan3,
            Ratio_HowMany
         };
			EnemyRatio getOurStrVsEnemy();
			bool engaged();
			bool shouldReact();
		   void getPrecentOfStartMorale(int& perOfStart);
			bool needsNewOrder(MU_DeployItem& di, const HexCord& destHex, BattleMeasure::HexPosition::Facing facing);
         bool procHQ();
#if 0
         void MidLevel_Op::getStartingLeft(
            const HexCord& eLeft,
            int& howMany,
            int spacing,
            HexPosition::Facing ourFacing);
#else
         bool getStartingLeft(
            const HexCord& enemyLeft,
         //   int& howMany,
            int spacing,
            BattleMeasure::HexPosition::Facing ourFacing,
            BattleMeasure::HexCord& startLeft);
#endif
         bool doDeploy(CRefBattleCP cp);
         bool doDeploy();
         bool doUpdateMove();
         void setHighLevelFlags(int id);
         bool deployFromColumn(MidUnitInfo& mi);
         enum AttachWhy {
            AW_Attacking,
            AW_Retreating
         };
         bool shouldAttach(const CRefBattleCP& cp, AttachWhy why);
         bool updateRetreatingUnits(MU_DeployItem& di, int r);
         bool monitorUnits();
         void procUnableToDeploy(const RefBattleCP& cp);
};

}; // end of namespace

#endif
