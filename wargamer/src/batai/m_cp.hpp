/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef M_CP_H
#define M_CP_H

#ifndef __cplusplus
#error m_cp.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1995, Steven Morle-Green, All Rights Reserved
 *----------------------------------------------------------------------
 *
 * Mid Level CP Info
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/06/13 08:52:36  greenius
 * Initial Import
 *
 *
 *----------------------------------------------------------------------
 */

#include "bobdef.hpp"
#include "contain.hpp"
#include "batcord.hpp"
#include "battime.hpp"
#include "batord.hpp"
//#include "bob_cp.hpp"
#include <list>
#include <vector>
//#include "batord.hpp"

class BattleOrder;
class FileReader;
class FileWriter;
class OrderBattle;
namespace WG_BattleAI_Internal
{

// MU_DeployItem holds a XX
class MU_DeployItem {
		CRefBattleCP d_cp;
		BattleOrder  d_order;
		BattleMeasure::BattleTime::Tick d_sendWhen;
      BattleMeasure::HexCord d_objHex;

//		BattleMeasure::HexCord d_leftHex;

		enum Flags {
			Ordered 	     = 0x0001,
			Line    	     = 0x0002,
			Support 	     = 0x0004,
         // Unchecked
         FlankedLeft          = 0x0008,
         FlankedRight         = 0x0010,
         FlankedRear          = 0x0020,
         FlankingLeft         = 0x0040,
         FlankingRight        = 0x0080,
         Bombarding           = 0x0100,
         Attacking            = 0x0200,
         ChangingSPFormation  = 0x0400,
         ChangingDivFormation = 0x0800,
         StartedMove          = 0x1000,
         AdjustCorpHolding    = 0x2000,
         WasCorpHolding     = 0x4000
         // End
		};

		UWORD d_flags;
		void setFlag(UWORD mask, bool f)
		{
			if(f)
				d_flags |= mask;
			else
				d_flags &= ~mask;
		}

	public:

		MU_DeployItem() :
			d_cp(NoBattleCP),
			d_order(),
		d_sendWhen(0),
			d_flags(0) {}

		MU_DeployItem(const CRefBattleCP& cp);
		MU_DeployItem(const MU_DeployItem& di);
		MU_DeployItem& operator = (const MU_DeployItem	& di);

		~MU_DeployItem();

		void cp(const CRefBattleCP& cp) { d_cp = cp; }
		CRefBattleCP cp() const { return d_cp; }

		BattleOrder& order() { return d_order; }
		const BattleOrder& order() const { return d_order; }

		void sendWhen(BattleMeasure::BattleTime::Tick when) { d_sendWhen = when; }
		bool canSend(BattleMeasure::BattleTime::Tick time) { return (time >= d_sendWhen); }

		void updateOrder();
		void ordered(bool f) 		{ setFlag(Ordered, f); }
		void setLine(bool f) 	  { setFlag(Line, f); }
		void setSupport(bool f) { setFlag(Support, f); }
      // Unchecked
      void flankedLeft(bool f) { setFlag(FlankedLeft, f); }
      void flankedRight(bool f) { setFlag(FlankedRight, f); }
      void flankedRear(bool f) { setFlag(FlankedRear, f); }
      void flankingLeft(bool f) { setFlag(FlankingLeft, f); }
      void flankingRight(bool f) { setFlag(FlankingRight, f); }
      void bombarding(bool f) { setFlag(Bombarding, f); }
      void attacking(bool f) { setFlag(Attacking, f); }
      void changingSPFormation(bool f) { setFlag(ChangingSPFormation, f); }
      void changingDivFormation(bool f) { setFlag(ChangingDivFormation, f); }
      void startedMove(bool f) { setFlag(StartedMove, f); }
      void adjustCorpHolding(bool f) { setFlag(AdjustCorpHolding, f); }
      void wasCorpHolding(bool f) { setFlag(WasCorpHolding, f); }
      // End
		bool ordered()    const { return (d_flags & Ordered) != 0; }
		bool isLine()     const { return (d_flags & Line) != 0; }
		bool isSupport()  const { return (d_flags & Support) != 0; }
      // Unchecked
      bool flankedLeft() const { return (d_flags & FlankedLeft) != 0; }
      bool flankedRight() const { return (d_flags & FlankedRight) != 0; }
      bool flankedRear() const { return (d_flags & FlankedRear) != 0; }
      bool flankingLeft() const { return (d_flags & FlankingLeft) != 0; }
      bool flankingRight() const { return (d_flags & FlankingRight) != 0; }
      bool bombarding() const { return (d_flags & Bombarding) != 0; }
      bool attacking() const { return (d_flags & Attacking) != 0; }
      bool changingSPFormation() const { return (d_flags & ChangingSPFormation) != 0; }
      bool changingDivFormation() const { return (d_flags & ChangingDivFormation) != 0; }
      bool startedMove() const { return (d_flags & StartedMove) != 0; }
      bool adjustCorpHolding() const { return (d_flags & AdjustCorpHolding) != 0; }
      bool wasCorpHolding() const { return (d_flags & WasCorpHolding) != 0; }
      // End

      void objHex(const BattleMeasure::HexCord& hex) { d_objHex = hex; }
      const BattleMeasure::HexCord& objHex() const { return d_objHex; }
      const BattleMeasure::HexCord& objHex() { return d_objHex; }


		// file interface
		bool readData(FileReader& f, OrderBattle* ob);
		bool writeData(FileWriter& f, OrderBattle* ob) const;
};

// DeployMap. A layout of a XXX's divisions (Front, Rear, Line, or support). Each XXX has one.
class MidUnitDeployMap {
	public:
		typedef std::vector<MU_DeployItem> Container;
	private:

	 Container d_container;

		int d_nFront; // Number of XX's in front rank
		int d_nRear;		// Number of XX's in rear rank
	public:
		MidUnitDeployMap() :
		 d_nFront(0),
		 d_nRear(0) {}

		~MidUnitDeployMap() {}

		MU_DeployItem* add(const CRefBattleCP& cp, int r);

		MU_DeployItem& item(int c, int r)
		{
			ASSERT(r < 2);
			int index = (r * d_nFront) + c;
			ASSERT(index < d_container.size());
			return d_container[index];
		}

		const MU_DeployItem& item(int c, int r) const
		{
			ASSERT(r < 2);
			int index = (r * d_nFront) + c;
			ASSERT(index < d_container.size());
			return d_container[index];
		}

		MU_DeployItem* find(const CRefBattleCP& cp);
		bool inMap(const CRefBattleCP& cp);

		const Container& container() const { return d_container; }
		Container& container() { return d_container; }

      void nFront(int n) { d_nFront = n; }
      void nRear(int n) { d_nRear = n; }
		int nFront() const { return d_nFront; }
		int nRear()  const { return d_nRear; }

		bool readData(FileReader& f, OrderBattle* ob);
		bool writeData(FileWriter& f, OrderBattle* ob) const;
};

class MidUnitInfo
{
		public:
			 MidUnitInfo() : d_cp(NoBattleCP), d_ordered(false) { }
			 MidUnitInfo(CRefBattleCP cp) : d_cp(cp), d_ordered(false) { }
			 ~MidUnitInfo() { }

			 CRefBattleCP cp() const { return d_cp; }

			 bool ordered() const { return d_ordered; }
			 void ordered(bool f) { d_ordered = f; }

			 MidUnitDeployMap& map() { return d_map; }
			 const MidUnitDeployMap& map() const { return d_map; }

			 bool readData(FileReader& f, OrderBattle* ob);
			 bool writeData(FileWriter& f, OrderBattle* ob) const;

		private:
			 CRefBattleCP d_cp;
			 bool d_ordered;

			 // Add more info here...
			 MidUnitDeployMap d_map;
};

inline bool operator==(const MidUnitInfo& lhs, const MidUnitInfo& rhs)
{
		return lhs.cp() == rhs.cp();
}

inline bool operator<(const MidUnitInfo& lhs, const MidUnitInfo& rhs)
{
    return lhs.cp() < rhs.cp();
}

class MidUnitList : public std::list<MidUnitInfo>//STLContainer<list<MidUnitInfo> >
{
    public:
        MidUnitList();
        ~MidUnitList() { }

        void addUnit(CRefBattleCP cp);
        bool inList(CRefBattleCP cp);

		  bool readData(FileReader& f, OrderBattle* ob);
		  bool writeData(FileWriter& f, OrderBattle* ob) const;
	 private:
       MidUnitInfo* find(CRefBattleCP cp);
};



};  // namespace WG_BattleAI_Internal




#endif /* M_CP_H */

