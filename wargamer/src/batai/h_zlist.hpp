/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef H_ZLIST_HPP
#define H_ZLIST_HPP

#ifndef __cplusplus
#error h_zlist.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 * Hi level Zone List
 *
 *----------------------------------------------------------------------
 */


#include "hi_zone.hpp"
#include "contain.hpp"
#include "hiopplan.hpp"
#include "battime.hpp"
#include "batcord.hpp"
#include <vector>

class FileReader;
class FileWriter;

namespace WG_BattleAI_Internal
{

class HiZoneList : public GameInfoUser, public STLContainer<std::vector<HiZone*> >
{
			 friend class BAI_ScriptReader;
		public:
				HiZoneList(GameInfo* gameInfo) :
					GameInfoUser(gameInfo),
					d_phase(0),
					d_nPhases(0) { }
				~HiZoneList();

				void updateZones(int nCorps, int nZones, Hi_OpPlan::Plan& plan);
						// Create and update the zones

				/*
				 * Access functions
				 */

				int size() const { return Container::size(); }

				HiZone* operator[](Container::size_type i) { return Container::operator[](i); }
				const HiZone* operator[](Container::size_type i) const { return Container::operator[](i); }

				// file interface
				bool readData(FileReader& f);
				bool writeData(FileWriter& f) const;
		private:

			struct AdvBoundry {
				int d_leftLR; // left-right
				int d_rightLR; // left-right
				int d_leftFB; // forward-back
				int d_rightFB; // forward-back
			};

				void doDeployZones(int nCorps, int nZones, Hi_OpPlan::Plan& plan);
				void doCalcNewZones();
				void updatePlan(Hi_OpPlan::Plan plan);
				HiZone* createZone(
               HexCord::Cord left,
               HexCord::Cord right,
               int depth,
               const HexCord& bLeft,
               const HexCord& tRight);
				HiZone* createZone(HexCord left, HexCord right);
				void deleteZone(const iterator& it);
				int zonePriority(int zone, int nZones, Hi_OpPlan::Plan plan);
				BattleMeasure::BattleTime::Tick zoneStartTime(int zone, int nZones, Hi_OpPlan::Plan plan);
				AdvBoundry advanceZoneBoundries(
					int zone,
					int nZones,
					Hi_OpPlan::Plan plan,
					int phase);
				void calcZones(int nZones, Hi_OpPlan::Plan plan);
				ZoneOrientation zoneOrientation(int zone, int nZones, Hi_OpPlan::Plan plan);
//				void getSideLeftRightBoundry(HexPosition::Facing avgFace, HexCord& leftHex, HexCord& rightHex);
#if 0
				bool HiZoneList::restrictedTerrainSplitsSide(
					const HexCord& leftHex,
					const HexCord& rightHex,
					const HexPosition::Facing avgFace,
					HexList& hl);
#endif
				void assignPriority(int nZones, Hi_OpPlan::Plan plan);
				void zoneAggPos(int zone, int nZones, Hi_OpPlan::Plan plan, BAI_AggressPosStruct& agPos);

		private:
				int d_phase;   // current phase of current plan
				int d_nPhases; // number of planning phases for current plan
};

};  // namespace WG_BattleAI_Internal


#endif /* H_ZLIST_HPP */

