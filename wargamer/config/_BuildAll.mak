# Microsoft Developer Studio Generated NMAKE File, Based on _BuildAll.dsp
!IF "$(CFG)" == ""
CFG=_BuildAll - Win32 Debug
!MESSAGE No configuration specified. Defaulting to _BuildAll - Win32 Debug.
!ENDIF 

!IF "$(CFG)" != "_BuildAll - Win32 Release" && "$(CFG)" != "_BuildAll - Win32 Debug"
!MESSAGE Invalid configuration "$(CFG)" specified.
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "_BuildAll.mak" CFG="_BuildAll - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "_BuildAll - Win32 Release" (based on "Win32 (x86) Generic Project")
!MESSAGE "_BuildAll - Win32 Debug" (based on "Win32 (x86) Generic Project")
!MESSAGE 
!ERROR An invalid configuration is specified.
!ENDIF 

!IF "$(OS)" == "Windows_NT"
NULL=
!ELSE 
NULL=nul
!ENDIF 

OUTDIR=.\Release
INTDIR=.\Release

ALL : 


CLEAN :
	-@erase 

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

MTL=midl.exe

!IF  "$(CFG)" == "_BuildAll - Win32 Release"

!ELSEIF  "$(CFG)" == "_BuildAll - Win32 Debug"

!ENDIF 

MTL_PROJ=

!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("_BuildAll.dep")
!INCLUDE "_BuildAll.dep"
!ELSE 
!MESSAGE Warning: cannot find "_BuildAll.dep"
!ENDIF 
!ENDIF 


!IF "$(CFG)" == "_BuildAll - Win32 Release" || "$(CFG)" == "_BuildAll - Win32 Debug"

!ENDIF 

