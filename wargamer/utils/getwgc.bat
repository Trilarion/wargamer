@echo off
rem Get Ben's latest Campaigns
setlocal
set SRCDIR=\\tigger\c\wargamer\nap1813\campaigns
set DESTDIR=%HOME%\game\nap1813
unset /q DEBUG

set SRCNAME=April1813full
set DESTNAME=April1813.wgc
gosub getFile

set SRCNAME=April1813short
set DESTNAME=April1813short.wgc
gosub getFile

set SRCNAME=Autumn1813
set DESTNAME=Autumn1813.wgc
gosub getFile

set SRCNAME=August1813full
set DESTNAME=August1813.wgc
gosub getFile

set SRCNAME=August1813short
set DESTNAME=August1813short.wgc
gosub getFile

set SRCNAME=March1813
set DESTNAME=March1813.wgc
gosub getFile

quit

:getFile
    unset /q BESTFILE
    set BESTDATE=0
    rem Iterate through SRC files and pick one with newest date
    for %FILE in (%SRCDIR%\%SRCNAME%*.wgc) do gosub checkFile
    rem Copy to destination
    IF DEFINED BESTFILE copy %BESTFILE% %DESTDIR%\%DESTNAME%
    return

:checkFile
    set AGE=%@FILEAGE[%FILE]
    if defined DEBUG echo %FILE %AGE
    IFF not defined BESTFILE .or. %AGE% gt %BESTDATE THEN
        set BESTDATE=%AGE%
        set BESTFILE=%FILE
        if defined DEBUG echo Set %FILE as Best
    ENDIFF
    return