rem -----------------------------------------------------
rem ---- Copy Files Into Directory for InstallBuilder ---
rem -----------------------------------------------------

setlocal
IFF defined WG_CD_SRC THEN
    set SRCDIR=%WG_CD_SRC
ELSE
    set SRCDIR=%HOME\game
ENDIFF

IFF defined WG_INSTALL THEN
    set DESTDIR=%WG_INSTALL
ELSE
    echo "You must set WG_INSTALL to the installation directory"
    quit -1
ENDIFF

rem set DESTDIR=f:\wginstall

set GAMEDIR=%DESTDIR\game

if not isdir %GAMEDIR md /s %GAMEDIR
if not isdir %GAMEDIR\nap1813 md /s %GAMEDIR\nap1813
if not isdir %GAMEDIR\nap1813\fonts     md /s %GAMEDIR\nap1813\fonts
if not isdir %GAMEDIR\nap1813\frontend  md /s %GAMEDIR\nap1813\frontend
if not isdir %GAMEDIR\nap1813\portraits md /s %GAMEDIR\nap1813\portraits
if not isdir %GAMEDIR\nap1813\provinces md /s %GAMEDIR\nap1813\provinces
if not isdir %GAMEDIR\nap1813\victory   md /s %GAMEDIR\nap1813\victory
if not isdir %GAMEDIR\nap1813\germany   md /s %GAMEDIR\nap1813\german
if not isdir %GAMEDIR\help              md /s %GAMEDIR\help
if not isdir %GAMEDIR\sounds            md /s %GAMEDIR\sounds

set COPYFLAGS=/z /e /u /a: /[!*.cam *.jbf *.gid *.sym *.rtf *.asc *.log *.raw *.dbg *.bat *.map *.mce *.mcx]
set MOVEFLAGS=/e /d

*copy %COPYFLAGS %SRCDIR\*.*                   %GAMEDIR
*copy %COPYFLAGS %SRCDIR\nap1813\*.*           %GAMEDIR\nap1813
*copy %COPYFLAGS %SRCDIR\nap1813\fonts\*.*     %GAMEDIR\nap1813\fonts
*copy %COPYFLAGS %SRCDIR\nap1813\frontend\*.*  %GAMEDIR\nap1813\frontend
*copy %COPYFLAGS %SRCDIR\nap1813\portraits\*.* %GAMEDIR\nap1813\portraits
*copy %COPYFLAGS %SRCDIR\nap1813\provinces\*.* %GAMEDIR\nap1813\provinces
*copy %COPYFLAGS %SRCDIR\nap1813\victory\*.*   %GAMEDIR\nap1813\victory
*copy %COPYFLAGS /s %SRCDIR\nap1813\german\*.*   %GAMEDIR\nap1813\german
*copy %COPYFLAGS %SRCDIR\help\*.*              %GAMEDIR\help
*copy /s %COPYFLAGS %SRCDIR\sounds\*.*         %GAMEDIR\sounds
*move %MOVEFLAGS %GAMEDIR\readme.txt           %DESTDIR

rem ----------------------------------------------
rem Remove Obsolete files and Empty subdirectories
rem ----------------------------------------------

*del /e /s %DESTDIR\cmap_*.bmp
*del /e /s "%DESTDIR\Copy of*.*"
*del /e /s %DESTDIR\c_map2048b.bmp
*del /e /s %DESTDIR\testbattle.inb
*del /e %DESTDIR\*.bmp
*del /x /s /a:d /e /y %DESTDIR\*.*

endlocal