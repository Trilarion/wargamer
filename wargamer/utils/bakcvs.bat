setlocal
pushd V:\WarGamer95.vcd

set batpath=%@PATH[%@TRUENAME[%_BATCHNAME]]

set dd=%_date
set dy=%@substr[%dd,0,2]%@substr[%dd,3,2]%@substr[%dd,6,2]
set datfile=%batpath%bakcvs.day
set BAKFILE=%BACKUP\vcs_%@SUBSTR[%dd,0,2]%@SUBSTR[%dd,3,2].zip

set ZIPFLAGS=-add -dir


iff exist %datfile then
  set lastdate=%@line[%datfile,0]
  rem pkzip25 -t%lastdate %ZIPFLAGS %BAKFILE *.* 
  pkzip25 -after=%lastdate %ZIPFLAGS %BAKFILE *.* 
 else
  pkzip25 %ZIPFLAGS %BAKFILE *.*
endiff

echo %dy >! %datfile

call tidy

popd
