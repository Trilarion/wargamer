setlocal
set batpath=%@PATH[%@TRUENAME[%_BATCHNAME]]
set dd=%_date
set dy=%@substr[%dd,0,2]%@substr[%dd,3,2]%@substr[%dd,6,2]
set BAKFILE=%BACKUP\wg_%@SUBSTR[%dd,0,2]%@SUBSTR[%dd,3,2].zip
set ZIPFLAGS=-add -rec -path -excl=@%batpath%zipext.lst
pkzip25 %ZIPFLAGS %BAKFILE *.*
endlocal
