#!/bin/bash
#--------------------------
# $Id$
#--------------------------
# Build all Wargamer executables
#
# Run from within Cygwin shell
# Requires Visual Studio Environment set up something like:

#-- Start of sample devenv.sh
# export VSINSTALLDIR=`cygpath "D:\msdev\Common7\IDE"`
# export VCINSTALLDIR=`cygpath "D:\msdev"`
# export FrameworkDir=`cygpath "C:\WINNT\Microsoft.NET\Framework"`
# export FrameworkVersion=v1.0.3705
# export FrameworkSDKDir=`cygpath "D:\msdev\FrameworkSDK"`
# 
# export DevEnvDir=$VSINSTALLDIR
# export MSVCDir=${VCINSTALLDIR}/Vc7
# 
# # This in cygwin format
# 
# export PATH=${DevEnvDir}:${MSVCDir}/bin:${VCINSTALLDIR}/Common7/Tools:${VCINSTALLDIR}/Common7/Tools/bin/prerelease:${VCINSTALLDIR}/Common7/Tools/bin:${FrameworkSDKDir}/bin:${FrameworkDir}/${FrameworkVersion}:$PATH
# 
# # These must be in Windows format!
# 
# MSVCDir_w=`cygpath -w "$MSVCDir"`
# FrameworkSDKDir_w=`cygpath -w "$FrameworkSDKDir"`
# 
# export INCLUDE="${MSVCDir_w}\atlmfc\include;${MSVCDir_w}\include;${MSVCDir_w}\PlatformSDK\include\prerelease;${MSVCDir_w}\PlatformSDK\include;${FrameworkSDKDir_w}\include;${INCLUDE}"
# 
# export LIB="${MSVCDir_w}\atlmfc\lib;${MSVCDir_w}\lib;${MSVCDir_w}\PlatformSDK\lib\prerelease;${MSVCDir_w}\PlatformSDK\lib;${FrameworkSDKDir_w}\lib;${LIB}"
#-- End of sample devenv.sh

#-- This should be set up elsewhere
WARGAMER_ROOT=~/sourceforge/wargamer/wargamer
cd ${WARGAMER_ROOT}
SOLUTION=`cygpath -w ${WARGAMER_ROOT}/wargamer.sln`
devenv ${SOLUTION} /build Debug /out debugBuild.log
devenv ${SOLUTION} /build Release /out releaseBuild.log
devenv ${SOLUTION} /build "Editor Debug" /out EditorDebugBuild.log
devenv ${SOLUTION} /build "Editor Release" /out EditorReleaseBuild.log

#--------------------------
# $Log$
#--------------------------
