setlocal
setdos /c%=^ /p%=&
unalias copy
cd %HOME
unset /q NOMAKELIB
wmake -h -f wargamer.mk %&
cd campgame
wmake -f campbat.mk
wmake -f campbatn.mk
cd %HOME
set NOMAKELIB=1
wmake -h -f campaign.mk %&
wmake -h -f battle.mk %&
wmake -h -f batedit.mk %&
unset /q NOMAKELIB
wmake -h EDITOR= NOLOG= -f campaign.mk %&
wmake -h NODEBUG= -f wargamer.mk %&
cd campgame
wmake NODEBUG= -f campbat.mk
wmake NODEBUG= -f campbatn.mk
cd %HOME
set NOMAKELIB=1
wmake -h NODEBUG= -f campaign.mk %&
wmake -h NODEBUG= -f battle.mk %&
wmake -h NODEBUG= -f batedit.mk %&
endlocal
