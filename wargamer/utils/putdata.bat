rem Update graphic and data files
setlocal

call synctime

set masterPath=\\Napoleon\vcs\datafiles
rem set masterPath=\\tigger\vcs\datafiles
rem set masterPath=v:\datafiles

pushd %masterpath
call dellist %masterPath\oldfiles.lst
popd

pushd %HOME
call dellist %masterPath\oldfiles.lst

unset /q NewVersion
IF "%_CMDPROC"=="4DOS" .AND. %_4VER ge 6.01 set NewVersion=1
IF "%_CMDPROC"=="4NT" .AND. %_4VER ge 3.01 set NewVersion=1

IFF DEFINED NewVersion THEN
  set COPYFLAGS=/e /u /s /z /h
ELSE
  set COPYFLAGS=/u /s /h
ENDIFF

rem Copy from local drive to Master

if not isdir %masterPath\artsrc\nap1813\BaseTerrain 	md %masterPath\artsrc\nap1813\BaseTerrain
if not isdir %masterPath\artsrc\nap1813\Buildings 		md %masterPath\artsrc\nap1813\Buildings
if not isdir %masterPath\artsrc\nap1813\MaskedTerrain 	md %masterPath\artsrc\nap1813\MaskedTerrain
if not isdir %masterPath\artsrc\nap1813\troops 			md %masterPath\artsrc\nap1813\troops
if not isdir %masterPath\game md %masterPath\game
if not isdir %masterPath\res md %masterPath\res
if not isdir %masterPath\splash md %masterPath\splash

*copy %COPYFLAGS artsrc\nap1813\BaseTerrain\*.bmp %masterPath\artsrc\nap1813\BaseTerrain
*copy %COPYFLAGS artsrc\nap1813\Buildings\*.bmp %masterPath\artsrc\nap1813\Buildings
*copy %COPYFLAGS artsrc\nap1813\MaskedTerrain\*.bmp %masterPath\artsrc\nap1813\MaskedTerrain
*copy %COPYFLAGS artsrc\nap1813\Troops\*.bmp %masterPath\artsrc\nap1813\Troops
*copy %COPYFLAGS game\*.bmp;*.wav;*.ttf;*.rtf;*.avi;*.spr %masterPath\game
*copy %COPYFLAGS res\*.bmp;*.cur %masterPath\res
*copy %COPYFLAGS splash\*.bmp;*.ico %masterPath\splash

popd

endlocal