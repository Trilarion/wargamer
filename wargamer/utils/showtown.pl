# Show information about a town in wargamer
#
# Requires campaign.asc in current directory

$fileName = "campaign.asc";
open(CAMPAIGNFILE, $fileName) || die("Can't open $fileName: $!");

foreach $line (<CAMPAIGNFILE>)
{
    if($line =~ /\[(.*)\]/)
    {
        $section = $1;
        # print "Section = $section\n";
        $count = 0;
    }
    elsif($line =~ /{/)
    {
    }
    elsif($line =~ /}/)
    {
        if($section =~ /Locations/)
        {
            $towns[$count] = $name;
            $townProv[$count] = $province;
            # print "towns[$count] = $name $towns[$count]\n";
            $count++;
        }
        elsif($section =~ /Provinces/)
        {
            $provinces[$count] = $name;
            # print "provinces[$count] = $name\n";
            $count++;
        }
    }
    else
    {
        # ($token, $value) = $line /\s*([^\s]+)\s+(.*)$/;

        # print("Token = $token, Value = $value\n");

        if($line =~ /\s*Name\s+\"(.*)\".*$/)
        {
            $name = $1;
        }

        if($line =~ /\s*Province\s+(\d*).*$/)
        {
            $province = $1;
        }


        if($section =~ /Locations/)
        {
        }
        elsif($section =~ /Provinces/)
        {
        }
        elsif($section =~ /Connections/)
        {
        }
    }
}
close(CAMPAIGNFILE);

$count = 0;
foreach (@towns)
{
   foreach (@ARGV)
   {
    # print "$_ $towns[$count]\n";

    if($towns[$count] =~ /$_/i)
    {
         $province = $provinces[$townProv[$count]];
         print "$towns[$count] is in $province\n";
    }
   }

   $count++;
}

# foreach (@townProv)
# {
#     print "TownProv $_\n";
# }
#
# foreach (@provinces)
# {
#     print "Province $_\n";
# }


