rem Backup files after a given date

iff "%1"=="" .OR. "%2"=="" then
 echo Usage:
 echo   Bakafter ddmm zipfile wildcards
 cancel -1
endiff

set WILD=%3
if "%WILD"=="" set WILD=*.*

set batpath=%@PATH[%@TRUENAME[%_BATCHNAME]]

set ZIPFLAGS=-add -rec -path -excl=@%batpath%zipext.lst

pkzip25 %ZIPFLAGS -after=%1 %2 %wild

