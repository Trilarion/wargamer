setlocal
setdos /c%=^ /p%=&
unalias copy
unalias cd
cdd %CDHOME
call setversion

Rem -------------
Rem Make Debug Versions
Rem -------------

unset /q RC_LANG
unset /q NOMAKELIB
wmake -h -f wargamer.mk %&
if ERRORLEVEL != 0 cancel %?
cd src\campgame
wmake -f campbat.mk
if ERRORLEVEL != 0 cancel %?
wmake -f campbatn.mk
if ERRORLEVEL != 0 cancel %?
cd %CDHOME
set NOMAKELIB=1
wmake -h -f campaign.mk %&
if ERRORLEVEL != 0 cancel %?
wmake -h -f battle.mk %&
if ERRORLEVEL != 0 cancel %?
unset /q NOMAKELIB

Rem -------------
Rem Make release Versions
Rem -------------

wmake -h NODEBUG= -f wargamer.mk %&
if ERRORLEVEL != 0 cancel %?
cd src\campgame
wmake NODEBUG= -f campbat.mk
if ERRORLEVEL != 0 cancel %?
wmake NODEBUG= -f campbatn.mk
if ERRORLEVEL != 0 cancel %?
cd %CDHOME
set NOMAKELIB=1
wmake -h NODEBUG= -f campaign.mk %&
if ERRORLEVEL != 0 cancel %?
wmake -h NODEBUG= -f battle.mk %&
if ERRORLEVEL != 0 cancel %?
unset /q NOMAKELIB

Rem -------------
Rem Make Splash Screen
Rem -------------

pushd splash
wmake -h NODEBUG=
if ERRORLEVEL != 0 cancel %?
*copy wg.exe ..\game
popd

Rem ---------------------
Rem Make German versions
Rem ---------------------

set RC_LANG=GERMAN
wmake -h -f wargamer.mk %&
if ERRORLEVEL != 0 cancel %?
wmake -h NODEBUG= -f wargamer.mk %&
if ERRORLEVEL != 0 cancel %?
pushd splash
wmake -h NODEBUG=
if ERRORLEVEL != 0 cancel %?
*copy wg_german.exe ..\game
popd
unset /q RC_LANG

Rem ---------------------
Rem Make English Only versions
Rem ---------------------

set RC_LANG=ENGLISH
wmake -h -f wargamer.mk %&
if ERRORLEVEL != 0 cancel %?
wmake -h NODEBUG= -f wargamer.mk %&
if ERRORLEVEL != 0 cancel %?

Rem -------------
Rem Make Editors
Rem -------------

unset /q RC_LANG

wmake -h EDITOR= NOLOG= -f campaign.mk %&
if ERRORLEVEL != 0 cancel %?

set NOMAKELIB=1
wmake -h -f batedit.mk %&
if ERRORLEVEL != 0 cancel %?
wmake -h NODEBUG= -f batedit.mk %&
if ERRORLEVEL != 0 cancel %?
unset /q NOMAKELIB



endlocal
