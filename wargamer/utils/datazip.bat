@echo on

rem Make a zip file of any data files that have changed since the
rem last time this was run

set masterPath=v:\datafiles
set batpath=%@PATH[%@TRUENAME[%_BATCHNAME]]
set destPath=f:\zipsForPaul

set dd=%_date
set dy=%@substr[%dd,0,2]%@substr[%dd,3,2]%@substr[%dd,6,2]
set datfile=%batpath%datazip.day
set bakfile=%destpath\wgdata_%@SUBSTR[%dd,0,2]%@SUBSTR[%dd,3,2].zip

set ZIPFLAGS=-add -dir -excl=@%batpath%zipext.lst -excl=*.spr

pushd %masterPath

iff exist %datfile then
  set lastdate=%@line[%datfile,0]
  pkzip25 -after=%lastdate %ZIPFLAGS %bakfile *.* 
 else
  pkzip25 %ZIPFLAGS %bakfile *.*
endiff

echo %dy >! %datfile

popd
