@echo off

IFF %# lt 2 THEN
    echo Usage: timecommand logfile commands ...
    quit -1
ELSE
    setlocal
    set LOGFILE=%1
    timer on >nul:
    @echo on
    %2&
    @echo off
    set RESULT=%?
    timer >>! %LOGFILE
    quit %RESULT
ENDIFF


