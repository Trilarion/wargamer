##############################################################################
# Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)	 #
# This Software is subject to the GNU General Public License.  				 #
# For License information see the file COPYING in the project root directory #
# For more information see the file README.									 #
##############################################################################

# Test Name Table program

NAME = TestNameTable
CDIR=c
HDIR=h
EXT=exe
ROOT=.
RDIR=res
# rcname=campaign.rc
# RC=1

!include $(ROOT)\wgpaths.mif

lnk_dependencies += testNameTable.mk

OBJS = testNameTable.obj

SYSLIBS = COMCTL32.LIB VFW32.LIB DSOUND.LIB DXGUID.LIB


# TARGETS += makedll
all :: $(TARGETS) .SYMBOLIC

!include system.mif

# !include exe95.mif
!include dosnt.mif


linkit : .PROCEDURE
	 @%append $(LNK) option stack=64k
	 @%append $(LNK) debug DWARF all
!ifdef NODEBUG
#	 @%append $(LNK) OPTION VFREMOVAL
	 @%append $(LNK) option ELIMINATE
!endif
	 @%append $(LNK) option SYMFILE


# makedll : .SYMBOLIC
# 	@!wmake $(__MAKEOPTS__) $(MFLAGS) -f nap1813.mk

