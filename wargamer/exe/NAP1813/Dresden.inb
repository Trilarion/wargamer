[DEFAULT]
{
MAP "frontend\battle_Dresden.bmp"
TEXT "Dresden"
DESCRIPTION "27 August 1813.
On the 19 August the Allied Army of Bohemia advanced from it's Bohemian bases and into Saxony, planning to capture Dresden (the main French supply center) whilst Napoleon was busy operating against Blucher in Silesia. Unfortunately for them Napoleon received word of this on the 22nd and rushed to support the Dresden garrison (commanded by St Cyr) with elements of his main army (including the Imperial Guard). On the 26th the Allies assaulted Dresden, and although some headway was made casualties were high and French resistance heavy. It became clear that not only was Napoleon present but also that the French were receiving continual reinforcements. Continual rain and the heavy mud did not help the Allies. Consequently the attack was broken off.
It might have been prudent for the Allies to have broken off during the night but instead they resolved to risk a second day of battle. During the night further French reinforcements arrived and it continued to rain. Most of the Allied troops spent a miserable night in the wet whilst the bulk of their enemies found shelter in Dresden. Meanwhile, to the east Vandamme was threatening to move against the Allied right and rear. Wurttemberg was holding him at bay - just - and he had to be sent reinforcements from the main Allied force at Dresden.
Both sides still have the opportunity of inflicting a decisive defeat on the enemy. For the Allies, the capture of Dresden would be a major blow that would unhinge the entire French position in Saxony. For the French, they have the main Allied army pinned in a somewhat suspect position with dangerously exposed flanks. From this position it is possible to achieve a truly 'Napoleonic' victory.

Objectives
The Allied aim is to capture Dresden. Perhaps more realistically the objective is to hold the position for a second day. Hopefully the French will be overly aggressive and a counterattack might achieve a local victory. Both flanks are vulnerable, especially the left which is isolated from the main army by the Weisseritz. This makes Plauen a vital position as this is the only real link the left has with the rest of the army.
The French must achieve the decisive battlefield success they have been looking for since April. Both Allied flanks can be turned. A purely tactical victory is possible by simply concentrating on the Allied left (around Rossthal and Doltschen), but for a more far reaching victory a successful assault against the Allied right and right center must be made (around Leuben and Leubnitz).

Aftermath
The French won a fairly comfortable victory and the Allied left was all but destroyed. The French effort to crush the Allied right was checked but with the possibility of Vandamme intervening the Allies decided to retreat back to Bohemia. The exhausted French let them go, waiting for Vandamme to 'finish the job'. He came close to cutting off the Allied retreat but a combination of accidents and Allied initiative and gallantry ended up with him being cut off from the rest of the French army, surrounded and all but destroyed in a 2 day battle around Kulm (29 - 30 August). Vandamme himself was taken prisoner. At a stroke the French success at Dresden was wiped out and Napoleon was back to square one."
FRENCH_KEY "F1 1st Cavalry XXX
F2 2nd XXX
F3 6th XXX
F4 14th XXX
F5 Young Guard
F6 Guard Cavalry
F7 Imperial Guard
"
ALLIED_KEY "A1 Austrian Left Wing
A2 Austrian Centre
A3 Austrian Cavalry
A4 2nd Prussian XXX
A5 Russian Right Wing
A6 Allied Reserve
A7 Austrian Artillery
"
}

