[DEFAULT]
{
MAP "frontend\August1813.bmp"
TEXT "NAPOLEON AGAINST EUROPE GRAND CAMPAIGN (August 1813)"
DESCRIPTION "August 10th 1813 to Autumn 1814.
Both sides have used the summer Armistice to rebuild their forces, raise fresh units and reorganise their armies. The Swedish army has mobilised and Austria has now joined the Allied cause. Denmark has sided with the French and their army is supporting Davout (F5) who has retaken Hamburg. So far the German kingdoms have remained loyal to Napoleon.
Both sides have now established regular supply lines and the main theatre of operations would appear to be Saxony, Silesia and the line of the Elbe.
The Allies have organised their forces in four main groups. The largest of these is the Army of Bohemia (A1) which is based in northern Bohemia. This force consists of the Austrian army supported by a substantial number of Russian formations (including the Guard - A2) and some Prussians. Holding the centre is the Army of Silesia commanded by Blucher (A3) which consists of Prussian and Russian formations. Protecting Berlin is the Army of the North (A5) which is still in the process of forming. This force will eventually consist of Prussians, Russians and Swedes. Finally, protecting Mecklenberg from Davout there is the force of Wallmoden (A6).
The French have largely adopted the strategy of the 'central position' with the bulk of the Grande Armee based around Dresden and Leipzig. These two cities are the main French supply sources (supported by Magdeberg). The French army has been reorganised during the Armistice and several new corps have arrived (or been formed) including the Polish 8th Corps under Ponitatowski and the 3rd and 4th Cavalry Corps. The fortresses of Stettin, Glogau and Kustrin continue to hold out against Allied siege operations.
The French must aim to comprehensively defeat one (or more) of the main Allied armies ranged against them whilst holding the others at bay. With Austria now in the war a swift victory is required to keep the support of Napoleon's German allies.
The Allies must hold the vital cities of Berlin and Breslau and bring their superiority of numbers to bear against the revitalised French army. Only then can a victorious march into France be contemplated. The Allies are perhaps vulnerable to being defeated before they can concentrate the French to being defeated by weight of numbers. Both sides must attack in some areas whilst defending in others.

Key areas and locations are
Saxony, one of Napoleons most important allies and the main base of the French army in Germany. Berlin, Prussian capital and a major supply source.
Breslau, a major Allied base area.
Hamburg, a major city, port and supply source
Prague, the supply source for Austrian forces operating in central Germany."
FRENCH_KEY "F1 Main French Army
F2 French Army of Berlin
F3 French Reserves
F4 Girard
F5 Davout and Danish Corps
F6 French Reinforcements
"
ALLIED_KEY "A1 Allied Army of Bohemia
A2 Main Russian Army
A3 Army of Silesia
A4 Prussian Army of Berlin
A5 Army of the North
A6 Wallmoden
A7 Austrian Reserve
A8 Russian Reinforcements
"
}

