[DEFAULT]
{
MAP "frontend\March1813.bmp"
TEXT "MARCH 1813 GRAND CAMPAIGN"
DESCRIPTION "March 1813 to Autumn 1814.
The remnants of the French army that invaded Russia the previous year straggled back into Germany cautiously pursued by the Russians. King Frederick William III of Prussia, pressured by his generals and ministers reluctantly declared war on France on 13 March. To his amazement his Prussian citizens greeted this with a wave of popular enthusiasm and thousands flocked to the colours to renew the struggle against France. The 'War of German Liberation' was about to begin.
Following the debacle in Russia Napoleon returned to France to raise and organise fresh forces. Eugene was left in command of what forces there were in Germany. Surrounded by a hostile populace and with reports of Allied armies advancing everywhere Eugene decided it was impossible to hold Berlin and the line of the Oder and pulled his forces back behind the Elbe. Hamburg had effectively revolted and the French were prevented from regaining control by the Danes who were attempting to negotiate with the Allies (these were to fail and Denmark was eventually to enter the war on the French side).
Further east French garrisons remain at the fortresses of Stettin, Kustrin, Glogau and Spandau. The Allies besieged all of these, mainly with '2nd line' units.
The Allies have two main armies in the field - Wittgenstein around Berlin and Blucher moving on Saxony from Breslau. The main Russian army is still organising at Kalisch. The Swedish army is still mobilising and is not yet ready to take the field.
The largest French force is with Eugene but there are units scattered throughout Germany (notably around Dresden). The main French army is still forming up behind the Rhine (mainly around Mainz).
The various German states have generally remained loyal to Napoleon, but some no doubt are considering changing sides to preserve their kingdoms, especially if the Allies continue to advance unchecked. Saxony in particular (one of the most important German kingdoms) seems to be wavering and might desert at any moment. So far Austria and Denmark have remained neutral.
As the campaign starts the major armies of both sides are still not ready to actively campaign. Consequently the situation is still very fluid. The Allies must strike into Germany and weaken French control over the various minor German kingdoms. They must also prepare themselves for the inevitable French counterattack. Having contained this and built up their forces they can defeat the French into Germany, encourage Napoleon's Allies to change sides (and encourage Austria to enter the war) and eventually march on the ultimate objective - Paris!
The French must check the Allied advance and prevent them from gaining control over any of their allies. When the rebuilt 'Grande Armee' is ready to campaign they must take the offensive and re-establish their hegemony over Germany, driving the Allies back and crushing Prussia once again. If they cannot then they face the prospect of fighting a desperate defensive campaign to keep the Allies from the gates of Paris, hoping that the fragile nature of the enemy coalition will eventually enable them to negotiate a 'peace with honour'.

Key areas and locations are
Saxony which is rich in supplies and is one of the most important German kingdoms.
Berlin, Prussian capital and a major supply source.
Breslau, a major Allied base area.
Hamburg, a major city and port, through which Prussia receives equipment from Britain. Denmark will not support France whilst the Allies hold this city.
Bohemia (if Austria enters the war).
The road from Mainz to Dresden that is the main French supply axis."
FRENCH_KEY "F1 Napoleon and Guard
F2 Eugene - Armee de l'Elbe
F3 Davout
F4 French reinforcements
F5 Grande Armee (assembling)
"
ALLIED_KEY "A1 Czar and Russian Reserve Army (at Kalisch)
A2 Blucher and Winzingerode
A3 Wittgenstein and Yorck
A4 Cossacks
A5 Swedish reinforcements
A6 Prussian III and IV corps
"
}

