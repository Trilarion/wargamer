[French]
{
MAP "frontend\Autumn1813.bmp"
TEXT "AUTOMNE 1813 (en route vers Leipzig)"
DESCRIPTION "de Septembre 1813 � la fin de l'automne 1813.
En six semaines de combat apr�s la fin de l'armistice, aucun des deux camps n'est arriv� � mettre son adversaire K.O. La pouss�e Fran�aise sur Breslau s'est achev�e dans un d�sastre avec la d�faite de Macdonald face � Bl�cher� la bataille de la Katzbach. L'offensive en direction de Berlin a �t� une premi�re fois contrari�e � Gross Beeren, pour �tre d�finitivement stopp�e � Dennewitz. Davout (toujours bas� � Hambourg) a �t� incapable d'appuyer efficacement les op�rations Fran�aises contre Berlin.
Cependant, la principale offensive Alli�e contre Dresde (la base d'approvisionnement principale de Napol�on en Allemagne) a aussi �t� d�faite lors d'une bataille de deux jours aux portes de la ville. En fait, l'Arm�e de Boh�me peut s'estim�e heureuse d'avoir pu �chapper � une destruction totale. Napol�on n'a pas pu tirer avantage de cette victoire � cause des d�faites inflig�es � ses subordonn�s � Gross Beeren et la Katzbach. La tentative de poursuite de l'Arm�e Alli�e par Vandamme a �t� an�antie � Kulm.
Avec l'�chec de leurs offensive, les Fran�ais a �tre repouss�s vers la Saxe. En fait, � l'exception de Davout (F2) la masse des forces Fran�aise (F1) est maintenant concentr�e autour de Dresde et Leipzig. Cela commence � placer un fardeau intol�rable sur le syst�me d'approvisionnement Fran�ais. Les renforts Fran�ais (y compris toujours plus de corps de cette cavalerie qui fait temps d�faut) sont en route pour le front (F4) mais pourraient ne pas arriver � temps, surtout maintenant que la fid�lit� des �tats Allemands envers Napol�on chancelle.
Pendant ce temps, les garnisons Fran�aise de Stettin, Glogau et Kustrin continuent de tenir, mais leur r�sistance faibli.
The Allies op�rent encore � partir de trois arm�es principales - L'Arm�e de Boh�me (A1) , qui s'est remise de sa d�faite � Dresde, est encore une fois pr�t � attaquer. L'Arm�e de Sil�sie (A2) p�n�tre en Saxe. L'Arm�e du Nord (A3 & A5) , laquelle ayant d�fendu Berlin avec succ�s, menace le nord de la Saxe Wallmoden (A4) continue � contenir Davout autour de Hambourg. Une force autrichienne suppl�mentaire (l'Arm�e du Danube, A8) s'est mise en marche pour envahir la Bavi�re. Approchant du front, se trouve une arm�e Russe fra�che (A6) et des d�tachements de cosaques (A7) se pr�parent � menacer � la fois les alli�s de Napol�on et les lignes d'approvisionnement Fran�aises.
La position Fran�aise est maintenant critique. Avec des alli�s vacillants et la situation p�rieuse de ses lignes de ravitaillement, Napol�on ne peut pas s'offrir une nouvelle d�faite. Les Fran�ais, cependant, conservent leur position centrale, et l'occasion de battre une ou plusieurs arm�es Alli�es s�par�ment avant que les Alli�s ne se soient regroup�s. Les Alli�s ont la victoire � leur port�e, mais s'ils se trompent ou font un faux mouvement ils seront alors vuln�rables � une contre-attaque soudaine qui pourrait renverser la situation rapidement.
La sc�ne est en place pour la plus grande bataille des Guerres Napol�oniennes - les trois jours de la 'Bataille des Nations' � Leipzig.
Key areas and locations are
Dresde et Leipzig. Les 'clefs' de la Saxe et les principaux centre de ravitaillement Fran�ais.
Magdeberg, un autre centre d'approvisionnement Fran�ais et la forteresse principale du centre de l'Allemagne.
Berlin, Breslau et Prague, les principaux centres de ravitaillement Alli�s.
Hamburg, une ville, un port et une source de ravitaillement importants
La Bavi�re, un royaume Allemand majeur, dont le support aux Fran�ais faibli quotidiennement"
FRENCH_KEY "F1 La Grande Arm�e
F2 Davout et les Danois
F3 Girard
F4 Les renforts Fran�ais
"
ALLIED_KEY "A1 L'Arm�e de Boh�me & l'Arm�e principale Russe
A2 L'Arm�e de Sil�sie
A3 L'Arm�e du Nord
A4 Wallmoden
A5 B�low et Taunentzien
A6 L'Arm�e Russe de r�serve 
A7 D�tachements cosaques
A8 L'Arm�e Autrichienne du Danube
"
}