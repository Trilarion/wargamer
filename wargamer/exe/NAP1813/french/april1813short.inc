[French]
{
MAP "frontend\GrandCampApril1813.bmp"
TEXT "Campagne de printemps Avril 1813"
DESCRIPTION "Du 31 mars 1813 au mois de juin 1813
Les Russes, maintenant rejoints par les Prussiens ont suivi en Allemagne les restes de l'arm�e Fran�aise qui a surv�cu � la retraite de Russie. Ces forces, sous les ordres du Prince Eug�ne, ont �t� incapables de contr�ler l'avance Alli�e et ont �t� oblig� de reculer jusqu'� la ville de Magdeberg (F2), emplacement vital pour les Fran�ais.
Les arm�es Alli�es sont maintenant divis�es en deux groupes principaux, Wittgenstein (A1) autour de Berlin et Bl�cher et Winzingerode (A2) � Dresde. Les cosaque ont lib�r� l'important port de Hambourg (A7) et de nouvelles unit�s Alli�es montent au front progressivement (A3, A5 et A6). Les unit�s de garnison Fran�aise dans les places fortes de l'est (Glogau, Stettin, Kustrin et Spandau), ont �t� incapables de ma�triser l'avance des troupes Alli�es, et sont actuellement assi�g�es par des unit�s de second ordre. Avec Berlin, Dresde et Hambourg sous contr�le Alli�, et avec beaucoup d'�tats allemands mineurs dont la loyaut� envers Napol�on vacille, les Alli�s ont une chance de ramener les Fran�ais derri�re le Rhin avant l'�t�.
Cependant, Napol�on a r�ussi � lever une arm�e fra�che et se pr�pare maintenant � entrer en campagne encore une fois (F1). Pendant ce temps, dans le nord, Davout (F2) a rassembl� une deuxi�me arm�e qui menance maintenant Hambourg.
La situation est encore fluide. Les Alli�s ont l'occasion de pousser leur avance jusque dans l'ouest de Allemagne et, peut-�tre, battre les Fran�ais avant qu'ils ne soient pleinement op�rationnels. Cela peut convaincre quelques-uns des �tats Allemands (Notamment La Saxe) de basculer. S'ils �chouent, les Alli�s devront engager une campagne d�fensive pour maintenir les Fran�ais dans une position d�licate jusqu'� ce que les renforts suppl�mentaires arrivent et que l'Autriche puissent �tre persuad�e de joindre ses forces  aux Alli�s dans la m�l�e.
Le Fran�ais doivent r�tablir rapidement leur contr�le sur les �tats Allemands et chercher les victoires d�cisives sur le champ de bataille, qui permettront � Napol�on de d�clarer 'je suis � nouveau Ma�tre de l'Europe.'

Les secteurs et les endroits principaux sont :
La Saxe qui est riche en approvisionnements et est l'un des royaumes Allemands les plus importants.
Berlin, capitale de la Prusse et une source de ravitaillement importante.
Breslau, une base Alli�e importante. 
Hambourg, une ville et un port importants, par lesquels la Prusse re�oit l'�quipement de la Grande-Bretagne. "
FRENCH_KEY "F1 L'Arm�e du Main
F2 L'Arm�e de l'Elbe
F3 La 32�me Arm�e
F4 Les renforts Fran�ais
F5 Vandamme
"
ALLIED_KEY "A1 Wittgenstein
A2 Le Tzar et l'arm�e Alli�e
A3 Les renforts Russes
A4 L'arm�e Su�doise
A5 Les r�serves Russes
A6 Les renforts Prussiens
A7 Wallmoden
"
}

