[French]
{
MAP "frontend\GrandCampApril1813.bmp"
TEXT "GRANDE CAMPAGNE Avril 1813"
DESCRIPTION "du 31 mars 1813 � l'automne 1814.
L'Arm�e fran�aise ne s'est pas encore remise de sa d�sastreuse campagne de Russie. Napol�on est revenu en France lever et organiser des forces fra�ches, en laissant les survivants de 1812 en Allemagne sous les ordres du Prince Eug�ne (Arm�e de l'Elbe).
Sous la pression de l'avance des Russes et des Prussiens le Prince Eug�ne (F2) a �t� forc� d'abandonner Berlin et de repasser l'Elbe, en basant sa d�fense sur la forteresse vitale de Magdeberg.
Les Alli�s avancent avec deux groupes principaux, Wittgenstein (A1) autour de Berlin et Bl�cher et Winzingerode (A2) � Dresde. Dans le nord des unit�s de Cosaques (A7), support�es par les soul�vements populaires ont lib�r� des Fran�ais le port vital de Hambourg. Pendant ce temps, l'Arm�e Russe principale se pr�pare � quitter Kalisch pour entrer en Prusse. Les forces principales alli�es ont mis hors circuit les forteresses de l'est tenues par les Fran�ais (Glogau, Stettin, Kustrin et Spandau), laissant � plusieurs unit�s de second ordre le soins de les assi�ger. L'arm�e su�doise (A4) est encore mobilising et n'est pas cependant pr�t prendre le champ.
La plupart des multiples �tats allemands sont rest�s g�n�ralement loyaux � Napol�on, mais quelques-uns n'aurait aucun scrupule � consid�rer un renversement d'alliance pour conserver leurs royaumes, surtout si les Alli�s continuent d'avancer sans rencontrer de r�sistance. La Saxe en particulier (un des plus importants royaumes d'Allemagne) para�t vaciller et peut abandonner la France � tout moment. L'Autriche si lointaine et le Danemark sont rest�s neutres.
La campagne de Saxe d�bute donc avec un Napol�on se pr�parant � p�n�trer en Allemagne � la t�te de l'Armee du Main (F1) r�cemment form�e, tandis que Davout forme une autre arm�e dans le nord (F3).
La situation est encore tr�s fluide, les forces en pr�sence des deux c�t�s ne sont pas encore organis�es compl�tement et les lignes de ravitaillement n'ont pas compl�tement �t� �tablies.
Les Alli�s doivent avoir l'intention de tenir les Fran�ais r�surgents � distance, quitte � c�der du terrain pour gagner le temps n�cessaire � la mobilisation compl�te de leurs forces. Alors ils pourront balayer l'Allemagne et sur marcher sur l'objectif ultime - Paris!
Le Fran�ais doivent r�tablir leur h�g�monie sur l'Allemagne, en repoussant les Alli�s et en �crasant la Prusse encore une fois. Si ils ne le peuvent pas, alors ils devront faire face � la perspective d'une lutte d�fensive d�sesp�r�e pour emp�cher les Alli�s de prendre Paris, en esp�rant que la nature fragile de la coalition de l'ennemi leur permettra finalement de n�gocier une 'paix honnorable.'

Les secteurs et les endroits principaux sont :
La Saxe qui est riche en approvisionnements et est l'un des royaumes Allemands les plus importants.
Berlin, capitale de la Prusse et une source de ravitaillement importante.
Breslau, une base Alli�e importante. 
Hambourg, une ville et un port importants, par lesquels la Prusse re�oit l'�quipement de la Grande-Bretagne. Le Danemark ne soutiendra pas la France tant que les alli�s tiennent cette ville.
La Boh�me (si l'Autriche entre en guerre).
La route de Mayence vers Dresde qui est l'axe principal d'approvisionnement de Fran�ais."
FRENCH_KEY "F1 L'Arm�e du Main
F2 L'Arm�e de l'Elbe
F3 La 32�me Arm�e
F4 Les renforts Fran�ais
F5 Vandamme
"
ALLIED_KEY "A1 Wittgenstein
A2 Le Tzar et l'arm�e Alli�e
A3 Les renforts Russes
A4 L'arm�e Su�doise
A5 Les r�serves Russes
A6 Les renforts Prussiens
A7 Wallmoden
"
}
