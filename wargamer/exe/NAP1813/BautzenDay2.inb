[DEFAULT]
{
MAP "frontend\battle_Bautzen2.bmp"
TEXT "Bautzen (Day 2)"
DESCRIPTION "22nd May 1813.
Although narrowly defeated at Lutzen the Allies decided to risk another battle. They therefore halted their retreat at Bautzen and deployed their army in a strong defensive position, hoping they would be able to shatter the French army as it attacked them.
On May 21 Napoleon, his army not yet fully concentrated, launched a probing attack. This was to achieve two objectives - Firstly to get his army across the Spree and in position to launch an all out assault on the 22nd. Secondly to pin the Allied army, thereby allowing Ney (still marching to the battlefield) to crush the Allied right, cutting their communications with their main supply base at Breslau. By the end of the day the Allied 1st line had been driven back and preparations were underway to assault the main Allied position which ran from Reischen - Baschutz - Litten - Gleina.
The Allied position was very strong, much of it on heights overlooking the French and bolstered by the many towns and villages in the area. There were however two problems with the Allied position, it was rather too long for the number of troops available and was vulnerable to a turning movement (especially one from the north).

Objectives
The French must either crush the Allied army or force them into an untenable position. This could be achieved by crushing the Allied center around Baschutz and Litten or by rolling up a flank. The Allied right flank is probably the most vulnerable, and French success here would cut the Allies off from their base at Breslau, forcing them into the Bohemian mountains and pinning them against the Austrian frontier. Such an attack would have to capture Wurschen and Preittitz at the very least.
The Allies must hold the line Reischen - Gleina and aim to inflict significant casualties on the French, counterattacking where possible.

Aftermath
After a day of intense combat with French attacking every section of the enemy line the Allies retreated when they realised the French were about to roll up their right flank. This withdrawal was skilfully executed and the Allied army remained intact. The French were too battered to take advantage of their narrow victory. In later years Ney was blamed for the French failure to win a decisive victory due to getting drawn into the bitter fighting around Bluchers position (the Allied center - right) instead of swinging completely around the Allied flank. However Napoleon's plan was possibly too grandiose for his inexperienced army and Blucher had been inflicting heavy losses on Bertrand before Ney intervened.
Following Bautzen the French continued to advance but both armies were exhausted and the French supply system and almost completely broken down. Consequently an Armistice was concluded on 4 June 1813."
FRENCH_KEY "F1 Imperial Guard
F2 Guard Cavalry
F3 1st Cavalry Corps
F4 12th Corps
F5 11th Corps
F6 6th Corps
F7 4th Corps
F8 3rd Corps
F9 5th Corps
F10 French Reinforcements
"
ALLIED_KEY "A1 Miloradovich
A2 Gortschakov
A3 Reserve
A4 2nd Prussian Corps
A5 1st Prussian Corps
A6 Kleist
A7 Barclay
A8 Allied Light Cavalry
"
}

