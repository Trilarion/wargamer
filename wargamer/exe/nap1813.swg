;-----------------------------------------------------------
; Scenario File for Napoleon 1813 Campaign Game
;
; Copyright (C) 1996, Steven Green.
; All rights reserved.
;
;-----------------------------------------------------------
; Notes:
;
; * Any text following a semicolon ';' is ignored
; * The first word on a line is a keyword
; * Keywords may be followed by '=' or spaces.
; * To allow spaces or semicolons in names, enclose them in "quotes"
; * Parameters may be seperated by spaces or commas ','
; * Spaces may be freely added to align text
; * The order of the keywords is not important
;
;-----------------------------------------------------------

; Default or unknown language uses this section
[default]
;--- Uncommment to force language to German
; language = German       ; folder within nap1813 where language specific files are stored


;-----------------------------------------------------------
; Where Data for this scenario is stored
; If it does not have a path name then it is relative to the
; application's folder.
;
; Files are found by looking in the following order:
;       folder\language\filename
;       folder\filename
;       filename
;-----------------------------------------------------------

folder = nap1813		; Where data for this scenario is stored
animationfolder = nap1813\animations  ; Where scenario .AVI files are stored

;-----------------------------------------------------------
; Where graphics files can be found
; Pathnames are relative to the scenario folder.
; If the path begins with \ then it is relative to the
; application's folder.
; e.g. myfile.exe : is found in currentdirectory\nap1813\myfile.exe
;      \myfile.exe : is found in currentDirectory\myfile.exe
;-----------------------------------------------------------

tinymap        = C_map128.bmp		; Locator Map
palette        = cmapPalette.bmp

campaignMap = C_map512.bmp
campaignMap = C_map640.bmp
campaignMap = C_map1024.bmp
campaignMap = C_map2048.bmp
campaignMap = C_map4096.bmp

campaignEditMap = C_map512.bmp
campaignEditMap = C_map640.bmp
campaignEditMap = C_map1024.bmp
campaignEditMap = C_map2048.bmp
campaignEditMap = C_map4096.bmp

scenarioResDLL = nap1813res.dll	        ; Resource File DLL File
battleResDLL = batres.dll	        ; Resource File DLL File

;-------------------------------------------------------------
; Where help files can be found either in application folder
; or CD-ROM
;-------------------------------------------------------------

helpFile       = \Help\Wg95Help.hlp  ;  Help files
HistoryFile    = nap1813.hlp         ; Historical Reference

;-------------------------------------------------------------
; Data referring to Campaign Map
;
; mapSize is in miles
; mapScale is used as a reference for mapPoints
; mapPoints are the 4 corners of the physical map.
; the 1st point is where 0,0 is within the graphic
; the 2nd is mapSize,0
;
; Physical Height is made using the aspect ratio of the first
; campaign map.

MapSize  = 700,583 ; 700 	; Map is 700 Miles across and 583 miles down
MapScale = 4096,3072 		; coordinate system that mapPoints belong to
MapPoint = 35,97		; Top Left coordinate
MapPoint = 4041,67		; Top Right
MapPoint = 33,3022		; Bottom Left
MapPoint = 4051,3012		; Bottom Right
MapAngle = 315			; Which direction the viewpoint is facing in degrees

;-----------------------------------------------------------
; Define some colours
; Usage: Colour colorName r,g,b
;
; These are used symbolically when defining Nations and Sides
; and are used as a base colour when drawing things related to the
; side or nation on the map or in info windows.

Colour Red 	255,   0,   0
Colour Blue 	  0,   0, 255
Colour Orange 	255, 128,   0
Colour Black 	  0,   0,   0
Colour Purple 	128,   0, 128
Colour Brown 	128, 128,   0
Colour LightBrown 189, 165, 123    ; The following four colors are
Colour OffWhite 255, 247, 215      ; for Window Borders
Colour Tan  230, 214, 123          ;
Colour DarkBrown 115, 90, 66       ;

Colour MapInsert 234,216,156	; Background colour for map inserts
Colour MenuText  234,216,156	; Text Colour for Menus
Colour MenuHilite 255,128,0	; Highlighted Text Colour for menus
Colour CaptionText 115,90,66	; Colour of Text in Caption bar

;-----------------------------------------------------------
; Define Border colours
; Usage: BorderColour <colour>
; At the moment there are 4 border colors
; Note: If more are added BorderColour_MAX needs to be redefined in gamedefs.h

BorderColour LightBrown
BorderColour OffWhite
BorderColour Tan
BorderColour DarkBrown

;-----------------------------------------------------------
; Define Sides
; Usage: Side sideName,<colour>
;
; Note that the first side must always be neutral
; There must be exactly 2 sides in addition to neutral

Side Neutral, Brown
Side French,  Blue
Side Allied,  Red

;-----------------------------------------------------------
; Define Nation Types
; Usage NationType type
;
; There are 3 Nation Types

NationType MajorNation
NationType MinorNation
NationType GenericNation

;-----------------------------------------------------------
; Define Nations
; Usage: Nation nationName, nationAdjective, <side>,<colour>
;
; There may be a maximum of 31 Nationalities

Nation France,       French,             French,  Blue,   MajorNation,   FrenchFlags, FrenchFlagAnim
Nation Prussia,      Prussian,           Allied,  Red,    MajorNation,   PrussianFlags, PrussiaFlagAnim
Nation Austria,      Austrian,           Neutral, Brown,  MajorNation,   AustrianFlags, AustriaFlagAnim
Nation Sweden,       Swedish,            Allied,  Red,    MajorNation,   SwedishFlags, SwedenFlagAnim
Nation Russia,       Russian,            Allied,  Red,    MajorNation,   RussianFlags, RussiaFlagAnim
Nation Denmark,      Danish,             Neutral, Brown,  MinorNation,   DanishFlags, DenmarkFlagAnim
Nation Italy,        Italian,            French,  Blue,   MinorNation,   ItalianFlags, ItalyFlagAnim
Nation Bavaria,      Bavarian,           French,  Blue,   MinorNation,   BarvarianFlags, BavariaFlagAnim
Nation Saxony,       Saxon,              French,  Blue,   MinorNation,   SaxonFlags, SaxonyFlagAnim
Nation Wurttemberg,  Wurttemberger,      French,  Blue,   MinorNation,   WurrtenburgerFlags, WurttembergFlagAnim
Nation Westphalia,   Westphalian,        French,  Blue,   MinorNation,   WestphalianFlags, WestphaliaFlagAnim
Nation Minor-French, Minor-German,       French,  Blue, GenericNation, MinorFrenchFlags, FrenchFlagAnim
Nation Minor-Allied, Minor-German,       Allied,  Red,  GenericNation, MinorAlliedFlags, PrussiaFlagAnim
Nation Poland,       Polish,             French,  Blue,   MinorNation,   PolishFlags, PolandFlagAnim

;-----------------------------------------------------------
; Default Portraits for each side

Portrait French, Eagle
Portrait Allied, IronCross

;-----------------------------------------------------------
; Define Font types
; Usage  FontType  faceName, fileName
;
; These must match up with the FontStyle enumeration

FontType  fonts\Normal.TTF		; Normal
FontType  fonts\Italic.TTF		; Italic
FontType  fonts\Bold.TTF		; Bold
FontType  fonts\BoldItalic.TTF		; Bold Italic
FontType  fonts\Small.TTF		; Small
FontType  fonts\SmallItalic.TTF		; SmallItalic
FontType  fonts\SmallBold.TTF		; Small Bold
FontType  fonts\SmallBoldItalic.TTF	; Small Bold Italic
FontType  fonts\Decorative.TTF		; Decorative

FontType  fonts\coprgtb.TTF		; This is used frequently

;------------------------------------------------------------
; BattleGame Display Stuff
; HexPixelX, HexPixelY = Size that Hex Graphics have been drawn
; TroopPixel    = Width of Hex that troops are designed to be drawn onto
; FXPixel       = Width of Hex that Effects are designed to be drawn onto
; BuildingPixel = Width of Hex that Buildings are designed for
;
; If TroopPixel, FXPixel or BuildingPixel are omitted then they
; default to HexPixelX


HexPixelX     =  82
HexPixelY     =  44
TroopPixel    =  120      ; Troops must be drawn smaller than other graphics
FXPixel       =  120
BuildingPixel =  82

;------------------------------------------------------------
; TimeControl settings
;
; ...TimeRatioMinimum 			= slowest game-speed multiplier
; ...TimeRatioMinimum 			= fastest game-speed multiplier
; ...TimeRatioResolution		= how many 'steps' are available between slowest & fastest
; ...Advance...Slow / Medium / Fast	= how much to advance time
;					  NB: (in Campaign this is days, in Battle this is minutes)


CampaignTimeRatioMinimum	=	10          ; Seconds Per Day
CampaignTimeRatioMaximum	=	300
CampaignTimeRatioResolution =	20          ; Number of settings
CampaignAdvanceTimeSlow		=	1           ; Days to advance
CampaignAdvanceTimeMedium	=	7
CampaignAdvanceTimeFast		=	14

BattleTimeRatioMinimum		=	8           ; real time to game time
BattleTimeRatioMaximum		=	24
BattleTimeRatioResolution	=	4           ; number of settings
BattleTimeRatioDefault      =   8
BattleAdvanceTimeSlow		=	5           ; advance 5 minutes
BattleAdvanceTimeMedium		=	15
BattleAdvanceTimeFast		=	30


;------------------------------------------------------------
; German uses this section
; Only contains settings that are different than default

[German]
; language = german       ; folder within nap1813 where language specific files are stored

Side "Neutral", Brown
Side "Frankreich",  Blue
Side "Alliierte",  Red

Nation "Frankreich",          "Frankreich",       "Frankreich", Blue,  MajorNation,   FrenchFlags, FrenchFlagAnim
Nation "Preu�en",             "Preu�en",          "Alliierte",  Red,   MajorNation,   PrussianFlags, PrussiaFlagAnim
Nation "�sterreich",          "�sterreich",       "Neutral",    Brown, MajorNation,   AustrianFlags, AustriaFlagAnim
Nation "Schweden",            "Schweden",         "Alliierte",  Red,   MajorNation,   SwedishFlags, SwedenFlagAnim
Nation "Ru�land",             "Ru�land",          "Alliierte",  Red,   MajorNation,   RussianFlags, RussiaFlagAnim
Nation "D�nemark",            "D�nemark",         "Neutral",    Brown, MinorNation,   DanishFlags, DenmarkFlagAnim
Nation "Italien",             "Italien",          "Frankreich", Blue,  MinorNation,   ItalianFlags, ItalyFlagAnim
Nation "Bayern",              "Bayern",           "Frankreich", Blue,  MinorNation,   BarvarianFlags, BavariaFlagAnim
Nation "Sachsen",             "Sachsen",          "Frankreich", Blue,  MinorNation,   SaxonFlags, SaxonyFlagAnim
Nation "W�rttemberg",         "W�rttemberg",      "Frankreich", Blue,  MinorNation,   WurrtenburgerFlags, WurttembergFlagAnim
Nation "Westfalen",           "Westfalen",        "Frankreich", Blue,  MinorNation,   WestphalianFlags, WestphaliaFlagAnim
Nation "Kleine Franzosen",    "Kleine Deutsche",  "Frankreich", Blue,  GenericNation, MinorFrenchFlags, FrenchFlagAnim
Nation "Kleine Alliierte",    "Kleine Deutsche",  "Alliierte",  Red,   GenericNation, MinorAlliedFlags, PrussiaFlagAnim
Nation "Polen",               "Polen",            "Frankreich", Blue,  MinorNation,   PolishFlags, PolandFlagAnim

Portrait Frankreich, Eagle
Portrait Alliierte, IronCross
