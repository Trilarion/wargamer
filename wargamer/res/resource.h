/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
#ifndef RESOURCE_H
#define RESOURCE_H
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1995, Steven Morle-Green, All Rights Reserved
 *----------------------------------------------------------------------
 *
 *      TestWindows header file used for accessing items in resource file
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.3  2001/06/26 19:06:20  greenius
 * Resources compile... and get someway into battle game before asserting
 *
 * Revision 1.2  2001/06/18 20:22:23  greenius
 * runBattle Project added, along with resources.  Compiles and almost runs!
 *
 * Revision 1.1  2001/06/13 08:22:48  greenius
 * Initial Import
 *
 *
 *----------------------------------------------------------------------
 */

#ifdef __cplusplus
extern "C" {
#endif

#include "res_str.h"            // String Definitions (IDS_ TTS_ SWS_)

/*
 * Icons
 */

//convRes: #define ICON_WARGAMER            1
#define ICON_WARGAMER            1

// #if defined(CAMPAIGN)
//convRes: #define ICON_MAPWIN                      2
#define ICON_MAPWIN                      2
// #endif


/*
 * Bitmaps
 */

// #if defined(CAMPAIGN)
// #define BM_TOOLBAR             1
// #endif

/*
 * Cursors
 */

//convRes: #define CURS_ZOOM_COLOR 200
#define CURS_ZOOM_COLOR 200
//convRes: #define CURS_ZOOM_MONO  201
#define CURS_ZOOM_MONO  201
//convRes: #define CURS_DRAG_COLOR       202
#define CURS_DRAG_COLOR       202
//convRes: #define CURS_DRAG_MONO       203
#define CURS_DRAG_MONO       203

/*
 * Menus
 */


// #if defined(CAMPAIGN)
//convRes: #define MENU_START                        1
#define MENU_START                        1
//convRes: #define MENU_TOWNEDITPOPUP        2
#define MENU_TOWNEDITPOPUP        2
//convRes: #define MENU_UNITORDERPOPUP  3
#define MENU_UNITORDERPOPUP  3
//convRes: #define MENU_FINDTOWNPOPUP   4
#define MENU_FINDTOWNPOPUP   4
//convRes: #define MENU_FINDLEADERPOPUP 5
#define MENU_FINDLEADERPOPUP 5
//convRes: #define MENU_OBEDITPOPUP     6
#define MENU_OBEDITPOPUP     6
//convRes: #define MENU_DELETESPPOPUP   7
#define MENU_DELETESPPOPUP   7
//convRes: #define MENU_OBEDITPOPUP2 13
#define MENU_OBEDITPOPUP2 13
// #endif
//convRes: #define MENU_STARTSCREEN     8
#define MENU_STARTSCREEN     8
// #if defined(CAMPAIGN)
//convRes: #define MENU_EDITALLEGIANCE  9
#define MENU_EDITALLEGIANCE  9
// #define MENU_TOWNORDERPOPUP 10
// #endif
// #if defined(BATTLE)
//convRes: #define MENU_BATTLE                       11
#define MENU_BATTLE                       11
//convRes: #define MENU_BUNITORDERPOPUP 12
#define MENU_BUNITORDERPOPUP 12
// #endif


/*
 * Menu Commands
 */

// generic Menu commands used by all parts of the game

//convRes: #define IDM_FIRST                                       100
#define IDM_FIRST                                       100

//convRes: #define IDM_LOADGAME                            IDM_FIRST+0
#define IDM_LOADGAME                            100
//convRes: #define IDM_SAVEGAME                            IDM_FIRST+1
#define IDM_SAVEGAME                            101
//convRes: #define IDM_ABANDONGAME                     IDM_FIRST+2
#define IDM_ABANDONGAME                     102
//convRes: #define IDM_EXIT                                        IDM_FIRST+3
#define IDM_EXIT                                        103
//convRes: #define IDM_HELP_CONTENTS                   IDM_FIRST+4
#define IDM_HELP_CONTENTS                   104
//convRes: #define IDM_ABOUT                                       IDM_FIRST+5
#define IDM_ABOUT                                       105
//convRes: #define IDM_HELP_HISTORY                    IDM_FIRST+6
#define IDM_HELP_HISTORY                    106
//convRes: #define IDM_DEBUG                                       IDM_FIRST+7
#define IDM_DEBUG                                       107
//convRes: #define IDM_OPTIONS                             IDM_FIRST+8
#define IDM_OPTIONS                             108
//convRes: #define IDM_VOLUMECONTROL               IDM_FIRST+9
#define IDM_VOLUMECONTROL               109
//convRes: #define IDM_WEB                         IDM_FIRST+10
#define IDM_WEB                         110

//convRes: #define IDM_SPECIFIC                IDM_FIRST+20
#define IDM_SPECIFIC                120

// #if defined(CAMPAIGN)

#ifdef DEBUG
//convRes: #define IDM_LOADVICTORYSCREEN_0     IDM_SPECIFIC+10
#define IDM_LOADVICTORYSCREEN_0     130
//convRes: #define IDM_LOADVICTORYSCREEN_1     IDM_SPECIFIC+11
#define IDM_LOADVICTORYSCREEN_1     131
//convRes: #define IDM_LOADVICTORYSCREEN_DRAW  IDM_SPECIFIC+12
#define IDM_LOADVICTORYSCREEN_DRAW  132
#endif  // DEBUG

//convRes: #define IDM_GRID                                        IDM_SPECIFIC+13
#define IDM_GRID                                        133
//convRes: #define IDM_ZOOMIN                                  IDM_SPECIFIC+14
#define IDM_ZOOMIN                                  134
//convRes: #define IDM_ZOOMOUT                                 IDM_SPECIFIC+15
#define IDM_ZOOMOUT                                 135
//convRes: #define IDM_SHOWALLORDERS           IDM_SPECIFIC+16
#define IDM_SHOWALLORDERS           136
//convRes: #define IDM_SHOWALLATTRIBS          IDM_SPECIFIC+17
#define IDM_SHOWALLATTRIBS          137


//convRes: #define IDM_INSTANTORDERS               IDM_SPECIFIC+18
#define IDM_INSTANTORDERS               138
//convRes: #define IDM_INSTANTMOVE                 IDM_SPECIFIC+19
#define IDM_INSTANTMOVE                 139
//convRes: #define IDM_QUICKMOVE                   IDM_SPECIFIC+20
#define IDM_QUICKMOVE                   140
//convRes: #define IDM_FULLROUTE                   IDM_SPECIFIC+21
#define IDM_FULLROUTE                   141
//convRes: #define IDM_COLORCURSOR                 IDM_SPECIFIC+22
#define IDM_COLORCURSOR                 142
//convRes: #define IDM_PLAYERSETTINGS          IDM_SPECIFIC+23
#define IDM_PLAYERSETTINGS          143
//convRes: #define IDM_GAMEOPTIONS             IDM_SPECIFIC+24
#define IDM_GAMEOPTIONS             144

//convRes: #define IDM_TINYMAPWINDOW               IDM_SPECIFIC+30
#define IDM_TINYMAPWINDOW               150
//convRes: #define IDM_PALETTEWINDOW               IDM_SPECIFIC+31
#define IDM_PALETTEWINDOW               151
//convRes: #define IDM_INFOWINDOW                  IDM_SPECIFIC+32
#define IDM_INFOWINDOW                  152
//convRes: #define IDM_CAMPAIGNCLOCK               IDM_SPECIFIC+33
#define IDM_CAMPAIGNCLOCK               153
//convRes: #define IDM_MESSAGEWINDOW               IDM_SPECIFIC+34
#define IDM_MESSAGEWINDOW               154
//convRes: #define IDM_WEATHERWINDOW           IDM_SPECIFIC+35
#define IDM_WEATHERWINDOW           155

//convRes: #define IDM_SHOWSUPPLY           IDM_SPECIFIC+36
#define IDM_SHOWSUPPLY           156
//convRes: #define IDM_SHOWCHOKEPOINTS      IDM_SPECIFIC+37
#define IDM_SHOWCHOKEPOINTS      157
//convRes: #define IDM_SHOWSUPPLYLINES      IDM_SPECIFIC+38
#define IDM_SHOWSUPPLYLINES      158

#if defined(CUSTOMIZE)
//convRes: #define IDM_LOADWORLD                   IDM_SPECIFIC+40
#define IDM_LOADWORLD                   160
//convRes: #define IDM_SAVEWORLD                   IDM_SPECIFIC+41
#define IDM_SAVEWORLD                   161
//convRes: #define IDM_EDIT_TOWNS                  IDM_SPECIFIC+42
#define IDM_EDIT_TOWNS                  162
//convRes: #define IDM_EDIT_PROVINCES              IDM_SPECIFIC+43
#define IDM_EDIT_PROVINCES              163
//convRes: #define IDM_EDIT_CONNECTIONS        IDM_SPECIFIC+44
#define IDM_EDIT_CONNECTIONS        164
//convRes: #define IDM_EDIT_OB                 IDM_SPECIFIC+45
#define IDM_EDIT_OB                 165
//convRes: #define IDM_EDIT_DATE                   IDM_SPECIFIC+46
#define IDM_EDIT_DATE                   166
//convRes: #define IDM_NEW_OBJECT                  IDM_SPECIFIC+47
#define IDM_NEW_OBJECT                  167
//convRes: #define IDM_EDIT_NOTHING                IDM_SPECIFIC+48
#define IDM_EDIT_NOTHING                168
//convRes: #define IDM_REMOVE_TOWNS                IDM_SPECIFIC+49
#define IDM_REMOVE_TOWNS                169
//convRes: #define IDM_REMOVE_UNITS                IDM_SPECIFIC+50
#define IDM_REMOVE_UNITS                170
//convRes: #define IDM_EDITINFO                IDM_SPECIFIC+51
#define IDM_EDITINFO                171
//convRes: #define IDM_EDITCONDITIONS          IDM_SPECIFIC+52
#define IDM_EDITCONDITIONS          172
//convRes: #define IDM_EDITALLEGIANCE          IDM_SPECIFIC+53
#define IDM_EDITALLEGIANCE          173
#ifdef CUSTOMIZE
//convRes: #define IDM_DATACHECK               IDM_SPECIFIC+55
#define IDM_DATACHECK               175
#endif  // DEBUG
#endif  //CUSTOMIZE

#ifdef CUSTOMIZE
//convRes: #define IDM_PRINTMAP                IDM_SPECIFIC+54
#define IDM_PRINTMAP                174
#endif

//convRes: #define IDM_OB                                  IDM_SPECIFIC+60
#define IDM_OB                                  180
//convRes: #define IDM_FINDTOWN                IDM_SPECIFIC+61
#define IDM_FINDTOWN                181
//convRes: #define IDM_FINDLEADER              IDM_SPECIFIC+62
#define IDM_FINDLEADER              182
//convRes: #define IDM_ILEADERS                IDM_SPECIFIC+63
#define IDM_ILEADERS                183
//convRes: #define IDM_REPOPOOL                IDM_SPECIFIC+64
#define IDM_REPOPOOL                184


//convRes: #define IDM_MENU                    IDM_SPECIFIC+80
#define IDM_MENU                    200

// #define IDM_RESOURCEMODE            IDM_SPECIFIC+82
// #define IDM_UNITMODE                IDM_SPECIFIC+83

//convRes: #define IDM_LAST                                IDM_SPECIFIC+100
#define IDM_LAST                                220

// id's for unitorderpopup menu
//convRes: #define IDM_UTB_FIRST           400
#define IDM_UTB_FIRST           400
//convRes: #define IDM_UTB_INFO            IDM_UTB_FIRST
#define IDM_UTB_INFO            400
//convRes: #define IDM_UTB_REORG           IDM_UTB_FIRST + 1
#define IDM_UTB_REORG           401
//convRes: #define IDM_UTB_ORDERSWIN       IDM_UTB_FIRST + 2
#define IDM_UTB_ORDERSWIN       402
//convRes: #define IDM_UTB_MOVETO          IDM_UTB_FIRST + 3
#define IDM_UTB_MOVETO          403
//convRes: #define IDM_UTB_HOLD            IDM_UTB_FIRST + 4
#define IDM_UTB_HOLD            404
//convRes: #define IDM_UTB_RESTRALLY       IDM_UTB_FIRST + 5
#define IDM_UTB_RESTRALLY       405
//convRes: #define IDM_UTB_DETACHLEADER    IDM_UTB_FIRST + 6
#define IDM_UTB_DETACHLEADER    406
//convRes: #define IDM_UTB_GARRISON        IDM_UTB_FIRST + 7
#define IDM_UTB_GARRISON        407
//convRes: #define IDM_UTB_INSTALLSUPPLY   IDM_UTB_FIRST + 8
#define IDM_UTB_INSTALLSUPPLY   408
//convRes: #define IDM_UTB_UPGRADEFORT     IDM_UTB_FIRST + 9
#define IDM_UTB_UPGRADEFORT     409
//convRes: #define IDM_UTB_MARCHTO         IDM_UTB_FIRST + 10
#define IDM_UTB_MARCHTO         410
//convRes: #define IDM_UTB_PURSUE          IDM_UTB_FIRST + 11
#define IDM_UTB_PURSUE          411
//convRes: #define IDM_UTB_SIEGEACTIVE     IDM_UTB_FIRST + 12
#define IDM_UTB_SIEGEACTIVE     412
//convRes: #define IDM_UTB_AUTOSTORM       IDM_UTB_FIRST + 13
#define IDM_UTB_AUTOSTORM       413
//convRes: #define IDM_UTB_DROPOFFGARRISON IDM_UTB_FIRST + 14
#define IDM_UTB_DROPOFFGARRISON 414
//convRes: #define IDM_UTB_NORMALMARCH     IDM_UTB_FIRST + 15
#define IDM_UTB_NORMALMARCH     415
//convRes: #define IDM_UTB_FORCEMARCH      IDM_UTB_FIRST + 16
#define IDM_UTB_FORCEMARCH      416
//convRes: #define IDM_UTB_EASYMARCH       IDM_UTB_FIRST + 17
#define IDM_UTB_EASYMARCH       417
//convRes: #define IDM_UTB_HOLD_OA         IDM_UTB_FIRST + 18
#define IDM_UTB_HOLD_OA         418
//convRes: #define IDM_UTB_RESTRALLY_OA    IDM_UTB_FIRST + 19
#define IDM_UTB_RESTRALLY_OA    419
//convRes: #define IDM_UTB_ATTACH_OA       IDM_UTB_FIRST + 20
#define IDM_UTB_ATTACH_OA       420
//convRes: #define IDM_UTB_GARRISON_OA     IDM_UTB_FIRST + 21
#define IDM_UTB_GARRISON_OA     421
//convRes: #define IDM_UTB_TIMID           IDM_UTB_FIRST + 22
#define IDM_UTB_TIMID           422
//convRes: #define IDM_UTB_DEFEND          IDM_UTB_FIRST + 23
#define IDM_UTB_DEFEND          423
//convRes: #define IDM_UTB_ATTACKSMALL     IDM_UTB_FIRST + 24
#define IDM_UTB_ATTACKSMALL     424
//convRes: #define IDM_UTB_ATTACK          IDM_UTB_FIRST + 25
#define IDM_UTB_ATTACK          425
//convRes: #define IDM_UTB_OFFENSIVE       IDM_UTB_FIRST + 26
#define IDM_UTB_OFFENSIVE       426
//convRes: #define IDM_UTB_DEFENSIVE       IDM_UTB_FIRST + 27
#define IDM_UTB_DEFENSIVE       427
//convRes: #define IDM_UTB_LAST            IDM_UTB_FIRST + 28
#define IDM_UTB_LAST            428

/*
 * Town order pop-up menu ids
 */

//convRes: #define IDM_TM_FIRST            IDM_UTB_LAST
#define IDM_TM_FIRST            428
//convRes: #define IDM_TM_BUILDS           IDM_TM_FIRST
#define IDM_TM_BUILDS           428
//convRes: #define IDM_TM_INFO             IDM_TM_FIRST + 1
#define IDM_TM_INFO             429
//convRes: #define IDM_TM_LAST             IDM_TM_FIRST + 2
#define IDM_TM_LAST             430

// #endif // CAMPAIGN

// #if defined(BATTLE)
/*
 * Menu IDs
 */

//convRes: #define IDM_BAT_FIRST           500
#define IDM_BAT_FIRST           500

//convRes: #define IDM_BAT_LOADGAME        IDM_LOADGAME
#define IDM_BAT_LOADGAME        100
//convRes: #define IDM_BAT_SAVEGAME        IDM_SAVEGAME
#define IDM_BAT_SAVEGAME        101
//convRes: #define IDM_BAT_ABANDONGAME     IDM_ABANDONGAME
#define IDM_BAT_ABANDONGAME     102
//convRes: #define IDM_BAT_EXIT            IDM_EXIT
#define IDM_BAT_EXIT            103

// #define IDM_BAT_LOADGAME             IDM_BAT_FIRST+0
// #define IDM_BAT_SAVEGAME             IDM_BAT_FIRST+1
// #define IDM_BAT_ABANDONGAME  IDM_BAT_FIRST+2
//convRes: #define IDM_BAT_LOADWORLD               IDM_BAT_FIRST+3
#define IDM_BAT_LOADWORLD               503
//convRes: #define IDM_BAT_SAVEWORLD               IDM_BAT_FIRST+4
#define IDM_BAT_SAVEWORLD               504
//convRes: #define IDM_BAT_SURRENDER               IDM_BAT_FIRST+5
#define IDM_BAT_SURRENDER               505
// #define IDM_BAT_EXIT                         IDM_BAT_FIRST+6

//convRes: #define IDM_BAT_ZOOMIN                  IDM_BAT_FIRST+20
#define IDM_BAT_ZOOMIN                  520
//convRes: #define IDM_BAT_ZOOMOVERVIEW    IDM_BAT_FIRST+21
#define IDM_BAT_ZOOMOVERVIEW    521
//convRes: #define IDM_BAT_ZOOMFOURMILE  IDM_BAT_FIRST+22
#define IDM_BAT_ZOOMFOURMILE  522
//convRes: #define IDM_BAT_ZOOMTWOMILE     IDM_BAT_FIRST+23
#define IDM_BAT_ZOOMTWOMILE     523
//convRes: #define IDM_BAT_ZOOMDETAIL              IDM_BAT_FIRST+24
#define IDM_BAT_ZOOMDETAIL              524
//convRes: #define IDM_BAT_HEXOUTLINE              IDM_BAT_FIRST+25
#define IDM_BAT_HEXOUTLINE              525
#ifdef DEBUG
//convRes: #define IDM_BAT_HEXDEBUG                IDM_BAT_FIRST+26
#define IDM_BAT_HEXDEBUG                526
#endif
//convRes: #define IDM_VISIBILITY                  IDM_BAT_FIRST+27
#define IDM_VISIBILITY                  527

//convRes: #define IDM_DRAW_DISPLAY_STATS          IDM_BAT_FIRST+28
#define IDM_DRAW_DISPLAY_STATS          528
//convRes: #define IDM_DRAW_TERRAIN                IDM_BAT_FIRST+29
#define IDM_DRAW_TERRAIN                529
//convRes: #define IDM_DRAW_OVERLAPS               IDM_BAT_FIRST+30
#define IDM_DRAW_OVERLAPS               530
//convRes: #define IDM_DRAW_PATHWAYS               IDM_BAT_FIRST+31
#define IDM_DRAW_PATHWAYS               531
//convRes: #define IDM_DRAW_COASTS         IDM_BAT_FIRST+32
#define IDM_DRAW_COASTS         532
//convRes: #define IDM_DRAW_HILLS          IDM_BAT_FIRST+33
#define IDM_DRAW_HILLS          533
//convRes: #define IDM_DRAW_EDGES          IDM_BAT_FIRST+34
#define IDM_DRAW_EDGES          534
//convRes: #define IDM_DRAW_TROOPS          IDM_BAT_FIRST+35
#define IDM_DRAW_TROOPS          535
//convRes: #define IDM_DRAW_BUILDINGS          IDM_BAT_FIRST+36
#define IDM_DRAW_BUILDINGS          536
//convRes: #define IDM_DRAW_EFFECTS          IDM_BAT_FIRST+37
#define IDM_DRAW_EFFECTS          537

//convRes: #define IDM_BAT_PLAYERSETTINGS  IDM_BAT_FIRST+40
#define IDM_BAT_PLAYERSETTINGS  540
//convRes: #define IDM_BAT_GAMEOPTIONS             IDM_BAT_FIRST+41
#define IDM_BAT_GAMEOPTIONS             541
//convRes: #define IDM_BAT_INSTANTORDERS    IDM_BAT_FIRST+42
#define IDM_BAT_INSTANTORDERS    542
#ifdef DEBUG
//convRes: #define IDM_BAT_INSTANTMOVE             IDM_BAT_FIRST+43
#define IDM_BAT_INSTANTMOVE             543
#endif

//convRes: #define IDM_BAT_TINYMAPWINDOW    IDM_BAT_FIRST+60
#define IDM_BAT_TINYMAPWINDOW    560
//convRes: #define IDM_BAT_PALETTEWINDOW    IDM_BAT_FIRST+61
#define IDM_BAT_PALETTEWINDOW    561
//convRes: #define IDM_BAT_CLOCKWINDOW      IDM_BAT_FIRST+62
#define IDM_BAT_CLOCKWINDOW      562
//convRes: #define IDM_BAT_MESSAGEWINDOW    IDM_BAT_FIRST+63
#define IDM_BAT_MESSAGEWINDOW    563
//convRes: #define IDM_BAT_TRACKINGWINDOW IDM_BAT_FIRST+64
#define IDM_BAT_TRACKINGWINDOW 564

//convRes: #define IDM_BAT_EDIT_OB                 IDM_BAT_FIRST+80
#define IDM_BAT_EDIT_OB                 580
//convRes: #define IDM_BAT_EDIT_DATE               IDM_BAT_FIRST+81
#define IDM_BAT_EDIT_DATE               581
//convRes: #define IDM_BAT_EDITINFO                        IDM_BAT_FIRST+82
#define IDM_BAT_EDITINFO                        582
//convRes: #define IDM_BAT_EDITCONDITIONS  IDM_BAT_FIRST+83
#define IDM_BAT_EDITCONDITIONS  583
//convRes: #define IDM_BAT_NEW_OBJECT              IDM_BAT_FIRST+84
#define IDM_BAT_NEW_OBJECT              584
//convRes: #define IDM_BAT_EDIT_NOTHING    IDM_BAT_FIRST+85
#define IDM_BAT_EDIT_NOTHING    585

//convRes: #define IDM_BAT_OB                                      IDM_BAT_FIRST+100
#define IDM_BAT_OB                                      600
//convRes: #define IDM_BAT_FINDLEADER                      IDM_BAT_FIRST+101
#define IDM_BAT_FINDLEADER                      601

// #define IDM_BAT_DEBUG                                IDM_BAT_FIRST+120
// #define IDM_BAT_HELP_CONTENTS                IDM_BAT_FIRST+140
// #define IDM_BAT_HELP_HISTORY         IDM_BAT_FIRST+141
// #define IDM_BAT_ABOUT                                IDM_BAT_FIRST+142
//convRes: #define IDM_BAT_DEBUG             IDM_DEBUG
#define IDM_BAT_DEBUG             107
//convRes: #define IDM_BAT_HELP_CONTENTS IDM_HELP_CONTENTS
#define IDM_BAT_HELP_CONTENTS 104
//convRes: #define IDM_BAT_HELP_HISTORY  IDM_HELP_HISTORY
#define IDM_BAT_HELP_HISTORY  106
//convRes: #define IDM_BAT_ABOUT             IDM_ABOUT
#define IDM_BAT_ABOUT             105

// popup menu
//convRes: #define IDM_BATM_FIRST              IDM_BAT_FIRST+200
#define IDM_BATM_FIRST              700
//convRes: #define IDM_BAT_ORDERS              IDM_BATM_FIRST
#define IDM_BAT_ORDERS              700
//convRes: #define IDM_BAT_DEPLOY              IDM_BATM_FIRST+1
#define IDM_BAT_DEPLOY              701
//convRes: #define IDM_BAT_INFO                IDM_BATM_FIRST+2
#define IDM_BAT_INFO                702
//convRes: #define IDM_BAT_ORDERHOLD           IDM_BATM_FIRST+3
#define IDM_BAT_ORDERHOLD           703
//convRes: #define IDM_BAT_ORDERMOVE           IDM_BATM_FIRST+4
#define IDM_BAT_ORDERMOVE           704
//convRes: #define IDM_BAT_ORDERRESTRALLY      IDM_BATM_FIRST+5
#define IDM_BAT_ORDERRESTRALLY      705
//convRes: #define IDM_BAT_ORDERATTACH         IDM_BATM_FIRST+6
#define IDM_BAT_ORDERATTACH         706
//convRes: #define IDM_BAT_ORDERDETACH         IDM_BATM_FIRST+7
#define IDM_BAT_ORDERDETACH         707
//convRes: #define IDM_BAT_ORDERATTACHLEADER   IDM_BATM_FIRST+8
#define IDM_BAT_ORDERATTACHLEADER   708
//convRes: #define IDM_BAT_ORDERDETACHLEADER   IDM_BATM_FIRST+9
#define IDM_BAT_ORDERDETACHLEADER   709
//convRes: #define IDM_BAT_ORDERBOMBARD        IDM_BATM_FIRST+10
#define IDM_BAT_ORDERBOMBARD        710
//convRes: #define IDM_BAT_ORDERROUT           IDM_BATM_FIRST+11
#define IDM_BAT_ORDERROUT           711
//convRes: #define IDM_BAT_AVOID               IDM_BATM_FIRST+12
#define IDM_BAT_AVOID               712
//convRes: #define IDM_BAT_PROBEDELAY          IDM_BATM_FIRST+13
#define IDM_BAT_PROBEDELAY          713
//convRes: #define IDM_BAT_LIMITED             IDM_BATM_FIRST+14
#define IDM_BAT_LIMITED             714
//convRes: #define IDM_BAT_ATALLCOST           IDM_BATM_FIRST+15
#define IDM_BAT_ATALLCOST           715
//convRes: #define IDM_BAT_OFFENSIVE           IDM_BATM_FIRST+16
#define IDM_BAT_OFFENSIVE           716
//convRes: #define IDM_BAT_DEFENSIVE           IDM_BATM_FIRST+17
#define IDM_BAT_DEFENSIVE           717
//convRes: #define IDM_BAT_DEPLOYMARCH         IDM_BATM_FIRST+18
#define IDM_BAT_DEPLOYMARCH         718
//convRes: #define IDM_BAT_DEPLOYMASSED        IDM_BATM_FIRST+19
#define IDM_BAT_DEPLOYMASSED        719
//convRes: #define IDM_BAT_DEPLOYDEPLOYED      IDM_BATM_FIRST+20
#define IDM_BAT_DEPLOYDEPLOYED      720
//convRes: #define IDM_BAT_DEPLOYEXTENDED      IDM_BATM_FIRST+21
#define IDM_BAT_DEPLOYEXTENDED      721
//convRes: #define IDM_BAT_DEPLOYRIGHT         IDM_BATM_FIRST+22
#define IDM_BAT_DEPLOYRIGHT         722
//convRes: #define IDM_BAT_DEPLOYCENTER        IDM_BATM_FIRST+23
#define IDM_BAT_DEPLOYCENTER        723
//convRes: #define IDM_BAT_DEPLOYLEFT          IDM_BATM_FIRST+24
#define IDM_BAT_DEPLOYLEFT          724
//convRes: #define IDM_BAT_MARCHFORMATION      IDM_BATM_FIRST+25
#define IDM_BAT_MARCHFORMATION      725
//convRes: #define IDM_BAT_COLUMNFORMATION     IDM_BATM_FIRST+26
#define IDM_BAT_COLUMNFORMATION     726
//convRes: #define IDM_BAT_CLOSEDFORMATION     IDM_BATM_FIRST+27
#define IDM_BAT_CLOSEDFORMATION     727
//convRes: #define IDM_BAT_LINEFORMATION       IDM_BATM_FIRST+28
#define IDM_BAT_LINEFORMATION       728
//convRes: #define IDM_BAT_SQUAREFORMATION     IDM_BATM_FIRST+29
#define IDM_BAT_SQUAREFORMATION     729
//convRes: #define IDM_BAT_LIMBEREDFORMATION   IDM_BATM_FIRST+30
#define IDM_BAT_LIMBEREDFORMATION   730
//convRes: #define IDM_BAT_UNLIMBEREDFORMATION IDM_BATM_FIRST+31
#define IDM_BAT_UNLIMBEREDFORMATION 731
//convRes: #define IDM_BAT_FORWARD             IDM_BATM_FIRST+32
#define IDM_BAT_FORWARD             732
//convRes: #define IDM_BAT_WHEELRIGHT          IDM_BATM_FIRST+33
#define IDM_BAT_WHEELRIGHT          733
//convRes: #define IDM_BAT_WHEELLEFT           IDM_BATM_FIRST+34
#define IDM_BAT_WHEELLEFT           734
//convRes: #define IDM_BAT_RWHEELRIGHT         IDM_BATM_FIRST+35
#define IDM_BAT_RWHEELRIGHT         735
//convRes: #define IDM_BAT_RWHEELLEFT          IDM_BATM_FIRST+36
#define IDM_BAT_RWHEELLEFT          736
//convRes: #define IDM_BAT_SIDESTEPRIGHT       IDM_BATM_FIRST+37
#define IDM_BAT_SIDESTEPRIGHT       737
//convRes: #define IDM_BAT_SIDESTEPLEFT        IDM_BATM_FIRST+38
#define IDM_BAT_SIDESTEPLEFT        738
//convRes: #define IDM_BAT_ABOUTFACE           IDM_BATM_FIRST+39
#define IDM_BAT_ABOUTFACE           739
//convRes: #define IDM_BAT_HOLD                IDM_BATM_FIRST+40
#define IDM_BAT_HOLD                740

//convRes: #define IDC_BAT_CURSFIRST       IDM_BATM_FIRST+100
#define IDC_BAT_CURSFIRST       800

//convRes: #define CURS_BAT_OVERUNIT_COLOR        IDC_BAT_CURSFIRST+1
#define CURS_BAT_OVERUNIT_COLOR        801
//convRes: #define CURS_BAT_OVERUNIT_MONO        IDC_BAT_CURSFIRST+2
#define CURS_BAT_OVERUNIT_MONO        802
//convRes: #define CURS_BAT_DRAGUNIT_COLOR        IDC_BAT_CURSFIRST+3
#define CURS_BAT_DRAGUNIT_COLOR        803
//convRes: #define CURS_BAT_DRAGUNIT_MONO        IDC_BAT_CURSFIRST+4
#define CURS_BAT_DRAGUNIT_MONO        804
//convRes: #define CURS_BAT_BADHEX_COLOR        IDC_BAT_CURSFIRST+5
#define CURS_BAT_BADHEX_COLOR        805
//convRes: #define CURS_BAT_BADHEX_MONO        IDC_BAT_CURSFIRST+6
#define CURS_BAT_BADHEX_MONO        806
//convRes: #define CURS_BAT_TARGETMODE_COLOR        IDC_BAT_CURSFIRST+7
#define CURS_BAT_TARGETMODE_COLOR        807
//convRes: #define CURS_BAT_TARGETMODE_MONO        IDC_BAT_CURSFIRST+8
#define CURS_BAT_TARGETMODE_MONO        808
//convRes: #define CURS_BAT_VALIDTARGET_COLOR        IDC_BAT_CURSFIRST+9
#define CURS_BAT_VALIDTARGET_COLOR        809
//convRes: #define CURS_BAT_VALIDTARGET_MONO        IDC_BAT_CURSFIRST+10
#define CURS_BAT_VALIDTARGET_MONO        810
//convRes: #define CURS_BAT_INVALIDTARGET_COLOR        IDC_BAT_CURSFIRST+11
#define CURS_BAT_INVALIDTARGET_COLOR        811
//convRes: #define CURS_BAT_INVALIDTARGET_MONO        IDC_BAT_CURSFIRST+12
#define CURS_BAT_INVALIDTARGET_MONO        812
//convRes: #define CURS_BAT_ATTATCHUNIT_COLOR        IDC_BAT_CURSFIRST+13
#define CURS_BAT_ATTATCHUNIT_COLOR        813
//convRes: #define CURS_BAT_ATTATCHUNIT_MONO        IDC_BAT_CURSFIRST+14
#define CURS_BAT_ATTATCHUNIT_MONO        814

// #endif  // BATTLE

/*
 *  Startup screen menu
 */

//convRes: #define FE_IDM_First          400       // IDM_TM_LAST+1
#define FE_IDM_First          400 // IDM_TM_LAST+1

// These 4 are for the bottoms on the MAIN SCREEN
//convRes: #define FE_IDM_NewCampaign    FE_IDM_First
#define FE_IDM_NewCampaign    400
//convRes: #define FE_IDM_NewBattle      FE_IDM_First+1
#define FE_IDM_NewBattle      401
//convRes: #define FE_IDM_ResumeGame     FE_IDM_First+2
#define FE_IDM_ResumeGame     402
//convRes: #define FE_IDM_Quit           FE_IDM_First+3
#define FE_IDM_Quit           403

//convRes: #define FE_IDM_Multiplayer      FE_IDM_First+4
#define FE_IDM_Multiplayer      404

//convRes: #define FE_IDM_HelpContents   IDM_HELP_CONTENTS
#define FE_IDM_HelpContents   104
//convRes: #define FE_IDM_Reference      IDM_HELP_HISTORY
#define FE_IDM_Reference      106
//convRes: #define FE_IDM_About          IDM_ABOUT
#define FE_IDM_About          105

//convRes: #define FE_IDM_LoadGame             IDM_LOADGAME
#define FE_IDM_LoadGame             100

//convRes: #define FE_IDM_Last           FE_IDM_First+8
#define FE_IDM_Last           408





/*
 * MultiPlayer dialog defines
 */

//convRes: #define IDD_MULTIPLAYERDIALOG 50
#define IDD_MULTIPLAYERDIALOG 50
//convRes: #define IDD_HOSTDIALOG 51
#define IDD_HOSTDIALOG 51
//convRes: #define IDD_JOINDIALOG  52
#define IDD_JOINDIALOG  52

//convRes: #define IDC_STATIC      -1 // 53
#define IDC_STATIC      -1 // 53

//convRes: #define IDC_RADIO_HOSTGAME              55
#define IDC_RADIO_HOSTGAME              55
//convRes: #define IDC_RADIO_JOINGAME              56
#define IDC_RADIO_JOINGAME              56
//convRes: #define IDC_EDIT_GAMENAME               57
#define IDC_EDIT_GAMENAME               57
//#define IDC_EDIT_MAXPLAYERS             58
//#define IDC_EDIT_HOSTPASSWORD           59
//#define IDC_SPIN_MAXPLAYERS             60
//convRes: #define IDC_BUTTON_REFRESH              61
#define IDC_BUTTON_REFRESH              61
//convRes: #define IDC_LIST_GAMESFOUND             62
#define IDC_LIST_GAMESFOUND             62
//convRes: #define IDC_EDIT_GAMEINFO               63
#define IDC_EDIT_GAMEINFO               63
//convRes: #define IDC_COMBO_CONNECTION            64
#define IDC_COMBO_CONNECTION            64
//#define IDC_EDIT_JOINPASSWORD           65
//convRes: #define IDC_GROUP_SESSIONTYPE           -1
#define IDC_GROUP_SESSIONTYPE           -1


//convRes: #define IDD_MUTLIPLAYERMSG      100
#define IDD_MUTLIPLAYERMSG      100
//convRes: #define IDC_EDIT_MESSAGE        101
#define IDC_EDIT_MESSAGE        101
//convRes: #define IDC_EDIT_HISTORY        102
#define IDC_EDIT_HISTORY        102
//convRes: #define IDC_EDIT_STATUS         103
#define IDC_EDIT_STATUS         103
//convRes: #define IDC_BUTTON_DISCONNECT   104
#define IDC_BUTTON_DISCONNECT   104
//convRes: #define IDC_SCROLL_HISTORY      105
#define IDC_SCROLL_HISTORY      105








// #if defined(CAMPAIGN)

/*
 * Menu Popups
 */

// #define MENU_FILE                    0
// #define MENU_VIEW                1
// #define MENU_WINDOW          2
// #define MENU_CUSTOMIZE      3
// #define MENU_INFO                    4
// #define MENU_HELP                    5

/*
 * Contents of toolbar.bmp
 */

// #define TBI_GRID             0
// #define TBI_LOCATOR  1
// #define TBI_PALETTE  2
// #define TBI_ZOOMIN   3
// #define TBI_ZOOMOUT  4
// #define TBI_HowMany  5




/*
 * ID's for Child Windows
 */

//convRes: #define WID_Map                 1
#define WID_Map                 1
//convRes: #define WID_TinyMap             2
#define WID_TinyMap             2
//convRes: #define WID_STATUSBAR   3
#define WID_STATUSBAR   3
//convRes: #define WID_TOOLBAR             4
#define WID_TOOLBAR             4
//convRes: #define WID_PALETTE             5
#define WID_PALETTE             5
//convRes: #define WID_OB                          6               // Order of Battle Window
#define WID_OB                          6 // Order of Battle Window
//convRes: #define WID_FT                          7               // Floating Toolbar
#define WID_FT                          7 // Floating Toolbar
//convRes: #define WID_CLOCK                       8               // Clock Window
#define WID_CLOCK                       8 // Clock Window

// #endif  // CAMPAIGN


/*
 * Dialogues
 */

/*
 * Used by Campaign AND battle
 */

//convRes: #define DLG_SAVEQUIT                                            2
#define DLG_SAVEQUIT                                            2
//convRes: #define DLG_PLAYER_SETTINGS                 3
#define DLG_PLAYER_SETTINGS                 3

/*
 * Save and Quit box buttons
 */

#define IDSQ_SAVE                                               IDOK
//convRes: #define IDSQ_QUIT                                               100
#define IDSQ_QUIT                                               100
#define IDSQ_CANCEL                                     IDCANCEL

/*
 * Player Settings Dialog Buttons
 */


#define playerSettingsDlgName       MAKEINTRESOURCE(DLG_PLAYER_SETTINGS)


//convRes: #define PSD_PLAYER1_BOX        200
#define PSD_PLAYER1_BOX        200
//convRes: #define PSD_PLAYER1_PLAYER     201
#define PSD_PLAYER1_PLAYER     201
//convRes: #define PSD_PLAYER1_AI         202
#define PSD_PLAYER1_AI         202
//convRes: #define PSD_PLAYER1_REMOTE     203
#define PSD_PLAYER1_REMOTE     203
//convRes: #define PSD_PLAYER2_BOX        204
#define PSD_PLAYER2_BOX        204
//convRes: #define PSD_PLAYER2_PLAYER     205
#define PSD_PLAYER2_PLAYER     205
//convRes: #define PSD_PLAYER2_AI         206
#define PSD_PLAYER2_AI         206
//convRes: #define PSD_PLAYER2_REMOTE     207
#define PSD_PLAYER2_REMOTE     207
//convRes: #define PSD_CANCEL             208
#define PSD_CANCEL             208
//convRes: #define PSD_OK                 209
#define PSD_OK                 209

/*========================================================
 * Campaign Only
 */

// #if defined(CAMPAIGN)
#if defined(CUSTOMIZE)
//convRes: #define DLG_EDIT_TOWN                                   10
#define DLG_EDIT_TOWN                                   10
//convRes: #define DLG_TE_GENERAL                                  11
#define DLG_TE_GENERAL                                  11
//convRes: #define DLG_TE_POSITION                                 12
#define DLG_TE_POSITION                                 12
//convRes: #define DLG_TE_RESOURCE                                 13
#define DLG_TE_RESOURCE                                 13
//convRes: #define DLG_EDIT_PROVINCE                               14
#define DLG_EDIT_PROVINCE                               14
//convRes: #define DLG_TOWNCONTROL                                 15
#define DLG_TOWNCONTROL                                 15
//convRes: #define DLG_CONNECTEDIT                                 16
#define DLG_CONNECTEDIT                                 16
//convRes: #define DLG_DATE_EDIT                                   17
#define DLG_DATE_EDIT                                   17
//convRes: #define DLG_EDIT_OB                 18
#define DLG_EDIT_OB                 18
//convRes: #define DLG_OBE_UNITS               19
#define DLG_OBE_UNITS               19
//convRes: #define DLG_OBE_POSITION                           20
#define DLG_OBE_POSITION                           20
//convRes: #define DLG_OBE_UNIT                21
#define DLG_OBE_UNIT                21
//convRes: #define DLG_OBE_LEADER              22
#define DLG_OBE_LEADER              22
//convRes: #define DLG_EDITCAMPAIGNINFO        23
#define DLG_EDITCAMPAIGNINFO        23
//convRes: #define DLG_CONDITIONEDIT           24
#define DLG_CONDITIONEDIT           24
//convRes: #define DLG_TOWNCHANGEPAGE          25
#define DLG_TOWNCHANGEPAGE          25
//convRes: #define DLG_CASUALTYPAGE            26
#define DLG_CASUALTYPAGE            26
//convRes: #define DLG_VICTORYPAGE             27
#define DLG_VICTORYPAGE             27
//convRes: #define DLG_NEUTRALTOWNPAGE         28
#define DLG_NEUTRALTOWNPAGE         28
//convRes: #define DLG_DEATHOFLEADERPAGE       29
#define DLG_DEATHOFLEADERPAGE       29
//convRes: #define DLG_DATEREACHEDPAGE         30
#define DLG_DATEREACHEDPAGE         30
//convRes: #define DLG_ACTIONSPAGE             31
#define DLG_ACTIONSPAGE             31
//convRes: #define DLG_EDITALLEGIANCE          32
#define DLG_EDITALLEGIANCE          32
//convRes: #define DLG_ACTION_VICTORYPAGE      33
#define DLG_ACTION_VICTORYPAGE      33
//convRes: #define DLG_ACTION_ARMISTICEPAGE    34
#define DLG_ACTION_ARMISTICEPAGE    34
//convRes: #define DLG_ACTION_NATIONENTERSPAGE 35
#define DLG_ACTION_NATIONENTERSPAGE 35
//convRes: #define DLG_ACTION_NEWUNITSPAGE     36
#define DLG_ACTION_NEWUNITSPAGE     36
//convRes: #define DLG_ACTION_LEADEREXITSPAGE  37
#define DLG_ACTION_LEADEREXITSPAGE  37
//convRes: #define DLG_ACTION_ALLYDEFECTSPAGE  38
#define DLG_ACTION_ALLYDEFECTSPAGE  38
//convRes: #define DLG_NATIONATWARPAGE         39
#define DLG_NATIONATWARPAGE         39
//convRes: #define DLG_SIDEOWNSTOWNPAGE        40
#define DLG_SIDEOWNSTOWNPAGE        40
//convRes: #define DLG_ARMISTICEENDSPAGE       41
#define DLG_ARMISTICEENDSPAGE       41
//convRes: #define DLG_ACTION_ADJUSTRESOURCESPAGE   42
#define DLG_ACTION_ADJUSTRESOURCESPAGE   42
//convRes: #define DLG_ARMISTICESTARTSPAGE       43
#define DLG_ARMISTICESTARTSPAGE       43
//convRes: #define DLG_LEADERENTERSPAGE       44
#define DLG_LEADERENTERSPAGE       44
#endif

//convRes: #define DLG_UNIT_ORDERS                                 50
#define DLG_UNIT_ORDERS                                 50
//convRes: #define DLG_UNITORDER_INFO                              51
#define DLG_UNITORDER_INFO                              51
//convRes: #define DLG_UNITORDER_ORDER                     52
#define DLG_UNITORDER_ORDER                     52
//convRes: #define DLG_UNITORDER_ORDER_ORDERS      53
#define DLG_UNITORDER_ORDER_ORDERS      53
//convRes: #define DLG_UNITORDER_VIA                     54
#define DLG_UNITORDER_VIA                     54
//convRes: #define DLG_UNITORDER_OPTIONS         55
#define DLG_UNITORDER_OPTIONS         55
//convRes: #define DLG_CAMPAIGN_TIME                               56
#define DLG_CAMPAIGN_TIME                               56
//convRes: #define DLG_UNITORDER_ORDER_VIA    57
#define DLG_UNITORDER_ORDER_VIA    57
//convRes: #define DLG_UNITORDER_ORDER_OPTION      58
#define DLG_UNITORDER_ORDER_OPTION      58
//convRes: #define DLG_UNITORDER_ATTACHSP      59
#define DLG_UNITORDER_ATTACHSP      59
//convRes: #define DLG_UNITORDER_LEADER        60
#define DLG_UNITORDER_LEADER        60
//convRes: #define DLG_PLAYER_OPTIONS          62
#define DLG_PLAYER_OPTIONS          62
//convRes: #define DLG_PO_ORDERS               63
#define DLG_PO_ORDERS               63
//convRes: #define DLG_PO_REALISM              64
#define DLG_PO_REALISM              64
//convRes: #define DLG_PO_MISC                 65
#define DLG_PO_MISC                 65
//convRes: #define DLG_PO_SHOWMESSAGE          66
#define DLG_PO_SHOWMESSAGE          66
//convRes: #define DLG_ALERTBOX                67
#define DLG_ALERTBOX                67
//convRes: #define DLG_FINDTOWN                68
#define DLG_FINDTOWN                68
//convRes: #define DLG_PATH                    69
#define DLG_PATH                    69
// #define DLG_OPENING_OPTIONS         60
//convRes: #define DLG_OPENING_SETTINGS        71
#define DLG_OPENING_SETTINGS        71

#ifdef CUSTOMIZE
//convRes: #define DLG_DATACHECK               72
#define DLG_DATACHECK               72
#endif        // DEBUG

/*----------- defines for new user order-interface dialogs ------------
 *
 */

//convRes: #define DLG_UNITMOVEDIAL            73
#define DLG_UNITMOVEDIAL            73
//convRes: #define DLG_UNITDIALOG              74
#define DLG_UNITDIALOG              74
//convRes: #define DLG_UNITORDERPAGE           75
#define DLG_UNITORDERPAGE           75
//convRes: #define DLG_UNITACTIVITYPAGE        76
#define DLG_UNITACTIVITYPAGE        76
//convRes: #define DLG_UNITSUMMARYPAGE         77
#define DLG_UNITSUMMARYPAGE         77
//convRes: #define DLG_UNITLEADERPAGE          78
#define DLG_UNITLEADERPAGE          78
//convRes: #define DLG_UNITOBPAGE              79
#define DLG_UNITOBPAGE              79

//convRes: #define DLG_TOWNBUILDDIAL           80
#define DLG_TOWNBUILDDIAL           80
//convRes: #define DLG_TOWNSUMMARYPAGE         81
#define DLG_TOWNSUMMARYPAGE         81
//convRes: #define DLG_TOWNBUILDPAGE           82
#define DLG_TOWNBUILDPAGE           82
//convRes: #define DLG_TOWNSTATUSPAGE          83
#define DLG_TOWNSTATUSPAGE          83
//convRes: #define DLG_TOWNTOWNPAGE            84
#define DLG_TOWNTOWNPAGE            84
//convRes: #define DLG_TOWNPROVINCEPAGE        85
#define DLG_TOWNPROVINCEPAGE        85

//convRes: #define DLG_UNITTRACKINGDIAL        86
#define DLG_UNITTRACKINGDIAL        86
//convRes: #define DLG_TOWNTRACKINGDIAL        87
#define DLG_TOWNTRACKINGDIAL        87
//#define DLG_BUILDITEMDIAL           86
//convRes: #define DLG_ENDOFBATTLEDAYDIAL      88
#define DLG_ENDOFBATTLEDAYDIAL      88
#if defined(CUSTOMIZE)
//convRes: #define DLG_PRINTMAP                89
#define DLG_PRINTMAP                89
#endif

//convRes: #define DLG_TOWN_ORDER                  1
#define DLG_TOWN_ORDER                  1
//convRes: #define DLG_TOWNINFO                            2
#define DLG_TOWNINFO                            2
//convRes: #define DLG_PROVINCEINFO                3
#define DLG_PROVINCEINFO                3
//convRes: #define DLG_BUILDPAGE                   4
#define DLG_BUILDPAGE                   4
//convRes: #define DLG_BUILDNEWPAGE                5
#define DLG_BUILDNEWPAGE                5

// #endif  // CAMPAIGN

/*
 * Battle Only
 */

// #if defined(BATTLE)
//convRes: #define DLG_BATTLEUNITMOVEDIAL          101
#define DLG_BATTLEUNITMOVEDIAL          101
//convRes: #define DLG_BATTLE_DIV_ORDER                    102
#define DLG_BATTLE_DIV_ORDER                    102
// #endif

// #if defined(CAMPAIGN)

/*
 * Generic Button IDs
 */

//convRes: #define ID_TABBED                                               200
#define ID_TABBED                                               200

#if defined(CUSTOMIZE)
/*
 * Town edit Dialogue
 */

#define IDTE_OK                                         IDOK
#define IDTE_CANCEL                                     IDCANCEL
//convRes: #define IDTE_TOWN_NAME                          98
#define IDTE_TOWN_NAME                          98
//convRes: #define IDTE_DELETE                                     99
#define IDTE_DELETE                                     99
//convRes: #define IDTE_TABBED                                     100
#define IDTE_TABBED                                     100

// From General

//convRes: #define IDTE_STATE_NAME                         101
#define IDTE_STATE_NAME                         101
//convRes: #define IDTE_SET_STATE                          102
#define IDTE_SET_STATE                          102
//convRes: #define IDTE_STATE_INFO                         103
#define IDTE_STATE_INFO                         103
//convRes: #define IDTE_OFFSCREEN           104
#define IDTE_OFFSCREEN           104
//convRes: #define IDTE_HELPID              105
#define IDTE_HELPID              105
//convRes: #define IDTE_DEFAULTHELPID       106
#define IDTE_DEFAULTHELPID       106

//convRes: #define IDTE_SIDE                                               107
#define IDTE_SIDE                                               107
// #define IDTE_START_SIDE                              106
//convRes: #define IDTE_TYPE                               108
#define IDTE_TYPE                               108
//convRes: #define IDTE_SIEGABLE                           109
#define IDTE_SIEGABLE                           109
//convRes: #define IDTE_VICTORYA                           110
#define IDTE_VICTORYA                           110
//convRes: #define IDTE_VICTORYASPIN                       111
#define IDTE_VICTORYASPIN                       111
//convRes: #define IDTE_VICTORYB                           112
#define IDTE_VICTORYB                           112
//convRes: #define IDTE_VICTORYBSPIN                       113
#define IDTE_VICTORYBSPIN                       113
//convRes: #define IDTE_STACKING                           114
#define IDTE_STACKING                           114
//convRes: #define IDTE_STACKINGSPIN                       115
#define IDTE_STACKINGSPIN                       115
//convRes: #define IDTE_FORTIFICATION                      116
#define IDTE_FORTIFICATION                      116
//convRes: #define IDTE_FORTIFICATIONSPIN  117
#define IDTE_FORTIFICATIONSPIN  117
//convRes: #define IDTE_POLITICAL                          118
#define IDTE_POLITICAL                          118
//convRes: #define IDTE_POLITICALSPIN                      119
#define IDTE_POLITICALSPIN                      119
//convRes: #define IDTE_STRENGTH                           120
#define IDTE_STRENGTH                           120
//convRes: #define IDTE_STRENGTHSPIN                       121
#define IDTE_STRENGTHSPIN                       121

// Position

//convRes: #define IDTE_ALIGN                                      122
#define IDTE_ALIGN                                      122
//convRes: #define IDTE_XOFFSET                                    123
#define IDTE_XOFFSET                                    123
//convRes: #define IDTE_XUPDOWN                                    124
#define IDTE_XUPDOWN                                    124
//convRes: #define IDTE_YOFFSET                                    125
#define IDTE_YOFFSET                                    125
//convRes: #define IDTE_YUPDOWN                                    126
#define IDTE_YUPDOWN                                    126
//convRes: #define IDTE_TERRAIN                                    127
#define IDTE_TERRAIN                                    127
//convRes: #define IDTE_MOVE                               128
#define IDTE_MOVE                               128

// Resources

//convRes: #define IDTE_SUPPLY                                     129
#define IDTE_SUPPLY                                     129
//convRes: #define IDTE_SUPPLYSPIN                         130
#define IDTE_SUPPLYSPIN                         130
//convRes: #define IDTE_SUPPLYSOURCE                       131
#define IDTE_SUPPLYSOURCE                       131
//convRes: #define IDTE_SUPPLYDEPOT                        132
#define IDTE_SUPPLYDEPOT                        132
//convRes: #define IDTE_MANPOWER                           133
#define IDTE_MANPOWER                           133
//convRes: #define IDTE_MANPOWERSPIN                       134
#define IDTE_MANPOWERSPIN                       134
// #define IDTE_FREEMANPOWER                    132
// #define IDTE_FREEMANPOWERSPIN                133
//convRes: #define IDTE_MPTRANSFER                         135
#define IDTE_MPTRANSFER                         135
//convRes: #define IDTE_RESOURCE                           136
#define IDTE_RESOURCE                           136
//convRes: #define IDTE_RESOURCESPIN                       137
#define IDTE_RESOURCESPIN                       137
// #define IDTE_FREERESOURCE                    137
// #define IDTE_FREERESOURCESPIN                138
//convRes: #define IDTE_FORAGE                                     139
#define IDTE_FORAGE                                     139
//convRes: #define IDTE_FORAGESPIN                         140
#define IDTE_FORAGESPIN                         140
//convRes: #define IDTE_SUPPLYPILE          141
#define IDTE_SUPPLYPILE          141
//convRes: #define IDTE_SUPPLYPILESPIN      142
#define IDTE_SUPPLYPILESPIN      142

// Flags

/*
 * Province Editor
 */


#define IDPE_OK                                         IDOK
#define IDPE_CANCEL                                     IDCANCEL
//convRes: #define IDPE_DELETE                                     100
#define IDPE_DELETE                                     100
//convRes: #define IDPE_MOVE                                               101
#define IDPE_MOVE                                               101
//convRes: #define IDPE_STATE_NAME                         102
#define IDPE_STATE_NAME                         102
//convRes: #define IDPE_SHORT_NAME                         103
#define IDPE_SHORT_NAME                         103
//convRes: #define IDPE_SIDE                                               104
#define IDPE_SIDE                                               104
//convRes: #define IDPE_START_SIDE                         105
#define IDPE_START_SIDE                         105
//convRes: #define IDPE_NATIONALITY                        106
#define IDPE_NATIONALITY                        106
//convRes: #define IDPE_CAPITAL                                    107
#define IDPE_CAPITAL                                    107
//convRes: #define IDPE_SET_CAPITAL                        108
#define IDPE_SET_CAPITAL                        108
//convRes: #define IDPE_SET_TOWNS                          109
#define IDPE_SET_TOWNS                          109
//convRes: #define IDPE_OFFSCREEN           110
#define IDPE_OFFSCREEN           110
//convRes: #define IDPE_HELPID              111
#define IDPE_HELPID              111
//convRes: #define IDPE_DEFAULTHELPID       112
#define IDPE_DEFAULTHELPID       112
//convRes: #define IDPE_PICTURE                                    113
#define IDPE_PICTURE                                    113

/*
 * Connection Editor
 */

#define IDCE_OK                                         IDOK
#define IDCE_CANCEL                                     IDCANCEL
//convRes: #define IDCE_LISTBOX                                    100
#define IDCE_LISTBOX                                    100
//convRes: #define IDCE_SETTINGSTITLE                      101
#define IDCE_SETTINGSTITLE                      101
//convRes: #define IDCE_HOWTITLE                           102
#define IDCE_HOWTITLE                           102
//convRes: #define IDCE_HOW                                                103
#define IDCE_HOW                                                103
//convRes: #define IDCE_QUALITYTITLE                       104
#define IDCE_QUALITYTITLE                       104
//convRes: #define IDCE_QUALITY                                    105
#define IDCE_QUALITY                                    105
//convRes: #define IDCE_CAPACITYTITLE                      106
#define IDCE_CAPACITYTITLE                      106
//convRes: #define IDCE_CAPACITY                           107
#define IDCE_CAPACITY                           107
//convRes: #define IDCE_CAPACITYSPIN                       108
#define IDCE_CAPACITYSPIN                       108
// #define IDCE_NEW                                     109
//convRes: #define IDCE_DELETE                                     110
#define IDCE_DELETE                                     110
//convRes: #define IDCE_CLOSE                                      111
#define IDCE_CLOSE                                      111
//convRes: #define IDCE_TITLE                                      112
#define IDCE_TITLE                                      112
//convRes: #define IDCE_OFFSCREEN           113
#define IDCE_OFFSCREEN           113
//convRes: #define IDCE_DISTANCE            114
#define IDCE_DISTANCE            114
//convRes: #define IDCE_WHICHSIDE           115
#define IDCE_WHICHSIDE           115

/*
 * Date Editor
 */

//convRes: #define ID_DATE_DAY                                     100
#define ID_DATE_DAY                                     100
//convRes: #define ID_DATE_DAYSPIN                         101
#define ID_DATE_DAYSPIN                         101
//convRes: #define ID_DATE_MONTH                           102
#define ID_DATE_MONTH                           102
//convRes: #define ID_DATE_YEAR                                    103
#define ID_DATE_YEAR                                    103
//convRes: #define ID_DATE_YEARSPIN                        104
#define ID_DATE_YEARSPIN                        104


/*
 * OB dialog editor
 */

#define IDOBE_OK                                                IDOK
#define IDOBE_CANCEL                                    IDCANCEL
//convRes: #define IDOBE_UNIT_NAME                         99
#define IDOBE_UNIT_NAME                         99
//convRes: #define IDOBE_DELETE                                    100
#define IDOBE_DELETE                                    100
//convRes: #define IDOBE_TABBED                                    101
#define IDOBE_TABBED                                    101
//convRes: #define IDOBE_NEW                102
#define IDOBE_NEW                102

/*
 *  OB Units editor
 */
//convRes: #define IDOBE_UNITS_NAME         103
#define IDOBE_UNITS_NAME         103
//convRes: #define IDOBE_UNITS_MODEBOX      104
#define IDOBE_UNITS_MODEBOX      104
//convRes: #define IDOBE_UNITS_ATTACHTO     105
#define IDOBE_UNITS_ATTACHTO     105
//convRes: #define IDOBE_UNITS_SPLIST       106
#define IDOBE_UNITS_SPLIST       106
//convRes: #define IDOBE_UNITS_UNIT         107
#define IDOBE_UNITS_UNIT         107
//convRes: #define IDOBE_UNITS_LEADER       108
#define IDOBE_UNITS_LEADER       108
//convRes: #define IDOBE_UNITS_SP           109
#define IDOBE_UNITS_SP           109
//convRes: #define IDOBE_INDEPENDENT        110
#define IDOBE_INDEPENDENT        110
//convRes: #define IDOBE_UNITS_ATTACH       111
#define IDOBE_UNITS_ATTACH       111
//convRes: #define IDOBE_UNITS_NAMEBOX      112
#define IDOBE_UNITS_NAMEBOX      112
//convRes: #define IDOBE_UNITS_RANKBOX      113
#define IDOBE_UNITS_RANKBOX      113
//convRes: #define IDOBE_UNITS_RANKLEVEL    114
#define IDOBE_UNITS_RANKLEVEL    114
//convRes: #define IDOBE_UNITS_SIDE         115
#define IDOBE_UNITS_SIDE         115
//convRes: #define IDOBE_UNITS_ATTACHTEXT   116
#define IDOBE_UNITS_ATTACHTEXT   116
//convRes: #define IDOBE_UNITS_SIDEBOX      117
#define IDOBE_UNITS_SIDEBOX      117
//convRes: #define IDOBE_UNITS_CREATE       118
#define IDOBE_UNITS_CREATE       118
//convRes: #define IDOBE_UNITS_NATIONALITY  119
#define IDOBE_UNITS_NATIONALITY  119

/*
 *  OB Position editor
 */
//convRes: #define IDOBE_CURTOWN            120
#define IDOBE_CURTOWN            120
//convRes: #define IDOBE_NEWTOWN            121
#define IDOBE_NEWTOWN            121
//convRes: #define IDOBE_UNITS_NEWTOWN      122
#define IDOBE_UNITS_NEWTOWN      122
//convRes: #define IDOBE_MOVE               123
#define IDOBE_MOVE               123

/*
 *  OB Attributes editor
 */



//convRes: #define IDOBE_MORALE             130
#define IDOBE_MORALE             130
//convRes: #define IDOBE_MORALESPIN         131
#define IDOBE_MORALESPIN         131
//convRes: #define IDOBE_SETMORALE                         132
#define IDOBE_SETMORALE                         132
//convRes: #define IDOBE_FATIGUE            133
#define IDOBE_FATIGUE            133
//convRes: #define IDOBE_FATIGUESPIN        134
#define IDOBE_FATIGUESPIN        134
//convRes: #define IDOBE_SUPPLY             135
#define IDOBE_SUPPLY             135
//convRes: #define IDOBE_SUPPLYSPIN         136
#define IDOBE_SUPPLYSPIN         136
//convRes: #define IDOBE_SP_CURRENT         137
#define IDOBE_SP_CURRENT         137
//convRes: #define IDOBE_SP_UNITTYPE        138
#define IDOBE_SP_UNITTYPE        138
//convRes: #define IDOBE_SP_INFANTRY        139
#define IDOBE_SP_INFANTRY        139
//convRes: #define IDOBE_SP_CAVALRY         140
#define IDOBE_SP_CAVALRY         140
//convRes: #define IDOBE_SP_ARTILLERY       141
#define IDOBE_SP_ARTILLERY       141
//convRes: #define IDOBE_SP_OTHER           142
#define IDOBE_SP_OTHER           142
//convRes: #define IDOBE_SP_ATTACHTO        143
#define IDOBE_SP_ATTACHTO        143
//convRes: #define IDOBE_LEADER_CURRENT     144
#define IDOBE_LEADER_CURRENT     144
//convRes: #define IDOBE_LEADER_INIT        145
#define IDOBE_LEADER_INIT        145
//convRes: #define IDOBE_LEADER_INITSPIN    146
#define IDOBE_LEADER_INITSPIN    146
//convRes: #define IDOBE_LEADER_STAFF       147
#define IDOBE_LEADER_STAFF       147
//convRes: #define IDOBE_LEADER_STAFFSPIN   148
#define IDOBE_LEADER_STAFFSPIN   148
//convRes: #define IDOBE_LEADER_SUB         149
#define IDOBE_LEADER_SUB         149
//convRes: #define IDOBE_LEADER_SUBSPIN     150
#define IDOBE_LEADER_SUBSPIN     150
//convRes: #define IDOBE_LEADER_AGGRESS     151
#define IDOBE_LEADER_AGGRESS     151
//convRes: #define IDOBE_LEADER_AGGRESSSPIN 152
#define IDOBE_LEADER_AGGRESSSPIN 152
//convRes: #define IDOBE_LEADER_CHARISMA    153
#define IDOBE_LEADER_CHARISMA    153
//convRes: #define IDOBE_LEADER_CHARISMASPIN  154
#define IDOBE_LEADER_CHARISMASPIN  154
//convRes: #define IDOBE_LEADER_TACTICAL      155
#define IDOBE_LEADER_TACTICAL      155
//convRes: #define IDOBE_LEADER_TACTICALSPIN  156
#define IDOBE_LEADER_TACTICALSPIN  156
//convRes: #define IDOBE_LEADER_RADIUS        157
#define IDOBE_LEADER_RADIUS        157
//convRes: #define IDOBE_LEADER_RADIUSSPIN    158
#define IDOBE_LEADER_RADIUSSPIN    158
// #define IDOBE_LEADER_RANK
// #define IDOBE_LEADER_RANKSPIN
//convRes: #define IDOBE_LEADER_HEALTH         159
#define IDOBE_LEADER_HEALTH         159
//convRes: #define IDOBE_LEADER_HEALTHSPIN     160
#define IDOBE_LEADER_HEALTHSPIN     160
//convRes: #define IDOBE_LEADER_RANKLEVEL      161
#define IDOBE_LEADER_RANKLEVEL      161
//convRes: #define IDOBE_LEADER_NATION         162
#define IDOBE_LEADER_NATION         162
//convRes: #define IDOBE_LEADER_HELPID         163
#define IDOBE_LEADER_HELPID         163
//convRes: #define IDOBE_LEADER_DEFAULTHELPID  164
#define IDOBE_LEADER_DEFAULTHELPID  164
//convRes: #define IDOBE_LEADER_SHQ                           165
#define IDOBE_LEADER_SHQ                           165
//convRes: #define IDOBE_UNIT_FIELDWORKS       166
#define IDOBE_UNIT_FIELDWORKS       166
//convRes: #define IDOBE_UNIT_FIELDWORKSSPIN   167
#define IDOBE_UNIT_FIELDWORKSSPIN   167
//convRes: #define IDOBE_UNIT_AGGRESSION       168
#define IDOBE_UNIT_AGGRESSION       168
//convRes: #define IDOBE_UNIT_ORDERS           169
#define IDOBE_UNIT_ORDERS           169
//convRes: #define IDOBE_LEADER_DEFAULT        170
#define IDOBE_LEADER_DEFAULT        170
//convRes: #define IDOBE_LEADER_SPECIALISTTYPE 171
#define IDOBE_LEADER_SPECIALISTTYPE 171
//convRes: #define IDOBE_UNIT_ACTIVE               172
#define IDOBE_UNIT_ACTIVE               172
//convRes: #define IDOBE_LEADER_PORTRAIT                   173
#define IDOBE_LEADER_PORTRAIT                   173
//convRes: #define IDOBE_LEADER_LUCKY                              174
#define IDOBE_LEADER_LUCKY                              174
//convRes: #define IDOBE_UNIT_SIEGE                175
#define IDOBE_UNIT_SIEGE                175
//convRes: #define IDOBE_LEADERS_LIST             176
#define IDOBE_LEADERS_LIST             176

// ob editor popup menu
//convRes: #define IDM_OBE_ATTACHUNIT       180
#define IDM_OBE_ATTACHUNIT       180
//convRes: #define IDM_OBE_ATTACHLEADER     181
#define IDM_OBE_ATTACHLEADER     181
//convRes: #define IDM_OBE_ATTACHSP         182
#define IDM_OBE_ATTACHSP         182
//convRes: #define IDM_OBE_CREATEUNIT       183
#define IDM_OBE_CREATEUNIT       183
//convRes: #define IDM_OBE_CREATELEADER     184
#define IDM_OBE_CREATELEADER     184
//convRes: #define IDM_OBE_CREATESP         185
#define IDM_OBE_CREATESP         185
//convRes: #define IDM_OBE_CREATEARMY       186
#define IDM_OBE_CREATEARMY       186
//convRes: #define IDM_OBE_CREATEARMYWING   187
#define IDM_OBE_CREATEARMYWING   187
//convRes: #define IDM_OBE_CREATECORPS      188
#define IDM_OBE_CREATECORPS      188
//convRes: #define IDM_OBE_CREATEDIVISION   189
#define IDM_OBE_CREATEDIVISION   189
//convRes: #define IDM_OBE_DELETEUNIT       190
#define IDM_OBE_DELETEUNIT       190
//convRes: #define IDM_OBE_DELETEUNITALL    191
#define IDM_OBE_DELETEUNITALL    191
//convRes: #define IDM_OBE_DELETELEADER     192
#define IDM_OBE_DELETELEADER     192
//convRes: #define IDM_OBE_DELETESP         193
#define IDM_OBE_DELETESP         193
//convRes: #define IDM_OBE_DUPLICATE        194
#define IDM_OBE_DUPLICATE        194
//convRes: #define IDM_OBE_NEWINACTIVELEADER 195
#define IDM_OBE_NEWINACTIVELEADER 195
//convRes: #define IDM_OBE_DELETEINDLEADER  196
#define IDM_OBE_DELETEINDLEADER  196

/*
 *  Campaign Info Editor
 */

//convRes: #define CIE_FIRST                200
#define CIE_FIRST                200
//convRes: #define CIE_SCENARIODESCRIPTION  CIE_FIRST
#define CIE_SCENARIODESCRIPTION  200
//convRes: #define CIE_RTFFILENAME          CIE_FIRST+1
#define CIE_RTFFILENAME          201
//convRes: #define CIE_BROWSEFORRTF         CIE_FIRST+2
#define CIE_BROWSEFORRTF         202
//convRes: #define CIE_GRAPHICFILENAME      CIE_FIRST+3
#define CIE_GRAPHICFILENAME      203
//convRes: #define CIE_BROWSEFORGRAPHIC     CIE_FIRST+4
#define CIE_BROWSEFORGRAPHIC     204
//convRes: #define CIE_SIDE0NAME            CIE_FIRST+5
#define CIE_SIDE0NAME            205
//convRes: #define CIE_SIDE0AVIFILENAME     CIE_FIRST+6
#define CIE_SIDE0AVIFILENAME     206
//convRes: #define CIE_BROWSEFORSIDE0AVI    CIE_FIRST+7
#define CIE_BROWSEFORSIDE0AVI    207
//convRes: #define CIE_SIDE1NAME            CIE_FIRST+8
#define CIE_SIDE1NAME            208
//convRes: #define CIE_SIDE1AVIFILENAME     CIE_FIRST+9
#define CIE_SIDE1AVIFILENAME     209
//convRes: #define CIE_BROWSEFORSIDE1AVI    CIE_FIRST+10
#define CIE_BROWSEFORSIDE1AVI    210
//convRes: #define CIE_DRAWAVIFILENAME      CIE_FIRST+11
#define CIE_DRAWAVIFILENAME      211
//convRes: #define CIE_BROWSEFORDRAWAVI     CIE_FIRST+12
#define CIE_BROWSEFORDRAWAVI     212
//convRes: #define CIE_LAST                 CIE_FIRST+13
#define CIE_LAST                 213

/*
 * IDs for Campaign Conditions Editor
 */

//convRes: #define CNDE_FIRST                 100
#define CNDE_FIRST                 100

/*
 * Town changes side controls
 */

//convRes: #define CNDE_TOWNNAME              CNDE_FIRST
#define CNDE_TOWNNAME              100
//convRes: #define CNDE_TOWNSIDE              CNDE_FIRST+1
#define CNDE_TOWNSIDE              101
//convRes: #define CNDE_TC_PREV               CNDE_FIRST+2
#define CNDE_TC_PREV               102
//convRes: #define CNDE_TC_NEXT               CNDE_FIRST+3
#define CNDE_TC_NEXT               103
//convRes: #define CNDE_TC_NEW                CNDE_FIRST+4
#define CNDE_TC_NEW                104
//convRes: #define CNDE_TC_DELETE             CNDE_FIRST+5
#define CNDE_TC_DELETE             105

/*
 * Casualty level controls
 */

//convRes: #define CNDE_C_SIDE0NAME           CNDE_FIRST+10
#define CNDE_C_SIDE0NAME           110
//convRes: #define CNDE_C_SIDE1NAME           CNDE_FIRST+11
#define CNDE_C_SIDE1NAME           111
//convRes: #define CNDE_C_SIDE0CASUALTIES     CNDE_FIRST+12
#define CNDE_C_SIDE0CASUALTIES     112
//convRes: #define CNDE_C_SIDE1CASUALTIES     CNDE_FIRST+13
#define CNDE_C_SIDE1CASUALTIES     113
//convRes: #define CNDE_C_TOTALCASUALTIES     CNDE_FIRST+14
#define CNDE_C_TOTALCASUALTIES     114
//convRes: #define CNDE_C_PREV                CNDE_FIRST+15
#define CNDE_C_PREV                115
//convRes: #define CNDE_C_NEXT                CNDE_FIRST+16
#define CNDE_C_NEXT                116
//convRes: #define CNDE_C_NEW                 CNDE_FIRST+17
#define CNDE_C_NEW                 117
//convRes: #define CNDE_C_DELETE              CNDE_FIRST+18
#define CNDE_C_DELETE              118

/*
 * Victory Level Controls
 */

//convRes: #define CNDE_V_SIDE0NAME           CNDE_FIRST+20
#define CNDE_V_SIDE0NAME           120
//#define CNDE_V_SIDE1NAME           CNDE_FIRST+21
//convRes: #define CNDE_V_SIDE0VICTORY        CNDE_FIRST+22
#define CNDE_V_SIDE0VICTORY        122
//#define CNDE_V_SIDE1VICTORY        CNDE_FIRST+23
//#define CNDE_V_DECREASERATE        CNDE_FIRST+24
//convRes: #define CNDE_V_PREV                CNDE_FIRST+25
#define CNDE_V_PREV                125
//convRes: #define CNDE_V_NEXT                CNDE_FIRST+26
#define CNDE_V_NEXT                126
//convRes: #define CNDE_V_NEW                 CNDE_FIRST+27
#define CNDE_V_NEW                 127
//convRes: #define CNDE_V_DELETE              CNDE_FIRST+28
#define CNDE_V_DELETE              128

/*
 * Neutral town is taken controls
 */

//convRes: #define CNDE_NT_NATION             CNDE_FIRST+30
#define CNDE_NT_NATION             130
//convRes: #define CNDE_NT_SIDE               CNDE_FIRST+31
#define CNDE_NT_SIDE               131
//convRes: #define CNDE_NT_PREV               CNDE_FIRST+32
#define CNDE_NT_PREV               132
//convRes: #define CNDE_NT_NEXT               CNDE_FIRST+33
#define CNDE_NT_NEXT               133
//convRes: #define CNDE_NT_NEW                CNDE_FIRST+34
#define CNDE_NT_NEW                134
//convRes: #define CNDE_NT_DELETE             CNDE_FIRST+35
#define CNDE_NT_DELETE             135

/*
 * Leader is Dead controls
 */

//convRes: #define CNDE_LD_LEADER             CNDE_FIRST+40
#define CNDE_LD_LEADER             140
//convRes: #define CNDE_LD_PREV               CNDE_FIRST+41
#define CNDE_LD_PREV               141
//convRes: #define CNDE_LD_NEXT               CNDE_FIRST+42
#define CNDE_LD_NEXT               142
//convRes: #define CNDE_LD_NEW                CNDE_FIRST+43
#define CNDE_LD_NEW                143
//convRes: #define CNDE_LD_DELETE             CNDE_FIRST+44
#define CNDE_LD_DELETE             144

/*
 * Date Reached\not Reached controls
 */

//convRes: #define CNDE_DR_DAY                CNDE_FIRST+50
#define CNDE_DR_DAY                150
//convRes: #define CNDE_DR_DAYSPIN            CNDE_FIRST+51
#define CNDE_DR_DAYSPIN            151
//convRes: #define CNDE_DR_MONTH              CNDE_FIRST+52
#define CNDE_DR_MONTH              152
//convRes: #define CNDE_DR_YEAR               CNDE_FIRST+53
#define CNDE_DR_YEAR               153
//convRes: #define CNDE_DR_YEARSPIN           CNDE_FIRST+54
#define CNDE_DR_YEARSPIN           154
//convRes: #define CNDE_DR_HASTOREACH         CNDE_FIRST+55
#define CNDE_DR_HASTOREACH         155
//convRes: #define CNDE_DR_PREV               CNDE_FIRST+56
#define CNDE_DR_PREV               156
//convRes: #define CNDE_DR_NEXT               CNDE_FIRST+57
#define CNDE_DR_NEXT               157
//convRes: #define CNDE_DR_NEW                CNDE_FIRST+58
#define CNDE_DR_NEW                158
//convRes: #define CNDE_DR_DELETE             CNDE_FIRST+59
#define CNDE_DR_DELETE             159

/*
 * NationAtWarPage
 */

//convRes: #define CNDE_NW_NATION             CNDE_FIRST+70
#define CNDE_NW_NATION             170
//convRes: #define CNDE_NW_ATWAR              CNDE_FIRST+71
#define CNDE_NW_ATWAR              171
//convRes: #define CNDE_NW_PREV               CNDE_FIRST+72
#define CNDE_NW_PREV               172
//convRes: #define CNDE_NW_NEXT               CNDE_FIRST+73
#define CNDE_NW_NEXT               173
//convRes: #define CNDE_NW_NEW                CNDE_FIRST+74
#define CNDE_NW_NEW                174
//convRes: #define CNDE_NW_DELETE             CNDE_FIRST+75
#define CNDE_NW_DELETE             175

/*
 * SideOwnsTownPage
 */

//convRes: #define CNDE_SOT_SIDE              CNDE_FIRST+80
#define CNDE_SOT_SIDE              180
//convRes: #define CNDE_SOT_TOWN              CNDE_FIRST+81
#define CNDE_SOT_TOWN              181
//convRes: #define CNDE_SOT_PREV              CNDE_FIRST+82
#define CNDE_SOT_PREV              182
//convRes: #define CNDE_SOT_NEXT              CNDE_FIRST+83
#define CNDE_SOT_NEXT              183
//convRes: #define CNDE_SOT_NEW               CNDE_FIRST+84
#define CNDE_SOT_NEW               184
//convRes: #define CNDE_SOT_DELETE            CNDE_FIRST+85
#define CNDE_SOT_DELETE            185

/*
 * ArmisticeEndsPage
 */

//convRes: #define CNDE_AE_PREV              CNDE_FIRST+90
#define CNDE_AE_PREV              190
//convRes: #define CNDE_AE_NEXT              CNDE_FIRST+91
#define CNDE_AE_NEXT              191
//convRes: #define CNDE_AE_NEW               CNDE_FIRST+92
#define CNDE_AE_NEW               192
//convRes: #define CNDE_AE_DELETE            CNDE_FIRST+93
#define CNDE_AE_DELETE            193

/*
 * Actions controls - SideHasVictoryPage
 */

//convRes: #define CNDE_ACTION_VP_WHOWINS     CNDE_FIRST+100
#define CNDE_ACTION_VP_WHOWINS     200
//convRes: #define CNDE_ACTION_VP_NEW         CNDE_FIRST+101
#define CNDE_ACTION_VP_NEW         201
//convRes: #define CNDE_ACTION_VP_DELETE      CNDE_FIRST+102
#define CNDE_ACTION_VP_DELETE      202

/*
 * ArmisticeReachedPage
 */

//convRes: #define CNDE_ACTION_AR_DAYS        CNDE_FIRST+110
#define CNDE_ACTION_AR_DAYS        210
//convRes: #define CNDE_ACTION_AR_NEW         CNDE_FIRST+111
#define CNDE_ACTION_AR_NEW         211
//convRes: #define CNDE_ACTION_AR_DELETE      CNDE_FIRST+112
#define CNDE_ACTION_AR_DELETE      212

/*
 * NationEntersWarPage
 */

//convRes: #define CNDE_ACTION_NE_NATION      CNDE_FIRST+120
#define CNDE_ACTION_NE_NATION      220
//convRes: #define CNDE_ACTION_NE_SIDE        CNDE_FIRST+121
#define CNDE_ACTION_NE_SIDE        221
//convRes: #define CNDE_ACTION_NE_ENTERS      CNDE_FIRST+122
#define CNDE_ACTION_NE_ENTERS      222
//convRes: #define CNDE_ACTION_NE_NEW         CNDE_FIRST+123
#define CNDE_ACTION_NE_NEW         223
//convRes: #define CNDE_ACTION_NE_DELETE      CNDE_FIRST+124
#define CNDE_ACTION_NE_DELETE      224

/*
 * ReinforcementsArrivePage
 */

//convRes: #define CNDE_ACTION_RA_UNIT        CNDE_FIRST+130
#define CNDE_ACTION_RA_UNIT        230
//convRes: #define CNDE_ACTION_RA_NEW         CNDE_FIRST+131
#define CNDE_ACTION_RA_NEW         231
//convRes: #define CNDE_ACTION_RA_DELETE      CNDE_FIRST+132
#define CNDE_ACTION_RA_DELETE      232

/*
 * LeaderExitsPage
 */

//convRes: #define CNDE_ACTION_LE_LEADER      CNDE_FIRST+140
#define CNDE_ACTION_LE_LEADER      240
//convRes: #define CNDE_ACTION_LE_NEW         CNDE_FIRST+141
#define CNDE_ACTION_LE_NEW         241
//convRes: #define CNDE_ACTION_LE_DELETE      CNDE_FIRST+142
#define CNDE_ACTION_LE_DELETE      242

/*
 * AllyDefectsPage
 */

//convRes: #define CNDE_ACTION_AD_NATION      CNDE_FIRST+150
#define CNDE_ACTION_AD_NATION      250
//convRes: #define CNDE_ACTION_AD_DEFECTS     CNDE_FIRST+151
#define CNDE_ACTION_AD_DEFECTS     251
//convRes: #define CNDE_ACTION_AD_NEW         CNDE_FIRST+152
#define CNDE_ACTION_AD_NEW         252
//convRes: #define CNDE_ACTION_AD_DELETE      CNDE_FIRST+153
#define CNDE_ACTION_AD_DELETE      253

/*
 * AdjustResourcesPage
 */

//convRes: #define CNDE_ACTION_ADR_NATION      CNDE_FIRST+155
#define CNDE_ACTION_ADR_NATION      255
//convRes: #define CNDE_ACTION_ADR_PERCENT     CNDE_FIRST+156
#define CNDE_ACTION_ADR_PERCENT     256
//convRes: #define CNDE_ACTION_ADR_NEW         CNDE_FIRST+157
#define CNDE_ACTION_ADR_NEW         257
//convRes: #define CNDE_ACTION_ADR_DELETE      CNDE_FIRST+158
#define CNDE_ACTION_ADR_DELETE      258

#if 0
//convRes: #define CNDE_ACTIONS_ARMISTICE     CNDE_FIRST+60
#define CNDE_ACTIONS_ARMISTICE     160
//convRes: #define CNDE_ACTIONS_NATIONENTERS  CNDE_FIRST+61
#define CNDE_ACTIONS_NATIONENTERS  161
//convRes: #define CNDE_ACTIONS_WHICHSIDE     CNDE_FIRST+62
#define CNDE_ACTIONS_WHICHSIDE     162
//convRes: #define CNDE_ACTIONS_NEVERENTERS   CNDE_FIRST+63
#define CNDE_ACTIONS_NEVERENTERS   163
//convRes: #define CNDE_ACTIONS_GAMEOVER      CNDE_FIRST+64
#define CNDE_ACTIONS_GAMEOVER      164
//convRes: #define CNDE_ACTIONS_WHOWINS       CNDE_FIRST+65
#define CNDE_ACTIONS_WHOWINS       165
#endif

/*
 * Conditions controls
 */

//convRes: #define CNDE_ALLCONDITIONS         CNDE_FIRST+160
#define CNDE_ALLCONDITIONS         260
//convRes: #define CNDE_PREV                  CNDE_FIRST+161
#define CNDE_PREV                  261
//convRes: #define CNDE_NEXT                  CNDE_FIRST+162
#define CNDE_NEXT                  262
//convRes: #define CNDE_NEW                   CNDE_FIRST+163
#define CNDE_NEW                   263
//convRes: #define CNDE_DELETE                CNDE_FIRST+164
#define CNDE_DELETE                264
//convRes: #define CNDE_OK                    CNDE_FIRST+165
#define CNDE_OK                    265
//convRes: #define CNDE_CONDITION_TABBED      CNDE_FIRST+166
#define CNDE_CONDITION_TABBED      266
//convRes: #define CNDE_ACTION_TABBED         CNDE_FIRST+167
#define CNDE_ACTION_TABBED         267
//convRes: #define CNDE_INSTANTUPDATEONLY     CNDE_FIRST+168
#define CNDE_INSTANTUPDATEONLY     268

/*
 * ArmisticeStartsPage
 */

//convRes: #define CNDE_AS_PREV              CNDE_FIRST+180
#define CNDE_AS_PREV              280
//convRes: #define CNDE_AS_NEXT              CNDE_FIRST+181
#define CNDE_AS_NEXT              281
//convRes: #define CNDE_AS_NEW               CNDE_FIRST+182
#define CNDE_AS_NEW               282
//convRes: #define CNDE_AS_DELETE            CNDE_FIRST+183
#define CNDE_AS_DELETE            283

/*
 * LeaderExitsPage
 */

//convRes: #define CNDE_ACTION_ENTER_LEADER      CNDE_FIRST+190
#define CNDE_ACTION_ENTER_LEADER      290
//convRes: #define CNDE_ACTION_ENTER_NEW         CNDE_FIRST+191
#define CNDE_ACTION_ENTER_NEW         291
//convRes: #define CNDE_ACTION_ENTER_DELETE      CNDE_FIRST+192
#define CNDE_ACTION_ENTER_DELETE      292

/*
 * Allegiance Editor
 */

//convRes: #define NAE_FIRST                  300
#define NAE_FIRST                  300
//convRes: #define NAE_NATIONLIST             NAE_FIRST
#define NAE_NATIONLIST             300
//convRes: #define NAE_NDEPOTS                NAE_FIRST+1
#define NAE_NDEPOTS                301
//convRes: #define NAE_NDEPOTSSPIN            NAE_FIRST+2
#define NAE_NDEPOTSSPIN            302
//convRes: #define NAE_LAST                   NAE_FIRST+3
#define NAE_LAST                   303

/*
 * Menu ids for Allegiance Editor popup menu
 */

//convRes: #define IDM_NAE_FIRST              NAE_LAST+1
#define IDM_NAE_FIRST              304
//convRes: #define IDM_NAE_SIDE0              IDM_NAE_FIRST
#define IDM_NAE_SIDE0              304
//convRes: #define IDM_NAE_SIDE1              IDM_NAE_FIRST+1
#define IDM_NAE_SIDE1              305
//convRes: #define IDM_NAE_SIDENEUTRAL        IDM_NAE_FIRST+2
#define IDM_NAE_SIDENEUTRAL        306
//convRes: #define IDM_NAE_LAST               IDM_NAE_FIRST+3
#define IDM_NAE_LAST               307

#endif  // CUSTOMIZE
// #endif // CAMPAIGN


// #if defined(CAMPAIGN)

/*
 * Print Map dialog
 */

#if defined(CUSTOMIZE)

//convRes: #define PM_FIRST                 100
#define PM_FIRST                 100
//convRes: #define PM_CAPITALS              PM_FIRST
#define PM_CAPITALS              100
//convRes: #define PM_CITIES                PM_FIRST+1
#define PM_CITIES                101
//convRes: #define PM_TOWNS                 PM_FIRST+2
#define PM_TOWNS                 102
//convRes: #define PM_OTHER                 PM_FIRST+3
#define PM_OTHER                 103
//convRes: #define PM_PROVINCENAMES         PM_FIRST+4
#define PM_PROVINCENAMES         104
//convRes: #define PM_TOWNNAMES             PM_FIRST+5
#define PM_TOWNNAMES             105
//convRes: #define PM_DOTMODE               PM_FIRST+6
#define PM_DOTMODE               106
//convRes: #define PM_CONNECTIONS           PM_FIRST+7
#define PM_CONNECTIONS           107
//convRes: #define PM_OFFSCREEN             PM_FIRST+8
#define PM_OFFSCREEN             108
//convRes: #define PM_PRINTSETUP            PM_FIRST+9
#define PM_PRINTSETUP            109
//convRes: #define PM_PRINT                 PM_FIRST+10
#define PM_PRINT                 110
//convRes: #define PM_CLOSE                 PM_FIRST+11
#define PM_CLOSE                 111
//convRes: #define PM_PROGRESS              PM_FIRST+12
#define PM_PROGRESS              112
//convRes: #define PM_LAST                  PM_FIRST+13
#define PM_LAST                  113

#endif


/*
 * Order of Battle Window
 */

#define IDOB_OK                                         IDOK
#define IDOB_CANCEL                                     IDCANCEL
//convRes: #define IDOB_DETACH              98
#define IDOB_DETACH              98
//convRes: #define IDOB_ORDERS              99
#define IDOB_ORDERS              99
//convRes: #define IDOB_TREEVIEW                           100
#define IDOB_TREEVIEW                           100
//convRes: #define IDOB_ILEADERS            101
#define IDOB_ILEADERS            101

/*
 * Unit Order Dialog Box
 */

//convRes: #define UD_SEND                         112
#define UD_SEND                         112
//convRes: #define UD_RESTORE                      113
#define UD_RESTORE                      113
//convRes: #define UD_DETACH                               114
#define UD_DETACH                               114
//convRes: #define UD_CLOSE                                115
#define UD_CLOSE                                115
//convRes: #define UD_CURRENTUNIT     116    // used in new tb class
#define UD_CURRENTUNIT     116 // used in new tb class
//convRes: #define UD_CURRENTUNIT_BOX 117
#define UD_CURRENTUNIT_BOX 117
//convRes: #define UD_TOOLTIP         118
#define UD_TOOLTIP         118

//convRes: #define UD_CURRENTORDER         120
#define UD_CURRENTORDER         120
//convRes: #define UD_MESSENGER                    121
#define UD_MESSENGER                    121
//convRes: #define UD_INTRAY                               122
#define UD_INTRAY                               122
//convRes: #define UD_UNITINFO                     123
#define UD_UNITINFO                     123
//convRes: #define UD_BASICINFO       124
#define UD_BASICINFO       124
//convRes: #define UD_ADDITIONALINFO  125
#define UD_ADDITIONALINFO  125

//convRes: #define UD_AGGRESSION           131
#define UD_AGGRESSION           131

//convRes: #define UD_ORDERTYPE                    132    // used in new tb class
#define UD_ORDERTYPE                    132 // used in new tb class

//convRes: #define UD_ATTACHSP        137
#define UD_ATTACHSP        137
//convRes: #define UD_LEADER          138
#define UD_LEADER          138
//convRes: #define UD_UNIT            139
#define UD_UNIT            139

//convRes: #define UD_DESTINATION          140
#define UD_DESTINATION          140
//convRes: #define UD_SETTOWN                      141
#define UD_SETTOWN                      141
//convRes: #define UD_SETUNIT                      142
#define UD_SETUNIT                      142
//convRes: #define UD_SETVIA                          143
#define UD_SETVIA                          143

//convRes: #define UD_TIMID           144
#define UD_TIMID           144
//convRes: #define UD_DEFEND          145
#define UD_DEFEND          145
//convRes: #define UD_ATTACKSMALL     146
#define UD_ATTACKSMALL     146
//convRes: #define UD_ATTACK          147
#define UD_ATTACK          147

//convRes: #define UD_VIA                                  150
#define UD_VIA                                  150
//convRes: #define UD_VIAFIRSTTOWN    151
#define UD_VIAFIRSTTOWN    151
//convRes: #define UD_VIASECONDTOWN   152
#define UD_VIASECONDTOWN   152
//convRes: #define UD_VIATHIRDTOWN    153
#define UD_VIATHIRDTOWN    153
//convRes: #define UD_FIRSTTOWNADD    154
#define UD_FIRSTTOWNADD    154
//convRes: #define UD_SECONDTOWNADD   155
#define UD_SECONDTOWNADD   155
//convRes: #define UD_THIRDTOWNADD    156
#define UD_THIRDTOWNADD    156
//convRes: #define UD_FIRSTTOWNDEL    157
#define UD_FIRSTTOWNDEL    157
//convRes: #define UD_SECONDTOWNDEL   158
#define UD_SECONDTOWNDEL   158
//convRes: #define UD_THIRDTOWNDEL    159
#define UD_THIRDTOWNDEL    159
//convRes: #define UD_DELALLVIAS      160
#define UD_DELALLVIAS      160
//convRes: #define UD_DESTTOWN        161
#define UD_DESTTOWN        161

//convRes: #define UD_MARCHTO         170   //
#define UD_MARCHTO         170 //
//convRes: #define UD_PURSUE          171   //
#define UD_PURSUE          171 //
//#define UD_FORCEMARCH      172   //
//#define UD_ALLSPEED        173   //
//convRes: #define UD_MOVEHOW         172
#define UD_MOVEHOW         172
//convRes: #define UD_ONARRIVAL       173
#define UD_ONARRIVAL       173
//convRes: #define UD_SIEGEACTIVE     174   //
#define UD_SIEGEACTIVE     174 //
//convRes: #define UD_DIGIN           175   //
#define UD_DIGIN           175 //
//convRes: #define UD_SPEED           176
#define UD_SPEED           176
//convRes: #define UD_SPEEDBOX        177
#define UD_SPEEDBOX        177
//convRes: #define UD_OPTIONS                      178
#define UD_OPTIONS                      178
//convRes: #define UD_AUTOSTORM       179
#define UD_AUTOSTORM       179

//convRes: #define UD_CURRENTLEADER   180
#define UD_CURRENTLEADER   180
//convRes: #define UD_ATTACHTOUNIT    181
#define UD_ATTACHTOUNIT    181
//convRes: #define UD_ATTACHTOTOWN    182
#define UD_ATTACHTOTOWN    182
//convRes: #define UD_OBWIN           183
#define UD_OBWIN           183
//convRes: #define UD_LEADERDEST      184
#define UD_LEADERDEST      184

//convRes: #define UD_ATTACHSPFROM     190
#define UD_ATTACHSPFROM     190
//convRes: #define UD_ATTACHSPTO       191
#define UD_ATTACHSPTO       191
//convRes: #define UD_SPFROM           192
#define UD_SPFROM           192
//convRes: #define UD_SPTO             193
#define UD_SPTO             193
//convRes: #define UD_ATTACHSPFROM_BTN 194
#define UD_ATTACHSPFROM_BTN 194
//convRes: #define UD_ATTACHSPTO_BTN   195
#define UD_ATTACHSPTO_BTN   195

/*
 * Town Dial
 */

//convRes: #define IDTC_OK                                 1
#define IDTC_OK                                 1
//convRes: #define IDTC_INFANTRY                   10
#define IDTC_INFANTRY                   10
//convRes: #define IDTC_CAVALRY                            11
#define IDTC_CAVALRY                            11
//convRes: #define IDTC_ARTILLERY                  12
#define IDTC_ARTILLERY                  12
//convRes: #define IDTC_SPECIAL                            13
#define IDTC_SPECIAL                            13
//convRes: #define IDTC_ENABLE                             14
#define IDTC_ENABLE                             14
//convRes: #define IDTC_AUTO                                       15
#define IDTC_AUTO                                       15
//convRes: #define IDTC_BUILD_WHAT                 16
#define IDTC_BUILD_WHAT                 16
//convRes: #define IDTC_PRIORITYFRAME              17
#define IDTC_PRIORITYFRAME              17
//convRes: #define IDTC_PRIORITY                   18
#define IDTC_PRIORITY                   18
//convRes: #define IDTC_QUALITYFRAME               19
#define IDTC_QUALITYFRAME               19
//convRes: #define IDTC_QUALITY                            20
#define IDTC_QUALITY                            20
//convRes: #define IDTC_PROGRESS                   21
#define IDTC_PROGRESS                   21
//convRes: #define IDTC_UNDO                                       22
#define IDTC_UNDO                                       22
//convRes: #define IDTC_APPLY                              23
#define IDTC_APPLY                              23
//convRes: #define IDTC_TOWNNAME         24
#define IDTC_TOWNNAME         24
//convRes: #define IDTC_TOWNINFO         25
#define IDTC_TOWNINFO         25


/*
 * Campaign Clock Dialog Window
 */

//convRes: #define CC_SPEED                                100
#define CC_SPEED                                100
//convRes: #define CC_RATE                         101
#define CC_RATE                         101
//convRes: #define CC_PAUSE                                102
#define CC_PAUSE                                102
//convRes: #define CC_SPEEDBOX                     103
#define CC_SPEEDBOX                     103
//convRes: #define CC_SPEEDFAST                    104
#define CC_SPEEDFAST                    104
//convRes: #define CC_SPEEDSLOW                    105
#define CC_SPEEDSLOW                    105
//convRes: #define CC_MONTH                                106
#define CC_MONTH                                106

//convRes: #define CC_DATE1                                110
#define CC_DATE1                                110
//convRes: #define CC_DATE2                                111
#define CC_DATE2                                111
//convRes: #define CC_DATE3                                112
#define CC_DATE3                                112
//convRes: #define CC_DATE4                                113
#define CC_DATE4                                113
//convRes: #define CC_DATE5                                114
#define CC_DATE5                                114
//convRes: #define CC_DATE6                                115
#define CC_DATE6                                115
//convRes: #define CC_DATE7                                116
#define CC_DATE7                                116
//convRes: #define CC_DATE8                                117
#define CC_DATE8                                117
//convRes: #define CC_DATE9                                118
#define CC_DATE9                                118
//convRes: #define CC_DATE10                               119
#define CC_DATE10                               119
//convRes: #define CC_DATE11                               120
#define CC_DATE11                               120
//convRes: #define CC_DATE12                               121
#define CC_DATE12                               121
//convRes: #define CC_DATE13                               122
#define CC_DATE13                               122
//convRes: #define CC_DATE14                               123
#define CC_DATE14                               123

/*
 * Message Window defines
 */
#if 0

//convRes: #define IDMW_PREV       1
#define IDMW_PREV       1
//convRes: #define IDMW_NEXT       2
#define IDMW_NEXT       2
//convRes: #define IDMW_DELETE     3
#define IDMW_DELETE     3
//convRes: #define IDMW_ORDER      4
#define IDMW_ORDER      4
//convRes: #define IDMW_DELETEALL  8
#define IDMW_DELETEALL  8
//convRes: #define IDMW_SETTINGS   9
#define IDMW_SETTINGS   9
//convRes: #define IDMW_DONTSHOW   10
#define IDMW_DONTSHOW   10
//convRes: #define IDMW_LISTBUTTON 11
#define IDMW_LISTBUTTON 11
//convRes: #define IDMW_LISTBOX    12
#define IDMW_LISTBOX    12
//convRes: #define IDMW_HIDE       13
#define IDMW_HIDE       13

#endif


/*
 * Player Options Dialog
 *
 * We really need to auto-generate these from the OptionsEnum
 * instead of hard wiring.
 * Adding new options is quite a chore!
 */

//convRes: #define POD_CANCEL             227
#define POD_CANCEL             227
//convRes: #define POD_OK                 228
#define POD_OK                 228
//convRes: #define POD_TABBED             250
#define POD_TABBED             250

#if 0
//convRes: #define POD_INSTANTORDERS      220
#define POD_INSTANTORDERS      220
//convRes: #define POD_INSTANTMOVE        221
#define POD_INSTANTMOVE        221
//convRes: #define POD_QUICKMOVE            222
#define POD_QUICKMOVE            222
//convRes: #define POD_FULLROUTE          223
#define POD_FULLROUTE          223
//convRes: #define POD_COLORCURSOR        224
#define POD_COLORCURSOR        224
//convRes: #define POD_VIEWENEMY          225
#define POD_VIEWENEMY          225
//convRes: #define POD_ORDERENEMY         226
#define POD_ORDERENEMY         226
//convRes: #define POD_TOOLTIPS           229
#define POD_TOOLTIPS           229
//convRes: #define POD_ALERTBOX           230
#define POD_ALERTBOX           230
//convRes: #define POD_TOWNNAMES          231
#define POD_TOWNNAMES          231
//convRes: #define POD_TIMEDORDER0        232
#define POD_TIMEDORDER0        232
//convRes: #define POD_TIMEDORDER1        233
#define POD_TIMEDORDER1        233
//convRes: #define POD_TIMEDORDER2        234
#define POD_TIMEDORDER2        234
//convRes: #define POD_AGRESSIONCHANGE      235
#define POD_AGRESSIONCHANGE      235
//convRes: #define POD_LIMITEDUNITINFO    236
#define POD_LIMITEDUNITINFO    236
//convRes: #define POD_LIMITEDTOWNINFO    237
#define POD_LIMITEDTOWNINFO    237
//convRes: #define POD_ArrivedAtTown      238
#define POD_ArrivedAtTown      238
//convRes: #define POD_NewUnitBuilt       239
#define POD_NewUnitBuilt       239
//convRes: #define POD_MetAtAndAwait      240
#define POD_MetAtAndAwait      240
//convRes: #define POD_TakenCommand       241
#define POD_TakenCommand       241
//convRes: #define POD_MetAndAwait        242
#define POD_MetAndAwait        242
//convRes: #define POD_Stranded           243
#define POD_Stranded           243
//convRes: #define POD_MovingInForBattle  244
#define POD_MovingInForBattle  244
//convRes: #define POD_HoldingBackAt8     245
#define POD_HoldingBackAt8     245
//convRes: #define POD_AvoidingContact    246
#define POD_AvoidingContact    246
//convRes: #define POD_PreparingForBattle 247
#define POD_PreparingForBattle 247
//convRes: #define POD_JoiningBattle      248
#define POD_JoiningBattle      248
//convRes: #define POD_EngagingInBattle   249
#define POD_EngagingInBattle   249
//convRes: #define POD_SHOWAI                               251
#define POD_SHOWAI                               251
//convRes: #define POD_SUPPLY                               252
#define POD_SUPPLY                               252
//convRes: #define POD_ATTRITION                    253
#define POD_ATTRITION                    253
//convRes: #define POD_NATIONRULES        254
#define POD_NATIONRULES        254
//convRes: #define POD_NATIONDISPLAY      255
#define POD_NATIONDISPLAY      255
//convRes: #define POD_TownIcons                    256
#define POD_TownIcons                    256
//convRes: #define POD_TownHilightIcons     257
#define POD_TownHilightIcons     257
#endif

// Alert Box Dialog

//convRes: #define AB_DISABLE             250
#define AB_DISABLE             250

// Find Town Dialog Strings

//convRes: #define FTD_LISTVIEW           260
#define FTD_LISTVIEW           260
//convRes: #define FTD_FIND               261
#define FTD_FIND               261
//convRes: #define FTD_OK                 262
#define FTD_OK                 262
//convRes: #define FTD_SEARCH             266
#define FTD_SEARCH             266
//convRes: #define FTD_IDM_FIND           267
#define FTD_IDM_FIND           267
//convRes: #define FTD_IDM_SORTCOL1       268
#define FTD_IDM_SORTCOL1       268
//convRes: #define FTD_IDM_SORTCOL2       269
#define FTD_IDM_SORTCOL2       269
//convRes: #define FTD_IDM_SORTCOL3       270
#define FTD_IDM_SORTCOL3       270


/*
 *  Shell Path Dialog
 */

//convRes: #define IDPD_TEXT              300
#define IDPD_TEXT              300
//convRes: #define IDPD_CANCEL            301
#define IDPD_CANCEL            301
//convRes: #define IDPD_BROWSE            302
#define IDPD_BROWSE            302

/*
 * Data Check Dialog
 */

#ifdef CUSTOMIZE
//convRes: #define DCD_FIRST              400
#define DCD_FIRST              400
//convRes: #define DCD_TEXT               DCD_FIRST
#define DCD_TEXT               400
//convRes: #define DCD_IGNORE             DCD_FIRST+1
#define DCD_IGNORE             401
//convRes: #define DCD_IGNOREALL          DCD_FIRST+2
#define DCD_IGNOREALL          402
//convRes: #define DCD_FIX                DCD_FIRST+3
#define DCD_FIX                403
//convRes: #define DCD_CANCEL             DCD_FIRST+4
#define DCD_CANCEL             404
//convRes: #define DCD_LAST               DCD_FIRST+5
#define DCD_LAST               405

#endif


/*
 * ID's for user order-interface
 */

// Unit Move-Order Dialog
//convRes: #define UMD_FIRST            100
#define UMD_FIRST            100
//convRes: #define UMD_UNITS            UMD_FIRST
#define UMD_UNITS            100
//convRes: #define UMD_UNITLIST         UMD_FIRST+1
#define UMD_UNITLIST         101
//convRes: #define UMD_BUTTONID_FIRST   UMD_FIRST+2
#define UMD_BUTTONID_FIRST   102
//convRes: #define UMD_SEND             UMD_BUTTONID_FIRST
#define UMD_SEND             102
//convRes: #define UMD_CANCEL           UMD_FIRST+3
#define UMD_CANCEL           103
//convRes: #define UMD_FULLDIAL         UMD_FIRST+4
#define UMD_FULLDIAL         104
//convRes: #define UMD_BUTTONID_LAST    UMD_FIRST+5
#define UMD_BUTTONID_LAST    105
//convRes: #define UMD_LAST             UMD_BUTTONID_LAST
#define UMD_LAST             105

// Unit Main-Interface Dialog
//convRes: #define UID_FIRST            100
#define UID_FIRST            100
//convRes: #define UID_FLAG             UID_FIRST
#define UID_FLAG             100
//convRes: #define UID_UNITS            UID_FIRST+1
#define UID_UNITS            101
//convRes: #define UID_UNITLIST         UID_FIRST+2
#define UID_UNITLIST         102
//convRes: #define UID_INFOTEXT         UID_FIRST+3
#define UID_INFOTEXT         103
//convRes: #define UID_BUTTONID_FIRST   UID_FIRST+4
#define UID_BUTTONID_FIRST   104
//convRes: #define UID_SHOWALL          UID_BUTTONID_FIRST
#define UID_SHOWALL          104
//convRes: #define UID_SEND             UID_FIRST+5
#define UID_SEND             105
//convRes: #define UID_CANCEL           UID_FIRST+6
#define UID_CANCEL           106
//convRes: #define UID_CLOSE            UID_FIRST+7
#define UID_CLOSE            107
//convRes: #define UID_BUTTONID_LAST    UID_FIRST+8
#define UID_BUTTONID_LAST    108
//convRes: #define UID_LAST             UID_BUTTONID_LAST
#define UID_LAST             108

// Unit Order Page   (tabbed page)
//convRes: #define UOP_FIRST            100
#define UOP_FIRST            100
//convRes: #define UOP_ORDERTYPE        UOP_FIRST
#define UOP_ORDERTYPE        100
//convRes: #define UOP_MOVEHOW          UOP_FIRST+1
#define UOP_MOVEHOW          101
//convRes: #define UOP_ORDERONARRIVAL   UOP_FIRST+2
#define UOP_ORDERONARRIVAL   102
//convRes: #define UOP_ORDERTEXT        UOP_FIRST+3
#define UOP_ORDERTEXT        103
//convRes: #define UOP_ORDERDATE        UOP_FIRST+4
#define UOP_ORDERDATE        104
//convRes: #define UOP_DESTINATION      UOP_FIRST+5
#define UOP_DESTINATION      105
//convRes: #define UOP_AGGRESSION       UOP_FIRST+6
#define UOP_AGGRESSION       106

//convRes: #define UOP_CHECKBOX_FIRST   UOP_FIRST+7
#define UOP_CHECKBOX_FIRST   107
//convRes: #define UOP_SOUNDGUNS        UOP_CHECKBOX_FIRST
#define UOP_SOUNDGUNS        107
//convRes: #define UOP_PURSUE           UOP_FIRST+8
#define UOP_PURSUE           108
//convRes: #define UOP_SIEGEACTIVE      UOP_FIRST+9
#define UOP_SIEGEACTIVE      109
//convRes: #define UOP_TOUNIT           UOP_FIRST+10
#define UOP_TOUNIT           110
//convRes: #define UOP_OFFENSIVE        UOP_FIRST+11
#define UOP_OFFENSIVE        111
//convRes: #define UOP_DEFENSIVE        UOP_FIRST+12
#define UOP_DEFENSIVE        112
//convRes: #define UOP_CHECKBOX_LAST    UOP_FIRST+13
#define UOP_CHECKBOX_LAST    113

//convRes: #define UOP_FIRSTCOMBO       UOP_CHECKBOX_LAST
#define UOP_FIRSTCOMBO       113
//convRes: #define UOP_MOVEHOWLIST      UOP_FIRSTCOMBO
#define UOP_MOVEHOWLIST      113
//convRes: #define UOP_ONARRIVALLIST    UOP_FIRST+14
#define UOP_ONARRIVALLIST    114
//convRes: #define UOP_ORDERTYPELIST    UOP_FIRST+15
#define UOP_ORDERTYPELIST    115
//convRes: #define UOP_AGGRESSIONLIST   UOP_FIRST+16
#define UOP_AGGRESSIONLIST   116
//convRes: #define UOP_LASTCOMBO        UOP_FIRST+17
#define UOP_LASTCOMBO        117
//convRes: #define UOP_LAST             UOP_FIRST+17
#define UOP_LAST             117

// Unit Activity Page  (tabbed page)
//convRes: #define UAP_FIRST            100
#define UAP_FIRST            100
//convRes: #define UAP_MODETIMETEXT     UAP_FIRST
#define UAP_MODETIMETEXT     100
//convRes: #define UAP_ACTIVITYTEXT     UAP_FIRST+1
#define UAP_ACTIVITYTEXT     101
//convRes: #define UAP_LAST                  UAP_FIRST+2
#define UAP_LAST                  102

// Unit Summary Page (tabbed page)
//convRes: #define USP_FIRST            100
#define USP_FIRST            100
//convRes: #define USP_SUMMARYTEXT      USP_FIRST
#define USP_SUMMARYTEXT      100
//convRes: #define USP_LAST             USP_FIRST+1
#define USP_LAST             101

// Unit Leader Page (tabbed page)
//convRes: #define ULP_FIRST            100
#define ULP_FIRST            100
//convRes: #define ULP_LEADERNAMETEXT   ULP_FIRST
#define ULP_LEADERNAMETEXT   100
//convRes: #define ULP_SHOWHELP         ULP_FIRST+1
#define ULP_SHOWHELP         101
//convRes: #define ULP_LEADERTEXT       ULP_FIRST+2
#define ULP_LEADERTEXT       102
//convRes: #define ULP_LAST             ULP_FIRST+3
#define ULP_LAST             103



/*
 * Town Dialog
 */

//convRes: #define TBD_FIRST            100
#define TBD_FIRST            100
//convRes: #define TBD_HEADERTEXT       TBD_FIRST
#define TBD_HEADERTEXT       100
//convRes: #define TBD_FIRSTBUTTON      TBD_FIRST+1
#define TBD_FIRSTBUTTON      101
//convRes: #define TBD_CANCEL           TBD_FIRSTBUTTON
#define TBD_CANCEL           101
//convRes: #define TBD_OK               TBD_FIRST+2
#define TBD_OK               102
//convRes: #define TBD_APPLY            TBD_FIRST+3
#define TBD_APPLY            103
//convRes: #define TBD_LASTBUTTON       TBD_FIRST+4
#define TBD_LASTBUTTON       104
//convRes: #define TBD_LAST             TBD_FIRST+4
#define TBD_LAST             104

// summary page
//convRes: #define TSP_FIRST            100
#define TSP_FIRST            100
//convRes: #define TSP_SUMMARYTEXT      TSP_FIRST
#define TSP_SUMMARYTEXT      100
//convRes: #define TSP_LAST             TSP_FIRST+1
#define TSP_LAST             101

// town page
//convRes: #define TTP_FIRST            100
#define TTP_FIRST            100
//convRes: #define TTP_SUMMARYTEXT      TTP_FIRST
#define TTP_SUMMARYTEXT      100
//convRes: #define TTP_LAST             TTP_FIRST+1
#define TTP_LAST             101

// status page
//convRes: #define TSTP_FIRST            100
#define TSTP_FIRST            100
//convRes: #define TSTP_SUMMARYTEXT      TSTP_FIRST
#define TSTP_SUMMARYTEXT      100
//convRes: #define TSTP_LAST             TSTP_FIRST+1
#define TSTP_LAST             101

// province page
//convRes: #define TPP_FIRST            100
#define TPP_FIRST            100
//convRes: #define TPP_SUMMARYTEXT      TPP_FIRST
#define TPP_SUMMARYTEXT      100
//convRes: #define TPP_LAST             TPP_FIRST+1
#define TPP_LAST             101

// build page
//convRes: #define TBP_FIRST            100
#define TBP_FIRST            100
//convRes: #define TBP_INFOTEXT         TBP_FIRST
#define TBP_INFOTEXT         100

//convRes: #define TBP_INFANTRY         TBP_FIRST+1
#define TBP_INFANTRY         101
//convRes: #define TBP_CAVALRY          TBP_FIRST+2
#define TBP_CAVALRY          102
//convRes: #define TBP_ARTILLERY        TBP_FIRST+3
#define TBP_ARTILLERY        103
//convRes: #define TBP_OTHER            TBP_FIRST+4
#define TBP_OTHER            104

//convRes: #define TBP_FIRSTPUSHBUTTON  TBP_FIRST+5
#define TBP_FIRSTPUSHBUTTON  105
//convRes: #define TBP_LESSINFANTRY     TBP_FIRSTPUSHBUTTON
#define TBP_LESSINFANTRY     105
//convRes: #define TBP_MOREINFANTRY     TBP_FIRST+6
#define TBP_MOREINFANTRY     106
//convRes: #define TBP_LESSCAVALRY      TBP_FIRST+7
#define TBP_LESSCAVALRY      107
//convRes: #define TBP_MORECAVALRY      TBP_FIRST+8
#define TBP_MORECAVALRY      108
//convRes: #define TBP_LESSARTILLERY    TBP_FIRST+9
#define TBP_LESSARTILLERY    109
//convRes: #define TBP_MOREARTILLERY    TBP_FIRST+10
#define TBP_MOREARTILLERY    110
//convRes: #define TBP_LESSOTHER        TBP_FIRST+11
#define TBP_LESSOTHER        111
//convRes: #define TBP_MOREOTHER        TBP_FIRST+12
#define TBP_MOREOTHER        112
//convRes: #define TBP_LASTPUSHBUTTON   TBP_FIRST+13
#define TBP_LASTPUSHBUTTON   113

//convRes: #define TBP_FIRSTCHECKBUTTON TBP_LASTPUSHBUTTON
#define TBP_FIRSTCHECKBUTTON 113
//convRes: #define TBP_AUTOINFANTRY     TBP_FIRSTCHECKBUTTON
#define TBP_AUTOINFANTRY     113
//convRes: #define TBP_AUTOCAVALRY      TBP_FIRST+14
#define TBP_AUTOCAVALRY      114
//convRes: #define TBP_AUTOARTILLERY    TBP_FIRST+15
#define TBP_AUTOARTILLERY    115
//convRes: #define TBP_AUTOOTHER        TBP_FIRST+16
#define TBP_AUTOOTHER        116
//convRes: #define TBP_LASTCHECKBUTTON  TBP_FIRST+17
#define TBP_LASTCHECKBUTTON  117

//convRes: #define TBP_LAST             TBP_FIRST+17
#define TBP_LAST             117

// Town and Unit Tracking dialog ids
//convRes: #define UTD_FIRST            100
#define UTD_FIRST            100
//convRes: #define UTD_INFO             UTD_FIRST
#define UTD_INFO             100
//convRes: #define UTD_LAST             UTD_FIRST+1
#define UTD_LAST             101

//convRes: #define TTD_FIRST            100
#define TTD_FIRST            100
//convRes: #define TTD_INFO             UTD_FIRST
#define TTD_INFO             100
//convRes: #define TTD_LAST             UTD_FIRST+1
#define TTD_LAST             101

/*
 * CampaignBattle Dialogs
 */
//convRes: #define EOBD_First           100
#define EOBD_First           100
//convRes: #define EOBD_Text            EOBD_First
#define EOBD_Text            100
//convRes: #define EOBD_FirstButton     EOBD_First+1
#define EOBD_FirstButton     101
//convRes: #define EOBD_Continue        EOBD_FirstButton
#define EOBD_Continue        101
//convRes: #define EOBD_Withdraw        EOBD_First+2
#define EOBD_Withdraw        102
//convRes: #define EOBD_LastButton      EOBD_First+3
#define EOBD_LastButton      103
//convRes: #define EOBD_Last            EOBD_First+3
#define EOBD_Last            103

#if 0
/*
 * Build-Item Dialog
 */

//convRes: #define BID_FIRST            100
#define BID_FIRST            100
//convRes: #define BID_HEADERTEXT       BID_FIRST
#define BID_HEADERTEXT       100
//convRes: #define BID_UNITTYPES        BID_FIRST+1
#define BID_UNITTYPES        101
//convRes: #define BID_LAST             BID_FIRST+2
#define BID_LAST             102
#endif



/*
 * Names of objects in file
 */

#define provinceEditDialogName      MAKEINTRESOURCE(DLG_EDIT_PROVINCE)
#define townEditDialogName          MAKEINTRESOURCE(DLG_EDIT_TOWN)
#define townEditGeneralPage         MAKEINTRESOURCE(DLG_TE_GENERAL)
#define townEditPositionPage        MAKEINTRESOURCE(DLG_TE_POSITION)
#define townEditResourcePage        MAKEINTRESOURCE(DLG_TE_RESOURCE)
//#define unitOrderDialog            MAKEINTRESOURCE(DLG_UNIT_ORDERS)
#define campaignClockDialog         MAKEINTRESOURCE(DLG_CAMPAIGN_TIME)
#define obEditDialogName            MAKEINTRESOURCE(DLG_EDIT_OB)
#define obEditAllUnitsPage          MAKEINTRESOURCE(DLG_OBE_UNITS)
#define obEditPositionPage          MAKEINTRESOURCE(DLG_OBE_POSITION)
#define obEditUnitPage              MAKEINTRESOURCE(DLG_OBE_UNIT)
#define obEditLeaderPage            MAKEINTRESOURCE(DLG_OBE_LEADER)
#define playerOptionsDlgName        MAKEINTRESOURCE(DLG_PLAYER_OPTIONS)
#define poOrdersDlgName             MAKEINTRESOURCE(DLG_PO_ORDERS)
#define poRealismDlgName            MAKEINTRESOURCE(DLG_PO_REALISM)
#define poMiscDlgName               MAKEINTRESOURCE(DLG_PO_MISC)
#define poShowMessageDlgName        MAKEINTRESOURCE(DLG_PO_SHOWMESSAGE)
#define alertBoxDlgName             MAKEINTRESOURCE(DLG_ALERTBOX)
#define findTownDlgName             MAKEINTRESOURCE(DLG_FINDTOWN)
#define pathDlgName                 MAKEINTRESOURCE(DLG_PATH)
// #define optionsScreenOptionsDlg     MAKEINTRESOURCE(DLG_OPENING_OPTIONS)
#define optionsScreenSettingsDlg    MAKEINTRESOURCE(DLG_OPENING_SETTINGS)

#if defined(CUSTOMIZE)
#define campaignInfoEditDlg         MAKEINTRESOURCE(DLG_EDITCAMPAIGNINFO)
#define conditionsEditDlg           MAKEINTRESOURCE(DLG_CONDITIONEDIT)
#define townChangePageDlg           MAKEINTRESOURCE(DLG_TOWNCHANGEPAGE)
#define casualtyPageDlg             MAKEINTRESOURCE(DLG_CASUALTYPAGE)
#define victoryPageDlg              MAKEINTRESOURCE(DLG_VICTORYPAGE)
#define neutralTownPageDlg          MAKEINTRESOURCE(DLG_NEUTRALTOWNPAGE)
#define deathOfLeaderPageDlg        MAKEINTRESOURCE(DLG_DEATHOFLEADERPAGE)
#define dateReachedPageDlg          MAKEINTRESOURCE(DLG_DATEREACHEDPAGE)
#define nationAtWarPageDlg          MAKEINTRESOURCE(DLG_NATIONATWARPAGE)
#define sideOwnsTownPageDlg         MAKEINTRESOURCE(DLG_SIDEOWNSTOWNPAGE)
#define armisticeEndsPageDlg        MAKEINTRESOURCE(DLG_ARMISTICEENDSPAGE)
#define actionsPageDlg              MAKEINTRESOURCE(DLG_ACTIONSPAGE)
#define allegianceEditDlg           MAKEINTRESOURCE(DLG_EDITALLEGIANCE)
#define actionsVictoryPageDlg       MAKEINTRESOURCE(DLG_ACTION_VICTORYPAGE)
#define actionsArmisticePageDlg     MAKEINTRESOURCE(DLG_ACTION_ARMISTICEPAGE)
#define actionsNationEntersPageDlg  MAKEINTRESOURCE(DLG_ACTION_NATIONENTERSPAGE)
#define actionsNewUnitsPageDlg      MAKEINTRESOURCE(DLG_ACTION_NEWUNITSPAGE)
#define actionsLeaderExitsPageDlg   MAKEINTRESOURCE(DLG_ACTION_LEADEREXITSPAGE)
#define actionsAllyDefectsPageDlg   MAKEINTRESOURCE(DLG_ACTION_ALLYDEFECTSPAGE)
#define actionsAdjustResourcesPageDlg   MAKEINTRESOURCE(DLG_ACTION_ADJUSTRESOURCESPAGE)
#endif       // CUSTOMIZE

#ifdef CUSTOMIZE
#define dataCheckDialog             MAKEINTRESOURCE(DLG_DATACHECK)
#endif      // DEBUG

#define moveOrderDialog                 MAKEINTRESOURCE(DLG_UNITMOVEDIAL)
#define unitOrderDialog                 MAKEINTRESOURCE(DLG_UNITDIALOG)
#define unitOrderPage                   MAKEINTRESOURCE(DLG_UNITORDERPAGE)
#define unitActivityPage                MAKEINTRESOURCE(DLG_UNITACTIVITYPAGE)
#define unitSummaryPage            MAKEINTRESOURCE(DLG_UNITSUMMARYPAGE)
#define unitLeaderPage                  MAKEINTRESOURCE(DLG_UNITLEADERPAGE)
#define unitOBPage                    MAKEINTRESOURCE(DLG_UNITOBPAGE)


#define townBuildDialog                 MAKEINTRESOURCE(DLG_TOWNBUILDDIAL)
#define townSummaryPage                 MAKEINTRESOURCE(DLG_TOWNSUMMARYPAGE)
#define townBuildPage                   MAKEINTRESOURCE(DLG_TOWNBUILDPAGE)
#define townStatusPage                  MAKEINTRESOURCE(DLG_TOWNSTATUSPAGE)
#define townTownPage                    MAKEINTRESOURCE(DLG_TOWNTOWNPAGE)
#define townProvincePage                MAKEINTRESOURCE(DLG_TOWNPROVINCEPAGE)

#define townOrderDialog                 MAKEINTRESOURCE(DLG_TOWN_ORDER)
#define townInfoDialog                  MAKEINTRESOURCE(DLG_TOWNINFO)
#define provinceInfoDialog              MAKEINTRESOURCE(DLG_PROVINCEINFO)
#define buildDialog                             MAKEINTRESOURCE(DLG_BUILDPAGE)
#define buildNewDialog                  MAKEINTRESOURCE(DLG_BUILDNEWPAGE)

#define unitTrackingDialog    MAKEINTRESOURCE(DLG_UNITTRACKINGDIAL)
#define townTrackingDialog    MAKEINTRESOURCE(DLG_TOWNTRACKINGDIAL)

#define endOfBattleDayDialog  MAKEINTRESOURCE(DLG_ENDOFBATTLEDAYDIAL)

#if defined(CUSTOMIZE)
#define printMapDialog        MAKEINTRESOURCE(DLG_PRINTMAP)
#endif

// #endif  // CAMPAIGN

// #if defined(BATTLE)

/*
 * Menus
 */


/*
 * Dialogs
 */

#define BattleDivOrderDialog                    MAKEINTRESOURCE(DLG_BATTLE_DIV_ORDER)
#define BattleMoveOrderDialog                   MAKEINTRESOURCE(DLG_BATTLEUNITMOVEDIAL)

/*
 * Bitmaps and Image Positions
 */

// #define BM_BATTLE_ZOOMICONS  100
// #define PR_BATTLE_ZOOMICONS  100
// #define NumBattleZoomImages  15

/*
 * ID's for user order-interface
 *
 * If only wrc supported enum, life would be much easier...
 */

// Unit Move-Order Dialog

//convRes: #define BUDM_FIRST            100
#define BUDM_FIRST            100
//convRes: #define BUDM_UNITS            BUDM_FIRST+0
#define BUDM_UNITS            100
//convRes: #define BUDM_SEND             BUDM_FIRST+1
#define BUDM_SEND             101
//convRes: #define BUDM_CANCEL           BUDM_FIRST+2
#define BUDM_CANCEL           102
//convRes: #define BUDM_FULLDIAL         BUDM_FIRST+3
#define BUDM_FULLDIAL         103
//convRes: #define BUDM_LAST             BUDM_FIRST+3
#define BUDM_LAST             103

// #define BUDM_UNITLIST         BUDM_FIRST+1
//convRes: #define BUDM_BUTTONID_FIRST   BUDM_SEND         // Used for a loop
#define BUDM_BUTTONID_FIRST   101 // Used for a loop
//convRes: #define BUDM_BUTTONID_LAST    BUDM_FULLDIAL
#define BUDM_BUTTONID_LAST    103

// Divisional Orders

//convRes: #define BDIV_FIRST                              100
#define BDIV_FIRST                              100
//convRes: #define BDIV_SEND                                       BDIV_FIRST+0
#define BDIV_SEND                                       100
//convRes: #define BDIV_CANCEL                             BDIV_FIRST+1
#define BDIV_CANCEL                             101
//convRes: #define BDIV_UNITS                              BDIV_FIRST+2
#define BDIV_UNITS                              102
//convRes: #define BDIV_SP_FORMATION               BDIV_FIRST+3
#define BDIV_SP_FORMATION               103
//convRes: #define BDIV_DIV_FORMATION              BDIV_FIRST+4
#define BDIV_DIV_FORMATION              104
//convRes: #define BDIV_LAST                                       BDIV_FIRST+5
#define BDIV_LAST                                       105

//convRes: #define BDIV_BUTTONID_FIRST   BDIV_SEND         // Used for a loop
#define BDIV_BUTTONID_FIRST   100 // Used for a loop
//convRes: #define BDIV_BUTTONID_LAST    BDIV_CANCEL
#define BDIV_BUTTONID_LAST    101


// #endif


//#define buildItemDialog       MAKEINTRESOURCE(DLG_BUILDITEMDIAL)
#ifdef __cplusplus
};
#endif

#endif /* RESOURCE_H */

/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.3  2001/06/26 19:06:20  greenius
 * Resources compile... and get someway into battle game before asserting
 *
 * Revision 1.2  2001/06/18 20:22:23  greenius
 * runBattle Project added, along with resources.  Compiles and almost runs!
 *
 *----------------------------------------------------------------------
 */
