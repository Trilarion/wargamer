# Microsoft Developer Studio Generated NMAKE File, Based on ResourceNap1813.dsp
!IF "$(CFG)" == ""
CFG=ResourceNap1813 - Win32 Debug
!MESSAGE No configuration specified. Defaulting to ResourceNap1813 - Win32 Debug.
!ENDIF 

!IF "$(CFG)" != "ResourceNap1813 - Win32 Release" && "$(CFG)" != "ResourceNap1813 - Win32 Debug"
!MESSAGE Invalid configuration "$(CFG)" specified.
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "ResourceNap1813.mak" CFG="ResourceNap1813 - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "ResourceNap1813 - Win32 Release" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE "ResourceNap1813 - Win32 Debug" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE 
!ERROR An invalid configuration is specified.
!ENDIF 

!IF "$(OS)" == "Windows_NT"
NULL=
!ELSE 
NULL=nul
!ENDIF 

CPP=cl.exe
MTL=midl.exe
RSC=rc.exe

!IF  "$(CFG)" == "ResourceNap1813 - Win32 Release"

OUTDIR=.\..\exe
INTDIR=.\o\Release
# Begin Custom Macros
OutDir=.\..\exe
# End Custom Macros

ALL : "$(OUTDIR)\nap1813\Nap1813res.dll"


CLEAN :
	-@erase "$(INTDIR)\nap1813.res"
	-@erase "$(INTDIR)\scn_dll.obj"
	-@erase "$(INTDIR)\vc60.idb"
	-@erase "$(INTDIR)\vc60.pdb"
	-@erase "$(OUTDIR)\nap1813\Nap1813res.dll"
	-@erase "$(OUTDIR)\Nap1813res.exp"
	-@erase "$(OUTDIR)\Nap1813res.lib"
	-@erase "$(OUTDIR)\Nap1813res.pdb"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

"$(INTDIR)" :
    if not exist "$(INTDIR)/$(NULL)" mkdir "$(INTDIR)"

CPP_PROJ=/nologo /MD /W3 /Gi /GR /GX /Zi /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "RESOURCENAP1813_EXPORTS" /Fp"$(INTDIR)\ResourceNap1813.pch" /YX"stdinc.hpp" /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /c 
MTL_PROJ=/nologo /D "NDEBUG" /mktyplib203 /win32 
RSC_PROJ=/l 0x809 /fo"$(INTDIR)\nap1813.res" /i ".." /d "NDEBUG" 
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\ResourceNap1813.bsc" 
BSC32_SBRS= \
	
LINK32=link.exe
LINK32_FLAGS=kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /dll /incremental:no /pdb:"$(OUTDIR)\Nap1813res.pdb" /debug /machine:I386 /out:"$(OUTDIR)\nap1813\Nap1813res.dll" /implib:"$(OUTDIR)\Nap1813res.lib" 
LINK32_OBJS= \
	"$(INTDIR)\scn_dll.obj" \
	"$(INTDIR)\nap1813.res"

"$(OUTDIR)\nap1813\Nap1813res.dll" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

!ELSEIF  "$(CFG)" == "ResourceNap1813 - Win32 Debug"

OUTDIR=.\..\exe
INTDIR=.\o\Debug
# Begin Custom Macros
OutDir=.\..\exe
# End Custom Macros

ALL : "$(OUTDIR)\nap1813\Nap1813res.dll"


CLEAN :
	-@erase "$(INTDIR)\nap1813.res"
	-@erase "$(INTDIR)\scn_dll.obj"
	-@erase "$(INTDIR)\vc60.idb"
	-@erase "$(INTDIR)\vc60.pdb"
	-@erase "$(OUTDIR)\nap1813\Nap1813res.dll"
	-@erase "$(OUTDIR)\nap1813\Nap1813res.ilk"
	-@erase "$(OUTDIR)\Nap1813res.exp"
	-@erase "$(OUTDIR)\Nap1813res.lib"
	-@erase "$(OUTDIR)\Nap1813res.pdb"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

"$(INTDIR)" :
    if not exist "$(INTDIR)/$(NULL)" mkdir "$(INTDIR)"

CPP_PROJ=/nologo /MTd /W3 /Gm /Gi /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "RESOURCENAP1813_EXPORTS" /Fp"$(INTDIR)\ResourceNap1813.pch" /YX"stdinc.hpp" /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /GZ /c 
MTL_PROJ=/nologo /D "_DEBUG" /mktyplib203 /win32 
RSC_PROJ=/l 0x809 /fo"$(INTDIR)\nap1813.res" /i ".." /d "_DEBUG" 
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\ResourceNap1813.bsc" 
BSC32_SBRS= \
	
LINK32=link.exe
LINK32_FLAGS=kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /dll /incremental:yes /pdb:"$(OUTDIR)\Nap1813res.pdb" /debug /machine:I386 /out:"$(OUTDIR)\nap1813\Nap1813res.dll" /implib:"$(OUTDIR)\Nap1813res.lib" /pdbtype:sept 
LINK32_OBJS= \
	"$(INTDIR)\scn_dll.obj" \
	"$(INTDIR)\nap1813.res"

"$(OUTDIR)\nap1813\Nap1813res.dll" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

!ENDIF 

.c{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.c{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<


!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("ResourceNap1813.dep")
!INCLUDE "ResourceNap1813.dep"
!ELSE 
!MESSAGE Warning: cannot find "ResourceNap1813.dep"
!ENDIF 
!ENDIF 


!IF "$(CFG)" == "ResourceNap1813 - Win32 Release" || "$(CFG)" == "ResourceNap1813 - Win32 Debug"
SOURCE=.\nap1813.rc

"$(INTDIR)\nap1813.res" : $(SOURCE) "$(INTDIR)"
	$(RSC) $(RSC_PROJ) $(SOURCE)


SOURCE=.\scn_dll.cpp

"$(INTDIR)\scn_dll.obj" : $(SOURCE) "$(INTDIR)"



!ENDIF 

