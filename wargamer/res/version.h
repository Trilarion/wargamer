/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
#ifndef VERSION_H
#define VERSION_H
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 * Version definitions
 *
 *----------------------------------------------------------------------
 */

#include <winver.h>

// These four constants define the version numbers of the
// product

#include "localVersion.dat"

/* #define VER_MAJOR           0 */
/* #define VER_MINOR           0 */
/* #define VER_RELEASE         1 */
/* #define VER_BUILD           0 */

// These define the product details

#if defined(BATTLE) && defined(CAMPAIGN)
    #define VER_PRODNAME   "Wargamer"
    #if defined(DEBUG)
        #define VER_DEFAULT_FILENAME    "WargamDB.exe"
    #else
        #define VER_DEFAULT_FILENAME    "Wargamer.exe"
    #endif
#elif defined(CAMPAIGN) && defined(EDITOR)
    #define VER_PRODNAME    "Wargamer Campaign Editor"
    #if defined(DEBUG)
        #define VER_DEFAULT_FILENAME        "WGEditDB.exe"
    #else
        #define VER_DEFAULT_FILENAME        "WGEdit.exe"
    #endif
#elif defined(CAMPAIGN)
    #define VER_PRODNAME    "Wargamer Campaign"
    #if defined(DEBUG)
        #define VER_DEFAULT_FILENAME        "WGcampDB.exe"
    #else
        #define VER_DEFAULT_FILENAME        "Campaign.exe"
    #endif
#elif defined(BATTLE) && defined(EDITOR)
    #define VER_PRODNAME    "Wargamer Battle Editor"
    #if defined(DEBUG)
        #define VER_DEFAULT_FILENAME        "BatedDB.exe"
    #else
        #define VER_DEFAULT_FILENAME        "Bated.exe"
    #endif
#elif defined(BATTLE)
    #define VER_PRODNAME    "Wargamer Battle"
    #if defined(DEBUG)
        #define VER_DEFAULT_FILENAME        "BattleDB.exe"
    #else
        #define VER_DEFAULT_FILENAME        "Battle.exe"
    #endif
#else
    #define VER_PRODNAME    "Wargamer"
    #define VER_DEFAULT_FILENAME    "Wargamer"
#endif

#if !defined(VER_FILENAME)
#define VER_FILENAME VER_DEFAULT_FILENAME
#endif


#define VER_DESC            "Historical Wargame Series"
#define VER_COMPANY         "Empire Interactive"
#define VER_COPYRIGHT       "\251 1999 Empire Interactive"


// To convert a literal to a string form
#define _XSTR(x)         #x
#define _STR(x)        _XSTR(x)

// These macros build up the version information from the
// constants above.

#define VER_BASEPRODUCT     _STR(VER_MAJOR) "." _STR(VER_MINOR) "." _STR(VER_RELEASE) "."
#define VER_BASEFILE        _STR(VER_MAJOR) "." _STR(VER_MINOR) "." _STR(VER_RELEASE) "."
#define VER_DATE            __DATE__
#define VER_TIME            __TIME__


#define VER_BUILDS          _STR(VER_BUILD)

#ifdef _DLL
#define VER_TYPE                VFT_DLL
#else
#define VER_TYPE                VFT_APP
#endif

#define VER_FILEVERSION         VER_BASEFILE VER_BUILDS
#define VER_PRODVERSION         VER_BASEPRODUCT VER_BUILDS

#define VER_PLATFORM            "WIN32"
#define VER_OS                  VOS__WINDOWS32

/*
 * Dialog About Box Info
 */

#define DLG_ABOUTBOX AboutBox
#define DLG_ABOUTBOX_RESOURCE "AboutBox"
#define ID_ABOUT_VERSION      100
#define ID_ABOUT_WHO       101
#define ID_ABOUT_DATE         102
#define ID_ABOUT_TITLE          103
#define ID_ABOUT_COPYRIGHT      104
#define ID_ABOUT_OK             105



#endif /* VERSION_H */

/*
 *----------------------------------------------------------------------
 * $Log$
 *----------------------------------------------------------------------
 */
