# Microsoft Developer Studio Project File - Name="ResourceNap1813" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Dynamic-Link Library" 0x0102

CFG=ResourceNap1813 - Win32 Debug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "ResourceNap1813.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "ResourceNap1813.mak" CFG="ResourceNap1813 - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "ResourceNap1813 - Win32 Release" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE "ResourceNap1813 - Win32 Debug" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""
CPP=cl.exe
MTL=midl.exe
RSC=rc.exe

!IF  "$(CFG)" == "ResourceNap1813 - Win32 Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "..\exe"
# PROP Intermediate_Dir "o\Release"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MT /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "RESOURCENAP1813_EXPORTS" /YX /FD /c
# ADD CPP /nologo /MD /W3 /Gi /GR /GX /Zi /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "RESOURCENAP1813_EXPORTS" /YX"stdinc.hpp" /FD /c
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x809 /d "NDEBUG"
# ADD RSC /l 0x809 /i ".." /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /dll /machine:I386
# ADD LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /dll /debug /machine:I386 /out:"..\exe\nap1813\Nap1813res.dll"

!ELSEIF  "$(CFG)" == "ResourceNap1813 - Win32 Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "..\exe"
# PROP Intermediate_Dir "o\Debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MTd /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "RESOURCENAP1813_EXPORTS" /YX /FD /GZ /c
# ADD CPP /nologo /MTd /W3 /Gm /Gi /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "RESOURCENAP1813_EXPORTS" /YX"stdinc.hpp" /FD /GZ /c
# ADD BASE MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x809 /d "_DEBUG"
# ADD RSC /l 0x809 /i ".." /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /dll /debug /machine:I386 /pdbtype:sept
# ADD LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /dll /debug /machine:I386 /out:"..\exe\nap1813\Nap1813res.dll" /pdbtype:sept

!ENDIF 

# Begin Target

# Name "ResourceNap1813 - Win32 Release"
# Name "ResourceNap1813 - Win32 Debug"
# Begin Group "Source Files"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat"
# Begin Source File

SOURCE=.\nap1813.rc
# End Source File
# Begin Source File

SOURCE=.\scn_dll.cpp
# End Source File
# End Group
# Begin Group "Header Files"

# PROP Default_Filter "h;hpp;hxx;hm;inl"
# Begin Source File

SOURCE=.\scn_res.h
# End Source File
# End Group
# Begin Group "Resource Files"

# PROP Default_Filter "ico;cur;bmp;dlg;rc2;rct;bin;rgs;gif;jpg;jpeg;jpe"
# Begin Source File

SOURCE=.\nap1813\AdvOrdrs.bmp
# End Source File
# Begin Source File

SOURCE=.\nap1813\aggresion.bmp
# End Source File
# Begin Source File

SOURCE=.\nap1813\AggressImages.bmp
# End Source File
# Begin Source File

SOURCE=.\nap1813\AllCities.bmp
# End Source File
# Begin Source File

SOURCE=.\nap1813\AlliedPaper.bmp
# End Source File
# Begin Source File

SOURCE=.\nap1813\attatch_unit.cur
# End Source File
# Begin Source File

SOURCE=.\nap1813\attatch_unit_m.cur
# End Source File
# Begin Source File

SOURCE=.\nap1813\AustrianFlags.bmp
# End Source File
# Begin Source File

SOURCE=.\res\nap1813\AustrianFlags.bmp
# End Source File
# Begin Source File

SOURCE=.\nap1813\bad_hex.cur
# End Source File
# Begin Source File

SOURCE=.\nap1813\bad_hex_m.cur
# End Source File
# Begin Source File

SOURCE=.\nap1813\BarChartBk.bmp
# End Source File
# Begin Source File

SOURCE=.\nap1813\BarChartFill.bmp
# End Source File
# Begin Source File

SOURCE=.\nap1813\BarvarianFlags.bmp
# End Source File
# Begin Source File

SOURCE=.\nap1813\baticon.bmp
# End Source File
# Begin Source File

SOURCE=.\nap1813\bfaceimages.bmp
# End Source File
# Begin Source File

SOURCE=.\nap1813\bldtype.bmp
# End Source File
# Begin Source File

SOURCE=.\nap1813\BToolbar.bmp
# End Source File
# Begin Source File

SOURCE=.\nap1813\BuildPageIcons.bmp
# End Source File
# Begin Source File

SOURCE=.\nap1813\Campaign.pal
# End Source File
# Begin Source File

SOURCE=.\nap1813\CampaignIcons.bmp
# End Source File
# Begin Source File

SOURCE=.\nap1813\CBattleAnimation.bmp
# End Source File
# Begin Source File

SOURCE=.\nap1813\CBattleAnimation1.bmp
# End Source File
# Begin Source File

SOURCE=.\nap1813\CClockIcons.bmp
# End Source File
# Begin Source File

SOURCE=.\nap1813\CheckButton.bmp
# End Source File
# Begin Source File

SOURCE=.\nap1813\Clear.bmp
# End Source File
# Begin Source File

SOURCE=.\nap1813\ComboButton.bmp
# End Source File
# Begin Source File

SOURCE=.\nap1813\CToolbar.bmp
# End Source File
# Begin Source File

SOURCE=.\nap1813\DanishFlags.bmp
# End Source File
# Begin Source File

SOURCE=.\nap1813\DotModeTImages.bmp
# End Source File
# Begin Source File

SOURCE=.\nap1813\drag_unit.cur
# End Source File
# Begin Source File

SOURCE=.\nap1813\drag_unit_m.cur
# End Source File
# Begin Source File

SOURCE=.\nap1813\facing_north.bmp
# End Source File
# Begin Source File

SOURCE=.\nap1813\facing_northeastpoint.bmp
# End Source File
# Begin Source File

SOURCE=.\nap1813\facing_northwestpoint.bmp
# End Source File
# Begin Source File

SOURCE=.\nap1813\facing_south.bmp
# End Source File
# Begin Source File

SOURCE=.\nap1813\facing_southeastpoint.bmp
# End Source File
# Begin Source File

SOURCE=.\nap1813\facing_southwestpoint.bmp
# End Source File
# Begin Source File

SOURCE=.\nap1813\FrenchFlags.bmp
# End Source File
# Begin Source File

SOURCE=.\nap1813\FrenchPaper.bmp
# End Source File
# Begin Source File

SOURCE=.\nap1813\GenericAlliedFlags.bmp
# End Source File
# Begin Source File

SOURCE=.\nap1813\hmapscrbtns.bmp
# End Source File
# Begin Source File

SOURCE=.\nap1813\hscrbk.bmp
# End Source File
# Begin Source File

SOURCE=.\nap1813\hthumbbk.bmp
# End Source File
# Begin Source File

SOURCE=.\nap1813\infoscrbtns.bmp
# End Source File
# Begin Source File

SOURCE=.\nap1813\invalid_target.cur
# End Source File
# Begin Source File

SOURCE=.\nap1813\invalid_target_m.cur
# End Source File
# Begin Source File

SOURCE=.\nap1813\ItalianFlags.bmp
# End Source File
# Begin Source File

SOURCE=.\nap1813\Lev1Hill.bmp
# End Source File
# Begin Source File

SOURCE=.\nap1813\Lev2Hill.bmp
# End Source File
# Begin Source File

SOURCE=.\nap1813\Lev3Hill.bmp
# End Source File
# Begin Source File

SOURCE=.\nap1813\Lev4Hill.bmp
# End Source File
# Begin Source File

SOURCE=.\nap1813\mapscrbtns.bmp
# End Source File
# Begin Source File

SOURCE=.\res\mapwin.ico
# End Source File
# Begin Source File

SOURCE=.\nap1813\menuimages.bmp
# End Source File
# Begin Source File

SOURCE=.\nap1813\MessageButtons.bmp
# End Source File
# Begin Source File

SOURCE=.\nap1813\moreinfo.bmp
# End Source File
# Begin Source File

SOURCE=.\nap1813\msgwin.bmp
# End Source File
# Begin Source File

SOURCE=.\nap1813\MsgWinHashed.bmp
# End Source File
# Begin Source File

SOURCE=.\nap1813\MsgWinSelect.bmp
# End Source File
# Begin Source File

SOURCE=.\nap1813\MsgWinStatic.bmp
# End Source File
# Begin Source File

SOURCE=.\nap1813\NapStartup.bmp
# End Source File
# Begin Source File

SOURCE=.\nap1813\NeutralPaper.bmp
# End Source File
# Begin Source File

SOURCE=.\nap1813\obicons.bmp
# End Source File
# Begin Source File

SOURCE=.\nap1813\OrderIcons.bmp
# End Source File
# Begin Source File

SOURCE=.\nap1813\over_unit.cur
# End Source File
# Begin Source File

SOURCE=.\res\nap1813\over_unit.cur
# End Source File
# Begin Source File

SOURCE=.\nap1813\over_unit_m.cur
# End Source File
# Begin Source File

SOURCE=.\res\nap1813\over_unit_m.cur
# End Source File
# Begin Source File

SOURCE=.\nap1813\PaperBk.bmp
# End Source File
# Begin Source File

SOURCE=.\nap1813\PolishFlags.bmp
# End Source File
# Begin Source File

SOURCE=.\nap1813\PopLev1.bmp
# End Source File
# Begin Source File

SOURCE=.\nap1813\PopLev2.bmp
# End Source File
# Begin Source File

SOURCE=.\nap1813\PopLev3.bmp
# End Source File
# Begin Source File

SOURCE=.\nap1813\PopLev4.bmp
# End Source File
# Begin Source File

SOURCE=.\nap1813\PrussianFlags.bmp
# End Source File
# Begin Source File

SOURCE=.\nap1813\RainyDry.bmp
# End Source File
# Begin Source File

SOURCE=.\nap1813\RainyMuddy.bmp
# End Source File
# Begin Source File

SOURCE=.\nap1813\Rough.bmp
# End Source File
# Begin Source File

SOURCE=.\nap1813\RussianFlags.bmp
# End Source File
# Begin Source File

SOURCE=.\nap1813\SaxonFlags.bmp
# End Source File
# Begin Source File

SOURCE=.\nap1813\Scale.bmp
# End Source File
# Begin Source File

SOURCE=.\nap1813\SmallTownImage.bmp
# End Source File
# Begin Source File

SOURCE=.\nap1813\spdicons.bmp
# End Source File
# Begin Source File

SOURCE=.\nap1813\SpecialistIcons.bmp
# End Source File
# Begin Source File

SOURCE=.\nap1813\StormyDry.bmp
# End Source File
# Begin Source File

SOURCE=.\nap1813\StormyMuddy.bmp
# End Source File
# Begin Source File

SOURCE=.\nap1813\SunnyDry.bmp
# End Source File
# Begin Source File

SOURCE=.\nap1813\SunnyMuddy.bmp
# End Source File
# Begin Source File

SOURCE=.\nap1813\SwedishFlags.bmp
# End Source File
# Begin Source File

SOURCE=.\nap1813\tabs.bmp
# End Source File
# Begin Source File

SOURCE=.\nap1813\target_mode.cur
# End Source File
# Begin Source File

SOURCE=.\nap1813\target_mode_m.cur
# End Source File
# Begin Source File

SOURCE=.\nap1813\taskbar.bmp
# End Source File
# Begin Source File

SOURCE=.\nap1813\tbscroll.bmp
# End Source File
# Begin Source File

SOURCE=.\nap1813\TIImages.bmp
# End Source File
# Begin Source File

SOURCE=.\nap1813\ToolBoxButtons.bmp
# End Source File
# Begin Source File

SOURCE=.\nap1813\townmap.bmp
# End Source File
# Begin Source File

SOURCE=.\nap1813\TownOrders.bmp
# End Source File
# Begin Source File

SOURCE=.\nap1813\troopDeployButton.bmp
# End Source File
# Begin Source File

SOURCE=.\nap1813\valid_target.cur
# End Source File
# Begin Source File

SOURCE=.\nap1813\valid_target_m.cur
# End Source File
# Begin Source File

SOURCE=.\nap1813\vscrbk.bmp
# End Source File
# Begin Source File

SOURCE=.\nap1813\vthumbbk.bmp
# End Source File
# Begin Source File

SOURCE=.\res\WarGamer.ico
# End Source File
# Begin Source File

SOURCE=.\nap1813\weather.bmp
# End Source File
# Begin Source File

SOURCE=.\nap1813\WeatherButtons.bmp
# End Source File
# Begin Source File

SOURCE=.\nap1813\WestphFlags.bmp
# End Source File
# Begin Source File

SOURCE=.\nap1813\woodbk1.bmp
# End Source File
# Begin Source File

SOURCE=.\nap1813\Woody.bmp
# End Source File
# Begin Source File

SOURCE=.\nap1813\WurttenFlags.bmp
# End Source File
# Begin Source File

SOURCE=.\nap1813\zoom.cur
# End Source File
# Begin Source File

SOURCE=.\nap1813\zoom_m.cur
# End Source File
# Begin Source File

SOURCE=.\nap1813\ZoomButtons.bmp
# End Source File
# End Group
# End Target
# End Project
